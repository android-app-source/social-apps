.class public abstract LX/IIn;
.super LX/IIm;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V
    .locals 0

    .prologue
    .line 2562054
    iput-object p1, p0, LX/IIn;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-direct {p0}, LX/IIm;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2562014
    iget-object v0, p0, LX/IIn;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->p:LX/IG7;

    sget-object v1, LX/IG6;->DASHBOARD_TTI:LX/IG6;

    invoke-virtual {v0, v1}, LX/IG7;->c(LX/IG6;)V

    .line 2562015
    iget-object v0, p0, LX/IIn;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    const/4 v1, 0x1

    .line 2562016
    iput-boolean v1, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ae:Z

    .line 2562017
    iget-object v0, p0, LX/IIn;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, p0, LX/IIn;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ay:LX/IIm;

    .line 2562018
    iget-boolean v2, v0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v2, v2

    .line 2562019
    if-eqz v2, :cond_0

    .line 2562020
    invoke-static {v0, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->a$redex1(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;LX/IIm;)V

    .line 2562021
    :goto_0
    return-void

    .line 2562022
    :cond_0
    iget-object v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->p:LX/IG7;

    sget-object p0, LX/IG6;->DASHBOARD_TTI:LX/IG6;

    invoke-virtual {v2, p0}, LX/IG7;->c(LX/IG6;)V

    .line 2562023
    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ae:Z

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 2562044
    iget-object v0, p0, LX/IIn;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->p:LX/IG7;

    sget-object v1, LX/IG6;->DASHBOARD_TTI:LX/IG6;

    invoke-virtual {v0, v1}, LX/IG7;->c(LX/IG6;)V

    .line 2562045
    iget-object v0, p0, LX/IIn;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    const/4 v1, 0x1

    .line 2562046
    iput-boolean v1, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ae:Z

    .line 2562047
    iget-object v0, p0, LX/IIn;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, p0, LX/IIn;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->az:LX/IIm;

    .line 2562048
    iget-boolean v2, v0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v2, v2

    .line 2562049
    if-eqz v2, :cond_0

    .line 2562050
    invoke-static {v0, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->a$redex1(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;LX/IIm;)V

    .line 2562051
    :goto_0
    return-void

    .line 2562052
    :cond_0
    iget-object v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->p:LX/IG7;

    sget-object p0, LX/IG6;->DASHBOARD_TTI:LX/IG6;

    invoke-virtual {v2, p0}, LX/IG7;->c(LX/IG6;)V

    .line 2562053
    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ae:Z

    goto :goto_0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 2562035
    iget-object v0, p0, LX/IIn;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->n:LX/IID;

    .line 2562036
    const-string v1, "friends_nearby_dashboard_settings"

    invoke-static {v0, v1}, LX/IID;->i(LX/IID;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 2562037
    iget-object v2, v0, LX/IID;->a:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2562038
    iget-object v0, p0, LX/IIn;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    .line 2562039
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ae:Z

    .line 2562040
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2562041
    sget-object v2, LX/0ax;->eb:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2562042
    iget-object v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->k:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-interface {v2, v1, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2562043
    return-void
.end method

.method public d()V
    .locals 0

    .prologue
    .line 2562034
    return-void
.end method

.method public e()V
    .locals 3

    .prologue
    .line 2562024
    iget-object v0, p0, LX/IIn;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->p:LX/IG7;

    sget-object v1, LX/IG6;->DASHBOARD_TTI:LX/IG6;

    invoke-virtual {v0, v1}, LX/IG7;->c(LX/IG6;)V

    .line 2562025
    iget-object v0, p0, LX/IIn;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    const/4 v1, 0x1

    .line 2562026
    iput-boolean v1, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ae:Z

    .line 2562027
    iget-object v0, p0, LX/IIn;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, p0, LX/IIn;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aI:LX/IIm;

    .line 2562028
    iget-boolean v2, v0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v2, v2

    .line 2562029
    if-eqz v2, :cond_0

    .line 2562030
    invoke-static {v0, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->a$redex1(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;LX/IIm;)V

    .line 2562031
    :goto_0
    return-void

    .line 2562032
    :cond_0
    iget-object v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->p:LX/IG7;

    sget-object p0, LX/IG6;->DASHBOARD_TTI:LX/IG6;

    invoke-virtual {v2, p0}, LX/IG7;->c(LX/IG6;)V

    .line 2562033
    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ae:Z

    goto :goto_0
.end method
