.class public LX/HON;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/23P;

.field public final b:LX/0wM;

.field public final c:LX/H9D;

.field public final d:Landroid/content/Context;

.field public final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "LX/HOK;",
            ">;>;"
        }
    .end annotation
.end field

.field public f:LX/HOG;


# direct methods
.method public constructor <init>(LX/23P;LX/0wM;LX/H9D;Landroid/content/Context;)V
    .locals 1
    .param p3    # LX/H9D;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2459976
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2459977
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, LX/HON;->e:Ljava/util/Map;

    .line 2459978
    iput-object p1, p0, LX/HON;->a:LX/23P;

    .line 2459979
    iput-object p2, p0, LX/HON;->b:LX/0wM;

    .line 2459980
    iput-object p3, p0, LX/HON;->c:LX/H9D;

    .line 2459981
    iput-object p4, p0, LX/HON;->d:Landroid/content/Context;

    .line 2459982
    return-void
.end method

.method private e(I)LX/HOK;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2459983
    if-nez p1, :cond_0

    .line 2459984
    new-instance v0, LX/HOK;

    sget-object v1, LX/HOL;->DESCRIPTION_HEADER:LX/HOL;

    invoke-direct {v0, v1, v5, v5, v5}, LX/HOK;-><init>(LX/HOL;Ljava/lang/String;Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;LX/HA7;)V

    .line 2459985
    :goto_0
    return-object v0

    .line 2459986
    :cond_0
    const/4 v0, 0x1

    .line 2459987
    iget-object v1, p0, LX/HON;->e:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2459988
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2459989
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 2459990
    sub-int v4, p1, v2

    if-nez v4, :cond_1

    .line 2459991
    new-instance v0, LX/HOK;

    sget-object v2, LX/HOL;->CATEGORY_HEADER:LX/HOL;

    invoke-direct {v0, v2, v1, v5, v5}, LX/HOK;-><init>(LX/HOL;Ljava/lang/String;Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;LX/HA7;)V

    goto :goto_0

    .line 2459992
    :cond_1
    add-int/lit8 v1, v2, 0x1

    .line 2459993
    sub-int v2, p1, v1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-ge v2, v4, :cond_2

    .line 2459994
    sub-int v1, p1, v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HOK;

    goto :goto_0

    .line 2459995
    :cond_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/2addr v0, v1

    move v2, v0

    .line 2459996
    goto :goto_1

    .line 2459997
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot get ItemViewType for position: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2459998
    sget-object v0, LX/HOL;->DESCRIPTION_HEADER:LX/HOL;

    invoke-virtual {v0}, LX/HOL;->ordinal()I

    move-result v0

    if-ne p2, v0, :cond_0

    .line 2459999
    new-instance v0, LX/HOM;

    iget-object v1, p0, LX/HON;->d:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030edc

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, LX/HOM;-><init>(Landroid/view/View;)V

    .line 2460000
    :goto_0
    return-object v0

    .line 2460001
    :cond_0
    sget-object v0, LX/HOL;->CATEGORY_HEADER:LX/HOL;

    invoke-virtual {v0}, LX/HOL;->ordinal()I

    move-result v0

    if-ne p2, v0, :cond_1

    .line 2460002
    iget-object v0, p0, LX/HON;->d:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030edb

    invoke-virtual {v0, v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2460003
    iget-object v1, p0, LX/HON;->a:LX/23P;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 2460004
    new-instance v1, LX/HOJ;

    invoke-direct {v1, v0}, LX/HOJ;-><init>(Lcom/facebook/resources/ui/FbTextView;)V

    move-object v0, v1

    goto :goto_0

    .line 2460005
    :cond_1
    sget-object v0, LX/HOL;->ACTION_ITEM:LX/HOL;

    invoke-virtual {v0}, LX/HOL;->ordinal()I

    move-result v0

    if-ne p2, v0, :cond_2

    .line 2460006
    iget-object v0, p0, LX/HON;->d:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030ede

    invoke-virtual {v0, v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/listitem/FigListItem;

    .line 2460007
    invoke-static {v0}, LX/8FY;->a(Landroid/view/View;)V

    .line 2460008
    new-instance v1, LX/HOI;

    invoke-direct {v1, p0, v0}, LX/HOI;-><init>(LX/HON;Lcom/facebook/fig/listitem/FigListItem;)V

    move-object v0, v1

    goto :goto_0

    .line 2460009
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot create ViewHolder itemViewType: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(LX/1a1;I)V
    .locals 5

    .prologue
    .line 2460010
    invoke-direct {p0, p2}, LX/HON;->e(I)LX/HOK;

    move-result-object v0

    .line 2460011
    instance-of v1, p1, LX/HOJ;

    if-eqz v1, :cond_1

    .line 2460012
    check-cast p1, LX/HOJ;

    .line 2460013
    iget-object v1, v0, LX/HOK;->b:Ljava/lang/String;

    move-object v0, v1

    .line 2460014
    iget-object v1, p1, LX/HOJ;->l:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2460015
    :cond_0
    :goto_0
    return-void

    .line 2460016
    :cond_1
    instance-of v1, p1, LX/HOI;

    if-eqz v1, :cond_2

    .line 2460017
    check-cast p1, LX/HOI;

    .line 2460018
    iget-object v1, v0, LX/HOK;->d:LX/HA7;

    move-object v1, v1

    .line 2460019
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2460020
    iget-object v2, v0, LX/HOK;->c:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;

    move-object v2, v2

    .line 2460021
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2460022
    iget-object v2, p1, LX/HOI;->m:Lcom/facebook/fig/listitem/FigListItem;

    iget-object v3, p1, LX/HOI;->l:LX/HON;

    iget-object v3, v3, LX/HON;->b:LX/0wM;

    .line 2460023
    iget v4, v1, LX/HA7;->d:I

    move v4, v4

    .line 2460024
    iget-object p0, p1, LX/HOI;->l:LX/HON;

    iget-object p0, p0, LX/HON;->d:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const p2, 0x7f0a00a6

    invoke-virtual {p0, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p0

    invoke-virtual {v3, v4, p0}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2460025
    iget-object v2, v1, LX/HA7;->c:Ljava/lang/String;

    move-object v2, v2

    .line 2460026
    if-eqz v2, :cond_3

    .line 2460027
    iget-object v2, p1, LX/HOI;->m:Lcom/facebook/fig/listitem/FigListItem;

    .line 2460028
    iget-object v3, v1, LX/HA7;->c:Ljava/lang/String;

    move-object v1, v3

    .line 2460029
    invoke-virtual {v2, v1}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2460030
    :goto_1
    iget-object v1, p1, LX/HOI;->m:Lcom/facebook/fig/listitem/FigListItem;

    new-instance v2, LX/HOH;

    invoke-direct {v2, p1, v0}, LX/HOH;-><init>(LX/HOI;LX/HOK;)V

    invoke-virtual {v1, v2}, Lcom/facebook/fig/listitem/FigListItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2460031
    goto :goto_0

    .line 2460032
    :cond_2
    instance-of v0, p1, LX/HOM;

    if-nez v0, :cond_0

    .line 2460033
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot bind ViewHolder for position: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2460034
    :cond_3
    iget-object v2, p1, LX/HOI;->m:Lcom/facebook/fig/listitem/FigListItem;

    .line 2460035
    iget v3, v1, LX/HA7;->b:I

    move v1, v3

    .line 2460036
    invoke-virtual {v2, v1}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(I)V

    goto :goto_1
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2460037
    invoke-direct {p0, p1}, LX/HON;->e(I)LX/HOK;

    move-result-object v0

    .line 2460038
    iget-object p0, v0, LX/HOK;->a:LX/HOL;

    move-object v0, p0

    .line 2460039
    invoke-virtual {v0}, LX/HOL;->ordinal()I

    move-result v0

    return v0
.end method

.method public final ij_()I
    .locals 4

    .prologue
    .line 2460040
    const/4 v0, 0x1

    .line 2460041
    iget-object v1, p0, LX/HON;->e:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 2460042
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 2460043
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 2460044
    goto :goto_0

    .line 2460045
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method
