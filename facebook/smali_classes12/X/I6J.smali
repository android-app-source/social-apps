.class public final LX/I6J;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/FeedUnit;

.field public final synthetic b:Landroid/view/View;

.field public final synthetic c:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic d:LX/I6K;


# direct methods
.method public constructor <init>(LX/I6K;Lcom/facebook/graphql/model/FeedUnit;Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0

    .prologue
    .line 2537471
    iput-object p1, p0, LX/I6J;->d:LX/I6K;

    iput-object p2, p0, LX/I6J;->a:Lcom/facebook/graphql/model/FeedUnit;

    iput-object p3, p0, LX/I6J;->b:Landroid/view/View;

    iput-object p4, p0, LX/I6J;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 9

    .prologue
    .line 2537472
    iget-object v0, p0, LX/I6J;->d:LX/I6K;

    iget-object v0, v0, LX/I6K;->b:LX/I6L;

    iget-object v1, p0, LX/I6J;->a:Lcom/facebook/graphql/model/FeedUnit;

    iget-object v2, p0, LX/I6J;->b:Landroid/view/View;

    .line 2537473
    invoke-virtual {v0, v1, v2}, LX/1SX;->a(Lcom/facebook/graphql/model/FeedUnit;Landroid/view/View;)V

    .line 2537474
    iget-object v0, p0, LX/I6J;->d:LX/I6K;

    iget-object v1, p0, LX/I6J;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p0, LX/I6J;->b:Landroid/view/View;

    .line 2537475
    iget-object v3, v0, LX/I6K;->b:LX/I6L;

    const/4 v6, 0x0

    sget-object v7, Lcom/facebook/graphql/enums/StoryVisibility;->HIDDEN:Lcom/facebook/graphql/enums/StoryVisibility;

    const/4 v8, 0x1

    move-object v4, v1

    move-object v5, v2

    .line 2537476
    invoke-virtual/range {v3 .. v8}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/graphql/enums/StoryVisibility;Z)V

    .line 2537477
    const/4 v0, 0x1

    return v0
.end method
