.class public LX/I8m;
.super LX/1Cv;
.source ""


# instance fields
.field public a:LX/0g8;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:LX/IBq;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2542013
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 2542014
    return-void
.end method

.method private static a(Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 2542011
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, p1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2542012
    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2542010
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2541995
    iget-object v0, p0, LX/I8m;->a:LX/0g8;

    if-nez v0, :cond_0

    .line 2541996
    invoke-static {p3, v1}, LX/I8m;->a(Landroid/view/View;I)V

    .line 2541997
    :goto_0
    return-void

    .line 2541998
    :cond_0
    invoke-virtual {p5}, Landroid/view/ViewGroup;->getHeight()I

    move-result v2

    iget-object v0, p0, LX/I8m;->b:LX/IBq;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    sub-int/2addr v2, v0

    move v0, v1

    .line 2541999
    :goto_2
    iget-object v3, p0, LX/I8m;->a:LX/0g8;

    invoke-interface {v3}, LX/0g8;->p()I

    move-result v3

    if-ge v0, v3, :cond_4

    .line 2542000
    iget-object v3, p0, LX/I8m;->a:LX/0g8;

    invoke-interface {v3, v0}, LX/0g8;->e(I)Landroid/view/View;

    move-result-object v3

    .line 2542001
    if-eqz v3, :cond_3

    instance-of v4, v3, Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;

    if-eqz v4, :cond_3

    .line 2542002
    iget-object v0, p0, LX/I8m;->a:LX/0g8;

    iget-object v4, p0, LX/I8m;->a:LX/0g8;

    invoke-interface {v4}, LX/0g8;->p()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-interface {v0, v4}, LX/0g8;->e(I)Landroid/view/View;

    move-result-object v0

    .line 2542003
    if-nez v0, :cond_2

    move v0, v1

    .line 2542004
    :goto_3
    if-gez v0, :cond_5

    .line 2542005
    :goto_4
    invoke-static {p3, v1}, LX/I8m;->a(Landroid/view/View;I)V

    goto :goto_0

    .line 2542006
    :cond_1
    iget-object v0, p0, LX/I8m;->b:LX/IBq;

    invoke-virtual {v0}, LX/IBq;->c()I

    move-result v0

    goto :goto_1

    .line 2542007
    :cond_2
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v3

    sub-int/2addr v0, v3

    sub-int v0, v2, v0

    goto :goto_3

    .line 2542008
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2542009
    :cond_4
    invoke-static {p3, v1}, LX/I8m;->a(Landroid/view/View;I)V

    goto :goto_0

    :cond_5
    move v1, v0

    goto :goto_4
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2542015
    const/4 v0, 0x1

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2541994
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2541993
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2541992
    const/4 v0, 0x0

    return v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2541991
    const/4 v0, 0x1

    return v0
.end method
