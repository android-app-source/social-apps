.class public LX/IiS;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/IiS;


# instance fields
.field public a:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FDs;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Or;
    .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2Oe;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2604368
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2604369
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2604370
    iput-object v0, p0, LX/IiS;->a:LX/0Ot;

    .line 2604371
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2604372
    iput-object v0, p0, LX/IiS;->b:LX/0Ot;

    .line 2604373
    return-void
.end method

.method public static a(LX/0QB;)LX/IiS;
    .locals 6

    .prologue
    .line 2604353
    sget-object v0, LX/IiS;->d:LX/IiS;

    if-nez v0, :cond_1

    .line 2604354
    const-class v1, LX/IiS;

    monitor-enter v1

    .line 2604355
    :try_start_0
    sget-object v0, LX/IiS;->d:LX/IiS;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2604356
    if-eqz v2, :cond_0

    .line 2604357
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2604358
    new-instance v3, LX/IiS;

    invoke-direct {v3}, LX/IiS;-><init>()V

    .line 2604359
    const/16 v4, 0x142d

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x2739

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 p0, 0xce5

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    .line 2604360
    iput-object v4, v3, LX/IiS;->a:LX/0Ot;

    iput-object v5, v3, LX/IiS;->b:LX/0Ot;

    iput-object p0, v3, LX/IiS;->c:LX/0Or;

    .line 2604361
    move-object v0, v3

    .line 2604362
    sput-object v0, LX/IiS;->d:LX/IiS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2604363
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2604364
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2604365
    :cond_1
    sget-object v0, LX/IiS;->d:LX/IiS;

    return-object v0

    .line 2604366
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2604367
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
