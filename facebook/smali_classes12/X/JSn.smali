.class public final LX/JSn;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/JTY;

.field public final b:LX/JSe;

.field public final c:LX/JSl;

.field public d:I

.field public final e:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/JSe;LX/JTY;LX/JSl;ILcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/JSe;",
            "LX/JTY;",
            "LX/JSl;",
            "I",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2696019
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2696020
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JSe;

    iput-object v0, p0, LX/JSn;->b:LX/JSe;

    .line 2696021
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JTY;

    iput-object v0, p0, LX/JSn;->a:LX/JTY;

    .line 2696022
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JSl;

    iput-object v0, p0, LX/JSn;->c:LX/JSl;

    .line 2696023
    iput p4, p0, LX/JSn;->d:I

    .line 2696024
    iput-object p5, p0, LX/JSn;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2696025
    return-void
.end method
