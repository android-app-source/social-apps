.class public final LX/IHN;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2560079
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 2560080
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2560081
    :goto_0
    return v1

    .line 2560082
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2560083
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 2560084
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 2560085
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2560086
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 2560087
    const-string v5, "display_name"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2560088
    invoke-static {p0, p1}, LX/IHL;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 2560089
    :cond_2
    const-string v5, "region_object"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2560090
    invoke-static {p0, p1}, LX/IHM;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 2560091
    :cond_3
    const-string v5, "region_topic_ids"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2560092
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 2560093
    :cond_4
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2560094
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 2560095
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2560096
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2560097
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 2560098
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2560099
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2560100
    if-eqz v0, :cond_0

    .line 2560101
    const-string v1, "display_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2560102
    invoke-static {p0, v0, p2}, LX/IHL;->a(LX/15i;ILX/0nX;)V

    .line 2560103
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2560104
    if-eqz v0, :cond_1

    .line 2560105
    const-string v1, "region_object"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2560106
    invoke-static {p0, v0, p2}, LX/IHM;->a(LX/15i;ILX/0nX;)V

    .line 2560107
    :cond_1
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2560108
    if-eqz v0, :cond_2

    .line 2560109
    const-string v0, "region_topic_ids"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2560110
    invoke-virtual {p0, p1, v2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 2560111
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2560112
    return-void
.end method
