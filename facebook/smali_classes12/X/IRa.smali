.class public final LX/IRa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DRb;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)V
    .locals 0

    .prologue
    .line 2576560
    iput-object p1, p0, LX/IRa;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/DRa;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 2576561
    iget-object v0, p0, LX/IRa;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    sget-object v1, LX/DRa;->LEARNING_UNITS:LX/DRa;

    invoke-virtual {p1, v1}, LX/DRa;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 2576562
    iput-boolean v1, v0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aT:Z

    .line 2576563
    iget-object v0, p0, LX/IRa;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->k:LX/DNJ;

    iget-object v1, p0, LX/IRa;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    invoke-static {v1}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->B(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)Z

    move-result v1

    invoke-virtual {v0, v1}, LX/DNJ;->c(Z)V

    .line 2576564
    iget-object v0, p0, LX/IRa;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->am:LX/ISH;

    iget-object v1, p0, LX/IRa;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    iget-object v1, v1, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    iget-object v2, p0, LX/IRa;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    invoke-static {v2}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ab(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)Z

    move-result v2

    if-nez v2, :cond_0

    move v2, v5

    :goto_0
    move v4, v3

    invoke-virtual/range {v0 .. v5}, LX/ISH;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;ZZZZ)V

    .line 2576565
    iget-object v0, p0, LX/IRa;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->k()LX/0g8;

    move-result-object v0

    iget-object v1, p0, LX/IRa;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    iget-object v1, v1, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aL:LX/DRc;

    .line 2576566
    iget v2, v1, LX/DRc;->g:I

    move v1, v2

    .line 2576567
    iget-object v2, p0, LX/IRa;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    iget-object v2, v2, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aL:LX/DRc;

    invoke-virtual {v2}, LX/DRc;->a()I

    move-result v2

    invoke-interface {v0, v1, v2}, LX/0g8;->c(II)V

    .line 2576568
    return-void

    :cond_0
    move v2, v3

    .line 2576569
    goto :goto_0
.end method
