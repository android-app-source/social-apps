.class public LX/ImG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7GO;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final f:Ljava/lang/Object;


# instance fields
.field private final a:LX/ImC;

.field private final b:LX/Im5;

.field private final c:LX/ImD;

.field private final d:LX/7GP;

.field private final e:LX/7GK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/7GK",
            "<",
            "LX/Ipt;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2609221
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/ImG;->f:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/ImC;LX/Im5;LX/ImD;LX/7GP;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2609185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2609186
    new-instance v0, LX/ImF;

    invoke-direct {v0, p0}, LX/ImF;-><init>(LX/ImG;)V

    iput-object v0, p0, LX/ImG;->e:LX/7GK;

    .line 2609187
    iput-object p1, p0, LX/ImG;->a:LX/ImC;

    .line 2609188
    iput-object p2, p0, LX/ImG;->b:LX/Im5;

    .line 2609189
    iput-object p3, p0, LX/ImG;->c:LX/ImD;

    .line 2609190
    iput-object p4, p0, LX/ImG;->d:LX/7GP;

    .line 2609191
    return-void
.end method

.method public static a(LX/0QB;)LX/ImG;
    .locals 10

    .prologue
    .line 2609192
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2609193
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2609194
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2609195
    if-nez v1, :cond_0

    .line 2609196
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2609197
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2609198
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2609199
    sget-object v1, LX/ImG;->f:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2609200
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2609201
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2609202
    :cond_1
    if-nez v1, :cond_4

    .line 2609203
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2609204
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2609205
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2609206
    new-instance p0, LX/ImG;

    invoke-static {v0}, LX/ImC;->a(LX/0QB;)LX/ImC;

    move-result-object v1

    check-cast v1, LX/ImC;

    invoke-static {v0}, LX/Im5;->a(LX/0QB;)LX/Im5;

    move-result-object v7

    check-cast v7, LX/Im5;

    invoke-static {v0}, LX/ImD;->a(LX/0QB;)LX/ImD;

    move-result-object v8

    check-cast v8, LX/ImD;

    invoke-static {v0}, LX/7GP;->a(LX/0QB;)LX/7GP;

    move-result-object v9

    check-cast v9, LX/7GP;

    invoke-direct {p0, v1, v7, v8, v9}, LX/ImG;-><init>(LX/ImC;LX/Im5;LX/ImD;LX/7GP;)V

    .line 2609207
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2609208
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2609209
    if-nez v1, :cond_2

    .line 2609210
    sget-object v0, LX/ImG;->f:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ImG;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2609211
    :goto_1
    if-eqz v0, :cond_3

    .line 2609212
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2609213
    :goto_3
    check-cast v0, LX/ImG;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2609214
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2609215
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2609216
    :catchall_1
    move-exception v0

    .line 2609217
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2609218
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2609219
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2609220
    :cond_2
    :try_start_8
    sget-object v0, LX/ImG;->f:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ImG;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 2609184
    return-void
.end method

.method public final a(LX/Iq2;)V
    .locals 12

    .prologue
    .line 2609182
    iget-object v1, p0, LX/ImG;->d:LX/7GP;

    sget-object v2, LX/7GT;->PAYMENTS_QUEUE_TYPE:LX/7GT;

    iget-object v3, p1, LX/Iq2;->deltas:Ljava/util/List;

    iget-object v0, p1, LX/Iq2;->firstDeltaSeqId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iget-object v6, p0, LX/ImG;->e:LX/7GK;

    iget-object v7, p0, LX/ImG;->a:LX/ImC;

    iget-object v8, p0, LX/ImG;->b:LX/Im5;

    iget-object v9, p0, LX/ImG;->c:LX/ImD;

    sget-object v10, Lcom/facebook/fbtrace/FbTraceNode;->a:Lcom/facebook/fbtrace/FbTraceNode;

    move-object v11, p0

    invoke-virtual/range {v1 .. v11}, LX/7GP;->a(LX/7GT;Ljava/util/List;JLX/7GK;LX/7GH;LX/7Fx;LX/7GI;Lcom/facebook/fbtrace/FbTraceNode;LX/7GO;)LX/7GN;

    .line 2609183
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 2609181
    return-void
.end method
