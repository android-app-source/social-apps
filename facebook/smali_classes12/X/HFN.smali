.class public final LX/HFN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;)V
    .locals 0

    .prologue
    .line 2443474
    iput-object p1, p0, LX/HFN;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x968efdc

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2443475
    iget-object v1, p0, LX/HFN;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->e:LX/HEr;

    .line 2443476
    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->d(Landroid/view/View;)I

    move-result v2

    invoke-virtual {v1, v2}, LX/HEr;->e(I)Lcom/facebook/pages/common/pagecreation/graphql/PageAddressSearchQueryModels$PageAddressSearchQueryModel$StreetResultsModel$NodesModel;

    move-result-object v1

    .line 2443477
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2443478
    iget-object v2, p0, LX/HFN;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v2, v2, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->q:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v1}, Lcom/facebook/pages/common/pagecreation/graphql/PageAddressSearchQueryModels$PageAddressSearchQueryModel$StreetResultsModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/widget/text/BetterEditTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2443479
    iget-object v2, p0, LX/HFN;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v2, v2, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->p:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v1}, Lcom/facebook/pages/common/pagecreation/graphql/PageAddressSearchQueryModels$PageAddressSearchQueryModel$StreetResultsModel$NodesModel;->a()Lcom/facebook/pages/common/pagecreation/graphql/PageAddressSearchQueryModels$PageAddressSearchQueryModel$StreetResultsModel$NodesModel$CityModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/pages/common/pagecreation/graphql/PageAddressSearchQueryModels$PageAddressSearchQueryModel$StreetResultsModel$NodesModel$CityModel;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/widget/text/BetterEditTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2443480
    iget-object v2, p0, LX/HFN;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    invoke-virtual {v1}, Lcom/facebook/pages/common/pagecreation/graphql/PageAddressSearchQueryModels$PageAddressSearchQueryModel$StreetResultsModel$NodesModel;->a()Lcom/facebook/pages/common/pagecreation/graphql/PageAddressSearchQueryModels$PageAddressSearchQueryModel$StreetResultsModel$NodesModel$CityModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/pages/common/pagecreation/graphql/PageAddressSearchQueryModels$PageAddressSearchQueryModel$StreetResultsModel$NodesModel$CityModel;->j()Ljava/lang/String;

    move-result-object v3

    .line 2443481
    iput-object v3, v2, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->v:Ljava/lang/String;

    .line 2443482
    iget-object v2, p0, LX/HFN;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v2, v2, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->r:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v1}, Lcom/facebook/pages/common/pagecreation/graphql/PageAddressSearchQueryModels$PageAddressSearchQueryModel$StreetResultsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/facebook/widget/text/BetterEditTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2443483
    iget-object v1, p0, LX/HFN;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->t:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 2443484
    iget-object v1, p0, LX/HFN;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->d:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2443485
    const v1, -0x1c943630

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
