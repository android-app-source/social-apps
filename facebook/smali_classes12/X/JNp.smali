.class public LX/JNp;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookCoverPhotoComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JNp",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookCoverPhotoComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2686536
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2686537
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JNp;->b:LX/0Zi;

    .line 2686538
    iput-object p1, p0, LX/JNp;->a:LX/0Ot;

    .line 2686539
    return-void
.end method

.method public static a(LX/0QB;)LX/JNp;
    .locals 4

    .prologue
    .line 2686540
    const-class v1, LX/JNp;

    monitor-enter v1

    .line 2686541
    :try_start_0
    sget-object v0, LX/JNp;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2686542
    sput-object v2, LX/JNp;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2686543
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2686544
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2686545
    new-instance v3, LX/JNp;

    const/16 p0, 0x1f41

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JNp;-><init>(LX/0Ot;)V

    .line 2686546
    move-object v0, v3

    .line 2686547
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2686548
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JNp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2686549
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2686550
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 2686551
    check-cast p2, LX/JNo;

    .line 2686552
    iget-object v0, p0, LX/JNp;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookCoverPhotoComponentSpec;

    iget-object v1, p2, LX/JNo;->a:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;

    iget v2, p2, LX/JNo;->b:I

    const/4 p2, 0x2

    const/4 p0, 0x1

    .line 2686553
    if-eqz v1, :cond_0

    .line 2686554
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v3

    .line 2686555
    if-eqz v3, :cond_0

    .line 2686556
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLProfile;->r()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v3

    .line 2686557
    if-eqz v3, :cond_0

    .line 2686558
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v3

    .line 2686559
    if-eqz v3, :cond_0

    .line 2686560
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPhoto;->aS()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    .line 2686561
    if-eqz v3, :cond_0

    .line 2686562
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 2686563
    :goto_0
    move-object v3, v3

    .line 2686564
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    const v5, 0x7f020a3f

    invoke-interface {v4, v5}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v4

    iget-object v5, v0, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookCoverPhotoComponentSpec;->b:LX/1nu;

    invoke-virtual {v5, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v5

    invoke-virtual {v5, v3}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v3

    sget-object v5, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookCoverPhotoComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v5}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v3

    const/high16 v5, 0x40000000    # 2.0f

    invoke-virtual {v3, v5}, LX/1nw;->c(F)LX/1nw;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    invoke-interface {v3, v2}, LX/1Di;->g(I)LX/1Di;

    move-result-object v3

    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v4

    const v5, 0x7f020afc

    invoke-virtual {v4, v5}, LX/1o5;->h(I)LX/1o5;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    .line 2686565
    const v5, 0x1c24b27f

    const/4 v0, 0x0

    invoke-static {p1, v5, v0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v5, v5

    .line 2686566
    invoke-interface {v4, v5}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v4

    invoke-interface {v4, p0}, LX/1Di;->c(I)LX/1Di;

    move-result-object v4

    const v5, 0x7f0b2551

    invoke-interface {v4, p0, v5}, LX/1Di;->l(II)LX/1Di;

    move-result-object v4

    const v5, 0x7f0b2551

    invoke-interface {v4, p2, v5}, LX/1Di;->l(II)LX/1Di;

    move-result-object v4

    const v5, 0x7f0a058e

    invoke-interface {v4, v5}, LX/1Di;->x(I)LX/1Di;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    .line 2686567
    const v4, -0x78044746

    const/4 v5, 0x0

    invoke-static {p1, v4, v5}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v4, v4

    .line 2686568
    invoke-interface {v3, v4}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2686569
    return-object v0

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2686512
    invoke-static {}, LX/1dS;->b()V

    .line 2686513
    iget v0, p1, LX/1dQ;->b:I

    .line 2686514
    sparse-switch v0, :sswitch_data_0

    .line 2686515
    :goto_0
    return-object v2

    .line 2686516
    :sswitch_0
    check-cast p2, LX/3Ae;

    .line 2686517
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2686518
    check-cast v1, LX/JNo;

    .line 2686519
    iget-object v3, p0, LX/JNp;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookCoverPhotoComponentSpec;

    iget-object p1, v1, LX/JNo;->c:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;

    iget-object p2, v1, LX/JNo;->a:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;

    .line 2686520
    iget-object p0, v3, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookCoverPhotoComponentSpec;->c:LX/JNi;

    invoke-virtual {p0, p1, p2, v0}, LX/JNi;->a(Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;Landroid/view/View;)V

    .line 2686521
    goto :goto_0

    .line 2686522
    :sswitch_1
    iget-object v0, p1, LX/1dQ;->a:LX/1X1;

    .line 2686523
    check-cast v0, LX/JNo;

    .line 2686524
    iget-object v1, p0, LX/JNp;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookCoverPhotoComponentSpec;

    iget-object v3, v0, LX/JNo;->c:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;

    iget-object p1, v0, LX/JNo;->a:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;

    .line 2686525
    invoke-static {p1, v3}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object p2

    .line 2686526
    invoke-static {p2}, LX/17Q;->F(LX/0lF;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 2686527
    const/4 p0, 0x0

    .line 2686528
    :goto_1
    move-object p2, p0

    .line 2686529
    iget-object p0, v1, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookCoverPhotoComponentSpec;->e:LX/0Zb;

    invoke-interface {p0, p2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2686530
    iget-object p2, v1, Lcom/facebook/feedplugins/goodwill/cwff/ConnectWithFacebookCoverPhotoComponentSpec;->d:Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p2, v3, p0}, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->a(Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;Ljava/lang/String;)V

    .line 2686531
    goto :goto_0

    .line 2686532
    :cond_0
    new-instance p0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "connect_with_facebook_xout"

    invoke-direct {p0, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v0, "tracking"

    invoke-virtual {p0, v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string v0, "native_newsfeed"

    .line 2686533
    iput-object v0, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2686534
    move-object p0, p0

    .line 2686535
    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x78044746 -> :sswitch_0
        0x1c24b27f -> :sswitch_1
    .end sparse-switch
.end method
