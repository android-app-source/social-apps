.class public LX/HwE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0io;
.implements LX/0iv;
.implements LX/2rh;
.implements LX/5Qw;
.implements LX/2ri;
.implements LX/5Qx;
.implements LX/5Qy;
.implements LX/5Qz;
.implements LX/5R0;
.implements LX/5R1;
.implements LX/0j0;
.implements LX/0ik;
.implements LX/0il;
.implements LX/0j3;
.implements LX/0j4;
.implements LX/0j6;
.implements LX/0jA;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0io;",
        "LX/0iv;",
        "LX/2rh;",
        "LX/5Qw;",
        "LX/2ri;",
        "LX/5Qx;",
        "LX/5Qy;",
        "LX/5Qz;",
        "LX/5R0;",
        "LX/5R1;",
        "LX/0j0;",
        "LX/0ik",
        "<",
        "LX/HwE;",
        ">;",
        "LX/0il",
        "<",
        "LX/HwE;",
        ">;",
        "LX/0j3;",
        "LX/0j4;",
        "LX/0j6;",
        "LX/0jA;"
    }
.end annotation


# instance fields
.field private final a:LX/HwG;


# direct methods
.method public constructor <init>(LX/HwG;)V
    .locals 0
    .param p1    # LX/HwG;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2520356
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2520357
    iput-object p1, p0, LX/HwE;->a:LX/HwG;

    .line 2520358
    return-void
.end method


# virtual methods
.method public final C()Z
    .locals 1

    .prologue
    .line 2520328
    iget-object v0, p0, LX/HwE;->a:LX/HwG;

    invoke-virtual {v0}, LX/HwG;->a()Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;

    move-result-object v0

    .line 2520329
    iget-boolean p0, v0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->k:Z

    move v0, p0

    .line 2520330
    return v0
.end method

.method public final D()Z
    .locals 1

    .prologue
    .line 2520359
    iget-object v0, p0, LX/HwE;->a:LX/HwG;

    invoke-virtual {v0}, LX/HwG;->a()Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;

    move-result-object v0

    .line 2520360
    iget-boolean p0, v0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->l:Z

    move v0, p0

    .line 2520361
    return v0
.end method

.method public final H()Z
    .locals 1

    .prologue
    .line 2520362
    iget-object v0, p0, LX/HwE;->a:LX/HwG;

    invoke-virtual {v0}, LX/HwG;->a()Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;

    move-result-object v0

    .line 2520363
    iget-boolean p0, v0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->m:Z

    move v0, p0

    .line 2520364
    return v0
.end method

.method public final a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2520366
    return-object p0
.end method

.method public final b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2520365
    return-object p0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2520370
    iget-object v0, p0, LX/HwE;->a:LX/HwG;

    invoke-virtual {v0}, LX/HwG;->a()Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;

    move-result-object v0

    .line 2520371
    iget-boolean p0, v0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->e:Z

    move v0, p0

    .line 2520372
    return v0
.end method

.method public final getAttachments()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2520367
    iget-object v0, p0, LX/HwE;->a:LX/HwG;

    invoke-virtual {v0}, LX/HwG;->a()Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;

    move-result-object v0

    .line 2520368
    iget-object p0, v0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->a:LX/0Px;

    move-object v0, p0

    .line 2520369
    return-object v0
.end method

.method public final getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;
    .locals 1

    .prologue
    .line 2520350
    iget-object v0, p0, LX/HwE;->a:LX/HwG;

    invoke-virtual {v0}, LX/HwG;->a()Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;

    move-result-object v0

    .line 2520351
    iget-object p0, v0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-object v0, p0

    .line 2520352
    return-object v0
.end method

.method public final getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;
    .locals 1

    .prologue
    .line 2520353
    iget-object v0, p0, LX/HwE;->a:LX/HwG;

    invoke-virtual {v0}, LX/HwG;->a()Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;

    move-result-object v0

    .line 2520354
    iget-object p0, v0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->n:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-object v0, p0

    .line 2520355
    return-object v0
.end method

.method public final getSessionId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2520347
    iget-object v0, p0, LX/HwE;->a:LX/HwG;

    invoke-virtual {v0}, LX/HwG;->a()Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;

    move-result-object v0

    .line 2520348
    iget-object p0, v0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->o:Ljava/lang/String;

    move-object v0, p0

    .line 2520349
    return-object v0
.end method

.method public final getSlideshowData()Lcom/facebook/ipc/composer/model/ComposerSlideshowData;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2520346
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;
    .locals 1

    .prologue
    .line 2520343
    iget-object v0, p0, LX/HwE;->a:LX/HwG;

    invoke-virtual {v0}, LX/HwG;->a()Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;

    move-result-object v0

    .line 2520344
    iget-object p0, v0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->q:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-object v0, p0

    .line 2520345
    return-object v0
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 2520340
    iget-object v0, p0, LX/HwE;->a:LX/HwG;

    invoke-virtual {v0}, LX/HwG;->a()Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;

    move-result-object v0

    .line 2520341
    iget-boolean p0, v0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->f:Z

    move v0, p0

    .line 2520342
    return v0
.end method

.method public final s()Z
    .locals 1

    .prologue
    .line 2520337
    iget-object v0, p0, LX/HwE;->a:LX/HwG;

    invoke-virtual {v0}, LX/HwG;->a()Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;

    move-result-object v0

    .line 2520338
    iget-boolean p0, v0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->g:Z

    move v0, p0

    .line 2520339
    return v0
.end method

.method public final x()Z
    .locals 1

    .prologue
    .line 2520334
    iget-object v0, p0, LX/HwE;->a:LX/HwG;

    invoke-virtual {v0}, LX/HwG;->a()Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;

    move-result-object v0

    .line 2520335
    iget-boolean p0, v0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->h:Z

    move v0, p0

    .line 2520336
    return v0
.end method

.method public final y()Z
    .locals 1

    .prologue
    .line 2520331
    iget-object v0, p0, LX/HwE;->a:LX/HwG;

    invoke-virtual {v0}, LX/HwG;->a()Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;

    move-result-object v0

    .line 2520332
    iget-boolean p0, v0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->i:Z

    move v0, p0

    .line 2520333
    return v0
.end method

.method public final z()Z
    .locals 1

    .prologue
    .line 2520325
    iget-object v0, p0, LX/HwE;->a:LX/HwG;

    invoke-virtual {v0}, LX/HwG;->a()Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;

    move-result-object v0

    .line 2520326
    iget-boolean p0, v0, Lcom/facebook/composer/ui/underwood/model/ModalUnderwoodData;->j:Z

    move v0, p0

    .line 2520327
    return v0
.end method
