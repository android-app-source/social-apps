.class public LX/HfR;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public a:LX/1vg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/work/groupstab/SuggestededGroupBlacklister;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/HgN;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2491785
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2491786
    return-void
.end method

.method public static a(LX/0QB;)LX/HfR;
    .locals 7

    .prologue
    .line 2491787
    const-class v1, LX/HfR;

    monitor-enter v1

    .line 2491788
    :try_start_0
    sget-object v0, LX/HfR;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2491789
    sput-object v2, LX/HfR;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2491790
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2491791
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2491792
    new-instance p0, LX/HfR;

    invoke-direct {p0}, LX/HfR;-><init>()V

    .line 2491793
    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v3

    check-cast v3, LX/1vg;

    .line 2491794
    new-instance v6, Lcom/facebook/work/groupstab/SuggestededGroupBlacklister;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v5

    check-cast v5, LX/0aG;

    invoke-direct {v6, v4, v5}, Lcom/facebook/work/groupstab/SuggestededGroupBlacklister;-><init>(LX/1Ck;LX/0aG;)V

    .line 2491795
    move-object v4, v6

    .line 2491796
    check-cast v4, Lcom/facebook/work/groupstab/SuggestededGroupBlacklister;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v5

    check-cast v5, LX/0kL;

    invoke-static {v0}, LX/HgN;->a(LX/0QB;)LX/HgN;

    move-result-object v6

    check-cast v6, LX/HgN;

    .line 2491797
    iput-object v3, p0, LX/HfR;->a:LX/1vg;

    iput-object v4, p0, LX/HfR;->b:Lcom/facebook/work/groupstab/SuggestededGroupBlacklister;

    iput-object v5, p0, LX/HfR;->c:LX/0kL;

    iput-object v6, p0, LX/HfR;->d:LX/HgN;

    .line 2491798
    move-object v0, p0

    .line 2491799
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2491800
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/HfR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2491801
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2491802
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
