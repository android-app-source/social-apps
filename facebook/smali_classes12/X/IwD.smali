.class public LX/IwD;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/IwD;


# instance fields
.field private final a:LX/0aG;

.field private final b:LX/3iV;


# direct methods
.method public constructor <init>(LX/0aG;LX/3iV;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2629564
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2629565
    iput-object p1, p0, LX/IwD;->a:LX/0aG;

    .line 2629566
    iput-object p2, p0, LX/IwD;->b:LX/3iV;

    .line 2629567
    return-void
.end method

.method public static a(LX/0QB;)LX/IwD;
    .locals 5

    .prologue
    .line 2629568
    sget-object v0, LX/IwD;->c:LX/IwD;

    if-nez v0, :cond_1

    .line 2629569
    const-class v1, LX/IwD;

    monitor-enter v1

    .line 2629570
    :try_start_0
    sget-object v0, LX/IwD;->c:LX/IwD;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2629571
    if-eqz v2, :cond_0

    .line 2629572
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2629573
    new-instance p0, LX/IwD;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v3

    check-cast v3, LX/0aG;

    invoke-static {v0}, LX/3iV;->a(LX/0QB;)LX/3iV;

    move-result-object v4

    check-cast v4, LX/3iV;

    invoke-direct {p0, v3, v4}, LX/IwD;-><init>(LX/0aG;LX/3iV;)V

    .line 2629574
    move-object v0, p0

    .line 2629575
    sput-object v0, LX/IwD;->c:LX/IwD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2629576
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2629577
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2629578
    :cond_1
    sget-object v0, LX/IwD;->c:LX/IwD;

    return-object v0

    .line 2629579
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2629580
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/IwD;Landroid/os/Parcelable;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Parcelable;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2629581
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2629582
    if-eqz p1, :cond_0

    .line 2629583
    invoke-virtual {v0, p2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2629584
    :cond_0
    iget-object v1, p0, LX/IwD;->a:LX/0aG;

    const v2, 0x6d1bac28

    invoke-static {v1, p3, v0, v2}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    return-object v0
.end method
