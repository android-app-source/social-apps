.class public final enum LX/HfC;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HfC;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/HfC;

.field public static final enum BLACKLIST_SUGGESTED_GROUP:LX/HfC;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2491418
    new-instance v0, LX/HfC;

    const-string v1, "BLACKLIST_SUGGESTED_GROUP"

    invoke-direct {v0, v1, v2}, LX/HfC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HfC;->BLACKLIST_SUGGESTED_GROUP:LX/HfC;

    .line 2491419
    const/4 v0, 0x1

    new-array v0, v0, [LX/HfC;

    sget-object v1, LX/HfC;->BLACKLIST_SUGGESTED_GROUP:LX/HfC;

    aput-object v1, v0, v2

    sput-object v0, LX/HfC;->$VALUES:[LX/HfC;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2491420
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HfC;
    .locals 1

    .prologue
    .line 2491421
    const-class v0, LX/HfC;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HfC;

    return-object v0
.end method

.method public static values()[LX/HfC;
    .locals 1

    .prologue
    .line 2491422
    sget-object v0, LX/HfC;->$VALUES:[LX/HfC;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HfC;

    return-object v0
.end method
