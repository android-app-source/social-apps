.class public final LX/HqZ;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/composer/activity/ComposerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/activity/ComposerFragment;)V
    .locals 0

    .prologue
    .line 2510604
    iput-object p1, p0, LX/HqZ;->a:Lcom/facebook/composer/activity/ComposerFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 2510607
    iget-object v0, p0, LX/HqZ;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    invoke-virtual {v0}, LX/Hre;->c()LX/0jJ;

    move-result-object v0

    sget-object v1, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    iget-object v1, p0, LX/HqZ;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v1, v1, Lcom/facebook/composer/activity/ComposerFragment;->K:LX/Hre;

    invoke-virtual {v1}, LX/Hre;->e()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->t()Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;->a(Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;)Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo$Builder;->setForceStopAutoTagging(Z)Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo$Builder;->a()Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0jL;->a(Lcom/facebook/composer/ui/tagging/ComposerAutoTagInfo;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 2510608
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2510609
    invoke-direct {p0}, LX/HqZ;->b()V

    .line 2510610
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2510605
    invoke-direct {p0}, LX/HqZ;->b()V

    .line 2510606
    return-void
.end method
