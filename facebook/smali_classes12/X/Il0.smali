.class public LX/Il0;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/73f;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/InQ;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field public final k:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final l:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final n:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;",
            ">;"
        }
    .end annotation
.end field

.field public final o:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final p:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final q:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final r:Z

.field public final s:Z


# direct methods
.method public constructor <init>(LX/Ikz;)V
    .locals 1

    .prologue
    .line 2607282
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2607283
    iget-object v0, p1, LX/Ikz;->a:LX/0am;

    iput-object v0, p0, LX/Il0;->a:LX/0am;

    .line 2607284
    iget-object v0, p1, LX/Ikz;->b:LX/0am;

    iput-object v0, p0, LX/Il0;->b:LX/0am;

    .line 2607285
    iget-object v0, p1, LX/Ikz;->c:LX/0am;

    iput-object v0, p0, LX/Il0;->c:LX/0am;

    .line 2607286
    iget-object v0, p1, LX/Ikz;->d:LX/0am;

    iput-object v0, p0, LX/Il0;->d:LX/0am;

    .line 2607287
    iget-object v0, p1, LX/Ikz;->e:LX/0am;

    iput-object v0, p0, LX/Il0;->e:LX/0am;

    .line 2607288
    iget-object v0, p1, LX/Ikz;->f:LX/0am;

    iput-object v0, p0, LX/Il0;->f:LX/0am;

    .line 2607289
    iget-object v0, p1, LX/Ikz;->g:LX/0am;

    iput-object v0, p0, LX/Il0;->g:LX/0am;

    .line 2607290
    iget-object v0, p1, LX/Ikz;->h:LX/0am;

    iput-object v0, p0, LX/Il0;->h:LX/0am;

    .line 2607291
    iget-object v0, p1, LX/Ikz;->i:LX/0am;

    iput-object v0, p0, LX/Il0;->i:LX/0am;

    .line 2607292
    iget-object v0, p1, LX/Ikz;->j:LX/0am;

    iput-object v0, p0, LX/Il0;->j:LX/0am;

    .line 2607293
    iget-object v0, p1, LX/Ikz;->k:LX/0am;

    iput-object v0, p0, LX/Il0;->k:LX/0am;

    .line 2607294
    iget-object v0, p1, LX/Ikz;->l:LX/0am;

    iput-object v0, p0, LX/Il0;->l:LX/0am;

    .line 2607295
    iget-object v0, p1, LX/Ikz;->m:LX/0am;

    iput-object v0, p0, LX/Il0;->m:LX/0am;

    .line 2607296
    iget-object v0, p1, LX/Ikz;->n:LX/0am;

    iput-object v0, p0, LX/Il0;->n:LX/0am;

    .line 2607297
    iget-object v0, p1, LX/Ikz;->o:LX/0am;

    iput-object v0, p0, LX/Il0;->o:LX/0am;

    .line 2607298
    iget-object v0, p1, LX/Ikz;->p:LX/0am;

    iput-object v0, p0, LX/Il0;->p:LX/0am;

    .line 2607299
    iget-object v0, p1, LX/Ikz;->q:LX/0am;

    iput-object v0, p0, LX/Il0;->q:LX/0am;

    .line 2607300
    iget-boolean v0, p1, LX/Ikz;->r:Z

    iput-boolean v0, p0, LX/Il0;->r:Z

    .line 2607301
    iget-boolean v0, p1, LX/Ikz;->s:Z

    iput-boolean v0, p0, LX/Il0;->s:Z

    .line 2607302
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2607303
    if-eqz p1, :cond_0

    instance-of v2, p1, LX/Il0;

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    .line 2607304
    :cond_1
    :goto_0
    return v0

    .line 2607305
    :cond_2
    if-eq p0, p1, :cond_1

    .line 2607306
    check-cast p1, LX/Il0;

    .line 2607307
    iget-object v2, p0, LX/Il0;->a:LX/0am;

    .line 2607308
    iget-object v3, p1, LX/Il0;->a:LX/0am;

    move-object v3, v3

    .line 2607309
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/Il0;->b:LX/0am;

    .line 2607310
    iget-object v3, p1, LX/Il0;->b:LX/0am;

    move-object v3, v3

    .line 2607311
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/Il0;->c:LX/0am;

    .line 2607312
    iget-object v3, p1, LX/Il0;->c:LX/0am;

    move-object v3, v3

    .line 2607313
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/Il0;->d:LX/0am;

    .line 2607314
    iget-object v3, p1, LX/Il0;->d:LX/0am;

    move-object v3, v3

    .line 2607315
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/Il0;->e:LX/0am;

    .line 2607316
    iget-object v3, p1, LX/Il0;->e:LX/0am;

    move-object v3, v3

    .line 2607317
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/Il0;->f:LX/0am;

    .line 2607318
    iget-object v3, p1, LX/Il0;->f:LX/0am;

    move-object v3, v3

    .line 2607319
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/Il0;->g:LX/0am;

    .line 2607320
    iget-object v3, p1, LX/Il0;->g:LX/0am;

    move-object v3, v3

    .line 2607321
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/Il0;->h:LX/0am;

    .line 2607322
    iget-object v3, p1, LX/Il0;->h:LX/0am;

    move-object v3, v3

    .line 2607323
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/Il0;->i:LX/0am;

    .line 2607324
    iget-object v3, p1, LX/Il0;->i:LX/0am;

    move-object v3, v3

    .line 2607325
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/Il0;->j:LX/0am;

    .line 2607326
    iget-object v3, p1, LX/Il0;->j:LX/0am;

    move-object v3, v3

    .line 2607327
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/Il0;->k:LX/0am;

    .line 2607328
    iget-object v3, p1, LX/Il0;->k:LX/0am;

    move-object v3, v3

    .line 2607329
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/Il0;->l:LX/0am;

    .line 2607330
    iget-object v3, p1, LX/Il0;->l:LX/0am;

    move-object v3, v3

    .line 2607331
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/Il0;->m:LX/0am;

    .line 2607332
    iget-object v3, p1, LX/Il0;->m:LX/0am;

    move-object v3, v3

    .line 2607333
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/Il0;->n:LX/0am;

    .line 2607334
    iget-object v3, p1, LX/Il0;->n:LX/0am;

    move-object v3, v3

    .line 2607335
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/Il0;->o:LX/0am;

    .line 2607336
    iget-object v3, p1, LX/Il0;->o:LX/0am;

    move-object v3, v3

    .line 2607337
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/Il0;->p:LX/0am;

    .line 2607338
    iget-object v3, p1, LX/Il0;->p:LX/0am;

    move-object v3, v3

    .line 2607339
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/Il0;->q:LX/0am;

    .line 2607340
    iget-object v3, p1, LX/Il0;->q:LX/0am;

    move-object v3, v3

    .line 2607341
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, LX/Il0;->r:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 2607342
    iget-boolean v3, p1, LX/Il0;->r:Z

    move v3, v3

    .line 2607343
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, LX/Il0;->s:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 2607344
    iget-boolean v3, p1, LX/Il0;->s:Z

    move v3, v3

    .line 2607345
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_3
    move v0, v1

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2607346
    const/16 v0, 0x13

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/Il0;->a:LX/0am;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/Il0;->b:LX/0am;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, LX/Il0;->c:LX/0am;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, LX/Il0;->d:LX/0am;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, LX/Il0;->e:LX/0am;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, LX/Il0;->f:LX/0am;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, LX/Il0;->g:LX/0am;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, LX/Il0;->h:LX/0am;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p0, LX/Il0;->i:LX/0am;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p0, LX/Il0;->j:LX/0am;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p0, LX/Il0;->k:LX/0am;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p0, LX/Il0;->l:LX/0am;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p0, LX/Il0;->m:LX/0am;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget-object v2, p0, LX/Il0;->n:LX/0am;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    iget-object v2, p0, LX/Il0;->o:LX/0am;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    iget-object v2, p0, LX/Il0;->p:LX/0am;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    iget-object v2, p0, LX/Il0;->q:LX/0am;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    iget-boolean v2, p0, LX/Il0;->r:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x12

    iget-boolean v2, p0, LX/Il0;->s:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
