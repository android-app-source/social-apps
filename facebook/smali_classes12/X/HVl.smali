.class public final LX/HVl;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesInterfaces$FBCrowdsourcingPlaceQuestionsQuery;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;

.field public final synthetic b:Landroid/view/View;

.field public final synthetic c:LX/HVo;


# direct methods
.method public constructor <init>(LX/HVo;Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2475319
    iput-object p1, p0, LX/HVl;->c:LX/HVo;

    iput-object p2, p0, LX/HVl;->a:Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;

    iput-object p3, p0, LX/HVl;->b:Landroid/view/View;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2475320
    iget-object v0, p0, LX/HVl;->c:LX/HVo;

    iget-object v0, v0, LX/HVo;->b:LX/03V;

    sget-object v1, LX/HVo;->a:Ljava/lang/String;

    const-string v2, "Failed to fetch place questions"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2475321
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 2475322
    check-cast p1, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionsQueryModel;

    const/4 v1, 0x0

    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 2475323
    const/4 v3, 0x0

    .line 2475324
    invoke-virtual {p1}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionsQueryModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionsQueryModel$CrowdsourcingPlaceQuestionsDataModel;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionsQueryModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionsQueryModel$CrowdsourcingPlaceQuestionsDataModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionsQueryModel$CrowdsourcingPlaceQuestionsDataModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionsQueryModel$CrowdsourcingPlaceQuestionsDataModel$PlaceQuestionsModel;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionsQueryModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionsQueryModel$CrowdsourcingPlaceQuestionsDataModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionsQueryModel$CrowdsourcingPlaceQuestionsDataModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionsQueryModel$CrowdsourcingPlaceQuestionsDataModel$PlaceQuestionsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionsQueryModel$CrowdsourcingPlaceQuestionsDataModel$PlaceQuestionsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-nez v2, :cond_9

    :cond_0
    move-object v2, v3

    .line 2475325
    :cond_1
    :goto_0
    move-object v2, v2

    .line 2475326
    move-object v2, v2

    .line 2475327
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->gp_()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionTextModel;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v2}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->d()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-nez v0, :cond_3

    .line 2475328
    :cond_2
    iget-object v0, p0, LX/HVl;->c:LX/HVo;

    iget-object v0, v0, LX/HVo;->b:LX/03V;

    sget-object v1, LX/HVo;->a:Ljava/lang/String;

    const-string v2, "Place Question Context Row fetched invalid data"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2475329
    iget-object v0, p0, LX/HVl;->c:LX/HVo;

    iget-object v1, p0, LX/HVl;->a:Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;

    iget-wide v2, v1, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->a:J

    invoke-static {v0, v2, v3}, LX/HVo;->a$redex0(LX/HVo;J)V

    .line 2475330
    :goto_1
    return-void

    .line 2475331
    :cond_3
    invoke-virtual {v2}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->d()LX/0Px;

    move-result-object v3

    .line 2475332
    new-instance v4, LX/HVn;

    iget-object v0, p0, LX/HVl;->c:LX/HVo;

    iget-object v5, p0, LX/HVl;->b:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v0, v5}, LX/HVn;-><init>(LX/HVo;Landroid/content/Context;)V

    .line 2475333
    invoke-virtual {v2}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->gp_()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionTextModel;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {v2}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->gp_()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionTextModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionTextModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2475334
    :goto_2
    iget-object p1, v4, LX/HVn;->b:Landroid/widget/TextView;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_b

    const/16 v5, 0x8

    :goto_3
    invoke-virtual {p1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2475335
    iget-object v5, v4, LX/HVn;->b:Landroid/widget/TextView;

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2475336
    invoke-virtual {v2}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$CrowdsourcingPlaceQuestionValueModel;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-virtual {v2}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$CrowdsourcingPlaceQuestionValueModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$CrowdsourcingPlaceQuestionValueModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2475337
    :goto_4
    iget-object p1, v4, LX/HVn;->c:Landroid/widget/TextView;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_c

    const/16 v5, 0x8

    :goto_5
    invoke-virtual {p1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2475338
    iget-object v5, v4, LX/HVn;->c:Landroid/widget/TextView;

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2475339
    invoke-virtual {v2}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->e()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionSubtextModel;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v2}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->e()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionSubtextModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionSubtextModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 2475340
    :cond_4
    iget-object v5, v4, LX/HVn;->d:Landroid/widget/TextView;

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    const/16 v0, 0x8

    :goto_6
    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2475341
    iget-object v0, v4, LX/HVn;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2475342
    new-instance v1, LX/0ju;

    iget-object v0, p0, LX/HVl;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const v5, 0x7f0e06f6

    invoke-direct {v1, v0, v5}, LX/0ju;-><init>(Landroid/content/Context;I)V

    .line 2475343
    invoke-virtual {v1, v7}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v0

    invoke-virtual {v0, v4}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0, v6}, LX/0ju;->a(Z)LX/0ju;

    .line 2475344
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-lez v0, :cond_5

    invoke-virtual {v3, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionAnswersModel;

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionAnswersModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionAnswersModel$PlaceQuestionAnswerLabelModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 2475345
    invoke-virtual {v3, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionAnswersModel;

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionAnswersModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionAnswersModel$PlaceQuestionAnswerLabelModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionAnswersModel$PlaceQuestionAnswerLabelModel;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v4, LX/HVi;

    invoke-direct {v4, p0, v2, v3}, LX/HVi;-><init>(LX/HVl;Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;LX/0Px;)V

    invoke-virtual {v1, v0, v4}, LX/0ju;->c(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2475346
    :cond_5
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-le v0, v6, :cond_6

    invoke-virtual {v3, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionAnswersModel;

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionAnswersModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionAnswersModel$PlaceQuestionAnswerLabelModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 2475347
    invoke-virtual {v3, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionAnswersModel;

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionAnswersModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionAnswersModel$PlaceQuestionAnswerLabelModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionAnswersModel$PlaceQuestionAnswerLabelModel;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v4, LX/HVj;

    invoke-direct {v4, p0, v2, v3}, LX/HVj;-><init>(LX/HVl;Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;LX/0Px;)V

    invoke-virtual {v1, v0, v4}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2475348
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-le v0, v8, :cond_6

    invoke-virtual {v3, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionAnswersModel;

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionAnswersModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionAnswersModel$PlaceQuestionAnswerLabelModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 2475349
    invoke-virtual {v3, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionAnswersModel;

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionAnswersModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionAnswersModel$PlaceQuestionAnswerLabelModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionAnswersModel$PlaceQuestionAnswerLabelModel;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v4, LX/HVk;

    invoke-direct {v4, p0, v2, v3}, LX/HVk;-><init>(LX/HVl;Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;LX/0Px;)V

    invoke-virtual {v1, v0, v4}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2475350
    :cond_6
    invoke-virtual {v1}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    goto/16 :goto_1

    :cond_7
    move-object v0, v1

    .line 2475351
    goto/16 :goto_2

    :cond_8
    move-object v0, v1

    .line 2475352
    goto/16 :goto_4

    .line 2475353
    :cond_9
    invoke-virtual {p1}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionsQueryModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionsQueryModel$CrowdsourcingPlaceQuestionsDataModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionsQueryModel$CrowdsourcingPlaceQuestionsDataModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionsQueryModel$CrowdsourcingPlaceQuestionsDataModel$PlaceQuestionsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionsQueryModel$CrowdsourcingPlaceQuestionsDataModel$PlaceQuestionsModel;->a()LX/0Px;

    move-result-object v2

    const/4 v0, 0x0

    invoke-virtual {v2, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionsQueryModel$CrowdsourcingPlaceQuestionsDataModel$PlaceQuestionsModel$EdgesModel;

    invoke-virtual {v2}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionsQueryModel$CrowdsourcingPlaceQuestionsDataModel$PlaceQuestionsModel$EdgesModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;

    move-result-object v2

    .line 2475354
    if-eqz v2, :cond_a

    invoke-virtual {v2}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->gp_()Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel$PlaceQuestionTextModel;

    move-result-object v0

    if-eqz v0, :cond_a

    invoke-virtual {v2}, Lcom/facebook/crowdsourcing/protocol/graphql/CrowdsourcingPlaceQuestionQueriesModels$FBCrowdsourcingPlaceQuestionFragmentModel;->d()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-nez v0, :cond_1

    :cond_a
    move-object v2, v3

    .line 2475355
    goto/16 :goto_0

    .line 2475356
    :cond_b
    const/4 v5, 0x0

    goto/16 :goto_3

    .line 2475357
    :cond_c
    const/4 v5, 0x0

    goto/16 :goto_5

    .line 2475358
    :cond_d
    const/4 v0, 0x0

    goto/16 :goto_6
.end method
