.class public LX/I2Z;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/I2Y;


# static fields
.field public static final a:Ljava/lang/Object;

.field public static final b:Ljava/lang/Object;

.field public static final c:Ljava/lang/Object;


# instance fields
.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/model/Event;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/model/Event;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/HyQ;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "LX/Hx6;",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPromptsModel$EventPromptsModel$NodesModel;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/I2V",
            "<*>;>;"
        }
    .end annotation
.end field

.field public i:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public j:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLInterfaces$ReactionUnitFragment;",
            ">;"
        }
    .end annotation
.end field

.field public final l:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final m:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "LX/Hx6;",
            "Ljava/util/List",
            "<",
            "LX/I2V",
            "<*>;>;>;"
        }
    .end annotation
.end field

.field public n:LX/Hx6;

.field public o:Z

.field public p:Z

.field public q:Z

.field public r:Z

.field private s:LX/0SG;

.field private t:LX/6RZ;

.field private u:LX/I2S;

.field public final v:Z

.field public final w:Z

.field public x:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/events/eventcollections/graphql/EventCollectionsCarouselGraphQLModels$EventCollectionsCarouselQueryModel$EventCollectionsModel$EdgesModel$NodeModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2530509
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/I2Z;->a:Ljava/lang/Object;

    .line 2530510
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/I2Z;->b:Ljava/lang/Object;

    .line 2530511
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/I2Z;->c:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;LX/Hx6;LX/0SG;LX/6RZ;LX/I2S;LX/0Uh;LX/0ad;)V
    .locals 3
    .param p1    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/Hx6;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2530489
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2530490
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/I2Z;->d:Ljava/util/List;

    .line 2530491
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/I2Z;->e:Ljava/util/List;

    .line 2530492
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/I2Z;->f:Ljava/util/List;

    .line 2530493
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/I2Z;->g:Ljava/util/HashMap;

    .line 2530494
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/I2Z;->h:Ljava/util/List;

    .line 2530495
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/I2Z;->i:Ljava/util/HashMap;

    .line 2530496
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/I2Z;->j:Ljava/util/HashMap;

    .line 2530497
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/I2Z;->k:Ljava/util/List;

    .line 2530498
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/I2Z;->l:Ljava/util/HashMap;

    .line 2530499
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/I2Z;->m:Ljava/util/HashMap;

    .line 2530500
    const/4 v0, 0x0

    iput-object v0, p0, LX/I2Z;->x:LX/0Px;

    .line 2530501
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/I2Z;->r:Z

    .line 2530502
    iput-object p2, p0, LX/I2Z;->n:LX/Hx6;

    .line 2530503
    iput-object p3, p0, LX/I2Z;->s:LX/0SG;

    .line 2530504
    iput-object p4, p0, LX/I2Z;->t:LX/6RZ;

    .line 2530505
    iput-object p5, p0, LX/I2Z;->u:LX/I2S;

    .line 2530506
    sget-short v0, LX/347;->k:S

    const/4 v1, 0x1

    invoke-interface {p7, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/I2Z;->v:Z

    .line 2530507
    const/16 v0, 0x39e

    invoke-virtual {p6, v0, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/I2Z;->w:Z

    .line 2530508
    return-void
.end method

.method public static a(LX/15i;ILjava/util/Calendar;)Z
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2530485
    invoke-virtual {p2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v3

    invoke-virtual {p0, p1, v1}, LX/15i;->j(II)I

    move-result v4

    invoke-virtual {p0, p1, v2}, LX/15i;->j(II)I

    move-result v5

    invoke-static {v0, v3, v4, v5}, LX/6RS;->a(Ljava/util/Date;Ljava/util/TimeZone;II)Ljava/util/Calendar;

    move-result-object v3

    .line 2530486
    invoke-virtual {p2}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    .line 2530487
    invoke-virtual {p0, p1, v7}, LX/15i;->j(II)I

    move-result v4

    invoke-virtual {p0, p1, v2}, LX/15i;->j(II)I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->j(II)I

    move-result v6

    invoke-virtual {v0, v4, v5, v6}, Ljava/util/Calendar;->set(III)V

    .line 2530488
    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Landroid/text/format/DateUtils;->isToday(J)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {p2, v0, v7}, LX/6RS;->a(Ljava/util/Calendar;Ljava/util/Calendar;I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public static b(LX/I2Z;)V
    .locals 10

    .prologue
    .line 2530426
    iget-object v0, p0, LX/I2Z;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2530427
    iget-object v0, p0, LX/I2Z;->i:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 2530428
    iget-boolean v0, p0, LX/I2Z;->p:Z

    if-eqz v0, :cond_1

    .line 2530429
    iget-boolean v0, p0, LX/I2Z;->p:Z

    if-eqz v0, :cond_0

    .line 2530430
    iget-object v0, p0, LX/I2Z;->h:Ljava/util/List;

    sget-object v1, LX/I2b;->g:LX/I2b;

    sget-object v2, LX/I2Z;->c:Ljava/lang/Object;

    invoke-static {v1, v2}, LX/I2V;->a(LX/I2b;Ljava/lang/Object;)LX/I2V;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2530431
    :cond_0
    :goto_0
    return-void

    .line 2530432
    :cond_1
    iget-object v0, p0, LX/I2Z;->n:LX/Hx6;

    sget-object v1, LX/Hx6;->BIRTHDAYS:LX/Hx6;

    if-ne v0, v1, :cond_9

    .line 2530433
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2530434
    iget-boolean v0, p0, LX/I2Z;->v:Z

    if-eqz v0, :cond_f

    .line 2530435
    invoke-static {p0, v2}, LX/I2Z;->d(LX/I2Z;Z)V

    move v0, v1

    .line 2530436
    :goto_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    .line 2530437
    const/16 v3, 0xa

    invoke-virtual {v6, v3, v2}, Ljava/util/Calendar;->set(II)V

    .line 2530438
    const/16 v3, 0xc

    invoke-virtual {v6, v3, v2}, Ljava/util/Calendar;->set(II)V

    .line 2530439
    const/16 v3, 0xd

    invoke-virtual {v6, v3, v2}, Ljava/util/Calendar;->set(II)V

    .line 2530440
    const/16 v3, 0xe

    invoke-virtual {v6, v3, v2}, Ljava/util/Calendar;->set(II)V

    .line 2530441
    iget-object v3, p0, LX/I2Z;->f:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v3, v2

    move v4, v0

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HyQ;

    .line 2530442
    iget-object v5, v0, LX/HyQ;->b:Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;

    move-object v8, v5

    .line 2530443
    if-nez v4, :cond_3

    if-eqz v8, :cond_3

    invoke-virtual {v8}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;->j()LX/1vs;

    move-result-object v5

    iget v5, v5, LX/1vs;->b:I

    if-eqz v5, :cond_2

    move v5, v1

    :goto_3
    if-eqz v5, :cond_5

    .line 2530444
    invoke-virtual {v8}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;->j()LX/1vs;

    move-result-object v5

    iget-object v8, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    .line 2530445
    invoke-static {v8, v5, v6}, LX/I2Z;->a(LX/15i;ILjava/util/Calendar;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 2530446
    iget-object v3, p0, LX/I2Z;->h:Ljava/util/List;

    sget-object v5, LX/I2b;->j:LX/I2b;

    invoke-static {v5, v0}, LX/I2V;->a(LX/I2b;Ljava/lang/Object;)LX/I2V;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v3, v1

    .line 2530447
    goto :goto_2

    :cond_2
    move v5, v2

    .line 2530448
    goto :goto_3

    :cond_3
    move v5, v2

    goto :goto_3

    .line 2530449
    :cond_4
    invoke-static {p0, v3}, LX/I2Z;->d(LX/I2Z;Z)V

    .line 2530450
    iget-object v4, p0, LX/I2Z;->h:Ljava/util/List;

    sget-object v5, LX/I2b;->j:LX/I2b;

    invoke-static {v5, v0}, LX/I2V;->a(LX/I2b;Ljava/lang/Object;)LX/I2V;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v4, v1

    .line 2530451
    goto :goto_2

    .line 2530452
    :cond_5
    iget-object v5, p0, LX/I2Z;->h:Ljava/util/List;

    sget-object v8, LX/I2b;->j:LX/I2b;

    invoke-static {v8, v0}, LX/I2V;->a(LX/I2b;Ljava/lang/Object;)LX/I2V;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2530453
    :cond_6
    :goto_4
    iget-boolean v0, p0, LX/I2Z;->o:Z

    if-eqz v0, :cond_7

    .line 2530454
    iget-object v0, p0, LX/I2Z;->h:Ljava/util/List;

    sget-object v1, LX/I2b;->c:LX/I2b;

    sget-object v2, LX/I2i;->a:Ljava/lang/Object;

    invoke-static {v1, v2}, LX/I2V;->a(LX/I2b;Ljava/lang/Object;)LX/I2V;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2530455
    :cond_7
    iget-object v1, p0, LX/I2Z;->m:Ljava/util/HashMap;

    iget-object v2, p0, LX/I2Z;->n:LX/Hx6;

    iget-object v0, p0, LX/I2Z;->n:LX/Hx6;

    sget-object v3, LX/Hx6;->UPCOMING:LX/Hx6;

    if-ne v0, v3, :cond_10

    new-instance v0, Ljava/util/ArrayList;

    iget-object v3, p0, LX/I2Z;->h:Ljava/util/List;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    :goto_5
    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2530456
    iget-object v0, p0, LX/I2Z;->n:LX/Hx6;

    sget-object v1, LX/Hx6;->UPCOMING:LX/Hx6;

    if-ne v0, v1, :cond_8

    .line 2530457
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/I2Z;->d:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, LX/I2Z;->e:Ljava/util/List;

    .line 2530458
    :cond_8
    goto/16 :goto_0

    .line 2530459
    :cond_9
    iget-object v0, p0, LX/I2Z;->n:LX/Hx6;

    sget-object v1, LX/Hx6;->UPCOMING:LX/Hx6;

    if-ne v0, v1, :cond_a

    .line 2530460
    iget-object v0, p0, LX/I2Z;->x:LX/0Px;

    if-eqz v0, :cond_a

    iget-object v0, p0, LX/I2Z;->x:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    .line 2530461
    iget-object v0, p0, LX/I2Z;->h:Ljava/util/List;

    sget-object v1, LX/I2b;->h:LX/I2b;

    iget-object v2, p0, LX/I2Z;->x:LX/0Px;

    invoke-static {v1, v2}, LX/I2V;->a(LX/I2b;Ljava/lang/Object;)LX/I2V;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2530462
    :cond_a
    iget-object v0, p0, LX/I2Z;->d:Ljava/util/List;

    if-nez v0, :cond_11

    const/4 v0, 0x0

    .line 2530463
    :goto_6
    iget-object v1, p0, LX/I2Z;->n:LX/Hx6;

    sget-object v2, LX/Hx6;->UPCOMING:LX/Hx6;

    if-ne v1, v2, :cond_b

    .line 2530464
    iget-boolean v1, p0, LX/I2Z;->r:Z

    if-eqz v1, :cond_b

    .line 2530465
    iget-boolean v1, p0, LX/I2Z;->q:Z

    if-eqz v1, :cond_12

    const/16 v1, 0xc

    :goto_7
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 2530466
    :cond_b
    iget-object v1, p0, LX/I2Z;->d:Ljava/util/List;

    if-eqz v1, :cond_13

    iget-object v1, p0, LX/I2Z;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_13

    .line 2530467
    invoke-static {p0, v0}, LX/I2Z;->b(LX/I2Z;I)V

    .line 2530468
    :goto_8
    iget-boolean v0, p0, LX/I2Z;->r:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, LX/I2Z;->n:LX/Hx6;

    sget-object v1, LX/Hx6;->UPCOMING:LX/Hx6;

    if-ne v0, v1, :cond_6

    .line 2530469
    iget-boolean v0, p0, LX/I2Z;->q:Z

    if-nez v0, :cond_c

    iget-object v0, p0, LX/I2Z;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x3

    if-le v0, v1, :cond_c

    .line 2530470
    iget-object v0, p0, LX/I2Z;->h:Ljava/util/List;

    sget-object v1, LX/I2b;->e:LX/I2b;

    sget-object v2, LX/I2Z;->a:Ljava/lang/Object;

    invoke-static {v1, v2}, LX/I2V;->a(LX/I2b;Ljava/lang/Object;)LX/I2V;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2530471
    :cond_c
    iget-boolean v0, p0, LX/I2Z;->q:Z

    if-eqz v0, :cond_d

    iget-object v0, p0, LX/I2Z;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0xc

    if-le v0, v1, :cond_d

    .line 2530472
    iget-object v0, p0, LX/I2Z;->h:Ljava/util/List;

    sget-object v1, LX/I2X;->VIEW_ALL:LX/I2X;

    .line 2530473
    sget-object v2, LX/I2W;->a:[I

    invoke-virtual {v1}, LX/I2X;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 2530474
    const/4 v2, 0x0

    :goto_9
    move-object v1, v2

    .line 2530475
    sget-object v2, LX/I2Z;->b:Ljava/lang/Object;

    invoke-static {v1, v2}, LX/I2V;->a(LX/I2b;Ljava/lang/Object;)LX/I2V;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2530476
    :cond_d
    const/4 v0, 0x0

    :goto_a
    iget-object v1, p0, LX/I2Z;->k:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_e

    .line 2530477
    iget-object v1, p0, LX/I2Z;->h:Ljava/util/List;

    sget-object v2, LX/I2b;->i:LX/I2b;

    iget-object v3, p0, LX/I2Z;->k:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v2, v3}, LX/I2V;->a(LX/I2b;Ljava/lang/Object;)LX/I2V;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2530478
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 2530479
    :cond_e
    goto/16 :goto_4

    :cond_f
    move v0, v2

    goto/16 :goto_1

    .line 2530480
    :cond_10
    new-instance v0, Ljava/util/ArrayList;

    iget-object v3, p0, LX/I2Z;->h:Ljava/util/List;

    const/4 v4, 0x0

    iget-object v5, p0, LX/I2Z;->h:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    const/16 v6, 0xc

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    invoke-interface {v3, v4, v5}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto/16 :goto_5

    .line 2530481
    :cond_11
    iget-object v0, p0, LX/I2Z;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto/16 :goto_6

    .line 2530482
    :cond_12
    const/4 v1, 0x3

    goto/16 :goto_7

    .line 2530483
    :cond_13
    iget-object v0, p0, LX/I2Z;->h:Ljava/util/List;

    sget-object v1, LX/I2b;->d:LX/I2b;

    iget-object v2, p0, LX/I2Z;->n:LX/Hx6;

    invoke-static {v1, v2}, LX/I2V;->a(LX/I2b;Ljava/lang/Object;)LX/I2V;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_8

    .line 2530484
    :pswitch_0
    iget-boolean v2, p0, LX/I2Z;->w:Z

    if-eqz v2, :cond_14

    sget-object v2, LX/I2b;->m:LX/I2b;

    goto :goto_9

    :cond_14
    sget-object v2, LX/I2b;->f:LX/I2b;

    goto :goto_9

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public static b(LX/I2Z;I)V
    .locals 13

    .prologue
    .line 2530398
    iget-object v0, p0, LX/I2Z;->d:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/I2Z;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2530399
    :cond_0
    return-void

    .line 2530400
    :cond_1
    const/4 v7, 0x0

    .line 2530401
    iget-object v0, p0, LX/I2Z;->s:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v10

    .line 2530402
    invoke-static {v10, v11}, LX/6Rc;->a(J)LX/6Rc;

    move-result-object v9

    .line 2530403
    invoke-static {v10, v11}, LX/6Rc;->b(J)LX/6Rc;

    move-result-object v12

    .line 2530404
    const/4 v0, 0x0

    move v8, v0

    :goto_0
    if-ge v8, p1, :cond_0

    .line 2530405
    iget-object v0, p0, LX/I2Z;->d:Ljava/util/List;

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/facebook/events/model/Event;

    .line 2530406
    iget-object v0, p0, LX/I2Z;->i:Ljava/util/HashMap;

    .line 2530407
    iget-object v1, v6, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2530408
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2530409
    iget-object v0, p0, LX/I2Z;->n:LX/Hx6;

    sget-object v1, LX/Hx6;->UPCOMING:LX/Hx6;

    if-ne v0, v1, :cond_2

    .line 2530410
    iget-object v0, p0, LX/I2Z;->h:Ljava/util/List;

    sget-object v1, LX/I2b;->b:LX/I2b;

    iget-object v2, p0, LX/I2Z;->d:Ljava/util/List;

    invoke-interface {v2, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v1, v2}, LX/I2V;->a(LX/I2b;Ljava/lang/Object;)LX/I2V;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v0, v7

    .line 2530411
    :goto_1
    add-int/lit8 v1, v8, 0x1

    move v8, v1

    move-object v7, v0

    goto :goto_0

    .line 2530412
    :cond_2
    invoke-virtual {v6}, Lcom/facebook/events/model/Event;->M()J

    move-result-wide v0

    .line 2530413
    invoke-virtual {v6}, Lcom/facebook/events/model/Event;->L()Ljava/util/Date;

    move-result-object v2

    .line 2530414
    invoke-virtual {v9, v0, v1}, LX/6Rc;->c(J)Z

    move-result v3

    .line 2530415
    invoke-virtual {v12, v0, v1}, LX/6Rc;->c(J)Z

    move-result v4

    .line 2530416
    iget-object v5, p0, LX/I2Z;->t:LX/6RZ;

    invoke-virtual {v5, v0, v1, v10, v11}, LX/6RZ;->a(JJ)LX/6RY;

    move-result-object v1

    .line 2530417
    iget-object v0, p0, LX/I2Z;->n:LX/Hx6;

    sget-object v5, LX/Hx6;->PAST:LX/Hx6;

    if-ne v0, v5, :cond_4

    .line 2530418
    if-eqz v4, :cond_3

    iget-object v0, p0, LX/I2Z;->t:LX/6RZ;

    invoke-virtual {v0, v2}, LX/6RZ;->f(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 2530419
    :goto_2
    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2530420
    iget-object v0, p0, LX/I2Z;->h:Ljava/util/List;

    sget-object v1, LX/I2b;->b:LX/I2b;

    iget-object v2, p0, LX/I2Z;->d:Ljava/util/List;

    invoke-interface {v2, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v1, v2}, LX/I2V;->a(LX/I2b;Ljava/lang/Object;)LX/I2V;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v0, v7

    .line 2530421
    goto :goto_1

    .line 2530422
    :cond_3
    iget-object v0, p0, LX/I2Z;->t:LX/6RZ;

    invoke-virtual {v0, v2}, LX/6RZ;->g(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 2530423
    :cond_4
    iget-object v0, p0, LX/I2Z;->u:LX/I2S;

    move v5, p1

    invoke-virtual/range {v0 .. v5}, LX/I2S;->a(LX/6RY;Ljava/util/Date;ZZI)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 2530424
    :cond_5
    iget-object v1, p0, LX/I2Z;->h:Ljava/util/List;

    sget-object v2, LX/I2b;->a:LX/I2b;

    invoke-static {v2, v0}, LX/I2V;->a(LX/I2b;Ljava/lang/Object;)LX/I2V;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2530425
    iget-object v1, p0, LX/I2Z;->h:Ljava/util/List;

    sget-object v2, LX/I2b;->b:LX/I2b;

    invoke-static {v2, v6}, LX/I2V;->a(LX/I2b;Ljava/lang/Object;)LX/I2V;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public static d(LX/I2Z;Z)V
    .locals 4

    .prologue
    .line 2530394
    iget-object v0, p0, LX/I2Z;->g:Ljava/util/HashMap;

    iget-object v1, p0, LX/I2Z;->n:LX/Hx6;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPromptsModel$EventPromptsModel$NodesModel;

    .line 2530395
    if-eqz v0, :cond_0

    .line 2530396
    iget-object v1, p0, LX/I2Z;->h:Ljava/util/List;

    sget-object v2, LX/I2b;->k:LX/I2b;

    new-instance v3, LX/Hyy;

    invoke-direct {v3, v0, p1}, LX/Hyy;-><init>(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPromptsModel$EventPromptsModel$NodesModel;Z)V

    invoke-static {v2, v3}, LX/I2V;->a(LX/I2b;Ljava/lang/Object;)LX/I2V;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2530397
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2530393
    iget-object v0, p0, LX/I2Z;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, LX/I2Z;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/I2V;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/events/model/Event;)V
    .locals 2

    .prologue
    .line 2530388
    if-eqz p2, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, LX/I2Z;->i:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2530389
    iget-object v1, p0, LX/I2Z;->d:Ljava/util/List;

    iget-object v0, p0, LX/I2Z;->i:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, v0, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2530390
    iget-object v0, p0, LX/I2Z;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 2530391
    invoke-static {p0}, LX/I2Z;->b(LX/I2Z;)V

    .line 2530392
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 2530384
    iget-boolean v0, p0, LX/I2Z;->q:Z

    if-eq v0, p1, :cond_0

    .line 2530385
    iput-boolean p1, p0, LX/I2Z;->q:Z

    .line 2530386
    invoke-static {p0}, LX/I2Z;->b(LX/I2Z;)V

    .line 2530387
    :cond_0
    return-void
.end method

.method public final b(Ljava/util/List;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2530373
    const/4 v2, 0x0

    .line 2530374
    if-nez p1, :cond_1

    .line 2530375
    :cond_0
    invoke-static {p0}, LX/I2Z;->b(LX/I2Z;)V

    .line 2530376
    return-void

    .line 2530377
    :cond_1
    iget-object v0, p0, LX/I2Z;->f:Ljava/util/List;

    if-nez v0, :cond_2

    .line 2530378
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/I2Z;->f:Ljava/util/List;

    :cond_2
    move v1, v2

    .line 2530379
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2530380
    iget-object v3, p0, LX/I2Z;->f:Ljava/util/List;

    new-instance v4, LX/HyQ;

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;

    invoke-direct {v4, p2, v0, v2}, LX/HyQ;-><init>(Ljava/lang/String;Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;Z)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2530381
    iget-object v3, p0, LX/I2Z;->j:Ljava/util/HashMap;

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;->m()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2530382
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 2530383
    iget-object v0, p0, LX/I2Z;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
