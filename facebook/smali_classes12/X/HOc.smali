.class public final LX/HOc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;)V
    .locals 0

    .prologue
    .line 2460444
    iput-object p1, p0, LX/HOc;->a:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3

    .prologue
    .line 2460445
    if-eqz p2, :cond_1

    .line 2460446
    iget-object v0, p0, LX/HOc;->a:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->h:Lcom/facebook/widget/CustomLinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->setVisibility(I)V

    .line 2460447
    iget-object v0, p0, LX/HOc;->a:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->a:LX/CY3;

    iget-object v1, p0, LX/HOc;->a:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;

    iget-object v1, v1, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->f:Ljava/lang/String;

    .line 2460448
    iget-object v2, v0, LX/CY3;->a:LX/0Zb;

    sget-object p1, LX/CY4;->EVENT_ADMIN_CALL_TO_ACTION_CHECK_DEEPLINK:LX/CY4;

    invoke-static {p1, v1}, LX/CY3;->a(LX/CY4;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    invoke-interface {v2, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2460449
    iget-object v0, p0, LX/HOc;->a:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;

    iget-object v1, v0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->c:LX/0if;

    iget-object v0, p0, LX/HOc;->a:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;

    iget-boolean v0, v0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->g:Z

    if-eqz v0, :cond_0

    sget-object v0, LX/0ig;->ak:LX/0ih;

    :goto_0
    const-string v2, "check_deeplink_box"

    invoke-virtual {v1, v0, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2460450
    :goto_1
    return-void

    .line 2460451
    :cond_0
    sget-object v0, LX/0ig;->aj:LX/0ih;

    goto :goto_0

    .line 2460452
    :cond_1
    iget-object v0, p0, LX/HOc;->a:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->h:Lcom/facebook/widget/CustomLinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->setVisibility(I)V

    .line 2460453
    iget-object v0, p0, LX/HOc;->a:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->a:LX/CY3;

    iget-object v1, p0, LX/HOc;->a:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;

    iget-object v1, v1, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->f:Ljava/lang/String;

    .line 2460454
    iget-object v2, v0, LX/CY3;->a:LX/0Zb;

    sget-object p1, LX/CY4;->EVENT_ADMIN_CALL_TO_ACTION_UNCHECK_DEEPLINK:LX/CY4;

    invoke-static {p1, v1}, LX/CY3;->a(LX/CY4;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    invoke-interface {v2, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2460455
    iget-object v0, p0, LX/HOc;->a:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;

    iget-object v1, v0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->c:LX/0if;

    iget-object v0, p0, LX/HOc;->a:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;

    iget-boolean v0, v0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionLinkoutFieldsContainer;->g:Z

    if-eqz v0, :cond_2

    sget-object v0, LX/0ig;->ak:LX/0ih;

    :goto_2
    const-string v2, "uncheck_deeplink_box"

    invoke-virtual {v1, v0, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    sget-object v0, LX/0ig;->aj:LX/0ih;

    goto :goto_2
.end method
