.class public LX/IhG;
.super Lcom/facebook/fbui/widget/text/ImageWithTextView;
.source ""


# instance fields
.field public a:LX/0wM;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private b:Landroid/animation/ValueAnimator;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2602395
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;-><init>(Landroid/content/Context;)V

    .line 2602396
    invoke-direct {p0}, LX/IhG;->a()V

    .line 2602397
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2602392
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/text/ImageWithTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2602393
    invoke-direct {p0}, LX/IhG;->a()V

    .line 2602394
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2602389
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/text/ImageWithTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2602390
    invoke-direct {p0}, LX/IhG;->a()V

    .line 2602391
    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 2602398
    const-class v0, LX/IhG;

    invoke-static {v0, p0}, LX/IhG;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2602399
    iget-object v0, p0, LX/IhG;->a:LX/0wM;

    const v1, 0x7f020eb5

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/0wM;->a(IIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2602400
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2602401
    invoke-virtual {p0}, LX/IhG;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b18ce

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p0, v1}, LX/IhG;->setCompoundDrawablePadding(I)V

    .line 2602402
    invoke-virtual {p0, v4}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setOrientation(I)V

    .line 2602403
    new-array v1, v4, [I

    fill-array-data v1, :array_0

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v1

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    move-result-object v1

    iput-object v1, p0, LX/IhG;->b:Landroid/animation/ValueAnimator;

    .line 2602404
    iget-object v1, p0, LX/IhG;->b:Landroid/animation/ValueAnimator;

    new-instance v2, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 2602405
    iget-object v1, p0, LX/IhG;->b:Landroid/animation/ValueAnimator;

    new-instance v2, LX/IhF;

    invoke-direct {v2, p0, v0}, LX/IhF;-><init>(LX/IhG;Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 2602406
    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x2710
    .end array-data
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, LX/IhG;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/IhG;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v0

    check-cast v0, LX/0wM;

    iput-object v0, p0, LX/IhG;->a:LX/0wM;

    return-void
.end method


# virtual methods
.method public setSelected(Z)V
    .locals 1

    .prologue
    .line 2602384
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setSelected(Z)V

    .line 2602385
    if-eqz p1, :cond_0

    .line 2602386
    iget-object v0, p0, LX/IhG;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 2602387
    :goto_0
    return-void

    .line 2602388
    :cond_0
    iget-object v0, p0, LX/IhG;->b:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->reverse()V

    goto :goto_0
.end method
