.class public LX/IWC;
.super LX/1Cv;
.source ""


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel$GroupAlbumsModel$NodesModel;",
            ">;"
        }
    .end annotation
.end field

.field private b:Z

.field private c:LX/IWF;

.field public d:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;


# direct methods
.method public constructor <init>(LX/IWF;)V
    .locals 1
    .param p1    # LX/IWF;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2583529
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 2583530
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2583531
    iput-object v0, p0, LX/IWC;->a:LX/0Px;

    .line 2583532
    iput-object p1, p0, LX/IWC;->c:LX/IWF;

    .line 2583533
    return-void
.end method

.method private a()Z
    .locals 2

    .prologue
    .line 2583528
    iget-object v0, p0, LX/IWC;->d:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;->CANNOT_POST:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2583521
    invoke-static {}, LX/IWB;->values()[LX/IWB;

    move-result-object v0

    aget-object v0, v0, p1

    .line 2583522
    sget-object v1, LX/IWA;->a:[I

    invoke-virtual {v0}, LX/IWB;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2583523
    new-instance v0, Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    :goto_0
    return-object v0

    .line 2583524
    :pswitch_0
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2583525
    const v1, 0x7f030825

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 2583526
    :pswitch_1
    new-instance v0, Lcom/facebook/groups/photos/view/GroupAlbumRow;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/groups/photos/view/GroupAlbumRow;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 2583527
    :pswitch_2
    new-instance v0, LX/IWw;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/IWw;-><init>(Landroid/content/Context;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 2583508
    invoke-static {}, LX/IWB;->values()[LX/IWB;

    move-result-object v0

    aget-object v0, v0, p4

    .line 2583509
    sget-object v1, LX/IWA;->a:[I

    invoke-virtual {v0}, LX/IWB;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2583510
    :goto_0
    return-void

    .line 2583511
    :pswitch_0
    check-cast p3, Lcom/facebook/groups/photos/view/GroupAlbumRow;

    .line 2583512
    check-cast p2, Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel$GroupAlbumsModel$NodesModel;

    invoke-virtual {p3, p2}, Lcom/facebook/groups/photos/view/GroupAlbumRow;->a(Lcom/facebook/groups/photos/protocol/FetchGroupAlbumsModels$FetchGroupAlbumsModel$GroupAlbumsModel$NodesModel;)V

    goto :goto_0

    .line 2583513
    :pswitch_1
    iget-boolean v0, p0, LX/IWC;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p3, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_0
    const/16 v0, 0x8

    goto :goto_1

    .line 2583514
    :pswitch_2
    check-cast p3, LX/IWw;

    .line 2583515
    iget-object v0, p0, LX/IWC;->d:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    iget-object v1, p0, LX/IWC;->c:LX/IWF;

    .line 2583516
    if-eqz v0, :cond_1

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;->CANNOT_POST:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    if-eq v0, p0, :cond_1

    .line 2583517
    const/4 p0, 0x0

    invoke-virtual {p3, p0}, LX/IWw;->setVisibility(I)V

    .line 2583518
    new-instance p0, LX/IWv;

    invoke-direct {p0, p3, v1}, LX/IWv;-><init>(LX/IWw;LX/IWF;)V

    invoke-virtual {p3, p0}, LX/IWw;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2583519
    :goto_2
    goto :goto_0

    .line 2583520
    :cond_1
    const/16 p0, 0x8

    invoke-virtual {p3, p0}, LX/IWw;->setVisibility(I)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 2583534
    iget-boolean v0, p0, LX/IWC;->b:Z

    if-eq p1, v0, :cond_0

    .line 2583535
    iput-boolean p1, p0, LX/IWC;->b:Z

    .line 2583536
    const v0, -0x3b156be3

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2583537
    :cond_0
    return-void
.end method

.method public final getCount()I
    .locals 2

    .prologue
    .line 2583502
    iget-object v0, p0, LX/IWC;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    .line 2583503
    invoke-direct {p0}, LX/IWC;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2583504
    add-int/lit8 v0, v0, 0x1

    .line 2583505
    :cond_0
    iget-boolean v1, p0, LX/IWC;->b:Z

    if-eqz v1, :cond_1

    .line 2583506
    add-int/lit8 v0, v0, 0x1

    .line 2583507
    :cond_1
    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2583496
    if-nez p1, :cond_1

    invoke-direct {p0}, LX/IWC;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2583497
    :cond_0
    :goto_0
    return-object v0

    .line 2583498
    :cond_1
    invoke-direct {p0}, LX/IWC;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2583499
    add-int/lit8 p1, p1, -0x1

    .line 2583500
    :cond_2
    iget-object v1, p0, LX/IWC;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 2583501
    iget-object v0, p0, LX/IWC;->a:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2583495
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2583487
    if-nez p1, :cond_0

    invoke-direct {p0}, LX/IWC;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2583488
    sget-object v0, LX/IWB;->CREATE_ALBUM_ROW:LX/IWB;

    invoke-virtual {v0}, LX/IWB;->ordinal()I

    move-result v0

    .line 2583489
    :goto_0
    return v0

    .line 2583490
    :cond_0
    invoke-direct {p0}, LX/IWC;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2583491
    add-int/lit8 p1, p1, -0x1

    .line 2583492
    :cond_1
    iget-object v0, p0, LX/IWC;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge p1, v0, :cond_2

    .line 2583493
    sget-object v0, LX/IWB;->ALBUM_ROW:LX/IWB;

    invoke-virtual {v0}, LX/IWB;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2583494
    :cond_2
    sget-object v0, LX/IWB;->LOADING_BAR:LX/IWB;

    invoke-virtual {v0}, LX/IWB;->ordinal()I

    move-result v0

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2583486
    invoke-static {}, LX/IWB;->values()[LX/IWB;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
