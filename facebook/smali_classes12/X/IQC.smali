.class public LX/IQC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/IQB;


# instance fields
.field public final a:LX/IRb;

.field private final b:Landroid/content/res/Resources;

.field public final c:LX/3mF;

.field public final d:LX/0Sh;

.field private final e:LX/0Zb;

.field public f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;


# direct methods
.method public constructor <init>(LX/IRb;LX/J7Y;Landroid/content/res/Resources;LX/0Sh;LX/3mF;LX/0Zb;)V
    .locals 0
    .param p1    # LX/IRb;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2575032
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2575033
    iput-object p1, p0, LX/IQC;->a:LX/IRb;

    .line 2575034
    iput-object p4, p0, LX/IQC;->d:LX/0Sh;

    .line 2575035
    iput-object p5, p0, LX/IQC;->c:LX/3mF;

    .line 2575036
    iput-object p3, p0, LX/IQC;->b:Landroid/content/res/Resources;

    .line 2575037
    iput-object p6, p0, LX/IQC;->e:LX/0Zb;

    .line 2575038
    return-void
.end method

.method public static b(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Z
    .locals 1

    .prologue
    .line 2575029
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->S()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerAddedByModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->v()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2575030
    :cond_0
    const/4 v0, 0x0

    .line 2575031
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 5

    .prologue
    .line 2575028
    iget-object v0, p0, LX/IQC;->b:Landroid/content/res/Resources;

    const v1, 0x7f081c0f

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/IQC;->f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->S()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerAddedByModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerAddedByModel;->d()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 5

    .prologue
    .line 2575027
    iget-object v0, p0, LX/IQC;->b:Landroid/content/res/Resources;

    const v1, 0x7f081c10

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/IQC;->f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->v()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2575023
    iget-object v0, p0, LX/IQC;->b:Landroid/content/res/Resources;

    const v1, 0x7f081c15

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2575026
    iget-object v0, p0, LX/IQC;->b:Landroid/content/res/Resources;

    const v1, 0x7f081c13

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2575025
    new-instance v0, LX/IQ8;

    invoke-direct {v0, p0}, LX/IQ8;-><init>(LX/IQC;)V

    return-object v0
.end method

.method public final f()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2575024
    new-instance v0, LX/IQ9;

    invoke-direct {v0, p0}, LX/IQ9;-><init>(LX/IQC;)V

    return-object v0
.end method
