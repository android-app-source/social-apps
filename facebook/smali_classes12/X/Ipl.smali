.class public LX/Ipl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;

.field private static final g:LX/1sw;

.field private static final h:LX/1sw;

.field private static final i:LX/1sw;

.field private static final j:LX/1sw;

.field private static final k:LX/1sw;

.field private static final l:LX/1sw;

.field private static final m:LX/1sw;

.field private static final n:LX/1sw;

.field private static final o:LX/1sw;

.field private static final p:LX/1sw;

.field private static final q:LX/1sw;

.field private static final r:LX/1sw;

.field private static final s:LX/1sw;

.field private static final t:LX/1sw;

.field private static final u:LX/1sw;

.field private static final v:LX/1sw;

.field private static final w:LX/1sw;


# instance fields
.field public final amount:Ljava/lang/Long;

.field public final amountFBDiscount:Ljava/lang/Long;

.field public final amountOffset:Ljava/lang/Integer;

.field public final commerceOrderId:Ljava/lang/String;

.field public final currency:Ljava/lang/String;

.field public final groupThreadFbId:Ljava/lang/Long;

.field public final hasMemoMultimedia:Ljava/lang/Boolean;

.field public final initialStatus:Ljava/lang/Integer;

.field public final irisSeqId:Ljava/lang/Long;

.field public final memoText:Ljava/lang/String;

.field public final offlineThreadingId:Ljava/lang/Long;

.field public final platformItemId:Ljava/lang/Long;

.field public final receiverStatus:Ljava/lang/Integer;

.field public final recipientFbId:Ljava/lang/Long;

.field public final requestFbId:Ljava/lang/Long;

.field public final senderFbId:Ljava/lang/Long;

.field public final senderStatus:Ljava/lang/Integer;

.field public final themeId:Ljava/lang/Long;

.field public final timestampMs:Ljava/lang/Long;

.field public final transferFbId:Ljava/lang/Long;

.field public final transferType:Ljava/lang/Integer;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/16 v5, 0xb

    const/16 v4, 0x8

    const/16 v3, 0xa

    .line 2614281
    new-instance v0, LX/1sv;

    const-string v1, "DeltaNewTransfer"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/Ipl;->b:LX/1sv;

    .line 2614282
    new-instance v0, LX/1sw;

    const-string v1, "transferFbId"

    invoke-direct {v0, v1, v3, v6}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipl;->c:LX/1sw;

    .line 2614283
    new-instance v0, LX/1sw;

    const-string v1, "senderFbId"

    invoke-direct {v0, v1, v3, v7}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipl;->d:LX/1sw;

    .line 2614284
    new-instance v0, LX/1sw;

    const-string v1, "recipientFbId"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipl;->e:LX/1sw;

    .line 2614285
    new-instance v0, LX/1sw;

    const-string v1, "timestampMs"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipl;->f:LX/1sw;

    .line 2614286
    new-instance v0, LX/1sw;

    const-string v1, "initialStatus"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipl;->g:LX/1sw;

    .line 2614287
    new-instance v0, LX/1sw;

    const-string v1, "currency"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v5, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipl;->h:LX/1sw;

    .line 2614288
    new-instance v0, LX/1sw;

    const-string v1, "amount"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipl;->i:LX/1sw;

    .line 2614289
    new-instance v0, LX/1sw;

    const-string v1, "amountOffset"

    invoke-direct {v0, v1, v4, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipl;->j:LX/1sw;

    .line 2614290
    new-instance v0, LX/1sw;

    const-string v1, "offlineThreadingId"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipl;->k:LX/1sw;

    .line 2614291
    new-instance v0, LX/1sw;

    const-string v1, "requestFbId"

    invoke-direct {v0, v1, v3, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipl;->l:LX/1sw;

    .line 2614292
    new-instance v0, LX/1sw;

    const-string v1, "senderStatus"

    invoke-direct {v0, v1, v4, v5}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipl;->m:LX/1sw;

    .line 2614293
    new-instance v0, LX/1sw;

    const-string v1, "receiverStatus"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipl;->n:LX/1sw;

    .line 2614294
    new-instance v0, LX/1sw;

    const-string v1, "amountFBDiscount"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipl;->o:LX/1sw;

    .line 2614295
    new-instance v0, LX/1sw;

    const-string v1, "commerceOrderId"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v5, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipl;->p:LX/1sw;

    .line 2614296
    new-instance v0, LX/1sw;

    const-string v1, "platformItemId"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipl;->q:LX/1sw;

    .line 2614297
    new-instance v0, LX/1sw;

    const-string v1, "memoText"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v5, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipl;->r:LX/1sw;

    .line 2614298
    new-instance v0, LX/1sw;

    const-string v1, "hasMemoMultimedia"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v7, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipl;->s:LX/1sw;

    .line 2614299
    new-instance v0, LX/1sw;

    const-string v1, "transferType"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipl;->t:LX/1sw;

    .line 2614300
    new-instance v0, LX/1sw;

    const-string v1, "themeId"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipl;->u:LX/1sw;

    .line 2614301
    new-instance v0, LX/1sw;

    const-string v1, "groupThreadFbId"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipl;->v:LX/1sw;

    .line 2614302
    new-instance v0, LX/1sw;

    const-string v1, "irisSeqId"

    const/16 v2, 0x3e8

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipl;->w:LX/1sw;

    .line 2614303
    sput-boolean v6, LX/Ipl;->a:Z

    return-void
.end method

.method private constructor <init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;)V
    .locals 1

    .prologue
    .line 2614258
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2614259
    iput-object p1, p0, LX/Ipl;->transferFbId:Ljava/lang/Long;

    .line 2614260
    iput-object p2, p0, LX/Ipl;->senderFbId:Ljava/lang/Long;

    .line 2614261
    iput-object p3, p0, LX/Ipl;->recipientFbId:Ljava/lang/Long;

    .line 2614262
    iput-object p4, p0, LX/Ipl;->timestampMs:Ljava/lang/Long;

    .line 2614263
    iput-object p5, p0, LX/Ipl;->initialStatus:Ljava/lang/Integer;

    .line 2614264
    iput-object p6, p0, LX/Ipl;->currency:Ljava/lang/String;

    .line 2614265
    iput-object p7, p0, LX/Ipl;->amount:Ljava/lang/Long;

    .line 2614266
    iput-object p8, p0, LX/Ipl;->amountOffset:Ljava/lang/Integer;

    .line 2614267
    iput-object p9, p0, LX/Ipl;->offlineThreadingId:Ljava/lang/Long;

    .line 2614268
    iput-object p10, p0, LX/Ipl;->requestFbId:Ljava/lang/Long;

    .line 2614269
    iput-object p11, p0, LX/Ipl;->senderStatus:Ljava/lang/Integer;

    .line 2614270
    iput-object p12, p0, LX/Ipl;->receiverStatus:Ljava/lang/Integer;

    .line 2614271
    iput-object p13, p0, LX/Ipl;->amountFBDiscount:Ljava/lang/Long;

    .line 2614272
    iput-object p14, p0, LX/Ipl;->commerceOrderId:Ljava/lang/String;

    .line 2614273
    move-object/from16 v0, p15

    iput-object v0, p0, LX/Ipl;->platformItemId:Ljava/lang/Long;

    .line 2614274
    move-object/from16 v0, p16

    iput-object v0, p0, LX/Ipl;->memoText:Ljava/lang/String;

    .line 2614275
    move-object/from16 v0, p17

    iput-object v0, p0, LX/Ipl;->hasMemoMultimedia:Ljava/lang/Boolean;

    .line 2614276
    move-object/from16 v0, p18

    iput-object v0, p0, LX/Ipl;->transferType:Ljava/lang/Integer;

    .line 2614277
    move-object/from16 v0, p19

    iput-object v0, p0, LX/Ipl;->themeId:Ljava/lang/Long;

    .line 2614278
    move-object/from16 v0, p20

    iput-object v0, p0, LX/Ipl;->groupThreadFbId:Ljava/lang/Long;

    .line 2614279
    move-object/from16 v0, p21

    iput-object v0, p0, LX/Ipl;->irisSeqId:Ljava/lang/Long;

    .line 2614280
    return-void
.end method

.method private static a(LX/Ipl;)V
    .locals 3

    .prologue
    .line 2614249
    iget-object v0, p0, LX/Ipl;->initialStatus:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    sget-object v0, LX/Iq1;->a:LX/1sn;

    iget-object v1, p0, LX/Ipl;->initialStatus:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, LX/1sn;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2614250
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The field \'initialStatus\' has been assigned the invalid value "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/Ipl;->initialStatus:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7H4;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2614251
    :cond_0
    iget-object v0, p0, LX/Ipl;->senderStatus:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    sget-object v0, LX/Iq8;->a:LX/1sn;

    iget-object v1, p0, LX/Ipl;->senderStatus:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, LX/1sn;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2614252
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The field \'senderStatus\' has been assigned the invalid value "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/Ipl;->senderStatus:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7H4;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2614253
    :cond_1
    iget-object v0, p0, LX/Ipl;->receiverStatus:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    sget-object v0, LX/Iq6;->a:LX/1sn;

    iget-object v1, p0, LX/Ipl;->receiverStatus:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, LX/1sn;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2614254
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The field \'receiverStatus\' has been assigned the invalid value "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/Ipl;->receiverStatus:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7H4;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2614255
    :cond_2
    iget-object v0, p0, LX/Ipl;->transferType:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    sget-object v0, LX/IqA;->a:LX/1sn;

    iget-object v1, p0, LX/Ipl;->transferType:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, LX/1sn;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2614256
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The field \'transferType\' has been assigned the invalid value "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/Ipl;->transferType:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7H4;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2614257
    :cond_3
    return-void
.end method

.method public static b(LX/1su;)LX/Ipl;
    .locals 26

    .prologue
    .line 2614156
    const/4 v3, 0x0

    .line 2614157
    const/4 v4, 0x0

    .line 2614158
    const/4 v5, 0x0

    .line 2614159
    const/4 v6, 0x0

    .line 2614160
    const/4 v7, 0x0

    .line 2614161
    const/4 v8, 0x0

    .line 2614162
    const/4 v9, 0x0

    .line 2614163
    const/4 v10, 0x0

    .line 2614164
    const/4 v11, 0x0

    .line 2614165
    const/4 v12, 0x0

    .line 2614166
    const/4 v13, 0x0

    .line 2614167
    const/4 v14, 0x0

    .line 2614168
    const/4 v15, 0x0

    .line 2614169
    const/16 v16, 0x0

    .line 2614170
    const/16 v17, 0x0

    .line 2614171
    const/16 v18, 0x0

    .line 2614172
    const/16 v19, 0x0

    .line 2614173
    const/16 v20, 0x0

    .line 2614174
    const/16 v21, 0x0

    .line 2614175
    const/16 v22, 0x0

    .line 2614176
    const/16 v23, 0x0

    .line 2614177
    invoke-virtual/range {p0 .. p0}, LX/1su;->r()LX/1sv;

    .line 2614178
    :goto_0
    invoke-virtual/range {p0 .. p0}, LX/1su;->f()LX/1sw;

    move-result-object v2

    .line 2614179
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v24, v0

    if-eqz v24, :cond_15

    .line 2614180
    iget-short v0, v2, LX/1sw;->c:S

    move/from16 v24, v0

    sparse-switch v24, :sswitch_data_0

    .line 2614181
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2614182
    :sswitch_0
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v24, v0

    const/16 v25, 0xa

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_0

    .line 2614183
    invoke-virtual/range {p0 .. p0}, LX/1su;->n()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    goto :goto_0

    .line 2614184
    :cond_0
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2614185
    :sswitch_1
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v24, v0

    const/16 v25, 0xa

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_1

    .line 2614186
    invoke-virtual/range {p0 .. p0}, LX/1su;->n()J

    move-result-wide v24

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    goto :goto_0

    .line 2614187
    :cond_1
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2614188
    :sswitch_2
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v24, v0

    const/16 v25, 0xa

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_2

    .line 2614189
    invoke-virtual/range {p0 .. p0}, LX/1su;->n()J

    move-result-wide v24

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    goto :goto_0

    .line 2614190
    :cond_2
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2614191
    :sswitch_3
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v24, v0

    const/16 v25, 0xa

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_3

    .line 2614192
    invoke-virtual/range {p0 .. p0}, LX/1su;->n()J

    move-result-wide v24

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    goto/16 :goto_0

    .line 2614193
    :cond_3
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2614194
    :sswitch_4
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v24, v0

    const/16 v25, 0x8

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_4

    .line 2614195
    invoke-virtual/range {p0 .. p0}, LX/1su;->m()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    goto/16 :goto_0

    .line 2614196
    :cond_4
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2614197
    :sswitch_5
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v24, v0

    const/16 v25, 0xb

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_5

    .line 2614198
    invoke-virtual/range {p0 .. p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_0

    .line 2614199
    :cond_5
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2614200
    :sswitch_6
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v24, v0

    const/16 v25, 0xa

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_6

    .line 2614201
    invoke-virtual/range {p0 .. p0}, LX/1su;->n()J

    move-result-wide v24

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    goto/16 :goto_0

    .line 2614202
    :cond_6
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2614203
    :sswitch_7
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v24, v0

    const/16 v25, 0x8

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_7

    .line 2614204
    invoke-virtual/range {p0 .. p0}, LX/1su;->m()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    goto/16 :goto_0

    .line 2614205
    :cond_7
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2614206
    :sswitch_8
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v24, v0

    const/16 v25, 0xa

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_8

    .line 2614207
    invoke-virtual/range {p0 .. p0}, LX/1su;->n()J

    move-result-wide v24

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    goto/16 :goto_0

    .line 2614208
    :cond_8
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2614209
    :sswitch_9
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v24, v0

    const/16 v25, 0xa

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_9

    .line 2614210
    invoke-virtual/range {p0 .. p0}, LX/1su;->n()J

    move-result-wide v24

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    goto/16 :goto_0

    .line 2614211
    :cond_9
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2614212
    :sswitch_a
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v24, v0

    const/16 v25, 0x8

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_a

    .line 2614213
    invoke-virtual/range {p0 .. p0}, LX/1su;->m()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    goto/16 :goto_0

    .line 2614214
    :cond_a
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2614215
    :sswitch_b
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v24, v0

    const/16 v25, 0x8

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_b

    .line 2614216
    invoke-virtual/range {p0 .. p0}, LX/1su;->m()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    goto/16 :goto_0

    .line 2614217
    :cond_b
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2614218
    :sswitch_c
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v24, v0

    const/16 v25, 0xa

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_c

    .line 2614219
    invoke-virtual/range {p0 .. p0}, LX/1su;->n()J

    move-result-wide v24

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    goto/16 :goto_0

    .line 2614220
    :cond_c
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2614221
    :sswitch_d
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v24, v0

    const/16 v25, 0xb

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_d

    .line 2614222
    invoke-virtual/range {p0 .. p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v16

    goto/16 :goto_0

    .line 2614223
    :cond_d
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2614224
    :sswitch_e
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v24, v0

    const/16 v25, 0xa

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_e

    .line 2614225
    invoke-virtual/range {p0 .. p0}, LX/1su;->n()J

    move-result-wide v24

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    goto/16 :goto_0

    .line 2614226
    :cond_e
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2614227
    :sswitch_f
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v24, v0

    const/16 v25, 0xb

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_f

    .line 2614228
    invoke-virtual/range {p0 .. p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v18

    goto/16 :goto_0

    .line 2614229
    :cond_f
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2614230
    :sswitch_10
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v24, v0

    const/16 v25, 0x2

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_10

    .line 2614231
    invoke-virtual/range {p0 .. p0}, LX/1su;->j()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v19

    goto/16 :goto_0

    .line 2614232
    :cond_10
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2614233
    :sswitch_11
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v24, v0

    const/16 v25, 0x8

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_11

    .line 2614234
    invoke-virtual/range {p0 .. p0}, LX/1su;->m()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    goto/16 :goto_0

    .line 2614235
    :cond_11
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2614236
    :sswitch_12
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v24, v0

    const/16 v25, 0xa

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_12

    .line 2614237
    invoke-virtual/range {p0 .. p0}, LX/1su;->n()J

    move-result-wide v24

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v21

    goto/16 :goto_0

    .line 2614238
    :cond_12
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2614239
    :sswitch_13
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v24, v0

    const/16 v25, 0xa

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_13

    .line 2614240
    invoke-virtual/range {p0 .. p0}, LX/1su;->n()J

    move-result-wide v24

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v22

    goto/16 :goto_0

    .line 2614241
    :cond_13
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2614242
    :sswitch_14
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v24, v0

    const/16 v25, 0xa

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_14

    .line 2614243
    invoke-virtual/range {p0 .. p0}, LX/1su;->n()J

    move-result-wide v24

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v23

    goto/16 :goto_0

    .line 2614244
    :cond_14
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2614245
    :cond_15
    invoke-virtual/range {p0 .. p0}, LX/1su;->e()V

    .line 2614246
    new-instance v2, LX/Ipl;

    invoke-direct/range {v2 .. v23}, LX/Ipl;-><init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;)V

    .line 2614247
    invoke-static {v2}, LX/Ipl;->a(LX/Ipl;)V

    .line 2614248
    return-object v2

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x4 -> :sswitch_3
        0x5 -> :sswitch_4
        0x6 -> :sswitch_5
        0x7 -> :sswitch_6
        0x8 -> :sswitch_7
        0x9 -> :sswitch_8
        0xa -> :sswitch_9
        0xb -> :sswitch_a
        0xc -> :sswitch_b
        0xd -> :sswitch_c
        0xe -> :sswitch_d
        0xf -> :sswitch_e
        0x10 -> :sswitch_f
        0x11 -> :sswitch_10
        0x12 -> :sswitch_11
        0x13 -> :sswitch_12
        0x14 -> :sswitch_13
        0x3e8 -> :sswitch_14
    .end sparse-switch
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2613930
    if-eqz p2, :cond_2c

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    .line 2613931
    :goto_0
    if-eqz p2, :cond_2d

    const-string v0, "\n"

    move-object v3, v0

    .line 2613932
    :goto_1
    if-eqz p2, :cond_2e

    const-string v0, " "

    move-object v1, v0

    .line 2613933
    :goto_2
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v0, "DeltaNewTransfer"

    invoke-direct {v5, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2613934
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613935
    const-string v0, "("

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613936
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613937
    const/4 v0, 0x1

    .line 2613938
    iget-object v6, p0, LX/Ipl;->transferFbId:Ljava/lang/Long;

    if-eqz v6, :cond_0

    .line 2613939
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613940
    const-string v0, "transferFbId"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613941
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613942
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613943
    iget-object v0, p0, LX/Ipl;->transferFbId:Ljava/lang/Long;

    if-nez v0, :cond_2f

    .line 2613944
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    move v0, v2

    .line 2613945
    :cond_0
    iget-object v6, p0, LX/Ipl;->senderFbId:Ljava/lang/Long;

    if-eqz v6, :cond_2

    .line 2613946
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613947
    :cond_1
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613948
    const-string v0, "senderFbId"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613949
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613950
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613951
    iget-object v0, p0, LX/Ipl;->senderFbId:Ljava/lang/Long;

    if-nez v0, :cond_30

    .line 2613952
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_4
    move v0, v2

    .line 2613953
    :cond_2
    iget-object v6, p0, LX/Ipl;->recipientFbId:Ljava/lang/Long;

    if-eqz v6, :cond_4

    .line 2613954
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613955
    :cond_3
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613956
    const-string v0, "recipientFbId"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613957
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613958
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613959
    iget-object v0, p0, LX/Ipl;->recipientFbId:Ljava/lang/Long;

    if-nez v0, :cond_31

    .line 2613960
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_5
    move v0, v2

    .line 2613961
    :cond_4
    iget-object v6, p0, LX/Ipl;->timestampMs:Ljava/lang/Long;

    if-eqz v6, :cond_6

    .line 2613962
    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613963
    :cond_5
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613964
    const-string v0, "timestampMs"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613965
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613966
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613967
    iget-object v0, p0, LX/Ipl;->timestampMs:Ljava/lang/Long;

    if-nez v0, :cond_32

    .line 2613968
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_6
    move v0, v2

    .line 2613969
    :cond_6
    iget-object v6, p0, LX/Ipl;->initialStatus:Ljava/lang/Integer;

    if-eqz v6, :cond_9

    .line 2613970
    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613971
    :cond_7
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613972
    const-string v0, "initialStatus"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613973
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613974
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613975
    iget-object v0, p0, LX/Ipl;->initialStatus:Ljava/lang/Integer;

    if-nez v0, :cond_33

    .line 2613976
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_8
    :goto_7
    move v0, v2

    .line 2613977
    :cond_9
    iget-object v6, p0, LX/Ipl;->currency:Ljava/lang/String;

    if-eqz v6, :cond_b

    .line 2613978
    if-nez v0, :cond_a

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613979
    :cond_a
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613980
    const-string v0, "currency"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613981
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613982
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613983
    iget-object v0, p0, LX/Ipl;->currency:Ljava/lang/String;

    if-nez v0, :cond_35

    .line 2613984
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_8
    move v0, v2

    .line 2613985
    :cond_b
    iget-object v6, p0, LX/Ipl;->amount:Ljava/lang/Long;

    if-eqz v6, :cond_d

    .line 2613986
    if-nez v0, :cond_c

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613987
    :cond_c
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613988
    const-string v0, "amount"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613989
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613990
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613991
    iget-object v0, p0, LX/Ipl;->amount:Ljava/lang/Long;

    if-nez v0, :cond_36

    .line 2613992
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_9
    move v0, v2

    .line 2613993
    :cond_d
    iget-object v6, p0, LX/Ipl;->amountOffset:Ljava/lang/Integer;

    if-eqz v6, :cond_f

    .line 2613994
    if-nez v0, :cond_e

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613995
    :cond_e
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613996
    const-string v0, "amountOffset"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613997
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613998
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613999
    iget-object v0, p0, LX/Ipl;->amountOffset:Ljava/lang/Integer;

    if-nez v0, :cond_37

    .line 2614000
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_a
    move v0, v2

    .line 2614001
    :cond_f
    iget-object v6, p0, LX/Ipl;->offlineThreadingId:Ljava/lang/Long;

    if-eqz v6, :cond_11

    .line 2614002
    if-nez v0, :cond_10

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614003
    :cond_10
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614004
    const-string v0, "offlineThreadingId"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614005
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614006
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614007
    iget-object v0, p0, LX/Ipl;->offlineThreadingId:Ljava/lang/Long;

    if-nez v0, :cond_38

    .line 2614008
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_b
    move v0, v2

    .line 2614009
    :cond_11
    iget-object v6, p0, LX/Ipl;->requestFbId:Ljava/lang/Long;

    if-eqz v6, :cond_13

    .line 2614010
    if-nez v0, :cond_12

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614011
    :cond_12
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614012
    const-string v0, "requestFbId"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614013
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614014
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614015
    iget-object v0, p0, LX/Ipl;->requestFbId:Ljava/lang/Long;

    if-nez v0, :cond_39

    .line 2614016
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_c
    move v0, v2

    .line 2614017
    :cond_13
    iget-object v6, p0, LX/Ipl;->senderStatus:Ljava/lang/Integer;

    if-eqz v6, :cond_16

    .line 2614018
    if-nez v0, :cond_14

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614019
    :cond_14
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614020
    const-string v0, "senderStatus"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614021
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614022
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614023
    iget-object v0, p0, LX/Ipl;->senderStatus:Ljava/lang/Integer;

    if-nez v0, :cond_3a

    .line 2614024
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_15
    :goto_d
    move v0, v2

    .line 2614025
    :cond_16
    iget-object v6, p0, LX/Ipl;->receiverStatus:Ljava/lang/Integer;

    if-eqz v6, :cond_19

    .line 2614026
    if-nez v0, :cond_17

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614027
    :cond_17
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614028
    const-string v0, "receiverStatus"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614029
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614030
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614031
    iget-object v0, p0, LX/Ipl;->receiverStatus:Ljava/lang/Integer;

    if-nez v0, :cond_3c

    .line 2614032
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_18
    :goto_e
    move v0, v2

    .line 2614033
    :cond_19
    iget-object v6, p0, LX/Ipl;->amountFBDiscount:Ljava/lang/Long;

    if-eqz v6, :cond_1b

    .line 2614034
    if-nez v0, :cond_1a

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614035
    :cond_1a
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614036
    const-string v0, "amountFBDiscount"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614037
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614038
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614039
    iget-object v0, p0, LX/Ipl;->amountFBDiscount:Ljava/lang/Long;

    if-nez v0, :cond_3e

    .line 2614040
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_f
    move v0, v2

    .line 2614041
    :cond_1b
    iget-object v6, p0, LX/Ipl;->commerceOrderId:Ljava/lang/String;

    if-eqz v6, :cond_1d

    .line 2614042
    if-nez v0, :cond_1c

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614043
    :cond_1c
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614044
    const-string v0, "commerceOrderId"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614045
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614046
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614047
    iget-object v0, p0, LX/Ipl;->commerceOrderId:Ljava/lang/String;

    if-nez v0, :cond_3f

    .line 2614048
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_10
    move v0, v2

    .line 2614049
    :cond_1d
    iget-object v6, p0, LX/Ipl;->platformItemId:Ljava/lang/Long;

    if-eqz v6, :cond_1f

    .line 2614050
    if-nez v0, :cond_1e

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614051
    :cond_1e
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614052
    const-string v0, "platformItemId"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614053
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614054
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614055
    iget-object v0, p0, LX/Ipl;->platformItemId:Ljava/lang/Long;

    if-nez v0, :cond_40

    .line 2614056
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_11
    move v0, v2

    .line 2614057
    :cond_1f
    iget-object v6, p0, LX/Ipl;->memoText:Ljava/lang/String;

    if-eqz v6, :cond_21

    .line 2614058
    if-nez v0, :cond_20

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614059
    :cond_20
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614060
    const-string v0, "memoText"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614061
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614062
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614063
    iget-object v0, p0, LX/Ipl;->memoText:Ljava/lang/String;

    if-nez v0, :cond_41

    .line 2614064
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_12
    move v0, v2

    .line 2614065
    :cond_21
    iget-object v6, p0, LX/Ipl;->hasMemoMultimedia:Ljava/lang/Boolean;

    if-eqz v6, :cond_23

    .line 2614066
    if-nez v0, :cond_22

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614067
    :cond_22
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614068
    const-string v0, "hasMemoMultimedia"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614069
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614070
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614071
    iget-object v0, p0, LX/Ipl;->hasMemoMultimedia:Ljava/lang/Boolean;

    if-nez v0, :cond_42

    .line 2614072
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_13
    move v0, v2

    .line 2614073
    :cond_23
    iget-object v6, p0, LX/Ipl;->transferType:Ljava/lang/Integer;

    if-eqz v6, :cond_26

    .line 2614074
    if-nez v0, :cond_24

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614075
    :cond_24
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614076
    const-string v0, "transferType"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614077
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614078
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614079
    iget-object v0, p0, LX/Ipl;->transferType:Ljava/lang/Integer;

    if-nez v0, :cond_43

    .line 2614080
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_25
    :goto_14
    move v0, v2

    .line 2614081
    :cond_26
    iget-object v6, p0, LX/Ipl;->themeId:Ljava/lang/Long;

    if-eqz v6, :cond_28

    .line 2614082
    if-nez v0, :cond_27

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614083
    :cond_27
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614084
    const-string v0, "themeId"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614085
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614086
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614087
    iget-object v0, p0, LX/Ipl;->themeId:Ljava/lang/Long;

    if-nez v0, :cond_45

    .line 2614088
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_15
    move v0, v2

    .line 2614089
    :cond_28
    iget-object v6, p0, LX/Ipl;->groupThreadFbId:Ljava/lang/Long;

    if-eqz v6, :cond_48

    .line 2614090
    if-nez v0, :cond_29

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614091
    :cond_29
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614092
    const-string v0, "groupThreadFbId"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614093
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614094
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614095
    iget-object v0, p0, LX/Ipl;->groupThreadFbId:Ljava/lang/Long;

    if-nez v0, :cond_46

    .line 2614096
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614097
    :goto_16
    iget-object v0, p0, LX/Ipl;->irisSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_2b

    .line 2614098
    if-nez v2, :cond_2a

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614099
    :cond_2a
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614100
    const-string v0, "irisSeqId"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614101
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614102
    const-string v0, ":"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614103
    iget-object v0, p0, LX/Ipl;->irisSeqId:Ljava/lang/Long;

    if-nez v0, :cond_47

    .line 2614104
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614105
    :cond_2b
    :goto_17
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614106
    const-string v0, ")"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614107
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2614108
    :cond_2c
    const-string v0, ""

    move-object v4, v0

    goto/16 :goto_0

    .line 2614109
    :cond_2d
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_1

    .line 2614110
    :cond_2e
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_2

    .line 2614111
    :cond_2f
    iget-object v0, p0, LX/Ipl;->transferFbId:Ljava/lang/Long;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 2614112
    :cond_30
    iget-object v0, p0, LX/Ipl;->senderFbId:Ljava/lang/Long;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 2614113
    :cond_31
    iget-object v0, p0, LX/Ipl;->recipientFbId:Ljava/lang/Long;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 2614114
    :cond_32
    iget-object v0, p0, LX/Ipl;->timestampMs:Ljava/lang/Long;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 2614115
    :cond_33
    sget-object v0, LX/Iq1;->b:Ljava/util/Map;

    iget-object v6, p0, LX/Ipl;->initialStatus:Ljava/lang/Integer;

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2614116
    if-eqz v0, :cond_34

    .line 2614117
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614118
    const-string v6, " ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614119
    :cond_34
    iget-object v6, p0, LX/Ipl;->initialStatus:Ljava/lang/Integer;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2614120
    if-eqz v0, :cond_8

    .line 2614121
    const-string v0, ")"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_7

    .line 2614122
    :cond_35
    iget-object v0, p0, LX/Ipl;->currency:Ljava/lang/String;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_8

    .line 2614123
    :cond_36
    iget-object v0, p0, LX/Ipl;->amount:Ljava/lang/Long;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_9

    .line 2614124
    :cond_37
    iget-object v0, p0, LX/Ipl;->amountOffset:Ljava/lang/Integer;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_a

    .line 2614125
    :cond_38
    iget-object v0, p0, LX/Ipl;->offlineThreadingId:Ljava/lang/Long;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_b

    .line 2614126
    :cond_39
    iget-object v0, p0, LX/Ipl;->requestFbId:Ljava/lang/Long;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_c

    .line 2614127
    :cond_3a
    sget-object v0, LX/Iq8;->b:Ljava/util/Map;

    iget-object v6, p0, LX/Ipl;->senderStatus:Ljava/lang/Integer;

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2614128
    if-eqz v0, :cond_3b

    .line 2614129
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614130
    const-string v6, " ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614131
    :cond_3b
    iget-object v6, p0, LX/Ipl;->senderStatus:Ljava/lang/Integer;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2614132
    if-eqz v0, :cond_15

    .line 2614133
    const-string v0, ")"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_d

    .line 2614134
    :cond_3c
    sget-object v0, LX/Iq6;->b:Ljava/util/Map;

    iget-object v6, p0, LX/Ipl;->receiverStatus:Ljava/lang/Integer;

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2614135
    if-eqz v0, :cond_3d

    .line 2614136
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614137
    const-string v6, " ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614138
    :cond_3d
    iget-object v6, p0, LX/Ipl;->receiverStatus:Ljava/lang/Integer;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2614139
    if-eqz v0, :cond_18

    .line 2614140
    const-string v0, ")"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_e

    .line 2614141
    :cond_3e
    iget-object v0, p0, LX/Ipl;->amountFBDiscount:Ljava/lang/Long;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_f

    .line 2614142
    :cond_3f
    iget-object v0, p0, LX/Ipl;->commerceOrderId:Ljava/lang/String;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_10

    .line 2614143
    :cond_40
    iget-object v0, p0, LX/Ipl;->platformItemId:Ljava/lang/Long;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_11

    .line 2614144
    :cond_41
    iget-object v0, p0, LX/Ipl;->memoText:Ljava/lang/String;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_12

    .line 2614145
    :cond_42
    iget-object v0, p0, LX/Ipl;->hasMemoMultimedia:Ljava/lang/Boolean;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_13

    .line 2614146
    :cond_43
    sget-object v0, LX/IqA;->b:Ljava/util/Map;

    iget-object v6, p0, LX/Ipl;->transferType:Ljava/lang/Integer;

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2614147
    if-eqz v0, :cond_44

    .line 2614148
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614149
    const-string v6, " ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614150
    :cond_44
    iget-object v6, p0, LX/Ipl;->transferType:Ljava/lang/Integer;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2614151
    if-eqz v0, :cond_25

    .line 2614152
    const-string v0, ")"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_14

    .line 2614153
    :cond_45
    iget-object v0, p0, LX/Ipl;->themeId:Ljava/lang/Long;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_15

    .line 2614154
    :cond_46
    iget-object v0, p0, LX/Ipl;->groupThreadFbId:Ljava/lang/Long;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v0, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_16

    .line 2614155
    :cond_47
    iget-object v0, p0, LX/Ipl;->irisSeqId:Ljava/lang/Long;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_17

    :cond_48
    move v2, v0

    goto/16 :goto_16
.end method

.method public final a(LX/1su;)V
    .locals 2

    .prologue
    .line 2613682
    invoke-static {p0}, LX/Ipl;->a(LX/Ipl;)V

    .line 2613683
    invoke-virtual {p1}, LX/1su;->a()V

    .line 2613684
    iget-object v0, p0, LX/Ipl;->transferFbId:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 2613685
    iget-object v0, p0, LX/Ipl;->transferFbId:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 2613686
    sget-object v0, LX/Ipl;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2613687
    iget-object v0, p0, LX/Ipl;->transferFbId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2613688
    :cond_0
    iget-object v0, p0, LX/Ipl;->senderFbId:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 2613689
    iget-object v0, p0, LX/Ipl;->senderFbId:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 2613690
    sget-object v0, LX/Ipl;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2613691
    iget-object v0, p0, LX/Ipl;->senderFbId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2613692
    :cond_1
    iget-object v0, p0, LX/Ipl;->recipientFbId:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 2613693
    iget-object v0, p0, LX/Ipl;->recipientFbId:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 2613694
    sget-object v0, LX/Ipl;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2613695
    iget-object v0, p0, LX/Ipl;->recipientFbId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2613696
    :cond_2
    iget-object v0, p0, LX/Ipl;->timestampMs:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 2613697
    iget-object v0, p0, LX/Ipl;->timestampMs:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 2613698
    sget-object v0, LX/Ipl;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2613699
    iget-object v0, p0, LX/Ipl;->timestampMs:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2613700
    :cond_3
    iget-object v0, p0, LX/Ipl;->initialStatus:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 2613701
    iget-object v0, p0, LX/Ipl;->initialStatus:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 2613702
    sget-object v0, LX/Ipl;->g:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2613703
    iget-object v0, p0, LX/Ipl;->initialStatus:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 2613704
    :cond_4
    iget-object v0, p0, LX/Ipl;->currency:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 2613705
    iget-object v0, p0, LX/Ipl;->currency:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 2613706
    sget-object v0, LX/Ipl;->h:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2613707
    iget-object v0, p0, LX/Ipl;->currency:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 2613708
    :cond_5
    iget-object v0, p0, LX/Ipl;->amount:Ljava/lang/Long;

    if-eqz v0, :cond_6

    .line 2613709
    iget-object v0, p0, LX/Ipl;->amount:Ljava/lang/Long;

    if-eqz v0, :cond_6

    .line 2613710
    sget-object v0, LX/Ipl;->i:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2613711
    iget-object v0, p0, LX/Ipl;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2613712
    :cond_6
    iget-object v0, p0, LX/Ipl;->amountOffset:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 2613713
    iget-object v0, p0, LX/Ipl;->amountOffset:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 2613714
    sget-object v0, LX/Ipl;->j:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2613715
    iget-object v0, p0, LX/Ipl;->amountOffset:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 2613716
    :cond_7
    iget-object v0, p0, LX/Ipl;->offlineThreadingId:Ljava/lang/Long;

    if-eqz v0, :cond_8

    .line 2613717
    iget-object v0, p0, LX/Ipl;->offlineThreadingId:Ljava/lang/Long;

    if-eqz v0, :cond_8

    .line 2613718
    sget-object v0, LX/Ipl;->k:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2613719
    iget-object v0, p0, LX/Ipl;->offlineThreadingId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2613720
    :cond_8
    iget-object v0, p0, LX/Ipl;->requestFbId:Ljava/lang/Long;

    if-eqz v0, :cond_9

    .line 2613721
    iget-object v0, p0, LX/Ipl;->requestFbId:Ljava/lang/Long;

    if-eqz v0, :cond_9

    .line 2613722
    sget-object v0, LX/Ipl;->l:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2613723
    iget-object v0, p0, LX/Ipl;->requestFbId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2613724
    :cond_9
    iget-object v0, p0, LX/Ipl;->senderStatus:Ljava/lang/Integer;

    if-eqz v0, :cond_a

    .line 2613725
    iget-object v0, p0, LX/Ipl;->senderStatus:Ljava/lang/Integer;

    if-eqz v0, :cond_a

    .line 2613726
    sget-object v0, LX/Ipl;->m:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2613727
    iget-object v0, p0, LX/Ipl;->senderStatus:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 2613728
    :cond_a
    iget-object v0, p0, LX/Ipl;->receiverStatus:Ljava/lang/Integer;

    if-eqz v0, :cond_b

    .line 2613729
    iget-object v0, p0, LX/Ipl;->receiverStatus:Ljava/lang/Integer;

    if-eqz v0, :cond_b

    .line 2613730
    sget-object v0, LX/Ipl;->n:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2613731
    iget-object v0, p0, LX/Ipl;->receiverStatus:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 2613732
    :cond_b
    iget-object v0, p0, LX/Ipl;->amountFBDiscount:Ljava/lang/Long;

    if-eqz v0, :cond_c

    .line 2613733
    iget-object v0, p0, LX/Ipl;->amountFBDiscount:Ljava/lang/Long;

    if-eqz v0, :cond_c

    .line 2613734
    sget-object v0, LX/Ipl;->o:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2613735
    iget-object v0, p0, LX/Ipl;->amountFBDiscount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2613736
    :cond_c
    iget-object v0, p0, LX/Ipl;->commerceOrderId:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 2613737
    iget-object v0, p0, LX/Ipl;->commerceOrderId:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 2613738
    sget-object v0, LX/Ipl;->p:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2613739
    iget-object v0, p0, LX/Ipl;->commerceOrderId:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 2613740
    :cond_d
    iget-object v0, p0, LX/Ipl;->platformItemId:Ljava/lang/Long;

    if-eqz v0, :cond_e

    .line 2613741
    iget-object v0, p0, LX/Ipl;->platformItemId:Ljava/lang/Long;

    if-eqz v0, :cond_e

    .line 2613742
    sget-object v0, LX/Ipl;->q:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2613743
    iget-object v0, p0, LX/Ipl;->platformItemId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2613744
    :cond_e
    iget-object v0, p0, LX/Ipl;->memoText:Ljava/lang/String;

    if-eqz v0, :cond_f

    .line 2613745
    iget-object v0, p0, LX/Ipl;->memoText:Ljava/lang/String;

    if-eqz v0, :cond_f

    .line 2613746
    sget-object v0, LX/Ipl;->r:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2613747
    iget-object v0, p0, LX/Ipl;->memoText:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 2613748
    :cond_f
    iget-object v0, p0, LX/Ipl;->hasMemoMultimedia:Ljava/lang/Boolean;

    if-eqz v0, :cond_10

    .line 2613749
    iget-object v0, p0, LX/Ipl;->hasMemoMultimedia:Ljava/lang/Boolean;

    if-eqz v0, :cond_10

    .line 2613750
    sget-object v0, LX/Ipl;->s:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2613751
    iget-object v0, p0, LX/Ipl;->hasMemoMultimedia:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 2613752
    :cond_10
    iget-object v0, p0, LX/Ipl;->transferType:Ljava/lang/Integer;

    if-eqz v0, :cond_11

    .line 2613753
    iget-object v0, p0, LX/Ipl;->transferType:Ljava/lang/Integer;

    if-eqz v0, :cond_11

    .line 2613754
    sget-object v0, LX/Ipl;->t:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2613755
    iget-object v0, p0, LX/Ipl;->transferType:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 2613756
    :cond_11
    iget-object v0, p0, LX/Ipl;->themeId:Ljava/lang/Long;

    if-eqz v0, :cond_12

    .line 2613757
    iget-object v0, p0, LX/Ipl;->themeId:Ljava/lang/Long;

    if-eqz v0, :cond_12

    .line 2613758
    sget-object v0, LX/Ipl;->u:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2613759
    iget-object v0, p0, LX/Ipl;->themeId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2613760
    :cond_12
    iget-object v0, p0, LX/Ipl;->groupThreadFbId:Ljava/lang/Long;

    if-eqz v0, :cond_13

    .line 2613761
    iget-object v0, p0, LX/Ipl;->groupThreadFbId:Ljava/lang/Long;

    if-eqz v0, :cond_13

    .line 2613762
    sget-object v0, LX/Ipl;->v:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2613763
    iget-object v0, p0, LX/Ipl;->groupThreadFbId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2613764
    :cond_13
    iget-object v0, p0, LX/Ipl;->irisSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_14

    .line 2613765
    iget-object v0, p0, LX/Ipl;->irisSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_14

    .line 2613766
    sget-object v0, LX/Ipl;->w:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2613767
    iget-object v0, p0, LX/Ipl;->irisSeqId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2613768
    :cond_14
    invoke-virtual {p1}, LX/1su;->c()V

    .line 2613769
    invoke-virtual {p1}, LX/1su;->b()V

    .line 2613770
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2613775
    if-nez p1, :cond_1

    .line 2613776
    :cond_0
    :goto_0
    return v0

    .line 2613777
    :cond_1
    instance-of v1, p1, LX/Ipl;

    if-eqz v1, :cond_0

    .line 2613778
    check-cast p1, LX/Ipl;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2613779
    if-nez p1, :cond_3

    .line 2613780
    :cond_2
    :goto_1
    move v0, v2

    .line 2613781
    goto :goto_0

    .line 2613782
    :cond_3
    iget-object v0, p0, LX/Ipl;->transferFbId:Ljava/lang/Long;

    if-eqz v0, :cond_2e

    move v0, v1

    .line 2613783
    :goto_2
    iget-object v3, p1, LX/Ipl;->transferFbId:Ljava/lang/Long;

    if-eqz v3, :cond_2f

    move v3, v1

    .line 2613784
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 2613785
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2613786
    iget-object v0, p0, LX/Ipl;->transferFbId:Ljava/lang/Long;

    iget-object v3, p1, LX/Ipl;->transferFbId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2613787
    :cond_5
    iget-object v0, p0, LX/Ipl;->senderFbId:Ljava/lang/Long;

    if-eqz v0, :cond_30

    move v0, v1

    .line 2613788
    :goto_4
    iget-object v3, p1, LX/Ipl;->senderFbId:Ljava/lang/Long;

    if-eqz v3, :cond_31

    move v3, v1

    .line 2613789
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 2613790
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2613791
    iget-object v0, p0, LX/Ipl;->senderFbId:Ljava/lang/Long;

    iget-object v3, p1, LX/Ipl;->senderFbId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2613792
    :cond_7
    iget-object v0, p0, LX/Ipl;->recipientFbId:Ljava/lang/Long;

    if-eqz v0, :cond_32

    move v0, v1

    .line 2613793
    :goto_6
    iget-object v3, p1, LX/Ipl;->recipientFbId:Ljava/lang/Long;

    if-eqz v3, :cond_33

    move v3, v1

    .line 2613794
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 2613795
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2613796
    iget-object v0, p0, LX/Ipl;->recipientFbId:Ljava/lang/Long;

    iget-object v3, p1, LX/Ipl;->recipientFbId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2613797
    :cond_9
    iget-object v0, p0, LX/Ipl;->timestampMs:Ljava/lang/Long;

    if-eqz v0, :cond_34

    move v0, v1

    .line 2613798
    :goto_8
    iget-object v3, p1, LX/Ipl;->timestampMs:Ljava/lang/Long;

    if-eqz v3, :cond_35

    move v3, v1

    .line 2613799
    :goto_9
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 2613800
    :cond_a
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2613801
    iget-object v0, p0, LX/Ipl;->timestampMs:Ljava/lang/Long;

    iget-object v3, p1, LX/Ipl;->timestampMs:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2613802
    :cond_b
    iget-object v0, p0, LX/Ipl;->initialStatus:Ljava/lang/Integer;

    if-eqz v0, :cond_36

    move v0, v1

    .line 2613803
    :goto_a
    iget-object v3, p1, LX/Ipl;->initialStatus:Ljava/lang/Integer;

    if-eqz v3, :cond_37

    move v3, v1

    .line 2613804
    :goto_b
    if-nez v0, :cond_c

    if-eqz v3, :cond_d

    .line 2613805
    :cond_c
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2613806
    iget-object v0, p0, LX/Ipl;->initialStatus:Ljava/lang/Integer;

    iget-object v3, p1, LX/Ipl;->initialStatus:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2613807
    :cond_d
    iget-object v0, p0, LX/Ipl;->currency:Ljava/lang/String;

    if-eqz v0, :cond_38

    move v0, v1

    .line 2613808
    :goto_c
    iget-object v3, p1, LX/Ipl;->currency:Ljava/lang/String;

    if-eqz v3, :cond_39

    move v3, v1

    .line 2613809
    :goto_d
    if-nez v0, :cond_e

    if-eqz v3, :cond_f

    .line 2613810
    :cond_e
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2613811
    iget-object v0, p0, LX/Ipl;->currency:Ljava/lang/String;

    iget-object v3, p1, LX/Ipl;->currency:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2613812
    :cond_f
    iget-object v0, p0, LX/Ipl;->amount:Ljava/lang/Long;

    if-eqz v0, :cond_3a

    move v0, v1

    .line 2613813
    :goto_e
    iget-object v3, p1, LX/Ipl;->amount:Ljava/lang/Long;

    if-eqz v3, :cond_3b

    move v3, v1

    .line 2613814
    :goto_f
    if-nez v0, :cond_10

    if-eqz v3, :cond_11

    .line 2613815
    :cond_10
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2613816
    iget-object v0, p0, LX/Ipl;->amount:Ljava/lang/Long;

    iget-object v3, p1, LX/Ipl;->amount:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2613817
    :cond_11
    iget-object v0, p0, LX/Ipl;->amountOffset:Ljava/lang/Integer;

    if-eqz v0, :cond_3c

    move v0, v1

    .line 2613818
    :goto_10
    iget-object v3, p1, LX/Ipl;->amountOffset:Ljava/lang/Integer;

    if-eqz v3, :cond_3d

    move v3, v1

    .line 2613819
    :goto_11
    if-nez v0, :cond_12

    if-eqz v3, :cond_13

    .line 2613820
    :cond_12
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2613821
    iget-object v0, p0, LX/Ipl;->amountOffset:Ljava/lang/Integer;

    iget-object v3, p1, LX/Ipl;->amountOffset:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2613822
    :cond_13
    iget-object v0, p0, LX/Ipl;->offlineThreadingId:Ljava/lang/Long;

    if-eqz v0, :cond_3e

    move v0, v1

    .line 2613823
    :goto_12
    iget-object v3, p1, LX/Ipl;->offlineThreadingId:Ljava/lang/Long;

    if-eqz v3, :cond_3f

    move v3, v1

    .line 2613824
    :goto_13
    if-nez v0, :cond_14

    if-eqz v3, :cond_15

    .line 2613825
    :cond_14
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2613826
    iget-object v0, p0, LX/Ipl;->offlineThreadingId:Ljava/lang/Long;

    iget-object v3, p1, LX/Ipl;->offlineThreadingId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2613827
    :cond_15
    iget-object v0, p0, LX/Ipl;->requestFbId:Ljava/lang/Long;

    if-eqz v0, :cond_40

    move v0, v1

    .line 2613828
    :goto_14
    iget-object v3, p1, LX/Ipl;->requestFbId:Ljava/lang/Long;

    if-eqz v3, :cond_41

    move v3, v1

    .line 2613829
    :goto_15
    if-nez v0, :cond_16

    if-eqz v3, :cond_17

    .line 2613830
    :cond_16
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2613831
    iget-object v0, p0, LX/Ipl;->requestFbId:Ljava/lang/Long;

    iget-object v3, p1, LX/Ipl;->requestFbId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2613832
    :cond_17
    iget-object v0, p0, LX/Ipl;->senderStatus:Ljava/lang/Integer;

    if-eqz v0, :cond_42

    move v0, v1

    .line 2613833
    :goto_16
    iget-object v3, p1, LX/Ipl;->senderStatus:Ljava/lang/Integer;

    if-eqz v3, :cond_43

    move v3, v1

    .line 2613834
    :goto_17
    if-nez v0, :cond_18

    if-eqz v3, :cond_19

    .line 2613835
    :cond_18
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2613836
    iget-object v0, p0, LX/Ipl;->senderStatus:Ljava/lang/Integer;

    iget-object v3, p1, LX/Ipl;->senderStatus:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2613837
    :cond_19
    iget-object v0, p0, LX/Ipl;->receiverStatus:Ljava/lang/Integer;

    if-eqz v0, :cond_44

    move v0, v1

    .line 2613838
    :goto_18
    iget-object v3, p1, LX/Ipl;->receiverStatus:Ljava/lang/Integer;

    if-eqz v3, :cond_45

    move v3, v1

    .line 2613839
    :goto_19
    if-nez v0, :cond_1a

    if-eqz v3, :cond_1b

    .line 2613840
    :cond_1a
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2613841
    iget-object v0, p0, LX/Ipl;->receiverStatus:Ljava/lang/Integer;

    iget-object v3, p1, LX/Ipl;->receiverStatus:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2613842
    :cond_1b
    iget-object v0, p0, LX/Ipl;->amountFBDiscount:Ljava/lang/Long;

    if-eqz v0, :cond_46

    move v0, v1

    .line 2613843
    :goto_1a
    iget-object v3, p1, LX/Ipl;->amountFBDiscount:Ljava/lang/Long;

    if-eqz v3, :cond_47

    move v3, v1

    .line 2613844
    :goto_1b
    if-nez v0, :cond_1c

    if-eqz v3, :cond_1d

    .line 2613845
    :cond_1c
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2613846
    iget-object v0, p0, LX/Ipl;->amountFBDiscount:Ljava/lang/Long;

    iget-object v3, p1, LX/Ipl;->amountFBDiscount:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2613847
    :cond_1d
    iget-object v0, p0, LX/Ipl;->commerceOrderId:Ljava/lang/String;

    if-eqz v0, :cond_48

    move v0, v1

    .line 2613848
    :goto_1c
    iget-object v3, p1, LX/Ipl;->commerceOrderId:Ljava/lang/String;

    if-eqz v3, :cond_49

    move v3, v1

    .line 2613849
    :goto_1d
    if-nez v0, :cond_1e

    if-eqz v3, :cond_1f

    .line 2613850
    :cond_1e
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2613851
    iget-object v0, p0, LX/Ipl;->commerceOrderId:Ljava/lang/String;

    iget-object v3, p1, LX/Ipl;->commerceOrderId:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2613852
    :cond_1f
    iget-object v0, p0, LX/Ipl;->platformItemId:Ljava/lang/Long;

    if-eqz v0, :cond_4a

    move v0, v1

    .line 2613853
    :goto_1e
    iget-object v3, p1, LX/Ipl;->platformItemId:Ljava/lang/Long;

    if-eqz v3, :cond_4b

    move v3, v1

    .line 2613854
    :goto_1f
    if-nez v0, :cond_20

    if-eqz v3, :cond_21

    .line 2613855
    :cond_20
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2613856
    iget-object v0, p0, LX/Ipl;->platformItemId:Ljava/lang/Long;

    iget-object v3, p1, LX/Ipl;->platformItemId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2613857
    :cond_21
    iget-object v0, p0, LX/Ipl;->memoText:Ljava/lang/String;

    if-eqz v0, :cond_4c

    move v0, v1

    .line 2613858
    :goto_20
    iget-object v3, p1, LX/Ipl;->memoText:Ljava/lang/String;

    if-eqz v3, :cond_4d

    move v3, v1

    .line 2613859
    :goto_21
    if-nez v0, :cond_22

    if-eqz v3, :cond_23

    .line 2613860
    :cond_22
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2613861
    iget-object v0, p0, LX/Ipl;->memoText:Ljava/lang/String;

    iget-object v3, p1, LX/Ipl;->memoText:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2613862
    :cond_23
    iget-object v0, p0, LX/Ipl;->hasMemoMultimedia:Ljava/lang/Boolean;

    if-eqz v0, :cond_4e

    move v0, v1

    .line 2613863
    :goto_22
    iget-object v3, p1, LX/Ipl;->hasMemoMultimedia:Ljava/lang/Boolean;

    if-eqz v3, :cond_4f

    move v3, v1

    .line 2613864
    :goto_23
    if-nez v0, :cond_24

    if-eqz v3, :cond_25

    .line 2613865
    :cond_24
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2613866
    iget-object v0, p0, LX/Ipl;->hasMemoMultimedia:Ljava/lang/Boolean;

    iget-object v3, p1, LX/Ipl;->hasMemoMultimedia:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2613867
    :cond_25
    iget-object v0, p0, LX/Ipl;->transferType:Ljava/lang/Integer;

    if-eqz v0, :cond_50

    move v0, v1

    .line 2613868
    :goto_24
    iget-object v3, p1, LX/Ipl;->transferType:Ljava/lang/Integer;

    if-eqz v3, :cond_51

    move v3, v1

    .line 2613869
    :goto_25
    if-nez v0, :cond_26

    if-eqz v3, :cond_27

    .line 2613870
    :cond_26
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2613871
    iget-object v0, p0, LX/Ipl;->transferType:Ljava/lang/Integer;

    iget-object v3, p1, LX/Ipl;->transferType:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2613872
    :cond_27
    iget-object v0, p0, LX/Ipl;->themeId:Ljava/lang/Long;

    if-eqz v0, :cond_52

    move v0, v1

    .line 2613873
    :goto_26
    iget-object v3, p1, LX/Ipl;->themeId:Ljava/lang/Long;

    if-eqz v3, :cond_53

    move v3, v1

    .line 2613874
    :goto_27
    if-nez v0, :cond_28

    if-eqz v3, :cond_29

    .line 2613875
    :cond_28
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2613876
    iget-object v0, p0, LX/Ipl;->themeId:Ljava/lang/Long;

    iget-object v3, p1, LX/Ipl;->themeId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2613877
    :cond_29
    iget-object v0, p0, LX/Ipl;->groupThreadFbId:Ljava/lang/Long;

    if-eqz v0, :cond_54

    move v0, v1

    .line 2613878
    :goto_28
    iget-object v3, p1, LX/Ipl;->groupThreadFbId:Ljava/lang/Long;

    if-eqz v3, :cond_55

    move v3, v1

    .line 2613879
    :goto_29
    if-nez v0, :cond_2a

    if-eqz v3, :cond_2b

    .line 2613880
    :cond_2a
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2613881
    iget-object v0, p0, LX/Ipl;->groupThreadFbId:Ljava/lang/Long;

    iget-object v3, p1, LX/Ipl;->groupThreadFbId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2613882
    :cond_2b
    iget-object v0, p0, LX/Ipl;->irisSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_56

    move v0, v1

    .line 2613883
    :goto_2a
    iget-object v3, p1, LX/Ipl;->irisSeqId:Ljava/lang/Long;

    if-eqz v3, :cond_57

    move v3, v1

    .line 2613884
    :goto_2b
    if-nez v0, :cond_2c

    if-eqz v3, :cond_2d

    .line 2613885
    :cond_2c
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2613886
    iget-object v0, p0, LX/Ipl;->irisSeqId:Ljava/lang/Long;

    iget-object v3, p1, LX/Ipl;->irisSeqId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_2d
    move v2, v1

    .line 2613887
    goto/16 :goto_1

    :cond_2e
    move v0, v2

    .line 2613888
    goto/16 :goto_2

    :cond_2f
    move v3, v2

    .line 2613889
    goto/16 :goto_3

    :cond_30
    move v0, v2

    .line 2613890
    goto/16 :goto_4

    :cond_31
    move v3, v2

    .line 2613891
    goto/16 :goto_5

    :cond_32
    move v0, v2

    .line 2613892
    goto/16 :goto_6

    :cond_33
    move v3, v2

    .line 2613893
    goto/16 :goto_7

    :cond_34
    move v0, v2

    .line 2613894
    goto/16 :goto_8

    :cond_35
    move v3, v2

    .line 2613895
    goto/16 :goto_9

    :cond_36
    move v0, v2

    .line 2613896
    goto/16 :goto_a

    :cond_37
    move v3, v2

    .line 2613897
    goto/16 :goto_b

    :cond_38
    move v0, v2

    .line 2613898
    goto/16 :goto_c

    :cond_39
    move v3, v2

    .line 2613899
    goto/16 :goto_d

    :cond_3a
    move v0, v2

    .line 2613900
    goto/16 :goto_e

    :cond_3b
    move v3, v2

    .line 2613901
    goto/16 :goto_f

    :cond_3c
    move v0, v2

    .line 2613902
    goto/16 :goto_10

    :cond_3d
    move v3, v2

    .line 2613903
    goto/16 :goto_11

    :cond_3e
    move v0, v2

    .line 2613904
    goto/16 :goto_12

    :cond_3f
    move v3, v2

    .line 2613905
    goto/16 :goto_13

    :cond_40
    move v0, v2

    .line 2613906
    goto/16 :goto_14

    :cond_41
    move v3, v2

    .line 2613907
    goto/16 :goto_15

    :cond_42
    move v0, v2

    .line 2613908
    goto/16 :goto_16

    :cond_43
    move v3, v2

    .line 2613909
    goto/16 :goto_17

    :cond_44
    move v0, v2

    .line 2613910
    goto/16 :goto_18

    :cond_45
    move v3, v2

    .line 2613911
    goto/16 :goto_19

    :cond_46
    move v0, v2

    .line 2613912
    goto/16 :goto_1a

    :cond_47
    move v3, v2

    .line 2613913
    goto/16 :goto_1b

    :cond_48
    move v0, v2

    .line 2613914
    goto/16 :goto_1c

    :cond_49
    move v3, v2

    .line 2613915
    goto/16 :goto_1d

    :cond_4a
    move v0, v2

    .line 2613916
    goto/16 :goto_1e

    :cond_4b
    move v3, v2

    .line 2613917
    goto/16 :goto_1f

    :cond_4c
    move v0, v2

    .line 2613918
    goto/16 :goto_20

    :cond_4d
    move v3, v2

    .line 2613919
    goto/16 :goto_21

    :cond_4e
    move v0, v2

    .line 2613920
    goto/16 :goto_22

    :cond_4f
    move v3, v2

    .line 2613921
    goto/16 :goto_23

    :cond_50
    move v0, v2

    .line 2613922
    goto/16 :goto_24

    :cond_51
    move v3, v2

    .line 2613923
    goto/16 :goto_25

    :cond_52
    move v0, v2

    .line 2613924
    goto/16 :goto_26

    :cond_53
    move v3, v2

    .line 2613925
    goto/16 :goto_27

    :cond_54
    move v0, v2

    .line 2613926
    goto/16 :goto_28

    :cond_55
    move v3, v2

    .line 2613927
    goto/16 :goto_29

    :cond_56
    move v0, v2

    .line 2613928
    goto/16 :goto_2a

    :cond_57
    move v3, v2

    .line 2613929
    goto/16 :goto_2b
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2613774
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2613771
    sget-boolean v0, LX/Ipl;->a:Z

    .line 2613772
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/Ipl;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2613773
    return-object v0
.end method
