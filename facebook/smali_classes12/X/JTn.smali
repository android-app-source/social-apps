.class public LX/JTn;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JTl;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JTo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2697233
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/JTn;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JTo;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2697234
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2697235
    iput-object p1, p0, LX/JTn;->b:LX/0Ot;

    .line 2697236
    return-void
.end method

.method public static a(LX/0QB;)LX/JTn;
    .locals 4

    .prologue
    .line 2697237
    const-class v1, LX/JTn;

    monitor-enter v1

    .line 2697238
    :try_start_0
    sget-object v0, LX/JTn;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2697239
    sput-object v2, LX/JTn;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2697240
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2697241
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2697242
    new-instance v3, LX/JTn;

    const/16 p0, 0x2053

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JTn;-><init>(LX/0Ot;)V

    .line 2697243
    move-object v0, v3

    .line 2697244
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2697245
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JTn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2697246
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2697247
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 2697248
    check-cast p2, LX/JTm;

    .line 2697249
    iget-object v0, p0, LX/JTn;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JTo;

    iget-object v1, p2, LX/JTm;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/JTm;->b:LX/1SX;

    iget-object v3, p2, LX/JTm;->c:LX/2ei;

    const/4 p2, 0x1

    .line 2697250
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x2

    invoke-interface {v4, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, p2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object p0

    .line 2697251
    iget-object v4, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 2697252
    check-cast v4, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    invoke-virtual {v3}, LX/2ei;->getColorResource()I

    move-result p0

    invoke-virtual {v4, p0}, LX/1ne;->n(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v3}, LX/2ei;->getFontSizeResource()I

    move-result p0

    invoke-virtual {v4, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v3}, LX/2ei;->getFontStyle()I

    move-result p0

    invoke-virtual {v4, p0}, LX/1ne;->t(I)LX/1ne;

    move-result-object v4

    const/4 p0, 0x0

    invoke-virtual {v4, p0}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const/high16 p0, 0x3f800000    # 1.0f

    invoke-interface {v4, p0}, LX/1Di;->a(F)LX/1Di;

    move-result-object v4

    const p0, 0x7f0b08fe

    invoke-interface {v4, p2, p0}, LX/1Di;->g(II)LX/1Di;

    move-result-object v4

    invoke-interface {v5, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    iget-object v5, v0, LX/JTo;->a:LX/1vb;

    invoke-virtual {v5, p1}, LX/1vb;->c(LX/1De;)LX/1vd;

    move-result-object v5

    invoke-virtual {v5, v1}, LX/1vd;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1vd;

    move-result-object v5

    sget-object p0, LX/1dl;->CLICKABLE:LX/1dl;

    invoke-virtual {v5, p0}, LX/1vd;->a(LX/1dl;)LX/1vd;

    move-result-object v5

    invoke-virtual {v5, v2}, LX/1vd;->a(LX/1SX;)LX/1vd;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 2697253
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2697254
    invoke-static {}, LX/1dS;->b()V

    .line 2697255
    const/4 v0, 0x0

    return-object v0
.end method
