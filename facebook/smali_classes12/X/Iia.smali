.class public final LX/Iia;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 19

    .prologue
    .line 2604834
    const/4 v14, 0x0

    .line 2604835
    const/4 v13, 0x0

    .line 2604836
    const/4 v12, 0x0

    .line 2604837
    const/4 v11, 0x0

    .line 2604838
    const/4 v10, 0x0

    .line 2604839
    const-wide/16 v8, 0x0

    .line 2604840
    const/4 v7, 0x0

    .line 2604841
    const/4 v6, 0x0

    .line 2604842
    const/4 v5, 0x0

    .line 2604843
    const/4 v4, 0x0

    .line 2604844
    const/4 v3, 0x0

    .line 2604845
    const/4 v2, 0x0

    .line 2604846
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v15

    sget-object v16, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v16

    if-eq v15, v0, :cond_e

    .line 2604847
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2604848
    const/4 v2, 0x0

    .line 2604849
    :goto_0
    return v2

    .line 2604850
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v16, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v16

    if-eq v2, v0, :cond_9

    .line 2604851
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 2604852
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2604853
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_0

    if-eqz v2, :cond_0

    .line 2604854
    const-string v16, "dimensionless_cache_key"

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_1

    .line 2604855
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v15, v2

    goto :goto_1

    .line 2604856
    :cond_1
    const-string v16, "height"

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_2

    .line 2604857
    const/4 v2, 0x1

    .line 2604858
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v7

    move v14, v7

    move v7, v2

    goto :goto_1

    .line 2604859
    :cond_2
    const-string v16, "is_silhouette"

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_3

    .line 2604860
    const/4 v2, 0x1

    .line 2604861
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v13, v6

    move v6, v2

    goto :goto_1

    .line 2604862
    :cond_3
    const-string v16, "mime_type"

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_4

    .line 2604863
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v12, v2

    goto :goto_1

    .line 2604864
    :cond_4
    const-string v16, "name"

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_5

    .line 2604865
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v11, v2

    goto/16 :goto_1

    .line 2604866
    :cond_5
    const-string v16, "scale"

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_6

    .line 2604867
    const/4 v2, 0x1

    .line 2604868
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    move v3, v2

    goto/16 :goto_1

    .line 2604869
    :cond_6
    const-string v16, "uri"

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_7

    .line 2604870
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v10, v2

    goto/16 :goto_1

    .line 2604871
    :cond_7
    const-string v16, "width"

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 2604872
    const/4 v2, 0x1

    .line 2604873
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v8

    move v9, v8

    move v8, v2

    goto/16 :goto_1

    .line 2604874
    :cond_8
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 2604875
    :cond_9
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 2604876
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 2604877
    if-eqz v7, :cond_a

    .line 2604878
    const/4 v2, 0x1

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14, v7}, LX/186;->a(III)V

    .line 2604879
    :cond_a
    if-eqz v6, :cond_b

    .line 2604880
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->a(IZ)V

    .line 2604881
    :cond_b
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 2604882
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 2604883
    if-eqz v3, :cond_c

    .line 2604884
    const/4 v3, 0x5

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 2604885
    :cond_c
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 2604886
    if-eqz v8, :cond_d

    .line 2604887
    const/4 v2, 0x7

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9, v3}, LX/186;->a(III)V

    .line 2604888
    :cond_d
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_e
    move v15, v14

    move v14, v13

    move v13, v12

    move v12, v11

    move v11, v10

    move v10, v7

    move v7, v5

    move/from16 v18, v6

    move v6, v4

    move-wide v4, v8

    move/from16 v9, v18

    move v8, v2

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 2604889
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2604890
    invoke-virtual {p0, p1, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2604891
    if-eqz v0, :cond_0

    .line 2604892
    const-string v1, "dimensionless_cache_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2604893
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2604894
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 2604895
    if-eqz v0, :cond_1

    .line 2604896
    const-string v1, "height"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2604897
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2604898
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2604899
    if-eqz v0, :cond_2

    .line 2604900
    const-string v1, "is_silhouette"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2604901
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2604902
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2604903
    if-eqz v0, :cond_3

    .line 2604904
    const-string v1, "mime_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2604905
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2604906
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2604907
    if-eqz v0, :cond_4

    .line 2604908
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2604909
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2604910
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 2604911
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_5

    .line 2604912
    const-string v2, "scale"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2604913
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 2604914
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2604915
    if-eqz v0, :cond_6

    .line 2604916
    const-string v1, "uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2604917
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2604918
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 2604919
    if-eqz v0, :cond_7

    .line 2604920
    const-string v1, "width"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2604921
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2604922
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2604923
    return-void
.end method
