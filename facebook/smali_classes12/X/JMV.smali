.class public LX/JMV;
.super LX/5p5;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "FBPerformanceLogger"
.end annotation


# instance fields
.field private final a:LX/JK6;


# direct methods
.method public constructor <init>(LX/JK6;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2684151
    invoke-direct {p0}, LX/5p5;-><init>()V

    .line 2684152
    iput-object p1, p0, LX/JMV;->a:LX/JK6;

    .line 2684153
    return-void
.end method

.method public static b(LX/0QB;)LX/JMV;
    .locals 2

    .prologue
    .line 2684149
    new-instance v1, LX/JMV;

    invoke-static {p0}, LX/JK6;->a(LX/0QB;)LX/JK6;

    move-result-object v0

    check-cast v0, LX/JK6;

    invoke-direct {v1, v0}, LX/JMV;-><init>(LX/JK6;)V

    .line 2684150
    return-object v1
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2684145
    const-string v0, "FBPerformanceLogger"

    return-object v0
.end method

.method public logEvents(LX/5pG;Ljava/lang/String;)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2684146
    const-string v0, "events_dashboard"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2684147
    iget-object v0, p0, LX/JMV;->a:LX/JK6;

    invoke-virtual {v0, p1}, LX/JK6;->a(LX/5pG;)V

    .line 2684148
    :cond_0
    return-void
.end method
