.class public LX/Hbd;
.super LX/1P1;
.source ""


# instance fields
.field private a:Landroid/content/Context;

.field public b:I

.field public c:Z

.field private d:Z

.field public e:LX/Haf;

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field public t:I

.field public u:I


# direct methods
.method public constructor <init>(Landroid/content/Context;IZ)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 2485979
    invoke-direct {p0, p2, p3}, LX/1P1;-><init>(IZ)V

    .line 2485980
    iput-boolean v1, p0, LX/Hbd;->c:Z

    .line 2485981
    iput-boolean v1, p0, LX/Hbd;->d:Z

    .line 2485982
    iput v0, p0, LX/Hbd;->f:I

    iput v0, p0, LX/Hbd;->g:I

    .line 2485983
    iput v0, p0, LX/Hbd;->h:I

    iput v0, p0, LX/Hbd;->i:I

    .line 2485984
    iput v0, p0, LX/Hbd;->t:I

    .line 2485985
    const/16 v0, 0x64

    iput v0, p0, LX/Hbd;->u:I

    .line 2485986
    iput-object p1, p0, LX/Hbd;->a:Landroid/content/Context;

    .line 2485987
    return-void
.end method

.method private a(D)D
    .locals 3

    .prologue
    .line 2485978
    iget-object v0, p0, LX/Hbd;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-double v0, v0

    mul-double/2addr v0, p1

    return-wide v0
.end method


# virtual methods
.method public final a(Landroid/support/v7/widget/RecyclerView;LX/1Ok;I)V
    .locals 6

    .prologue
    .line 2485988
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2485989
    iget v0, p0, LX/Hbd;->b:I

    if-nez v0, :cond_1

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v0

    .line 2485990
    :goto_0
    invoke-static {v1}, Landroid/support/v7/widget/RecyclerView;->d(Landroid/view/View;)I

    move-result v2

    .line 2485991
    sub-int/2addr v2, p3

    mul-int/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v3

    .line 2485992
    if-nez v3, :cond_0

    .line 2485993
    invoke-virtual {v1}, Landroid/view/View;->getY()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-int v3, v0

    .line 2485994
    :cond_0
    new-instance v0, LX/Hbc;

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget v4, p0, LX/Hbd;->u:I

    iget v5, p0, LX/Hbd;->t:I

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, LX/Hbc;-><init>(LX/Hbd;Landroid/content/Context;III)V

    .line 2485995
    iput p3, v0, LX/25Y;->a:I

    .line 2485996
    invoke-virtual {p0, v0}, LX/1OR;->a(LX/25Y;)V

    .line 2485997
    return-void

    .line 2485998
    :cond_1
    iget v0, p0, LX/Hbd;->b:I

    goto :goto_0
.end method

.method public final b(ILX/1Od;LX/1Ok;)I
    .locals 10

    .prologue
    .line 2485944
    iget-boolean v0, p0, LX/Hbd;->d:Z

    if-eqz v0, :cond_0

    .line 2485945
    invoke-super {p0, p1, p2, p3}, LX/1P1;->b(ILX/1Od;LX/1Ok;)I

    .line 2485946
    :goto_0
    return p1

    .line 2485947
    :cond_0
    const/4 v0, -0x1

    .line 2485948
    invoke-virtual {p0}, LX/1P1;->n()I

    move-result v3

    .line 2485949
    invoke-virtual {p0, v3}, LX/1OR;->c(I)Landroid/view/View;

    move-result-object v1

    .line 2485950
    if-eqz v1, :cond_1

    .line 2485951
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v0

    .line 2485952
    :cond_1
    const/4 v1, -0x1

    .line 2485953
    invoke-virtual {p0}, LX/1P1;->l()I

    move-result v4

    .line 2485954
    invoke-virtual {p0, v4}, LX/1OR;->c(I)Landroid/view/View;

    move-result-object v2

    .line 2485955
    if-eqz v2, :cond_b

    .line 2485956
    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v1

    move v2, v1

    .line 2485957
    :goto_1
    if-gez p1, :cond_5

    .line 2485958
    iget v1, p0, LX/Hbd;->h:I

    if-ne v1, v4, :cond_3

    iget v1, p0, LX/Hbd;->f:I

    const/4 v5, -0x1

    if-eq v1, v5, :cond_3

    .line 2485959
    iget v1, p0, LX/Hbd;->f:I

    int-to-double v6, v1

    const-wide v8, 0x3fb999999999999aL    # 0.1

    invoke-direct {p0, v8, v9}, LX/Hbd;->a(D)D

    move-result-wide v8

    cmpg-double v1, v6, v8

    if-gez v1, :cond_2

    int-to-double v6, v2

    const-wide v8, 0x3fb999999999999aL    # 0.1

    invoke-direct {p0, v8, v9}, LX/Hbd;->a(D)D

    move-result-wide v8

    cmpl-double v1, v6, v8

    if-ltz v1, :cond_2

    .line 2485960
    iget-object v1, p0, LX/Hbd;->e:LX/Haf;

    const-wide v6, 0x3fb999999999999aL    # 0.1

    invoke-virtual {v1, v4, v6, v7}, LX/Haf;->b(ID)V

    .line 2485961
    :cond_2
    iget v1, p0, LX/Hbd;->f:I

    int-to-double v6, v1

    const-wide v8, 0x3fa999999999999aL    # 0.05

    invoke-direct {p0, v8, v9}, LX/Hbd;->a(D)D

    move-result-wide v8

    cmpg-double v1, v6, v8

    if-gez v1, :cond_3

    int-to-double v6, v2

    const-wide v8, 0x3fa999999999999aL    # 0.05

    invoke-direct {p0, v8, v9}, LX/Hbd;->a(D)D

    move-result-wide v8

    cmpl-double v1, v6, v8

    if-ltz v1, :cond_3

    .line 2485962
    iget-object v1, p0, LX/Hbd;->e:LX/Haf;

    const-wide v6, 0x3fa999999999999aL    # 0.05

    invoke-virtual {v1, v4, v6, v7}, LX/Haf;->b(ID)V

    .line 2485963
    :cond_3
    iget-boolean v1, p0, LX/Hbd;->c:Z

    if-eqz v1, :cond_a

    int-to-double v6, v2

    const-wide v8, 0x3fb999999999999aL    # 0.1

    invoke-direct {p0, v8, v9}, LX/Hbd;->a(D)D

    move-result-wide v8

    cmpg-double v1, v6, v8

    if-gtz v1, :cond_a

    .line 2485964
    int-to-double v6, p1

    const-wide v8, 0x3fe3333333333333L    # 0.6

    mul-double/2addr v6, v8

    double-to-int v1, v6

    .line 2485965
    :goto_2
    invoke-virtual {p0}, LX/1OR;->D()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-ne v3, v5, :cond_9

    int-to-double v6, v0

    const-wide v8, 0x3fb999999999999aL    # 0.1

    invoke-direct {p0, v8, v9}, LX/Hbd;->a(D)D

    move-result-wide v8

    cmpg-double v5, v6, v8

    if-gtz v5, :cond_9

    .line 2485966
    int-to-double v6, p1

    const-wide v8, 0x3fe3333333333333L    # 0.6

    mul-double/2addr v6, v8

    double-to-int p1, v6

    .line 2485967
    :cond_4
    :goto_3
    invoke-super {p0, p1, p2, p3}, LX/1P1;->b(ILX/1Od;LX/1Ok;)I

    .line 2485968
    iput v2, p0, LX/Hbd;->f:I

    .line 2485969
    iput v0, p0, LX/Hbd;->g:I

    .line 2485970
    iput v4, p0, LX/Hbd;->h:I

    .line 2485971
    iput v3, p0, LX/Hbd;->i:I

    goto/16 :goto_0

    .line 2485972
    :cond_5
    iget v1, p0, LX/Hbd;->i:I

    if-ne v1, v3, :cond_6

    iget v1, p0, LX/Hbd;->g:I

    const/4 v5, -0x1

    if-eq v1, v5, :cond_6

    iget v1, p0, LX/Hbd;->g:I

    int-to-double v6, v1

    const-wide v8, 0x3feccccccccccccdL    # 0.9

    invoke-direct {p0, v8, v9}, LX/Hbd;->a(D)D

    move-result-wide v8

    cmpl-double v1, v6, v8

    if-lez v1, :cond_6

    int-to-double v6, v0

    const-wide v8, 0x3feccccccccccccdL    # 0.9

    invoke-direct {p0, v8, v9}, LX/Hbd;->a(D)D

    move-result-wide v8

    cmpg-double v1, v6, v8

    if-gtz v1, :cond_6

    .line 2485973
    iget-object v1, p0, LX/Hbd;->e:LX/Haf;

    const-wide v6, 0x3feccccccccccccdL    # 0.9

    invoke-virtual {v1, v3, v6, v7}, LX/Haf;->a(ID)V

    .line 2485974
    :cond_6
    invoke-virtual {p0}, LX/1OR;->D()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-eq v3, v1, :cond_7

    iget-boolean v1, p0, LX/Hbd;->c:Z

    if-eqz v1, :cond_8

    :cond_7
    int-to-double v6, v0

    const-wide v8, 0x3feccccccccccccdL    # 0.9

    invoke-direct {p0, v8, v9}, LX/Hbd;->a(D)D

    move-result-wide v8

    cmpl-double v1, v6, v8

    if-lez v1, :cond_8

    .line 2485975
    int-to-double v6, p1

    const-wide v8, 0x3fe3333333333333L    # 0.6

    mul-double/2addr v6, v8

    double-to-int p1, v6

    .line 2485976
    :cond_8
    iget v1, p0, LX/Hbd;->h:I

    if-ne v1, v4, :cond_4

    iget v1, p0, LX/Hbd;->f:I

    const/4 v5, -0x1

    if-eq v1, v5, :cond_4

    iget v1, p0, LX/Hbd;->f:I

    int-to-double v6, v1

    const-wide v8, 0x3fa999999999999aL    # 0.05

    invoke-direct {p0, v8, v9}, LX/Hbd;->a(D)D

    move-result-wide v8

    cmpl-double v1, v6, v8

    if-lez v1, :cond_4

    int-to-double v6, v2

    const-wide v8, 0x3fa999999999999aL    # 0.05

    invoke-direct {p0, v8, v9}, LX/Hbd;->a(D)D

    move-result-wide v8

    cmpg-double v1, v6, v8

    if-gtz v1, :cond_4

    .line 2485977
    iget-object v1, p0, LX/Hbd;->e:LX/Haf;

    const-wide v6, 0x3fa999999999999aL    # 0.05

    invoke-virtual {v1, v4, v6, v7}, LX/Haf;->c(ID)V

    goto/16 :goto_3

    :cond_9
    move p1, v1

    goto/16 :goto_3

    :cond_a
    move v1, p1

    goto/16 :goto_2

    :cond_b
    move v2, v1

    goto/16 :goto_1
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 2485941
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Hbd;->d:Z

    .line 2485942
    const/4 v0, -0x1

    iput v0, p0, LX/Hbd;->i:I

    iput v0, p0, LX/Hbd;->h:I

    iput v0, p0, LX/Hbd;->g:I

    iput v0, p0, LX/Hbd;->f:I

    .line 2485943
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 2485939
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Hbd;->d:Z

    .line 2485940
    return-void
.end method
