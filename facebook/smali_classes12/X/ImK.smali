.class public LX/ImK;
.super LX/ImH;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final d:Ljava/lang/Object;


# instance fields
.field private final a:Lcom/facebook/gk/store/GatekeeperWriter;

.field private final b:LX/IzM;

.field private final c:LX/J0B;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2609438
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/ImK;->d:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/gk/store/GatekeeperWriter;LX/IzM;LX/J0B;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2609439
    invoke-direct {p0}, LX/ImH;-><init>()V

    .line 2609440
    iput-object p1, p0, LX/ImK;->a:Lcom/facebook/gk/store/GatekeeperWriter;

    .line 2609441
    iput-object p2, p0, LX/ImK;->b:LX/IzM;

    .line 2609442
    iput-object p3, p0, LX/ImK;->c:LX/J0B;

    .line 2609443
    return-void
.end method

.method public static a(LX/0QB;)LX/ImK;
    .locals 9

    .prologue
    .line 2609444
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2609445
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2609446
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2609447
    if-nez v1, :cond_0

    .line 2609448
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2609449
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2609450
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2609451
    sget-object v1, LX/ImK;->d:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2609452
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2609453
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2609454
    :cond_1
    if-nez v1, :cond_4

    .line 2609455
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2609456
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2609457
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2609458
    new-instance p0, LX/ImK;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v1

    check-cast v1, Lcom/facebook/gk/store/GatekeeperWriter;

    invoke-static {v0}, LX/IzM;->a(LX/0QB;)LX/IzM;

    move-result-object v7

    check-cast v7, LX/IzM;

    invoke-static {v0}, LX/J0B;->a(LX/0QB;)LX/J0B;

    move-result-object v8

    check-cast v8, LX/J0B;

    invoke-direct {p0, v1, v7, v8}, LX/ImK;-><init>(Lcom/facebook/gk/store/GatekeeperWriter;LX/IzM;LX/J0B;)V

    .line 2609459
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2609460
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2609461
    if-nez v1, :cond_2

    .line 2609462
    sget-object v0, LX/ImK;->d:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ImK;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2609463
    :goto_1
    if-eqz v0, :cond_3

    .line 2609464
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2609465
    :goto_3
    check-cast v0, LX/ImK;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2609466
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2609467
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2609468
    :catchall_1
    move-exception v0

    .line 2609469
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2609470
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2609471
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2609472
    :cond_2
    :try_start_8
    sget-object v0, LX/ImK;->d:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ImK;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(LX/7GJ;)Landroid/os/Bundle;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7GJ",
            "<",
            "LX/Ipt;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 2609473
    iget-object v0, p1, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/Ipt;

    invoke-virtual {v0}, LX/Ipt;->g()LX/Ipm;

    move-result-object v0

    .line 2609474
    iget-object v0, v0, LX/Ipm;->enabled:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 2609475
    iget-object v1, p0, LX/ImK;->a:Lcom/facebook/gk/store/GatekeeperWriter;

    invoke-interface {v1}, Lcom/facebook/gk/store/GatekeeperWriter;->e()LX/2LD;

    move-result-object v1

    const/16 v2, 0x5a4

    invoke-interface {v1, v2, v0}, LX/2LD;->a(IZ)LX/2LD;

    move-result-object v1

    invoke-interface {v1}, LX/2LD;->a()V

    .line 2609476
    iget-object v1, p0, LX/ImK;->b:LX/IzM;

    sget-object v2, LX/IzL;->j:LX/IzK;

    invoke-static {v0}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/2Iu;->a(LX/0To;LX/03R;)V

    .line 2609477
    iget-object v1, p0, LX/ImK;->c:LX/J0B;

    .line 2609478
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 2609479
    const-string v3, "extra_payment_account_enabled_status"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2609480
    const-string v3, "com.facebook.messaging.payment.ACTION_PAYMENT_ACCOUNT_ENABLED_STATUS_UPDATED"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2609481
    invoke-static {v1, v2}, LX/J0B;->a(LX/J0B;Landroid/content/Intent;)V

    .line 2609482
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2609483
    const-string v1, "newPaymentsResult"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2609484
    return-object v0
.end method

.method public final a(Landroid/os/Bundle;LX/7GJ;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "LX/7GJ",
            "<",
            "LX/Ipt;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2609485
    return-void
.end method
