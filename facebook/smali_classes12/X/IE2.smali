.class public LX/IE2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/Filterable;
.implements LX/BWf;
.implements LX/DSX;


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "LX/IDH;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/2Rd;

.field public c:Z

.field public d:Z

.field private e:LX/EWL;

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/IDH;",
            ">;"
        }
    .end annotation
.end field

.field private g:Landroid/widget/Filter;

.field public h:Ljava/lang/String;

.field private i:[Ljava/lang/Object;


# direct methods
.method public constructor <init>(LX/2Rd;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2551458
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2551459
    invoke-static {}, LX/0PM;->d()Ljava/util/LinkedHashMap;

    move-result-object v0

    iput-object v0, p0, LX/IE2;->a:Ljava/util/Map;

    .line 2551460
    sget-object v0, LX/EWL;->a:LX/EWL;

    iput-object v0, p0, LX/IE2;->e:LX/EWL;

    .line 2551461
    iput-object p1, p0, LX/IE2;->b:LX/2Rd;

    .line 2551462
    invoke-static {p0}, LX/IE2;->e(LX/IE2;)V

    .line 2551463
    return-void
.end method

.method public static b(LX/0QB;)LX/IE2;
    .locals 2

    .prologue
    .line 2551464
    new-instance v1, LX/IE2;

    invoke-static {p0}, LX/2Rd;->b(LX/0QB;)LX/2Rd;

    move-result-object v0

    check-cast v0, LX/2Rd;

    invoke-direct {v1, v0}, LX/IE2;-><init>(LX/2Rd;)V

    .line 2551465
    return-object v1
.end method

.method public static e(LX/IE2;)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 2551466
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 2551467
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2551468
    iget-boolean v0, p0, LX/IE2;->c:Z

    if-nez v0, :cond_1

    .line 2551469
    iget-object v0, p0, LX/IE2;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2551470
    new-instance v0, LX/EWI;

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1, v8}, LX/EWI;-><init>(LX/0Px;Z)V

    .line 2551471
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2551472
    :goto_0
    new-instance v0, LX/EWL;

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, LX/EWL;-><init>(LX/0Px;)V

    iput-object v0, p0, LX/IE2;->e:LX/EWL;

    .line 2551473
    iget-boolean v0, p0, LX/IE2;->d:Z

    if-eqz v0, :cond_0

    .line 2551474
    invoke-static {p0}, LX/IE2;->g(LX/IE2;)V

    .line 2551475
    :cond_0
    return-void

    .line 2551476
    :cond_1
    const-string v2, ""

    .line 2551477
    iget-object v0, p0, LX/IE2;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IDH;

    .line 2551478
    iget-object v1, v0, LX/IDH;->d:Ljava/lang/String;

    move-object v1, v1

    .line 2551479
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2551480
    iget-object v1, v0, LX/IDH;->d:Ljava/lang/String;

    move-object v1, v1

    .line 2551481
    invoke-virtual {v1, v8, v9}, Ljava/lang/String;->offsetByCodePoints(II)I

    move-result v6

    invoke-virtual {v1, v8, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 2551482
    iget-object v1, p0, LX/IE2;->b:LX/2Rd;

    invoke-virtual {v1, v6}, LX/2Rd;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2551483
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 2551484
    new-instance v2, LX/EWI;

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    invoke-direct {v2, v3, v9}, LX/EWI;-><init>(LX/0Px;Z)V

    .line 2551485
    invoke-virtual {v2}, LX/EWI;->a()I

    move-result v3

    if-eqz v3, :cond_3

    .line 2551486
    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2551487
    :cond_3
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2551488
    invoke-virtual {v2, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2551489
    :goto_2
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-object v3, v2

    move-object v2, v1

    .line 2551490
    goto :goto_1

    .line 2551491
    :cond_4
    new-instance v0, LX/EWI;

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1, v9}, LX/EWI;-><init>(LX/0Px;Z)V

    .line 2551492
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    :cond_5
    move-object v1, v2

    move-object v2, v3

    goto :goto_2
.end method

.method public static declared-synchronized f(LX/IE2;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/IDH;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2551513
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/IE2;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static g(LX/IE2;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2551493
    iget-object v1, p0, LX/IE2;->e:LX/EWL;

    invoke-virtual {v1}, LX/EWL;->getSections()[Ljava/lang/Object;

    move-result-object v1

    array-length v1, v1

    add-int/lit8 v1, v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iput-object v1, p0, LX/IE2;->i:[Ljava/lang/Object;

    .line 2551494
    iget-object v1, p0, LX/IE2;->i:[Ljava/lang/Object;

    const-string v2, ""

    aput-object v2, v1, v0

    .line 2551495
    :goto_0
    iget-object v1, p0, LX/IE2;->e:LX/EWL;

    invoke-virtual {v1}, LX/EWL;->getSections()[Ljava/lang/Object;

    move-result-object v1

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 2551496
    iget-object v1, p0, LX/IE2;->i:[Ljava/lang/Object;

    add-int/lit8 v2, v0, 0x1

    iget-object v3, p0, LX/IE2;->e:LX/EWL;

    invoke-virtual {v3}, LX/EWL;->getSections()[Ljava/lang/Object;

    move-result-object v3

    aget-object v3, v3, v0

    aput-object v3, v1, v2

    .line 2551497
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2551498
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 2551499
    invoke-virtual {p0}, LX/IE2;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/IE2;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 2551500
    :goto_0
    iget-boolean v1, p0, LX/IE2;->d:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    add-int/2addr v0, v1

    return v0

    .line 2551501
    :cond_0
    iget-object v0, p0, LX/IE2;->e:LX/EWL;

    .line 2551502
    iget v1, v0, LX/EWL;->d:I

    move v0, v1

    .line 2551503
    goto :goto_0

    .line 2551504
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2551505
    iget-boolean v0, p0, LX/IE2;->d:Z

    if-eqz v0, :cond_1

    .line 2551506
    if-nez p1, :cond_0

    .line 2551507
    const/4 v0, 0x0

    .line 2551508
    :goto_0
    return-object v0

    .line 2551509
    :cond_0
    add-int/lit8 p1, p1, -0x1

    .line 2551510
    :cond_1
    invoke-virtual {p0}, LX/IE2;->d()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2551511
    iget-object v0, p0, LX/IE2;->e:LX/EWL;

    invoke-virtual {v0, p1}, LX/EWL;->a(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 2551512
    :cond_2
    iget-object v0, p0, LX/IE2;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(J)V
    .locals 3

    .prologue
    .line 2551451
    iget-object v0, p0, LX/IE2;->a:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2551452
    invoke-virtual {p0}, LX/IE2;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2551453
    iget-object v0, p0, LX/IE2;->f:Ljava/util/List;

    iget-object v1, p0, LX/IE2;->a:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 2551454
    :cond_0
    iget-object v0, p0, LX/IE2;->a:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2551455
    :cond_1
    invoke-static {p0}, LX/IE2;->e(LX/IE2;)V

    .line 2551456
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2551457
    invoke-static {}, LX/IE0;->values()[LX/IE0;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public final b(I)I
    .locals 1

    .prologue
    .line 2551414
    iget-boolean v0, p0, LX/IE2;->d:Z

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 2551415
    sget-object v0, LX/IE0;->DISCOVERY_ENTRY_POINT:LX/IE0;

    invoke-virtual {v0}, LX/IE0;->ordinal()I

    move-result v0

    .line 2551416
    :goto_0
    return v0

    .line 2551417
    :cond_0
    invoke-virtual {p0, p1}, LX/IE2;->a(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2551418
    sget-object v0, LX/IE0;->HEADER:LX/IE0;

    invoke-virtual {v0}, LX/IE0;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2551419
    :cond_1
    sget-object v0, LX/IE0;->FRIEND:LX/IE0;

    invoke-virtual {v0}, LX/IE0;->ordinal()I

    move-result v0

    goto :goto_0
.end method

.method public final c()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "LX/IDH;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2551420
    iget-object v0, p0, LX/IE2;->a:Ljava/util/Map;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final c(I)Z
    .locals 1

    .prologue
    .line 2551445
    iget-boolean v0, p0, LX/IE2;->d:Z

    if-eqz v0, :cond_1

    .line 2551446
    if-nez p1, :cond_0

    .line 2551447
    const/4 v0, 0x1

    .line 2551448
    :goto_0
    return v0

    .line 2551449
    :cond_0
    add-int/lit8 p1, p1, -0x1

    .line 2551450
    :cond_1
    iget-object v0, p0, LX/IE2;->e:LX/EWL;

    invoke-virtual {v0, p1}, LX/EWL;->c(I)Z

    move-result v0

    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 2551421
    iget-object v0, p0, LX/IE2;->h:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getFilter()Landroid/widget/Filter;
    .locals 2

    .prologue
    .line 2551422
    iget-object v0, p0, LX/IE2;->g:Landroid/widget/Filter;

    if-nez v0, :cond_0

    .line 2551423
    new-instance v0, LX/IE1;

    invoke-direct {v0, p0}, LX/IE1;-><init>(LX/IE2;)V

    iput-object v0, p0, LX/IE2;->g:Landroid/widget/Filter;

    .line 2551424
    :cond_0
    iget-object v0, p0, LX/IE2;->g:Landroid/widget/Filter;

    return-object v0
.end method

.method public final getPositionForSection(I)I
    .locals 2

    .prologue
    .line 2551425
    iget-boolean v0, p0, LX/IE2;->d:Z

    if-eqz v0, :cond_1

    .line 2551426
    if-nez p1, :cond_0

    .line 2551427
    const/4 v0, 0x0

    .line 2551428
    :goto_0
    return v0

    .line 2551429
    :cond_0
    iget-object v0, p0, LX/IE2;->e:LX/EWL;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, LX/EWL;->getPositionForSection(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2551430
    :cond_1
    iget-object v0, p0, LX/IE2;->e:LX/EWL;

    invoke-virtual {v0, p1}, LX/EWL;->getPositionForSection(I)I

    move-result v0

    goto :goto_0
.end method

.method public final getSectionForPosition(I)I
    .locals 2

    .prologue
    .line 2551431
    iget-boolean v0, p0, LX/IE2;->d:Z

    if-eqz v0, :cond_1

    .line 2551432
    if-nez p1, :cond_0

    .line 2551433
    const/4 v0, 0x0

    .line 2551434
    :goto_0
    return v0

    .line 2551435
    :cond_0
    iget-object v0, p0, LX/IE2;->e:LX/EWL;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, LX/EWL;->getSectionForPosition(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2551436
    :cond_1
    iget-object v0, p0, LX/IE2;->e:LX/EWL;

    invoke-virtual {v0, p1}, LX/EWL;->getSectionForPosition(I)I

    move-result v0

    goto :goto_0
.end method

.method public final getSections()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2551437
    iget-boolean v0, p0, LX/IE2;->d:Z

    if-eqz v0, :cond_0

    .line 2551438
    iget-object v0, p0, LX/IE2;->i:[Ljava/lang/Object;

    .line 2551439
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/IE2;->e:LX/EWL;

    invoke-virtual {v0}, LX/EWL;->getSections()[Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final v_(I)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2551440
    iget-boolean v1, p0, LX/IE2;->d:Z

    if-eqz v1, :cond_2

    .line 2551441
    if-nez p1, :cond_1

    .line 2551442
    :cond_0
    :goto_0
    return v0

    .line 2551443
    :cond_1
    add-int/lit8 p1, p1, -0x1

    .line 2551444
    :cond_2
    invoke-virtual {p0}, LX/IE2;->d()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/IE2;->e:LX/EWL;

    invoke-virtual {v1, p1}, LX/EWL;->v_(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
