.class public LX/JRi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1KL;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1KL",
        "<",
        "Ljava/lang/String;",
        "LX/2oL;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:I

.field private final c:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/graphql/model/GraphQLVideo;

.field private final e:LX/1VK;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;ILX/1VK;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLVideo;",
            "I",
            "LX/1VK;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2693705
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2693706
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 2693707
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2693708
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/1WT;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/JRi;->a:Ljava/lang/String;

    .line 2693709
    iput p3, p0, LX/JRi;->b:I

    .line 2693710
    iput-object p1, p0, LX/JRi;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2693711
    iput-object p2, p0, LX/JRi;->d:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 2693712
    iput-object p4, p0, LX/JRi;->e:LX/1VK;

    .line 2693713
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2693714
    new-instance v0, LX/2oL;

    invoke-direct {v0}, LX/2oL;-><init>()V

    .line 2693715
    iget-object v1, p0, LX/JRi;->e:LX/1VK;

    iget-object v2, p0, LX/JRi;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p0, LX/JRi;->d:Lcom/facebook/graphql/model/GraphQLVideo;

    iget v4, p0, LX/JRi;->b:I

    add-int/lit8 v4, v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, LX/1VK;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;Ljava/lang/Integer;)LX/2oO;

    move-result-object v1

    .line 2693716
    iput-object v1, v0, LX/2oL;->f:LX/2oO;

    .line 2693717
    return-object v0
.end method

.method public final b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2693718
    iget-object v0, p0, LX/JRi;->a:Ljava/lang/String;

    return-object v0
.end method
