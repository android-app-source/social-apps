.class public final LX/HHQ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2448004
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 2448005
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2448006
    :goto_0
    return v1

    .line 2448007
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2448008
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 2448009
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2448010
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2448011
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 2448012
    const-string v4, "nodes"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2448013
    invoke-static {p0, p1}, LX/8J0;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 2448014
    :cond_2
    const-string v4, "page_info"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2448015
    invoke-static {p0, p1}, LX/3Bn;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 2448016
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2448017
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2448018
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2448019
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2447993
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2447994
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2447995
    if-eqz v0, :cond_0

    .line 2447996
    const-string v1, "nodes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2447997
    invoke-static {p0, v0, p2, p3}, LX/8J0;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2447998
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2447999
    if-eqz v0, :cond_1

    .line 2448000
    const-string v1, "page_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2448001
    invoke-static {p0, v0, p2}, LX/3Bn;->a(LX/15i;ILX/0nX;)V

    .line 2448002
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2448003
    return-void
.end method
