.class public LX/HVr;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/CSL;

.field private final b:Lcom/facebook/content/SecureContextHelper;

.field private final c:LX/03V;


# direct methods
.method public constructor <init>(LX/CSL;Lcom/facebook/content/SecureContextHelper;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2475427
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2475428
    iput-object p1, p0, LX/HVr;->a:LX/CSL;

    .line 2475429
    iput-object p2, p0, LX/HVr;->b:Lcom/facebook/content/SecureContextHelper;

    .line 2475430
    iput-object p3, p0, LX/HVr;->c:LX/03V;

    .line 2475431
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;)V
    .locals 7

    .prologue
    .line 2475432
    iget-object v1, p0, LX/HVr;->a:LX/CSL;

    iget-wide v2, p3, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->a:J

    iget-object v4, p3, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->e:Ljava/lang/String;

    const-string v0, "pages_identity"

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "page_permalink_context_row"

    invoke-virtual/range {v1 .. v6}, LX/CSL;->a(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2475433
    if-eqz v0, :cond_0

    .line 2475434
    iget-object v1, p0, LX/HVr;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2475435
    :goto_0
    return-void

    .line 2475436
    :cond_0
    iget-object v0, p0, LX/HVr;->c:LX/03V;

    const-string v1, "page_context_rows_subscribe_to_nearby_events_fail"

    const-string v2, "Failed to resolve nearby events list intent!"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
