.class public LX/IRS;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# static fields
.field public static final g:Ljava/lang/String;


# instance fields
.field public a:LX/DOL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Tf;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Xp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

.field public i:Landroid/support/v7/widget/RecyclerView;

.field public j:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAdapter;

.field public k:Lcom/facebook/fig/header/FigHeader;

.field public l:LX/1P1;

.field public m:Ljava/lang/String;

.field public n:Z

.field public o:Z

.field public p:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel$GroupPostTopicsModel$NodesModel;",
            ">;"
        }
    .end annotation
.end field

.field public q:Ljava/lang/String;

.field public r:LX/0Yd;

.field public s:Z

.field private final t:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2576504
    const-class v0, LX/IRS;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/IRS;->g:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2576502
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/IRS;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2576503
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2576500
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/IRS;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2576501
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    .prologue
    .line 2576467
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2576468
    new-instance v0, LX/IRR;

    invoke-direct {v0, p0}, LX/IRR;-><init>(LX/IRS;)V

    iput-object v0, p0, LX/IRS;->t:LX/0TF;

    .line 2576469
    const/4 v2, 0x0

    .line 2576470
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v3, p0

    check-cast v3, LX/IRS;

    invoke-static {v0}, LX/DOL;->a(LX/0QB;)LX/DOL;

    move-result-object v4

    check-cast v4, LX/DOL;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object p2

    check-cast p2, LX/0tX;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object p3

    check-cast p3, LX/0Tf;

    const/16 p1, 0x15e7

    invoke-static {v0, p1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p1

    invoke-static {v0}, LX/0Xo;->a(LX/0QB;)LX/0Xp;

    move-result-object v0

    check-cast v0, LX/0Xp;

    iput-object v4, v3, LX/IRS;->a:LX/DOL;

    iput-object v5, v3, LX/IRS;->b:Lcom/facebook/content/SecureContextHelper;

    iput-object p2, v3, LX/IRS;->c:LX/0tX;

    iput-object p3, v3, LX/IRS;->d:LX/0Tf;

    iput-object p1, v3, LX/IRS;->e:LX/0Or;

    iput-object v0, v3, LX/IRS;->f:LX/0Xp;

    .line 2576471
    const v0, 0x7f030845

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2576472
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/IRS;->setOrientation(I)V

    .line 2576473
    invoke-virtual {p0}, LX/IRS;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1233

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v2, v0, v2, v2}, LX/IRS;->setPadding(IIII)V

    .line 2576474
    const v0, 0x7f0d158e

    invoke-virtual {p0, v0}, LX/IRS;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/header/FigHeader;

    iput-object v0, p0, LX/IRS;->k:Lcom/facebook/fig/header/FigHeader;

    .line 2576475
    iget-object v0, p0, LX/IRS;->k:Lcom/facebook/fig/header/FigHeader;

    new-instance v1, LX/IRN;

    invoke-direct {v1, p0}, LX/IRN;-><init>(LX/IRS;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/header/FigHeader;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2576476
    const v0, 0x7f0d158f

    invoke-virtual {p0, v0}, LX/IRS;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, LX/IRS;->i:Landroid/support/v7/widget/RecyclerView;

    .line 2576477
    new-instance v0, LX/1P1;

    invoke-direct {v0, v2, v2}, LX/1P1;-><init>(IZ)V

    iput-object v0, p0, LX/IRS;->l:LX/1P1;

    .line 2576478
    new-instance v0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAdapter;

    invoke-virtual {p0}, LX/IRS;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/IRS;->j:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAdapter;

    .line 2576479
    iget-object v0, p0, LX/IRS;->j:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAdapter;

    new-instance v1, LX/IRO;

    invoke-direct {v1, p0}, LX/IRO;-><init>(LX/IRS;)V

    .line 2576480
    iput-object v1, v0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAdapter;->c:LX/IRO;

    .line 2576481
    iget-object v0, p0, LX/IRS;->i:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, LX/IRS;->j:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2576482
    iget-object v0, p0, LX/IRS;->i:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, LX/IRS;->l:LX/1P1;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2576483
    iget-object v0, p0, LX/IRS;->i:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, LX/IRP;

    invoke-direct {v1, p0}, LX/IRP;-><init>(LX/IRS;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setOnScrollListener(LX/1OX;)V

    .line 2576484
    new-instance v0, LX/IRQ;

    invoke-direct {v0, p0}, LX/IRQ;-><init>(LX/IRS;)V

    .line 2576485
    new-instance v1, LX/0Yd;

    const-string v2, "group_discussion_topics_mutation"

    invoke-static {v2, v0}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v0

    invoke-direct {v1, v0}, LX/0Yd;-><init>(Ljava/util/Map;)V

    iput-object v1, p0, LX/IRS;->r:LX/0Yd;

    .line 2576486
    iget-object v0, p0, LX/IRS;->f:LX/0Xp;

    iget-object v1, p0, LX/IRS;->r:LX/0Yd;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "group_discussion_topics_mutation"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, LX/0Xp;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 2576487
    return-void
.end method

.method public static b(LX/IRS;)V
    .locals 3

    .prologue
    .line 2576491
    iget-boolean v0, p0, LX/IRS;->o:Z

    if-nez v0, :cond_1

    iget-object v0, p0, LX/IRS;->m:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2576492
    new-instance v0, LX/9M6;

    invoke-direct {v0}, LX/9M6;-><init>()V

    move-object v0, v0

    .line 2576493
    const-string v1, "count"

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2576494
    const-string v1, "group_id"

    iget-object v2, p0, LX/IRS;->m:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2576495
    iget-object v1, p0, LX/IRS;->p:LX/0Px;

    if-eqz v1, :cond_0

    .line 2576496
    const-string v1, "topics_after"

    iget-object v2, p0, LX/IRS;->q:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2576497
    :cond_0
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2576498
    iget-object v1, p0, LX/IRS;->c:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    iget-object v1, p0, LX/IRS;->t:LX/0TF;

    iget-object v2, p0, LX/IRS;->d:LX/0Tf;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2576499
    :cond_1
    return-void
.end method


# virtual methods
.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x467e9136

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2576488
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onDetachedFromWindow()V

    .line 2576489
    iget-object v1, p0, LX/IRS;->f:LX/0Xp;

    iget-object v2, p0, LX/IRS;->r:LX/0Yd;

    invoke-virtual {v1, v2}, LX/0Xp;->a(Landroid/content/BroadcastReceiver;)V

    .line 2576490
    const/16 v1, 0x2d

    const v2, -0x5a17d8ac

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
