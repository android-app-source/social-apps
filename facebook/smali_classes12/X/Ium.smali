.class public LX/Ium;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Iua;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2627305
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2627306
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/service/model/NewMessageResult;)LX/Iuf;
    .locals 4

    .prologue
    .line 2627307
    iget-object v0, p1, Lcom/facebook/messaging/service/model/NewMessageResult;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v0, v0

    .line 2627308
    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->v:LX/0P1;

    .line 2627309
    if-nez v0, :cond_0

    .line 2627310
    sget-object v0, LX/Iuf;->BUZZ:LX/Iuf;

    .line 2627311
    :goto_0
    return-object v0

    .line 2627312
    :cond_0
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2627313
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v1, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_1
    move v0, v1

    :goto_2
    packed-switch v0, :pswitch_data_0

    goto :goto_1

    .line 2627314
    :pswitch_0
    sget-object v0, LX/Iuf;->FORCE_BUZZ:LX/Iuf;

    goto :goto_0

    .line 2627315
    :sswitch_0
    const-string v3, "force_push"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    goto :goto_2

    :sswitch_1
    const-string v3, "silent_push"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_2

    :sswitch_2
    const-string v3, "no_push"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    goto :goto_2

    .line 2627316
    :pswitch_1
    sget-object v0, LX/Iuf;->FORCE_SILENT:LX/Iuf;

    goto :goto_0

    .line 2627317
    :pswitch_2
    sget-object v0, LX/Iuf;->FORCE_SUPPRESS:LX/Iuf;

    goto :goto_0

    .line 2627318
    :cond_2
    sget-object v0, LX/Iuf;->BUZZ:LX/Iuf;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x806f7ee -> :sswitch_0
        0x23e040c4 -> :sswitch_1
        0x7dc6cad8 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2627319
    const-string v0, "TagRule"

    return-object v0
.end method
