.class public LX/JLK;
.super LX/5pb;
.source ""

# interfaces
.implements LX/5on;


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "GoodwillVideoNativeModule"
.end annotation


# instance fields
.field private final a:LX/98q;

.field private final b:Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;

.field private final c:Lcom/facebook/auth/viewercontext/ViewerContext;

.field private final d:LX/1Nq;

.field private final e:LX/03V;

.field private final f:LX/1Cn;


# direct methods
.method public constructor <init>(LX/98q;Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;Lcom/facebook/auth/viewercontext/ViewerContext;LX/1Nq;LX/03V;LX/1Cn;LX/5pY;)V
    .locals 0
    .param p7    # LX/5pY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2680980
    invoke-direct {p0, p7}, LX/5pb;-><init>(LX/5pY;)V

    .line 2680981
    iput-object p1, p0, LX/JLK;->a:LX/98q;

    .line 2680982
    iput-object p2, p0, LX/JLK;->b:Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;

    .line 2680983
    iput-object p3, p0, LX/JLK;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2680984
    iput-object p4, p0, LX/JLK;->d:LX/1Nq;

    .line 2680985
    iput-object p5, p0, LX/JLK;->e:LX/03V;

    .line 2680986
    iput-object p6, p0, LX/JLK;->f:LX/1Cn;

    .line 2680987
    invoke-virtual {p7, p0}, LX/5pX;->a(LX/5on;)V

    .line 2680988
    return-void
.end method

.method private static a(LX/5pC;)LX/0Px;
    .locals 8
    .param p0    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5pC;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2680961
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2680962
    if-nez p0, :cond_0

    .line 2680963
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2680964
    :goto_0
    return-object v0

    .line 2680965
    :cond_0
    const/4 v0, 0x0

    :goto_1
    invoke-interface {p0}, LX/5pC;->size()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 2680966
    invoke-interface {p0, v0}, LX/5pC;->a(I)LX/5pG;

    move-result-object v1

    .line 2680967
    const-string v3, "id"

    invoke-interface {v1, v3}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "full_name"

    invoke-interface {v1, v3}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2680968
    const-string v3, "id"

    invoke-interface {v1, v3}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 2680969
    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_1

    .line 2680970
    invoke-static {v4, v5}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->a(J)LX/5Rc;

    move-result-object v3

    const-string v4, "full_name"

    invoke-interface {v1, v4}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2680971
    iput-object v4, v3, LX/5Rc;->b:Ljava/lang/String;

    .line 2680972
    move-object v3, v3

    .line 2680973
    const-string v4, "uri"

    invoke-interface {v1, v4}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v4, "uri"

    invoke-interface {v1, v4}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    const-string v4, "uri"

    invoke-interface {v1, v4}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2680974
    :goto_2
    iput-object v1, v3, LX/5Rc;->c:Ljava/lang/String;

    .line 2680975
    move-object v1, v3

    .line 2680976
    invoke-virtual {v1}, LX/5Rc;->a()Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2680977
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2680978
    :cond_2
    const/4 v1, 0x0

    goto :goto_2

    .line 2680979
    :cond_3
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/fbreact/goodwill/GoodwillVideoState;)LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/fbreact/goodwill/GoodwillVideoState;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2680954
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2680955
    iget-object v3, p0, Lcom/facebook/fbreact/goodwill/GoodwillVideoState;->e:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbreact/goodwill/GoodwillVideoState$PhotoData;

    .line 2680956
    iget-object v5, v0, Lcom/facebook/fbreact/goodwill/GoodwillVideoState$PhotoData;->a:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2680957
    new-instance v5, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;

    const-string v6, "0"

    iget-object v0, v0, Lcom/facebook/fbreact/goodwill/GoodwillVideoState$PhotoData;->b:Ljava/lang/String;

    invoke-direct {v5, v6, v0}, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2680958
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2680959
    :cond_0
    new-instance v5, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;

    iget-object v6, v0, Lcom/facebook/fbreact/goodwill/GoodwillVideoState$PhotoData;->a:Ljava/lang/String;

    iget-object v0, v0, Lcom/facebook/fbreact/goodwill/GoodwillVideoState$PhotoData;->b:Ljava/lang/String;

    invoke-direct {v5, v6, v0}, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 2680960
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/content/Intent;)V
    .locals 23

    .prologue
    .line 2680942
    move-object/from16 v0, p0

    iget-object v4, v0, LX/JLK;->a:LX/98q;

    invoke-virtual {v4}, LX/98q;->c()Landroid/os/Bundle;

    move-result-object v4

    .line 2680943
    if-nez v4, :cond_0

    .line 2680944
    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "Cannot load bundle from activity"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 2680945
    :cond_0
    const-string v5, "saved_video_state"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    move-object/from16 v19, v4

    check-cast v19, Lcom/facebook/fbreact/goodwill/GoodwillVideoState;

    .line 2680946
    if-nez v19, :cond_1

    .line 2680947
    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "Cannot load saved video state."

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 2680948
    :cond_1
    const-string v4, "publishPostParams"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    move-object v15, v4

    check-cast v15, Lcom/facebook/composer/publish/common/PublishPostParams;

    .line 2680949
    if-nez v15, :cond_2

    .line 2680950
    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "Composer did not return parameters even though it succeeded!"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 2680951
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, LX/JLK;->b:Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;

    invoke-virtual/range {p0 .. p0}, LX/5pb;->s()LX/5pY;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, LX/JLK;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-virtual {v6}, Lcom/facebook/auth/viewercontext/ViewerContext;->a()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v19

    iget-object v7, v0, Lcom/facebook/fbreact/goodwill/GoodwillVideoState;->a:Ljava/lang/String;

    move-object/from16 v0, v19

    iget-object v8, v0, Lcom/facebook/fbreact/goodwill/GoodwillVideoState;->b:Ljava/lang/String;

    move-object/from16 v0, v19

    iget-object v9, v0, Lcom/facebook/fbreact/goodwill/GoodwillVideoState;->c:Ljava/lang/String;

    const-string v10, "editor"

    move-object/from16 v0, v19

    iget-object v11, v0, Lcom/facebook/fbreact/goodwill/GoodwillVideoState;->d:Ljava/lang/String;

    iget-object v12, v15, Lcom/facebook/composer/publish/common/PublishPostParams;->privacy:Ljava/lang/String;

    iget-object v13, v15, Lcom/facebook/composer/publish/common/PublishPostParams;->rawMessage:Ljava/lang/String;

    iget-object v14, v15, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    iget-object v15, v15, Lcom/facebook/composer/publish/common/PublishPostParams;->taggedIds:LX/0Px;

    invoke-static/range {v19 .. v19}, LX/JLK;->a(Lcom/facebook/fbreact/goodwill/GoodwillVideoState;)LX/0Px;

    move-result-object v16

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/facebook/fbreact/goodwill/GoodwillVideoState;->f:Ljava/lang/String;

    move-object/from16 v17, v0

    new-instance v18, Lcom/facebook/goodwill/publish/GoodwillPublishNotificationConfig;

    invoke-virtual/range {p0 .. p0}, LX/5pb;->s()LX/5pY;

    move-result-object v20

    const v21, 0x7f082a1f

    invoke-virtual/range {v20 .. v21}, LX/5pY;->getString(I)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, LX/JLK;->b(Lcom/facebook/fbreact/goodwill/GoodwillVideoState;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {p0 .. p0}, LX/5pb;->s()LX/5pY;

    move-result-object v21

    const v22, 0x7f082a23

    invoke-virtual/range {v21 .. v22}, LX/5pY;->getString(I)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    move-object/from16 v2, v19

    move-object/from16 v3, v21

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/goodwill/publish/GoodwillPublishNotificationConfig;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/16 v19, 0x0

    invoke-virtual/range {v4 .. v19}, Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;LX/0Px;Ljava/lang/String;Lcom/facebook/goodwill/publish/GoodwillPublishNotificationConfig;Lcom/facebook/goodwill/publish/GoodwillPublishUploadHandler$UploadStatusCallback;)V

    .line 2680952
    move-object/from16 v0, p0

    iget-object v4, v0, LX/JLK;->a:LX/98q;

    invoke-virtual {v4}, LX/98q;->a()V

    .line 2680953
    return-void
.end method

.method private a(Ljava/lang/String;LX/0Pz;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Pz",
            "<",
            "Lcom/facebook/fbreact/goodwill/GoodwillVideoState$PhotoData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2680920
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 2680921
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 2680922
    invoke-virtual {v1}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v3

    .line 2680923
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2680924
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 2680925
    const-string v4, "photos"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2680926
    const-string v4, "photos"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    .line 2680927
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v0, v5, :cond_0

    .line 2680928
    invoke-virtual {v4, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 2680929
    const-string v6, "mutations"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2680930
    const-string v6, "mutations"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    .line 2680931
    const-string v6, "id"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2680932
    const-string v6, "id"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2680933
    const-string v6, "uploaded_"

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2680934
    const/16 v6, 0x9

    invoke-virtual {v5, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 2680935
    invoke-virtual {v2, v5}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 2680936
    new-instance v6, Lcom/facebook/fbreact/goodwill/GoodwillVideoState$PhotoData;

    const/4 v7, 0x0

    invoke-direct {v6, v7, v5}, Lcom/facebook/fbreact/goodwill/GoodwillVideoState$PhotoData;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p2, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2680937
    invoke-virtual {v2, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2680938
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2680939
    :catch_0
    move-exception v0

    .line 2680940
    iget-object v1, p0, LX/JLK;->e:LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Failed to parse share payload when reading from editor!"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2680941
    :cond_2
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2680917
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2680918
    const-class v1, Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;

    invoke-virtual {v0, v1}, LX/5pX;->a(Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;

    invoke-interface {v0, p1, p2}, Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;->emit(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2680919
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/5pC;Ljava/lang/String;)V
    .locals 7
    .param p5    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2680850
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 2680851
    if-eqz p5, :cond_0

    .line 2680852
    const/4 v0, 0x0

    :goto_0
    invoke-interface {p5}, LX/5pC;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 2680853
    invoke-interface {p5, v0}, LX/5pC;->a(I)LX/5pG;

    move-result-object v2

    .line 2680854
    new-instance v3, Lcom/facebook/fbreact/goodwill/GoodwillVideoState$PhotoData;

    const-string v4, "id"

    invoke-interface {v2, v4}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "uri"

    invoke-interface {v2, v5}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v4, v2}, Lcom/facebook/fbreact/goodwill/GoodwillVideoState$PhotoData;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2680855
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2680856
    :cond_0
    if-eqz p6, :cond_1

    .line 2680857
    invoke-direct {p0, p6, v1}, LX/JLK;->a(Ljava/lang/String;LX/0Pz;)V

    .line 2680858
    :cond_1
    new-instance v0, Lcom/facebook/fbreact/goodwill/GoodwillVideoState;

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/facebook/fbreact/goodwill/GoodwillVideoState;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/String;)V

    .line 2680859
    iget-object v1, p0, LX/JLK;->a:LX/98q;

    invoke-virtual {v1}, LX/98q;->c()Landroid/os/Bundle;

    move-result-object v1

    .line 2680860
    if-nez v1, :cond_2

    .line 2680861
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2680862
    :cond_2
    const-string v2, "saved_video_state"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2680863
    iget-object v0, p0, LX/JLK;->a:LX/98q;

    invoke-virtual {v0, v1}, LX/98q;->a(Landroid/os/Bundle;)V

    .line 2680864
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLX/5pC;Ljava/lang/String;)V
    .locals 10
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p10    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2680909
    const/4 v7, 0x0

    .line 2680910
    if-eqz p9, :cond_0

    .line 2680911
    invoke-static/range {p9 .. p9}, LX/JLK;->a(LX/5pC;)LX/0Px;

    move-result-object v7

    .line 2680912
    :cond_0
    sget-object v0, LX/21D;->GOODWILL_REACT:LX/21D;

    iget-object v9, p0, LX/JLK;->d:LX/1Nq;

    move-object v1, p1

    move-object v2, p4

    move-object v3, p5

    move-object/from16 v4, p6

    move-object/from16 v5, p7

    move/from16 v6, p8

    move-object/from16 v8, p10

    invoke-static/range {v0 .. v9}, Lcom/facebook/goodwill/composer/GoodwillVideoComposerLauncher;->a(LX/21D;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLX/0Px;Ljava/lang/String;LX/1Nq;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    .line 2680913
    iget-object v1, p0, LX/JLK;->f:LX/1Cn;

    const-string v2, "editor"

    invoke-virtual {v1, p1, p2, v2, p3}, LX/1Cn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2680914
    invoke-virtual {p0}, LX/5pb;->s()LX/5pY;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2, v0}, Lcom/facebook/ipc/composer/launch/ComposerLaunchActivity;->a(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)Landroid/content/Intent;

    move-result-object v0

    .line 2680915
    invoke-virtual {p0}, LX/5pb;->s()LX/5pY;

    move-result-object v1

    const/16 v2, 0x2711

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, LX/5pX;->a(Landroid/content/Intent;ILandroid/os/Bundle;)Z

    .line 2680916
    return-void
.end method

.method private b(Lcom/facebook/fbreact/goodwill/GoodwillVideoState;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2680903
    iget-object v0, p1, Lcom/facebook/fbreact/goodwill/GoodwillVideoState;->e:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/facebook/fbreact/goodwill/GoodwillVideoState;->e:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p1, Lcom/facebook/fbreact/goodwill/GoodwillVideoState;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2680904
    :cond_1
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2680905
    const v1, 0x7f082a21

    invoke-virtual {v0, v1}, LX/5pY;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2680906
    :goto_0
    return-object v0

    .line 2680907
    :cond_2
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2680908
    const v1, 0x7f082a22

    invoke-virtual {v0, v1}, LX/5pY;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private b(Landroid/content/Intent;)V
    .locals 8

    .prologue
    .line 2680885
    const-string v0, "extra_media_items"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 2680886
    if-nez v2, :cond_0

    .line 2680887
    :goto_0
    return-void

    .line 2680888
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2680889
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 2680890
    new-instance v5, Lcom/facebook/fbreact/goodwill/GoodwillVideoState$PhotoData;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v6, v0}, Lcom/facebook/fbreact/goodwill/GoodwillVideoState$PhotoData;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2680891
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2680892
    :cond_1
    new-instance v0, LX/JLO;

    invoke-direct {v0, v3}, LX/JLO;-><init>(Ljava/util/ArrayList;)V

    .line 2680893
    const-string v1, "carmeraRollPhotoUploaded"

    .line 2680894
    invoke-static {}, LX/5op;->a()LX/5pD;

    move-result-object v4

    .line 2680895
    iget-object v2, v0, LX/JLO;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v2, 0x0

    move v3, v2

    :goto_2
    if-ge v3, v5, :cond_2

    iget-object v2, v0, LX/JLO;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/fbreact/goodwill/GoodwillVideoState$PhotoData;

    .line 2680896
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v6

    .line 2680897
    const-string v7, "id"

    iget-object p1, v2, Lcom/facebook/fbreact/goodwill/GoodwillVideoState$PhotoData;->a:Ljava/lang/String;

    invoke-interface {v6, v7, p1}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2680898
    const-string v7, "uri"

    iget-object v2, v2, Lcom/facebook/fbreact/goodwill/GoodwillVideoState$PhotoData;->b:Ljava/lang/String;

    invoke-interface {v6, v7, v2}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2680899
    invoke-interface {v4, v6}, LX/5pD;->a(LX/5pH;)V

    .line 2680900
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    .line 2680901
    :cond_2
    move-object v0, v4

    .line 2680902
    invoke-direct {p0, v1, v0}, LX/JLK;->a(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2680880
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 2680881
    :goto_0
    return-void

    .line 2680882
    :cond_0
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 2680883
    :pswitch_0
    invoke-direct {p0, p3}, LX/JLK;->a(Landroid/content/Intent;)V

    goto :goto_0

    .line 2680884
    :pswitch_1
    invoke-direct {p0, p3}, LX/JLK;->b(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2711
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2680879
    const-string v0, "GoodwillVideoNativeModule"

    return-object v0
.end method

.method public openNativePhotoPicker(IZ)V
    .locals 4
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2680869
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2680870
    invoke-virtual {v0}, LX/5pX;->i()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2680871
    :goto_0
    return-void

    .line 2680872
    :cond_0
    new-instance v0, LX/8AA;

    sget-object v1, LX/8AB;->GOODWILL_COMPOSER:LX/8AB;

    invoke-direct {v0, v1}, LX/8AA;-><init>(LX/8AB;)V

    invoke-virtual {v0}, LX/8AA;->j()LX/8AA;

    move-result-object v0

    invoke-virtual {v0}, LX/8AA;->l()LX/8AA;

    move-result-object v0

    sget-object v1, LX/8A9;->NONE:LX/8A9;

    invoke-virtual {v0, v1}, LX/8AA;->a(LX/8A9;)LX/8AA;

    move-result-object v0

    .line 2680873
    if-nez p2, :cond_1

    .line 2680874
    invoke-virtual {v0}, LX/8AA;->i()LX/8AA;

    .line 2680875
    :cond_1
    iget-object v1, p0, LX/5pb;->a:LX/5pY;

    move-object v1, v1

    .line 2680876
    invoke-static {v1, v0}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->a(Landroid/content/Context;LX/8AA;)Landroid/content/Intent;

    move-result-object v0

    .line 2680877
    iget-object v1, p0, LX/5pb;->a:LX/5pY;

    move-object v1, v1

    .line 2680878
    const/16 v2, 0x2712

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, LX/5pX;->a(Landroid/content/Intent;ILandroid/os/Bundle;)Z

    goto :goto_0
.end method

.method public openShareComposer(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/5pG;LX/5pC;)V
    .locals 12
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2680865
    invoke-virtual {p0}, LX/5pb;->s()LX/5pY;

    move-result-object v1

    invoke-virtual {v1}, LX/5pX;->i()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2680866
    :goto_0
    return-void

    .line 2680867
    :cond_0
    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v7, p6

    invoke-direct/range {v1 .. v7}, LX/JLK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/5pC;Ljava/lang/String;)V

    .line 2680868
    const-string v1, "uri"

    move-object/from16 v0, p10

    invoke-interface {v0, v1}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/4 v9, 0x1

    const/4 v11, 0x0

    move-object v1, p0

    move-object v2, p3

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    move-object/from16 v5, p9

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v10, p11

    invoke-direct/range {v1 .. v11}, LX/JLK;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLX/5pC;Ljava/lang/String;)V

    goto :goto_0
.end method
