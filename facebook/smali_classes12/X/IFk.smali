.class public final enum LX/IFk;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IFk;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IFk;

.field public static final enum COMPLETE:LX/IFk;

.field public static final enum ERROR:LX/IFk;

.field public static final enum HAS_MORE:LX/IFk;

.field public static final enum INITIAL:LX/IFk;

.field public static final enum LOADING:LX/IFk;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2554795
    new-instance v0, LX/IFk;

    const-string v1, "INITIAL"

    invoke-direct {v0, v1, v2}, LX/IFk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IFk;->INITIAL:LX/IFk;

    .line 2554796
    new-instance v0, LX/IFk;

    const-string v1, "HAS_MORE"

    invoke-direct {v0, v1, v3}, LX/IFk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IFk;->HAS_MORE:LX/IFk;

    .line 2554797
    new-instance v0, LX/IFk;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v4}, LX/IFk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IFk;->LOADING:LX/IFk;

    .line 2554798
    new-instance v0, LX/IFk;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v5}, LX/IFk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IFk;->ERROR:LX/IFk;

    .line 2554799
    new-instance v0, LX/IFk;

    const-string v1, "COMPLETE"

    invoke-direct {v0, v1, v6}, LX/IFk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IFk;->COMPLETE:LX/IFk;

    .line 2554800
    const/4 v0, 0x5

    new-array v0, v0, [LX/IFk;

    sget-object v1, LX/IFk;->INITIAL:LX/IFk;

    aput-object v1, v0, v2

    sget-object v1, LX/IFk;->HAS_MORE:LX/IFk;

    aput-object v1, v0, v3

    sget-object v1, LX/IFk;->LOADING:LX/IFk;

    aput-object v1, v0, v4

    sget-object v1, LX/IFk;->ERROR:LX/IFk;

    aput-object v1, v0, v5

    sget-object v1, LX/IFk;->COMPLETE:LX/IFk;

    aput-object v1, v0, v6

    sput-object v0, LX/IFk;->$VALUES:[LX/IFk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2554801
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IFk;
    .locals 1

    .prologue
    .line 2554802
    const-class v0, LX/IFk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IFk;

    return-object v0
.end method

.method public static values()[LX/IFk;
    .locals 1

    .prologue
    .line 2554803
    sget-object v0, LX/IFk;->$VALUES:[LX/IFk;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IFk;

    return-object v0
.end method
