.class public LX/JOY;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JOY",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2687836
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2687837
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JOY;->b:LX/0Zi;

    .line 2687838
    iput-object p1, p0, LX/JOY;->a:LX/0Ot;

    .line 2687839
    return-void
.end method

.method public static a(LX/0QB;)LX/JOY;
    .locals 4

    .prologue
    .line 2687765
    const-class v1, LX/JOY;

    monitor-enter v1

    .line 2687766
    :try_start_0
    sget-object v0, LX/JOY;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2687767
    sput-object v2, LX/JOY;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2687768
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2687769
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2687770
    new-instance v3, LX/JOY;

    const/16 p0, 0x1fc6

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JOY;-><init>(LX/0Ot;)V

    .line 2687771
    move-object v0, v3

    .line 2687772
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2687773
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JOY;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2687774
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2687775
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Landroid/view/View;LX/1X1;)V
    .locals 12

    .prologue
    .line 2687808
    check-cast p2, LX/JOW;

    .line 2687809
    iget-object v0, p0, LX/JOY;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;

    iget-object v1, p2, LX/JOW;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/JOW;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    const/4 v5, 0x0

    const/4 v10, 0x0

    .line 2687810
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2687811
    check-cast v3, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->s()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v7

    .line 2687812
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->k()Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLAYMTChannel;->k()LX/0Px;

    move-result-object v3

    invoke-virtual {v3, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLAYMTTip;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLAYMTTip;->p()Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;

    move-result-object v4

    .line 2687813
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->k()Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLAYMTChannel;->k()LX/0Px;

    move-result-object v3

    invoke-virtual {v3, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLAYMTTip;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLAYMTTip;->k()Ljava/lang/String;

    move-result-object v3

    .line 2687814
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    if-nez v5, :cond_2

    move-object v8, v10

    .line 2687815
    :goto_0
    sget-object v5, LX/JOb;->a:[I

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLAYMTNativeAction;->ordinal()I

    move-result v4

    aget v4, v5, v4

    packed-switch v4, :pswitch_data_0

    .line 2687816
    if-eqz v3, :cond_3

    .line 2687817
    iget-object v4, v0, Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;->e:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-interface {v4, v5, v3}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2687818
    :cond_0
    :goto_1
    iget-object v3, v0, Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;->k:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JPN;

    invoke-virtual {v3, v1, v2}, LX/JPN;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;)V

    .line 2687819
    const/4 v6, 0x0

    .line 2687820
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->k()Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLAYMTChannel;->j()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->k()Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLAYMTChannel;->k()LX/0Px;

    move-result-object v3

    invoke-virtual {v3, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLAYMTTip;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLAYMTTip;->m()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_4

    .line 2687821
    :cond_1
    iget-object v3, v0, Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;->k:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JPN;

    const-string v4, "Cannot log hpp aymt click without ID"

    invoke-virtual {v3, v1, v2, v4}, LX/JPN;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;Ljava/lang/String;)V

    .line 2687822
    :goto_2
    return-void

    .line 2687823
    :cond_2
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v8

    goto :goto_0

    .line 2687824
    :pswitch_0
    iget-object v3, v0, Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;->i:LX/0SI;

    invoke-interface {v3}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v9

    .line 2687825
    iget-object v11, v0, Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;->g:LX/1Kf;

    iget-object v4, v0, Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;->h:LX/CSL;

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v4 .. v9}, LX/CSL;->a(JLjava/lang/String;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v4

    const/16 v5, 0x6dc

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const-class v6, Landroid/app/Activity;

    invoke-static {v3, v6}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/Activity;

    invoke-interface {v11, v10, v4, v5, v3}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    goto :goto_1

    .line 2687826
    :pswitch_1
    iget-object v4, v0, Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;->f:LX/17Y;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const-class v5, Landroid/app/Activity;

    invoke-static {v3, v5}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    sget-object v5, LX/0ax;->bb:Ljava/lang/String;

    invoke-interface {v4, v3, v5}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    .line 2687827
    if-eqz v4, :cond_0

    .line 2687828
    iget-object v5, v0, Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;->c:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const-class v6, Landroid/app/Activity;

    invoke-static {v3, v6}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-interface {v5, v4, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto/16 :goto_1

    .line 2687829
    :cond_3
    iget-object v3, v0, Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;->j:LX/1nA;

    invoke-static {v7}, LX/3Bd;->a(Lcom/facebook/graphql/model/GraphQLPage;)LX/1y5;

    move-result-object v4

    invoke-virtual {v3, p1, v4, v10}, LX/1nA;->a(Landroid/view/View;LX/1y5;Landroid/os/Bundle;)V

    goto/16 :goto_1

    .line 2687830
    :cond_4
    invoke-static {}, LX/81d;->a()LX/81c;

    move-result-object v4

    .line 2687831
    new-instance v3, LX/4D5;

    invoke-direct {v3}, LX/4D5;-><init>()V

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->k()Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLAYMTChannel;->j()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, LX/4D5;->a(Ljava/lang/String;)LX/4D5;

    move-result-object v3

    const-string v5, "CLICK"

    invoke-virtual {v3, v5}, LX/4D5;->b(Ljava/lang/String;)LX/4D5;

    move-result-object v5

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->k()Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLAYMTChannel;->k()LX/0Px;

    move-result-object v3

    invoke-virtual {v3, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLAYMTTip;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLAYMTTip;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, LX/4D5;->c(Ljava/lang/String;)LX/4D5;

    move-result-object v3

    const-string v5, "primary"

    invoke-virtual {v3, v5}, LX/4D5;->d(Ljava/lang/String;)LX/4D5;

    move-result-object v3

    .line 2687832
    const-string v5, "input"

    invoke-virtual {v4, v5, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2687833
    invoke-static {v4}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v3

    .line 2687834
    iget-object v4, v0, Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;->d:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 2687835
    new-instance v4, LX/JOa;

    invoke-direct {v4, v0, v1, v2}, LX/JOa;-><init>(Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;)V

    invoke-static {v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 10

    .prologue
    .line 2687782
    check-cast p2, LX/JOW;

    .line 2687783
    iget-object v0, p0, LX/JOY;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;

    iget-object v1, p2, LX/JOW;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/JOW;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    const/4 v7, 0x0

    .line 2687784
    const/4 v5, 0x0

    .line 2687785
    const-string v4, ""

    .line 2687786
    const-string v3, ""

    .line 2687787
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->k()Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLAYMTChannel;->k()LX/0Px;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->k()Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLAYMTChannel;->k()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_2

    .line 2687788
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->k()Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLAYMTChannel;->k()LX/0Px;

    move-result-object v3

    invoke-virtual {v3, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLAYMTTip;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLAYMTTip;->l()Ljava/lang/String;

    move-result-object v4

    .line 2687789
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->k()Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLAYMTChannel;->k()LX/0Px;

    move-result-object v3

    invoke-virtual {v3, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLAYMTTip;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLAYMTTip;->o()Ljava/lang/String;

    move-result-object v5

    .line 2687790
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->k()Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLAYMTChannel;->k()LX/0Px;

    move-result-object v3

    invoke-virtual {v3, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLAYMTTip;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLAYMTTip;->j()Ljava/lang/String;

    move-result-object v3

    move-object v8, v3

    move-object v3, v4

    move-object v4, v8

    .line 2687791
    :goto_0
    if-eqz v5, :cond_1

    .line 2687792
    const/4 p2, 0x2

    .line 2687793
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    const/4 v8, 0x0

    invoke-interface {v6, v8}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, p2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v6

    const v8, 0x7f0b258c

    invoke-interface {v6, v8}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v6

    iget-object v8, v0, Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;->a:LX/1nu;

    invoke-virtual {v8, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v8

    sget-object v9, Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;->l:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v8, v9}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v8

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v8, v9}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v8

    invoke-virtual {v8}, LX/1X5;->c()LX/1Di;

    move-result-object v8

    const v9, 0x7f0b257f

    invoke-interface {v8, v9}, LX/1Di;->i(I)LX/1Di;

    move-result-object v8

    const/high16 v9, 0x40800000    # 4.0f

    invoke-interface {v8, v9}, LX/1Di;->a(F)LX/1Di;

    move-result-object v8

    const/4 v9, 0x7

    const p0, 0x7f0b257b

    invoke-interface {v8, v9, p0}, LX/1Di;->g(II)LX/1Di;

    move-result-object v8

    invoke-interface {v6, v8}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v8

    invoke-virtual {v8, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v8

    const v9, 0x7f0a010c

    invoke-virtual {v8, v9}, LX/1ne;->n(I)LX/1ne;

    move-result-object v8

    const v9, 0x7f0b2582

    invoke-virtual {v8, v9}, LX/1ne;->q(I)LX/1ne;

    move-result-object v8

    invoke-virtual {v8, p2}, LX/1ne;->j(I)LX/1ne;

    move-result-object v8

    sget-object v9, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v8, v9}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v8

    invoke-virtual {v8}, LX/1X5;->c()LX/1Di;

    move-result-object v8

    const/high16 v9, 0x3f800000    # 1.0f

    invoke-interface {v8, v9}, LX/1Di;->a(F)LX/1Di;

    move-result-object v8

    const/16 v9, 0x8

    const p0, 0x7f0b2581

    invoke-interface {v8, v9, p0}, LX/1Di;->g(II)LX/1Di;

    move-result-object v8

    invoke-interface {v6, v8}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    move-object v3, v6

    .line 2687794
    :goto_1
    const/4 v9, 0x0

    .line 2687795
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->k()Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLAYMTChannel;->j()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->k()Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLAYMTChannel;->k()LX/0Px;

    move-result-object v5

    invoke-virtual {v5, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLAYMTTip;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLAYMTTip;->m()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_3

    .line 2687796
    :cond_0
    iget-object v5, v0, Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;->k:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/JPN;

    const-string v6, "Cannot log hpp aymt impression without ID"

    invoke-virtual {v5, v1, v2, v6}, LX/JPN;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;Ljava/lang/String;)V

    .line 2687797
    :goto_2
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v7}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x2

    invoke-interface {v5, v6}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v5

    const v6, 0x7f0b257f

    invoke-interface {v5, v6}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v5

    const v6, 0x7f0b2580

    invoke-interface {v5, v6}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v5

    const v6, 0x7f020a3d

    invoke-interface {v5, v6}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/JPK;->a(LX/1De;)LX/1Di;

    move-result-object v5

    invoke-interface {v3, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    const v5, 0x7f0a010e

    invoke-virtual {v4, v5}, LX/1ne;->n(I)LX/1ne;

    move-result-object v4

    const v5, 0x7f0b2582

    invoke-virtual {v4, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const/4 v5, 0x7

    const v6, 0x7f0b2571

    invoke-interface {v4, v5, v6}, LX/1Di;->g(II)LX/1Di;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x3

    const v5, 0x7f0b2581

    invoke-interface {v3, v4, v5}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v3

    .line 2687798
    const v4, 0x1485d8ce

    const/4 v5, 0x0

    invoke-static {p1, v4, v5}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v4, v4

    .line 2687799
    invoke-interface {v3, v4}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2687800
    return-object v0

    .line 2687801
    :cond_1
    invoke-static {v0, p1, v3}, Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;->a(Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;LX/1De;Ljava/lang/String;)LX/1Dh;

    move-result-object v3

    goto/16 :goto_1

    :cond_2
    move-object v8, v3

    move-object v3, v4

    move-object v4, v8

    goto/16 :goto_0

    .line 2687802
    :cond_3
    invoke-static {}, LX/81d;->a()LX/81c;

    move-result-object v6

    .line 2687803
    new-instance v5, LX/4D5;

    invoke-direct {v5}, LX/4D5;-><init>()V

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->k()Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLAYMTChannel;->j()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, LX/4D5;->a(Ljava/lang/String;)LX/4D5;

    move-result-object v5

    const-string v8, "IMPRESSION"

    invoke-virtual {v5, v8}, LX/4D5;->b(Ljava/lang/String;)LX/4D5;

    move-result-object v8

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->k()Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLAYMTChannel;->k()LX/0Px;

    move-result-object v5

    invoke-virtual {v5, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLAYMTTip;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLAYMTTip;->m()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v8, v5}, LX/4D5;->c(Ljava/lang/String;)LX/4D5;

    move-result-object v5

    .line 2687804
    const-string v8, "input"

    invoke-virtual {v6, v8, v5}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2687805
    invoke-static {v6}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v5

    .line 2687806
    iget-object v6, v0, Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;->d:LX/0tX;

    invoke-virtual {v6, v5}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    .line 2687807
    new-instance v6, LX/JOZ;

    invoke-direct {v6, v0, v1, v2}, LX/JOZ;-><init>(Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;)V

    invoke-static {v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto/16 :goto_2
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2687776
    invoke-static {}, LX/1dS;->b()V

    .line 2687777
    iget v0, p1, LX/1dQ;->b:I

    .line 2687778
    packed-switch v0, :pswitch_data_0

    .line 2687779
    :goto_0
    return-object v2

    .line 2687780
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2687781
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0, v1}, LX/JOY;->a(Landroid/view/View;LX/1X1;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1485d8ce
        :pswitch_0
    .end packed-switch
.end method
