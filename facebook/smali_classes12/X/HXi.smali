.class public LX/HXi;
.super LX/D2z;
.source ""


# static fields
.field public static final g:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final h:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final i:LX/0Uh;

.field public final j:LX/0tX;

.field public final k:LX/9XE;

.field public final l:LX/0Sh;

.field public m:Landroid/support/v4/app/Fragment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2479278
    const-class v0, LX/HXi;

    sput-object v0, LX/HXi;->g:Ljava/lang/Class;

    .line 2479279
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->DONT_LIKE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE_FROM_TIMELINE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    invoke-static {v0, v1}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/HXi;->h:LX/0Rf;

    return-void
.end method

.method public constructor <init>(LX/5SB;LX/0Or;LX/0Or;LX/1Kf;LX/1Ck;LX/0Ot;LX/0Or;LX/0Or;LX/0Or;LX/0Zb;LX/17Q;LX/BPq;LX/0Or;LX/0lC;LX/1Sa;LX/1Sj;LX/0Ot;LX/16H;LX/0Or;LX/0SG;LX/0bH;LX/0Sh;LX/0Or;LX/0Or;LX/0Or;LX/14w;LX/0ad;LX/0Or;LX/0Ot;LX/0Ot;LX/1Qj;Ljava/lang/Boolean;LX/0qn;LX/1e4;LX/0W3;LX/0Ot;LX/1Sl;LX/0Uh;LX/0tX;LX/1Sm;LX/0Ot;LX/0Ot;LX/0Ot;LX/9XE;LX/0Or;LX/0Ot;LX/0wL;)V
    .locals 47
    .param p1    # LX/5SB;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsNativeNewsfeedSpamReportingEnabled;
        .end annotation
    .end param
    .param p8    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsNativeNewsFeedPrivacyEditingEnabled;
        .end annotation
    .end param
    .param p13    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
        .end annotation
    .end param
    .param p24    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsNotifyMeSubscriptionEnabled;
        .end annotation
    .end param
    .param p31    # LX/1Qj;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p32    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5SB;",
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;",
            "LX/1Kf;",
            "LX/1Ck;",
            "LX/0Ot",
            "<",
            "LX/9LP;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "LX/1dv;",
            ">;",
            "LX/0Zb;",
            "LX/17Q;",
            "LX/BPq;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0lC;",
            "LX/1Sa;",
            "LX/1Sj;",
            "LX/0Ot",
            "<",
            "LX/79m;",
            ">;",
            "LX/16H;",
            "LX/0Or",
            "<",
            "LX/0kL;",
            ">;",
            "LX/0SG;",
            "LX/0bH;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0Or",
            "<",
            "LX/8Q3;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "LX/BNQ;",
            ">;",
            "LX/14w;",
            "LX/0ad;",
            "LX/0Or",
            "<",
            "LX/3Af;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1e0;",
            ">;",
            "LX/1Qj;",
            "Ljava/lang/Boolean;",
            "LX/0qn;",
            "Lcom/facebook/privacy/ui/PrivacyScopeResourceResolver;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "LX/0Ot",
            "<",
            "LX/9yI;",
            ">;",
            "LX/1Sl;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0tX;",
            "LX/1Sm;",
            "LX/0Ot",
            "<",
            "LX/BUA;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0tQ;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/downloadmanager/db/OfflineVideoCache;",
            ">;",
            "LX/9XE;",
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;",
            "LX/0wL;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2479271
    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    move-object/from16 v12, p10

    move-object/from16 v13, p11

    move-object/from16 v14, p12

    move-object/from16 v15, p13

    move-object/from16 v16, p14

    move-object/from16 v17, p15

    move-object/from16 v18, p16

    move-object/from16 v19, p17

    move-object/from16 v20, p18

    move-object/from16 v21, p19

    move-object/from16 v22, p20

    move-object/from16 v23, p21

    move-object/from16 v24, p22

    move-object/from16 v25, p23

    move-object/from16 v26, p24

    move-object/from16 v27, p25

    move-object/from16 v28, p26

    move-object/from16 v29, p27

    move-object/from16 v30, p28

    move-object/from16 v31, p30

    move-object/from16 v32, p31

    move-object/from16 v33, p32

    move-object/from16 v34, p33

    move-object/from16 v35, p34

    move-object/from16 v36, p35

    move-object/from16 v37, p36

    move-object/from16 v38, p37

    move-object/from16 v39, p39

    move-object/from16 v40, p40

    move-object/from16 v41, p41

    move-object/from16 v42, p42

    move-object/from16 v43, p43

    move-object/from16 v44, p45

    move-object/from16 v45, p46

    move-object/from16 v46, p47

    invoke-direct/range {v2 .. v46}, LX/D2z;-><init>(LX/5SB;LX/0Or;LX/0Or;LX/1Kf;LX/1Ck;LX/0Ot;LX/0Or;LX/0Or;LX/0Or;LX/0Zb;LX/17Q;LX/BPq;LX/0Or;LX/0lC;LX/1Sa;LX/1Sj;LX/0Ot;LX/16H;LX/0Or;LX/0SG;LX/0bH;LX/0Sh;LX/0Or;LX/0Or;LX/0Or;LX/14w;LX/0ad;LX/0Or;LX/0Ot;LX/1Pf;Ljava/lang/Boolean;LX/0qn;LX/1e4;LX/0W3;LX/0Ot;LX/1Sl;LX/0tX;LX/1Sm;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/0Ot;LX/0wL;)V

    .line 2479272
    move-object/from16 v0, p22

    move-object/from16 v1, p0

    iput-object v0, v1, LX/HXi;->l:LX/0Sh;

    .line 2479273
    move-object/from16 v0, p29

    move-object/from16 v1, p0

    iput-object v0, v1, LX/HXi;->o:LX/0Ot;

    .line 2479274
    move-object/from16 v0, p38

    move-object/from16 v1, p0

    iput-object v0, v1, LX/HXi;->i:LX/0Uh;

    .line 2479275
    move-object/from16 v0, p39

    move-object/from16 v1, p0

    iput-object v0, v1, LX/HXi;->j:LX/0tX;

    .line 2479276
    move-object/from16 v0, p44

    move-object/from16 v1, p0

    iput-object v0, v1, LX/HXi;->k:LX/9XE;

    .line 2479277
    return-void
.end method

.method public static e(LX/HXi;Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2479261
    if-nez p1, :cond_0

    move v0, v1

    .line 2479262
    :goto_0
    return v0

    .line 2479263
    :cond_0
    iget-object v0, p0, LX/D2z;->a:LX/5SB;

    invoke-virtual {v0}, LX/5SB;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p1}, LX/D2z;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    if-nez v0, :cond_2

    :cond_1
    move v0, v1

    .line 2479264
    goto :goto_0

    .line 2479265
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 2479266
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v4, p0, LX/D2z;->a:LX/5SB;

    .line 2479267
    iget-wide v6, v4, LX/5SB;->b:J

    move-wide v4, v6

    .line 2479268
    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v0, p0, LX/D2z;->a:LX/5SB;

    .line 2479269
    iget-wide v6, v0, LX/5SB;->a:J

    move-wide v4, v6

    .line 2479270
    cmp-long v0, v2, v4

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 2479257
    iget-object v0, p0, LX/HXi;->m:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_0

    .line 2479258
    iget-object v0, p0, LX/HXi;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Kf;

    const/4 v1, 0x0

    invoke-virtual {p0, p1}, LX/1SX;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    const/16 v3, 0x6de

    iget-object v4, p0, LX/HXi;->m:Landroid/support/v4/app/Fragment;

    invoke-interface {v0, v1, v2, v3, v4}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/support/v4/app/Fragment;)V

    .line 2479259
    :goto_0
    return-void

    .line 2479260
    :cond_0
    invoke-super {p0, p1, p2}, LX/D2z;->a(Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;)Z
    .locals 1

    .prologue
    .line 2479280
    sget-object v0, LX/HXi;->h:LX/0Rf;

    invoke-virtual {v0, p1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2479255
    iget-object v1, p0, LX/D2z;->a:LX/5SB;

    invoke-virtual {v1}, LX/5SB;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2479256
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->D_()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->D_()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v2, 0x4c808d5

    if-ne v1, v2, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final d(Lcom/facebook/graphql/model/FeedUnit;)LX/1wH;
    .locals 2

    .prologue
    .line 2479250
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    .line 2479251
    new-instance v0, LX/HXh;

    invoke-direct {v0, p0}, LX/HXh;-><init>(LX/HXi;)V

    .line 2479252
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()LX/0wD;
    .locals 1

    .prologue
    .line 2479254
    sget-object v0, LX/0wD;->PAGE_TIMELINE:LX/0wD;

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2479253
    sget-object v0, LX/04D;->PAGE_TIMELINE_CHEVRON:LX/04D;

    invoke-virtual {v0}, LX/04D;->asString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
