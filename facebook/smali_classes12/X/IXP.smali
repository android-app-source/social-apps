.class public final LX/IXP;
.super Landroid/webkit/WebViewClient;
.source ""


# instance fields
.field public final synthetic a:LX/IXQ;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/IXQ;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2585962
    iput-object p1, p0, LX/IXP;->a:LX/IXQ;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    .line 2585963
    iput-object p2, p0, LX/IXP;->b:Ljava/lang/String;

    .line 2585964
    iput-object p3, p0, LX/IXP;->c:Ljava/lang/String;

    .line 2585965
    return-void
.end method


# virtual methods
.method public final onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2585973
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 2585974
    iget-object v0, p0, LX/IXP;->a:LX/IXQ;

    iget-object v1, p0, LX/IXP;->c:Ljava/lang/String;

    iget-object v2, p0, LX/IXP;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2}, LX/IXQ;->e(LX/IXQ;Ljava/lang/String;Ljava/lang/String;)V

    .line 2585975
    return-void
.end method

.method public final onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2585970
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 2585971
    iget-object v0, p0, LX/IXP;->a:LX/IXQ;

    iget-object v1, p0, LX/IXP;->c:Ljava/lang/String;

    iget-object v2, p0, LX/IXP;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2}, LX/IXQ;->f(LX/IXQ;Ljava/lang/String;Ljava/lang/String;)V

    .line 2585972
    return-void
.end method

.method public final onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V
    .locals 3

    .prologue
    .line 2585967
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V

    .line 2585968
    iget-object v0, p0, LX/IXP;->a:LX/IXQ;

    iget-object v1, p0, LX/IXP;->c:Ljava/lang/String;

    iget-object v2, p0, LX/IXP;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2}, LX/IXQ;->f(LX/IXQ;Ljava/lang/String;Ljava/lang/String;)V

    .line 2585969
    return-void
.end method

.method public final shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2585966
    const/4 v0, 0x0

    return v0
.end method
