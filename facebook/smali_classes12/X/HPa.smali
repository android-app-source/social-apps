.class public LX/HPa;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:J


# direct methods
.method public constructor <init>(J)V
    .locals 1

    .prologue
    .line 2462233
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2462234
    iput-wide p1, p0, LX/HPa;->a:J

    .line 2462235
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2462231
    instance-of v1, p1, LX/HPa;

    if-nez v1, :cond_1

    .line 2462232
    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, LX/HPa;

    iget-wide v2, p1, LX/HPa;->a:J

    iget-wide v4, p0, LX/HPa;->a:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 2462230
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-wide v2, p0, LX/HPa;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
