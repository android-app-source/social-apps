.class public final LX/IJL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;)V
    .locals 0

    .prologue
    .line 2563475
    iput-object p1, p0, LX/IJL;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/6ax;)Landroid/view/View;
    .locals 6

    .prologue
    .line 2563457
    invoke-virtual {p1}, LX/6ax;->b()Ljava/lang/String;

    move-result-object v0

    .line 2563458
    iget-object v1, p0, LX/IJL;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->A:LX/IFX;

    invoke-virtual {v1, v0}, LX/IFX;->c(Ljava/lang/String;)Lcom/facebook/user/model/User;

    move-result-object v0

    .line 2563459
    iget-object v1, p0, LX/IJL;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->p:Landroid/widget/TextView;

    iget-object v2, p0, LX/IJL;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;

    const v3, 0x7f083863

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->j()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2563460
    iget-object v0, p0, LX/IJL;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->p:Landroid/widget/TextView;

    return-object v0
.end method

.method public final b(LX/6ax;)Landroid/view/View;
    .locals 8

    .prologue
    .line 2563461
    invoke-virtual {p1}, LX/6ax;->b()Ljava/lang/String;

    move-result-object v0

    .line 2563462
    iget-object v1, p0, LX/IJL;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->u:LX/IJV;

    sget-object v2, LX/IJV;->EXPANDED:LX/IJV;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, LX/IJL;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;

    const/4 v3, 0x0

    .line 2563463
    iget-object v2, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->g:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v5

    move v4, v3

    :goto_0
    if-ge v4, v5, :cond_2

    iget-object v2, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->g:LX/0Px;

    invoke-virtual {v2, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/IJZ;

    .line 2563464
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v6

    iget-object v7, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->A:LX/IFX;

    invoke-virtual {v7, v0}, LX/IFX;->e(Ljava/lang/String;)LX/IFb;

    move-result-object v7

    .line 2563465
    iget-object p1, v7, LX/IFb;->a:Lcom/facebook/location/ImmutableLocation;

    move-object v7, p1

    .line 2563466
    iget-object p1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->A:LX/IFX;

    invoke-virtual {p1, v0}, LX/IFX;->c(Ljava/lang/String;)Lcom/facebook/user/model/User;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/user/model/User;->j()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, v6, v7, p1}, LX/IJZ;->a(Landroid/content/Context;Lcom/facebook/location/ImmutableLocation;Ljava/lang/String;)LX/0am;

    move-result-object v2

    .line 2563467
    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2563468
    const/4 v2, 0x1

    .line 2563469
    :goto_1
    move v0, v2

    .line 2563470
    if-eqz v0, :cond_0

    .line 2563471
    const/4 v0, 0x0

    .line 2563472
    :goto_2
    return-object v0

    :cond_0
    iget-object v0, p0, LX/IJL;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->o:Landroid/view/View;

    goto :goto_2

    .line 2563473
    :cond_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_0

    :cond_2
    move v2, v3

    .line 2563474
    goto :goto_1
.end method
