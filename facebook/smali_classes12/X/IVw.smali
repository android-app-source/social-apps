.class public LX/IVw;
.super LX/ISH;
.source ""


# instance fields
.field private a:LX/ITj;

.field private b:LX/ISI;

.field private final c:LX/EQS;


# direct methods
.method public constructor <init>(LX/ITj;LX/ISI;)V
    .locals 2

    .prologue
    .line 2583221
    invoke-direct {p0}, LX/ISH;-><init>()V

    .line 2583222
    iput-object p1, p0, LX/IVw;->a:LX/ITj;

    .line 2583223
    iput-object p2, p0, LX/IVw;->b:LX/ISI;

    .line 2583224
    new-instance v0, LX/EQS;

    invoke-direct {v0, p0}, LX/EQS;-><init>(Landroid/widget/BaseAdapter;)V

    iput-object v0, p0, LX/IVw;->c:LX/EQS;

    .line 2583225
    iget-object v0, p0, LX/IVw;->b:LX/ISI;

    iget-object v1, p0, LX/IVw;->c:LX/EQS;

    invoke-virtual {v0, v1}, LX/ISI;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 2583226
    iget-object v0, p0, LX/IVw;->a:LX/ITj;

    iget-object v1, p0, LX/IVw;->c:LX/EQS;

    invoke-virtual {v0, v1}, LX/ITj;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 2583227
    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2583247
    iget-object v0, p0, LX/IVw;->a:LX/ITj;

    invoke-virtual {v0}, LX/ITj;->getViewTypeCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 2583248
    iget-object v0, p0, LX/IVw;->a:LX/ITj;

    invoke-virtual {v0, p1, p2}, LX/1Cv;->a(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 2583249
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/IVw;->b:LX/ISI;

    iget-object v1, p0, LX/IVw;->a:LX/ITj;

    invoke-virtual {v1}, LX/ITj;->getViewTypeCount()I

    move-result v1

    sub-int v1, p1, v1

    invoke-virtual {v0, v1, p2}, LX/1Cv;->a(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 2583250
    iget-object v0, p0, LX/IVw;->a:LX/ITj;

    invoke-virtual {v0}, LX/ISH;->a()V

    .line 2583251
    iget-object v0, p0, LX/IVw;->b:LX/ISI;

    invoke-virtual {v0}, LX/ISH;->a()V

    .line 2583252
    iget-object v0, p0, LX/IVw;->a:LX/ITj;

    if-eqz v0, :cond_0

    .line 2583253
    iget-object v0, p0, LX/IVw;->a:LX/ITj;

    iget-object v1, p0, LX/IVw;->c:LX/EQS;

    invoke-virtual {v0, v1}, LX/ITj;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 2583254
    :cond_0
    iget-object v0, p0, LX/IVw;->b:LX/ISI;

    if-eqz v0, :cond_1

    .line 2583255
    iget-object v0, p0, LX/IVw;->b:LX/ISI;

    iget-object v1, p0, LX/IVw;->c:LX/EQS;

    invoke-virtual {v0, v1}, LX/ISI;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 2583256
    :cond_1
    return-void
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 6

    .prologue
    .line 2583257
    iget-object v0, p0, LX/IVw;->a:LX/ITj;

    invoke-virtual {v0}, LX/ITj;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 2583258
    iget-object v0, p0, LX/IVw;->a:LX/ITj;

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, LX/1Cv;->a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V

    .line 2583259
    :goto_0
    return-void

    .line 2583260
    :cond_0
    iget-object v0, p0, LX/IVw;->b:LX/ISI;

    iget-object v1, p0, LX/IVw;->a:LX/ITj;

    invoke-virtual {v1}, LX/ITj;->getCount()I

    move-result v1

    sub-int v1, p1, v1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, LX/1Cv;->a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;ZZZZ)V
    .locals 7

    .prologue
    .line 2583261
    new-instance v6, LX/IVp;

    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    invoke-direct {v6, v0}, LX/IVp;-><init>(LX/0Pz;)V

    .line 2583262
    iget-object v0, p0, LX/IVw;->a:LX/ITj;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v6}, LX/ITj;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;ZZZZLX/IVp;)V

    .line 2583263
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 2583264
    iput-object v0, v6, LX/IVp;->b:LX/0Pz;

    .line 2583265
    const/4 v1, 0x0

    iput v1, v6, LX/IVp;->d:I

    .line 2583266
    iget-object v1, p0, LX/IVw;->b:LX/ISI;

    move-object v2, p1

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v1 .. v6}, LX/ISI;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;ZZZLX/IVp;)V

    .line 2583267
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2583268
    iget-object v0, p0, LX/IVw;->a:LX/ITj;

    invoke-virtual {v0, p1}, LX/ISH;->a(Ljava/lang/String;)V

    .line 2583269
    iget-object v0, p0, LX/IVw;->b:LX/ISI;

    invoke-virtual {v0, p1}, LX/ISH;->a(Ljava/lang/String;)V

    .line 2583270
    return-void
.end method

.method public final areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 2583271
    iget-object v0, p0, LX/IVw;->a:LX/ITj;

    invoke-virtual {v0}, LX/ITj;->areAllItemsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/IVw;->b:LX/ISI;

    invoke-virtual {v0}, LX/ISI;->areAllItemsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getCount()I
    .locals 2

    .prologue
    .line 2583243
    iget-object v0, p0, LX/IVw;->a:LX/ITj;

    invoke-virtual {v0}, LX/ITj;->getCount()I

    move-result v0

    iget-object v1, p0, LX/IVw;->b:LX/ISI;

    invoke-virtual {v1}, LX/ISI;->getCount()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2583244
    iget-object v0, p0, LX/IVw;->a:LX/ITj;

    invoke-virtual {v0}, LX/ITj;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 2583245
    iget-object v0, p0, LX/IVw;->a:LX/ITj;

    invoke-virtual {v0, p1}, LX/ITj;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 2583246
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/IVw;->b:LX/ISI;

    iget-object v1, p0, LX/IVw;->a:LX/ITj;

    invoke-virtual {v1}, LX/ITj;->getCount()I

    move-result v1

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, LX/ISI;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2583240
    iget-object v0, p0, LX/IVw;->a:LX/ITj;

    invoke-virtual {v0}, LX/ITj;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 2583241
    iget-object v0, p0, LX/IVw;->a:LX/ITj;

    invoke-virtual {v0, p1}, LX/ITj;->getItemId(I)J

    move-result-wide v0

    .line 2583242
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, LX/IVw;->b:LX/ISI;

    iget-object v1, p0, LX/IVw;->a:LX/ITj;

    invoke-virtual {v1}, LX/ITj;->getCount()I

    move-result v1

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, LX/ISI;->getItemId(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 3

    .prologue
    .line 2583237
    iget-object v0, p0, LX/IVw;->a:LX/ITj;

    invoke-virtual {v0}, LX/ITj;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 2583238
    iget-object v0, p0, LX/IVw;->a:LX/ITj;

    invoke-virtual {v0, p1}, LX/ITj;->getItemViewType(I)I

    move-result v0

    .line 2583239
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/IVw;->a:LX/ITj;

    invoke-virtual {v0}, LX/ITj;->getViewTypeCount()I

    move-result v0

    iget-object v1, p0, LX/IVw;->b:LX/ISI;

    iget-object v2, p0, LX/IVw;->a:LX/ITj;

    invoke-virtual {v2}, LX/ITj;->getCount()I

    move-result v2

    sub-int v2, p1, v2

    invoke-virtual {v1, v2}, LX/ISI;->getItemViewType(I)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p2    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2583234
    iget-object v0, p0, LX/IVw;->a:LX/ITj;

    invoke-virtual {v0}, LX/ITj;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 2583235
    iget-object v0, p0, LX/IVw;->a:LX/ITj;

    invoke-virtual {v0, p1, p2, p3}, LX/1Cv;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 2583236
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/IVw;->b:LX/ISI;

    iget-object v1, p0, LX/IVw;->a:LX/ITj;

    invoke-virtual {v1}, LX/ITj;->getCount()I

    move-result v1

    sub-int v1, p1, v1

    invoke-virtual {v0, v1, p2, p3}, LX/1Cv;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 2

    .prologue
    .line 2583233
    iget-object v0, p0, LX/IVw;->a:LX/ITj;

    invoke-virtual {v0}, LX/ITj;->getViewTypeCount()I

    move-result v0

    iget-object v1, p0, LX/IVw;->b:LX/ISI;

    invoke-virtual {v1}, LX/ISI;->getViewTypeCount()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final hasStableIds()Z
    .locals 1

    .prologue
    .line 2583232
    iget-object v0, p0, LX/IVw;->a:LX/ITj;

    invoke-virtual {v0}, LX/ITj;->hasStableIds()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/IVw;->b:LX/ISI;

    invoke-virtual {v0}, LX/ISI;->hasStableIds()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isEmpty()Z
    .locals 1

    .prologue
    .line 2583231
    iget-object v0, p0, LX/IVw;->a:LX/ITj;

    invoke-virtual {v0}, LX/ITj;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/IVw;->b:LX/ISI;

    invoke-virtual {v0}, LX/ISI;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isEnabled(I)Z
    .locals 2

    .prologue
    .line 2583228
    iget-object v0, p0, LX/IVw;->a:LX/ITj;

    invoke-virtual {v0}, LX/ITj;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 2583229
    iget-object v0, p0, LX/IVw;->a:LX/ITj;

    invoke-virtual {v0, p1}, LX/ITj;->isEnabled(I)Z

    move-result v0

    .line 2583230
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/IVw;->b:LX/ISI;

    iget-object v1, p0, LX/IVw;->a:LX/ITj;

    invoke-virtual {v1}, LX/ITj;->getCount()I

    move-result v1

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, LX/ISI;->isEnabled(I)Z

    move-result v0

    goto :goto_0
.end method
