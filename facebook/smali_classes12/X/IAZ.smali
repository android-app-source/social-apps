.class public final LX/IAZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Landroid/content/Context;

.field public final synthetic c:Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;


# direct methods
.method public constructor <init>(Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;Ljava/lang/String;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2545749
    iput-object p1, p0, LX/IAZ;->c:Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;

    iput-object p2, p0, LX/IAZ;->a:Ljava/lang/String;

    iput-object p3, p0, LX/IAZ;->b:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 7

    .prologue
    .line 2545750
    iget-object v0, p0, LX/IAZ;->c:Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;

    iget-object v0, v0, Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;->f:LX/IAW;

    iget-object v1, p0, LX/IAZ;->c:Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;

    iget-object v1, v1, Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;->h:Lcom/facebook/events/model/Event;

    .line 2545751
    iget-object v2, v1, Lcom/facebook/events/model/Event;->aj:Lcom/facebook/events/model/EventUser;

    move-object v1, v2

    .line 2545752
    iget-object v2, v1, Lcom/facebook/events/model/EventUser;->b:Ljava/lang/String;

    move-object v1, v2

    .line 2545753
    iget-object v2, p0, LX/IAZ;->a:Ljava/lang/String;

    iget-object v3, p0, LX/IAZ;->c:Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;

    iget-object v3, v3, Lcom/facebook/events/permalink/invitedbybar/EventInvitedByBarToxicleView;->i:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v4, p0, LX/IAZ;->b:Landroid/content/Context;

    .line 2545754
    new-instance v6, LX/IAT;

    invoke-direct {v6, v0, v4, v2}, LX/IAT;-><init>(LX/IAW;Landroid/content/Context;Ljava/lang/String;)V

    .line 2545755
    new-instance v5, LX/4Ef;

    invoke-direct {v5}, LX/4Ef;-><init>()V

    iget-object p0, v0, LX/IAW;->d:Ljava/lang/String;

    .line 2545756
    const-string p1, "actor_id"

    invoke-virtual {v5, p1, p0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2545757
    move-object v5, v5

    .line 2545758
    const-string p0, "target_user_id"

    invoke-virtual {v5, p0, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2545759
    move-object v5, v5

    .line 2545760
    const-string p0, "INVITE"

    .line 2545761
    const-string p1, "block_type"

    invoke-virtual {v5, p1, p0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2545762
    move-object v5, v5

    .line 2545763
    new-instance p1, LX/4EG;

    invoke-direct {p1}, LX/4EG;-><init>()V

    .line 2545764
    iget-object p0, v3, Lcom/facebook/events/common/EventAnalyticsParams;->d:Ljava/lang/String;

    invoke-virtual {p1, p0}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    .line 2545765
    sget-object p0, Lcom/facebook/events/common/ActionMechanism;->PERMALINK:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {p0}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, LX/4EG;->b(Ljava/lang/String;)LX/4EG;

    .line 2545766
    iget-object p0, v3, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    if-nez p0, :cond_0

    .line 2545767
    new-instance p0, LX/IAU;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1, p1}, LX/IAU;-><init>(LX/IAW;ILX/4EG;)V

    .line 2545768
    :goto_0
    new-instance p1, LX/4EL;

    invoke-direct {p1}, LX/4EL;-><init>()V

    invoke-virtual {p1, p0}, LX/4EL;->a(Ljava/util/List;)LX/4EL;

    move-result-object p0

    move-object p0, p0

    .line 2545769
    const-string p1, "context"

    invoke-virtual {v5, p1, p0}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 2545770
    move-object v5, v5

    .line 2545771
    new-instance p0, LX/7uN;

    invoke-direct {p0}, LX/7uN;-><init>()V

    move-object p0, p0

    .line 2545772
    const-string p1, "input"

    invoke-virtual {p0, p1, v5}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v5

    check-cast v5, LX/7uN;

    invoke-static {v5}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v5

    .line 2545773
    iget-object p0, v0, LX/IAW;->a:LX/0tX;

    invoke-virtual {p0, v5}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    .line 2545774
    iget-object p0, v0, LX/IAW;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v5, v6, p0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2545775
    const/4 v0, 0x1

    return v0

    .line 2545776
    :cond_0
    new-instance v1, LX/4EG;

    invoke-direct {v1}, LX/4EG;-><init>()V

    .line 2545777
    iget-object p0, v3, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    invoke-virtual {v1, p0}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    .line 2545778
    new-instance p0, LX/IAV;

    const/4 v2, 0x2

    invoke-direct {p0, v0, v2, v1, p1}, LX/IAV;-><init>(LX/IAW;ILX/4EG;LX/4EG;)V

    goto :goto_0
.end method
