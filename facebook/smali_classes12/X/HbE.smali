.class public LX/HbE;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/HbG;

.field public b:LX/HbG;

.field public c:LX/HbG;

.field public d:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public e:LX/1P1;

.field public f:Lcom/facebook/resources/ui/FbButton;

.field public g:Lcom/facebook/resources/ui/FbButton;

.field public h:Lcom/facebook/resources/ui/FbButton;

.field public i:Landroid/content/Context;

.field public j:Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;

.field public k:LX/Hba;

.field public l:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

.field public m:Landroid/widget/ImageView;

.field public n:Landroid/widget/ImageView;

.field public o:Landroid/widget/ImageView;

.field public p:Lcom/facebook/resources/ui/FbTextView;

.field public q:Lcom/facebook/securitycheckup/inner/InertCheckBox;

.field public r:Z

.field public s:Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;

.field public t:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation
.end field

.field public u:LX/Hbq;

.field public v:LX/Hai;

.field public w:Ljava/lang/Runnable;

.field public x:LX/HaZ;


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/content/Context;LX/Hba;Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;Ljava/lang/Runnable;LX/0Ot;LX/Hbq;LX/Hai;LX/HaZ;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/Hba;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Ljava/lang/Runnable;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Landroid/content/Context;",
            "LX/Hba;",
            "Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;",
            "Ljava/lang/Runnable;",
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;",
            "LX/Hbq;",
            "LX/Hai;",
            "LX/HaZ;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2485192
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2485193
    const v0, 0x7f0d2bd3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, LX/HbE;->d:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2485194
    const v0, 0x7f0d2bd7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, LX/HbE;->f:Lcom/facebook/resources/ui/FbButton;

    .line 2485195
    const v0, 0x7f0d2bd6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, LX/HbE;->g:Lcom/facebook/resources/ui/FbButton;

    .line 2485196
    const v0, 0x7f0d2bd8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, LX/HbE;->h:Lcom/facebook/resources/ui/FbButton;

    .line 2485197
    const v0, 0x7f0d2bce

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    iput-object v0, p0, LX/HbE;->l:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    .line 2485198
    const v0, 0x7f0d2bcd

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/HbE;->m:Landroid/widget/ImageView;

    .line 2485199
    const v0, 0x7f0d2bd2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/HbE;->n:Landroid/widget/ImageView;

    .line 2485200
    const v0, 0x7f0d2bcf

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/HbE;->p:Lcom/facebook/resources/ui/FbTextView;

    .line 2485201
    const v0, 0x7f0d2bd1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/securitycheckup/inner/InertCheckBox;

    iput-object v0, p0, LX/HbE;->q:Lcom/facebook/securitycheckup/inner/InertCheckBox;

    .line 2485202
    const v0, 0x7f0d2bd5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/HbE;->o:Landroid/widget/ImageView;

    .line 2485203
    iput-object p2, p0, LX/HbE;->i:Landroid/content/Context;

    .line 2485204
    iput-object p3, p0, LX/HbE;->k:LX/Hba;

    .line 2485205
    iput-object p4, p0, LX/HbE;->j:Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;

    .line 2485206
    iput-object p6, p0, LX/HbE;->t:LX/0Ot;

    .line 2485207
    iput-object p7, p0, LX/HbE;->u:LX/Hbq;

    .line 2485208
    iput-object p8, p0, LX/HbE;->v:LX/Hai;

    .line 2485209
    iput-object p5, p0, LX/HbE;->w:Ljava/lang/Runnable;

    .line 2485210
    iput-object p9, p0, LX/HbE;->x:LX/HaZ;

    .line 2485211
    return-void
.end method

.method private static a(LX/HbE;J)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v4, 0x1

    .line 2485212
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    .line 2485213
    sub-long/2addr v0, p1

    .line 2485214
    long-to-int v0, v0

    const v1, 0x278d00

    div-int/2addr v0, v1

    invoke-static {v4, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 2485215
    iget-object v1, p0, LX/HbE;->i:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f016c

    new-array v3, v4, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static h(LX/HbE;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/HbG;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, -0x1

    .line 2485216
    iget-object v0, p0, LX/HbE;->v:LX/Hai;

    .line 2485217
    iget-object v1, v0, LX/Hai;->a:Ljava/util/List;

    move-object v0, v1

    .line 2485218
    if-eqz v0, :cond_1

    .line 2485219
    iget-object v0, p0, LX/HbE;->v:LX/Hai;

    .line 2485220
    iget-object v1, v0, LX/Hai;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HbG;

    .line 2485221
    const/4 v3, 0x1

    .line 2485222
    iput-boolean v3, v1, LX/HbG;->e:Z

    .line 2485223
    goto :goto_0

    .line 2485224
    :cond_0
    iget-object v0, p0, LX/HbE;->v:LX/Hai;

    .line 2485225
    iget-object v1, v0, LX/Hai;->a:Ljava/util/List;

    move-object v0, v1

    .line 2485226
    :goto_1
    return-object v0

    .line 2485227
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2485228
    iget-object v0, p0, LX/HbE;->j:Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;

    invoke-virtual {v0}, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;->j()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    .line 2485229
    const/4 v0, 0x0

    move v4, v0

    :goto_2
    if-ge v4, v6, :cond_5

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupUnusedUserSessionsFragmentModel;

    .line 2485230
    invoke-virtual {v0}, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupUnusedUserSessionsFragmentModel;->a()Ljava/lang/String;

    move-result-object v1

    const-string v7, "desktop"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2485231
    const v1, 0x7f020836

    .line 2485232
    :goto_3
    if-eq v1, v3, :cond_4

    .line 2485233
    invoke-virtual {v0}, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupUnusedUserSessionsFragmentModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0}, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupUnusedUserSessionsFragmentModel;->j()I

    move-result v8

    int-to-long v8, v8

    invoke-static {p0, v8, v9}, LX/HbE;->a(LX/HbE;J)Ljava/lang/String;

    move-result-object v8

    invoke-static {v1, v7, v8}, LX/HbG;->a(ILjava/lang/String;Ljava/lang/String;)LX/HbG;

    move-result-object v1

    .line 2485234
    invoke-virtual {v0}, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupUnusedUserSessionsFragmentModel;->l()Ljava/lang/String;

    move-result-object v0

    .line 2485235
    iput-object v0, v1, LX/HbG;->f:Ljava/lang/String;

    .line 2485236
    move-object v0, v1

    .line 2485237
    :goto_4
    const/4 v1, 0x1

    .line 2485238
    iput-boolean v1, v0, LX/HbG;->e:Z

    .line 2485239
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2485240
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_2

    .line 2485241
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupUnusedUserSessionsFragmentModel;->a()Ljava/lang/String;

    move-result-object v1

    const-string v7, "app"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2485242
    const v1, 0x7f02074d

    goto :goto_3

    .line 2485243
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupUnusedUserSessionsFragmentModel;->a()Ljava/lang/String;

    move-result-object v1

    const-string v7, "tablet"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2485244
    const v1, 0x7f0209ef

    goto :goto_3

    .line 2485245
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupUnusedUserSessionsFragmentModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupUnusedUserSessionsFragmentModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0}, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupUnusedUserSessionsFragmentModel;->j()I

    move-result v8

    int-to-long v8, v8

    invoke-static {p0, v8, v9}, LX/HbE;->a(LX/HbE;J)Ljava/lang/String;

    move-result-object v8

    .line 2485246
    new-instance v9, LX/HbG;

    invoke-direct {v9, v7, v8}, LX/HbG;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2485247
    invoke-static {v1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v7

    iput-object v7, v9, LX/HbG;->b:LX/0am;

    .line 2485248
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v7

    iput-object v7, v9, LX/HbG;->a:LX/0am;

    .line 2485249
    move-object v1, v9

    .line 2485250
    invoke-virtual {v0}, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupUnusedUserSessionsFragmentModel;->l()Ljava/lang/String;

    move-result-object v0

    .line 2485251
    iput-object v0, v1, LX/HbG;->f:Ljava/lang/String;

    .line 2485252
    move-object v0, v1

    goto :goto_4

    .line 2485253
    :cond_5
    iget-object v0, p0, LX/HbE;->v:LX/Hai;

    .line 2485254
    iput-object v2, v0, LX/Hai;->a:Ljava/util/List;

    .line 2485255
    move-object v0, v2

    .line 2485256
    goto/16 :goto_1

    :cond_6
    move v1, v3

    goto/16 :goto_3
.end method
