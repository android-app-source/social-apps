.class public final LX/J4l;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/5mv;

.field public final synthetic c:LX/5mu;

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:LX/J4p;


# direct methods
.method public constructor <init>(LX/J4p;Ljava/lang/String;LX/5mv;LX/5mu;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2644469
    iput-object p1, p0, LX/J4l;->e:LX/J4p;

    iput-object p2, p0, LX/J4l;->a:Ljava/lang/String;

    iput-object p3, p0, LX/J4l;->b:LX/5mv;

    iput-object p4, p0, LX/J4l;->c:LX/5mu;

    iput-object p5, p0, LX/J4l;->d:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2644470
    iget-object v0, p0, LX/J4l;->e:LX/J4p;

    iget-object v0, v0, LX/J4p;->b:Lcom/facebook/privacy/PrivacyOperationsClient;

    iget-object v1, p0, LX/J4l;->a:Ljava/lang/String;

    iget-object v2, p0, LX/J4l;->b:LX/5mv;

    iget-object v3, p0, LX/J4l;->c:LX/5mu;

    iget-object v4, p0, LX/J4l;->e:LX/J4p;

    iget-object v4, v4, LX/J4p;->c:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    iget-object v6, p0, LX/J4l;->d:Ljava/lang/String;

    invoke-virtual/range {v0 .. v6}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(Ljava/lang/String;LX/5mv;LX/5mu;JLjava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
