.class public LX/JR8;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pm;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JRA;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JR8",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JRA;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2692830
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2692831
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JR8;->b:LX/0Zi;

    .line 2692832
    iput-object p1, p0, LX/JR8;->a:LX/0Ot;

    .line 2692833
    return-void
.end method

.method public static a(LX/0QB;)LX/JR8;
    .locals 4

    .prologue
    .line 2692819
    const-class v1, LX/JR8;

    monitor-enter v1

    .line 2692820
    :try_start_0
    sget-object v0, LX/JR8;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2692821
    sput-object v2, LX/JR8;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2692822
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2692823
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2692824
    new-instance v3, LX/JR8;

    const/16 p0, 0x201e

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JR8;-><init>(LX/0Ot;)V

    .line 2692825
    move-object v0, v3

    .line 2692826
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2692827
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JR8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2692828
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2692829
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2692817
    invoke-static {}, LX/1dS;->b()V

    .line 2692818
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2692814
    iget-object v0, p0, LX/JR8;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 2692815
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    move-object v0, v0

    .line 2692816
    return-object v0
.end method

.method public final e(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 11

    .prologue
    .line 2692796
    check-cast p3, LX/JR7;

    .line 2692797
    iget-object v0, p0, LX/JR8;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JRA;

    iget-object v1, p3, LX/JR7;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p3, LX/JR7;->b:LX/1Pm;

    .line 2692798
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2692799
    check-cast v3, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;

    .line 2692800
    new-instance v4, LX/JR2;

    invoke-direct {v4, v3}, LX/JR2;-><init>(Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;)V

    invoke-interface {v2, v4, v3}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JR3;

    .line 2692801
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    .line 2692802
    iget-wide v9, v3, LX/JR3;->b:J

    move-wide v7, v9

    .line 2692803
    sub-long/2addr v5, v7

    const-wide/32 v7, 0x2bf20

    cmp-long v4, v5, v7

    if-lez v4, :cond_0

    .line 2692804
    iget-object v4, v0, LX/JRA;->a:LX/JQo;

    new-instance v5, LX/JR9;

    invoke-direct {v5, v0, v3, v2, v1}, LX/JR9;-><init>(LX/JRA;LX/JR3;LX/1Pm;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2692805
    iget-object v3, v4, LX/JQo;->i:LX/JR9;

    if-eqz v3, :cond_1

    .line 2692806
    :cond_0
    :goto_0
    return-void

    .line 2692807
    :cond_1
    iput-object v5, v4, LX/JQo;->i:LX/JR9;

    .line 2692808
    iget-object v3, v4, LX/JQo;->e:LX/3Lb;

    iget-object v6, v4, LX/JQo;->f:LX/3Mb;

    .line 2692809
    iput-object v6, v3, LX/3Lb;->B:LX/3Mb;

    .line 2692810
    iget-object v3, v4, LX/JQo;->c:LX/2CH;

    invoke-virtual {v3, v4}, LX/2CH;->a(Ljava/lang/Object;)V

    .line 2692811
    iget-object v3, v4, LX/JQo;->c:LX/2CH;

    iget-object v6, v4, LX/JQo;->g:LX/3Mg;

    invoke-virtual {v3, v6}, LX/2CH;->a(LX/3Mg;)V

    .line 2692812
    invoke-static {v4}, LX/JQo;->b$redex0(LX/JQo;)V

    .line 2692813
    goto :goto_0
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 2692795
    sget-object v0, LX/1mv;->VIEW:LX/1mv;

    return-object v0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 2692794
    const/16 v0, 0xf

    return v0
.end method
