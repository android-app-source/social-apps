.class public LX/Ikn;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/Ikm;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/Il2;

.field private final b:LX/Ikr;

.field private final c:LX/Iku;

.field public d:LX/Il0;

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/Il1;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Il2;LX/Ikr;LX/Iku;)V
    .locals 1
    .param p3    # LX/Iku;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2607062
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2607063
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2607064
    iput-object v0, p0, LX/Ikn;->e:LX/0Px;

    .line 2607065
    iput-object p1, p0, LX/Ikn;->a:LX/Il2;

    .line 2607066
    iput-object p2, p0, LX/Ikn;->b:LX/Ikr;

    .line 2607067
    iput-object p3, p0, LX/Ikn;->c:LX/Iku;

    .line 2607068
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2607069
    iget-object v0, p0, LX/Ikn;->b:LX/Ikr;

    invoke-virtual {v0, p1, p2}, LX/Ikr;->a(Landroid/view/ViewGroup;I)LX/Ikp;

    move-result-object v1

    .line 2607070
    instance-of v0, v1, LX/Il9;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 2607071
    check-cast v0, LX/Il9;

    iget-object v2, p0, LX/Ikn;->c:LX/Iku;

    .line 2607072
    iput-object v2, v0, LX/Il9;->e:LX/Iku;

    .line 2607073
    :cond_0
    new-instance v0, LX/Ikm;

    invoke-direct {v0, v1}, LX/Ikm;-><init>(LX/Ikp;)V

    return-object v0
.end method

.method public final a(LX/1a1;I)V
    .locals 1

    .prologue
    .line 2607074
    check-cast p1, LX/Ikm;

    .line 2607075
    iget-object v0, p0, LX/Ikn;->d:LX/Il0;

    .line 2607076
    iget-object p0, p1, LX/Ikm;->l:LX/Ikp;

    invoke-interface {p0, v0}, LX/Ikp;->a(LX/Il0;)V

    .line 2607077
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2607078
    iget-object v0, p0, LX/Ikn;->e:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Il1;

    invoke-virtual {v0}, LX/Il1;->getItemViewType()I

    move-result v0

    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2607079
    iget-object v0, p0, LX/Ikn;->e:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
