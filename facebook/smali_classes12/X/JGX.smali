.class public final LX/JGX;
.super LX/JGN;
.source ""


# instance fields
.field public c:Landroid/text/Layout;

.field private d:F

.field private e:F


# direct methods
.method public constructor <init>(Landroid/text/Layout;)V
    .locals 0

    .prologue
    .line 2667616
    invoke-direct {p0}, LX/JGN;-><init>()V

    .line 2667617
    invoke-virtual {p0, p1}, LX/JGX;->a(Landroid/text/Layout;)V

    .line 2667618
    return-void
.end method


# virtual methods
.method public final a()Landroid/text/Layout;
    .locals 1

    .prologue
    .line 2667604
    iget-object v0, p0, LX/JGX;->c:Landroid/text/Layout;

    return-object v0
.end method

.method public final a(Landroid/text/Layout;)V
    .locals 1

    .prologue
    .line 2667612
    iput-object p1, p0, LX/JGX;->c:Landroid/text/Layout;

    .line 2667613
    invoke-virtual {p1}, Landroid/text/Layout;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, LX/JGX;->d:F

    .line 2667614
    invoke-static {p1}, LX/JMd;->b(Landroid/text/Layout;)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, LX/JGX;->e:F

    .line 2667615
    return-void
.end method

.method public final b()F
    .locals 1

    .prologue
    .line 2667619
    iget v0, p0, LX/JGX;->d:F

    return v0
.end method

.method public final c()F
    .locals 1

    .prologue
    .line 2667611
    iget v0, p0, LX/JGX;->e:F

    return v0
.end method

.method public final c(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 2667605
    invoke-virtual {p0}, LX/JGN;->j()F

    move-result v0

    .line 2667606
    invoke-virtual {p0}, LX/JGN;->k()F

    move-result v1

    .line 2667607
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2667608
    iget-object v2, p0, LX/JGX;->c:Landroid/text/Layout;

    invoke-virtual {v2, p1}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    .line 2667609
    neg-float v0, v0

    neg-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2667610
    return-void
.end method
