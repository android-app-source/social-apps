.class public LX/IO9;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public a:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/17Y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/DWD;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/IOF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public i:Lcom/facebook/widget/text/BetterTextView;

.field public j:Lcom/facebook/widget/text/BetterTextView;

.field public k:Lcom/facebook/widget/text/BetterTextView;

.field public l:Lcom/facebook/fig/button/FigButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 8

    .prologue
    .line 2571476
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2571477
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, LX/IO9;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v5

    check-cast v5, LX/17Y;

    invoke-static {v0}, LX/DWD;->b(LX/0QB;)LX/DWD;

    move-result-object v6

    check-cast v6, LX/DWD;

    invoke-static {v0}, LX/IOF;->a(LX/0QB;)LX/IOF;

    move-result-object v7

    check-cast v7, LX/IOF;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object p1

    check-cast p1, LX/0kL;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    iput-object v3, v2, LX/IO9;->a:Ljava/util/concurrent/ExecutorService;

    iput-object v4, v2, LX/IO9;->b:Lcom/facebook/content/SecureContextHelper;

    iput-object v5, v2, LX/IO9;->c:LX/17Y;

    iput-object v6, v2, LX/IO9;->d:LX/DWD;

    iput-object v7, v2, LX/IO9;->e:LX/IOF;

    iput-object p1, v2, LX/IO9;->f:LX/0kL;

    iput-object v0, v2, LX/IO9;->g:LX/0Zb;

    .line 2571478
    const v0, 0x7f0302f4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2571479
    const v0, 0x7f0d0a2b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/IO9;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2571480
    const v0, 0x7f0d0a2c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/IO9;->i:Lcom/facebook/widget/text/BetterTextView;

    .line 2571481
    const v0, 0x7f0d0a2d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/IO9;->j:Lcom/facebook/widget/text/BetterTextView;

    .line 2571482
    const v0, 0x7f0d0a2e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/IO9;->k:Lcom/facebook/widget/text/BetterTextView;

    .line 2571483
    const v0, 0x7f0d0a2f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    iput-object v0, p0, LX/IO9;->l:Lcom/facebook/fig/button/FigButton;

    .line 2571484
    return-void
.end method

.method public static a$redex0(LX/IO9;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2571466
    iget-object v0, p0, LX/IO9;->g:LX/0Zb;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 2571467
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2571468
    const-string v1, "group_email_verification"

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v1, "community_id"

    invoke-virtual {v0, v1, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v1, "invitee_id"

    invoke-virtual {v0, v1, p3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    invoke-virtual {v0}, LX/0oG;->d()V

    .line 2571469
    :cond_0
    return-void
.end method

.method public static setInviteButtonState(LX/IO9;LX/IOE;)V
    .locals 2

    .prologue
    .line 2571470
    sget-object v0, LX/IO8;->a:[I

    invoke-virtual {p1}, LX/IOE;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2571471
    :goto_0
    return-void

    .line 2571472
    :pswitch_0
    iget-object v0, p0, LX/IO9;->l:Lcom/facebook/fig/button/FigButton;

    const v1, 0x7f082fec

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setText(I)V

    .line 2571473
    iget-object v0, p0, LX/IO9;->l:Lcom/facebook/fig/button/FigButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setEnabled(Z)V

    goto :goto_0

    .line 2571474
    :pswitch_1
    iget-object v0, p0, LX/IO9;->l:Lcom/facebook/fig/button/FigButton;

    const v1, 0x7f082feb

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setText(I)V

    .line 2571475
    iget-object v0, p0, LX/IO9;->l:Lcom/facebook/fig/button/FigButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setEnabled(Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
