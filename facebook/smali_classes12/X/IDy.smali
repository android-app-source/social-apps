.class public LX/IDy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DS5;


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field private final d:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation
.end field

.field public final e:LX/03V;

.field private final f:Lcom/facebook/friendlist/data/FriendListDiscoveryEntryPointQueryExecutor;

.field public final g:LX/IE7;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;LX/DHs;LX/DHr;Ljava/util/concurrent/Executor;LX/03V;Lcom/facebook/friendlist/data/FriendListDiscoveryEntryPointQueryExecutor;LX/IE8;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/DHs;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/DHr;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2551363
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2551364
    iput-object p1, p0, LX/IDy;->a:Landroid/content/Context;

    .line 2551365
    iput-object p2, p0, LX/IDy;->b:Ljava/lang/String;

    .line 2551366
    iput-object p3, p0, LX/IDy;->c:Ljava/lang/String;

    .line 2551367
    iput-object p6, p0, LX/IDy;->d:Ljava/util/concurrent/Executor;

    .line 2551368
    iput-object p7, p0, LX/IDy;->e:LX/03V;

    .line 2551369
    iput-object p8, p0, LX/IDy;->f:Lcom/facebook/friendlist/data/FriendListDiscoveryEntryPointQueryExecutor;

    .line 2551370
    invoke-virtual {p9, p1, p4, p5}, LX/IE8;->a(Landroid/content/Context;LX/DHs;LX/DHr;)LX/IE7;

    move-result-object v0

    iput-object v0, p0, LX/IDy;->g:LX/IE7;

    .line 2551371
    return-void
.end method

.method private static a(Lcom/facebook/fbui/widget/contentview/ContentView;)Lcom/facebook/fbui/widget/contentview/ContentView;
    .locals 1

    .prologue
    .line 2551360
    sget-object v0, LX/6VF;->XLARGE:LX/6VF;

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setThumbnailSize(LX/6VF;)V

    .line 2551361
    const v0, 0x7f0207fb

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setBackgroundResource(I)V

    .line 2551362
    return-object p0
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 2551330
    iget-object v0, p0, LX/IDy;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b23d7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    .line 2551336
    sget-object v1, LX/IDx;->a:[I

    sget-object v0, LX/IE0;->VALUES:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IE0;

    invoke-virtual {v0}, LX/IE0;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2551337
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2551338
    :pswitch_0
    iget-object v0, p0, LX/IDy;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031056

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 2551339
    :pswitch_1
    new-instance v0, LX/IE9;

    iget-object v1, p0, LX/IDy;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/IE9;-><init>(Landroid/content/Context;)V

    .line 2551340
    const v1, 0x7f0d0061

    invoke-virtual {v0, v1}, LX/IE9;->setId(I)V

    .line 2551341
    invoke-static {v0}, LX/IDy;->a(Lcom/facebook/fbui/widget/contentview/ContentView;)Lcom/facebook/fbui/widget/contentview/ContentView;

    move-result-object v0

    goto :goto_0

    .line 2551342
    :pswitch_2
    new-instance v0, LX/IDv;

    iget-object v1, p0, LX/IDy;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/IDv;-><init>(Landroid/content/Context;)V

    .line 2551343
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, LX/IDv;->setVisibility(I)V

    .line 2551344
    iget-object v1, p0, LX/IDy;->f:Lcom/facebook/friendlist/data/FriendListDiscoveryEntryPointQueryExecutor;

    iget-object v2, p0, LX/IDy;->b:Ljava/lang/String;

    .line 2551345
    new-instance v3, LX/IEf;

    invoke-direct {v3}, LX/IEf;-><init>()V

    move-object v3, v3

    .line 2551346
    const-string v4, "profile_id"

    invoke-virtual {v3, v4, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2551347
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/friendlist/data/FriendListDiscoveryEntryPointQueryExecutor;->a(LX/0zO;)LX/0zO;

    move-result-object v3

    .line 2551348
    new-instance v4, LX/IEe;

    invoke-direct {v4}, LX/IEe;-><init>()V

    move-object v4, v4

    .line 2551349
    const-string p1, "profile_id"

    invoke-virtual {v4, p1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2551350
    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/friendlist/data/FriendListDiscoveryEntryPointQueryExecutor;->a(LX/0zO;)LX/0zO;

    move-result-object v4

    .line 2551351
    new-instance p1, LX/0v6;

    const-string p2, "FriendListDiscoveryEntryPoint"

    invoke-direct {p1, p2}, LX/0v6;-><init>(Ljava/lang/String;)V

    .line 2551352
    invoke-virtual {p1, v3}, LX/0v6;->b(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    invoke-virtual {p1, v4}, LX/0v6;->b(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    invoke-static {v3, v4}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    .line 2551353
    iget-object v4, v1, Lcom/facebook/friendlist/data/FriendListDiscoveryEntryPointQueryExecutor;->b:LX/0tX;

    invoke-virtual {v4, p1}, LX/0tX;->a(LX/0v6;)V

    .line 2551354
    invoke-static {v3}, LX/0Vg;->a(Ljava/lang/Iterable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    new-instance v4, LX/ID5;

    invoke-direct {v4, v1}, LX/ID5;-><init>(Lcom/facebook/friendlist/data/FriendListDiscoveryEntryPointQueryExecutor;)V

    .line 2551355
    sget-object p1, LX/131;->INSTANCE:LX/131;

    move-object p1, p1

    .line 2551356
    invoke-static {v3, v4, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v1, v3

    .line 2551357
    new-instance v2, LX/IDw;

    invoke-direct {v2, p0, v0}, LX/IDw;-><init>(LX/IDy;LX/IDv;)V

    move-object v2, v2

    .line 2551358
    iget-object v3, p0, LX/IDy;->d:Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2551359
    invoke-static {v0}, LX/IDy;->a(Lcom/facebook/fbui/widget/contentview/ContentView;)Lcom/facebook/fbui/widget/contentview/ContentView;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Ljava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 2551332
    sget-object v1, LX/IDx;->a:[I

    sget-object v0, LX/IE0;->VALUES:LX/0Px;

    invoke-virtual {v0, p3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IE0;

    invoke-virtual {v0}, LX/IE0;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2551333
    :goto_0
    return-void

    .line 2551334
    :pswitch_0
    check-cast p2, Landroid/widget/TextView;

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2551335
    :pswitch_1
    iget-object v0, p0, LX/IDy;->g:LX/IE7;

    check-cast p2, LX/IE9;

    check-cast p1, LX/IDH;

    invoke-virtual {v0, p2, p1}, LX/IE7;->a(LX/IE9;LX/IDH;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 2551331
    iget-object v0, p0, LX/IDy;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0121

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    return v0
.end method
