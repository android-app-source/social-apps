.class public final LX/I78;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0fu;


# instance fields
.field public final synthetic a:Lcom/facebook/events/permalink/EventPermalinkFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/permalink/EventPermalinkFragment;)V
    .locals 0

    .prologue
    .line 2538585
    iput-object p1, p0, LX/I78;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final ko_()Z
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2538586
    iget-object v2, p0, LX/I78;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    iget-object v2, v2, Lcom/facebook/events/permalink/EventPermalinkFragment;->az:LX/I8k;

    .line 2538587
    iget v3, v2, LX/I8k;->C:I

    if-nez v3, :cond_4

    const/4 v3, 0x1

    :goto_0
    move v2, v3

    .line 2538588
    if-nez v2, :cond_3

    .line 2538589
    iget-object v2, p0, LX/I78;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    iget-object v2, v2, Lcom/facebook/events/permalink/EventPermalinkFragment;->aZ:LX/0ta;

    if-eqz v2, :cond_0

    move v0, v1

    .line 2538590
    :cond_0
    iget-object v2, p0, LX/I78;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    iget-object v2, v2, Lcom/facebook/events/permalink/EventPermalinkFragment;->aY:Lcom/facebook/events/model/Event;

    if-eqz v2, :cond_1

    .line 2538591
    iget-object v0, p0, LX/I78;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/events/permalink/EventPermalinkFragment;->s:LX/IBN;

    .line 2538592
    iget-object v3, v0, LX/IBN;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v4, 0x60015

    const-string v5, "EventPermalinkFragment"

    const/4 v6, 0x0

    const-string v7, "EventPermalinkLoadSourceType"

    const-string v8, "notification_prefetch"

    invoke-interface/range {v3 .. v8}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2538593
    :goto_1
    iget-object v0, p0, LX/I78;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/events/permalink/EventPermalinkFragment;->f:LX/BiW;

    .line 2538594
    iget-object v2, v0, LX/BiW;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    iget-object v3, v0, LX/BiW;->a:LX/0Yj;

    invoke-interface {v2, v3}, Lcom/facebook/performancelogger/PerformanceLogger;->b(LX/0Yj;)V

    .line 2538595
    iget-object v0, p0, LX/I78;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/events/permalink/EventPermalinkFragment;->t:LX/IBQ;

    sget-object v2, LX/IBP;->RENDERING:LX/IBP;

    invoke-virtual {v0, v2}, LX/IBQ;->d(LX/IBP;)V

    .line 2538596
    iget-object v0, p0, LX/I78;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/events/permalink/EventPermalinkFragment;->t:LX/IBQ;

    .line 2538597
    iget-object v2, v0, LX/IBQ;->d:LX/0id;

    const-string v3, "LoadEventPermalink"

    invoke-virtual {v2, v3}, LX/0id;->b(Ljava/lang/String;)V

    .line 2538598
    iget-object v0, p0, LX/I78;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    invoke-static {v0}, Lcom/facebook/events/permalink/EventPermalinkFragment;->y(Lcom/facebook/events/permalink/EventPermalinkFragment;)V

    .line 2538599
    :goto_2
    return v1

    .line 2538600
    :cond_1
    if-eqz v0, :cond_2

    .line 2538601
    iget-object v0, p0, LX/I78;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/events/permalink/EventPermalinkFragment;->s:LX/IBN;

    .line 2538602
    iget-object v3, v0, LX/IBN;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v4, 0x60015

    const-string v5, "EventPermalinkFragment"

    const/4 v6, 0x0

    const-string v7, "EventPermalinkLoadSourceType"

    const-string v8, "graphql"

    invoke-interface/range {v3 .. v8}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2538603
    goto :goto_1

    .line 2538604
    :cond_2
    iget-object v0, p0, LX/I78;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/events/permalink/EventPermalinkFragment;->s:LX/IBN;

    .line 2538605
    iget-object v3, v0, LX/IBN;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v4, 0x60015

    const-string v5, "EventPermalinkFragment"

    const/4 v6, 0x0

    const-string v7, "EventPermalinkLoadSourceType"

    const-string v8, "db"

    invoke-interface/range {v3 .. v8}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2538606
    goto :goto_1

    :cond_3
    move v1, v0

    .line 2538607
    goto :goto_2

    :cond_4
    const/4 v3, 0x0

    goto :goto_0
.end method
