.class public final LX/I3W;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSuggestionsForForCutTypeModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Hxb;

.field public final synthetic b:LX/I3Y;


# direct methods
.method public constructor <init>(LX/I3Y;LX/Hxb;)V
    .locals 0

    .prologue
    .line 2531466
    iput-object p1, p0, LX/I3W;->b:LX/I3Y;

    iput-object p2, p0, LX/I3W;->a:LX/Hxb;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2531467
    iget-object v0, p0, LX/I3W;->a:LX/Hxb;

    invoke-interface {v0}, LX/Hxb;->a()V

    .line 2531468
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2531469
    invoke-direct {p0}, LX/I3W;->a()V

    .line 2531470
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2531471
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2531472
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2531473
    if-eqz v0, :cond_1

    .line 2531474
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2531475
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSuggestionsForForCutTypeModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSuggestionsForForCutTypeModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSuggestionsForForCutTypeModel$SuggestedEventCutsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2531476
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2531477
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSuggestionsForForCutTypeModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSuggestionsForForCutTypeModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSuggestionsForForCutTypeModel$SuggestedEventCutsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSuggestionsForForCutTypeModel$SuggestedEventCutsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    .line 2531478
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;

    .line 2531479
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;->c()Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel$EventsModel;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;->c()Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel$EventsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel$EventsModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 2531480
    iget-object v1, p0, LX/I3W;->a:LX/Hxb;

    invoke-interface {v1, v0}, LX/Hxb;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;)V

    .line 2531481
    :goto_1
    return-void

    .line 2531482
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2531483
    :cond_1
    invoke-direct {p0}, LX/I3W;->a()V

    goto :goto_1
.end method
