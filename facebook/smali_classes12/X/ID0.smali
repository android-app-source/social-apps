.class public LX/ID0;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/ID0;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2550275
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2550276
    const-string v0, "friending/suggestion/{#%s}/{%s}"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.facebook.katana.profile.id"

    const-string v2, "profile_name"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v2, LX/0cQ;->FRIEND_SUGGESTION_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    .line 2550277
    return-void
.end method

.method public static a(LX/0QB;)LX/ID0;
    .locals 3

    .prologue
    .line 2550278
    sget-object v0, LX/ID0;->a:LX/ID0;

    if-nez v0, :cond_1

    .line 2550279
    const-class v1, LX/ID0;

    monitor-enter v1

    .line 2550280
    :try_start_0
    sget-object v0, LX/ID0;->a:LX/ID0;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2550281
    if-eqz v2, :cond_0

    .line 2550282
    :try_start_1
    new-instance v0, LX/ID0;

    invoke-direct {v0}, LX/ID0;-><init>()V

    .line 2550283
    move-object v0, v0

    .line 2550284
    sput-object v0, LX/ID0;->a:LX/ID0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2550285
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2550286
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2550287
    :cond_1
    sget-object v0, LX/ID0;->a:LX/ID0;

    return-object v0

    .line 2550288
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2550289
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
