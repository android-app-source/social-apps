.class public final LX/IJA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Ljava/util/List",
        "<*>;",
        "LX/0P1",
        "<",
        "Ljava/lang/String;",
        "LX/0Px",
        "<",
        "Lcom/facebook/user/model/User;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;)V
    .locals 0

    .prologue
    .line 2563124
    iput-object p1, p0, LX/IJA;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 12
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2563125
    check-cast p1, Ljava/util/List;

    .line 2563126
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    .line 2563127
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x3

    if-eq v0, v2, :cond_1

    .line 2563128
    :cond_0
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    .line 2563129
    :goto_0
    return-object v0

    .line 2563130
    :cond_1
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2563131
    iget-object v2, p0, LX/IJA;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;

    const/4 v6, 0x0

    .line 2563132
    new-instance v7, LX/0cA;

    invoke-direct {v7}, LX/0cA;-><init>()V

    .line 2563133
    new-instance v8, LX/0cA;

    invoke-direct {v8}, LX/0cA;-><init>()V

    .line 2563134
    iget-object v3, v0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v3, v3

    .line 2563135
    check-cast v3, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyInviteStatusQueryModel;

    invoke-virtual {v3}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyInviteStatusQueryModel;->a()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyInviteStatusQueryModel$AllFriendsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyInviteStatusQueryModel$AllFriendsModel;->a()LX/0Px;

    move-result-object v9

    .line 2563136
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    move v5, v6

    :goto_1
    if-ge v5, v10, :cond_4

    invoke-virtual {v9, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyInviteFriendModel;

    .line 2563137
    invoke-virtual {v3}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyInviteFriendModel;->k()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyInviteFriendModel$RequestableFieldsModel;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v3}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyInviteFriendModel;->k()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyInviteFriendModel$RequestableFieldsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyInviteFriendModel$RequestableFieldsModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    .line 2563138
    invoke-virtual {v3}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyInviteFriendModel;->k()Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyInviteFriendModel$RequestableFieldsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyInviteFriendModel$RequestableFieldsModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyInviteFriendModel$RequestableFieldsModel$NodesModel;

    .line 2563139
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyInviteFriendModel$RequestableFieldsModel$NodesModel;->j()Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    move-result-object v11

    if-eqz v11, :cond_2

    invoke-virtual {v4}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyInviteFriendModel$RequestableFieldsModel$NodesModel;->j()Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;->name()Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_2

    .line 2563140
    invoke-virtual {v4}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyInviteFriendModel$RequestableFieldsModel$NodesModel;->j()Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;->name()Ljava/lang/String;

    move-result-object v4

    .line 2563141
    const-string v11, "REQUESTABLE"

    invoke-virtual {v11, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 2563142
    invoke-virtual {v3}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyInviteFriendModel;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 2563143
    :cond_2
    :goto_2
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_1

    .line 2563144
    :cond_3
    const-string v11, "REQUESTED"

    invoke-virtual {v11, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2563145
    invoke-virtual {v3}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyInviteFriendModel;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 2563146
    invoke-virtual {v3}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyInviteFriendModel;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8, v3}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto :goto_2

    .line 2563147
    :cond_4
    invoke-virtual {v7}, LX/0cA;->b()LX/0Rf;

    move-result-object v3

    iput-object v3, v2, Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;->E:LX/0Rf;

    .line 2563148
    invoke-virtual {v8}, LX/0cA;->b()LX/0Rf;

    move-result-object v3

    iput-object v3, v2, Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;->D:LX/0Rf;

    .line 2563149
    iget-object v0, p0, LX/IJA;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;->A:LX/IJ6;

    iget-object v2, p0, LX/IJA;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;

    iget-object v2, v2, Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;->D:LX/0Rf;

    .line 2563150
    iput-object v2, v0, LX/IJ6;->a:LX/0Rf;

    .line 2563151
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0P1;

    .line 2563152
    iget-object v3, p0, LX/IJA;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;

    iget-object v3, v3, Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;->E:LX/0Rf;

    .line 2563153
    if-nez v0, :cond_5

    .line 2563154
    sget-object v4, LX/0Rg;->a:LX/0Rg;

    move-object v4, v4

    .line 2563155
    :goto_3
    move-object v4, v4

    .line 2563156
    move-object v0, v4

    .line 2563157
    invoke-virtual {v1, v0}, LX/0P2;->a(Ljava/util/Map;)LX/0P2;

    .line 2563158
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    .line 2563159
    sget-object v2, Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;->u:Ljava/lang/String;

    iget-object v4, p0, LX/IJA;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;

    iget-object v4, v4, Lcom/facebook/friendsnearby/ui/FriendsNearbyInviteMultipickerFragment;->E:LX/0Rf;

    .line 2563160
    const/4 v5, 0x0

    .line 2563161
    if-nez v0, :cond_a

    .line 2563162
    sget-object v5, LX/0Q7;->a:LX/0Px;

    move-object v5, v5

    .line 2563163
    :goto_4
    move-object v5, v5

    .line 2563164
    move-object v0, v5

    .line 2563165
    invoke-virtual {v1, v2, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2563166
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    goto/16 :goto_0

    .line 2563167
    :cond_5
    new-instance v8, LX/0P2;

    invoke-direct {v8}, LX/0P2;-><init>()V

    .line 2563168
    invoke-virtual {v0}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v4

    invoke-virtual {v4}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_6
    :goto_5
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 2563169
    invoke-virtual {v0, v4}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0Px;

    .line 2563170
    new-instance v10, LX/0Pz;

    invoke-direct {v10}, LX/0Pz;-><init>()V

    .line 2563171
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v11

    const/4 v6, 0x0

    move v7, v6

    :goto_6
    if-ge v7, v11, :cond_8

    invoke-virtual {v5, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/user/model/User;

    .line 2563172
    iget-object v2, v6, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2563173
    invoke-virtual {v3, v2}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2563174
    invoke-virtual {v10, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2563175
    :cond_7
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_6

    .line 2563176
    :cond_8
    invoke-virtual {v10}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    .line 2563177
    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_6

    .line 2563178
    invoke-virtual {v8, v4, v5}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_5

    .line 2563179
    :cond_9
    invoke-virtual {v8}, LX/0P2;->b()LX/0P1;

    move-result-object v4

    goto :goto_3

    .line 2563180
    :cond_a
    new-instance v8, LX/0Pz;

    invoke-direct {v8}, LX/0Pz;-><init>()V

    .line 2563181
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v9

    move v7, v5

    move v6, v5

    :goto_7
    if-ge v7, v9, :cond_b

    invoke-virtual {v0, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/user/model/User;

    .line 2563182
    iget-object v3, v5, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v3, v3

    .line 2563183
    invoke-virtual {v4, v3}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 2563184
    invoke-virtual {v8, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2563185
    add-int/lit8 v5, v6, 0x1

    const/16 v6, 0xa

    if-ge v5, v6, :cond_b

    .line 2563186
    :goto_8
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    move v6, v5

    goto :goto_7

    .line 2563187
    :cond_b
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    goto/16 :goto_4

    :cond_c
    move v5, v6

    goto :goto_8
.end method
