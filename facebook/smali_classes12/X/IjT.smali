.class public LX/IjT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6yL;


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:LX/6ye;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/03V;

.field private final f:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

.field private final g:Ljava/util/concurrent/Executor;

.field public h:LX/6qh;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2605807
    const-class v0, LX/IjT;

    sput-object v0, LX/IjT;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/6ye;LX/0Or;LX/03V;Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUser;
        .end annotation
    .end param
    .param p6    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/6ye;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;",
            "Ljava/util/concurrent/Executor;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2605799
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2605800
    iput-object p1, p0, LX/IjT;->b:Landroid/content/Context;

    .line 2605801
    iput-object p2, p0, LX/IjT;->c:LX/6ye;

    .line 2605802
    iput-object p3, p0, LX/IjT;->d:LX/0Or;

    .line 2605803
    iput-object p4, p0, LX/IjT;->e:LX/03V;

    .line 2605804
    iput-object p5, p0, LX/IjT;->f:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    .line 2605805
    iput-object p6, p0, LX/IjT;->g:Ljava/util/concurrent/Executor;

    .line 2605806
    return-void
.end method

.method private a(LX/73T;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/73T;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2605792
    const-string v0, "payment_card_id"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, LX/73T;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2605793
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2605794
    iget-object v1, p0, LX/IjT;->f:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iget-object v0, p0, LX/IjT;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2605795
    iget-object p1, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, p1

    .line 2605796
    invoke-virtual {v1, v2, v3, v0}, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->a(JLjava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2605797
    new-instance v1, LX/IjS;

    invoke-direct {v1, p0}, LX/IjS;-><init>(LX/IjT;)V

    iget-object v2, p0, LX/IjT;->g:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2605798
    return-object v0
.end method

.method private b(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;LX/73T;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6

    .prologue
    .line 2605783
    iget-object v0, p0, LX/IjT;->c:LX/6ye;

    invoke-virtual {v0, p1}, LX/6ye;->a(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)V

    .line 2605784
    const-string v0, "extra_fb_payment_card"

    invoke-virtual {p2, v0}, LX/73T;->a(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;

    .line 2605785
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2605786
    iget-object v1, p0, LX/IjT;->f:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    invoke-interface {v0}, Lcom/facebook/payments/paymentmethods/model/PaymentOption;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2605787
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2605788
    sget-object v5, Lcom/facebook/payments/p2p/service/model/cards/DeletePaymentCardParams;->a:Ljava/lang/String;

    new-instance p2, Lcom/facebook/payments/p2p/service/model/cards/DeletePaymentCardParams;

    invoke-direct {p2, v2, v3}, Lcom/facebook/payments/p2p/service/model/cards/DeletePaymentCardParams;-><init>(J)V

    invoke-virtual {v4, v5, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2605789
    const-string v5, "delete_payment_card"

    invoke-static {v1, v4, v5}, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->a(Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v1, v4

    .line 2605790
    new-instance v2, LX/IjR;

    invoke-direct {v2, p0, p1, v0}, LX/IjR;-><init>(LX/IjT;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;)V

    iget-object v0, p0, LX/IjT;->g:Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2605791
    return-object v1
.end method

.method public static b(LX/IjT;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;LX/6y8;)V
    .locals 13

    .prologue
    const/4 v9, 0x1

    .line 2605769
    iget-object v0, p0, LX/IjT;->h:LX/6qh;

    if-nez v0, :cond_0

    .line 2605770
    :goto_0
    return-void

    .line 2605771
    :cond_0
    invoke-interface {p1}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->e:Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;

    check-cast v0, Lcom/facebook/payments/p2p/model/PaymentCard;

    .line 2605772
    new-instance v1, Lcom/facebook/payments/p2p/model/PartialPaymentCard;

    .line 2605773
    iget-wide v11, v0, Lcom/facebook/payments/p2p/model/PaymentCard;->a:J

    move-wide v2, v11

    .line 2605774
    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/PaymentCard;->f()Ljava/lang/String;

    move-result-object v4

    iget v5, p2, LX/6y8;->c:I

    iget v6, p2, LX/6y8;->d:I

    add-int/lit16 v6, v6, 0x7d0

    new-instance v7, Lcom/facebook/payments/p2p/model/Address;

    iget-object v8, p2, LX/6y8;->f:Ljava/lang/String;

    invoke-direct {v7, v8}, Lcom/facebook/payments/p2p/model/Address;-><init>(Ljava/lang/String;)V

    .line 2605775
    iget-object v8, v0, Lcom/facebook/payments/p2p/model/PaymentCard;->f:Ljava/lang/String;

    move-object v8, v8

    .line 2605776
    move v10, v9

    invoke-direct/range {v1 .. v10}, Lcom/facebook/payments/p2p/model/PartialPaymentCard;-><init>(JLjava/lang/String;IILcom/facebook/payments/p2p/model/Address;Ljava/lang/String;ZZ)V

    .line 2605777
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2605778
    const-string v2, "partial_payment_card"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2605779
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2605780
    const-string v2, "extra_activity_result_data"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2605781
    new-instance v0, LX/73T;

    sget-object v2, LX/73S;->FINISH_ACTIVITY:LX/73S;

    invoke-direct {v0, v2, v1}, LX/73T;-><init>(LX/73S;Landroid/os/Bundle;)V

    .line 2605782
    iget-object v1, p0, LX/IjT;->h:LX/6qh;

    invoke-virtual {v1, v0}, LX/6qh;->a(LX/73T;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;LX/6y8;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 8

    .prologue
    .line 2605762
    invoke-interface {p1}, Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;->a()Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/payments/paymentmethods/cardform/CardFormCommonParams;->e:Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;

    move-object v6, v0

    check-cast v6, Lcom/facebook/payments/p2p/model/PaymentCard;

    .line 2605763
    iget-object v0, p0, LX/IjT;->f:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    invoke-virtual {v6}, Lcom/facebook/payments/p2p/model/PaymentCard;->a()Ljava/lang/String;

    move-result-object v1

    iget v2, p2, LX/6y8;->c:I

    iget v3, p2, LX/6y8;->d:I

    iget-object v4, p2, LX/6y8;->e:Ljava/lang/String;

    iget-object v5, p2, LX/6y8;->f:Ljava/lang/String;

    .line 2605764
    iget-boolean v7, v6, Lcom/facebook/payments/p2p/model/PaymentCard;->g:Z

    move v6, v7

    .line 2605765
    if-nez v6, :cond_0

    const/4 v6, 0x1

    :goto_0
    invoke-virtual/range {v0 .. v6}, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->a(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2605766
    new-instance v1, LX/IjQ;

    invoke-direct {v1, p0, p1, p2}, LX/IjQ;-><init>(LX/IjT;Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;LX/6y8;)V

    iget-object v2, p0, LX/IjT;->g:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2605767
    return-object v0

    .line 2605768
    :cond_0
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;LX/73T;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2

    .prologue
    .line 2605755
    const-string v0, "extra_mutation"

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, LX/73T;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2605756
    const-string v1, "action_set_primary"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2605757
    invoke-direct {p0, p2}, LX/IjT;->a(LX/73T;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2605758
    :goto_0
    return-object v0

    .line 2605759
    :cond_0
    const-string v1, "action_delete_payment_card"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2605760
    invoke-direct {p0, p1, p2}, LX/IjT;->b(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;LX/73T;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    .line 2605761
    :cond_1
    iget-object v0, p0, LX/IjT;->c:LX/6ye;

    invoke-virtual {v0, p1, p2}, LX/6ye;->a(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;LX/73T;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/6qh;)V
    .locals 2

    .prologue
    .line 2605752
    iput-object p1, p0, LX/IjT;->h:LX/6qh;

    .line 2605753
    iget-object v0, p0, LX/IjT;->c:LX/6ye;

    iget-object v1, p0, LX/IjT;->h:LX/6qh;

    invoke-virtual {v0, v1}, LX/6ye;->a(LX/6qh;)V

    .line 2605754
    return-void
.end method
