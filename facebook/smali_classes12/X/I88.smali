.class public LX/I88;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Z

.field public b:LX/AhO;

.field public c:Lcom/facebook/events/model/Event;

.field public d:LX/I8J;

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/I83;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Z

.field public final g:LX/Bl6;

.field private final h:LX/Bla;

.field private final i:LX/0Uh;

.field public final j:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/I7Z;",
            ">;"
        }
    .end annotation
.end field

.field public final k:LX/0ad;

.field public final l:LX/I86;

.field public final m:LX/I87;

.field public n:I


# direct methods
.method public constructor <init>(LX/AhO;Landroid/content/Context;LX/Bl6;LX/I8K;LX/Bla;LX/0Uh;Ljava/lang/Boolean;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/0Or;LX/0ad;)V
    .locals 2
    .param p1    # LX/AhO;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .param p9    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/AhO;",
            "Landroid/content/Context;",
            "LX/Bl6;",
            "LX/I8K;",
            "LX/Bla;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Ljava/lang/Boolean;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Or",
            "<",
            "LX/I7Z;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2540739
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2540740
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2540741
    iput-object v0, p0, LX/I88;->e:LX/0Px;

    .line 2540742
    new-instance v0, LX/I86;

    invoke-direct {v0}, LX/I86;-><init>()V

    iput-object v0, p0, LX/I88;->l:LX/I86;

    .line 2540743
    new-instance v0, LX/I87;

    invoke-direct {v0}, LX/I87;-><init>()V

    iput-object v0, p0, LX/I88;->m:LX/I87;

    .line 2540744
    iput-object p3, p0, LX/I88;->g:LX/Bl6;

    .line 2540745
    iput-object p5, p0, LX/I88;->h:LX/Bla;

    .line 2540746
    iput-object p6, p0, LX/I88;->i:LX/0Uh;

    .line 2540747
    invoke-virtual {p7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/I88;->f:Z

    .line 2540748
    invoke-interface {p9}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/1nR;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v0

    invoke-interface {p8, v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    iput-boolean v0, p0, LX/I88;->a:Z

    .line 2540749
    iput-object p10, p0, LX/I88;->j:LX/0Or;

    .line 2540750
    iput-object p11, p0, LX/I88;->k:LX/0ad;

    .line 2540751
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0060

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/I88;->n:I

    .line 2540752
    iput-object p1, p0, LX/I88;->b:LX/AhO;

    .line 2540753
    iget-object v0, p0, LX/I88;->b:LX/AhO;

    invoke-virtual {p4, v0}, LX/I8K;->a(Landroid/view/View;)LX/I8J;

    move-result-object v0

    iput-object v0, p0, LX/I88;->d:LX/I8J;

    .line 2540754
    iget-object v0, p0, LX/I88;->b:LX/AhO;

    iget-object v1, p0, LX/I88;->d:LX/I8J;

    .line 2540755
    iput-object v1, v0, LX/AhO;->b:LX/AhN;

    .line 2540756
    iget-object v0, p0, LX/I88;->b:LX/AhO;

    new-instance v1, LX/I84;

    invoke-direct {v1, p0}, LX/I84;-><init>(LX/I88;)V

    invoke-virtual {v0, v1}, LX/AhO;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 2540757
    return-void
.end method

.method public static b(LX/I88;LX/I80;)V
    .locals 3

    .prologue
    .line 2540683
    iget-object v0, p0, LX/I88;->c:Lcom/facebook/events/model/Event;

    invoke-static {v0}, Lcom/facebook/events/model/Event;->a(Lcom/facebook/events/model/Event;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2540684
    iget-object v0, p0, LX/I88;->c:Lcom/facebook/events/model/Event;

    sget-object v1, LX/7vK;->ADMIN:LX/7vK;

    invoke-virtual {v0, v1}, Lcom/facebook/events/model/Event;->a(LX/7vK;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/I88;->c:Lcom/facebook/events/model/Event;

    sget-object v1, LX/7vK;->PROMOTE:LX/7vK;

    invoke-virtual {v0, v1}, Lcom/facebook/events/model/Event;->a(LX/7vK;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/I88;->c:Lcom/facebook/events/model/Event;

    sget-object v1, LX/7vK;->EDIT_PROMOTION:LX/7vK;

    invoke-virtual {v0, v1}, Lcom/facebook/events/model/Event;->a(LX/7vK;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, LX/I88;->k:LX/0ad;

    sget-short v1, LX/347;->a:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2540685
    iget-object v0, p0, LX/I88;->c:Lcom/facebook/events/model/Event;

    .line 2540686
    iget-object v1, v0, Lcom/facebook/events/model/Event;->D:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-object v0, v1

    .line 2540687
    iget-object v1, p0, LX/I88;->c:Lcom/facebook/events/model/Event;

    .line 2540688
    iget-boolean v2, v1, Lcom/facebook/events/model/Event;->B:Z

    move v1, v2

    .line 2540689
    if-nez v1, :cond_7

    .line 2540690
    invoke-static {p0, p1}, LX/I88;->e(LX/I88;LX/I80;)V

    .line 2540691
    sget-object v0, LX/I83;->EDIT:LX/I83;

    invoke-virtual {p1, v0}, LX/I80;->a(LX/I83;)V

    .line 2540692
    :cond_1
    :goto_0
    return-void

    .line 2540693
    :cond_2
    iget-object v0, p0, LX/I88;->c:Lcom/facebook/events/model/Event;

    .line 2540694
    iget-boolean v1, v0, Lcom/facebook/events/model/Event;->B:Z

    move v0, v1

    .line 2540695
    if-eqz v0, :cond_5

    .line 2540696
    iget-object v0, p0, LX/I88;->c:Lcom/facebook/events/model/Event;

    sget-object v1, LX/7vK;->ADMIN:LX/7vK;

    invoke-virtual {v0, v1}, Lcom/facebook/events/model/Event;->a(LX/7vK;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2540697
    sget-object v0, LX/I83;->EDIT_OVERFLOW:LX/I83;

    invoke-virtual {p1, v0}, LX/I80;->b(LX/I83;)V

    .line 2540698
    :cond_3
    iget-object v0, p0, LX/I88;->c:Lcom/facebook/events/model/Event;

    .line 2540699
    iget-boolean v1, v0, Lcom/facebook/events/model/Event;->H:Z

    move v0, v1

    .line 2540700
    if-eqz v0, :cond_4

    .line 2540701
    invoke-static {p1}, LX/I88;->f(LX/I80;)V

    goto :goto_0

    .line 2540702
    :cond_4
    iget-object v0, p0, LX/I88;->c:Lcom/facebook/events/model/Event;

    .line 2540703
    iget-object v1, v0, Lcom/facebook/events/model/Event;->D:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-object v0, v1

    .line 2540704
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->WATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne v0, v1, :cond_c

    .line 2540705
    iget-object v1, p0, LX/I88;->j:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/I7Z;

    const/4 p0, 0x1

    .line 2540706
    iget v0, v1, LX/I7Z;->a:I

    if-ne v0, p0, :cond_f

    :goto_1
    move v1, p0

    .line 2540707
    if-eqz v1, :cond_b

    .line 2540708
    sget-object v1, LX/I83;->TOXICLE_PUBLIC_INTERESTED_SELECTED_WITH_CHEVRON:LX/I83;

    invoke-virtual {p1, v1}, LX/I80;->a(LX/I83;)V

    .line 2540709
    :goto_2
    goto :goto_0

    .line 2540710
    :cond_5
    iget-object v0, p0, LX/I88;->c:Lcom/facebook/events/model/Event;

    sget-object v1, LX/7vK;->ADMIN:LX/7vK;

    invoke-virtual {v0, v1}, Lcom/facebook/events/model/Event;->a(LX/7vK;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2540711
    sget-object v0, LX/I83;->EDIT:LX/I83;

    invoke-virtual {p1, v0}, LX/I80;->a(LX/I83;)V

    goto :goto_0

    .line 2540712
    :cond_6
    iget-object v0, p0, LX/I88;->c:Lcom/facebook/events/model/Event;

    .line 2540713
    iget-boolean v1, v0, Lcom/facebook/events/model/Event;->y:Z

    move v0, v1

    .line 2540714
    if-eqz v0, :cond_1

    .line 2540715
    sget-object v0, LX/I83;->CANCELLED:LX/I83;

    invoke-virtual {p1, v0}, LX/I80;->a(LX/I83;)V

    goto :goto_0

    .line 2540716
    :cond_7
    iget-object v1, p0, LX/I88;->c:Lcom/facebook/events/model/Event;

    .line 2540717
    iget-boolean v2, v1, Lcom/facebook/events/model/Event;->H:Z

    move v1, v2

    .line 2540718
    if-eqz v1, :cond_8

    .line 2540719
    invoke-static {p1}, LX/I88;->f(LX/I80;)V

    goto :goto_0

    .line 2540720
    :cond_8
    invoke-static {p0, p1}, LX/I88;->e(LX/I88;LX/I80;)V

    .line 2540721
    sget-object v1, LX/I83;->EDIT_OVERFLOW:LX/I83;

    invoke-virtual {p1, v1}, LX/I80;->b(LX/I83;)V

    .line 2540722
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->WATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne v0, v1, :cond_9

    .line 2540723
    sget-object v0, LX/I83;->TOXICLE_PUBLIC_INTERESTED_SELECTED_WITH_CHEVRON:LX/I83;

    invoke-virtual {p1, v0}, LX/I80;->a(LX/I83;)V

    goto :goto_0

    .line 2540724
    :cond_9
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne v0, v1, :cond_a

    .line 2540725
    sget-object v0, LX/I83;->TOXICLE_PUBLIC_GOING_SELECTED_WITH_CHEVRON:LX/I83;

    invoke-virtual {p1, v0}, LX/I80;->a(LX/I83;)V

    goto/16 :goto_0

    .line 2540726
    :cond_a
    sget-object v0, LX/I83;->TOXICLE_PUBLIC_GOING:LX/I83;

    invoke-virtual {p1, v0}, LX/I80;->a(LX/I83;)V

    .line 2540727
    sget-object v0, LX/I83;->TOXICLE_PUBLIC_INTERESTED_OVERFLOW:LX/I83;

    invoke-virtual {p1, v0}, LX/I80;->b(LX/I83;)V

    goto/16 :goto_0

    .line 2540728
    :cond_b
    sget-object v1, LX/I83;->TOXICLE_PUBLIC_INTERESTED_SELECTED:LX/I83;

    invoke-virtual {p1, v1}, LX/I80;->a(LX/I83;)V

    .line 2540729
    sget-object v1, LX/I83;->TOXICLE_PUBLIC_GOING:LX/I83;

    invoke-virtual {p1, v1}, LX/I80;->a(LX/I83;)V

    goto :goto_2

    .line 2540730
    :cond_c
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne v0, v1, :cond_e

    .line 2540731
    iget-object v1, p0, LX/I88;->j:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/I7Z;

    const/4 p0, 0x1

    .line 2540732
    iget v0, v1, LX/I7Z;->a:I

    if-ne v0, p0, :cond_10

    :goto_3
    move v1, p0

    .line 2540733
    if-eqz v1, :cond_d

    .line 2540734
    sget-object v1, LX/I83;->TOXICLE_PUBLIC_GOING_SELECTED_WITH_CHEVRON:LX/I83;

    invoke-virtual {p1, v1}, LX/I80;->a(LX/I83;)V

    goto :goto_2

    .line 2540735
    :cond_d
    sget-object v1, LX/I83;->TOXICLE_PUBLIC_INTERESTED:LX/I83;

    invoke-virtual {p1, v1}, LX/I80;->a(LX/I83;)V

    .line 2540736
    sget-object v1, LX/I83;->TOXICLE_PUBLIC_GOING_SELECTED:LX/I83;

    invoke-virtual {p1, v1}, LX/I80;->a(LX/I83;)V

    goto/16 :goto_2

    .line 2540737
    :cond_e
    sget-object v1, LX/I83;->TOXICLE_PUBLIC_INTERESTED:LX/I83;

    invoke-virtual {p1, v1}, LX/I80;->a(LX/I83;)V

    .line 2540738
    sget-object v1, LX/I83;->TOXICLE_PUBLIC_GOING:LX/I83;

    invoke-virtual {p1, v1}, LX/I80;->a(LX/I83;)V

    goto/16 :goto_2

    :cond_f
    const/4 p0, 0x0

    goto/16 :goto_1

    :cond_10
    const/4 p0, 0x0

    goto :goto_3
.end method

.method public static e(LX/I88;LX/I80;)V
    .locals 2

    .prologue
    .line 2540659
    iget-object v0, p0, LX/I88;->c:Lcom/facebook/events/model/Event;

    sget-object v1, LX/7vK;->PROMOTE:LX/7vK;

    invoke-virtual {v0, v1}, Lcom/facebook/events/model/Event;->a(LX/7vK;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2540660
    sget-object v0, LX/I83;->CREATOR_PROMOTE:LX/I83;

    invoke-virtual {p1, v0}, LX/I80;->a(LX/I83;)V

    .line 2540661
    :cond_0
    :goto_0
    return-void

    .line 2540662
    :cond_1
    iget-object v0, p0, LX/I88;->c:Lcom/facebook/events/model/Event;

    sget-object v1, LX/7vK;->EDIT_PROMOTION:LX/7vK;

    invoke-virtual {v0, v1}, Lcom/facebook/events/model/Event;->a(LX/7vK;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2540663
    sget-object v0, LX/I83;->CREATOR_PROMOTED:LX/I83;

    invoke-virtual {p1, v0}, LX/I80;->a(LX/I83;)V

    goto :goto_0
.end method

.method public static f(LX/I80;)V
    .locals 1

    .prologue
    .line 2540758
    sget-object v0, LX/I83;->TOXICLE_PUBLIC_INTERESTED:LX/I83;

    invoke-virtual {p0, v0}, LX/I80;->a(LX/I83;)V

    .line 2540759
    sget-object v0, LX/I83;->TOXICLE_PUBLIC_GOING:LX/I83;

    invoke-virtual {p0, v0}, LX/I80;->a(LX/I83;)V

    .line 2540760
    sget-object v0, LX/I83;->TOXICLE_PUBLIC_IGNORE:LX/I83;

    invoke-virtual {p0, v0}, LX/I80;->a(LX/I83;)V

    .line 2540761
    return-void
.end method

.method public static g(LX/I80;)V
    .locals 2

    .prologue
    .line 2540681
    sget-object v0, LX/I83;->PUBLIC_INVITE_QE_SHARE:LX/I83;

    sget-object v1, LX/I83;->PUBLIC_INVITE_QE_SHARE_OVERFLOW:LX/I83;

    invoke-virtual {p0, v0, v1}, LX/I80;->a(LX/I83;LX/I83;)V

    .line 2540682
    return-void
.end method

.method public static h(LX/I80;)V
    .locals 2

    .prologue
    .line 2540679
    sget-object v0, LX/I83;->PUBLIC_INVITE_QE_SEND:LX/I83;

    sget-object v1, LX/I83;->PUBLIC_INVITE_QE_SEND_OVERFLOW:LX/I83;

    invoke-virtual {p0, v0, v1}, LX/I80;->a(LX/I83;LX/I83;)V

    .line 2540680
    return-void
.end method

.method public static i(LX/I88;LX/I80;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2540664
    iget-object v1, p0, LX/I88;->c:Lcom/facebook/events/model/Event;

    sget-object v2, LX/7vK;->INVITE:LX/7vK;

    invoke-virtual {v1, v2}, Lcom/facebook/events/model/Event;->a(LX/7vK;)Z

    move-result v1

    .line 2540665
    iget-object v2, p0, LX/I88;->c:Lcom/facebook/events/model/Event;

    sget-object v3, LX/7vK;->SHARE:LX/7vK;

    invoke-virtual {v2, v3}, Lcom/facebook/events/model/Event;->a(LX/7vK;)Z

    move-result v2

    .line 2540666
    if-eqz v1, :cond_1

    if-eqz v2, :cond_1

    .line 2540667
    sget-object v0, LX/I83;->INVITE_OR_SHARE:LX/I83;

    sget-object v1, LX/I83;->INVITE_OR_SHARE_OVERFLOW:LX/I83;

    invoke-virtual {p1, v0, v1}, LX/I80;->a(LX/I83;LX/I83;)V

    .line 2540668
    :cond_0
    :goto_0
    return-void

    .line 2540669
    :cond_1
    if-eqz v1, :cond_5

    .line 2540670
    iget-object v1, p0, LX/I88;->c:Lcom/facebook/events/model/Event;

    .line 2540671
    iget-object v2, v1, Lcom/facebook/events/model/Event;->t:Ljava/lang/String;

    move-object v1, v2

    .line 2540672
    iget-object v2, p0, LX/I88;->c:Lcom/facebook/events/model/Event;

    .line 2540673
    iget-object v3, v2, Lcom/facebook/events/model/Event;->d:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-object v2, v3

    .line 2540674
    iget-boolean v3, p0, LX/I88;->f:Z

    iget-object v4, p0, LX/I88;->c:Lcom/facebook/events/model/Event;

    sget-object v5, LX/7vK;->ADMIN:LX/7vK;

    invoke-virtual {v4, v5}, Lcom/facebook/events/model/Event;->a(LX/7vK;)Z

    move-result v4

    invoke-static {v1, v2, v3, v4}, LX/Bla;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;ZZ)Z

    move-result v1

    .line 2540675
    if-eqz v1, :cond_3

    iget-boolean v1, p0, LX/I88;->a:Z

    if-nez v1, :cond_2

    iget-object v1, p0, LX/I88;->i:LX/0Uh;

    const/16 v2, 0x3a6

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    iget-object v1, p0, LX/I88;->k:LX/0ad;

    sget-short v2, LX/347;->y:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v0, 0x1

    .line 2540676
    :cond_3
    if-eqz v0, :cond_4

    sget-object v0, LX/I83;->INVITE_OPTIONS:LX/I83;

    :goto_1
    sget-object v1, LX/I83;->INVITE_OVERFLOW:LX/I83;

    invoke-virtual {p1, v0, v1}, LX/I80;->a(LX/I83;LX/I83;)V

    goto :goto_0

    :cond_4
    sget-object v0, LX/I83;->INVITE:LX/I83;

    goto :goto_1

    .line 2540677
    :cond_5
    if-eqz v2, :cond_0

    .line 2540678
    sget-object v0, LX/I83;->SHARE:LX/I83;

    sget-object v1, LX/I83;->SHARE_OVERFLOW:LX/I83;

    invoke-virtual {p1, v0, v1}, LX/I80;->a(LX/I83;LX/I83;)V

    goto :goto_0
.end method
