.class public final LX/IbW;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 2593962
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 2593963
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2593964
    :goto_0
    return v1

    .line 2593965
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2593966
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_3

    .line 2593967
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 2593968
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2593969
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 2593970
    const-string v3, "ride_providers"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2593971
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2593972
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->START_ARRAY:LX/15z;

    if-ne v2, v3, :cond_2

    .line 2593973
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_ARRAY:LX/15z;

    if-eq v2, v3, :cond_2

    .line 2593974
    const/4 v3, 0x0

    .line 2593975
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v4, :cond_8

    .line 2593976
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2593977
    :goto_3
    move v2, v3

    .line 2593978
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2593979
    :cond_2
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    move v0, v0

    .line 2593980
    goto :goto_1

    .line 2593981
    :cond_3
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2593982
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2593983
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    .line 2593984
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2593985
    :cond_6
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_7

    .line 2593986
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 2593987
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2593988
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_6

    if-eqz v4, :cond_6

    .line 2593989
    const-string v5, "ride_invite"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2593990
    const/4 v4, 0x0

    .line 2593991
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v5, :cond_c

    .line 2593992
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2593993
    :goto_5
    move v2, v4

    .line 2593994
    goto :goto_4

    .line 2593995
    :cond_7
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2593996
    invoke-virtual {p1, v3, v2}, LX/186;->b(II)V

    .line 2593997
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_3

    :cond_8
    move v2, v3

    goto :goto_4

    .line 2593998
    :cond_9
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2593999
    :cond_a
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_b

    .line 2594000
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2594001
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2594002
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_a

    if-eqz v5, :cond_a

    .line 2594003
    const-string v6, "eligible_threads"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 2594004
    const/4 v5, 0x0

    .line 2594005
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v6, :cond_11

    .line 2594006
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2594007
    :goto_7
    move v2, v5

    .line 2594008
    goto :goto_6

    .line 2594009
    :cond_b
    const/4 v5, 0x1

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2594010
    invoke-virtual {p1, v4, v2}, LX/186;->b(II)V

    .line 2594011
    invoke-virtual {p1}, LX/186;->d()I

    move-result v4

    goto :goto_5

    :cond_c
    move v2, v4

    goto :goto_6

    .line 2594012
    :cond_d
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2594013
    :cond_e
    :goto_8
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_10

    .line 2594014
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 2594015
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2594016
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_e

    if-eqz v6, :cond_e

    .line 2594017
    const-string v7, "nodes"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 2594018
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2594019
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->START_ARRAY:LX/15z;

    if-ne v6, v7, :cond_f

    .line 2594020
    :goto_9
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_ARRAY:LX/15z;

    if-eq v6, v7, :cond_f

    .line 2594021
    const/4 v7, 0x0

    .line 2594022
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v6, v8, :cond_15

    .line 2594023
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2594024
    :goto_a
    move v6, v7

    .line 2594025
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_9

    .line 2594026
    :cond_f
    invoke-static {v2, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v2

    move v2, v2

    .line 2594027
    goto :goto_8

    .line 2594028
    :cond_10
    const/4 v6, 0x1

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 2594029
    invoke-virtual {p1, v5, v2}, LX/186;->b(II)V

    .line 2594030
    invoke-virtual {p1}, LX/186;->d()I

    move-result v5

    goto :goto_7

    :cond_11
    move v2, v5

    goto :goto_8

    .line 2594031
    :cond_12
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2594032
    :cond_13
    :goto_b
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_14

    .line 2594033
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 2594034
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2594035
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_13

    if-eqz v8, :cond_13

    .line 2594036
    const-string v9, "thread_key"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_12

    .line 2594037
    const/4 v8, 0x0

    .line 2594038
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v6, v9, :cond_1a

    .line 2594039
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2594040
    :goto_c
    move v6, v8

    .line 2594041
    goto :goto_b

    .line 2594042
    :cond_14
    const/4 v8, 0x1

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 2594043
    invoke-virtual {p1, v7, v6}, LX/186;->b(II)V

    .line 2594044
    invoke-virtual {p1}, LX/186;->d()I

    move-result v7

    goto :goto_a

    :cond_15
    move v6, v7

    goto :goto_b

    .line 2594045
    :cond_16
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2594046
    :cond_17
    :goto_d
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_19

    .line 2594047
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 2594048
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2594049
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_17

    if-eqz v10, :cond_17

    .line 2594050
    const-string v11, "other_user_id"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_18

    .line 2594051
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_d

    .line 2594052
    :cond_18
    const-string v11, "thread_fbid"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_16

    .line 2594053
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_d

    .line 2594054
    :cond_19
    const/4 v10, 0x2

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 2594055
    invoke-virtual {p1, v8, v9}, LX/186;->b(II)V

    .line 2594056
    const/4 v8, 0x1

    invoke-virtual {p1, v8, v6}, LX/186;->b(II)V

    .line 2594057
    invoke-virtual {p1}, LX/186;->d()I

    move-result v8

    goto :goto_c

    :cond_1a
    move v6, v8

    move v9, v8

    goto :goto_d
.end method
