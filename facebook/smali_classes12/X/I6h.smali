.class public LX/I6h;
.super LX/1a1;
.source ""


# instance fields
.field public l:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

.field public m:LX/15i;

.field public n:I

.field public o:LX/I6i;

.field private final p:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Lcom/facebook/fbui/widget/contentview/CheckedContentView;)V
    .locals 1

    .prologue
    .line 2537831
    invoke-direct {p0, p1}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 2537832
    iput-object p1, p0, LX/I6h;->l:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    .line 2537833
    new-instance v0, LX/I6g;

    invoke-direct {v0, p0}, LX/I6g;-><init>(LX/I6h;)V

    move-object v0, v0

    .line 2537834
    iput-object v0, p0, LX/I6h;->p:Landroid/view/View$OnClickListener;

    .line 2537835
    return-void
.end method


# virtual methods
.method public final a(LX/15i;ILcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;LX/I6i;)V
    .locals 4
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "bindModel"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2537836
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput-object p1, p0, LX/I6h;->m:LX/15i;

    iput p2, p0, LX/I6h;->n:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2537837
    iput-object p4, p0, LX/I6h;->o:LX/I6i;

    .line 2537838
    if-nez p2, :cond_0

    .line 2537839
    iget-object v0, p0, LX/I6h;->l:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setVisibility(I)V

    .line 2537840
    iget-object v0, p0, LX/I6h;->l:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2537841
    :goto_0
    return-void

    .line 2537842
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2537843
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    .line 2537844
    iget-object v2, p0, LX/I6h;->l:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setVisibility(I)V

    .line 2537845
    iget-object v2, p0, LX/I6h;->l:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    invoke-virtual {v2, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2537846
    iget-object v2, p0, LX/I6h;->l:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2537847
    iget-object v0, p0, LX/I6h;->l:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    invoke-virtual {p1, p2, v1, v2, v3}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v1

    invoke-static {p3, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setChecked(Z)V

    .line 2537848
    iget-object v0, p0, LX/I6h;->l:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    iget-object v1, p0, LX/I6h;->p:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method
