.class public LX/IW5;
.super Ljava/util/Observable;
.source ""

# interfaces
.implements LX/IRb;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static j:LX/0Xm;


# instance fields
.field private final a:Landroid/content/Context;

.field public b:Z

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

.field public i:LX/IW2;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2583424
    invoke-direct {p0}, Ljava/util/Observable;-><init>()V

    .line 2583425
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/IW5;->b:Z

    .line 2583426
    sget-object v0, LX/IW2;->ENDED:LX/IW2;

    iput-object v0, p0, LX/IW5;->i:LX/IW2;

    .line 2583427
    iput-object p1, p0, LX/IW5;->a:Landroid/content/Context;

    .line 2583428
    return-void
.end method

.method public static a(LX/0QB;)LX/IW5;
    .locals 4

    .prologue
    .line 2583429
    const-class v1, LX/IW5;

    monitor-enter v1

    .line 2583430
    :try_start_0
    sget-object v0, LX/IW5;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2583431
    sput-object v2, LX/IW5;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2583432
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2583433
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2583434
    new-instance p0, LX/IW5;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, LX/IW5;-><init>(Landroid/content/Context;)V

    .line 2583435
    move-object v0, p0

    .line 2583436
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2583437
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IW5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2583438
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2583439
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V
    .locals 1

    .prologue
    .line 2583417
    iget-object v0, p0, LX/IW5;->i:LX/IW2;

    invoke-virtual {p0, p1, v0}, LX/IW5;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;LX/IW2;)V

    .line 2583418
    return-void
.end method

.method public final a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;LX/IW2;)V
    .locals 1

    .prologue
    .line 2583419
    iput-object p2, p0, LX/IW5;->i:LX/IW2;

    .line 2583420
    iget-object v0, p0, LX/IW5;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-ne p1, v0, :cond_0

    .line 2583421
    :goto_0
    return-void

    .line 2583422
    :cond_0
    iput-object p1, p0, LX/IW5;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 2583423
    invoke-virtual {p0}, LX/IW5;->i()V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V
    .locals 4

    .prologue
    .line 2583405
    iget-object v0, p0, LX/IW5;->a:Landroid/content/Context;

    instance-of v0, v0, Landroid/app/Activity;

    if-nez v0, :cond_1

    .line 2583406
    :cond_0
    :goto_0
    return-void

    .line 2583407
    :cond_1
    iget-object v0, p0, LX/IW5;->a:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 2583408
    iget-object v1, p0, LX/IW5;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-object v1, v1

    .line 2583409
    if-ne p1, v1, :cond_0

    .line 2583410
    invoke-virtual {p2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CAN_JOIN:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    const/4 v1, 0x1

    .line 2583411
    :goto_1
    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    if-eqz v1, :cond_4

    .line 2583412
    invoke-virtual {v0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_0

    .line 2583413
    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    .line 2583414
    :cond_4
    if-eqz v1, :cond_5

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->T()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {p2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->T()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;

    move-result-object v1

    if-nez v1, :cond_5

    .line 2583415
    invoke-virtual {v0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_0

    .line 2583416
    :cond_5
    invoke-virtual {p0, p2}, LX/IW5;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 2583402
    invoke-virtual {p0}, LX/IW5;->setChanged()V

    .line 2583403
    new-instance v0, LX/IW4;

    invoke-direct {v0, p1}, LX/IW4;-><init>(Z)V

    invoke-virtual {p0, v0}, LX/IW5;->notifyObservers(Ljava/lang/Object;)V

    .line 2583404
    return-void
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 2583399
    invoke-virtual {p0}, LX/IW5;->setChanged()V

    .line 2583400
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/IW5;->notifyObservers(Ljava/lang/Object;)V

    .line 2583401
    return-void
.end method
