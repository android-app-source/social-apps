.class public final LX/HuD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/HuG;


# direct methods
.method public constructor <init>(LX/HuG;)V
    .locals 0

    .prologue
    .line 2517259
    iput-object p1, p0, LX/HuD;->a:LX/HuG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x5fb0eb0

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2517260
    iget-object v1, p0, LX/HuD;->a:LX/HuG;

    const/4 p1, 0x1

    .line 2517261
    iget-object v3, v1, LX/HuG;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0il;

    move-object v4, v3

    .line 2517262
    check-cast v4, LX/0im;

    invoke-interface {v4}, LX/0im;->c()LX/0jJ;

    move-result-object v4

    sget-object p0, LX/HuG;->b:LX/0jK;

    invoke-virtual {v4, p0}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v4

    check-cast v4, LX/0jL;

    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/0j0;

    check-cast p0, LX/0j8;

    invoke-interface {p0}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object p0

    invoke-static {p0}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a(Lcom/facebook/ipc/composer/model/ComposerLocationInfo;)LX/5RO;

    move-result-object p0

    .line 2517263
    iput-boolean p1, p0, LX/5RO;->b:Z

    .line 2517264
    move-object p0, p0

    .line 2517265
    invoke-virtual {p0}, LX/5RO;->b()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object p0

    invoke-virtual {v4, p0}, LX/0jL;->a(Lcom/facebook/ipc/composer/model/ComposerLocationInfo;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0jL;

    invoke-virtual {v4}, LX/0jL;->a()V

    .line 2517266
    iget-boolean v4, v1, LX/HuG;->t:Z

    if-nez v4, :cond_0

    .line 2517267
    iput-boolean p1, v1, LX/HuG;->t:Z

    .line 2517268
    iget-object v4, v1, LX/HuG;->g:LX/0gd;

    sget-object p0, LX/0ge;->CHECKIN_PREVIEW_DISMISSED:LX/0ge;

    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0j0;

    invoke-interface {v3}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, p0, v3}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 2517269
    :cond_0
    const v1, -0x3446b5ce    # -2.4286308E7f

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
