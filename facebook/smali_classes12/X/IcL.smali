.class public final LX/IcL;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypesInfoQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/IdA;

.field public final synthetic b:LX/IcM;


# direct methods
.method public constructor <init>(LX/IcM;LX/IdA;)V
    .locals 0

    .prologue
    .line 2595484
    iput-object p1, p0, LX/IcL;->b:LX/IcM;

    iput-object p2, p0, LX/IcL;->a:LX/IdA;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2595446
    iget-object v0, p0, LX/IcL;->b:LX/IcM;

    iget-object v0, v0, LX/IcM;->a:LX/03V;

    const-string v1, "RideTypeInfoHelper"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2595447
    iget-object v0, p0, LX/IcL;->a:LX/IdA;

    .line 2595448
    iget-object v1, v0, LX/IdA;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v1, v1, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->B:Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;

    iget-object p0, v0, LX/IdA;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    const p1, 0x7f080039

    invoke-virtual {p0, p1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->a(Ljava/lang/String;)V

    .line 2595449
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 2595450
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2595451
    if-eqz p1, :cond_0

    .line 2595452
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2595453
    if-eqz v0, :cond_0

    .line 2595454
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2595455
    check-cast v0, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypesInfoQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypesInfoQueryModel;->a()Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypesInfoQueryModel$MessengerCommerceModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2595456
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2595457
    check-cast v0, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypesInfoQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypesInfoQueryModel;->a()Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypesInfoQueryModel$MessengerCommerceModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypesInfoQueryModel$MessengerCommerceModel;->a()LX/2uF;

    move-result-object v0

    invoke-virtual {v0}, LX/39O;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2595458
    :cond_0
    iget-object v0, p0, LX/IcL;->b:LX/IcM;

    iget-object v0, v0, LX/IcM;->a:LX/03V;

    const-string v1, "RideService"

    const-string v2, "GraphQL return invalid results"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2595459
    :goto_0
    return-void

    .line 2595460
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2595461
    check-cast v0, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypesInfoQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypesInfoQueryModel;->a()Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypesInfoQueryModel$MessengerCommerceModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypesInfoQueryModel$MessengerCommerceModel;->a()LX/2uF;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2595462
    iget-object v2, p0, LX/IcL;->a:LX/IdA;

    const v5, 0x2acf7ada

    const/4 p1, 0x0

    .line 2595463
    invoke-virtual {v1, v0, p1}, LX/15i;->g(II)I

    move-result v3

    if-nez v3, :cond_3

    const/4 v3, 0x1

    :goto_1
    if-eqz v3, :cond_5

    .line 2595464
    const/4 v3, 0x0

    .line 2595465
    :goto_2
    move-object v0, v3

    .line 2595466
    iget-object v1, v2, LX/IdA;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    invoke-virtual {v1}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->c()Z

    move-result v1

    if-eqz v1, :cond_9

    iget-object v1, v2, LX/IdA;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v1, v1, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->o:LX/Ib3;

    iget-object v3, v2, LX/IdA;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v3, v3, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->w:Lcom/facebook/messaging/business/ride/utils/RideServiceParams;

    .line 2595467
    iget-object v4, v3, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->e:Ljava/lang/String;

    move-object v3, v4

    .line 2595468
    invoke-static {v1, v3}, LX/Ib3;->b(LX/Ib3;Ljava/lang/String;)LX/Ib4;

    move-result-object v4

    iget-object v4, v4, LX/Ib4;->b:Ljava/lang/String;

    move-object v1, v4

    .line 2595469
    :goto_3
    iget-object v3, v2, LX/IdA;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v3, v3, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->B:Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;

    invoke-virtual {v3, v1, v0}, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->a(Ljava/lang/String;LX/0Px;)V

    .line 2595470
    iget-object v1, v2, LX/IdA;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    invoke-virtual {v1}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->c()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2595471
    iget-object v1, v2, LX/IdA;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v1, v1, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->B:Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;

    invoke-virtual {v1}, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->a()V

    .line 2595472
    :cond_2
    iget-object v1, v2, LX/IdA;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    invoke-static {v1}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->x(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)V

    .line 2595473
    goto :goto_0

    .line 2595474
    :cond_3
    invoke-virtual {v1, v0, p1}, LX/15i;->g(II)I

    move-result v3

    invoke-static {v1, v3, p1, v5}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v3

    .line 2595475
    if-eqz v3, :cond_4

    invoke-static {v3}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v3

    :goto_4
    invoke-virtual {v3}, LX/39O;->a()Z

    move-result v3

    goto :goto_1

    :cond_4
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v3

    goto :goto_4

    .line 2595476
    :cond_5
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 2595477
    invoke-virtual {v1, v0, p1}, LX/15i;->g(II)I

    move-result v3

    invoke-static {v1, v3, p1, v5}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v3

    if-eqz v3, :cond_7

    invoke-static {v3}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v3

    .line 2595478
    :goto_5
    invoke-virtual {v3}, LX/3Sa;->e()LX/3Sh;

    move-result-object v3

    :cond_6
    :goto_6
    invoke-interface {v3}, LX/2sN;->a()Z

    move-result v5

    if-eqz v5, :cond_8

    invoke-interface {v3}, LX/2sN;->b()LX/1vs;

    move-result-object v5

    iget-object v6, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    .line 2595479
    const-class p0, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;

    invoke-virtual {v6, v5, p1, p0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object p0

    if-eqz p0, :cond_6

    .line 2595480
    const-class p0, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;

    invoke-virtual {v6, v5, p1, p0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_6

    .line 2595481
    :cond_7
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v3

    goto :goto_5

    .line 2595482
    :cond_8
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    goto/16 :goto_2

    .line 2595483
    :cond_9
    const/4 v1, 0x0

    goto :goto_3
.end method
