.class public final LX/IBs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/47M;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2547820
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/content/Intent;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2547821
    new-instance v0, Lcom/facebook/reaction/ReactionQueryParams;

    invoke-direct {v0}, Lcom/facebook/reaction/ReactionQueryParams;-><init>()V

    const-string v1, "com.facebook.katana.profile.id"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 2547822
    iput-object v1, v0, Lcom/facebook/reaction/ReactionQueryParams;->t:Ljava/lang/Long;

    .line 2547823
    move-object v0, v0

    .line 2547824
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "ANDROID_EVENT_PERMALINK"

    invoke-static {p1, v1, v0, v2}, Lcom/facebook/reaction/activity/ReactionDialogActivity;->a(Landroid/content/Context;Landroid/content/Intent;Lcom/facebook/reaction/ReactionQueryParams;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method
