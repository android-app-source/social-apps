.class public final LX/ItZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6cX;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6cX",
        "<",
        "Ljava/util/List",
        "<",
        "LX/765;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:J

.field public final synthetic b:LX/Itc;

.field private c:LX/6me;


# direct methods
.method public constructor <init>(LX/Itc;J)V
    .locals 0

    .prologue
    .line 2623529
    iput-object p1, p0, LX/ItZ;->b:LX/Itc;

    iput-wide p2, p0, LX/ItZ;->a:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a([B)V
    .locals 10

    .prologue
    .line 2623486
    :try_start_0
    iget-object v0, p0, LX/ItZ;->b:LX/Itc;

    invoke-static {v0, p1}, LX/Itc;->a(LX/Itc;[B)LX/1su;

    move-result-object v0

    .line 2623487
    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 2623488
    invoke-virtual {v0}, LX/1su;->r()LX/1sv;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    .line 2623489
    :cond_0
    :goto_0
    invoke-virtual {v0}, LX/1su;->f()LX/1sw;

    move-result-object v6

    .line 2623490
    iget-byte v8, v6, LX/1sw;->b:B

    if-eqz v8, :cond_6

    .line 2623491
    iget-short v8, v6, LX/1sw;->c:S

    packed-switch v8, :pswitch_data_0

    .line 2623492
    iget-byte v6, v6, LX/1sw;->b:B

    invoke-static {v0, v6}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2623493
    :pswitch_0
    iget-byte v8, v6, LX/1sw;->b:B

    const/16 v9, 0xa

    if-ne v8, v9, :cond_1

    .line 2623494
    invoke-virtual {v0}, LX/1su;->n()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    goto :goto_0

    .line 2623495
    :cond_1
    iget-byte v6, v6, LX/1sw;->b:B

    invoke-static {v0, v6}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2623496
    :pswitch_1
    iget-byte v8, v6, LX/1sw;->b:B

    const/16 v9, 0xb

    if-ne v8, v9, :cond_2

    .line 2623497
    invoke-virtual {v0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 2623498
    :cond_2
    iget-byte v6, v6, LX/1sw;->b:B

    invoke-static {v0, v6}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2623499
    :pswitch_2
    iget-byte v8, v6, LX/1sw;->b:B

    const/16 v9, 0xf

    if-ne v8, v9, :cond_4

    .line 2623500
    invoke-virtual {v0}, LX/1su;->h()LX/1u3;

    move-result-object v8

    .line 2623501
    new-instance v3, Ljava/util/ArrayList;

    iget v6, v8, LX/1u3;->b:I

    invoke-static {v7, v6}, Ljava/lang/Math;->max(II)I

    move-result v6

    invoke-direct {v3, v6}, Ljava/util/ArrayList;-><init>(I)V

    move v6, v7

    .line 2623502
    :goto_1
    iget v9, v8, LX/1u3;->b:I

    if-gez v9, :cond_3

    invoke-static {}, LX/1su;->t()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 2623503
    :goto_2
    invoke-static {v0}, LX/6md;->b(LX/1su;)LX/6md;

    move-result-object v9

    .line 2623504
    invoke-interface {v3, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2623505
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 2623506
    :cond_3
    iget v9, v8, LX/1u3;->b:I

    if-ge v6, v9, :cond_0

    goto :goto_2

    .line 2623507
    :cond_4
    iget-byte v6, v6, LX/1sw;->b:B

    invoke-static {v0, v6}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2623508
    :pswitch_3
    iget-byte v8, v6, LX/1sw;->b:B

    const/4 v9, 0x2

    if-ne v8, v9, :cond_5

    .line 2623509
    invoke-virtual {v0}, LX/1su;->j()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto :goto_0

    .line 2623510
    :cond_5
    iget-byte v6, v6, LX/1sw;->b:B

    invoke-static {v0, v6}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2623511
    :cond_6
    invoke-virtual {v0}, LX/1su;->e()V

    .line 2623512
    new-instance v6, LX/6me;

    invoke-direct {v6, v5, v4, v3, v2}, LX/6me;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;)V

    .line 2623513
    move-object v0, v6

    .line 2623514
    iput-object v0, p0, LX/ItZ;->c:LX/6me;
    :try_end_0
    .catch LX/7H0; {:try_start_0 .. :try_end_0} :catch_0

    .line 2623515
    :goto_3
    return-void

    .line 2623516
    :catch_0
    sget-object v0, LX/Itc;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Failed to read SendMessageResponseBatch"

    invoke-static {v0, v1}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v0

    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    .line 2623517
    iget-object v1, p0, LX/ItZ;->b:LX/Itc;

    iget-object v1, v1, LX/Itc;->b:LX/03V;

    invoke-virtual {v1, v0}, LX/03V;->a(LX/0VG;)V

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2623525
    iget-object v1, p0, LX/ItZ;->c:LX/6me;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/ItZ;->c:LX/6me;

    iget-object v1, v1, LX/6me;->responses:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2623526
    :cond_0
    :goto_0
    return v0

    .line 2623527
    :cond_1
    iget-object v1, p0, LX/ItZ;->c:LX/6me;

    iget-object v1, v1, LX/6me;->batchId:Ljava/lang/Long;

    .line 2623528
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-wide v4, p0, LX/ItZ;->a:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2623518
    iget-object v0, p0, LX/ItZ;->b:LX/Itc;

    iget-object v1, p0, LX/ItZ;->c:LX/6me;

    .line 2623519
    iget-object v2, v1, LX/6me;->responses:Ljava/util/List;

    .line 2623520
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2623521
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6md;

    .line 2623522
    invoke-static {v0, v2}, LX/Itc;->a$redex0(LX/Itc;LX/6md;)LX/765;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2623523
    :cond_0
    move-object v0, v3

    .line 2623524
    return-object v0
.end method
