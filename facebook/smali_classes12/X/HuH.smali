.class public LX/HuH;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/HuG;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2517341
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 2517342
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/HqR;)LX/HuG;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData:",
            "Ljava/lang/Object;",
            ":",
            "LX/0j0;",
            ":",
            "LX/0j8;",
            "DerivedData::",
            "LX/5RE;",
            "Mutation:",
            "Ljava/lang/Object;",
            ":",
            "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
            ":",
            "Lcom/facebook/ipc/composer/model/ComposerLocationInfo$SetsLocationInfo",
            "<TMutation;>;Services:",
            "Ljava/lang/Object;",
            ":",
            "LX/0il",
            "<TModelData;>;:",
            "LX/0ik",
            "<TDerivedData;>;:",
            "LX/0im",
            "<TMutation;>;>(TServices;",
            "Lcom/facebook/composer/location/feedattachment/CheckinPreviewAttachment$Callback;",
            ")",
            "LX/HuG",
            "<TModelData;TDerivedData;TMutation;TServices;>;"
        }
    .end annotation

    .prologue
    .line 2517343
    new-instance v0, LX/HuG;

    move-object v1, p1

    check-cast v1, LX/0il;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {p0}, LX/0gd;->a(LX/0QB;)LX/0gd;

    move-result-object v4

    check-cast v4, LX/0gd;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-static {p0}, Lcom/facebook/maps/rows/MapPartDefinition;->a(LX/0QB;)Lcom/facebook/maps/rows/MapPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/maps/rows/MapPartDefinition;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v7

    check-cast v7, LX/0tX;

    invoke-static {p0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v8

    check-cast v8, LX/0rq;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v9

    check-cast v9, Landroid/content/res/Resources;

    invoke-static {p0}, Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;->a(LX/0QB;)Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;

    invoke-static {p0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v11

    check-cast v11, LX/0wM;

    invoke-static {p0}, LX/3BL;->a(LX/0QB;)LX/3BL;

    move-result-object v12

    check-cast v12, LX/3BL;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v13

    check-cast v13, Ljava/util/concurrent/Executor;

    move-object/from16 v2, p2

    invoke-direct/range {v0 .. v13}, LX/HuG;-><init>(LX/0il;LX/HqR;LX/03V;LX/0gd;LX/0ad;Lcom/facebook/maps/rows/MapPartDefinition;LX/0tX;LX/0rq;Landroid/content/res/Resources;Lcom/facebook/checkin/rows/BaseCheckinStoryPartDefinition;LX/0wM;LX/3BL;Ljava/util/concurrent/Executor;)V

    .line 2517344
    return-object v0
.end method
