.class public LX/Hkr;
.super Landroid/os/AsyncTask;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "[",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Landroid/widget/ImageView;

.field private final d:LX/Hl4;

.field public e:LX/HjJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    const-class v0, LX/Hkr;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Hkr;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/Hl4;)V
    .locals 1

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    invoke-virtual {p1}, LX/Hl4;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LX/Hkr;->b:Landroid/content/Context;

    iput-object p1, p0, LX/Hkr;->d:LX/Hl4;

    const/4 v0, 0x0

    iput-object v0, p0, LX/Hkr;->c:Landroid/widget/ImageView;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p1, p0, LX/Hkr;->b:Landroid/content/Context;

    iput-object v0, p0, LX/Hkr;->d:LX/Hl4;

    iput-object v0, p0, LX/Hkr;->c:Landroid/widget/ImageView;

    return-void
.end method

.method public constructor <init>(Landroid/widget/ImageView;)V
    .locals 1

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    invoke-virtual {p1}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LX/Hkr;->b:Landroid/content/Context;

    const/4 v0, 0x0

    iput-object v0, p0, LX/Hkr;->d:LX/Hl4;

    iput-object p1, p0, LX/Hkr;->c:Landroid/widget/ImageView;

    return-void
.end method


# virtual methods
.method public final doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 10

    check-cast p1, [Ljava/lang/String;

    const/4 v8, 0x0

    const/4 v0, 0x0

    aget-object v4, p1, v8

    :try_start_0
    iget-object v1, p0, LX/Hkr;->b:Landroid/content/Context;

    new-instance v2, Ljava/io/File;

    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v3

    const-string v5, "%d.png"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v3, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    move-object v2, v2
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v2, :cond_0

    :try_start_1
    new-instance v1, LX/HkR;

    iget-object v3, p0, LX/Hkr;->b:Landroid/content/Context;

    invoke-direct {v1, v3}, LX/HkR;-><init>(Landroid/content/Context;)V

    const/4 v3, 0x0

    invoke-virtual {v1, v4, v3}, LX/HkR;->a(Ljava/lang/String;LX/Hkc;)LX/Hkb;

    move-result-object v1

    iget-object v3, v1, LX/Hkb;->d:[B

    move-object v1, v3

    const/4 v3, 0x0

    array-length v5, v1

    invoke-static {v1, v3, v5}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v2

    iget-object v1, p0, LX/Hkr;->b:Landroid/content/Context;

    new-instance v3, Ljava/io/File;

    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v5

    const-string v6, "%d.png"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v7, v9

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v5, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    :try_start_2
    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v6, 0x64

    invoke-virtual {v2, v3, v6, v5}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    invoke-virtual {v5}, Ljava/io/FileOutputStream;->flush()V

    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_0
    :goto_0
    move-object v1, v2

    :try_start_3
    iget-object v2, p0, LX/Hkr;->d:LX/Hl4;
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2

    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    :try_start_4
    new-instance v2, LX/Hkx;

    invoke-direct {v2, v1}, LX/Hkx;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    const/high16 v5, 0x42200000    # 40.0f

    div-float/2addr v3, v5

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    iget-object v5, v2, LX/Hkx;->f:LX/Hku;

    iget-object v6, v2, LX/Hkx;->d:Landroid/graphics/Bitmap;

    int-to-float v7, v3

    invoke-virtual {v5, v6, v7}, LX/Hku;->a(Landroid/graphics/Bitmap;F)Landroid/graphics/Bitmap;

    move-result-object v5

    iput-object v5, v2, LX/Hkx;->e:Landroid/graphics/Bitmap;

    iget-object v3, v2, LX/Hkx;->e:Landroid/graphics/Bitmap;

    move-object v0, v3
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_3

    :cond_1
    :goto_1
    const/4 v2, 0x2

    new-array v2, v2, [Landroid/graphics/Bitmap;

    aput-object v1, v2, v8

    const/4 v1, 0x1

    aput-object v0, v2, v1

    return-object v2

    :catch_0
    move-exception v1

    move-object v3, v1

    move-object v2, v0

    move-object v1, v0

    :goto_2
    sget-object v5, LX/Hkr;->a:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Error downloading image: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-static {v3, v0}, LX/Hkg;->a(Ljava/lang/Throwable;Ljava/lang/String;)LX/Hkg;

    move-result-object v0

    invoke-static {v0}, LX/Hkh;->a(LX/Hkg;)V

    move-object v0, v1

    move-object v1, v2

    goto :goto_1

    :catch_1
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    goto :goto_2

    :catch_2
    move-exception v2

    move-object v3, v2

    move-object v2, v1

    move-object v1, v0

    goto :goto_2

    :catch_3
    move-exception v2

    move-object v3, v2

    move-object v2, v1

    goto :goto_2

    :catch_4
    goto :goto_0
.end method

.method public final onPostExecute(Ljava/lang/Object;)V
    .locals 3

    check-cast p1, [Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    iget-object v0, p0, LX/Hkr;->c:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Hkr;->c:Landroid/widget/ImageView;

    aget-object v1, p1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_0
    iget-object v0, p0, LX/Hkr;->d:LX/Hl4;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Hkr;->d:LX/Hl4;

    aget-object v1, p1, v2

    const/4 v2, 0x1

    aget-object v2, p1, v2

    invoke-virtual {v0, v1, v2}, LX/Hl4;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    :cond_1
    iget-object v0, p0, LX/Hkr;->e:LX/HjJ;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/Hkr;->e:LX/HjJ;

    invoke-interface {v0}, LX/HjJ;->a()V

    :cond_2
    return-void
.end method
