.class public LX/J1p;
.super LX/48b;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/facebook/auth/viewercontext/ViewerContext;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/auth/viewercontext/ViewerContext;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2639411
    invoke-direct {p0}, LX/48b;-><init>()V

    .line 2639412
    iput-object p1, p0, LX/J1p;->a:Landroid/content/Context;

    .line 2639413
    iput-object p2, p0, LX/J1p;->b:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2639414
    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/facebook/auth/viewercontext/ViewerContext;Landroid/os/Bundle;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 2639400
    invoke-static {}, Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;->newBuilder()LX/J2t;

    move-result-object v0

    const-string v1, "product_type"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/6xh;->forValue(Ljava/lang/String;)LX/6xh;

    move-result-object v1

    .line 2639401
    iput-object v1, v0, LX/J2t;->c:LX/6xh;

    .line 2639402
    move-object v0, v0

    .line 2639403
    const-string v1, "product_id"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2639404
    iput-object v1, v0, LX/J2t;->d:Ljava/lang/String;

    .line 2639405
    move-object v0, v0

    .line 2639406
    invoke-virtual {v0}, LX/J2t;->a()Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/payments/receipt/model/ReceiptCommonParams;->a(Lcom/facebook/payments/receipt/model/ReceiptComponentControllerParams;)LX/J2p;

    move-result-object v0

    invoke-virtual {v0}, LX/J2p;->a()Lcom/facebook/payments/receipt/model/ReceiptCommonParams;

    move-result-object v0

    .line 2639407
    invoke-static {p0, p1, v0}, Lcom/facebook/payments/receipt/PaymentsReceiptActivity;->a(Landroid/content/Context;Lcom/facebook/auth/viewercontext/ViewerContext;Lcom/facebook/payments/receipt/model/ReceiptCommonParams;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/J1p;
    .locals 3

    .prologue
    .line 2639409
    new-instance v2, LX/J1p;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0eQ;->b(LX/0QB;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    check-cast v1, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-direct {v2, v0, v1}, LX/J1p;-><init>(Landroid/content/Context;Lcom/facebook/auth/viewercontext/ViewerContext;)V

    .line 2639410
    return-object v2
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 2639408
    iget-object v0, p0, LX/J1p;->a:Landroid/content/Context;

    iget-object v1, p0, LX/J1p;->b:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/J1p;->a(Landroid/content/Context;Lcom/facebook/auth/viewercontext/ViewerContext;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method
