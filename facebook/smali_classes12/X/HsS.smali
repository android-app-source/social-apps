.class public LX/HsS;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2514512
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/uicontrib/datepicker/Date;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2514503
    if-nez p0, :cond_1

    .line 2514504
    :cond_0
    :goto_0
    return v0

    .line 2514505
    :cond_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 2514506
    invoke-virtual {p0}, Lcom/facebook/uicontrib/datepicker/Date;->a()I

    move-result v3

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v4

    if-eq v3, v4, :cond_2

    move v0, v1

    .line 2514507
    goto :goto_0

    .line 2514508
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/uicontrib/datepicker/Date;->b()Ljava/lang/Integer;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual {p0}, Lcom/facebook/uicontrib/datepicker/Date;->b()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v4, 0x2

    invoke-virtual {v2, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    if-eq v3, v4, :cond_4

    :cond_3
    move v0, v1

    .line 2514509
    goto :goto_0

    .line 2514510
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/uicontrib/datepicker/Date;->c()Ljava/lang/Integer;

    move-result-object v3

    if-eqz v3, :cond_5

    invoke-virtual {p0}, Lcom/facebook/uicontrib/datepicker/Date;->c()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v4, 0x5

    invoke-virtual {v2, v4}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-eq v3, v2, :cond_0

    :cond_5
    move v0, v1

    .line 2514511
    goto :goto_0
.end method
