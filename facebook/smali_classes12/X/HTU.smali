.class public final LX/HTU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsGraphQLModels$PoliticalEndorsementUpdateStatusMutationModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/HTX;


# direct methods
.method public constructor <init>(LX/HTX;)V
    .locals 0

    .prologue
    .line 2469539
    iput-object p1, p0, LX/HTU;->a:LX/HTX;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2469517
    new-instance v0, LX/HTb;

    invoke-direct {v0}, LX/HTb;-><init>()V

    move-object v0, v0

    .line 2469518
    new-instance v1, LX/4IU;

    invoke-direct {v1}, LX/4IU;-><init>()V

    iget-object v2, p0, LX/HTU;->a:LX/HTX;

    iget-object v2, v2, LX/HTX;->l:Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;

    iget-object v2, v2, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;->i:Ljava/lang/String;

    .line 2469519
    const-string v3, "page_id"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2469520
    move-object v1, v1

    .line 2469521
    iget-object v2, p0, LX/HTU;->a:LX/HTX;

    iget-object v2, v2, LX/HTX;->l:Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;

    iget-object v2, v2, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;->f:LX/0SI;

    invoke-interface {v2}, LX/0SI;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v2

    .line 2469522
    iget-object v3, v2, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2469523
    const-string v3, "actor_id"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2469524
    move-object v1, v1

    .line 2469525
    iget-object v2, p0, LX/HTU;->a:LX/HTX;

    iget-object v2, v2, LX/HTX;->u:Ljava/lang/String;

    .line 2469526
    const-string v3, "endorsement_id"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2469527
    move-object v1, v1

    .line 2469528
    const-string v2, "REJECT"

    .line 2469529
    const-string v3, "action_type"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2469530
    move-object v1, v1

    .line 2469531
    iget-object v2, v0, LX/0gW;->h:Ljava/lang/String;

    move-object v2, v2

    .line 2469532
    const-string v3, "client_mutation_id"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2469533
    move-object v1, v1

    .line 2469534
    const-string v2, "input"

    invoke-virtual {v0, v2, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2469535
    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 2469536
    iget-object v1, p0, LX/HTU;->a:LX/HTX;

    iget-object v1, v1, LX/HTX;->l:Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;

    iget-object v1, v1, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;->f:LX/0SI;

    invoke-interface {v1}, LX/0SI;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    .line 2469537
    iput-object v1, v0, LX/399;->e:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2469538
    iget-object v1, p0, LX/HTU;->a:LX/HTX;

    iget-object v1, v1, LX/HTX;->l:Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;

    iget-object v1, v1, Lcom/facebook/pages/fb4a/politics/PagePoliticalEndorsementsAdapter;->e:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
