.class public final LX/HXh;
.super LX/D2y;
.source ""


# instance fields
.field public final synthetic c:LX/HXi;


# direct methods
.method public constructor <init>(LX/HXi;)V
    .locals 0

    .prologue
    .line 2479214
    iput-object p1, p0, LX/HXh;->c:LX/HXi;

    invoke-direct {p0, p1}, LX/D2y;-><init>(LX/D2z;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Menu;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2479215
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2479216
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 2479217
    instance-of v1, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_2

    .line 2479218
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2479219
    iget-object v1, p0, LX/HXh;->c:LX/HXi;

    invoke-static {v1, v0}, LX/HXi;->e(LX/HXi;Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2479220
    const v1, 0x7f081579

    invoke-interface {p1, v1}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 2479221
    const v2, 0x7f02092a

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 2479222
    new-instance v2, LX/HXg;

    invoke-direct {v2, p0, v0, p3}, LX/HXg;-><init>(LX/HXh;Lcom/facebook/graphql/model/GraphQLStory;Landroid/view/View;)V

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2479223
    :cond_0
    iget-object v1, p0, LX/HXh;->c:LX/HXi;

    .line 2479224
    const/4 v2, 0x0

    .line 2479225
    iget-object v3, v1, LX/HXi;->i:LX/0Uh;

    const/16 v4, 0x233

    invoke-virtual {v3, v4, v2}, LX/0Uh;->a(IZ)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2479226
    iget-object v3, v1, LX/HXi;->m:Landroid/support/v4/app/Fragment;

    if-eqz v3, :cond_7

    const-class v3, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;

    iget-object v4, v1, LX/HXi;->m:Landroid/support/v4/app/Fragment;

    invoke-virtual {v3, v4}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    const/4 v3, 0x1

    :goto_0
    move v3, v3

    .line 2479227
    if-eqz v3, :cond_1

    .line 2479228
    iget-object v3, v1, LX/D2z;->a:LX/5SB;

    invoke-virtual {v3}, LX/5SB;->e()LX/0am;

    move-result-object v3

    invoke-virtual {v3}, LX/0am;->isPresent()Z

    move-result v3

    if-eqz v3, :cond_8

    iget-object v3, v1, LX/D2z;->a:LX/5SB;

    invoke-virtual {v3}, LX/5SB;->e()LX/0am;

    move-result-object v3

    invoke-virtual {v3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string v4, "page_only"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    const/4 v3, 0x1

    :goto_1
    move v3, v3

    .line 2479229
    if-eqz v3, :cond_1

    iget-object v3, v1, LX/D2z;->a:LX/5SB;

    invoke-virtual {v3}, LX/5SB;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v2, 0x1

    :cond_1
    move v2, v2

    .line 2479230
    if-nez v2, :cond_3

    .line 2479231
    :cond_2
    :goto_2
    invoke-super {p0, p1, p2, p3}, LX/D2y;->a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V

    .line 2479232
    return-void

    .line 2479233
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v1, LX/HXi;->n:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    move v2, v2

    .line 2479234
    if-eqz v2, :cond_5

    .line 2479235
    const v2, 0x7f081841

    invoke-interface {p1, v2}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 2479236
    instance-of v2, v3, LX/3Ai;

    if-eqz v2, :cond_4

    move-object v2, v3

    .line 2479237
    check-cast v2, LX/3Ai;

    const v4, 0x7f081843

    invoke-virtual {v2, v4}, LX/3Ai;->a(I)Landroid/view/MenuItem;

    .line 2479238
    :cond_4
    const v2, 0x7f020995

    move v2, v2

    .line 2479239
    invoke-interface {v3, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 2479240
    new-instance v2, LX/HXb;

    invoke-direct {v2, v1, v0, p3}, LX/HXb;-><init>(LX/HXi;Lcom/facebook/graphql/model/GraphQLStory;Landroid/view/View;)V

    invoke-interface {v3, v2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_2

    .line 2479241
    :cond_5
    const v2, 0x7f081840

    invoke-interface {p1, v2}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 2479242
    instance-of v2, v3, LX/3Ai;

    if-eqz v2, :cond_6

    move-object v2, v3

    .line 2479243
    check-cast v2, LX/3Ai;

    const v4, 0x7f081842

    invoke-virtual {v2, v4}, LX/3Ai;->a(I)Landroid/view/MenuItem;

    .line 2479244
    :cond_6
    const v2, 0x7f020995

    move v2, v2

    .line 2479245
    invoke-interface {v3, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 2479246
    new-instance v2, LX/HXc;

    invoke-direct {v2, v1, v0, p3}, LX/HXc;-><init>(LX/HXi;Lcom/facebook/graphql/model/GraphQLStory;Landroid/view/View;)V

    invoke-interface {v3, v2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_2

    :cond_7
    const/4 v3, 0x0

    goto/16 :goto_0

    :cond_8
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2479247
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2479248
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2479249
    invoke-super {p0, p1}, LX/D2y;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/HXh;->c:LX/HXi;

    invoke-static {v1, v0}, LX/HXi;->e(LX/HXi;Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
