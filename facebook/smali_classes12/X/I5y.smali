.class public final LX/I5y;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 2536827
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 2536828
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2536829
    :goto_0
    return v1

    .line 2536830
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2536831
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_4

    .line 2536832
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2536833
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2536834
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 2536835
    const-string v4, "__type__"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "__typename"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2536836
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v2

    goto :goto_1

    .line 2536837
    :cond_3
    const-string v4, "admin_info"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2536838
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2536839
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v5, :cond_a

    .line 2536840
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2536841
    :goto_2
    move v0, v3

    .line 2536842
    goto :goto_1

    .line 2536843
    :cond_4
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2536844
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2536845
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2536846
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 2536847
    :cond_6
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_8

    .line 2536848
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 2536849
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2536850
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_6

    if-eqz v6, :cond_6

    .line 2536851
    const-string v7, "can_viewer_promote"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 2536852
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v5, v0

    move v0, v4

    goto :goto_3

    .line 2536853
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 2536854
    :cond_8
    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2536855
    if-eqz v0, :cond_9

    .line 2536856
    invoke-virtual {p1, v3, v5}, LX/186;->a(IZ)V

    .line 2536857
    :cond_9
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_a
    move v0, v3

    move v5, v3

    goto :goto_3
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2536858
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2536859
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 2536860
    if-eqz v0, :cond_0

    .line 2536861
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2536862
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2536863
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2536864
    if-eqz v0, :cond_2

    .line 2536865
    const-string v1, "admin_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2536866
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2536867
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->b(II)Z

    move-result v1

    .line 2536868
    if-eqz v1, :cond_1

    .line 2536869
    const-string p1, "can_viewer_promote"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2536870
    invoke-virtual {p2, v1}, LX/0nX;->a(Z)V

    .line 2536871
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2536872
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2536873
    return-void
.end method
