.class public final LX/J3z;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel;",
        ">;",
        "LX/J4L;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/J40;


# direct methods
.method public constructor <init>(LX/J40;)V
    .locals 0

    .prologue
    .line 2643467
    iput-object p1, p0, LX/J3z;->a:LX/J40;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2643468
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2643469
    iget-object v0, p0, LX/J3z;->a:LX/J40;

    iget-object v0, v0, LX/J40;->b:LX/J45;

    iget-object v0, v0, LX/J45;->f:LX/J4N;

    const/4 v3, 0x0

    .line 2643470
    if-eqz p1, :cond_0

    .line 2643471
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2643472
    if-eqz v1, :cond_0

    .line 2643473
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2643474
    check-cast v1, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel;

    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2643475
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2643476
    check-cast v1, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel;

    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2643477
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2643478
    check-cast v1, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel;

    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    move-object v1, v3

    .line 2643479
    :goto_0
    move-object v0, v1

    .line 2643480
    return-object v0

    .line 2643481
    :cond_1
    new-instance v2, LX/J4L;

    sget-object v1, LX/J4K;->PROFILE_STEP:LX/J4K;

    invoke-direct {v2, v1}, LX/J4L;-><init>(LX/J4K;)V

    .line 2643482
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2643483
    check-cast v1, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel;

    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel;

    move-result-object v1

    .line 2643484
    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel;->c()Z

    move-result v4

    if-nez v4, :cond_2

    move-object v1, v2

    .line 2643485
    goto :goto_0

    .line 2643486
    :cond_2
    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel;->c()Z

    move-result v4

    .line 2643487
    iput-boolean v4, v2, LX/J4L;->i:Z

    .line 2643488
    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel;->b()LX/0Px;

    move-result-object v4

    invoke-virtual {v2, v4}, LX/J4L;->a(LX/0Px;)V

    .line 2643489
    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel;

    move-result-object v4

    sget-object p0, LX/J4K;->PROFILE_STEP:LX/J4K;

    invoke-static {v0, v4, p0}, LX/J4N;->a(LX/J4N;LX/J5E;LX/J4K;)LX/0Px;

    move-result-object v4

    invoke-virtual {v2, v4}, LX/J4L;->a(Ljava/util/Collection;)V

    .line 2643490
    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel;->a()LX/0us;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 2643491
    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel;->a()LX/0us;

    move-result-object v3

    invoke-interface {v3}, LX/0us;->b()Z

    move-result v3

    .line 2643492
    iput-boolean v3, v2, LX/J4L;->k:Z

    .line 2643493
    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupProfileStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ProfileReviewModel$CheckupItemsModel;->a()LX/0us;

    move-result-object v1

    invoke-interface {v1}, LX/0us;->a()Ljava/lang/String;

    move-result-object v1

    .line 2643494
    iput-object v1, v2, LX/J4L;->j:Ljava/lang/String;

    .line 2643495
    :goto_1
    move-object v1, v2

    .line 2643496
    goto :goto_0

    .line 2643497
    :cond_3
    const/4 v1, 0x0

    .line 2643498
    iput-boolean v1, v2, LX/J4L;->k:Z

    .line 2643499
    iput-object v3, v2, LX/J4L;->j:Ljava/lang/String;

    .line 2643500
    goto :goto_1
.end method
