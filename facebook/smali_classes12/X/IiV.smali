.class public final LX/IiV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7wC;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment$2;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment$2;)V
    .locals 0

    .prologue
    .line 2604410
    iput-object p1, p0, LX/IiV;->a:Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/1yA;)V
    .locals 3

    .prologue
    .line 2604411
    iget-object v0, p0, LX/IiV;->a:Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment$2;

    iget-object v0, v0, Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment$2;->g:Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment;

    iget-object v0, v0, Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1nG;

    invoke-virtual {v0, p1}, LX/1nG;->a(LX/1yA;)Ljava/lang/String;

    move-result-object v0

    .line 2604412
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2604413
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2604414
    const/high16 v0, 0x20000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2604415
    iget-object v0, p0, LX/IiV;->a:Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment$2;

    iget-object v0, v0, Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment$2;->g:Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment;

    iget-object v0, v0, Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/IiV;->a:Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment$2;

    iget-object v2, v2, Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment$2;->g:Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2604416
    iget-object v0, p0, LX/IiV;->a:Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment$2;

    iget-object v0, v0, Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment$2;->g:Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment;

    .line 2604417
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v1

    .line 2604418
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/IiV;->a:Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment$2;

    iget-object v0, v0, Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment$2;->g:Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment;

    .line 2604419
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v1

    .line 2604420
    const-string v1, "ACTION_ID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 2604421
    :goto_0
    if-nez v0, :cond_0

    .line 2604422
    iget-object v0, p0, LX/IiV;->a:Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment$2;

    iget-object v0, v0, Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment$2;->g:Lcom/facebook/messaging/omnim/nux/OmniMNuxFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 2604423
    :cond_0
    return-void

    .line 2604424
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
