.class public LX/IQY;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/IUu;

.field private final b:LX/IUl;

.field private final c:LX/IQD;

.field private d:LX/IUk;

.field private e:LX/IUt;

.field private f:LX/IQC;


# direct methods
.method public constructor <init>(LX/IUu;LX/IUl;LX/IQD;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2575324
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2575325
    iput-object p2, p0, LX/IQY;->b:LX/IUl;

    .line 2575326
    iput-object p1, p0, LX/IQY;->a:LX/IUu;

    .line 2575327
    iput-object p3, p0, LX/IQY;->c:LX/IQD;

    .line 2575328
    return-void
.end method

.method public static a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Z
    .locals 1

    .prologue
    .line 2575329
    invoke-static {p0}, LX/IQY;->c(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, LX/IQC;->b(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, LX/IQY;->d(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/IQY;
    .locals 4

    .prologue
    .line 2575330
    new-instance v3, LX/IQY;

    const-class v0, LX/IUu;

    invoke-interface {p0, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/IUu;

    const-class v1, LX/IUl;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/IUl;

    const-class v2, LX/IQD;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/IQD;

    invoke-direct {v3, v0, v1, v2}, LX/IQY;-><init>(LX/IUu;LX/IUl;LX/IQD;)V

    .line 2575331
    return-object v3
.end method

.method public static c(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Z
    .locals 1

    .prologue
    .line 2575332
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2575333
    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CAN_JOIN:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v2, v0

    .line 2575334
    :goto_0
    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->T()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->T()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupViewerInviteInformationModel$ViewerInviteToGroupModel$InviterModel;

    move-result-object v2

    if-eqz v2, :cond_2

    :goto_1
    return v0

    :cond_1
    move v2, v1

    .line 2575335
    goto :goto_0

    :cond_2
    move v0, v1

    .line 2575336
    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;LX/IRb;)LX/IQB;
    .locals 10
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2575337
    const/4 v1, 0x0

    .line 2575338
    if-nez p1, :cond_5

    .line 2575339
    :cond_0
    :goto_0
    move-object v1, v1

    .line 2575340
    if-nez v1, :cond_1

    .line 2575341
    :goto_1
    return-object v0

    .line 2575342
    :cond_1
    sget-object v2, LX/IQX;->a:[I

    invoke-virtual {v1}, LX/IQW;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    goto :goto_1

    .line 2575343
    :pswitch_0
    iget-object v0, p0, LX/IQY;->d:LX/IUk;

    if-nez v0, :cond_2

    .line 2575344
    iget-object v0, p0, LX/IQY;->b:LX/IUl;

    .line 2575345
    new-instance v3, LX/IUk;

    invoke-static {v0}, LX/J7Y;->b(LX/0QB;)LX/J7Y;

    move-result-object v5

    check-cast v5, LX/J7Y;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v6

    check-cast v6, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v7

    check-cast v7, LX/0Sh;

    invoke-static {v0}, LX/3mF;->b(LX/0QB;)LX/3mF;

    move-result-object v8

    check-cast v8, LX/3mF;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v9

    check-cast v9, LX/0Zb;

    move-object v4, p2

    invoke-direct/range {v3 .. v9}, LX/IUk;-><init>(LX/IRb;LX/J7Y;Landroid/content/res/Resources;LX/0Sh;LX/3mF;LX/0Zb;)V

    .line 2575346
    move-object v0, v3

    .line 2575347
    iput-object v0, p0, LX/IQY;->d:LX/IUk;

    .line 2575348
    :cond_2
    iget-object v0, p0, LX/IQY;->d:LX/IUk;

    .line 2575349
    iput-object p1, v0, LX/IUk;->g:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 2575350
    invoke-static {v0}, LX/IUk;->g(LX/IUk;)Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v1, v0, LX/IUk;->g:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->v()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_8

    const/4 v1, 0x1

    :goto_2
    iput-boolean v1, v0, LX/IUk;->i:Z

    .line 2575351
    iget-object v0, p0, LX/IQY;->d:LX/IUk;

    goto :goto_1

    .line 2575352
    :pswitch_1
    iget-object v0, p0, LX/IQY;->e:LX/IUt;

    if-nez v0, :cond_3

    .line 2575353
    iget-object v0, p0, LX/IQY;->a:LX/IUu;

    .line 2575354
    new-instance v3, LX/IUt;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v6

    check-cast v6, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/3mF;->b(LX/0QB;)LX/3mF;

    move-result-object v7

    check-cast v7, LX/3mF;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/ExecutorService;

    move-object v5, p2

    invoke-direct/range {v3 .. v8}, LX/IUt;-><init>(Landroid/content/res/Resources;LX/IRb;Lcom/facebook/content/SecureContextHelper;LX/3mF;Ljava/util/concurrent/ExecutorService;)V

    .line 2575355
    move-object v0, v3

    .line 2575356
    iput-object v0, p0, LX/IQY;->e:LX/IUt;

    .line 2575357
    :cond_3
    iget-object v0, p0, LX/IQY;->e:LX/IUt;

    .line 2575358
    iput-object p1, v0, LX/IUt;->f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 2575359
    iget-object v0, p0, LX/IQY;->e:LX/IUt;

    goto :goto_1

    .line 2575360
    :pswitch_2
    iget-object v0, p0, LX/IQY;->f:LX/IQC;

    if-nez v0, :cond_4

    .line 2575361
    iget-object v0, p0, LX/IQY;->c:LX/IQD;

    .line 2575362
    new-instance v3, LX/IQC;

    invoke-static {v0}, LX/J7Y;->b(LX/0QB;)LX/J7Y;

    move-result-object v5

    check-cast v5, LX/J7Y;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v6

    check-cast v6, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v7

    check-cast v7, LX/0Sh;

    invoke-static {v0}, LX/3mF;->b(LX/0QB;)LX/3mF;

    move-result-object v8

    check-cast v8, LX/3mF;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v9

    check-cast v9, LX/0Zb;

    move-object v4, p2

    invoke-direct/range {v3 .. v9}, LX/IQC;-><init>(LX/IRb;LX/J7Y;Landroid/content/res/Resources;LX/0Sh;LX/3mF;LX/0Zb;)V

    .line 2575363
    move-object v0, v3

    .line 2575364
    iput-object v0, p0, LX/IQY;->f:LX/IQC;

    .line 2575365
    :cond_4
    iget-object v0, p0, LX/IQY;->f:LX/IQC;

    .line 2575366
    iput-object p1, v0, LX/IQC;->f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 2575367
    iget-object v0, p0, LX/IQY;->f:LX/IQC;

    goto/16 :goto_1

    .line 2575368
    :cond_5
    invoke-static {p1}, LX/IQY;->d(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2575369
    sget-object v1, LX/IQW;->INVITED_MEMBER:LX/IQW;

    goto/16 :goto_0

    .line 2575370
    :cond_6
    invoke-static {p1}, LX/IQC;->b(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2575371
    sget-object v1, LX/IQW;->ADDED_MEMBER:LX/IQW;

    goto/16 :goto_0

    .line 2575372
    :cond_7
    invoke-static {p1}, LX/IQY;->c(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2575373
    sget-object v1, LX/IQW;->NOTIF_SETTINGS:LX/IQW;

    goto/16 :goto_0

    .line 2575374
    :cond_8
    const/4 v1, 0x0

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
