.class public final enum LX/I2X;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/I2X;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/I2X;

.field public static final enum VIEW_ALL:LX/I2X;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2530371
    new-instance v0, LX/I2X;

    const-string v1, "VIEW_ALL"

    invoke-direct {v0, v1, v2}, LX/I2X;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I2X;->VIEW_ALL:LX/I2X;

    .line 2530372
    const/4 v0, 0x1

    new-array v0, v0, [LX/I2X;

    sget-object v1, LX/I2X;->VIEW_ALL:LX/I2X;

    aput-object v1, v0, v2

    sput-object v0, LX/I2X;->$VALUES:[LX/I2X;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2530368
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/I2X;
    .locals 1

    .prologue
    .line 2530370
    const-class v0, LX/I2X;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/I2X;

    return-object v0
.end method

.method public static values()[LX/I2X;
    .locals 1

    .prologue
    .line 2530369
    sget-object v0, LX/I2X;->$VALUES:[LX/I2X;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/I2X;

    return-object v0
.end method
