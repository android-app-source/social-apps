.class public final LX/Hb4;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2484980
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 2484981
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2484982
    :goto_0
    return v1

    .line 2484983
    :cond_0
    const-string v8, "last_used"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 2484984
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v5, v0

    move v0, v2

    .line 2484985
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_5

    .line 2484986
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 2484987
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2484988
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_1

    if-eqz v7, :cond_1

    .line 2484989
    const-string v8, "icon"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 2484990
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 2484991
    :cond_2
    const-string v8, "name"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 2484992
    invoke-static {p0, p1}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 2484993
    :cond_3
    const-string v8, "session_id"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 2484994
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 2484995
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2484996
    :cond_5
    const/4 v7, 0x4

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 2484997
    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 2484998
    if-eqz v0, :cond_6

    .line 2484999
    invoke-virtual {p1, v2, v5, v1}, LX/186;->a(III)V

    .line 2485000
    :cond_6
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2485001
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2485002
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2485003
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2485004
    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2485005
    if-eqz v0, :cond_0

    .line 2485006
    const-string v1, "icon"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2485007
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2485008
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2485009
    if-eqz v0, :cond_1

    .line 2485010
    const-string v1, "last_used"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2485011
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2485012
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2485013
    if-eqz v0, :cond_2

    .line 2485014
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2485015
    invoke-static {p0, v0, p2}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 2485016
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2485017
    if-eqz v0, :cond_3

    .line 2485018
    const-string v1, "session_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2485019
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2485020
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2485021
    return-void
.end method
