.class public final LX/HQt;
.super Ljava/util/HashMap;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;",
        "Lcom/facebook/graphql/enums/GraphQLPageActionType;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2464057
    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 2464058
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->OPEN_PAGE_INFO:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_ABOUT:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-virtual {p0, v0, v1}, LX/HQt;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2464059
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->SEE_ALL_PAGE_VIDEO_PLAYLISTS:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_VIDEOS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-virtual {p0, v0, v1}, LX/HQt;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2464060
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->SEE_ALL_PAGE_VIDEOS:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_VIDEOS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-virtual {p0, v0, v1}, LX/HQt;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2464061
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->VIEW_PAGE_EVENTS:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_EVENTS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-virtual {p0, v0, v1}, LX/HQt;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2464062
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->VIEW_PAGE_SERVICES:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_SERVICES:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-virtual {p0, v0, v1}, LX/HQt;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2464063
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->SEE_ALL_PAGE_REVIEWS:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_REVIEWS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-virtual {p0, v0, v1}, LX/HQt;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2464064
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->SEE_ALL_PAGE_PHOTOS:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_PHOTOS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-virtual {p0, v0, v1}, LX/HQt;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2464065
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->VIEW_PAGE_CHILD_LOCATIONS:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_LOCATIONS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-virtual {p0, v0, v1}, LX/HQt;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2464066
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->PAGE_COMMERCE_OPEN_COLLECTION:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_SHOP:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-virtual {p0, v0, v1}, LX/HQt;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2464067
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->PAGE_COMMERCE_OPEN_STORE:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_SHOP:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-virtual {p0, v0, v1}, LX/HQt;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2464068
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->SEE_ALL_PAGE_OFFERS:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_OFFERS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-virtual {p0, v0, v1}, LX/HQt;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2464069
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->SEE_ALL_PAGE_JOBS:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_JOBS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-virtual {p0, v0, v1}, LX/HQt;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2464070
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->SEE_PAGE_COMMUNITY_CONTENT:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_COMMUNITY:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-virtual {p0, v0, v1}, LX/HQt;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2464071
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;->SEE_ALL_PAGE_POSTS:Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_POSTS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-virtual {p0, v0, v1}, LX/HQt;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2464072
    return-void
.end method
