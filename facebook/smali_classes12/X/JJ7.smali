.class public final LX/JJ7;
.super LX/63W;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;)V
    .locals 0

    .prologue
    .line 2678953
    iput-object p1, p0, LX/JJ7;->a:Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;

    invoke-direct {p0}, LX/63W;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 13

    .prologue
    .line 2678931
    iget-object v0, p0, LX/JJ7;->a:Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;

    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 2678932
    iget-object v1, v0, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;->g:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getPickedTokens()LX/0Px;

    move-result-object v1

    .line 2678933
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2678934
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1, v5, v3}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 2678935
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 2678936
    :goto_0
    return-void

    .line 2678937
    :cond_0
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 2678938
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2678939
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8QK;

    move-object v2, v1

    .line 2678940
    check-cast v2, LX/JJD;

    .line 2678941
    iget-wide v11, v2, LX/JJD;->f:J

    move-wide v9, v11

    .line 2678942
    invoke-static {v9, v10}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2678943
    invoke-virtual {v1}, LX/8QK;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2678944
    :cond_1
    const-string v1, "react_native_launcher"

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v6, "typeahead_launcher_extra_key"

    invoke-virtual {v2, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2678945
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2678946
    const-string v2, "typeahead_result_ids_extra_key"

    invoke-virtual {v1, v2, v7}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 2678947
    const-string v2, "typeahead_result_names_extra_key"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 2678948
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const/4 v3, -0x1

    invoke-virtual {v2, v3, v1}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 2678949
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0

    .line 2678950
    :cond_2
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f083a6f

    invoke-virtual {v0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x1

    move-object v6, v3

    invoke-static/range {v1 .. v6}, LX/4BY;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZLandroid/content/DialogInterface$OnCancelListener;)LX/4BY;

    move-result-object v2

    .line 2678951
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v3, "discovery_curation_logging_data_extra_key"

    invoke-static {v1, v3}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;

    .line 2678952
    iget-object v3, v0, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;->d:LX/JIO;

    invoke-virtual {v3, v1}, LX/JIO;->a(Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;)LX/JIN;

    move-result-object v1

    iget-object v3, v0, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;->e:Ljava/lang/String;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v1, v3, v4, v5, v7}, LX/JIN;->a(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v3, LX/JJC;

    invoke-direct {v3, v0, v2}, LX/JJC;-><init>(Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;LX/4BY;)V

    iget-object v2, v0, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;->c:Ljava/util/concurrent/Executor;

    invoke-static {v1, v3, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto/16 :goto_0
.end method
