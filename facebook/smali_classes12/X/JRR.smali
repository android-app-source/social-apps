.class public final LX/JRR;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/3hV;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1Pm;

.field public b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public c:Z

.field public final synthetic d:LX/3hV;


# direct methods
.method public constructor <init>(LX/3hV;)V
    .locals 1

    .prologue
    .line 2693318
    iput-object p1, p0, LX/JRR;->d:LX/3hV;

    .line 2693319
    move-object v0, p1

    .line 2693320
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2693321
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2693338
    const-string v0, "MultiShareAttachmentComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2693322
    if-ne p0, p1, :cond_1

    .line 2693323
    :cond_0
    :goto_0
    return v0

    .line 2693324
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2693325
    goto :goto_0

    .line 2693326
    :cond_3
    check-cast p1, LX/JRR;

    .line 2693327
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2693328
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2693329
    if-eq v2, v3, :cond_0

    .line 2693330
    iget-object v2, p0, LX/JRR;->a:LX/1Pm;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JRR;->a:LX/1Pm;

    iget-object v3, p1, LX/JRR;->a:LX/1Pm;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2693331
    goto :goto_0

    .line 2693332
    :cond_5
    iget-object v2, p1, LX/JRR;->a:LX/1Pm;

    if-nez v2, :cond_4

    .line 2693333
    :cond_6
    iget-object v2, p0, LX/JRR;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/JRR;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/JRR;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2693334
    goto :goto_0

    .line 2693335
    :cond_8
    iget-object v2, p1, LX/JRR;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_7

    .line 2693336
    :cond_9
    iget-boolean v2, p0, LX/JRR;->c:Z

    iget-boolean v3, p1, LX/JRR;->c:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 2693337
    goto :goto_0
.end method
