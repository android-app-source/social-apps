.class public LX/Iem;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2598975
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 2598976
    return-void
.end method

.method public static a(LX/0W9;LX/00H;)Landroid/net/Uri;
    .locals 3
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/contactsync/learn/ForContactsLearnMore;
    .end annotation

    .prologue
    .line 2598977
    const-string v0, "https://www.facebook.com/mobile/messenger/contacts"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 2598978
    invoke-virtual {p0}, LX/0W9;->d()Ljava/lang/String;

    move-result-object v1

    .line 2598979
    const-string v2, "locale"

    invoke-virtual {v0, v2, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2598980
    const-string v1, "cid"

    invoke-virtual {p1}, LX/00H;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2598981
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 2598982
    return-void
.end method
