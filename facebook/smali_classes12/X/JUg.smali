.class public LX/JUg;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JUb;",
            ">;"
        }
    .end annotation
.end field

.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JUY;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JUU;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JUj;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JUn;",
            ">;"
        }
    .end annotation
.end field

.field private f:LX/JUQ;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/JUQ;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JUb;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/JUY;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/JUU;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/JUj;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/JUn;",
            ">;",
            "LX/JUQ;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2699437
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2699438
    iput-object p1, p0, LX/JUg;->a:LX/0Ot;

    .line 2699439
    iput-object p2, p0, LX/JUg;->b:LX/0Ot;

    .line 2699440
    iput-object p3, p0, LX/JUg;->c:LX/0Ot;

    .line 2699441
    iput-object p4, p0, LX/JUg;->d:LX/0Ot;

    .line 2699442
    iput-object p5, p0, LX/JUg;->e:LX/0Ot;

    .line 2699443
    iput-object p6, p0, LX/JUg;->f:LX/JUQ;

    .line 2699444
    return-void
.end method

.method private static a(LX/JUg;LX/1De;Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;LX/JUq;LX/1Pr;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;)LX/1X5;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;",
            "LX/JUq;",
            "TE;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;",
            ")",
            "LX/1X5;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 2699445
    sget-object v1, LX/JUf;->a:[I

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->l()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2699446
    :cond_0
    :goto_0
    return-object v0

    .line 2699447
    :pswitch_0
    iget-object v0, p0, LX/JUg;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JUY;

    const/4 v1, 0x0

    .line 2699448
    new-instance v2, LX/JUX;

    invoke-direct {v2, v0}, LX/JUX;-><init>(LX/JUY;)V

    .line 2699449
    sget-object v3, LX/JUY;->a:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JUW;

    .line 2699450
    if-nez v3, :cond_1

    .line 2699451
    new-instance v3, LX/JUW;

    invoke-direct {v3}, LX/JUW;-><init>()V

    .line 2699452
    :cond_1
    invoke-static {v3, p1, v1, v1, v2}, LX/JUW;->a$redex0(LX/JUW;LX/1De;IILX/JUX;)V

    .line 2699453
    move-object v2, v3

    .line 2699454
    move-object v1, v2

    .line 2699455
    move-object v0, v1

    .line 2699456
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-static {v1}, LX/2yc;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/3Ab;

    move-result-object v1

    .line 2699457
    iget-object v2, v0, LX/JUW;->a:LX/JUX;

    iput-object v1, v2, LX/JUX;->a:LX/3Ab;

    .line 2699458
    iget-object v2, v0, LX/JUW;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2699459
    move-object v0, v0

    .line 2699460
    goto :goto_0

    .line 2699461
    :pswitch_1
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 2699462
    :goto_1
    iget-object v0, p0, LX/JUg;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JUU;

    const/4 v2, 0x0

    .line 2699463
    new-instance v3, LX/JUT;

    invoke-direct {v3, v0}, LX/JUT;-><init>(LX/JUU;)V

    .line 2699464
    iget-object p0, v0, LX/JUU;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/JUS;

    .line 2699465
    if-nez p0, :cond_2

    .line 2699466
    new-instance p0, LX/JUS;

    invoke-direct {p0, v0}, LX/JUS;-><init>(LX/JUU;)V

    .line 2699467
    :cond_2
    invoke-static {p0, p1, v2, v2, v3}, LX/JUS;->a$redex0(LX/JUS;LX/1De;IILX/JUT;)V

    .line 2699468
    move-object v3, p0

    .line 2699469
    move-object v2, v3

    .line 2699470
    move-object v0, v2

    .line 2699471
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    .line 2699472
    iget-object v3, v0, LX/JUS;->a:LX/JUT;

    iput-object v2, v3, LX/JUT;->a:Ljava/lang/String;

    .line 2699473
    iget-object v3, v0, LX/JUS;->e:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v3, p0}, Ljava/util/BitSet;->set(I)V

    .line 2699474
    move-object v2, v0

    .line 2699475
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    .line 2699476
    :goto_2
    iget-object v3, v2, LX/JUS;->a:LX/JUT;

    iput-object v0, v3, LX/JUT;->b:Ljava/lang/String;

    .line 2699477
    iget-object v3, v2, LX/JUS;->e:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v3, p0}, Ljava/util/BitSet;->set(I)V

    .line 2699478
    move-object v0, v2

    .line 2699479
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->u()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v2

    .line 2699480
    iget-object v3, v0, LX/JUS;->a:LX/JUT;

    iput-object v2, v3, LX/JUT;->c:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 2699481
    iget-object v3, v0, LX/JUS;->e:Ljava/util/BitSet;

    const/4 p0, 0x2

    invoke-virtual {v3, p0}, Ljava/util/BitSet;->set(I)V

    .line 2699482
    move-object v0, v0

    .line 2699483
    iget-object v2, v0, LX/JUS;->a:LX/JUT;

    iput-object v1, v2, LX/JUT;->e:Ljava/lang/String;

    .line 2699484
    move-object v0, v0

    .line 2699485
    iget-object v1, v0, LX/JUS;->a:LX/JUT;

    iput-object p4, v1, LX/JUT;->f:LX/1Pr;

    .line 2699486
    iget-object v1, v0, LX/JUS;->e:Ljava/util/BitSet;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2699487
    move-object v0, v0

    .line 2699488
    iget-object v1, v0, LX/JUS;->a:LX/JUT;

    iput-object p5, v1, LX/JUT;->g:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2699489
    iget-object v1, v0, LX/JUS;->e:Ljava/util/BitSet;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2699490
    move-object v0, v0

    .line 2699491
    iget-object v1, v0, LX/JUS;->a:LX/JUT;

    iput-object p3, v1, LX/JUT;->d:LX/JUq;

    .line 2699492
    iget-object v1, v0, LX/JUS;->e:Ljava/util/BitSet;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2699493
    move-object v0, v0

    .line 2699494
    invoke-virtual {p6}, Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;->a()Ljava/lang/String;

    move-result-object v1

    .line 2699495
    iget-object v2, v0, LX/JUS;->a:LX/JUT;

    iput-object v1, v2, LX/JUT;->h:Ljava/lang/String;

    .line 2699496
    move-object v0, v0

    .line 2699497
    goto/16 :goto_0

    .line 2699498
    :cond_3
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_1

    .line 2699499
    :cond_4
    const-string v0, ""

    goto :goto_2

    .line 2699500
    :pswitch_2
    iget-object v0, p0, LX/JUg;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JUn;

    const/4 v1, 0x0

    .line 2699501
    new-instance v2, LX/JUm;

    invoke-direct {v2, v0}, LX/JUm;-><init>(LX/JUn;)V

    .line 2699502
    sget-object v3, LX/JUn;->a:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JUl;

    .line 2699503
    if-nez v3, :cond_5

    .line 2699504
    new-instance v3, LX/JUl;

    invoke-direct {v3}, LX/JUl;-><init>()V

    .line 2699505
    :cond_5
    invoke-static {v3, p1, v1, v1, v2}, LX/JUl;->a$redex0(LX/JUl;LX/1De;IILX/JUm;)V

    .line 2699506
    move-object v2, v3

    .line 2699507
    move-object v1, v2

    .line 2699508
    move-object v0, v1

    .line 2699509
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->r()D

    move-result-wide v2

    double-to-float v1, v2

    .line 2699510
    iget-object v2, v0, LX/JUl;->a:LX/JUm;

    iput v1, v2, LX/JUm;->b:F

    .line 2699511
    iget-object v2, v0, LX/JUl;->d:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2699512
    move-object v0, v0

    .line 2699513
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    .line 2699514
    iget-object v2, v0, LX/JUl;->a:LX/JUm;

    iput-object v1, v2, LX/JUm;->a:Ljava/lang/String;

    .line 2699515
    iget-object v2, v0, LX/JUl;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2699516
    move-object v0, v0

    .line 2699517
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->u()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v1

    .line 2699518
    iget-object v2, v0, LX/JUl;->a:LX/JUm;

    iput-object v1, v2, LX/JUm;->d:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 2699519
    move-object v0, v0

    .line 2699520
    iget-object v1, v0, LX/JUl;->a:LX/JUm;

    iput-object p5, v1, LX/JUm;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2699521
    move-object v0, v0

    .line 2699522
    goto/16 :goto_0

    .line 2699523
    :pswitch_3
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->o()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLocation;

    .line 2699524
    iget-object v1, p0, LX/JUg;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/JUj;

    const/4 v2, 0x0

    .line 2699525
    new-instance v3, LX/JUi;

    invoke-direct {v3, v1}, LX/JUi;-><init>(LX/JUj;)V

    .line 2699526
    sget-object p0, LX/JUj;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/JUh;

    .line 2699527
    if-nez p0, :cond_6

    .line 2699528
    new-instance p0, LX/JUh;

    invoke-direct {p0}, LX/JUh;-><init>()V

    .line 2699529
    :cond_6
    invoke-static {p0, p1, v2, v2, v3}, LX/JUh;->a$redex0(LX/JUh;LX/1De;IILX/JUi;)V

    .line 2699530
    move-object v3, p0

    .line 2699531
    move-object v2, v3

    .line 2699532
    move-object v1, v2

    .line 2699533
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLocation;->a()D

    move-result-wide v2

    .line 2699534
    iget-object v4, v1, LX/JUh;->a:LX/JUi;

    iput-wide v2, v4, LX/JUi;->a:D

    .line 2699535
    iget-object v4, v1, LX/JUh;->d:Ljava/util/BitSet;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 2699536
    move-object v1, v1

    .line 2699537
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLocation;->b()D

    move-result-wide v2

    .line 2699538
    iget-object v4, v1, LX/JUh;->a:LX/JUi;

    iput-wide v2, v4, LX/JUi;->b:D

    .line 2699539
    iget-object v4, v1, LX/JUh;->d:Ljava/util/BitSet;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 2699540
    move-object v0, v1

    .line 2699541
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->p()Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    move-result-object v1

    .line 2699542
    iget-object v2, v0, LX/JUh;->a:LX/JUi;

    iput-object v1, v2, LX/JUi;->c:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    .line 2699543
    move-object v0, v0

    .line 2699544
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->u()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v1

    .line 2699545
    iget-object v2, v0, LX/JUh;->a:LX/JUi;

    iput-object v1, v2, LX/JUi;->e:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 2699546
    move-object v0, v0

    .line 2699547
    iget-object v1, v0, LX/JUh;->a:LX/JUi;

    iput-object p5, v1, LX/JUi;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2699548
    move-object v0, v0

    .line 2699549
    goto/16 :goto_0

    .line 2699550
    :pswitch_4
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->n()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->n()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2699551
    iget-object v0, p0, LX/JUg;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JUb;

    const/4 v1, 0x0

    .line 2699552
    new-instance v2, LX/JUa;

    invoke-direct {v2, v0}, LX/JUa;-><init>(LX/JUb;)V

    .line 2699553
    sget-object v4, LX/JUb;->a:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/JUZ;

    .line 2699554
    if-nez v4, :cond_7

    .line 2699555
    new-instance v4, LX/JUZ;

    invoke-direct {v4}, LX/JUZ;-><init>()V

    .line 2699556
    :cond_7
    invoke-static {v4, p1, v1, v1, v2}, LX/JUZ;->a$redex0(LX/JUZ;LX/1De;IILX/JUa;)V

    .line 2699557
    move-object v2, v4

    .line 2699558
    move-object v1, v2

    .line 2699559
    move-object v1, v1

    .line 2699560
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->n()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    .line 2699561
    iget-object v2, v1, LX/JUZ;->a:LX/JUa;

    iput-object v0, v2, LX/JUa;->a:Ljava/lang/String;

    .line 2699562
    iget-object v2, v1, LX/JUZ;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2699563
    move-object v0, v1

    .line 2699564
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->u()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v1

    .line 2699565
    iget-object v2, v0, LX/JUZ;->a:LX/JUa;

    iput-object v1, v2, LX/JUa;->c:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    .line 2699566
    move-object v0, v0

    .line 2699567
    iget-object v1, v0, LX/JUZ;->a:LX/JUa;

    iput-object p5, v1, LX/JUa;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2699568
    move-object v0, v0

    .line 2699569
    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static a(LX/0QB;)LX/JUg;
    .locals 10

    .prologue
    .line 2699570
    const-class v1, LX/JUg;

    monitor-enter v1

    .line 2699571
    :try_start_0
    sget-object v0, LX/JUg;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2699572
    sput-object v2, LX/JUg;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2699573
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2699574
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2699575
    new-instance v3, LX/JUg;

    const/16 v4, 0x207d

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x207b

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x2079

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x2081

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x2083

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/JUQ;->b(LX/0QB;)LX/JUQ;

    move-result-object v9

    check-cast v9, LX/JUQ;

    invoke-direct/range {v3 .. v9}, LX/JUg;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/JUQ;)V

    .line 2699576
    move-object v0, v3

    .line 2699577
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2699578
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JUg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2699579
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2699580
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnitItem;LX/1Pr;)LX/1Dg;
    .locals 11
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnitItem;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # LX/1Pr;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnitItem;",
            "TE;)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2699581
    new-instance v2, LX/JUr;

    invoke-direct {v2, p2}, LX/JUr;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2699582
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2699583
    check-cast v0, LX/0jW;

    invoke-interface {p4, v2, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JUq;

    .line 2699584
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v1}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    const/4 v2, 0x1

    invoke-interface {v0, v2}, LX/1Dh;->B(I)LX/1Dh;

    move-result-object v0

    const v2, 0x7f0b25be

    invoke-interface {v0, v2}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v0

    const v2, 0x7f0b25bf

    invoke-interface {v0, v2}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v0

    const/4 v2, 0x6

    const v4, 0x7f0b25c3

    invoke-interface {v0, v2, v4}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v0

    const v2, 0x7f020a3c

    invoke-interface {v0, v2}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v0

    .line 2699585
    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;

    move-result-object v6

    .line 2699586
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;->j()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    move v7, v1

    move-object v8, v0

    :goto_0
    if-ge v7, v10, :cond_0

    invoke-virtual {v9, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    move-object v0, p0

    move-object v1, p1

    move-object v4, p4

    move-object v5, p2

    .line 2699587
    invoke-static/range {v0 .. v6}, LX/JUg;->a(LX/JUg;LX/1De;Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;LX/JUq;LX/1Pr;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLReactionUnitStaticAggregationComponent;)LX/1X5;

    move-result-object v0

    invoke-interface {v8, v0}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v1

    .line 2699588
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    move-object v8, v1

    goto :goto_0

    .line 2699589
    :cond_0
    invoke-interface {v8}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0
.end method
