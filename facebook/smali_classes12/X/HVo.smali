.class public LX/HVo;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/03V;

.field private final c:LX/Bf1;

.field private final d:LX/0tX;

.field private final e:LX/CXj;

.field private final f:LX/31f;

.field private final g:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2475402
    const-class v0, LX/HVo;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/HVo;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/Bf1;LX/0tX;LX/CXj;LX/31f;LX/1Ck;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2475370
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2475371
    iput-object p1, p0, LX/HVo;->b:LX/03V;

    .line 2475372
    iput-object p2, p0, LX/HVo;->c:LX/Bf1;

    .line 2475373
    iput-object p3, p0, LX/HVo;->d:LX/0tX;

    .line 2475374
    iput-object p4, p0, LX/HVo;->e:LX/CXj;

    .line 2475375
    iput-object p5, p0, LX/HVo;->f:LX/31f;

    .line 2475376
    iput-object p6, p0, LX/HVo;->g:LX/1Ck;

    .line 2475377
    return-void
.end method

.method public static a$redex0(LX/HVo;J)V
    .locals 3

    .prologue
    .line 2475378
    iget-object v0, p0, LX/HVo;->e:LX/CXj;

    new-instance v1, LX/CY0;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {v1, v2}, LX/CY0;-><init>(Ljava/lang/Long;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2475379
    return-void
.end method

.method public static a$redex0(LX/HVo;JLjava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 2475380
    invoke-static {p0, p1, p2}, LX/HVo;->a$redex0(LX/HVo;J)V

    .line 2475381
    new-instance v0, LX/96L;

    invoke-direct {v0}, LX/96L;-><init>()V

    move-object v0, v0

    .line 2475382
    new-instance v1, LX/4Dw;

    invoke-direct {v1}, LX/4Dw;-><init>()V

    .line 2475383
    const-string v2, "place_question_id"

    invoke-virtual {v1, v2, p3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2475384
    const-string v2, "place_question_answer_value"

    invoke-virtual {v1, v2, p4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2475385
    const-string v2, "input"

    invoke-virtual {v0, v2, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2475386
    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 2475387
    iget-object v1, p0, LX/HVo;->g:LX/1Ck;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "key_submit_answer"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/HVo;->d:LX/0tX;

    sget-object v4, LX/3Fz;->b:LX/3Fz;

    invoke-virtual {v3, v0, v4}, LX/0tX;->a(LX/399;LX/3Fz;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v3, LX/HVm;

    invoke-direct {v3, p0}, LX/HVm;-><init>(LX/HVo;)V

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2475388
    return-void
.end method

.method public static b(LX/0QB;)LX/HVo;
    .locals 7

    .prologue
    .line 2475389
    new-instance v0, LX/HVo;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    .line 2475390
    new-instance v4, LX/Bf1;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, LX/0TD;

    invoke-direct {v4, v2, v3}, LX/Bf1;-><init>(LX/0tX;LX/0TD;)V

    .line 2475391
    move-object v2, v4

    .line 2475392
    check-cast v2, LX/Bf1;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {p0}, LX/CXj;->a(LX/0QB;)LX/CXj;

    move-result-object v4

    check-cast v4, LX/CXj;

    invoke-static {p0}, LX/31f;->a(LX/0QB;)LX/31f;

    move-result-object v5

    check-cast v5, LX/31f;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v6

    check-cast v6, LX/1Ck;

    invoke-direct/range {v0 .. v6}, LX/HVo;-><init>(LX/03V;LX/Bf1;LX/0tX;LX/CXj;LX/31f;LX/1Ck;)V

    .line 2475393
    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;LX/BeB;)V
    .locals 10

    .prologue
    .line 2475394
    iget-object v0, p0, LX/HVo;->f:LX/31f;

    new-instance v1, Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    const-string v2, "android_entity_card_edit_action_button"

    const-string v3, "page_context_row_place_question"

    invoke-direct {v1, v2, v3}, Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-wide v2, p3, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->a:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/31f;->a(Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;Ljava/lang/String;)V

    .line 2475395
    iget-object v6, p0, LX/HVo;->g:LX/1Ck;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "key_fetch_place_questions"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p3, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iget-object v0, p0, LX/HVo;->c:LX/Bf1;

    iget-wide v2, p3, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->a:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p4}, LX/BeB;->getValue()J

    move-result-wide v2

    const-string v4, "PAGE_CONTEXT_ROW"

    const/4 v5, 0x1

    .line 2475396
    new-instance v8, LX/96P;

    invoke-direct {v8}, LX/96P;-><init>()V

    move-object v8, v8

    .line 2475397
    const-string v9, "page_id"

    invoke-virtual {v8, v9, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v9

    const-string p2, "field_type"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p4

    invoke-virtual {v9, p2, p4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v9

    const-string p2, "question_context"

    invoke-virtual {v9, p2, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v9

    const-string p2, "question_count"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p4

    invoke-virtual {v9, p2, p4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2475398
    iget-object v9, v0, LX/Bf1;->a:LX/0tX;

    invoke-static {v8}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v8

    invoke-virtual {v9, v8}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v8

    .line 2475399
    new-instance v9, LX/Bf0;

    invoke-direct {v9, v0}, LX/Bf0;-><init>(LX/Bf1;)V

    iget-object p2, v0, LX/Bf1;->b:LX/0TD;

    invoke-static {v8, v9, p2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    move-object v0, v8

    .line 2475400
    new-instance v1, LX/HVl;

    invoke-direct {v1, p0, p3, p1}, LX/HVl;-><init>(LX/HVo;Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;Landroid/view/View;)V

    invoke-virtual {v6, v7, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2475401
    return-void
.end method
