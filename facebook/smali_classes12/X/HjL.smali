.class public final LX/HjL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Ljava/util/EnumSet;

.field public final synthetic b:LX/HjM;


# direct methods
.method public constructor <init>(LX/HjM;Ljava/util/EnumSet;)V
    .locals 0

    iput-object p1, p0, LX/HjL;->b:LX/HjM;

    iput-object p2, p0, LX/HjL;->a:Ljava/util/EnumSet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/Hjr;)V
    .locals 2

    iget-object v0, p0, LX/HjL;->b:LX/HjM;

    iget-object v0, v0, LX/HjM;->g:Lcom/facebook/neko/util/MainActivityFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HjL;->b:LX/HjM;

    iget-object v0, v0, LX/HjM;->g:Lcom/facebook/neko/util/MainActivityFragment;

    invoke-virtual {p1}, LX/Hjr;->b()LX/Hj0;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/neko/util/MainActivityFragment;->a(LX/Hj0;)V

    :cond_0
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/Hjj;",
            ">;)V"
        }
    .end annotation

    const/4 v3, 0x0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v2, v0, [LX/HjF;

    const/4 v0, 0x1

    new-array v5, v0, [I

    aput v3, v5, v3

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_3

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hjj;

    new-instance v6, Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-direct {v6, v1}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v1, p0, LX/HjL;->a:Ljava/util/EnumSet;

    sget-object v4, LX/HjA;->ICON:LX/HjA;

    invoke-virtual {v1, v4}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, LX/Hjj;->h()LX/Hj9;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, LX/Hjj;->h()LX/Hj9;

    move-result-object v1

    iget-object v4, v1, LX/Hj9;->a:Ljava/lang/String;

    move-object v1, v4

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v1, p0, LX/HjL;->a:Ljava/util/EnumSet;

    sget-object v4, LX/HjA;->IMAGE:LX/HjA;

    invoke-virtual {v1, v4}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, LX/Hjj;->i()LX/Hj9;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, LX/Hjj;->i()LX/Hj9;

    move-result-object v0

    iget-object v1, v0, LX/Hj9;->a:Ljava/lang/String;

    move-object v0, v1

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    iget-object v0, p0, LX/HjL;->b:LX/HjM;

    iget-object v7, v0, LX/HjM;->b:Landroid/content/Context;

    new-instance v0, LX/HjK;

    move-object v1, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, LX/HjK;-><init>(LX/HjL;[LX/HjF;ILjava/util/List;[I)V

    const/4 v12, 0x1

    const/4 v11, 0x0

    new-array v4, v12, [I

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v1

    aput v1, v4, v11

    aget v1, v4, v11

    if-nez v1, :cond_4

    if-eqz v0, :cond_2

    invoke-interface {v0}, LX/HjJ;->a()V

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    return-void

    :cond_4
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    new-instance v9, LX/Hkr;

    invoke-direct {v9, v7}, LX/Hkr;-><init>(Landroid/content/Context;)V

    new-instance v10, LX/Hks;

    invoke-direct {v10, v4, v0}, LX/Hks;-><init>([ILX/HjJ;)V

    iput-object v10, v9, LX/Hkr;->e:LX/HjJ;

    move-object v9, v9

    new-array v10, v12, [Ljava/lang/String;

    aput-object v1, v10, v11

    invoke-virtual {v9, v10}, LX/Hkr;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_1
.end method
