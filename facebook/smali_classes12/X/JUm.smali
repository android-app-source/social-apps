.class public final LX/JUm;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JUn;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:F

.field public c:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

.field public e:Ljava/lang/String;

.field public final synthetic f:LX/JUn;


# direct methods
.method public constructor <init>(LX/JUn;)V
    .locals 1

    .prologue
    .line 2699715
    iput-object p1, p0, LX/JUm;->f:LX/JUn;

    .line 2699716
    move-object v0, p1

    .line 2699717
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2699718
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2699719
    const-string v0, "PageContextualRecommendationsReviewComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2699720
    if-ne p0, p1, :cond_1

    .line 2699721
    :cond_0
    :goto_0
    return v0

    .line 2699722
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2699723
    goto :goto_0

    .line 2699724
    :cond_3
    check-cast p1, LX/JUm;

    .line 2699725
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2699726
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2699727
    if-eq v2, v3, :cond_0

    .line 2699728
    iget-object v2, p0, LX/JUm;->a:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JUm;->a:Ljava/lang/String;

    iget-object v3, p1, LX/JUm;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2699729
    goto :goto_0

    .line 2699730
    :cond_5
    iget-object v2, p1, LX/JUm;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 2699731
    :cond_6
    iget v2, p0, LX/JUm;->b:F

    iget v3, p1, LX/JUm;->b:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_7

    move v0, v1

    .line 2699732
    goto :goto_0

    .line 2699733
    :cond_7
    iget-object v2, p0, LX/JUm;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_9

    iget-object v2, p0, LX/JUm;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/JUm;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    :cond_8
    move v0, v1

    .line 2699734
    goto :goto_0

    .line 2699735
    :cond_9
    iget-object v2, p1, LX/JUm;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_8

    .line 2699736
    :cond_a
    iget-object v2, p0, LX/JUm;->d:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    if-eqz v2, :cond_c

    iget-object v2, p0, LX/JUm;->d:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    iget-object v3, p1, LX/JUm;->d:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    :cond_b
    move v0, v1

    .line 2699737
    goto :goto_0

    .line 2699738
    :cond_c
    iget-object v2, p1, LX/JUm;->d:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    if-nez v2, :cond_b

    .line 2699739
    :cond_d
    iget-object v2, p0, LX/JUm;->e:Ljava/lang/String;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/JUm;->e:Ljava/lang/String;

    iget-object v3, p1, LX/JUm;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2699740
    goto :goto_0

    .line 2699741
    :cond_e
    iget-object v2, p1, LX/JUm;->e:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
