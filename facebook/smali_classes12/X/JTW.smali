.class public final enum LX/JTW;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/JTW;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/JTW;

.field public static final enum PAUSE:LX/JTW;

.field public static final enum PLAY:LX/JTW;

.field public static final enum PLAYBACK_FAILED:LX/JTW;

.field public static final enum PLAYBACK_FINISHED:LX/JTW;

.field public static final enum TAP_ON_CTA:LX/JTW;

.field public static final enum TAP_TO_PLAY:LX/JTW;


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2696788
    new-instance v0, LX/JTW;

    const-string v1, "PLAY"

    const-string v2, "music_play"

    invoke-direct {v0, v1, v4, v2}, LX/JTW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/JTW;->PLAY:LX/JTW;

    .line 2696789
    new-instance v0, LX/JTW;

    const-string v1, "TAP_TO_PLAY"

    const-string v2, "music_play_requested"

    invoke-direct {v0, v1, v5, v2}, LX/JTW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/JTW;->TAP_TO_PLAY:LX/JTW;

    .line 2696790
    new-instance v0, LX/JTW;

    const-string v1, "PAUSE"

    const-string v2, "music_pause"

    invoke-direct {v0, v1, v6, v2}, LX/JTW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/JTW;->PAUSE:LX/JTW;

    .line 2696791
    new-instance v0, LX/JTW;

    const-string v1, "PLAYBACK_FINISHED"

    const-string v2, "music_reached_end"

    invoke-direct {v0, v1, v7, v2}, LX/JTW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/JTW;->PLAYBACK_FINISHED:LX/JTW;

    .line 2696792
    new-instance v0, LX/JTW;

    const-string v1, "PLAYBACK_FAILED"

    const-string v2, "music_error"

    invoke-direct {v0, v1, v8, v2}, LX/JTW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/JTW;->PLAYBACK_FAILED:LX/JTW;

    .line 2696793
    new-instance v0, LX/JTW;

    const-string v1, "TAP_ON_CTA"

    const/4 v2, 0x5

    const-string v3, "music_cta_click"

    invoke-direct {v0, v1, v2, v3}, LX/JTW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/JTW;->TAP_ON_CTA:LX/JTW;

    .line 2696794
    const/4 v0, 0x6

    new-array v0, v0, [LX/JTW;

    sget-object v1, LX/JTW;->PLAY:LX/JTW;

    aput-object v1, v0, v4

    sget-object v1, LX/JTW;->TAP_TO_PLAY:LX/JTW;

    aput-object v1, v0, v5

    sget-object v1, LX/JTW;->PAUSE:LX/JTW;

    aput-object v1, v0, v6

    sget-object v1, LX/JTW;->PLAYBACK_FINISHED:LX/JTW;

    aput-object v1, v0, v7

    sget-object v1, LX/JTW;->PLAYBACK_FAILED:LX/JTW;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/JTW;->TAP_ON_CTA:LX/JTW;

    aput-object v2, v0, v1

    sput-object v0, LX/JTW;->$VALUES:[LX/JTW;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2696785
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2696786
    iput-object p3, p0, LX/JTW;->name:Ljava/lang/String;

    .line 2696787
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/JTW;
    .locals 1

    .prologue
    .line 2696795
    const-class v0, LX/JTW;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/JTW;

    return-object v0
.end method

.method public static values()[LX/JTW;
    .locals 1

    .prologue
    .line 2696784
    sget-object v0, LX/JTW;->$VALUES:[LX/JTW;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/JTW;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2696783
    iget-object v0, p0, LX/JTW;->name:Ljava/lang/String;

    return-object v0
.end method
