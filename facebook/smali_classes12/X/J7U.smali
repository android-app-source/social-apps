.class public final enum LX/J7U;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/J7U;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/J7U;

.field public static final enum MESSENGER:LX/J7U;


# instance fields
.field public final appId:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2650885
    new-instance v0, LX/J7U;

    const-string v1, "MESSENGER"

    const-string v2, "256002347743983"

    invoke-direct {v0, v1, v3, v2}, LX/J7U;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J7U;->MESSENGER:LX/J7U;

    .line 2650886
    const/4 v0, 0x1

    new-array v0, v0, [LX/J7U;

    sget-object v1, LX/J7U;->MESSENGER:LX/J7U;

    aput-object v1, v0, v3

    sput-object v0, LX/J7U;->$VALUES:[LX/J7U;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2650889
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2650890
    iput-object p3, p0, LX/J7U;->appId:Ljava/lang/String;

    .line 2650891
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/J7U;
    .locals 1

    .prologue
    .line 2650888
    const-class v0, LX/J7U;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/J7U;

    return-object v0
.end method

.method public static values()[LX/J7U;
    .locals 1

    .prologue
    .line 2650887
    sget-object v0, LX/J7U;->$VALUES:[LX/J7U;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/J7U;

    return-object v0
.end method
