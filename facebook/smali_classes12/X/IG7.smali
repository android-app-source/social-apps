.class public LX/IG7;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/performancelogger/PerformanceLogger;


# direct methods
.method public constructor <init>(Lcom/facebook/performancelogger/PerformanceLogger;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2555299
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2555300
    iput-object p1, p0, LX/IG7;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 2555301
    return-void
.end method

.method public static a(LX/0QB;)LX/IG7;
    .locals 2

    .prologue
    .line 2555296
    new-instance v1, LX/IG7;

    invoke-static {p0}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v0

    check-cast v0, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-direct {v1, v0}, LX/IG7;-><init>(Lcom/facebook/performancelogger/PerformanceLogger;)V

    .line 2555297
    move-object v0, v1

    .line 2555298
    return-object v0
.end method


# virtual methods
.method public final a(LX/IG6;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 2555302
    sget-object v0, LX/IG6;->DASHBOARD_TTI:LX/IG6;

    if-ne p1, v0, :cond_0

    .line 2555303
    iget-object v0, p0, LX/IG7;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    new-instance v1, LX/0Yj;

    iget v2, p1, LX/IG6;->markerId:I

    iget-object v3, p1, LX/IG6;->markerName:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, LX/0Yj;-><init>(ILjava/lang/String;)V

    new-array v2, v5, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "friends_nearby"

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, LX/0Yj;->a([Ljava/lang/String;)LX/0Yj;

    move-result-object v1

    .line 2555304
    iput-boolean v5, v1, LX/0Yj;->n:Z

    .line 2555305
    move-object v1, v1

    .line 2555306
    invoke-interface {v0, v1}, Lcom/facebook/performancelogger/PerformanceLogger;->c(LX/0Yj;)V

    .line 2555307
    :goto_0
    return-void

    .line 2555308
    :cond_0
    iget-object v0, p0, LX/IG7;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    iget v1, p1, LX/IG6;->markerId:I

    iget-object v2, p1, LX/IG6;->markerName:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public final b(LX/IG6;)V
    .locals 3

    .prologue
    .line 2555294
    iget-object v0, p0, LX/IG7;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    iget v1, p1, LX/IG6;->markerId:I

    iget-object v2, p1, LX/IG6;->markerName:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 2555295
    return-void
.end method

.method public final c(LX/IG6;)V
    .locals 3

    .prologue
    .line 2555292
    iget-object v0, p0, LX/IG7;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    iget v1, p1, LX/IG6;->markerId:I

    iget-object v2, p1, LX/IG6;->markerName:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->f(ILjava/lang/String;)V

    .line 2555293
    return-void
.end method
