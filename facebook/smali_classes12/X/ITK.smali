.class public LX/ITK;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/ITJ;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2579252
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 2579253
    return-void
.end method


# virtual methods
.method public final a(LX/IRb;)LX/ITJ;
    .locals 21

    .prologue
    .line 2579254
    new-instance v1, LX/ITJ;

    invoke-static/range {p0 .. p0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v3

    check-cast v3, LX/17W;

    invoke-static/range {p0 .. p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {p0 .. p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v5

    check-cast v5, LX/0Zb;

    invoke-static/range {p0 .. p0}, Lcom/facebook/common/shortcuts/InstallShortcutHelper;->a(LX/0QB;)Lcom/facebook/common/shortcuts/InstallShortcutHelper;

    move-result-object v6

    check-cast v6, Lcom/facebook/common/shortcuts/InstallShortcutHelper;

    invoke-static/range {p0 .. p0}, LX/0kL;->a(LX/0QB;)LX/0kL;

    move-result-object v7

    check-cast v7, LX/0kL;

    invoke-static/range {p0 .. p0}, LX/3mF;->a(LX/0QB;)LX/3mF;

    move-result-object v8

    check-cast v8, LX/3mF;

    const/16 v2, 0xc

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static/range {p0 .. p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v10

    check-cast v10, LX/0Sh;

    invoke-static/range {p0 .. p0}, Lcom/facebook/bookmark/client/BookmarkClient;->a(LX/0QB;)Lcom/facebook/bookmark/client/BookmarkClient;

    move-result-object v11

    check-cast v11, Lcom/facebook/bookmark/client/BookmarkClient;

    invoke-static/range {p0 .. p0}, LX/1Ck;->a(LX/0QB;)LX/1Ck;

    move-result-object v12

    check-cast v12, LX/1Ck;

    const/16 v2, 0x19e

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v14

    check-cast v14, LX/03V;

    invoke-static/range {p0 .. p0}, Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;->a(LX/0QB;)Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;

    move-result-object v15

    check-cast v15, Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;

    invoke-static/range {p0 .. p0}, LX/Jaq;->a(LX/0QB;)LX/Jaq;

    move-result-object v16

    check-cast v16, LX/DVF;

    invoke-static/range {p0 .. p0}, LX/DQW;->a(LX/0QB;)LX/DQW;

    move-result-object v17

    check-cast v17, LX/DQW;

    const/16 v2, 0x2453

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v18

    invoke-static/range {p0 .. p0}, LX/DK3;->a(LX/0QB;)LX/DK3;

    move-result-object v19

    check-cast v19, LX/DK3;

    invoke-static/range {p0 .. p0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v20

    check-cast v20, Ljava/lang/Boolean;

    move-object/from16 v2, p1

    invoke-direct/range {v1 .. v20}, LX/ITJ;-><init>(LX/IRb;LX/17W;Lcom/facebook/content/SecureContextHelper;LX/0Zb;Lcom/facebook/common/shortcuts/InstallShortcutHelper;LX/0kL;LX/3mF;LX/0Or;LX/0Sh;Lcom/facebook/bookmark/client/BookmarkClient;LX/1Ck;LX/0Or;LX/03V;Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;LX/DVF;LX/DQW;LX/0Ot;LX/DK3;Ljava/lang/Boolean;)V

    .line 2579255
    return-object v1
.end method
