.class public final LX/IuJ;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;)V
    .locals 0

    .prologue
    .line 2625971
    iput-object p1, p0, LX/IuJ;->a:Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2625972
    iget-object v0, p0, LX/IuJ;->a:Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;

    const/4 v1, 0x0

    .line 2625973
    iput-object v1, v0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->s:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2625974
    iget-object v0, p0, LX/IuJ;->a:Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;

    sget-object v1, LX/IuM;->FAILED_GENERATE:LX/IuM;

    invoke-static {v0, v1}, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->a$redex0(Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;LX/IuM;)V

    .line 2625975
    sget-object v0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->a:Ljava/lang/Class;

    const-string v1, "Failed to generate pre-keys"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2625976
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2625977
    iget-object v0, p0, LX/IuJ;->a:Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;

    const/4 v1, 0x0

    .line 2625978
    iput-object v1, v0, Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;->s:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2625979
    return-void
.end method
