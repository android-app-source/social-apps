.class public LX/HlH;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/HlH;


# instance fields
.field private final a:LX/0WJ;

.field public final b:LX/0Uo;

.field public final c:LX/0Uq;

.field private final d:LX/0ad;


# direct methods
.method public constructor <init>(LX/0WJ;LX/0Uo;LX/0Uq;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2498306
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2498307
    iput-object p1, p0, LX/HlH;->a:LX/0WJ;

    .line 2498308
    iput-object p2, p0, LX/HlH;->b:LX/0Uo;

    .line 2498309
    iput-object p3, p0, LX/HlH;->c:LX/0Uq;

    .line 2498310
    iput-object p4, p0, LX/HlH;->d:LX/0ad;

    .line 2498311
    return-void
.end method

.method public static a(LX/0QB;)LX/HlH;
    .locals 7

    .prologue
    .line 2498317
    sget-object v0, LX/HlH;->e:LX/HlH;

    if-nez v0, :cond_1

    .line 2498318
    const-class v1, LX/HlH;

    monitor-enter v1

    .line 2498319
    :try_start_0
    sget-object v0, LX/HlH;->e:LX/HlH;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2498320
    if-eqz v2, :cond_0

    .line 2498321
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2498322
    new-instance p0, LX/HlH;

    invoke-static {v0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v3

    check-cast v3, LX/0WJ;

    invoke-static {v0}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v4

    check-cast v4, LX/0Uo;

    invoke-static {v0}, LX/0Uq;->a(LX/0QB;)LX/0Uq;

    move-result-object v5

    check-cast v5, LX/0Uq;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-direct {p0, v3, v4, v5, v6}, LX/HlH;-><init>(LX/0WJ;LX/0Uo;LX/0Uq;LX/0ad;)V

    .line 2498323
    move-object v0, p0

    .line 2498324
    sput-object v0, LX/HlH;->e:LX/HlH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2498325
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2498326
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2498327
    :cond_1
    sget-object v0, LX/HlH;->e:LX/HlH;

    return-object v0

    .line 2498328
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2498329
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static c(LX/HlH;)Z
    .locals 2

    .prologue
    .line 2498312
    iget-object v0, p0, LX/HlH;->a:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HlH;->a:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->c()Lcom/facebook/user/model/User;

    move-result-object v0

    .line 2498313
    iget-object v1, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v1

    .line 2498314
    if-eqz v0, :cond_0

    const-string v0, "0"

    iget-object v1, p0, LX/HlH;->a:LX/0WJ;

    invoke-virtual {v1}, LX/0WJ;->c()Lcom/facebook/user/model/User;

    move-result-object v1

    .line 2498315
    iget-object p0, v1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v1, p0

    .line 2498316
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
