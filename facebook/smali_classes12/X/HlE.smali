.class public LX/HlE;
.super Landroid/widget/VideoView;
.source ""

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;
.implements LX/HlB;


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/net/Uri;

.field private c:LX/Hl9;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/VideoView;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/net/Uri;)V
    .locals 0

    iput-object p1, p0, LX/HlE;->a:Landroid/view/View;

    iput-object p2, p0, LX/HlE;->b:Landroid/net/Uri;

    invoke-virtual {p0, p0}, LX/HlE;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    return-void
.end method

.method public final onPrepared(Landroid/media/MediaPlayer;)V
    .locals 2

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/media/MediaPlayer;->setLooping(Z)V

    new-instance v0, LX/HlA;

    iget-object v1, p0, LX/HlE;->a:Landroid/view/View;

    invoke-direct {v0, v1}, LX/HlA;-><init>(Landroid/view/View;)V

    invoke-virtual {p1, v0}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    return-void
.end method

.method public final pause()V
    .locals 2

    iget-object v0, p0, LX/HlE;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, LX/HlE;->stopPlayback()V

    return-void
.end method

.method public setFrameVideoViewListener(LX/Hl9;)V
    .locals 0

    iput-object p1, p0, LX/HlE;->c:LX/Hl9;

    return-void
.end method

.method public final start()V
    .locals 1

    iget-object v0, p0, LX/HlE;->b:Landroid/net/Uri;

    invoke-virtual {p0, v0}, LX/HlE;->setVideoURI(Landroid/net/Uri;)V

    invoke-super {p0}, Landroid/widget/VideoView;->start()V

    return-void
.end method
