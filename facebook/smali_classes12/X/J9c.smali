.class public final LX/J9c;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/B0L;


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

.field public b:J


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;)V
    .locals 2

    .prologue
    .line 2653126
    iput-object p1, p0, LX/J9c;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2653127
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/J9c;->b:J

    return-void
.end method


# virtual methods
.method public final a(LX/2nf;)V
    .locals 8

    .prologue
    .line 2653103
    if-nez p1, :cond_1

    .line 2653104
    iget-object v0, p0, LX/J9c;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    iget-object v0, v0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->u:LX/J9a;

    if-eqz v0, :cond_0

    .line 2653105
    iget-object v0, p0, LX/J9c;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    iget-object v0, v0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->u:LX/J9a;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/3tK;->a(Landroid/database/Cursor;)V

    .line 2653106
    :cond_0
    :goto_0
    return-void

    .line 2653107
    :cond_1
    iget-object v0, p0, LX/J9c;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    iget-object v0, v0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->v:LX/62k;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/J9c;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    iget-object v0, v0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->I:LX/B0O;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/J9c;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    iget-object v0, v0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->u:LX/J9a;

    if-nez v0, :cond_3

    .line 2653108
    :cond_2
    invoke-interface {p1}, LX/2nf;->close()V

    goto :goto_0

    .line 2653109
    :cond_3
    invoke-interface {p1}, LX/2nf;->getCount()I

    move-result v0

    if-lez v0, :cond_4

    .line 2653110
    iget-object v0, p0, LX/J9c;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    iget-object v0, v0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0xbf0001

    iget-object v2, p0, LX/J9c;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    iget v2, v2, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->J:I

    const/4 v3, 0x2

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 2653111
    iget-object v0, p0, LX/J9c;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    iget-object v0, v0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->c:LX/J8y;

    invoke-virtual {v0}, LX/J8y;->a()V

    .line 2653112
    iget-object v0, p0, LX/J9c;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    invoke-static {v0}, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->e$redex0(Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;)V

    .line 2653113
    invoke-interface {p1}, LX/2nf;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    .line 2653114
    iget-wide v6, p0, LX/J9c;->b:J

    .line 2653115
    const-string v5, "CHANGE_NUMBER"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 2653116
    iput-wide v4, p0, LX/J9c;->b:J

    .line 2653117
    cmp-long v4, v4, v6

    if-lez v4, :cond_5

    const/4 v4, 0x1

    :goto_1
    move v0, v4

    .line 2653118
    if-eqz v0, :cond_4

    .line 2653119
    iget-object v0, p0, LX/J9c;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    const/4 v1, 0x0

    .line 2653120
    iput-boolean v1, v0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->A:Z

    .line 2653121
    iget-object v0, p0, LX/J9c;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    iget-object v0, v0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->x:Lcom/facebook/feed/banner/GenericNotificationBanner;

    if-eqz v0, :cond_4

    .line 2653122
    iget-object v0, p0, LX/J9c;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    iget-object v0, v0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->x:Lcom/facebook/feed/banner/GenericNotificationBanner;

    invoke-virtual {v0}, LX/4nk;->a()V

    .line 2653123
    :cond_4
    iget-object v0, p0, LX/J9c;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    iget-object v1, v0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->u:LX/J9a;

    move-object v0, p1

    check-cast v0, Landroid/database/Cursor;

    invoke-virtual {v1, v0}, LX/3tK;->a(Landroid/database/Cursor;)V

    .line 2653124
    invoke-interface {p1}, LX/2nf;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, LX/J9c;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    iget-object v0, v0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->I:LX/B0O;

    invoke-virtual {v0}, LX/B0O;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2653125
    iget-object v0, p0, LX/J9c;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    iget-object v0, v0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->v:LX/62k;

    invoke-interface {v0}, LX/62k;->f()V

    goto/16 :goto_0

    :cond_5
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2653093
    iget-object v0, p0, LX/J9c;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    invoke-virtual {v0, p1}, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->a(Ljava/lang/Throwable;)V

    .line 2653094
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 2653095
    iget-object v0, p0, LX/J9c;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    iget-object v0, v0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->u:LX/J9a;

    if-eqz v0, :cond_0

    .line 2653096
    iget-object v0, p0, LX/J9c;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    iget-object v0, v0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->u:LX/J9a;

    .line 2653097
    iget-boolean v1, v0, LX/J9a;->u:Z

    if-eq v1, p1, :cond_0

    .line 2653098
    iput-boolean p1, v0, LX/J9a;->u:Z

    .line 2653099
    const v1, 0x41736c06

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2653100
    :cond_0
    iget-object v0, p0, LX/J9c;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    iget-object v0, v0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->v:LX/62k;

    if-eqz v0, :cond_1

    if-nez p1, :cond_1

    .line 2653101
    iget-object v0, p0, LX/J9c;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    iget-object v0, v0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->v:LX/62k;

    invoke-interface {v0}, LX/62k;->f()V

    .line 2653102
    :cond_1
    return-void
.end method
