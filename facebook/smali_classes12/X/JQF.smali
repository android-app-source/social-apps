.class public LX/JQF;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JQD;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JQG;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2691235
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/JQF;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JQG;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2691236
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2691237
    iput-object p1, p0, LX/JQF;->b:LX/0Ot;

    .line 2691238
    return-void
.end method

.method public static a(LX/0QB;)LX/JQF;
    .locals 4

    .prologue
    .line 2691239
    const-class v1, LX/JQF;

    monitor-enter v1

    .line 2691240
    :try_start_0
    sget-object v0, LX/JQF;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2691241
    sput-object v2, LX/JQF;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2691242
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2691243
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2691244
    new-instance v3, LX/JQF;

    const/16 p0, 0x1ff8

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JQF;-><init>(LX/0Ot;)V

    .line 2691245
    move-object v0, v3

    .line 2691246
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2691247
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JQF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2691248
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2691249
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 2691250
    check-cast p2, LX/JQE;

    .line 2691251
    iget-object v0, p0, LX/JQF;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JQG;

    iget-object v1, p2, LX/JQE;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 p2, 0x0

    const/4 p0, 0x1

    .line 2691252
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 2691253
    check-cast v2, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;

    .line 2691254
    iget-object v3, v0, LX/JQG;->a:LX/17S;

    sget-object v4, LX/99r;->Ego:LX/99r;

    invoke-virtual {v3, v4}, LX/17S;->b(LX/99r;)V

    .line 2691255
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 2691256
    iget-object v4, v0, LX/JQG;->a:LX/17S;

    invoke-virtual {v4}, LX/17S;->d()I

    move-result v4

    .line 2691257
    if-lez v4, :cond_0

    const v2, 0x7f0810e2

    new-array v5, p0, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v5, p2

    invoke-virtual {v3, v2, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2691258
    :goto_0
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    const v3, 0x7f0b0050

    invoke-virtual {v2, v3}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, p0}, LX/1ne;->t(I)LX/1ne;

    move-result-object v2

    const v3, 0x7f0a015d

    invoke-virtual {v2, v3}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v2

    sget-object v3, LX/1nd;->CENTER:LX/1nd;

    invoke-virtual {v2, v3}, LX/1ne;->a(LX/1nd;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const v3, 0x7f0b0982

    invoke-interface {v2, p0, v3}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2691259
    return-object v0

    .line 2691260
    :cond_0
    const v4, 0x7f0810de

    new-array v5, p0, [Ljava/lang/Object;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;->k()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, p2

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2691261
    invoke-static {}, LX/1dS;->b()V

    .line 2691262
    const/4 v0, 0x0

    return-object v0
.end method
