.class public final LX/Ibw;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$RideUpdateDestinationMutationModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Ibo;

.field public final synthetic b:LX/Ic4;


# direct methods
.method public constructor <init>(LX/Ic4;LX/Ibo;)V
    .locals 0

    .prologue
    .line 2594946
    iput-object p1, p0, LX/Ibw;->b:LX/Ic4;

    iput-object p2, p0, LX/Ibw;->a:LX/Ibo;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2594943
    iget-object v0, p0, LX/Ibw;->b:LX/Ic4;

    iget-object v0, v0, LX/Ic4;->c:LX/03V;

    sget-object v1, LX/Ic4;->a:Ljava/lang/String;

    const-string v2, "Fail to update destination."

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2594944
    iget-object v0, p0, LX/Ibw;->a:LX/Ibo;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/Ibo;->a(Ljava/lang/String;)V

    .line 2594945
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2594930
    check-cast p1, Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$RideUpdateDestinationMutationModel;

    .line 2594931
    if-nez p1, :cond_0

    .line 2594932
    iget-object v0, p0, LX/Ibw;->a:LX/Ibo;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/Ibo;->a(Ljava/lang/String;)V

    .line 2594933
    :goto_0
    return-void

    .line 2594934
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->SUCCESS:Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    invoke-virtual {p1}, Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$RideUpdateDestinationMutationModel;->j()Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLRideRequestOutcome;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2594935
    iget-object v0, p0, LX/Ibw;->a:LX/Ibo;

    .line 2594936
    iget-object v1, v0, LX/Ibo;->a:LX/Ibp;

    invoke-static {v1}, LX/Ibp;->c(LX/Ibp;)V

    .line 2594937
    iget-object v1, v0, LX/Ibo;->a:LX/Ibp;

    iget-object v1, v1, LX/Ibp;->e:LX/HiV;

    if-eqz v1, :cond_1

    .line 2594938
    iget-object v1, v0, LX/Ibo;->a:LX/Ibp;

    iget-object v1, v1, LX/Ibp;->e:LX/HiV;

    .line 2594939
    iget-object p0, v1, LX/HiV;->a:LX/HiW;

    iget-object p0, p0, LX/HiW;->a:LX/HiX;

    iget-object p0, p0, LX/HiX;->c:LX/HiJ;

    if-eqz p0, :cond_1

    .line 2594940
    iget-object p0, v1, LX/HiV;->a:LX/HiW;

    iget-object p0, p0, LX/HiW;->a:LX/HiX;

    iget-object p0, p0, LX/HiX;->c:LX/HiJ;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/HiJ;->a(Landroid/location/Address;)V

    .line 2594941
    :cond_1
    goto :goto_0

    .line 2594942
    :cond_2
    iget-object v0, p0, LX/Ibw;->a:LX/Ibo;

    invoke-virtual {p1}, Lcom/facebook/messaging/business/ride/graphql/RideMutaionsModels$RideUpdateDestinationMutationModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Ibo;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
