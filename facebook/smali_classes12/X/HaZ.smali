.class public LX/HaZ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/HaZ;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/SecurityCheckupEntryPoint;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2484003
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2484004
    const-string v0, "QP"

    iput-object v0, p0, LX/HaZ;->b:Ljava/lang/String;

    .line 2484005
    iput-object p1, p0, LX/HaZ;->a:LX/0Ot;

    .line 2484006
    return-void
.end method

.method public static a(LX/0QB;)LX/HaZ;
    .locals 4

    .prologue
    .line 2484007
    sget-object v0, LX/HaZ;->c:LX/HaZ;

    if-nez v0, :cond_1

    .line 2484008
    const-class v1, LX/HaZ;

    monitor-enter v1

    .line 2484009
    :try_start_0
    sget-object v0, LX/HaZ;->c:LX/HaZ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2484010
    if-eqz v2, :cond_0

    .line 2484011
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2484012
    new-instance v3, LX/HaZ;

    const/16 p0, 0xafd

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/HaZ;-><init>(LX/0Ot;)V

    .line 2484013
    move-object v0, v3

    .line 2484014
    sput-object v0, LX/HaZ;->c:LX/HaZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2484015
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2484016
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2484017
    :cond_1
    sget-object v0, LX/HaZ;->c:LX/HaZ;

    return-object v0

    .line 2484018
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2484019
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/SecurityCheckupLoggerEvent;
        .end annotation
    .end param

    .prologue
    .line 2484020
    new-instance v0, LX/4JJ;

    invoke-direct {v0}, LX/4JJ;-><init>()V

    .line 2484021
    const-string v1, "event"

    invoke-virtual {v0, v1, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2484022
    move-object v0, v0

    .line 2484023
    iget-object v1, p0, LX/HaZ;->b:Ljava/lang/String;

    .line 2484024
    const-string v2, "source"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2484025
    move-object v0, v0

    .line 2484026
    new-instance v1, LX/Har;

    invoke-direct {v1}, LX/Har;-><init>()V

    move-object v1, v1

    .line 2484027
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2484028
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    .line 2484029
    iget-object v0, p0, LX/HaZ;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2484030
    return-void
.end method
