.class public LX/HjY;
.super Landroid/content/BroadcastReceiver;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Landroid/content/Context;

.field private c:LX/Hk9;

.field private d:LX/HjV;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;LX/HjV;LX/Hk9;)V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    iput-object p1, p0, LX/HjY;->b:Landroid/content/Context;

    iput-object p2, p0, LX/HjY;->a:Ljava/lang/String;

    iput-object p4, p0, LX/HjY;->c:LX/Hk9;

    iput-object p3, p0, LX/HjY;->d:LX/HjV;

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, -0xbfdd005

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v1, v1, v2

    iget-object v2, p0, LX/HjY;->c:LX/Hk9;

    if-eqz v2, :cond_0

    if-nez v1, :cond_1

    :cond_0
    const/16 v1, 0x27

    const v2, 0x1838091a

    invoke-static {p2, v3, v1, v2, v0}, LX/02F;->a(Landroid/content/Intent;IIII)V

    :goto_0
    return-void

    :cond_1
    const-string v2, "com.facebook.ads.interstitial.clicked"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v1, "com.facebook.ads.interstitial.ad.click.url"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.facebook.ads.interstitial.ad.player.handles.click"

    const/4 v3, 0x1

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    iget-object v3, p0, LX/HjY;->c:LX/Hk9;

    invoke-static {}, LX/HkC;->b()V

    iget-object p0, v3, LX/Hk9;->b:LX/HkC;

    iget-object p0, p0, LX/HkC;->a:LX/HjO;

    invoke-virtual {p0}, LX/HjO;->a()V

    invoke-static {v1}, LX/Hky;->a(Ljava/lang/String;)Z

    move-result p0

    if-nez p0, :cond_8

    const/4 p0, 0x1

    :goto_1
    if-eqz v2, :cond_3

    if-eqz p0, :cond_3

    new-instance p0, Landroid/content/Intent;

    const-string p1, "android.intent.action.VIEW"

    invoke-direct {p0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object p1, v3, LX/Hk9;->b:LX/HkC;

    iget-object p1, p1, LX/HkC;->m:LX/Hk0;

    iget-object p1, p1, LX/Hk0;->d:Landroid/content/Context;

    instance-of p1, p1, Landroid/app/Activity;

    if-nez p1, :cond_2

    const/high16 p1, 0x10000000

    invoke-virtual {p0, p1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :cond_2
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    invoke-virtual {p0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    iget-object p1, v3, LX/Hk9;->b:LX/HkC;

    iget-object p1, p1, LX/HkC;->m:LX/Hk0;

    iget-object p1, p1, LX/Hk0;->d:Landroid/content/Context;

    invoke-virtual {p1, p0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    :cond_3
    :goto_2
    const v1, -0x15be27ff

    invoke-static {p2, v1, v0}, LX/02F;->a(Landroid/content/Intent;II)V

    goto :goto_0

    :cond_4
    const-string v2, "com.facebook.ads.interstitial.dismissed"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-static {}, LX/HkC;->b()V

    goto :goto_2

    :cond_5
    const-string v2, "com.facebook.ads.interstitial.displayed"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-static {}, LX/HkC;->b()V

    goto :goto_2

    :cond_6
    const-string v2, "com.facebook.ads.interstitial.impression.logged"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v1, p0, LX/HjY;->c:LX/Hk9;

    invoke-static {}, LX/HkC;->b()V

    iget-object v2, v1, LX/Hk9;->b:LX/HkC;

    iget-object v2, v2, LX/HkC;->a:LX/HjO;

    invoke-virtual {v2}, LX/HjO;->b()V

    goto :goto_2

    :cond_7
    const-string v2, "com.facebook.ads.interstitial.error"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/HjY;->c:LX/Hk9;

    iget-object v2, p0, LX/HjY;->d:LX/HjV;

    invoke-virtual {v1, v2}, LX/Hk9;->b(LX/HjV;)V

    goto :goto_2

    :cond_8
    const/4 p0, 0x0

    goto :goto_1
.end method
