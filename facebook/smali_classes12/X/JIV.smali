.class public final LX/JIV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/JIe;


# direct methods
.method public constructor <init>(LX/JIe;)V
    .locals 0

    .prologue
    .line 2677997
    iput-object p1, p0, LX/JIV;->a:LX/JIe;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x2

    const/4 v1, 0x1

    const v3, -0x5e3001bf

    invoke-static {v0, v1, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2677998
    iget-object v0, p0, LX/JIV;->a:LX/JIe;

    iget-object v0, v0, LX/JIe;->g:LX/Emj;

    sget-object v1, LX/Emo;->SEE_MORE:LX/Emo;

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v3

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v4

    iget-object v5, p0, LX/JIV;->a:LX/JIe;

    iget-object v5, v5, LX/JIe;->l:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    invoke-interface/range {v0 .. v5}, LX/Emj;->a(LX/Emo;Ljava/lang/String;LX/0am;LX/0am;Ljava/lang/Object;)V

    .line 2677999
    iget-object v0, p0, LX/JIV;->a:LX/JIe;

    .line 2678000
    invoke-virtual {v0}, LX/Eme;->a()LX/0am;

    move-result-object v1

    move-object v0, v1

    .line 2678001
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JJF;

    .line 2678002
    iget-object v1, v0, LX/JJF;->j:LX/JJE;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, LX/JJE;->setEnabled(Z)V

    .line 2678003
    iget-object v0, p0, LX/JIV;->a:LX/JIe;

    .line 2678004
    invoke-virtual {v0}, LX/Eme;->a()LX/0am;

    move-result-object v1

    move-object v0, v1

    .line 2678005
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JJF;

    .line 2678006
    iget-object v1, v0, LX/JJF;->k:Landroid/widget/ProgressBar;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2678007
    iget-object v0, p0, LX/JIV;->a:LX/JIe;

    iget-object v0, v0, LX/JIe;->j:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/CurationTagsCardTagsFetcher;

    iget-object v1, p0, LX/JIV;->a:LX/JIe;

    iget-object v1, v1, LX/JIe;->l:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->l()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;

    move-result-object v1

    if-nez v1, :cond_2

    :goto_0
    iget-object v1, p0, LX/JIV;->a:LX/JIe;

    iget-object v1, v1, LX/JIe;->l:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 2678008
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;->a()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;->b()Z

    move-result v7

    if-nez v7, :cond_3

    .line 2678009
    :cond_0
    invoke-static {v2}, Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/CurationTagsCardTagsFetcher;->b(Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;)Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;

    move-result-object v7

    invoke-static {v7}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    .line 2678010
    :goto_1
    move-object v0, v7

    .line 2678011
    iget-object v1, p0, LX/JIV;->a:LX/JIe;

    .line 2678012
    iget-object v2, v1, LX/JIe;->m:LX/0Vd;

    if-nez v2, :cond_1

    .line 2678013
    new-instance v2, LX/JIb;

    invoke-direct {v2, v1, p1}, LX/JIb;-><init>(LX/JIe;Landroid/view/View;)V

    iput-object v2, v1, LX/JIe;->m:LX/0Vd;

    .line 2678014
    :cond_1
    iget-object v2, v1, LX/JIe;->m:LX/0Vd;

    move-object v1, v2

    .line 2678015
    iget-object v2, p0, LX/JIV;->a:LX/JIe;

    iget-object v2, v2, LX/JIe;->a:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2678016
    const v0, -0x61bf36a1

    invoke-static {v0, v6}, LX/02F;->a(II)V

    return-void

    .line 2678017
    :cond_2
    iget-object v1, p0, LX/JIV;->a:LX/JIe;

    iget-object v1, v1, LX/JIe;->l:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->l()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v2

    goto :goto_0

    .line 2678018
    :cond_3
    invoke-static {}, LX/FSs;->b()LX/FSp;

    move-result-object v7

    const-string v8, "bucket_item_id"

    invoke-virtual {v7, v8, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v7

    const-string v8, "tags_cursor"

    invoke-virtual {v2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v7

    const-string v8, "tags_page_size"

    const/16 v9, 0xc

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v7

    check-cast v7, LX/FSp;

    .line 2678019
    invoke-static {v7}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v7

    sget-object v8, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v7, v8}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v7

    sget-object v8, Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/CurationTagsCardTagsFetcher;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 2678020
    iput-object v8, v7, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2678021
    move-object v7, v7

    .line 2678022
    sget-object v8, LX/0zS;->a:LX/0zS;

    invoke-virtual {v7, v8}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v7

    const-wide/32 v9, 0x15180

    invoke-virtual {v7, v9, v10}, LX/0zO;->a(J)LX/0zO;

    move-result-object v7

    const-string v8, "com.facebook.entitycardsplugins.discoverycuration.fetchers.discoveryCurationCardsCacheTag"

    invoke-static {v8}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v8

    .line 2678023
    iput-object v8, v7, LX/0zO;->d:Ljava/util/Set;

    .line 2678024
    move-object v7, v7

    .line 2678025
    iget-object v8, v0, Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/CurationTagsCardTagsFetcher;->b:LX/0tX;

    invoke-virtual {v8, v7}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v7

    .line 2678026
    new-instance v8, LX/JIM;

    invoke-direct {v8, v0, v2}, LX/JIM;-><init>(Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/CurationTagsCardTagsFetcher;Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;)V

    .line 2678027
    sget-object v9, LX/131;->INSTANCE:LX/131;

    move-object v9, v9

    .line 2678028
    invoke-static {v7, v8, v9}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    goto/16 :goto_1
.end method
