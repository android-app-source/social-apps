.class public LX/JM5;
.super LX/5pb;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "FBProfileEditViewEventHandler"
.end annotation


# instance fields
.field private final a:LX/JM7;


# direct methods
.method public constructor <init>(LX/JM7;LX/5pY;)V
    .locals 0
    .param p2    # LX/5pY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2682861
    invoke-direct {p0, p2}, LX/5pb;-><init>(LX/5pY;)V

    .line 2682862
    iput-object p1, p0, LX/JM5;->a:LX/JM7;

    .line 2682863
    return-void
.end method


# virtual methods
.method public didCompleteProfileEditing()V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2682859
    iget-object v0, p0, LX/JM5;->a:LX/JM7;

    new-instance v1, LX/JM4;

    invoke-direct {v1}, LX/JM4;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2682860
    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2682858
    const-string v0, "FBProfileEditViewEventHandler"

    return-object v0
.end method
