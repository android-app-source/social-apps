.class public final LX/I3y;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 2532328
    const-class v1, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;

    const v0, 0x43d3c9cf

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x3

    const-string v5, "EventCollectionsQuery"

    const-string v6, "8c683bd8ff484b448a9c6cd462832039"

    const-string v7, "node"

    const-string v8, "10155217777146729"

    const/4 v9, 0x0

    .line 2532329
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 2532330
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2532331
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2532298
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2532299
    sparse-switch v0, :sswitch_data_0

    .line 2532300
    :goto_0
    return-object p1

    .line 2532301
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 2532302
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 2532303
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 2532304
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 2532305
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 2532306
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 2532307
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 2532308
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 2532309
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 2532310
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 2532311
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 2532312
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 2532313
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    .line 2532314
    :sswitch_d
    const-string p1, "13"

    goto :goto_0

    .line 2532315
    :sswitch_e
    const-string p1, "14"

    goto :goto_0

    .line 2532316
    :sswitch_f
    const-string p1, "15"

    goto :goto_0

    .line 2532317
    :sswitch_10
    const-string p1, "16"

    goto :goto_0

    .line 2532318
    :sswitch_11
    const-string p1, "17"

    goto :goto_0

    .line 2532319
    :sswitch_12
    const-string p1, "18"

    goto :goto_0

    .line 2532320
    :sswitch_13
    const-string p1, "19"

    goto :goto_0

    .line 2532321
    :sswitch_14
    const-string p1, "20"

    goto :goto_0

    .line 2532322
    :sswitch_15
    const-string p1, "21"

    goto :goto_0

    .line 2532323
    :sswitch_16
    const-string p1, "22"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x41a91745 -> :sswitch_13
        -0x33f8633e -> :sswitch_d
        -0x29107a89 -> :sswitch_0
        -0x15afd767 -> :sswitch_4
        -0x38aa96a -> :sswitch_8
        0x180aba4 -> :sswitch_15
        0x2ef6341 -> :sswitch_c
        0x683094a -> :sswitch_a
        0x291d8de0 -> :sswitch_16
        0x2e61b03a -> :sswitch_7
        0x3052e0ff -> :sswitch_e
        0x45e5f0b4 -> :sswitch_5
        0x4b46b5f1 -> :sswitch_f
        0x4c6d50cb -> :sswitch_11
        0x5f424068 -> :sswitch_14
        0x61bc9553 -> :sswitch_12
        0x66c8fb9c -> :sswitch_6
        0x6d2645b9 -> :sswitch_10
        0x7191d8b1 -> :sswitch_b
        0x73a026b5 -> :sswitch_3
        0x78668257 -> :sswitch_9
        0x78a3267b -> :sswitch_2
        0x7e6ceb8b -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2532324
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 2532325
    :goto_1
    return v0

    .line 2532326
    :pswitch_0
    const-string v2, "14"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    .line 2532327
    :pswitch_1
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x623
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
