.class public LX/HNH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/H8Z;


# static fields
.field private static final a:I

.field private static final b:I

.field private static final c:I


# instance fields
.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Landroid/content/Context;

.field private final f:Z

.field public g:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2458345
    const v0, 0x7f0208fd

    sput v0, LX/HNH;->a:I

    .line 2458346
    const v0, 0x7f083679

    sput v0, LX/HNH;->b:I

    .line 2458347
    const v0, 0x7f083677

    sput v0, LX/HNH;->c:I

    return-void
.end method

.method public constructor <init>(LX/0Ot;Landroid/content/Context;Ljava/lang/Boolean;)V
    .locals 1
    .param p3    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;",
            "Landroid/content/Context;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2458348
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2458349
    iput-object p1, p0, LX/HNH;->d:LX/0Ot;

    .line 2458350
    iput-object p2, p0, LX/HNH;->e:Landroid/content/Context;

    .line 2458351
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/HNH;->f:Z

    .line 2458352
    return-void
.end method


# virtual methods
.method public final a()LX/HA7;
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 2458353
    new-instance v0, LX/HA7;

    sget-object v1, LX/HNG;->COPY_TAB_LINK:LX/HNG;

    invoke-virtual {v1}, LX/HNG;->ordinal()I

    move-result v1

    iget-boolean v2, p0, LX/HNH;->f:Z

    if-eqz v2, :cond_0

    sget v2, LX/HNH;->c:I

    :goto_0
    sget v3, LX/HNH;->a:I

    iget-object v5, p0, LX/HNH;->g:Ljava/lang/String;

    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    move v5, v4

    :goto_1
    invoke-direct/range {v0 .. v5}, LX/HA7;-><init>(IIIIZ)V

    return-object v0

    :cond_0
    sget v2, LX/HNH;->b:I

    goto :goto_0

    :cond_1
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public final b()LX/HA7;
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 2458354
    new-instance v0, LX/HA7;

    sget-object v1, LX/HNG;->COPY_TAB_LINK:LX/HNG;

    invoke-virtual {v1}, LX/HNG;->ordinal()I

    move-result v1

    iget-boolean v2, p0, LX/HNH;->f:Z

    if-eqz v2, :cond_0

    sget v2, LX/HNH;->c:I

    :goto_0
    sget v3, LX/HNH;->a:I

    move v5, v4

    invoke-direct/range {v0 .. v5}, LX/HA7;-><init>(IIIIZ)V

    return-object v0

    :cond_0
    sget v2, LX/HNH;->b:I

    goto :goto_0
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 2458355
    iget-object v0, p0, LX/HNH;->e:Landroid/content/Context;

    iget-object v1, p0, LX/HNH;->g:Ljava/lang/String;

    invoke-static {v0, v1}, LX/47Y;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 2458356
    iget-object v0, p0, LX/HNH;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    iget-object v2, p0, LX/HNH;->e:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08367e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2458357
    return-void
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/H8E;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2458358
    const/4 v0, 0x0

    return-object v0
.end method
