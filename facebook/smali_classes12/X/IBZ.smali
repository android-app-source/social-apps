.class public LX/IBZ;
.super Lcom/facebook/resources/ui/FbTextView;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements LX/IBX;


# instance fields
.field public a:LX/1nQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/I76;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/IBl;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/BiT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/DB4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private f:Lcom/facebook/events/common/EventAnalyticsParams;

.field private g:Lcom/facebook/events/model/Event;

.field public h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 2547259
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;)V

    .line 2547260
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, LX/IBZ;

    invoke-static {v0}, LX/1nQ;->b(LX/0QB;)LX/1nQ;

    move-result-object v3

    check-cast v3, LX/1nQ;

    invoke-static {v0}, LX/I76;->b(LX/0QB;)LX/I76;

    move-result-object v4

    check-cast v4, LX/I76;

    invoke-static {v0}, LX/IBl;->b(LX/0QB;)LX/IBl;

    move-result-object v5

    check-cast v5, LX/IBl;

    invoke-static {v0}, LX/BiT;->a(LX/0QB;)LX/BiT;

    move-result-object p1

    check-cast p1, LX/BiT;

    invoke-static {v0}, LX/DB4;->b(LX/0QB;)LX/DB4;

    move-result-object v0

    check-cast v0, LX/DB4;

    iput-object v3, v2, LX/IBZ;->a:LX/1nQ;

    iput-object v4, v2, LX/IBZ;->b:LX/I76;

    iput-object v5, v2, LX/IBZ;->c:LX/IBl;

    iput-object p1, v2, LX/IBZ;->d:LX/BiT;

    iput-object v0, v2, LX/IBZ;->e:LX/DB4;

    .line 2547261
    const/4 v0, 0x1

    move v0, v0

    .line 2547262
    iput-boolean v0, p0, LX/IBZ;->h:Z

    .line 2547263
    invoke-virtual {p0, p0}, LX/IBZ;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2547264
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/events/common/EventAnalyticsParams;Z)V
    .locals 7
    .param p2    # Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 2547265
    iput-object p1, p0, LX/IBZ;->g:Lcom/facebook/events/model/Event;

    .line 2547266
    iput-object p3, p0, LX/IBZ;->f:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2547267
    iget-boolean v0, p0, LX/IBZ;->h:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/facebook/events/model/Event;->a(Lcom/facebook/events/model/Event;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 2547268
    :goto_0
    iget-object v1, p0, LX/IBZ;->g:Lcom/facebook/events/model/Event;

    .line 2547269
    iget-object v3, v1, Lcom/facebook/events/model/Event;->aj:Lcom/facebook/events/model/EventUser;

    move-object v1, v3

    .line 2547270
    if-nez v1, :cond_1

    move-object v1, v2

    .line 2547271
    :goto_1
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2547272
    const-string v1, ""

    invoke-virtual {p0, v1}, LX/IBZ;->setText(Ljava/lang/CharSequence;)V

    .line 2547273
    :goto_2
    if-eqz v0, :cond_3

    const v0, 0x7f02033a

    .line 2547274
    :goto_3
    iget-object v1, p0, LX/IBZ;->c:LX/IBl;

    invoke-virtual {v1, p0, v0, p4}, LX/IBl;->a(Landroid/widget/TextView;IZ)V

    .line 2547275
    return-void

    .line 2547276
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2547277
    :cond_1
    iget-object v3, v1, Lcom/facebook/events/model/EventUser;->c:Ljava/lang/String;

    move-object v1, v3

    .line 2547278
    goto :goto_1

    .line 2547279
    :cond_2
    iget-object v3, p0, LX/IBZ;->c:LX/IBl;

    iget-object v4, p0, LX/IBZ;->e:LX/DB4;

    iget-object v5, p0, LX/IBZ;->g:Lcom/facebook/events/model/Event;

    .line 2547280
    iget-object v6, v5, Lcom/facebook/events/model/Event;->n:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    move-object v5, v6

    .line 2547281
    iget-object v6, p0, LX/IBZ;->g:Lcom/facebook/events/model/Event;

    .line 2547282
    iget-object p1, v6, Lcom/facebook/events/model/Event;->m:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-object v6, p1

    .line 2547283
    invoke-virtual {v4, v1, v5, v6}, LX/DB4;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEventActionStyle;Lcom/facebook/graphql/enums/GraphQLConnectionStyle;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, p0, v1, v2, p4}, LX/IBl;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)V

    goto :goto_2

    .line 2547284
    :cond_3
    const v0, 0x7f020338

    goto :goto_3
.end method

.method public final a(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)Z
    .locals 1
    .param p2    # Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2547285
    iget-boolean v0, p1, Lcom/facebook/events/model/Event;->H:Z

    move v0, v0

    .line 2547286
    if-eqz v0, :cond_0

    .line 2547287
    iget-object v0, p1, Lcom/facebook/events/model/Event;->aj:Lcom/facebook/events/model/EventUser;

    move-object v0, v0

    .line 2547288
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x7d107cd9

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2547289
    iget-object v0, p0, LX/IBZ;->g:Lcom/facebook/events/model/Event;

    if-eqz v0, :cond_0

    .line 2547290
    iget-object v0, p0, LX/IBZ;->a:LX/1nQ;

    iget-object v2, p0, LX/IBZ;->g:Lcom/facebook/events/model/Event;

    .line 2547291
    iget-object v3, v2, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2547292
    iget-object v3, p0, LX/IBZ;->f:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v3, v3, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 2547293
    iget-object v4, v3, Lcom/facebook/events/common/EventActionContext;->e:Lcom/facebook/events/common/ActionSource;

    move-object v3, v4

    .line 2547294
    invoke-virtual {v3}, Lcom/facebook/events/common/ActionSource;->getParamValue()I

    move-result v3

    iget-object v4, p0, LX/IBZ;->f:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v4, v4, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 2547295
    iget-object p1, v4, Lcom/facebook/events/common/EventActionContext;->f:Lcom/facebook/events/common/ActionSource;

    move-object v4, p1

    .line 2547296
    invoke-virtual {v4}, Lcom/facebook/events/common/ActionSource;->getParamValue()I

    move-result v4

    const/4 p1, 0x0

    .line 2547297
    iget-object v5, v0, LX/1nQ;->i:LX/0Zb;

    const-string v6, "event_invited_by_summary_click"

    invoke-interface {v5, v6, p1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v5

    .line 2547298
    invoke-virtual {v5}, LX/0oG;->a()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2547299
    const-string v6, "event_permalink"

    invoke-virtual {v5, v6}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v5

    iget-object v6, v0, LX/1nQ;->j:LX/0kv;

    iget-object v7, v0, LX/1nQ;->g:Landroid/content/Context;

    invoke-virtual {v6, v7}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    move-result-object v5

    const-string v6, "Event"

    invoke-virtual {v5, v6}, LX/0oG;->b(Ljava/lang/String;)LX/0oG;

    move-result-object v5

    invoke-virtual {v5, v2}, LX/0oG;->c(Ljava/lang/String;)LX/0oG;

    move-result-object v5

    const-string v6, "action_source"

    invoke-virtual {v5, v6, v3}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    move-result-object v5

    const-string v6, "action_ref"

    invoke-virtual {v5, v6, v4}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    move-result-object v5

    const-string v6, "has_installed_launcher"

    invoke-virtual {v5, v6, p1}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v5

    invoke-virtual {v5}, LX/0oG;->d()V

    .line 2547300
    :cond_0
    iget-object v0, p0, LX/IBZ;->g:Lcom/facebook/events/model/Event;

    if-nez v0, :cond_2

    const/4 v0, 0x0

    .line 2547301
    :goto_0
    if-eqz v0, :cond_1

    .line 2547302
    iget-object v2, p0, LX/IBZ;->b:LX/I76;

    invoke-virtual {p0}, LX/IBZ;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, LX/I76;->a(Landroid/content/Context;Lcom/facebook/events/model/EventUser;)V

    .line 2547303
    :cond_1
    const v0, -0x3399c228    # -6.0356448E7f

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 2547304
    :cond_2
    iget-object v0, p0, LX/IBZ;->g:Lcom/facebook/events/model/Event;

    .line 2547305
    iget-object v2, v0, Lcom/facebook/events/model/Event;->aj:Lcom/facebook/events/model/EventUser;

    move-object v0, v2

    .line 2547306
    goto :goto_0
.end method
