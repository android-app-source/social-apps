.class public LX/I2t;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/I2l;


# instance fields
.field private a:Lcom/facebook/base/fragment/FbFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/base/fragment/FbFragment;)V
    .locals 3
    .param p1    # Lcom/facebook/base/fragment/FbFragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2530881
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2530882
    if-eqz p1, :cond_0

    instance-of v0, p1, LX/Hws;

    if-nez v0, :cond_0

    .line 2530883
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not implement BaseEventsDashboardFragment."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2530884
    :cond_0
    iput-object p1, p0, LX/I2t;->a:Lcom/facebook/base/fragment/FbFragment;

    .line 2530885
    return-void
.end method


# virtual methods
.method public final x()Lcom/facebook/base/fragment/FbFragment;
    .locals 1

    .prologue
    .line 2530886
    iget-object v0, p0, LX/I2t;->a:Lcom/facebook/base/fragment/FbFragment;

    return-object v0
.end method
