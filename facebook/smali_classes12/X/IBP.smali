.class public final enum LX/IBP;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IBP;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IBP;

.field public static final enum CACHED_STORIES_INITIAL_FETCH:LX/IBP;

.field public static final enum CREATE_VIEW:LX/IBP;

.field public static final enum DB_FETCH:LX/IBP;

.field public static final enum EARLY_FETCH:LX/IBP;

.field public static final enum FRESH_STORIES_INITIAL_FETCH:LX/IBP;

.field public static final enum NETWORK_FETCH:LX/IBP;

.field public static final enum REACTION_FIRST_PAGE_NETWORK_FETCH:LX/IBP;

.field public static final enum RENDERING:LX/IBP;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2547005
    new-instance v0, LX/IBP;

    const-string v1, "EARLY_FETCH"

    invoke-direct {v0, v1, v3}, LX/IBP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IBP;->EARLY_FETCH:LX/IBP;

    .line 2547006
    new-instance v0, LX/IBP;

    const-string v1, "CREATE_VIEW"

    invoke-direct {v0, v1, v4}, LX/IBP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IBP;->CREATE_VIEW:LX/IBP;

    .line 2547007
    new-instance v0, LX/IBP;

    const-string v1, "DB_FETCH"

    invoke-direct {v0, v1, v5}, LX/IBP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IBP;->DB_FETCH:LX/IBP;

    .line 2547008
    new-instance v0, LX/IBP;

    const-string v1, "NETWORK_FETCH"

    invoke-direct {v0, v1, v6}, LX/IBP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IBP;->NETWORK_FETCH:LX/IBP;

    .line 2547009
    new-instance v0, LX/IBP;

    const-string v1, "RENDERING"

    invoke-direct {v0, v1, v7}, LX/IBP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IBP;->RENDERING:LX/IBP;

    .line 2547010
    new-instance v0, LX/IBP;

    const-string v1, "REACTION_FIRST_PAGE_NETWORK_FETCH"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/IBP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IBP;->REACTION_FIRST_PAGE_NETWORK_FETCH:LX/IBP;

    .line 2547011
    new-instance v0, LX/IBP;

    const-string v1, "CACHED_STORIES_INITIAL_FETCH"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/IBP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IBP;->CACHED_STORIES_INITIAL_FETCH:LX/IBP;

    .line 2547012
    new-instance v0, LX/IBP;

    const-string v1, "FRESH_STORIES_INITIAL_FETCH"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/IBP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IBP;->FRESH_STORIES_INITIAL_FETCH:LX/IBP;

    .line 2547013
    const/16 v0, 0x8

    new-array v0, v0, [LX/IBP;

    sget-object v1, LX/IBP;->EARLY_FETCH:LX/IBP;

    aput-object v1, v0, v3

    sget-object v1, LX/IBP;->CREATE_VIEW:LX/IBP;

    aput-object v1, v0, v4

    sget-object v1, LX/IBP;->DB_FETCH:LX/IBP;

    aput-object v1, v0, v5

    sget-object v1, LX/IBP;->NETWORK_FETCH:LX/IBP;

    aput-object v1, v0, v6

    sget-object v1, LX/IBP;->RENDERING:LX/IBP;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/IBP;->REACTION_FIRST_PAGE_NETWORK_FETCH:LX/IBP;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/IBP;->CACHED_STORIES_INITIAL_FETCH:LX/IBP;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/IBP;->FRESH_STORIES_INITIAL_FETCH:LX/IBP;

    aput-object v2, v0, v1

    sput-object v0, LX/IBP;->$VALUES:[LX/IBP;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2547014
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IBP;
    .locals 1

    .prologue
    .line 2547004
    const-class v0, LX/IBP;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IBP;

    return-object v0
.end method

.method public static values()[LX/IBP;
    .locals 1

    .prologue
    .line 2547003
    sget-object v0, LX/IBP;->$VALUES:[LX/IBP;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IBP;

    return-object v0
.end method
