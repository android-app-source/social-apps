.class public final enum LX/HZz;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HZz;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/HZz;

.field public static final enum AUTOCOMPLETE:LX/HZz;

.field public static final enum PREFILL:LX/HZz;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2482960
    new-instance v0, LX/HZz;

    const-string v1, "PREFILL"

    invoke-direct {v0, v1, v2}, LX/HZz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HZz;->PREFILL:LX/HZz;

    .line 2482961
    new-instance v0, LX/HZz;

    const-string v1, "AUTOCOMPLETE"

    invoke-direct {v0, v1, v3}, LX/HZz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HZz;->AUTOCOMPLETE:LX/HZz;

    .line 2482962
    const/4 v0, 0x2

    new-array v0, v0, [LX/HZz;

    sget-object v1, LX/HZz;->PREFILL:LX/HZz;

    aput-object v1, v0, v2

    sget-object v1, LX/HZz;->AUTOCOMPLETE:LX/HZz;

    aput-object v1, v0, v3

    sput-object v0, LX/HZz;->$VALUES:[LX/HZz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2482964
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HZz;
    .locals 1

    .prologue
    .line 2482965
    const-class v0, LX/HZz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HZz;

    return-object v0
.end method

.method public static values()[LX/HZz;
    .locals 1

    .prologue
    .line 2482963
    sget-object v0, LX/HZz;->$VALUES:[LX/HZz;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HZz;

    return-object v0
.end method
