.class public LX/JVx;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/17Q;

.field public final b:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;LX/17Q;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2701898
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2701899
    iput-object p2, p0, LX/JVx;->a:LX/17Q;

    .line 2701900
    iput-object p1, p0, LX/JVx;->b:LX/0Zb;

    .line 2701901
    return-void
.end method

.method public static a(LX/0QB;)LX/JVx;
    .locals 5

    .prologue
    .line 2701902
    const-class v1, LX/JVx;

    monitor-enter v1

    .line 2701903
    :try_start_0
    sget-object v0, LX/JVx;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2701904
    sput-object v2, LX/JVx;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2701905
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2701906
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2701907
    new-instance p0, LX/JVx;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v4

    check-cast v4, LX/17Q;

    invoke-direct {p0, v3, v4}, LX/JVx;-><init>(LX/0Zb;LX/17Q;)V

    .line 2701908
    move-object v0, p0

    .line 2701909
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2701910
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JVx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2701911
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2701912
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 2701913
    invoke-static {p1}, LX/1fz;->a(Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;)LX/162;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;->o()Lcom/facebook/graphql/enums/GraphQLPYMACategory;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, LX/4Zk;->a(Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, LX/4Zk;->b(Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;)Ljava/lang/String;

    move-result-object v3

    .line 2701914
    invoke-static {v0}, LX/17Q;->F(LX/0lF;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2701915
    const/4 v4, 0x0

    .line 2701916
    :goto_0
    move-object v0, v4

    .line 2701917
    iget-object v1, p0, LX/JVx;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2701918
    return-void

    .line 2701919
    :cond_0
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "pyma_error"

    invoke-direct {v4, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "tracking"

    invoke-virtual {v4, p1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string p1, "pyma_category"

    invoke-virtual {v4, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string p1, "error_msg"

    invoke-virtual {v4, p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string p1, "page_id"

    invoke-virtual {v4, p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string p1, "pyma_additional_info"

    invoke-virtual {v4, p1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string p1, "native_newsfeed"

    .line 2701920
    iput-object p1, v4, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2701921
    move-object v4, v4

    .line 2701922
    goto :goto_0
.end method
