.class public abstract LX/IA6;
.super LX/622;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/622",
        "<",
        "Lcom/facebook/events/model/EventUser;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/model/EventUser;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/IA5;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2544679
    invoke-direct {p0, p1}, LX/622;-><init>(Ljava/lang/String;)V

    .line 2544680
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/IA6;->b:Ljava/util/List;

    .line 2544681
    invoke-virtual {p0}, LX/IA6;->g()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/IA6;->a:Ljava/util/List;

    .line 2544682
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "LX/IA5;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2544674
    invoke-direct {p0, p1}, LX/IA6;-><init>(Ljava/lang/String;)V

    .line 2544675
    iget-object v0, p0, LX/IA6;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2544676
    return-void
.end method


# virtual methods
.method public abstract a(Lcom/facebook/events/model/EventUser;)V
.end method

.method public abstract a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
.end method

.method public abstract a(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/model/EventUser;",
            ">;)V"
        }
    .end annotation
.end method

.method public final d()Ljava/util/List;
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "PublicMethodReturnMutableCollection"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/model/EventUser;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2544677
    iget-object v0, p0, LX/IA6;->a:Ljava/util/List;

    return-object v0
.end method

.method public final e()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/model/EventUser;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2544678
    iget-object v0, p0, LX/IA6;->a:Ljava/util/List;

    return-object v0
.end method

.method public abstract g()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/model/EventUser;",
            ">;"
        }
    .end annotation
.end method
