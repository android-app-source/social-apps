.class public LX/J31;
.super Landroid/widget/BaseAdapter;
.source ""


# instance fields
.field public a:LX/J3F;

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/J3T;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lcom/facebook/payments/sample/PaymentsFlowSampleData;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2641870
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 2641871
    return-void
.end method

.method private a(I)LX/J3T;
    .locals 1

    .prologue
    .line 2641983
    iget-object v0, p0, LX/J31;->b:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/J3T;

    return-object v0
.end method

.method public static a(Landroid/view/ViewGroup;LX/J3T;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2641980
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030881

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 2641981
    invoke-virtual {p1}, LX/J3T;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2641982
    return-object v0
.end method

.method public static a(LX/J31;Landroid/view/ViewGroup;LX/J3T;Z)Landroid/view/ViewGroup;
    .locals 3

    .prologue
    .line 2641975
    const v0, 0x7f03145d

    invoke-static {p1, v0, p2}, LX/J31;->a(Landroid/view/ViewGroup;ILX/J3T;)Landroid/view/ViewGroup;

    move-result-object v1

    .line 2641976
    const v0, 0x7f0d1a93

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/SwitchCompat;

    .line 2641977
    invoke-virtual {v0, p3}, Lcom/facebook/widget/SwitchCompat;->setChecked(Z)V

    .line 2641978
    new-instance v2, LX/J2z;

    invoke-direct {v2, p0, p2}, LX/J2z;-><init>(LX/J31;LX/J3T;)V

    invoke-virtual {v0, v2}, Lcom/facebook/widget/SwitchCompat;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2641979
    return-object v1
.end method

.method private static a(Landroid/view/ViewGroup;ILX/J3T;)Landroid/view/ViewGroup;
    .locals 3
    .param p1    # I
        .annotation build Landroid/support/annotation/LayoutRes;
        .end annotation
    .end param

    .prologue
    .line 2641971
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2641972
    const v1, 0x7f0d0beb

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    .line 2641973
    invoke-virtual {p2}, LX/J3T;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2641974
    return-object v0
.end method

.method public static a(Landroid/view/ViewGroup;LX/J3T;Ljava/lang/String;)Landroid/view/ViewGroup;
    .locals 2

    .prologue
    .line 2641984
    const v0, 0x7f031492

    invoke-static {p0, v0, p1}, LX/J31;->a(Landroid/view/ViewGroup;ILX/J3T;)Landroid/view/ViewGroup;

    move-result-object v1

    .line 2641985
    const v0, 0x7f0d0cbc

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2641986
    invoke-virtual {v0, p2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2641987
    return-object v1
.end method


# virtual methods
.method public final getCount()I
    .locals 1

    .prologue
    .line 2641970
    iget-object v0, p0, LX/J31;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2641969
    invoke-direct {p0, p1}, LX/J31;->a(I)LX/J3T;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2641968
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    .line 2641872
    sget-object v0, LX/J30;->a:[I

    invoke-direct {p0, p1}, LX/J31;->a(I)LX/J3T;

    move-result-object v1

    invoke-virtual {v1}, LX/J3T;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2641873
    :goto_0
    return-object p2

    .line 2641874
    :pswitch_0
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030c0c

    const/4 p0, 0x0

    invoke-virtual {v0, v1, p3, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 2641875
    move-object p2, v0

    .line 2641876
    goto :goto_0

    .line 2641877
    :pswitch_1
    sget-object v0, LX/J3T;->HEADER_TITLE_BAR:LX/J3T;

    invoke-static {p3, v0}, LX/J31;->a(Landroid/view/ViewGroup;LX/J3T;)Landroid/view/View;

    move-result-object v0

    move-object p2, v0

    .line 2641878
    goto :goto_0

    .line 2641879
    :pswitch_2
    sget-object v0, LX/J3T;->USE_APP_ICON:LX/J3T;

    iget-object v1, p0, LX/J31;->c:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-boolean v1, v1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->a:Z

    invoke-static {p0, p3, v0, v1}, LX/J31;->a(LX/J31;Landroid/view/ViewGroup;LX/J3T;Z)Landroid/view/ViewGroup;

    move-result-object v0

    move-object p2, v0

    .line 2641880
    goto :goto_0

    .line 2641881
    :pswitch_3
    sget-object v0, LX/J3T;->HEADER_ENTITY:LX/J3T;

    invoke-static {p3, v0}, LX/J31;->a(Landroid/view/ViewGroup;LX/J3T;)Landroid/view/View;

    move-result-object v0

    move-object p2, v0

    .line 2641882
    goto :goto_0

    .line 2641883
    :pswitch_4
    sget-object v0, LX/J3T;->USE_ENTITY:LX/J3T;

    iget-object v1, p0, LX/J31;->c:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-boolean v1, v1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->b:Z

    invoke-static {p0, p3, v0, v1}, LX/J31;->a(LX/J31;Landroid/view/ViewGroup;LX/J3T;Z)Landroid/view/ViewGroup;

    move-result-object v0

    move-object p2, v0

    .line 2641884
    goto :goto_0

    .line 2641885
    :pswitch_5
    const/4 v2, 0x0

    .line 2641886
    sget-object v0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->a:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2641887
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object p2, p0, LX/J31;->c:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-object p2, p2, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->c:Ljava/lang/String;

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2641888
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2641889
    :goto_1
    sget-object v1, LX/J3T;->ENTITY_IMAGE:LX/J3T;

    invoke-static {p3, v1, v0}, LX/J31;->a(Landroid/view/ViewGroup;LX/J3T;Ljava/lang/String;)Landroid/view/ViewGroup;

    move-result-object v0

    move-object p2, v0

    .line 2641890
    goto :goto_0

    .line 2641891
    :pswitch_6
    sget-object v0, LX/J3T;->ENTITY_TITLE:LX/J3T;

    iget-object v1, p0, LX/J31;->c:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-object v1, v1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->d:Ljava/lang/String;

    invoke-static {p3, v0, v1}, LX/J31;->a(Landroid/view/ViewGroup;LX/J3T;Ljava/lang/String;)Landroid/view/ViewGroup;

    move-result-object v0

    move-object p2, v0

    .line 2641892
    goto :goto_0

    .line 2641893
    :pswitch_7
    sget-object v0, LX/J3T;->ENTITY_SUBTITLE:LX/J3T;

    iget-object v1, p0, LX/J31;->c:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-object v1, v1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->e:Ljava/lang/String;

    invoke-static {p3, v0, v1}, LX/J31;->a(Landroid/view/ViewGroup;LX/J3T;Ljava/lang/String;)Landroid/view/ViewGroup;

    move-result-object v0

    move-object p2, v0

    .line 2641894
    goto/16 :goto_0

    .line 2641895
    :pswitch_8
    sget-object v0, LX/J3T;->ENTITY_DESCRIPTION:LX/J3T;

    iget-object v1, p0, LX/J31;->c:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-object v1, v1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->f:Ljava/lang/String;

    invoke-static {p3, v0, v1}, LX/J31;->a(Landroid/view/ViewGroup;LX/J3T;Ljava/lang/String;)Landroid/view/ViewGroup;

    move-result-object v0

    move-object p2, v0

    .line 2641896
    goto/16 :goto_0

    .line 2641897
    :pswitch_9
    sget-object v0, LX/J3T;->HEADER_ITEM_INFO:LX/J3T;

    invoke-static {p3, v0}, LX/J31;->a(Landroid/view/ViewGroup;LX/J3T;)Landroid/view/View;

    move-result-object v0

    move-object p2, v0

    .line 2641898
    goto/16 :goto_0

    .line 2641899
    :pswitch_a
    sget-object v0, LX/J3T;->ITEM_INFO:LX/J3T;

    iget-object v1, p0, LX/J31;->c:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-boolean v1, v1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->g:Z

    invoke-static {p0, p3, v0, v1}, LX/J31;->a(LX/J31;Landroid/view/ViewGroup;LX/J3T;Z)Landroid/view/ViewGroup;

    move-result-object v0

    move-object p2, v0

    .line 2641900
    goto/16 :goto_0

    .line 2641901
    :pswitch_b
    const/4 v2, 0x0

    .line 2641902
    sget-object v0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->a:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2641903
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object p2, p0, LX/J31;->c:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-object p2, p2, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->h:Ljava/lang/String;

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2641904
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2641905
    :goto_2
    sget-object v1, LX/J3T;->ITEM_IMAGE:LX/J3T;

    invoke-static {p3, v1, v0}, LX/J31;->a(Landroid/view/ViewGroup;LX/J3T;Ljava/lang/String;)Landroid/view/ViewGroup;

    move-result-object v0

    move-object p2, v0

    .line 2641906
    goto/16 :goto_0

    .line 2641907
    :pswitch_c
    sget-object v0, LX/J3T;->ITEM_TITLE:LX/J3T;

    iget-object v1, p0, LX/J31;->c:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-object v1, v1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->i:Ljava/lang/String;

    invoke-static {p3, v0, v1}, LX/J31;->a(Landroid/view/ViewGroup;LX/J3T;Ljava/lang/String;)Landroid/view/ViewGroup;

    move-result-object v0

    move-object p2, v0

    .line 2641908
    goto/16 :goto_0

    .line 2641909
    :pswitch_d
    sget-object v0, LX/J3T;->ITEM_SUBTITLE:LX/J3T;

    iget-object v1, p0, LX/J31;->c:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-object v1, v1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->j:Ljava/lang/String;

    invoke-static {p3, v0, v1}, LX/J31;->a(Landroid/view/ViewGroup;LX/J3T;Ljava/lang/String;)Landroid/view/ViewGroup;

    move-result-object v0

    move-object p2, v0

    .line 2641910
    goto/16 :goto_0

    .line 2641911
    :pswitch_e
    sget-object v0, LX/J3T;->ITEM_SUB_SUBTITLE:LX/J3T;

    iget-object v1, p0, LX/J31;->c:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-object v1, v1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->k:Ljava/lang/String;

    invoke-static {p3, v0, v1}, LX/J31;->a(Landroid/view/ViewGroup;LX/J3T;Ljava/lang/String;)Landroid/view/ViewGroup;

    move-result-object v0

    move-object p2, v0

    .line 2641912
    goto/16 :goto_0

    .line 2641913
    :pswitch_f
    sget-object v0, LX/J3T;->ITEM_SUB_SUB_SUBTITLE:LX/J3T;

    iget-object v1, p0, LX/J31;->c:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-object v1, v1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->l:Ljava/lang/String;

    invoke-static {p3, v0, v1}, LX/J31;->a(Landroid/view/ViewGroup;LX/J3T;Ljava/lang/String;)Landroid/view/ViewGroup;

    move-result-object v0

    move-object p2, v0

    .line 2641914
    goto/16 :goto_0

    .line 2641915
    :pswitch_10
    sget-object v0, LX/J3T;->HEADER_PRICE_TABLE:LX/J3T;

    invoke-static {p3, v0}, LX/J31;->a(Landroid/view/ViewGroup;LX/J3T;)Landroid/view/View;

    move-result-object v0

    move-object p2, v0

    .line 2641916
    goto/16 :goto_0

    .line 2641917
    :pswitch_11
    sget-object v0, LX/J3T;->PRICE_TABLE:LX/J3T;

    iget-object v1, p0, LX/J31;->c:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-boolean v1, v1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->m:Z

    invoke-static {p0, p3, v0, v1}, LX/J31;->a(LX/J31;Landroid/view/ViewGroup;LX/J3T;Z)Landroid/view/ViewGroup;

    move-result-object v0

    move-object p2, v0

    .line 2641918
    goto/16 :goto_0

    .line 2641919
    :pswitch_12
    sget-object v0, LX/J3T;->CURRENCY:LX/J3T;

    iget-object v1, p0, LX/J31;->c:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-object v1, v1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->n:Ljava/lang/String;

    invoke-static {p3, v0, v1}, LX/J31;->a(Landroid/view/ViewGroup;LX/J3T;Ljava/lang/String;)Landroid/view/ViewGroup;

    move-result-object v0

    move-object p2, v0

    .line 2641920
    goto/16 :goto_0

    .line 2641921
    :pswitch_13
    sget-object v0, LX/J3T;->AMOUNT_CUSTOM_LABEL:LX/J3T;

    iget-object v1, p0, LX/J31;->c:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-object v1, v1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->o:Ljava/lang/String;

    invoke-static {p3, v0, v1}, LX/J31;->a(Landroid/view/ViewGroup;LX/J3T;Ljava/lang/String;)Landroid/view/ViewGroup;

    move-result-object v0

    move-object p2, v0

    .line 2641922
    goto/16 :goto_0

    .line 2641923
    :pswitch_14
    sget-object v0, LX/J3T;->AMOUNT_TAX:LX/J3T;

    iget-object v1, p0, LX/J31;->c:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-object v1, v1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->p:Ljava/lang/String;

    invoke-static {p3, v0, v1}, LX/J31;->a(Landroid/view/ViewGroup;LX/J3T;Ljava/lang/String;)Landroid/view/ViewGroup;

    move-result-object v0

    move-object p2, v0

    .line 2641924
    goto/16 :goto_0

    .line 2641925
    :pswitch_15
    sget-object v0, LX/J3T;->AMOUNT_SHIPPING:LX/J3T;

    iget-object v1, p0, LX/J31;->c:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-object v1, v1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->q:Ljava/lang/String;

    invoke-static {p3, v0, v1}, LX/J31;->a(Landroid/view/ViewGroup;LX/J3T;Ljava/lang/String;)Landroid/view/ViewGroup;

    move-result-object v0

    move-object p2, v0

    .line 2641926
    goto/16 :goto_0

    .line 2641927
    :pswitch_16
    sget-object v0, LX/J3T;->AMOUNT_TOTAL:LX/J3T;

    iget-object v1, p0, LX/J31;->c:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-object v1, v1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->r:Ljava/lang/String;

    invoke-static {p3, v0, v1}, LX/J31;->a(Landroid/view/ViewGroup;LX/J3T;Ljava/lang/String;)Landroid/view/ViewGroup;

    move-result-object v0

    move-object p2, v0

    .line 2641928
    goto/16 :goto_0

    .line 2641929
    :pswitch_17
    sget-object v0, LX/J3T;->HEADER_PURCHASE_INFO:LX/J3T;

    invoke-static {p3, v0}, LX/J31;->a(Landroid/view/ViewGroup;LX/J3T;)Landroid/view/View;

    move-result-object v0

    move-object p2, v0

    .line 2641930
    goto/16 :goto_0

    .line 2641931
    :pswitch_18
    sget-object v0, LX/J3T;->NAME:LX/J3T;

    iget-object v1, p0, LX/J31;->c:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-boolean v1, v1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->s:Z

    invoke-static {p0, p3, v0, v1}, LX/J31;->a(LX/J31;Landroid/view/ViewGroup;LX/J3T;Z)Landroid/view/ViewGroup;

    move-result-object v0

    move-object p2, v0

    .line 2641932
    goto/16 :goto_0

    .line 2641933
    :pswitch_19
    sget-object v0, LX/J3T;->EMAIL:LX/J3T;

    iget-object v1, p0, LX/J31;->c:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-boolean v1, v1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->t:Z

    invoke-static {p0, p3, v0, v1}, LX/J31;->a(LX/J31;Landroid/view/ViewGroup;LX/J3T;Z)Landroid/view/ViewGroup;

    move-result-object v0

    move-object p2, v0

    .line 2641934
    goto/16 :goto_0

    .line 2641935
    :pswitch_1a
    sget-object v0, LX/J3T;->PHONE_NUMBER:LX/J3T;

    iget-object v1, p0, LX/J31;->c:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-boolean v1, v1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->u:Z

    invoke-static {p0, p3, v0, v1}, LX/J31;->a(LX/J31;Landroid/view/ViewGroup;LX/J3T;Z)Landroid/view/ViewGroup;

    move-result-object v0

    move-object p2, v0

    .line 2641936
    goto/16 :goto_0

    .line 2641937
    :pswitch_1b
    sget-object v0, LX/J3T;->MAILING_ADDRESS:LX/J3T;

    iget-object v1, p0, LX/J31;->c:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-boolean v1, v1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->v:Z

    invoke-static {p0, p3, v0, v1}, LX/J31;->a(LX/J31;Landroid/view/ViewGroup;LX/J3T;Z)Landroid/view/ViewGroup;

    move-result-object v0

    move-object p2, v0

    .line 2641938
    goto/16 :goto_0

    .line 2641939
    :pswitch_1c
    sget-object v0, LX/J3T;->SHIPPING_OPTION:LX/J3T;

    iget-object v1, p0, LX/J31;->c:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-boolean v1, v1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->w:Z

    invoke-static {p0, p3, v0, v1}, LX/J31;->a(LX/J31;Landroid/view/ViewGroup;LX/J3T;Z)Landroid/view/ViewGroup;

    move-result-object v0

    move-object p2, v0

    .line 2641940
    goto/16 :goto_0

    .line 2641941
    :pswitch_1d
    sget-object v0, LX/J3T;->PAYMENT_METHOD:LX/J3T;

    iget-object v1, p0, LX/J31;->c:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-boolean v1, v1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->x:Z

    invoke-static {p0, p3, v0, v1}, LX/J31;->a(LX/J31;Landroid/view/ViewGroup;LX/J3T;Z)Landroid/view/ViewGroup;

    move-result-object v0

    move-object p2, v0

    .line 2641942
    goto/16 :goto_0

    .line 2641943
    :pswitch_1e
    sget-object v0, LX/J3T;->USE_AUTHENTICATION:LX/J3T;

    iget-object v1, p0, LX/J31;->c:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-boolean v1, v1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->y:Z

    invoke-static {p0, p3, v0, v1}, LX/J31;->a(LX/J31;Landroid/view/ViewGroup;LX/J3T;Z)Landroid/view/ViewGroup;

    move-result-object v0

    move-object p2, v0

    .line 2641944
    goto/16 :goto_0

    .line 2641945
    :pswitch_1f
    sget-object v1, LX/J3T;->PLAIN_TEXT:LX/J3T;

    sget-object v0, Lcom/facebook/payments/sample/PaymentsFlowSampleFragment;->b:LX/0P1;

    iget-object v2, p0, LX/J31;->c:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget v2, v2, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->z:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p3, v1, v0}, LX/J31;->a(Landroid/view/ViewGroup;LX/J3T;Ljava/lang/String;)Landroid/view/ViewGroup;

    move-result-object v0

    move-object p2, v0

    .line 2641946
    goto/16 :goto_0

    .line 2641947
    :pswitch_20
    sget-object v0, LX/J3T;->HEADER_CALL_TO_ACTION:LX/J3T;

    invoke-static {p3, v0}, LX/J31;->a(Landroid/view/ViewGroup;LX/J3T;)Landroid/view/View;

    move-result-object v0

    move-object p2, v0

    .line 2641948
    goto/16 :goto_0

    .line 2641949
    :pswitch_21
    sget-object v0, LX/J3T;->PAYMENT_PROCESSOR_NAME:LX/J3T;

    iget-object v1, p0, LX/J31;->c:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-object v1, v1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->A:Ljava/lang/String;

    invoke-static {p3, v0, v1}, LX/J31;->a(Landroid/view/ViewGroup;LX/J3T;Ljava/lang/String;)Landroid/view/ViewGroup;

    move-result-object v0

    move-object p2, v0

    .line 2641950
    goto/16 :goto_0

    .line 2641951
    :pswitch_22
    sget-object v0, LX/J3T;->MERCHANT_NAME:LX/J3T;

    iget-object v1, p0, LX/J31;->c:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-object v1, v1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->B:Ljava/lang/String;

    invoke-static {p3, v0, v1}, LX/J31;->a(Landroid/view/ViewGroup;LX/J3T;Ljava/lang/String;)Landroid/view/ViewGroup;

    move-result-object v0

    move-object p2, v0

    .line 2641952
    goto/16 :goto_0

    .line 2641953
    :pswitch_23
    sget-object v0, LX/J3T;->DIALOG_BASED_PROGRESS:LX/J3T;

    iget-object v1, p0, LX/J31;->c:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget-boolean v1, v1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->C:Z

    invoke-static {p0, p3, v0, v1}, LX/J31;->a(LX/J31;Landroid/view/ViewGroup;LX/J3T;Z)Landroid/view/ViewGroup;

    move-result-object v0

    move-object p2, v0

    .line 2641954
    goto/16 :goto_0

    .line 2641955
    :pswitch_24
    sget-object v0, LX/J3T;->PAY_BUTTON_TEXT:LX/J3T;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/J31;->c:Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    iget v2, v2, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->D:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p3, v0, v1}, LX/J31;->a(Landroid/view/ViewGroup;LX/J3T;Ljava/lang/String;)Landroid/view/ViewGroup;

    move-result-object v0

    move-object p2, v0

    .line 2641956
    goto/16 :goto_0

    .line 2641957
    :pswitch_25
    sget-object v0, LX/J3T;->HEADER_ACTION_BUTTONS:LX/J3T;

    invoke-static {p3, v0}, LX/J31;->a(Landroid/view/ViewGroup;LX/J3T;)Landroid/view/View;

    move-result-object p2

    goto/16 :goto_0

    .line 2641958
    :pswitch_26
    const/4 v3, 0x0

    .line 2641959
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030025

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2641960
    invoke-static {}, LX/J32;->values()[LX/J32;

    move-result-object v4

    array-length v5, v4

    move v2, v3

    :goto_3
    if-ge v2, v5, :cond_2

    aget-object p1, v4, v2

    .line 2641961
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const p2, 0x7f030024

    invoke-virtual {v1, p2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/text/BetterButton;

    .line 2641962
    invoke-virtual {p1}, LX/J32;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v1, p2}, Lcom/facebook/widget/text/BetterButton;->setText(Ljava/lang/CharSequence;)V

    .line 2641963
    new-instance p2, LX/J2y;

    invoke-direct {p2, p0, p1}, LX/J2y;-><init>(LX/J31;LX/J32;)V

    invoke-virtual {v1, p2}, Lcom/facebook/widget/text/BetterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2641964
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2641965
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    .line 2641966
    :cond_2
    move-object p2, v0

    .line 2641967
    goto/16 :goto_0

    :cond_3
    move-object v0, v2

    goto/16 :goto_1

    :cond_4
    move-object v0, v2

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
    .end packed-switch
.end method
