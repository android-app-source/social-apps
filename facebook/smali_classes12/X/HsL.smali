.class public LX/HsL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0iK;
.implements LX/HsK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0io;",
        ":",
        "LX/0jD;",
        ":",
        "LX/0j6;",
        "DerivedData::",
        "LX/5RE;",
        "Mutation::",
        "Lcom/facebook/composer/attachments/ComposerAttachment$SetsAttachments",
        "<TMutation;>;:",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
        "Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;:",
        "LX/0im",
        "<TMutation;>;>",
        "Ljava/lang/Object;",
        "LX/0iK",
        "<TModelData;TDerivedData;>;",
        "LX/HsK;"
    }
.end annotation


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:LX/9A6;

.field private final c:LX/HsE;

.field private final d:Lcom/facebook/composer/collage/creators/CollageDraweeControllerCreator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/composer/collage/creators/CollageDraweeControllerCreator",
            "<TModelData;TServices;>;"
        }
    .end annotation
.end field

.field private final e:LX/7l0;

.field private final f:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field private final h:LX/0Uh;

.field public final i:LX/HsF;

.field public j:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:LX/HqG;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Landroid/view/ViewGroup;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/9A6;LX/HsE;LX/HsD;LX/7l0;LX/1Ck;LX/0Uh;LX/0il;)V
    .locals 2
    .param p8    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/9A6;",
            "LX/HsE;",
            "LX/HsD;",
            "LX/7l0;",
            "LX/1Ck;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "TServices;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2514327
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2514328
    new-instance v0, LX/HsG;

    invoke-direct {v0, p0}, LX/HsG;-><init>(LX/HsL;)V

    iput-object v0, p0, LX/HsL;->i:LX/HsF;

    .line 2514329
    iput-object p1, p0, LX/HsL;->a:Landroid/content/Context;

    .line 2514330
    iput-object p2, p0, LX/HsL;->b:LX/9A6;

    .line 2514331
    iput-object p3, p0, LX/HsL;->c:LX/HsE;

    .line 2514332
    invoke-virtual {p4, p8}, LX/HsD;->a(LX/0il;)Lcom/facebook/composer/collage/creators/CollageDraweeControllerCreator;

    move-result-object v0

    iput-object v0, p0, LX/HsL;->d:Lcom/facebook/composer/collage/creators/CollageDraweeControllerCreator;

    .line 2514333
    iput-object p5, p0, LX/HsL;->e:LX/7l0;

    .line 2514334
    iput-object p6, p0, LX/HsL;->f:LX/1Ck;

    .line 2514335
    iput-object p7, p0, LX/HsL;->h:LX/0Uh;

    .line 2514336
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p8}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/HsL;->g:Ljava/lang/ref/WeakReference;

    .line 2514337
    return-void
.end method

.method public static b(LX/0Px;LX/0Px;)Z
    .locals 5
    .param p0    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2514271
    if-ne p0, p1, :cond_1

    move v1, v2

    .line 2514272
    :cond_0
    :goto_0
    return v1

    .line 2514273
    :cond_1
    if-nez p0, :cond_2

    if-nez p1, :cond_0

    :cond_2
    if-eqz p0, :cond_3

    if-eqz p1, :cond_0

    .line 2514274
    :cond_3
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    if-ne v0, v3, :cond_0

    move v0, v1

    .line 2514275
    :goto_1
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    if-ge v0, v3, :cond_4

    .line 2514276
    invoke-virtual {p0, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    if-ne v3, v4, :cond_0

    .line 2514277
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    move v1, v2

    .line 2514278
    goto :goto_0
.end method

.method private d()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/HsI;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2514314
    iget-object v0, p0, LX/HsL;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 2514315
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    invoke-interface {v1}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v1

    iput-object v1, p0, LX/HsL;->j:LX/0Px;

    .line 2514316
    iget-object v1, p0, LX/HsL;->b:LX/9A6;

    const/4 v2, 0x1

    .line 2514317
    iput-boolean v2, v1, LX/9A6;->j:Z

    .line 2514318
    move-object v1, v1

    .line 2514319
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/9A6;->a(LX/0Px;)LX/9A6;

    move-result-object v0

    invoke-virtual {v0}, LX/9A6;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    const-string v1, "This means we didn\'t properly check if this feed attachment was needed."

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2514320
    iget-object v1, p0, LX/HsL;->c:LX/HsE;

    .line 2514321
    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 2514322
    iget-object v3, v1, LX/HsE;->d:LX/26H;

    iget-object v4, v1, LX/HsE;->a:LX/0tK;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, LX/0tK;->b(Z)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, v1, LX/HsE;->b:LX/26F;

    invoke-virtual {v4, v2}, LX/26F;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/Aj3;

    move-result-object v2

    :goto_0
    invoke-virtual {v3, v2}, LX/26H;->a(LX/26L;)LX/26O;

    move-result-object v2

    move-object v1, v2

    .line 2514323
    iget-object v2, p0, LX/HsL;->d:Lcom/facebook/composer/collage/creators/CollageDraweeControllerCreator;

    invoke-virtual {v2, v0, v1}, Lcom/facebook/composer/collage/creators/CollageDraweeControllerCreator;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/26O;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 2514324
    new-instance v3, LX/HsH;

    invoke-direct {v3, p0, v0, v1}, LX/HsH;-><init>(LX/HsL;Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/26O;)V

    .line 2514325
    sget-object v0, LX/131;->INSTANCE:LX/131;

    move-object v0, v0

    .line 2514326
    invoke-static {v2, v3, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v4, v1, LX/HsE;->c:LX/26G;

    invoke-virtual {v4, v2}, LX/26G;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/26K;

    move-result-object v2

    goto :goto_0
.end method

.method public static e(LX/HsL;)V
    .locals 5

    .prologue
    .line 2514308
    iget-object v0, p0, LX/HsL;->m:Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;

    const-string v1, "Only call this method when the attachment view has been initialized."

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2514309
    iget-object v0, p0, LX/HsL;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 2514310
    iget-object v1, p0, LX/HsL;->m:Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;

    iget-object v2, p0, LX/HsL;->e:LX/7l0;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    .line 2514311
    sget-object v3, LX/0Re;->a:LX/0Re;

    move-object v3, v3

    .line 2514312
    const/4 v4, 0x0

    invoke-static {v2, v0, v3, v4}, LX/7ky;->a(LX/7l0;LX/0Px;LX/0Rf;Z)LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;->setTaggedUserCount(I)V

    .line 2514313
    return-void
.end method


# virtual methods
.method public final a(LX/5L2;)V
    .locals 1

    .prologue
    .line 2514304
    iget-object v0, p0, LX/HsL;->l:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/HsL;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2514305
    :cond_0
    :goto_0
    return-void

    .line 2514306
    :cond_1
    iget-boolean v0, p0, LX/HsL;->n:Z

    if-nez v0, :cond_0

    .line 2514307
    sget-object v0, LX/5L2;->ON_USER_POST:LX/5L2;

    if-ne p1, v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, LX/HsL;->n:Z

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Landroid/view/ViewGroup;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2514299
    iget-object v0, p0, LX/HsL;->l:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Are we calling bind twice?  Did we not unbind cleanly?"

    invoke-static {v0, v2}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2514300
    iput-object p1, p0, LX/HsL;->l:Landroid/view/ViewGroup;

    .line 2514301
    iget-object v0, p0, LX/HsL;->f:LX/1Ck;

    const-string v2, "generate_collage_task"

    invoke-direct {p0}, LX/HsL;->d()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    new-instance v4, LX/HsJ;

    invoke-direct {v4, p0}, LX/HsJ;-><init>(LX/HsL;)V

    invoke-virtual {v0, v2, v3, v4}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2514302
    return-void

    :cond_0
    move v0, v1

    .line 2514303
    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2514292
    iget-object v0, p0, LX/HsL;->l:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/HsL;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2514293
    :cond_0
    :goto_0
    return-void

    .line 2514294
    :cond_1
    iget-object v0, p0, LX/HsL;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 2514295
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    iget-object v1, p0, LX/HsL;->j:LX/0Px;

    invoke-static {v0, v1}, LX/HsL;->b(LX/0Px;LX/0Px;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2514296
    iget-object v0, p0, LX/HsL;->f:LX/1Ck;

    const-string v1, "generate_collage_task"

    invoke-direct {p0}, LX/HsL;->d()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, LX/HsJ;

    invoke-direct {v3, p0}, LX/HsJ;-><init>(LX/HsL;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2514297
    :cond_2
    iget-object v0, p0, LX/HsL;->m:Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;

    if-eqz v0, :cond_0

    .line 2514298
    invoke-static {p0}, LX/HsL;->e(LX/HsL;)V

    goto :goto_0
.end method

.method public final a()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2514289
    iget-object v0, p0, LX/HsL;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    move-object v1, v0

    .line 2514290
    check-cast v1, LX/0ik;

    invoke-interface {v1}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/5RE;

    invoke-interface {v1}, LX/5RE;->I()LX/5RF;

    move-result-object v1

    .line 2514291
    sget-object v4, LX/5RF;->STICKER:LX/5RF;

    if-eq v1, v4, :cond_0

    sget-object v4, LX/5RF;->MINUTIAE:LX/5RF;

    if-eq v1, v4, :cond_0

    sget-object v4, LX/5RF;->MULTIPLE_VIDEOS:LX/5RF;

    if-eq v1, v4, :cond_0

    sget-object v4, LX/5RF;->MULTIPLE_PHOTOS:LX/5RF;

    if-eq v1, v4, :cond_0

    sget-object v4, LX/5RF;->MULTIMEDIA:LX/5RF;

    if-ne v1, v4, :cond_1

    :cond_0
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-le v0, v2, :cond_1

    iget-object v0, p0, LX/HsL;->h:LX/0Uh;

    sget v1, LX/7l1;->b:I

    invoke-virtual {v0, v1, v3}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    :goto_0
    return v0

    :cond_1
    move v0, v3

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2514279
    iget-object v0, p0, LX/HsL;->f:LX/1Ck;

    const-string v1, "generate_collage_task"

    invoke-virtual {v0, v1}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2514280
    iget-object v0, p0, LX/HsL;->m:Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;

    if-eqz v0, :cond_0

    .line 2514281
    iget-object v0, p0, LX/HsL;->m:Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;

    invoke-virtual {v0}, Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;->a()V

    .line 2514282
    iget-object v0, p0, LX/HsL;->m:Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;->setTaggedUserCount(I)V

    .line 2514283
    iget-object v0, p0, LX/HsL;->m:Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;

    .line 2514284
    iput-object v2, v0, Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;->d:LX/HsF;

    .line 2514285
    iput-object v2, p0, LX/HsL;->m:Lcom/facebook/composer/collage/ui/ComposerCollageAttachmentView;

    .line 2514286
    :cond_0
    iput-object v2, p0, LX/HsL;->l:Landroid/view/ViewGroup;

    .line 2514287
    iput-object v2, p0, LX/HsL;->j:LX/0Px;

    .line 2514288
    return-void
.end method
