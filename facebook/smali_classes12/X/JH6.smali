.class public LX/JH6;
.super Landroid/view/View;
.source ""


# instance fields
.field public a:F

.field public b:F

.field public c:F

.field public d:F

.field public e:[I

.field public f:[F
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final g:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2672400
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2672401
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/JH6;->g:Landroid/graphics/Paint;

    .line 2672402
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, LX/JH6;->e:[I

    .line 2672403
    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method


# virtual methods
.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 2672404
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 2672405
    iget-object v8, p0, LX/JH6;->g:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/LinearGradient;

    iget v1, p0, LX/JH6;->a:F

    iget v2, p0, LX/JH6;->b:F

    iget v3, p0, LX/JH6;->c:F

    invoke-virtual {p0}, LX/JH6;->getWidth()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget v4, p0, LX/JH6;->d:F

    invoke-virtual {p0}, LX/JH6;->getHeight()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v4, v5

    iget-object v5, p0, LX/JH6;->e:[I

    iget-object v6, p0, LX/JH6;->f:[F

    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    invoke-virtual {v8, v0}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 2672406
    invoke-virtual {p0}, LX/JH6;->getWidth()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p0}, LX/JH6;->getHeight()I

    move-result v0

    int-to-float v4, v0

    iget-object v5, p0, LX/JH6;->g:Landroid/graphics/Paint;

    move-object v0, p1

    move v1, v9

    move v2, v9

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 2672407
    return-void
.end method
