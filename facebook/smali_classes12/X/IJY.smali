.class public LX/IJY;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/graphics/drawable/Drawable;

.field public final b:LX/4oL;

.field public final c:I

.field public final d:LX/1FZ;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/1FZ;)V
    .locals 10
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2563919
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2563920
    iput-object p2, p0, LX/IJY;->d:LX/1FZ;

    .line 2563921
    const v0, 0x7f020b94

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/IJY;->a:Landroid/graphics/drawable/Drawable;

    .line 2563922
    const/high16 v0, 0x40800000    # 4.0f

    invoke-static {p1, v0}, LX/0tP;->a(Landroid/content/res/Resources;F)I

    move-result v0

    iput v0, p0, LX/IJY;->c:I

    .line 2563923
    iget-object v0, p0, LX/IJY;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    .line 2563924
    iget-object v0, p0, LX/IJY;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    .line 2563925
    iget-object v0, p0, LX/IJY;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2563926
    new-instance v8, LX/4oL;

    invoke-static {}, LX/4oN;->a()LX/4oN;

    move-result-object v9

    new-instance v0, LX/4oP;

    iget v4, p0, LX/IJY;->c:I

    mul-int/lit8 v4, v4, 0x2

    sub-int/2addr v1, v4

    iget v4, p0, LX/IJY;->c:I

    mul-int/lit8 v4, v4, 0x2

    sub-int/2addr v2, v4

    sget-object v7, LX/4oO;->CENTER_CROP:LX/4oO;

    move v4, v3

    move v5, v3

    move v6, v3

    invoke-direct/range {v0 .. v7}, LX/4oP;-><init>(IIIIIILX/4oO;)V

    invoke-direct {v8, v9, v0}, LX/4oL;-><init>(LX/4oN;LX/4oP;)V

    iput-object v8, p0, LX/IJY;->b:LX/4oL;

    .line 2563927
    return-void
.end method
