.class public LX/Izg;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile i:LX/Izg;


# instance fields
.field private final a:LX/IzM;

.field private final b:LX/IzO;

.field private final c:LX/Izt;

.field private final d:LX/03V;

.field private final e:LX/Izp;

.field private final f:LX/2JT;

.field private final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/Izh;


# direct methods
.method public constructor <init>(LX/IzM;LX/IzO;LX/Izt;LX/03V;LX/Izp;LX/2JT;LX/0Or;LX/Izh;)V
    .locals 0
    .param p7    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/IzM;",
            "LX/IzO;",
            "LX/Izt;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/Izp;",
            "LX/2JT;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/Izh;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2634968
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2634969
    iput-object p1, p0, LX/Izg;->a:LX/IzM;

    .line 2634970
    iput-object p2, p0, LX/Izg;->b:LX/IzO;

    .line 2634971
    iput-object p3, p0, LX/Izg;->c:LX/Izt;

    .line 2634972
    iput-object p4, p0, LX/Izg;->d:LX/03V;

    .line 2634973
    iput-object p5, p0, LX/Izg;->e:LX/Izp;

    .line 2634974
    iput-object p6, p0, LX/Izg;->f:LX/2JT;

    .line 2634975
    iput-object p7, p0, LX/Izg;->g:LX/0Or;

    .line 2634976
    iput-object p8, p0, LX/Izg;->h:LX/Izh;

    .line 2634977
    return-void
.end method

.method private static a(LX/Izg;LX/0Px;)LX/0Px;
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2634960
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2634961
    iget-object v0, p0, LX/Izg;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2634962
    iget-object v1, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v1

    .line 2634963
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2634964
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 2634965
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {p0, v4, v5}, LX/Izg;->b(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2634966
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2634967
    :cond_0
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/Izg;
    .locals 12

    .prologue
    .line 2634947
    sget-object v0, LX/Izg;->i:LX/Izg;

    if-nez v0, :cond_1

    .line 2634948
    const-class v1, LX/Izg;

    monitor-enter v1

    .line 2634949
    :try_start_0
    sget-object v0, LX/Izg;->i:LX/Izg;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2634950
    if-eqz v2, :cond_0

    .line 2634951
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2634952
    new-instance v3, LX/Izg;

    invoke-static {v0}, LX/IzM;->a(LX/0QB;)LX/IzM;

    move-result-object v4

    check-cast v4, LX/IzM;

    invoke-static {v0}, LX/IzO;->a(LX/0QB;)LX/IzO;

    move-result-object v5

    check-cast v5, LX/IzO;

    invoke-static {v0}, LX/Izt;->b(LX/0QB;)LX/Izt;

    move-result-object v6

    check-cast v6, LX/Izt;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static {v0}, LX/Izp;->a(LX/0QB;)LX/Izp;

    move-result-object v8

    check-cast v8, LX/Izp;

    invoke-static {v0}, LX/2JT;->a(LX/0QB;)LX/2JT;

    move-result-object v9

    check-cast v9, LX/2JT;

    const/16 v10, 0x12cc

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    invoke-static {v0}, LX/Izh;->a(LX/0QB;)LX/Izh;

    move-result-object v11

    check-cast v11, LX/Izh;

    invoke-direct/range {v3 .. v11}, LX/Izg;-><init>(LX/IzM;LX/IzO;LX/Izt;LX/03V;LX/Izp;LX/2JT;LX/0Or;LX/Izh;)V

    .line 2634953
    move-object v0, v3

    .line 2634954
    sput-object v0, LX/Izg;->i:LX/Izg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2634955
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2634956
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2634957
    :cond_1
    sget-object v0, LX/Izg;->i:LX/Izg;

    return-object v0

    .line 2634958
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2634959
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Landroid/database/Cursor;)Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 2634793
    sget-object v0, LX/IzX;->b:LX/0U1;

    .line 2634794
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 2634795
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2634796
    sget-object v0, LX/IzX;->c:LX/0U1;

    .line 2634797
    iget-object v1, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v1

    .line 2634798
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2634799
    new-instance v0, LX/Dtl;

    invoke-direct {v0}, LX/Dtl;-><init>()V

    sget-object v1, LX/IzX;->g:LX/0U1;

    .line 2634800
    iget-object v5, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v5

    .line 2634801
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 2634802
    iput v1, v0, LX/Dtl;->a:I

    .line 2634803
    move-object v0, v0

    .line 2634804
    sget-object v1, LX/IzX;->h:LX/0U1;

    .line 2634805
    iget-object v5, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v5

    .line 2634806
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 2634807
    iput v1, v0, LX/Dtl;->c:I

    .line 2634808
    move-object v0, v0

    .line 2634809
    sget-object v1, LX/IzX;->i:LX/0U1;

    .line 2634810
    iget-object v5, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v5

    .line 2634811
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2634812
    iput-object v1, v0, LX/Dtl;->b:Ljava/lang/String;

    .line 2634813
    move-object v0, v0

    .line 2634814
    invoke-virtual {v0}, LX/Dtl;->a()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    move-result-object v5

    .line 2634815
    iget-object v0, p0, LX/Izg;->c:LX/Izt;

    sget-object v1, LX/IzX;->k:LX/0U1;

    .line 2634816
    iget-object v6, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v6

    .line 2634817
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Izt;->a(Ljava/lang/String;)Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;

    move-result-object v0

    .line 2634818
    sget-object v1, LX/IzX;->m:LX/0U1;

    .line 2634819
    iget-object v6, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v6

    .line 2634820
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2634821
    if-nez v1, :cond_0

    move-object v1, v2

    .line 2634822
    :goto_0
    new-instance v6, LX/Dtm;

    invoke-direct {v6}, LX/Dtm;-><init>()V

    sget-object v7, LX/IzX;->a:LX/0U1;

    .line 2634823
    iget-object v8, v7, LX/0U1;->d:Ljava/lang/String;

    move-object v7, v8

    .line 2634824
    invoke-interface {p1, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {p1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 2634825
    iput-object v7, v6, LX/Dtm;->d:Ljava/lang/String;

    .line 2634826
    move-object v6, v6

    .line 2634827
    iget-object v7, p0, LX/Izg;->e:LX/Izp;

    invoke-virtual {v7, v3}, LX/Izp;->a(Ljava/lang/String;)Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;->a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;)Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v3

    .line 2634828
    iput-object v3, v6, LX/Dtm;->i:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    .line 2634829
    move-object v3, v6

    .line 2634830
    iget-object v6, p0, LX/Izg;->e:LX/Izp;

    invoke-virtual {v6, v4}, LX/Izp;->a(Ljava/lang/String;)Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;->a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;)Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v4

    .line 2634831
    iput-object v4, v3, LX/Dtm;->h:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    .line 2634832
    move-object v3, v3

    .line 2634833
    sget-object v4, LX/IzX;->e:LX/0U1;

    .line 2634834
    iget-object v6, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v6

    .line 2634835
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 2634836
    iput-wide v6, v3, LX/Dtm;->b:J

    .line 2634837
    move-object v3, v3

    .line 2634838
    sget-object v4, LX/IzX;->f:LX/0U1;

    .line 2634839
    iget-object v6, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v6

    .line 2634840
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 2634841
    iput-wide v6, v3, LX/Dtm;->k:J

    .line 2634842
    move-object v3, v3

    .line 2634843
    sget-object v4, LX/IzX;->d:LX/0U1;

    .line 2634844
    iget-object v6, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v6

    .line 2634845
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    move-result-object v4

    .line 2634846
    iput-object v4, v3, LX/Dtm;->f:Lcom/facebook/graphql/enums/GraphQLPeerToPeerPaymentRequestStatus;

    .line 2634847
    move-object v3, v3

    .line 2634848
    iput-object v5, v3, LX/Dtm;->a:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    .line 2634849
    move-object v3, v3

    .line 2634850
    sget-object v4, LX/IzX;->j:LX/0U1;

    .line 2634851
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 2634852
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2634853
    iput-object v4, v3, LX/Dtm;->e:Ljava/lang/String;

    .line 2634854
    move-object v3, v3

    .line 2634855
    instance-of v4, v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;

    if-eqz v4, :cond_1

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;

    .line 2634856
    :goto_1
    iput-object v0, v3, LX/Dtm;->g:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;

    .line 2634857
    move-object v0, v3

    .line 2634858
    sget-object v3, LX/IzX;->l:LX/0U1;

    .line 2634859
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 2634860
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2634861
    iput-object v3, v0, LX/Dtm;->c:Ljava/lang/String;

    .line 2634862
    move-object v0, v0

    .line 2634863
    if-nez v1, :cond_2

    .line 2634864
    :goto_2
    iput-object v2, v0, LX/Dtm;->j:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;

    .line 2634865
    move-object v0, v0

    .line 2634866
    invoke-virtual {v0}, LX/Dtm;->a()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;

    move-result-object v0

    return-object v0

    .line 2634867
    :cond_0
    iget-object v6, p0, LX/Izg;->h:LX/Izh;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, LX/Izh;->a(J)Lcom/facebook/payments/p2p/model/PaymentTransaction;

    move-result-object v1

    goto/16 :goto_0

    :cond_1
    move-object v0, v2

    .line 2634868
    goto :goto_1

    :cond_2
    invoke-virtual {v1}, Lcom/facebook/payments/p2p/model/PaymentTransaction;->p()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;

    move-result-object v2

    goto :goto_2
.end method

.method private static b(LX/Izg;)LX/0Px;
    .locals 9
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 2634930
    const-string v0, "getIncomingRequestIds"

    const v1, -0x7f096751

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2634931
    :try_start_0
    iget-object v0, p0, LX/Izg;->a:LX/IzM;

    sget-object v1, LX/IzL;->d:LX/IzK;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/2Iu;->a(LX/0To;Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v0

    if-nez v0, :cond_0

    .line 2634932
    const v0, 0x76d05058

    invoke-static {v0}, LX/02m;->a(I)V

    move-object v0, v8

    :goto_0
    return-object v0

    .line 2634933
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/Izg;->b:LX/IzO;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2634934
    const-string v1, "incoming_request_ids"

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v1

    .line 2634935
    :try_start_2
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 2634936
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2634937
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 2634938
    :catch_0
    move-exception v0

    .line 2634939
    :try_start_3
    iget-object v2, p0, LX/Izg;->d:LX/03V;

    const-string v3, "DbFetchPaymentRequestHandler"

    const-string v4, "Reading the request from the database threw an exception."

    invoke-virtual {v2, v3, v4, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2634940
    :try_start_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2634941
    const v0, 0x5a170795

    invoke-static {v0}, LX/02m;->a(I)V

    move-object v0, v8

    goto :goto_0

    .line 2634942
    :cond_1
    :try_start_5
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v0

    .line 2634943
    :try_start_6
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2634944
    const v1, -0x5f848ace

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_0

    .line 2634945
    :catchall_0
    move-exception v0

    :try_start_7
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 2634946
    :catchall_1
    move-exception v0

    const v1, -0x5d4d8deb

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method private b(J)Ljava/lang/String;
    .locals 9
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 2634905
    const-string v0, "getRequesterIdForRequest"

    const v1, -0x1d8e38e8

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2634906
    :try_start_0
    iget-object v0, p0, LX/Izg;->b:LX/IzO;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2634907
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/IzX;->a:LX/0U1;

    .line 2634908
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2634909
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2634910
    const-string v1, "requests"

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 2634911
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    const/4 v2, 0x1

    if-le v0, v2, :cond_0

    .line 2634912
    iget-object v0, p0, LX/Izg;->d:LX/03V;

    const-string v2, "DbFetchPaymentRequestsHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Requests table should only have one row for a given request ID, but it has "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2634913
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2634914
    const v0, 0x2d3c5dbf

    invoke-static {v0}, LX/02m;->a(I)V

    move-object v0, v8

    :goto_0
    return-object v0

    .line 2634915
    :cond_0
    :try_start_3
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 2634916
    :try_start_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2634917
    const v0, -0x1e9b5f5f

    invoke-static {v0}, LX/02m;->a(I)V

    move-object v0, v8

    goto :goto_0

    .line 2634918
    :cond_1
    :try_start_5
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2634919
    sget-object v0, LX/IzX;->b:LX/0U1;

    .line 2634920
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 2634921
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v0

    .line 2634922
    :try_start_6
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2634923
    const v1, -0x5d46328c

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_0

    .line 2634924
    :catch_0
    move-exception v0

    .line 2634925
    :try_start_7
    iget-object v2, p0, LX/Izg;->d:LX/03V;

    const-string v3, "DbFetchPaymentRequestHandler"

    const-string v4, "Reading the request from the database threw an exception."

    invoke-virtual {v2, v3, v4, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 2634926
    :try_start_8
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 2634927
    const v0, 0x12b6dc65

    invoke-static {v0}, LX/02m;->a(I)V

    move-object v0, v8

    goto :goto_0

    .line 2634928
    :catchall_0
    move-exception v0

    :try_start_9
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 2634929
    :catchall_1
    move-exception v0

    const v1, 0x681d859d

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLInterfaces$PaymentRequest;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2634892
    const-string v0, "getIncomingPaymentRequests"

    const v1, -0x36272732

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2634893
    :try_start_0
    invoke-static {p0}, LX/Izg;->b(LX/Izg;)LX/0Px;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 2634894
    if-nez v2, :cond_0

    .line 2634895
    const v0, 0x5050b063

    invoke-static {v0}, LX/02m;->a(I)V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2634896
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/Izg;->e:LX/Izp;

    invoke-static {p0, v2}, LX/Izg;->a(LX/Izg;LX/0Px;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Izp;->a(LX/0Px;)V

    .line 2634897
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 2634898
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_2

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 2634899
    invoke-virtual {p0, v6, v7}, LX/Izg;->a(J)Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;

    move-result-object v0

    .line 2634900
    if-eqz v0, :cond_1

    .line 2634901
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2634902
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2634903
    :cond_2
    iget-object v0, p0, LX/Izg;->f:LX/2JT;

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2JT;->a(LX/0Px;)LX/0Px;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 2634904
    const v1, -0x4ec4de05

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    const v1, 0x72820b52

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final a(J)Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;
    .locals 9
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 2634869
    const-string v0, "getPaymentRequest"

    const v1, 0x2c4484fe

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2634870
    :try_start_0
    iget-object v0, p0, LX/Izg;->b:LX/IzO;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2634871
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/IzX;->a:LX/0U1;

    .line 2634872
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2634873
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2634874
    const-string v1, "requests"

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 2634875
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    const/4 v2, 0x1

    if-le v0, v2, :cond_0

    .line 2634876
    iget-object v0, p0, LX/Izg;->d:LX/03V;

    const-string v2, "DbFetchPaymentRequestsHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Requests table should only have one row for a given request ID, but it has "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2634877
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2634878
    const v0, -0x2fab932d

    invoke-static {v0}, LX/02m;->a(I)V

    move-object v0, v8

    :goto_0
    return-object v0

    .line 2634879
    :cond_0
    :try_start_3
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 2634880
    :try_start_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2634881
    const v0, -0x74ddd5ca

    invoke-static {v0}, LX/02m;->a(I)V

    move-object v0, v8

    goto :goto_0

    .line 2634882
    :cond_1
    :try_start_5
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2634883
    invoke-direct {p0, v1}, LX/Izg;->a(Landroid/database/Cursor;)Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v0

    .line 2634884
    :try_start_6
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2634885
    const v1, -0x6542d7c4

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_0

    .line 2634886
    :catch_0
    move-exception v0

    .line 2634887
    :try_start_7
    iget-object v2, p0, LX/Izg;->d:LX/03V;

    const-string v3, "DbFetchPaymentRequestHandler"

    const-string v4, "Reading the request from the database threw an exception."

    invoke-virtual {v2, v3, v4, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 2634888
    :try_start_8
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 2634889
    const v0, -0x77c495d1

    invoke-static {v0}, LX/02m;->a(I)V

    move-object v0, v8

    goto :goto_0

    .line 2634890
    :catchall_0
    move-exception v0

    :try_start_9
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 2634891
    :catchall_1
    move-exception v0

    const v1, 0x29b90522

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
