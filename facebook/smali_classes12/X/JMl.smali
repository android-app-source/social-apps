.class public LX/JMl;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JMn;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JMl",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JMn;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2684692
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2684693
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JMl;->b:LX/0Zi;

    .line 2684694
    iput-object p1, p0, LX/JMl;->a:LX/0Ot;

    .line 2684695
    return-void
.end method

.method public static a(LX/0QB;)LX/JMl;
    .locals 4

    .prologue
    .line 2684696
    const-class v1, LX/JMl;

    monitor-enter v1

    .line 2684697
    :try_start_0
    sget-object v0, LX/JMl;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2684698
    sput-object v2, LX/JMl;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2684699
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2684700
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2684701
    new-instance v3, LX/JMl;

    const/16 p0, 0x1e9e

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JMl;-><init>(LX/0Ot;)V

    .line 2684702
    move-object v0, v3

    .line 2684703
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2684704
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JMl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2684705
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2684706
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Landroid/view/View;LX/1X1;)V
    .locals 10

    .prologue
    .line 2684649
    check-cast p2, LX/JMj;

    .line 2684650
    iget-object v0, p0, LX/JMl;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JMn;

    iget-object v1, p2, LX/JMj;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2684651
    invoke-static {v1}, LX/JMi;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;

    move-result-object v5

    .line 2684652
    if-eqz v5, :cond_0

    .line 2684653
    iget-object v2, v0, LX/JMn;->d:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/JMw;

    .line 2684654
    iget-object v3, v0, LX/JMn;->c:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    move-object v8, v3

    check-cast v8, Ljava/util/concurrent/Executor;

    .line 2684655
    iget-object v3, v0, LX/JMn;->b:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0kL;

    .line 2684656
    iget-object v3, v0, LX/JMn;->a:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0bH;

    .line 2684657
    invoke-static {v5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2684658
    new-instance v7, LX/4D6;

    invoke-direct {v7}, LX/4D6;-><init>()V

    iget-object v3, v2, LX/JMw;->b:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2684659
    const-string v9, "actor_id"

    invoke-virtual {v7, v9, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2684660
    move-object v3, v7

    .line 2684661
    const-string v7, "video_id"

    invoke-virtual {v3, v7, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2684662
    move-object v3, v3

    .line 2684663
    new-instance v7, LX/JMs;

    invoke-direct {v7}, LX/JMs;-><init>()V

    move-object v7, v7

    .line 2684664
    const-string v9, "input"

    invoke-virtual {v7, v9, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2684665
    invoke-static {v7}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v3

    .line 2684666
    iget-object v7, v2, LX/JMw;->a:LX/0tX;

    invoke-virtual {v7, v3}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v9, v3

    .line 2684667
    new-instance v2, LX/JMm;

    move-object v3, v0

    move-object v5, v1

    move-object v7, p1

    invoke-direct/range {v2 .. v7}, LX/JMm;-><init>(LX/JMn;LX/0bH;Lcom/facebook/feed/rows/core/props/FeedProps;LX/0kL;Landroid/view/View;)V

    invoke-static {v9, v2, v8}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2684668
    :cond_0
    return-void
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2684691
    const v0, 0x6691943f

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 2684675
    check-cast p2, LX/JMj;

    .line 2684676
    iget-object v0, p0, LX/JMl;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p2, LX/JMj;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 p2, 0x3

    const/16 p0, 0x9

    const/4 v7, 0x2

    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 2684677
    const v1, 0x7f0e0180

    invoke-static {p1, v5, v1}, LX/1n8;->a(LX/1De;II)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, v7}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, v6, p0}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, p2, p0}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, v7}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, v6}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v1

    const v2, 0x7f020799

    invoke-interface {v1, v2}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v1

    .line 2684678
    const v2, 0x6691943f

    const/4 v3, 0x0

    invoke-static {p1, v2, v3}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v2

    move-object v2, v2

    .line 2684679
    invoke-interface {v1, v2}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v2

    .line 2684680
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 2684681
    check-cast v1, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;

    .line 2684682
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->o()LX/0Px;

    move-result-object v1

    .line 2684683
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2684684
    const/4 v1, 0x0

    .line 2684685
    :goto_0
    move-object v3, v1

    .line 2684686
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v4

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem;->j()Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {v4, v1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    const v4, 0x7f0b0050

    invoke-virtual {v1, v4}, LX/1ne;->q(I)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, v6}, LX/1ne;->t(I)LX/1ne;

    move-result-object v1

    const v4, 0x7f0a09f4

    invoke-virtual {v1, v4}, LX/1ne;->n(I)LX/1ne;

    move-result-object v1

    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    .line 2684687
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    const v4, 0x7f0b0917

    invoke-interface {v1, v5, v4}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v1

    const v4, 0x7f0b0917

    invoke-interface {v1, v7, v4}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem;->k()Ljava/lang/String;

    move-result-object v1

    :goto_2
    invoke-virtual {v5, v1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    const v3, 0x7f0b0050

    invoke-virtual {v1, v3}, LX/1ne;->q(I)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, v6}, LX/1ne;->t(I)LX/1ne;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    invoke-interface {v1, p2, p0}, LX/1Di;->d(II)LX/1Di;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    invoke-interface {v4, v1}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v1

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 2684688
    return-object v0

    .line 2684689
    :cond_0
    const-string v1, ""

    goto :goto_1

    .line 2684690
    :cond_1
    const-string v1, ""

    goto :goto_2

    :cond_2
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem;

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2684669
    invoke-static {}, LX/1dS;->b()V

    .line 2684670
    iget v0, p1, LX/1dQ;->b:I

    .line 2684671
    packed-switch v0, :pswitch_data_0

    .line 2684672
    :goto_0
    return-object v2

    .line 2684673
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2684674
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0, v1}, LX/JMl;->a(Landroid/view/View;LX/1X1;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x6691943f
        :pswitch_0
    .end packed-switch
.end method
