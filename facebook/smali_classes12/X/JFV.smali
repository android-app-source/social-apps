.class public LX/JFV;
.super LX/2wH;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2wH",
        "<",
        "LX/JFK;",
        ">;"
    }
.end annotation


# instance fields
.field private final d:Lcom/google/android/gms/location/places/internal/PlacesParams;

.field private final e:Ljava/util/Locale;


# direct methods
.method private constructor <init>(Landroid/content/Context;Landroid/os/Looper;LX/2wA;LX/1qf;LX/1qg;Ljava/lang/String;LX/JF5;)V
    .locals 7

    const/16 v3, 0x43

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, LX/2wH;-><init>(Landroid/content/Context;Landroid/os/Looper;ILX/2wA;LX/1qf;LX/1qg;)V

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    iput-object v0, p0, LX/JFV;->e:Ljava/util/Locale;

    const/4 v3, 0x0

    iget-object v0, p3, LX/2wA;->a:Landroid/accounts/Account;

    move-object v0, v0

    if-eqz v0, :cond_0

    iget-object v0, p3, LX/2wA;->a:Landroid/accounts/Account;

    move-object v0, v0

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    :cond_0
    new-instance v0, Lcom/google/android/gms/location/places/internal/PlacesParams;

    iget-object v2, p0, LX/JFV;->e:Ljava/util/Locale;

    iget-object v4, p7, LX/JF5;->b:Ljava/lang/String;

    iget v5, p7, LX/JF5;->c:I

    move-object v1, p6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/location/places/internal/PlacesParams;-><init>(Ljava/lang/String;Ljava/util/Locale;Ljava/lang/String;Ljava/lang/String;I)V

    iput-object v0, p0, LX/JFV;->d:Lcom/google/android/gms/location/places/internal/PlacesParams;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/os/Looper;LX/2wA;LX/1qf;LX/1qg;Ljava/lang/String;LX/JF5;B)V
    .locals 0

    invoke-direct/range {p0 .. p7}, LX/JFV;-><init>(Landroid/content/Context;Landroid/os/Looper;LX/2wA;LX/1qf;LX/1qg;Ljava/lang/String;LX/JF5;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    move-object v0, v0

    return-object v0

    :cond_0
    const-string v0, "com.google.android.gms.location.places.internal.IGooglePlaceDetectionService"

    invoke-interface {p1, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of p0, v0, LX/JFK;

    if-eqz p0, :cond_1

    check-cast v0, LX/JFK;

    goto :goto_0

    :cond_1
    new-instance v0, LX/JFL;

    invoke-direct {v0, p1}, LX/JFL;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.location.places.PlaceDetectionApi"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.location.places.internal.IGooglePlaceDetectionService"

    return-object v0
.end method
