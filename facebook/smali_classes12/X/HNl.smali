.class public final LX/HNl;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$FetchPageAdminInfoModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;

.field public final synthetic b:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;)V
    .locals 0

    .prologue
    .line 2458862
    iput-object p1, p0, LX/HNl;->b:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;

    iput-object p2, p0, LX/HNl;->a:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2458863
    iget-object v0, p0, LX/HNl;->b:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;

    invoke-static {v0, p1}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->a$redex0(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;Ljava/lang/Throwable;)V

    .line 2458864
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2458865
    check-cast p1, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$FetchPageAdminInfoModel;

    .line 2458866
    if-nez p1, :cond_0

    .line 2458867
    iget-object v0, p0, LX/HNl;->b:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;

    new-instance v1, Ljava/lang/Throwable;

    const-string v2, "Result of fetching page call to action admin info cannot be null"

    invoke-direct {v1, v2}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->a$redex0(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;Ljava/lang/Throwable;)V

    .line 2458868
    :goto_0
    return-void

    .line 2458869
    :cond_0
    iget-object v0, p0, LX/HNl;->b:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;

    iget-object v1, p0, LX/HNl;->a:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;

    invoke-virtual {p1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$FetchPageAdminInfoModel;->j()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminInfoDataModel;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;->a$redex0(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageCallToActionFragment;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminInfoDataModel;)V

    goto :goto_0
.end method
