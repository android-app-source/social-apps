.class public LX/Ito;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final q:Ljava/lang/Object;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/send/service/SendApiHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/Itm;

.field private final c:LX/FDt;

.field public final d:LX/18V;

.field public final e:LX/FKx;

.field private final f:LX/7V0;

.field private final g:LX/Iuh;

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Itm;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FN1;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FNf;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/0Ot;
    .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Itf;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/0Ot;
    .annotation runtime Lcom/facebook/messaging/cache/SmsMessages;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Itf;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/0Ot;
    .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Oe;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/0Ot;
    .annotation runtime Lcom/facebook/messaging/cache/SmsMessages;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Oe;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Itp;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2624625
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Ito;->q:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/FDt;LX/0Ot;LX/Itm;LX/18V;LX/FKx;LX/7V0;LX/Iuh;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/FDt;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/send/service/SendApiHandler;",
            ">;",
            "LX/Itm;",
            "Lcom/facebook/http/protocol/ApiMethodRunner;",
            "LX/FKx;",
            "LX/7V0;",
            "LX/Iuh;",
            "LX/0Ot",
            "<",
            "LX/Itm;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2624599
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2624600
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2624601
    iput-object v0, p0, LX/Ito;->i:LX/0Ot;

    .line 2624602
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2624603
    iput-object v0, p0, LX/Ito;->j:LX/0Ot;

    .line 2624604
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2624605
    iput-object v0, p0, LX/Ito;->k:LX/0Ot;

    .line 2624606
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2624607
    iput-object v0, p0, LX/Ito;->l:LX/0Ot;

    .line 2624608
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2624609
    iput-object v0, p0, LX/Ito;->m:LX/0Ot;

    .line 2624610
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2624611
    iput-object v0, p0, LX/Ito;->n:LX/0Ot;

    .line 2624612
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2624613
    iput-object v0, p0, LX/Ito;->o:LX/0Ot;

    .line 2624614
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2624615
    iput-object v0, p0, LX/Ito;->p:LX/0Ot;

    .line 2624616
    iput-object p1, p0, LX/Ito;->c:LX/FDt;

    .line 2624617
    iput-object p2, p0, LX/Ito;->a:LX/0Ot;

    .line 2624618
    iput-object p3, p0, LX/Ito;->b:LX/Itm;

    .line 2624619
    iput-object p4, p0, LX/Ito;->d:LX/18V;

    .line 2624620
    iput-object p5, p0, LX/Ito;->e:LX/FKx;

    .line 2624621
    iput-object p6, p0, LX/Ito;->f:LX/7V0;

    .line 2624622
    iput-object p7, p0, LX/Ito;->g:LX/Iuh;

    .line 2624623
    iput-object p8, p0, LX/Ito;->h:LX/0Ot;

    .line 2624624
    return-void
.end method

.method public static a(LX/0QB;)LX/Ito;
    .locals 7

    .prologue
    .line 2624572
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2624573
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2624574
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2624575
    if-nez v1, :cond_0

    .line 2624576
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2624577
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2624578
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2624579
    sget-object v1, LX/Ito;->q:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2624580
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2624581
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2624582
    :cond_1
    if-nez v1, :cond_4

    .line 2624583
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2624584
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2624585
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/Ito;->b(LX/0QB;)LX/Ito;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 2624586
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2624587
    if-nez v1, :cond_2

    .line 2624588
    sget-object v0, LX/Ito;->q:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ito;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2624589
    :goto_1
    if-eqz v0, :cond_3

    .line 2624590
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2624591
    :goto_3
    check-cast v0, LX/Ito;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2624592
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2624593
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2624594
    :catchall_1
    move-exception v0

    .line 2624595
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2624596
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2624597
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2624598
    :cond_2
    :try_start_8
    sget-object v0, LX/Ito;->q:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ito;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public static a(LX/Ito;Lcom/facebook/messaging/model/messages/Message;)V
    .locals 1

    .prologue
    .line 2624569
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2624570
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, LX/Ito;->a(LX/Ito;Lcom/facebook/messaging/model/messages/Message;Z)V

    .line 2624571
    :cond_0
    return-void
.end method

.method private static a(LX/Ito;Lcom/facebook/messaging/model/messages/Message;Z)V
    .locals 1

    .prologue
    .line 2624561
    if-eqz p2, :cond_0

    .line 2624562
    iget-object v0, p0, LX/Ito;->c:LX/FDt;

    .line 2624563
    const/4 p2, 0x1

    invoke-static {v0, p1, p2}, LX/FDt;->a(LX/FDt;Lcom/facebook/messaging/model/messages/Message;Z)V

    .line 2624564
    :goto_0
    iget-object v0, p0, LX/Ito;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Oe;

    invoke-virtual {v0, p1}, LX/2Oe;->a(Lcom/facebook/messaging/model/messages/Message;)V

    .line 2624565
    return-void

    .line 2624566
    :cond_0
    iget-object v0, p0, LX/Ito;->c:LX/FDt;

    .line 2624567
    const/4 p2, 0x0

    invoke-static {v0, p1, p2}, LX/FDt;->a(LX/FDt;Lcom/facebook/messaging/model/messages/Message;Z)V

    .line 2624568
    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/Ito;
    .locals 9

    .prologue
    .line 2624557
    new-instance v0, LX/Ito;

    invoke-static {p0}, LX/FDt;->a(LX/0QB;)LX/FDt;

    move-result-object v1

    check-cast v1, LX/FDt;

    const/16 v2, 0x292b

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {p0}, LX/Itm;->a(LX/0QB;)LX/Itm;

    move-result-object v3

    check-cast v3, LX/Itm;

    invoke-static {p0}, LX/18V;->a(LX/0QB;)LX/18V;

    move-result-object v4

    check-cast v4, LX/18V;

    invoke-static {p0}, LX/FKx;->b(LX/0QB;)LX/FKx;

    move-result-object v5

    check-cast v5, LX/FKx;

    invoke-static {p0}, LX/7V1;->a(LX/0QB;)LX/7V1;

    move-result-object v6

    check-cast v6, LX/7V0;

    invoke-static {p0}, LX/Iuh;->a(LX/0QB;)LX/Iuh;

    move-result-object v7

    check-cast v7, LX/Iuh;

    const/16 v8, 0x292d

    invoke-static {p0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, LX/Ito;-><init>(LX/FDt;LX/0Ot;LX/Itm;LX/18V;LX/FKx;LX/7V0;LX/Iuh;LX/0Ot;)V

    .line 2624558
    const/16 v1, 0x2e3

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x299f

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x29b6

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x2929

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x292a

    invoke-static {p0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0xce5

    invoke-static {p0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0xce6

    invoke-static {p0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x292f

    invoke-static {p0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    .line 2624559
    iput-object v1, v0, LX/Ito;->i:LX/0Ot;

    iput-object v2, v0, LX/Ito;->j:LX/0Ot;

    iput-object v3, v0, LX/Ito;->k:LX/0Ot;

    iput-object v4, v0, LX/Ito;->l:LX/0Ot;

    iput-object v5, v0, LX/Ito;->m:LX/0Ot;

    iput-object v6, v0, LX/Ito;->n:LX/0Ot;

    iput-object v7, v0, LX/Ito;->o:LX/0Ot;

    iput-object v8, v0, LX/Ito;->p:LX/0Ot;

    .line 2624560
    return-object v0
.end method

.method private c(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 9

    .prologue
    .line 2624539
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2624540
    const-string v1, "outgoingMessage"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 2624541
    iget-object v1, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2624542
    iget-object v1, p0, LX/Ito;->i:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 2624543
    iget-object v1, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-lez v1, :cond_0

    .line 2624544
    iget-object v1, p0, LX/Ito;->k:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FNf;

    iget-object v4, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v4, v4, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b:J

    invoke-virtual {v1, v4, v5}, LX/FNf;->a(J)J

    move-result-wide v4

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 2624545
    :cond_0
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6f7;

    move-result-object v0

    .line 2624546
    iput-wide v2, v0, LX/6f7;->c:J

    .line 2624547
    move-object v0, v0

    .line 2624548
    iput-wide v2, v0, LX/6f7;->d:J

    .line 2624549
    move-object v0, v0

    .line 2624550
    invoke-virtual {v0}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v1

    .line 2624551
    iget-object v0, p0, LX/Ito;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FN1;

    .line 2624552
    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/FN1;->a(Lcom/facebook/messaging/model/messages/Message;Z)Ljava/lang/String;

    .line 2624553
    iget-object v0, p0, LX/Ito;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Oe;

    invoke-virtual {v0, v1}, LX/2Oe;->a(Lcom/facebook/messaging/model/messages/Message;)V

    .line 2624554
    :goto_0
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2624555
    return-object v0

    .line 2624556
    :cond_1
    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, LX/Ito;->a(LX/Ito;Lcom/facebook/messaging/model/messages/Message;Z)V

    goto :goto_0
.end method

.method private d(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 8
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2624508
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2624509
    const-string v1, "sendMessageParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/SendMessageParams;

    .line 2624510
    iget-object v2, v0, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    .line 2624511
    iget-object v1, v2, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 2624512
    invoke-static {p0, v2}, LX/Ito;->a(LX/Ito;Lcom/facebook/messaging/model/messages/Message;)V

    .line 2624513
    const/4 v3, 0x0

    .line 2624514
    iget-object v1, v2, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2624515
    :goto_1
    move-object v2, v2

    .line 2624516
    iget-object v1, p0, LX/Ito;->c:LX/FDt;

    invoke-virtual {v1}, LX/FDt;->a()V

    .line 2624517
    iget-object v1, p0, LX/Ito;->g:LX/Iuh;

    invoke-virtual {v1}, LX/Iuh;->a()V

    .line 2624518
    :try_start_0
    iget-object v1, p0, LX/Ito;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/send/service/SendApiHandler;

    invoke-virtual {v1, v0}, Lcom/facebook/messaging/send/service/SendApiHandler;->a(Lcom/facebook/messaging/service/model/SendMessageParams;)Lcom/facebook/messaging/service/model/NewMessageResult;

    move-result-object v3

    .line 2624519
    iget-object v1, p0, LX/Ito;->l:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Itf;

    invoke-virtual {v1, v0, v3}, LX/Itf;->a(Lcom/facebook/messaging/service/model/SendMessageParams;Lcom/facebook/messaging/service/model/NewMessageResult;)V

    .line 2624520
    sget-object v1, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v1
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 2624521
    return-object v0

    .line 2624522
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 2624523
    :catch_0
    move-exception v1

    .line 2624524
    iget-object v3, p0, LX/Ito;->b:LX/Itm;

    sget-object v4, LX/6f3;->UNKNOWN:LX/6f3;

    invoke-virtual {v3, v1, v2, v4}, LX/Itm;->a(Ljava/lang/Throwable;Lcom/facebook/messaging/model/messages/Message;LX/6f3;)LX/FKG;

    move-result-object v2

    .line 2624525
    iget-object v1, p0, LX/Ito;->l:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Itf;

    iget-boolean v0, v0, Lcom/facebook/messaging/service/model/SendMessageParams;->b:Z

    invoke-virtual {v1, v2, v0}, LX/Itf;->a(LX/FKG;Z)V

    .line 2624526
    throw v2

    .line 2624527
    :cond_1
    invoke-static {}, LX/5zj;->values()[LX/5zj;

    move-result-object v1

    array-length v1, v1

    new-array v5, v1, [I

    .line 2624528
    iget-object v6, v2, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v4, v3

    :goto_2
    if-ge v4, v7, :cond_2

    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2624529
    iget-object v1, v1, Lcom/facebook/ui/media/attachments/MediaResource;->e:LX/5zj;

    invoke-virtual {v1}, LX/5zj;->ordinal()I

    move-result v1

    aget p1, v5, v1

    add-int/lit8 p1, p1, 0x1

    aput p1, v5, v1

    .line 2624530
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_2

    .line 2624531
    :cond_2
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6f7;

    move-result-object v4

    move v1, v3

    .line 2624532
    :goto_3
    array-length v3, v5

    if-ge v1, v3, :cond_4

    .line 2624533
    aget v3, v5, v1

    if-eqz v3, :cond_3

    .line 2624534
    invoke-static {}, LX/5zj;->values()[LX/5zj;

    move-result-object v3

    aget-object v3, v3, v1

    invoke-virtual {v3}, LX/5zj;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2624535
    aget v6, v5, v1

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    .line 2624536
    invoke-virtual {v4, v3, v6}, LX/6f7;->a(Ljava/lang/String;Ljava/lang/String;)LX/6f7;

    .line 2624537
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 2624538
    :cond_4
    invoke-virtual {v4}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v2

    goto/16 :goto_1
.end method

.method private e(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 10
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 2624383
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v0

    .line 2624384
    const-string v2, "sendMessageParams"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v7

    .line 2624385
    invoke-interface {v7, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/service/model/SendMessageParams;

    iget-object v1, v1, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v1, v1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2624386
    invoke-static {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 2624387
    iget-object v1, p0, LX/Ito;->c:LX/FDt;

    invoke-virtual {v1}, LX/FDt;->a()V

    .line 2624388
    iget-object v1, p0, LX/Ito;->g:LX/Iuh;

    invoke-virtual {v1}, LX/Iuh;->a()V

    .line 2624389
    :try_start_0
    iget-object v1, p0, LX/Ito;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/send/service/SendApiHandler;

    invoke-virtual {v1, v7}, Lcom/facebook/messaging/send/service/SendApiHandler;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    .line 2624390
    invoke-interface {v4}, Ljava/util/List;->size()I

    move v6, v5

    .line 2624391
    :goto_1
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    if-ge v6, v1, :cond_2

    .line 2624392
    invoke-interface {v7, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/service/model/SendMessageParams;

    .line 2624393
    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/service/model/NewMessageResult;

    .line 2624394
    iget-object v0, v2, Lcom/facebook/messaging/service/model/NewMessageResult;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v3, v0

    .line 2624395
    iget-boolean v0, v2, Lcom/facebook/messaging/service/model/NewMessageResult;->e:Z

    move v8, v0

    .line 2624396
    if-eqz v8, :cond_1

    .line 2624397
    iget-object v3, p0, LX/Ito;->l:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Itf;

    invoke-virtual {v3, v1, v2}, LX/Itf;->a(Lcom/facebook/messaging/service/model/SendMessageParams;Lcom/facebook/messaging/service/model/NewMessageResult;)V

    .line 2624398
    :goto_2
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto :goto_1

    :cond_0
    move v1, v5

    .line 2624399
    goto :goto_0

    .line 2624400
    :cond_1
    iget-object v8, p0, LX/Ito;->b:LX/Itm;

    new-instance v9, LX/FKG;

    .line 2624401
    iget-object v0, v2, Lcom/facebook/messaging/service/model/NewMessageResult;->f:Ljava/lang/String;

    move-object v2, v0

    .line 2624402
    invoke-direct {v9, v2, v3}, LX/FKG;-><init>(Ljava/lang/String;Lcom/facebook/messaging/model/messages/Message;)V

    iget-object v2, v1, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    sget-object v3, LX/6f3;->UNKNOWN:LX/6f3;

    invoke-virtual {v8, v9, v2, v3}, LX/Itm;->a(Ljava/lang/Throwable;Lcom/facebook/messaging/model/messages/Message;LX/6f3;)LX/FKG;

    move-result-object v3

    .line 2624403
    iget-object v2, p0, LX/Ito;->l:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Itf;

    iget-boolean v1, v1, Lcom/facebook/messaging/service/model/SendMessageParams;->b:Z

    invoke-virtual {v2, v3, v1}, LX/Itf;->a(LX/FKG;Z)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 2624404
    :catch_0
    move-exception v1

    move-object v2, v1

    .line 2624405
    iget-object v3, p0, LX/Ito;->b:LX/Itm;

    invoke-interface {v7, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/service/model/SendMessageParams;

    iget-object v1, v1, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    sget-object v4, LX/6f3;->UNKNOWN:LX/6f3;

    invoke-virtual {v3, v2, v1, v4}, LX/Itm;->a(Ljava/lang/Throwable;Lcom/facebook/messaging/model/messages/Message;LX/6f3;)LX/FKG;

    move-result-object v3

    .line 2624406
    iget-object v1, p0, LX/Ito;->l:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Itf;

    invoke-interface {v7, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/service/model/SendMessageParams;

    iget-boolean v2, v2, Lcom/facebook/messaging/service/model/SendMessageParams;->b:Z

    invoke-virtual {v1, v3, v2}, LX/Itf;->a(LX/FKG;Z)V

    .line 2624407
    throw v3

    .line 2624408
    :cond_2
    :try_start_1
    move-object v0, v4

    check-cast v0, Ljava/util/ArrayList;

    move-object v1, v0

    invoke-static {v1}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/util/ArrayList;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v1

    return-object v1
.end method

.method private g(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 5

    .prologue
    .line 2624497
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2624498
    const-string v1, "sendMessageParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/SendMessageParams;

    .line 2624499
    iget-object v2, v0, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    .line 2624500
    invoke-static {p0, v2}, LX/Ito;->a(LX/Ito;Lcom/facebook/messaging/model/messages/Message;)V

    .line 2624501
    :try_start_0
    iget-object v1, p0, LX/Ito;->p:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Itp;

    invoke-virtual {v1, v0}, LX/Itp;->a(Lcom/facebook/messaging/service/model/SendMessageParams;)Lcom/facebook/messaging/service/model/NewMessageResult;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 2624502
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2624503
    return-object v0

    .line 2624504
    :catch_0
    move-exception v1

    .line 2624505
    iget-object v3, p0, LX/Ito;->b:LX/Itm;

    sget-object v4, LX/6f3;->GRAPH:LX/6f3;

    invoke-virtual {v3, v1, v2, v4}, LX/Itm;->a(Ljava/lang/Throwable;Lcom/facebook/messaging/model/messages/Message;LX/6f3;)LX/FKG;

    move-result-object v2

    .line 2624506
    iget-object v1, p0, LX/Ito;->l:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Itf;

    iget-boolean v0, v0, Lcom/facebook/messaging/service/model/SendMessageParams;->b:Z

    invoke-virtual {v1, v2, v0}, LX/Itf;->a(LX/FKG;Z)V

    .line 2624507
    throw v2
.end method

.method private i(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 2624475
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2624476
    const-string v1, "sendMessageToPendingThreadParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/SendMessageToPendingThreadParams;

    .line 2624477
    iget-object v1, v0, Lcom/facebook/messaging/service/model/SendMessageToPendingThreadParams;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v2, v1

    .line 2624478
    iget-object v1, v2, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v1

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 2624479
    iget-object v1, v2, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 2624480
    :try_start_0
    iget-object v1, p0, LX/Ito;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/send/service/SendApiHandler;

    .line 2624481
    iget-object v3, v1, Lcom/facebook/messaging/send/service/SendApiHandler;->k:LX/6f6;

    .line 2624482
    iget-object p1, v0, Lcom/facebook/messaging/service/model/SendMessageToPendingThreadParams;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object p1, p1

    .line 2624483
    invoke-virtual {v3, p1}, LX/6f6;->b(Lcom/facebook/messaging/model/messages/Message;)LX/6f4;

    move-result-object v3

    .line 2624484
    const-string p1, "via_graph"

    invoke-static {v1, v0, v3, p1}, Lcom/facebook/messaging/send/service/SendApiHandler;->a(Lcom/facebook/messaging/send/service/SendApiHandler;Lcom/facebook/messaging/service/model/SendMessageToPendingThreadParams;LX/6f4;Ljava/lang/String;)Lcom/facebook/messaging/service/model/NewMessageResult;

    move-result-object v3

    move-object v3, v3

    .line 2624485
    iget-object v1, p0, LX/Ito;->l:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Itf;

    .line 2624486
    iget-object p1, v0, Lcom/facebook/messaging/service/model/SendMessageToPendingThreadParams;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object p1, p1

    .line 2624487
    invoke-static {v1, p1, v3}, LX/Itf;->a(LX/Itf;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/service/model/NewMessageResult;)V

    .line 2624488
    invoke-static {v3}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 2624489
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 2624490
    :catch_0
    move-exception v0

    .line 2624491
    iget-object v1, p0, LX/Ito;->b:LX/Itm;

    sget-object v3, LX/6f3;->UNKNOWN:LX/6f3;

    invoke-virtual {v1, v0, v2, v3}, LX/Itm;->a(Ljava/lang/Throwable;Lcom/facebook/messaging/model/messages/Message;LX/6f3;)LX/FKG;

    move-result-object v1

    .line 2624492
    iget-object v0, p0, LX/Ito;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Itf;

    .line 2624493
    iget-object v2, v1, LX/FKG;->failedMessage:Lcom/facebook/messaging/model/messages/Message;

    .line 2624494
    const-string v3, "Unable to send message to pending thread. message offline id: %s"

    const/4 p0, 0x1

    new-array p0, p0, [Ljava/lang/Object;

    const/4 p1, 0x0

    iget-object v2, v2, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    aput-object v2, p0, p1

    invoke-static {v3, p0}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2624495
    iget-object v3, v0, LX/Itf;->e:LX/03V;

    const-string p0, "send_to_pending_thread_failed"

    invoke-virtual {v3, p0, v2, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2624496
    throw v1
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 6

    .prologue
    const-wide/16 v4, 0xa

    .line 2624409
    const-string v0, "SendDataServiceHandler"

    const v1, -0x5a7a9c3e

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2624410
    :try_start_0
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 2624411
    const-string v1, "insert_pending_sent_message"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2624412
    invoke-direct {p0, p1}, LX/Ito;->c(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2624413
    const v1, -0x1898dc9e

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    :goto_0
    return-object v0

    .line 2624414
    :cond_0
    :try_start_1
    const-string v1, "send_batch"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2624415
    invoke-direct {p0, p1}, LX/Ito;->e(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 2624416
    const v1, -0x13dfae32

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto :goto_0

    .line 2624417
    :cond_1
    :try_start_2
    const-string v1, "send"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2624418
    invoke-direct {p0, p1}, LX/Ito;->d(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 2624419
    const v1, -0x16212e99

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto :goto_0

    .line 2624420
    :cond_2
    :try_start_3
    const-string v1, "broadcast_message"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2624421
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2624422
    const-string v1, "broadcastMessage"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 2624423
    iget-object v1, p0, LX/Ito;->d:LX/18V;

    iget-object v2, p0, LX/Ito;->e:LX/FKx;

    invoke-virtual {v1, v2, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2624424
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2624425
    move-object v0, v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2624426
    const v1, -0x2f002ca7

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto :goto_0

    .line 2624427
    :cond_3
    :try_start_4
    const-string v1, "send_to_montage"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2624428
    invoke-direct {p0, p1}, LX/Ito;->g(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v0

    .line 2624429
    const v1, 0x27b1449b

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto :goto_0

    .line 2624430
    :cond_4
    :try_start_5
    const-string v1, "sms_mms_sent"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2624431
    const/4 v3, 0x0

    .line 2624432
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2624433
    const-string v1, "message"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 2624434
    sget-object v1, Lcom/facebook/messaging/model/send/SendError;->a:Lcom/facebook/messaging/model/send/SendError;

    iget-object v2, v0, Lcom/facebook/messaging/model/messages/Message;->w:Lcom/facebook/messaging/model/send/SendError;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 2624435
    iget-object v1, p0, LX/Ito;->m:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Itf;

    invoke-virtual {v1, v0, v3, v3}, LX/Itf;->a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/threads/ThreadSummary;)V

    .line 2624436
    :goto_1
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2624437
    move-object v0, v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 2624438
    const v1, -0x5d35b883

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto/16 :goto_0

    .line 2624439
    :cond_5
    :try_start_6
    const-string v1, "send_to_pending_thread"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2624440
    invoke-direct {p0, p1}, LX/Ito;->i(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v0

    .line 2624441
    const v1, -0x2bdd0a35

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto/16 :goto_0

    .line 2624442
    :cond_6
    :try_start_7
    const-string v1, "handle_send_result"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2624443
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v2, v0

    .line 2624444
    const-string v0, "sendMessageParams"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/SendMessageParams;

    .line 2624445
    const-string v1, "newMessageResult"

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/service/model/NewMessageResult;

    .line 2624446
    const-string v3, "sendMessageException"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, LX/FKG;

    .line 2624447
    if-eqz v1, :cond_b

    .line 2624448
    iget-object v2, p0, LX/Ito;->l:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Itf;

    invoke-virtual {v2, v0, v1}, LX/Itf;->a(Lcom/facebook/messaging/service/model/SendMessageParams;Lcom/facebook/messaging/service/model/NewMessageResult;)V

    .line 2624449
    :goto_2
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2624450
    move-object v0, v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 2624451
    const v1, 0x1d535127

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto/16 :goto_0

    .line 2624452
    :cond_7
    :try_start_8
    const-string v1, "handle_media_db"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 2624453
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2624454
    const-string v1, "sendMessageParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/SendMessageParams;

    .line 2624455
    iget-object v1, v0, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v1, v1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v1

    if-nez v1, :cond_d

    const/4 v1, 0x1

    :goto_3
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 2624456
    iget-object v0, v0, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    invoke-static {p0, v0}, LX/Ito;->a(LX/Ito;Lcom/facebook/messaging/model/messages/Message;)V

    .line 2624457
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2624458
    move-object v0, v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 2624459
    const v1, 0x78a5462e

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    goto/16 :goto_0

    .line 2624460
    :cond_8
    :try_start_9
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown operation type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 2624461
    :catchall_0
    move-exception v0

    const v1, -0x4eff3b55

    invoke-static {v4, v5, v1}, LX/02m;->a(JI)V

    throw v0

    .line 2624462
    :cond_9
    :try_start_a
    new-instance v1, LX/FKG;

    iget-object v2, v0, Lcom/facebook/messaging/model/messages/Message;->w:Lcom/facebook/messaging/model/send/SendError;

    iget-object v2, v2, Lcom/facebook/messaging/model/send/SendError;->b:LX/6fP;

    iget-object v2, v2, LX/6fP;->serializedString:Ljava/lang/String;

    invoke-direct {v1, v2, v0}, LX/FKG;-><init>(Ljava/lang/String;Lcom/facebook/messaging/model/messages/Message;)V

    .line 2624463
    iget-object v0, p0, LX/Ito;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Itf;

    .line 2624464
    iget-object v2, v1, LX/FKG;->failedMessage:Lcom/facebook/messaging/model/messages/Message;

    .line 2624465
    iget-object v3, v0, LX/Itf;->c:LX/2Ow;

    invoke-virtual {v3, v2}, LX/2Ow;->c(Lcom/facebook/messaging/model/messages/Message;)V

    .line 2624466
    iget-object v3, v0, LX/Itf;->b:LX/2OQ;

    invoke-virtual {v3, v2}, LX/2OQ;->a(Lcom/facebook/messaging/model/messages/Message;)V

    .line 2624467
    iget-object v3, v0, LX/Itf;->c:LX/2Ow;

    iget-object p0, v2, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v3, p0}, LX/2Ow;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2624468
    iget-object v3, v2, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_a

    .line 2624469
    iget-object v2, v2, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    const/16 v3, 0xcc

    invoke-static {v0, v2, v3}, LX/Itf;->a(LX/Itf;Ljava/lang/String;I)V

    .line 2624470
    :cond_a
    goto/16 :goto_1
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 2624471
    :cond_b
    :try_start_b
    if-eqz v2, :cond_c

    const/4 v1, 0x1

    :goto_4
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 2624472
    iget-object v1, p0, LX/Ito;->l:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Itf;

    iget-boolean v0, v0, Lcom/facebook/messaging/service/model/SendMessageParams;->b:Z

    invoke-virtual {v1, v2, v0}, LX/Itf;->a(LX/FKG;Z)V

    goto/16 :goto_2

    .line 2624473
    :cond_c
    const/4 v1, 0x0

    goto :goto_4
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 2624474
    :cond_d
    const/4 v1, 0x0

    goto :goto_3
.end method
