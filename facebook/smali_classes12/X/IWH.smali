.class public final LX/IWH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;)V
    .locals 0

    .prologue
    .line 2583595
    iput-object p1, p0, LX/IWH;->a:Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 2

    .prologue
    .line 2583596
    iget-object v0, p0, LX/IWH;->a:Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;

    iget-boolean v0, v0, Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;->j:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/IWH;->a:Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;

    const/4 v1, 0x0

    .line 2583597
    iget-object p1, v0, Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;->i:LX/IWC;

    invoke-virtual {p1}, LX/IWC;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 2583598
    :cond_0
    :goto_0
    move v0, v1

    .line 2583599
    if-eqz v0, :cond_1

    .line 2583600
    iget-object v0, p0, LX/IWH;->a:Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;

    invoke-static {v0}, Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;->b(Lcom/facebook/groups/photos/fragment/GroupAlbumsFragment;)V

    .line 2583601
    :cond_1
    return-void

    .line 2583602
    :cond_2
    if-lez p3, :cond_0

    if-lez p4, :cond_0

    .line 2583603
    add-int p1, p2, p3

    add-int/lit8 p1, p1, 0x4

    if-le p1, p4, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 2583604
    return-void
.end method
