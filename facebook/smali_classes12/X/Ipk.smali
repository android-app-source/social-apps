.class public LX/Ipk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;

.field private static final g:LX/1sw;

.field private static final h:LX/1sw;

.field private static final i:LX/1sw;

.field private static final j:LX/1sw;

.field private static final k:LX/1sw;

.field private static final l:LX/1sw;

.field private static final m:LX/1sw;

.field private static final n:LX/1sw;

.field private static final o:LX/1sw;

.field private static final p:LX/1sw;


# instance fields
.field public final amount:Ljava/lang/Long;

.field public final amountOffset:Ljava/lang/Integer;

.field public final currency:Ljava/lang/String;

.field public final groupThreadFbId:Ljava/lang/Long;

.field public final hasMemoMultimedia:Ljava/lang/Boolean;

.field public final initialStatus:Ljava/lang/Integer;

.field public final irisSeqId:Ljava/lang/Long;

.field public final memoText:Ljava/lang/String;

.field public final offlineThreadingId:Ljava/lang/Long;

.field public final requestFbId:Ljava/lang/Long;

.field public final requesteeFbId:Ljava/lang/Long;

.field public final requesterFbId:Ljava/lang/Long;

.field public final themeId:Ljava/lang/Long;

.field public final timestampMs:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/16 v5, 0xb

    const/16 v4, 0x8

    const/16 v3, 0xa

    .line 2613666
    new-instance v0, LX/1sv;

    const-string v1, "DeltaNewPaymentRequest"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/Ipk;->b:LX/1sv;

    .line 2613667
    new-instance v0, LX/1sw;

    const-string v1, "requestFbId"

    invoke-direct {v0, v1, v3, v6}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipk;->c:LX/1sw;

    .line 2613668
    new-instance v0, LX/1sw;

    const-string v1, "requesterFbId"

    invoke-direct {v0, v1, v3, v7}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipk;->d:LX/1sw;

    .line 2613669
    new-instance v0, LX/1sw;

    const-string v1, "requesteeFbId"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipk;->e:LX/1sw;

    .line 2613670
    new-instance v0, LX/1sw;

    const-string v1, "timestampMs"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipk;->f:LX/1sw;

    .line 2613671
    new-instance v0, LX/1sw;

    const-string v1, "initialStatus"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipk;->g:LX/1sw;

    .line 2613672
    new-instance v0, LX/1sw;

    const-string v1, "currency"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v5, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipk;->h:LX/1sw;

    .line 2613673
    new-instance v0, LX/1sw;

    const-string v1, "amount"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipk;->i:LX/1sw;

    .line 2613674
    new-instance v0, LX/1sw;

    const-string v1, "amountOffset"

    invoke-direct {v0, v1, v4, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipk;->j:LX/1sw;

    .line 2613675
    new-instance v0, LX/1sw;

    const-string v1, "offlineThreadingId"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipk;->k:LX/1sw;

    .line 2613676
    new-instance v0, LX/1sw;

    const-string v1, "memoText"

    invoke-direct {v0, v1, v5, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipk;->l:LX/1sw;

    .line 2613677
    new-instance v0, LX/1sw;

    const-string v1, "hasMemoMultimedia"

    invoke-direct {v0, v1, v7, v5}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipk;->m:LX/1sw;

    .line 2613678
    new-instance v0, LX/1sw;

    const-string v1, "themeId"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipk;->n:LX/1sw;

    .line 2613679
    new-instance v0, LX/1sw;

    const-string v1, "groupThreadFbId"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipk;->o:LX/1sw;

    .line 2613680
    new-instance v0, LX/1sw;

    const-string v1, "irisSeqId"

    const/16 v2, 0x3e8

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipk;->p:LX/1sw;

    .line 2613681
    sput-boolean v6, LX/Ipk;->a:Z

    return-void
.end method

.method private constructor <init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 2613650
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2613651
    iput-object p1, p0, LX/Ipk;->requestFbId:Ljava/lang/Long;

    .line 2613652
    iput-object p2, p0, LX/Ipk;->requesterFbId:Ljava/lang/Long;

    .line 2613653
    iput-object p3, p0, LX/Ipk;->requesteeFbId:Ljava/lang/Long;

    .line 2613654
    iput-object p4, p0, LX/Ipk;->timestampMs:Ljava/lang/Long;

    .line 2613655
    iput-object p5, p0, LX/Ipk;->initialStatus:Ljava/lang/Integer;

    .line 2613656
    iput-object p6, p0, LX/Ipk;->currency:Ljava/lang/String;

    .line 2613657
    iput-object p7, p0, LX/Ipk;->amount:Ljava/lang/Long;

    .line 2613658
    iput-object p8, p0, LX/Ipk;->amountOffset:Ljava/lang/Integer;

    .line 2613659
    iput-object p9, p0, LX/Ipk;->offlineThreadingId:Ljava/lang/Long;

    .line 2613660
    iput-object p10, p0, LX/Ipk;->memoText:Ljava/lang/String;

    .line 2613661
    iput-object p11, p0, LX/Ipk;->hasMemoMultimedia:Ljava/lang/Boolean;

    .line 2613662
    iput-object p12, p0, LX/Ipk;->themeId:Ljava/lang/Long;

    .line 2613663
    iput-object p13, p0, LX/Ipk;->groupThreadFbId:Ljava/lang/Long;

    .line 2613664
    iput-object p14, p0, LX/Ipk;->irisSeqId:Ljava/lang/Long;

    .line 2613665
    return-void
.end method

.method private static a(LX/Ipk;)V
    .locals 4

    .prologue
    const/4 v3, 0x6

    .line 2613267
    iget-object v0, p0, LX/Ipk;->requestFbId:Ljava/lang/Long;

    if-nez v0, :cond_0

    .line 2613268
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'requestFbId\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/Ipk;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2613269
    :cond_0
    iget-object v0, p0, LX/Ipk;->requesterFbId:Ljava/lang/Long;

    if-nez v0, :cond_1

    .line 2613270
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'requesterFbId\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/Ipk;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2613271
    :cond_1
    iget-object v0, p0, LX/Ipk;->requesteeFbId:Ljava/lang/Long;

    if-nez v0, :cond_2

    .line 2613272
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'requesteeFbId\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/Ipk;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2613273
    :cond_2
    iget-object v0, p0, LX/Ipk;->timestampMs:Ljava/lang/Long;

    if-nez v0, :cond_3

    .line 2613274
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'timestampMs\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/Ipk;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2613275
    :cond_3
    iget-object v0, p0, LX/Ipk;->initialStatus:Ljava/lang/Integer;

    if-nez v0, :cond_4

    .line 2613276
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'initialStatus\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/Ipk;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2613277
    :cond_4
    iget-object v0, p0, LX/Ipk;->currency:Ljava/lang/String;

    if-nez v0, :cond_5

    .line 2613278
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'currency\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/Ipk;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2613279
    :cond_5
    iget-object v0, p0, LX/Ipk;->amount:Ljava/lang/Long;

    if-nez v0, :cond_6

    .line 2613280
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'amount\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/Ipk;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2613281
    :cond_6
    iget-object v0, p0, LX/Ipk;->amountOffset:Ljava/lang/Integer;

    if-nez v0, :cond_7

    .line 2613282
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'amountOffset\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/Ipk;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2613283
    :cond_7
    iget-object v0, p0, LX/Ipk;->initialStatus:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    sget-object v0, LX/Ipz;->a:LX/1sn;

    iget-object v1, p0, LX/Ipk;->initialStatus:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, LX/1sn;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 2613284
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The field \'initialStatus\' has been assigned the invalid value "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/Ipk;->initialStatus:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7H4;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2613285
    :cond_8
    return-void
.end method

.method public static b(LX/1su;)LX/Ipk;
    .locals 20

    .prologue
    .line 2613585
    const/4 v3, 0x0

    .line 2613586
    const/4 v4, 0x0

    .line 2613587
    const/4 v5, 0x0

    .line 2613588
    const/4 v6, 0x0

    .line 2613589
    const/4 v7, 0x0

    .line 2613590
    const/4 v8, 0x0

    .line 2613591
    const/4 v9, 0x0

    .line 2613592
    const/4 v10, 0x0

    .line 2613593
    const/4 v11, 0x0

    .line 2613594
    const/4 v12, 0x0

    .line 2613595
    const/4 v13, 0x0

    .line 2613596
    const/4 v14, 0x0

    .line 2613597
    const/4 v15, 0x0

    .line 2613598
    const/16 v16, 0x0

    .line 2613599
    invoke-virtual/range {p0 .. p0}, LX/1su;->r()LX/1sv;

    .line 2613600
    :goto_0
    invoke-virtual/range {p0 .. p0}, LX/1su;->f()LX/1sw;

    move-result-object v2

    .line 2613601
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v17, v0

    if-eqz v17, :cond_e

    .line 2613602
    iget-short v0, v2, LX/1sw;->c:S

    move/from16 v17, v0

    sparse-switch v17, :sswitch_data_0

    .line 2613603
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2613604
    :sswitch_0
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v17, v0

    const/16 v18, 0xa

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_0

    .line 2613605
    invoke-virtual/range {p0 .. p0}, LX/1su;->n()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    goto :goto_0

    .line 2613606
    :cond_0
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2613607
    :sswitch_1
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v17, v0

    const/16 v18, 0xa

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_1

    .line 2613608
    invoke-virtual/range {p0 .. p0}, LX/1su;->n()J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    goto :goto_0

    .line 2613609
    :cond_1
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2613610
    :sswitch_2
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v17, v0

    const/16 v18, 0xa

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_2

    .line 2613611
    invoke-virtual/range {p0 .. p0}, LX/1su;->n()J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    goto :goto_0

    .line 2613612
    :cond_2
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2613613
    :sswitch_3
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v17, v0

    const/16 v18, 0xa

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_3

    .line 2613614
    invoke-virtual/range {p0 .. p0}, LX/1su;->n()J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    goto/16 :goto_0

    .line 2613615
    :cond_3
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2613616
    :sswitch_4
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v17, v0

    const/16 v18, 0x8

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_4

    .line 2613617
    invoke-virtual/range {p0 .. p0}, LX/1su;->m()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    goto/16 :goto_0

    .line 2613618
    :cond_4
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2613619
    :sswitch_5
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v17, v0

    const/16 v18, 0xb

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_5

    .line 2613620
    invoke-virtual/range {p0 .. p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_0

    .line 2613621
    :cond_5
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2613622
    :sswitch_6
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v17, v0

    const/16 v18, 0xa

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_6

    .line 2613623
    invoke-virtual/range {p0 .. p0}, LX/1su;->n()J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    goto/16 :goto_0

    .line 2613624
    :cond_6
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2613625
    :sswitch_7
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v17, v0

    const/16 v18, 0x8

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_7

    .line 2613626
    invoke-virtual/range {p0 .. p0}, LX/1su;->m()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    goto/16 :goto_0

    .line 2613627
    :cond_7
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2613628
    :sswitch_8
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v17, v0

    const/16 v18, 0xa

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_8

    .line 2613629
    invoke-virtual/range {p0 .. p0}, LX/1su;->n()J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    goto/16 :goto_0

    .line 2613630
    :cond_8
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2613631
    :sswitch_9
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v17, v0

    const/16 v18, 0xb

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_9

    .line 2613632
    invoke-virtual/range {p0 .. p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v12

    goto/16 :goto_0

    .line 2613633
    :cond_9
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2613634
    :sswitch_a
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v17, v0

    const/16 v18, 0x2

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_a

    .line 2613635
    invoke-virtual/range {p0 .. p0}, LX/1su;->j()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    goto/16 :goto_0

    .line 2613636
    :cond_a
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2613637
    :sswitch_b
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v17, v0

    const/16 v18, 0xa

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_b

    .line 2613638
    invoke-virtual/range {p0 .. p0}, LX/1su;->n()J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    goto/16 :goto_0

    .line 2613639
    :cond_b
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2613640
    :sswitch_c
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v17, v0

    const/16 v18, 0xa

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_c

    .line 2613641
    invoke-virtual/range {p0 .. p0}, LX/1su;->n()J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    goto/16 :goto_0

    .line 2613642
    :cond_c
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2613643
    :sswitch_d
    iget-byte v0, v2, LX/1sw;->b:B

    move/from16 v17, v0

    const/16 v18, 0xa

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_d

    .line 2613644
    invoke-virtual/range {p0 .. p0}, LX/1su;->n()J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    goto/16 :goto_0

    .line 2613645
    :cond_d
    iget-byte v2, v2, LX/1sw;->b:B

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2613646
    :cond_e
    invoke-virtual/range {p0 .. p0}, LX/1su;->e()V

    .line 2613647
    new-instance v2, LX/Ipk;

    invoke-direct/range {v2 .. v16}, LX/Ipk;-><init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;)V

    .line 2613648
    invoke-static {v2}, LX/Ipk;->a(LX/Ipk;)V

    .line 2613649
    return-object v2

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x4 -> :sswitch_3
        0x5 -> :sswitch_4
        0x6 -> :sswitch_5
        0x7 -> :sswitch_6
        0x8 -> :sswitch_7
        0x9 -> :sswitch_8
        0xa -> :sswitch_9
        0xb -> :sswitch_a
        0xc -> :sswitch_b
        0xd -> :sswitch_c
        0x3e8 -> :sswitch_d
    .end sparse-switch
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 2613449
    if-eqz p2, :cond_7

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 2613450
    :goto_0
    if-eqz p2, :cond_8

    const-string v0, "\n"

    move-object v2, v0

    .line 2613451
    :goto_1
    if-eqz p2, :cond_9

    const-string v0, " "

    move-object v1, v0

    .line 2613452
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v0, "DeltaNewPaymentRequest"

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2613453
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613454
    const-string v0, "("

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613455
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613456
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613457
    const-string v0, "requestFbId"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613458
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613459
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613460
    iget-object v0, p0, LX/Ipk;->requestFbId:Ljava/lang/Long;

    if-nez v0, :cond_a

    .line 2613461
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613462
    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613463
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613464
    const-string v0, "requesterFbId"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613465
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613466
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613467
    iget-object v0, p0, LX/Ipk;->requesterFbId:Ljava/lang/Long;

    if-nez v0, :cond_b

    .line 2613468
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613469
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613470
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613471
    const-string v0, "requesteeFbId"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613472
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613473
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613474
    iget-object v0, p0, LX/Ipk;->requesteeFbId:Ljava/lang/Long;

    if-nez v0, :cond_c

    .line 2613475
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613476
    :goto_5
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613477
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613478
    const-string v0, "timestampMs"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613479
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613480
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613481
    iget-object v0, p0, LX/Ipk;->timestampMs:Ljava/lang/Long;

    if-nez v0, :cond_d

    .line 2613482
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613483
    :goto_6
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613484
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613485
    const-string v0, "initialStatus"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613486
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613487
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613488
    iget-object v0, p0, LX/Ipk;->initialStatus:Ljava/lang/Integer;

    if-nez v0, :cond_e

    .line 2613489
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613490
    :cond_0
    :goto_7
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613491
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613492
    const-string v0, "currency"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613493
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613494
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613495
    iget-object v0, p0, LX/Ipk;->currency:Ljava/lang/String;

    if-nez v0, :cond_10

    .line 2613496
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613497
    :goto_8
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613498
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613499
    const-string v0, "amount"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613500
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613501
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613502
    iget-object v0, p0, LX/Ipk;->amount:Ljava/lang/Long;

    if-nez v0, :cond_11

    .line 2613503
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613504
    :goto_9
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613505
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613506
    const-string v0, "amountOffset"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613507
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613508
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613509
    iget-object v0, p0, LX/Ipk;->amountOffset:Ljava/lang/Integer;

    if-nez v0, :cond_12

    .line 2613510
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613511
    :goto_a
    iget-object v0, p0, LX/Ipk;->offlineThreadingId:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 2613512
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613513
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613514
    const-string v0, "offlineThreadingId"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613515
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613516
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613517
    iget-object v0, p0, LX/Ipk;->offlineThreadingId:Ljava/lang/Long;

    if-nez v0, :cond_13

    .line 2613518
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613519
    :cond_1
    :goto_b
    iget-object v0, p0, LX/Ipk;->memoText:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 2613520
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613521
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613522
    const-string v0, "memoText"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613523
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613524
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613525
    iget-object v0, p0, LX/Ipk;->memoText:Ljava/lang/String;

    if-nez v0, :cond_14

    .line 2613526
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613527
    :cond_2
    :goto_c
    iget-object v0, p0, LX/Ipk;->hasMemoMultimedia:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 2613528
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613529
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613530
    const-string v0, "hasMemoMultimedia"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613531
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613532
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613533
    iget-object v0, p0, LX/Ipk;->hasMemoMultimedia:Ljava/lang/Boolean;

    if-nez v0, :cond_15

    .line 2613534
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613535
    :cond_3
    :goto_d
    iget-object v0, p0, LX/Ipk;->themeId:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 2613536
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613537
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613538
    const-string v0, "themeId"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613539
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613540
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613541
    iget-object v0, p0, LX/Ipk;->themeId:Ljava/lang/Long;

    if-nez v0, :cond_16

    .line 2613542
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613543
    :cond_4
    :goto_e
    iget-object v0, p0, LX/Ipk;->groupThreadFbId:Ljava/lang/Long;

    if-eqz v0, :cond_5

    .line 2613544
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613545
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613546
    const-string v0, "groupThreadFbId"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613547
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613548
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613549
    iget-object v0, p0, LX/Ipk;->groupThreadFbId:Ljava/lang/Long;

    if-nez v0, :cond_17

    .line 2613550
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613551
    :cond_5
    :goto_f
    iget-object v0, p0, LX/Ipk;->irisSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_6

    .line 2613552
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613553
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613554
    const-string v0, "irisSeqId"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613555
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613556
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613557
    iget-object v0, p0, LX/Ipk;->irisSeqId:Ljava/lang/Long;

    if-nez v0, :cond_18

    .line 2613558
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613559
    :cond_6
    :goto_10
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613560
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613561
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2613562
    :cond_7
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_0

    .line 2613563
    :cond_8
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_1

    .line 2613564
    :cond_9
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_2

    .line 2613565
    :cond_a
    iget-object v0, p0, LX/Ipk;->requestFbId:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 2613566
    :cond_b
    iget-object v0, p0, LX/Ipk;->requesterFbId:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 2613567
    :cond_c
    iget-object v0, p0, LX/Ipk;->requesteeFbId:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 2613568
    :cond_d
    iget-object v0, p0, LX/Ipk;->timestampMs:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 2613569
    :cond_e
    sget-object v0, LX/Ipz;->b:Ljava/util/Map;

    iget-object v5, p0, LX/Ipk;->initialStatus:Ljava/lang/Integer;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2613570
    if-eqz v0, :cond_f

    .line 2613571
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613572
    const-string v5, " ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2613573
    :cond_f
    iget-object v5, p0, LX/Ipk;->initialStatus:Ljava/lang/Integer;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2613574
    if-eqz v0, :cond_0

    .line 2613575
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_7

    .line 2613576
    :cond_10
    iget-object v0, p0, LX/Ipk;->currency:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_8

    .line 2613577
    :cond_11
    iget-object v0, p0, LX/Ipk;->amount:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_9

    .line 2613578
    :cond_12
    iget-object v0, p0, LX/Ipk;->amountOffset:Ljava/lang/Integer;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_a

    .line 2613579
    :cond_13
    iget-object v0, p0, LX/Ipk;->offlineThreadingId:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_b

    .line 2613580
    :cond_14
    iget-object v0, p0, LX/Ipk;->memoText:Ljava/lang/String;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_c

    .line 2613581
    :cond_15
    iget-object v0, p0, LX/Ipk;->hasMemoMultimedia:Ljava/lang/Boolean;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_d

    .line 2613582
    :cond_16
    iget-object v0, p0, LX/Ipk;->themeId:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_e

    .line 2613583
    :cond_17
    iget-object v0, p0, LX/Ipk;->groupThreadFbId:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_f

    .line 2613584
    :cond_18
    iget-object v0, p0, LX/Ipk;->irisSeqId:Ljava/lang/Long;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_10
.end method

.method public final a(LX/1su;)V
    .locals 2

    .prologue
    .line 2613396
    invoke-static {p0}, LX/Ipk;->a(LX/Ipk;)V

    .line 2613397
    invoke-virtual {p1}, LX/1su;->a()V

    .line 2613398
    iget-object v0, p0, LX/Ipk;->requestFbId:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 2613399
    sget-object v0, LX/Ipk;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2613400
    iget-object v0, p0, LX/Ipk;->requestFbId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2613401
    :cond_0
    iget-object v0, p0, LX/Ipk;->requesterFbId:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 2613402
    sget-object v0, LX/Ipk;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2613403
    iget-object v0, p0, LX/Ipk;->requesterFbId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2613404
    :cond_1
    iget-object v0, p0, LX/Ipk;->requesteeFbId:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 2613405
    sget-object v0, LX/Ipk;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2613406
    iget-object v0, p0, LX/Ipk;->requesteeFbId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2613407
    :cond_2
    iget-object v0, p0, LX/Ipk;->timestampMs:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 2613408
    sget-object v0, LX/Ipk;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2613409
    iget-object v0, p0, LX/Ipk;->timestampMs:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2613410
    :cond_3
    iget-object v0, p0, LX/Ipk;->initialStatus:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 2613411
    sget-object v0, LX/Ipk;->g:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2613412
    iget-object v0, p0, LX/Ipk;->initialStatus:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 2613413
    :cond_4
    iget-object v0, p0, LX/Ipk;->currency:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 2613414
    sget-object v0, LX/Ipk;->h:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2613415
    iget-object v0, p0, LX/Ipk;->currency:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 2613416
    :cond_5
    iget-object v0, p0, LX/Ipk;->amount:Ljava/lang/Long;

    if-eqz v0, :cond_6

    .line 2613417
    sget-object v0, LX/Ipk;->i:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2613418
    iget-object v0, p0, LX/Ipk;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2613419
    :cond_6
    iget-object v0, p0, LX/Ipk;->amountOffset:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 2613420
    sget-object v0, LX/Ipk;->j:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2613421
    iget-object v0, p0, LX/Ipk;->amountOffset:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 2613422
    :cond_7
    iget-object v0, p0, LX/Ipk;->offlineThreadingId:Ljava/lang/Long;

    if-eqz v0, :cond_8

    .line 2613423
    iget-object v0, p0, LX/Ipk;->offlineThreadingId:Ljava/lang/Long;

    if-eqz v0, :cond_8

    .line 2613424
    sget-object v0, LX/Ipk;->k:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2613425
    iget-object v0, p0, LX/Ipk;->offlineThreadingId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2613426
    :cond_8
    iget-object v0, p0, LX/Ipk;->memoText:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 2613427
    iget-object v0, p0, LX/Ipk;->memoText:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 2613428
    sget-object v0, LX/Ipk;->l:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2613429
    iget-object v0, p0, LX/Ipk;->memoText:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 2613430
    :cond_9
    iget-object v0, p0, LX/Ipk;->hasMemoMultimedia:Ljava/lang/Boolean;

    if-eqz v0, :cond_a

    .line 2613431
    iget-object v0, p0, LX/Ipk;->hasMemoMultimedia:Ljava/lang/Boolean;

    if-eqz v0, :cond_a

    .line 2613432
    sget-object v0, LX/Ipk;->m:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2613433
    iget-object v0, p0, LX/Ipk;->hasMemoMultimedia:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 2613434
    :cond_a
    iget-object v0, p0, LX/Ipk;->themeId:Ljava/lang/Long;

    if-eqz v0, :cond_b

    .line 2613435
    iget-object v0, p0, LX/Ipk;->themeId:Ljava/lang/Long;

    if-eqz v0, :cond_b

    .line 2613436
    sget-object v0, LX/Ipk;->n:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2613437
    iget-object v0, p0, LX/Ipk;->themeId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2613438
    :cond_b
    iget-object v0, p0, LX/Ipk;->groupThreadFbId:Ljava/lang/Long;

    if-eqz v0, :cond_c

    .line 2613439
    iget-object v0, p0, LX/Ipk;->groupThreadFbId:Ljava/lang/Long;

    if-eqz v0, :cond_c

    .line 2613440
    sget-object v0, LX/Ipk;->o:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2613441
    iget-object v0, p0, LX/Ipk;->groupThreadFbId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2613442
    :cond_c
    iget-object v0, p0, LX/Ipk;->irisSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_d

    .line 2613443
    iget-object v0, p0, LX/Ipk;->irisSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_d

    .line 2613444
    sget-object v0, LX/Ipk;->p:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2613445
    iget-object v0, p0, LX/Ipk;->irisSeqId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2613446
    :cond_d
    invoke-virtual {p1}, LX/1su;->c()V

    .line 2613447
    invoke-virtual {p1}, LX/1su;->b()V

    .line 2613448
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2613290
    if-nez p1, :cond_1

    .line 2613291
    :cond_0
    :goto_0
    return v0

    .line 2613292
    :cond_1
    instance-of v1, p1, LX/Ipk;

    if-eqz v1, :cond_0

    .line 2613293
    check-cast p1, LX/Ipk;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2613294
    if-nez p1, :cond_3

    .line 2613295
    :cond_2
    :goto_1
    move v0, v2

    .line 2613296
    goto :goto_0

    .line 2613297
    :cond_3
    iget-object v0, p0, LX/Ipk;->requestFbId:Ljava/lang/Long;

    if-eqz v0, :cond_20

    move v0, v1

    .line 2613298
    :goto_2
    iget-object v3, p1, LX/Ipk;->requestFbId:Ljava/lang/Long;

    if-eqz v3, :cond_21

    move v3, v1

    .line 2613299
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 2613300
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2613301
    iget-object v0, p0, LX/Ipk;->requestFbId:Ljava/lang/Long;

    iget-object v3, p1, LX/Ipk;->requestFbId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2613302
    :cond_5
    iget-object v0, p0, LX/Ipk;->requesterFbId:Ljava/lang/Long;

    if-eqz v0, :cond_22

    move v0, v1

    .line 2613303
    :goto_4
    iget-object v3, p1, LX/Ipk;->requesterFbId:Ljava/lang/Long;

    if-eqz v3, :cond_23

    move v3, v1

    .line 2613304
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 2613305
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2613306
    iget-object v0, p0, LX/Ipk;->requesterFbId:Ljava/lang/Long;

    iget-object v3, p1, LX/Ipk;->requesterFbId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2613307
    :cond_7
    iget-object v0, p0, LX/Ipk;->requesteeFbId:Ljava/lang/Long;

    if-eqz v0, :cond_24

    move v0, v1

    .line 2613308
    :goto_6
    iget-object v3, p1, LX/Ipk;->requesteeFbId:Ljava/lang/Long;

    if-eqz v3, :cond_25

    move v3, v1

    .line 2613309
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 2613310
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2613311
    iget-object v0, p0, LX/Ipk;->requesteeFbId:Ljava/lang/Long;

    iget-object v3, p1, LX/Ipk;->requesteeFbId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2613312
    :cond_9
    iget-object v0, p0, LX/Ipk;->timestampMs:Ljava/lang/Long;

    if-eqz v0, :cond_26

    move v0, v1

    .line 2613313
    :goto_8
    iget-object v3, p1, LX/Ipk;->timestampMs:Ljava/lang/Long;

    if-eqz v3, :cond_27

    move v3, v1

    .line 2613314
    :goto_9
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 2613315
    :cond_a
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2613316
    iget-object v0, p0, LX/Ipk;->timestampMs:Ljava/lang/Long;

    iget-object v3, p1, LX/Ipk;->timestampMs:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2613317
    :cond_b
    iget-object v0, p0, LX/Ipk;->initialStatus:Ljava/lang/Integer;

    if-eqz v0, :cond_28

    move v0, v1

    .line 2613318
    :goto_a
    iget-object v3, p1, LX/Ipk;->initialStatus:Ljava/lang/Integer;

    if-eqz v3, :cond_29

    move v3, v1

    .line 2613319
    :goto_b
    if-nez v0, :cond_c

    if-eqz v3, :cond_d

    .line 2613320
    :cond_c
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2613321
    iget-object v0, p0, LX/Ipk;->initialStatus:Ljava/lang/Integer;

    iget-object v3, p1, LX/Ipk;->initialStatus:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2613322
    :cond_d
    iget-object v0, p0, LX/Ipk;->currency:Ljava/lang/String;

    if-eqz v0, :cond_2a

    move v0, v1

    .line 2613323
    :goto_c
    iget-object v3, p1, LX/Ipk;->currency:Ljava/lang/String;

    if-eqz v3, :cond_2b

    move v3, v1

    .line 2613324
    :goto_d
    if-nez v0, :cond_e

    if-eqz v3, :cond_f

    .line 2613325
    :cond_e
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2613326
    iget-object v0, p0, LX/Ipk;->currency:Ljava/lang/String;

    iget-object v3, p1, LX/Ipk;->currency:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2613327
    :cond_f
    iget-object v0, p0, LX/Ipk;->amount:Ljava/lang/Long;

    if-eqz v0, :cond_2c

    move v0, v1

    .line 2613328
    :goto_e
    iget-object v3, p1, LX/Ipk;->amount:Ljava/lang/Long;

    if-eqz v3, :cond_2d

    move v3, v1

    .line 2613329
    :goto_f
    if-nez v0, :cond_10

    if-eqz v3, :cond_11

    .line 2613330
    :cond_10
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2613331
    iget-object v0, p0, LX/Ipk;->amount:Ljava/lang/Long;

    iget-object v3, p1, LX/Ipk;->amount:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2613332
    :cond_11
    iget-object v0, p0, LX/Ipk;->amountOffset:Ljava/lang/Integer;

    if-eqz v0, :cond_2e

    move v0, v1

    .line 2613333
    :goto_10
    iget-object v3, p1, LX/Ipk;->amountOffset:Ljava/lang/Integer;

    if-eqz v3, :cond_2f

    move v3, v1

    .line 2613334
    :goto_11
    if-nez v0, :cond_12

    if-eqz v3, :cond_13

    .line 2613335
    :cond_12
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2613336
    iget-object v0, p0, LX/Ipk;->amountOffset:Ljava/lang/Integer;

    iget-object v3, p1, LX/Ipk;->amountOffset:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2613337
    :cond_13
    iget-object v0, p0, LX/Ipk;->offlineThreadingId:Ljava/lang/Long;

    if-eqz v0, :cond_30

    move v0, v1

    .line 2613338
    :goto_12
    iget-object v3, p1, LX/Ipk;->offlineThreadingId:Ljava/lang/Long;

    if-eqz v3, :cond_31

    move v3, v1

    .line 2613339
    :goto_13
    if-nez v0, :cond_14

    if-eqz v3, :cond_15

    .line 2613340
    :cond_14
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2613341
    iget-object v0, p0, LX/Ipk;->offlineThreadingId:Ljava/lang/Long;

    iget-object v3, p1, LX/Ipk;->offlineThreadingId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2613342
    :cond_15
    iget-object v0, p0, LX/Ipk;->memoText:Ljava/lang/String;

    if-eqz v0, :cond_32

    move v0, v1

    .line 2613343
    :goto_14
    iget-object v3, p1, LX/Ipk;->memoText:Ljava/lang/String;

    if-eqz v3, :cond_33

    move v3, v1

    .line 2613344
    :goto_15
    if-nez v0, :cond_16

    if-eqz v3, :cond_17

    .line 2613345
    :cond_16
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2613346
    iget-object v0, p0, LX/Ipk;->memoText:Ljava/lang/String;

    iget-object v3, p1, LX/Ipk;->memoText:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2613347
    :cond_17
    iget-object v0, p0, LX/Ipk;->hasMemoMultimedia:Ljava/lang/Boolean;

    if-eqz v0, :cond_34

    move v0, v1

    .line 2613348
    :goto_16
    iget-object v3, p1, LX/Ipk;->hasMemoMultimedia:Ljava/lang/Boolean;

    if-eqz v3, :cond_35

    move v3, v1

    .line 2613349
    :goto_17
    if-nez v0, :cond_18

    if-eqz v3, :cond_19

    .line 2613350
    :cond_18
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2613351
    iget-object v0, p0, LX/Ipk;->hasMemoMultimedia:Ljava/lang/Boolean;

    iget-object v3, p1, LX/Ipk;->hasMemoMultimedia:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2613352
    :cond_19
    iget-object v0, p0, LX/Ipk;->themeId:Ljava/lang/Long;

    if-eqz v0, :cond_36

    move v0, v1

    .line 2613353
    :goto_18
    iget-object v3, p1, LX/Ipk;->themeId:Ljava/lang/Long;

    if-eqz v3, :cond_37

    move v3, v1

    .line 2613354
    :goto_19
    if-nez v0, :cond_1a

    if-eqz v3, :cond_1b

    .line 2613355
    :cond_1a
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2613356
    iget-object v0, p0, LX/Ipk;->themeId:Ljava/lang/Long;

    iget-object v3, p1, LX/Ipk;->themeId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2613357
    :cond_1b
    iget-object v0, p0, LX/Ipk;->groupThreadFbId:Ljava/lang/Long;

    if-eqz v0, :cond_38

    move v0, v1

    .line 2613358
    :goto_1a
    iget-object v3, p1, LX/Ipk;->groupThreadFbId:Ljava/lang/Long;

    if-eqz v3, :cond_39

    move v3, v1

    .line 2613359
    :goto_1b
    if-nez v0, :cond_1c

    if-eqz v3, :cond_1d

    .line 2613360
    :cond_1c
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2613361
    iget-object v0, p0, LX/Ipk;->groupThreadFbId:Ljava/lang/Long;

    iget-object v3, p1, LX/Ipk;->groupThreadFbId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2613362
    :cond_1d
    iget-object v0, p0, LX/Ipk;->irisSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_3a

    move v0, v1

    .line 2613363
    :goto_1c
    iget-object v3, p1, LX/Ipk;->irisSeqId:Ljava/lang/Long;

    if-eqz v3, :cond_3b

    move v3, v1

    .line 2613364
    :goto_1d
    if-nez v0, :cond_1e

    if-eqz v3, :cond_1f

    .line 2613365
    :cond_1e
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2613366
    iget-object v0, p0, LX/Ipk;->irisSeqId:Ljava/lang/Long;

    iget-object v3, p1, LX/Ipk;->irisSeqId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1f
    move v2, v1

    .line 2613367
    goto/16 :goto_1

    :cond_20
    move v0, v2

    .line 2613368
    goto/16 :goto_2

    :cond_21
    move v3, v2

    .line 2613369
    goto/16 :goto_3

    :cond_22
    move v0, v2

    .line 2613370
    goto/16 :goto_4

    :cond_23
    move v3, v2

    .line 2613371
    goto/16 :goto_5

    :cond_24
    move v0, v2

    .line 2613372
    goto/16 :goto_6

    :cond_25
    move v3, v2

    .line 2613373
    goto/16 :goto_7

    :cond_26
    move v0, v2

    .line 2613374
    goto/16 :goto_8

    :cond_27
    move v3, v2

    .line 2613375
    goto/16 :goto_9

    :cond_28
    move v0, v2

    .line 2613376
    goto/16 :goto_a

    :cond_29
    move v3, v2

    .line 2613377
    goto/16 :goto_b

    :cond_2a
    move v0, v2

    .line 2613378
    goto/16 :goto_c

    :cond_2b
    move v3, v2

    .line 2613379
    goto/16 :goto_d

    :cond_2c
    move v0, v2

    .line 2613380
    goto/16 :goto_e

    :cond_2d
    move v3, v2

    .line 2613381
    goto/16 :goto_f

    :cond_2e
    move v0, v2

    .line 2613382
    goto/16 :goto_10

    :cond_2f
    move v3, v2

    .line 2613383
    goto/16 :goto_11

    :cond_30
    move v0, v2

    .line 2613384
    goto/16 :goto_12

    :cond_31
    move v3, v2

    .line 2613385
    goto/16 :goto_13

    :cond_32
    move v0, v2

    .line 2613386
    goto/16 :goto_14

    :cond_33
    move v3, v2

    .line 2613387
    goto/16 :goto_15

    :cond_34
    move v0, v2

    .line 2613388
    goto/16 :goto_16

    :cond_35
    move v3, v2

    .line 2613389
    goto/16 :goto_17

    :cond_36
    move v0, v2

    .line 2613390
    goto/16 :goto_18

    :cond_37
    move v3, v2

    .line 2613391
    goto/16 :goto_19

    :cond_38
    move v0, v2

    .line 2613392
    goto :goto_1a

    :cond_39
    move v3, v2

    .line 2613393
    goto :goto_1b

    :cond_3a
    move v0, v2

    .line 2613394
    goto :goto_1c

    :cond_3b
    move v3, v2

    .line 2613395
    goto :goto_1d
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2613289
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2613286
    sget-boolean v0, LX/Ipk;->a:Z

    .line 2613287
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/Ipk;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2613288
    return-object v0
.end method
