.class public LX/HN4;
.super LX/1Cv;
.source ""


# instance fields
.field private a:Landroid/view/LayoutInflater;

.field public final b:LX/0if;

.field private c:Z

.field public d:Landroid/view/View$OnClickListener;

.field public e:LX/HMw;

.field public f:LX/HMi;

.field public g:LX/HMh;

.field public h:Ljava/lang/String;

.field public i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/9Zu;",
            ">;"
        }
    .end annotation
.end field

.field public j:Ljava/lang/String;

.field public k:Z

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;LX/0if;)V
    .locals 1

    .prologue
    .line 2458069
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 2458070
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/HN4;->c:Z

    .line 2458071
    iput-object p1, p0, LX/HN4;->a:Landroid/view/LayoutInflater;

    .line 2458072
    iput-object p2, p0, LX/HN4;->b:LX/0if;

    .line 2458073
    return-void
.end method

.method private a(I)LX/HN2;
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 2458074
    invoke-virtual {p0}, LX/HN4;->getCount()I

    move-result v0

    invoke-static {p1, v0}, LX/0PB;->checkPositionIndex(II)I

    .line 2458075
    const/4 v0, 0x0

    .line 2458076
    invoke-direct {p0}, LX/HN4;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2458077
    add-int/lit8 v0, p1, 0x0

    if-nez v0, :cond_0

    .line 2458078
    new-instance v0, LX/HN2;

    sget-object v1, LX/HN3;->ADMIN_PUBLISH_SERVICES_TEXT:LX/HN3;

    invoke-direct {v0, v1, v2}, LX/HN2;-><init>(LX/HN3;I)V

    .line 2458079
    :goto_0
    return-object v0

    .line 2458080
    :cond_0
    const/4 v0, 0x1

    .line 2458081
    :cond_1
    iget-boolean v1, p0, LX/HN4;->c:Z

    if-eqz v1, :cond_3

    .line 2458082
    sub-int v1, p1, v0

    if-nez v1, :cond_2

    .line 2458083
    new-instance v0, LX/HN2;

    sget-object v1, LX/HN3;->ADD_SERVICE_BUTTON:LX/HN3;

    invoke-direct {v0, v1, v2}, LX/HN2;-><init>(LX/HN3;I)V

    goto :goto_0

    .line 2458084
    :cond_2
    add-int/lit8 v0, v0, 0x1

    .line 2458085
    :cond_3
    iget-object v1, p0, LX/HN4;->h:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 2458086
    sub-int v1, p1, v0

    if-nez v1, :cond_4

    .line 2458087
    new-instance v0, LX/HN2;

    sget-object v1, LX/HN3;->INTRO_MESSAGE:LX/HN3;

    invoke-direct {v0, v1, v2}, LX/HN2;-><init>(LX/HN3;I)V

    goto :goto_0

    .line 2458088
    :cond_4
    add-int/lit8 v0, v0, 0x1

    .line 2458089
    :cond_5
    iget-object v1, p0, LX/HN4;->i:Ljava/util/List;

    invoke-static {v1}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2458090
    new-instance v1, LX/HN2;

    sget-object v2, LX/HN3;->SERVICE_ITEM:LX/HN3;

    sub-int v0, p1, v0

    invoke-direct {v1, v2, v0}, LX/HN2;-><init>(LX/HN3;I)V

    move-object v0, v1

    goto :goto_0

    .line 2458091
    :cond_6
    iget-boolean v0, p0, LX/HN4;->k:Z

    if-eqz v0, :cond_7

    .line 2458092
    new-instance v0, LX/HN2;

    sget-object v1, LX/HN3;->ADMIN_NO_SERVICES_TEXT:LX/HN3;

    invoke-direct {v0, v1, v2}, LX/HN2;-><init>(LX/HN3;I)V

    goto :goto_0

    .line 2458093
    :cond_7
    new-instance v0, LX/HN2;

    sget-object v1, LX/HN3;->EMPTY_LIST_TEXT:LX/HN3;

    invoke-direct {v0, v1, v2}, LX/HN2;-><init>(LX/HN3;I)V

    goto :goto_0
.end method

.method private a()Z
    .locals 2

    .prologue
    .line 2458111
    const-string v0, "admin_publish_services"

    iget-object v1, p0, LX/HN4;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "admin_publish_services_edit_flow"

    iget-object v1, p0, LX/HN4;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2458094
    sget-object v0, LX/HN3;->SERVICE_ITEM:LX/HN3;

    invoke-virtual {v0}, LX/HN3;->ordinal()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 2458095
    new-instance v0, LX/HMf;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/HMf;-><init>(Landroid/content/Context;)V

    .line 2458096
    :goto_0
    return-object v0

    .line 2458097
    :cond_0
    sget-object v0, LX/HN3;->ADD_SERVICE_BUTTON:LX/HN3;

    invoke-virtual {v0}, LX/HN3;->ordinal()I

    move-result v0

    if-ne p1, v0, :cond_1

    .line 2458098
    iget-object v0, p0, LX/HN4;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f030ee3

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 2458099
    :cond_1
    sget-object v0, LX/HN3;->INTRO_MESSAGE:LX/HN3;

    invoke-virtual {v0}, LX/HN3;->ordinal()I

    move-result v0

    if-ne p1, v0, :cond_2

    .line 2458100
    iget-object v0, p0, LX/HN4;->h:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2458101
    iget-object v0, p0, LX/HN4;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f030ee6

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2458102
    iget-object v1, p0, LX/HN4;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2458103
    const v1, 0x7f0a00d5

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setBackgroundResource(I)V

    goto :goto_0

    .line 2458104
    :cond_2
    sget-object v0, LX/HN3;->EMPTY_LIST_TEXT:LX/HN3;

    invoke-virtual {v0}, LX/HN3;->ordinal()I

    move-result v0

    if-ne p1, v0, :cond_3

    .line 2458105
    iget-object v0, p0, LX/HN4;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f030ee4

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 2458106
    :cond_3
    sget-object v0, LX/HN3;->ADMIN_NO_SERVICES_TEXT:LX/HN3;

    invoke-virtual {v0}, LX/HN3;->ordinal()I

    move-result v0

    if-ne p1, v0, :cond_4

    .line 2458107
    iget-object v0, p0, LX/HN4;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f030ee1

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 2458108
    :cond_4
    sget-object v0, LX/HN3;->ADMIN_PUBLISH_SERVICES_TEXT:LX/HN3;

    invoke-virtual {v0}, LX/HN3;->ordinal()I

    move-result v0

    if-ne p1, v0, :cond_5

    .line 2458109
    iget-object v0, p0, LX/HN4;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f030ee7

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 2458110
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 5

    .prologue
    .line 2458053
    sget-object v0, LX/HN3;->SERVICE_ITEM:LX/HN3;

    invoke-virtual {v0}, LX/HN3;->ordinal()I

    move-result v0

    if-ne p4, v0, :cond_1

    .line 2458054
    instance-of v0, p3, LX/HMf;

    if-eqz v0, :cond_0

    .line 2458055
    check-cast p2, LX/9Zu;

    move-object v0, p3

    .line 2458056
    check-cast v0, LX/HMf;

    invoke-static {p2}, LX/HMg;->a(LX/9Zu;)Landroid/net/Uri;

    move-result-object v1

    invoke-interface {p2}, LX/9Zu;->e()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2}, LX/9Zu;->c()Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/HN0;

    invoke-direct {v4, p0, p2}, LX/HN0;-><init>(LX/HN4;LX/9Zu;)V

    invoke-virtual {v0, v1, v2, v3, v4}, LX/HMf;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 2458057
    invoke-interface {p2}, LX/9Zu;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, LX/9Zu;->d()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/HN4;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2458058
    check-cast p3, LX/HMf;

    const/16 v0, 0x5dc

    .line 2458059
    const v1, 0x7f0213a4

    invoke-virtual {p3, v1}, LX/HMf;->setBackgroundResource(I)V

    .line 2458060
    invoke-virtual {p3}, LX/HMf;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/TransitionDrawable;

    .line 2458061
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 2458062
    :cond_0
    :goto_0
    return-void

    .line 2458063
    :cond_1
    sget-object v0, LX/HN3;->ADD_SERVICE_BUTTON:LX/HN3;

    invoke-virtual {v0}, LX/HN3;->ordinal()I

    move-result v0

    if-ne p4, v0, :cond_0

    .line 2458064
    check-cast p2, Landroid/view/View$OnClickListener;

    invoke-virtual {p3, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1    # Landroid/view/View$OnClickListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2458065
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/HN4;->c:Z

    .line 2458066
    iput-object p1, p0, LX/HN4;->d:Landroid/view/View$OnClickListener;

    .line 2458067
    return-void

    .line 2458068
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getCount()I
    .locals 2

    .prologue
    .line 2458045
    const/4 v0, 0x0

    .line 2458046
    invoke-direct {p0}, LX/HN4;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2458047
    const/4 v0, 0x1

    .line 2458048
    :cond_0
    iget-boolean v1, p0, LX/HN4;->c:Z

    if-eqz v1, :cond_1

    .line 2458049
    add-int/lit8 v0, v0, 0x1

    .line 2458050
    :cond_1
    iget-object v1, p0, LX/HN4;->h:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 2458051
    add-int/lit8 v0, v0, 0x1

    .line 2458052
    :cond_2
    iget-object v1, p0, LX/HN4;->i:Ljava/util/List;

    invoke-static {v1}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/HN4;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    :goto_0
    return v0

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2458035
    invoke-direct {p0, p1}, LX/HN4;->a(I)LX/HN2;

    move-result-object v0

    .line 2458036
    sget-object v1, LX/HN1;->a:[I

    iget-object v2, v0, LX/HN2;->a:LX/HN3;

    invoke-virtual {v2}, LX/HN3;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2458037
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2458038
    :pswitch_0
    iget-object v0, p0, LX/HN4;->d:Landroid/view/View$OnClickListener;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2458039
    iget-object v0, p0, LX/HN4;->d:Landroid/view/View$OnClickListener;

    move-object v0, v0

    .line 2458040
    goto :goto_0

    .line 2458041
    :pswitch_1
    iget v0, v0, LX/HN2;->b:I

    .line 2458042
    iget-object v1, p0, LX/HN4;->i:Ljava/util/List;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2458043
    iget-object v1, p0, LX/HN4;->i:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/9Zu;

    move-object v0, v1

    .line 2458044
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2458034
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2458033
    invoke-direct {p0, p1}, LX/HN4;->a(I)LX/HN2;

    move-result-object v0

    iget-object v0, v0, LX/HN2;->a:LX/HN3;

    invoke-virtual {v0}, LX/HN3;->ordinal()I

    move-result v0

    return v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2458032
    invoke-static {}, LX/HN3;->values()[LX/HN3;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
