.class public LX/HiN;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/HiO;

.field private final b:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(LX/HiO;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2497283
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2497284
    iput-object p1, p0, LX/HiN;->a:LX/HiO;

    .line 2497285
    iput-object p2, p0, LX/HiN;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2497286
    return-void
.end method

.method private static b(LX/HiM;)LX/0Tn;
    .locals 2

    .prologue
    .line 2497287
    sget-object v0, LX/HiL;->a:[I

    invoke-virtual {p0}, LX/HiM;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2497288
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown source type."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2497289
    :pswitch_0
    sget-object v0, LX/0db;->aJ:LX/0Tn;

    .line 2497290
    :goto_0
    return-object v0

    :pswitch_1
    sget-object v0, LX/0db;->aK:LX/0Tn;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static b(LX/0QB;)LX/HiN;
    .locals 4

    .prologue
    .line 2497291
    new-instance v2, LX/HiN;

    .line 2497292
    new-instance v3, LX/HiO;

    invoke-static {p0}, LX/2N8;->a(LX/0QB;)LX/2N8;

    move-result-object v0

    check-cast v0, LX/2N8;

    invoke-static {p0}, LX/0eD;->b(LX/0QB;)Ljava/util/Locale;

    move-result-object v1

    check-cast v1, Ljava/util/Locale;

    invoke-direct {v3, v0, v1}, LX/HiO;-><init>(LX/2N8;Ljava/util/Locale;)V

    .line 2497293
    move-object v0, v3

    .line 2497294
    check-cast v0, LX/HiO;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v2, v0, v1}, LX/HiN;-><init>(LX/HiO;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 2497295
    return-object v2
.end method


# virtual methods
.method public final a(LX/HiM;)LX/0Px;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/HiM;",
            ")",
            "LX/0Px",
            "<",
            "Landroid/location/Address;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2497296
    iget-object v0, p0, LX/HiN;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p1}, LX/HiN;->b(LX/HiM;)LX/0Tn;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2497297
    iget-object v1, p0, LX/HiN;->a:LX/HiO;

    .line 2497298
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 2497299
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2497300
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 2497301
    :goto_0
    move-object v0, v3

    .line 2497302
    return-object v0

    .line 2497303
    :cond_0
    const/4 v3, 0x0

    .line 2497304
    :try_start_0
    iget-object v5, v1, LX/HiO;->a:LX/2N8;

    invoke-virtual {v5, v0}, LX/2N8;->a(Ljava/lang/String;)LX/0lF;
    :try_end_0
    .catch LX/462; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 2497305
    :goto_1
    if-eqz v3, :cond_1

    invoke-virtual {v3}, LX/0lF;->h()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2497306
    invoke-virtual {v3}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0lF;

    .line 2497307
    new-instance v6, Landroid/location/Address;

    iget-object v7, v1, LX/HiO;->b:Ljava/util/Locale;

    invoke-direct {v6, v7}, Landroid/location/Address;-><init>(Ljava/util/Locale;)V

    .line 2497308
    const/4 v7, 0x0

    const-string v8, "title"

    invoke-virtual {v3, v8}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v8

    invoke-static {v8}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/location/Address;->setAddressLine(ILjava/lang/String;)V

    .line 2497309
    const-string v7, "subtitle"

    invoke-virtual {v3, v7}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v7

    invoke-static {v7}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/location/Address;->setLocality(Ljava/lang/String;)V

    .line 2497310
    const-string v7, "latitude"

    invoke-virtual {v3, v7}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v7

    invoke-static {v7}, LX/16N;->e(LX/0lF;)D

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Landroid/location/Address;->setLatitude(D)V

    .line 2497311
    const-string v7, "longitude"

    invoke-virtual {v3, v7}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v7

    invoke-static {v7}, LX/16N;->e(LX/0lF;)D

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Landroid/location/Address;->setLongitude(D)V

    .line 2497312
    move-object v3, v6

    .line 2497313
    invoke-virtual {v4, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2

    .line 2497314
    :cond_1
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    goto :goto_0

    :catch_0
    goto :goto_1
.end method

.method public final a(Landroid/location/Address;LX/HiM;)V
    .locals 4

    .prologue
    .line 2497315
    iget-object v0, p0, LX/HiN;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    .line 2497316
    iget-object v1, p0, LX/HiN;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p2}, LX/HiN;->b(LX/HiM;)LX/0Tn;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2497317
    iget-object v2, p0, LX/HiN;->a:LX/HiO;

    const/4 v3, 0x3

    invoke-virtual {v2, v1, p1, v3}, LX/HiO;->a(Ljava/lang/String;Landroid/location/Address;I)Ljava/lang/String;

    move-result-object v1

    .line 2497318
    invoke-static {p2}, LX/HiN;->b(LX/HiM;)LX/0Tn;

    move-result-object v2

    invoke-interface {v0, v2, v1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    .line 2497319
    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2497320
    return-void
.end method
