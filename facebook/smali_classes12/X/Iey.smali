.class public LX/Iey;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/0tX;

.field public final c:LX/Iew;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2599231
    const-class v0, LX/Iey;

    sput-object v0, LX/Iey;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0tX;LX/Iew;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2599232
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2599233
    iput-object p1, p0, LX/Iey;->b:LX/0tX;

    .line 2599234
    iput-object p2, p0, LX/Iey;->c:LX/Iew;

    .line 2599235
    return-void
.end method

.method public static b(Ljava/lang/String;)LX/0zO;
    .locals 4
    .param p0    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CYMKSuggestionSurface;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0zO",
            "<",
            "Lcom/facebook/messaging/contactsyoumayknow/graphql/ContactsYouMayKnowQueryModels$ContactsYouMayKnowQueryModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2599236
    new-instance v0, LX/Ddq;

    invoke-direct {v0}, LX/Ddq;-><init>()V

    move-object v0, v0

    .line 2599237
    const-string v1, "max_count"

    const/16 v2, 0x32

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "square_profile_pic_size_big"

    invoke-static {}, LX/0wB;->c()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "surface"

    invoke-virtual {v0, v1, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    move-object v0, v0

    .line 2599238
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->a:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    const-wide/32 v2, 0x15180

    invoke-virtual {v0, v2, v3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v0

    const/4 v1, 0x1

    .line 2599239
    iput-boolean v1, v0, LX/0zO;->p:Z

    .line 2599240
    move-object v0, v0

    .line 2599241
    return-object v0
.end method
