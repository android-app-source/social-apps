.class public LX/JO9;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:LX/JNp;

.field public final b:LX/JOC;

.field public final c:LX/JNe;

.field public final d:LX/1DR;


# direct methods
.method public constructor <init>(LX/JNp;LX/JOC;LX/JNe;LX/1DR;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2686998
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2686999
    iput-object p1, p0, LX/JO9;->a:LX/JNp;

    .line 2687000
    iput-object p2, p0, LX/JO9;->b:LX/JOC;

    .line 2687001
    iput-object p3, p0, LX/JO9;->c:LX/JNe;

    .line 2687002
    iput-object p4, p0, LX/JO9;->d:LX/1DR;

    .line 2687003
    return-void
.end method

.method public static a(LX/0QB;)LX/JO9;
    .locals 7

    .prologue
    .line 2687004
    const-class v1, LX/JO9;

    monitor-enter v1

    .line 2687005
    :try_start_0
    sget-object v0, LX/JO9;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2687006
    sput-object v2, LX/JO9;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2687007
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2687008
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2687009
    new-instance p0, LX/JO9;

    invoke-static {v0}, LX/JNp;->a(LX/0QB;)LX/JNp;

    move-result-object v3

    check-cast v3, LX/JNp;

    invoke-static {v0}, LX/JOC;->a(LX/0QB;)LX/JOC;

    move-result-object v4

    check-cast v4, LX/JOC;

    invoke-static {v0}, LX/JNe;->a(LX/0QB;)LX/JNe;

    move-result-object v5

    check-cast v5, LX/JNe;

    invoke-static {v0}, LX/1DR;->a(LX/0QB;)LX/1DR;

    move-result-object v6

    check-cast v6, LX/1DR;

    invoke-direct {p0, v3, v4, v5, v6}, LX/JO9;-><init>(LX/JNp;LX/JOC;LX/JNe;LX/1DR;)V

    .line 2687010
    move-object v0, p0

    .line 2687011
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2687012
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JO9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2687013
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2687014
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
