.class public LX/JIz;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/JIz;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/entitycardsplugins/discoverycuration/presenters/DiscoveryMediaGridBinder;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/JJ0;",
            ">;"
        }
    .end annotation
.end field

.field public volatile c:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/G1G;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2678775
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2678776
    return-void
.end method

.method public static a(LX/0QB;)LX/JIz;
    .locals 6

    .prologue
    .line 2678777
    sget-object v0, LX/JIz;->d:LX/JIz;

    if-nez v0, :cond_1

    .line 2678778
    const-class v1, LX/JIz;

    monitor-enter v1

    .line 2678779
    :try_start_0
    sget-object v0, LX/JIz;->d:LX/JIz;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2678780
    if-eqz v2, :cond_0

    .line 2678781
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2678782
    new-instance v3, LX/JIz;

    invoke-direct {v3}, LX/JIz;-><init>()V

    .line 2678783
    const/16 v4, 0x1ad3

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x1ad5

    invoke-static {v0, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 p0, 0x36b9

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    .line 2678784
    iput-object v4, v3, LX/JIz;->a:LX/0Or;

    iput-object v5, v3, LX/JIz;->b:LX/0Or;

    iput-object p0, v3, LX/JIz;->c:LX/0Or;

    .line 2678785
    move-object v0, v3

    .line 2678786
    sput-object v0, LX/JIz;->d:LX/JIz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2678787
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2678788
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2678789
    :cond_1
    sget-object v0, LX/JIz;->d:LX/JIz;

    return-object v0

    .line 2678790
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2678791
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
