.class public LX/J8u;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/J8u;


# instance fields
.field public final a:LX/17W;


# direct methods
.method public constructor <init>(LX/17W;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2652232
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2652233
    iput-object p1, p0, LX/J8u;->a:LX/17W;

    .line 2652234
    return-void
.end method

.method public static a(LX/0QB;)LX/J8u;
    .locals 4

    .prologue
    .line 2652235
    sget-object v0, LX/J8u;->b:LX/J8u;

    if-nez v0, :cond_1

    .line 2652236
    const-class v1, LX/J8u;

    monitor-enter v1

    .line 2652237
    :try_start_0
    sget-object v0, LX/J8u;->b:LX/J8u;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2652238
    if-eqz v2, :cond_0

    .line 2652239
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2652240
    new-instance p0, LX/J8u;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v3

    check-cast v3, LX/17W;

    invoke-direct {p0, v3}, LX/J8u;-><init>(LX/17W;)V

    .line 2652241
    move-object v0, p0

    .line 2652242
    sput-object v0, LX/J8u;->b:LX/J8u;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2652243
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2652244
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2652245
    :cond_1
    sget-object v0, LX/J8u;->b:LX/J8u;

    return-object v0

    .line 2652246
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2652247
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;LX/9lP;LX/1B1;)V
    .locals 1

    .prologue
    .line 2652248
    new-instance v0, LX/J8t;

    invoke-direct {v0, p0, p2, p1}, LX/J8t;-><init>(LX/J8u;LX/9lP;Landroid/content/Context;)V

    invoke-virtual {p3, v0}, LX/1B1;->a(LX/0b2;)Z

    .line 2652249
    return-void
.end method
