.class public final LX/HFK;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/pages/common/pagecreation/graphql/PageAddressSearchQueryModels$PageAddressSearchQueryModel$StreetResultsModel$NodesModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;)V
    .locals 0

    .prologue
    .line 2443447
    iput-object p1, p0, LX/HFK;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2443448
    iget-object v0, p0, LX/HFK;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f08003a

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2443449
    iget-object v0, p0, LX/HFK;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->a:LX/03V;

    sget-object v1, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->l:Ljava/lang/String;

    const-string v2, "address search failed"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2443450
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2443451
    check-cast p1, LX/0Px;

    .line 2443452
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2443453
    iget-object v0, p0, LX/HFK;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->e:LX/HEr;

    .line 2443454
    iput-object p1, v0, LX/HEr;->a:LX/0Px;

    .line 2443455
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2443456
    iget-object v0, p0, LX/HFK;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->t:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 2443457
    iget-object v0, p0, LX/HFK;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->x:Landroid/widget/ScrollView;

    new-instance v1, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment$10$1;

    invoke-direct {v1, p0}, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment$10$1;-><init>(LX/HFK;)V

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->post(Ljava/lang/Runnable;)Z

    .line 2443458
    :cond_0
    return-void
.end method
