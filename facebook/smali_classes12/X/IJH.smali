.class public LX/IJH;
.super LX/3Tf;
.source ""

# interfaces
.implements LX/2ht;


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:LX/0ad;

.field public e:LX/IFX;

.field private f:LX/IJ4;

.field private g:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

.field private h:I

.field private i:Z


# direct methods
.method public constructor <init>(LX/IJ4;Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;Landroid/content/Context;LX/0ad;)V
    .locals 1
    .param p1    # LX/IJ4;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2563332
    invoke-direct {p0}, LX/3Tf;-><init>()V

    .line 2563333
    const/4 v0, -0x1

    iput v0, p0, LX/IJH;->h:I

    .line 2563334
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/IJH;->i:Z

    .line 2563335
    iput-object p1, p0, LX/IJH;->f:LX/IJ4;

    .line 2563336
    iput-object p2, p0, LX/IJH;->g:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    .line 2563337
    iput-object p3, p0, LX/IJH;->c:Landroid/content/Context;

    .line 2563338
    iput-object p4, p0, LX/IJH;->d:LX/0ad;

    .line 2563339
    return-void
.end method

.method private static a(Landroid/view/View;)I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2563340
    new-instance v0, Landroid/widget/AbsListView$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2563341
    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Landroid/view/View;->measure(II)V

    .line 2563342
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method private a(LX/IJG;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2563327
    sget-object v0, LX/IJE;->a:[I

    invoke-virtual {p1}, LX/IJG;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2563328
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid header view type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2563329
    :pswitch_0
    new-instance v0, LX/IJi;

    iget-object v1, p0, LX/IJH;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/IJi;-><init>(Landroid/content/Context;)V

    .line 2563330
    :goto_0
    return-object v0

    :pswitch_1
    new-instance v0, LX/IJF;

    iget-object v1, p0, LX/IJH;->c:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, LX/IJF;-><init>(LX/IJH;Landroid/content/Context;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(LX/IFd;)Z
    .locals 1

    .prologue
    .line 2563343
    instance-of v0, p0, LX/IFe;

    if-eqz v0, :cond_1

    .line 2563344
    iget-object v0, p0, LX/IFd;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2563345
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 2563346
    :goto_0
    return v0

    .line 2563347
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2563348
    :cond_1
    invoke-virtual {p0}, LX/622;->c()Z

    move-result v0

    goto :goto_0
.end method

.method private b(LX/IJG;)Landroid/view/View;
    .locals 5

    .prologue
    .line 2563349
    sget-object v0, LX/IJE;->a:[I

    invoke-virtual {p1}, LX/IJG;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2563350
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid child view type: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, LX/IJG;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2563351
    :pswitch_0
    new-instance v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;

    iget-object v1, p0, LX/IJH;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;-><init>(Landroid/content/Context;)V

    .line 2563352
    :goto_0
    return-object v0

    :pswitch_1
    new-instance v0, LX/IJd;

    iget-object v1, p0, LX/IJH;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/IJd;-><init>(Landroid/content/Context;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private d(II)LX/IFR;
    .locals 1

    .prologue
    .line 2563353
    iget-object v0, p0, LX/IJH;->e:LX/IFX;

    invoke-virtual {v0}, LX/IFX;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IFd;

    invoke-virtual {v0}, LX/622;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IFR;

    return-object v0
.end method

.method private g(I)LX/IFd;
    .locals 1

    .prologue
    .line 2563301
    iget-object v0, p0, LX/IJH;->e:LX/IFX;

    invoke-virtual {v0}, LX/IFX;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IFd;

    return-object v0
.end method

.method private h(I)I
    .locals 1

    .prologue
    .line 2563354
    iget-boolean v0, p0, LX/IJH;->i:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sub-int v0, p1, v0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)I
    .locals 1

    .prologue
    .line 2563355
    iget-object v0, p0, LX/IJH;->e:LX/IFX;

    invoke-virtual {v0}, LX/IFX;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2563356
    sget-object v0, LX/IJG;->FAKE_HEADER:LX/IJG;

    invoke-virtual {v0}, LX/IJG;->ordinal()I

    move-result v0

    .line 2563357
    :goto_0
    return v0

    :cond_0
    sget-object v0, LX/IJG;->HEADER:LX/IJG;

    invoke-virtual {v0}, LX/IJG;->ordinal()I

    move-result v0

    goto :goto_0
.end method

.method public final a(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p4    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2563358
    invoke-virtual {p0, p1, p2}, LX/IJH;->c(II)I

    move-result v0

    .line 2563359
    invoke-static {}, LX/IJG;->values()[LX/IJG;

    move-result-object v1

    aget-object v0, v1, v0

    .line 2563360
    if-nez p4, :cond_3

    .line 2563361
    invoke-direct {p0, v0}, LX/IJH;->b(LX/IJG;)Landroid/view/View;

    move-result-object v1

    .line 2563362
    :goto_0
    sget-object v2, LX/IJG;->CHILD:LX/IJG;

    if-ne v0, v2, :cond_1

    .line 2563363
    invoke-direct {p0, p1, p2}, LX/IJH;->d(II)LX/IFR;

    move-result-object v2

    move-object v0, v1

    .line 2563364
    check-cast v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;

    iget-object v3, p0, LX/IJH;->f:LX/IJ4;

    invoke-virtual {v0, v2, v3}, Lcom/facebook/friendsnearby/ui/FriendsNearbyRowView;->a(LX/IFR;LX/IJ4;)V

    .line 2563365
    :cond_0
    :goto_1
    return-object v1

    .line 2563366
    :cond_1
    sget-object v2, LX/IJG;->CHILD_MORE:LX/IJG;

    if-ne v0, v2, :cond_0

    .line 2563367
    iget-object v0, p0, LX/IJH;->e:LX/IFX;

    invoke-virtual {v0}, LX/IFX;->j()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2563368
    invoke-direct {p0, p1}, LX/IJH;->g(I)LX/IFd;

    move-result-object v2

    move-object v0, v1

    .line 2563369
    check-cast v0, LX/IJd;

    iget-object v3, p0, LX/IJH;->g:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    .line 2563370
    iput-object v2, v0, LX/IJd;->c:LX/IFd;

    .line 2563371
    iput-object v3, v0, LX/IJd;->d:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    .line 2563372
    goto :goto_1

    .line 2563373
    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    :cond_3
    move-object v1, p4

    goto :goto_0
.end method

.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p2    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2563374
    invoke-virtual {p0, p1}, LX/IJH;->a(I)I

    move-result v0

    .line 2563375
    invoke-static {}, LX/IJG;->values()[LX/IJG;

    move-result-object v1

    aget-object v0, v1, v0

    .line 2563376
    if-nez p2, :cond_2

    .line 2563377
    invoke-direct {p0, v0}, LX/IJH;->a(LX/IJG;)Landroid/view/View;

    move-result-object v1

    .line 2563378
    :goto_0
    sget-object v2, LX/IJG;->HEADER:LX/IJG;

    if-ne v0, v2, :cond_1

    .line 2563379
    iget v0, p0, LX/IJH;->h:I

    if-gez v0, :cond_0

    .line 2563380
    invoke-static {v1}, LX/IJH;->a(Landroid/view/View;)I

    move-result v0

    iput v0, p0, LX/IJH;->h:I

    .line 2563381
    :cond_0
    invoke-direct {p0, p1}, LX/IJH;->g(I)LX/IFd;

    move-result-object v2

    move-object v0, v1

    .line 2563382
    check-cast v0, LX/IJi;

    invoke-virtual {v2}, LX/622;->a()Ljava/lang/String;

    move-result-object v2

    const/4 p3, 0x0

    const/4 p2, 0x1

    const/4 p1, 0x0

    .line 2563383
    const/16 v3, 0xb7

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    .line 2563384
    if-gez v3, :cond_3

    .line 2563385
    invoke-virtual {v0, v2}, LX/IJi;->setTitleText(Ljava/lang/String;)V

    .line 2563386
    :cond_1
    :goto_1
    return-object v1

    :cond_2
    move-object v1, p2

    goto :goto_0

    .line 2563387
    :cond_3
    iget-object v4, v0, LX/IJi;->b:LX/0wJ;

    add-int/lit8 v5, v3, 0x1

    invoke-virtual {v2, p1, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, p3}, LX/0wJ;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 2563388
    new-instance v5, Landroid/text/SpannableString;

    invoke-direct {v5, v4}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2563389
    new-instance v4, Landroid/text/style/StyleSpan;

    invoke-direct {v4, p2}, Landroid/text/style/StyleSpan;-><init>(I)V

    add-int/lit8 v6, v3, 0x1

    const/16 p0, 0x21

    invoke-virtual {v5, v4, p1, v6, p0}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 2563390
    iget-object v4, v0, LX/IJi;->a:Landroid/widget/TextView;

    invoke-virtual {v4, p3, p1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 2563391
    iget-object v4, v0, LX/IJi;->a:Landroid/widget/TextView;

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/CharSequence;

    aput-object v5, v6, p1

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, p2

    invoke-static {v6}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public final synthetic a(II)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2563392
    invoke-direct {p0, p1, p2}, LX/IJH;->d(II)LX/IFR;

    move-result-object v0

    return-object v0
.end method

.method public final areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 2563331
    const/4 v0, 0x0

    return v0
.end method

.method public final b(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p2    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 2563316
    iget-object v0, p0, LX/IJH;->e:LX/IFX;

    invoke-virtual {v0}, LX/IFX;->j()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/IJH;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2563317
    if-eqz v0, :cond_1

    .line 2563318
    const/4 v0, 0x0

    .line 2563319
    :goto_1
    return-object v0

    .line 2563320
    :cond_1
    iget-boolean v0, p0, LX/IJH;->i:Z

    if-eqz v0, :cond_2

    if-nez p1, :cond_2

    .line 2563321
    invoke-virtual {p0, v1, p2, p3}, LX/IJH;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/IJi;

    .line 2563322
    iget-object v1, p0, LX/IJH;->e:LX/IFX;

    .line 2563323
    iget-object p0, v1, LX/IFX;->f:Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;

    iget-object p1, v1, LX/IFX;->l:LX/IG2;

    invoke-virtual {p0, p1}, Lcom/facebook/friendsnearby/model/pings/FriendsNearbyPingsUtil;->c(LX/IG2;)Ljava/lang/String;

    move-result-object p0

    move-object v1, p0

    .line 2563324
    invoke-virtual {v0, v1}, LX/IJi;->setTitleText(Ljava/lang/String;)V

    goto :goto_1

    .line 2563325
    :cond_2
    invoke-direct {p0, p1}, LX/IJH;->h(I)I

    move-result v0

    invoke-virtual {p0, v0}, LX/3Tf;->d(I)[I

    move-result-object v0

    aget v0, v0, v1

    .line 2563326
    invoke-virtual {p0, v0, p2, p3}, LX/IJH;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic b(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2563315
    invoke-direct {p0, p1}, LX/IJH;->g(I)LX/IFd;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2563308
    iget-object v0, p0, LX/IJH;->e:LX/IFX;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/IJH;->e:LX/IFX;

    invoke-virtual {v0}, LX/IFX;->b()LX/0Px;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v2

    .line 2563309
    :goto_0
    return v0

    .line 2563310
    :cond_1
    iget-object v0, p0, LX/IJH;->e:LX/IFX;

    invoke-virtual {v0}, LX/IFX;->b()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v3, v1

    :goto_1
    if-ge v3, v5, :cond_3

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IFd;

    .line 2563311
    invoke-virtual {v0}, LX/622;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 2563312
    goto :goto_0

    .line 2563313
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_3
    move v0, v2

    .line 2563314
    goto :goto_0
.end method

.method public final b(II)Z
    .locals 1

    .prologue
    .line 2563307
    const/4 v0, 0x1

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 2563306
    iget-object v0, p0, LX/IJH;->e:LX/IFX;

    invoke-virtual {v0}, LX/IFX;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final c(I)I
    .locals 2

    .prologue
    .line 2563302
    invoke-direct {p0, p1}, LX/IJH;->g(I)LX/IFd;

    move-result-object v0

    .line 2563303
    invoke-static {v0}, LX/IJH;->a(LX/IFd;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2563304
    invoke-virtual {v0}, LX/622;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 2563305
    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, LX/622;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public final c(II)I
    .locals 2

    .prologue
    .line 2563297
    invoke-direct {p0, p1}, LX/IJH;->g(I)LX/IFd;

    move-result-object v0

    .line 2563298
    invoke-static {v0}, LX/IJH;->a(LX/IFd;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, LX/622;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne p2, v0, :cond_0

    .line 2563299
    sget-object v0, LX/IJG;->CHILD_MORE:LX/IJG;

    invoke-virtual {v0}, LX/IJG;->ordinal()I

    move-result v0

    .line 2563300
    :goto_0
    return v0

    :cond_0
    sget-object v0, LX/IJG;->CHILD:LX/IJG;

    invoke-virtual {v0}, LX/IJG;->ordinal()I

    move-result v0

    goto :goto_0
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 2563296
    iget-object v0, p0, LX/IJH;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0121

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    return v0
.end method

.method public final e(I)I
    .locals 1

    .prologue
    .line 2563292
    iget v0, p0, LX/IJH;->h:I

    if-gez v0, :cond_0

    .line 2563293
    sget-object v0, LX/IJG;->HEADER:LX/IJG;

    invoke-direct {p0, v0}, LX/IJH;->a(LX/IJG;)Landroid/view/View;

    move-result-object v0

    .line 2563294
    invoke-static {v0}, LX/IJH;->a(Landroid/view/View;)I

    move-result v0

    iput v0, p0, LX/IJH;->h:I

    .line 2563295
    :cond_0
    iget v0, p0, LX/IJH;->h:I

    return v0
.end method

.method public final f(I)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2563286
    invoke-direct {p0, p1}, LX/IJH;->h(I)I

    move-result v2

    .line 2563287
    if-ltz v2, :cond_0

    invoke-virtual {p0}, LX/3Tf;->getCount()I

    move-result v3

    if-lt v2, v3, :cond_2

    :cond_0
    move v0, v1

    .line 2563288
    :cond_1
    :goto_0
    return v0

    .line 2563289
    :cond_2
    invoke-virtual {p0, v2}, LX/3Tf;->d(I)[I

    move-result-object v2

    .line 2563290
    aget v2, v2, v0

    .line 2563291
    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    move v0, v1

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2563285
    invoke-static {}, LX/IJG;->values()[LX/IJG;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public final o_(I)I
    .locals 1

    .prologue
    .line 2563284
    sget-object v0, LX/IJG;->HEADER:LX/IJG;

    invoke-virtual {v0}, LX/IJG;->ordinal()I

    move-result v0

    return v0
.end method
