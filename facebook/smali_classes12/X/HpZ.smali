.class public LX/HpZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1rs;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TResponseModel:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/1rs",
        "<",
        "Lcom/facebook/graphql/modelutil/BaseModel;",
        "Ljava/lang/Void;",
        "TTResponseModel;>;"
    }
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;

.field private static final d:[Ljava/lang/String;

.field private static final e:[Ljava/lang/String;

.field private static final f:[Ljava/lang/String;


# instance fields
.field private g:[Ljava/lang/String;

.field private h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private i:LX/Hpf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/componentscript/sectioncomponent/RuntimeQueryFactory",
            "<TTResponseModel;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2509681
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "page_info"

    aput-object v1, v0, v2

    sput-object v0, LX/HpZ;->a:[Ljava/lang/String;

    .line 2509682
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "edges"

    aput-object v1, v0, v2

    sput-object v0, LX/HpZ;->b:[Ljava/lang/String;

    .line 2509683
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "start_cursor"

    aput-object v1, v0, v2

    sput-object v0, LX/HpZ;->c:[Ljava/lang/String;

    .line 2509684
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "end_cursor"

    aput-object v1, v0, v2

    sput-object v0, LX/HpZ;->d:[Ljava/lang/String;

    .line 2509685
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "has_next_page"

    aput-object v1, v0, v2

    sput-object v0, LX/HpZ;->e:[Ljava/lang/String;

    .line 2509686
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "has_previous_page"

    aput-object v1, v0, v2

    sput-object v0, LX/HpZ;->f:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>([Ljava/lang/String;Ljava/util/Map;LX/Hpf;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/facebook/componentscript/sectioncomponent/RuntimeQueryFactory",
            "<TTResponseModel;>;)V"
        }
    .end annotation

    .prologue
    .line 2509687
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2509688
    iput-object p1, p0, LX/HpZ;->g:[Ljava/lang/String;

    .line 2509689
    iput-object p2, p0, LX/HpZ;->h:Ljava/util/Map;

    .line 2509690
    iput-object p3, p0, LX/HpZ;->i:LX/Hpf;

    .line 2509691
    return-void
.end method


# virtual methods
.method public final a(LX/3DR;Ljava/lang/Object;)LX/0gW;
    .locals 4

    .prologue
    .line 2509692
    iget-object v0, p0, LX/HpZ;->i:LX/Hpf;

    .line 2509693
    iget-object v1, v0, LX/Hpf;->a:LX/0gW;

    invoke-virtual {v1}, LX/0gW;->p()LX/0gW;

    move-result-object v1

    move-object v2, v1

    .line 2509694
    iget-object v0, p0, LX/HpZ;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2509695
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    goto :goto_0

    .line 2509696
    :cond_0
    const-string v0, "after_cursor"

    .line 2509697
    iget-object v1, p1, LX/3DR;->d:Ljava/lang/String;

    move-object v1, v1

    .line 2509698
    invoke-virtual {v2, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "before_cursor"

    .line 2509699
    iget-object v2, p1, LX/3DR;->c:Ljava/lang/String;

    move-object v2, v2

    .line 2509700
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "limit"

    .line 2509701
    iget v2, p1, LX/3DR;->e:I

    move v2, v2

    .line 2509702
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/5Mb;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TTResponseModel;>;)",
            "LX/5Mb",
            "<",
            "Lcom/facebook/graphql/modelutil/BaseModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2509703
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2509704
    invoke-static {v1}, LX/Hpf;->c(Ljava/lang/Object;)LX/Hpd;

    move-result-object v2

    move-object v1, v2

    .line 2509705
    iget-object v0, p0, LX/HpZ;->g:[Ljava/lang/String;

    const/4 v2, 0x1

    iget-object v3, p0, LX/HpZ;->g:[Ljava/lang/String;

    array-length v3, v3

    invoke-static {v0, v2, v3}, Ljava/util/Arrays;->copyOfRange([Ljava/lang/Object;II)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    const-class v2, LX/Hpd;

    invoke-interface {v1, v0, v2}, LX/Hpd;->a([Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, LX/Hpd;

    .line 2509706
    sget-object v0, LX/HpZ;->a:[Ljava/lang/String;

    const-class v2, LX/Hpd;

    invoke-interface {v1, v0, v2}, LX/Hpd;->a([Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, LX/Hpd;

    .line 2509707
    new-instance v0, LX/5Mb;

    sget-object v2, LX/HpZ;->b:[Ljava/lang/String;

    const-class v3, Ljava/util/List;

    invoke-interface {v1, v2, v3}, LX/Hpd;->a([Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    sget-object v2, LX/HpZ;->c:[Ljava/lang/String;

    const-class v3, Ljava/lang/String;

    invoke-interface {v5, v2, v3}, LX/Hpd;->a([Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    sget-object v3, LX/HpZ;->d:[Ljava/lang/String;

    const-class v4, Ljava/lang/String;

    invoke-interface {v5, v3, v4}, LX/Hpd;->a([Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    sget-object v4, LX/HpZ;->f:[Ljava/lang/String;

    const-class v6, Ljava/lang/Boolean;

    invoke-interface {v5, v4, v6}, LX/Hpd;->a([Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    sget-object v6, LX/HpZ;->e:[Ljava/lang/String;

    const-class v7, Ljava/lang/Boolean;

    invoke-interface {v5, v6, v7}, LX/Hpd;->a([Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    invoke-direct/range {v0 .. v5}, LX/5Mb;-><init>(LX/0Px;Ljava/lang/String;Ljava/lang/String;ZZ)V

    return-object v0
.end method
