.class public LX/J0Z;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Lcom/facebook/payments/p2p/service/model/transactions/FetchMoreTransactionsParams;",
        "Lcom/facebook/payments/p2p/service/model/transactions/FetchMoreTransactionsResult;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/J0Z;


# instance fields
.field private final b:LX/J0h;


# direct methods
.method public constructor <init>(LX/J0h;LX/0sO;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2637280
    invoke-direct {p0, p2}, LX/0ro;-><init>(LX/0sO;)V

    .line 2637281
    iput-object p1, p0, LX/J0Z;->b:LX/J0h;

    .line 2637282
    return-void
.end method

.method public static a(LX/0QB;)LX/J0Z;
    .locals 5

    .prologue
    .line 2637283
    sget-object v0, LX/J0Z;->c:LX/J0Z;

    if-nez v0, :cond_1

    .line 2637284
    const-class v1, LX/J0Z;

    monitor-enter v1

    .line 2637285
    :try_start_0
    sget-object v0, LX/J0Z;->c:LX/J0Z;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2637286
    if-eqz v2, :cond_0

    .line 2637287
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2637288
    new-instance p0, LX/J0Z;

    invoke-static {v0}, LX/J0h;->b(LX/0QB;)LX/J0h;

    move-result-object v3

    check-cast v3, LX/J0h;

    invoke-static {v0}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v4

    check-cast v4, LX/0sO;

    invoke-direct {p0, v3, v4}, LX/J0Z;-><init>(LX/J0h;LX/0sO;)V

    .line 2637289
    move-object v0, p0

    .line 2637290
    sput-object v0, LX/J0Z;->c:LX/J0Z;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2637291
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2637292
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2637293
    :cond_1
    sget-object v0, LX/J0Z;->c:LX/J0Z;

    return-object v0

    .line 2637294
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2637295
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 2637296
    check-cast p1, Lcom/facebook/payments/p2p/service/model/transactions/FetchMoreTransactionsParams;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2637297
    const/4 v3, 0x0

    .line 2637298
    sget-object v0, LX/J0Y;->a:[I

    .line 2637299
    iget-object v4, p1, Lcom/facebook/payments/p2p/service/model/transactions/FetchMoreTransactionsParams;->b:LX/DtK;

    move-object v4, v4

    .line 2637300
    invoke-virtual {v4}, LX/DtK;->ordinal()I

    move-result v4

    aget v0, v0, v4

    packed-switch v0, :pswitch_data_0

    .line 2637301
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown queryType seen "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2637302
    iget-object v2, p1, Lcom/facebook/payments/p2p/service/model/transactions/FetchMoreTransactionsParams;->b:LX/DtK;

    move-object v2, v2

    .line 2637303
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2637304
    :pswitch_0
    const-class v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchAllMoreTransactionsQueryModel;

    invoke-virtual {p3, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchAllMoreTransactionsQueryModel;

    .line 2637305
    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchAllMoreTransactionsQueryModel;->a()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchAllMoreTransactionsQueryModel$AllMessengerPaymentsModel;

    move-result-object v4

    .line 2637306
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchAllMoreTransactionsQueryModel$AllMessengerPaymentsModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_9

    .line 2637307
    invoke-virtual {v4}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchAllMoreTransactionsQueryModel$AllMessengerPaymentsModel;->a()LX/0Px;

    move-result-object v3

    .line 2637308
    invoke-virtual {v4}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchAllMoreTransactionsQueryModel$AllMessengerPaymentsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v4, v0, v2}, LX/15i;->h(II)Z

    move-result v0

    move v4, v0

    move-object v5, v3

    .line 2637309
    :goto_1
    if-nez v5, :cond_6

    .line 2637310
    new-instance v0, Lcom/facebook/payments/p2p/service/model/transactions/FetchMoreTransactionsResult;

    .line 2637311
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2637312
    invoke-direct {v0, v2, v1}, Lcom/facebook/payments/p2p/service/model/transactions/FetchMoreTransactionsResult;-><init>(LX/0Px;Z)V

    .line 2637313
    :goto_2
    return-object v0

    :cond_0
    move v0, v2

    .line 2637314
    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_0

    .line 2637315
    :pswitch_1
    const-class v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingMoreTransactionsQueryModel;

    invoke-virtual {p3, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingMoreTransactionsQueryModel;

    .line 2637316
    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingMoreTransactionsQueryModel;->a()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingMoreTransactionsQueryModel$IncomingMessengerPaymentsModel;

    move-result-object v4

    .line 2637317
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingMoreTransactionsQueryModel$IncomingMessengerPaymentsModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    move v0, v1

    :goto_3
    if-eqz v0, :cond_9

    .line 2637318
    invoke-virtual {v4}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingMoreTransactionsQueryModel$IncomingMessengerPaymentsModel;->a()LX/0Px;

    move-result-object v3

    .line 2637319
    invoke-virtual {v4}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingMoreTransactionsQueryModel$IncomingMessengerPaymentsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v4, v0, v2}, LX/15i;->h(II)Z

    move-result v0

    move v4, v0

    move-object v5, v3

    .line 2637320
    goto :goto_1

    :cond_2
    move v0, v2

    .line 2637321
    goto :goto_3

    :cond_3
    move v0, v2

    goto :goto_3

    .line 2637322
    :pswitch_2
    const-class v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchOutgoingMoreTransactionsQueryModel;

    invoke-virtual {p3, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchOutgoingMoreTransactionsQueryModel;

    .line 2637323
    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchOutgoingMoreTransactionsQueryModel;->a()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchOutgoingMoreTransactionsQueryModel$OutgoingMessengerPaymentsModel;

    move-result-object v4

    .line 2637324
    if-eqz v4, :cond_5

    invoke-virtual {v4}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchOutgoingMoreTransactionsQueryModel$OutgoingMessengerPaymentsModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    if-eqz v0, :cond_9

    .line 2637325
    invoke-virtual {v4}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchOutgoingMoreTransactionsQueryModel$OutgoingMessengerPaymentsModel;->a()LX/0Px;

    move-result-object v3

    .line 2637326
    invoke-virtual {v4}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchOutgoingMoreTransactionsQueryModel$OutgoingMessengerPaymentsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v4, v0, v2}, LX/15i;->h(II)Z

    move-result v0

    move v4, v0

    move-object v5, v3

    .line 2637327
    goto :goto_1

    :cond_4
    move v0, v2

    .line 2637328
    goto :goto_4

    :cond_5
    move v0, v2

    goto :goto_4

    .line 2637329
    :cond_6
    new-instance v6, LX/0Pz;

    invoke-direct {v6}, LX/0Pz;-><init>()V

    .line 2637330
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v7

    move v3, v2

    :goto_5
    if-ge v3, v7, :cond_7

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;

    .line 2637331
    iget-object v8, p0, LX/J0Z;->b:LX/J0h;

    invoke-virtual {v8, v0}, LX/J0h;->a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;)Lcom/facebook/payments/p2p/model/PaymentTransaction;

    move-result-object v0

    invoke-virtual {v6, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2637332
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_5

    .line 2637333
    :cond_7
    new-instance v0, Lcom/facebook/payments/p2p/service/model/transactions/FetchMoreTransactionsResult;

    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    if-nez v4, :cond_8

    :goto_6
    invoke-direct {v0, v3, v1}, Lcom/facebook/payments/p2p/service/model/transactions/FetchMoreTransactionsResult;-><init>(LX/0Px;Z)V

    goto/16 :goto_2

    :cond_8
    move v1, v2

    goto :goto_6

    :cond_9
    move v4, v2

    move-object v5, v3

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 2637334
    const/4 v0, 0x1

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 6

    .prologue
    .line 2637335
    check-cast p1, Lcom/facebook/payments/p2p/service/model/transactions/FetchMoreTransactionsParams;

    .line 2637336
    sget-object v0, LX/J0Y;->a:[I

    .line 2637337
    iget-object v1, p1, Lcom/facebook/payments/p2p/service/model/transactions/FetchMoreTransactionsParams;->b:LX/DtK;

    move-object v1, v1

    .line 2637338
    invoke-virtual {v1}, LX/DtK;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2637339
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown queryType seen "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2637340
    iget-object v2, p1, Lcom/facebook/payments/p2p/service/model/transactions/FetchMoreTransactionsParams;->b:LX/DtK;

    move-object v2, v2

    .line 2637341
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2637342
    :pswitch_0
    new-instance v0, LX/DtT;

    invoke-direct {v0}, LX/DtT;-><init>()V

    move-object v0, v0

    .line 2637343
    :goto_0
    const-string v1, "max_transactions"

    .line 2637344
    const/16 v2, 0x32

    move v2, v2

    .line 2637345
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2637346
    const-string v1, "before_time"

    .line 2637347
    iget-wide v4, p1, Lcom/facebook/payments/p2p/service/model/transactions/FetchMoreTransactionsParams;->c:J

    move-wide v2, v4

    .line 2637348
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2637349
    return-object v0

    .line 2637350
    :pswitch_1
    new-instance v0, LX/DtW;

    invoke-direct {v0}, LX/DtW;-><init>()V

    move-object v0, v0

    .line 2637351
    goto :goto_0

    .line 2637352
    :pswitch_2
    new-instance v0, LX/DtZ;

    invoke-direct {v0}, LX/DtZ;-><init>()V

    move-object v0, v0

    .line 2637353
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
