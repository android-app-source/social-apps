.class public final LX/IoF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/IoE;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/payment/value/input/MemoInputView;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/payment/value/input/MemoInputView;)V
    .locals 0

    .prologue
    .line 2611607
    iput-object p1, p0, LX/IoF;->a:Lcom/facebook/messaging/payment/value/input/MemoInputView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2611608
    iget-object v0, p0, LX/IoF;->a:Lcom/facebook/messaging/payment/value/input/MemoInputView;

    iget-object v0, v0, Lcom/facebook/messaging/payment/value/input/MemoInputView;->e:LX/IoG;

    iget-object v1, p0, LX/IoF;->a:Lcom/facebook/messaging/payment/value/input/MemoInputView;

    iget-object v1, v1, Lcom/facebook/messaging/payment/value/input/MemoInputView;->d:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/IoG;->a(Ljava/lang/String;)V

    .line 2611609
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 2611610
    iget-object v0, p0, LX/IoF;->a:Lcom/facebook/messaging/payment/value/input/MemoInputView;

    iget-object v0, v0, Lcom/facebook/messaging/payment/value/input/MemoInputView;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/J1c;

    iget-object v1, p0, LX/IoF;->a:Lcom/facebook/messaging/payment/value/input/MemoInputView;

    iget-object v1, v1, Lcom/facebook/messaging/payment/value/input/MemoInputView;->d:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, v1}, LX/J1c;->a(Landroid/view/View;)V

    .line 2611611
    iget-object v0, p0, LX/IoF;->a:Lcom/facebook/messaging/payment/value/input/MemoInputView;

    iget-object v0, v0, Lcom/facebook/messaging/payment/value/input/MemoInputView;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v2, v3}, Landroid/os/Vibrator;->vibrate(J)V

    .line 2611612
    return-void
.end method
