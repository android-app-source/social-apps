.class public LX/IGi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/friendsnearby/server/FriendsNearbyDeleteInviteParams;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2556464
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 2556465
    check-cast p1, Lcom/facebook/friendsnearby/server/FriendsNearbyDeleteInviteParams;

    .line 2556466
    const-string v0, "%s/info_requests"

    iget-object v1, p1, Lcom/facebook/friendsnearby/server/FriendsNearbyDeleteInviteParams;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2556467
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 2556468
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "field_types"

    iget-object v4, p1, Lcom/facebook/friendsnearby/server/FriendsNearbyDeleteInviteParams;->b:LX/0Px;

    .line 2556469
    sget-object v5, LX/0mC;->a:LX/0mC;

    invoke-virtual {v5}, LX/0mC;->b()LX/162;

    move-result-object p0

    .line 2556470
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 2556471
    invoke-virtual {p0, v5}, LX/162;->g(Ljava/lang/String;)LX/162;

    goto :goto_0

    .line 2556472
    :cond_0
    invoke-virtual {p0}, LX/162;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v4, v5

    .line 2556473
    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2556474
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v2

    const-string v3, "FRIENDS_NEARBY_DELETE_INVITE"

    .line 2556475
    iput-object v3, v2, LX/14O;->b:Ljava/lang/String;

    .line 2556476
    move-object v2, v2

    .line 2556477
    const-string v3, "DELETE"

    .line 2556478
    iput-object v3, v2, LX/14O;->c:Ljava/lang/String;

    .line 2556479
    move-object v2, v2

    .line 2556480
    iput-object v0, v2, LX/14O;->d:Ljava/lang/String;

    .line 2556481
    move-object v0, v2

    .line 2556482
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 2556483
    move-object v0, v0

    .line 2556484
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2556485
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2556486
    move-object v0, v0

    .line 2556487
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2556488
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2556489
    const/4 v0, 0x0

    return-object v0
.end method
