.class public final LX/HS3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:J

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/0am;

.field public final synthetic d:Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityDefaultLinkView;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityDefaultLinkView;JLjava/lang/String;LX/0am;)V
    .locals 0

    .prologue
    .line 2466456
    iput-object p1, p0, LX/HS3;->d:Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityDefaultLinkView;

    iput-wide p2, p0, LX/HS3;->a:J

    iput-object p4, p0, LX/HS3;->b:Ljava/lang/String;

    iput-object p5, p0, LX/HS3;->c:LX/0am;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x43fdd317

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2466457
    iget-object v0, p0, LX/HS3;->d:Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityDefaultLinkView;

    iget-wide v2, p0, LX/HS3;->a:J

    invoke-virtual {v0, v2, v3}, LX/HS4;->a(J)V

    .line 2466458
    iget-object v0, p0, LX/HS3;->b:Ljava/lang/String;

    iget-wide v2, p0, LX/HS3;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2466459
    iget-object v2, p0, LX/HS3;->d:Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityDefaultLinkView;

    iget-object v2, v2, LX/HS4;->e:LX/17Y;

    iget-object v3, p0, LX/HS3;->d:Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityDefaultLinkView;

    invoke-virtual {v3}, Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityDefaultLinkView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v3, v0}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 2466460
    const-string v0, "uri_unhandled_report_category_name"

    const-string v3, "PageFacewebUriNotHandled"

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2466461
    iget-object v0, p0, LX/HS3;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2466462
    iget-object v0, p0, LX/HS3;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HRS;

    invoke-virtual {v0}, LX/HRS;->a()V

    .line 2466463
    :cond_0
    iget-object v0, p0, LX/HS3;->d:Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityDefaultLinkView;

    iget-object v0, v0, LX/HS4;->c:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/HS3;->d:Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityDefaultLinkView;

    invoke-virtual {v3}, Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityDefaultLinkView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2466464
    const v0, 0x51e7b029

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
