.class public LX/IDk;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/IDk;


# direct methods
.method public constructor <init>()V
    .locals 5
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2551127
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2551128
    const-string v0, "profile/{#%s}/friends/{%s}?source_ref={%s %s}"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.facebook.katana.profile.id"

    const-string v2, "target_tab_name"

    const-string v3, "source_ref"

    const-string v4, "unknown"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v2, LX/0cQ;->FRIEND_LIST_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    .line 2551129
    return-void
.end method

.method public static a(LX/0QB;)LX/IDk;
    .locals 3

    .prologue
    .line 2551130
    sget-object v0, LX/IDk;->a:LX/IDk;

    if-nez v0, :cond_1

    .line 2551131
    const-class v1, LX/IDk;

    monitor-enter v1

    .line 2551132
    :try_start_0
    sget-object v0, LX/IDk;->a:LX/IDk;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2551133
    if-eqz v2, :cond_0

    .line 2551134
    :try_start_1
    new-instance v0, LX/IDk;

    invoke-direct {v0}, LX/IDk;-><init>()V

    .line 2551135
    move-object v0, v0

    .line 2551136
    sput-object v0, LX/IDk;->a:LX/IDk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2551137
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2551138
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2551139
    :cond_1
    sget-object v0, LX/IDk;->a:LX/IDk;

    return-object v0

    .line 2551140
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2551141
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
