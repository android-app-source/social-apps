.class public LX/JSc;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/musicstory/SingleSongComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JSc",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/musicstory/SingleSongComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2695656
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2695657
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JSc;->b:LX/0Zi;

    .line 2695658
    iput-object p1, p0, LX/JSc;->a:LX/0Ot;

    .line 2695659
    return-void
.end method

.method public static a(LX/0QB;)LX/JSc;
    .locals 4

    .prologue
    .line 2695660
    const-class v1, LX/JSc;

    monitor-enter v1

    .line 2695661
    :try_start_0
    sget-object v0, LX/JSc;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2695662
    sput-object v2, LX/JSc;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2695663
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2695664
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2695665
    new-instance v3, LX/JSc;

    const/16 p0, 0x2040

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JSc;-><init>(LX/0Ot;)V

    .line 2695666
    move-object v0, v3

    .line 2695667
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2695668
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JSc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2695669
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2695670
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 13

    .prologue
    .line 2695671
    check-cast p2, LX/JSb;

    .line 2695672
    iget-object v0, p0, LX/JSc;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/musicstory/SingleSongComponentSpec;

    iget-object v2, p2, LX/JSb;->a:LX/JSe;

    iget-object v3, p2, LX/JSb;->b:LX/JTY;

    iget-object v4, p2, LX/JSb;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v5, p2, LX/JSb;->d:LX/1Pn;

    move-object v1, p1

    .line 2695673
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    invoke-static {v0, v1, v5, v2}, Lcom/facebook/feedplugins/musicstory/SingleSongComponentSpec;->a(Lcom/facebook/feedplugins/musicstory/SingleSongComponentSpec;LX/1De;LX/1Pn;LX/JSe;)LX/1Dg;

    move-result-object v7

    invoke-interface {v6, v7}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v12

    move-object v6, v0

    move-object v7, v1

    move-object v8, v2

    move-object v9, v4

    move-object v10, v3

    move-object v11, v5

    invoke-static/range {v6 .. v11}, Lcom/facebook/feedplugins/musicstory/SingleSongComponentSpec;->a(Lcom/facebook/feedplugins/musicstory/SingleSongComponentSpec;LX/1De;LX/JSe;Lcom/facebook/feed/rows/core/props/FeedProps;LX/JTY;LX/1Pn;)LX/1Dg;

    move-result-object v6

    invoke-interface {v12, v6}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6}, LX/1Di;->k()LX/1Dg;

    move-result-object v6

    move-object v0, v6

    .line 2695674
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2695675
    invoke-static {}, LX/1dS;->b()V

    .line 2695676
    iget v0, p1, LX/1dQ;->b:I

    .line 2695677
    packed-switch v0, :pswitch_data_0

    .line 2695678
    :goto_0
    return-object v2

    .line 2695679
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2695680
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2695681
    check-cast v1, LX/JSb;

    .line 2695682
    iget-object v3, p0, LX/JSc;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/musicstory/SingleSongComponentSpec;

    iget-object v4, v1, LX/JSb;->a:LX/JSe;

    iget-object p1, v1, LX/JSb;->b:LX/JTY;

    iget-object p2, v1, LX/JSb;->d:LX/1Pn;

    .line 2695683
    iget-object p0, v3, Lcom/facebook/feedplugins/musicstory/SingleSongComponentSpec;->d:LX/JTD;

    check-cast p2, LX/1Pq;

    new-instance v1, LX/JSf;

    invoke-direct {v1}, LX/JSf;-><init>()V

    invoke-virtual {p0, v4, p1, p2, v1}, LX/JTD;->a(LX/JSe;LX/JTY;LX/1Pq;LX/JSf;)LX/JT0;

    move-result-object p0

    .line 2695684
    if-eqz p0, :cond_0

    .line 2695685
    invoke-interface {p0}, LX/JT0;->b()Landroid/view/View$OnClickListener;

    move-result-object p0

    invoke-interface {p0, v0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 2695686
    :cond_0
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x58d52a2
        :pswitch_0
    .end packed-switch
.end method
