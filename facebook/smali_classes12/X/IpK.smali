.class public final LX/IpK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;)V
    .locals 0

    .prologue
    .line 2612729
    iput-object p1, p0, LX/IpK;->a:Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x66afc540

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2612730
    iget-object v1, p0, LX/IpK;->a:Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;

    iget-object v1, v1, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;->q:LX/Io1;

    .line 2612731
    iget-object v3, v1, LX/Io1;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    const/4 p1, 0x1

    const/4 v1, 0x0

    .line 2612732
    const-string v4, "p2p_initiate_decline_request"

    invoke-static {v3, v4}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->e(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;Ljava/lang/String;)V

    .line 2612733
    iget-object v4, v3, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2612734
    iget-object v5, v4, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->o:Lcom/facebook/user/model/Name;

    move-object v4, v5

    .line 2612735
    invoke-virtual {v4}, Lcom/facebook/user/model/Name;->b()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, v3, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2612736
    iget-object v5, v4, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->o:Lcom/facebook/user/model/Name;

    move-object v4, v5

    .line 2612737
    invoke-virtual {v4}, Lcom/facebook/user/model/Name;->a()Ljava/lang/String;

    move-result-object v4

    .line 2612738
    :goto_0
    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f082d27

    new-array p0, p1, [Ljava/lang/Object;

    aput-object v4, p0, v1

    invoke-virtual {v5, v6, p0}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const p0, 0x7f082d28

    new-array p1, p1, [Ljava/lang/Object;

    aput-object v4, p1, v1

    invoke-virtual {v6, p0, p1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const p0, 0x7f082d29

    invoke-virtual {v6, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 p0, 0x0

    invoke-static {v5, v4, v6, p0, v1}, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;

    move-result-object v4

    .line 2612739
    iget-object v5, v3, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->J:LX/6wv;

    .line 2612740
    iput-object v5, v4, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;->m:LX/6wv;

    .line 2612741
    invoke-virtual {v3}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v5

    iget-object v6, v3, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->w:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2612742
    const v1, -0x3f28aa2f

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2612743
    :cond_0
    iget-object v4, v3, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;->B:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2612744
    iget-object v5, v4, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->o:Lcom/facebook/user/model/Name;

    move-object v4, v5

    .line 2612745
    invoke-virtual {v4}, Lcom/facebook/user/model/Name;->g()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method
