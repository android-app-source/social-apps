.class public final LX/IQs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/IQt;


# direct methods
.method public constructor <init>(LX/IQt;)V
    .locals 0

    .prologue
    .line 2575836
    iput-object p1, p0, LX/IQs;->a:LX/IQt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x4fc600f8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2575837
    iget-object v1, p0, LX/IQs;->a:LX/IQt;

    iget-object v1, v1, LX/IQt;->o:LX/IRO;

    if-eqz v1, :cond_2

    .line 2575838
    iget-object v1, p0, LX/IQs;->a:LX/IQt;

    iget-object v1, v1, LX/IQt;->o:LX/IRO;

    iget-object v2, p0, LX/IQs;->a:LX/IQt;

    invoke-virtual {v2}, LX/1a1;->d()I

    move-result v2

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 2575839
    iget-object v4, v1, LX/IRO;->a:LX/IRS;

    iget-object v4, v4, LX/IRS;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v4, :cond_2

    .line 2575840
    iget-object v4, v1, LX/IRO;->a:LX/IRS;

    iget-object v4, v4, LX/IRS;->a:LX/DOL;

    iget-object v5, v1, LX/IRO;->a:LX/IRS;

    iget-object v5, v5, LX/IRS;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 2575841
    invoke-static {v4}, LX/DOL;->a(LX/DOL;)Landroid/content/Intent;

    move-result-object v8

    .line 2575842
    const-string p0, "target_fragment"

    sget-object p1, LX/0cQ;->GROUPS_DISCUSSION_TOPICS_FRAGMENT:LX/0cQ;

    invoke-virtual {p1}, LX/0cQ;->ordinal()I

    move-result p1

    invoke-virtual {v8, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2575843
    const-string p0, "group_feed_model"

    invoke-static {v8, p0, v5}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2575844
    const-string p0, "group_feed_id"

    invoke-virtual {v5}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v8, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2575845
    move-object v8, v8

    .line 2575846
    iget-object v4, v1, LX/IRO;->a:LX/IRS;

    iget-object v4, v4, LX/IRS;->p:LX/0Px;

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel$GroupPostTopicsModel$NodesModel;

    .line 2575847
    const-string v5, "group_can_viewer_change_post_topics"

    iget-object p0, v1, LX/IRO;->a:LX/IRS;

    iget-boolean p0, p0, LX/IRS;->s:Z

    invoke-virtual {v8, v5, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2575848
    const-string v5, "group_discussion_topics_title"

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel$GroupPostTopicsModel$NodesModel;->l()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v8, v5, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2575849
    const-string v5, "group_discussion_topics_subscribe_status"

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel$GroupPostTopicsModel$NodesModel;->n()Lcom/facebook/graphql/enums/GraphQLGroupPostTopicSubscriptionState;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLGroupPostTopicSubscriptionState;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v8, v5, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2575850
    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel$GroupPostTopicsModel$NodesModel;->k()LX/1vs;

    move-result-object v5

    iget v5, v5, LX/1vs;->b:I

    if-eqz v5, :cond_4

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel$GroupPostTopicsModel$NodesModel;->k()LX/1vs;

    move-result-object v5

    iget-object p0, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    invoke-virtual {p0, v5, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_3

    move v5, v6

    :goto_0
    if-eqz v5, :cond_0

    .line 2575851
    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel$GroupPostTopicsModel$NodesModel;->k()LX/1vs;

    move-result-object v5

    iget-object p0, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    .line 2575852
    const-string p1, "group_discussion_topics_cover_photo_uri"

    invoke-virtual {p0, v5, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v8, p1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2575853
    :cond_0
    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel$GroupPostTopicsModel$NodesModel;->m()LX/1vs;

    move-result-object v5

    iget v5, v5, LX/1vs;->b:I

    if-eqz v5, :cond_5

    .line 2575854
    const-string v5, "groups_discussion_topics_id"

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel$GroupPostTopicsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2575855
    :goto_1
    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel$GroupPostTopicsModel$NodesModel;->m()LX/1vs;

    move-result-object v5

    iget v5, v5, LX/1vs;->b:I

    if-eqz v5, :cond_1

    .line 2575856
    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsGraphQLModels$GroupDiscussionTopicInformationModel$GroupPostTopicsModel$NodesModel;->m()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    .line 2575857
    const-string v6, "group_discussion_topics_stories_number"

    invoke-virtual {v5, v4, v7}, LX/15i;->j(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v6, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2575858
    :cond_1
    iget-object v4, v1, LX/IRO;->a:LX/IRS;

    iget-object v4, v4, LX/IRS;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v5, v1, LX/IRO;->a:LX/IRS;

    invoke-virtual {v5}, LX/IRS;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-interface {v4, v8, v5}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2575859
    :cond_2
    const v1, 0x3d2b8e4c

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_3
    move v5, v7

    .line 2575860
    goto :goto_0

    :cond_4
    move v5, v7

    goto :goto_0

    .line 2575861
    :cond_5
    sget-object v5, LX/IRS;->g:Ljava/lang/String;

    const-string v6, "The discussion topic Id is null"

    invoke-static {v5, v6}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
