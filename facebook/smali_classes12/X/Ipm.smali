.class public LX/Ipm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;


# instance fields
.field public final enabled:Ljava/lang/Boolean;

.field public final irisSeqId:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2614378
    new-instance v0, LX/1sv;

    const-string v1, "DeltaPaymentEnable"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/Ipm;->b:LX/1sv;

    .line 2614379
    new-instance v0, LX/1sw;

    const-string v1, "enabled"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipm;->c:LX/1sw;

    .line 2614380
    new-instance v0, LX/1sw;

    const-string v1, "irisSeqId"

    const/16 v2, 0xa

    const/16 v3, 0x3e8

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipm;->d:LX/1sw;

    .line 2614381
    sput-boolean v4, LX/Ipm;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 2614374
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2614375
    iput-object p1, p0, LX/Ipm;->enabled:Ljava/lang/Boolean;

    .line 2614376
    iput-object p2, p0, LX/Ipm;->irisSeqId:Ljava/lang/Long;

    .line 2614377
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 2614342
    if-eqz p2, :cond_3

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 2614343
    :goto_0
    if-eqz p2, :cond_4

    const-string v0, "\n"

    move-object v2, v0

    .line 2614344
    :goto_1
    if-eqz p2, :cond_5

    const-string v0, " "

    .line 2614345
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v1, "DeltaPaymentEnable"

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2614346
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614347
    const-string v1, "("

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614348
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614349
    const/4 v1, 0x1

    .line 2614350
    iget-object v5, p0, LX/Ipm;->enabled:Ljava/lang/Boolean;

    if-eqz v5, :cond_0

    .line 2614351
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614352
    const-string v1, "enabled"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614353
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614354
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614355
    iget-object v1, p0, LX/Ipm;->enabled:Ljava/lang/Boolean;

    if-nez v1, :cond_6

    .line 2614356
    const-string v1, "null"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614357
    :goto_3
    const/4 v1, 0x0

    .line 2614358
    :cond_0
    iget-object v5, p0, LX/Ipm;->irisSeqId:Ljava/lang/Long;

    if-eqz v5, :cond_2

    .line 2614359
    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614360
    :cond_1
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614361
    const-string v1, "irisSeqId"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614362
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614363
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614364
    iget-object v0, p0, LX/Ipm;->irisSeqId:Ljava/lang/Long;

    if-nez v0, :cond_7

    .line 2614365
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614366
    :cond_2
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614367
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614368
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2614369
    :cond_3
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_0

    .line 2614370
    :cond_4
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_1

    .line 2614371
    :cond_5
    const-string v0, ""

    goto/16 :goto_2

    .line 2614372
    :cond_6
    iget-object v1, p0, LX/Ipm;->enabled:Ljava/lang/Boolean;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v1, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 2614373
    :cond_7
    iget-object v0, p0, LX/Ipm;->irisSeqId:Ljava/lang/Long;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 2

    .prologue
    .line 2614330
    invoke-virtual {p1}, LX/1su;->a()V

    .line 2614331
    iget-object v0, p0, LX/Ipm;->enabled:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 2614332
    iget-object v0, p0, LX/Ipm;->enabled:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 2614333
    sget-object v0, LX/Ipm;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2614334
    iget-object v0, p0, LX/Ipm;->enabled:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 2614335
    :cond_0
    iget-object v0, p0, LX/Ipm;->irisSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 2614336
    iget-object v0, p0, LX/Ipm;->irisSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 2614337
    sget-object v0, LX/Ipm;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2614338
    iget-object v0, p0, LX/Ipm;->irisSeqId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2614339
    :cond_1
    invoke-virtual {p1}, LX/1su;->c()V

    .line 2614340
    invoke-virtual {p1}, LX/1su;->b()V

    .line 2614341
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2614308
    if-nez p1, :cond_1

    .line 2614309
    :cond_0
    :goto_0
    return v0

    .line 2614310
    :cond_1
    instance-of v1, p1, LX/Ipm;

    if-eqz v1, :cond_0

    .line 2614311
    check-cast p1, LX/Ipm;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2614312
    if-nez p1, :cond_3

    .line 2614313
    :cond_2
    :goto_1
    move v0, v2

    .line 2614314
    goto :goto_0

    .line 2614315
    :cond_3
    iget-object v0, p0, LX/Ipm;->enabled:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    move v0, v1

    .line 2614316
    :goto_2
    iget-object v3, p1, LX/Ipm;->enabled:Ljava/lang/Boolean;

    if-eqz v3, :cond_9

    move v3, v1

    .line 2614317
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 2614318
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2614319
    iget-object v0, p0, LX/Ipm;->enabled:Ljava/lang/Boolean;

    iget-object v3, p1, LX/Ipm;->enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2614320
    :cond_5
    iget-object v0, p0, LX/Ipm;->irisSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_a

    move v0, v1

    .line 2614321
    :goto_4
    iget-object v3, p1, LX/Ipm;->irisSeqId:Ljava/lang/Long;

    if-eqz v3, :cond_b

    move v3, v1

    .line 2614322
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 2614323
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2614324
    iget-object v0, p0, LX/Ipm;->irisSeqId:Ljava/lang/Long;

    iget-object v3, p1, LX/Ipm;->irisSeqId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_7
    move v2, v1

    .line 2614325
    goto :goto_1

    :cond_8
    move v0, v2

    .line 2614326
    goto :goto_2

    :cond_9
    move v3, v2

    .line 2614327
    goto :goto_3

    :cond_a
    move v0, v2

    .line 2614328
    goto :goto_4

    :cond_b
    move v3, v2

    .line 2614329
    goto :goto_5
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2614304
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2614305
    sget-boolean v0, LX/Ipm;->a:Z

    .line 2614306
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/Ipm;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2614307
    return-object v0
.end method
