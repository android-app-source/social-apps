.class public LX/Hx8;
.super Landroid/text/style/MetricAffectingSpan;
.source ""


# instance fields
.field private final a:Landroid/graphics/Typeface;

.field private final b:I

.field private final c:I


# direct methods
.method public constructor <init>(Landroid/graphics/Typeface;II)V
    .locals 0

    .prologue
    .line 2522021
    invoke-direct {p0}, Landroid/text/style/MetricAffectingSpan;-><init>()V

    .line 2522022
    iput-object p1, p0, LX/Hx8;->a:Landroid/graphics/Typeface;

    .line 2522023
    iput p2, p0, LX/Hx8;->b:I

    .line 2522024
    iput p3, p0, LX/Hx8;->c:I

    .line 2522025
    return-void
.end method

.method private static a(Landroid/graphics/Paint;Landroid/graphics/Typeface;II)V
    .locals 2

    .prologue
    .line 2522026
    invoke-virtual {p0}, Landroid/graphics/Paint;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    .line 2522027
    if-nez v0, :cond_4

    .line 2522028
    const/4 v0, 0x0

    .line 2522029
    :goto_0
    if-nez p1, :cond_0

    .line 2522030
    invoke-static {p1, p3}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    .line 2522031
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Typeface;->getStyle()I

    move-result v1

    xor-int/lit8 v1, v1, -0x1

    and-int/2addr v0, v1

    .line 2522032
    and-int/lit8 v1, v0, 0x1

    if-eqz v1, :cond_1

    .line 2522033
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    .line 2522034
    :cond_1
    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    .line 2522035
    const/high16 v0, -0x41800000    # -0.25f

    invoke-virtual {p0, v0}, Landroid/graphics/Paint;->setTextSkewX(F)V

    .line 2522036
    :cond_2
    invoke-virtual {p0, p1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 2522037
    if-lez p2, :cond_3

    .line 2522038
    int-to-float v0, p2

    invoke-virtual {p0, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 2522039
    :cond_3
    return-void

    .line 2522040
    :cond_4
    invoke-virtual {v0}, Landroid/graphics/Typeface;->getStyle()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 3

    .prologue
    .line 2522041
    iget-object v0, p0, LX/Hx8;->a:Landroid/graphics/Typeface;

    iget v1, p0, LX/Hx8;->b:I

    iget v2, p0, LX/Hx8;->c:I

    invoke-static {p1, v0, v1, v2}, LX/Hx8;->a(Landroid/graphics/Paint;Landroid/graphics/Typeface;II)V

    .line 2522042
    return-void
.end method

.method public final updateMeasureState(Landroid/text/TextPaint;)V
    .locals 3

    .prologue
    .line 2522043
    iget-object v0, p0, LX/Hx8;->a:Landroid/graphics/Typeface;

    iget v1, p0, LX/Hx8;->b:I

    iget v2, p0, LX/Hx8;->c:I

    invoke-static {p1, v0, v1, v2}, LX/Hx8;->a(Landroid/graphics/Paint;Landroid/graphics/Typeface;II)V

    .line 2522044
    return-void
.end method
