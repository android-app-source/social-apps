.class public LX/I8K;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/I8J;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2540939
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 2540940
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)LX/I8J;
    .locals 19

    .prologue
    .line 2540941
    new-instance v1, LX/I8J;

    invoke-static/range {p0 .. p0}, LX/DBA;->a(LX/0QB;)LX/DBA;

    move-result-object v3

    check-cast v3, LX/DBA;

    invoke-static/range {p0 .. p0}, LX/I7w;->a(LX/0QB;)LX/I7w;

    move-result-object v4

    check-cast v4, LX/I7w;

    invoke-static/range {p0 .. p0}, LX/I7z;->a(LX/0QB;)LX/I7z;

    move-result-object v5

    check-cast v5, LX/I7z;

    invoke-static/range {p0 .. p0}, LX/I7n;->a(LX/0QB;)LX/I7n;

    move-result-object v6

    check-cast v6, LX/I7n;

    invoke-static/range {p0 .. p0}, LX/DBE;->a(LX/0QB;)LX/DBE;

    move-result-object v7

    check-cast v7, LX/DBE;

    invoke-static/range {p0 .. p0}, LX/DBH;->a(LX/0QB;)LX/DBH;

    move-result-object v8

    check-cast v8, LX/DBH;

    const-class v2, LX/I82;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/I82;

    invoke-static/range {p0 .. p0}, LX/1nQ;->a(LX/0QB;)LX/1nQ;

    move-result-object v10

    check-cast v10, LX/1nQ;

    invoke-static/range {p0 .. p0}, LX/Bie;->a(LX/0QB;)LX/Bie;

    move-result-object v11

    check-cast v11, LX/Bie;

    invoke-static/range {p0 .. p0}, LX/6RZ;->a(LX/0QB;)LX/6RZ;

    move-result-object v12

    check-cast v12, LX/6RZ;

    invoke-static/range {p0 .. p0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v13

    check-cast v13, LX/0wM;

    invoke-static/range {p0 .. p0}, LX/15N;->a(LX/0QB;)LX/01T;

    move-result-object v14

    check-cast v14, LX/01T;

    invoke-static/range {p0 .. p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v15

    check-cast v15, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {p0 .. p0}, LX/B9y;->a(LX/0QB;)LX/B9y;

    move-result-object v16

    check-cast v16, LX/B9y;

    invoke-static/range {p0 .. p0}, LX/1Sa;->a(LX/0QB;)LX/1Sa;

    move-result-object v17

    check-cast v17, LX/1Sa;

    invoke-static/range {p0 .. p0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v18

    check-cast v18, Ljava/lang/Boolean;

    move-object/from16 v2, p1

    invoke-direct/range {v1 .. v18}, LX/I8J;-><init>(Landroid/view/View;LX/DBA;LX/I7w;LX/I7z;LX/I7n;LX/DBE;LX/DBH;LX/I82;LX/1nQ;LX/Bie;LX/6RZ;LX/0wM;LX/01T;Lcom/facebook/content/SecureContextHelper;LX/B9y;LX/1Sa;Ljava/lang/Boolean;)V

    .line 2540942
    return-object v1
.end method
