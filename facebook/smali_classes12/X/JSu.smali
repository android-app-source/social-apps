.class public LX/JSu;
.super Landroid/view/animation/Animation;
.source ""


# instance fields
.field private final a:Lcom/facebook/feedplugins/musicstory/animations/VinylView;

.field private b:F

.field private c:F

.field public d:F


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/musicstory/animations/VinylView;)V
    .locals 1

    .prologue
    .line 2696193
    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    .line 2696194
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/musicstory/animations/VinylView;

    iput-object v0, p0, LX/JSu;->a:Lcom/facebook/feedplugins/musicstory/animations/VinylView;

    .line 2696195
    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {p0, v0}, LX/JSu;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 2696196
    return-void
.end method


# virtual methods
.method public final applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 5

    .prologue
    .line 2696197
    invoke-virtual {p0}, LX/JSu;->getScaleFactor()F

    move-result v0

    .line 2696198
    iget v1, p0, LX/JSu;->d:F

    const/high16 v2, 0x40e00000    # 7.0f

    add-float/2addr v1, v2

    iput v1, p0, LX/JSu;->d:F

    .line 2696199
    invoke-virtual {p2}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    iget v2, p0, LX/JSu;->d:F

    iget v3, p0, LX/JSu;->b:F

    mul-float/2addr v3, v0

    iget v4, p0, LX/JSu;->c:F

    mul-float/2addr v0, v4

    invoke-virtual {v1, v2, v3, v0}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 2696200
    iget-object v0, p0, LX/JSu;->a:Lcom/facebook/feedplugins/musicstory/animations/VinylView;

    invoke-virtual {v0}, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->invalidate()V

    .line 2696201
    return-void
.end method

.method public final initialize(IIII)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/high16 v1, 0x3f000000    # 0.5f

    .line 2696202
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/animation/Animation;->initialize(IIII)V

    .line 2696203
    invoke-virtual {p0, v2, v1, p1, p3}, LX/JSu;->resolveSize(IFII)F

    move-result v0

    iput v0, p0, LX/JSu;->b:F

    .line 2696204
    invoke-virtual {p0, v2, v1, p2, p4}, LX/JSu;->resolveSize(IFII)F

    move-result v0

    iput v0, p0, LX/JSu;->c:F

    .line 2696205
    return-void
.end method
