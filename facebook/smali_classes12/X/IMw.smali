.class public final LX/IMw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/IMx;


# direct methods
.method public constructor <init>(LX/IMx;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2570311
    iput-object p1, p0, LX/IMw;->b:LX/IMx;

    iput-object p2, p0, LX/IMw;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v8, 0x2

    const v0, 0x40a47cef

    invoke-static {v8, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    .line 2570312
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2570313
    const-string v0, "group_name"

    iget-object v1, p0, LX/IMw;->a:Ljava/lang/String;

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2570314
    const-string v0, "parent_group_id"

    iget-object v1, p0, LX/IMw;->b:LX/IMx;

    iget-object v1, v1, LX/IMx;->a:Lcom/facebook/groups/community/search/CommunitySearchFragment;

    iget-object v1, v1, Lcom/facebook/groups/community/search/CommunitySearchFragment;->t:Ljava/lang/String;

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2570315
    const-string v0, "parent_group_or_page_name"

    iget-object v1, p0, LX/IMw;->b:LX/IMx;

    iget-object v1, v1, LX/IMx;->a:Lcom/facebook/groups/community/search/CommunitySearchFragment;

    iget-object v1, v1, Lcom/facebook/groups/community/search/CommunitySearchFragment;->u:Ljava/lang/String;

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2570316
    const-string v0, "ref"

    const-string v1, "BOOKMARKS"

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2570317
    const-string v0, "quick_return"

    invoke-virtual {v3, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2570318
    iget-object v0, p0, LX/IMw;->b:LX/IMx;

    iget-object v0, v0, LX/IMx;->a:Lcom/facebook/groups/community/search/CommunitySearchFragment;

    iget-object v0, v0, Lcom/facebook/groups/community/search/CommunitySearchFragment;->f:LX/17W;

    iget-object v1, p0, LX/IMw;->b:LX/IMx;

    iget-object v1, v1, LX/IMx;->a:Lcom/facebook/groups/community/search/CommunitySearchFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, LX/0ax;->N:Ljava/lang/String;

    const/4 v4, 0x0

    const/16 v5, 0x64

    iget-object v6, p0, LX/IMw;->b:LX/IMx;

    iget-object v6, v6, LX/IMx;->a:Lcom/facebook/groups/community/search/CommunitySearchFragment;

    invoke-virtual {v6}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v6

    check-cast v6, Landroid/app/Activity;

    invoke-virtual/range {v0 .. v6}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;ILandroid/app/Activity;)Z

    .line 2570319
    const v0, -0x594fca85

    invoke-static {v8, v8, v0, v7}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
