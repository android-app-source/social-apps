.class public LX/HcQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/tigon/tigonapi/TigonBodyProvider;


# instance fields
.field public final a:Lorg/apache/http/HttpEntity;

.field private final b:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Lorg/apache/http/HttpEntity;Ljava/util/concurrent/Executor;)V
    .locals 0

    .prologue
    .line 2487076
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2487077
    iput-object p1, p0, LX/HcQ;->a:Lorg/apache/http/HttpEntity;

    .line 2487078
    iput-object p2, p0, LX/HcQ;->b:Ljava/util/concurrent/Executor;

    .line 2487079
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 4

    .prologue
    .line 2487080
    iget-object v0, p0, LX/HcQ;->a:Lorg/apache/http/HttpEntity;

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v0

    .line 2487081
    const-wide/32 v2, 0x7fffffff

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 2487082
    const v0, 0x7fffffff

    .line 2487083
    :goto_0
    return v0

    :cond_0
    long-to-int v0, v0

    goto :goto_0
.end method

.method public final beginStream(Lcom/facebook/tigon/tigonapi/TigonBodyStream;)V
    .locals 3

    .prologue
    .line 2487084
    iget-object v0, p0, LX/HcQ;->b:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/tigon/httpclientadapter/TigonHttpEntityBodyProvider$EntityReader;

    invoke-direct {v1, p0, p1}, Lcom/facebook/tigon/httpclientadapter/TigonHttpEntityBodyProvider$EntityReader;-><init>(LX/HcQ;Lcom/facebook/tigon/tigonapi/TigonBodyStream;)V

    const v2, -0x2ebda30e

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2487085
    return-void
.end method
