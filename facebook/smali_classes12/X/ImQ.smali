.class public LX/ImQ;
.super LX/ImH;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final h:Ljava/lang/Object;


# instance fields
.field private final a:LX/J0B;

.field private final b:LX/IzJ;

.field private final c:LX/ImB;

.field private final d:LX/Duh;

.field private final e:LX/Izh;

.field private final f:LX/Izl;

.field public final g:LX/0Zb;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2609741
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/ImQ;->h:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/J0B;LX/IzJ;LX/ImB;LX/Duh;LX/Izh;LX/Izl;LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2609742
    invoke-direct {p0}, LX/ImH;-><init>()V

    .line 2609743
    iput-object p1, p0, LX/ImQ;->a:LX/J0B;

    .line 2609744
    iput-object p2, p0, LX/ImQ;->b:LX/IzJ;

    .line 2609745
    iput-object p3, p0, LX/ImQ;->c:LX/ImB;

    .line 2609746
    iput-object p4, p0, LX/ImQ;->d:LX/Duh;

    .line 2609747
    iput-object p5, p0, LX/ImQ;->e:LX/Izh;

    .line 2609748
    iput-object p6, p0, LX/ImQ;->f:LX/Izl;

    .line 2609749
    iput-object p7, p0, LX/ImQ;->g:LX/0Zb;

    .line 2609750
    return-void
.end method

.method public static a(LX/0QB;)LX/ImQ;
    .locals 15

    .prologue
    .line 2609751
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2609752
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2609753
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2609754
    if-nez v1, :cond_0

    .line 2609755
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2609756
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2609757
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2609758
    sget-object v1, LX/ImQ;->h:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2609759
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2609760
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2609761
    :cond_1
    if-nez v1, :cond_4

    .line 2609762
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2609763
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2609764
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2609765
    new-instance v7, LX/ImQ;

    invoke-static {v0}, LX/J0B;->a(LX/0QB;)LX/J0B;

    move-result-object v8

    check-cast v8, LX/J0B;

    invoke-static {v0}, LX/IzJ;->a(LX/0QB;)LX/IzJ;

    move-result-object v9

    check-cast v9, LX/IzJ;

    invoke-static {v0}, LX/ImB;->a(LX/0QB;)LX/ImB;

    move-result-object v10

    check-cast v10, LX/ImB;

    invoke-static {v0}, LX/Duh;->a(LX/0QB;)LX/Duh;

    move-result-object v11

    check-cast v11, LX/Duh;

    invoke-static {v0}, LX/Izh;->a(LX/0QB;)LX/Izh;

    move-result-object v12

    check-cast v12, LX/Izh;

    invoke-static {v0}, LX/Izl;->a(LX/0QB;)LX/Izl;

    move-result-object v13

    check-cast v13, LX/Izl;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v14

    check-cast v14, LX/0Zb;

    invoke-direct/range {v7 .. v14}, LX/ImQ;-><init>(LX/J0B;LX/IzJ;LX/ImB;LX/Duh;LX/Izh;LX/Izl;LX/0Zb;)V

    .line 2609766
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2609767
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2609768
    if-nez v1, :cond_2

    .line 2609769
    sget-object v0, LX/ImQ;->h:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ImQ;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2609770
    :goto_1
    if-eqz v0, :cond_3

    .line 2609771
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2609772
    :goto_3
    check-cast v0, LX/ImQ;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2609773
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2609774
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2609775
    :catchall_1
    move-exception v0

    .line 2609776
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2609777
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2609778
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2609779
    :cond_2
    :try_start_8
    sget-object v0, LX/ImQ;->h:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ImQ;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public static a(Ljava/lang/Long;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2609780
    if-nez p0, :cond_0

    .line 2609781
    const/4 v0, 0x0

    .line 2609782
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/7GJ;)Landroid/os/Bundle;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7GJ",
            "<",
            "LX/Ipt;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 2609783
    iget-object v0, p1, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/Ipt;

    invoke-virtual {v0}, LX/Ipt;->d()LX/Ipx;

    move-result-object v0

    .line 2609784
    iget-object v1, v0, LX/Ipx;->transferFbId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 2609785
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2609786
    :try_start_0
    iget-object v4, p0, LX/ImQ;->e:LX/Izh;

    invoke-virtual {v4, v2, v3}, LX/Izh;->a(J)Lcom/facebook/payments/p2p/model/PaymentTransaction;

    move-result-object v4

    .line 2609787
    if-nez v4, :cond_0

    .line 2609788
    iget-object v0, p0, LX/ImQ;->c:LX/ImB;

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/ImB;->a(Ljava/lang/String;)V

    .line 2609789
    :goto_0
    return-object v1

    .line 2609790
    :cond_0
    iget-object v2, p0, LX/ImQ;->d:LX/Duh;

    invoke-virtual {v2, v4}, LX/Duh;->a(Lcom/facebook/payments/p2p/model/PaymentTransaction;)Z

    move-result v2

    .line 2609791
    if-eqz v2, :cond_2

    .line 2609792
    iget-object v3, v0, LX/Ipx;->newReceiverStatus:Ljava/lang/Integer;

    .line 2609793
    sget-object v5, LX/Iq6;->b:Ljava/util/Map;

    invoke-interface {v5, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, LX/DtQ;->fromString(Ljava/lang/String;)LX/DtQ;

    move-result-object v3

    .line 2609794
    :goto_1
    move-object v3, v3

    .line 2609795
    iget-object v5, v0, LX/Ipx;->timestampMs:Ljava/lang/Long;

    .line 2609796
    sget-object v6, LX/DtQ;->R_COMPLETED:LX/DtQ;

    if-eq v3, v6, :cond_1

    sget-object v6, LX/DtQ;->S_COMPLETED:LX/DtQ;

    if-ne v3, v6, :cond_3

    .line 2609797
    :cond_1
    invoke-static {v4}, LX/DtJ;->a(Lcom/facebook/payments/p2p/model/PaymentTransaction;)LX/DtJ;

    move-result-object v6

    .line 2609798
    iput-object v3, v6, LX/DtJ;->f:LX/DtQ;

    .line 2609799
    move-object v6, v6

    .line 2609800
    invoke-static {v5}, LX/ImQ;->a(Ljava/lang/Long;)Ljava/lang/String;

    move-result-object p1

    .line 2609801
    iput-object p1, v6, LX/DtJ;->g:Ljava/lang/String;

    .line 2609802
    move-object v6, v6

    .line 2609803
    invoke-static {v5}, LX/ImQ;->a(Ljava/lang/Long;)Ljava/lang/String;

    move-result-object p1

    .line 2609804
    iput-object p1, v6, LX/DtJ;->h:Ljava/lang/String;

    .line 2609805
    move-object v6, v6

    .line 2609806
    invoke-virtual {v6}, LX/DtJ;->o()Lcom/facebook/payments/p2p/model/PaymentTransaction;

    move-result-object v6

    .line 2609807
    :goto_2
    move-object v3, v6

    .line 2609808
    iget-object v4, p0, LX/ImQ;->f:LX/Izl;

    invoke-virtual {v4, v3}, LX/Izl;->b(Lcom/facebook/payments/p2p/model/PaymentTransaction;)V

    .line 2609809
    const-string v4, "newPaymentTransaction"

    invoke-virtual {v1, v4, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2609810
    if-eqz v2, :cond_4

    const-string v3, "p2p_receive"

    .line 2609811
    :goto_3
    iget-object v4, p0, LX/ImQ;->g:LX/0Zb;

    const-string v5, "p2p_sync_delta"

    invoke-static {v5, v3}, Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;->d(Ljava/lang/String;Ljava/lang/String;)LX/5fz;

    move-result-object v3

    const-string v5, "DeltaTransferStatus"

    invoke-virtual {v3, v5}, LX/5fz;->j(Ljava/lang/String;)LX/5fz;

    move-result-object v3

    iget-object v5, v0, LX/Ipx;->irisSeqId:Ljava/lang/Long;

    invoke-virtual {v3, v5}, LX/5fz;->a(Ljava/lang/Long;)LX/5fz;

    move-result-object v3

    .line 2609812
    iget-object v5, v3, LX/5fz;->a:Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;

    move-object v3, v5

    .line 2609813
    invoke-interface {v4, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2609814
    goto :goto_0

    :catch_0
    goto :goto_0

    .line 2609815
    :cond_2
    :try_start_1
    iget-object v3, v0, LX/Ipx;->newSenderStatus:Ljava/lang/Integer;

    .line 2609816
    sget-object v5, LX/Iq8;->b:Ljava/util/Map;

    invoke-interface {v5, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, LX/DtQ;->fromString(Ljava/lang/String;)LX/DtQ;

    move-result-object v3

    goto :goto_1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 2609817
    :cond_3
    :try_start_2
    invoke-static {v4}, LX/DtJ;->a(Lcom/facebook/payments/p2p/model/PaymentTransaction;)LX/DtJ;

    move-result-object v6

    .line 2609818
    iput-object v3, v6, LX/DtJ;->f:LX/DtQ;

    .line 2609819
    move-object v6, v6

    .line 2609820
    invoke-static {v5}, LX/ImQ;->a(Ljava/lang/Long;)Ljava/lang/String;

    move-result-object p1

    .line 2609821
    iput-object p1, v6, LX/DtJ;->h:Ljava/lang/String;

    .line 2609822
    move-object v6, v6

    .line 2609823
    invoke-virtual {v6}, LX/DtJ;->o()Lcom/facebook/payments/p2p/model/PaymentTransaction;

    move-result-object v6

    goto :goto_2
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 2609824
    :cond_4
    const-string v3, "p2p_send"

    goto :goto_3
.end method

.method public final a(Landroid/os/Bundle;LX/7GJ;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "LX/7GJ",
            "<",
            "LX/Ipt;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2609825
    const-string v0, "newPaymentTransaction"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/PaymentTransaction;

    .line 2609826
    if-eqz v0, :cond_0

    .line 2609827
    iget-object v1, p0, LX/ImQ;->b:LX/IzJ;

    invoke-virtual {v1, v0}, LX/IzJ;->a(Lcom/facebook/payments/p2p/model/PaymentTransaction;)V

    .line 2609828
    iget-object v1, p0, LX/ImQ;->a:LX/J0B;

    .line 2609829
    iget-object v2, v0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->g:LX/DtQ;

    move-object v2, v2

    .line 2609830
    iget-object v3, v0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->b:Ljava/lang/String;

    move-object v0, v3

    .line 2609831
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, LX/J0B;->a(LX/DtQ;J)V

    .line 2609832
    :cond_0
    return-void
.end method
