.class public final LX/Ivj;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V
    .locals 0

    .prologue
    .line 2628540
    iput-object p1, p0, LX/Ivj;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;B)V
    .locals 0

    .prologue
    .line 2628541
    invoke-direct {p0, p1}, LX/Ivj;-><init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V

    return-void
.end method


# virtual methods
.method public final onDown(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 2628542
    const/4 v0, 0x1

    return v0
.end method

.method public final onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2628543
    iget-object v0, p0, LX/Ivj;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    invoke-static {v0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->r(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/Ivj;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-object v0, v0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->A:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getTranslationY()F

    move-result v0

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 2628544
    iget-object v0, p0, LX/Ivj;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    .line 2628545
    iput-boolean v4, v0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ah:Z

    .line 2628546
    :cond_0
    iget-object v0, p0, LX/Ivj;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-object v0, v0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ad:LX/0hs;

    if-eqz v0, :cond_1

    .line 2628547
    iget-object v0, p0, LX/Ivj;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    .line 2628548
    invoke-static {v0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->i$redex0(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V

    .line 2628549
    :goto_0
    return v4

    .line 2628550
    :cond_1
    iget-object v0, p0, LX/Ivj;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-object v0, v0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->J:LX/0wd;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    goto :goto_0
.end method
