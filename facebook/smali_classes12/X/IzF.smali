.class public LX/IzF;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final c:LX/IzF;

.field public static final d:LX/IzF;


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/p2p/model/PaymentCard;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/IzE;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2634349
    new-instance v0, LX/IzF;

    .line 2634350
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2634351
    sget-object v2, LX/IzE;->CARDS_EXIST_BUT_NOT_ALL_IN_CACHE:LX/IzE;

    invoke-direct {v0, v1, v2}, LX/IzF;-><init>(LX/0Px;LX/IzE;)V

    sput-object v0, LX/IzF;->c:LX/IzF;

    .line 2634352
    new-instance v0, LX/IzF;

    .line 2634353
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2634354
    sget-object v2, LX/IzE;->NO_CARDS_EXIST:LX/IzE;

    invoke-direct {v0, v1, v2}, LX/IzF;-><init>(LX/0Px;LX/IzE;)V

    sput-object v0, LX/IzF;->d:LX/IzF;

    return-void
.end method

.method public constructor <init>(LX/0Px;LX/IzE;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/p2p/model/PaymentCard;",
            ">;",
            "LX/IzE;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2634345
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2634346
    iput-object p1, p0, LX/IzF;->a:LX/0Px;

    .line 2634347
    iput-object p2, p0, LX/IzF;->b:LX/IzE;

    .line 2634348
    return-void
.end method
