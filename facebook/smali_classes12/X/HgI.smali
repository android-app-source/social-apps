.class public LX/HgI;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1rq;

.field public final b:LX/0Sh;

.field public final c:LX/95S;

.field public final d:LX/95S;

.field public final e:LX/95S;

.field public final f:LX/95S;

.field public final g:I


# direct methods
.method public constructor <init>(LX/1rq;LX/0Sh;I)V
    .locals 7
    .param p3    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2492861
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2492862
    iput p3, p0, LX/HgI;->g:I

    .line 2492863
    iput-object p1, p0, LX/HgI;->a:LX/1rq;

    .line 2492864
    iput-object p2, p0, LX/HgI;->b:LX/0Sh;

    .line 2492865
    new-instance v0, LX/95S;

    .line 2492866
    iget-object v2, p0, LX/HgI;->a:LX/1rq;

    const-string v3, "WORK_GROUPS_TAB_ALL"

    new-instance v4, LX/HgE;

    iget v5, p0, LX/HgI;->g:I

    invoke-direct {v4, v5}, LX/HgE;-><init>(I)V

    invoke-virtual {v2, v3, v4}, LX/1rq;->a(Ljava/lang/String;LX/1rs;)LX/2jj;

    move-result-object v2

    const-wide/16 v4, 0x258

    .line 2492867
    iput-wide v4, v2, LX/2jj;->j:J

    .line 2492868
    move-object v2, v2

    .line 2492869
    const/4 v3, 0x1

    .line 2492870
    iput-boolean v3, v2, LX/2jj;->k:Z

    .line 2492871
    move-object v2, v2

    .line 2492872
    move-object v1, v2

    .line 2492873
    invoke-direct {v0, v1}, LX/95S;-><init>(LX/2jj;)V

    iput-object v0, p0, LX/HgI;->c:LX/95S;

    .line 2492874
    new-instance v0, LX/95S;

    .line 2492875
    iget-object v2, p0, LX/HgI;->a:LX/1rq;

    const-string v3, "WORK_GROUPS_TAB_RECENT"

    new-instance v4, LX/HgG;

    iget v5, p0, LX/HgI;->g:I

    invoke-direct {v4, v5}, LX/HgG;-><init>(I)V

    invoke-virtual {v2, v3, v4}, LX/1rq;->a(Ljava/lang/String;LX/1rs;)LX/2jj;

    move-result-object v2

    const-wide/16 v4, 0x258

    .line 2492876
    iput-wide v4, v2, LX/2jj;->j:J

    .line 2492877
    move-object v2, v2

    .line 2492878
    const/4 v3, 0x1

    .line 2492879
    iput-boolean v3, v2, LX/2jj;->k:Z

    .line 2492880
    move-object v2, v2

    .line 2492881
    move-object v1, v2

    .line 2492882
    invoke-direct {v0, v1}, LX/95S;-><init>(LX/2jj;)V

    iput-object v0, p0, LX/HgI;->d:LX/95S;

    .line 2492883
    new-instance v0, LX/95S;

    .line 2492884
    iget-object v2, p0, LX/HgI;->a:LX/1rq;

    const-string v3, "WORK_GROUPS_TAB_FAVORITED"

    new-instance v4, LX/HgF;

    iget v5, p0, LX/HgI;->g:I

    invoke-direct {v4, v5}, LX/HgF;-><init>(I)V

    invoke-virtual {v2, v3, v4}, LX/1rq;->a(Ljava/lang/String;LX/1rs;)LX/2jj;

    move-result-object v2

    const-wide/16 v4, 0x258

    .line 2492885
    iput-wide v4, v2, LX/2jj;->j:J

    .line 2492886
    move-object v2, v2

    .line 2492887
    const/4 v3, 0x1

    .line 2492888
    iput-boolean v3, v2, LX/2jj;->k:Z

    .line 2492889
    move-object v2, v2

    .line 2492890
    move-object v1, v2

    .line 2492891
    invoke-direct {v0, v1}, LX/95S;-><init>(LX/2jj;)V

    iput-object v0, p0, LX/HgI;->e:LX/95S;

    .line 2492892
    new-instance v0, LX/95S;

    .line 2492893
    iget-object v2, p0, LX/HgI;->a:LX/1rq;

    const-string v3, "WORK_GROUPS_TAB_TOP"

    new-instance v4, LX/HgH;

    iget v5, p0, LX/HgI;->g:I

    invoke-direct {v4, v5}, LX/HgH;-><init>(I)V

    invoke-virtual {v2, v3, v4}, LX/1rq;->a(Ljava/lang/String;LX/1rs;)LX/2jj;

    move-result-object v2

    const-wide/16 v4, 0x258

    .line 2492894
    iput-wide v4, v2, LX/2jj;->j:J

    .line 2492895
    move-object v2, v2

    .line 2492896
    const/4 v3, 0x1

    .line 2492897
    iput-boolean v3, v2, LX/2jj;->k:Z

    .line 2492898
    move-object v2, v2

    .line 2492899
    move-object v1, v2

    .line 2492900
    invoke-direct {v0, v1}, LX/95S;-><init>(LX/2jj;)V

    iput-object v0, p0, LX/HgI;->f:LX/95S;

    .line 2492901
    return-void
.end method
