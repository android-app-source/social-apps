.class public final enum LX/IBr;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IBr;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IBr;

.field public static final enum ABOUT:LX/IBr;

.field public static final enum ACTIVITY:LX/IBr;

.field public static final enum DISCUSSION:LX/IBr;

.field public static final enum EVENT:LX/IBr;


# instance fields
.field private final mTabName:I
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2547809
    new-instance v0, LX/IBr;

    const-string v1, "ABOUT"

    const v2, 0x7f081f06

    invoke-direct {v0, v1, v3, v2}, LX/IBr;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/IBr;->ABOUT:LX/IBr;

    .line 2547810
    new-instance v0, LX/IBr;

    const-string v1, "DISCUSSION"

    const v2, 0x7f081f07

    invoke-direct {v0, v1, v4, v2}, LX/IBr;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/IBr;->DISCUSSION:LX/IBr;

    .line 2547811
    new-instance v0, LX/IBr;

    const-string v1, "EVENT"

    const v2, 0x7f081f08

    invoke-direct {v0, v1, v5, v2}, LX/IBr;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/IBr;->EVENT:LX/IBr;

    .line 2547812
    new-instance v0, LX/IBr;

    const-string v1, "ACTIVITY"

    const v2, 0x7f081f09

    invoke-direct {v0, v1, v6, v2}, LX/IBr;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/IBr;->ACTIVITY:LX/IBr;

    .line 2547813
    const/4 v0, 0x4

    new-array v0, v0, [LX/IBr;

    sget-object v1, LX/IBr;->ABOUT:LX/IBr;

    aput-object v1, v0, v3

    sget-object v1, LX/IBr;->DISCUSSION:LX/IBr;

    aput-object v1, v0, v4

    sget-object v1, LX/IBr;->EVENT:LX/IBr;

    aput-object v1, v0, v5

    sget-object v1, LX/IBr;->ACTIVITY:LX/IBr;

    aput-object v1, v0, v6

    sput-object v0, LX/IBr;->$VALUES:[LX/IBr;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 2547814
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2547815
    iput p3, p0, LX/IBr;->mTabName:I

    .line 2547816
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IBr;
    .locals 1

    .prologue
    .line 2547817
    const-class v0, LX/IBr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IBr;

    return-object v0
.end method

.method public static values()[LX/IBr;
    .locals 1

    .prologue
    .line 2547818
    sget-object v0, LX/IBr;->$VALUES:[LX/IBr;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IBr;

    return-object v0
.end method


# virtual methods
.method public final getTabNameStringRes()I
    .locals 1
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation

    .prologue
    .line 2547819
    iget v0, p0, LX/IBr;->mTabName:I

    return v0
.end method
