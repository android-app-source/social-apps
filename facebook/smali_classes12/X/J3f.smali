.class public final LX/J3f;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/HuN;

.field public final synthetic b:LX/J3g;


# direct methods
.method public constructor <init>(LX/J3g;LX/HuN;)V
    .locals 0

    .prologue
    .line 2643195
    iput-object p1, p0, LX/J3f;->b:LX/J3g;

    iput-object p2, p0, LX/J3f;->a:LX/HuN;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x56f3a704

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2643196
    iget-object v1, p0, LX/J3f;->b:LX/J3g;

    iget-object v1, v1, LX/J3g;->m:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    if-eqz v1, :cond_0

    .line 2643197
    iget-object v1, p0, LX/J3f;->a:LX/HuN;

    iget-object v2, p0, LX/J3f;->b:LX/J3g;

    iget-object v2, v2, LX/J3g;->m:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    iget-object v3, p0, LX/J3f;->b:LX/J3g;

    iget v3, v3, LX/J3g;->n:I

    .line 2643198
    iget-object v5, v1, LX/HuN;->a:LX/HuQ;

    iget-object v5, v5, LX/HuQ;->a:LX/HuR;

    iget-object v5, v5, LX/HuR;->e:LX/HrC;

    const/4 v7, 0x0

    .line 2643199
    iget-object v6, v5, LX/HrC;->a:Lcom/facebook/composer/activity/ComposerFragment;

    invoke-static {v6}, Lcom/facebook/composer/activity/ComposerFragment;->ac(Lcom/facebook/composer/activity/ComposerFragment;)V

    .line 2643200
    iget-object v6, v5, LX/HrC;->a:Lcom/facebook/composer/activity/ComposerFragment;

    invoke-static {v6, v2, v7, v7}, Lcom/facebook/composer/activity/ComposerFragment;->a$redex0(Lcom/facebook/composer/activity/ComposerFragment;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;Lcom/facebook/composer/minutiae/model/MinutiaeObject;)V

    .line 2643201
    iget-object v5, v1, LX/HuN;->a:LX/HuQ;

    iget-object v5, v5, LX/HuQ;->a:LX/HuR;

    .line 2643202
    iget-object v6, v5, LX/HuR;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v6}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v6

    invoke-static {v6}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    move-object v7, v6

    check-cast v7, LX/0il;

    .line 2643203
    invoke-interface {v7}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0j0;

    check-cast v6, LX/0j8;

    invoke-interface {v6}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v1

    .line 2643204
    iget-object v6, v5, LX/HuR;->f:LX/9j4;

    invoke-interface {v7}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0j0;

    invoke-interface {v7}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->h()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->i()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->g()LX/0Px;

    move-result-object v2

    move v1, v3

    .line 2643205
    const-string v5, "lightweight_place_picker_place_picked"

    invoke-static {v5, v7, p0, p1, v2}, LX/9j4;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    .line 2643206
    const-string v3, "selected_row"

    invoke-virtual {v5, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2643207
    iget-object v3, v6, LX/9j4;->a:LX/0Zb;

    invoke-interface {v3, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2643208
    :cond_0
    const v1, 0x3d470ad7

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
