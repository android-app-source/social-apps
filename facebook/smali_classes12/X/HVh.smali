.class public LX/HVh;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/E1d;

.field private final b:Landroid/content/Context;

.field private final c:LX/17W;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/E1d;LX/17W;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2475305
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2475306
    iput-object p1, p0, LX/HVh;->b:Landroid/content/Context;

    .line 2475307
    iput-object p2, p0, LX/HVh;->a:LX/E1d;

    .line 2475308
    iput-object p3, p0, LX/HVh;->c:LX/17W;

    .line 2475309
    return-void
.end method


# virtual methods
.method public onClick(Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;)V
    .locals 6

    .prologue
    .line 2475294
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2475295
    invoke-virtual {p1}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->ak_()Ljava/lang/String;

    move-result-object v1

    .line 2475296
    const-string v2, "ANDROID_PAGE_ADS_AFTER_PARTY_AYMT_CONTEXT_ITEM"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2475297
    const-string v2, "ANDROID_PAGE_ADS_AFTER_PARTY_AYMT_CONTEXT_ITEM"

    .line 2475298
    :goto_0
    move-object v1, v2

    .line 2475299
    iget-wide v2, p2, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->a:J

    .line 2475300
    iget-object v4, p0, LX/HVh;->a:LX/E1d;

    invoke-virtual {v4, v1, v2, v3, v0}, LX/E1d;->a(Ljava/lang/String;JLjava/lang/String;)LX/2jY;

    .line 2475301
    iget-object v1, p0, LX/HVh;->b:Landroid/content/Context;

    invoke-static {v1, v0, v2, v3}, Lcom/facebook/pages/fb4a/admin_activity/views/PageIdentityPageTipView;->a(Landroid/content/Context;Ljava/lang/String;J)Landroid/os/Bundle;

    move-result-object v0

    .line 2475302
    sget-object v1, LX/0ax;->aO:Ljava/lang/String;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v4, v5

    invoke-static {v1, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2475303
    iget-object v2, p0, LX/HVh;->c:LX/17W;

    iget-object v3, p0, LX/HVh;->b:Landroid/content/Context;

    invoke-virtual {v2, v3, v1, v0}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 2475304
    return-void

    :cond_0
    const-string v2, "ANDROID_PMA_AFTER_PARTY_AYMT_CONTEXT_ITEM"

    goto :goto_0
.end method
