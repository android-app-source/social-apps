.class public final LX/I3T;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUserSuggestionsModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:I

.field public final synthetic b:I

.field public final synthetic c:LX/I3Y;


# direct methods
.method public constructor <init>(LX/I3Y;II)V
    .locals 0

    .prologue
    .line 2531433
    iput-object p1, p0, LX/I3T;->c:LX/I3Y;

    iput p2, p0, LX/I3T;->a:I

    iput p3, p0, LX/I3T;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2531434
    new-instance v0, LX/7oT;

    invoke-direct {v0}, LX/7oT;-><init>()V

    move-object v0, v0

    .line 2531435
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2531436
    new-instance v1, LX/7oT;

    invoke-direct {v1}, LX/7oT;-><init>()V

    const-string v2, "cuts_count"

    iget v3, p0, LX/I3T;->a:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "per_cut_count"

    iget v3, p0, LX/I3T;->b:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    .line 2531437
    iget-object v2, v1, LX/0gW;->e:LX/0w7;

    move-object v1, v2

    .line 2531438
    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0w7;)LX/0zO;

    .line 2531439
    iget-object v1, p0, LX/I3T;->c:LX/I3Y;

    iget-object v1, v1, LX/I3Y;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0
.end method
