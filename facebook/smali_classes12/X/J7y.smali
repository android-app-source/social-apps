.class public final LX/J7y;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/62F;


# instance fields
.field public final synthetic a:Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;)V
    .locals 0

    .prologue
    .line 2651346
    iput-object p1, p0, LX/J7y;->a:Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2651302
    iget-object v2, p0, LX/J7y;->a:Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2651303
    iget-object v5, v2, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->s:LX/J82;

    if-eqz v5, :cond_0

    iget-object v5, v2, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->s:LX/J82;

    invoke-virtual {v5}, LX/J82;->size()I

    move-result v5

    const/4 v6, 0x2

    if-eq v5, v6, :cond_7

    :cond_0
    move v3, v4

    .line 2651304
    :cond_1
    :goto_0
    move v2, v3

    .line 2651305
    if-nez v2, :cond_3

    .line 2651306
    :cond_2
    :goto_1
    return v1

    .line 2651307
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v2

    .line 2651308
    packed-switch v2, :pswitch_data_0

    :pswitch_0
    goto :goto_1

    .line 2651309
    :pswitch_1
    iget-object v1, p0, LX/J7y;->a:Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;

    invoke-static {p1, v0}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v2

    .line 2651310
    iput v2, v1, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->v:I

    .line 2651311
    iget-object v1, p0, LX/J7y;->a:Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    .line 2651312
    iput v2, v1, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->u:F

    .line 2651313
    move v1, v0

    .line 2651314
    goto :goto_1

    .line 2651315
    :pswitch_2
    iget-object v2, p0, LX/J7y;->a:Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;

    iget v2, v2, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->v:I

    invoke-static {p1, v2}, LX/2xd;->a(Landroid/view/MotionEvent;I)I

    move-result v2

    .line 2651316
    invoke-static {p1, v2}, LX/2xd;->c(Landroid/view/MotionEvent;I)F

    move-result v2

    .line 2651317
    iget-object v3, p0, LX/J7y;->a:Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;

    iget v3, v3, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->u:F

    sub-float v3, v2, v3

    .line 2651318
    iget-object v4, p0, LX/J7y;->a:Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;

    iget-boolean v4, v4, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->w:Z

    if-nez v4, :cond_4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    iget-object v4, p0, LX/J7y;->a:Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;

    iget v4, v4, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->t:I

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_4

    .line 2651319
    iget-object v3, p0, LX/J7y;->a:Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;

    .line 2651320
    iput-boolean v1, v3, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->w:Z

    .line 2651321
    :cond_4
    iget-object v3, p0, LX/J7y;->a:Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;

    iget-boolean v3, v3, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->w:Z

    if-eqz v3, :cond_2

    .line 2651322
    iget-object v1, p0, LX/J7y;->a:Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;

    .line 2651323
    iput v2, v1, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->u:F

    .line 2651324
    move v1, v0

    .line 2651325
    goto :goto_1

    .line 2651326
    :pswitch_3
    iget-object v2, p0, LX/J7y;->a:Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;

    .line 2651327
    iput-boolean v0, v2, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->w:Z

    .line 2651328
    iget-object v0, p0, LX/J7y;->a:Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;

    const/4 v2, -0x1

    .line 2651329
    iput v2, v0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->v:I

    .line 2651330
    goto :goto_1

    .line 2651331
    :pswitch_4
    invoke-static {p1}, LX/2xd;->b(Landroid/view/MotionEvent;)I

    move-result v0

    .line 2651332
    iget-object v2, p0, LX/J7y;->a:Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;

    invoke-static {p1, v0}, LX/2xd;->c(Landroid/view/MotionEvent;I)F

    move-result v3

    .line 2651333
    iput v3, v2, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->u:F

    .line 2651334
    iget-object v2, p0, LX/J7y;->a:Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;

    invoke-static {p1, v0}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    .line 2651335
    iput v0, v2, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->v:I

    .line 2651336
    goto :goto_1

    .line 2651337
    :pswitch_5
    invoke-static {p1}, LX/2xd;->b(Landroid/view/MotionEvent;)I

    move-result v2

    .line 2651338
    invoke-static {p1, v2}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v3

    .line 2651339
    iget-object v4, p0, LX/J7y;->a:Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;

    iget v4, v4, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->v:I

    if-ne v3, v4, :cond_6

    .line 2651340
    if-nez v2, :cond_5

    move v0, v1

    .line 2651341
    :cond_5
    iget-object v2, p0, LX/J7y;->a:Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;

    invoke-static {p1, v0}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    .line 2651342
    iput v0, v2, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->v:I

    .line 2651343
    :cond_6
    iget-object v0, p0, LX/J7y;->a:Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;

    iget-object v2, p0, LX/J7y;->a:Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;

    iget v2, v2, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->v:I

    invoke-static {p1, v2}, LX/2xd;->a(Landroid/view/MotionEvent;I)I

    move-result v2

    invoke-static {p1, v2}, LX/2xd;->c(Landroid/view/MotionEvent;I)F

    move-result v2

    .line 2651344
    iput v2, v0, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->u:F

    .line 2651345
    goto/16 :goto_1

    :cond_7
    iget-object v5, v2, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->s:LX/J82;

    invoke-virtual {v5, v4}, LX/J82;->b(I)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v5

    if-eqz v5, :cond_8

    iget-object v5, v2, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->s:LX/J82;

    invoke-virtual {v5, v3}, LX/J82;->b(I)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/storygallerysurvey/fragment/StoryGallerySurveyWithStoryFragment;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v5

    if-nez v5, :cond_1

    :cond_8
    move v3, v4

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
