.class public LX/Hsm;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:LX/Hsf;

.field private final b:LX/Ht2;

.field private final c:LX/AnV;

.field private final d:LX/AnS;


# direct methods
.method public constructor <init>(LX/Hsf;LX/Ht2;LX/AnV;LX/AnS;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2515012
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2515013
    iput-object p1, p0, LX/Hsm;->a:LX/Hsf;

    .line 2515014
    iput-object p2, p0, LX/Hsm;->b:LX/Ht2;

    .line 2515015
    iput-object p3, p0, LX/Hsm;->c:LX/AnV;

    .line 2515016
    iput-object p4, p0, LX/Hsm;->d:LX/AnS;

    .line 2515017
    return-void
.end method

.method public static a(LX/0QB;)LX/Hsm;
    .locals 7

    .prologue
    .line 2515018
    const-class v1, LX/Hsm;

    monitor-enter v1

    .line 2515019
    :try_start_0
    sget-object v0, LX/Hsm;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2515020
    sput-object v2, LX/Hsm;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2515021
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2515022
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2515023
    new-instance p0, LX/Hsm;

    invoke-static {v0}, LX/Hsf;->a(LX/0QB;)LX/Hsf;

    move-result-object v3

    check-cast v3, LX/Hsf;

    invoke-static {v0}, LX/Ht2;->a(LX/0QB;)LX/Ht2;

    move-result-object v4

    check-cast v4, LX/Ht2;

    invoke-static {v0}, LX/AnV;->a(LX/0QB;)LX/AnV;

    move-result-object v5

    check-cast v5, LX/AnV;

    invoke-static {v0}, LX/AnS;->a(LX/0QB;)LX/AnS;

    move-result-object v6

    check-cast v6, LX/AnS;

    invoke-direct {p0, v3, v4, v5, v6}, LX/Hsm;-><init>(LX/Hsf;LX/Ht2;LX/AnV;LX/AnS;)V

    .line 2515024
    move-object v0, p0

    .line 2515025
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2515026
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Hsm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2515027
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2515028
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/ipc/composer/model/ComposerReshareContext;LX/HtA;)LX/1Dg;
    .locals 11
    .param p2    # Lcom/facebook/graphql/model/GraphQLStoryAttachment;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/ipc/composer/model/ComposerReshareContext;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # LX/HtA;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x4

    .line 2514969
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v5}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v5}, LX/1Dh;->B(I)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/1ni;->a(LX/1De;)LX/1nm;

    move-result-object v1

    const v2, 0x7f020a43

    invoke-virtual {v1, v2}, LX/1nm;->h(I)LX/1nm;

    move-result-object v1

    invoke-virtual {v1}, LX/1n6;->b()LX/1dc;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->b(LX/1dc;)LX/1Dh;

    move-result-object v0

    .line 2514970
    if-eqz p3, :cond_2

    .line 2514971
    iget-object v1, p0, LX/Hsm;->b:LX/Ht2;

    const/4 v2, 0x0

    .line 2514972
    new-instance v3, LX/Ht1;

    invoke-direct {v3, v1}, LX/Ht1;-><init>(LX/Ht2;)V

    .line 2514973
    sget-object v4, LX/Ht2;->a:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Ht0;

    .line 2514974
    if-nez v4, :cond_0

    .line 2514975
    new-instance v4, LX/Ht0;

    invoke-direct {v4}, LX/Ht0;-><init>()V

    .line 2514976
    :cond_0
    invoke-static {v4, p1, v2, v2, v3}, LX/Ht0;->a$redex0(LX/Ht0;LX/1De;IILX/Ht1;)V

    .line 2514977
    move-object v3, v4

    .line 2514978
    move-object v2, v3

    .line 2514979
    move-object v1, v2

    .line 2514980
    iget-object v2, v1, LX/Ht0;->a:LX/Ht1;

    iput-object p2, v2, LX/Ht1;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2514981
    iget-object v2, v1, LX/Ht0;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2514982
    move-object v1, v1

    .line 2514983
    iget-object v2, v1, LX/Ht0;->a:LX/Ht1;

    iput-object p3, v2, LX/Ht1;->c:Lcom/facebook/ipc/composer/model/ComposerReshareContext;

    .line 2514984
    iget-object v2, v1, LX/Ht0;->d:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2514985
    move-object v1, v1

    .line 2514986
    iget-object v2, v1, LX/Ht0;->a:LX/Ht1;

    iput-object p4, v2, LX/Ht1;->d:LX/HtA;

    .line 2514987
    iget-object v2, v1, LX/Ht0;->d:Ljava/util/BitSet;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2514988
    move-object v1, v1

    .line 2514989
    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    .line 2514990
    :goto_0
    const v1, -0x67292209

    invoke-static {p2, v1}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    .line 2514991
    if-eqz v1, :cond_1

    iget-object v2, p0, LX/Hsm;->d:LX/AnS;

    .line 2514992
    iget-object v7, v2, LX/AnS;->a:LX/0W3;

    sget-wide v9, LX/0X5;->kv:J

    invoke-interface {v7, v9, v10}, LX/0W4;->b(J)Z

    move-result v7

    move v2, v7

    .line 2514993
    if-eqz v2, :cond_1

    .line 2514994
    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v2

    const v3, 0x7f0a011d

    invoke-virtual {v2, v3}, LX/25Q;->i(I)LX/25Q;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    invoke-interface {v2, v6}, LX/1Di;->o(I)LX/1Di;

    move-result-object v2

    const/4 v3, 0x6

    const/4 v4, 0x2

    invoke-interface {v2, v3, v4}, LX/1Di;->d(II)LX/1Di;

    move-result-object v2

    invoke-interface {v2, v5}, LX/1Di;->b(I)LX/1Di;

    move-result-object v2

    invoke-interface {v0, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    iget-object v3, p0, LX/Hsm;->c:LX/AnV;

    invoke-virtual {v3, p1}, LX/AnV;->c(LX/1De;)LX/AnT;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/AnT;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)LX/AnT;

    move-result-object v1

    .line 2514995
    iget-object v3, v1, LX/AnT;->a:LX/AnU;

    iput-boolean v6, v3, LX/AnU;->c:Z

    .line 2514996
    move-object v1, v1

    .line 2514997
    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    .line 2514998
    :cond_1
    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0

    .line 2514999
    :cond_2
    iget-object v1, p0, LX/Hsm;->a:LX/Hsf;

    const/4 v2, 0x0

    .line 2515000
    new-instance v3, LX/Hsd;

    invoke-direct {v3, v1}, LX/Hsd;-><init>(LX/Hsf;)V

    .line 2515001
    sget-object v4, LX/Hsf;->a:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Hse;

    .line 2515002
    if-nez v4, :cond_3

    .line 2515003
    new-instance v4, LX/Hse;

    invoke-direct {v4}, LX/Hse;-><init>()V

    .line 2515004
    :cond_3
    invoke-static {v4, p1, v2, v2, v3}, LX/Hse;->a$redex0(LX/Hse;LX/1De;IILX/Hsd;)V

    .line 2515005
    move-object v3, v4

    .line 2515006
    move-object v2, v3

    .line 2515007
    move-object v1, v2

    .line 2515008
    iget-object v2, v1, LX/Hse;->a:LX/Hsd;

    iput-object p2, v2, LX/Hsd;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2515009
    iget-object v2, v1, LX/Hse;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2515010
    move-object v1, v1

    .line 2515011
    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    goto :goto_0
.end method
