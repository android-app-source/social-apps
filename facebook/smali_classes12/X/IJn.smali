.class public LX/IJn;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile e:LX/IJn;


# instance fields
.field public final b:LX/0tX;

.field public final c:LX/03V;

.field public final d:Ljava/util/Locale;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2564232
    const-class v0, LX/IJn;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/IJn;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0tX;LX/03V;LX/0W9;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2564233
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2564234
    iput-object p1, p0, LX/IJn;->b:LX/0tX;

    .line 2564235
    iput-object p2, p0, LX/IJn;->c:LX/03V;

    .line 2564236
    invoke-virtual {p3}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v0

    iput-object v0, p0, LX/IJn;->d:Ljava/util/Locale;

    .line 2564237
    return-void
.end method

.method public static a(LX/0QB;)LX/IJn;
    .locals 6

    .prologue
    .line 2564238
    sget-object v0, LX/IJn;->e:LX/IJn;

    if-nez v0, :cond_1

    .line 2564239
    const-class v1, LX/IJn;

    monitor-enter v1

    .line 2564240
    :try_start_0
    sget-object v0, LX/IJn;->e:LX/IJn;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2564241
    if-eqz v2, :cond_0

    .line 2564242
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2564243
    new-instance p0, LX/IJn;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v5

    check-cast v5, LX/0W9;

    invoke-direct {p0, v3, v4, v5}, LX/IJn;-><init>(LX/0tX;LX/03V;LX/0W9;)V

    .line 2564244
    move-object v0, p0

    .line 2564245
    sput-object v0, LX/IJn;->e:LX/IJn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2564246
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2564247
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2564248
    :cond_1
    sget-object v0, LX/IJn;->e:LX/IJn;

    return-object v0

    .line 2564249
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2564250
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(DDI)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(DDI)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Px",
            "<",
            "Landroid/location/Address;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2564251
    const-wide v4, -0x3fa9800000000000L    # -90.0

    cmpl-double v0, p1, v4

    if-ltz v0, :cond_0

    const-wide v4, 0x4056800000000000L    # 90.0

    cmpg-double v0, p1, v4

    if-gtz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2564252
    const-wide v4, -0x3f99800000000000L    # -180.0

    cmpl-double v0, p3, v4

    if-ltz v0, :cond_1

    const-wide v4, 0x4066800000000000L    # 180.0

    cmpg-double v0, p3, v4

    if-gtz v0, :cond_1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 2564253
    new-instance v0, LX/2vb;

    invoke-direct {v0}, LX/2vb;-><init>()V

    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2vb;->a(Ljava/lang/Double;)LX/2vb;

    move-result-object v0

    invoke-static {p3, p4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2vb;->b(Ljava/lang/Double;)LX/2vb;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 2564254
    new-instance v1, LX/4JC;

    invoke-direct {v1}, LX/4JC;-><init>()V

    invoke-virtual {v1, v0}, LX/4JC;->a(Ljava/util/List;)LX/4JC;

    move-result-object v0

    .line 2564255
    new-instance v1, LX/IJp;

    invoke-direct {v1}, LX/IJp;-><init>()V

    move-object v1, v1

    .line 2564256
    const-string v2, "coordinates"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    const-string v1, "limit"

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/IJp;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2564257
    iget-object v1, p0, LX/IJn;->b:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2564258
    new-instance v1, LX/IJl;

    invoke-direct {v1, p0}, LX/IJl;-><init>(LX/IJn;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    .line 2564259
    goto :goto_0

    :cond_1
    move v1, v2

    .line 2564260
    goto :goto_1
.end method
