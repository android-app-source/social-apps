.class public final enum LX/HYi;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HYi;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/HYi;

.field public static final enum BIRTHDAY:LX/HYi;

.field public static final enum CREATE:LX/HYi;

.field public static final enum EMAIL:LX/HYi;

.field public static final enum EXISTING_ACCOUNT:LX/HYi;

.field public static final enum GENDER:LX/HYi;

.field public static final enum NAME:LX/HYi;

.field public static final enum PASSWORD:LX/HYi;

.field public static final enum PHONE:LX/HYi;

.field public static final enum START:LX/HYi;

.field public static final enum UNKNOWN:LX/HYi;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2480975
    new-instance v0, LX/HYi;

    const-string v1, "START"

    invoke-direct {v0, v1, v3}, LX/HYi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HYi;->START:LX/HYi;

    .line 2480976
    new-instance v0, LX/HYi;

    const-string v1, "PHONE"

    invoke-direct {v0, v1, v4}, LX/HYi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HYi;->PHONE:LX/HYi;

    .line 2480977
    new-instance v0, LX/HYi;

    const-string v1, "EMAIL"

    invoke-direct {v0, v1, v5}, LX/HYi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HYi;->EMAIL:LX/HYi;

    .line 2480978
    new-instance v0, LX/HYi;

    const-string v1, "NAME"

    invoke-direct {v0, v1, v6}, LX/HYi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HYi;->NAME:LX/HYi;

    .line 2480979
    new-instance v0, LX/HYi;

    const-string v1, "BIRTHDAY"

    invoke-direct {v0, v1, v7}, LX/HYi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HYi;->BIRTHDAY:LX/HYi;

    .line 2480980
    new-instance v0, LX/HYi;

    const-string v1, "GENDER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/HYi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HYi;->GENDER:LX/HYi;

    .line 2480981
    new-instance v0, LX/HYi;

    const-string v1, "PASSWORD"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/HYi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HYi;->PASSWORD:LX/HYi;

    .line 2480982
    new-instance v0, LX/HYi;

    const-string v1, "CREATE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/HYi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HYi;->CREATE:LX/HYi;

    .line 2480983
    new-instance v0, LX/HYi;

    const-string v1, "EXISTING_ACCOUNT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/HYi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HYi;->EXISTING_ACCOUNT:LX/HYi;

    .line 2480984
    new-instance v0, LX/HYi;

    const-string v1, "UNKNOWN"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/HYi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HYi;->UNKNOWN:LX/HYi;

    .line 2480985
    const/16 v0, 0xa

    new-array v0, v0, [LX/HYi;

    sget-object v1, LX/HYi;->START:LX/HYi;

    aput-object v1, v0, v3

    sget-object v1, LX/HYi;->PHONE:LX/HYi;

    aput-object v1, v0, v4

    sget-object v1, LX/HYi;->EMAIL:LX/HYi;

    aput-object v1, v0, v5

    sget-object v1, LX/HYi;->NAME:LX/HYi;

    aput-object v1, v0, v6

    sget-object v1, LX/HYi;->BIRTHDAY:LX/HYi;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/HYi;->GENDER:LX/HYi;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/HYi;->PASSWORD:LX/HYi;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/HYi;->CREATE:LX/HYi;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/HYi;->EXISTING_ACCOUNT:LX/HYi;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/HYi;->UNKNOWN:LX/HYi;

    aput-object v2, v0, v1

    sput-object v0, LX/HYi;->$VALUES:[LX/HYi;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2480986
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HYi;
    .locals 1

    .prologue
    .line 2480987
    const-class v0, LX/HYi;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HYi;

    return-object v0
.end method

.method public static values()[LX/HYi;
    .locals 1

    .prologue
    .line 2480988
    sget-object v0, LX/HYi;->$VALUES:[LX/HYi;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HYi;

    return-object v0
.end method
