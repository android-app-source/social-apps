.class public final enum LX/Hx4;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Hx4;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Hx4;

.field public static final enum BIRTHDAYS_CARD:LX/Hx4;

.field public static final enum BOTTOM_PADDING:LX/Hx4;

.field public static final enum DASHBOARD_FILTER:LX/Hx4;

.field public static final enum EMPTY_HEADER:LX/Hx4;

.field public static final enum EVENT:LX/Hx4;

.field public static final enum LOADING:LX/Hx4;

.field public static final enum NO_EVENTS:LX/Hx4;

.field public static final enum SUBSCRIPTIONS:LX/Hx4;

.field public static final enum SUGGESTIONS:LX/Hx4;

.field public static final enum TEXT_HEADER:LX/Hx4;

.field public static final enum VIEW_ALL:LX/Hx4;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2521548
    new-instance v0, LX/Hx4;

    const-string v1, "DASHBOARD_FILTER"

    invoke-direct {v0, v1, v3}, LX/Hx4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hx4;->DASHBOARD_FILTER:LX/Hx4;

    .line 2521549
    new-instance v0, LX/Hx4;

    const-string v1, "TEXT_HEADER"

    invoke-direct {v0, v1, v4}, LX/Hx4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hx4;->TEXT_HEADER:LX/Hx4;

    .line 2521550
    new-instance v0, LX/Hx4;

    const-string v1, "EMPTY_HEADER"

    invoke-direct {v0, v1, v5}, LX/Hx4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hx4;->EMPTY_HEADER:LX/Hx4;

    .line 2521551
    new-instance v0, LX/Hx4;

    const-string v1, "EVENT"

    invoke-direct {v0, v1, v6}, LX/Hx4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hx4;->EVENT:LX/Hx4;

    .line 2521552
    new-instance v0, LX/Hx4;

    const-string v1, "BIRTHDAYS_CARD"

    invoke-direct {v0, v1, v7}, LX/Hx4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hx4;->BIRTHDAYS_CARD:LX/Hx4;

    .line 2521553
    new-instance v0, LX/Hx4;

    const-string v1, "LOADING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/Hx4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hx4;->LOADING:LX/Hx4;

    .line 2521554
    new-instance v0, LX/Hx4;

    const-string v1, "VIEW_ALL"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/Hx4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hx4;->VIEW_ALL:LX/Hx4;

    .line 2521555
    new-instance v0, LX/Hx4;

    const-string v1, "NO_EVENTS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/Hx4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hx4;->NO_EVENTS:LX/Hx4;

    .line 2521556
    new-instance v0, LX/Hx4;

    const-string v1, "SUGGESTIONS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/Hx4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hx4;->SUGGESTIONS:LX/Hx4;

    .line 2521557
    new-instance v0, LX/Hx4;

    const-string v1, "SUBSCRIPTIONS"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/Hx4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hx4;->SUBSCRIPTIONS:LX/Hx4;

    .line 2521558
    new-instance v0, LX/Hx4;

    const-string v1, "BOTTOM_PADDING"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/Hx4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hx4;->BOTTOM_PADDING:LX/Hx4;

    .line 2521559
    const/16 v0, 0xb

    new-array v0, v0, [LX/Hx4;

    sget-object v1, LX/Hx4;->DASHBOARD_FILTER:LX/Hx4;

    aput-object v1, v0, v3

    sget-object v1, LX/Hx4;->TEXT_HEADER:LX/Hx4;

    aput-object v1, v0, v4

    sget-object v1, LX/Hx4;->EMPTY_HEADER:LX/Hx4;

    aput-object v1, v0, v5

    sget-object v1, LX/Hx4;->EVENT:LX/Hx4;

    aput-object v1, v0, v6

    sget-object v1, LX/Hx4;->BIRTHDAYS_CARD:LX/Hx4;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/Hx4;->LOADING:LX/Hx4;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/Hx4;->VIEW_ALL:LX/Hx4;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/Hx4;->NO_EVENTS:LX/Hx4;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/Hx4;->SUGGESTIONS:LX/Hx4;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/Hx4;->SUBSCRIPTIONS:LX/Hx4;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/Hx4;->BOTTOM_PADDING:LX/Hx4;

    aput-object v2, v0, v1

    sput-object v0, LX/Hx4;->$VALUES:[LX/Hx4;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2521546
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Hx4;
    .locals 1

    .prologue
    .line 2521560
    const-class v0, LX/Hx4;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Hx4;

    return-object v0
.end method

.method public static values()[LX/Hx4;
    .locals 1

    .prologue
    .line 2521547
    sget-object v0, LX/Hx4;->$VALUES:[LX/Hx4;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Hx4;

    return-object v0
.end method
