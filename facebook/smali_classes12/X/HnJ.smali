.class public final enum LX/HnJ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HnJ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/HnJ;

.field public static final enum CONNECT_TIMEOUT_EXCEPTION_SCREEN_OPENED:LX/HnJ;

.field public static final enum CREATE_HOTSPOT_ERROR_SCREEN_OPENED:LX/HnJ;

.field public static final enum CREATE_HOTSPOT_SCREEN_OPENED:LX/HnJ;

.field public static final enum DECIDE_SHOULD_SEND_APK:LX/HnJ;

.field public static final enum DETERMINING_COMPATIBILITY_SCREEN_OPENED:LX/HnJ;

.field public static final enum ERROR_SCREEN_OPENED:LX/HnJ;

.field public static final enum GOOGLE_PLAY_ERROR_SCREEN_OPENED:LX/HnJ;

.field public static final enum MOBILE_DATA_WARNING_SCREEN_CLICKED:LX/HnJ;

.field public static final enum MOBILE_DATA_WARNING_SCREEN_OPENED:LX/HnJ;

.field public static final enum NOTHING_TO_SEND_SCREEN_OPENED:LX/HnJ;

.field public static final enum START_TRANSFER_SCREEN_CLICKED:LX/HnJ;

.field public static final enum START_TRANSFER_SCREEN_OPENED:LX/HnJ;

.field public static final enum SUCCESSFUL_TRANSFER_SCREEN_OPENED:LX/HnJ;

.field public static final enum TRANSFERRING_APK_SCREEN_OPENED:LX/HnJ;

.field public static final enum TRANSFER_ACTIVITY_BACK_PRESSED:LX/HnJ;

.field public static final enum TRANSFER_ACTIVITY_ON_DESTROY:LX/HnJ;

.field public static final enum TRY_AGAIN_BUTTON_CLICKED:LX/HnJ;

.field public static final enum UNEXPECTED_ERROR_SCREEN_OPENED:LX/HnJ;

.field public static final enum WAITING_FOR_CONNECTION_SCREEN_OPENED:LX/HnJ;

.field public static final enum WEBVIEW_CHECKED:LX/HnJ;

.field public static final enum WEBVIEW_PICKER_OPENED:LX/HnJ;


# instance fields
.field public final actionName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2501652
    new-instance v0, LX/HnJ;

    const-string v1, "GOOGLE_PLAY_ERROR_SCREEN_OPENED"

    const-string v2, "google_play_error_screen_opened"

    invoke-direct {v0, v1, v4, v2}, LX/HnJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HnJ;->GOOGLE_PLAY_ERROR_SCREEN_OPENED:LX/HnJ;

    .line 2501653
    new-instance v0, LX/HnJ;

    const-string v1, "WEBVIEW_CHECKED"

    const-string v2, "webview_checked"

    invoke-direct {v0, v1, v5, v2}, LX/HnJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HnJ;->WEBVIEW_CHECKED:LX/HnJ;

    .line 2501654
    new-instance v0, LX/HnJ;

    const-string v1, "WEBVIEW_PICKER_OPENED"

    const-string v2, "webview_picker_opened"

    invoke-direct {v0, v1, v6, v2}, LX/HnJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HnJ;->WEBVIEW_PICKER_OPENED:LX/HnJ;

    .line 2501655
    new-instance v0, LX/HnJ;

    const-string v1, "START_TRANSFER_SCREEN_OPENED"

    const-string v2, "start_transfer_screen_opened"

    invoke-direct {v0, v1, v7, v2}, LX/HnJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HnJ;->START_TRANSFER_SCREEN_OPENED:LX/HnJ;

    .line 2501656
    new-instance v0, LX/HnJ;

    const-string v1, "START_TRANSFER_SCREEN_CLICKED"

    const-string v2, "start_transfer_screen_clicked"

    invoke-direct {v0, v1, v8, v2}, LX/HnJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HnJ;->START_TRANSFER_SCREEN_CLICKED:LX/HnJ;

    .line 2501657
    new-instance v0, LX/HnJ;

    const-string v1, "MOBILE_DATA_WARNING_SCREEN_OPENED"

    const/4 v2, 0x5

    const-string v3, "mobile_data_warning_screen_opened"

    invoke-direct {v0, v1, v2, v3}, LX/HnJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HnJ;->MOBILE_DATA_WARNING_SCREEN_OPENED:LX/HnJ;

    .line 2501658
    new-instance v0, LX/HnJ;

    const-string v1, "MOBILE_DATA_WARNING_SCREEN_CLICKED"

    const/4 v2, 0x6

    const-string v3, "mobile_data_warning_screen_clicked"

    invoke-direct {v0, v1, v2, v3}, LX/HnJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HnJ;->MOBILE_DATA_WARNING_SCREEN_CLICKED:LX/HnJ;

    .line 2501659
    new-instance v0, LX/HnJ;

    const-string v1, "CREATE_HOTSPOT_SCREEN_OPENED"

    const/4 v2, 0x7

    const-string v3, "create_hotspot_screen_opened"

    invoke-direct {v0, v1, v2, v3}, LX/HnJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HnJ;->CREATE_HOTSPOT_SCREEN_OPENED:LX/HnJ;

    .line 2501660
    new-instance v0, LX/HnJ;

    const-string v1, "WAITING_FOR_CONNECTION_SCREEN_OPENED"

    const/16 v2, 0x8

    const-string v3, "waiting_for_connection_screen_opened"

    invoke-direct {v0, v1, v2, v3}, LX/HnJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HnJ;->WAITING_FOR_CONNECTION_SCREEN_OPENED:LX/HnJ;

    .line 2501661
    new-instance v0, LX/HnJ;

    const-string v1, "DETERMINING_COMPATIBILITY_SCREEN_OPENED"

    const/16 v2, 0x9

    const-string v3, "determining_compatibility_screen_opened"

    invoke-direct {v0, v1, v2, v3}, LX/HnJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HnJ;->DETERMINING_COMPATIBILITY_SCREEN_OPENED:LX/HnJ;

    .line 2501662
    new-instance v0, LX/HnJ;

    const-string v1, "DECIDE_SHOULD_SEND_APK"

    const/16 v2, 0xa

    const-string v3, "decide_should_send_apk"

    invoke-direct {v0, v1, v2, v3}, LX/HnJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HnJ;->DECIDE_SHOULD_SEND_APK:LX/HnJ;

    .line 2501663
    new-instance v0, LX/HnJ;

    const-string v1, "TRANSFERRING_APK_SCREEN_OPENED"

    const/16 v2, 0xb

    const-string v3, "transferring_apk_screen_opened"

    invoke-direct {v0, v1, v2, v3}, LX/HnJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HnJ;->TRANSFERRING_APK_SCREEN_OPENED:LX/HnJ;

    .line 2501664
    new-instance v0, LX/HnJ;

    const-string v1, "NOTHING_TO_SEND_SCREEN_OPENED"

    const/16 v2, 0xc

    const-string v3, "nothing_to_send_screen_opened"

    invoke-direct {v0, v1, v2, v3}, LX/HnJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HnJ;->NOTHING_TO_SEND_SCREEN_OPENED:LX/HnJ;

    .line 2501665
    new-instance v0, LX/HnJ;

    const-string v1, "SUCCESSFUL_TRANSFER_SCREEN_OPENED"

    const/16 v2, 0xd

    const-string v3, "successful_transfer_screen_opened"

    invoke-direct {v0, v1, v2, v3}, LX/HnJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HnJ;->SUCCESSFUL_TRANSFER_SCREEN_OPENED:LX/HnJ;

    .line 2501666
    new-instance v0, LX/HnJ;

    const-string v1, "ERROR_SCREEN_OPENED"

    const/16 v2, 0xe

    const-string v3, "error_screen_opened"

    invoke-direct {v0, v1, v2, v3}, LX/HnJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HnJ;->ERROR_SCREEN_OPENED:LX/HnJ;

    .line 2501667
    new-instance v0, LX/HnJ;

    const-string v1, "UNEXPECTED_ERROR_SCREEN_OPENED"

    const/16 v2, 0xf

    const-string v3, "unexpected_error_screen_opened"

    invoke-direct {v0, v1, v2, v3}, LX/HnJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HnJ;->UNEXPECTED_ERROR_SCREEN_OPENED:LX/HnJ;

    .line 2501668
    new-instance v0, LX/HnJ;

    const-string v1, "CREATE_HOTSPOT_ERROR_SCREEN_OPENED"

    const/16 v2, 0x10

    const-string v3, "create_hotspot_error_screen_opened"

    invoke-direct {v0, v1, v2, v3}, LX/HnJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HnJ;->CREATE_HOTSPOT_ERROR_SCREEN_OPENED:LX/HnJ;

    .line 2501669
    new-instance v0, LX/HnJ;

    const-string v1, "CONNECT_TIMEOUT_EXCEPTION_SCREEN_OPENED"

    const/16 v2, 0x11

    const-string v3, "connect_timeout_exception_screen_opened"

    invoke-direct {v0, v1, v2, v3}, LX/HnJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HnJ;->CONNECT_TIMEOUT_EXCEPTION_SCREEN_OPENED:LX/HnJ;

    .line 2501670
    new-instance v0, LX/HnJ;

    const-string v1, "TRY_AGAIN_BUTTON_CLICKED"

    const/16 v2, 0x12

    const-string v3, "try_again_button_clicked"

    invoke-direct {v0, v1, v2, v3}, LX/HnJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HnJ;->TRY_AGAIN_BUTTON_CLICKED:LX/HnJ;

    .line 2501671
    new-instance v0, LX/HnJ;

    const-string v1, "TRANSFER_ACTIVITY_ON_DESTROY"

    const/16 v2, 0x13

    const-string v3, "transfer_activity_on_destroy"

    invoke-direct {v0, v1, v2, v3}, LX/HnJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HnJ;->TRANSFER_ACTIVITY_ON_DESTROY:LX/HnJ;

    .line 2501672
    new-instance v0, LX/HnJ;

    const-string v1, "TRANSFER_ACTIVITY_BACK_PRESSED"

    const/16 v2, 0x14

    const-string v3, "transfer_activity_back_pressed"

    invoke-direct {v0, v1, v2, v3}, LX/HnJ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HnJ;->TRANSFER_ACTIVITY_BACK_PRESSED:LX/HnJ;

    .line 2501673
    const/16 v0, 0x15

    new-array v0, v0, [LX/HnJ;

    sget-object v1, LX/HnJ;->GOOGLE_PLAY_ERROR_SCREEN_OPENED:LX/HnJ;

    aput-object v1, v0, v4

    sget-object v1, LX/HnJ;->WEBVIEW_CHECKED:LX/HnJ;

    aput-object v1, v0, v5

    sget-object v1, LX/HnJ;->WEBVIEW_PICKER_OPENED:LX/HnJ;

    aput-object v1, v0, v6

    sget-object v1, LX/HnJ;->START_TRANSFER_SCREEN_OPENED:LX/HnJ;

    aput-object v1, v0, v7

    sget-object v1, LX/HnJ;->START_TRANSFER_SCREEN_CLICKED:LX/HnJ;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/HnJ;->MOBILE_DATA_WARNING_SCREEN_OPENED:LX/HnJ;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/HnJ;->MOBILE_DATA_WARNING_SCREEN_CLICKED:LX/HnJ;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/HnJ;->CREATE_HOTSPOT_SCREEN_OPENED:LX/HnJ;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/HnJ;->WAITING_FOR_CONNECTION_SCREEN_OPENED:LX/HnJ;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/HnJ;->DETERMINING_COMPATIBILITY_SCREEN_OPENED:LX/HnJ;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/HnJ;->DECIDE_SHOULD_SEND_APK:LX/HnJ;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/HnJ;->TRANSFERRING_APK_SCREEN_OPENED:LX/HnJ;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/HnJ;->NOTHING_TO_SEND_SCREEN_OPENED:LX/HnJ;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/HnJ;->SUCCESSFUL_TRANSFER_SCREEN_OPENED:LX/HnJ;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/HnJ;->ERROR_SCREEN_OPENED:LX/HnJ;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/HnJ;->UNEXPECTED_ERROR_SCREEN_OPENED:LX/HnJ;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/HnJ;->CREATE_HOTSPOT_ERROR_SCREEN_OPENED:LX/HnJ;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/HnJ;->CONNECT_TIMEOUT_EXCEPTION_SCREEN_OPENED:LX/HnJ;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/HnJ;->TRY_AGAIN_BUTTON_CLICKED:LX/HnJ;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/HnJ;->TRANSFER_ACTIVITY_ON_DESTROY:LX/HnJ;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/HnJ;->TRANSFER_ACTIVITY_BACK_PRESSED:LX/HnJ;

    aput-object v2, v0, v1

    sput-object v0, LX/HnJ;->$VALUES:[LX/HnJ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2501674
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2501675
    iput-object p3, p0, LX/HnJ;->actionName:Ljava/lang/String;

    .line 2501676
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HnJ;
    .locals 1

    .prologue
    .line 2501677
    const-class v0, LX/HnJ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HnJ;

    return-object v0
.end method

.method public static values()[LX/HnJ;
    .locals 1

    .prologue
    .line 2501678
    sget-object v0, LX/HnJ;->$VALUES:[LX/HnJ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HnJ;

    return-object v0
.end method
