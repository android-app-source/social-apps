.class public LX/Hpm;
.super LX/BcS;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Hpk;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Hpp;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2509858
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Hpm;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Hpp;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2509859
    invoke-direct {p0}, LX/BcS;-><init>()V

    .line 2509860
    iput-object p1, p0, LX/Hpm;->b:LX/0Ot;

    .line 2509861
    return-void
.end method

.method public static a(LX/0QB;)LX/Hpm;
    .locals 4

    .prologue
    .line 2509862
    const-class v1, LX/Hpm;

    monitor-enter v1

    .line 2509863
    :try_start_0
    sget-object v0, LX/Hpm;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2509864
    sput-object v2, LX/Hpm;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2509865
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2509866
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2509867
    new-instance v3, LX/Hpm;

    const/16 p0, 0x1952

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Hpm;-><init>(LX/0Ot;)V

    .line 2509868
    move-object v0, v3

    .line 2509869
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2509870
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Hpm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2509871
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2509872
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/BcQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2509873
    invoke-static {}, LX/1dS;->b()V

    .line 2509874
    iget v0, p1, LX/BcQ;->b:I

    .line 2509875
    packed-switch v0, :pswitch_data_0

    .line 2509876
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2509877
    :pswitch_0
    check-cast p2, LX/Hpz;

    .line 2509878
    iget-object v2, p2, LX/Hpz;->b:Lcom/facebook/graphql/modelutil/BaseModel;

    iget-object v0, p1, LX/BcQ;->c:[Ljava/lang/Object;

    const/4 v3, 0x0

    aget-object v0, v0, v3

    check-cast v0, Lcom/facebook/java2js/JSValue;

    iget-object v3, p1, LX/BcQ;->a:LX/BcO;

    .line 2509879
    check-cast v3, LX/Hpl;

    .line 2509880
    iget-object v4, p0, LX/Hpm;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Hpp;

    iget-object v5, v3, LX/Hpl;->c:LX/5KI;

    const/4 v1, 0x0

    .line 2509881
    const/4 p1, 0x1

    new-array p1, p1, [Ljava/lang/Object;

    .line 2509882
    :try_start_0
    iget-object p2, v4, LX/Hpp;->b:LX/0lB;

    iget-object p0, v4, LX/Hpp;->b:LX/0lB;

    invoke-virtual {p0, v2}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    const-class v3, LX/0P1;

    invoke-virtual {p2, p0, v3}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/0P1;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2509883
    :goto_1
    move-object p2, p2

    .line 2509884
    const-string p0, "node"

    invoke-virtual {p2, p0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    aput-object p2, p1, v1

    invoke-virtual {v0, p1}, Lcom/facebook/java2js/JSValue;->callAsFunction([Ljava/lang/Object;)Lcom/facebook/java2js/JSValue;

    move-result-object p1

    .line 2509885
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/java2js/JSValue;->isNull()Z

    move-result p2

    if-nez p2, :cond_0

    invoke-virtual {p1}, Lcom/facebook/java2js/JSValue;->isUndefined()Z

    move-result p2

    if-nez p2, :cond_0

    .line 2509886
    invoke-virtual {v5, p1}, LX/5KI;->a(Lcom/facebook/java2js/JSValue;)LX/1X5;

    move-result-object p1

    invoke-virtual {p1}, LX/1X5;->d()LX/1X1;

    move-result-object p1

    .line 2509887
    :goto_2
    move-object v4, p1

    .line 2509888
    move-object v0, v4

    .line 2509889
    goto :goto_0

    :cond_0
    new-instance p1, LX/Hpo;

    invoke-direct {p1, v4}, LX/Hpo;-><init>(LX/Hpp;)V

    goto :goto_2

    :catch_0
    new-instance p2, LX/0P2;

    invoke-direct {p2}, LX/0P2;-><init>()V

    invoke-virtual {p2}, LX/0P2;->b()LX/0P1;

    move-result-object p2

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch -0x613b35b8
        :pswitch_0
    .end packed-switch
.end method

.method public final a(LX/BcP;Ljava/util/List;LX/BcO;)V
    .locals 3

    .prologue
    .line 2509890
    check-cast p3, LX/Hpl;

    .line 2509891
    iget-object v0, p0, LX/Hpm;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hpp;

    iget-object v1, p3, LX/Hpl;->b:Lcom/facebook/java2js/JSValue;

    iget-object v2, p3, LX/Hpl;->c:LX/5KI;

    .line 2509892
    const-string p0, "section"

    invoke-virtual {v1, p0}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object p0

    .line 2509893
    invoke-static {v0, p1, v2, p0}, LX/Hpp;->a$redex0(LX/Hpp;LX/BcP;LX/5KI;Lcom/facebook/java2js/JSValue;)LX/BcO;

    move-result-object p0

    invoke-interface {p2, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2509894
    return-void
.end method
