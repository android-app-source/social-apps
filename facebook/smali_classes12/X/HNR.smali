.class public LX/HNR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/H8Z;


# static fields
.field private static final a:I

.field private static final b:I


# instance fields
.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Landroid/content/Context;

.field public f:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2458427
    const v0, 0x7f020743

    sput v0, LX/HNR;->a:I

    .line 2458428
    const v0, 0x7f08367d

    sput v0, LX/HNR;->b:I

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;Landroid/content/Context;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2458429
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2458430
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/HNR;->f:J

    .line 2458431
    iput-object p1, p0, LX/HNR;->c:LX/0Ot;

    .line 2458432
    iput-object p2, p0, LX/HNR;->d:LX/0Ot;

    .line 2458433
    iput-object p3, p0, LX/HNR;->e:Landroid/content/Context;

    .line 2458434
    return-void
.end method


# virtual methods
.method public final a()LX/HA7;
    .locals 10

    .prologue
    const/4 v4, 0x1

    .line 2458435
    new-instance v0, LX/HA7;

    sget-object v1, LX/HNG;->VISIT_PAGE:LX/HNG;

    invoke-virtual {v1}, LX/HNG;->ordinal()I

    move-result v1

    sget v2, LX/HNR;->b:I

    sget v3, LX/HNR;->a:I

    iget-wide v6, p0, LX/HNR;->f:J

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-lez v5, :cond_0

    move v5, v4

    :goto_0
    invoke-direct/range {v0 .. v5}, LX/HA7;-><init>(IIIIZ)V

    return-object v0

    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public final b()LX/HA7;
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 2458436
    new-instance v0, LX/HA7;

    sget-object v1, LX/HNG;->VISIT_PAGE:LX/HNG;

    invoke-virtual {v1}, LX/HNG;->ordinal()I

    move-result v1

    sget v2, LX/HNR;->b:I

    sget v3, LX/HNR;->a:I

    move v5, v4

    invoke-direct/range {v0 .. v5}, LX/HA7;-><init>(IIIIZ)V

    return-object v0
.end method

.method public final c()V
    .locals 6

    .prologue
    .line 2458437
    iget-wide v0, p0, LX/HNR;->f:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 2458438
    iget-object v0, p0, LX/HNR;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    iget-object v1, p0, LX/HNR;->e:Landroid/content/Context;

    sget-object v2, LX/0ax;->aE:Ljava/lang/String;

    iget-wide v4, p0, LX/HNR;->f:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2458439
    const-string v0, "page_view_referrer"

    sget-object v2, LX/89z;->DEEPLINK:LX/89z;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2458440
    iget-object v0, p0, LX/HNR;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/HNR;->e:Landroid/content/Context;

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2458441
    :cond_0
    return-void
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/H8E;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2458442
    const/4 v0, 0x0

    return-object v0
.end method
