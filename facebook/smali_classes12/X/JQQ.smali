.class public LX/JQQ;
.super LX/An9;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LX/An9",
        "<",
        "Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JQi;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JQV;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JQi;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/JQV;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2691783
    invoke-direct {p0}, LX/An9;-><init>()V

    .line 2691784
    iput-object p1, p0, LX/JQQ;->a:LX/0Ot;

    .line 2691785
    iput-object p2, p0, LX/JQQ;->b:LX/0Ot;

    .line 2691786
    return-void
.end method

.method public static a(LX/0QB;)LX/JQQ;
    .locals 5

    .prologue
    .line 2691772
    const-class v1, LX/JQQ;

    monitor-enter v1

    .line 2691773
    :try_start_0
    sget-object v0, LX/JQQ;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2691774
    sput-object v2, LX/JQQ;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2691775
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2691776
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2691777
    new-instance v3, LX/JQQ;

    const/16 v4, 0x2009

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0x2003

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, p0}, LX/JQQ;-><init>(LX/0Ot;LX/0Ot;)V

    .line 2691778
    move-object v0, v3

    .line 2691779
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2691780
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JQQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2691781
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2691782
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X1;
    .locals 1
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2691771
    iget-object v0, p0, LX/JQQ;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JQV;

    invoke-virtual {v0, p1}, LX/JQV;->c(LX/1De;)LX/JQT;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/3mj;Ljava/lang/Object;Ljava/lang/Object;)LX/1X1;
    .locals 3

    .prologue
    .line 2691761
    check-cast p4, Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;

    .line 2691762
    const/4 v1, 0x0

    .line 2691763
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2691764
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/1Wr;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;

    if-eqz v0, :cond_0

    .line 2691765
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2691766
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/1Wr;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;

    .line 2691767
    :goto_0
    new-instance v1, LX/JQj;

    const-string v2, "pivot"

    invoke-direct {v1, v0, p4, v2}, LX/JQj;-><init>(Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;Lcom/facebook/graphql/model/GraphQLItemListFeedUnitItem;Ljava/lang/String;)V

    .line 2691768
    iget-object v0, p0, LX/JQQ;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JQi;

    invoke-virtual {v0, p1}, LX/JQi;->c(LX/1De;)LX/JQg;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/JQg;->a(LX/JQj;)LX/JQg;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2691770
    const-class v0, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2691769
    const/16 v0, 0x8

    return v0
.end method
