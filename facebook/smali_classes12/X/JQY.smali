.class public final LX/JQY;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JQZ;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1Pn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;

.field public final synthetic c:LX/JQZ;


# direct methods
.method public constructor <init>(LX/JQZ;)V
    .locals 1

    .prologue
    .line 2691903
    iput-object p1, p0, LX/JQY;->c:LX/JQZ;

    .line 2691904
    move-object v0, p1

    .line 2691905
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2691906
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2691907
    const-string v0, "JobSearchHScrollComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2691908
    if-ne p0, p1, :cond_1

    .line 2691909
    :cond_0
    :goto_0
    return v0

    .line 2691910
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2691911
    goto :goto_0

    .line 2691912
    :cond_3
    check-cast p1, LX/JQY;

    .line 2691913
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2691914
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2691915
    if-eq v2, v3, :cond_0

    .line 2691916
    iget-object v2, p0, LX/JQY;->a:LX/1Pn;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JQY;->a:LX/1Pn;

    iget-object v3, p1, LX/JQY;->a:LX/1Pn;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2691917
    goto :goto_0

    .line 2691918
    :cond_5
    iget-object v2, p1, LX/JQY;->a:LX/1Pn;

    if-nez v2, :cond_4

    .line 2691919
    :cond_6
    iget-object v2, p0, LX/JQY;->b:Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/JQY;->b:Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;

    iget-object v3, p1, LX/JQY;->b:Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2691920
    goto :goto_0

    .line 2691921
    :cond_7
    iget-object v2, p1, LX/JQY;->b:Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
