.class public final LX/JNa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Z

.field public final synthetic c:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic d:LX/1Pf;

.field public final synthetic e:Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

.field public final synthetic f:Lcom/facebook/content/SecureContextHelper;

.field public final synthetic g:Lcom/facebook/feedplugins/games/QuicksilverContentAttachmentGroupPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/games/QuicksilverContentAttachmentGroupPartDefinition;ZZLcom/facebook/graphql/model/GraphQLStory;LX/1Pf;Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0

    .prologue
    .line 2686151
    iput-object p1, p0, LX/JNa;->g:Lcom/facebook/feedplugins/games/QuicksilverContentAttachmentGroupPartDefinition;

    iput-boolean p2, p0, LX/JNa;->a:Z

    iput-boolean p3, p0, LX/JNa;->b:Z

    iput-object p4, p0, LX/JNa;->c:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object p5, p0, LX/JNa;->d:LX/1Pf;

    iput-object p6, p0, LX/JNa;->e:Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    iput-object p7, p0, LX/JNa;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x6258f730    # 1.0005771E21f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 2686113
    iget-boolean v0, p0, LX/JNa;->a:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/JNa;->b:Z

    if-nez v0, :cond_1

    .line 2686114
    :cond_0
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "quicksilver_not_supported"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "quicksilver"

    .line 2686115
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2686116
    move-object v0, v0

    .line 2686117
    const-string v1, "supported_device"

    iget-boolean v2, p0, LX/JNa;->a:Z

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "passing_gk"

    iget-boolean v2, p0, LX/JNa;->b:Z

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2686118
    iget-object v1, p0, LX/JNa;->g:Lcom/facebook/feedplugins/games/QuicksilverContentAttachmentGroupPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/games/QuicksilverContentAttachmentGroupPartDefinition;->f:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2686119
    check-cast p1, Lcom/facebook/feedplugins/games/QuicksilverAttachmentContainerView;

    .line 2686120
    iget-object v0, p1, Lcom/facebook/feedplugins/games/QuicksilverAttachmentContainerView;->c:LX/7NI;

    .line 2686121
    iget-object v1, v0, LX/7NI;->e:Landroid/view/View;

    const/4 p1, 0x0

    invoke-virtual {v1, p1}, Landroid/view/View;->setVisibility(I)V

    .line 2686122
    const v0, 0x25ce31ab

    invoke-static {v4, v4, v0, v3}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2686123
    :goto_0
    return-void

    :cond_1
    move-object v0, p1

    .line 2686124
    check-cast v0, Lcom/facebook/feedplugins/games/QuicksilverAttachmentContainerView;

    invoke-virtual {v0}, Lcom/facebook/feedplugins/games/QuicksilverAttachmentContainerView;->f()V

    .line 2686125
    sget-object v2, LX/8TZ;->FB_FEED:LX/8TZ;

    .line 2686126
    const/4 v0, 0x0

    .line 2686127
    iget-object v1, p0, LX/JNa;->c:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_2

    .line 2686128
    iget-object v0, p0, LX/JNa;->c:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    .line 2686129
    new-instance v0, Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v1, v4}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    .line 2686130
    :cond_2
    sget-object v1, LX/8Ta;->Story:LX/8Ta;

    .line 2686131
    iget-object v4, p0, LX/JNa;->d:LX/1Pf;

    invoke-interface {v4}, LX/1Po;->c()LX/1PT;

    move-result-object v4

    invoke-interface {v4}, LX/1PT;->a()LX/1Qt;

    move-result-object v4

    sget-object v5, LX/1Qt;->GROUPS:LX/1Qt;

    if-ne v4, v5, :cond_5

    .line 2686132
    sget-object v2, LX/8TZ;->FB_GROUP_MALL:LX/8TZ;

    .line 2686133
    iget-object v1, p0, LX/JNa;->c:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/JNa;->c:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 2686134
    iget-object v0, p0, LX/JNa;->c:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v0

    .line 2686135
    :cond_3
    sget-object v1, LX/8Ta;->Group:LX/8Ta;

    .line 2686136
    :cond_4
    :goto_1
    new-instance v4, Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    const-class v6, Lcom/facebook/quicksilver/QuicksilverActivity;

    invoke-direct {v4, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2686137
    const-string v5, "source_context"

    invoke-virtual {v4, v5, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2686138
    const-string v1, "source"

    invoke-virtual {v4, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2686139
    const-string v1, "source_id"

    invoke-virtual {v4, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2686140
    const-string v0, "app_id"

    iget-object v1, p0, LX/JNa;->e:Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2686141
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2686142
    instance-of v1, v0, Landroid/app/Activity;

    if-eqz v1, :cond_7

    .line 2686143
    iget-object v1, p0, LX/JNa;->f:Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0x22b2

    check-cast v0, Landroid/app/Activity;

    invoke-interface {v1, v4, v2, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2686144
    :goto_2
    const v0, 0x6599623d

    invoke-static {v0, v3}, LX/02F;->a(II)V

    goto/16 :goto_0

    .line 2686145
    :cond_5
    iget-object v4, p0, LX/JNa;->d:LX/1Pf;

    invoke-interface {v4}, LX/1Po;->c()LX/1PT;

    move-result-object v4

    invoke-interface {v4}, LX/1PT;->a()LX/1Qt;

    move-result-object v4

    sget-object v5, LX/1Qt;->FEED:LX/1Qt;

    if-ne v4, v5, :cond_4

    iget-object v4, p0, LX/JNa;->c:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v4}, LX/14w;->r(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2686146
    sget-object v2, LX/8TZ;->FB_GROUP_NEWSFEED:LX/8TZ;

    .line 2686147
    iget-object v1, p0, LX/JNa;->c:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 2686148
    iget-object v0, p0, LX/JNa;->c:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v0

    .line 2686149
    :cond_6
    sget-object v1, LX/8Ta;->Group:LX/8Ta;

    goto :goto_1

    .line 2686150
    :cond_7
    iget-object v1, p0, LX/JNa;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v4, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_2
.end method
