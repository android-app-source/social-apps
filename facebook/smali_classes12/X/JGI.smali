.class public final LX/JGI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/JGB;


# instance fields
.field public final synthetic a:LX/5pC;

.field public final synthetic b:Lcom/facebook/react/bridge/Callback;

.field public final synthetic c:LX/JGL;


# direct methods
.method public constructor <init>(LX/JGL;LX/5pC;Lcom/facebook/react/bridge/Callback;)V
    .locals 0

    .prologue
    .line 2666961
    iput-object p1, p0, LX/JGI;->c:LX/JGL;

    iput-object p2, p0, LX/JGI;->a:LX/5pC;

    iput-object p3, p0, LX/JGI;->b:Lcom/facebook/react/bridge/Callback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/JG9;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2666931
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v2

    move v0, v1

    .line 2666932
    :goto_0
    iget-object v3, p0, LX/JGI;->a:LX/5pC;

    invoke-interface {v3}, LX/5pC;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 2666933
    iget-object v3, p0, LX/JGI;->a:LX/5pC;

    invoke-interface {v3, v0}, LX/5pC;->getInt(I)I

    move-result v3

    .line 2666934
    packed-switch v3, :pswitch_data_0

    .line 2666935
    new-instance v0, LX/5p9;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown sound property: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5p9;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2666936
    :pswitch_0
    sget-object v4, LX/JGL;->a:[Ljava/lang/String;

    aget-object v3, v4, v3

    invoke-virtual {p1}, LX/JG9;->d()D

    move-result-wide v4

    invoke-interface {v2, v3, v4, v5}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    .line 2666937
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2666938
    :pswitch_1
    sget-object v4, LX/JGL;->a:[Ljava/lang/String;

    aget-object v3, v4, v3

    .line 2666939
    iget-boolean v6, p1, LX/JG9;->b:Z

    if-eqz v6, :cond_2

    .line 2666940
    iget-object v6, p1, LX/JG9;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v6}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v6

    .line 2666941
    const/4 v7, -0x1

    if-ne v6, v7, :cond_1

    .line 2666942
    const-wide/high16 v6, -0x4010000000000000L    # -1.0

    .line 2666943
    :goto_2
    move-wide v4, v6

    .line 2666944
    invoke-interface {v2, v3, v4, v5}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    goto :goto_1

    .line 2666945
    :pswitch_2
    sget-object v4, LX/JGL;->a:[Ljava/lang/String;

    aget-object v3, v4, v3

    .line 2666946
    iget-boolean v4, p1, LX/JG9;->b:Z

    if-eqz v4, :cond_3

    .line 2666947
    iget-object v4, p1, LX/JG9;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v4

    .line 2666948
    :goto_3
    move v4, v4

    .line 2666949
    invoke-interface {v2, v3, v4}, LX/5pH;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_1

    .line 2666950
    :pswitch_3
    sget-object v4, LX/JGL;->a:[Ljava/lang/String;

    aget-object v3, v4, v3

    .line 2666951
    iget-boolean v4, p1, LX/JG9;->c:Z

    move v4, v4

    .line 2666952
    invoke-interface {v2, v3, v4}, LX/5pH;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_1

    .line 2666953
    :pswitch_4
    sget-object v4, LX/JGL;->a:[Ljava/lang/String;

    aget-object v3, v4, v3

    invoke-interface {v2, v3, v1}, LX/5pH;->putInt(Ljava/lang/String;I)V

    goto :goto_1

    .line 2666954
    :pswitch_5
    sget-object v4, LX/JGL;->a:[Ljava/lang/String;

    aget-object v3, v4, v3

    .line 2666955
    iget v4, p1, LX/JG9;->d:F

    move v4, v4

    .line 2666956
    float-to-double v4, v4

    invoke-interface {v2, v3, v4, v5}, LX/5pH;->putDouble(Ljava/lang/String;D)V

    goto :goto_1

    .line 2666957
    :cond_0
    iget-object v0, p0, LX/JGI;->b:Lcom/facebook/react/bridge/Callback;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v4, v3, v1

    const/4 v1, 0x1

    aput-object v2, v3, v1

    invoke-interface {v0, v3}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    .line 2666958
    return-void

    .line 2666959
    :cond_1
    invoke-static {v6}, LX/JG9;->a(I)D

    move-result-wide v6

    goto :goto_2

    .line 2666960
    :cond_2
    const-wide/16 v6, 0x0

    goto :goto_2

    :cond_3
    const/4 v4, 0x0

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
