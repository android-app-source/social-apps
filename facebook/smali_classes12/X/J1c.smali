.class public LX/J1c;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2639066
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2639067
    iput-object p1, p0, LX/J1c;->a:Landroid/content/Context;

    .line 2639068
    return-void
.end method

.method public static b(LX/0QB;)LX/J1c;
    .locals 2

    .prologue
    .line 2639069
    new-instance v1, LX/J1c;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LX/J1c;-><init>(Landroid/content/Context;)V

    .line 2639070
    return-object v1
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2639071
    iget-object v0, p0, LX/J1c;->a:Landroid/content/Context;

    const v1, 0x7f040063

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 2639072
    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2639073
    return-void
.end method
