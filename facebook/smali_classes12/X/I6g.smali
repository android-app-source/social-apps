.class public final LX/I6g;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/I6h;


# direct methods
.method public constructor <init>(LX/I6h;)V
    .locals 0

    .prologue
    .line 2537826
    iput-object p1, p0, LX/I6g;->a:LX/I6h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, 0x552b3afa

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2537827
    iget-object v0, p0, LX/I6g;->a:LX/I6h;

    iget-object v0, v0, LX/I6h;->o:LX/I6i;

    if-eqz v0, :cond_0

    .line 2537828
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, LX/I6g;->a:LX/I6h;

    iget-object v0, v0, LX/I6h;->m:LX/15i;

    iget-object v3, p0, LX/I6g;->a:LX/I6h;

    iget v3, v3, LX/I6h;->n:I

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v2, p0, LX/I6g;->a:LX/I6h;

    iget-object v2, v2, LX/I6h;->o:LX/I6i;

    const-class v4, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    invoke-virtual {v0, v3, v6, v4, v5}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    invoke-virtual {v2, v0}, LX/I6i;->a(Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;)V

    .line 2537829
    :cond_0
    const v0, -0x647bfcbc

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 2537830
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const v2, 0x5e072850

    invoke-static {v2, v1}, LX/02F;->a(II)V

    throw v0
.end method
