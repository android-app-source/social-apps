.class public abstract LX/JGT;
.super LX/JGS;
.source ""


# instance fields
.field public a:[F

.field public b:[F

.field public c:[F

.field public d:[F

.field public final e:Landroid/graphics/Rect;

.field private final f:LX/JGs;

.field private g:[LX/JGM;

.field private h:[LX/JGu;

.field private i:I

.field private j:I

.field public k:Landroid/util/SparseIntArray;

.field public final l:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final n:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final o:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/5r7;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/JGs;[LX/JGM;)V
    .locals 7

    .prologue
    .line 2667321
    invoke-direct {p0}, LX/JGS;-><init>()V

    .line 2667322
    sget-object v0, LX/JGM;->b:[LX/JGM;

    iput-object v0, p0, LX/JGT;->g:[LX/JGM;

    .line 2667323
    sget-object v0, LX/JH2;->a:[F

    iput-object v0, p0, LX/JGT;->a:[F

    .line 2667324
    sget-object v0, LX/JH2;->a:[F

    iput-object v0, p0, LX/JGT;->b:[F

    .line 2667325
    sget-object v0, LX/JGu;->a:[LX/JGu;

    iput-object v0, p0, LX/JGT;->h:[LX/JGu;

    .line 2667326
    sget-object v0, LX/JH2;->a:[F

    iput-object v0, p0, LX/JGT;->c:[F

    .line 2667327
    sget-object v0, LX/JH2;->a:[F

    iput-object v0, p0, LX/JGT;->d:[F

    .line 2667328
    sget-object v0, LX/JH2;->b:Landroid/util/SparseIntArray;

    iput-object v0, p0, LX/JGT;->k:Landroid/util/SparseIntArray;

    .line 2667329
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/JGT;->l:Landroid/util/SparseArray;

    .line 2667330
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/JGT;->e:Landroid/graphics/Rect;

    .line 2667331
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/JGT;->m:Landroid/util/SparseArray;

    .line 2667332
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/JGT;->n:Ljava/util/ArrayList;

    .line 2667333
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/JGT;->o:Ljava/util/ArrayList;

    .line 2667334
    iput-object p1, p0, LX/JGT;->f:LX/JGs;

    .line 2667335
    iget-object v3, p0, LX/JGT;->k:Landroid/util/SparseIntArray;

    iget-object v4, p0, LX/JGT;->a:[F

    iget-object v5, p0, LX/JGT;->b:[F

    const/4 v6, 0x1

    move-object v1, p0

    move-object v2, p2

    invoke-virtual/range {v1 .. v6}, LX/JGT;->a([LX/JGM;Landroid/util/SparseIntArray;[F[FZ)V

    .line 2667336
    invoke-virtual {p0}, LX/JGT;->b()Z

    .line 2667337
    return-void
.end method

.method private a(ILandroid/view/View;)V
    .locals 1

    .prologue
    .line 2667338
    iget-object v0, p0, LX/JGT;->l:Landroid/util/SparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2667339
    return-void
.end method

.method private b(I)V
    .locals 1

    .prologue
    .line 2667340
    iget-object v0, p0, LX/JGT;->l:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 2667341
    return-void
.end method

.method private static c(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 2667342
    invoke-virtual {p0}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    .line 2667343
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/animation/Animation;->hasEnded()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()V
    .locals 4

    .prologue
    .line 2667344
    const/4 v0, 0x0

    iget-object v1, p0, LX/JGT;->o:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_1

    .line 2667345
    iget-object v0, p0, LX/JGT;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5r7;

    move-object v1, v0

    .line 2667346
    check-cast v1, Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-direct {p0, v1}, LX/JGT;->d(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2667347
    invoke-interface {v0}, LX/5r7;->a()V

    .line 2667348
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2667349
    :cond_1
    return-void
.end method

.method private d(I)Z
    .locals 1

    .prologue
    .line 2667350
    iget-object v0, p0, LX/JGT;->l:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()V
    .locals 11

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 2667274
    iget-object v0, p0, LX/JGT;->f:LX/JGs;

    invoke-virtual {v0}, LX/JGs;->getChildCount()I

    move-result v2

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_2

    .line 2667275
    iget-object v4, p0, LX/JGT;->f:LX/JGs;

    invoke-virtual {v4, v0}, LX/JGs;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 2667276
    iget-object v5, p0, LX/JGT;->k:Landroid/util/SparseIntArray;

    invoke-virtual {v4}, Landroid/view/View;->getId()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/util/SparseIntArray;->get(I)I

    move-result v5

    .line 2667277
    invoke-direct {p0, v5}, LX/JGT;->e(I)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-static {v4}, LX/JGT;->c(Landroid/view/View;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2667278
    :cond_0
    iget-object v5, p0, LX/JGT;->n:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2667279
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2667280
    :cond_1
    iget-object v5, p0, LX/JGT;->m:Landroid/util/SparseArray;

    invoke-virtual {v5, v0, v4}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 2667281
    invoke-virtual {v4}, Landroid/view/View;->getId()I

    move-result v5

    invoke-direct {p0, v5, v4}, LX/JGT;->a(ILandroid/view/View;)V

    goto :goto_1

    .line 2667282
    :cond_2
    iget-object v0, p0, LX/JGT;->m:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v2

    .line 2667283
    const/4 v0, 0x2

    if-le v2, v0, :cond_3

    move v7, v3

    .line 2667284
    :goto_2
    if-eqz v7, :cond_c

    .line 2667285
    iget-object v0, p0, LX/JGT;->f:LX/JGs;

    invoke-virtual {v0}, LX/JGs;->detachAllViewsFromParent()V

    move v3, v1

    .line 2667286
    :goto_3
    if-ge v3, v2, :cond_4

    .line 2667287
    iget-object v4, p0, LX/JGT;->f:LX/JGs;

    iget-object v0, p0, LX/JGT;->m:Landroid/util/SparseArray;

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v4, v0}, LX/JGs;->b(Landroid/view/View;)V

    .line 2667288
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    :cond_3
    move v7, v1

    .line 2667289
    goto :goto_2

    .line 2667290
    :goto_4
    add-int/lit8 v2, v0, -0x1

    if-lez v0, :cond_4

    .line 2667291
    iget-object v0, p0, LX/JGT;->f:LX/JGs;

    iget-object v4, p0, LX/JGT;->m:Landroid/util/SparseArray;

    invoke-virtual {v4, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v4

    invoke-virtual {v0, v4, v3}, LX/JGs;->removeViewsInLayout(II)V

    move v0, v2

    goto :goto_4

    .line 2667292
    :cond_4
    iget-object v0, p0, LX/JGT;->m:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 2667293
    iget v0, p0, LX/JGT;->i:I

    .line 2667294
    iget-object v2, p0, LX/JGT;->n:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v8

    move v6, v1

    move v3, v1

    move v1, v0

    :goto_5
    if-ge v6, v8, :cond_8

    .line 2667295
    iget-object v0, p0, LX/JGT;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2667296
    iget-object v2, p0, LX/JGT;->k:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/util/SparseIntArray;->get(I)I

    move-result v9

    .line 2667297
    if-gt v1, v9, :cond_6

    move v4, v3

    move v5, v1

    .line 2667298
    :goto_6
    if-eq v5, v9, :cond_5

    .line 2667299
    iget-object v1, p0, LX/JGT;->g:[LX/JGM;

    aget-object v1, v1, v5

    instance-of v1, v1, LX/JGY;

    if-eqz v1, :cond_b

    .line 2667300
    iget-object v1, p0, LX/JGT;->g:[LX/JGM;

    aget-object v1, v1, v5

    check-cast v1, LX/JGY;

    .line 2667301
    iget-object v10, p0, LX/JGT;->f:LX/JGs;

    iget-object v2, p0, LX/JGT;->l:Landroid/util/SparseArray;

    iget v3, v1, LX/JGY;->d:I

    invoke-virtual {v2, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    .line 2667302
    move-object v2, v2

    .line 2667303
    check-cast v2, Landroid/view/View;

    add-int/lit8 v3, v4, 0x1

    invoke-virtual {v10, v2, v4}, LX/JGs;->a(Landroid/view/View;I)V

    .line 2667304
    iget v1, v1, LX/JGY;->d:I

    invoke-direct {p0, v1}, LX/JGT;->b(I)V

    move v1, v3

    .line 2667305
    :goto_7
    add-int/lit8 v2, v5, 0x1

    move v4, v1

    move v5, v2

    goto :goto_6

    .line 2667306
    :cond_5
    add-int/lit8 v1, v5, 0x1

    move v3, v4

    .line 2667307
    :cond_6
    if-eqz v7, :cond_7

    .line 2667308
    iget-object v2, p0, LX/JGT;->f:LX/JGs;

    invoke-virtual {v2, v0, v3}, LX/JGs;->b(Landroid/view/View;I)V

    .line 2667309
    :cond_7
    add-int/lit8 v3, v3, 0x1

    .line 2667310
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_5

    .line 2667311
    :cond_8
    iget-object v0, p0, LX/JGT;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    move v4, v1

    .line 2667312
    :goto_8
    iget v0, p0, LX/JGT;->j:I

    if-ge v4, v0, :cond_9

    .line 2667313
    iget-object v0, p0, LX/JGT;->g:[LX/JGM;

    aget-object v0, v0, v4

    instance-of v0, v0, LX/JGY;

    if-eqz v0, :cond_a

    .line 2667314
    iget-object v0, p0, LX/JGT;->g:[LX/JGM;

    aget-object v0, v0, v4

    move-object v1, v0

    check-cast v1, LX/JGY;

    .line 2667315
    iget-object v5, p0, LX/JGT;->f:LX/JGs;

    iget-object v0, p0, LX/JGT;->l:Landroid/util/SparseArray;

    iget v2, v1, LX/JGY;->d:I

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 2667316
    move-object v0, v0

    .line 2667317
    check-cast v0, Landroid/view/View;

    add-int/lit8 v2, v3, 0x1

    invoke-virtual {v5, v0, v3}, LX/JGs;->a(Landroid/view/View;I)V

    .line 2667318
    iget v0, v1, LX/JGY;->d:I

    invoke-direct {p0, v0}, LX/JGT;->b(I)V

    move v0, v2

    .line 2667319
    :goto_9
    add-int/lit8 v1, v4, 0x1

    move v3, v0

    move v4, v1

    goto :goto_8

    .line 2667320
    :cond_9
    return-void

    :cond_a
    move v0, v3

    goto :goto_9

    :cond_b
    move v1, v4

    goto :goto_7

    :cond_c
    move v0, v2

    goto/16 :goto_4
.end method

.method private e(I)Z
    .locals 1

    .prologue
    .line 2667351
    iget v0, p0, LX/JGT;->i:I

    if-gt v0, p1, :cond_0

    iget v0, p0, LX/JGT;->j:I

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public abstract a()I
.end method

.method public abstract a(FF)I
.end method

.method public abstract a(I)I
.end method

.method public final a(LX/JGd;[I[I)V
    .locals 10

    .prologue
    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 2667352
    iget-object v0, p0, LX/JGT;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2667353
    array-length v7, p2

    move v6, v3

    :goto_0
    if-ge v6, v7, :cond_b

    aget v0, p2, v6

    .line 2667354
    if-lez v0, :cond_4

    move v4, v5

    .line 2667355
    :goto_1
    if-nez v4, :cond_0

    .line 2667356
    neg-int v0, v0

    .line 2667357
    :cond_0
    iget-object v1, p0, LX/JGT;->k:Landroid/util/SparseIntArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseIntArray;->get(I)I

    move-result v8

    .line 2667358
    iget-object v0, p0, LX/JGT;->g:[LX/JGM;

    aget-object v0, v0, v8

    check-cast v0, LX/JGY;

    .line 2667359
    iget v1, v0, LX/JGY;->d:I

    invoke-virtual {p1, v1}, LX/JGd;->d(I)Landroid/view/View;

    move-result-object v2

    .line 2667360
    invoke-static {v2}, LX/JGS;->b(Landroid/view/View;)V

    .line 2667361
    instance-of v1, v2, LX/5r7;

    if-eqz v1, :cond_1

    move-object v1, v2

    check-cast v1, LX/5r7;

    invoke-interface {v1}, LX/5r7;->getRemoveClippedSubviews()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2667362
    iget-object v9, p0, LX/JGT;->o:Ljava/util/ArrayList;

    move-object v1, v2

    check-cast v1, LX/5r7;

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2667363
    :cond_1
    if-eqz v4, :cond_6

    .line 2667364
    iput-boolean v5, v0, LX/JGY;->e:Z

    .line 2667365
    invoke-static {v2}, LX/JGT;->c(Landroid/view/View;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-direct {p0, v8}, LX/JGT;->e(I)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2667366
    :cond_2
    iget-object v0, p0, LX/JGT;->f:LX/JGs;

    invoke-virtual {v0, v2}, LX/JGs;->c(Landroid/view/View;)V

    .line 2667367
    :cond_3
    :goto_2
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    :cond_4
    move v4, v3

    .line 2667368
    goto :goto_1

    .line 2667369
    :cond_5
    iget v0, v0, LX/JGY;->d:I

    invoke-direct {p0, v0, v2}, LX/JGT;->a(ILandroid/view/View;)V

    goto :goto_2

    .line 2667370
    :cond_6
    iget-boolean v1, v0, LX/JGY;->e:Z

    if-eqz v1, :cond_7

    .line 2667371
    iget v0, v0, LX/JGY;->d:I

    invoke-direct {p0, v0}, LX/JGT;->d(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2667372
    iget-object v0, p0, LX/JGT;->f:LX/JGs;

    invoke-virtual {v0, v2}, LX/JGs;->d(Landroid/view/View;)V

    goto :goto_2

    .line 2667373
    :cond_7
    iput-boolean v5, v0, LX/JGY;->e:Z

    .line 2667374
    invoke-static {v2}, LX/JGT;->c(Landroid/view/View;)Z

    move-result v1

    if-nez v1, :cond_8

    invoke-direct {p0, v8}, LX/JGT;->e(I)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 2667375
    :cond_8
    iget v1, v0, LX/JGY;->d:I

    .line 2667376
    iget-object v4, p0, LX/JGT;->l:Landroid/util/SparseArray;

    invoke-virtual {v4, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_e

    const/4 v4, 0x1

    :goto_3
    move v1, v4

    .line 2667377
    if-eqz v1, :cond_9

    .line 2667378
    iget-object v1, p0, LX/JGT;->f:LX/JGs;

    invoke-virtual {v1, v2}, LX/JGs;->c(Landroid/view/View;)V

    .line 2667379
    iget v0, v0, LX/JGY;->d:I

    invoke-direct {p0, v0}, LX/JGT;->b(I)V

    goto :goto_2

    .line 2667380
    :cond_9
    iget-object v0, p0, LX/JGT;->f:LX/JGs;

    invoke-virtual {v0, v2}, LX/JGs;->d(Landroid/view/View;)V

    goto :goto_2

    .line 2667381
    :cond_a
    iget v1, v0, LX/JGY;->d:I

    invoke-direct {p0, v1}, LX/JGT;->d(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2667382
    iget-object v1, p0, LX/JGT;->f:LX/JGs;

    invoke-virtual {v1, v2}, LX/JGs;->b(Landroid/view/View;)V

    .line 2667383
    iget v0, v0, LX/JGY;->d:I

    invoke-direct {p0, v0, v2}, LX/JGT;->a(ILandroid/view/View;)V

    goto :goto_2

    .line 2667384
    :cond_b
    array-length v1, p3

    move v0, v3

    :goto_4
    if-ge v0, v1, :cond_d

    aget v2, p3, v0

    .line 2667385
    invoke-virtual {p1, v2}, LX/JGd;->d(I)Landroid/view/View;

    move-result-object v3

    .line 2667386
    invoke-virtual {v3}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    if-eqz v4, :cond_c

    .line 2667387
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Trying to remove view not owned by FlatViewGroup"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2667388
    :cond_c
    iget-object v4, p0, LX/JGT;->f:LX/JGs;

    invoke-virtual {v4, v3}, LX/JGs;->b(Landroid/view/View;)V

    .line 2667389
    invoke-direct {p0, v2}, LX/JGT;->b(I)V

    .line 2667390
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 2667391
    :cond_d
    return-void

    :cond_e
    const/4 v4, 0x0

    goto :goto_3
.end method

.method public final a(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 2667203
    iget v1, p0, LX/JGT;->i:I

    .line 2667204
    iget-object v0, p0, LX/JGT;->f:LX/JGs;

    invoke-virtual {v0}, LX/JGs;->getChildCount()I

    move-result v3

    .line 2667205
    const/4 v0, 0x0

    move v2, v0

    move v0, v1

    :goto_0
    if-ge v2, v3, :cond_3

    .line 2667206
    iget-object v1, p0, LX/JGT;->k:Landroid/util/SparseIntArray;

    iget-object v4, p0, LX/JGT;->f:LX/JGs;

    invoke-virtual {v4, v2}, LX/JGs;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getId()I

    move-result v4

    invoke-virtual {v1, v4}, Landroid/util/SparseIntArray;->get(I)I

    move-result v4

    .line 2667207
    iget v1, p0, LX/JGT;->j:I

    if-ge v1, v4, :cond_0

    .line 2667208
    :goto_1
    iget v1, p0, LX/JGT;->j:I

    if-ge v0, v1, :cond_2

    .line 2667209
    iget-object v5, p0, LX/JGT;->g:[LX/JGM;

    add-int/lit8 v1, v0, 0x1

    aget-object v0, v5, v0

    iget-object v5, p0, LX/JGT;->f:LX/JGs;

    invoke-virtual {v0, v5, p1}, LX/JGM;->a(LX/JGs;Landroid/graphics/Canvas;)V

    move v0, v1

    goto :goto_1

    .line 2667210
    :cond_0
    if-gt v0, v4, :cond_2

    .line 2667211
    :goto_2
    if-ge v0, v4, :cond_1

    .line 2667212
    iget-object v5, p0, LX/JGT;->g:[LX/JGM;

    add-int/lit8 v1, v0, 0x1

    aget-object v0, v5, v0

    iget-object v5, p0, LX/JGT;->f:LX/JGs;

    invoke-virtual {v0, v5, p1}, LX/JGM;->a(LX/JGs;Landroid/graphics/Canvas;)V

    move v0, v1

    goto :goto_2

    .line 2667213
    :cond_1
    add-int/lit8 v0, v0, 0x1

    .line 2667214
    :cond_2
    iget-object v1, p0, LX/JGT;->g:[LX/JGM;

    aget-object v1, v1, v4

    iget-object v4, p0, LX/JGT;->f:LX/JGs;

    invoke-virtual {v1, v4, p1}, LX/JGM;->a(LX/JGs;Landroid/graphics/Canvas;)V

    .line 2667215
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2667216
    :cond_3
    :goto_3
    iget v1, p0, LX/JGT;->j:I

    if-ge v0, v1, :cond_4

    .line 2667217
    iget-object v2, p0, LX/JGT;->g:[LX/JGM;

    add-int/lit8 v1, v0, 0x1

    aget-object v0, v2, v0

    iget-object v2, p0, LX/JGT;->f:LX/JGs;

    invoke-virtual {v0, v2, p1}, LX/JGM;->a(LX/JGs;Landroid/graphics/Canvas;)V

    move v0, v1

    goto :goto_3

    .line 2667218
    :cond_4
    return-void
.end method

.method public final a(Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 2667219
    iget-object v0, p0, LX/JGT;->e:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 2667220
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2667221
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    invoke-direct {p0, v0}, LX/JGT;->b(I)V

    .line 2667222
    iget-object v0, p0, LX/JGT;->f:LX/JGs;

    invoke-virtual {v0, p1}, LX/JGs;->b(Landroid/view/View;)V

    .line 2667223
    return-void
.end method

.method public final a([LX/JGM;Landroid/util/SparseIntArray;[F[FZ)V
    .locals 2

    .prologue
    .line 2667224
    iput-object p1, p0, LX/JGT;->g:[LX/JGM;

    .line 2667225
    iput-object p3, p0, LX/JGT;->a:[F

    .line 2667226
    iput-object p4, p0, LX/JGT;->b:[F

    .line 2667227
    iput-object p2, p0, LX/JGT;->k:Landroid/util/SparseIntArray;

    .line 2667228
    iget-object v0, p0, LX/JGT;->e:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iget-object v1, p0, LX/JGT;->e:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    if-eq v0, v1, :cond_0

    .line 2667229
    invoke-virtual {p0}, LX/JGT;->a()I

    move-result v0

    iput v0, p0, LX/JGT;->i:I

    .line 2667230
    iget v0, p0, LX/JGT;->i:I

    invoke-virtual {p0, v0}, LX/JGT;->a(I)I

    move-result v0

    iput v0, p0, LX/JGT;->j:I

    .line 2667231
    if-nez p5, :cond_0

    .line 2667232
    invoke-direct {p0}, LX/JGT;->e()V

    .line 2667233
    :cond_0
    return-void
.end method

.method public final a([LX/JGu;[F[F)V
    .locals 0

    .prologue
    .line 2667234
    iput-object p1, p0, LX/JGT;->h:[LX/JGu;

    .line 2667235
    iput-object p2, p0, LX/JGT;->c:[F

    .line 2667236
    iput-object p3, p0, LX/JGT;->d:[F

    .line 2667237
    return-void
.end method

.method public abstract a(IFF)Z
.end method

.method public final b(FF)LX/JGu;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2667238
    invoke-virtual {p0, p1, p2}, LX/JGT;->a(FF)I

    move-result v0

    .line 2667239
    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-lez v0, :cond_1

    .line 2667240
    iget-object v0, p0, LX/JGT;->h:[LX/JGu;

    aget-object v0, v0, v1

    .line 2667241
    iget-boolean v2, v0, LX/JGu;->d:Z

    if-eqz v2, :cond_2

    .line 2667242
    invoke-virtual {p0, v1, p1, p2}, LX/JGT;->a(IFF)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2667243
    invoke-virtual {v0, p1, p2}, LX/JGu;->a(FF)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2667244
    :goto_1
    return-object v0

    :cond_0
    move v0, v1

    .line 2667245
    goto :goto_0

    .line 2667246
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final b(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    .line 2667247
    iget-object v3, p0, LX/JGT;->g:[LX/JGM;

    array-length v4, v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v1, v3, v2

    .line 2667248
    instance-of v0, v1, LX/JGY;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 2667249
    check-cast v0, LX/JGY;

    iget v0, v0, LX/JGY;->d:I

    invoke-direct {p0, v0}, LX/JGT;->d(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2667250
    :cond_0
    iget-object v0, p0, LX/JGT;->f:LX/JGs;

    invoke-virtual {v1, v0, p1}, LX/JGM;->b(LX/JGs;Landroid/graphics/Canvas;)V

    .line 2667251
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2667252
    :cond_2
    return-void
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2667253
    iget-object v1, p0, LX/JGT;->f:LX/JGs;

    iget-object v2, p0, LX/JGT;->e:Landroid/graphics/Rect;

    invoke-static {v1, v2}, LX/5r8;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 2667254
    iget-object v1, p0, LX/JGT;->f:LX/JGs;

    invoke-virtual {v1}, LX/JGs;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/JGT;->e:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, LX/JGT;->e:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    if-ne v1, v2, :cond_1

    .line 2667255
    :cond_0
    :goto_0
    return v0

    .line 2667256
    :cond_1
    invoke-virtual {p0}, LX/JGT;->a()I

    move-result v1

    .line 2667257
    invoke-virtual {p0, v1}, LX/JGT;->a(I)I

    move-result v2

    .line 2667258
    iget v3, p0, LX/JGT;->i:I

    if-gt v3, v1, :cond_2

    iget v3, p0, LX/JGT;->j:I

    if-gt v2, v3, :cond_2

    .line 2667259
    invoke-direct {p0}, LX/JGT;->d()V

    goto :goto_0

    .line 2667260
    :cond_2
    iput v1, p0, LX/JGT;->i:I

    .line 2667261
    iput v2, p0, LX/JGT;->j:I

    .line 2667262
    invoke-direct {p0}, LX/JGT;->e()V

    .line 2667263
    invoke-direct {p0}, LX/JGT;->d()V

    .line 2667264
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final c(FF)LX/JGu;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2667265
    invoke-virtual {p0, p1, p2}, LX/JGT;->a(FF)I

    move-result v0

    .line 2667266
    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-lez v0, :cond_1

    .line 2667267
    iget-object v0, p0, LX/JGT;->h:[LX/JGu;

    aget-object v0, v0, v1

    .line 2667268
    invoke-virtual {p0, v1, p1, p2}, LX/JGT;->a(IFF)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2667269
    invoke-virtual {v0, p1, p2}, LX/JGu;->a(FF)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2667270
    :goto_1
    return-object v0

    :cond_0
    move v0, v1

    .line 2667271
    goto :goto_0

    .line 2667272
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final c()Landroid/util/SparseArray;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2667273
    iget-object v0, p0, LX/JGT;->l:Landroid/util/SparseArray;

    return-object v0
.end method
