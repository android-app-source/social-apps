.class public final LX/HnA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "Ljava/net/Socket;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;)V
    .locals 0

    .prologue
    .line 2501404
    iput-object p1, p0, LX/HnA;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Ljava/net/Socket;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .param p1    # Ljava/net/Socket;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/net/Socket;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2501405
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Tp;->b(Z)V

    .line 2501406
    iget-object v0, p0, LX/HnA;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    new-instance v1, LX/Hm4;

    invoke-direct {v1, p1}, LX/Hm4;-><init>(Ljava/net/Socket;)V

    .line 2501407
    iput-object v1, v0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->x:LX/Hm4;

    .line 2501408
    iget-object v0, p0, LX/HnA;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v1, p0, LX/HnA;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v1, v1, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->p:LX/Hn0;

    iget-object v2, p0, LX/HnA;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v2, v2, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->x:LX/Hm4;

    .line 2501409
    new-instance p1, LX/Hmz;

    invoke-static {v1}, LX/0UA;->b(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    invoke-direct {p1, v2, v3}, LX/Hmz;-><init>(LX/Hm4;Ljava/util/concurrent/ExecutorService;)V

    .line 2501410
    move-object v1, p1

    .line 2501411
    iput-object v1, v0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->y:LX/Hmz;

    .line 2501412
    iget-object v0, p0, LX/HnA;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v0, v0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->B:LX/HnH;

    invoke-virtual {v0}, LX/HnH;->c()V

    .line 2501413
    iget-object v0, p0, LX/HnA;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v0, v0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->y:LX/Hmz;

    .line 2501414
    iget-object v1, v0, LX/Hmz;->a:LX/0TD;

    new-instance v2, LX/Hmr;

    invoke-direct {v2, v0}, LX/Hmr;-><init>(LX/Hmz;)V

    invoke-interface {v1, v2}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object v0, v1

    .line 2501415
    return-object v0

    .line 2501416
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2501417
    check-cast p1, Ljava/net/Socket;

    invoke-direct {p0, p1}, LX/HnA;->a(Ljava/net/Socket;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
