.class public final LX/Hcc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Landroid/view/View$OnClickListener;

.field public final synthetic b:LX/2km;

.field public final synthetic c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

.field public final synthetic d:Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;Landroid/view/View$OnClickListener;LX/2km;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V
    .locals 0

    .prologue
    .line 2487161
    iput-object p1, p0, LX/Hcc;->d:Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;

    iput-object p2, p0, LX/Hcc;->a:Landroid/view/View$OnClickListener;

    iput-object p3, p0, LX/Hcc;->b:LX/2km;

    iput-object p4, p0, LX/Hcc;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 14

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x48464ee1

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2487162
    iget-object v1, p0, LX/Hcc;->a:Landroid/view/View$OnClickListener;

    invoke-interface {v1, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 2487163
    iget-object v1, p0, LX/Hcc;->d:Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;

    iget-object v2, p0, LX/Hcc;->b:LX/2km;

    iget-object v3, p0, LX/Hcc;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2487164
    iget-object v5, v1, Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;->i:LX/E1f;

    .line 2487165
    iget-object v6, v3, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v6, v6

    .line 2487166
    invoke-interface {v6}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v6

    move-object v7, v2

    check-cast v7, LX/1Pn;

    invoke-interface {v7}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v7

    const/4 v8, 0x0

    move-object v9, v2

    check-cast v9, LX/2kp;

    invoke-interface {v9}, LX/2kp;->t()LX/2jY;

    move-result-object v9

    .line 2487167
    iget-object v10, v9, LX/2jY;->a:Ljava/lang/String;

    move-object v9, v10

    .line 2487168
    move-object v10, v2

    check-cast v10, LX/2kp;

    invoke-interface {v10}, LX/2kp;->t()LX/2jY;

    move-result-object v10

    .line 2487169
    iget-object v11, v10, LX/2jY;->b:Ljava/lang/String;

    move-object v10, v11

    .line 2487170
    iget-object v11, v3, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v11, v11

    .line 2487171
    iget-object v12, v3, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v12, v12

    .line 2487172
    move-object v13, v2

    check-cast v13, LX/2kn;

    invoke-interface {v13}, LX/2kn;->v()Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    move-result-object v13

    invoke-virtual/range {v5 .. v13}, LX/E1f;->a(LX/9rk;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;)LX/Cfl;

    move-result-object v5

    .line 2487173
    iget-object v6, v3, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v6, v6

    .line 2487174
    iget-object v7, v3, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v7, v7

    .line 2487175
    invoke-interface {v2, v6, v7, v5}, LX/2km;->a(Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V

    .line 2487176
    const v1, 0x1ec60470

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
