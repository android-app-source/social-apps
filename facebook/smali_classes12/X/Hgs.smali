.class public final enum LX/Hgs;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Hgs;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Hgs;

.field public static final enum GROUP:LX/Hgs;

.field public static final enum NEWSFEED:LX/Hgs;


# instance fields
.field public final layout:I
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2494706
    new-instance v0, LX/Hgs;

    const-string v1, "NEWSFEED"

    const v2, 0x7f030ff4

    invoke-direct {v0, v1, v3, v2}, LX/Hgs;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Hgs;->NEWSFEED:LX/Hgs;

    .line 2494707
    new-instance v0, LX/Hgs;

    const-string v1, "GROUP"

    const v2, 0x7f030ff3

    invoke-direct {v0, v1, v4, v2}, LX/Hgs;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Hgs;->GROUP:LX/Hgs;

    .line 2494708
    const/4 v0, 0x2

    new-array v0, v0, [LX/Hgs;

    sget-object v1, LX/Hgs;->NEWSFEED:LX/Hgs;

    aput-object v1, v0, v3

    sget-object v1, LX/Hgs;->GROUP:LX/Hgs;

    aput-object v1, v0, v4

    sput-object v0, LX/Hgs;->$VALUES:[LX/Hgs;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3    # I
        .annotation build Landroid/support/annotation/LayoutRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 2494709
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2494710
    iput p3, p0, LX/Hgs;->layout:I

    .line 2494711
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Hgs;
    .locals 1

    .prologue
    .line 2494712
    const-class v0, LX/Hgs;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Hgs;

    return-object v0
.end method

.method public static values()[LX/Hgs;
    .locals 1

    .prologue
    .line 2494713
    sget-object v0, LX/Hgs;->$VALUES:[LX/Hgs;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Hgs;

    return-object v0
.end method
