.class public LX/HjM;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/Hjs;


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:Ljava/lang/String;

.field public final d:I

.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/HjF;",
            ">;"
        }
    .end annotation
.end field

.field public f:I

.field public g:Lcom/facebook/neko/util/MainActivityFragment;

.field public h:LX/HkE;

.field public i:Z

.field public j:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    sget-object v0, LX/Hjs;->a:LX/Hjs;

    sput-object v0, LX/HjM;->a:LX/Hjs;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LX/HjM;->b:Landroid/content/Context;

    iput-object p2, p0, LX/HjM;->c:Ljava/lang/String;

    invoke-static {p3, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, LX/HjM;->d:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/HjM;->e:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, LX/HjM;->f:I

    iput-boolean v1, p0, LX/HjM;->j:Z

    iput-boolean v1, p0, LX/HjM;->i:Z

    return-void
.end method


# virtual methods
.method public final b()I
    .locals 1

    iget-object v0, p0, LX/HjM;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
