.class public final LX/JOB;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JOC;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;

.field public b:I

.field public c:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;

.field public final synthetic d:LX/JOC;


# direct methods
.method public constructor <init>(LX/JOC;)V
    .locals 1

    .prologue
    .line 2687037
    iput-object p1, p0, LX/JOB;->d:LX/JOC;

    .line 2687038
    move-object v0, p1

    .line 2687039
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2687040
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2687041
    const-string v0, "ConnectWithFacebookProfilePictureComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2687042
    if-ne p0, p1, :cond_1

    .line 2687043
    :cond_0
    :goto_0
    return v0

    .line 2687044
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2687045
    goto :goto_0

    .line 2687046
    :cond_3
    check-cast p1, LX/JOB;

    .line 2687047
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2687048
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2687049
    if-eq v2, v3, :cond_0

    .line 2687050
    iget-object v2, p0, LX/JOB;->a:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JOB;->a:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;

    iget-object v3, p1, LX/JOB;->a:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2687051
    goto :goto_0

    .line 2687052
    :cond_5
    iget-object v2, p1, LX/JOB;->a:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;

    if-nez v2, :cond_4

    .line 2687053
    :cond_6
    iget v2, p0, LX/JOB;->b:I

    iget v3, p1, LX/JOB;->b:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 2687054
    goto :goto_0

    .line 2687055
    :cond_7
    iget-object v2, p0, LX/JOB;->c:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/JOB;->c:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;

    iget-object v3, p1, LX/JOB;->c:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2687056
    goto :goto_0

    .line 2687057
    :cond_8
    iget-object v2, p1, LX/JOB;->c:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
