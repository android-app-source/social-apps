.class public final LX/Ikd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:LX/Ike;


# direct methods
.method public constructor <init>(LX/Ike;)V
    .locals 0

    .prologue
    .line 2606911
    iput-object p1, p0, LX/Ikd;->a:LX/Ike;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 2606912
    if-nez p2, :cond_2

    sget-object v0, LX/Ih7;->CAMERA:LX/Ih7;

    .line 2606913
    :goto_0
    new-instance v2, LX/Ih6;

    invoke-direct {v2}, LX/Ih6;-><init>()V

    move-object v2, v2

    .line 2606914
    sget-object p1, LX/2MK;->PHOTO:LX/2MK;

    invoke-static {p1}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object p1

    .line 2606915
    iput-object p1, v2, LX/Ih6;->d:Ljava/util/Set;

    .line 2606916
    move-object v2, v2

    .line 2606917
    iput-object v0, v2, LX/Ih6;->a:LX/Ih7;

    .line 2606918
    move-object v2, v2

    .line 2606919
    new-instance p1, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;

    invoke-direct {p1, v2}, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;-><init>(LX/Ih6;)V

    move-object v2, p1

    .line 2606920
    const/4 v1, 0x0

    const/4 p2, 0x1

    .line 2606921
    iget-object p1, v2, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->a:LX/Ih7;

    sget-object v0, LX/Ih7;->CAMERA:LX/Ih7;

    if-ne p1, v0, :cond_0

    .line 2606922
    iget-object p1, v2, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->d:LX/0Rf;

    invoke-virtual {p1}, LX/0Rf;->size()I

    move-result p1

    if-lez p1, :cond_3

    move p1, p2

    :goto_1
    invoke-static {p1}, LX/0PB;->checkArgument(Z)V

    .line 2606923
    iget-object p1, v2, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->d:LX/0Rf;

    sget-object v0, LX/2MK;->PHOTO:LX/2MK;

    invoke-virtual {p1, v0}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result p1

    invoke-static {p1}, LX/0PB;->checkArgument(Z)V

    .line 2606924
    :cond_0
    iget-object p1, v2, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->b:Lcom/facebook/messaging/media/mediapicker/dialog/CropImageParams;

    if-eqz p1, :cond_1

    .line 2606925
    iget-object p1, v2, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->d:LX/0Rf;

    invoke-virtual {p1}, LX/0Rf;->size()I

    move-result p1

    if-ne p1, p2, :cond_4

    move p1, p2

    :goto_2
    invoke-static {p1}, LX/0PB;->checkArgument(Z)V

    .line 2606926
    iget-object p1, v2, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->d:LX/0Rf;

    sget-object v0, LX/2MK;->PHOTO:LX/2MK;

    invoke-virtual {p1, v0}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result p1

    invoke-static {p1}, LX/0PB;->checkArgument(Z)V

    .line 2606927
    iget-boolean p1, v2, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogParams;->c:Z

    if-nez p1, :cond_5

    :goto_3
    invoke-static {p2}, LX/0PB;->checkArgument(Z)V

    .line 2606928
    :cond_1
    new-instance p1, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;

    invoke-direct {p1}, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;-><init>()V

    .line 2606929
    new-instance p2, Landroid/os/Bundle;

    invoke-direct {p2}, Landroid/os/Bundle;-><init>()V

    .line 2606930
    const-string v1, "p"

    invoke-virtual {p2, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2606931
    invoke-virtual {p1, p2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2606932
    move-object v2, p1

    .line 2606933
    move-object v2, v2

    .line 2606934
    move-object v0, v2

    .line 2606935
    iget-object v1, p0, LX/Ikd;->a:LX/Ike;

    iget-object v1, v1, LX/Ike;->a:Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;

    .line 2606936
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v2

    const-string p0, "receipt_image_media_picker_fragment"

    invoke-virtual {v0, v2, p0}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2606937
    return-void

    .line 2606938
    :cond_2
    sget-object v0, LX/Ih7;->GALLERY:LX/Ih7;

    goto :goto_0

    :cond_3
    move p1, v1

    .line 2606939
    goto :goto_1

    :cond_4
    move p1, v1

    .line 2606940
    goto :goto_2

    :cond_5
    move p2, v1

    .line 2606941
    goto :goto_3
.end method
