.class public LX/J0E;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/payments/p2p/service/model/cards/EditPaymentCardParams;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2636680
    const-class v0, LX/J0E;

    sput-object v0, LX/J0E;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2636681
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2636682
    return-void
.end method

.method public static a(LX/0QB;)LX/J0E;
    .locals 1

    .prologue
    .line 2636683
    new-instance v0, LX/J0E;

    invoke-direct {v0}, LX/J0E;-><init>()V

    .line 2636684
    move-object v0, v0

    .line 2636685
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 5

    .prologue
    .line 2636686
    check-cast p1, Lcom/facebook/payments/p2p/service/model/cards/EditPaymentCardParams;

    .line 2636687
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 2636688
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "month"

    .line 2636689
    iget v3, p1, Lcom/facebook/payments/p2p/service/model/cards/EditPaymentCardParams;->b:I

    move v3, v3

    .line 2636690
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2636691
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "year"

    .line 2636692
    iget v3, p1, Lcom/facebook/payments/p2p/service/model/cards/EditPaymentCardParams;->c:I

    move v3, v3

    .line 2636693
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2636694
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "csc"

    .line 2636695
    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/cards/EditPaymentCardParams;->d:Ljava/lang/String;

    move-object v3, v3

    .line 2636696
    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2636697
    iget-object v0, p1, Lcom/facebook/payments/p2p/service/model/cards/EditPaymentCardParams;->e:Ljava/lang/String;

    move-object v0, v0

    .line 2636698
    if-eqz v0, :cond_0

    .line 2636699
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "zip"

    .line 2636700
    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/cards/EditPaymentCardParams;->e:Ljava/lang/String;

    move-object v3, v3

    .line 2636701
    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2636702
    :cond_0
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "mobile_eligibility"

    .line 2636703
    iget-boolean v0, p1, Lcom/facebook/payments/p2p/service/model/cards/EditPaymentCardParams;->f:Z

    move v0, v0

    .line 2636704
    if-eqz v0, :cond_1

    const-string v0, "1"

    :goto_0
    invoke-direct {v2, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2636705
    const-string v0, "/p2p_%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 2636706
    iget-object v4, p1, Lcom/facebook/payments/p2p/service/model/cards/EditPaymentCardParams;->a:Ljava/lang/String;

    move-object v4, v4

    .line 2636707
    aput-object v4, v2, v3

    invoke-static {v0, v2}, LX/73t;->a(Ljava/lang/String;[Ljava/lang/Object;)LX/14O;

    move-result-object v0

    const-string v2, "p2p_edit_cards"

    .line 2636708
    iput-object v2, v0, LX/14O;->b:Ljava/lang/String;

    .line 2636709
    move-object v0, v0

    .line 2636710
    const-string v2, "POST"

    .line 2636711
    iput-object v2, v0, LX/14O;->c:Ljava/lang/String;

    .line 2636712
    move-object v0, v0

    .line 2636713
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 2636714
    move-object v0, v0

    .line 2636715
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2636716
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2636717
    move-object v0, v0

    .line 2636718
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    .line 2636719
    :cond_1
    const-string v0, "0"

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2636720
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2636721
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 2636722
    const-string v1, "credential_id"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
