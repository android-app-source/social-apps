.class public final LX/Id5;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)V
    .locals 0

    .prologue
    .line 2596051
    iput-object p1, p0, LX/Id5;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2596052
    iget-object v0, p0, LX/Id5;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    .line 2596053
    iput-object p1, v0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->I:Ljava/lang/String;

    .line 2596054
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2596055
    iget-object v0, p0, LX/Id5;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    invoke-static {v0, p1}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->a$redex0(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;Ljava/lang/String;)V

    .line 2596056
    :goto_0
    return-void

    .line 2596057
    :cond_0
    iget-object v0, p0, LX/Id5;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v0, v0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->p:LX/0Uh;

    const/16 v1, 0x1b6

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2596058
    iget-object v0, p0, LX/Id5;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v0, v0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->b:LX/Ic9;

    new-instance v1, LX/Id4;

    invoke-direct {v1, p0}, LX/Id4;-><init>(LX/Id5;)V

    .line 2596059
    iget-object v2, v0, LX/Ic9;->c:LX/70D;

    sget-object v3, LX/6xg;->MOR_MESSENGER_COMMERCE:LX/6xg;

    invoke-static {v3}, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->a(LX/6xg;)LX/70C;

    move-result-object v3

    invoke-virtual {v3}, LX/70C;->a()Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/6u3;->b(Landroid/os/Parcelable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 2596060
    iget-object v3, v0, LX/Ic9;->d:LX/1Ck;

    const-string p0, "task_key_fetch_last_used_payment_info"

    .line 2596061
    new-instance p1, LX/Ic7;

    invoke-direct {p1, v0, v1}, LX/Ic7;-><init>(LX/Ic9;LX/Id4;)V

    move-object p1, p1

    .line 2596062
    invoke-virtual {v3, p0, v2, p1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2596063
    goto :goto_0

    .line 2596064
    :cond_1
    iget-object v0, p0, LX/Id5;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v0, v0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->F:Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;

    iget-object v1, p0, LX/Id5;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    const v2, 0x7f082d7f

    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->setActionText(Ljava/lang/String;)V

    goto :goto_0
.end method
