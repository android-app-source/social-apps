.class public final enum LX/ITI;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ITI;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ITI;

.field public static final enum GROUP_ADD_TO_FAVORITES:LX/ITI;

.field public static final enum GROUP_FOLLOW_UNFOLLOW:LX/ITI;

.field public static final enum GROUP_REMOVE_FROM_FAVORITES:LX/ITI;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2579092
    new-instance v0, LX/ITI;

    const-string v1, "GROUP_ADD_TO_FAVORITES"

    invoke-direct {v0, v1, v2}, LX/ITI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ITI;->GROUP_ADD_TO_FAVORITES:LX/ITI;

    .line 2579093
    new-instance v0, LX/ITI;

    const-string v1, "GROUP_REMOVE_FROM_FAVORITES"

    invoke-direct {v0, v1, v3}, LX/ITI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ITI;->GROUP_REMOVE_FROM_FAVORITES:LX/ITI;

    .line 2579094
    new-instance v0, LX/ITI;

    const-string v1, "GROUP_FOLLOW_UNFOLLOW"

    invoke-direct {v0, v1, v4}, LX/ITI;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ITI;->GROUP_FOLLOW_UNFOLLOW:LX/ITI;

    .line 2579095
    const/4 v0, 0x3

    new-array v0, v0, [LX/ITI;

    sget-object v1, LX/ITI;->GROUP_ADD_TO_FAVORITES:LX/ITI;

    aput-object v1, v0, v2

    sget-object v1, LX/ITI;->GROUP_REMOVE_FROM_FAVORITES:LX/ITI;

    aput-object v1, v0, v3

    sget-object v1, LX/ITI;->GROUP_FOLLOW_UNFOLLOW:LX/ITI;

    aput-object v1, v0, v4

    sput-object v0, LX/ITI;->$VALUES:[LX/ITI;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2579096
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/ITI;
    .locals 1

    .prologue
    .line 2579097
    const-class v0, LX/ITI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ITI;

    return-object v0
.end method

.method public static values()[LX/ITI;
    .locals 1

    .prologue
    .line 2579098
    sget-object v0, LX/ITI;->$VALUES:[LX/ITI;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ITI;

    return-object v0
.end method
