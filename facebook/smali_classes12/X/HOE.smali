.class public final LX/HOE;
.super LX/63W;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;)V
    .locals 0

    .prologue
    .line 2459768
    iput-object p1, p0, LX/HOE;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;

    invoke-direct {p0}, LX/63W;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 8

    .prologue
    .line 2459769
    iget-object v0, p0, LX/HOE;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    const-string v1, "page_action_channel_replace_in_ordering_mutation"

    invoke-virtual {v0, v1}, LX/1Ck;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2459770
    :cond_0
    :goto_0
    return-void

    .line 2459771
    :cond_1
    iget-object v0, p0, LX/HOE;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->k:LX/CYF;

    sget-object v1, LX/CYF;->CREATE_ACTION:LX/CYF;

    if-ne v0, v1, :cond_0

    .line 2459772
    iget-object v0, p0, LX/HOE;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->i:LX/CYE;

    .line 2459773
    iget-object v1, v0, LX/CYE;->mActionType:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-object v0, v1

    .line 2459774
    if-eqz v0, :cond_2

    .line 2459775
    iget-object v0, p0, LX/HOE;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    const-string v7, "page_action_channel_replace_in_ordering_mutation"

    iget-object v1, p0, LX/HOE;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HNW;

    iget-object v2, p0, LX/HOE;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;

    iget-wide v2, v2, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->h:J

    iget-object v4, p0, LX/HOE;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;

    iget-object v4, v4, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->i:LX/CYE;

    iget-object v5, p0, LX/HOE;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;

    iget-object v5, v5, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->j:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    invoke-virtual {v5}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v5

    iget-object v6, p0, LX/HOE;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;

    iget-object v6, v6, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->j:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    invoke-virtual {v6}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->l()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v1 .. v6}, LX/HNW;->b(JLX/CYE;Lcom/facebook/graphql/enums/GraphQLPageActionType;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, LX/HOD;

    invoke-direct {v2, p0}, LX/HOD;-><init>(LX/HOE;)V

    invoke-virtual {v0, v7, v1, v2}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0

    .line 2459776
    :cond_2
    iget-object v0, p0, LX/HOE;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;

    invoke-static {v0}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->e(Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;)V

    goto :goto_0
.end method
