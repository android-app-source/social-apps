.class public final enum LX/IJC;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IJC;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IJC;

.field public static final enum SEND_INVITE:LX/IJC;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2563190
    new-instance v0, LX/IJC;

    const-string v1, "SEND_INVITE"

    invoke-direct {v0, v1, v2}, LX/IJC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IJC;->SEND_INVITE:LX/IJC;

    .line 2563191
    const/4 v0, 0x1

    new-array v0, v0, [LX/IJC;

    sget-object v1, LX/IJC;->SEND_INVITE:LX/IJC;

    aput-object v1, v0, v2

    sput-object v0, LX/IJC;->$VALUES:[LX/IJC;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2563192
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IJC;
    .locals 1

    .prologue
    .line 2563193
    const-class v0, LX/IJC;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IJC;

    return-object v0
.end method

.method public static values()[LX/IJC;
    .locals 1

    .prologue
    .line 2563194
    sget-object v0, LX/IJC;->$VALUES:[LX/IJC;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IJC;

    return-object v0
.end method
