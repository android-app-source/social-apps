.class public LX/Ivz;
.super LX/398;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/Ivz;


# instance fields
.field private final a:LX/0xW;


# direct methods
.method public constructor <init>(LX/0xW;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2629353
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2629354
    iput-object p1, p0, LX/Ivz;->a:LX/0xW;

    .line 2629355
    sget-object v0, LX/0ax;->iA:Ljava/lang/String;

    const-class v1, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v2, LX/0cQ;->NOTIFICATIONS_FRIENDING_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    .line 2629356
    return-void
.end method

.method public static a(LX/0QB;)LX/Ivz;
    .locals 4

    .prologue
    .line 2629357
    sget-object v0, LX/Ivz;->b:LX/Ivz;

    if-nez v0, :cond_1

    .line 2629358
    const-class v1, LX/Ivz;

    monitor-enter v1

    .line 2629359
    :try_start_0
    sget-object v0, LX/Ivz;->b:LX/Ivz;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2629360
    if-eqz v2, :cond_0

    .line 2629361
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2629362
    new-instance p0, LX/Ivz;

    invoke-static {v0}, LX/0xW;->a(LX/0QB;)LX/0xW;

    move-result-object v3

    check-cast v3, LX/0xW;

    invoke-direct {p0, v3}, LX/Ivz;-><init>(LX/0xW;)V

    .line 2629363
    move-object v0, p0

    .line 2629364
    sput-object v0, LX/Ivz;->b:LX/Ivz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2629365
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2629366
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2629367
    :cond_1
    sget-object v0, LX/Ivz;->b:LX/Ivz;

    return-object v0

    .line 2629368
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2629369
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2629352
    iget-object v0, p0, LX/Ivz;->a:LX/0xW;

    invoke-virtual {v0}, LX/0xW;->e()Z

    move-result v0

    return v0
.end method
