.class public final LX/IUr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/IUt;


# direct methods
.method public constructor <init>(LX/IUt;)V
    .locals 0

    .prologue
    .line 2581101
    iput-object p1, p0, LX/IUr;->a:LX/IUt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x72a6014b

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2581102
    iget-object v1, p0, LX/IUr;->a:LX/IUt;

    iget-object v1, v1, LX/IUt;->f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/IUr;->a:LX/IUt;

    iget-object v1, v1, LX/IUt;->f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/IUr;->a:LX/IUt;

    iget-object v1, v1, LX/IUt;->f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->c()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2581103
    iget-object v1, p0, LX/IUr;->a:LX/IUt;

    iget-object v1, v1, LX/IUt;->f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v1

    .line 2581104
    const-string v2, "https://m.facebook.com/group/settings/?group_id=%s"

    invoke-static {v2, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2581105
    new-instance v3, Landroid/content/Intent;

    const-string v5, "android.intent.action.VIEW"

    invoke-direct {v3, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2581106
    sget-object v5, LX/0ax;->do:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2581107
    move-object v1, v3

    .line 2581108
    iget-object v2, p0, LX/IUr;->a:LX/IUt;

    iget-object v2, v2, LX/IUt;->c:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2581109
    :cond_0
    iget-object v1, p0, LX/IUr;->a:LX/IUt;

    .line 2581110
    new-instance v2, LX/9OZ;

    invoke-direct {v2}, LX/9OZ;-><init>()V

    .line 2581111
    iget-object v3, v1, LX/IUt;->f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, LX/9OZ;->A:Ljava/lang/String;

    .line 2581112
    iget-object v3, v1, LX/IUt;->f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-static {v3}, LX/9OQ;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)LX/9OQ;

    move-result-object v3

    .line 2581113
    const/4 v5, 0x0

    iput-boolean v5, v3, LX/9OQ;->s:Z

    .line 2581114
    invoke-virtual {v2}, LX/9OZ;->a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v2

    .line 2581115
    iput-object v2, v3, LX/9OQ;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    .line 2581116
    invoke-virtual {v3}, LX/9OQ;->a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-result-object v2

    .line 2581117
    iget-object v3, v1, LX/IUt;->b:LX/IRb;

    iget-object v5, v1, LX/IUt;->f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-interface {v3, v5, v2}, LX/IRb;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V

    .line 2581118
    iget-object v3, v1, LX/IUt;->d:LX/3mF;

    iget-object v5, v1, LX/IUt;->f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v5}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v5

    iget-object v6, v1, LX/IUt;->f:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 2581119
    new-instance v7, LX/4Fx;

    invoke-direct {v7}, LX/4Fx;-><init>()V

    .line 2581120
    const-string p0, "group_id"

    invoke-virtual {v7, p0, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2581121
    move-object v7, v7

    .line 2581122
    new-instance p0, LX/B28;

    invoke-direct {p0}, LX/B28;-><init>()V

    move-object p0, p0

    .line 2581123
    const-string p1, "input"

    invoke-virtual {p0, p1, v7}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2581124
    invoke-static {p0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v7

    invoke-virtual {v7, v6}, LX/399;->a(LX/0jT;)LX/399;

    move-result-object v7

    .line 2581125
    iget-object p0, v3, LX/3mF;->a:LX/0tX;

    invoke-virtual {p0, v7}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    const/4 p0, 0x0

    invoke-static {p0}, LX/2Ra;->constant(Ljava/lang/Object;)LX/0QK;

    move-result-object p0

    invoke-static {v7, p0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    move-object v3, v7

    .line 2581126
    new-instance v5, LX/IUs;

    invoke-direct {v5, v1, v2}, LX/IUs;-><init>(LX/IUt;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V

    iget-object v2, v1, LX/IUt;->e:Ljava/util/concurrent/ExecutorService;

    invoke-static {v3, v5, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2581127
    const v1, -0x4a37f4d7

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
