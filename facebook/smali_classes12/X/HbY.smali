.class public LX/HbY;
.super LX/HbN;
.source ""


# instance fields
.field public A:LX/HbE;

.field private B:LX/HbF;

.field public C:LX/HaY;

.field public D:LX/Hbf;

.field private final E:Ljava/lang/Runnable;

.field public F:LX/HaZ;

.field public G:Z

.field public H:LX/Hai;

.field private final o:Lcom/facebook/resources/ui/FbTextView;

.field public final p:Lcom/facebook/resources/ui/FbTextView;

.field public final q:Lcom/facebook/resources/ui/FbButton;

.field public final r:Landroid/view/View;

.field private final s:Landroid/widget/LinearLayout;

.field public final t:Lcom/facebook/securitycheckup/items/DisableScrollRelativeLayout;

.field private final u:Landroid/widget/FrameLayout;

.field public v:I

.field public w:I

.field public x:LX/Had;

.field public y:Z

.field private z:Z


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/content/Context;ILX/Hbf;LX/HbF;LX/HaZ;LX/Hai;)V
    .locals 2
    .param p1    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2485825
    invoke-direct {p0, p1, p2}, LX/HbN;-><init>(Landroid/view/View;Landroid/content/Context;)V

    .line 2485826
    iput-boolean v1, p0, LX/HbY;->y:Z

    .line 2485827
    iput-boolean v1, p0, LX/HbY;->z:Z

    .line 2485828
    new-instance v0, Lcom/facebook/securitycheckup/items/SecurityCheckupExpandableViewHolder$1;

    invoke-direct {v0, p0}, Lcom/facebook/securitycheckup/items/SecurityCheckupExpandableViewHolder$1;-><init>(LX/HbY;)V

    iput-object v0, p0, LX/HbY;->E:Ljava/lang/Runnable;

    .line 2485829
    iput-boolean v1, p0, LX/HbY;->G:Z

    .line 2485830
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v0, p0, LX/HbY;->w:I

    .line 2485831
    const v0, 0x7f0d2bdc

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/HbY;->o:Lcom/facebook/resources/ui/FbTextView;

    .line 2485832
    const v0, 0x7f0d2bdd

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/HbY;->p:Lcom/facebook/resources/ui/FbTextView;

    .line 2485833
    const v0, 0x7f0d2bdf

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, LX/HbY;->q:Lcom/facebook/resources/ui/FbButton;

    .line 2485834
    const v0, 0x7f0d2be0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/HbY;->r:Landroid/view/View;

    .line 2485835
    invoke-virtual {p1, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/HbY;->s:Landroid/widget/LinearLayout;

    .line 2485836
    const v0, 0x7f0d2bde

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/securitycheckup/items/DisableScrollRelativeLayout;

    iput-object v0, p0, LX/HbY;->t:Lcom/facebook/securitycheckup/items/DisableScrollRelativeLayout;

    .line 2485837
    const v0, 0x7f0d2bdb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, LX/HbY;->u:Landroid/widget/FrameLayout;

    .line 2485838
    iget-object v0, p0, LX/HbY;->q:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/HbX;

    invoke-direct {v1, p0}, LX/HbX;-><init>(LX/HbY;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2485839
    new-instance v0, LX/HbQ;

    invoke-direct {v0, p0}, LX/HbQ;-><init>(LX/HbY;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2485840
    iput-object p4, p0, LX/HbY;->D:LX/Hbf;

    .line 2485841
    iput-object p5, p0, LX/HbY;->B:LX/HbF;

    .line 2485842
    iput-object p6, p0, LX/HbY;->F:LX/HaZ;

    .line 2485843
    iput-object p7, p0, LX/HbY;->H:LX/Hai;

    .line 2485844
    return-void
.end method

.method public static A(LX/HbY;)V
    .locals 14

    .prologue
    .line 2485674
    sget-object v0, LX/HbU;->a:[I

    iget-object v1, p0, LX/HbN;->l:LX/Hbb;

    .line 2485675
    iget-object v2, v1, LX/Hbb;->d:LX/Hba;

    move-object v1, v2

    .line 2485676
    invoke-virtual {v1}, LX/Hba;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2485677
    :goto_0
    sget-object v0, LX/HbU;->a:[I

    iget-object v1, p0, LX/HbN;->l:LX/Hbb;

    .line 2485678
    iget-object v2, v1, LX/Hbb;->d:LX/Hba;

    move-object v1, v2

    .line 2485679
    invoke-virtual {v1}, LX/Hba;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 2485680
    :goto_1
    iget-object v6, p0, LX/HbY;->t:Lcom/facebook/securitycheckup/items/DisableScrollRelativeLayout;

    invoke-virtual {v6}, Lcom/facebook/securitycheckup/items/DisableScrollRelativeLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v6

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-virtual {v6, v7}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v6

    const-wide/16 v8, 0xc8

    invoke-virtual {v6, v8, v9}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v6

    new-instance v7, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v7}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v6, v7}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v6

    new-instance v7, LX/HbT;

    invoke-direct {v7, p0}, LX/HbT;-><init>(LX/HbY;)V

    invoke-virtual {v6, v7}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 2485681
    return-void

    .line 2485682
    :pswitch_0
    iget-object v0, p0, LX/HbN;->m:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0312d0

    iget-object v2, p0, LX/HbY;->t:Lcom/facebook/securitycheckup/items/DisableScrollRelativeLayout;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2485683
    iget-object v0, p0, LX/HbY;->B:LX/HbF;

    iget-object v2, p0, LX/HbN;->m:Landroid/content/Context;

    iget-object v3, p0, LX/HbN;->l:LX/Hbb;

    .line 2485684
    iget-object v4, v3, LX/Hbb;->d:LX/Hba;

    move-object v3, v4

    .line 2485685
    iget-object v4, p0, LX/HbN;->l:LX/Hbb;

    .line 2485686
    iget-object v5, v4, LX/Hbb;->g:Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;

    move-object v4, v5

    .line 2485687
    iget-object v5, p0, LX/HbY;->E:Ljava/lang/Runnable;

    invoke-virtual/range {v0 .. v5}, LX/HbF;->a(Landroid/view/View;Landroid/content/Context;LX/Hba;Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;Ljava/lang/Runnable;)LX/HbE;

    move-result-object v0

    iput-object v0, p0, LX/HbY;->A:LX/HbE;

    .line 2485688
    iget-object v0, p0, LX/HbY;->A:LX/HbE;

    .line 2485689
    new-instance v1, LX/1P1;

    iget-object v2, v0, LX/HbE;->i:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/1P1;-><init>(Landroid/content/Context;)V

    iput-object v1, v0, LX/HbE;->e:LX/1P1;

    .line 2485690
    iget-object v1, v0, LX/HbE;->d:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v2, v0, LX/HbE;->e:LX/1P1;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2485691
    sget-object v1, LX/HbC;->a:[I

    iget-object v2, v0, LX/HbE;->k:LX/Hba;

    invoke-virtual {v2}, LX/Hba;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_2

    .line 2485692
    :goto_2
    iget-object v1, v0, LX/HbE;->d:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->requestFocus()Z

    .line 2485693
    goto :goto_0

    .line 2485694
    :pswitch_1
    invoke-static {p0}, LX/HbY;->B(LX/HbY;)V

    goto/16 :goto_0

    .line 2485695
    :pswitch_2
    iget-object v0, p0, LX/HbY;->F:LX/HaZ;

    const-string v1, "END_SESSION_EXPAND"

    invoke-virtual {v0, v1}, LX/HaZ;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 2485696
    :pswitch_3
    iget-object v0, p0, LX/HbY;->F:LX/HaZ;

    const-string v1, "LA_EXPAND"

    invoke-virtual {v0, v1}, LX/HaZ;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2485697
    :pswitch_4
    iget-object v0, p0, LX/HbY;->F:LX/HaZ;

    const-string v1, "PASSWORD_EXPAND"

    invoke-virtual {v0, v1}, LX/HaZ;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2485698
    :pswitch_5
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/16 v6, 0x8

    .line 2485699
    new-instance v1, Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;

    invoke-static {v0}, LX/HbE;->h(LX/HbE;)Ljava/util/List;

    move-result-object v4

    invoke-direct {v1, v4}, Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;-><init>(Ljava/util/List;)V

    iput-object v1, v0, LX/HbE;->s:Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;

    .line 2485700
    iget-object v1, v0, LX/HbE;->s:Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;

    new-instance v4, LX/HbD;

    invoke-direct {v4, v0}, LX/HbD;-><init>(LX/HbE;)V

    .line 2485701
    iput-object v4, v1, Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;->b:LX/HbD;

    .line 2485702
    iget-object v1, v0, LX/HbE;->h:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v1, v6}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2485703
    iget-object v1, v0, LX/HbE;->g:Lcom/facebook/resources/ui/FbButton;

    iget-object v4, v0, LX/HbE;->i:Landroid/content/Context;

    const v5, 0x7f08363d

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2485704
    iget-object v1, v0, LX/HbE;->g:Lcom/facebook/resources/ui/FbButton;

    new-instance v4, LX/Hb6;

    invoke-direct {v4, v0}, LX/Hb6;-><init>(LX/HbE;)V

    invoke-virtual {v1, v4}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2485705
    iget-object v1, v0, LX/HbE;->s:Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;

    invoke-virtual {v1}, LX/1OM;->ij_()I

    move-result v1

    if-lez v1, :cond_2

    .line 2485706
    iget-object v4, v0, LX/HbE;->f:Lcom/facebook/resources/ui/FbButton;

    iget-object v5, v0, LX/HbE;->i:Landroid/content/Context;

    iget-object v1, v0, LX/HbE;->s:Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;

    invoke-virtual {v1}, LX/1OM;->ij_()I

    move-result v1

    if-ne v1, v2, :cond_0

    const v1, 0x7f08363c

    :goto_3
    invoke-virtual {v5, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2485707
    iget-object v1, v0, LX/HbE;->f:Lcom/facebook/resources/ui/FbButton;

    new-instance v4, LX/Hb7;

    invoke-direct {v4, v0}, LX/Hb7;-><init>(LX/HbE;)V

    invoke-virtual {v1, v4}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2485708
    iget-object v1, v0, LX/HbE;->g:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v1, v6}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2485709
    iget-object v1, v0, LX/HbE;->f:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v1, v3}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2485710
    iget-object v1, v0, LX/HbE;->p:Lcom/facebook/resources/ui/FbTextView;

    iget-object v4, v0, LX/HbE;->i:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f08363f

    new-array v6, v2, [Ljava/lang/Object;

    iget-object v7, v0, LX/HbE;->s:Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;

    invoke-virtual {v7}, LX/1OM;->ij_()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v3

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2485711
    iput-boolean v2, v0, LX/HbE;->r:Z

    .line 2485712
    iget-object v4, v0, LX/HbE;->q:Lcom/facebook/securitycheckup/inner/InertCheckBox;

    iget-object v1, v0, LX/HbE;->s:Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;

    invoke-virtual {v1}, LX/1OM;->ij_()I

    move-result v1

    if-lez v1, :cond_1

    move v1, v2

    :goto_4
    invoke-virtual {v4, v1}, Lcom/facebook/securitycheckup/inner/InertCheckBox;->setChecked(Z)V

    .line 2485713
    iget-object v1, v0, LX/HbE;->l:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    new-instance v2, LX/Hb8;

    invoke-direct {v2, v0}, LX/Hb8;-><init>(LX/HbE;)V

    invoke-virtual {v1, v2}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2485714
    iget-object v1, v0, LX/HbE;->d:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v2, v0, LX/HbE;->s:Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2485715
    iget-object v1, v0, LX/HbE;->d:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v2, LX/HbI;

    iget-object v3, v0, LX/HbE;->i:Landroid/content/Context;

    invoke-direct {v2, v3}, LX/HbI;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 2485716
    :goto_5
    goto/16 :goto_2

    .line 2485717
    :pswitch_6
    const/16 v4, 0x8

    .line 2485718
    new-instance v1, Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;

    const/4 v13, 0x4

    const/4 v12, 0x3

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2485719
    iget-object v5, v0, LX/HbE;->j:Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;

    invoke-virtual {v5}, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;->a()LX/1vs;

    move-result-object v5

    iget v5, v5, LX/1vs;->b:I

    if-nez v5, :cond_3

    .line 2485720
    const/4 v2, 0x0

    .line 2485721
    :goto_6
    move-object v2, v2

    .line 2485722
    invoke-direct {v1, v2}, Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;-><init>(Ljava/util/List;)V

    iput-object v1, v0, LX/HbE;->s:Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;

    .line 2485723
    iget-object v1, v0, LX/HbE;->s:Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;

    new-instance v2, LX/HbD;

    invoke-direct {v2, v0}, LX/HbD;-><init>(LX/HbE;)V

    .line 2485724
    iput-object v2, v1, Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;->b:LX/HbD;

    .line 2485725
    iget-object v1, v0, LX/HbE;->g:Lcom/facebook/resources/ui/FbButton;

    iget-object v2, v0, LX/HbE;->i:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f083636

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2485726
    iget-object v1, v0, LX/HbE;->g:Lcom/facebook/resources/ui/FbButton;

    new-instance v2, LX/Hb9;

    invoke-direct {v2, v0}, LX/Hb9;-><init>(LX/HbE;)V

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2485727
    iget-object v1, v0, LX/HbE;->f:Lcom/facebook/resources/ui/FbButton;

    iget-object v2, v0, LX/HbE;->i:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f083635

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2485728
    iget-object v1, v0, LX/HbE;->f:Lcom/facebook/resources/ui/FbButton;

    new-instance v2, LX/HbA;

    invoke-direct {v2, v0}, LX/HbA;-><init>(LX/HbE;)V

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2485729
    iget-object v1, v0, LX/HbE;->h:Lcom/facebook/resources/ui/FbButton;

    iget-object v2, v0, LX/HbE;->i:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08363a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2485730
    iget-object v1, v0, LX/HbE;->h:Lcom/facebook/resources/ui/FbButton;

    new-instance v2, LX/HbB;

    invoke-direct {v2, v0}, LX/HbB;-><init>(LX/HbE;)V

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2485731
    iget-object v1, v0, LX/HbE;->d:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v2, v0, LX/HbE;->s:Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2485732
    iget-object v1, v0, LX/HbE;->d:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v2, LX/HbI;

    iget-object v3, v0, LX/HbE;->i:Landroid/content/Context;

    invoke-direct {v2, v3}, LX/HbI;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 2485733
    iget-object v1, v0, LX/HbE;->l:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    invoke-virtual {v1, v4}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->setVisibility(I)V

    .line 2485734
    iget-object v1, v0, LX/HbE;->m:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2485735
    iget-object v1, v0, LX/HbE;->o:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2485736
    goto/16 :goto_2

    .line 2485737
    :cond_0
    const v1, 0x7f08363b

    goto/16 :goto_3

    :cond_1
    move v1, v3

    .line 2485738
    goto/16 :goto_4

    .line 2485739
    :cond_2
    iget-object v1, v0, LX/HbE;->m:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2485740
    iget-object v1, v0, LX/HbE;->n:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2485741
    iget-object v1, v0, LX/HbE;->l:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    invoke-virtual {v1, v6}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->setVisibility(I)V

    .line 2485742
    iget-object v1, v0, LX/HbE;->g:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v1, v3}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2485743
    iget-object v1, v0, LX/HbE;->f:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v1, v6}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    goto/16 :goto_5

    .line 2485744
    :cond_3
    iget-object v5, v0, LX/HbE;->v:LX/Hai;

    .line 2485745
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 2485746
    iget-object v7, v5, LX/Hai;->b:LX/HbG;

    if-eqz v7, :cond_4

    .line 2485747
    iget-object v7, v5, LX/Hai;->b:LX/HbG;

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2485748
    :cond_4
    iget-object v7, v5, LX/Hai;->c:LX/HbG;

    if-eqz v7, :cond_5

    .line 2485749
    iget-object v7, v5, LX/Hai;->c:LX/HbG;

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2485750
    :cond_5
    iget-object v7, v5, LX/Hai;->d:LX/HbG;

    if-eqz v7, :cond_6

    .line 2485751
    iget-object v7, v5, LX/Hai;->d:LX/HbG;

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2485752
    :cond_6
    move-object v5, v6

    .line 2485753
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    if-lez v6, :cond_8

    .line 2485754
    iget-object v2, v0, LX/HbE;->v:LX/Hai;

    invoke-virtual {v2}, LX/Hai;->i()Z

    move-result v2

    if-nez v2, :cond_7

    .line 2485755
    iget-object v2, v0, LX/HbE;->v:LX/Hai;

    .line 2485756
    iget-boolean v3, v2, LX/Hai;->h:Z

    if-nez v3, :cond_10

    .line 2485757
    iget-object v3, v2, LX/Hai;->b:LX/HbG;

    const/4 v6, 0x1

    .line 2485758
    iput-boolean v6, v3, LX/HbG;->e:Z

    .line 2485759
    :cond_7
    :goto_7
    iget-object v2, v0, LX/HbE;->v:LX/Hai;

    .line 2485760
    iget-object v3, v2, LX/Hai;->b:LX/HbG;

    move-object v2, v3

    .line 2485761
    iput-object v2, v0, LX/HbE;->a:LX/HbG;

    .line 2485762
    iget-object v2, v0, LX/HbE;->v:LX/Hai;

    .line 2485763
    iget-object v3, v2, LX/Hai;->c:LX/HbG;

    move-object v2, v3

    .line 2485764
    iput-object v2, v0, LX/HbE;->b:LX/HbG;

    .line 2485765
    iget-object v2, v0, LX/HbE;->v:LX/Hai;

    .line 2485766
    iget-object v3, v2, LX/Hai;->d:LX/HbG;

    move-object v2, v3

    .line 2485767
    iput-object v2, v0, LX/HbE;->c:LX/HbG;

    move-object v2, v5

    .line 2485768
    goto/16 :goto_6

    .line 2485769
    :cond_8
    const v6, 0x7f020e69

    iget-object v7, v0, LX/HbE;->i:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f083637

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    const-string v8, ""

    invoke-static {v6, v7, v8}, LX/HbG;->a(ILjava/lang/String;Ljava/lang/String;)LX/HbG;

    move-result-object v6

    iput-object v6, v0, LX/HbE;->a:LX/HbG;

    .line 2485770
    iget-object v6, v0, LX/HbE;->j:Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;

    invoke-virtual {v6}, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;->a()LX/1vs;

    move-result-object v6

    iget-object v7, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    iget-object v8, v0, LX/HbE;->a:LX/HbG;

    const/4 v9, 0x2

    invoke-virtual {v7, v6, v9}, LX/15i;->h(II)Z

    move-result v6

    .line 2485771
    iput-boolean v6, v8, LX/HbG;->e:Z

    .line 2485772
    iget-object v6, v0, LX/HbE;->a:LX/HbG;

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2485773
    iget-object v6, v0, LX/HbE;->j:Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;

    invoke-virtual {v6}, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;->a()LX/1vs;

    move-result-object v6

    iget-object v7, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    .line 2485774
    invoke-virtual {v7, v6, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_d

    move v7, v3

    .line 2485775
    :goto_8
    if-eqz v7, :cond_9

    .line 2485776
    iget-object v6, v0, LX/HbE;->j:Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;

    invoke-virtual {v6}, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;->a()LX/1vs;

    move-result-object v6

    iget-object v8, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    .line 2485777
    const v9, 0x7f020e68

    iget-object v10, v0, LX/HbE;->i:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f083638

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v6, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v6

    invoke-static {v9, v10, v6}, LX/HbG;->a(ILjava/lang/String;Ljava/lang/String;)LX/HbG;

    move-result-object v6

    iput-object v6, v0, LX/HbE;->b:LX/HbG;

    .line 2485778
    iget-object v6, v0, LX/HbE;->j:Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;

    invoke-virtual {v6}, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;->a()LX/1vs;

    move-result-object v6

    iget-object v8, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    iget-object v9, v0, LX/HbE;->b:LX/HbG;

    invoke-virtual {v8, v6, v3}, LX/15i;->h(II)Z

    move-result v6

    .line 2485779
    iput-boolean v6, v9, LX/HbG;->e:Z

    .line 2485780
    iget-object v6, v0, LX/HbE;->b:LX/HbG;

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2485781
    :cond_9
    iget-object v6, v0, LX/HbE;->j:Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;

    invoke-virtual {v6}, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;->a()LX/1vs;

    move-result-object v6

    iget-object v8, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    .line 2485782
    invoke-virtual {v8, v6, v13}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_e

    move v6, v3

    .line 2485783
    :goto_9
    if-eqz v6, :cond_a

    .line 2485784
    if-nez v7, :cond_f

    move v2, v3

    .line 2485785
    :cond_a
    :goto_a
    if-eqz v2, :cond_b

    .line 2485786
    iget-object v2, v0, LX/HbE;->j:Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;

    invoke-virtual {v2}, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;->a()LX/1vs;

    move-result-object v2

    iget-object v6, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2485787
    const v7, 0x7f020e6a

    iget-object v8, v0, LX/HbE;->i:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f083639

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v2, v13}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v7, v8, v2}, LX/HbG;->a(ILjava/lang/String;Ljava/lang/String;)LX/HbG;

    move-result-object v2

    iput-object v2, v0, LX/HbE;->c:LX/HbG;

    .line 2485788
    iget-object v2, v0, LX/HbE;->j:Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;

    invoke-virtual {v2}, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;->a()LX/1vs;

    move-result-object v2

    iget-object v6, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    iget-object v7, v0, LX/HbE;->c:LX/HbG;

    invoke-virtual {v6, v2, v12}, LX/15i;->h(II)Z

    move-result v2

    .line 2485789
    iput-boolean v2, v7, LX/HbG;->e:Z

    .line 2485790
    iget-object v2, v0, LX/HbE;->c:LX/HbG;

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2485791
    :cond_b
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 2485792
    iget-object v2, v0, LX/HbE;->j:Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;

    if-eqz v2, :cond_15

    iget-object v2, v0, LX/HbE;->j:Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;

    invoke-virtual {v2}, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;->a()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    if-eqz v2, :cond_14

    move v2, v6

    :goto_b
    if-eqz v2, :cond_18

    .line 2485793
    iget-object v2, v0, LX/HbE;->j:Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;

    invoke-virtual {v2}, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;->a()LX/1vs;

    move-result-object v2

    iget-object v7, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    invoke-virtual {v7, v2, v6}, LX/15i;->h(II)Z

    move-result v2

    if-eqz v2, :cond_16

    move v2, v6

    :goto_c
    if-eqz v2, :cond_17

    .line 2485794
    :goto_d
    move v2, v6

    .line 2485795
    if-nez v2, :cond_c

    .line 2485796
    iget-object v2, v0, LX/HbE;->a:LX/HbG;

    .line 2485797
    iput-boolean v3, v2, LX/HbG;->e:Z

    .line 2485798
    :cond_c
    iget-object v2, v0, LX/HbE;->v:LX/Hai;

    iget-object v3, v0, LX/HbE;->a:LX/HbG;

    .line 2485799
    iput-object v3, v2, LX/Hai;->b:LX/HbG;

    .line 2485800
    iget-object v2, v0, LX/HbE;->v:LX/Hai;

    iget-object v3, v0, LX/HbE;->b:LX/HbG;

    .line 2485801
    iput-object v3, v2, LX/Hai;->c:LX/HbG;

    .line 2485802
    iget-object v2, v0, LX/HbE;->v:LX/Hai;

    iget-object v3, v0, LX/HbE;->c:LX/HbG;

    .line 2485803
    iput-object v3, v2, LX/Hai;->d:LX/HbG;

    .line 2485804
    move-object v2, v5

    .line 2485805
    goto/16 :goto_6

    :cond_d
    move v7, v2

    .line 2485806
    goto/16 :goto_8

    :cond_e
    move v6, v2

    .line 2485807
    goto/16 :goto_9

    .line 2485808
    :cond_f
    iget-object v2, v0, LX/HbE;->j:Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;

    invoke-virtual {v2}, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;->a()LX/1vs;

    move-result-object v2

    iget-object v6, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2485809
    invoke-virtual {v6, v2, v12}, LX/15i;->h(II)Z

    move-result v2

    goto/16 :goto_a

    .line 2485810
    :cond_10
    iget-object v3, v2, LX/Hai;->b:LX/HbG;

    if-eqz v3, :cond_11

    .line 2485811
    iget-object v3, v2, LX/Hai;->b:LX/HbG;

    iget-boolean v6, v2, LX/Hai;->e:Z

    .line 2485812
    iput-boolean v6, v3, LX/HbG;->e:Z

    .line 2485813
    :cond_11
    iget-object v3, v2, LX/Hai;->c:LX/HbG;

    if-eqz v3, :cond_12

    .line 2485814
    iget-object v3, v2, LX/Hai;->c:LX/HbG;

    iget-boolean v6, v2, LX/Hai;->f:Z

    .line 2485815
    iput-boolean v6, v3, LX/HbG;->e:Z

    .line 2485816
    :cond_12
    iget-object v3, v2, LX/Hai;->d:LX/HbG;

    if-eqz v3, :cond_13

    .line 2485817
    iget-object v3, v2, LX/Hai;->d:LX/HbG;

    iget-boolean v6, v2, LX/Hai;->g:Z

    .line 2485818
    iput-boolean v6, v3, LX/HbG;->e:Z

    .line 2485819
    :cond_13
    const/4 v3, 0x0

    iput-boolean v3, v2, LX/Hai;->h:Z

    goto/16 :goto_7

    :cond_14
    move v2, v7

    goto :goto_b

    :cond_15
    move v2, v7

    goto :goto_b

    .line 2485820
    :cond_16
    iget-object v2, v0, LX/HbE;->j:Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;

    invoke-virtual {v2}, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;->a()LX/1vs;

    move-result-object v2

    iget-object v7, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2485821
    const/4 v8, 0x2

    invoke-virtual {v7, v2, v8}, LX/15i;->h(II)Z

    move-result v2

    goto :goto_c

    .line 2485822
    :cond_17
    iget-object v2, v0, LX/HbE;->j:Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;

    invoke-virtual {v2}, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;->a()LX/1vs;

    move-result-object v2

    iget-object v6, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2485823
    const/4 v7, 0x3

    invoke-virtual {v6, v2, v7}, LX/15i;->h(II)Z

    move-result v6

    goto :goto_d

    :cond_18
    move v6, v7

    .line 2485824
    goto :goto_d

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private static B(LX/HbY;)V
    .locals 10

    .prologue
    .line 2485861
    iget-object v0, p0, LX/HbN;->m:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0312cf

    iget-object v2, p0, LX/HbY;->t:Lcom/facebook/securitycheckup/items/DisableScrollRelativeLayout;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2485862
    iget-object v4, p0, LX/HbN;->m:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    .line 2485863
    const/16 v5, 0x8

    invoke-static {v4, v5}, LX/Hbr;->a(Landroid/util/DisplayMetrics;I)I

    move-result v8

    .line 2485864
    const/4 v5, 0x2

    invoke-static {v4, v5}, LX/Hbr;->a(Landroid/util/DisplayMetrics;I)I

    move-result v9

    .line 2485865
    const v6, 0x7f0d2bca

    const v7, 0x7f083641

    move-object v4, p0

    move-object v5, v0

    invoke-static/range {v4 .. v9}, LX/HbY;->a(LX/HbY;Landroid/view/View;IIII)V

    .line 2485866
    const v6, 0x7f0d2bcb

    const v7, 0x7f083642

    move-object v4, p0

    move-object v5, v0

    invoke-static/range {v4 .. v9}, LX/HbY;->a(LX/HbY;Landroid/view/View;IIII)V

    .line 2485867
    const v6, 0x7f0d2bcc

    const v7, 0x7f083643

    move-object v4, p0

    move-object v5, v0

    invoke-static/range {v4 .. v9}, LX/HbY;->a(LX/HbY;Landroid/view/View;IIII)V

    .line 2485868
    const v1, 0x7f0d2bc8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    .line 2485869
    new-instance v1, LX/HbS;

    invoke-direct {v1, p0}, LX/HbS;-><init>(LX/HbY;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2485870
    return-void
.end method

.method public static a(LX/HbY;Landroid/view/View;IIII)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2485855
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2485856
    iget-object v1, p0, LX/HbN;->m:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2485857
    new-instance v2, Landroid/text/SpannableString;

    invoke-direct {v2, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2485858
    new-instance v1, LX/Hbe;

    invoke-direct {v1, p4, p5}, LX/Hbe;-><init>(II)V

    invoke-virtual {v2}, Landroid/text/SpannableString;->length()I

    move-result v3

    invoke-virtual {v2, v1, v4, v3, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 2485859
    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2485860
    return-void
.end method


# virtual methods
.method public final a(LX/Hbb;I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x8

    .line 2485871
    invoke-super {p0, p1, p2}, LX/HbN;->a(LX/Hbb;I)V

    .line 2485872
    iget-object v0, p0, LX/HbY;->o:Lcom/facebook/resources/ui/FbTextView;

    .line 2485873
    iget-object v1, p1, LX/Hbb;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2485874
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2485875
    iget-object v0, p0, LX/HbY;->p:Lcom/facebook/resources/ui/FbTextView;

    .line 2485876
    iget-object v1, p1, LX/Hbb;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2485877
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2485878
    iget-object v0, p0, LX/HbN;->m:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 2485879
    iget-object v0, p1, LX/Hbb;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2485880
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2485881
    iget-object v0, p0, LX/HbY;->q:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v4}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2485882
    iget-object v0, p0, LX/HbY;->s:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 2485883
    const/16 v2, 0x20

    invoke-static {v1, v2}, LX/Hbr;->a(Landroid/util/DisplayMetrics;I)I

    move-result v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 2485884
    iget-object v1, p0, LX/HbY;->s:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2485885
    :goto_0
    iget-object v0, p0, LX/HbY;->u:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 2485886
    new-instance v0, LX/HaY;

    iget-object v1, p0, LX/HbY;->u:Landroid/widget/FrameLayout;

    iget-object v2, p0, LX/HbN;->l:LX/Hbb;

    .line 2485887
    iget-object v3, v2, LX/Hbb;->d:LX/Hba;

    move-object v2, v3

    .line 2485888
    invoke-direct {v0, v1, v2}, LX/HaY;-><init>(Landroid/widget/FrameLayout;LX/Hba;)V

    iput-object v0, p0, LX/HbY;->C:LX/HaY;

    .line 2485889
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    new-instance v1, Lcom/facebook/securitycheckup/items/SecurityCheckupExpandableViewHolder$3;

    invoke-direct {v1, p0}, Lcom/facebook/securitycheckup/items/SecurityCheckupExpandableViewHolder$3;-><init>(LX/HbY;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 2485890
    iget-object v0, p0, LX/HbY;->r:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2485891
    iget-object v0, p0, LX/HbY;->t:Lcom/facebook/securitycheckup/items/DisableScrollRelativeLayout;

    invoke-virtual {v0, v4}, Lcom/facebook/securitycheckup/items/DisableScrollRelativeLayout;->setVisibility(I)V

    .line 2485892
    iput-boolean v5, p0, LX/HbY;->z:Z

    .line 2485893
    return-void

    .line 2485894
    :cond_0
    iget-object v0, p0, LX/HbY;->q:Lcom/facebook/resources/ui/FbButton;

    .line 2485895
    iget-object v2, p1, LX/Hbb;->c:Ljava/lang/String;

    move-object v2, v2

    .line 2485896
    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2485897
    iget-object v0, p0, LX/HbY;->q:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v5}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2485898
    iget-boolean v0, p1, LX/Hbb;->f:Z

    move v0, v0

    .line 2485899
    if-eqz v0, :cond_1

    .line 2485900
    iget-object v0, p0, LX/HbY;->q:Lcom/facebook/resources/ui/FbButton;

    iget-object v2, p0, LX/HbN;->m:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00e6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbButton;->setTextColor(I)V

    .line 2485901
    :goto_1
    iget-object v0, p0, LX/HbY;->s:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 2485902
    const/16 v2, 0x48

    invoke-static {v1, v2}, LX/Hbr;->a(Landroid/util/DisplayMetrics;I)I

    move-result v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 2485903
    iget-object v1, p0, LX/HbY;->s:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 2485904
    :cond_1
    iget-object v0, p0, LX/HbY;->q:Lcom/facebook/resources/ui/FbButton;

    iget-object v2, p0, LX/HbN;->m:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00d2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbButton;->setTextColor(I)V

    goto :goto_1
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 2485852
    iput-boolean p1, p0, LX/HbY;->y:Z

    .line 2485853
    iget-object v0, p0, LX/HbY;->q:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbButton;->callOnClick()Z

    .line 2485854
    return-void
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 2485848
    iget-boolean v0, p0, LX/HbY;->z:Z

    if-eqz v0, :cond_0

    .line 2485849
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/HbY;->z:Z

    .line 2485850
    :goto_0
    return-void

    .line 2485851
    :cond_0
    iget-object v0, p0, LX/HbY;->r:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public final y()V
    .locals 4

    .prologue
    .line 2485845
    iget-object v0, p0, LX/HbY;->r:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, LX/HbR;

    invoke-direct {v1, p0}, LX/HbR;-><init>(LX/HbY;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 2485846
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/HbY;->z:Z

    .line 2485847
    return-void
.end method
