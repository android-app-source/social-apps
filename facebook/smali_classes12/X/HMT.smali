.class public LX/HMT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/HMI;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/0kL;

.field private final c:Landroid/content/res/Resources;

.field private final d:LX/9XE;

.field private final e:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

.field private final f:LX/03V;

.field private g:J


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0kL;Landroid/content/res/Resources;LX/9XE;Lcom/facebook/composer/publish/ComposerPublishServiceHelper;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2457169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2457170
    iput-object p1, p0, LX/HMT;->a:Landroid/content/Context;

    .line 2457171
    iput-object p2, p0, LX/HMT;->b:LX/0kL;

    .line 2457172
    iput-object p3, p0, LX/HMT;->c:Landroid/content/res/Resources;

    .line 2457173
    iput-object p4, p0, LX/HMT;->d:LX/9XE;

    .line 2457174
    iput-object p5, p0, LX/HMT;->e:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    .line 2457175
    iput-object p6, p0, LX/HMT;->f:LX/03V;

    .line 2457176
    return-void
.end method


# virtual methods
.method public final a()LX/4At;
    .locals 4

    .prologue
    .line 2457168
    new-instance v0, LX/4At;

    iget-object v1, p0, LX/HMT;->a:Landroid/content/Context;

    iget-object v2, p0, LX/HMT;->c:Landroid/content/res/Resources;

    const v3, 0x7f0817d4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/4At;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(JLX/8A4;Lcom/facebook/base/fragment/FbFragment;Landroid/content/Intent;I)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p3    # LX/8A4;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LX/8A4;",
            "Lcom/facebook/base/fragment/FbFragment;",
            "Landroid/content/Intent;",
            "I)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2457166
    iput-wide p1, p0, LX/HMT;->g:J

    .line 2457167
    iget-object v0, p0, LX/HMT;->e:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-virtual {v0, p5}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 4

    .prologue
    .line 2457177
    iget-object v0, p0, LX/HMT;->b:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0817d5

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2457178
    iget-object v0, p0, LX/HMT;->d:LX/9XE;

    sget-object v1, LX/9XB;->EVENT_SHARE_PAGE_SUCCESS:LX/9XB;

    iget-wide v2, p0, LX/HMT;->g:J

    invoke-virtual {v0, v1, v2, v3}, LX/9XE;->a(LX/9X2;J)V

    .line 2457179
    return-void
.end method

.method public final a(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 4

    .prologue
    .line 2457162
    iget-object v0, p0, LX/HMT;->b:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0817d6

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2457163
    iget-object v0, p0, LX/HMT;->d:LX/9XE;

    sget-object v1, LX/9XA;->EVENT_SHARE_PAGE_ERROR:LX/9XA;

    iget-wide v2, p0, LX/HMT;->g:J

    invoke-virtual {v0, v1, v2, v3}, LX/9XE;->a(LX/9X2;J)V

    .line 2457164
    iget-object v0, p0, LX/HMT;->f:LX/03V;

    const-string v1, "page_identity_share_fail"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2457165
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2457161
    const/4 v0, 0x0

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2457160
    const/4 v0, 0x1

    return v0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2457159
    const/16 v0, 0x2775

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
