.class public LX/JLo;
.super LX/5pb;
.source ""

# interfaces
.implements LX/5on;


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "PageServiceAddEditPickerNativeModule"
.end annotation


# instance fields
.field private final a:LX/2Qx;


# direct methods
.method public constructor <init>(LX/2Qx;LX/5pY;)V
    .locals 0
    .param p2    # LX/5pY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2681607
    invoke-direct {p0, p2}, LX/5pb;-><init>(LX/5pY;)V

    .line 2681608
    iput-object p1, p0, LX/JLo;->a:LX/2Qx;

    .line 2681609
    invoke-virtual {p2, p0}, LX/5pX;->a(LX/5on;)V

    .line 2681610
    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .locals 5

    .prologue
    .line 2681592
    const-string v0, "extra_media_items"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2681593
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2681594
    :cond_0
    :goto_0
    return-void

    .line 2681595
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 2681596
    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/2Qx;->a(Ljava/lang/String;)LX/434;

    move-result-object v1

    .line 2681597
    new-instance v2, Lcom/facebook/fbreact/pageserviceaddedit/PhotoData;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    iget v4, v1, LX/434;->b:I

    iget v1, v1, LX/434;->a:I

    invoke-direct {v2, v3, v0, v4, v1}, Lcom/facebook/fbreact/pageserviceaddedit/PhotoData;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    .line 2681598
    new-instance v0, LX/JLr;

    invoke-direct {v0, v2}, LX/JLr;-><init>(Lcom/facebook/fbreact/pageserviceaddedit/PhotoData;)V

    .line 2681599
    const-string v1, "serviceAddEditAndroidPhotoPicked"

    .line 2681600
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v2

    .line 2681601
    const-string v3, "id"

    iget-object v4, v0, LX/JLr;->a:Lcom/facebook/fbreact/pageserviceaddedit/PhotoData;

    iget-object v4, v4, Lcom/facebook/fbreact/pageserviceaddedit/PhotoData;->a:Ljava/lang/String;

    invoke-interface {v2, v3, v4}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2681602
    const-string v3, "uri"

    iget-object v4, v0, LX/JLr;->a:Lcom/facebook/fbreact/pageserviceaddedit/PhotoData;

    iget-object v4, v4, Lcom/facebook/fbreact/pageserviceaddedit/PhotoData;->b:Ljava/lang/String;

    invoke-interface {v2, v3, v4}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2681603
    const-string v3, "width"

    iget-object v4, v0, LX/JLr;->a:Lcom/facebook/fbreact/pageserviceaddedit/PhotoData;

    iget v4, v4, Lcom/facebook/fbreact/pageserviceaddedit/PhotoData;->c:I

    invoke-interface {v2, v3, v4}, LX/5pH;->putInt(Ljava/lang/String;I)V

    .line 2681604
    const-string v3, "height"

    iget-object v4, v0, LX/JLr;->a:Lcom/facebook/fbreact/pageserviceaddedit/PhotoData;

    iget v4, v4, Lcom/facebook/fbreact/pageserviceaddedit/PhotoData;->d:I

    invoke-interface {v2, v3, v4}, LX/5pH;->putInt(Ljava/lang/String;I)V

    .line 2681605
    move-object v0, v2

    .line 2681606
    invoke-direct {p0, v1, v0}, LX/JLo;->a(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2681611
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2681612
    const-class v1, Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;

    invoke-virtual {v0, v1}, LX/5pX;->a(Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;

    invoke-interface {v0, p1, p2}, Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;->emit(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2681613
    return-void
.end method


# virtual methods
.method public final a(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2681588
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 2681589
    :goto_0
    return-void

    .line 2681590
    :cond_0
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 2681591
    :pswitch_0
    invoke-direct {p0, p3}, LX/JLo;->a(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2713
        :pswitch_0
    .end packed-switch
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2681587
    const-string v0, "PageServiceAddEditPickerNativeModule"

    return-object v0
.end method

.method public openNativePhotoPicker()V
    .locals 4
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2681580
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2681581
    invoke-virtual {v0}, LX/5pX;->i()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2681582
    :goto_0
    return-void

    .line 2681583
    :cond_0
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2681584
    new-instance v1, LX/8AA;

    sget-object v2, LX/8AB;->PAGE_SERVICE:LX/8AB;

    invoke-direct {v1, v2}, LX/8AA;-><init>(LX/8AB;)V

    invoke-virtual {v1}, LX/8AA;->i()LX/8AA;

    move-result-object v1

    invoke-virtual {v1}, LX/8AA;->j()LX/8AA;

    move-result-object v1

    invoke-virtual {v1}, LX/8AA;->l()LX/8AA;

    move-result-object v1

    sget-object v2, LX/8A9;->NONE:LX/8A9;

    invoke-virtual {v1, v2}, LX/8AA;->a(LX/8A9;)LX/8AA;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->a(Landroid/content/Context;LX/8AA;)Landroid/content/Intent;

    move-result-object v0

    .line 2681585
    iget-object v1, p0, LX/5pb;->a:LX/5pY;

    move-object v1, v1

    .line 2681586
    const/16 v2, 0x2713

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, LX/5pX;->a(Landroid/content/Intent;ILandroid/os/Bundle;)Z

    goto :goto_0
.end method
