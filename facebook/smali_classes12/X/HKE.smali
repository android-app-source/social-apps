.class public LX/HKE;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumUnitComponentComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/HKE",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumUnitComponentComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2454039
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2454040
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/HKE;->b:LX/0Zi;

    .line 2454041
    iput-object p1, p0, LX/HKE;->a:LX/0Ot;

    .line 2454042
    return-void
.end method

.method public static a(LX/0QB;)LX/HKE;
    .locals 4

    .prologue
    .line 2454043
    const-class v1, LX/HKE;

    monitor-enter v1

    .line 2454044
    :try_start_0
    sget-object v0, LX/HKE;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2454045
    sput-object v2, LX/HKE;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2454046
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2454047
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2454048
    new-instance v3, LX/HKE;

    const/16 p0, 0x2bc4

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/HKE;-><init>(LX/0Ot;)V

    .line 2454049
    move-object v0, v3

    .line 2454050
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2454051
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/HKE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2454052
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2454053
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private b(LX/1X1;)V
    .locals 10

    .prologue
    .line 2454054
    check-cast p1, LX/HKD;

    .line 2454055
    iget-object v0, p0, LX/HKE;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumUnitComponentComponentSpec;

    iget-object v1, p1, LX/HKD;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v2, p1, LX/HKD;->b:LX/2km;

    .line 2454056
    iget-object v3, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v3

    .line 2454057
    invoke-interface {v3}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v3

    if-nez v3, :cond_0

    .line 2454058
    :goto_0
    return-void

    .line 2454059
    :cond_0
    iget-object v3, v0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumUnitComponentComponentSpec;->c:LX/E1f;

    .line 2454060
    iget-object v4, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v4, v4

    .line 2454061
    invoke-interface {v4}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v4

    move-object v5, v2

    check-cast v5, LX/1Pn;

    invoke-interface {v5}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v5

    const/4 v6, 0x0

    move-object v7, v2

    check-cast v7, LX/2kp;

    invoke-interface {v7}, LX/2kp;->t()LX/2jY;

    move-result-object v7

    .line 2454062
    iget-object v8, v7, LX/2jY;->a:Ljava/lang/String;

    move-object v7, v8

    .line 2454063
    move-object v8, v2

    check-cast v8, LX/2kp;

    invoke-interface {v8}, LX/2kp;->t()LX/2jY;

    move-result-object v8

    .line 2454064
    iget-object v9, v8, LX/2jY;->b:Ljava/lang/String;

    move-object v8, v9

    .line 2454065
    iget-object v9, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v9, v9

    .line 2454066
    invoke-virtual/range {v3 .. v9}, LX/E1f;->a(LX/9rk;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v3

    .line 2454067
    iget-object v4, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v4, v4

    .line 2454068
    iget-object v5, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v5, v5

    .line 2454069
    invoke-interface {v2, v4, v5, v3}, LX/2km;->a(Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V

    goto :goto_0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2454070
    const v0, 0x24d07328

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 11

    .prologue
    .line 2454071
    check-cast p2, LX/HKD;

    .line 2454072
    iget-object v0, p0, LX/HKE;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumUnitComponentComponentSpec;

    iget-object v1, p2, LX/HKD;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v2, p2, LX/HKD;->b:LX/2km;

    const/4 p2, 0x3

    const/4 p0, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2454073
    iget-object v3, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v3

    .line 2454074
    invoke-interface {v3}, LX/9uc;->s()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel;

    move-result-object v8

    .line 2454075
    invoke-virtual {v8}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v8}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel$ImageLowModel;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v8}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel$ImageLowModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel$ImageLowModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    move v3, v7

    .line 2454076
    :goto_0
    if-eqz v3, :cond_1

    invoke-virtual {v8}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel;->b()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel;->c()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel$ImageLowModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$AlbumCoverPhotoModel$ImageLowModel;->a()Ljava/lang/String;

    move-result-object v3

    .line 2454077
    :goto_1
    invoke-virtual {v8}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel;->e()LX/174;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v8}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel;->e()LX/174;

    move-result-object v4

    invoke-interface {v4}, LX/174;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    move v4, v7

    .line 2454078
    :goto_2
    if-eqz v4, :cond_3

    invoke-virtual {v8}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel;->e()LX/174;

    move-result-object v4

    invoke-interface {v4}, LX/174;->a()Ljava/lang/String;

    move-result-object v4

    .line 2454079
    :goto_3
    invoke-virtual {v8}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel;->d()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$PhotoItemsModel;

    move-result-object v5

    if-eqz v5, :cond_4

    move v5, v7

    .line 2454080
    :goto_4
    if-eqz v5, :cond_5

    invoke-virtual {v8}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel;->d()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$PhotoItemsModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitPagePhotoAlbumComponentFragmentModel$AlbumModel$PhotoItemsModel;->a()I

    move-result v5

    .line 2454081
    :goto_5
    check-cast v2, LX/1Pn;

    invoke-interface {v2}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v5, v8}, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumUnitComponentComponentSpec;->a(ILandroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 2454082
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v8

    invoke-interface {v8, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v8

    .line 2454083
    const v9, 0x24d07328

    const/4 v10, 0x0

    invoke-static {p1, v9, v10}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v9

    move-object v9, v9

    .line 2454084
    invoke-interface {v8, v9}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v8

    invoke-interface {v8, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v8

    const/high16 v9, 0x3f800000    # 1.0f

    invoke-interface {v8, v9}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v8

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v9

    invoke-interface {v9, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v9

    const v10, 0x7f02008a

    invoke-interface {v9, v10}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v9

    invoke-interface {v9, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v6

    const v9, 0x7f0b0e43

    invoke-interface {v6, p2, v9}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v6

    const/high16 v1, 0x3f800000    # 1.0f

    .line 2454085
    if-nez v3, :cond_6

    .line 2454086
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v9

    const v10, 0x7f020089

    invoke-interface {v9, v10}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v9

    iget-object v10, v0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumUnitComponentComponentSpec;->b:LX/1nu;

    invoke-virtual {v10, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v10

    invoke-virtual {v10, v1}, LX/1nw;->c(F)LX/1nw;

    move-result-object v10

    sget-object v1, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumUnitComponentComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v10, v1}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v10

    const/4 v1, 0x0

    invoke-virtual {v10, v1}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v10

    const v1, 0x7f020626

    invoke-virtual {v10, v1}, LX/1nw;->h(I)LX/1nw;

    move-result-object v10

    invoke-virtual {v10}, LX/1X5;->c()LX/1Di;

    move-result-object v10

    const/16 v1, 0x8

    const v2, 0x7f0b0e3c

    invoke-interface {v10, v1, v2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v10

    invoke-interface {v9, v10}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v9

    .line 2454087
    :goto_6
    move-object v3, v9

    .line 2454088
    const/4 v9, 0x4

    invoke-interface {v3, v9}, LX/1Di;->b(I)LX/1Di;

    move-result-object v3

    const v9, 0x7f0b0e44

    invoke-interface {v3, p0, v9}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    const v9, 0x7f0b0e44

    invoke-interface {v3, p2, v9}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    invoke-interface {v6, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v8, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v6

    sget-object v8, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v6, v8}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v7}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    const v6, 0x7f0a0099

    invoke-virtual {v4, v6}, LX/1ne;->n(I)LX/1ne;

    move-result-object v4

    const v6, 0x7f0b004f

    invoke-virtual {v4, v6}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v7}, LX/1ne;->t(I)LX/1ne;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v7}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v5}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    const v5, 0x7f0a00a3

    invoke-virtual {v4, v5}, LX/1ne;->n(I)LX/1ne;

    move-result-object v4

    const v5, 0x7f0b004e

    invoke-virtual {v4, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2454089
    return-object v0

    :cond_0
    move v3, v6

    .line 2454090
    goto/16 :goto_0

    .line 2454091
    :cond_1
    const/4 v3, 0x0

    goto/16 :goto_1

    :cond_2
    move v4, v6

    .line 2454092
    goto/16 :goto_2

    .line 2454093
    :cond_3
    const-string v4, ""

    goto/16 :goto_3

    :cond_4
    move v5, v6

    .line 2454094
    goto/16 :goto_4

    :cond_5
    move v5, v6

    .line 2454095
    goto/16 :goto_5

    :cond_6
    iget-object v9, v0, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumUnitComponentComponentSpec;->b:LX/1nu;

    invoke-virtual {v9, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v9

    invoke-virtual {v9, v1}, LX/1nw;->c(F)LX/1nw;

    move-result-object v9

    sget-object v10, Lcom/facebook/pages/common/reaction/components/PagePhotoAlbumUnitComponentComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v9, v10}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v9

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    invoke-virtual {v9, v10}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v9

    invoke-virtual {v9}, LX/1X5;->c()LX/1Di;

    move-result-object v9

    goto/16 :goto_6
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2454096
    invoke-static {}, LX/1dS;->b()V

    .line 2454097
    iget v0, p1, LX/1dQ;->b:I

    .line 2454098
    packed-switch v0, :pswitch_data_0

    .line 2454099
    :goto_0
    return-object v1

    .line 2454100
    :pswitch_0
    iget-object v0, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0}, LX/HKE;->b(LX/1X1;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x24d07328
        :pswitch_0
    .end packed-switch
.end method
