.class public LX/JIN;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0tX;

.field private final b:LX/1mR;

.field private final c:Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;


# direct methods
.method public constructor <init>(LX/0tX;LX/1mR;Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;)V
    .locals 0
    .param p3    # Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2677847
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2677848
    iput-object p1, p0, LX/JIN;->a:LX/0tX;

    .line 2677849
    iput-object p2, p0, LX/JIN;->b:LX/1mR;

    .line 2677850
    iput-object p3, p0, LX/JIN;->c:Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;

    .line 2677851
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/profile/discovery/protocol/CurationTagsMutationModels$CurationTagsMutationModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2677852
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2677853
    const/4 v0, 0x0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2677854
    :goto_0
    return-object v0

    .line 2677855
    :cond_0
    new-instance v0, LX/4Id;

    invoke-direct {v0}, LX/4Id;-><init>()V

    .line 2677856
    const-string v1, "actor_id"

    invoke-virtual {v0, v1, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2677857
    const-string v1, "add_ids"

    invoke-virtual {v0, v1, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 2677858
    const-string v1, "remove_ids"

    invoke-virtual {v0, v1, p3}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 2677859
    const-string v1, "typeahead_add_ids"

    invoke-virtual {v0, v1, p4}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 2677860
    iget-object v1, p0, LX/JIN;->c:Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;

    .line 2677861
    iget-object v2, v1, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->f:Ljava/lang/String;

    move-object v1, v2

    .line 2677862
    const-string v2, "referrer_type"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2677863
    iget-object v1, p0, LX/JIN;->c:Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;

    .line 2677864
    iget-object v2, v1, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;->e:Ljava/lang/String;

    move-object v1, v2

    .line 2677865
    const-string v2, "referrer_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2677866
    new-instance v1, LX/FSe;

    invoke-direct {v1}, LX/FSe;-><init>()V

    move-object v1, v1

    .line 2677867
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2677868
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 2677869
    iget-object v1, p0, LX/JIN;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2677870
    iget-object v1, p0, LX/JIN;->b:LX/1mR;

    const-string v2, "com.facebook.entitycardsplugins.discoverycuration.fetchers.discoveryCurationCardsCacheTag"

    invoke-static {v2}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1mR;->a(Ljava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method
