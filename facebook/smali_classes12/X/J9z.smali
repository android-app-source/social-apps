.class public final enum LX/J9z;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/J9z;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/J9z;

.field public static final enum CANNOT_JOIN_OR_REQUEST:LX/J9z;

.field public static final enum CAN_JOIN:LX/J9z;

.field public static final enum CAN_REQUEST:LX/J9z;

.field public static final enum MEMBER:LX/J9z;

.field public static final enum REQUESTED:LX/J9z;

.field public static final enum UNKNOWN:LX/J9z;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2653670
    new-instance v0, LX/J9z;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v3}, LX/J9z;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J9z;->UNKNOWN:LX/J9z;

    .line 2653671
    new-instance v0, LX/J9z;

    const-string v1, "CAN_JOIN"

    invoke-direct {v0, v1, v4}, LX/J9z;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J9z;->CAN_JOIN:LX/J9z;

    .line 2653672
    new-instance v0, LX/J9z;

    const-string v1, "CAN_REQUEST"

    invoke-direct {v0, v1, v5}, LX/J9z;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J9z;->CAN_REQUEST:LX/J9z;

    .line 2653673
    new-instance v0, LX/J9z;

    const-string v1, "CANNOT_JOIN_OR_REQUEST"

    invoke-direct {v0, v1, v6}, LX/J9z;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J9z;->CANNOT_JOIN_OR_REQUEST:LX/J9z;

    .line 2653674
    new-instance v0, LX/J9z;

    const-string v1, "REQUESTED"

    invoke-direct {v0, v1, v7}, LX/J9z;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J9z;->REQUESTED:LX/J9z;

    .line 2653675
    new-instance v0, LX/J9z;

    const-string v1, "MEMBER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/J9z;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J9z;->MEMBER:LX/J9z;

    .line 2653676
    const/4 v0, 0x6

    new-array v0, v0, [LX/J9z;

    sget-object v1, LX/J9z;->UNKNOWN:LX/J9z;

    aput-object v1, v0, v3

    sget-object v1, LX/J9z;->CAN_JOIN:LX/J9z;

    aput-object v1, v0, v4

    sget-object v1, LX/J9z;->CAN_REQUEST:LX/J9z;

    aput-object v1, v0, v5

    sget-object v1, LX/J9z;->CANNOT_JOIN_OR_REQUEST:LX/J9z;

    aput-object v1, v0, v6

    sget-object v1, LX/J9z;->REQUESTED:LX/J9z;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/J9z;->MEMBER:LX/J9z;

    aput-object v2, v0, v1

    sput-object v0, LX/J9z;->$VALUES:[LX/J9z;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2653669
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/J9z;
    .locals 1

    .prologue
    .line 2653662
    if-nez p0, :cond_0

    .line 2653663
    sget-object v0, LX/J9z;->UNKNOWN:LX/J9z;

    .line 2653664
    :goto_0
    return-object v0

    .line 2653665
    :cond_0
    :try_start_0
    invoke-static {p0}, LX/J9z;->valueOf(Ljava/lang/String;)LX/J9z;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 2653666
    :catch_0
    sget-object v0, LX/J9z;->UNKNOWN:LX/J9z;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/J9z;
    .locals 1

    .prologue
    .line 2653668
    const-class v0, LX/J9z;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/J9z;

    return-object v0
.end method

.method public static values()[LX/J9z;
    .locals 1

    .prologue
    .line 2653667
    sget-object v0, LX/J9z;->$VALUES:[LX/J9z;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/J9z;

    return-object v0
.end method
