.class public LX/Iv6;
.super LX/Iur;
.source ""


# instance fields
.field private a:Lcom/facebook/neko/util/AppShimmerFrameLayout;

.field public b:Landroid/view/View;

.field public c:Landroid/view/View;

.field public d:Lcom/facebook/resources/ui/FbTextView;

.field public e:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2627900
    invoke-direct {p0, p1}, LX/Iur;-><init>(Landroid/content/Context;)V

    .line 2627901
    invoke-virtual {p0}, LX/Iv6;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2627902
    const v1, 0x7f0300ec

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 2627903
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 2627904
    invoke-virtual {p0}, LX/Iv6;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    const v3, 0x7f0107f6

    invoke-virtual {v2, v3, v1, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 2627905
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    iget v1, v1, Landroid/util/TypedValue;->data:I

    invoke-direct {v2, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v2}, LX/Iv6;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2627906
    invoke-virtual {p0, v4}, LX/Iv6;->setOrientation(I)V

    .line 2627907
    const v1, 0x7f0d054e

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 2627908
    invoke-virtual {p0, v1}, LX/Iur;->a(Landroid/view/View;)V

    .line 2627909
    const v1, 0x7f0d0552

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/neko/util/AppShimmerFrameLayout;

    iput-object v1, p0, LX/Iv6;->a:Lcom/facebook/neko/util/AppShimmerFrameLayout;

    .line 2627910
    const v1, 0x7f0d0557

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, LX/Iv6;->b:Landroid/view/View;

    .line 2627911
    const v1, 0x7f0d0554

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, LX/Iv6;->c:Landroid/view/View;

    .line 2627912
    const v1, 0x7f0d0555

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    iput-object v1, p0, LX/Iv6;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 2627913
    const v1, 0x7f0d0556

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/Iv6;->e:Landroid/view/View;

    .line 2627914
    iget-object v0, p0, LX/Iv6;->a:Lcom/facebook/neko/util/AppShimmerFrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->setTilt(F)V

    .line 2627915
    iget-object v0, p0, LX/Iv6;->c:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2627916
    return-void
.end method


# virtual methods
.method public final a(LX/Iv7;ZZ)V
    .locals 5

    .prologue
    .line 2627921
    sget-object v0, LX/Iv7;->Error:LX/Iv7;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const/16 p1, 0x8

    const/4 v4, 0x4

    const/4 v2, 0x0

    .line 2627922
    if-nez v0, :cond_1

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {p0, v1}, LX/Iv6;->setShowLoading(Z)V

    .line 2627923
    if-eqz p3, :cond_2

    .line 2627924
    iget-object v1, p0, LX/Iv6;->d:Lcom/facebook/resources/ui/FbTextView;

    const v3, 0x7f0838c3

    invoke-virtual {v1, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2627925
    iget-object v1, p0, LX/Iv6;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2627926
    iget-object v1, p0, LX/Iv6;->e:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2627927
    :goto_2
    if-eqz p2, :cond_3

    if-eqz v0, :cond_3

    .line 2627928
    iget-object v1, p0, LX/Iv6;->b:Landroid/view/View;

    iget-object v2, p0, LX/Iv6;->c:Landroid/view/View;

    invoke-static {v1, v2, v4}, LX/Iut;->a(Landroid/view/View;Landroid/view/View;I)V

    .line 2627929
    :goto_3
    invoke-virtual {p0}, LX/Iv6;->requestLayout()V

    .line 2627930
    return-void

    .line 2627931
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    move v1, v2

    .line 2627932
    goto :goto_1

    .line 2627933
    :cond_2
    iget-object v1, p0, LX/Iv6;->d:Lcom/facebook/resources/ui/FbTextView;

    const v3, 0x7f0838c4

    invoke-virtual {v1, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2627934
    iget-object v1, p0, LX/Iv6;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2627935
    iget-object v1, p0, LX/Iv6;->e:Landroid/view/View;

    invoke-virtual {v1, p1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 2627936
    :cond_3
    if-eqz v0, :cond_4

    .line 2627937
    iget-object v1, p0, LX/Iv6;->b:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2627938
    iget-object v1, p0, LX/Iv6;->c:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    .line 2627939
    :cond_4
    iget-object v1, p0, LX/Iv6;->b:Landroid/view/View;

    .line 2627940
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    .line 2627941
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2627942
    iget-object v1, p0, LX/Iv6;->c:Landroid/view/View;

    invoke-virtual {v1, p1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3
.end method

.method public setShowLoading(Z)V
    .locals 1

    .prologue
    .line 2627917
    if-eqz p1, :cond_0

    .line 2627918
    iget-object v0, p0, LX/Iv6;->a:Lcom/facebook/neko/util/AppShimmerFrameLayout;

    invoke-virtual {v0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->a()V

    .line 2627919
    :goto_0
    return-void

    .line 2627920
    :cond_0
    iget-object v0, p0, LX/Iv6;->a:Lcom/facebook/neko/util/AppShimmerFrameLayout;

    invoke-virtual {v0}, Lcom/facebook/neko/util/AppShimmerFrameLayout;->b()V

    goto :goto_0
.end method
