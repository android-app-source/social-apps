.class public LX/HPs;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Landroid/content/res/Resources;

.field private final c:LX/0tX;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    .line 2462597
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->COPY_TAB_LINK:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->name()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageActionType;->DELETE_TAB:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->name()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->REORDER_TABS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->name()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageActionType;->SHARE_TAB:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->name()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPageActionType;->VISIT_PAGE:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->name()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/HPs;->a:LX/0Px;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;LX/0tX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2462598
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2462599
    iput-object p1, p0, LX/HPs;->b:Landroid/content/res/Resources;

    .line 2462600
    iput-object p2, p0, LX/HPs;->c:LX/0tX;

    .line 2462601
    return-void
.end method

.method public static a(JLcom/facebook/graphql/enums/GraphQLPageActionType;)Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/facebook/graphql/enums/GraphQLPageActionType;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2462602
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 2462603
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PagesTabDataFetcher_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0, p1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2462604
    return-object v0
.end method


# virtual methods
.method public final a(JLcom/facebook/graphql/enums/GraphQLPageActionType;ZZ)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/facebook/graphql/enums/GraphQLPageActionType;",
            "ZZ)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2462605
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2462606
    new-instance v0, LX/4CV;

    invoke-direct {v0}, LX/4CV;-><init>()V

    .line 2462607
    sget-object v1, LX/HPs;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/4CV;->a(Ljava/util/List;)LX/4CV;

    move-result-object v1

    invoke-virtual {p3}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->name()Ljava/lang/String;

    move-result-object v2

    .line 2462608
    const-string v3, "tab_action_type"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2462609
    move-object v1, v1

    .line 2462610
    invoke-static {p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 2462611
    const-string v3, "is_deeplink"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2462612
    new-instance v1, LX/CYU;

    invoke-direct {v1}, LX/CYU;-><init>()V

    move-object v1, v1

    .line 2462613
    const-string v2, "page_id"

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "tab_type"

    invoke-virtual {v1, v2, p3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v1

    const-string v2, "tab_admin_settings_channel_context"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    const-string v1, "cta_icon_size"

    iget-object v2, p0, LX/HPs;->b:Landroid/content/res/Resources;

    const v3, 0x7f0b0f8e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "cta_icon_scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/CYU;

    .line 2462614
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    if-eqz p4, :cond_1

    sget-object v1, LX/0zS;->a:LX/0zS;

    :goto_1
    invoke-virtual {v2, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    const-wide/16 v2, 0xe10

    invoke-virtual {v1, v2, v3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v1

    invoke-static {p1, p2, p3}, LX/HPs;->a(JLcom/facebook/graphql/enums/GraphQLPageActionType;)Ljava/util/Set;

    move-result-object v2

    .line 2462615
    iput-object v2, v1, LX/0zO;->d:Ljava/util/Set;

    .line 2462616
    move-object v1, v1

    .line 2462617
    new-instance v2, LX/HPr;

    invoke-direct {v2, v0}, LX/HPr;-><init>(LX/CYU;)V

    invoke-virtual {v1, v2}, LX/0zO;->a(LX/0zT;)LX/0zO;

    move-result-object v0

    .line 2462618
    iget-object v1, p0, LX/HPs;->c:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0

    .line 2462619
    :cond_0
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 2462620
    :cond_1
    sget-object v1, LX/0zS;->d:LX/0zS;

    goto :goto_1
.end method
