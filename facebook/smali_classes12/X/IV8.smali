.class public LX/IV8;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/IV6;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/IV9;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2581371
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/IV8;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/IV9;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2581372
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2581373
    iput-object p1, p0, LX/IV8;->b:LX/0Ot;

    .line 2581374
    return-void
.end method

.method public static a(LX/0QB;)LX/IV8;
    .locals 4

    .prologue
    .line 2581375
    const-class v1, LX/IV8;

    monitor-enter v1

    .line 2581376
    :try_start_0
    sget-object v0, LX/IV8;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2581377
    sput-object v2, LX/IV8;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2581378
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2581379
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2581380
    new-instance v3, LX/IV8;

    const/16 p0, 0x2421

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/IV8;-><init>(LX/0Ot;)V

    .line 2581381
    move-object v0, v3

    .line 2581382
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2581383
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IV8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2581384
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2581385
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 11

    .prologue
    .line 2581386
    check-cast p2, LX/IV7;

    .line 2581387
    iget-object v0, p0, LX/IV8;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IV9;

    iget-object v1, p2, LX/IV7;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    iget-object v2, p2, LX/IV7;->b:LX/IRb;

    const/4 v7, 0x0

    const/16 v5, 0xa

    const/4 v6, 0x1

    const/4 v4, 0x5

    const/high16 p2, 0x3f800000    # 1.0f

    .line 2581388
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const/4 v8, 0x2

    invoke-interface {v3, v8}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    const/4 v8, 0x2

    invoke-interface {v3, v8}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v6}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v3

    const/4 v8, 0x7

    const/16 v9, 0x1e

    invoke-interface {v3, v8, v9}, LX/1Dh;->o(II)LX/1Dh;

    move-result-object v8

    .line 2581389
    invoke-static {v1}, LX/IQY;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Z

    move-result v3

    .line 2581390
    iget-object v9, v0, LX/IV9;->c:LX/2g9;

    invoke-virtual {v9, p1}, LX/2g9;->c(LX/1De;)LX/2gA;

    move-result-object v9

    const/16 v10, 0x18

    invoke-virtual {v9, v10}, LX/2gA;->h(I)LX/2gA;

    move-result-object v9

    .line 2581391
    if-eqz v3, :cond_3

    .line 2581392
    iget-object v3, v0, LX/IV9;->d:LX/IQY;

    invoke-virtual {v3, v1, v2}, LX/IQY;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;LX/IRb;)LX/IQB;

    move-result-object v10

    .line 2581393
    if-nez v10, :cond_0

    .line 2581394
    const/4 v3, 0x0

    .line 2581395
    :goto_0
    move-object v0, v3

    .line 2581396
    return-object v0

    .line 2581397
    :cond_0
    invoke-interface {v10}, LX/IQB;->c()Ljava/lang/String;

    move-result-object v3

    .line 2581398
    new-array p0, v6, [Ljava/lang/CharSequence;

    aput-object v3, p0, v7

    invoke-static {p0}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    move v3, v6

    .line 2581399
    :goto_1
    if-eqz v3, :cond_1

    .line 2581400
    iget-object v6, v0, LX/IV9;->c:LX/2g9;

    invoke-virtual {v6, p1}, LX/2g9;->c(LX/1De;)LX/2gA;

    move-result-object v6

    const/16 v7, 0x108

    invoke-virtual {v6, v7}, LX/2gA;->h(I)LX/2gA;

    move-result-object v6

    invoke-interface {v10}, LX/IQB;->c()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/2gA;->a(Ljava/lang/CharSequence;)LX/2gA;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    const v7, 0x7f0b1214

    invoke-interface {v6, v7}, LX/1Di;->q(I)LX/1Di;

    move-result-object v6

    const/4 v7, 0x4

    invoke-interface {v6, v7, v5}, LX/1Di;->d(II)LX/1Di;

    move-result-object v6

    invoke-interface {v6, v4, v4}, LX/1Di;->d(II)LX/1Di;

    move-result-object v6

    .line 2581401
    const v7, 0x7cf6c9bd

    const/4 p0, 0x0

    invoke-static {p1, v7, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v7

    move-object v7, v7

    .line 2581402
    invoke-interface {v6, v7}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v6

    invoke-interface {v6, p2}, LX/1Di;->b(F)LX/1Di;

    move-result-object v6

    invoke-interface {v6, p2}, LX/1Di;->a(F)LX/1Di;

    move-result-object v6

    invoke-interface {v8, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2581403
    :cond_1
    invoke-interface {v10}, LX/IQB;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v9, v6}, LX/2gA;->a(Ljava/lang/CharSequence;)LX/2gA;

    .line 2581404
    :goto_2
    invoke-virtual {v9}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    const v7, 0x7f0b1214

    invoke-interface {v6, v7}, LX/1Di;->q(I)LX/1Di;

    move-result-object v6

    const/4 v7, 0x4

    if-eqz v3, :cond_6

    move v3, v4

    :goto_3
    invoke-interface {v6, v7, v3}, LX/1Di;->d(II)LX/1Di;

    move-result-object v3

    invoke-interface {v3, v4, v5}, LX/1Di;->d(II)LX/1Di;

    move-result-object v3

    .line 2581405
    const v4, 0x7cf6d17d

    const/4 v5, 0x0

    invoke-static {p1, v4, v5}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v4, v4

    .line 2581406
    invoke-interface {v3, v4}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v3

    invoke-interface {v3, p2}, LX/1Di;->b(F)LX/1Di;

    move-result-object v3

    invoke-interface {v3, p2}, LX/1Di;->a(F)LX/1Di;

    move-result-object v3

    invoke-interface {v8, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2581407
    invoke-interface {v8}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    goto/16 :goto_0

    :cond_2
    move v3, v7

    .line 2581408
    goto :goto_1

    .line 2581409
    :cond_3
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v6

    if-eqz v6, :cond_4

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v6

    if-nez v6, :cond_7

    .line 2581410
    :cond_4
    const/4 v6, 0x0

    .line 2581411
    :cond_5
    :goto_4
    move v6, v6

    .line 2581412
    invoke-virtual {v9, v6}, LX/2gA;->i(I)LX/2gA;

    goto :goto_2

    :cond_6
    move v3, v5

    .line 2581413
    goto :goto_3

    .line 2581414
    :cond_7
    iget-object v6, v0, LX/IV9;->b:LX/3my;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/3my;->d(Lcom/facebook/graphql/enums/GraphQLGroupCategory;)Z

    move-result v6

    .line 2581415
    iget-object v7, v0, LX/IV9;->b:LX/3my;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v10

    invoke-virtual {v7, v10}, LX/3my;->b(Lcom/facebook/graphql/enums/GraphQLGroupCategory;)Z

    move-result v7

    .line 2581416
    if-eqz v6, :cond_8

    const v6, 0x7f081c1d

    .line 2581417
    :goto_5
    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v7

    sget-object v10, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v7, v10}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    const v6, 0x7f081c1e    # 1.80921E38f

    goto :goto_4

    .line 2581418
    :cond_8
    if-eqz v7, :cond_9

    const v6, 0x7f081c1c

    goto :goto_5

    :cond_9
    const v6, 0x7f081c1b

    goto :goto_5
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2581419
    invoke-static {}, LX/1dS;->b()V

    .line 2581420
    iget v0, p1, LX/1dQ;->b:I

    .line 2581421
    sparse-switch v0, :sswitch_data_0

    .line 2581422
    :goto_0
    return-object v2

    .line 2581423
    :sswitch_0
    check-cast p2, LX/3Ae;

    .line 2581424
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2581425
    check-cast v1, LX/IV7;

    .line 2581426
    iget-object v3, p0, LX/IV8;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/IV9;

    iget-object v4, v1, LX/IV7;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    iget-object p1, v1, LX/IV7;->b:LX/IRb;

    .line 2581427
    invoke-static {v4}, LX/IQY;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Z

    move-result p2

    if-eqz p2, :cond_3

    .line 2581428
    iget-object p2, v3, LX/IV9;->d:LX/IQY;

    invoke-virtual {p2, v4, p1}, LX/IQY;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;LX/IRb;)LX/IQB;

    move-result-object p2

    .line 2581429
    if-nez p2, :cond_2

    .line 2581430
    :cond_0
    :goto_1
    goto :goto_0

    .line 2581431
    :sswitch_1
    check-cast p2, LX/3Ae;

    .line 2581432
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2581433
    check-cast v1, LX/IV7;

    .line 2581434
    iget-object v3, p0, LX/IV8;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/IV9;

    iget-object v4, v1, LX/IV7;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    iget-object p1, v1, LX/IV7;->b:LX/IRb;

    .line 2581435
    iget-object p0, v3, LX/IV9;->d:LX/IQY;

    invoke-virtual {p0, v4, p1}, LX/IQY;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;LX/IRb;)LX/IQB;

    move-result-object p0

    .line 2581436
    if-eqz p0, :cond_1

    invoke-interface {p0}, LX/IQB;->f()Landroid/view/View$OnClickListener;

    move-result-object v1

    if-nez v1, :cond_5

    .line 2581437
    :cond_1
    :goto_2
    goto :goto_0

    .line 2581438
    :cond_2
    invoke-interface {p2}, LX/IQB;->e()Landroid/view/View$OnClickListener;

    move-result-object p2

    invoke-interface {p2, v0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    goto :goto_1

    .line 2581439
    :cond_3
    iget-object p2, v3, LX/IV9;->a:LX/ITK;

    invoke-virtual {p2, p1}, LX/ITK;->a(LX/IRb;)LX/ITJ;

    move-result-object p2

    .line 2581440
    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object p0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {p0, v1}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_4

    .line 2581441
    invoke-virtual {p2, v4}, LX/ITJ;->d(LX/9N6;)V

    .line 2581442
    iget-object p2, v3, LX/IV9;->f:LX/0Ot;

    invoke-interface {p2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/0Uh;

    const/16 p0, 0x3c2

    const/4 v1, 0x0

    invoke-virtual {p2, p0, v1}, LX/0Uh;->a(IZ)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 2581443
    iget-object p2, v3, LX/IV9;->e:LX/0Ot;

    invoke-interface {p2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/B1P;

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object p0

    .line 2581444
    new-instance v1, LX/B1M;

    invoke-direct {v1, p2, p0}, LX/B1M;-><init>(LX/B1P;Ljava/lang/String;)V

    .line 2581445
    new-instance v3, LX/B1N;

    invoke-direct {v3, p2}, LX/B1N;-><init>(LX/B1P;)V

    .line 2581446
    iget-object v0, p2, LX/B1P;->a:LX/1Ck;

    sget-object v4, LX/B1O;->GROUP_SUGGESTIONS_CHAINING_UNIT_QUERY:LX/B1O;

    invoke-virtual {v0, v4, v1, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2581447
    goto :goto_1

    .line 2581448
    :cond_4
    invoke-virtual {p2, v4}, LX/ITJ;->e(LX/9N6;)V

    goto :goto_1

    .line 2581449
    :cond_5
    invoke-interface {p0}, LX/IQB;->f()Landroid/view/View$OnClickListener;

    move-result-object p0

    invoke-interface {p0, v0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    goto :goto_2

    :sswitch_data_0
    .sparse-switch
        0x7cf6c9bd -> :sswitch_1
        0x7cf6d17d -> :sswitch_0
    .end sparse-switch
.end method
