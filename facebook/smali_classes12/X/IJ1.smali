.class public final LX/IJ1;
.super LX/IIn;
.source ""


# instance fields
.field public final synthetic b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V
    .locals 1

    .prologue
    .line 2562365
    iput-object p1, p0, LX/IJ1;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-direct {p0, p1}, LX/IIn;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    return-void
.end method


# virtual methods
.method public final e()V
    .locals 2

    .prologue
    .line 2562366
    iget-object v0, p0, LX/IJ1;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-static {v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->M(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    .line 2562367
    iget-object v0, p0, LX/IJ1;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Z:LX/IFX;

    invoke-virtual {v0}, LX/IFX;->a()V

    .line 2562368
    iget-object v0, p0, LX/IJ1;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, p0, LX/IJ1;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aw:LX/IIm;

    .line 2562369
    invoke-static {v0, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->a$redex1(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;LX/IIm;)V

    .line 2562370
    return-void
.end method

.method public final f()V
    .locals 12

    .prologue
    .line 2562371
    iget-object v0, p0, LX/IJ1;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-static {v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->I(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    .line 2562372
    iget-object v0, p0, LX/IJ1;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-static {v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->J(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    .line 2562373
    iget-object v0, p0, LX/IJ1;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-static {v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->D(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    .line 2562374
    iget-object v0, p0, LX/IJ1;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    const/4 v4, 0x1

    const/4 v8, 0x0

    .line 2562375
    iget-object v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Z:LX/IFX;

    .line 2562376
    iget-object v3, v2, LX/IFX;->i:LX/IFx;

    move-object v2, v3

    .line 2562377
    iput-boolean v4, v2, LX/IFx;->e:Z

    .line 2562378
    iget-object v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->T:Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;

    iget-object v3, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Z:LX/IFX;

    .line 2562379
    iget-object v5, v3, LX/IFX;->i:LX/IFx;

    move-object v3, v5

    .line 2562380
    invoke-virtual {v2, v3, v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;->a(LX/IFx;LX/IJ4;)V

    .line 2562381
    iget-object v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->T:Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;

    const v3, 0x7f083877

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->I:LX/GUm;

    iget-object v6, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Z:LX/IFX;

    .line 2562382
    iget-wide v9, v6, LX/IFX;->A:J

    move-wide v6, v9

    .line 2562383
    iget-object v9, v5, LX/GUm;->b:LX/11S;

    sget-object v10, LX/1lB;->EXACT_TIME_DATE_LOWERCASE_STYLE:LX/1lB;

    invoke-interface {v9, v10, v6, v7}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v9

    move-object v5, v9

    .line 2562384
    aput-object v5, v4, v8

    invoke-virtual {v0, v3, v4}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;->setContextText(Ljava/lang/CharSequence;)V

    .line 2562385
    iget-object v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->T:Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;

    new-instance v3, LX/IIb;

    invoke-direct {v3, v0}, LX/IIb;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    invoke-virtual {v2, v3}, Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;->setResumeButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2562386
    iget-object v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->T:Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;

    invoke-virtual {v2, v8}, Lcom/facebook/friendsnearby/ui/FriendsNearbyPauseView;->setVisibility(I)V

    .line 2562387
    iget-object v0, p0, LX/IJ1;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->n:LX/IID;

    sget-object v1, LX/IIC;->PAUSE:LX/IIC;

    invoke-virtual {v0, v1}, LX/IID;->a(LX/IIC;)V

    .line 2562388
    return-void
.end method

.method public final r()V
    .locals 4

    .prologue
    .line 2562389
    iget-object v0, p0, LX/IJ1;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->n:LX/IID;

    invoke-virtual {v0}, LX/IID;->d()V

    .line 2562390
    iget-object v0, p0, LX/IJ1;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->E:LX/IIB;

    const/4 v1, 0x0

    iget-object v2, p0, LX/IJ1;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-virtual {v2}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v2

    iget-object v3, p0, LX/IJ1;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v3, v3, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aK:LX/IIm;

    invoke-virtual {v0, v1, v2, v3}, LX/IIB;->a(Lcom/facebook/widget/SwitchCompat;LX/0gc;LX/IIm;)V

    .line 2562391
    return-void
.end method
