.class public final LX/Ivn;
.super LX/0xh;
.source ""


# instance fields
.field public final synthetic a:LX/Ivo;


# direct methods
.method public constructor <init>(LX/Ivo;)V
    .locals 0

    .prologue
    .line 2629189
    iput-object p1, p0, LX/Ivn;->a:LX/Ivo;

    invoke-direct {p0}, LX/0xh;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 4

    .prologue
    .line 2629190
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    double-to-float v0, v0

    .line 2629191
    iget-object v1, p0, LX/Ivn;->a:LX/Ivo;

    iget-object v1, v1, LX/Ivo;->i:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setScaleX(F)V

    .line 2629192
    iget-object v1, p0, LX/Ivn;->a:LX/Ivo;

    iget-object v1, v1, LX/Ivo;->i:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setScaleY(F)V

    .line 2629193
    iget-object v1, p0, LX/Ivn;->a:LX/Ivo;

    iget-boolean v1, v1, LX/Ivo;->f:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/Ivn;->a:LX/Ivo;

    iget v1, v1, LX/Ivo;->e:F

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    .line 2629194
    iget-object v0, p0, LX/Ivn;->a:LX/Ivo;

    iget-object v0, v0, LX/Ivo;->n:LX/0wd;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 2629195
    iget-object v0, p0, LX/Ivn;->a:LX/Ivo;

    const/4 v1, 0x0

    .line 2629196
    iput-boolean v1, v0, LX/Ivo;->f:Z

    .line 2629197
    :cond_0
    return-void
.end method

.method public final b(LX/0wd;)V
    .locals 6

    .prologue
    .line 2629198
    iget-object v0, p0, LX/Ivn;->a:LX/Ivo;

    iget-boolean v0, v0, LX/Ivo;->f:Z

    if-nez v0, :cond_0

    .line 2629199
    iget-object v1, p0, LX/Ivn;->a:LX/Ivo;

    iget-object v1, v1, LX/Ivo;->i:Landroid/view/View;

    invoke-static {v1}, LX/0wc;->b(Landroid/view/View;)V

    .line 2629200
    :cond_0
    iget-object v0, p0, LX/Ivn;->a:LX/Ivo;

    iget-boolean v0, v0, LX/Ivo;->h:Z

    if-eqz v0, :cond_1

    .line 2629201
    iget-wide v4, p1, LX/0wd;->i:D

    move-wide v0, v4

    .line 2629202
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_1

    .line 2629203
    iget-object v0, p0, LX/Ivn;->a:LX/Ivo;

    iget-object v0, v0, LX/Ivo;->j:LX/IvW;

    iget-object v1, p0, LX/Ivn;->a:LX/Ivo;

    iget-object v1, v1, LX/Ivo;->i:Landroid/view/View;

    invoke-virtual {v0, v1}, LX/IvW;->a(Landroid/view/View;)Z

    .line 2629204
    iget-object v0, p0, LX/Ivn;->a:LX/Ivo;

    const/4 v1, 0x0

    .line 2629205
    iput-boolean v1, v0, LX/Ivo;->h:Z

    .line 2629206
    :cond_1
    return-void
.end method

.method public final c(LX/0wd;)V
    .locals 2

    .prologue
    .line 2629207
    iget-object v1, p0, LX/Ivn;->a:LX/Ivo;

    iget-object v1, v1, LX/Ivo;->i:Landroid/view/View;

    invoke-static {v1}, LX/0wc;->a(Landroid/view/View;)V

    .line 2629208
    return-void
.end method
