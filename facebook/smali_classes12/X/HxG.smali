.class public LX/HxG;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/23P;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/7v8;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/HxN;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/I76;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/6RZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0iA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1ay;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:Lcom/facebook/base/fragment/FbFragment;

.field public k:Lcom/facebook/fbui/widget/contentview/ContentView;

.field public l:Lcom/facebook/widget/text/BetterTextView;

.field public m:Lcom/facebook/fbui/glyph/GlyphView;

.field public n:Lcom/facebook/fbui/glyph/GlyphView;

.field public o:Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;

.field public p:Z

.field public q:Z

.field public r:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 10

    .prologue
    .line 2522212
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2522213
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, LX/HxG;

    invoke-static {v0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v3

    check-cast v3, LX/23P;

    invoke-static {v0}, LX/7v8;->a(LX/0QB;)LX/7v8;

    move-result-object v4

    check-cast v4, LX/7v8;

    invoke-static {v0}, LX/HxN;->b(LX/0QB;)LX/HxN;

    move-result-object v5

    check-cast v5, LX/HxN;

    invoke-static {v0}, LX/I76;->b(LX/0QB;)LX/I76;

    move-result-object v6

    check-cast v6, LX/I76;

    invoke-static {v0}, LX/6RZ;->a(LX/0QB;)LX/6RZ;

    move-result-object v7

    check-cast v7, LX/6RZ;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v8

    check-cast v8, LX/0wM;

    invoke-static {v0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v9

    check-cast v9, LX/0iA;

    const/16 p1, 0xbde

    invoke-static {v0, p1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iput-object v3, v2, LX/HxG;->a:LX/23P;

    iput-object v4, v2, LX/HxG;->b:LX/7v8;

    iput-object v5, v2, LX/HxG;->c:LX/HxN;

    iput-object v6, v2, LX/HxG;->d:LX/I76;

    iput-object v7, v2, LX/HxG;->e:LX/6RZ;

    iput-object v8, v2, LX/HxG;->f:LX/0wM;

    iput-object v9, v2, LX/HxG;->g:LX/0iA;

    iput-object p1, v2, LX/HxG;->h:LX/0Ot;

    iput-object v0, v2, LX/HxG;->i:Lcom/facebook/content/SecureContextHelper;

    .line 2522214
    const v0, 0x7f030551

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2522215
    invoke-virtual {p0}, LX/HxG;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b15a9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2522216
    invoke-virtual {p0}, LX/HxG;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b15aa

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2522217
    invoke-virtual {p0}, LX/HxG;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b15ab

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2522218
    invoke-virtual {p0, v2, v0, v2, v1}, LX/HxG;->setPadding(IIII)V

    .line 2522219
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/HxG;->setOrientation(I)V

    .line 2522220
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, LX/HxG;->setBackgroundColor(I)V

    .line 2522221
    const v0, 0x7f021903

    invoke-virtual {p0, v0}, LX/HxG;->setBackgroundResource(I)V

    .line 2522222
    new-instance v0, LX/HxB;

    invoke-direct {v0, p0}, LX/HxB;-><init>(LX/HxG;)V

    .line 2522223
    invoke-virtual {p0, v0}, LX/HxG;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2522224
    const v0, 0x7f0d057e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    iput-object v0, p0, LX/HxG;->k:Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2522225
    iget-object v0, p0, LX/HxG;->k:Lcom/facebook/fbui/widget/contentview/ContentView;

    const v1, 0x7f0a06cc

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailResource(I)V

    .line 2522226
    const/4 v3, 0x0

    .line 2522227
    const v0, 0x7f0d0ef3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/HxG;->l:Lcom/facebook/widget/text/BetterTextView;

    .line 2522228
    const v0, 0x7f0d0ef2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    .line 2522229
    const v1, 0x7f020671

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setBackgroundResource(I)V

    .line 2522230
    invoke-virtual {p0}, LX/HxG;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b15af

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    .line 2522231
    iget-object v2, p0, LX/HxG;->l:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, v1, v3, v3, v3}, Lcom/facebook/widget/text/BetterTextView;->setPadding(IIII)V

    .line 2522232
    new-instance v1, LX/HxC;

    invoke-direct {v1, p0}, LX/HxC;-><init>(LX/HxG;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2522233
    new-instance v0, LX/HxF;

    invoke-direct {v0, p0}, LX/HxF;-><init>(LX/HxG;)V

    move-object v1, v0

    .line 2522234
    const v0, 0x7f0d08fc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/HxG;->m:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2522235
    new-instance v0, LX/HxE;

    invoke-direct {v0, p0}, LX/HxE;-><init>(LX/HxG;)V

    move-object v0, v0

    .line 2522236
    iget-object v2, p0, LX/HxG;->m:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v2, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2522237
    iget-object v0, p0, LX/HxG;->m:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2522238
    const v0, 0x7f0d0ef4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/HxG;->n:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2522239
    new-instance v0, LX/HxD;

    invoke-direct {v0, p0}, LX/HxD;-><init>(LX/HxG;)V

    move-object v0, v0

    .line 2522240
    iget-object v2, p0, LX/HxG;->n:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v2, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2522241
    iget-object v0, p0, LX/HxG;->n:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2522242
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;Lcom/facebook/base/fragment/FbFragment;ZZLjava/lang/String;)V
    .locals 15

    .prologue
    .line 2522243
    move-object/from16 v0, p1

    iput-object v0, p0, LX/HxG;->o:Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;

    .line 2522244
    move-object/from16 v0, p2

    iput-object v0, p0, LX/HxG;->j:Lcom/facebook/base/fragment/FbFragment;

    .line 2522245
    move-object/from16 v0, p5

    iput-object v0, p0, LX/HxG;->r:Ljava/lang/String;

    .line 2522246
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;->n()Ljava/lang/String;

    move-result-object v3

    .line 2522247
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 2522248
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;->p()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    .line 2522249
    if-nez v2, :cond_3

    const/4 v6, 0x0

    .line 2522250
    :goto_0
    new-instance v8, Ljava/util/Date;

    invoke-direct {v8}, Ljava/util/Date;-><init>()V

    .line 2522251
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;->j()LX/1vs;

    move-result-object v2

    iget-object v9, v2, LX/1vs;->a:LX/15i;

    iget v10, v2, LX/1vs;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2522252
    if-eqz v10, :cond_2

    .line 2522253
    const-string v5, ""

    .line 2522254
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v9, v10, v4}, LX/15i;->j(II)I

    move-result v4

    const/4 v11, 0x1

    invoke-virtual {v9, v10, v11}, LX/15i;->j(II)I

    move-result v11

    invoke-static {v8, v2, v4, v11}, LX/6RS;->a(Ljava/util/Date;Ljava/util/TimeZone;II)Ljava/util/Calendar;

    move-result-object v4

    .line 2522255
    invoke-virtual {v4}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Calendar;

    .line 2522256
    const/4 v11, 0x1

    const/4 v12, -0x1

    invoke-virtual {v2, v11, v12}, Ljava/util/Calendar;->roll(II)V

    .line 2522257
    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v12

    invoke-static {v12, v13}, Landroid/text/format/DateUtils;->isToday(J)Z

    move-result v11

    iput-boolean v11, p0, LX/HxG;->p:Z

    .line 2522258
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v11

    .line 2522259
    const/4 v12, 0x2

    invoke-virtual {v9, v10, v12}, LX/15i;->j(II)I

    move-result v12

    const/4 v13, 0x1

    invoke-virtual {v9, v10, v13}, LX/15i;->j(II)I

    move-result v13

    add-int/lit8 v13, v13, -0x1

    const/4 v14, 0x0

    invoke-virtual {v9, v10, v14}, LX/15i;->j(II)I

    move-result v14

    invoke-virtual {v11, v12, v13, v14}, Ljava/util/Calendar;->set(III)V

    .line 2522260
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v12

    invoke-static {v12, v11}, LX/6RS;->a(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v11

    iput-boolean v11, p0, LX/HxG;->q:Z

    .line 2522261
    iget-boolean v11, p0, LX/HxG;->q:Z

    if-eqz v11, :cond_4

    .line 2522262
    :goto_1
    if-eqz p3, :cond_5

    .line 2522263
    iget-object v4, p0, LX/HxG;->e:LX/6RZ;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v11

    invoke-virtual {v4, v11, v8}, LX/6RZ;->b(Ljava/util/Date;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    .line 2522264
    :goto_2
    const/4 v8, 0x2

    invoke-virtual {v9, v10, v8}, LX/15i;->j(II)I

    move-result v8

    if-lez v8, :cond_0

    .line 2522265
    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Ljava/util/Calendar;->get(I)I

    move-result v2

    const/4 v5, 0x2

    invoke-virtual {v9, v10, v5}, LX/15i;->j(II)I

    move-result v5

    sub-int v5, v2, v5

    .line 2522266
    iget-boolean v2, p0, LX/HxG;->q:Z

    if-eqz v2, :cond_6

    const v2, 0x7f0f0105

    .line 2522267
    :goto_3
    invoke-virtual {p0}, LX/HxG;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v8, v2, v5, v9}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    :cond_0
    move-object v2, p0

    .line 2522268
    invoke-virtual/range {v2 .. v7}, LX/HxG;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)V

    .line 2522269
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;->l()Z

    move-result v2

    if-nez v2, :cond_1

    if-eqz p4, :cond_8

    :cond_1
    const/4 v2, 0x1

    :goto_4
    invoke-virtual {p0, v2}, LX/HxG;->setComposeViews(Z)V

    .line 2522270
    :cond_2
    return-void

    .line 2522271
    :cond_3
    invoke-virtual {v2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_0

    .line 2522272
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    :cond_4
    move-object v2, v4

    .line 2522273
    goto :goto_1

    .line 2522274
    :cond_5
    iget-object v4, p0, LX/HxG;->e:LX/6RZ;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v8

    invoke-virtual {v4, v8}, LX/6RZ;->b(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    .line 2522275
    :cond_6
    iget-boolean v2, p0, LX/HxG;->p:Z

    if-eqz v2, :cond_7

    const v2, 0x7f0f0103

    goto :goto_3

    :cond_7
    const v2, 0x7f0f0104

    goto :goto_3

    .line 2522276
    :cond_8
    const/4 v2, 0x0

    goto :goto_4
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)V
    .locals 2

    .prologue
    .line 2522277
    iget-object v1, p0, LX/HxG;->k:Lcom/facebook/fbui/widget/contentview/ContentView;

    if-nez p4, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 2522278
    iget-object v0, p0, LX/HxG;->k:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2522279
    iget-object v0, p0, LX/HxG;->k:Lcom/facebook/fbui/widget/contentview/ContentView;

    const v1, 0x7f0e08cf

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleTextAppearance(I)V

    .line 2522280
    iget-object v0, p0, LX/HxG;->k:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v0, p2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2522281
    iget-object v0, p0, LX/HxG;->k:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v0, p3}, Lcom/facebook/fbui/widget/contentview/ContentView;->setMetaText(Ljava/lang/CharSequence;)V

    .line 2522282
    iget-boolean v0, p0, LX/HxG;->p:Z

    if-eqz v0, :cond_1

    .line 2522283
    iget-object v0, p0, LX/HxG;->k:Lcom/facebook/fbui/widget/contentview/ContentView;

    const v1, 0x7f0e08d1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleTextAppearance(I)V

    .line 2522284
    :goto_1
    invoke-virtual {p5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2522285
    const-string v0, "\n"

    invoke-virtual {p5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2522286
    const-string v0, "\n"

    invoke-virtual {p5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2522287
    iget-object v0, p0, LX/HxG;->k:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {p5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2522288
    return-void

    .line 2522289
    :cond_0
    invoke-static {p4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 2522290
    :cond_1
    iget-object v0, p0, LX/HxG;->k:Lcom/facebook/fbui/widget/contentview/ContentView;

    const v1, 0x7f0e08d0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleTextAppearance(I)V

    goto :goto_1
.end method

.method public setComposeViews(Z)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/16 v3, 0x8

    .line 2522291
    iget-boolean v0, p0, LX/HxG;->p:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/HxG;->q:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    move v1, v0

    .line 2522292
    :goto_0
    const v0, 0x7f0d0ef0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/ImageWithTextView;

    .line 2522293
    if-eqz p1, :cond_2

    .line 2522294
    iget-object v1, p0, LX/HxG;->f:LX/0wM;

    const v4, 0x7f0207d6

    const v5, -0x423e37

    invoke-virtual {v1, v4, v5}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2522295
    iget-object v1, p0, LX/HxG;->a:LX/23P;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 2522296
    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setVisibility(I)V

    .line 2522297
    iget-object v0, p0, LX/HxG;->l:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2522298
    iget-object v0, p0, LX/HxG;->m:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2522299
    iget-object v0, p0, LX/HxG;->n:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2522300
    :goto_1
    return-void

    :cond_1
    move v1, v2

    .line 2522301
    goto :goto_0

    .line 2522302
    :cond_2
    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setVisibility(I)V

    .line 2522303
    iget-object v0, p0, LX/HxG;->l:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0}, LX/HxG;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0821b4    # 1.8095E38f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2522304
    iget-object v0, p0, LX/HxG;->o:Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;->k()Z

    move-result v0

    if-eqz v0, :cond_3

    if-eqz v1, :cond_3

    .line 2522305
    :goto_2
    iget-object v0, p0, LX/HxG;->l:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2522306
    iget-object v0, p0, LX/HxG;->m:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2522307
    iget-object v0, p0, LX/HxG;->n:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto :goto_1

    :cond_3
    move v2, v3

    .line 2522308
    goto :goto_2
.end method
