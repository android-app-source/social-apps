.class public LX/Iu4;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:Landroid/content/Context;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2625242
    const-class v0, LX/Iu4;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Iu4;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2625236
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2625237
    iput-object p1, p0, LX/Iu4;->b:Landroid/content/Context;

    .line 2625238
    return-void
.end method

.method public static a(LX/0QB;)LX/Iu4;
    .locals 3

    .prologue
    .line 2625239
    new-instance v1, LX/Iu4;

    const-class v0, Landroid/content/Context;

    const-class v2, Lcom/facebook/inject/ForAppContext;

    invoke-interface {p0, v0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LX/Iu4;-><init>(Landroid/content/Context;)V

    .line 2625240
    move-object v0, v1

    .line 2625241
    return-object v0
.end method
