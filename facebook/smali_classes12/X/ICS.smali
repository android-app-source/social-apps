.class public final enum LX/ICS;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ICS;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ICS;

.field public static final enum BAKEOFF_NEUTRAL:LX/ICS;

.field public static final enum BAKEOFF_PREF_FIRST:LX/ICS;

.field public static final enum BAKEOFF_PREF_SECOND:LX/ICS;

.field public static final enum PREF_FIRST:LX/ICS;

.field public static final enum PREF_SECOND:LX/ICS;

.field public static final enum PREF_SKIP:LX/ICS;


# instance fields
.field private final mRating:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2549413
    new-instance v0, LX/ICS;

    const-string v1, "PREF_FIRST"

    const-string v2, "PREF_FIRST"

    invoke-direct {v0, v1, v4, v2}, LX/ICS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ICS;->PREF_FIRST:LX/ICS;

    .line 2549414
    new-instance v0, LX/ICS;

    const-string v1, "PREF_SECOND"

    const-string v2, "PREF_SECOND"

    invoke-direct {v0, v1, v5, v2}, LX/ICS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ICS;->PREF_SECOND:LX/ICS;

    .line 2549415
    new-instance v0, LX/ICS;

    const-string v1, "PREF_SKIP"

    const-string v2, "PREF_SKIP"

    invoke-direct {v0, v1, v6, v2}, LX/ICS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ICS;->PREF_SKIP:LX/ICS;

    .line 2549416
    new-instance v0, LX/ICS;

    const-string v1, "BAKEOFF_PREF_FIRST"

    const-string v2, "BAKEOFF_PREF_FIRST"

    invoke-direct {v0, v1, v7, v2}, LX/ICS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ICS;->BAKEOFF_PREF_FIRST:LX/ICS;

    .line 2549417
    new-instance v0, LX/ICS;

    const-string v1, "BAKEOFF_PREF_SECOND"

    const-string v2, "BAKEOFF_PREF_SECOND"

    invoke-direct {v0, v1, v8, v2}, LX/ICS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ICS;->BAKEOFF_PREF_SECOND:LX/ICS;

    .line 2549418
    new-instance v0, LX/ICS;

    const-string v1, "BAKEOFF_NEUTRAL"

    const/4 v2, 0x5

    const-string v3, "BAKEOFF_NEUTRAL"

    invoke-direct {v0, v1, v2, v3}, LX/ICS;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ICS;->BAKEOFF_NEUTRAL:LX/ICS;

    .line 2549419
    const/4 v0, 0x6

    new-array v0, v0, [LX/ICS;

    sget-object v1, LX/ICS;->PREF_FIRST:LX/ICS;

    aput-object v1, v0, v4

    sget-object v1, LX/ICS;->PREF_SECOND:LX/ICS;

    aput-object v1, v0, v5

    sget-object v1, LX/ICS;->PREF_SKIP:LX/ICS;

    aput-object v1, v0, v6

    sget-object v1, LX/ICS;->BAKEOFF_PREF_FIRST:LX/ICS;

    aput-object v1, v0, v7

    sget-object v1, LX/ICS;->BAKEOFF_PREF_SECOND:LX/ICS;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/ICS;->BAKEOFF_NEUTRAL:LX/ICS;

    aput-object v2, v0, v1

    sput-object v0, LX/ICS;->$VALUES:[LX/ICS;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2549409
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2549410
    iput-object p3, p0, LX/ICS;->mRating:Ljava/lang/String;

    .line 2549411
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/ICS;
    .locals 1

    .prologue
    .line 2549412
    const-class v0, LX/ICS;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ICS;

    return-object v0
.end method

.method public static values()[LX/ICS;
    .locals 1

    .prologue
    .line 2549408
    sget-object v0, LX/ICS;->$VALUES:[LX/ICS;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ICS;

    return-object v0
.end method


# virtual methods
.method public final toEventName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2549400
    sget-object v0, LX/ICP;->b:[I

    invoke-virtual {p0}, LX/ICS;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2549401
    const-string v0, ""

    :goto_0
    return-object v0

    .line 2549402
    :pswitch_0
    const-string v0, "story_gallery_survey_ratings_pref_first"

    goto :goto_0

    .line 2549403
    :pswitch_1
    const-string v0, "story_gallery_survey_ratings_pref_second"

    goto :goto_0

    .line 2549404
    :pswitch_2
    const-string v0, "story_gallery_survey_ratings_pref_skip"

    goto :goto_0

    .line 2549405
    :pswitch_3
    const-string v0, "rater_bakeoff_ratings_pref_first"

    goto :goto_0

    .line 2549406
    :pswitch_4
    const-string v0, "rater_bakeoff_ratings_pref_second"

    goto :goto_0

    .line 2549407
    :pswitch_5
    const-string v0, "rater_bakeoff_ratings_neutral"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2549399
    iget-object v0, p0, LX/ICS;->mRating:Ljava/lang/String;

    return-object v0
.end method
