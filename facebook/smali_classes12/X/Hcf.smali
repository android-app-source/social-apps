.class public final LX/Hcf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public final synthetic b:LX/HcX;

.field public final synthetic c:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/atomic/AtomicBoolean;LX/HcX;Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 2487217
    iput-object p1, p0, LX/Hcf;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    iput-object p2, p0, LX/Hcf;->b:LX/HcX;

    iput-object p3, p0, LX/Hcf;->c:Ljava/lang/CharSequence;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const v0, 0x247e3cb1

    invoke-static {v3, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2487218
    iget-object v1, p0, LX/Hcf;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 2487219
    iget-object v1, p0, LX/Hcf;->b:LX/HcX;

    .line 2487220
    iput-boolean v2, v1, LX/HcX;->a:Z

    .line 2487221
    invoke-static {p1}, Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition;->b(Landroid/view/View;)LX/Hcl;

    move-result-object v1

    .line 2487222
    if-eqz v1, :cond_0

    .line 2487223
    iget-object v2, v1, LX/Hcl;->b:Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;

    move-object v2, v2

    .line 2487224
    if-nez v2, :cond_1

    .line 2487225
    :cond_0
    const v1, -0x4b026422

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2487226
    :goto_0
    return-void

    .line 2487227
    :cond_1
    iget-object v2, v1, LX/Hcl;->b:Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;

    move-object v1, v2

    .line 2487228
    new-instance v2, Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition$6$1;

    invoke-direct {v2, p0, v1}, Lcom/facebook/today/ui/components/partdefinition/ReactionFeedCommentUnitComponentPartDefinition$6$1;-><init>(LX/Hcf;Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;)V

    invoke-virtual {v1, v2}, Lcom/facebook/feedback/ui/rows/views/CommentTruncatableHeaderView;->post(Ljava/lang/Runnable;)Z

    .line 2487229
    const v1, 0x6ae0065

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0
.end method
