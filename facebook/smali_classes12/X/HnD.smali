.class public final LX/HnD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "Ljava/lang/Boolean;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;)V
    .locals 0

    .prologue
    .line 2501447
    iput-object p1, p0, LX/HnD;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/Boolean;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .param p1    # Ljava/lang/Boolean;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2501448
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Tp;->b(Z)V

    .line 2501449
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2501450
    iget-object v0, p0, LX/HnD;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v0, v0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->r:LX/Hmi;

    .line 2501451
    iget-object v1, v0, LX/Hmi;->c:Ljava/util/List;

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    move-object v0, v1

    .line 2501452
    iget-object v1, p0, LX/HnD;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    invoke-static {v1, v0}, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->a(Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;LX/0Px;)Ljava/lang/String;

    move-result-object v1

    .line 2501453
    iget-object v2, p0, LX/HnD;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v2, v2, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->B:LX/HnH;

    invoke-virtual {v2, v1, v0}, LX/HnH;->a(Ljava/lang/String;LX/0Px;)V

    .line 2501454
    iget-object v0, p0, LX/HnD;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v0, v0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->y:LX/Hmz;

    .line 2501455
    iget-object v2, v0, LX/Hmz;->a:LX/0TD;

    new-instance p0, LX/Hmw;

    invoke-direct {p0, v0, v1}, LX/Hmw;-><init>(LX/Hmz;Ljava/lang/String;)V

    invoke-interface {v2, p0}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v0, v2

    .line 2501456
    :goto_1
    return-object v0

    .line 2501457
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2501458
    :cond_1
    iget-object v0, p0, LX/HnD;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v1, p0, LX/HnD;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v1, v1, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->r:LX/Hmi;

    .line 2501459
    new-instance v2, LX/Hmp;

    .line 2501460
    :try_start_0
    iget-object v3, v1, LX/Hmi;->a:Landroid/content/pm/PackageManager;

    const-string v4, "com.facebook.katana"

    const/4 p1, 0x0

    invoke-virtual {v3, v4, p1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 2501461
    new-instance v3, Ljava/io/File;

    iget-object v4, v4, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v4, v4, Landroid/content/pm/ApplicationInfo;->publicSourceDir:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :goto_2
    move-object v3, v3

    .line 2501462
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v4, v4

    .line 2501463
    invoke-direct {v2, v3, v4}, LX/Hmp;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object v1, v2

    .line 2501464
    iput-object v1, v0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->A:LX/Hmp;

    .line 2501465
    iget-object v0, p0, LX/HnD;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v0, v0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->B:LX/HnH;

    iget-object v1, p0, LX/HnD;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v1, v1, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->A:LX/Hmp;

    invoke-virtual {v0, v1}, LX/HnH;->a(LX/Hmp;)V

    .line 2501466
    iget-object v0, p0, LX/HnD;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v0, v0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->y:LX/Hmz;

    iget-object v1, p0, LX/HnD;->a:Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    iget-object v1, v1, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->A:LX/Hmp;

    .line 2501467
    iget-object v2, v0, LX/Hmz;->a:LX/0TD;

    new-instance v3, LX/Hmu;

    invoke-direct {v3, v0, v1}, LX/Hmu;-><init>(LX/Hmz;LX/Hmp;)V

    invoke-interface {v2, v3}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v0, v2

    .line 2501468
    goto :goto_1

    .line 2501469
    :catch_0
    const/4 v3, 0x0

    goto :goto_2
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2501470
    check-cast p1, Ljava/lang/Boolean;

    invoke-direct {p0, p1}, LX/HnD;->a(Ljava/lang/Boolean;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
