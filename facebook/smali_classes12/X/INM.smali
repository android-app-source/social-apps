.class public final LX/INM;
.super LX/3x6;
.source ""


# instance fields
.field public final synthetic a:LX/INO;


# direct methods
.method public constructor <init>(LX/INO;)V
    .locals 0

    .prologue
    .line 2570754
    iput-object p1, p0, LX/INM;->a:LX/INO;

    invoke-direct {p0}, LX/3x6;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Rect;Landroid/view/View;Landroid/support/v7/widget/RecyclerView;LX/1Ok;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2570755
    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    .line 2570756
    invoke-static {p2}, Landroid/support/v7/widget/RecyclerView;->d(Landroid/view/View;)I

    move-result v2

    .line 2570757
    iget-object v1, p0, LX/INM;->a:LX/INO;

    invoke-virtual {v1}, LX/INO;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0b1ffa

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2570758
    const/4 v3, 0x3

    if-lt v2, v3, :cond_0

    move v0, v1

    :cond_0
    iput v0, p1, Landroid/graphics/Rect;->top:I

    .line 2570759
    rem-int/lit8 v0, v2, 0x3

    .line 2570760
    mul-int v2, v0, v1

    div-int/lit8 v2, v2, 0x3

    iput v2, p1, Landroid/graphics/Rect;->left:I

    .line 2570761
    add-int/lit8 v0, v0, 0x1

    mul-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x3

    sub-int v0, v1, v0

    iput v0, p1, Landroid/graphics/Rect;->right:I

    .line 2570762
    return-void
.end method
