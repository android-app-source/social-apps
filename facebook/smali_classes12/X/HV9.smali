.class public LX/HV9;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/9XE;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/17W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/fbui/widget/text/BadgeTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 2474749
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, LX/HV9;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2474750
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2474751
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2474752
    const-class v0, LX/HV9;

    invoke-static {v0, p0}, LX/HV9;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2474753
    const v0, 0x7f0315a9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2474754
    const v0, 0x7f0d30da

    invoke-virtual {p0, v0}, LX/HV9;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/BadgeTextView;

    iput-object v0, p0, LX/HV9;->c:Lcom/facebook/fbui/widget/text/BadgeTextView;

    .line 2474755
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/HV9;

    invoke-static {p0}, LX/9XE;->a(LX/0QB;)LX/9XE;

    move-result-object v1

    check-cast v1, LX/9XE;

    invoke-static {p0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object p0

    check-cast p0, LX/17W;

    iput-object v1, p1, LX/HV9;->a:LX/9XE;

    iput-object p0, p1, LX/HV9;->b:LX/17W;

    return-void
.end method
