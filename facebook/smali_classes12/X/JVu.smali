.class public final LX/JVu;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JVv;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Pn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/JVv;


# direct methods
.method public constructor <init>(LX/JVv;)V
    .locals 1

    .prologue
    .line 2701769
    iput-object p1, p0, LX/JVu;->c:LX/JVv;

    .line 2701770
    move-object v0, p1

    .line 2701771
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2701772
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2701773
    const-string v0, "PagesStoriesYouMissedHeaderComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2701774
    if-ne p0, p1, :cond_1

    .line 2701775
    :cond_0
    :goto_0
    return v0

    .line 2701776
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2701777
    goto :goto_0

    .line 2701778
    :cond_3
    check-cast p1, LX/JVu;

    .line 2701779
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2701780
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2701781
    if-eq v2, v3, :cond_0

    .line 2701782
    iget-object v2, p0, LX/JVu;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JVu;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/JVu;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2701783
    goto :goto_0

    .line 2701784
    :cond_5
    iget-object v2, p1, LX/JVu;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 2701785
    :cond_6
    iget-object v2, p0, LX/JVu;->b:LX/1Pn;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/JVu;->b:LX/1Pn;

    iget-object v3, p1, LX/JVu;->b:LX/1Pn;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2701786
    goto :goto_0

    .line 2701787
    :cond_7
    iget-object v2, p1, LX/JVu;->b:LX/1Pn;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
