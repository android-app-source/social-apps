.class public LX/IlB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Ikp;


# instance fields
.field private final a:Landroid/view/ViewGroup;

.field private final b:Lcom/facebook/widget/text/BetterTextView;

.field private final c:Lcom/facebook/widget/text/BetterTextView;

.field private final d:LX/Iky;


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;LX/Iky;)V
    .locals 2
    .param p2    # Landroid/view/ViewGroup;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/Iky;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2607547
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2607548
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Iky;

    iput-object v0, p0, LX/IlB;->d:LX/Iky;

    .line 2607549
    const v0, 0x7f0311a0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LX/IlB;->a:Landroid/view/ViewGroup;

    .line 2607550
    iget-object v0, p0, LX/IlB;->a:Landroid/view/ViewGroup;

    const v1, 0x7f0d02c4

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/IlB;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 2607551
    iget-object v0, p0, LX/IlB;->a:Landroid/view/ViewGroup;

    const v1, 0x7f0d0626

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/IlB;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 2607552
    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 1

    .prologue
    .line 2607553
    iget-object v0, p0, LX/IlB;->a:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public final a(LX/Il0;)V
    .locals 2

    .prologue
    .line 2607554
    iget-object v0, p0, LX/IlB;->b:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, LX/IlB;->d:LX/Iky;

    invoke-interface {v1}, LX/Iky;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2607555
    iget-object v0, p0, LX/IlB;->c:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, LX/IlB;->d:LX/Iky;

    invoke-interface {v1, p1}, LX/Iky;->a(LX/Il0;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2607556
    return-void
.end method
