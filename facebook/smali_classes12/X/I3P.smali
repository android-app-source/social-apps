.class public final LX/I3P;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "LX/7oa;",
        "Lcom/facebook/events/model/Event;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/I3Q;


# direct methods
.method public constructor <init>(LX/I3Q;)V
    .locals 0

    .prologue
    .line 2531329
    iput-object p1, p0, LX/I3P;->a:LX/I3Q;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2531319
    check-cast p1, LX/7oa;

    .line 2531320
    if-nez p1, :cond_0

    .line 2531321
    const/4 v0, 0x0

    .line 2531322
    :goto_0
    return-object v0

    .line 2531323
    :cond_0
    invoke-static {p1}, LX/Bm1;->c(LX/7oa;)LX/7vC;

    move-result-object v0

    .line 2531324
    invoke-interface {p1}, LX/7oa;->W()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2531325
    invoke-interface {p1}, LX/7oa;->W()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 2531326
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2531327
    iput-object v1, v0, LX/7vC;->ak:Ljava/lang/String;

    .line 2531328
    :cond_1
    invoke-virtual {v0}, LX/7vC;->b()Lcom/facebook/events/model/Event;

    move-result-object v0

    goto :goto_0
.end method
