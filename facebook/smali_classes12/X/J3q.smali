.class public final LX/J3q;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/util/List",
        "<",
        "LX/J4L;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/J3t;

.field private b:LX/J3r;


# direct methods
.method public constructor <init>(LX/J3t;LX/J3r;)V
    .locals 0

    .prologue
    .line 2643296
    iput-object p1, p0, LX/J3q;->a:LX/J3t;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    .line 2643297
    iput-object p2, p0, LX/J3q;->b:LX/J3r;

    .line 2643298
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2643299
    iget-object v0, p0, LX/J3q;->b:LX/J3r;

    invoke-interface {v0}, LX/J3r;->b()V

    .line 2643300
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2643301
    check-cast p1, Ljava/util/List;

    .line 2643302
    if-nez p1, :cond_0

    .line 2643303
    :goto_0
    return-void

    .line 2643304
    :cond_0
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, LX/J3q;->a:LX/J3t;

    iget-object v1, v1, LX/J3t;->l:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 2643305
    iget-object v0, p0, LX/J3q;->a:LX/J3t;

    iget-object v0, v0, LX/J3t;->l:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/J4L;

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/J4L;

    invoke-virtual {v0, v1}, LX/J4L;->a(LX/J4L;)V

    .line 2643306
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 2643307
    :cond_1
    iget-object v0, p0, LX/J3q;->a:LX/J3t;

    iget-object v0, v0, LX/J3t;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 2643308
    iget-object v1, p0, LX/J3q;->a:LX/J3t;

    iget-object v1, v1, LX/J3t;->l:Ljava/util/List;

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2643309
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2643310
    :cond_2
    iget-object v0, p0, LX/J3q;->b:LX/J3r;

    invoke-interface {v0}, LX/J3r;->a()V

    goto :goto_0
.end method
