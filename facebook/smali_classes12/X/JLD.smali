.class public LX/JLD;
.super LX/5pb;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "FigBottomSheetReactModule"
.end annotation


# instance fields
.field private final a:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public constructor <init>(LX/5pY;Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .param p1    # LX/5pY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2680771
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 2680772
    iput-object p2, p0, LX/JLD;->a:Ljava/util/concurrent/ExecutorService;

    .line 2680773
    return-void
.end method

.method public static synthetic a(LX/JLD;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 2680742
    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(LX/JLD;Landroid/content/Context;LX/5pC;LX/5pG;Lcom/facebook/react/bridge/Callback;)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 2680770
    invoke-direct {p0, p1, p2, p3, p4}, LX/JLD;->a(Landroid/content/Context;LX/5pC;LX/5pG;Lcom/facebook/react/bridge/Callback;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/content/Context;LX/5pC;LX/5pG;Lcom/facebook/react/bridge/Callback;)Landroid/app/Dialog;
    .locals 11

    .prologue
    .line 2680746
    new-instance v2, LX/3Af;

    invoke-direct {v2, p1}, LX/3Af;-><init>(Landroid/content/Context;)V

    .line 2680747
    new-instance v3, LX/34b;

    invoke-direct {v3, p1}, LX/34b;-><init>(Landroid/content/Context;)V

    .line 2680748
    const-string v0, "title"

    invoke-interface {p3, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2680749
    const-string v0, "title"

    invoke-interface {p3, v0}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/34b;->a(Ljava/lang/String;)V

    .line 2680750
    :cond_0
    const-string v0, "cancelButtonIndex"

    invoke-interface {p3, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "cancelButtonIndex"

    invoke-interface {p3, v0}, LX/5pG;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 2680751
    :goto_0
    new-instance v4, LX/JLC;

    invoke-direct {v4, p4, v0}, LX/JLC;-><init>(Lcom/facebook/react/bridge/Callback;I)V

    .line 2680752
    invoke-virtual {v2, v4}, LX/3Af;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 2680753
    const/4 v1, 0x0

    :goto_1
    invoke-interface {p2}, LX/5pC;->size()I

    move-result v5

    if-ge v1, v5, :cond_5

    .line 2680754
    if-eq v1, v0, :cond_3

    .line 2680755
    invoke-interface {p2, v1}, LX/5pC;->a(I)LX/5pG;

    move-result-object v5

    .line 2680756
    if-eqz v5, :cond_3

    const-string v6, "title"

    invoke-interface {v5, v6}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2680757
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 2680758
    const-string v7, "title"

    invoke-interface {v5, v7}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, LX/34c;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v7

    .line 2680759
    const-string v8, "description"

    invoke-interface {v5, v8}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 2680760
    const-string v8, "description"

    invoke-interface {v5, v8}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/3Ai;->a(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 2680761
    :cond_1
    const-string v8, "glyphName"

    invoke-interface {v5, v8}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 2680762
    const-string v8, "glyphName"

    invoke-interface {v5, v8}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    sget-object v8, LX/JLG;->LARGE:LX/JLG;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v10

    invoke-static {v5, v8, v9, v10}, LX/JLH;->a(Ljava/lang/String;LX/JLG;Landroid/content/res/Resources;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 2680763
    invoke-virtual {v7, v5}, LX/3Ai;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 2680764
    :cond_2
    new-instance v5, LX/JLB;

    invoke-direct {v5, p0, v4, p4, v6}, LX/JLB;-><init>(LX/JLD;LX/JLC;Lcom/facebook/react/bridge/Callback;Ljava/lang/Integer;)V

    invoke-virtual {v7, v5}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2680765
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2680766
    :cond_4
    const/4 v0, -0x1

    goto :goto_0

    .line 2680767
    :cond_5
    invoke-virtual {v2, v3}, LX/3Af;->a(LX/1OM;)V

    .line 2680768
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, LX/3Af;->a(I)V

    .line 2680769
    return-object v2
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2680745
    const-string v0, "FigBottomSheetReactModule"

    return-object v0
.end method

.method public showBottomSheet(LX/5pC;LX/5pG;Lcom/facebook/react/bridge/Callback;)V
    .locals 3
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2680743
    iget-object v0, p0, LX/JLD;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/fbreact/fig/bottomsheet/FigBottomSheetReactModule$1;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/facebook/fbreact/fig/bottomsheet/FigBottomSheetReactModule$1;-><init>(LX/JLD;LX/5pC;LX/5pG;Lcom/facebook/react/bridge/Callback;)V

    const v2, 0xaf0f1e2

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2680744
    return-void
.end method
