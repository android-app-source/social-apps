.class public LX/IkW;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0tX;

.field public final b:LX/Iyi;

.field public final c:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/3Ed;

.field public final f:LX/03V;

.field public final g:LX/Il6;


# direct methods
.method public constructor <init>(LX/0tX;LX/Iyi;Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;LX/0Or;LX/3Ed;LX/03V;LX/Il6;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0tX;",
            "LX/Iyi;",
            "Lcom/facebook/messaging/media/upload/MediaUploadManager;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "LX/3Ed;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/Il6;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2606776
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2606777
    iput-object p1, p0, LX/IkW;->a:LX/0tX;

    .line 2606778
    iput-object p2, p0, LX/IkW;->b:LX/Iyi;

    .line 2606779
    iput-object p3, p0, LX/IkW;->c:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    .line 2606780
    iput-object p4, p0, LX/IkW;->d:LX/0Or;

    .line 2606781
    iput-object p5, p0, LX/IkW;->e:LX/3Ed;

    .line 2606782
    iput-object p6, p0, LX/IkW;->f:LX/03V;

    .line 2606783
    iput-object p7, p0, LX/IkW;->g:LX/Il6;

    .line 2606784
    return-void
.end method

.method public static b(LX/0QB;)LX/IkW;
    .locals 8

    .prologue
    .line 2606785
    new-instance v0, LX/IkW;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-static {p0}, LX/Iyi;->b(LX/0QB;)LX/Iyi;

    move-result-object v2

    check-cast v2, LX/Iyi;

    invoke-static {p0}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a(LX/0QB;)Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    move-result-object v3

    check-cast v3, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    const/16 v4, 0x19e

    invoke-static {p0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {p0}, LX/3Ed;->a(LX/0QB;)LX/3Ed;

    move-result-object v5

    check-cast v5, LX/3Ed;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static {p0}, LX/Il6;->b(LX/0QB;)LX/Il6;

    move-result-object v7

    check-cast v7, LX/Il6;

    invoke-direct/range {v0 .. v7}, LX/IkW;-><init>(LX/0tX;LX/Iyi;Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;LX/0Or;LX/3Ed;LX/03V;LX/Il6;)V

    .line 2606786
    return-object v0
.end method
