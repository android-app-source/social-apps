.class public LX/Im5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7Fx;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/7Fx",
        "<",
        "Lcom/facebook/messaging/payment/sync/delta/PaymentsPrefetchedSyncData;",
        "LX/Ipt;",
        ">;"
    }
.end annotation


# static fields
.field private static final d:Ljava/lang/Object;


# instance fields
.field public final a:LX/ImD;

.field private final b:LX/IzM;

.field private final c:LX/IzO;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2608803
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Im5;->d:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/ImD;LX/IzM;LX/IzO;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2608833
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2608834
    iput-object p1, p0, LX/Im5;->a:LX/ImD;

    .line 2608835
    iput-object p2, p0, LX/Im5;->b:LX/IzM;

    .line 2608836
    iput-object p3, p0, LX/Im5;->c:LX/IzO;

    .line 2608837
    return-void
.end method

.method public static a(LX/0QB;)LX/Im5;
    .locals 9

    .prologue
    .line 2608804
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2608805
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2608806
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2608807
    if-nez v1, :cond_0

    .line 2608808
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2608809
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2608810
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2608811
    sget-object v1, LX/Im5;->d:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2608812
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2608813
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2608814
    :cond_1
    if-nez v1, :cond_4

    .line 2608815
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2608816
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2608817
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2608818
    new-instance p0, LX/Im5;

    invoke-static {v0}, LX/ImD;->a(LX/0QB;)LX/ImD;

    move-result-object v1

    check-cast v1, LX/ImD;

    invoke-static {v0}, LX/IzM;->a(LX/0QB;)LX/IzM;

    move-result-object v7

    check-cast v7, LX/IzM;

    invoke-static {v0}, LX/IzO;->a(LX/0QB;)LX/IzO;

    move-result-object v8

    check-cast v8, LX/IzO;

    invoke-direct {p0, v1, v7, v8}, LX/Im5;-><init>(LX/ImD;LX/IzM;LX/IzO;)V

    .line 2608819
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2608820
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2608821
    if-nez v1, :cond_2

    .line 2608822
    sget-object v0, LX/Im5;->d:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Im5;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2608823
    :goto_1
    if-eqz v0, :cond_3

    .line 2608824
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2608825
    :goto_3
    check-cast v0, LX/Im5;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2608826
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2608827
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2608828
    :catchall_1
    move-exception v0

    .line 2608829
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2608830
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2608831
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2608832
    :cond_2
    :try_start_8
    sget-object v0, LX/Im5;->d:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Im5;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a()J
    .locals 4

    .prologue
    .line 2608780
    iget-object v0, p0, LX/Im5;->b:LX/IzM;

    sget-object v1, LX/IzL;->f:LX/IzK;

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, LX/2Iu;->a(LX/0To;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(Ljava/lang/Object;Ljava/util/List;)LX/0P1;
    .locals 1

    .prologue
    .line 2608802
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Ljava/lang/Object;LX/7GJ;)Landroid/os/Bundle;
    .locals 6

    .prologue
    .line 2608791
    check-cast p1, Lcom/facebook/messaging/payment/sync/delta/PaymentsPrefetchedSyncData;

    .line 2608792
    iget-object v0, p0, LX/Im5;->c:LX/IzO;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2608793
    const v0, -0x7418a16d

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 2608794
    :try_start_0
    iget-object v2, p0, LX/Im5;->a:LX/ImD;

    iget-object v0, p2, LX/7GJ;->a:Ljava/lang/Object;

    check-cast v0, LX/Ipt;

    invoke-virtual {v2, v0}, LX/ImD;->a(LX/Ipt;)LX/ImH;

    move-result-object v0

    .line 2608795
    invoke-virtual {v0, p2}, LX/ImH;->a(LX/7GJ;)Landroid/os/Bundle;

    move-result-object v2

    move-object v0, v2

    .line 2608796
    move-object v0, v0

    .line 2608797
    iget-object v2, p0, LX/Im5;->b:LX/IzM;

    sget-object v3, LX/IzL;->f:LX/IzK;

    iget-wide v4, p2, LX/7GJ;->b:J

    invoke-virtual {v2, v3, v4, v5}, LX/2Iu;->b(LX/0To;J)V

    .line 2608798
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2608799
    const v2, 0x1af28966

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 2608800
    return-object v0

    .line 2608801
    :catchall_0
    move-exception v0

    const v2, 0x13a3d7eb

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method public final a(J)V
    .locals 3

    .prologue
    .line 2608789
    iget-object v0, p0, LX/Im5;->b:LX/IzM;

    sget-object v1, LX/IzL;->f:LX/IzK;

    invoke-virtual {v0, v1, p1, p2}, LX/2Iu;->b(LX/0To;J)V

    .line 2608790
    return-void
.end method

.method public final a(ZLcom/facebook/sync/analytics/FullRefreshReason;)V
    .locals 3

    .prologue
    .line 2608838
    iget-object v0, p0, LX/Im5;->b:LX/IzM;

    sget-object v1, LX/IzL;->g:LX/IzK;

    invoke-virtual {v0, v1, p1}, LX/2Iu;->b(LX/0To;Z)V

    .line 2608839
    iget-object v0, p0, LX/Im5;->b:LX/IzM;

    sget-object v1, LX/IzL;->h:LX/IzK;

    invoke-virtual {p2}, Lcom/facebook/sync/analytics/FullRefreshReason;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/2Iu;->b(LX/0To;Ljava/lang/String;)V

    .line 2608840
    return-void
.end method

.method public final b(J)V
    .locals 3

    .prologue
    .line 2608787
    iget-object v0, p0, LX/Im5;->b:LX/IzM;

    sget-object v1, LX/IzL;->i:LX/IzK;

    invoke-virtual {v0, v1, p1, p2}, LX/2Iu;->b(LX/0To;J)V

    .line 2608788
    return-void
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 2608786
    iget-object v0, p0, LX/Im5;->b:LX/IzM;

    sget-object v1, LX/IzL;->g:LX/IzK;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/2Iu;->a(LX/0To;Z)Z

    move-result v0

    return v0
.end method

.method public final c()Lcom/facebook/sync/analytics/FullRefreshReason;
    .locals 3

    .prologue
    .line 2608784
    iget-object v0, p0, LX/Im5;->b:LX/IzM;

    sget-object v1, LX/IzL;->h:LX/IzK;

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, LX/2Iu;->a(LX/0To;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2608785
    invoke-static {v0}, Lcom/facebook/sync/analytics/FullRefreshReason;->a(Ljava/lang/String;)Lcom/facebook/sync/analytics/FullRefreshReason;

    move-result-object v0

    return-object v0
.end method

.method public final d()J
    .locals 4

    .prologue
    .line 2608783
    iget-object v0, p0, LX/Im5;->b:LX/IzM;

    sget-object v1, LX/IzL;->i:LX/IzK;

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, LX/2Iu;->a(LX/0To;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2608782
    iget-object v0, p0, LX/Im5;->b:LX/IzM;

    sget-object v1, LX/IzL;->e:LX/IzK;

    invoke-virtual {v0, v1}, LX/2Iu;->a(LX/0To;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 2608781
    const/4 v0, 0x0

    return v0
.end method
