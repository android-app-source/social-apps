.class public LX/Isw;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field private static final e:Ljava/lang/Object;


# instance fields
.field private final a:Ljava/util/concurrent/ScheduledExecutorService;

.field private final b:LX/0Sh;

.field public c:Ljava/util/concurrent/Future;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public d:J
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2621461
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Isw;->e:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/ScheduledExecutorService;LX/0Sh;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2621462
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2621463
    iput-object p1, p0, LX/Isw;->a:Ljava/util/concurrent/ScheduledExecutorService;

    .line 2621464
    iput-object p2, p0, LX/Isw;->b:LX/0Sh;

    .line 2621465
    return-void
.end method

.method public static a(LX/0QB;)LX/Isw;
    .locals 8

    .prologue
    .line 2621466
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2621467
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2621468
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2621469
    if-nez v1, :cond_0

    .line 2621470
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2621471
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2621472
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2621473
    sget-object v1, LX/Isw;->e:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2621474
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2621475
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2621476
    :cond_1
    if-nez v1, :cond_4

    .line 2621477
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2621478
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2621479
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2621480
    new-instance p0, LX/Isw;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v7

    check-cast v7, LX/0Sh;

    invoke-direct {p0, v1, v7}, LX/Isw;-><init>(Ljava/util/concurrent/ScheduledExecutorService;LX/0Sh;)V

    .line 2621481
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2621482
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2621483
    if-nez v1, :cond_2

    .line 2621484
    sget-object v0, LX/Isw;->e:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Isw;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2621485
    :goto_1
    if-eqz v0, :cond_3

    .line 2621486
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2621487
    :goto_3
    check-cast v0, LX/Isw;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2621488
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2621489
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2621490
    :catchall_1
    move-exception v0

    .line 2621491
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2621492
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2621493
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2621494
    :cond_2
    :try_start_8
    sget-object v0, LX/Isw;->e:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Isw;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 2

    .prologue
    .line 2621495
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Isw;->c:Ljava/util/concurrent/Future;

    if-nez v0, :cond_0

    .line 2621496
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/Isw;->d:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2621497
    :cond_0
    monitor-exit p0

    return-void

    .line 2621498
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/Runnable;)V
    .locals 5

    .prologue
    .line 2621499
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Isw;->c:Ljava/util/concurrent/Future;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 2621500
    :goto_0
    monitor-exit p0

    return-void

    .line 2621501
    :cond_0
    :try_start_1
    iget-wide v0, p0, LX/Isw;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 2621502
    const-wide/32 v0, 0x927c0

    iput-wide v0, p0, LX/Isw;->d:J

    .line 2621503
    :goto_1
    iget-object v0, p0, LX/Isw;->a:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/messaging/send/client/ExponentialBackoffRetryManager$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/messaging/send/client/ExponentialBackoffRetryManager$1;-><init>(LX/Isw;Ljava/lang/Runnable;)V

    iget-wide v2, p0, LX/Isw;->d:J

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, LX/Isw;->c:Ljava/util/concurrent/Future;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2621504
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2621505
    :cond_1
    :try_start_2
    iget-wide v0, p0, LX/Isw;->d:J

    const-wide/16 v2, 0x2

    mul-long/2addr v0, v2

    iput-wide v0, p0, LX/Isw;->d:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public final declared-synchronized b()V
    .locals 2

    .prologue
    .line 2621506
    monitor-enter p0

    const-wide/16 v0, 0x0

    :try_start_0
    iput-wide v0, p0, LX/Isw;->d:J

    .line 2621507
    iget-object v0, p0, LX/Isw;->c:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    .line 2621508
    iget-object v0, p0, LX/Isw;->c:Ljava/util/concurrent/Future;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 2621509
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/Isw;->c:Ljava/util/concurrent/Future;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2621510
    monitor-exit p0

    return-void

    .line 2621511
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
