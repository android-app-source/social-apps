.class public LX/I1a;
.super LX/1a1;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public l:Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterSingleItemView;

.field public m:Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterSingleItemView;

.field public n:Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterSingleItemView;

.field public final o:Landroid/content/Context;

.field public final p:LX/0wM;

.field private final q:Lcom/facebook/events/common/EventAnalyticsParams;

.field private final r:LX/I53;

.field private final s:LX/1nQ;

.field public t:LX/0m9;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public u:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardTimeFiltersQueryModel$EventDiscoverTimeFiltersModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterView;Landroid/content/Context;LX/I53;LX/0wM;LX/1nQ;)V
    .locals 1
    .param p1    # Lcom/facebook/events/common/EventAnalyticsParams;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2529084
    invoke-direct {p0, p2}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 2529085
    iput-object p3, p0, LX/I1a;->o:Landroid/content/Context;

    .line 2529086
    iput-object p1, p0, LX/I1a;->q:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2529087
    iput-object p4, p0, LX/I1a;->r:LX/I53;

    .line 2529088
    iput-object p6, p0, LX/I1a;->s:LX/1nQ;

    .line 2529089
    iput-object p5, p0, LX/I1a;->p:LX/0wM;

    .line 2529090
    const v0, 0x7f0d0f47

    invoke-virtual {p2, v0}, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterSingleItemView;

    iput-object v0, p0, LX/I1a;->l:Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterSingleItemView;

    .line 2529091
    const v0, 0x7f0d0f48

    invoke-virtual {p2, v0}, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterSingleItemView;

    iput-object v0, p0, LX/I1a;->m:Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterSingleItemView;

    .line 2529092
    const v0, 0x7f0d0f49

    invoke-virtual {p2, v0}, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterSingleItemView;

    iput-object v0, p0, LX/I1a;->n:Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterSingleItemView;

    .line 2529093
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v5, 0x0

    const/4 v1, 0x2

    const v0, -0x463d5d3b

    invoke-static {v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v8

    .line 2529094
    iget-object v0, p0, LX/I1a;->u:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/I1a;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2529095
    :cond_0
    const v0, 0x3c98ebbe

    invoke-static {v1, v1, v0, v8}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2529096
    :goto_0
    return-void

    .line 2529097
    :cond_1
    iget-object v0, p0, LX/I1a;->l:Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterSingleItemView;

    if-ne p1, v0, :cond_2

    iget-object v0, p0, LX/I1a;->u:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2529098
    sget-object v1, Lcom/facebook/events/common/ActionMechanism;->EVENT_DASHBOARD_HOME_TAB_TODAY_TIME_FILTER:Lcom/facebook/events/common/ActionMechanism;

    .line 2529099
    iget-object v0, p0, LX/I1a;->u:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardTimeFiltersQueryModel$EventDiscoverTimeFiltersModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardTimeFiltersQueryModel$EventDiscoverTimeFiltersModel;->k()Ljava/lang/String;

    move-result-object v0

    move-object v6, v0

    move-object v7, v1

    .line 2529100
    :goto_1
    iget-object v9, p0, LX/I1a;->r:LX/I53;

    iget-object v10, p0, LX/I1a;->t:LX/0m9;

    new-instance v0, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    iget-object v1, p0, LX/I1a;->q:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v1, v1, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    iget-object v2, p0, LX/I1a;->q:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v2, v2, Lcom/facebook/events/common/EventAnalyticsParams;->e:Ljava/lang/String;

    iget-object v3, p0, LX/I1a;->q:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v3, v3, Lcom/facebook/events/common/EventAnalyticsParams;->d:Ljava/lang/String;

    sget-object v4, Lcom/facebook/events/common/ActionMechanism;->EVENT_DASHBORD_TIME_ROW:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v4}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-virtual {v9, v6, v10, v0}, LX/I53;->a(Ljava/lang/String;LX/0m9;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;)V

    .line 2529101
    iget-object v0, p0, LX/I1a;->s:LX/1nQ;

    iget-object v1, p0, LX/I1a;->q:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v1, v1, Lcom/facebook/events/common/EventAnalyticsParams;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v7, v5}, LX/1nQ;->a(Ljava/lang/String;Lcom/facebook/events/common/ActionMechanism;Ljava/lang/String;)V

    .line 2529102
    const v0, 0x1614eb86

    invoke-static {v0, v8}, LX/02F;->a(II)V

    goto :goto_0

    .line 2529103
    :cond_2
    iget-object v0, p0, LX/I1a;->m:Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterSingleItemView;

    if-ne p1, v0, :cond_3

    iget-object v0, p0, LX/I1a;->u:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2529104
    iget-object v0, p0, LX/I1a;->u:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardTimeFiltersQueryModel$EventDiscoverTimeFiltersModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardTimeFiltersQueryModel$EventDiscoverTimeFiltersModel;->k()Ljava/lang/String;

    move-result-object v0

    .line 2529105
    sget-object v1, Lcom/facebook/events/common/ActionMechanism;->EVENT_DASHBOARD_HOME_TAB_TOMORROW_TIME_FILTER:Lcom/facebook/events/common/ActionMechanism;

    move-object v6, v0

    move-object v7, v1

    goto :goto_1

    .line 2529106
    :cond_3
    iget-object v0, p0, LX/I1a;->n:Lcom/facebook/events/dashboard/home/adapters/EventsHomeDashboardTimeFilterSingleItemView;

    if-ne p1, v0, :cond_4

    iget-object v0, p0, LX/I1a;->u:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2529107
    iget-object v0, p0, LX/I1a;->u:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardTimeFiltersQueryModel$EventDiscoverTimeFiltersModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardTimeFiltersQueryModel$EventDiscoverTimeFiltersModel;->k()Ljava/lang/String;

    move-result-object v0

    .line 2529108
    sget-object v1, Lcom/facebook/events/common/ActionMechanism;->EVENT_DASHBOARD_HOME_TAB_WEEKEND_TIME_FILTER:Lcom/facebook/events/common/ActionMechanism;

    move-object v6, v0

    move-object v7, v1

    goto :goto_1

    :cond_4
    move-object v6, v5

    move-object v7, v5

    goto :goto_1
.end method
