.class public LX/IFU;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "LX/IFy;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0tX;

.field public final f:Ljava/lang/String;

.field public final g:LX/03V;

.field public final h:LX/IFa;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2554216
    const-class v0, LX/IFU;

    sput-object v0, LX/IFU;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;LX/1Ck;Landroid/content/res/Resources;LX/0tX;LX/03V;LX/IFa;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/1Ck;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2554217
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2554218
    iput-object p1, p0, LX/IFU;->b:Ljava/lang/String;

    .line 2554219
    iput-object p2, p0, LX/IFU;->c:Ljava/lang/String;

    .line 2554220
    const v0, 0x7f0b23da

    invoke-virtual {p4, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/IFU;->f:Ljava/lang/String;

    .line 2554221
    iput-object p5, p0, LX/IFU;->e:LX/0tX;

    .line 2554222
    iput-object p3, p0, LX/IFU;->d:LX/1Ck;

    .line 2554223
    iput-object p6, p0, LX/IFU;->g:LX/03V;

    .line 2554224
    iput-object p7, p0, LX/IFU;->h:LX/IFa;

    .line 2554225
    return-void
.end method
