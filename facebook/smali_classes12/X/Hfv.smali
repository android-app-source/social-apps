.class public LX/Hfv;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static j:LX/0Xm;


# instance fields
.field public a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1vg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/17Y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/3mF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/DQW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/3Cq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/HgN;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2492567
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2492568
    return-void
.end method

.method public static a(LX/Hfv;LX/1De;Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;Ljava/lang/Boolean;)LX/1Dh;
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 2492545
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-interface {v0, v1}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v0

    const v1, 0x7f0e0bb3

    invoke-static {p1, v3, v1}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v1

    invoke-static {p0, p2}, LX/Hfv;->a(LX/Hfv;Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const v2, 0x7f0b2349

    invoke-interface {v1, v4, v2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v1

    invoke-static {p1, p1}, LX/Hfr;->a(LX/1De;LX/1De;)LX/1dQ;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-static {p2, p3}, LX/Hfv;->a(Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;Ljava/lang/Boolean;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-interface {v1, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    return-object v0

    :cond_0
    const v0, 0x7f0e0bb4

    invoke-static {p1, v3, v0}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v0

    invoke-virtual {p2}, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;->a()Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel$NodeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel$NodeModel;->p()I

    move-result v2

    .line 2492546
    iget-object v3, p0, LX/Hfv;->c:Landroid/content/res/Resources;

    const v5, 0x7f0f0171

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 p2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    aput-object p3, v6, p2

    invoke-virtual {v3, v5, v2, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move-object v2, v3

    .line 2492547
    invoke-virtual {v0, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const v2, 0x7f0b2349

    invoke-interface {v0, v4, v2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v0

    invoke-static {p1, p1}, LX/Hfr;->a(LX/1De;LX/1De;)LX/1dQ;

    move-result-object v2

    invoke-interface {v0, v2}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/Hfv;
    .locals 12

    .prologue
    .line 2492554
    const-class v1, LX/Hfv;

    monitor-enter v1

    .line 2492555
    :try_start_0
    sget-object v0, LX/Hfv;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2492556
    sput-object v2, LX/Hfv;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2492557
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2492558
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2492559
    new-instance v3, LX/Hfv;

    invoke-direct {v3}, LX/Hfv;-><init>()V

    .line 2492560
    const/16 v4, 0x509

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v5

    check-cast v5, LX/1vg;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v6

    check-cast v6, Landroid/content/res/Resources;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v7

    check-cast v7, LX/17Y;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v8

    check-cast v8, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/3mF;->b(LX/0QB;)LX/3mF;

    move-result-object v9

    check-cast v9, LX/3mF;

    invoke-static {v0}, LX/DQW;->b(LX/0QB;)LX/DQW;

    move-result-object v10

    check-cast v10, LX/DQW;

    invoke-static {v0}, LX/3Cq;->a(LX/0QB;)LX/3Cq;

    move-result-object v11

    check-cast v11, LX/3Cq;

    invoke-static {v0}, LX/HgN;->a(LX/0QB;)LX/HgN;

    move-result-object p0

    check-cast p0, LX/HgN;

    .line 2492561
    iput-object v4, v3, LX/Hfv;->a:LX/0Or;

    iput-object v5, v3, LX/Hfv;->b:LX/1vg;

    iput-object v6, v3, LX/Hfv;->c:Landroid/content/res/Resources;

    iput-object v7, v3, LX/Hfv;->d:LX/17Y;

    iput-object v8, v3, LX/Hfv;->e:Lcom/facebook/content/SecureContextHelper;

    iput-object v9, v3, LX/Hfv;->f:LX/3mF;

    iput-object v10, v3, LX/Hfv;->g:LX/DQW;

    iput-object v11, v3, LX/Hfv;->h:LX/3Cq;

    iput-object p0, v3, LX/Hfv;->i:LX/HgN;

    .line 2492562
    move-object v0, v3

    .line 2492563
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2492564
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Hfv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2492565
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2492566
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(LX/Hfv;Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 2492549
    invoke-virtual {p1}, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;->a()Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel$NodeModel;->l()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2492550
    invoke-virtual {p1}, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;->a()Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel$NodeModel;->m()Ljava/lang/String;

    move-result-object v0

    .line 2492551
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-virtual {p1}, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;->a()Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel$NodeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel$NodeModel;->m()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2492552
    const/4 p0, 0x0

    move-object v1, p0

    .line 2492553
    invoke-static {v0, v1}, LX/47q;->a(Landroid/text/SpannableStringBuilder;Landroid/graphics/drawable/Drawable;)Landroid/text/Spannable;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;Ljava/lang/Boolean;)Z
    .locals 1

    .prologue
    .line 2492548
    if-nez p1, :cond_0

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel;->a()Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/work/groupstab/protocol/UserGroupConnectionNodeModels$UserGroupConnectionNodeModel$NodeModel;->p()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
