.class public LX/Hpt;
.super LX/BcS;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Hpr;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Hpu;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2510039
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Hpt;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Hpu;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2510078
    invoke-direct {p0}, LX/BcS;-><init>()V

    .line 2510079
    iput-object p1, p0, LX/Hpt;->b:LX/0Ot;

    .line 2510080
    return-void
.end method

.method public static a(LX/0QB;)LX/Hpt;
    .locals 4

    .prologue
    .line 2510067
    const-class v1, LX/Hpt;

    monitor-enter v1

    .line 2510068
    :try_start_0
    sget-object v0, LX/Hpt;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2510069
    sput-object v2, LX/Hpt;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2510070
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2510071
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2510072
    new-instance v3, LX/Hpt;

    const/16 p0, 0x1955

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Hpt;-><init>(LX/0Ot;)V

    .line 2510073
    move-object v0, v3

    .line 2510074
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2510075
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Hpt;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2510076
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2510077
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/BcQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2510081
    invoke-static {}, LX/1dS;->b()V

    .line 2510082
    iget v0, p1, LX/BcQ;->b:I

    .line 2510083
    packed-switch v0, :pswitch_data_0

    .line 2510084
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2510085
    :pswitch_0
    check-cast p2, LX/BdG;

    .line 2510086
    iget v1, p2, LX/BdG;->a:I

    iget-object v2, p2, LX/BdG;->b:Ljava/lang/Object;

    iget-object v0, p1, LX/BcQ;->c:[Ljava/lang/Object;

    const/4 v3, 0x0

    aget-object v0, v0, v3

    check-cast v0, LX/BcP;

    .line 2510087
    iget-object p1, p0, LX/Hpt;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    .line 2510088
    invoke-virtual {v0}, LX/BcP;->i()LX/BcO;

    move-result-object p1

    if-nez p1, :cond_0

    .line 2510089
    const/4 p1, 0x0

    .line 2510090
    :goto_1
    move-object p1, p1

    .line 2510091
    check-cast v2, Lcom/facebook/graphql/modelutil/BaseModel;

    .line 2510092
    new-instance p0, LX/Hpz;

    invoke-direct {p0}, LX/Hpz;-><init>()V

    .line 2510093
    iput v1, p0, LX/Hpz;->a:I

    .line 2510094
    iput-object v2, p0, LX/Hpz;->b:Lcom/facebook/graphql/modelutil/BaseModel;

    .line 2510095
    iget-object v0, p1, LX/BcQ;->a:LX/BcO;

    .line 2510096
    iget-object v1, v0, LX/BcO;->i:LX/BcS;

    move-object v0, v1

    .line 2510097
    invoke-virtual {v0, p1, p0}, LX/BcS;->a(LX/BcQ;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/1X1;

    move-object p1, p0

    .line 2510098
    move-object p1, p1

    .line 2510099
    move-object v0, p1

    .line 2510100
    goto :goto_0

    :cond_0
    invoke-virtual {v0}, LX/BcP;->i()LX/BcO;

    move-result-object p1

    check-cast p1, LX/Hps;

    iget-object p1, p1, LX/Hps;->g:LX/BcQ;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch -0x7d7deeba
        :pswitch_0
    .end packed-switch
.end method

.method public final a(LX/BcO;LX/BcO;)V
    .locals 1

    .prologue
    .line 2510063
    check-cast p1, LX/Hps;

    .line 2510064
    check-cast p2, LX/Hps;

    .line 2510065
    iget-object v0, p1, LX/Hps;->f:LX/2kW;

    iput-object v0, p2, LX/Hps;->f:LX/2kW;

    .line 2510066
    return-void
.end method

.method public final a(LX/BcP;Ljava/util/List;LX/BcO;)V
    .locals 2

    .prologue
    .line 2510057
    check-cast p3, LX/Hps;

    .line 2510058
    iget-object v0, p0, LX/Hpt;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p3, LX/Hps;->f:LX/2kW;

    .line 2510059
    invoke-static {p1}, LX/Bd3;->b(LX/BcP;)LX/Bd0;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/Bd0;->a(LX/2kW;)LX/Bd0;

    move-result-object v1

    .line 2510060
    const p0, -0x7d7deeba

    const/4 p3, 0x1

    new-array p3, p3, [Ljava/lang/Object;

    const/4 v0, 0x0

    aput-object p1, p3, v0

    invoke-static {p1, p0, p3}, LX/BcS;->a(LX/BcP;I[Ljava/lang/Object;)LX/BcQ;

    move-result-object p0

    move-object p0, p0

    .line 2510061
    invoke-virtual {v1, p0}, LX/Bd0;->b(LX/BcQ;)LX/Bd0;

    move-result-object v1

    const/16 p0, 0x14

    invoke-virtual {v1, p0}, LX/Bd0;->b(I)LX/Bd0;

    move-result-object v1

    invoke-virtual {v1}, LX/Bd0;->b()LX/BcO;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2510062
    return-void
.end method

.method public final c(LX/BcP;LX/BcO;)V
    .locals 11

    .prologue
    .line 2510040
    check-cast p2, LX/Hps;

    .line 2510041
    invoke-static {}, LX/BcS;->a()LX/1np;

    move-result-object v1

    .line 2510042
    iget-object v0, p0, LX/Hpt;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hpu;

    iget-object v2, p2, LX/Hps;->b:LX/Hpa;

    iget-object v3, p2, LX/Hps;->c:Ljava/util/Map;

    iget-object v4, p2, LX/Hps;->d:[Ljava/lang/String;

    check-cast v4, [Ljava/lang/String;

    iget-object v5, p2, LX/Hps;->e:LX/Hpf;

    .line 2510043
    iget-object v6, v0, LX/Hpu;->a:LX/1rq;

    invoke-interface {v2}, LX/Hpa;->getSessionId()Ljava/lang/String;

    move-result-object v7

    new-instance v8, LX/HpZ;

    invoke-direct {v8, v4, v3, v5}, LX/HpZ;-><init>([Ljava/lang/String;Ljava/util/Map;LX/Hpf;)V

    invoke-virtual {v6, v7, v8}, LX/1rq;->a(Ljava/lang/String;LX/1rs;)LX/2jj;

    move-result-object v6

    const-wide/16 v8, 0x258

    .line 2510044
    iput-wide v8, v6, LX/2jj;->j:J

    .line 2510045
    move-object v6, v6

    .line 2510046
    const/4 v7, 0x1

    .line 2510047
    iput-boolean v7, v6, LX/2jj;->k:Z

    .line 2510048
    move-object v6, v6

    .line 2510049
    const/4 v7, 0x0

    .line 2510050
    iput v7, v6, LX/2jj;->o:I

    .line 2510051
    move-object v6, v6

    .line 2510052
    invoke-virtual {v6}, LX/2jj;->a()LX/2kW;

    move-result-object v6

    .line 2510053
    iput-object v6, v1, LX/1np;->a:Ljava/lang/Object;

    .line 2510054
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2510055
    check-cast v0, LX/2kW;

    iput-object v0, p2, LX/Hps;->f:LX/2kW;

    .line 2510056
    return-void
.end method
