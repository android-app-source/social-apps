.class public final LX/HUr;
.super LX/1Cv;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;)V
    .locals 0

    .prologue
    .line 2473904
    iput-object p1, p0, LX/HUr;->a:Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;

    invoke-direct {p0}, LX/1Cv;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2473898
    sget-object v0, LX/HUs;->PLAYLIST_INFO_AND_VIDEOS:LX/HUs;

    invoke-virtual {v0}, LX/HUs;->ordinal()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 2473899
    new-instance v0, LX/HV4;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f010633

    invoke-direct {v0, v1, v2}, LX/HV4;-><init>(Landroid/content/Context;I)V

    .line 2473900
    :goto_0
    return-object v0

    .line 2473901
    :cond_0
    sget-object v0, LX/HUs;->NAV_ITEM:LX/HUs;

    invoke-virtual {v0}, LX/HUs;->ordinal()I

    move-result v0

    if-ne p1, v0, :cond_1

    .line 2473902
    new-instance v0, LX/HV7;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f010633

    invoke-direct {v0, v1, v2}, LX/HV7;-><init>(Landroid/content/Context;I)V

    goto :goto_0

    .line 2473903
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 8

    .prologue
    .line 2473876
    sget-object v0, LX/HUs;->PLAYLIST_INFO_AND_VIDEOS:LX/HUs;

    invoke-virtual {v0}, LX/HUs;->ordinal()I

    move-result v0

    if-ne p4, v0, :cond_1

    .line 2473877
    check-cast p3, LX/HV4;

    .line 2473878
    iget-object v0, p0, LX/HUr;->a:Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;->j:Ljava/util/List;

    iget-object v1, p0, LX/HUr;->a:Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;

    iget-object v1, v1, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;->l:Ljava/lang/String;

    iget-object v2, p0, LX/HUr;->a:Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;

    iget-object v2, v2, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;->m:Ljava/lang/String;

    const/4 p5, 0x1

    const/4 p4, 0x0

    .line 2473879
    iget-object v3, p3, LX/HV4;->a:Lcom/facebook/fbui/widget/text/BadgeTextView;

    invoke-virtual {v3, v1}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2473880
    iget-object v3, p3, LX/HV4;->a:Lcom/facebook/fbui/widget/text/BadgeTextView;

    invoke-virtual {p3}, LX/HV4;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f00b0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    new-array v7, p5, [Ljava/lang/Object;

    const-string p0, "%,d"

    new-array p1, p5, [Ljava/lang/Object;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, p1, p4

    invoke-static {p0, p1}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    aput-object p0, v7, p4

    invoke-virtual {v4, v5, v6, v7}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setBadgeText(Ljava/lang/CharSequence;)V

    .line 2473881
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2473882
    iget-object v3, p3, LX/HV4;->b:Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->setVisibility(I)V

    .line 2473883
    :goto_0
    iget-object v3, p3, LX/HV4;->b:Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;

    .line 2473884
    iput-boolean p5, v3, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->e:Z

    .line 2473885
    iget-object v3, p3, LX/HV4;->c:Lcom/facebook/widget/listview/BetterListView;

    new-instance v4, LX/HV3;

    invoke-direct {v4, v0}, LX/HV3;-><init>(Ljava/util/List;)V

    invoke-virtual {v3, v4}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2473886
    iget-object v3, p3, LX/HV4;->c:Lcom/facebook/widget/listview/BetterListView;

    .line 2473887
    invoke-virtual {v3}, Landroid/widget/ListView;->getWidth()I

    move-result v4

    const v5, 0x1fffffff

    const/high16 v6, -0x80000000

    invoke-static {v5, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/widget/ListView;->measure(II)V

    .line 2473888
    invoke-virtual {v3}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    invoke-virtual {v3}, Landroid/widget/ListView;->getMeasuredHeight()I

    move-result v5

    iput v5, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2473889
    :cond_0
    :goto_1
    return-void

    .line 2473890
    :cond_1
    sget-object v0, LX/HUs;->NAV_ITEM:LX/HUs;

    invoke-virtual {v0}, LX/HUs;->ordinal()I

    move-result v0

    if-ne p4, v0, :cond_0

    .line 2473891
    check-cast p3, LX/HV7;

    .line 2473892
    iget-object v0, p0, LX/HUr;->a:Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;

    invoke-static {v0}, Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;->c(Lcom/facebook/pages/fb4a/videohub/fragments/VideoPlaylistPermalinkFragment;)Ljava/lang/String;

    move-result-object v0

    .line 2473893
    iput-object v0, p3, LX/HV7;->b:Ljava/lang/String;

    .line 2473894
    invoke-static {p3}, LX/HV7;->a(LX/HV7;)V

    .line 2473895
    new-instance v0, LX/HUq;

    invoke-direct {v0, p0}, LX/HUq;-><init>(LX/HUr;)V

    invoke-virtual {p3, v0}, LX/HV7;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 2473896
    :cond_2
    iget-object v3, p3, LX/HV4;->b:Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;

    invoke-virtual {v3, p4}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->setVisibility(I)V

    .line 2473897
    iget-object v3, p3, LX/HV4;->b:Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;

    invoke-virtual {v3, v2}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2473875
    invoke-static {}, LX/HUs;->values()[LX/HUs;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2473874
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2473866
    if-ltz p1, :cond_0

    invoke-virtual {p0}, LX/HUr;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 2473867
    int-to-long v0, p1

    .line 2473868
    :goto_0
    return-wide v0

    :cond_0
    const-wide/high16 v0, -0x8000000000000000L

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2473870
    invoke-virtual {p0}, LX/HUr;->getCount()I

    move-result v0

    invoke-static {p1, v0}, LX/0PB;->checkPositionIndex(II)I

    .line 2473871
    if-nez p1, :cond_0

    .line 2473872
    sget-object v0, LX/HUs;->NAV_ITEM:LX/HUs;

    invoke-virtual {v0}, LX/HUs;->ordinal()I

    move-result v0

    .line 2473873
    :goto_0
    return v0

    :cond_0
    sget-object v0, LX/HUs;->PLAYLIST_INFO_AND_VIDEOS:LX/HUs;

    invoke-virtual {v0}, LX/HUs;->ordinal()I

    move-result v0

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2473869
    invoke-static {}, LX/HUs;->values()[LX/HUs;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
