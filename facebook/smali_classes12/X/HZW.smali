.class public final enum LX/HZW;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HZW;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/HZW;

.field public static final enum DATA_POLICY:LX/HZW;

.field public static final enum LOCATION_SUPPLEMENT:LX/HZW;

.field public static final enum TERMS_OF_SERVICE:LX/HZW;


# instance fields
.field public final titleResId:I

.field public final url:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2482239
    new-instance v0, LX/HZW;

    const-string v1, "TERMS_OF_SERVICE"

    const v2, 0x7f0835a6

    const-string v3, "https://m.facebook.com/reg/app_terms/tos/"

    invoke-direct {v0, v1, v4, v2, v3}, LX/HZW;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/HZW;->TERMS_OF_SERVICE:LX/HZW;

    .line 2482240
    new-instance v0, LX/HZW;

    const-string v1, "DATA_POLICY"

    const v2, 0x7f0835a7

    const-string v3, "https://m.facebook.com/reg/app_terms/data_policy/"

    invoke-direct {v0, v1, v5, v2, v3}, LX/HZW;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/HZW;->DATA_POLICY:LX/HZW;

    .line 2482241
    new-instance v0, LX/HZW;

    const-string v1, "LOCATION_SUPPLEMENT"

    const v2, 0x7f0835a8

    const-string v3, "https://m.facebook.com/reg/app_terms/location/"

    invoke-direct {v0, v1, v6, v2, v3}, LX/HZW;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/HZW;->LOCATION_SUPPLEMENT:LX/HZW;

    .line 2482242
    const/4 v0, 0x3

    new-array v0, v0, [LX/HZW;

    sget-object v1, LX/HZW;->TERMS_OF_SERVICE:LX/HZW;

    aput-object v1, v0, v4

    sget-object v1, LX/HZW;->DATA_POLICY:LX/HZW;

    aput-object v1, v0, v5

    sget-object v1, LX/HZW;->LOCATION_SUPPLEMENT:LX/HZW;

    aput-object v1, v0, v6

    sput-object v0, LX/HZW;->$VALUES:[LX/HZW;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2482243
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2482244
    iput p3, p0, LX/HZW;->titleResId:I

    .line 2482245
    iput-object p4, p0, LX/HZW;->url:Ljava/lang/String;

    .line 2482246
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HZW;
    .locals 1

    .prologue
    .line 2482247
    const-class v0, LX/HZW;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HZW;

    return-object v0
.end method

.method public static values()[LX/HZW;
    .locals 1

    .prologue
    .line 2482248
    sget-object v0, LX/HZW;->$VALUES:[LX/HZW;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HZW;

    return-object v0
.end method
