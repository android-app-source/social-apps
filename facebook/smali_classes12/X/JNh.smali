.class public LX/JNh;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field public final a:LX/JNi;

.field public final b:LX/2g9;

.field public final c:Landroid/content/res/Resources;

.field public final d:LX/1vg;

.field public final e:LX/20i;

.field public final f:LX/1Ck;

.field public final g:LX/25G;

.field public final h:LX/0Zb;


# direct methods
.method public constructor <init>(LX/JNi;LX/2g9;Landroid/content/res/Resources;LX/1vg;LX/20i;LX/1Ck;LX/25G;LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2686314
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2686315
    iput-object p1, p0, LX/JNh;->a:LX/JNi;

    .line 2686316
    iput-object p2, p0, LX/JNh;->b:LX/2g9;

    .line 2686317
    iput-object p3, p0, LX/JNh;->c:Landroid/content/res/Resources;

    .line 2686318
    iput-object p4, p0, LX/JNh;->d:LX/1vg;

    .line 2686319
    iput-object p5, p0, LX/JNh;->e:LX/20i;

    .line 2686320
    iput-object p6, p0, LX/JNh;->f:LX/1Ck;

    .line 2686321
    iput-object p7, p0, LX/JNh;->g:LX/25G;

    .line 2686322
    iput-object p8, p0, LX/JNh;->h:LX/0Zb;

    .line 2686323
    return-void
.end method

.method public static a(LX/0QB;)LX/JNh;
    .locals 12

    .prologue
    .line 2686324
    const-class v1, LX/JNh;

    monitor-enter v1

    .line 2686325
    :try_start_0
    sget-object v0, LX/JNh;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2686326
    sput-object v2, LX/JNh;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2686327
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2686328
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2686329
    new-instance v3, LX/JNh;

    invoke-static {v0}, LX/JNi;->a(LX/0QB;)LX/JNi;

    move-result-object v4

    check-cast v4, LX/JNi;

    invoke-static {v0}, LX/2g9;->a(LX/0QB;)LX/2g9;

    move-result-object v5

    check-cast v5, LX/2g9;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v6

    check-cast v6, Landroid/content/res/Resources;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v7

    check-cast v7, LX/1vg;

    invoke-static {v0}, LX/20i;->a(LX/0QB;)LX/20i;

    move-result-object v8

    check-cast v8, LX/20i;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v9

    check-cast v9, LX/1Ck;

    invoke-static {v0}, LX/25G;->b(LX/0QB;)LX/25G;

    move-result-object v10

    check-cast v10, LX/25G;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v11

    check-cast v11, LX/0Zb;

    invoke-direct/range {v3 .. v11}, LX/JNh;-><init>(LX/JNi;LX/2g9;Landroid/content/res/Resources;LX/1vg;LX/20i;LX/1Ck;LX/25G;LX/0Zb;)V

    .line 2686330
    move-object v0, v3

    .line 2686331
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2686332
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JNh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2686333
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2686334
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
