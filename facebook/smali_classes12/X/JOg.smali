.class public final LX/JOg;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JOi;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

.field public c:LX/1Po;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic d:LX/JOi;


# direct methods
.method public constructor <init>(LX/JOi;)V
    .locals 1

    .prologue
    .line 2688003
    iput-object p1, p0, LX/JOg;->d:LX/JOi;

    .line 2688004
    move-object v0, p1

    .line 2688005
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2688006
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2688007
    const-string v0, "AdsSingleCampaignInsightsBodyComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2688008
    if-ne p0, p1, :cond_1

    .line 2688009
    :cond_0
    :goto_0
    return v0

    .line 2688010
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2688011
    goto :goto_0

    .line 2688012
    :cond_3
    check-cast p1, LX/JOg;

    .line 2688013
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2688014
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2688015
    if-eq v2, v3, :cond_0

    .line 2688016
    iget-object v2, p0, LX/JOg;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JOg;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/JOg;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2688017
    goto :goto_0

    .line 2688018
    :cond_5
    iget-object v2, p1, LX/JOg;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 2688019
    :cond_6
    iget-object v2, p0, LX/JOg;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/JOg;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    iget-object v3, p1, LX/JOg;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2688020
    goto :goto_0

    .line 2688021
    :cond_8
    iget-object v2, p1, LX/JOg;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    if-nez v2, :cond_7

    .line 2688022
    :cond_9
    iget-object v2, p0, LX/JOg;->c:LX/1Po;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/JOg;->c:LX/1Po;

    iget-object v3, p1, LX/JOg;->c:LX/1Po;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2688023
    goto :goto_0

    .line 2688024
    :cond_a
    iget-object v2, p1, LX/JOg;->c:LX/1Po;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
