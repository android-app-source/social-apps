.class public LX/HSk;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0tX;

.field public final b:LX/1Ck;

.field private final c:LX/1My;

.field public d:I

.field public e:LX/HSM;

.field private final f:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0tX;LX/1Ck;LX/1My;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2467456
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2467457
    new-instance v0, LX/HSb;

    invoke-direct {v0, p0}, LX/HSb;-><init>(LX/HSk;)V

    iput-object v0, p0, LX/HSk;->f:LX/0TF;

    .line 2467458
    iput-object p1, p0, LX/HSk;->a:LX/0tX;

    .line 2467459
    iput-object p2, p0, LX/HSk;->b:LX/1Ck;

    .line 2467460
    iput-object p3, p0, LX/HSk;->c:LX/1My;

    .line 2467461
    return-void
.end method

.method public static a$redex0(LX/HSk;Ljava/util/List;Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;",
            ">;",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$PageUpcomingEventsQueryModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2467462
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    .line 2467463
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 2467464
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v6, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2467465
    new-instance v1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2467466
    iget-object v3, p2, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v3, v3

    .line 2467467
    iget-wide v7, p2, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v4, v7

    .line 2467468
    invoke-direct/range {v1 .. v6}, Lcom/facebook/graphql/executor/GraphQLResult;-><init>(Ljava/lang/Object;LX/0ta;JLjava/util/Set;)V

    .line 2467469
    iget-object v3, p0, LX/HSk;->c:LX/1My;

    iget-object v4, p0, LX/HSk;->f:LX/0TF;

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v4, v2, v1}, LX/1My;->a(LX/0TF;Ljava/lang/String;Lcom/facebook/graphql/executor/GraphQLResult;)V

    goto :goto_0

    .line 2467470
    :cond_0
    return-void
.end method
