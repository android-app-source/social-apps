.class public final LX/HX3;
.super LX/HDh;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V
    .locals 0

    .prologue
    .line 2477206
    iput-object p1, p0, LX/HX3;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    invoke-direct {p0}, LX/HDh;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 12

    .prologue
    .line 2477207
    check-cast p1, LX/HDg;

    .line 2477208
    iget-object v0, p0, LX/HX3;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    .line 2477209
    iget-object v1, p1, LX/HDg;->a:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-object v1, v1

    .line 2477210
    if-eqz v1, :cond_1

    invoke-static {v0, v1}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->f(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;Lcom/facebook/graphql/enums/GraphQLPageActionType;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2477211
    iget-object v3, v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aM:LX/4At;

    if-nez v3, :cond_0

    .line 2477212
    new-instance v3, LX/4At;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0817cf

    invoke-direct {v3, v4, v5}, LX/4At;-><init>(Landroid/content/Context;I)V

    iput-object v3, v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aM:LX/4At;

    .line 2477213
    :cond_0
    iget-object v3, v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aM:LX/4At;

    invoke-virtual {v3}, LX/4At;->beginShowingProgress()V

    .line 2477214
    iget-object v3, v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->K:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/HQ9;

    iget-object v3, v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    .line 2477215
    iget-wide v10, v3, LX/CZd;->a:J

    move-wide v5, v10

    .line 2477216
    const/4 v8, 0x0

    new-instance v9, LX/HWu;

    invoke-direct {v9, v0}, LX/HWu;-><init>(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V

    move-object v7, v1

    invoke-virtual/range {v4 .. v9}, LX/HQ9;->a(JLcom/facebook/graphql/enums/GraphQLPageActionType;Ljava/lang/String;LX/HMt;)V

    .line 2477217
    :goto_0
    return-void

    .line 2477218
    :cond_1
    const v2, 0x7f0817cd

    invoke-static {v0, v2}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->f(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;I)V

    goto :goto_0
.end method
