.class public final LX/IT3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;)V
    .locals 0

    .prologue
    .line 2578741
    iput-object p1, p0, LX/IT3;->a:Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x1771951

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2578742
    iget-object v0, p0, LX/IT3;->a:Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DOL;

    iget-object v2, p0, LX/IT3;->a:Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;

    iget-object v2, v2, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->o:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->S()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupRelatedInformationModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupRelatedInformationModel;->c()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/IT3;->a:Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;

    iget-object v3, v3, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->o:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->S()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupRelatedInformationModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupRelatedInformationModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/DOL;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2578743
    iget-object v2, p0, LX/IT3;->a:Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;

    iget-object v2, v2, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->p:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/IT3;->a:Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/ui/GroupsPlutoniumHeader;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2578744
    const v0, 0x1aef83a8

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
