.class public LX/JJt;
.super LX/0b4;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0b4",
        "<",
        "LX/JIY;",
        "LX/JJp;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/JJt;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2679874
    invoke-direct {p0}, LX/0b4;-><init>()V

    .line 2679875
    return-void
.end method

.method public static a(LX/0QB;)LX/JJt;
    .locals 3

    .prologue
    .line 2679876
    sget-object v0, LX/JJt;->a:LX/JJt;

    if-nez v0, :cond_1

    .line 2679877
    const-class v1, LX/JJt;

    monitor-enter v1

    .line 2679878
    :try_start_0
    sget-object v0, LX/JJt;->a:LX/JJt;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2679879
    if-eqz v2, :cond_0

    .line 2679880
    :try_start_1
    new-instance v0, LX/JJt;

    invoke-direct {v0}, LX/JJt;-><init>()V

    .line 2679881
    move-object v0, v0

    .line 2679882
    sput-object v0, LX/JJt;->a:LX/JJt;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2679883
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2679884
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2679885
    :cond_1
    sget-object v0, LX/JJt;->a:LX/JJt;

    return-object v0

    .line 2679886
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2679887
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
