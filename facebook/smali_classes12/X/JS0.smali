.class public LX/JS0;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1PJ;

.field private final b:LX/8D9;


# direct methods
.method public constructor <init>(LX/1PJ;LX/8D9;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2694583
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2694584
    iput-object p1, p0, LX/JS0;->a:LX/1PJ;

    .line 2694585
    iput-object p2, p0, LX/JS0;->b:LX/8D9;

    .line 2694586
    return-void
.end method

.method public static b(LX/0QB;)LX/JS0;
    .locals 9

    .prologue
    .line 2694587
    new-instance v2, LX/JS0;

    invoke-static {p0}, LX/1PJ;->a(LX/0QB;)LX/1PJ;

    move-result-object v0

    check-cast v0, LX/1PJ;

    .line 2694588
    new-instance v3, LX/8D9;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v4

    check-cast v4, LX/0lC;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v6

    check-cast v6, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v7

    check-cast v7, LX/0iA;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v8

    check-cast v8, LX/0SG;

    invoke-direct/range {v3 .. v8}, LX/8D9;-><init>(LX/0lC;LX/03V;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0iA;LX/0SG;)V

    .line 2694589
    move-object v1, v3

    .line 2694590
    check-cast v1, LX/8D9;

    invoke-direct {v2, v0, v1}, LX/JS0;-><init>(LX/1PJ;LX/8D9;)V

    .line 2694591
    return-object v2
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/view/View;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 2694592
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081856

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2694593
    iget-object v7, p0, LX/JS0;->a:LX/1PJ;

    new-instance v0, LX/8D5;

    const v1, 0x7f030ba4

    new-array v3, v5, [Ljava/lang/Object;

    aput-object p3, v3, v6

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, LX/JS0;->b:LX/8D9;

    move-object v3, p2

    invoke-direct/range {v0 .. v6}, LX/8D5;-><init>(ILjava/lang/String;Landroid/view/View;LX/8D6;ZI)V

    invoke-virtual {v7, v0}, LX/1PJ;->a(LX/8D5;)V

    .line 2694594
    return-void
.end method
