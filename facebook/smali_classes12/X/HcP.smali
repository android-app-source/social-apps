.class public final LX/HcP;
.super Ljava/io/OutputStream;
.source ""


# instance fields
.field public final synthetic a:LX/HcQ;

.field private final b:Lcom/facebook/tigon/tigonapi/TigonBodyStream;

.field private c:Ljava/nio/ByteBuffer;

.field public d:Z


# direct methods
.method public constructor <init>(LX/HcQ;Lcom/facebook/tigon/tigonapi/TigonBodyStream;)V
    .locals 1

    .prologue
    .line 2487060
    iput-object p1, p0, LX/HcP;->a:LX/HcQ;

    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 2487061
    const/16 v0, 0x1000

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, LX/HcP;->c:Ljava/nio/ByteBuffer;

    .line 2487062
    iput-object p2, p0, LX/HcP;->b:Lcom/facebook/tigon/tigonapi/TigonBodyStream;

    .line 2487063
    return-void
.end method

.method private c()V
    .locals 1

    .prologue
    .line 2487056
    iget-object v0, p0, LX/HcP;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2487057
    invoke-virtual {p0}, LX/HcP;->a()V

    .line 2487058
    const/16 v0, 0x1000

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, LX/HcP;->c:Ljava/nio/ByteBuffer;

    .line 2487059
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2487037
    iget-object v0, p0, LX/HcP;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    if-lez v0, :cond_0

    .line 2487038
    iget-object v0, p0, LX/HcP;->b:Lcom/facebook/tigon/tigonapi/TigonBodyStream;

    iget-object v1, p0, LX/HcP;->c:Ljava/nio/ByteBuffer;

    iget-object v2, p0, LX/HcP;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->position()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/tigon/tigonapi/TigonBodyStream;->a(Ljava/nio/ByteBuffer;I)I

    move-result v0

    if-ne v3, v0, :cond_0

    .line 2487039
    iput-boolean v3, p0, LX/HcP;->d:Z

    .line 2487040
    :cond_0
    return-void
.end method

.method public final write(I)V
    .locals 2

    .prologue
    .line 2487051
    invoke-direct {p0}, LX/HcP;->c()V

    .line 2487052
    iget-boolean v0, p0, LX/HcP;->d:Z

    if-eqz v0, :cond_0

    .line 2487053
    :goto_0
    return-void

    .line 2487054
    :cond_0
    iget-object v0, p0, LX/HcP;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2487055
    iget-object v0, p0, LX/HcP;->c:Ljava/nio/ByteBuffer;

    int-to-byte v1, p1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    goto :goto_0
.end method

.method public final write([BII)V
    .locals 2

    .prologue
    .line 2487041
    :goto_0
    if-lez p3, :cond_0

    .line 2487042
    invoke-direct {p0}, LX/HcP;->c()V

    .line 2487043
    iget-boolean v0, p0, LX/HcP;->d:Z

    if-eqz v0, :cond_1

    .line 2487044
    :cond_0
    return-void

    .line 2487045
    :cond_1
    iget-object v0, p0, LX/HcP;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2487046
    iget-object v0, p0, LX/HcP;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    invoke-static {v0, p3}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 2487047
    iget-object v1, p0, LX/HcP;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, p1, p2, v0}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 2487048
    add-int/2addr p2, v0

    .line 2487049
    sub-int/2addr p3, v0

    .line 2487050
    goto :goto_0
.end method
