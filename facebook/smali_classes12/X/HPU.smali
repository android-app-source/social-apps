.class public final LX/HPU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0Px;

.field public final synthetic b:LX/CfG;

.field public final synthetic c:LX/HPW;


# direct methods
.method public constructor <init>(LX/HPW;LX/0Px;LX/CfG;)V
    .locals 0

    .prologue
    .line 2462145
    iput-object p1, p0, LX/HPU;->c:LX/HPW;

    iput-object p2, p0, LX/HPU;->a:LX/0Px;

    iput-object p3, p0, LX/HPU;->b:LX/CfG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 2462146
    iget-object v0, p0, LX/HPU;->c:LX/HPW;

    iget-boolean v0, v0, LX/HPW;->n:Z

    if-nez v0, :cond_0

    .line 2462147
    :goto_0
    return-object v5

    .line 2462148
    :cond_0
    iget-object v0, p0, LX/HPU;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    move v1, v2

    :goto_1
    if-ge v1, v3, :cond_1

    iget-object v0, p0, LX/HPU;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9qT;

    .line 2462149
    iget-object v4, p0, LX/HPU;->c:LX/HPW;

    iget-object v4, v4, LX/HPW;->l:LX/2jY;

    invoke-virtual {v4, v0}, LX/2jY;->a(LX/9qT;)V

    .line 2462150
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2462151
    :cond_1
    iget-object v0, p0, LX/HPU;->c:LX/HPW;

    iget-object v0, v0, LX/HPW;->l:LX/2jY;

    .line 2462152
    iput-boolean v2, v0, LX/2jY;->n:Z

    .line 2462153
    iget-object v0, p0, LX/HPU;->c:LX/HPW;

    invoke-static {v0}, LX/HPW;->d(LX/HPW;)V

    .line 2462154
    iget-object v0, p0, LX/HPU;->b:LX/CfG;

    invoke-interface {v0}, LX/CfG;->kL_()V

    goto :goto_0
.end method
