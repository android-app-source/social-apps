.class public LX/I8W;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0g1;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0g1",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/Object;


# instance fields
.field private final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/IBE",
            "<*>;>;"
        }
    .end annotation
.end field

.field public c:LX/IBE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/IBE",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/IBE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/IBE",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0fz;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2541614
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/I8W;->a:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2541611
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2541612
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/I8W;->b:Ljava/util/ArrayList;

    .line 2541613
    return-void
.end method

.method private a(LX/IBE;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/IBE",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 2541608
    if-eqz p1, :cond_0

    .line 2541609
    iget-object v0, p0, LX/I8W;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2541610
    :cond_0
    return-void
.end method

.method public static c(LX/I8W;)V
    .locals 1

    .prologue
    .line 2541604
    iget-object v0, p0, LX/I8W;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2541605
    iget-object v0, p0, LX/I8W;->c:LX/IBE;

    invoke-direct {p0, v0}, LX/I8W;->a(LX/IBE;)V

    .line 2541606
    iget-object v0, p0, LX/I8W;->d:LX/IBE;

    invoke-direct {p0, v0}, LX/I8W;->a(LX/IBE;)V

    .line 2541607
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 1

    .prologue
    .line 2541599
    iget-object v0, p0, LX/I8W;->c:LX/IBE;

    .line 2541600
    if-nez v0, :cond_0

    const/4 p0, 0x0

    :goto_0
    move-object v0, p0

    .line 2541601
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0

    .line 2541602
    :cond_0
    iget-object p0, v0, LX/IBE;->a:Ljava/lang/Object;

    move-object p0, p0

    .line 2541603
    goto :goto_0
.end method

.method public final a(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2541596
    iget-object v0, p0, LX/I8W;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 2541597
    if-ge p1, v0, :cond_0

    iget-object v0, p0, LX/I8W;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LX/I8W;->e:LX/0fz;

    sub-int v0, p1, v0

    invoke-virtual {v1, v0}, LX/0fz;->b(I)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v0

    goto :goto_0
.end method

.method public final size()I
    .locals 2

    .prologue
    .line 2541598
    iget-object v0, p0, LX/I8W;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget-object v0, p0, LX/I8W;->e:LX/0fz;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, LX/I8W;->e:LX/0fz;

    invoke-virtual {v0}, LX/0fz;->size()I

    move-result v0

    goto :goto_0
.end method
