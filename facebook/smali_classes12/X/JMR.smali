.class public final LX/JMR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/JMS;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/JMT;


# direct methods
.method public constructor <init>(LX/JMT;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 2683937
    iput-object p1, p0, LX/JMR;->c:LX/JMT;

    iput p2, p0, LX/JMR;->a:I

    iput-object p3, p0, LX/JMR;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2683933
    iget-object v0, p0, LX/JMR;->c:LX/JMT;

    iget v1, p0, LX/JMR;->a:I

    invoke-static {v0, v1}, LX/JMT;->a(LX/JMT;I)LX/15D;

    .line 2683934
    const-class v0, LX/JMT;

    const-string v1, "Error while invoking request"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2683935
    iget-object v0, p0, LX/JMR;->c:LX/JMT;

    iget v1, p0, LX/JMR;->a:I

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/JMT;->b(LX/JMT;ILjava/lang/String;)V

    .line 2683936
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2683927
    check-cast p1, LX/JMS;

    .line 2683928
    invoke-static {p1}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JMS;

    .line 2683929
    iget-object v1, p0, LX/JMR;->c:LX/JMT;

    iget v2, p0, LX/JMR;->a:I

    invoke-static {v1, v2}, LX/JMT;->a(LX/JMT;I)LX/15D;

    .line 2683930
    iget-object v1, p0, LX/JMR;->c:LX/JMT;

    iget v2, p0, LX/JMR;->a:I

    iget-object v3, p0, LX/JMR;->b:Ljava/lang/String;

    .line 2683931
    invoke-static {v1, v2, v0, v3}, LX/JMT;->a$redex0(LX/JMT;ILX/JMS;Ljava/lang/String;)V

    .line 2683932
    return-void
.end method
