.class public final LX/HxJ;
.super LX/0Rr;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Rr",
        "<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/HxK;

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>(LX/HxK;)V
    .locals 0

    .prologue
    .line 2522321
    iput-object p1, p0, LX/HxJ;->a:LX/HxK;

    invoke-direct {p0}, LX/0Rr;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2522322
    iget v0, p0, LX/HxJ;->b:I

    iget-object v1, p0, LX/HxJ;->a:LX/HxK;

    iget-object v1, v1, LX/HxK;->c:LX/Bl1;

    invoke-virtual {v1}, LX/Bl1;->b()I

    move-result v1

    if-ge v0, v1, :cond_3

    iget v0, p0, LX/HxJ;->c:I

    iget-object v1, p0, LX/HxJ;->a:LX/HxK;

    iget-object v1, v1, LX/HxK;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 2522323
    iget-object v0, p0, LX/HxJ;->a:LX/HxK;

    iget-object v0, v0, LX/HxK;->c:LX/Bl1;

    iget v1, p0, LX/HxJ;->b:I

    invoke-virtual {v0, v1}, LX/Bl1;->a(I)Z

    .line 2522324
    iget-object v0, p0, LX/HxJ;->a:LX/HxK;

    iget-object v0, v0, LX/HxK;->c:LX/Bl1;

    invoke-virtual {v0}, LX/Bl1;->c()J

    move-result-wide v2

    .line 2522325
    iget-object v0, p0, LX/HxJ;->a:LX/HxK;

    iget-object v0, v0, LX/HxK;->d:Ljava/util/List;

    iget v1, p0, LX/HxJ;->c:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/model/Event;

    .line 2522326
    invoke-virtual {v0}, Lcom/facebook/events/model/Event;->M()J

    move-result-wide v0

    .line 2522327
    iget-object v4, p0, LX/HxJ;->a:LX/HxK;

    iget-boolean v4, v4, LX/HxK;->e:Z

    if-eqz v4, :cond_1

    cmp-long v4, v2, v0

    if-gez v4, :cond_2

    .line 2522328
    :cond_0
    iget v0, p0, LX/HxJ;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/HxJ;->b:I

    .line 2522329
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 2522330
    :goto_0
    return-object v0

    .line 2522331
    :cond_1
    cmp-long v4, v2, v0

    if-gtz v4, :cond_0

    .line 2522332
    :cond_2
    iget v2, p0, LX/HxJ;->c:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, LX/HxJ;->c:I

    .line 2522333
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 2522334
    :cond_3
    iget v0, p0, LX/HxJ;->b:I

    iget-object v1, p0, LX/HxJ;->a:LX/HxK;

    iget-object v1, v1, LX/HxK;->c:LX/Bl1;

    invoke-virtual {v1}, LX/Bl1;->b()I

    move-result v1

    if-ge v0, v1, :cond_4

    .line 2522335
    iget-object v0, p0, LX/HxJ;->a:LX/HxK;

    iget-object v0, v0, LX/HxK;->c:LX/Bl1;

    iget v1, p0, LX/HxJ;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/HxJ;->b:I

    invoke-virtual {v0, v1}, LX/Bl1;->a(I)Z

    .line 2522336
    iget-object v0, p0, LX/HxJ;->a:LX/HxK;

    iget-object v0, v0, LX/HxK;->c:LX/Bl1;

    invoke-virtual {v0}, LX/Bl1;->c()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 2522337
    :cond_4
    iget v0, p0, LX/HxJ;->c:I

    iget-object v1, p0, LX/HxJ;->a:LX/HxK;

    iget-object v1, v1, LX/HxK;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_5

    .line 2522338
    iget-object v0, p0, LX/HxJ;->a:LX/HxK;

    iget-object v0, v0, LX/HxK;->d:Ljava/util/List;

    iget v1, p0, LX/HxJ;->c:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LX/HxJ;->c:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/model/Event;

    invoke-virtual {v0}, Lcom/facebook/events/model/Event;->M()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 2522339
    :cond_5
    invoke-virtual {p0}, LX/0Rr;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    goto :goto_0
.end method
