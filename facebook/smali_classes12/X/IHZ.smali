.class public final LX/IHZ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 2560477
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2560478
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 2560479
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 2560480
    invoke-static {p0, p1}, LX/IHZ;->b(LX/15w;LX/186;)I

    move-result v1

    .line 2560481
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2560482
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2560483
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2560484
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 2560485
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/IHZ;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2560486
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2560487
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2560488
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 2560489
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_8

    .line 2560490
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2560491
    :goto_0
    return v1

    .line 2560492
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2560493
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_7

    .line 2560494
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 2560495
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2560496
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_1

    if-eqz v7, :cond_1

    .line 2560497
    const-string v8, "action_type"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 2560498
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    goto :goto_1

    .line 2560499
    :cond_2
    const-string v8, "additional_item_description"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 2560500
    invoke-static {p0, p1}, LX/IHU;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 2560501
    :cond_3
    const-string v8, "contact"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 2560502
    invoke-static {p0, p1}, LX/IHW;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 2560503
    :cond_4
    const-string v8, "id"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 2560504
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 2560505
    :cond_5
    const-string v8, "item_description"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 2560506
    invoke-static {p0, p1}, LX/IHX;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 2560507
    :cond_6
    const-string v8, "item_explanation"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2560508
    invoke-static {p0, p1}, LX/IHY;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 2560509
    :cond_7
    const/4 v7, 0x6

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 2560510
    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 2560511
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 2560512
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 2560513
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 2560514
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2560515
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2560516
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_8
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2560517
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2560518
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 2560519
    if-eqz v0, :cond_0

    .line 2560520
    const-string v0, "action_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2560521
    invoke-virtual {p0, p1, v1}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2560522
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2560523
    if-eqz v0, :cond_1

    .line 2560524
    const-string v1, "additional_item_description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2560525
    invoke-static {p0, v0, p2}, LX/IHU;->a(LX/15i;ILX/0nX;)V

    .line 2560526
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2560527
    if-eqz v0, :cond_2

    .line 2560528
    const-string v1, "contact"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2560529
    invoke-static {p0, v0, p2, p3}, LX/IHW;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2560530
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2560531
    if-eqz v0, :cond_3

    .line 2560532
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2560533
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2560534
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2560535
    if-eqz v0, :cond_4

    .line 2560536
    const-string v1, "item_description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2560537
    invoke-static {p0, v0, p2}, LX/IHX;->a(LX/15i;ILX/0nX;)V

    .line 2560538
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2560539
    if-eqz v0, :cond_5

    .line 2560540
    const-string v1, "item_explanation"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2560541
    invoke-static {p0, v0, p2}, LX/IHY;->a(LX/15i;ILX/0nX;)V

    .line 2560542
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2560543
    return-void
.end method
