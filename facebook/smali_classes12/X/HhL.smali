.class public final enum LX/HhL;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HhL;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/HhL;

.field public static final enum HTTP:LX/HhL;

.field public static final enum HTTPS:LX/HhL;

.field public static final enum HTTPS_443:LX/HhL;

.field public static final enum HTTP_80:LX/HhL;

.field public static final enum HTTP_AKAMAI:LX/HhL;

.field public static final enum HTTP_OPERA:LX/HhL;

.field public static final enum MQTT_SSL:LX/HhL;

.field public static final enum PROXY_CONNECT:LX/HhL;

.field public static final enum SSL:LX/HhL;

.field public static final enum TCP_OPEN:LX/HhL;

.field public static final enum TCP_OPEN_443:LX/HhL;

.field public static final enum TCP_OPEN_80:LX/HhL;

.field public static final enum UDP_3478:LX/HhL;

.field public static final enum VOIP_TURN:LX/HhL;

.field public static final enum WA_443:LX/HhL;


# instance fields
.field public final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2495437
    new-instance v0, LX/HhL;

    const-string v1, "TCP_OPEN"

    const-string v2, "tcp"

    invoke-direct {v0, v1, v4, v2}, LX/HhL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HhL;->TCP_OPEN:LX/HhL;

    .line 2495438
    new-instance v0, LX/HhL;

    const-string v1, "TCP_OPEN_80"

    const-string v2, "tcp80"

    invoke-direct {v0, v1, v5, v2}, LX/HhL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HhL;->TCP_OPEN_80:LX/HhL;

    .line 2495439
    new-instance v0, LX/HhL;

    const-string v1, "TCP_OPEN_443"

    const-string v2, "tcp443"

    invoke-direct {v0, v1, v6, v2}, LX/HhL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HhL;->TCP_OPEN_443:LX/HhL;

    .line 2495440
    new-instance v0, LX/HhL;

    const-string v1, "SSL"

    const-string v2, "ssl"

    invoke-direct {v0, v1, v7, v2}, LX/HhL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HhL;->SSL:LX/HhL;

    .line 2495441
    new-instance v0, LX/HhL;

    const-string v1, "VOIP_TURN"

    const-string v2, "turn"

    invoke-direct {v0, v1, v8, v2}, LX/HhL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HhL;->VOIP_TURN:LX/HhL;

    .line 2495442
    new-instance v0, LX/HhL;

    const-string v1, "MQTT_SSL"

    const/4 v2, 0x5

    const-string v3, "mqtt_ssl"

    invoke-direct {v0, v1, v2, v3}, LX/HhL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HhL;->MQTT_SSL:LX/HhL;

    .line 2495443
    new-instance v0, LX/HhL;

    const-string v1, "HTTP"

    const/4 v2, 0x6

    const-string v3, "http"

    invoke-direct {v0, v1, v2, v3}, LX/HhL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HhL;->HTTP:LX/HhL;

    .line 2495444
    new-instance v0, LX/HhL;

    const-string v1, "HTTP_80"

    const/4 v2, 0x7

    const-string v3, "http80"

    invoke-direct {v0, v1, v2, v3}, LX/HhL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HhL;->HTTP_80:LX/HhL;

    .line 2495445
    new-instance v0, LX/HhL;

    const-string v1, "HTTP_AKAMAI"

    const/16 v2, 0x8

    const-string v3, "http_akamai"

    invoke-direct {v0, v1, v2, v3}, LX/HhL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HhL;->HTTP_AKAMAI:LX/HhL;

    .line 2495446
    new-instance v0, LX/HhL;

    const-string v1, "HTTP_OPERA"

    const/16 v2, 0x9

    const-string v3, "http_opera"

    invoke-direct {v0, v1, v2, v3}, LX/HhL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HhL;->HTTP_OPERA:LX/HhL;

    .line 2495447
    new-instance v0, LX/HhL;

    const-string v1, "HTTPS"

    const/16 v2, 0xa

    const-string v3, "https"

    invoke-direct {v0, v1, v2, v3}, LX/HhL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HhL;->HTTPS:LX/HhL;

    .line 2495448
    new-instance v0, LX/HhL;

    const-string v1, "HTTPS_443"

    const/16 v2, 0xb

    const-string v3, "https443"

    invoke-direct {v0, v1, v2, v3}, LX/HhL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HhL;->HTTPS_443:LX/HhL;

    .line 2495449
    new-instance v0, LX/HhL;

    const-string v1, "PROXY_CONNECT"

    const/16 v2, 0xc

    const-string v3, "proxy_connect"

    invoke-direct {v0, v1, v2, v3}, LX/HhL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HhL;->PROXY_CONNECT:LX/HhL;

    .line 2495450
    new-instance v0, LX/HhL;

    const-string v1, "UDP_3478"

    const/16 v2, 0xd

    const-string v3, "udp_3478"

    invoke-direct {v0, v1, v2, v3}, LX/HhL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HhL;->UDP_3478:LX/HhL;

    .line 2495451
    new-instance v0, LX/HhL;

    const-string v1, "WA_443"

    const/16 v2, 0xe

    const-string v3, "wa_443"

    invoke-direct {v0, v1, v2, v3}, LX/HhL;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HhL;->WA_443:LX/HhL;

    .line 2495452
    const/16 v0, 0xf

    new-array v0, v0, [LX/HhL;

    sget-object v1, LX/HhL;->TCP_OPEN:LX/HhL;

    aput-object v1, v0, v4

    sget-object v1, LX/HhL;->TCP_OPEN_80:LX/HhL;

    aput-object v1, v0, v5

    sget-object v1, LX/HhL;->TCP_OPEN_443:LX/HhL;

    aput-object v1, v0, v6

    sget-object v1, LX/HhL;->SSL:LX/HhL;

    aput-object v1, v0, v7

    sget-object v1, LX/HhL;->VOIP_TURN:LX/HhL;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/HhL;->MQTT_SSL:LX/HhL;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/HhL;->HTTP:LX/HhL;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/HhL;->HTTP_80:LX/HhL;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/HhL;->HTTP_AKAMAI:LX/HhL;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/HhL;->HTTP_OPERA:LX/HhL;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/HhL;->HTTPS:LX/HhL;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/HhL;->HTTPS_443:LX/HhL;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/HhL;->PROXY_CONNECT:LX/HhL;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/HhL;->UDP_3478:LX/HhL;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/HhL;->WA_443:LX/HhL;

    aput-object v2, v0, v1

    sput-object v0, LX/HhL;->$VALUES:[LX/HhL;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2495434
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2495435
    iput-object p3, p0, LX/HhL;->name:Ljava/lang/String;

    .line 2495436
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HhL;
    .locals 1

    .prologue
    .line 2495433
    const-class v0, LX/HhL;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HhL;

    return-object v0
.end method

.method public static valueOfName(Ljava/lang/String;)LX/HhL;
    .locals 5

    .prologue
    .line 2495428
    invoke-static {}, LX/HhL;->values()[LX/HhL;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 2495429
    iget-object v4, v3, LX/HhL;->name:Ljava/lang/String;

    invoke-static {p0, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2495430
    return-object v3

    .line 2495431
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2495432
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " isn\'t a valid ConnectionTestType"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOrDefault(Ljava/lang/String;)LX/HhL;
    .locals 5

    .prologue
    .line 2495423
    invoke-static {}, LX/HhL;->values()[LX/HhL;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 2495424
    iget-object v4, v0, LX/HhL;->name:Ljava/lang/String;

    invoke-static {p0, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2495425
    :goto_1
    return-object v0

    .line 2495426
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2495427
    :cond_1
    sget-object v0, LX/HhL;->HTTPS_443:LX/HhL;

    goto :goto_1
.end method

.method public static values()[LX/HhL;
    .locals 1

    .prologue
    .line 2495422
    sget-object v0, LX/HhL;->$VALUES:[LX/HhL;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HhL;

    return-object v0
.end method
