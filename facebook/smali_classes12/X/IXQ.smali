.class public LX/IXQ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:I

.field private static final b:I

.field private static y:LX/0Xm;


# instance fields
.field public final c:LX/0So;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/IXE;

.field private final f:LX/Cs3;

.field public final g:LX/Chv;

.field public final h:LX/0Uh;

.field public final i:LX/CH8;

.field public final j:LX/03V;

.field public final k:LX/Ckw;

.field public final l:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/IXL;",
            ">;"
        }
    .end annotation
.end field

.field public m:Ljava/lang/String;

.field public final n:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/IXN;",
            ">;>;"
        }
    .end annotation
.end field

.field public final o:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/IXK;",
            ">;"
        }
    .end annotation
.end field

.field public final p:LX/0p3;

.field private final q:Landroid/os/Handler;

.field public r:Landroid/widget/FrameLayout;

.field public s:Ljava/lang/String;

.field public t:Ljava/lang/String;

.field public u:Z

.field public v:Z

.field private final w:Z

.field public final x:LX/ChN;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2586089
    const-string v0, "tracker_block_uuid"

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    sput v0, LX/IXQ;->a:I

    .line 2586090
    const v0, 0x7f0d020d

    sput v0, LX/IXQ;->b:I

    return-void
.end method

.method public constructor <init>(LX/Cs3;LX/Chv;LX/0Uh;LX/CH8;LX/03V;LX/Ckw;LX/0So;LX/8bK;LX/0Ot;LX/IXE;LX/8bL;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Cs3;",
            "LX/Chv;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/CH8;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/Ckw;",
            "LX/0So;",
            "LX/8bK;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;",
            "LX/IXE;",
            "LX/8bL;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2586091
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2586092
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/IXQ;->l:Ljava/util/Set;

    .line 2586093
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/IXQ;->n:Ljava/util/Map;

    .line 2586094
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/IXQ;->o:Ljava/util/Map;

    .line 2586095
    iput-boolean v1, p0, LX/IXQ;->u:Z

    .line 2586096
    iput-boolean v1, p0, LX/IXQ;->v:Z

    .line 2586097
    new-instance v0, LX/IXH;

    invoke-direct {v0, p0}, LX/IXH;-><init>(LX/IXQ;)V

    iput-object v0, p0, LX/IXQ;->x:LX/ChN;

    .line 2586098
    iput-object p1, p0, LX/IXQ;->f:LX/Cs3;

    .line 2586099
    iput-object p2, p0, LX/IXQ;->g:LX/Chv;

    .line 2586100
    iget-object v0, p0, LX/IXQ;->g:LX/Chv;

    iget-object v1, p0, LX/IXQ;->x:LX/ChN;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2586101
    iput-object p3, p0, LX/IXQ;->h:LX/0Uh;

    .line 2586102
    iput-object p4, p0, LX/IXQ;->i:LX/CH8;

    .line 2586103
    iput-object p5, p0, LX/IXQ;->j:LX/03V;

    .line 2586104
    iput-object p6, p0, LX/IXQ;->k:LX/Ckw;

    .line 2586105
    invoke-virtual {p8}, LX/8bK;->a()LX/0p3;

    move-result-object v0

    iput-object v0, p0, LX/IXQ;->p:LX/0p3;

    .line 2586106
    iput-object p7, p0, LX/IXQ;->c:LX/0So;

    .line 2586107
    iput-object p9, p0, LX/IXQ;->d:LX/0Ot;

    .line 2586108
    invoke-virtual {p11}, LX/8bL;->b()Z

    move-result v0

    iput-boolean v0, p0, LX/IXQ;->w:Z

    .line 2586109
    iput-object p10, p0, LX/IXQ;->e:LX/IXE;

    .line 2586110
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LX/IXQ;->q:Landroid/os/Handler;

    .line 2586111
    return-void
.end method

.method public static a(LX/0QB;)LX/IXQ;
    .locals 15

    .prologue
    .line 2586112
    const-class v1, LX/IXQ;

    monitor-enter v1

    .line 2586113
    :try_start_0
    sget-object v0, LX/IXQ;->y:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2586114
    sput-object v2, LX/IXQ;->y:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2586115
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2586116
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2586117
    new-instance v3, LX/IXQ;

    invoke-static {v0}, LX/Cs3;->a(LX/0QB;)LX/Cs3;

    move-result-object v4

    check-cast v4, LX/Cs3;

    invoke-static {v0}, LX/Chv;->a(LX/0QB;)LX/Chv;

    move-result-object v5

    check-cast v5, LX/Chv;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v6

    check-cast v6, LX/0Uh;

    invoke-static {v0}, LX/CH8;->a(LX/0QB;)LX/CH8;

    move-result-object v7

    check-cast v7, LX/CH8;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-static {v0}, LX/Ckw;->a(LX/0QB;)LX/Ckw;

    move-result-object v9

    check-cast v9, LX/Ckw;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v10

    check-cast v10, LX/0So;

    invoke-static {v0}, LX/8bK;->a(LX/0QB;)LX/8bK;

    move-result-object v11

    check-cast v11, LX/8bK;

    const/16 v12, 0xdf4

    invoke-static {v0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-static {v0}, LX/IXE;->a(LX/0QB;)LX/IXE;

    move-result-object v13

    check-cast v13, LX/IXE;

    invoke-static {v0}, LX/8bL;->b(LX/0QB;)LX/8bL;

    move-result-object v14

    check-cast v14, LX/8bL;

    invoke-direct/range {v3 .. v14}, LX/IXQ;-><init>(LX/Cs3;LX/Chv;LX/0Uh;LX/CH8;LX/03V;LX/Ckw;LX/0So;LX/8bK;LX/0Ot;LX/IXE;LX/8bL;)V

    .line 2586118
    move-object v0, v3

    .line 2586119
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2586120
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IXQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2586121
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2586122
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private declared-synchronized a(Ljava/lang/String;LX/IXO;)V
    .locals 2

    .prologue
    .line 2586133
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/IXQ;->n:Ljava/util/Map;

    iget-object v1, p0, LX/IXQ;->m:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 2586134
    if-eqz v0, :cond_0

    .line 2586135
    new-instance v1, LX/IXN;

    invoke-direct {v1, p0, p2}, LX/IXN;-><init>(LX/IXQ;LX/IXO;)V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2586136
    :cond_0
    monitor-exit p0

    return-void

    .line 2586137
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static a$redex0(LX/IXQ;Ljava/lang/String;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/webkit/WebView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2586123
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2586124
    iget-object v0, p0, LX/IXQ;->r:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v4

    .line 2586125
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_1

    .line 2586126
    iget-object v0, p0, LX/IXQ;->r:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2586127
    instance-of v0, v1, Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    .line 2586128
    sget v0, LX/IXQ;->b:I

    invoke-virtual {v1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2586129
    invoke-static {v0, p1}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 2586130
    check-cast v0, Landroid/webkit/WebView;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2586131
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2586132
    :cond_1
    return-object v3
.end method

.method public static a$redex0(LX/IXQ;Landroid/webkit/WebView;)V
    .locals 3

    .prologue
    .line 2586138
    iget-boolean v0, p0, LX/IXQ;->w:Z

    if-eqz v0, :cond_0

    .line 2586139
    iget-object v0, p0, LX/IXQ;->e:LX/IXE;

    .line 2586140
    if-nez p1, :cond_1

    .line 2586141
    :goto_0
    iget-object v0, p0, LX/IXQ;->r:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 2586142
    return-void

    .line 2586143
    :cond_0
    invoke-static {p1}, LX/Cs3;->b(Landroid/webkit/WebView;)V

    goto :goto_0

    .line 2586144
    :cond_1
    invoke-virtual {v0}, LX/IXE;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, v0, LX/IXE;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    iget v2, v0, LX/IXE;->c:I

    if-lt v1, v2, :cond_3

    .line 2586145
    :cond_2
    invoke-static {p1}, LX/Cs3;->b(Landroid/webkit/WebView;)V

    goto :goto_0

    .line 2586146
    :cond_3
    invoke-static {p1}, LX/Cs3;->c(Landroid/webkit/WebView;)V

    .line 2586147
    invoke-virtual {p1}, Landroid/webkit/WebView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    instance-of v1, v1, Landroid/view/ViewGroup;

    if-eqz v1, :cond_4

    .line 2586148
    invoke-virtual {p1}, Landroid/webkit/WebView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 2586149
    invoke-virtual {v1, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2586150
    :cond_4
    iget-object v1, v0, LX/IXE;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static b(LX/IXQ;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2586069
    iget-object v0, p0, LX/IXQ;->r:Landroid/widget/FrameLayout;

    if-nez v0, :cond_0

    .line 2586070
    :goto_0
    return-void

    .line 2586071
    :cond_0
    sget-object v0, LX/IXO;->JS:LX/IXO;

    invoke-direct {p0, p2, v0}, LX/IXQ;->a(Ljava/lang/String;LX/IXO;)V

    .line 2586072
    invoke-direct {p0}, LX/IXQ;->e()Landroid/webkit/WebView;

    move-result-object v0

    .line 2586073
    new-instance v1, LX/IXP;

    iget-object v2, p0, LX/IXQ;->m:Ljava/lang/String;

    invoke-direct {v1, p0, p2, v2}, LX/IXP;-><init>(LX/IXQ;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 2586074
    iget-object v1, p0, LX/IXQ;->f:LX/Cs3;

    invoke-virtual {v1, v0}, LX/Cs3;->a(Landroid/webkit/WebView;)V

    .line 2586075
    sget v1, LX/IXQ;->a:I

    invoke-virtual {v0, v1, p2}, Landroid/webkit/WebView;->setTag(ILjava/lang/Object;)V

    .line 2586076
    sget v1, LX/IXQ;->b:I

    iget-object v2, p0, LX/IXQ;->m:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/webkit/WebView;->setTag(ILjava/lang/Object;)V

    .line 2586077
    iget-object v1, p0, LX/IXQ;->r:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 2586078
    const-string v3, "text/html"

    const-string v4, "utf-8"

    const/4 v5, 0x0

    move-object v1, p3

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static b$redex0(LX/IXQ;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2586079
    iget-object v0, p0, LX/IXQ;->r:Landroid/widget/FrameLayout;

    if-nez v0, :cond_0

    .line 2586080
    :goto_0
    return-void

    .line 2586081
    :cond_0
    sget-object v0, LX/IXO;->URL:LX/IXO;

    invoke-direct {p0, p2, v0}, LX/IXQ;->a(Ljava/lang/String;LX/IXO;)V

    .line 2586082
    invoke-direct {p0}, LX/IXQ;->e()Landroid/webkit/WebView;

    move-result-object v0

    .line 2586083
    new-instance v1, LX/IXP;

    iget-object v2, p0, LX/IXQ;->m:Ljava/lang/String;

    invoke-direct {v1, p0, p2, v2}, LX/IXP;-><init>(LX/IXQ;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 2586084
    iget-object v1, p0, LX/IXQ;->f:LX/Cs3;

    invoke-virtual {v1, v0}, LX/Cs3;->a(Landroid/webkit/WebView;)V

    .line 2586085
    sget v1, LX/IXQ;->a:I

    invoke-virtual {v0, v1, p2}, Landroid/webkit/WebView;->setTag(ILjava/lang/Object;)V

    .line 2586086
    sget v1, LX/IXQ;->b:I

    iget-object v2, p0, LX/IXQ;->m:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/webkit/WebView;->setTag(ILjava/lang/Object;)V

    .line 2586087
    iget-object v1, p0, LX/IXQ;->r:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 2586088
    invoke-virtual {v0, p1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static c(LX/IXQ;)V
    .locals 6

    .prologue
    .line 2586043
    iget-boolean v0, p0, LX/IXQ;->u:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, LX/IXQ;->v:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2586044
    if-eqz v0, :cond_1

    .line 2586045
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/IXQ;->m:Ljava/lang/String;

    .line 2586046
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2586047
    iget-object v1, p0, LX/IXQ;->n:Ljava/util/Map;

    iget-object v2, p0, LX/IXQ;->m:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2586048
    new-instance v1, LX/IXK;

    invoke-direct {v1, p0, v0}, LX/IXK;-><init>(LX/IXQ;Ljava/util/Map;)V

    .line 2586049
    iget-object v0, p0, LX/IXQ;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v2

    .line 2586050
    iput-wide v2, v1, LX/IXK;->b:J

    .line 2586051
    iget-object v0, p0, LX/IXQ;->o:Ljava/util/Map;

    iget-object v2, p0, LX/IXQ;->m:Ljava/lang/String;

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2586052
    iget-object v0, p0, LX/IXQ;->l:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IXL;

    .line 2586053
    iget-object v2, v0, LX/IXL;->b:Ljava/lang/String;

    invoke-static {v2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 2586054
    iget-object v2, v0, LX/IXL;->b:Ljava/lang/String;

    iget-object v3, v0, LX/IXL;->c:Ljava/lang/String;

    .line 2586055
    iget-object v4, p0, LX/IXQ;->r:Landroid/widget/FrameLayout;

    if-nez v4, :cond_5

    .line 2586056
    :cond_0
    :goto_2
    goto :goto_1

    .line 2586057
    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 2586058
    :cond_3
    iget-object v2, v0, LX/IXL;->a:Ljava/lang/String;

    iget-object v3, v0, LX/IXL;->c:Ljava/lang/String;

    .line 2586059
    iget-object v4, p0, LX/IXQ;->m:Ljava/lang/String;

    invoke-static {p0, v3, v4}, LX/IXQ;->d(LX/IXQ;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 2586060
    :cond_4
    :goto_3
    goto :goto_2

    .line 2586061
    :cond_5
    iget-object v4, p0, LX/IXQ;->m:Ljava/lang/String;

    invoke-static {p0, v3, v4}, LX/IXQ;->d(LX/IXQ;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2586062
    invoke-static {p0}, LX/IXQ;->f(LX/IXQ;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 2586063
    iget-object v4, p0, LX/IXQ;->i:LX/CH8;

    iget-object v5, p0, LX/IXQ;->s:Ljava/lang/String;

    new-instance v0, LX/IXJ;

    invoke-direct {v0, p0, v3}, LX/IXJ;-><init>(LX/IXQ;Ljava/lang/String;)V

    invoke-virtual {v4, v3, v5, v0}, LX/CH8;->a(Ljava/lang/String;Ljava/lang/String;LX/0Ve;)V

    goto :goto_2

    .line 2586064
    :cond_6
    iget-object v4, p0, LX/IXQ;->t:Ljava/lang/String;

    invoke-static {p0, v2, v3, v4}, LX/IXQ;->b(LX/IXQ;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 2586065
    :cond_7
    iget-object v4, p0, LX/IXQ;->r:Landroid/widget/FrameLayout;

    if-eqz v4, :cond_4

    .line 2586066
    invoke-static {p0}, LX/IXQ;->f(LX/IXQ;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 2586067
    iget-object v4, p0, LX/IXQ;->i:LX/CH8;

    iget-object v5, p0, LX/IXQ;->s:Ljava/lang/String;

    new-instance v0, LX/IXI;

    invoke-direct {v0, p0, v3}, LX/IXI;-><init>(LX/IXQ;Ljava/lang/String;)V

    invoke-virtual {v4, v3, v5, v0}, LX/CH8;->a(Ljava/lang/String;Ljava/lang/String;LX/0Ve;)V

    goto :goto_3

    .line 2586068
    :cond_8
    invoke-static {p0, v2, v3}, LX/IXQ;->b$redex0(LX/IXQ;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3
.end method

.method public static d(LX/IXQ;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .param p0    # LX/IXQ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 2586035
    invoke-static {p1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2586036
    :cond_0
    :goto_0
    return v3

    :cond_1
    move v2, v3

    .line 2586037
    :goto_1
    iget-object v0, p0, LX/IXQ;->r:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 2586038
    iget-object v0, p0, LX/IXQ;->r:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    sget v1, LX/IXQ;->b:I

    invoke-virtual {v0, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2586039
    iget-object v1, p0, LX/IXQ;->r:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    sget v4, LX/IXQ;->a:I

    invoke-virtual {v1, v4}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2586040
    invoke-static {v0, p2}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {v1, p1}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2586041
    const/4 v3, 0x1

    goto :goto_0

    .line 2586042
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1
.end method

.method private e()Landroid/webkit/WebView;
    .locals 2

    .prologue
    .line 2586025
    iget-boolean v0, p0, LX/IXQ;->w:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/IXQ;->e:LX/IXE;

    invoke-virtual {v0}, LX/IXE;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2586026
    iget-object v0, p0, LX/IXQ;->e:LX/IXE;

    .line 2586027
    invoke-virtual {v0}, LX/IXE;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2586028
    const/4 v1, 0x0

    .line 2586029
    :goto_0
    move-object v0, v1

    .line 2586030
    :goto_1
    return-object v0

    .line 2586031
    :cond_0
    new-instance v0, LX/0D4;

    iget-object v1, p0, LX/IXQ;->r:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0D4;-><init>(Landroid/content/Context;)V

    goto :goto_1

    .line 2586032
    :cond_1
    iget-object v1, v0, LX/IXE;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2586033
    new-instance v1, LX/0D4;

    iget-object p0, v0, LX/IXE;->b:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    invoke-direct {v1, p0}, LX/0D4;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 2586034
    :cond_2
    iget-object v1, v0, LX/IXE;->a:Ljava/util/List;

    iget-object p0, v0, LX/IXE;->a:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p0

    add-int/lit8 p0, p0, -0x1

    invoke-interface {v1, p0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/webkit/WebView;

    goto :goto_0
.end method

.method public static declared-synchronized e(LX/IXQ;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 2586010
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/IXQ;->n:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2586011
    if-nez v0, :cond_1

    .line 2586012
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 2586013
    :cond_1
    :try_start_1
    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/IXN;

    .line 2586014
    if-eqz v1, :cond_0

    .line 2586015
    sget-object v2, LX/IXM;->SUCCESS:LX/IXM;

    .line 2586016
    iput-object v2, v1, LX/IXN;->e:LX/IXM;

    .line 2586017
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2586018
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IXN;

    iget-object v0, v0, LX/IXN;->e:LX/IXM;

    sget-object v2, LX/IXM;->SUCCESS:LX/IXM;

    if-eq v0, v2, :cond_2

    goto :goto_0

    .line 2586019
    :cond_3
    iget-object v0, p0, LX/IXQ;->o:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IXK;

    .line 2586020
    if-eqz v0, :cond_0

    .line 2586021
    iget-object v1, p0, LX/IXQ;->c:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    iget-wide v4, v0, LX/IXK;->b:J

    sub-long/2addr v2, v4

    .line 2586022
    iput-wide v2, v0, LX/IXK;->c:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2586023
    goto :goto_0

    .line 2586024
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized f(LX/IXQ;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2586001
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/IXQ;->n:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2586002
    if-nez v0, :cond_1

    .line 2586003
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 2586004
    :cond_1
    :try_start_1
    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IXN;

    .line 2586005
    if-eqz v0, :cond_0

    .line 2586006
    sget-object v1, LX/IXM;->FAILED:LX/IXM;

    .line 2586007
    iput-object v1, v0, LX/IXN;->e:LX/IXM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2586008
    goto :goto_0

    .line 2586009
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static f(LX/IXQ;)Z
    .locals 3

    .prologue
    .line 2586000
    iget-object v0, p0, LX/IXQ;->h:LX/0Uh;

    const/16 v1, 0x3d4

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 13

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2585976
    iput-boolean v4, p0, LX/IXQ;->v:Z

    .line 2585977
    iget-object v0, p0, LX/IXQ;->n:Ljava/util/Map;

    iget-object v1, p0, LX/IXQ;->m:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 2585978
    if-nez v0, :cond_1

    .line 2585979
    :cond_0
    :goto_0
    return-void

    .line 2585980
    :cond_1
    iget-object v1, p0, LX/IXQ;->o:Ljava/util/Map;

    iget-object v2, p0, LX/IXQ;->m:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/IXK;

    .line 2585981
    iget-boolean v2, v1, LX/IXK;->e:Z

    if-nez v2, :cond_0

    .line 2585982
    iput-boolean v3, v1, LX/IXK;->e:Z

    .line 2585983
    iget-object v2, p0, LX/IXQ;->r:Landroid/widget/FrameLayout;

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/IXQ;->r:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v2

    if-eqz v2, :cond_0

    .line 2585984
    iget-object v2, p0, LX/IXQ;->c:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v6

    iget-wide v8, v1, LX/IXK;->b:J

    sub-long/2addr v6, v8

    .line 2585985
    iget-object v1, p0, LX/IXQ;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0W3;

    sget-wide v8, LX/0X5;->fq:J

    const-wide/16 v10, 0x2ee0

    invoke-interface {v1, v8, v9, v10, v11}, LX/0W4;->a(JJ)J

    move-result-wide v8

    .line 2585986
    iget-object v1, p0, LX/IXQ;->h:LX/0Uh;

    const/16 v2, 0x4ff

    invoke-virtual {v1, v2, v4}, LX/0Uh;->a(IZ)Z

    move-result v5

    .line 2585987
    iget-object v1, p0, LX/IXQ;->m:Ljava/lang/String;

    invoke-static {p0, v1}, LX/IXQ;->a$redex0(LX/IXQ;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 2585988
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_2
    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/webkit/WebView;

    .line 2585989
    sget v2, LX/IXQ;->a:I

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2585990
    invoke-static {v2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_2

    .line 2585991
    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/IXN;

    .line 2585992
    iget-object v11, v2, LX/IXN;->e:LX/IXM;

    sget-object v12, LX/IXM;->FAILED:LX/IXM;

    if-eq v11, v12, :cond_3

    iget-object v2, v2, LX/IXN;->e:LX/IXM;

    sget-object v11, LX/IXM;->SUCCESS:LX/IXM;

    if-ne v2, v11, :cond_2

    .line 2585993
    :cond_3
    invoke-static {p0, v1}, LX/IXQ;->a$redex0(LX/IXQ;Landroid/webkit/WebView;)V

    goto :goto_1

    .line 2585994
    :cond_4
    iget-object v0, p0, LX/IXQ;->m:Ljava/lang/String;

    invoke-static {p0, v0}, LX/IXQ;->a$redex0(LX/IXQ;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_6

    move v0, v3

    .line 2585995
    :goto_2
    new-instance v1, Lcom/facebook/instantarticles/ThirdPartyTrackerHandler$KillTrackersForArticleOpenRunnable;

    iget-object v2, p0, LX/IXQ;->m:Ljava/lang/String;

    invoke-direct {v1, p0, v2}, Lcom/facebook/instantarticles/ThirdPartyTrackerHandler$KillTrackersForArticleOpenRunnable;-><init>(LX/IXQ;Ljava/lang/String;)V

    .line 2585996
    cmp-long v2, v6, v8

    if-gtz v2, :cond_5

    if-eqz v5, :cond_5

    if-eqz v0, :cond_7

    .line 2585997
    :cond_5
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto/16 :goto_0

    :cond_6
    move v0, v4

    .line 2585998
    goto :goto_2

    .line 2585999
    :cond_7
    iget-object v0, p0, LX/IXQ;->q:Landroid/os/Handler;

    sub-long v2, v8, v6

    const v4, -0xc27623f

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto/16 :goto_0
.end method
