.class public final LX/Hr3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Hr2;


# instance fields
.field public final synthetic a:Lcom/facebook/composer/activity/ComposerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/activity/ComposerFragment;)V
    .locals 0

    .prologue
    .line 2510848
    iput-object p1, p0, LX/Hr3;->a:Lcom/facebook/composer/activity/ComposerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick()V
    .locals 14

    .prologue
    .line 2510849
    iget-object v0, p0, LX/Hr3;->a:Lcom/facebook/composer/activity/ComposerFragment;

    .line 2510850
    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->o:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0gh;

    const-string v2, "tap_composer_footer_buttons"

    invoke-virtual {v1, v2}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 2510851
    iget-object v2, v0, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    sget-object v3, LX/0ge;->COMPOSER_FACECAST_CLICK:LX/0ge;

    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 2510852
    const/4 v1, 0x1

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v6, 0x0

    .line 2510853
    iget-object v4, v0, Lcom/facebook/composer/activity/ComposerFragment;->b:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1b2;

    .line 2510854
    iget-object v5, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v5}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v5}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->a(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)Lcom/facebook/ipc/composer/model/MinutiaeTag;

    move-result-object v9

    .line 2510855
    iget-object v5, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v5}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v5}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v5

    iget-object v5, v5, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object v10, LX/2rw;->PAGE:LX/2rw;

    if-ne v5, v10, :cond_2

    move v5, v7

    .line 2510856
    :goto_0
    if-eqz v5, :cond_3

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/1b2;->b(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v4

    move-object v5, v4

    .line 2510857
    :goto_1
    new-instance v10, LX/AWP;

    invoke-direct {v10}, LX/AWP;-><init>()V

    iget-object v4, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v4}, LX/0j2;->getTextWithEntities()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    .line 2510858
    iput-object v4, v10, LX/AWP;->a:Ljava/lang/String;

    .line 2510859
    move-object v10, v10

    .line 2510860
    iget-object v4, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v4}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v4

    iget-object v4, v4, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 2510861
    invoke-static {v10}, LX/AWP;->b(LX/AWP;)LX/AWS;

    move-result-object v11

    .line 2510862
    iput-object v4, v11, LX/AWS;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 2510863
    move-object v11, v11

    .line 2510864
    invoke-virtual {v11}, LX/AWS;->a()Lcom/facebook/facecast/model/FacecastPrivacyData;

    move-result-object v11

    iput-object v11, v10, LX/AWP;->b:Lcom/facebook/facecast/model/FacecastPrivacyData;

    .line 2510865
    move-object v10, v10

    .line 2510866
    iget-object v4, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v4}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v4

    .line 2510867
    iput-object v4, v10, LX/AWP;->c:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 2510868
    move-object v10, v10

    .line 2510869
    iget-object v4, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v4}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v4

    if-eqz v4, :cond_4

    iget-object v4, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v4}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->j()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    .line 2510870
    :goto_2
    iput-object v4, v10, LX/AWP;->d:Ljava/lang/String;

    .line 2510871
    move-object v10, v10

    .line 2510872
    iget-object v4, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v4}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v4

    if-eqz v4, :cond_5

    iget-object v4, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v4}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k()Ljava/lang/String;

    move-result-object v4

    .line 2510873
    :goto_3
    iput-object v4, v10, LX/AWP;->e:Ljava/lang/String;

    .line 2510874
    move-object v4, v10

    .line 2510875
    iget-object v10, v9, Lcom/facebook/ipc/composer/model/MinutiaeTag;->ogActionTypeId:Ljava/lang/String;

    .line 2510876
    iput-object v10, v4, LX/AWP;->f:Ljava/lang/String;

    .line 2510877
    move-object v4, v4

    .line 2510878
    iget-object v10, v9, Lcom/facebook/ipc/composer/model/MinutiaeTag;->ogObjectId:Ljava/lang/String;

    .line 2510879
    iput-object v10, v4, LX/AWP;->g:Ljava/lang/String;

    .line 2510880
    move-object v4, v4

    .line 2510881
    iget-object v10, v9, Lcom/facebook/ipc/composer/model/MinutiaeTag;->ogIconId:Ljava/lang/String;

    .line 2510882
    iput-object v10, v4, LX/AWP;->h:Ljava/lang/String;

    .line 2510883
    move-object v4, v4

    .line 2510884
    iget-object v9, v9, Lcom/facebook/ipc/composer/model/MinutiaeTag;->ogPhrase:Ljava/lang/String;

    .line 2510885
    iput-object v9, v4, LX/AWP;->i:Ljava/lang/String;

    .line 2510886
    move-object v9, v4

    .line 2510887
    iget-object v4, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v4}, LX/0jD;->getTaggedUsers()LX/0Px;

    move-result-object v4

    invoke-static {v4}, LX/7ky;->a(LX/0Px;)LX/0Px;

    move-result-object v4

    .line 2510888
    iput-object v4, v9, LX/AWP;->j:LX/0Px;

    .line 2510889
    move-object v9, v9

    .line 2510890
    iget-object v4, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v4}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getLaunchLoggingParams()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->getSourceSurface()LX/21D;

    move-result-object v4

    .line 2510891
    iput-object v4, v9, LX/AWP;->k:LX/21D;

    .line 2510892
    move-object v9, v9

    .line 2510893
    iget-object v4, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v4}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v4

    .line 2510894
    iput-object v4, v9, LX/AWP;->l:Ljava/lang/String;

    .line 2510895
    move-object v9, v9

    .line 2510896
    iget-object v4, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v4}, LX/0iw;->isFeedOnlyPost()Z

    move-result v4

    .line 2510897
    iput-boolean v4, v9, LX/AWP;->p:Z

    .line 2510898
    move-object v9, v9

    .line 2510899
    iget-object v4, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v4}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getPluginConfig()Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v4

    if-eqz v4, :cond_6

    iget-object v4, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v4}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getPluginConfig()Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;->a()Ljava/lang/String;

    move-result-object v4

    .line 2510900
    :goto_4
    invoke-static {v9}, LX/AWP;->b(LX/AWP;)LX/AWS;

    move-result-object v10

    .line 2510901
    iput-object v4, v10, LX/AWS;->a:Ljava/lang/String;

    .line 2510902
    move-object v10, v10

    .line 2510903
    invoke-virtual {v10}, LX/AWS;->a()Lcom/facebook/facecast/model/FacecastPrivacyData;

    move-result-object v10

    iput-object v10, v9, LX/AWP;->b:Lcom/facebook/facecast/model/FacecastPrivacyData;

    .line 2510904
    move-object v9, v9

    .line 2510905
    iget-object v4, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v4}, LX/0jG;->r()Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v4}, LX/0jG;->r()Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    move-result-object v4

    .line 2510906
    iget-object v6, v4, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;->a:Ljava/lang/String;

    move-object v6, v6

    .line 2510907
    :cond_0
    iput-object v6, v9, LX/AWP;->q:Ljava/lang/String;

    .line 2510908
    move-object v4, v9

    .line 2510909
    invoke-virtual {v4}, LX/AWP;->a()Lcom/facebook/facecast/model/FacecastCompositionData;

    move-result-object v6

    .line 2510910
    iget-object v4, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v4}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v4

    invoke-static {v4}, LX/2rm;->a(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Z

    move-result v4

    invoke-static {v6, v1, v4}, Lcom/facebook/facecast/FacecastActivity;->a(Lcom/facebook/facecast/model/FacecastCompositionData;ZZ)Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v5, v4}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2510911
    iget-object v4, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v4}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 2510912
    const-string v6, "com.facebook.orca.auth.OVERRIDDEN_VIEWER_CONTEXT"

    iget-object v4, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v4}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->getPostAsPageViewerContext()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v4

    invoke-virtual {v5, v6, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2510913
    :cond_1
    new-instance v4, LX/HrM;

    invoke-direct {v4, v0, v5}, LX/HrM;-><init>(Lcom/facebook/composer/activity/ComposerFragment;Landroid/content/Intent;)V

    .line 2510914
    iget-object v5, v0, Lcom/facebook/composer/activity/ComposerFragment;->cj:LX/121;

    sget-object v6, LX/0yY;->VIDEO_PLAY_INTERSTITIAL:LX/0yY;

    const v9, 0x7f080e4a

    invoke-virtual {v0, v9}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v6, v9, v4}, LX/121;->a(LX/0yY;Ljava/lang/String;LX/39A;)LX/121;

    .line 2510915
    iget-object v5, v0, Lcom/facebook/composer/activity/ComposerFragment;->cj:LX/121;

    sget-object v6, LX/0yY;->VIDEO_UPLOAD_INTERSTITIAL:LX/0yY;

    const v9, 0x7f080e49

    invoke-virtual {v0, v9}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v6, v9, v4}, LX/121;->a(LX/0yY;Ljava/lang/String;LX/39A;)LX/121;

    .line 2510916
    iget-object v5, v0, Lcom/facebook/composer/activity/ComposerFragment;->cj:LX/121;

    const/4 v4, 0x2

    new-array v6, v4, [LX/0yY;

    sget-object v4, LX/0yY;->VIDEO_PLAY_INTERSTITIAL:LX/0yY;

    aput-object v4, v6, v8

    sget-object v4, LX/0yY;->VIDEO_UPLOAD_INTERSTITIAL:LX/0yY;

    aput-object v4, v6, v7

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v4

    check-cast v4, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v4

    .line 2510917
    const/4 v7, 0x0

    const/4 v10, 0x0

    .line 2510918
    move v9, v10

    :goto_5
    array-length v8, v6

    if-ge v9, v8, :cond_9

    .line 2510919
    aget-object v11, v6, v9

    .line 2510920
    iget-object v8, v5, LX/121;->a:Ljava/util/Map;

    invoke-interface {v8, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    if-eqz v8, :cond_7

    .line 2510921
    iget-object v8, v5, LX/121;->a:Ljava/util/Map;

    invoke-interface {v8, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/3J6;

    .line 2510922
    iget-object v8, v8, LX/3J6;->j:LX/2nT;

    invoke-virtual {v5, v8, v11}, LX/121;->a(LX/2nT;LX/0yY;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 2510923
    invoke-virtual {v5, v11, v4, v7}, LX/121;->a(LX/0yY;LX/0gc;Ljava/lang/Object;)Landroid/support/v4/app/DialogFragment;

    .line 2510924
    :goto_6
    iget-object v0, p0, LX/Hr3;->a:Lcom/facebook/composer/activity/ComposerFragment;

    invoke-static {v0}, Lcom/facebook/composer/activity/ComposerFragment;->R$redex0(Lcom/facebook/composer/activity/ComposerFragment;)V

    .line 2510925
    return-void

    :cond_2
    move v5, v8

    .line 2510926
    goto/16 :goto_0

    .line 2510927
    :cond_3
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/1b2;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v4

    move-object v5, v4

    goto/16 :goto_1

    :cond_4
    move-object v4, v6

    .line 2510928
    goto/16 :goto_2

    :cond_5
    move-object v4, v6

    goto/16 :goto_3

    :cond_6
    move-object v4, v6

    goto/16 :goto_4

    .line 2510929
    :cond_7
    add-int/lit8 v8, v9, 0x1

    move v9, v8

    goto :goto_5

    .line 2510930
    :cond_8
    add-int/lit8 v10, v10, 0x1

    :cond_9
    array-length v8, v6

    if-ge v10, v8, :cond_a

    .line 2510931
    aget-object v8, v6, v10

    .line 2510932
    iget-object v9, v5, LX/121;->a:Ljava/util/Map;

    invoke-interface {v9, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    if-eqz v9, :cond_8

    .line 2510933
    invoke-virtual {v5, v8, v4, v7}, LX/121;->a(LX/0yY;LX/0gc;Ljava/lang/Object;)Landroid/support/v4/app/DialogFragment;

    goto :goto_6

    .line 2510934
    :cond_a
    goto :goto_6
.end method
