.class public final LX/JGz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1cj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1cj",
        "<",
        "LX/1FJ",
        "<",
        "LX/1ln;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final a:LX/1bf;

.field public b:LX/JGR;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:LX/1ca;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:LX/1FJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:I


# direct methods
.method public constructor <init>(LX/1bf;)V
    .locals 0

    .prologue
    .line 2669778
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2669779
    iput-object p1, p0, LX/JGz;->a:LX/1bf;

    .line 2669780
    return-void
.end method


# virtual methods
.method public final a(LX/1ca;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 2669757
    invoke-interface {p1}, LX/1ca;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2669758
    :goto_0
    return-void

    .line 2669759
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/JGz;->c:LX/1ca;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v0, p1, :cond_1

    .line 2669760
    invoke-interface {p1}, LX/1ca;->g()Z

    goto :goto_0

    .line 2669761
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    iput-object v0, p0, LX/JGz;->c:LX/1ca;

    .line 2669762
    invoke-interface {p1}, LX/1ca;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2669763
    if-nez v0, :cond_2

    .line 2669764
    invoke-interface {p1}, LX/1ca;->g()Z

    goto :goto_0

    .line 2669765
    :cond_2
    :try_start_2
    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ln;

    .line 2669766
    instance-of v1, v1, LX/1lm;

    if-nez v1, :cond_3

    .line 2669767
    invoke-virtual {v0}, LX/1FJ;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2669768
    invoke-interface {p1}, LX/1ca;->g()Z

    goto :goto_0

    .line 2669769
    :cond_3
    :try_start_3
    iput-object v0, p0, LX/JGz;->d:LX/1FJ;

    .line 2669770
    invoke-virtual {p0}, LX/JGz;->b()Landroid/graphics/Bitmap;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    .line 2669771
    if-nez v0, :cond_4

    .line 2669772
    invoke-interface {p1}, LX/1ca;->g()Z

    goto :goto_0

    .line 2669773
    :cond_4
    :try_start_4
    iget-object v0, p0, LX/JGz;->b:LX/JGR;

    .line 2669774
    move-object v0, v0

    .line 2669775
    check-cast v0, LX/JGR;

    .line 2669776
    invoke-interface {v0}, LX/JGR;->c()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2669777
    invoke-interface {p1}, LX/1ca;->g()Z

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {p1}, LX/1ca;->g()Z

    throw v0
.end method

.method public final b()Landroid/graphics/Bitmap;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2669749
    iget-object v0, p0, LX/JGz;->d:LX/1FJ;

    if-nez v0, :cond_0

    move-object v0, v1

    .line 2669750
    :goto_0
    return-object v0

    .line 2669751
    :cond_0
    iget-object v0, p0, LX/JGz;->d:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ln;

    .line 2669752
    instance-of v2, v0, LX/1lm;

    if-nez v2, :cond_1

    .line 2669753
    iget-object v0, p0, LX/JGz;->d:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->close()V

    .line 2669754
    iput-object v1, p0, LX/JGz;->d:LX/1FJ;

    move-object v0, v1

    .line 2669755
    goto :goto_0

    .line 2669756
    :cond_1
    check-cast v0, LX/1lm;

    invoke-virtual {v0}, LX/1lm;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(LX/1ca;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 2669745
    iget-object v0, p0, LX/JGz;->c:LX/1ca;

    if-ne v0, p1, :cond_0

    .line 2669746
    const/4 v0, 0x0

    iput-object v0, p0, LX/JGz;->c:LX/1ca;

    .line 2669747
    :cond_0
    invoke-interface {p1}, LX/1ca;->g()Z

    .line 2669748
    return-void
.end method

.method public final c(LX/1ca;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 2669741
    iget-object v0, p0, LX/JGz;->c:LX/1ca;

    if-ne v0, p1, :cond_0

    .line 2669742
    const/4 v0, 0x0

    iput-object v0, p0, LX/JGz;->c:LX/1ca;

    .line 2669743
    :cond_0
    invoke-interface {p1}, LX/1ca;->g()Z

    .line 2669744
    return-void
.end method

.method public final d(LX/1ca;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 2669740
    return-void
.end method
