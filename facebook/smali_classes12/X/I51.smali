.class public final LX/I51;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/107;


# instance fields
.field public final synthetic a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;)V
    .locals 0

    .prologue
    .line 2534705
    iput-object p1, p0, LX/I51;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 9

    .prologue
    .line 2534706
    iget-object v0, p0, LX/I51;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;

    .line 2534707
    iget-object v1, v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->h:LX/0Uh;

    const/16 v2, 0x3a2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2534708
    iget-object v1, v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->G:Ljava/util/List;

    iget-object v2, v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->I:Ljava/util/HashSet;

    iget-object v3, v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->J:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;

    iget-object v4, v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->K:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    iget v5, v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->A:I

    iget-object v6, v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->p:LX/I4y;

    .line 2534709
    new-instance v7, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;

    invoke-direct {v7}, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;-><init>()V

    .line 2534710
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 2534711
    const-string p0, "extra_events_discovery_fragment_category_filters"

    invoke-static {v8, p0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/util/List;)V

    .line 2534712
    const-string p0, "extra_events_discovery_fragment_selected_category"

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v8, p0, p1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2534713
    const-string p0, "extra_events_discovery_fragment_location_filters"

    invoke-virtual {v8, p0, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2534714
    const-string p0, "extra_events_discovery_fragment_selected_location"

    invoke-static {v8, p0, v4}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2534715
    const-string p0, "extra_events_discovery_fragment_range_index"

    invoke-virtual {v8, p0, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2534716
    invoke-virtual {v7, v8}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2534717
    iput-object v6, v7, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFilterFragment;->x:LX/I4y;

    .line 2534718
    move-object v1, v7

    .line 2534719
    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v2

    const-string v3, "filter_fragment_tag"

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2534720
    :goto_0
    return-void

    .line 2534721
    :cond_0
    iget-object v1, v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->e:LX/1nQ;

    const-string v2, "event_discovery"

    const-string v3, "discover_list"

    const-string v4, "surface"

    iget-object v5, v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->z:Ljava/lang/String;

    .line 2534722
    iget-object v6, v1, LX/1nQ;->i:LX/0Zb;

    const-string v7, "tapped_location_filter"

    const/4 v8, 0x1

    invoke-interface {v6, v7, v8}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v6

    .line 2534723
    invoke-virtual {v6}, LX/0oG;->a()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 2534724
    invoke-virtual {v6, v2}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v6

    const-string v7, "action_source"

    invoke-virtual {v6, v7, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v6

    const-string v7, "mechanism"

    invoke-virtual {v6, v7, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v6

    const-string v7, "event_suggestion_token"

    invoke-virtual {v6, v7, v5}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v6

    invoke-virtual {v6}, LX/0oG;->d()V

    .line 2534725
    :cond_1
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    iget-object v1, v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->c:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/ComponentName;

    invoke-virtual {v2, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v2

    .line 2534726
    const-string v1, "target_fragment"

    sget-object v3, LX/0cQ;->EVENTS_DISCOVERY_LOCATION_PICKER_FRAGMENT:LX/0cQ;

    invoke-virtual {v3}, LX/0cQ;->ordinal()I

    move-result v3

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2534727
    iget-object v1, v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->K:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    if-nez v1, :cond_4

    const/4 v1, 0x0

    .line 2534728
    :goto_1
    const-string v3, "extra_is_current_location_selected"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2534729
    const-string v1, "extra_location_range"

    iget v3, v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->A:I

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2534730
    const-string v1, "extra_events_discovery_suggestion_token"

    iget-object v3, v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->z:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2534731
    iget-object v3, v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->g:Lcom/facebook/content/SecureContextHelper;

    const/16 v4, 0x65

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v5, Landroid/app/Activity;

    invoke-static {v1, v5}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    invoke-interface {v3, v2, v4, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2534732
    iget-object v1, v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->q:LX/I5K;

    const/4 v2, 0x1

    .line 2534733
    const/4 v3, 0x0

    move v4, v3

    :goto_2
    invoke-virtual {v1}, LX/0gG;->b()I

    move-result v3

    if-ge v4, v3, :cond_3

    .line 2534734
    invoke-virtual {v1, v4}, LX/0gF;->e(I)Landroid/support/v4/app/Fragment;

    move-result-object v3

    check-cast v3, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;

    .line 2534735
    if-eqz v3, :cond_2

    .line 2534736
    iput-boolean v2, v3, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->v:Z

    .line 2534737
    :cond_2
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_2

    .line 2534738
    :cond_3
    goto/16 :goto_0

    .line 2534739
    :cond_4
    iget-object v1, v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->K:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2534740
    iget-boolean v3, v1, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->b:Z

    move v1, v3

    .line 2534741
    goto :goto_1
.end method
