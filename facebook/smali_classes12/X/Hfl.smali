.class public final LX/Hfl;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/Hfn;",
        ">;"
    }
.end annotation


# static fields
.field private static b:[Ljava/lang/String;

.field private static c:I


# instance fields
.field public a:LX/Hfm;

.field public d:Ljava/util/BitSet;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 2492287
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "height"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "pageWidth"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "section"

    aput-object v2, v0, v1

    sput-object v0, LX/Hfl;->b:[Ljava/lang/String;

    .line 2492288
    sput v3, LX/Hfl;->c:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2492289
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 2492290
    new-instance v0, Ljava/util/BitSet;

    sget v1, LX/Hfl;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/Hfl;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/Hfl;LX/1De;IILX/Hfm;)V
    .locals 1

    .prologue
    .line 2492291
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 2492292
    iput-object p4, p0, LX/Hfl;->a:LX/Hfm;

    .line 2492293
    iget-object v0, p0, LX/Hfl;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 2492294
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2492295
    invoke-super {p0}, LX/1X5;->a()V

    .line 2492296
    const/4 v0, 0x0

    iput-object v0, p0, LX/Hfl;->a:LX/Hfm;

    .line 2492297
    sget-object v0, LX/Hfn;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 2492298
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/Hfn;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2492299
    iget-object v1, p0, LX/Hfl;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/Hfl;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    sget v2, LX/Hfl;->c:I

    if-ge v1, v2, :cond_2

    .line 2492300
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2492301
    :goto_0
    sget v2, LX/Hfl;->c:I

    if-ge v0, v2, :cond_1

    .line 2492302
    iget-object v2, p0, LX/Hfl;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2492303
    sget-object v2, LX/Hfl;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2492304
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2492305
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2492306
    :cond_2
    iget-object v0, p0, LX/Hfl;->a:LX/Hfm;

    .line 2492307
    invoke-virtual {p0}, LX/Hfl;->a()V

    .line 2492308
    return-object v0
.end method
