.class public final enum LX/J2x;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/J2x;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/J2x;

.field public static final enum PAGES_COMMERCE:LX/J2x;

.field public static final enum SIMPLE:LX/J2x;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2641845
    new-instance v0, LX/J2x;

    const-string v1, "PAGES_COMMERCE"

    invoke-direct {v0, v1, v2}, LX/J2x;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J2x;->PAGES_COMMERCE:LX/J2x;

    .line 2641846
    new-instance v0, LX/J2x;

    const-string v1, "SIMPLE"

    invoke-direct {v0, v1, v3}, LX/J2x;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J2x;->SIMPLE:LX/J2x;

    .line 2641847
    const/4 v0, 0x2

    new-array v0, v0, [LX/J2x;

    sget-object v1, LX/J2x;->PAGES_COMMERCE:LX/J2x;

    aput-object v1, v0, v2

    sget-object v1, LX/J2x;->SIMPLE:LX/J2x;

    aput-object v1, v0, v3

    sput-object v0, LX/J2x;->$VALUES:[LX/J2x;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2641848
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/J2x;
    .locals 1

    .prologue
    .line 2641849
    const-class v0, LX/J2x;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/J2x;

    return-object v0
.end method

.method public static values()[LX/J2x;
    .locals 1

    .prologue
    .line 2641850
    sget-object v0, LX/J2x;->$VALUES:[LX/J2x;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/J2x;

    return-object v0
.end method
