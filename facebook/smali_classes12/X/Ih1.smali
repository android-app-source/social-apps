.class public final LX/Ih1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/ui/media/attachments/MediaResource;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/List;

.field public final synthetic b:Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 2601894
    iput-object p1, p0, LX/Ih1;->b:Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;

    iput-object p2, p0, LX/Ih1;->a:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2601895
    iget-object v0, p0, LX/Ih1;->b:Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;

    iget-object v1, p0, LX/Ih1;->a:Ljava/util/List;

    .line 2601896
    iget-object v2, v0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->w:LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->b()V

    .line 2601897
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2601898
    new-instance v4, Landroid/content/Intent;

    const-string v5, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2601899
    iget-object v2, v2, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-virtual {v4, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2601900
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 2601901
    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2601902
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2601903
    invoke-static {v2}, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->a(Lcom/facebook/ui/media/attachments/MediaResource;)Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, v2, Lcom/facebook/ui/media/attachments/MediaResource;->e:LX/5zj;

    sget-object p0, LX/5zj;->UNSPECIFIED:LX/5zj;

    if-ne v5, p0, :cond_2

    .line 2601904
    :cond_1
    invoke-static {}, Lcom/facebook/ui/media/attachments/MediaResource;->a()LX/5zn;

    move-result-object v5

    invoke-virtual {v5, v2}, LX/5zn;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5zn;

    move-result-object v2

    .line 2601905
    iget-object v5, v0, Lcom/facebook/messaging/media/mediapicker/dialog/PickMediaDialogFragment;->o:Lcom/facebook/ui/media/attachments/MediaResourceHelper;

    invoke-virtual {v5, v2}, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->a(LX/5zn;)V

    .line 2601906
    sget-object v5, LX/5zj;->MEDIA_PICKER_GALLERY:LX/5zj;

    .line 2601907
    iput-object v5, v2, LX/5zn;->d:LX/5zj;

    .line 2601908
    invoke-virtual {v2}, LX/5zn;->G()Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v2

    .line 2601909
    :cond_2
    invoke-virtual {v3, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 2601910
    :cond_3
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    move-object v0, v2

    .line 2601911
    return-object v0
.end method
