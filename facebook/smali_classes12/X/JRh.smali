.class public LX/JRh;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JRf;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/multishare/MultiShareProductItemComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2693621
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/JRh;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/multishare/MultiShareProductItemComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2693684
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2693685
    iput-object p1, p0, LX/JRh;->b:LX/0Ot;

    .line 2693686
    return-void
.end method

.method public static a(LX/0QB;)LX/JRh;
    .locals 4

    .prologue
    .line 2693673
    const-class v1, LX/JRh;

    monitor-enter v1

    .line 2693674
    :try_start_0
    sget-object v0, LX/JRh;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2693675
    sput-object v2, LX/JRh;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2693676
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2693677
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2693678
    new-instance v3, LX/JRh;

    const/16 p0, 0x2030

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JRh;-><init>(LX/0Ot;)V

    .line 2693679
    move-object v0, v3

    .line 2693680
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2693681
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JRh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2693682
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2693683
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2693687
    const v0, 0x5352cbc3

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 11

    .prologue
    .line 2693632
    check-cast p2, LX/JRg;

    .line 2693633
    iget-object v0, p0, LX/JRh;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/multishare/MultiShareProductItemComponentSpec;

    iget-object v1, p2, LX/JRg;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/JRg;->b:LX/1Pm;

    const/4 v9, 0x2

    const/4 v8, 0x1

    .line 2693634
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2693635
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2693636
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    .line 2693637
    :goto_0
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2693638
    invoke-static {v3}, LX/1VO;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    invoke-static {v3}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v3

    .line 2693639
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    iget-object v7, v0, Lcom/facebook/feedplugins/multishare/MultiShareProductItemComponentSpec;->a:LX/ApZ;

    const/4 v10, 0x0

    .line 2693640
    new-instance p0, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;

    invoke-direct {p0, v7}, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;-><init>(LX/ApZ;)V

    .line 2693641
    iget-object p2, v7, LX/ApZ;->b:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/ApY;

    .line 2693642
    if-nez p2, :cond_0

    .line 2693643
    new-instance p2, LX/ApY;

    invoke-direct {p2, v7}, LX/ApY;-><init>(LX/ApZ;)V

    .line 2693644
    :cond_0
    invoke-static {p2, p1, v10, v10, p0}, LX/ApY;->a$redex0(LX/ApY;LX/1De;IILcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;)V

    .line 2693645
    move-object p0, p2

    .line 2693646
    move-object v10, p0

    .line 2693647
    move-object v7, v10

    .line 2693648
    iget-object v10, v7, LX/ApY;->a:Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;

    iput v9, v10, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->a:I

    .line 2693649
    iget-object v10, v7, LX/ApY;->e:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v10, p0}, Ljava/util/BitSet;->set(I)V

    .line 2693650
    move-object v7, v7

    .line 2693651
    iget-object v10, v7, LX/ApY;->a:Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;

    iput-object v2, v10, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->e:LX/1Po;

    .line 2693652
    move-object v7, v7

    .line 2693653
    iget-object v10, v7, LX/ApY;->a:Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;

    iput-object v5, v10, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->b:Ljava/lang/CharSequence;

    .line 2693654
    iget-object v10, v7, LX/ApY;->e:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v10, p0}, Ljava/util/BitSet;->set(I)V

    .line 2693655
    move-object v5, v7

    .line 2693656
    iget-object v7, v5, LX/ApY;->a:Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;

    iput-object v4, v7, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->h:Ljava/lang/CharSequence;

    .line 2693657
    move-object v4, v5

    .line 2693658
    iget-object v5, v4, LX/ApY;->a:Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;

    iput-object v3, v5, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->c:Landroid/net/Uri;

    .line 2693659
    move-object v3, v4

    .line 2693660
    sget-object v4, Lcom/facebook/feedplugins/multishare/MultiShareProductItemComponentSpec;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 2693661
    iget-object v5, v3, LX/ApY;->a:Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;

    iput-object v4, v5, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 2693662
    move-object v3, v3

    .line 2693663
    iget-object v4, v0, Lcom/facebook/feedplugins/multishare/MultiShareProductItemComponentSpec;->b:LX/2sO;

    invoke-virtual {v4, v1}, LX/2sO;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/2y5;

    move-result-object v4

    invoke-virtual {v4, p1, v2, v1, v8}, LX/2y5;->c(LX/1De;LX/1PW;Lcom/facebook/feed/rows/core/props/FeedProps;Z)LX/1X1;

    move-result-object v4

    .line 2693664
    iget-object v5, v3, LX/ApY;->a:Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;

    iput-object v4, v5, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->k:LX/1X1;

    .line 2693665
    move-object v3, v3

    .line 2693666
    iget-object v4, v3, LX/ApY;->a:Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;

    iput-boolean v8, v4, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->l:Z

    .line 2693667
    move-object v3, v3

    .line 2693668
    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    .line 2693669
    const v4, 0x5352cbc3

    const/4 v5, 0x0

    invoke-static {p1, v4, v5}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v4, v4

    .line 2693670
    invoke-interface {v3, v4}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v3

    const/4 v4, 0x6

    const/4 v5, 0x5

    invoke-interface {v3, v4, v5}, LX/1Di;->d(II)LX/1Di;

    move-result-object v3

    const/16 v4, 0x8

    invoke-interface {v3, v4, v9}, LX/1Di;->h(II)LX/1Di;

    move-result-object v3

    const v4, 0x7f020a3c

    invoke-interface {v3, v4}, LX/1Di;->x(I)LX/1Di;

    move-result-object v3

    invoke-interface {v6, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2693671
    return-object v0

    .line 2693672
    :cond_1
    const/4 v4, 0x0

    goto/16 :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2693622
    invoke-static {}, LX/1dS;->b()V

    .line 2693623
    iget v0, p1, LX/1dQ;->b:I

    .line 2693624
    packed-switch v0, :pswitch_data_0

    .line 2693625
    :goto_0
    return-object v2

    .line 2693626
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2693627
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2693628
    check-cast v1, LX/JRg;

    .line 2693629
    iget-object v3, p0, LX/JRh;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/multishare/MultiShareProductItemComponentSpec;

    iget-object p1, v1, LX/JRg;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget p2, v1, LX/JRg;->c:I

    .line 2693630
    iget-object p0, v3, Lcom/facebook/feedplugins/multishare/MultiShareProductItemComponentSpec;->c:LX/3hk;

    invoke-virtual {p0, v0, p1, p2}, LX/3hk;->onClick(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;I)V

    .line 2693631
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x5352cbc3
        :pswitch_0
    .end packed-switch
.end method
