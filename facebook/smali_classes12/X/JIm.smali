.class public final LX/JIm;
.super LX/1a1;
.source ""

# interfaces
.implements LX/JIl;
.implements LX/6WM;


# instance fields
.field private final l:Lcom/facebook/fig/mediagrid/FigMediaGrid;

.field private final m:LX/JIw;

.field private final n:LX/Fxj;

.field public o:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLInterfaces$FavoritePhoto;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/fig/mediagrid/FigMediaGrid;LX/JIw;LX/Fxj;)V
    .locals 0

    .prologue
    .line 2678335
    invoke-direct {p0, p1}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 2678336
    iput-object p1, p0, LX/JIm;->l:Lcom/facebook/fig/mediagrid/FigMediaGrid;

    .line 2678337
    iput-object p2, p0, LX/JIm;->m:LX/JIw;

    .line 2678338
    iput-object p3, p0, LX/JIm;->n:LX/Fxj;

    .line 2678339
    invoke-virtual {p1, p0}, Lcom/facebook/fig/mediagrid/FigMediaGrid;->a(LX/6WM;)Lcom/facebook/fig/mediagrid/FigMediaGrid;

    .line 2678340
    return-void
.end method


# virtual methods
.method public final a(ILX/1aZ;Landroid/graphics/Rect;)V
    .locals 8

    .prologue
    .line 2678343
    iget-object v0, p0, LX/JIm;->m:LX/JIw;

    iget-object v1, p0, LX/JIm;->l:Lcom/facebook/fig/mediagrid/FigMediaGrid;

    iget-object v3, p0, LX/JIm;->o:LX/0Px;

    iget-object v4, p0, LX/JIm;->n:LX/Fxj;

    sget-object v7, LX/Emo;->FEATURED_PHOTO:LX/Emo;

    move v2, p1

    move-object v5, p2

    move-object v6, p3

    invoke-virtual/range {v0 .. v7}, LX/JIw;->a(Lcom/facebook/fig/mediagrid/FigMediaGrid;ILX/0Px;LX/Fx7;LX/1aZ;Landroid/graphics/Rect;LX/Emo;)V

    .line 2678344
    return-void
.end method

.method public final a(LX/0Px;LX/6WP;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/1aZ;",
            ">;",
            "LX/6WP;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2678341
    iget-object v0, p0, LX/JIm;->l:Lcom/facebook/fig/mediagrid/FigMediaGrid;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/fig/mediagrid/FigMediaGrid;->a(LX/0Px;LX/6WP;)Lcom/facebook/fig/mediagrid/FigMediaGrid;

    .line 2678342
    return-void
.end method
