.class public LX/HVR;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/Bgf;

.field private final b:LX/9XE;

.field private c:LX/03V;

.field private d:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(LX/03V;LX/Bgf;LX/9XE;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2475050
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2475051
    iput-object p1, p0, LX/HVR;->c:LX/03V;

    .line 2475052
    iput-object p2, p0, LX/HVR;->a:LX/Bgf;

    .line 2475053
    iput-object p3, p0, LX/HVR;->b:LX/9XE;

    .line 2475054
    iput-object p4, p0, LX/HVR;->d:Lcom/facebook/content/SecureContextHelper;

    .line 2475055
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;)V
    .locals 8

    .prologue
    .line 2475056
    iget-object v0, p0, LX/HVR;->b:LX/9XE;

    sget-object v1, LX/9XI;->EVENT_TAPPED_SUGGEST_EDIT:LX/9XI;

    iget-wide v2, p3, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->a:J

    invoke-virtual {v0, v1, v2, v3}, LX/9XE;->a(LX/9X2;J)V

    .line 2475057
    iget-object v1, p0, LX/HVR;->a:LX/Bgf;

    iget-wide v2, p3, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->a:J

    iget-object v4, p3, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->e:Ljava/lang/String;

    iget-object v5, p3, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->f:Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "android_add_info_button"

    invoke-virtual/range {v1 .. v7}, LX/Bgf;->a(JLjava/lang/String;Ljava/lang/String;LX/CdT;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2475058
    if-nez v1, :cond_0

    .line 2475059
    iget-object v0, p0, LX/HVR;->c:LX/03V;

    const-string v1, "page_context_rows_suggest_edit_fail"

    const-string v2, "Failed to resolve suggest edits intent!"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2475060
    :goto_0
    return-void

    .line 2475061
    :cond_0
    iget-object v2, p0, LX/HVR;->d:Lcom/facebook/content/SecureContextHelper;

    const/16 v3, 0x2776

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-interface {v2, v1, v3, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    goto :goto_0
.end method
