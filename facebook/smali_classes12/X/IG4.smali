.class public LX/IG4;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "LX/IFd;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/IG0;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2555198
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2555199
    invoke-static {}, LX/0PM;->d()Ljava/util/LinkedHashMap;

    move-result-object v0

    iput-object v0, p0, LX/IG4;->a:Ljava/util/LinkedHashMap;

    .line 2555200
    new-instance v0, LX/IG0;

    invoke-direct {v0}, LX/IG0;-><init>()V

    iput-object v0, p0, LX/IG4;->b:LX/IG0;

    return-void
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/IFd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2555201
    iget-object v0, p0, LX/IG4;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/IFd;)V
    .locals 4

    .prologue
    .line 2555202
    iget-object v0, p0, LX/IG4;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2555203
    invoke-virtual {p2}, LX/622;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IFR;

    .line 2555204
    iget-object v2, p0, LX/IG4;->b:LX/IG0;

    invoke-interface {v0}, LX/IFR;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, LX/IG0;->a(Ljava/lang/String;LX/IFR;)V

    goto :goto_0

    .line 2555205
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/String;)LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0Rf",
            "<",
            "LX/IFR;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2555206
    iget-object v0, p0, LX/IG4;->b:LX/IG0;

    invoke-virtual {v0, p1}, LX/IG0;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 2555207
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    goto :goto_0
.end method
