.class public LX/J5y;
.super LX/2s5;
.source ""


# instance fields
.field public a:Landroid/content/res/Resources;

.field public b:[LX/J4K;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/0gc;[LX/J4K;)V
    .locals 0

    .prologue
    .line 2649170
    invoke-direct {p0, p2}, LX/2s5;-><init>(LX/0gc;)V

    .line 2649171
    iput-object p1, p0, LX/J5y;->a:Landroid/content/res/Resources;

    .line 2649172
    iput-object p3, p0, LX/J5y;->b:[LX/J4K;

    .line 2649173
    return-void
.end method


# virtual methods
.method public final G_(I)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 2649174
    invoke-virtual {p0, p1}, LX/J5y;->f(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 2649175
    iget-object v0, p0, LX/J5y;->b:[LX/J4K;

    aget-object v0, v0, p1

    .line 2649176
    new-instance p0, Landroid/content/Intent;

    invoke-direct {p0}, Landroid/content/Intent;-><init>()V

    .line 2649177
    const-string p1, "extra_privacy_checkup_step"

    invoke-virtual {p0, p1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2649178
    sget-object p1, LX/J4K;->COMPOSER_STEP:LX/J4K;

    if-ne v0, p1, :cond_0

    .line 2649179
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    .line 2649180
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 2649181
    new-instance v0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupComposerStepFragment;

    invoke-direct {v0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupComposerStepFragment;-><init>()V

    .line 2649182
    invoke-virtual {v0, p1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2649183
    move-object p0, v0

    .line 2649184
    :goto_0
    move-object v0, p0

    .line 2649185
    return-object v0

    .line 2649186
    :cond_0
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    .line 2649187
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 2649188
    new-instance v0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;

    invoke-direct {v0}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;-><init>()V

    .line 2649189
    invoke-virtual {v0, p1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2649190
    move-object p0, v0

    .line 2649191
    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2649192
    iget-object v0, p0, LX/J5y;->b:[LX/J4K;

    array-length v0, v0

    return v0
.end method

.method public final e(I)LX/J4K;
    .locals 1

    .prologue
    .line 2649193
    iget-object v0, p0, LX/J5y;->b:[LX/J4K;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final f(I)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, -0x1

    .line 2649194
    iget-object v0, p0, LX/J5y;->b:[LX/J4K;

    aget-object v0, v0, p1

    .line 2649195
    sget-object v2, LX/J5x;->a:[I

    invoke-virtual {v0}, LX/J4K;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 2649196
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Unable to find title for step: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, LX/J4K;->name()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 2649197
    :goto_0
    if-ne v0, v1, :cond_0

    .line 2649198
    const/4 v0, 0x0

    .line 2649199
    :goto_1
    return-object v0

    .line 2649200
    :pswitch_0
    const v0, 0x7f0838d7

    goto :goto_0

    .line 2649201
    :pswitch_1
    const v0, 0x7f0838d8

    goto :goto_0

    .line 2649202
    :pswitch_2
    const v0, 0x7f0838d9

    goto :goto_0

    .line 2649203
    :cond_0
    iget-object v1, p0, LX/J5y;->a:Landroid/content/res/Resources;

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
