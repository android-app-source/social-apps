.class public final LX/HXg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:Landroid/view/View;

.field public final synthetic c:LX/HXh;


# direct methods
.method public constructor <init>(LX/HXh;Lcom/facebook/graphql/model/GraphQLStory;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2479205
    iput-object p1, p0, LX/HXg;->c:LX/HXh;

    iput-object p2, p0, LX/HXg;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object p3, p0, LX/HXg;->b:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 7

    .prologue
    .line 2479206
    iget-object v0, p0, LX/HXg;->c:LX/HXh;

    iget-object v0, v0, LX/HXh;->c:LX/HXi;

    iget-object v1, p0, LX/HXg;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iget-object v2, p0, LX/HXg;->b:Landroid/view/View;

    .line 2479207
    invoke-static {v1}, LX/D2z;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v3

    .line 2479208
    if-nez v3, :cond_0

    .line 2479209
    sget-object v3, LX/HXi;->g:Ljava/lang/Class;

    const-string v4, "Error: ban user action see empty actor"

    invoke-static {v3, v4}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2479210
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 2479211
    :cond_0
    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 2479212
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f08157a

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 p0, 0x0

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v6, p0

    invoke-static {v5, v6}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 2479213
    new-instance v6, LX/0ju;

    invoke-direct {v6, v4}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v5}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v4

    const v5, 0x7f081579

    new-instance v6, LX/HXf;

    invoke-direct {v6, v0, v3, v1, v2}, LX/HXf;-><init>(LX/HXi;Lcom/facebook/graphql/model/GraphQLActor;Lcom/facebook/graphql/model/GraphQLStory;Landroid/view/View;)V

    invoke-virtual {v4, v5, v6}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v3

    const v4, 0x7f0815b3

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v3

    invoke-virtual {v3}, LX/0ju;->b()LX/2EJ;

    goto :goto_0
.end method
