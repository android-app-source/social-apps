.class public LX/I11;
.super Landroid/graphics/drawable/Drawable;
.source ""

# interfaces
.implements Landroid/graphics/drawable/Drawable$Callback;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public final a:LX/0hL;

.field public final b:Landroid/content/res/Resources;

.field public final c:I

.field public final d:I

.field public e:Landroid/graphics/drawable/Drawable;

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public g:I

.field private h:Landroid/graphics/Paint;

.field public i:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(LX/0hL;Landroid/content/res/Resources;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 2528467
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 2528468
    iput-object p1, p0, LX/I11;->a:LX/0hL;

    .line 2528469
    iput-object p2, p0, LX/I11;->b:Landroid/content/res/Resources;

    .line 2528470
    const v0, 0x7f0b15d5

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, LX/I11;->c:I

    .line 2528471
    const v0, 0x7f0b15d6

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, LX/I11;->d:I

    .line 2528472
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/I11;->h:Landroid/graphics/Paint;

    .line 2528473
    iget-object v0, p0, LX/I11;->h:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2528474
    iget-object v0, p0, LX/I11;->h:Landroid/graphics/Paint;

    iget-object v1, p0, LX/I11;->b:Landroid/content/res/Resources;

    const v2, 0x7f0a00d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2528475
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/I11;->i:Landroid/graphics/Paint;

    .line 2528476
    iget-object v0, p0, LX/I11;->i:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2528477
    return-void
.end method

.method public static a(LX/I11;Landroid/graphics/Canvas;F)V
    .locals 2

    .prologue
    .line 2528461
    const/high16 v0, 0x40400000    # 3.0f

    add-float/2addr v0, p2

    iget-object v1, p0, LX/I11;->h:Landroid/graphics/Paint;

    invoke-virtual {p1, p2, p2, v0, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 2528462
    iget-object v0, p0, LX/I11;->i:Landroid/graphics/Paint;

    invoke-virtual {p1, p2, p2, p2, v0}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 2528463
    iget v0, p0, LX/I11;->c:I

    iget-object v1, p0, LX/I11;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    sub-int/2addr v0, v1

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    .line 2528464
    invoke-virtual {p1, v0, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2528465
    iget-object v0, p0, LX/I11;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 2528466
    return-void
.end method

.method public static a(LX/I11;Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;FI)V
    .locals 2

    .prologue
    .line 2528457
    const/high16 v0, 0x40400000    # 3.0f

    add-float/2addr v0, p3

    iget-object v1, p0, LX/I11;->h:Landroid/graphics/Paint;

    invoke-virtual {p1, p3, p3, v0, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 2528458
    invoke-virtual {p2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 2528459
    int-to-float v0, p4

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2528460
    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    .line 2528431
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 2528432
    invoke-virtual {p0}, LX/I11;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 2528433
    iget v1, v0, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2528434
    iget-object v0, p0, LX/I11;->a:LX/0hL;

    invoke-virtual {v0}, LX/0hL;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2528435
    iget v0, p0, LX/I11;->c:I

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v0, v1

    .line 2528436
    iget-object v0, p0, LX/I11;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    .line 2528437
    iget v3, p0, LX/I11;->c:I

    iget v4, p0, LX/I11;->d:I

    sub-int/2addr v3, v4

    invoke-static {p0, p1, v0, v1, v3}, LX/I11;->a(LX/I11;Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;FI)V

    goto :goto_0

    .line 2528438
    :cond_0
    invoke-static {p0, p1, v1}, LX/I11;->a(LX/I11;Landroid/graphics/Canvas;F)V

    .line 2528439
    :goto_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 2528440
    return-void

    .line 2528441
    :cond_1
    iget v0, p0, LX/I11;->c:I

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v0, v1

    .line 2528442
    iget v0, p0, LX/I11;->g:I

    iget v2, p0, LX/I11;->c:I

    sub-int/2addr v0, v2

    int-to-float v0, v0

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2528443
    iget-object v0, p0, LX/I11;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    .line 2528444
    iget v3, p0, LX/I11;->c:I

    neg-int v3, v3

    iget v4, p0, LX/I11;->d:I

    add-int/2addr v3, v4

    invoke-static {p0, p1, v0, v1, v3}, LX/I11;->a(LX/I11;Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;FI)V

    goto :goto_2

    .line 2528445
    :cond_2
    invoke-static {p0, p1, v1}, LX/I11;->a(LX/I11;Landroid/graphics/Canvas;F)V

    .line 2528446
    goto :goto_1
.end method

.method public final getIntrinsicHeight()I
    .locals 1

    .prologue
    .line 2528456
    iget v0, p0, LX/I11;->c:I

    return v0
.end method

.method public final getIntrinsicWidth()I
    .locals 1

    .prologue
    .line 2528455
    iget v0, p0, LX/I11;->g:I

    return v0
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 2528478
    const/4 v0, -0x2

    return v0
.end method

.method public final invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 2528453
    invoke-virtual {p0}, LX/I11;->invalidateSelf()V

    .line 2528454
    return-void
.end method

.method public final scheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;J)V
    .locals 1

    .prologue
    .line 2528451
    invoke-virtual {p0, p2, p3, p4}, LX/I11;->scheduleSelf(Ljava/lang/Runnable;J)V

    .line 2528452
    return-void
.end method

.method public final setAlpha(I)V
    .locals 0

    .prologue
    .line 2528450
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0

    .prologue
    .line 2528449
    return-void
.end method

.method public final unscheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 2528447
    invoke-virtual {p0, p2}, LX/I11;->unscheduleSelf(Ljava/lang/Runnable;)V

    .line 2528448
    return-void
.end method
