.class public LX/J0P;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/payments/p2p/service/model/request/CancelPaymentRequestParams;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2636988
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2636989
    iput-object p1, p0, LX/J0P;->a:LX/0Or;

    .line 2636990
    return-void
.end method

.method public static a(LX/0QB;)LX/J0P;
    .locals 2

    .prologue
    .line 2636991
    new-instance v0, LX/J0P;

    const/16 v1, 0x12cc

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-direct {v0, v1}, LX/J0P;-><init>(LX/0Or;)V

    .line 2636992
    move-object v0, v0

    .line 2636993
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 2636994
    check-cast p1, Lcom/facebook/payments/p2p/service/model/request/CancelPaymentRequestParams;

    .line 2636995
    iget-object v0, p0, LX/J0P;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2636996
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "null ViewerContextUser found when canceling payment request id %s"

    .line 2636997
    iget-object v2, p1, Lcom/facebook/payments/p2p/service/model/request/CancelPaymentRequestParams;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2636998
    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2636999
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2637000
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "request_id"

    .line 2637001
    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/request/CancelPaymentRequestParams;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2637002
    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2637003
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "format"

    const-string v3, "json"

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2637004
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    const-string v2, "cancel_payment_request"

    .line 2637005
    iput-object v2, v0, LX/14O;->b:Ljava/lang/String;

    .line 2637006
    move-object v0, v0

    .line 2637007
    const-string v2, "POST"

    .line 2637008
    iput-object v2, v0, LX/14O;->c:Ljava/lang/String;

    .line 2637009
    move-object v2, v0

    .line 2637010
    const-string v3, "/%s/p2p_canceled_payment_requests"

    iget-object v0, p0, LX/J0P;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2637011
    iget-object p0, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, p0

    .line 2637012
    invoke-static {v3, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2637013
    iput-object v0, v2, LX/14O;->d:Ljava/lang/String;

    .line 2637014
    move-object v0, v2

    .line 2637015
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 2637016
    move-object v0, v0

    .line 2637017
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2637018
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2637019
    move-object v0, v0

    .line 2637020
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2637021
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2637022
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->F()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
