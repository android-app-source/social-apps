.class public final LX/Iah;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/business/nativesignup/view/BusinessEmailInputFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessEmailInputFragment;)V
    .locals 0

    .prologue
    .line 2591218
    iput-object p1, p0, LX/Iah;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessEmailInputFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x72a283d4

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2591219
    iget-object v1, p0, LX/Iah;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessEmailInputFragment;

    iget-object v1, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessEmailInputFragment;->a:LX/IZw;

    const-string v2, "click_request_code_button"

    invoke-virtual {v1, v2}, LX/IZw;->a(Ljava/lang/String;)V

    .line 2591220
    iget-object v1, p0, LX/Iah;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessEmailInputFragment;

    iget-object v1, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessEmailInputFragment;->c:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/messaging/business/nativesignup/view/BusinessEmailInputFragment;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2591221
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2591222
    const-string v2, "updated_email"

    iget-object v3, p0, LX/Iah;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessEmailInputFragment;

    iget-object v3, v3, Lcom/facebook/messaging/business/nativesignup/view/BusinessEmailInputFragment;->c:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2591223
    iget-object v2, p0, LX/Iah;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessEmailInputFragment;

    invoke-virtual {v2}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v2

    const/4 v3, -0x1

    invoke-virtual {v2, v3, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2591224
    iget-object v1, p0, LX/Iah;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessEmailInputFragment;

    invoke-virtual {v1}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    .line 2591225
    :goto_0
    const v1, -0x6a6c89c5

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2591226
    :cond_0
    iget-object v1, p0, LX/Iah;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessEmailInputFragment;

    iget-object v1, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessEmailInputFragment;->b:LX/IZK;

    invoke-virtual {v1}, LX/IZK;->a()V

    goto :goto_0
.end method
