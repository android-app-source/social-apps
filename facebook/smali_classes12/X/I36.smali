.class public LX/I36;
.super LX/Gcw;
.source ""


# instance fields
.field public c:LX/HxA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;",
            ">;"
        }
    .end annotation
.end field

.field public e:I

.field public f:Lcom/facebook/events/common/EventAnalyticsParams;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2531045
    invoke-direct {p0, p1}, LX/Gcw;-><init>(Landroid/content/Context;)V

    .line 2531046
    const-class p1, LX/I36;

    invoke-static {p1, p0}, LX/I36;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2531047
    new-instance p1, LX/I35;

    invoke-direct {p1, p0}, LX/I35;-><init>(LX/I36;)V

    iput-object p1, p0, LX/I36;->b:Landroid/view/View$OnClickListener;

    .line 2531048
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/I36;

    invoke-static {p0}, LX/HxA;->b(LX/0QB;)LX/HxA;

    move-result-object p0

    check-cast p0, LX/HxA;

    iput-object p0, p1, LX/I36;->c:LX/HxA;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2531049
    const v0, 0x7f0821cb

    const v1, 0x7f020872

    const v2, 0x7f0a010e

    invoke-virtual {p0, v0, v1, v2}, LX/Gcw;->a(III)V

    .line 2531050
    return-void
.end method

.method public final c(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2531051
    iget-object v0, p0, LX/I36;->d:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/I36;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gt v0, p1, :cond_1

    .line 2531052
    :cond_0
    const/4 v0, 0x0

    .line 2531053
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/I36;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->e()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getEventAnalyticsParams()Lcom/facebook/events/common/EventAnalyticsParams;
    .locals 1

    .prologue
    .line 2531054
    iget-object v0, p0, LX/I36;->f:Lcom/facebook/events/common/EventAnalyticsParams;

    return-object v0
.end method

.method public getModule()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2531055
    const-string v0, "event_subscriptions"

    return-object v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 2531056
    invoke-virtual {p0}, LX/I36;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0821ca

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getViewAllText()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 2531057
    invoke-virtual {p0}, LX/I36;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0821cc

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
