.class public final enum LX/IPL;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IPL;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IPL;

.field public static final enum FETCH_GROUPS:LX/IPL;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2573994
    new-instance v0, LX/IPL;

    const-string v1, "FETCH_GROUPS"

    invoke-direct {v0, v1, v2}, LX/IPL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IPL;->FETCH_GROUPS:LX/IPL;

    .line 2573995
    const/4 v0, 0x1

    new-array v0, v0, [LX/IPL;

    sget-object v1, LX/IPL;->FETCH_GROUPS:LX/IPL;

    aput-object v1, v0, v2

    sput-object v0, LX/IPL;->$VALUES:[LX/IPL;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2573996
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IPL;
    .locals 1

    .prologue
    .line 2573997
    const-class v0, LX/IPL;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IPL;

    return-object v0
.end method

.method public static values()[LX/IPL;
    .locals 1

    .prologue
    .line 2573998
    sget-object v0, LX/IPL;->$VALUES:[LX/IPL;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IPL;

    return-object v0
.end method
