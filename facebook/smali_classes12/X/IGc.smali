.class public final LX/IGc;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$FriendsNearbyLocationPingToUserModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;)V
    .locals 0

    .prologue
    .line 2556129
    iput-object p1, p0, LX/IGc;->a:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2556130
    iget-object v0, p0, LX/IGc;->a:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->x:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x30000b

    const-string v2, "FriendsNearbyPingFetchExist"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->f(ILjava/lang/String;)V

    .line 2556131
    iget-object v0, p0, LX/IGc;->a:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2556132
    iget-object v0, p0, LX/IGc;->a:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->w:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f080039

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2556133
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 2556134
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v4, 0x0

    .line 2556135
    iget-object v0, p0, LX/IGc;->a:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->x:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x30000b

    const-string v2, "FriendsNearbyPingFetchExist"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 2556136
    iget-object v0, p0, LX/IGc;->a:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    invoke-static {v0}, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->m$redex0(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;)V

    .line 2556137
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2556138
    check-cast v0, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$FriendsNearbyLocationPingToUserModel;

    invoke-virtual {v0}, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$FriendsNearbyLocationPingToUserModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2556139
    const-class v2, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$FriendsNearbyLocationPingToUserModel$LocationSharingModel$LocationPingToUserModel;

    invoke-virtual {v1, v0, v4, v2}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$FriendsNearbyLocationPingToUserModel$LocationSharingModel$LocationPingToUserModel;

    invoke-virtual {v0}, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$FriendsNearbyLocationPingToUserModel$LocationSharingModel$LocationPingToUserModel;->a()LX/2uF;

    move-result-object v0

    .line 2556140
    invoke-virtual {v0}, LX/39O;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2556141
    iget-object v0, p0, LX/IGc;->a:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    const/4 v1, 0x0

    .line 2556142
    iput-object v1, v0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->B:Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;

    .line 2556143
    :goto_0
    iget-object v0, p0, LX/IGc;->a:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    invoke-static {v0}, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->k(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;)V

    .line 2556144
    return-void

    .line 2556145
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2556146
    check-cast v0, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$FriendsNearbyLocationPingToUserModel;

    invoke-virtual {v0}, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$FriendsNearbyLocationPingToUserModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v2, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$FriendsNearbyLocationPingToUserModel$LocationSharingModel$LocationPingToUserModel;

    invoke-virtual {v1, v0, v4, v2}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$FriendsNearbyLocationPingToUserModel$LocationSharingModel$LocationPingToUserModel;

    invoke-virtual {v0}, Lcom/facebook/friendsnearby/pingdialog/graphql/FriendsNearbyPingGraphQLModels$FriendsNearbyLocationPingToUserModel$LocationSharingModel$LocationPingToUserModel;->a()LX/2uF;

    move-result-object v0

    invoke-virtual {v0, v4}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v2, v0, LX/1vs;->b:I

    .line 2556147
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2556148
    invoke-virtual {v1, v2, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 2556149
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    .line 2556150
    :goto_1
    iget-object v3, p0, LX/IGc;->a:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    new-instance v4, Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;

    iget-object v5, p0, LX/IGc;->a:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    iget-object v5, v5, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->A:Lcom/facebook/user/model/User;

    .line 2556151
    iget-object v6, v5, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v5, v6

    .line 2556152
    iget-object v6, p0, LX/IGc;->a:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    iget-object v6, v6, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->p:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v6

    invoke-static {v1, v2, v6, v7}, Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;->a(LX/15i;IJ)Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;

    move-result-object v1

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v2

    invoke-direct {v4, v5, v1, v2, v0}, Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;-><init>(Ljava/lang/String;Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;LX/0am;LX/0am;)V

    .line 2556153
    iput-object v4, v3, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->B:Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;

    .line 2556154
    goto :goto_0

    .line 2556155
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2556156
    :cond_1
    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    goto :goto_1
.end method
