.class public final LX/IWS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel;",
        ">;",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/IWT;


# direct methods
.method public constructor <init>(LX/IWT;)V
    .locals 0

    .prologue
    .line 2583871
    iput-object p1, p0, LX/IWS;->a:LX/IWT;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2583872
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2583873
    if-eqz p1, :cond_0

    .line 2583874
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2583875
    if-nez v0, :cond_1

    .line 2583876
    :cond_0
    sget-object v0, LX/1nY;->API_ERROR:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2583877
    :goto_0
    return-object v0

    .line 2583878
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2583879
    check-cast v0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel;

    invoke-virtual {v0}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel;->k()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v0

    .line 2583880
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->OPEN:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    if-eq v0, v1, :cond_2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    if-eq v0, v1, :cond_2

    .line 2583881
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2583882
    check-cast v0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel;

    invoke-virtual {v0}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel;->j()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-eq v0, v1, :cond_2

    .line 2583883
    iget-object v0, p0, LX/IWS;->a:LX/IWT;

    iget-object v0, v0, LX/IWT;->e:LX/IWJ;

    .line 2583884
    iget-object v1, v0, LX/IWJ;->a:Lcom/facebook/groups/photos/fragment/GroupAllPhotosFragment;

    iget-object v1, v1, Lcom/facebook/groups/photos/fragment/GroupAllPhotosFragment;->q:Lcom/facebook/widget/text/BetterTextView;

    const v2, 0x7f083897

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 2583885
    :cond_2
    iget-object v1, p0, LX/IWS;->a:LX/IWT;

    .line 2583886
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2583887
    check-cast v0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel;

    .line 2583888
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel;->a()Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;

    move-result-object v2

    if-nez v2, :cond_6

    .line 2583889
    :cond_3
    const-string v2, ""

    .line 2583890
    :goto_1
    move-object v0, v2

    .line 2583891
    iput-object v0, v1, LX/IWT;->a:Ljava/lang/String;

    .line 2583892
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2583893
    check-cast v0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel;

    .line 2583894
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel;->a()Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel;->a()Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;->k()Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel$MediaModel;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel;->a()Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;->k()Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel$MediaModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel$MediaModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2583895
    :cond_4
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2583896
    :goto_2
    move-object v1, v1

    .line 2583897
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2583898
    check-cast v0, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel;

    .line 2583899
    new-instance v2, LX/17L;

    invoke-direct {v2}, LX/17L;-><init>()V

    .line 2583900
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel;->a()Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;

    move-result-object v3

    if-eqz v3, :cond_5

    invoke-virtual {v0}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel;->a()Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;->k()Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel$MediaModel;

    move-result-object v3

    if-eqz v3, :cond_5

    invoke-virtual {v0}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel;->a()Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;->k()Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel$MediaModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel$MediaModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 2583901
    :cond_5
    invoke-virtual {v2}, LX/17L;->a()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    .line 2583902
    :goto_3
    move-object v0, v2

    .line 2583903
    new-instance v2, Lcom/facebook/photos/pandora/common/data/PandoraSlicedFeedResult;

    invoke-direct {v2, v0, v1}, Lcom/facebook/photos/pandora/common/data/PandoraSlicedFeedResult;-><init>(Lcom/facebook/graphql/model/GraphQLPageInfo;LX/0Px;)V

    invoke-static {v2}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_0

    :cond_6
    invoke-virtual {v0}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel;->a()Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;->j()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 2583904
    :cond_7
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 2583905
    const/4 v1, 0x0

    move v2, v1

    :goto_4
    invoke-virtual {v0}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel;->a()Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;->k()Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel$MediaModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel$MediaModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v2, v1, :cond_8

    .line 2583906
    new-instance p0, Lcom/facebook/photos/pandora/common/data/model/PandoraSingleMediaModel;

    invoke-virtual {v0}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel;->a()Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;->k()Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel$MediaModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel$MediaModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    invoke-direct {p0, v1}, Lcom/facebook/photos/pandora/common/data/model/PandoraSingleMediaModel;-><init>(Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;)V

    invoke-virtual {v3, p0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2583907
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_4

    .line 2583908
    :cond_8
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    goto/16 :goto_2

    .line 2583909
    :cond_9
    invoke-virtual {v0}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel;->a()Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;->k()Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel$MediaModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel$MediaModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->p_()Ljava/lang/String;

    move-result-object v3

    .line 2583910
    iput-object v3, v2, LX/17L;->f:Ljava/lang/String;

    .line 2583911
    invoke-virtual {v0}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel;->a()Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;->k()Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel$MediaModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel$MediaModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->a()Ljava/lang/String;

    move-result-object v3

    .line 2583912
    iput-object v3, v2, LX/17L;->c:Ljava/lang/String;

    .line 2583913
    invoke-virtual {v0}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel;->a()Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;->k()Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel$MediaModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel$MediaModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->b()Z

    move-result v3

    .line 2583914
    iput-boolean v3, v2, LX/17L;->d:Z

    .line 2583915
    invoke-virtual {v0}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel;->a()Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel;->k()Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel$MediaModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/photos/protocol/FetchGroupPhotosModels$FetchGroupPhotosModel$GroupMediasetModel$MediaModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->c()Z

    move-result v3

    .line 2583916
    iput-boolean v3, v2, LX/17L;->e:Z

    .line 2583917
    invoke-virtual {v2}, LX/17L;->a()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    goto/16 :goto_3
.end method
