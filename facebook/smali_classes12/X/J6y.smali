.class public final LX/J6y;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;)V
    .locals 0

    .prologue
    .line 2650419
    iput-object p1, p0, LX/J6y;->a:Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x72ce2343

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2650420
    iget-object v1, p0, LX/J6y;->a:Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;

    iget-object v1, v1, Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;->p:LX/Hrh;

    .line 2650421
    iget-object v3, v1, LX/Hrh;->a:Lcom/facebook/privacy/educator/InlinePrivacySurveyDialog;

    invoke-virtual {v3}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2650422
    iget-object v3, v1, LX/Hrh;->d:LX/Hrk;

    const/4 v4, 0x1

    .line 2650423
    iput-boolean v4, v3, LX/Hrk;->e:Z

    .line 2650424
    iget-object v3, v1, LX/Hrh;->d:LX/Hrk;

    iget-object v3, v3, LX/Hrk;->i:LX/8Ps;

    iget-object v4, v1, LX/Hrh;->d:LX/Hrk;

    iget-object v4, v4, LX/Hrk;->d:Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;

    iget-object v4, v4, Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;->mTriggerPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    const-string v5, "traditional_composer"

    .line 2650425
    iget-object v6, v3, LX/8Ps;->b:Lcom/facebook/privacy/PrivacyOperationsClient;

    sget-object v7, LX/5nc;->MORE_OPTIONS:LX/5nc;

    invoke-static {v3}, LX/8Ps;->a(LX/8Ps;)Ljava/lang/Long;

    move-result-object v8

    invoke-static {v4}, LX/8Ps;->a(LX/1oU;)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    move-object v11, v5

    invoke-virtual/range {v6 .. v11}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(LX/5nc;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2650426
    iget-object v3, v1, LX/Hrh;->c:LX/HqM;

    .line 2650427
    iget-object v4, v3, LX/HqM;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v4, v4, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v4}, Lcom/facebook/composer/system/model/ComposerModelImpl;->u()Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->a()LX/AP3;

    move-result-object v4

    sget-object v5, LX/2by;->INLINE_PRIVACY_SURVEY:LX/2by;

    .line 2650428
    iput-object v5, v4, LX/AP3;->e:LX/2by;

    .line 2650429
    move-object v4, v4

    .line 2650430
    invoke-virtual {v4}, LX/AP3;->a()Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    move-result-object v5

    .line 2650431
    iget-object v4, v3, LX/HqM;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v4, v4, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object v6, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v4, v6}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v4

    check-cast v4, LX/0jL;

    invoke-virtual {v4, v5}, LX/0jL;->a(Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0jL;

    invoke-virtual {v4}, LX/0jL;->a()V

    .line 2650432
    iget-object v4, v3, LX/HqM;->a:Lcom/facebook/composer/activity/ComposerFragment;

    invoke-static {v4}, Lcom/facebook/composer/activity/ComposerFragment;->N$redex0(Lcom/facebook/composer/activity/ComposerFragment;)V

    .line 2650433
    const v1, -0x63c000ac

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
