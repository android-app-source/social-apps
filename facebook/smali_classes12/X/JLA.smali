.class public LX/JLA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/345;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/JLA;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0mh;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1tk;


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0mh;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2680705
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2680706
    iput-object p1, p0, LX/JLA;->a:LX/0Ot;

    .line 2680707
    const/4 p1, 0x1

    .line 2680708
    const-string v0, "react_native_red_box_module"

    const-string v1, "react_native_red_box_shown"

    sget-object v2, LX/0mq;->CLIENT_EVENT:LX/0mq;

    invoke-static {v0, v1, p1, v2, p1}, LX/1tk;->a(Ljava/lang/String;Ljava/lang/String;ZLX/0mq;Z)LX/1tk;

    move-result-object v0

    iput-object v0, p0, LX/JLA;->b:LX/1tk;

    .line 2680709
    return-void
.end method

.method public static a(LX/0QB;)LX/JLA;
    .locals 4

    .prologue
    .line 2680710
    sget-object v0, LX/JLA;->c:LX/JLA;

    if-nez v0, :cond_1

    .line 2680711
    const-class v1, LX/JLA;

    monitor-enter v1

    .line 2680712
    :try_start_0
    sget-object v0, LX/JLA;->c:LX/JLA;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2680713
    if-eqz v2, :cond_0

    .line 2680714
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2680715
    new-instance v3, LX/JLA;

    const/16 p0, 0xd4

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JLA;-><init>(LX/0Ot;)V

    .line 2680716
    move-object v0, v3

    .line 2680717
    sput-object v0, LX/JLA;->c:LX/JLA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2680718
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2680719
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2680720
    :cond_1
    sget-object v0, LX/JLA;->c:LX/JLA;

    return-object v0

    .line 2680721
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2680722
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
