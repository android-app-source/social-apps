.class public final LX/HMY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;)V
    .locals 0

    .prologue
    .line 2457268
    iput-object p1, p0, LX/HMY;->a:Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2457269
    iget-object v0, p0, LX/HMY;->a:Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;

    .line 2457270
    iget-object v1, v0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->q:Lcom/facebook/widget/listview/BetterListView;

    move-object v0, v1

    .line 2457271
    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/HMY;->a:Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;

    .line 2457272
    iget-object v1, v0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->q:Lcom/facebook/widget/listview/BetterListView;

    move-object v0, v1

    .line 2457273
    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    move v1, v0

    .line 2457274
    :goto_0
    iget-object v0, p0, LX/HMY;->a:Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;

    iget-boolean v0, v0, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->q:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HMY;->a:Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;

    iget v0, v0, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->r:I

    if-eq v0, v1, :cond_1

    .line 2457275
    :cond_0
    iget-object v0, p0, LX/HMY;->a:Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->k:Landroid/view/View;

    iget-object v3, p0, LX/HMY;->a:Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;

    .line 2457276
    iget-object v4, v3, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->q:Lcom/facebook/widget/listview/BetterListView;

    move-object v3, v4

    .line 2457277
    sget-object v4, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->p:[I

    iget-object v5, p0, LX/HMY;->a:Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;

    .line 2457278
    iget-object p1, v5, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->h:LX/0hB;

    invoke-virtual {p1}, LX/0hB;->d()I

    move-result p1

    move p1, p1

    .line 2457279
    move v5, p1

    .line 2457280
    invoke-static {v0, v3, p2, v4, v5}, LX/8FX;->a(Landroid/view/View;Landroid/view/ViewGroup;I[II)LX/3rL;

    move-result-object v3

    .line 2457281
    iget-object v4, p0, LX/HMY;->a:Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;

    iget-object v0, v3, LX/3rL;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 2457282
    iput-boolean v0, v4, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->q:Z

    .line 2457283
    iget-object v0, p0, LX/HMY;->a:Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;

    iget-boolean v0, v0, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->q:Z

    if-eqz v0, :cond_1

    .line 2457284
    iget-object v4, p0, LX/HMY;->a:Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;

    iget-object v0, v3, LX/3rL;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v4, v0}, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->E_(I)V

    .line 2457285
    iget-object v0, p0, LX/HMY;->a:Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;

    .line 2457286
    iput v1, v0, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->r:I

    .line 2457287
    :cond_1
    iget-object v0, p0, LX/HMY;->a:Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;

    .line 2457288
    iget-object v1, v0, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->q:Lcom/facebook/widget/listview/BetterListView;

    move-object v0, v1

    .line 2457289
    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/BetterListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    .line 2457290
    iget-object v1, p0, LX/HMY;->a:Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->l:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/HMY;->a:Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;

    .line 2457291
    iget-object v2, v1, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->q:Lcom/facebook/widget/listview/BetterListView;

    move-object v1, v2

    .line 2457292
    if-eqz v1, :cond_2

    iget-object v1, p0, LX/HMY;->a:Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;

    iget v1, v1, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->m:I

    if-eq v1, v0, :cond_2

    iget-object v1, p0, LX/HMY;->a:Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2457293
    iget-object v1, p0, LX/HMY;->a:Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->l:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v2, p0, LX/HMY;->a:Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;

    .line 2457294
    iget-object v3, v2, Lcom/facebook/reviews/ui/PageReviewsFeedFragment;->q:Lcom/facebook/widget/listview/BetterListView;

    move-object v2, v3

    .line 2457295
    invoke-virtual {v1, v2, p2}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->a(Landroid/view/ViewGroup;I)V

    .line 2457296
    iget-object v1, p0, LX/HMY;->a:Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;

    .line 2457297
    iput v0, v1, Lcom/facebook/pages/common/reviews/PageSurfaceReviewsFeedFragment;->m:I

    .line 2457298
    :cond_2
    return-void

    :cond_3
    move v1, v2

    .line 2457299
    goto/16 :goto_0
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 2457267
    return-void
.end method
