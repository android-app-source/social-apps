.class public LX/Ijd;
.super LX/73G;
.source ""


# instance fields
.field private a:Lcom/facebook/common/locale/Country;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2605950
    invoke-direct {p0, p1}, LX/73G;-><init>(Landroid/content/res/Resources;)V

    .line 2605951
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 2605942
    sget-object v0, Lcom/facebook/common/locale/Country;->a:Lcom/facebook/common/locale/Country;

    iget-object v1, p0, LX/Ijd;->a:Lcom/facebook/common/locale/Country;

    invoke-virtual {v0, v1}, Lcom/facebook/common/locale/Country;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2605943
    invoke-super {p0}, LX/73G;->a()I

    move-result v0

    .line 2605944
    :goto_0
    return v0

    :cond_0
    const v0, 0x7fffffff

    goto :goto_0
.end method

.method public final a(Lcom/facebook/common/locale/Country;)V
    .locals 0

    .prologue
    .line 2605945
    iput-object p1, p0, LX/Ijd;->a:Lcom/facebook/common/locale/Country;

    .line 2605946
    return-void
.end method

.method public final a(LX/6z8;)Z
    .locals 2

    .prologue
    .line 2605947
    sget-object v0, Lcom/facebook/common/locale/Country;->a:Lcom/facebook/common/locale/Country;

    iget-object v1, p0, LX/Ijd;->a:Lcom/facebook/common/locale/Country;

    invoke-virtual {v0, v1}, Lcom/facebook/common/locale/Country;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2605948
    invoke-super {p0, p1}, LX/73G;->a(LX/6z8;)Z

    move-result v0

    .line 2605949
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
