.class public final LX/ItC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/facebook/messaging/service/model/NewMessageResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/service/model/SendMessageParams;

.field public final synthetic b:Lcom/facebook/messaging/send/client/SendMessageManager;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/service/model/SendMessageParams;)V
    .locals 0

    .prologue
    .line 2622190
    iput-object p1, p0, LX/ItC;->b:Lcom/facebook/messaging/send/client/SendMessageManager;

    iput-object p2, p0, LX/ItC;->a:Lcom/facebook/messaging/service/model/SendMessageParams;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2622191
    iget-object v0, p0, LX/ItC;->b:Lcom/facebook/messaging/send/client/SendMessageManager;

    iget-object v0, v0, Lcom/facebook/messaging/send/client/SendMessageManager;->z:LX/Itk;

    iget-object v1, p0, LX/ItC;->a:Lcom/facebook/messaging/service/model/SendMessageParams;

    invoke-virtual {v0, v1}, LX/Itk;->b(Lcom/facebook/messaging/service/model/SendMessageParams;)Lcom/facebook/messaging/service/model/NewMessageResult;

    move-result-object v0

    return-object v0
.end method
