.class public LX/IK9;
.super Ljava/util/Observable;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field public final a:LX/0WJ;

.field public b:Ljava/lang/String;

.field public c:LX/2kM;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2kM",
            "<",
            "Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/2kM;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2kM",
            "<",
            "Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;",
            ">;"
        }
    .end annotation
.end field

.field public e:Z

.field public f:LX/1lD;


# direct methods
.method public constructor <init>(LX/0WJ;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2565091
    invoke-direct {p0}, Ljava/util/Observable;-><init>()V

    .line 2565092
    sget-object v0, LX/1lD;->LOAD_FINISHED:LX/1lD;

    iput-object v0, p0, LX/IK9;->f:LX/1lD;

    .line 2565093
    iput-object p1, p0, LX/IK9;->a:LX/0WJ;

    .line 2565094
    return-void
.end method

.method public static a(LX/0QB;)LX/IK9;
    .locals 4

    .prologue
    .line 2565095
    const-class v1, LX/IK9;

    monitor-enter v1

    .line 2565096
    :try_start_0
    sget-object v0, LX/IK9;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2565097
    sput-object v2, LX/IK9;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2565098
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2565099
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2565100
    new-instance p0, LX/IK9;

    invoke-static {v0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v3

    check-cast v3, LX/0WJ;

    invoke-direct {p0, v3}, LX/IK9;-><init>(LX/0WJ;)V

    .line 2565101
    move-object v0, p0

    .line 2565102
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2565103
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IK9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2565104
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2565105
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private k()V
    .locals 3

    .prologue
    .line 2565106
    invoke-static {}, LX/44p;->b()LX/44p;

    move-result-object v0

    new-instance v1, Lcom/facebook/groups/channels/GroupChannelsUIModel$1;

    invoke-direct {v1, p0}, Lcom/facebook/groups/channels/GroupChannelsUIModel$1;-><init>(LX/IK9;)V

    const v2, 0x667faa7f

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2565107
    return-void
.end method


# virtual methods
.method public final a(LX/1lD;)V
    .locals 1

    .prologue
    .line 2565108
    iget-object v0, p0, LX/IK9;->f:LX/1lD;

    if-eq p1, v0, :cond_0

    .line 2565109
    iput-object p1, p0, LX/IK9;->f:LX/1lD;

    .line 2565110
    invoke-virtual {p0}, LX/IK9;->setChanged()V

    .line 2565111
    invoke-direct {p0}, LX/IK9;->k()V

    .line 2565112
    :cond_0
    return-void
.end method

.method public final a(LX/2kM;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2kM",
            "<",
            "Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2565113
    if-eqz p1, :cond_0

    .line 2565114
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/IK9;->e:Z

    .line 2565115
    :cond_0
    iput-object p1, p0, LX/IK9;->c:LX/2kM;

    .line 2565116
    invoke-virtual {p0}, LX/IK9;->setChanged()V

    .line 2565117
    invoke-direct {p0}, LX/IK9;->k()V

    .line 2565118
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 2565119
    invoke-virtual {p0}, LX/IK9;->setChanged()V

    .line 2565120
    invoke-direct {p0}, LX/IK9;->k()V

    .line 2565121
    iput-boolean p1, p0, LX/IK9;->e:Z

    .line 2565122
    return-void
.end method

.method public final b(LX/2kM;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2kM",
            "<",
            "Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2565123
    if-eqz p1, :cond_0

    .line 2565124
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/IK9;->e:Z

    .line 2565125
    :cond_0
    iput-object p1, p0, LX/IK9;->d:LX/2kM;

    .line 2565126
    invoke-virtual {p0}, LX/IK9;->setChanged()V

    .line 2565127
    invoke-direct {p0}, LX/IK9;->k()V

    .line 2565128
    return-void
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 2565129
    iget-object v0, p0, LX/IK9;->c:LX/2kM;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/IK9;->c:LX/2kM;

    invoke-interface {v0}, LX/2kM;->c()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 2565130
    iget-object v0, p0, LX/IK9;->d:LX/2kM;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/IK9;->d:LX/2kM;

    invoke-interface {v0}, LX/2kM;->c()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
