.class public final LX/IDC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchSuggestionsFriendListQueryModel;",
        ">;",
        "LX/IDG;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/IDE;


# direct methods
.method public constructor <init>(LX/IDE;)V
    .locals 0

    .prologue
    .line 2550434
    iput-object p1, p0, LX/IDC;->a:LX/IDE;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2550435
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2550436
    if-eqz p1, :cond_0

    .line 2550437
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2550438
    if-eqz v0, :cond_0

    .line 2550439
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2550440
    check-cast v0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchSuggestionsFriendListQueryModel;

    invoke-virtual {v0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchSuggestionsFriendListQueryModel;->a()Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchSuggestionsFriendListQueryModel$FriendsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2550441
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2550442
    check-cast v0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchSuggestionsFriendListQueryModel;

    invoke-virtual {v0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchSuggestionsFriendListQueryModel;->a()Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchSuggestionsFriendListQueryModel$FriendsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchSuggestionsFriendListQueryModel$FriendsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2550443
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2550444
    sget-object v1, LX/IDE;->a:LX/4a7;

    invoke-virtual {v1}, LX/4a7;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v1

    invoke-static {v0, v1}, LX/IDE;->b(Ljava/util/List;Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;)LX/IDG;

    move-result-object v0

    .line 2550445
    :goto_0
    return-object v0

    .line 2550446
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2550447
    check-cast v0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchSuggestionsFriendListQueryModel;

    invoke-virtual {v0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchSuggestionsFriendListQueryModel;->a()Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchSuggestionsFriendListQueryModel$FriendsModel;

    move-result-object v0

    .line 2550448
    invoke-virtual {v0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchSuggestionsFriendListQueryModel$FriendsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchSuggestionsFriendListQueryModel$FriendsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    invoke-static {v1, v0}, LX/IDE;->b(Ljava/util/List;Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;)LX/IDG;

    move-result-object v0

    goto :goto_0
.end method
