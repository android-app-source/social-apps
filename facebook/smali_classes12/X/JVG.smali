.class public LX/JVG;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/3mL;

.field public final b:LX/JV6;

.field public final c:LX/1LV;


# direct methods
.method public constructor <init>(LX/3mL;LX/JV6;LX/1LV;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2700418
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2700419
    iput-object p1, p0, LX/JVG;->a:LX/3mL;

    .line 2700420
    iput-object p2, p0, LX/JVG;->b:LX/JV6;

    .line 2700421
    iput-object p3, p0, LX/JVG;->c:LX/1LV;

    .line 2700422
    return-void
.end method

.method public static a(LX/0QB;)LX/JVG;
    .locals 6

    .prologue
    .line 2700423
    const-class v1, LX/JVG;

    monitor-enter v1

    .line 2700424
    :try_start_0
    sget-object v0, LX/JVG;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2700425
    sput-object v2, LX/JVG;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2700426
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2700427
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2700428
    new-instance p0, LX/JVG;

    invoke-static {v0}, LX/3mL;->a(LX/0QB;)LX/3mL;

    move-result-object v3

    check-cast v3, LX/3mL;

    const-class v4, LX/JV6;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/JV6;

    invoke-static {v0}, LX/1LV;->a(LX/0QB;)LX/1LV;

    move-result-object v5

    check-cast v5, LX/1LV;

    invoke-direct {p0, v3, v4, v5}, LX/JVG;-><init>(LX/3mL;LX/JV6;LX/1LV;)V

    .line 2700429
    move-object v0, p0

    .line 2700430
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2700431
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JVG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2700432
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2700433
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
