.class public final LX/IGa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;)V
    .locals 0

    .prologue
    .line 2556100
    iput-object p1, p0, LX/IGa;->a:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x3667b4a4    # -1247595.5f

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2556101
    iget-object v1, p0, LX/IGa;->a:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    .line 2556102
    iget-object v2, v1, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->F:LX/0Px;

    iget-object v3, v1, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->J:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v3}, Lcom/facebook/widget/listview/BetterListView;->getCheckedItemPosition()I

    move-result v3

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/IGe;

    move-object v1, v2

    .line 2556103
    iget-object v2, p0, LX/IGa;->a:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    .line 2556104
    iget-object v3, v2, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->M:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    .line 2556105
    if-nez v3, :cond_2

    .line 2556106
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v3

    .line 2556107
    :goto_0
    move-object v2, v3

    .line 2556108
    iget-object v3, p0, LX/IGa;->a:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    iget-object v3, v3, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->B:Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;

    if-nez v3, :cond_1

    .line 2556109
    iget-object v3, p0, LX/IGa;->a:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    iget-object v3, v3, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->z:LX/IGP;

    iget-object v4, v1, LX/IGe;->d:Ljava/lang/String;

    .line 2556110
    new-instance v5, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v6, "friends_nearby_dashboard_ping_send"

    invoke-direct {v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v6, "background_location"

    .line 2556111
    iput-object v6, v5, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2556112
    move-object v6, v5

    .line 2556113
    const-string v5, "duration_option"

    invoke-virtual {v6, v5, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2556114
    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2556115
    const-string p1, "message"

    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v6, p1, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2556116
    :cond_0
    iget-object v5, v3, LX/IGP;->a:LX/0Zb;

    invoke-interface {v5, v6}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2556117
    :goto_1
    iget-object v3, p0, LX/IGa;->a:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    iget-object v1, v1, LX/IGe;->a:Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;

    invoke-static {v3, v1, v2}, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->a$redex0(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;LX/0am;)V

    .line 2556118
    const v1, -0xcd9b694

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2556119
    :cond_1
    iget-object v3, p0, LX/IGa;->a:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    iget-object v3, v3, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->z:LX/IGP;

    iget-object v4, v1, LX/IGe;->d:Ljava/lang/String;

    .line 2556120
    new-instance v5, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v6, "friends_nearby_dashboard_ping_edit"

    invoke-direct {v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v6, "background_location"

    .line 2556121
    iput-object v6, v5, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2556122
    move-object v5, v5

    .line 2556123
    const-string v6, "duration_option"

    invoke-virtual {v5, v6, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2556124
    iget-object v6, v3, LX/IGP;->a:LX/0Zb;

    invoke-interface {v6, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2556125
    goto :goto_1

    :cond_2
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v3

    goto :goto_0
.end method
