.class public final LX/IWy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AhN;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/widget/actionbar/GroupsActionBar;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/widget/actionbar/GroupsActionBar;)V
    .locals 0

    .prologue
    .line 2585349
    iput-object p1, p0, LX/IWy;->a:Lcom/facebook/groups/widget/actionbar/GroupsActionBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 2585350
    invoke-interface {p1}, Landroid/view/MenuItem;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 2585351
    :goto_0
    return v0

    .line 2585352
    :cond_0
    iget-object v0, p0, LX/IWy;->a:Lcom/facebook/groups/widget/actionbar/GroupsActionBar;

    iget-object v0, v0, Lcom/facebook/groups/widget/actionbar/GroupsActionBar;->d:LX/IT4;

    if-nez v0, :cond_1

    .line 2585353
    const/4 v0, 0x0

    goto :goto_0

    .line 2585354
    :cond_1
    invoke-static {}, LX/IX1;->values()[LX/IX1;

    move-result-object v2

    iget-object v0, p0, LX/IWy;->a:Lcom/facebook/groups/widget/actionbar/GroupsActionBar;

    iget-object v0, v0, Lcom/facebook/groups/widget/actionbar/GroupsActionBar;->b:Ljava/util/Map;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aget-object v0, v2, v0

    .line 2585355
    iget-object v2, p0, LX/IWy;->a:Lcom/facebook/groups/widget/actionbar/GroupsActionBar;

    iget-object v2, v2, Lcom/facebook/groups/widget/actionbar/GroupsActionBar;->d:LX/IT4;

    invoke-interface {v2, v0}, LX/IT4;->a(LX/IX1;)V

    move v0, v1

    .line 2585356
    goto :goto_0
.end method
