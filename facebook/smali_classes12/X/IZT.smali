.class public final enum LX/IZT;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IZT;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IZT;

.field public static final enum HORIZONTAL:LX/IZT;

.field public static final enum VERTICAL:LX/IZT;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2589279
    new-instance v0, LX/IZT;

    const-string v1, "VERTICAL"

    invoke-direct {v0, v1, v2}, LX/IZT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IZT;->VERTICAL:LX/IZT;

    .line 2589280
    new-instance v0, LX/IZT;

    const-string v1, "HORIZONTAL"

    invoke-direct {v0, v1, v3}, LX/IZT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IZT;->HORIZONTAL:LX/IZT;

    .line 2589281
    const/4 v0, 0x2

    new-array v0, v0, [LX/IZT;

    sget-object v1, LX/IZT;->VERTICAL:LX/IZT;

    aput-object v1, v0, v2

    sget-object v1, LX/IZT;->HORIZONTAL:LX/IZT;

    aput-object v1, v0, v3

    sput-object v0, LX/IZT;->$VALUES:[LX/IZT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2589282
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IZT;
    .locals 1

    .prologue
    .line 2589283
    const-class v0, LX/IZT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IZT;

    return-object v0
.end method

.method public static values()[LX/IZT;
    .locals 1

    .prologue
    .line 2589284
    sget-object v0, LX/IZT;->$VALUES:[LX/IZT;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IZT;

    return-object v0
.end method
