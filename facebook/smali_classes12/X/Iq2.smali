.class public LX/Iq2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;

.field private static final g:LX/1sw;

.field private static final h:LX/1sw;


# instance fields
.field public final deltas:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Ipt;",
            ">;"
        }
    .end annotation
.end field

.field public final errorCode:Ljava/lang/String;

.field public final firstDeltaSeqId:Ljava/lang/Long;

.field public final lastIssuedSeqId:Ljava/lang/Long;

.field public final queueEntityId:Ljava/lang/Long;

.field public final syncToken:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/16 v4, 0xb

    const/16 v3, 0xa

    .line 2616474
    new-instance v0, LX/1sv;

    const-string v1, "PaymentSyncPayload"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/Iq2;->b:LX/1sv;

    .line 2616475
    new-instance v0, LX/1sw;

    const-string v1, "deltas"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2, v5}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Iq2;->c:LX/1sw;

    .line 2616476
    new-instance v0, LX/1sw;

    const-string v1, "firstDeltaSeqId"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Iq2;->d:LX/1sw;

    .line 2616477
    new-instance v0, LX/1sw;

    const-string v1, "lastIssuedSeqId"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Iq2;->e:LX/1sw;

    .line 2616478
    new-instance v0, LX/1sw;

    const-string v1, "queueEntityId"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v3, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Iq2;->f:LX/1sw;

    .line 2616479
    new-instance v0, LX/1sw;

    const-string v1, "syncToken"

    invoke-direct {v0, v1, v4, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Iq2;->g:LX/1sw;

    .line 2616480
    new-instance v0, LX/1sw;

    const-string v1, "errorCode"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Iq2;->h:LX/1sw;

    .line 2616481
    sput-boolean v5, LX/Iq2;->a:Z

    return-void
.end method

.method private constructor <init>(Ljava/util/List;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/Ipt;",
            ">;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2616647
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2616648
    iput-object p1, p0, LX/Iq2;->deltas:Ljava/util/List;

    .line 2616649
    iput-object p2, p0, LX/Iq2;->firstDeltaSeqId:Ljava/lang/Long;

    .line 2616650
    iput-object p3, p0, LX/Iq2;->lastIssuedSeqId:Ljava/lang/Long;

    .line 2616651
    iput-object p4, p0, LX/Iq2;->queueEntityId:Ljava/lang/Long;

    .line 2616652
    iput-object p5, p0, LX/Iq2;->syncToken:Ljava/lang/String;

    .line 2616653
    iput-object p6, p0, LX/Iq2;->errorCode:Ljava/lang/String;

    .line 2616654
    return-void
.end method

.method public static b(LX/1su;)LX/Iq2;
    .locals 14

    .prologue
    const/16 v11, 0xb

    const/4 v7, 0x0

    const/16 v10, 0xa

    const/4 v6, 0x0

    .line 2616603
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    move-object v5, v6

    move-object v4, v6

    move-object v3, v6

    move-object v2, v6

    move-object v1, v6

    .line 2616604
    :cond_0
    :goto_0
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v0

    .line 2616605
    iget-byte v8, v0, LX/1sw;->b:B

    if-eqz v8, :cond_9

    .line 2616606
    iget-short v8, v0, LX/1sw;->c:S

    packed-switch v8, :pswitch_data_0

    .line 2616607
    :pswitch_0
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2616608
    :pswitch_1
    iget-byte v8, v0, LX/1sw;->b:B

    const/16 v9, 0xf

    if-ne v8, v9, :cond_3

    .line 2616609
    invoke-virtual {p0}, LX/1su;->h()LX/1u3;

    move-result-object v8

    .line 2616610
    new-instance v1, Ljava/util/ArrayList;

    iget v0, v8, LX/1u3;->b:I

    invoke-static {v7, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    move v0, v7

    .line 2616611
    :goto_1
    iget v9, v8, LX/1u3;->b:I

    if-gez v9, :cond_2

    invoke-static {}, LX/1su;->t()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 2616612
    :goto_2
    new-instance v9, LX/Ipt;

    invoke-direct {v9}, LX/Ipt;-><init>()V

    .line 2616613
    new-instance v9, LX/Ipt;

    invoke-direct {v9}, LX/Ipt;-><init>()V

    .line 2616614
    const/4 v12, 0x0

    iput v12, v9, LX/Ipt;->setField_:I

    .line 2616615
    const/4 v12, 0x0

    iput-object v12, v9, LX/Ipt;->value_:Ljava/lang/Object;

    .line 2616616
    invoke-virtual {p0}, LX/1su;->r()LX/1sv;

    .line 2616617
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    move-result-object v12

    .line 2616618
    invoke-virtual {v9, p0, v12}, LX/Ipt;->a(LX/1su;LX/1sw;)Ljava/lang/Object;

    move-result-object v13

    iput-object v13, v9, LX/Ipt;->value_:Ljava/lang/Object;

    .line 2616619
    iget-object v13, v9, LX/6kT;->value_:Ljava/lang/Object;

    if-eqz v13, :cond_1

    .line 2616620
    iget-short v12, v12, LX/1sw;->c:S

    iput v12, v9, LX/Ipt;->setField_:I

    .line 2616621
    :cond_1
    invoke-virtual {p0}, LX/1su;->f()LX/1sw;

    .line 2616622
    invoke-virtual {p0}, LX/1su;->e()V

    .line 2616623
    move-object v9, v9

    .line 2616624
    move-object v9, v9

    .line 2616625
    invoke-interface {v1, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2616626
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2616627
    :cond_2
    iget v9, v8, LX/1u3;->b:I

    if-ge v0, v9, :cond_0

    goto :goto_2

    .line 2616628
    :cond_3
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto :goto_0

    .line 2616629
    :pswitch_2
    iget-byte v8, v0, LX/1sw;->b:B

    if-ne v8, v10, :cond_4

    .line 2616630
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    goto :goto_0

    .line 2616631
    :cond_4
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2616632
    :pswitch_3
    iget-byte v8, v0, LX/1sw;->b:B

    if-ne v8, v10, :cond_5

    .line 2616633
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    goto/16 :goto_0

    .line 2616634
    :cond_5
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2616635
    :pswitch_4
    iget-byte v8, v0, LX/1sw;->b:B

    if-ne v8, v10, :cond_6

    .line 2616636
    invoke-virtual {p0}, LX/1su;->n()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    goto/16 :goto_0

    .line 2616637
    :cond_6
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2616638
    :pswitch_5
    iget-byte v8, v0, LX/1sw;->b:B

    if-ne v8, v11, :cond_7

    .line 2616639
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0

    .line 2616640
    :cond_7
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2616641
    :pswitch_6
    iget-byte v8, v0, LX/1sw;->b:B

    if-ne v8, v11, :cond_8

    .line 2616642
    invoke-virtual {p0}, LX/1su;->p()Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_0

    .line 2616643
    :cond_8
    iget-byte v0, v0, LX/1sw;->b:B

    invoke-static {p0, v0}, LX/3ae;->a(LX/1su;B)V

    goto/16 :goto_0

    .line 2616644
    :cond_9
    invoke-virtual {p0}, LX/1su;->e()V

    .line 2616645
    new-instance v0, LX/Iq2;

    invoke-direct/range {v0 .. v6}, LX/Iq2;-><init>(Ljava/util/List;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)V

    .line 2616646
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2616536
    if-eqz p2, :cond_a

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    .line 2616537
    :goto_0
    if-eqz p2, :cond_b

    const-string v0, "\n"

    move-object v3, v0

    .line 2616538
    :goto_1
    if-eqz p2, :cond_c

    const-string v0, " "

    .line 2616539
    :goto_2
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v1, "PaymentSyncPayload"

    invoke-direct {v5, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2616540
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616541
    const-string v1, "("

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616542
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616543
    const/4 v1, 0x1

    .line 2616544
    iget-object v6, p0, LX/Iq2;->deltas:Ljava/util/List;

    if-eqz v6, :cond_0

    .line 2616545
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616546
    const-string v1, "deltas"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616547
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616548
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616549
    iget-object v1, p0, LX/Iq2;->deltas:Ljava/util/List;

    if-nez v1, :cond_d

    .line 2616550
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    move v1, v2

    .line 2616551
    :cond_0
    iget-object v6, p0, LX/Iq2;->firstDeltaSeqId:Ljava/lang/Long;

    if-eqz v6, :cond_2

    .line 2616552
    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616553
    :cond_1
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616554
    const-string v1, "firstDeltaSeqId"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616555
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616556
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616557
    iget-object v1, p0, LX/Iq2;->firstDeltaSeqId:Ljava/lang/Long;

    if-nez v1, :cond_e

    .line 2616558
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_4
    move v1, v2

    .line 2616559
    :cond_2
    iget-object v6, p0, LX/Iq2;->lastIssuedSeqId:Ljava/lang/Long;

    if-eqz v6, :cond_4

    .line 2616560
    if-nez v1, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616561
    :cond_3
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616562
    const-string v1, "lastIssuedSeqId"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616563
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616564
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616565
    iget-object v1, p0, LX/Iq2;->lastIssuedSeqId:Ljava/lang/Long;

    if-nez v1, :cond_f

    .line 2616566
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_5
    move v1, v2

    .line 2616567
    :cond_4
    iget-object v6, p0, LX/Iq2;->queueEntityId:Ljava/lang/Long;

    if-eqz v6, :cond_6

    .line 2616568
    if-nez v1, :cond_5

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616569
    :cond_5
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616570
    const-string v1, "queueEntityId"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616571
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616572
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616573
    iget-object v1, p0, LX/Iq2;->queueEntityId:Ljava/lang/Long;

    if-nez v1, :cond_10

    .line 2616574
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_6
    move v1, v2

    .line 2616575
    :cond_6
    iget-object v6, p0, LX/Iq2;->syncToken:Ljava/lang/String;

    if-eqz v6, :cond_13

    .line 2616576
    if-nez v1, :cond_7

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616577
    :cond_7
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616578
    const-string v1, "syncToken"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616579
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616580
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616581
    iget-object v1, p0, LX/Iq2;->syncToken:Ljava/lang/String;

    if-nez v1, :cond_11

    .line 2616582
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616583
    :goto_7
    iget-object v1, p0, LX/Iq2;->errorCode:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 2616584
    if-nez v2, :cond_8

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616585
    :cond_8
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616586
    const-string v1, "errorCode"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616587
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616588
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616589
    iget-object v0, p0, LX/Iq2;->errorCode:Ljava/lang/String;

    if-nez v0, :cond_12

    .line 2616590
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616591
    :cond_9
    :goto_8
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616592
    const-string v0, ")"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2616593
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2616594
    :cond_a
    const-string v0, ""

    move-object v4, v0

    goto/16 :goto_0

    .line 2616595
    :cond_b
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_1

    .line 2616596
    :cond_c
    const-string v0, ""

    goto/16 :goto_2

    .line 2616597
    :cond_d
    iget-object v1, p0, LX/Iq2;->deltas:Ljava/util/List;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 2616598
    :cond_e
    iget-object v1, p0, LX/Iq2;->firstDeltaSeqId:Ljava/lang/Long;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 2616599
    :cond_f
    iget-object v1, p0, LX/Iq2;->lastIssuedSeqId:Ljava/lang/Long;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 2616600
    :cond_10
    iget-object v1, p0, LX/Iq2;->queueEntityId:Ljava/lang/Long;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 2616601
    :cond_11
    iget-object v1, p0, LX/Iq2;->syncToken:Ljava/lang/String;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_7

    .line 2616602
    :cond_12
    iget-object v0, p0, LX/Iq2;->errorCode:Ljava/lang/String;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_8

    :cond_13
    move v2, v1

    goto/16 :goto_7
.end method

.method public final a(LX/1su;)V
    .locals 3

    .prologue
    .line 2616655
    invoke-virtual {p1}, LX/1su;->a()V

    .line 2616656
    iget-object v0, p0, LX/Iq2;->deltas:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 2616657
    iget-object v0, p0, LX/Iq2;->deltas:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 2616658
    sget-object v0, LX/Iq2;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2616659
    new-instance v0, LX/1u3;

    const/16 v1, 0xc

    iget-object v2, p0, LX/Iq2;->deltas:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 2616660
    iget-object v0, p0, LX/Iq2;->deltas:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ipt;

    .line 2616661
    invoke-virtual {v0, p1}, LX/6kT;->a(LX/1su;)V

    goto :goto_0

    .line 2616662
    :cond_0
    iget-object v0, p0, LX/Iq2;->firstDeltaSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 2616663
    iget-object v0, p0, LX/Iq2;->firstDeltaSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 2616664
    sget-object v0, LX/Iq2;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2616665
    iget-object v0, p0, LX/Iq2;->firstDeltaSeqId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2616666
    :cond_1
    iget-object v0, p0, LX/Iq2;->lastIssuedSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 2616667
    iget-object v0, p0, LX/Iq2;->lastIssuedSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 2616668
    sget-object v0, LX/Iq2;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2616669
    iget-object v0, p0, LX/Iq2;->lastIssuedSeqId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2616670
    :cond_2
    iget-object v0, p0, LX/Iq2;->queueEntityId:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 2616671
    iget-object v0, p0, LX/Iq2;->queueEntityId:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 2616672
    sget-object v0, LX/Iq2;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2616673
    iget-object v0, p0, LX/Iq2;->queueEntityId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2616674
    :cond_3
    iget-object v0, p0, LX/Iq2;->syncToken:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 2616675
    iget-object v0, p0, LX/Iq2;->syncToken:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 2616676
    sget-object v0, LX/Iq2;->g:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2616677
    iget-object v0, p0, LX/Iq2;->syncToken:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 2616678
    :cond_4
    iget-object v0, p0, LX/Iq2;->errorCode:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 2616679
    iget-object v0, p0, LX/Iq2;->errorCode:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 2616680
    sget-object v0, LX/Iq2;->h:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2616681
    iget-object v0, p0, LX/Iq2;->errorCode:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/1su;->a(Ljava/lang/String;)V

    .line 2616682
    :cond_5
    invoke-virtual {p1}, LX/1su;->c()V

    .line 2616683
    invoke-virtual {p1}, LX/1su;->b()V

    .line 2616684
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2616486
    if-nez p1, :cond_1

    .line 2616487
    :cond_0
    :goto_0
    return v0

    .line 2616488
    :cond_1
    instance-of v1, p1, LX/Iq2;

    if-eqz v1, :cond_0

    .line 2616489
    check-cast p1, LX/Iq2;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2616490
    if-nez p1, :cond_3

    .line 2616491
    :cond_2
    :goto_1
    move v0, v2

    .line 2616492
    goto :goto_0

    .line 2616493
    :cond_3
    iget-object v0, p0, LX/Iq2;->deltas:Ljava/util/List;

    if-eqz v0, :cond_10

    move v0, v1

    .line 2616494
    :goto_2
    iget-object v3, p1, LX/Iq2;->deltas:Ljava/util/List;

    if-eqz v3, :cond_11

    move v3, v1

    .line 2616495
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 2616496
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2616497
    iget-object v0, p0, LX/Iq2;->deltas:Ljava/util/List;

    iget-object v3, p1, LX/Iq2;->deltas:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2616498
    :cond_5
    iget-object v0, p0, LX/Iq2;->firstDeltaSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_12

    move v0, v1

    .line 2616499
    :goto_4
    iget-object v3, p1, LX/Iq2;->firstDeltaSeqId:Ljava/lang/Long;

    if-eqz v3, :cond_13

    move v3, v1

    .line 2616500
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 2616501
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2616502
    iget-object v0, p0, LX/Iq2;->firstDeltaSeqId:Ljava/lang/Long;

    iget-object v3, p1, LX/Iq2;->firstDeltaSeqId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2616503
    :cond_7
    iget-object v0, p0, LX/Iq2;->lastIssuedSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_14

    move v0, v1

    .line 2616504
    :goto_6
    iget-object v3, p1, LX/Iq2;->lastIssuedSeqId:Ljava/lang/Long;

    if-eqz v3, :cond_15

    move v3, v1

    .line 2616505
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 2616506
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2616507
    iget-object v0, p0, LX/Iq2;->lastIssuedSeqId:Ljava/lang/Long;

    iget-object v3, p1, LX/Iq2;->lastIssuedSeqId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2616508
    :cond_9
    iget-object v0, p0, LX/Iq2;->queueEntityId:Ljava/lang/Long;

    if-eqz v0, :cond_16

    move v0, v1

    .line 2616509
    :goto_8
    iget-object v3, p1, LX/Iq2;->queueEntityId:Ljava/lang/Long;

    if-eqz v3, :cond_17

    move v3, v1

    .line 2616510
    :goto_9
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 2616511
    :cond_a
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2616512
    iget-object v0, p0, LX/Iq2;->queueEntityId:Ljava/lang/Long;

    iget-object v3, p1, LX/Iq2;->queueEntityId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2616513
    :cond_b
    iget-object v0, p0, LX/Iq2;->syncToken:Ljava/lang/String;

    if-eqz v0, :cond_18

    move v0, v1

    .line 2616514
    :goto_a
    iget-object v3, p1, LX/Iq2;->syncToken:Ljava/lang/String;

    if-eqz v3, :cond_19

    move v3, v1

    .line 2616515
    :goto_b
    if-nez v0, :cond_c

    if-eqz v3, :cond_d

    .line 2616516
    :cond_c
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2616517
    iget-object v0, p0, LX/Iq2;->syncToken:Ljava/lang/String;

    iget-object v3, p1, LX/Iq2;->syncToken:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2616518
    :cond_d
    iget-object v0, p0, LX/Iq2;->errorCode:Ljava/lang/String;

    if-eqz v0, :cond_1a

    move v0, v1

    .line 2616519
    :goto_c
    iget-object v3, p1, LX/Iq2;->errorCode:Ljava/lang/String;

    if-eqz v3, :cond_1b

    move v3, v1

    .line 2616520
    :goto_d
    if-nez v0, :cond_e

    if-eqz v3, :cond_f

    .line 2616521
    :cond_e
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2616522
    iget-object v0, p0, LX/Iq2;->errorCode:Ljava/lang/String;

    iget-object v3, p1, LX/Iq2;->errorCode:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_f
    move v2, v1

    .line 2616523
    goto/16 :goto_1

    :cond_10
    move v0, v2

    .line 2616524
    goto/16 :goto_2

    :cond_11
    move v3, v2

    .line 2616525
    goto/16 :goto_3

    :cond_12
    move v0, v2

    .line 2616526
    goto/16 :goto_4

    :cond_13
    move v3, v2

    .line 2616527
    goto/16 :goto_5

    :cond_14
    move v0, v2

    .line 2616528
    goto :goto_6

    :cond_15
    move v3, v2

    .line 2616529
    goto :goto_7

    :cond_16
    move v0, v2

    .line 2616530
    goto :goto_8

    :cond_17
    move v3, v2

    .line 2616531
    goto :goto_9

    :cond_18
    move v0, v2

    .line 2616532
    goto :goto_a

    :cond_19
    move v3, v2

    .line 2616533
    goto :goto_b

    :cond_1a
    move v0, v2

    .line 2616534
    goto :goto_c

    :cond_1b
    move v3, v2

    .line 2616535
    goto :goto_d
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2616485
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2616482
    sget-boolean v0, LX/Iq2;->a:Z

    .line 2616483
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/Iq2;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2616484
    return-object v0
.end method
