.class public final LX/Ivv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;I)V
    .locals 0

    .prologue
    .line 2629330
    iput-object p1, p0, LX/Ivv;->b:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    iput p2, p0, LX/Ivv;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2629336
    iget-object v0, p0, LX/Ivv;->b:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    iget-object v0, v0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->P:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0xa0030

    const-string v2, "NNF_PermalinkNotificationLoad"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->f(ILjava/lang/String;)V

    .line 2629337
    iget-object v0, p0, LX/Ivv;->b:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    iget-object v0, v0, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->x:LX/0id;

    invoke-virtual {v0}, LX/0id;->a()V

    .line 2629338
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2629331
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2629332
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 2629333
    :goto_0
    iget-object v1, p0, LX/Ivv;->b:Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;

    iget v2, p0, LX/Ivv;->a:I

    invoke-static {v1, v0, v2}, Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;->a$redex0(Lcom/facebook/notifications/notificationsfriending/NotificationsFriendingFragment;Lcom/facebook/feed/rows/core/props/FeedProps;I)V

    .line 2629334
    return-void

    .line 2629335
    :cond_0
    invoke-static {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    goto :goto_0
.end method
