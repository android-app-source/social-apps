.class public final LX/J2P;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 2641215
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_c

    .line 2641216
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2641217
    :goto_0
    return v1

    .line 2641218
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2641219
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_b

    .line 2641220
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 2641221
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2641222
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_1

    if-eqz v9, :cond_1

    .line 2641223
    const-string v10, "__type__"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_2

    const-string v10, "__typename"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 2641224
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v8

    goto :goto_1

    .line 2641225
    :cond_3
    const-string v10, "actions"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 2641226
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 2641227
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->START_ARRAY:LX/15z;

    if-ne v9, v10, :cond_4

    .line 2641228
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_ARRAY:LX/15z;

    if-eq v9, v10, :cond_4

    .line 2641229
    invoke-static {p0, p1}, LX/J2N;->b(LX/15w;LX/186;)I

    move-result v9

    .line 2641230
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2641231
    :cond_4
    invoke-static {v7, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v7

    move v7, v7

    .line 2641232
    goto :goto_1

    .line 2641233
    :cond_5
    const-string v10, "available_steps"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 2641234
    invoke-static {p0, p1}, LX/J2Z;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 2641235
    :cond_6
    const-string v10, "current_step"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 2641236
    invoke-static {p0, p1}, LX/J2a;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 2641237
    :cond_7
    const-string v10, "extension_type"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 2641238
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto/16 :goto_1

    .line 2641239
    :cond_8
    const-string v10, "images"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 2641240
    invoke-static {p0, p1}, LX/J2T;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 2641241
    :cond_9
    const-string v10, "items"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_a

    .line 2641242
    invoke-static {p0, p1}, LX/J2W;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 2641243
    :cond_a
    const-string v10, "price_list"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 2641244
    invoke-static {p0, p1}, LX/J2Y;->a(LX/15w;LX/186;)I

    move-result v0

    goto/16 :goto_1

    .line 2641245
    :cond_b
    const/16 v9, 0x8

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 2641246
    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 2641247
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 2641248
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 2641249
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 2641250
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 2641251
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 2641252
    const/4 v1, 0x6

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2641253
    const/4 v1, 0x7

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2641254
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_c
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2641255
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2641256
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 2641257
    if-eqz v0, :cond_0

    .line 2641258
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2641259
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2641260
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2641261
    if-eqz v0, :cond_2

    .line 2641262
    const-string v1, "actions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2641263
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2641264
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 2641265
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/J2N;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2641266
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2641267
    :cond_1
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2641268
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2641269
    if-eqz v0, :cond_3

    .line 2641270
    const-string v1, "available_steps"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2641271
    invoke-static {p0, v0, p2, p3}, LX/J2Z;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2641272
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2641273
    if-eqz v0, :cond_4

    .line 2641274
    const-string v1, "current_step"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2641275
    invoke-static {p0, v0, p2}, LX/J2a;->a(LX/15i;ILX/0nX;)V

    .line 2641276
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2641277
    if-eqz v0, :cond_5

    .line 2641278
    const-string v1, "extension_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2641279
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2641280
    :cond_5
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2641281
    if-eqz v0, :cond_6

    .line 2641282
    const-string v1, "images"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2641283
    invoke-static {p0, v0, p2, p3}, LX/J2T;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2641284
    :cond_6
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2641285
    if-eqz v0, :cond_7

    .line 2641286
    const-string v1, "items"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2641287
    invoke-static {p0, v0, p2, p3}, LX/J2W;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2641288
    :cond_7
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2641289
    if-eqz v0, :cond_8

    .line 2641290
    const-string v1, "price_list"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2641291
    invoke-static {p0, v0, p2, p3}, LX/J2Y;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2641292
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2641293
    return-void
.end method
