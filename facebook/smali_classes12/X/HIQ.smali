.class public LX/HIQ;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/3U8;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kp;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/pages/common/reaction/components/HScrollPageCardComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/HIQ",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/pages/common/reaction/components/HScrollPageCardComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2451164
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2451165
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/HIQ;->b:LX/0Zi;

    .line 2451166
    iput-object p1, p0, LX/HIQ;->a:LX/0Ot;

    .line 2451167
    return-void
.end method

.method public static a(LX/0QB;)LX/HIQ;
    .locals 4

    .prologue
    .line 2451153
    const-class v1, LX/HIQ;

    monitor-enter v1

    .line 2451154
    :try_start_0
    sget-object v0, LX/HIQ;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2451155
    sput-object v2, LX/HIQ;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2451156
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2451157
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2451158
    new-instance v3, LX/HIQ;

    const/16 p0, 0x2b91

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/HIQ;-><init>(LX/0Ot;)V

    .line 2451159
    move-object v0, v3

    .line 2451160
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2451161
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/HIQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2451162
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2451163
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 10

    .prologue
    .line 2451126
    check-cast p2, LX/HIP;

    .line 2451127
    iget-object v0, p0, LX/HIQ;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/reaction/components/HScrollPageCardComponentSpec;

    iget-object v2, p2, LX/HIP;->a:LX/2km;

    iget-object v3, p2, LX/HIP;->b:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;

    iget v4, p2, LX/HIP;->c:I

    iget-object v5, p2, LX/HIP;->d:LX/HIT;

    iget-object v6, p2, LX/HIP;->e:LX/HIS;

    iget-object v7, p2, LX/HIP;->f:LX/HIU;

    move-object v1, p1

    .line 2451128
    invoke-static {v3}, Lcom/facebook/pages/common/reaction/components/HScrollPageCardComponentSpec;->a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;)LX/HIR;

    move-result-object v8

    .line 2451129
    const/high16 v9, -0x80000000

    if-ne v4, v9, :cond_0

    .line 2451130
    check-cast v2, LX/1Pn;

    invoke-interface {v2}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v9

    iget-object p0, v0, Lcom/facebook/pages/common/reaction/components/HScrollPageCardComponentSpec;->g:LX/1DR;

    iget-object p1, v0, Lcom/facebook/pages/common/reaction/components/HScrollPageCardComponentSpec;->e:Landroid/content/Context;

    invoke-virtual {p0, p1}, LX/1DR;->a(Landroid/content/Context;)I

    move-result p0

    int-to-float p0, p0

    invoke-static {v9, p0}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v4

    .line 2451131
    :cond_0
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v9

    const p0, 0x7f020a3d

    invoke-interface {v9, p0}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v9

    const/4 p0, 0x0

    invoke-interface {v9, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v9

    invoke-interface {v9, v4}, LX/1Dh;->F(I)LX/1Dh;

    move-result-object v9

    iget-object p0, v0, Lcom/facebook/pages/common/reaction/components/HScrollPageCardComponentSpec;->h:LX/1nu;

    invoke-virtual {p0, v1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object p0

    sget-object p1, Lcom/facebook/pages/common/reaction/components/HScrollPageCardComponentSpec;->d:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p0, p1}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object p0

    iget-object p1, v8, LX/HIR;->h:Landroid/net/Uri;

    invoke-virtual {p0, p1}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object p0

    sget-object p1, LX/1Up;->g:LX/1Up;

    invoke-virtual {p0, p1}, LX/1nw;->a(LX/1Up;)LX/1nw;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    .line 2451132
    const p1, -0x1d6d3da8

    const/4 p2, 0x0

    invoke-static {v1, p1, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p1

    move-object p1, p1

    .line 2451133
    invoke-interface {p0, p1}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object p0

    const p1, 0x7f0b232d

    invoke-interface {p0, p1}, LX/1Di;->q(I)LX/1Di;

    move-result-object p0

    const/4 p1, 0x6

    const p2, 0x7f0b232c

    invoke-interface {p0, p1, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object p0

    invoke-interface {v9, p0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v9

    iget-object p0, v0, Lcom/facebook/pages/common/reaction/components/HScrollPageCardComponentSpec;->i:LX/HIX;

    const/4 p1, 0x0

    .line 2451134
    new-instance p2, LX/HIW;

    invoke-direct {p2, p0}, LX/HIW;-><init>(LX/HIX;)V

    .line 2451135
    iget-object v0, p0, LX/HIX;->b:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HIV;

    .line 2451136
    if-nez v0, :cond_1

    .line 2451137
    new-instance v0, LX/HIV;

    invoke-direct {v0, p0}, LX/HIV;-><init>(LX/HIX;)V

    .line 2451138
    :cond_1
    invoke-static {v0, v1, p1, p1, p2}, LX/HIV;->a$redex0(LX/HIV;LX/1De;IILX/HIW;)V

    .line 2451139
    move-object p2, v0

    .line 2451140
    move-object p1, p2

    .line 2451141
    move-object p0, p1

    .line 2451142
    iget-object p1, p0, LX/HIV;->a:LX/HIW;

    iput-object v5, p1, LX/HIW;->c:LX/HIT;

    .line 2451143
    move-object p0, p0

    .line 2451144
    iget-object p1, p0, LX/HIV;->a:LX/HIW;

    iput-object v6, p1, LX/HIW;->d:LX/HIS;

    .line 2451145
    move-object p0, p0

    .line 2451146
    iget-object p1, p0, LX/HIV;->a:LX/HIW;

    iput-object v7, p1, LX/HIW;->b:LX/HIU;

    .line 2451147
    move-object p0, p0

    .line 2451148
    iget-object p1, p0, LX/HIV;->a:LX/HIW;

    iput-object v8, p1, LX/HIW;->a:LX/HIR;

    .line 2451149
    iget-object p1, p0, LX/HIV;->e:Ljava/util/BitSet;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Ljava/util/BitSet;->set(I)V

    .line 2451150
    move-object v8, p0

    .line 2451151
    invoke-interface {v9, v8}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v8

    invoke-interface {v8}, LX/1Di;->k()LX/1Dg;

    move-result-object v8

    move-object v0, v8

    .line 2451152
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2451114
    invoke-static {}, LX/1dS;->b()V

    .line 2451115
    iget v0, p1, LX/1dQ;->b:I

    .line 2451116
    packed-switch v0, :pswitch_data_0

    .line 2451117
    :goto_0
    return-object v1

    .line 2451118
    :pswitch_0
    iget-object v0, p1, LX/1dQ;->a:LX/1X1;

    .line 2451119
    check-cast v0, LX/HIP;

    .line 2451120
    iget-object v2, p0, LX/HIQ;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/pages/common/reaction/components/HScrollPageCardComponentSpec;

    iget-object v3, v0, LX/HIP;->b:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;

    iget-object v4, v0, LX/HIP;->d:LX/HIT;

    .line 2451121
    invoke-static {v3}, Lcom/facebook/pages/common/reaction/components/HScrollPageCardComponentSpec;->a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;)LX/HIR;

    move-result-object p1

    .line 2451122
    if-eqz v4, :cond_0

    .line 2451123
    invoke-interface {v4, p1}, LX/HIT;->a(LX/HIR;)V

    .line 2451124
    :cond_0
    iget-object p2, v2, Lcom/facebook/pages/common/reaction/components/HScrollPageCardComponentSpec;->f:LX/17W;

    iget-object p0, v2, Lcom/facebook/pages/common/reaction/components/HScrollPageCardComponentSpec;->e:Landroid/content/Context;

    sget-object v0, LX/0ax;->aE:Ljava/lang/String;

    iget-object p1, p1, LX/HIR;->a:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p0, p1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2451125
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x1d6d3da8
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/HIO;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/HIQ",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2451106
    new-instance v1, LX/HIP;

    invoke-direct {v1, p0}, LX/HIP;-><init>(LX/HIQ;)V

    .line 2451107
    iget-object v2, p0, LX/HIQ;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/HIO;

    .line 2451108
    if-nez v2, :cond_0

    .line 2451109
    new-instance v2, LX/HIO;

    invoke-direct {v2, p0}, LX/HIO;-><init>(LX/HIQ;)V

    .line 2451110
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/HIO;->a$redex0(LX/HIO;LX/1De;IILX/HIP;)V

    .line 2451111
    move-object v1, v2

    .line 2451112
    move-object v0, v1

    .line 2451113
    return-object v0
.end method
