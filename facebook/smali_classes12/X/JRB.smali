.class public LX/JRB;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:LX/17W;

.field public final b:LX/23i;

.field public final c:LX/0Zb;

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/3CA;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/17W;LX/23i;LX/0Zb;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/17W;",
            "LX/23i;",
            "LX/0Zb;",
            "LX/0Or",
            "<",
            "LX/3CA;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2692867
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2692868
    iput-object p1, p0, LX/JRB;->a:LX/17W;

    .line 2692869
    iput-object p2, p0, LX/JRB;->b:LX/23i;

    .line 2692870
    iput-object p3, p0, LX/JRB;->c:LX/0Zb;

    .line 2692871
    iput-object p4, p0, LX/JRB;->d:LX/0Or;

    .line 2692872
    return-void
.end method

.method public static a(LX/0QB;)LX/JRB;
    .locals 7

    .prologue
    .line 2692873
    const-class v1, LX/JRB;

    monitor-enter v1

    .line 2692874
    :try_start_0
    sget-object v0, LX/JRB;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2692875
    sput-object v2, LX/JRB;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2692876
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2692877
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2692878
    new-instance v6, LX/JRB;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v3

    check-cast v3, LX/17W;

    invoke-static {v0}, LX/23i;->a(LX/0QB;)LX/23i;

    move-result-object v4

    check-cast v4, LX/23i;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v5

    check-cast v5, LX/0Zb;

    const/16 p0, 0xfa6

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v6, v3, v4, v5, p0}, LX/JRB;-><init>(LX/17W;LX/23i;LX/0Zb;LX/0Or;)V

    .line 2692879
    move-object v0, v6

    .line 2692880
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2692881
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JRB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2692882
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2692883
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
