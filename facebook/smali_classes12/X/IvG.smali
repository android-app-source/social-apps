.class public LX/IvG;
.super Landroid/widget/ArrayAdapter;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "LX/DqP;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:I

.field private static k:LX/0Xm;


# instance fields
.field public b:LX/IvW;

.field private final c:Landroid/content/Context;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/11S;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Lcom/facebook/notifications/lockscreenservice/PushNotificationRenderer;

.field private final f:Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;

.field public final g:LX/0wW;

.field public final h:LX/0wc;

.field public final i:Landroid/view/LayoutInflater;

.field public final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2628128
    const v0, 0x7f0d0091

    sput v0, LX/IvG;->a:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Or;Lcom/facebook/notifications/lockscreenservice/PushNotificationRenderer;Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;LX/0wW;LX/0wc;Landroid/view/LayoutInflater;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "LX/11S;",
            ">;",
            "Lcom/facebook/notifications/lockscreenservice/PushNotificationRenderer;",
            "Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;",
            "LX/0wW;",
            "LX/0wc;",
            "Landroid/view/LayoutInflater;",
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2628257
    const v0, 0x7f030a50

    invoke-direct {p0, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 2628258
    iput-object p1, p0, LX/IvG;->c:Landroid/content/Context;

    .line 2628259
    iput-object p2, p0, LX/IvG;->d:LX/0Or;

    .line 2628260
    iput-object p3, p0, LX/IvG;->e:Lcom/facebook/notifications/lockscreenservice/PushNotificationRenderer;

    .line 2628261
    iput-object p4, p0, LX/IvG;->f:Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;

    .line 2628262
    iput-object p5, p0, LX/IvG;->g:LX/0wW;

    .line 2628263
    iput-object p6, p0, LX/IvG;->h:LX/0wc;

    .line 2628264
    iput-object p7, p0, LX/IvG;->i:Landroid/view/LayoutInflater;

    .line 2628265
    iput-object p8, p0, LX/IvG;->j:LX/0Ot;

    .line 2628266
    return-void
.end method

.method public static a(LX/0QB;)LX/IvG;
    .locals 12

    .prologue
    .line 2628143
    const-class v1, LX/IvG;

    monitor-enter v1

    .line 2628144
    :try_start_0
    sget-object v0, LX/IvG;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2628145
    sput-object v2, LX/IvG;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2628146
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2628147
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2628148
    new-instance v3, LX/IvG;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    const/16 v5, 0x2e4

    invoke-static {v0, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v0}, Lcom/facebook/notifications/lockscreenservice/PushNotificationRenderer;->a(LX/0QB;)Lcom/facebook/notifications/lockscreenservice/PushNotificationRenderer;

    move-result-object v6

    check-cast v6, Lcom/facebook/notifications/lockscreenservice/PushNotificationRenderer;

    invoke-static {v0}, Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;->a(LX/0QB;)Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;

    move-result-object v7

    check-cast v7, Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;

    invoke-static {v0}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v8

    check-cast v8, LX/0wW;

    invoke-static {v0}, LX/0wc;->a(LX/0QB;)LX/0wc;

    move-result-object v9

    check-cast v9, LX/0wc;

    invoke-static {v0}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v10

    check-cast v10, Landroid/view/LayoutInflater;

    const/16 v11, 0x97

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-direct/range {v3 .. v11}, LX/IvG;-><init>(Landroid/content/Context;LX/0Or;Lcom/facebook/notifications/lockscreenservice/PushNotificationRenderer;Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;LX/0wW;LX/0wc;Landroid/view/LayoutInflater;LX/0Ot;)V

    .line 2628149
    move-object v0, v3

    .line 2628150
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2628151
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IvG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2628152
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2628153
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Landroid/view/View;LX/DqP;)V
    .locals 11

    .prologue
    .line 2628154
    sget v0, LX/IvG;->a:I

    invoke-virtual {p1, v0, p2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 2628155
    iget-object v0, p0, LX/IvG;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11S;

    sget-object v1, LX/1lB;->HOUR_MINUTE_STYLE:LX/1lB;

    iget-wide v2, p2, LX/DqP;->a:J

    invoke-interface {v0, v1, v2, v3}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v1

    .line 2628156
    const v0, 0x7f0d1a11

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2628157
    instance-of v0, p2, LX/DqQ;

    if-eqz v0, :cond_1

    .line 2628158
    iget-object v0, p0, LX/IvG;->e:Lcom/facebook/notifications/lockscreenservice/PushNotificationRenderer;

    check-cast p2, LX/DqQ;

    const/4 v5, 0x0

    .line 2628159
    const v1, 0x7f0d1a13

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 2628160
    const v2, 0x7f0d1a16

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2628161
    const v3, 0x7f0d1a12

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 2628162
    const v4, 0x7f0d1a15

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ViewAnimator;

    .line 2628163
    const/4 p0, 0x0

    invoke-virtual {v4, p0}, Landroid/widget/ViewAnimator;->setDisplayedChild(I)V

    .line 2628164
    iget-object v4, v0, Lcom/facebook/notifications/lockscreenservice/PushNotificationRenderer;->k:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, v5, v5, v4, v5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2628165
    const-string v4, ""

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2628166
    iget-object v3, p2, LX/DqQ;->g:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v3, :cond_4

    .line 2628167
    iget-object v3, p2, LX/DqQ;->d:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2628168
    iget-object v1, p2, LX/DqQ;->f:Lcom/facebook/notifications/constants/NotificationType;

    sget-object v3, Lcom/facebook/notifications/constants/NotificationType;->FRIEND:Lcom/facebook/notifications/constants/NotificationType;

    if-ne v1, v3, :cond_3

    iget-object v1, p2, LX/DqQ;->h:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 2628169
    iget-object v1, p2, LX/DqQ;->h:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2628170
    invoke-virtual {v1}, Landroid/net/Uri;->isAbsolute()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2628171
    sget-object v3, Lcom/facebook/notifications/lockscreenservice/PushNotificationRenderer;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v1, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2628172
    :cond_0
    :goto_0
    return-void

    .line 2628173
    :cond_1
    instance-of v0, p2, LX/Ivl;

    if-eqz v0, :cond_0

    .line 2628174
    iget-object v0, p0, LX/IvG;->f:Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;

    check-cast p2, LX/Ivl;

    .line 2628175
    const v1, 0x7f0d1a13

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 2628176
    const v2, 0x7f0d1a16

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2628177
    const v3, 0x7f0d1a12

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 2628178
    const v4, 0x7f0d1a15

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ViewAnimator;

    .line 2628179
    iget-object v7, p2, LX/Ivl;->c:Lcom/facebook/messages/ipc/FrozenNewMessageNotification;

    .line 2628180
    iget-object v5, v7, Lcom/facebook/messages/ipc/FrozenNewMessageNotification;->l:Lcom/facebook/messages/ipc/FrozenGroupMessageInfo;

    move-object v8, v5

    .line 2628181
    if-nez v8, :cond_8

    .line 2628182
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ViewAnimator;->setDisplayedChild(I)V

    .line 2628183
    iget-object v4, v7, Lcom/facebook/messages/ipc/FrozenNewMessageNotification;->c:Ljava/lang/String;

    move-object v4, v4

    .line 2628184
    invoke-static {v4}, Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    sget-object v5, Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v4, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2628185
    :goto_1
    const/4 v2, 0x0

    .line 2628186
    if-eqz v8, :cond_2

    .line 2628187
    iget-object v4, v8, Lcom/facebook/messages/ipc/FrozenGroupMessageInfo;->c:Ljava/lang/String;

    move-object v4, v4

    .line 2628188
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 2628189
    iget-object v2, v8, Lcom/facebook/messages/ipc/FrozenGroupMessageInfo;->c:Ljava/lang/String;

    move-object v2, v2

    .line 2628190
    :cond_2
    iget-object v4, v7, Lcom/facebook/messages/ipc/FrozenNewMessageNotification;->d:Ljava/lang/String;

    move-object v5, v4

    .line 2628191
    iget-object v4, v7, Lcom/facebook/messages/ipc/FrozenNewMessageNotification;->f:Ljava/lang/String;

    move-object v4, v4

    .line 2628192
    iget-object v6, v7, Lcom/facebook/messages/ipc/FrozenNewMessageNotification;->d:Ljava/lang/String;

    move-object v6, v6

    .line 2628193
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_e

    .line 2628194
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_d

    .line 2628195
    const-string v4, "%s: %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v6, v5, v8

    const/4 v6, 0x1

    .line 2628196
    iget-object v8, v7, Lcom/facebook/messages/ipc/FrozenNewMessageNotification;->f:Ljava/lang/String;

    move-object v8, v8

    .line 2628197
    aput-object v8, v5, v6

    invoke-static {v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    move-object p0, v4

    move-object v4, v2

    move-object v2, p0

    .line 2628198
    :goto_2
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 2628199
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2628200
    :goto_3
    iget-object v1, v7, Lcom/facebook/messages/ipc/FrozenNewMessageNotification;->g:Landroid/app/PendingIntent;

    move-object v1, v1

    .line 2628201
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x11

    if-lt v2, v4, :cond_f

    .line 2628202
    invoke-virtual {v1}, Landroid/app/PendingIntent;->getCreatorPackage()Ljava/lang/String;

    move-result-object v2

    .line 2628203
    :goto_4
    move-object v1, v2

    .line 2628204
    iget-object v2, v0, Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 2628205
    iget-object v1, v0, Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;->h:Landroid/graphics/drawable/Drawable;

    .line 2628206
    :goto_5
    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3, v2, v4, v1, v5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2628207
    iget v1, p2, LX/Ivl;->b:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2628208
    goto/16 :goto_0

    .line 2628209
    :cond_3
    sget-object v1, Lcom/facebook/notifications/lockscreenservice/PushNotificationRenderer;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v5, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto/16 :goto_0

    .line 2628210
    :cond_4
    iget-object v3, p2, LX/DqQ;->g:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2628211
    iget-object v4, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v4

    .line 2628212
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2628213
    invoke-static {v3}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v4

    if-eqz v4, :cond_6

    invoke-static {v3}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    if-eqz v4, :cond_6

    invoke-static {v3}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v4

    .line 2628214
    :goto_6
    if-eqz v4, :cond_7

    .line 2628215
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 2628216
    invoke-virtual {v4}, Landroid/net/Uri;->isAbsolute()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 2628217
    sget-object v5, Lcom/facebook/notifications/lockscreenservice/PushNotificationRenderer;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v4, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2628218
    :cond_5
    :goto_7
    iget-object v2, v0, Lcom/facebook/notifications/lockscreenservice/PushNotificationRenderer;->e:LX/3Cm;

    sget-object v4, LX/3DG;->LOCK_SCREEN:LX/3DG;

    invoke-virtual {v2, v3, v4}, LX/3Cm;->b(Lcom/facebook/graphql/model/GraphQLStory;LX/3DG;)Landroid/text/Spannable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_6
    move-object v4, v5

    .line 2628219
    goto :goto_6

    .line 2628220
    :cond_7
    sget-object v4, Lcom/facebook/notifications/lockscreenservice/PushNotificationRenderer;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v5, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_7

    .line 2628221
    :cond_8
    iget-object v5, v8, Lcom/facebook/messages/ipc/FrozenGroupMessageInfo;->d:Ljava/lang/String;

    move-object v5, v5

    .line 2628222
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_9

    .line 2628223
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ViewAnimator;->setDisplayedChild(I)V

    .line 2628224
    iget-object v4, v7, Lcom/facebook/messages/ipc/FrozenNewMessageNotification;->e:Ljava/lang/String;

    move-object v4, v4

    .line 2628225
    iget-object v5, v8, Lcom/facebook/messages/ipc/FrozenGroupMessageInfo;->d:Ljava/lang/String;

    move-object v5, v5

    .line 2628226
    invoke-static {v0, v4, v5}, Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;->a(Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    sget-object v5, Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v4, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto/16 :goto_1

    .line 2628227
    :cond_9
    iget-object v2, v8, Lcom/facebook/messages/ipc/FrozenGroupMessageInfo;->b:LX/0Px;

    move-object v9, v2

    .line 2628228
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v2

    const/4 v5, 0x2

    if-ne v2, v5, :cond_a

    .line 2628229
    const/4 v2, 0x1

    invoke-virtual {v4, v2}, Landroid/widget/ViewAnimator;->setDisplayedChild(I)V

    .line 2628230
    const v2, 0x7f0d1a18

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2628231
    const v4, 0x7f0d1a19

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2628232
    const/4 v5, 0x0

    invoke-virtual {v9, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/messages/ipc/FrozenParticipant;

    .line 2628233
    iget-object v6, v5, Lcom/facebook/messages/ipc/FrozenParticipant;->a:Ljava/lang/String;

    move-object v5, v6

    .line 2628234
    invoke-static {v5}, Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    sget-object v6, Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v5, v6}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2628235
    const/4 v2, 0x1

    invoke-virtual {v9, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messages/ipc/FrozenParticipant;

    .line 2628236
    iget-object v5, v2, Lcom/facebook/messages/ipc/FrozenParticipant;->a:Ljava/lang/String;

    move-object v2, v5

    .line 2628237
    invoke-static {v2}, Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v5, Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v4, v2, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto/16 :goto_1

    .line 2628238
    :cond_a
    const/4 v2, 0x2

    invoke-virtual {v4, v2}, Landroid/widget/ViewAnimator;->setDisplayedChild(I)V

    .line 2628239
    const v2, 0x7f0d1a1b

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2628240
    const v4, 0x7f0d1a1c

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2628241
    const v5, 0x7f0d1a1d

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2628242
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v9, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/messages/ipc/FrozenParticipant;

    .line 2628243
    iget-object v10, v6, Lcom/facebook/messages/ipc/FrozenParticipant;->a:Ljava/lang/String;

    move-object v6, v10

    .line 2628244
    invoke-static {v6}, Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    sget-object v10, Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v6, v10}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2628245
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {v9, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messages/ipc/FrozenParticipant;

    .line 2628246
    iget-object v6, v2, Lcom/facebook/messages/ipc/FrozenParticipant;->a:Ljava/lang/String;

    move-object v2, v6

    .line 2628247
    invoke-static {v2}, Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v6, Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v4, v2, v6}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2628248
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x3

    invoke-virtual {v9, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messages/ipc/FrozenParticipant;

    .line 2628249
    iget-object v4, v2, Lcom/facebook/messages/ipc/FrozenParticipant;->a:Ljava/lang/String;

    move-object v2, v4

    .line 2628250
    invoke-static {v2}, Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v4, Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v5, v2, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto/16 :goto_1

    .line 2628251
    :cond_b
    new-instance v5, Landroid/text/SpannableStringBuilder;

    invoke-direct {v5, v4}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2628252
    iget-object v6, v0, Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;->e:LX/1Uj;

    invoke-virtual {v6}, LX/1Uj;->a()Landroid/text/style/MetricAffectingSpan;

    move-result-object v6

    const/4 v8, 0x0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    const/16 v9, 0x21

    invoke-virtual {v5, v6, v8, v4, v9}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2628253
    const-string v4, "\n"

    invoke-virtual {v5, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2628254
    invoke-virtual {v5, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2628255
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 2628256
    :cond_c
    iget-object v1, v0, Lcom/facebook/notifications/lockscreenservice/MessageNotificationRenderer;->i:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_5

    :cond_d
    move-object p0, v4

    move-object v4, v2

    move-object v2, p0

    goto/16 :goto_2

    :cond_e
    move-object v2, v4

    move-object v4, v5

    goto/16 :goto_2

    :cond_f
    invoke-virtual {v1}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_4
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "LX/DqP;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2628138
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/IvG;->setNotifyOnChange(Z)V

    .line 2628139
    invoke-virtual {p0}, LX/IvG;->clear()V

    .line 2628140
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/IvG;->setNotifyOnChange(Z)V

    .line 2628141
    invoke-virtual {p0, p1}, LX/IvG;->addAll(Ljava/util/Collection;)V

    .line 2628142
    return-void
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p2    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2628129
    if-nez p2, :cond_0

    .line 2628130
    iget-object v0, p0, LX/IvG;->i:Landroid/view/LayoutInflater;

    const v1, 0x7f030a50

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2628131
    new-instance v1, LX/Ivo;

    iget-object v2, p0, LX/IvG;->b:LX/IvW;

    iget-object p2, p0, LX/IvG;->g:LX/0wW;

    iget-object p3, p0, LX/IvG;->h:LX/0wc;

    invoke-direct {v1, v0, v2, p2, p3}, LX/Ivo;-><init>(Landroid/view/View;LX/IvW;LX/0wW;LX/0wc;)V

    .line 2628132
    const/4 v2, 0x1

    .line 2628133
    iput-boolean v2, v1, LX/Ivo;->g:Z

    .line 2628134
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2628135
    move-object p2, v0

    .line 2628136
    :cond_0
    invoke-virtual {p0, p1}, LX/IvG;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DqP;

    invoke-direct {p0, p2, v0}, LX/IvG;->a(Landroid/view/View;LX/DqP;)V

    .line 2628137
    return-object p2
.end method
