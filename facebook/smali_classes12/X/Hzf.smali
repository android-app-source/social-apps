.class public final LX/Hzf;
.super LX/1OX;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;)V
    .locals 0

    .prologue
    .line 2525885
    iput-object p1, p0, LX/Hzf;->a:Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;

    invoke-direct {p0}, LX/1OX;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 4

    .prologue
    .line 2525886
    invoke-super {p0, p1, p2, p3}, LX/1OX;->a(Landroid/support/v7/widget/RecyclerView;II)V

    .line 2525887
    iget-object v0, p0, LX/Hzf;->a:Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->z:LX/2ja;

    iget-object v1, p0, LX/Hzf;->a:Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;

    iget-object v1, v1, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->j:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    .line 2525888
    invoke-static {v0, v2, v3}, LX/2ja;->g(LX/2ja;J)V

    .line 2525889
    iget-object v0, p0, LX/Hzf;->a:Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->B:LX/Hx7;

    sget-object v1, LX/Hx7;->CALENDAR:LX/Hx7;

    if-ne v0, v1, :cond_0

    .line 2525890
    iget-object v0, p0, LX/Hzf;->a:Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;

    .line 2525891
    iget-boolean v1, v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->y:Z

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->p:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getLastVisiblePosition()I

    move-result v1

    add-int/lit8 v1, v1, 0x3

    iget-object v2, v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->r:LX/Hzt;

    invoke-virtual {v2}, LX/1OM;->ij_()I

    move-result v2

    if-le v1, v2, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 2525892
    if-eqz v1, :cond_0

    .line 2525893
    sget-object v1, LX/I05;->LOADING_MORE:LX/I05;

    iput-object v1, v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->x:LX/I05;

    .line 2525894
    iget-object v1, v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->r:LX/Hzt;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/Hzt;->a(Ljava/lang/Boolean;)V

    .line 2525895
    invoke-static {v0}, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->c(Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;)V

    .line 2525896
    :cond_0
    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
