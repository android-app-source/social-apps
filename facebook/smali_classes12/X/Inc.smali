.class public LX/Inc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Inb;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Iog;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/IpP;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/Inb;

.field private d:LX/Io3;

.field private e:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Uh;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Iog;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/IpP;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2610721
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2610722
    iput-object p1, p0, LX/Inc;->a:LX/0Ot;

    .line 2610723
    iput-object p2, p0, LX/Inc;->b:LX/0Ot;

    .line 2610724
    iput-object p3, p0, LX/Inc;->e:LX/0Uh;

    .line 2610725
    return-void
.end method

.method public static a(LX/0QB;)LX/Inc;
    .locals 4

    .prologue
    .line 2610726
    new-instance v1, LX/Inc;

    const/16 v0, 0x28ce

    invoke-static {p0, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v0, 0x28d6

    invoke-static {p0, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    invoke-direct {v1, v2, v3, v0}, LX/Inc;-><init>(LX/0Ot;LX/0Ot;LX/0Uh;)V

    .line 2610727
    move-object v0, v1

    .line 2610728
    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2610729
    iget-object v0, p0, LX/Inc;->c:LX/Inb;

    if-nez v0, :cond_0

    .line 2610730
    :goto_0
    return-void

    .line 2610731
    :cond_0
    iget-object v0, p0, LX/Inc;->c:LX/Inb;

    invoke-interface {v0}, LX/Inb;->a()V

    goto :goto_0
.end method

.method public final a(LX/Io3;)V
    .locals 0

    .prologue
    .line 2610732
    iput-object p1, p0, LX/Inc;->d:LX/Io3;

    .line 2610733
    return-void
.end method

.method public final a(Landroid/os/Bundle;Lcom/facebook/messaging/payment/value/input/MessengerPayData;)V
    .locals 4

    .prologue
    .line 2610734
    const-string v0, "payment_flow_type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/5g0;

    .line 2610735
    sget-object v1, LX/Ina;->a:[I

    invoke-virtual {v0}, LX/5g0;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2610736
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported enterPaymentValueType: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2610737
    :pswitch_0
    iget-object v0, p0, LX/Inc;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Inb;

    iput-object v0, p0, LX/Inc;->c:LX/Inb;

    .line 2610738
    :goto_0
    iget-object v0, p0, LX/Inc;->c:LX/Inb;

    iget-object v1, p0, LX/Inc;->d:LX/Io3;

    invoke-interface {v0, v1}, LX/Inb;->a(LX/Io3;)V

    .line 2610739
    iget-object v0, p0, LX/Inc;->c:LX/Inb;

    invoke-interface {v0, p1, p2}, LX/Inb;->a(Landroid/os/Bundle;Lcom/facebook/messaging/payment/value/input/MessengerPayData;)V

    .line 2610740
    return-void

    .line 2610741
    :pswitch_1
    iget-object v0, p0, LX/Inc;->e:LX/0Uh;

    const/16 v1, 0x22b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2610742
    iget-object v0, p0, LX/Inc;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Inb;

    iput-object v0, p0, LX/Inc;->c:LX/Inb;

    goto :goto_0

    .line 2610743
    :cond_0
    iget-object v0, p0, LX/Inc;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Inb;

    iput-object v0, p0, LX/Inc;->c:LX/Inb;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
