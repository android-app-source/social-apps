.class public final LX/Ive;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0q0;


# instance fields
.field public final synthetic a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V
    .locals 0

    .prologue
    .line 2628468
    iput-object p1, p0, LX/Ive;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;B)V
    .locals 0

    .prologue
    .line 2628469
    invoke-direct {p0, p1}, LX/Ive;-><init>(Lcom/facebook/notifications/lockscreenservice/LockScreenService;)V

    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2628470
    iget-object v0, p0, LX/Ive;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-object v0, v0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->z:Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Ive;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-boolean v0, v0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->ae:Z

    if-eqz v0, :cond_0

    .line 2628471
    iget-object v0, p0, LX/Ive;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-object v0, v0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->z:Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;

    invoke-virtual {v0}, Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;->requestFocus()Z

    .line 2628472
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 2

    .prologue
    .line 2628473
    if-eqz p1, :cond_1

    .line 2628474
    iget-object v0, p0, LX/Ive;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-object v0, v0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->k:LX/IvG;

    const v1, 0x481d25de

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2628475
    :cond_0
    invoke-direct {p0}, LX/Ive;->a()V

    .line 2628476
    :goto_0
    return-void

    .line 2628477
    :cond_1
    iget-object v0, p0, LX/Ive;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    iget-boolean v0, v0, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->al:Z

    if-eqz v0, :cond_0

    .line 2628478
    iget-object v0, p0, LX/Ive;->a:Lcom/facebook/notifications/lockscreenservice/LockScreenService;

    invoke-virtual {v0}, Lcom/facebook/notifications/lockscreenservice/LockScreenService;->stopSelf()V

    goto :goto_0
.end method
