.class public LX/IVV;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pb;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/IVZ;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/IVV",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/IVZ;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2581970
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2581971
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/IVV;->b:LX/0Zi;

    .line 2581972
    iput-object p1, p0, LX/IVV;->a:LX/0Ot;

    .line 2581973
    return-void
.end method

.method public static a(LX/0QB;)LX/IVV;
    .locals 4

    .prologue
    .line 2581974
    const-class v1, LX/IVV;

    monitor-enter v1

    .line 2581975
    :try_start_0
    sget-object v0, LX/IVV;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2581976
    sput-object v2, LX/IVV;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2581977
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2581978
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2581979
    new-instance v3, LX/IVV;

    const/16 p0, 0x242b

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/IVV;-><init>(LX/0Ot;)V

    .line 2581980
    move-object v0, v3

    .line 2581981
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2581982
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IVV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2581983
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2581984
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 13

    .prologue
    .line 2581985
    check-cast p2, LX/IVU;

    .line 2581986
    iget-object v0, p0, LX/IVV;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IVZ;

    iget-object v1, p2, LX/IVU;->a:LX/1Pn;

    iget-object v2, p2, LX/IVU;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2581987
    new-instance v4, LX/IVY;

    invoke-direct {v4, v0, v2}, LX/IVY;-><init>(LX/IVZ;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2581988
    invoke-static {}, LX/3mP;->newBuilder()LX/3mP;

    move-result-object v5

    .line 2581989
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2581990
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/25L;->a(Ljava/lang/String;)LX/25L;

    move-result-object v3

    .line 2581991
    iput-object v3, v5, LX/3mP;->d:LX/25L;

    .line 2581992
    move-object v5, v5

    .line 2581993
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2581994
    check-cast v3, LX/0jW;

    .line 2581995
    iput-object v3, v5, LX/3mP;->e:LX/0jW;

    .line 2581996
    move-object v3, v5

    .line 2581997
    const/16 v5, 0x8

    .line 2581998
    iput v5, v3, LX/3mP;->b:I

    .line 2581999
    move-object v3, v3

    .line 2582000
    iput-object v4, v3, LX/3mP;->g:LX/25K;

    .line 2582001
    move-object v3, v3

    .line 2582002
    invoke-virtual {v3}, LX/3mP;->a()LX/25M;

    move-result-object v3

    .line 2582003
    iget-object v4, v0, LX/IVZ;->b:LX/IVX;

    .line 2582004
    new-instance v6, LX/IVW;

    invoke-static {v4}, LX/IVg;->a(LX/0QB;)LX/IVg;

    move-result-object v7

    check-cast v7, LX/IVg;

    const-class v8, LX/IVS;

    invoke-interface {v4, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/IVS;

    move-object v11, v1

    check-cast v11, LX/1Pn;

    move-object v9, p1

    move-object v10, v2

    move-object v12, v3

    invoke-direct/range {v6 .. v12}, LX/IVW;-><init>(LX/IVg;LX/IVS;Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;LX/25M;)V

    .line 2582005
    move-object v3, v6

    .line 2582006
    iget-object v4, v0, LX/IVZ;->a:LX/3mL;

    invoke-virtual {v4, p1}, LX/3mL;->c(LX/1De;)LX/3ml;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/3ml;->a(LX/3mX;)LX/3ml;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2582007
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2582008
    invoke-static {}, LX/1dS;->b()V

    .line 2582009
    const/4 v0, 0x0

    return-object v0
.end method
