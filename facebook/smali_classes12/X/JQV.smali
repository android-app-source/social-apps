.class public LX/JQV;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JQT;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JQW;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2691825
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/JQV;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JQW;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2691826
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2691827
    iput-object p1, p0, LX/JQV;->b:LX/0Ot;

    .line 2691828
    return-void
.end method

.method public static a(LX/0QB;)LX/JQV;
    .locals 4

    .prologue
    .line 2691829
    const-class v1, LX/JQV;

    monitor-enter v1

    .line 2691830
    :try_start_0
    sget-object v0, LX/JQV;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2691831
    sput-object v2, LX/JQV;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2691832
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2691833
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2691834
    new-instance v3, LX/JQV;

    const/16 p0, 0x2004

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JQV;-><init>(LX/0Ot;)V

    .line 2691835
    move-object v0, v3

    .line 2691836
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2691837
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JQV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2691838
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2691839
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 2

    .prologue
    .line 2691840
    iget-object v0, p0, LX/JQV;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    const/4 p2, 0x1

    .line 2691841
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v1

    const p0, 0x7f082991

    invoke-virtual {v1, p0}, LX/1ne;->h(I)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v1

    const p0, 0x7f0b004e

    invoke-virtual {v1, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v1

    const p0, 0x7f0a043b

    invoke-virtual {v1, p0}, LX/1ne;->n(I)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/1ne;->t(I)LX/1ne;

    move-result-object v1

    sget-object p0, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v1, p0}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/high16 p0, 0x3f800000    # 1.0f

    invoke-interface {v1, p0}, LX/1Di;->a(F)LX/1Di;

    move-result-object v1

    const/16 p0, 0x8

    const/4 p2, 0x4

    invoke-interface {v1, p0, p2}, LX/1Di;->d(II)LX/1Di;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    .line 2691842
    const v1, 0x66db88ed

    const/4 p0, 0x0

    invoke-static {p1, v1, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v1

    move-object v1, v1

    .line 2691843
    invoke-interface {v0, v1}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    move-object v0, v0

    .line 2691844
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2691845
    invoke-static {}, LX/1dS;->b()V

    .line 2691846
    iget v0, p1, LX/1dQ;->b:I

    .line 2691847
    packed-switch v0, :pswitch_data_0

    .line 2691848
    :goto_0
    return-object v2

    .line 2691849
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2691850
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    .line 2691851
    iget-object p1, p0, LX/JQV;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/JQW;

    .line 2691852
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    .line 2691853
    new-instance p0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "job_carousel_seemore_click"

    invoke-direct {p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "waterfall_session_id"

    sget-object v0, LX/17Q;->a:Ljava/lang/String;

    invoke-virtual {p0, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    move-object p0, p0

    .line 2691854
    iget-object v1, p1, LX/JQW;->b:LX/0Zb;

    invoke-interface {v1, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2691855
    sget-object p0, LX/0ax;->iU:Ljava/lang/String;

    sget-object v1, LX/17Q;->a:Ljava/lang/String;

    invoke-static {p0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    .line 2691856
    iget-object v1, p1, LX/JQW;->a:LX/17W;

    invoke-virtual {v1, p2, p0}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2691857
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x66db88ed
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/JQT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2691858
    new-instance v1, LX/JQU;

    invoke-direct {v1, p0}, LX/JQU;-><init>(LX/JQV;)V

    .line 2691859
    sget-object v2, LX/JQV;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/JQT;

    .line 2691860
    if-nez v2, :cond_0

    .line 2691861
    new-instance v2, LX/JQT;

    invoke-direct {v2}, LX/JQT;-><init>()V

    .line 2691862
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/JQT;->a$redex0(LX/JQT;LX/1De;IILX/JQU;)V

    .line 2691863
    move-object v1, v2

    .line 2691864
    move-object v0, v1

    .line 2691865
    return-object v0
.end method
