.class public LX/IkR;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/Duh;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/Duh;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2606748
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2606749
    iput-object p2, p0, LX/IkR;->a:LX/Duh;

    .line 2606750
    const v0, 0x7f082c4c

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/IkR;->b:Ljava/lang/String;

    .line 2606751
    const v0, 0x7f082c4b

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/IkR;->c:Ljava/lang/String;

    .line 2606752
    return-void
.end method

.method public static a(LX/0QB;)LX/IkR;
    .locals 3

    .prologue
    .line 2606753
    const-class v1, LX/IkR;

    monitor-enter v1

    .line 2606754
    :try_start_0
    sget-object v0, LX/IkR;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2606755
    sput-object v2, LX/IkR;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2606756
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2606757
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/IkR;->b(LX/0QB;)LX/IkR;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2606758
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IkR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2606759
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2606760
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/IkR;
    .locals 3

    .prologue
    .line 2606761
    new-instance v2, LX/IkR;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-static {p0}, LX/Duh;->a(LX/0QB;)LX/Duh;

    move-result-object v1

    check-cast v1, LX/Duh;

    invoke-direct {v2, v0, v1}, LX/IkR;-><init>(Landroid/content/res/Resources;LX/Duh;)V

    .line 2606762
    return-object v2
.end method
