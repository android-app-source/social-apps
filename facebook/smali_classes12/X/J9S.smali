.class public LX/J9S;
.super LX/B0V;
.source ""


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:LX/J90;

.field private final c:LX/J9L;


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/J90;LX/J9L;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2652801
    invoke-direct {p0}, LX/B0V;-><init>()V

    .line 2652802
    iput-object p1, p0, LX/J9S;->a:Ljava/lang/String;

    .line 2652803
    iput-object p2, p0, LX/J9S;->b:LX/J90;

    .line 2652804
    iput-object p3, p0, LX/J9S;->c:LX/J9L;

    .line 2652805
    return-void
.end method


# virtual methods
.method public final a(LX/B0d;)LX/0zO;
    .locals 4

    .prologue
    .line 2652806
    iget-object v0, p0, LX/J9S;->c:LX/J9L;

    invoke-virtual {v0}, LX/J9L;->h()I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    .line 2652807
    iget-object v1, p0, LX/J9S;->b:LX/J90;

    iget-object v2, p0, LX/J9S;->a:Ljava/lang/String;

    iget-object v3, p1, LX/B0d;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v0}, LX/J90;->a(Ljava/lang/String;Ljava/lang/String;I)LX/JBh;

    move-result-object v0

    .line 2652808
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/B0d;Lcom/facebook/graphql/executor/GraphQLResult;)LX/B0N;
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 2652809
    iget-object v0, p2, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v0

    .line 2652810
    check-cast v1, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;

    .line 2652811
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->l()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2652812
    :cond_0
    const-string v0, "CollectionTailConnectionConfiguration"

    const-string v1, "Missing items connection"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2652813
    new-instance v0, LX/B0e;

    invoke-direct {v0, p1}, LX/B0e;-><init>(LX/B0d;)V

    .line 2652814
    :goto_0
    return-object v0

    .line 2652815
    :cond_1
    new-instance v2, LX/9JH;

    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->l()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;->d()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-direct {v2, v0}, LX/9JH;-><init>(I)V

    .line 2652816
    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->l()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;->d()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v5, v4

    :goto_1
    if-ge v5, v7, :cond_3

    invoke-virtual {v6, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;

    .line 2652817
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v8

    invoke-static {v0}, LX/0w5;->a(Lcom/facebook/flatbuffers/Flattenable;)LX/0w5;

    move-result-object v9

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->d()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_2

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    :goto_2
    invoke-virtual {v2, v8, v9, v0, v4}, LX/9JH;->a(ILX/0w5;Ljava/util/Collection;I)V

    .line 2652818
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_1

    :cond_2
    move-object v0, v3

    .line 2652819
    goto :goto_2

    .line 2652820
    :cond_3
    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->l()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    .line 2652821
    if-nez v0, :cond_4

    .line 2652822
    const-string v0, "CollectionTailConnectionConfiguration"

    const-string v5, "Missing page info"

    invoke-static {v0, v5}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v5, v4

    move-object v4, v3

    .line 2652823
    :goto_3
    new-instance v0, LX/B0i;

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, LX/B0i;-><init>(Lcom/facebook/flatbuffers/MutableFlattenable;LX/9JH;LX/B0d;Ljava/lang/String;Z)V

    goto :goto_0

    .line 2652824
    :cond_4
    invoke-interface {v0}, LX/0us;->a()Ljava/lang/String;

    move-result-object v3

    .line 2652825
    invoke-interface {v0}, LX/0us;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x1

    :goto_4
    move v5, v0

    move-object v4, v3

    goto :goto_3

    :cond_5
    move v0, v4

    goto :goto_4
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2652826
    const-string v0, "CollectionsCollectionTail"

    return-object v0
.end method
