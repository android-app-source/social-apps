.class public final LX/JJU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/JJX;


# direct methods
.method public constructor <init>(LX/JJX;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2679553
    iput-object p1, p0, LX/JJU;->b:LX/JJX;

    iput-object p2, p0, LX/JJU;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0xaa14912

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2679554
    iget-object v1, p0, LX/JJU;->b:LX/JJX;

    iget-object v1, v1, LX/76U;->a:LX/78A;

    invoke-virtual {v1}, LX/78A;->c()V

    .line 2679555
    iget-object v1, p0, LX/JJU;->b:LX/JJX;

    iget-object v2, p0, LX/JJU;->a:Ljava/lang/String;

    .line 2679556
    iget-object v4, v1, LX/JJX;->f:LX/JJS;

    invoke-virtual {v4, v2}, LX/JJS;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 2679557
    new-instance v5, LX/JJT;

    invoke-direct {v5, v1, v2}, LX/JJT;-><init>(LX/JJX;Ljava/lang/String;)V

    iget-object p1, v1, LX/JJX;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v4, v5, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2679558
    iget-object v1, p0, LX/JJU;->b:LX/JJX;

    iget-object v1, v1, LX/JJX;->e:LX/JJR;

    .line 2679559
    iget-object v4, v1, LX/JJR;->f:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    const v5, 0x3e4ccccd    # 0.2f

    invoke-virtual {v4, v5}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    const-wide/16 v6, 0xc8

    invoke-virtual {v4, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    new-instance v5, LX/JJN;

    invoke-direct {v5, v1}, LX/JJN;-><init>(LX/JJR;)V

    invoke-virtual {v4, v5}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 2679560
    const v1, 0x5283ac1f

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
