.class public LX/HRe;
.super LX/1Qj;
.source ""

# interfaces
.implements LX/2kk;


# instance fields
.field private final n:LX/Amz;

.field private final o:LX/1PT;

.field private final p:LX/1ro;

.field private final q:LX/HRU;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1PT;LX/Amz;Ljava/lang/Runnable;LX/1PY;LX/1ro;LX/HRU;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/1PT;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/Amz;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Runnable;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/1PY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2465695
    invoke-direct {p0, p1, p4, p5}, LX/1Qj;-><init>(Landroid/content/Context;Ljava/lang/Runnable;LX/1PY;)V

    .line 2465696
    iput-object p2, p0, LX/HRe;->o:LX/1PT;

    .line 2465697
    iput-object p3, p0, LX/HRe;->n:LX/Amz;

    .line 2465698
    iget-object v0, p0, LX/HRe;->n:LX/Amz;

    if-eqz v0, :cond_0

    .line 2465699
    iget-object v0, p0, LX/HRe;->n:LX/Amz;

    invoke-virtual {v0, p0}, LX/1SX;->a(LX/1Pf;)V

    .line 2465700
    :cond_0
    iput-object p6, p0, LX/HRe;->p:LX/1ro;

    .line 2465701
    iput-object p7, p0, LX/HRe;->q:LX/HRU;

    .line 2465702
    return-void
.end method


# virtual methods
.method public final a(LX/2nq;)V
    .locals 2

    .prologue
    .line 2465703
    if-eqz p1, :cond_0

    invoke-interface {p1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2465704
    iget-object v0, p0, LX/HRe;->p:LX/1ro;

    invoke-interface {p1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1ro;->a(Ljava/lang/String;)V

    .line 2465705
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/9uc;)V
    .locals 0

    .prologue
    .line 2465706
    return-void
.end method

.method public final a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2465707
    return-void
.end method

.method public final a(LX/2nq;Z)Z
    .locals 1

    .prologue
    .line 2465708
    const/4 v0, 0x0

    return v0
.end method

.method public final b(LX/2nq;)Landroid/view/View$OnClickListener;
    .locals 9

    .prologue
    .line 2465709
    iget-object v0, p0, LX/HRe;->q:LX/HRU;

    .line 2465710
    new-instance v1, LX/HRT;

    move-object v3, p0

    check-cast v3, LX/1Pq;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v4

    check-cast v4, LX/17W;

    invoke-static {v0}, LX/2Yg;->a(LX/0QB;)LX/2Yg;

    move-result-object v5

    check-cast v5, LX/2Yg;

    invoke-static {v0}, LX/3Cm;->a(LX/0QB;)LX/3Cm;

    move-result-object v6

    check-cast v6, LX/3Cm;

    invoke-static {v0}, LX/3DA;->a(LX/0QB;)LX/3DA;

    move-result-object v7

    check-cast v7, LX/3DA;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    move-object v2, p1

    invoke-direct/range {v1 .. v8}, LX/HRT;-><init>(LX/2nq;LX/1Pq;LX/17W;LX/2Yg;LX/3Cm;LX/3DA;LX/03V;)V

    .line 2465711
    move-object v0, v1

    .line 2465712
    return-object v0
.end method

.method public final c()LX/1PT;
    .locals 1

    .prologue
    .line 2465713
    iget-object v0, p0, LX/HRe;->o:LX/1PT;

    return-object v0
.end method

.method public final e()LX/1SX;
    .locals 1

    .prologue
    .line 2465714
    iget-object v0, p0, LX/HRe;->n:LX/Amz;

    return-object v0
.end method

.method public final e_(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 2465715
    const/4 v0, 0x0

    return v0
.end method
