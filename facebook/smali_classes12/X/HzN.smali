.class public final LX/HzN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/HzM;


# instance fields
.field public final synthetic a:Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;)V
    .locals 0

    .prologue
    .line 2525514
    iput-object p1, p0, LX/HzN;->a:Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/Hx7;Z)V
    .locals 3

    .prologue
    .line 2525497
    if-eqz p2, :cond_1

    .line 2525498
    iget-object v0, p0, LX/HzN;->a:Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;

    .line 2525499
    invoke-static {v0, p1}, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->b$redex0(Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;LX/Hx7;)V

    .line 2525500
    iget-object v0, p0, LX/HzN;->a:Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;

    .line 2525501
    const/4 v1, 0x0

    .line 2525502
    sget-object v2, LX/Hx7;->DISCOVER:LX/Hx7;

    if-ne p1, v2, :cond_2

    .line 2525503
    sget-object v1, Lcom/facebook/events/common/ActionMechanism;->HOME_TAB:Lcom/facebook/events/common/ActionMechanism;

    .line 2525504
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 2525505
    iget-object v2, v0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->y:LX/1nQ;

    iget-object p0, v0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->l:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object p0, p0, Lcom/facebook/events/common/EventAnalyticsParams;->d:Ljava/lang/String;

    .line 2525506
    iget-object p2, v2, LX/1nQ;->i:LX/0Zb;

    const-string v0, "tap_on_tabs_bar"

    const/4 p1, 0x0

    invoke-interface {p2, v0, p1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object p2

    .line 2525507
    invoke-virtual {p2}, LX/0oG;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2525508
    const-string v0, "event_dashboard"

    invoke-virtual {p2, v0}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object p2

    iget-object v0, v2, LX/1nQ;->j:LX/0kv;

    iget-object p1, v2, LX/1nQ;->g:Landroid/content/Context;

    invoke-virtual {v0, p1}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    move-result-object p2

    const-string v0, "source_module"

    invoke-virtual {p2, v0, p0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object p2

    const-string v0, "mechanism"

    invoke-virtual {v1}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, v0, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object p2

    invoke-virtual {p2}, LX/0oG;->d()V

    .line 2525509
    :cond_1
    return-void

    .line 2525510
    :cond_2
    sget-object v2, LX/Hx7;->CALENDAR:LX/Hx7;

    if-ne p1, v2, :cond_3

    .line 2525511
    sget-object v1, Lcom/facebook/events/common/ActionMechanism;->CALENDAR_TAB:Lcom/facebook/events/common/ActionMechanism;

    goto :goto_0

    .line 2525512
    :cond_3
    sget-object v2, LX/Hx7;->HOSTING:LX/Hx7;

    if-ne p1, v2, :cond_0

    .line 2525513
    sget-object v1, Lcom/facebook/events/common/ActionMechanism;->HOSTING_TAB:Lcom/facebook/events/common/ActionMechanism;

    goto :goto_0
.end method
