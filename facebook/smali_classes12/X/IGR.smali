.class public final LX/IGR;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/location/ImmutableLocation;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;

.field public final synthetic b:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;)V
    .locals 0

    .prologue
    .line 2556010
    iput-object p1, p0, LX/IGR;->b:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    iput-object p2, p0, LX/IGR;->a:Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2556011
    iget-object v0, p0, LX/IGR;->b:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->x:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x30000a

    const-string v2, "FriendsNearbyPingWrite"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->f(ILjava/lang/String;)V

    .line 2556012
    sget-object v0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->m:Ljava/lang/Class;

    const-string v1, "Could not get location for ping"

    invoke-static {v0, v1, p1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2556013
    iget-object v0, p0, LX/IGR;->b:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->w:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f083887

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2556014
    iget-object v0, p0, LX/IGR;->b:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    invoke-static {v0}, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->m$redex0(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;)V

    .line 2556015
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2556016
    check-cast p1, Lcom/facebook/location/ImmutableLocation;

    .line 2556017
    iget-object v0, p0, LX/IGR;->b:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->x:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x30000a

    const-string v2, "FriendsNearbyPingWrite"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 2556018
    iget-object v0, p0, LX/IGR;->b:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    new-instance v1, Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;

    iget-object v2, p0, LX/IGR;->a:Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;

    iget-object v2, v2, Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;->a:Ljava/lang/String;

    iget-object v3, p0, LX/IGR;->a:Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;

    iget-object v3, v3, Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;->b:Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;

    invoke-static {p1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v4

    iget-object v5, p0, LX/IGR;->a:Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;

    iget-object v5, v5, Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;->d:LX/0am;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;-><init>(Ljava/lang/String;Lcom/facebook/friendsnearby/pingdialog/LocationPingTime;LX/0am;LX/0am;)V

    invoke-static {v0, v1}, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->b$redex0(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;)V

    .line 2556019
    return-void
.end method
