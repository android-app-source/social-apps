.class public LX/HL4;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kp;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/HL5;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/HL4",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/HL5;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2455491
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2455492
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/HL4;->b:LX/0Zi;

    .line 2455493
    iput-object p1, p0, LX/HL4;->a:LX/0Ot;

    .line 2455494
    return-void
.end method

.method public static a(LX/0QB;)LX/HL4;
    .locals 4

    .prologue
    .line 2455495
    const-class v1, LX/HL4;

    monitor-enter v1

    .line 2455496
    :try_start_0
    sget-object v0, LX/HL4;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2455497
    sput-object v2, LX/HL4;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2455498
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2455499
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2455500
    new-instance v3, LX/HL4;

    const/16 p0, 0x2bdf

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/HL4;-><init>(LX/0Ot;)V

    .line 2455501
    move-object v0, v3

    .line 2455502
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2455503
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/HL4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2455504
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2455505
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2455506
    const v0, -0x7c2de191

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 7

    .prologue
    .line 2455507
    check-cast p2, LX/HL3;

    .line 2455508
    iget-object v0, p0, LX/HL4;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HL5;

    iget-object v1, p2, LX/HL3;->a:Ljava/lang/String;

    .line 2455509
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0a0097

    invoke-interface {v2, v3}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x2

    invoke-interface {v2, v3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    const/4 p2, 0x1

    .line 2455510
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0836a9

    new-array v6, p2, [Ljava/lang/Object;

    const/4 p0, 0x0

    aput-object v1, v6, p0

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    const v4, 0x7f0b0050

    invoke-virtual {v3, v4}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    const v4, 0x7f0a0265

    invoke-virtual {v3, v4}, LX/1ne;->n(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, p2}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-interface {v3, v4}, LX/1Di;->a(F)LX/1Di;

    move-result-object v3

    move-object v3, v3

    .line 2455511
    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    .line 2455512
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v3

    iget-object v4, v0, LX/HL5;->c:LX/1vg;

    invoke-virtual {v4, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v4

    const v5, 0x7f020ee4

    invoke-virtual {v4, v5}, LX/2xv;->h(I)LX/2xv;

    move-result-object v4

    const v5, 0x7f0a0265

    invoke-virtual {v4, v5}, LX/2xv;->j(I)LX/2xv;

    move-result-object v4

    invoke-virtual {v4}, LX/1n6;->b()LX/1dc;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/4 v4, 0x3

    invoke-interface {v3, v4}, LX/1Di;->b(I)LX/1Di;

    move-result-object v3

    const v4, 0x7f0b1631

    invoke-interface {v3, v4}, LX/1Di;->i(I)LX/1Di;

    move-result-object v3

    const v4, 0x7f0b1631

    invoke-interface {v3, v4}, LX/1Di;->q(I)LX/1Di;

    move-result-object v3

    move-object v3, v3

    .line 2455513
    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    .line 2455514
    const v3, -0x7c2de191

    const/4 v4, 0x0

    invoke-static {p1, v3, v4}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v3

    move-object v3, v3

    .line 2455515
    invoke-interface {v2, v3}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v2

    const/16 v3, 0x8

    const v4, 0x7f0b163c

    invoke-interface {v2, v3, v4}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v2

    .line 2455516
    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2455517
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2455518
    invoke-static {}, LX/1dS;->b()V

    .line 2455519
    iget v0, p1, LX/1dQ;->b:I

    .line 2455520
    packed-switch v0, :pswitch_data_0

    .line 2455521
    :goto_0
    return-object v1

    .line 2455522
    :pswitch_0
    iget-object v0, p1, LX/1dQ;->a:LX/1X1;

    .line 2455523
    check-cast v0, LX/HL3;

    .line 2455524
    iget-object v2, p0, LX/HL4;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/HL5;

    iget-object p1, v0, LX/HL3;->b:Ljava/lang/String;

    iget-object p2, v0, LX/HL3;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-virtual {v2, p1, p2}, LX/HL5;->onClick(Ljava/lang/String;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V

    .line 2455525
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x7c2de191
        :pswitch_0
    .end packed-switch
.end method
