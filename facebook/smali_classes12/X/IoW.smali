.class public LX/IoW;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Uh;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Or;LX/0Uh;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/payments/p2p/config/IsP2pPaymentsRequestEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2611897
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2611898
    iput-object p1, p0, LX/IoW;->a:Landroid/content/Context;

    .line 2611899
    iput-object p2, p0, LX/IoW;->b:LX/0Or;

    .line 2611900
    iput-object p3, p0, LX/IoW;->c:LX/0Uh;

    .line 2611901
    return-void
.end method

.method public static a(LX/0QB;)LX/IoW;
    .locals 4

    .prologue
    .line 2611902
    new-instance v2, LX/IoW;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const/16 v1, 0x1546

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v1

    check-cast v1, LX/0Uh;

    invoke-direct {v2, v0, v3, v1}, LX/IoW;-><init>(Landroid/content/Context;LX/0Or;LX/0Uh;)V

    .line 2611903
    move-object v0, v2

    .line 2611904
    return-object v0
.end method

.method public static a(LX/5g0;Lcom/facebook/messaging/payment/value/input/MessengerPayData;)LX/Ios;
    .locals 3

    .prologue
    .line 2611905
    sget-object v0, LX/IoV;->a:[I

    invoke-virtual {p0}, LX/5g0;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2611906
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported PaymentFlowType: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2611907
    :pswitch_0
    new-instance v0, LX/Iot;

    invoke-direct {v0}, LX/Iot;-><init>()V

    move-object v0, v0

    .line 2611908
    iget-object v1, p1, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->b:LX/IoT;

    move-object v1, v1

    .line 2611909
    iput-object v1, v0, LX/Iot;->a:LX/IoT;

    .line 2611910
    move-object v0, v0

    .line 2611911
    iget-object v1, p1, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->f:LX/0am;

    move-object v1, v1

    .line 2611912
    iput-object v1, v0, LX/Iot;->b:LX/0am;

    .line 2611913
    move-object v0, v0

    .line 2611914
    iget-object v1, p1, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->q:LX/03R;

    move-object v1, v1

    .line 2611915
    iput-object v1, v0, LX/Iot;->e:LX/03R;

    .line 2611916
    move-object v0, v0

    .line 2611917
    iget-object v1, p1, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->o:Lcom/facebook/user/model/Name;

    move-object v1, v1

    .line 2611918
    iput-object v1, v0, LX/Iot;->c:Lcom/facebook/user/model/Name;

    .line 2611919
    move-object v0, v0

    .line 2611920
    iget-object v1, p1, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->p:Lcom/facebook/user/model/UserKey;

    move-object v1, v1

    .line 2611921
    iput-object v1, v0, LX/Iot;->d:Lcom/facebook/user/model/UserKey;

    .line 2611922
    move-object v0, v0

    .line 2611923
    iget-object v1, p1, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->s:Ljava/lang/String;

    move-object v1, v1

    .line 2611924
    iput-object v1, v0, LX/Iot;->g:Ljava/lang/String;

    .line 2611925
    move-object v0, v0

    .line 2611926
    iget-object v1, p1, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->m:Ljava/util/List;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    move-object v1, v1

    .line 2611927
    iput-object v1, v0, LX/Iot;->h:Ljava/util/List;

    .line 2611928
    move-object v0, v0

    .line 2611929
    iget-object v1, p1, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->n:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;

    move-object v1, v1

    .line 2611930
    iput-object v1, v0, LX/Iot;->i:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;

    .line 2611931
    move-object v0, v0

    .line 2611932
    iget-object v1, p1, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->r:Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    move-object v1, v1

    .line 2611933
    iput-object v1, v0, LX/Iot;->f:Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    .line 2611934
    move-object v0, v0

    .line 2611935
    new-instance v1, LX/Ios;

    invoke-direct {v1, v0}, LX/Ios;-><init>(LX/Iot;)V

    move-object v0, v1

    .line 2611936
    return-object v0

    :cond_0
    iget-object v1, p1, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->m:Ljava/util/List;

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/5g0;)LX/IoU;
    .locals 3

    .prologue
    .line 2611937
    sget-object v0, LX/IoV;->a:[I

    invoke-virtual {p1}, LX/5g0;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2611938
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported enterPaymentValueType: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2611939
    :pswitch_0
    new-instance v0, LX/Ior;

    iget-object v1, p0, LX/IoW;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/Ior;-><init>(Landroid/content/Context;)V

    .line 2611940
    :goto_0
    return-object v0

    .line 2611941
    :pswitch_1
    iget-object v0, p0, LX/IoW;->c:LX/0Uh;

    const/16 v1, 0x22b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2611942
    new-instance v0, LX/Ip7;

    iget-object v1, p0, LX/IoW;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/Ip7;-><init>(Landroid/content/Context;)V

    .line 2611943
    sget-object v1, LX/5g0;->SEND:LX/5g0;

    invoke-virtual {v0, v1}, LX/Ip7;->setPaymentFlowType(LX/5g0;)V

    .line 2611944
    :goto_1
    move-object v0, v0

    .line 2611945
    goto :goto_0

    .line 2611946
    :pswitch_2
    iget-object v0, p0, LX/IoW;->c:LX/0Uh;

    const/16 v1, 0x22b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2611947
    new-instance v0, LX/Ip7;

    iget-object v1, p0, LX/IoW;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/Ip7;-><init>(Landroid/content/Context;)V

    .line 2611948
    sget-object v1, LX/5g0;->GROUP_COMMERCE_REQUEST:LX/5g0;

    invoke-virtual {v0, v1}, LX/Ip7;->setPaymentFlowType(LX/5g0;)V

    .line 2611949
    :goto_2
    move-object v0, v0

    .line 2611950
    goto :goto_0

    .line 2611951
    :pswitch_3
    iget-object v0, p0, LX/IoW;->c:LX/0Uh;

    const/16 v1, 0x22b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2611952
    new-instance v0, LX/Ip7;

    iget-object v1, p0, LX/IoW;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/Ip7;-><init>(Landroid/content/Context;)V

    .line 2611953
    sget-object v1, LX/5g0;->REQUEST:LX/5g0;

    invoke-virtual {v0, v1}, LX/Ip7;->setPaymentFlowType(LX/5g0;)V

    .line 2611954
    :goto_3
    move-object v0, v0

    .line 2611955
    goto :goto_0

    .line 2611956
    :pswitch_4
    new-instance v0, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;

    iget-object v1, p0, LX/IoW;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/facebook/messaging/payment/value/input/OrionRequestAckMessengerPayView;-><init>(Landroid/content/Context;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, LX/IoW;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;

    iget-object v1, p0, LX/IoW;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/facebook/messaging/payment/value/input/OrionPayMessengerPayView;-><init>(Landroid/content/Context;)V

    :goto_4
    check-cast v0, LX/IoU;

    goto :goto_1

    :cond_1
    new-instance v0, LX/Ior;

    iget-object v1, p0, LX/IoW;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/Ior;-><init>(Landroid/content/Context;)V

    goto :goto_4

    :cond_2
    new-instance v0, LX/IpY;

    iget-object v1, p0, LX/IoW;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/IpY;-><init>(Landroid/content/Context;)V

    goto :goto_2

    :cond_3
    new-instance v0, LX/IpY;

    iget-object v1, p0, LX/IoW;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/IpY;-><init>(Landroid/content/Context;)V

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
