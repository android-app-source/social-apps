.class public final LX/IHQ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2560171
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_5

    .line 2560172
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2560173
    :goto_0
    return v1

    .line 2560174
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_3

    .line 2560175
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2560176
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2560177
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v5, :cond_0

    .line 2560178
    const-string v6, "count_nearby_friends_sharing_location"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2560179
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v4, v0

    move v0, v2

    goto :goto_1

    .line 2560180
    :cond_1
    const-string v6, "friends_sharing_location_connection"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2560181
    invoke-static {p0, p1}, LX/IHP;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 2560182
    :cond_2
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2560183
    :cond_3
    const/4 v5, 0x2

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2560184
    if-eqz v0, :cond_4

    .line 2560185
    invoke-virtual {p1, v1, v4, v1}, LX/186;->a(III)V

    .line 2560186
    :cond_4
    invoke-virtual {p1, v2, v3}, LX/186;->b(II)V

    .line 2560187
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v3, v1

    move v4, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2560188
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2560189
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v0

    .line 2560190
    if-eqz v0, :cond_0

    .line 2560191
    const-string v1, "count_nearby_friends_sharing_location"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2560192
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2560193
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2560194
    if-eqz v0, :cond_1

    .line 2560195
    const-string v1, "friends_sharing_location_connection"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2560196
    invoke-static {p0, v0, p2, p3}, LX/IHP;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2560197
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2560198
    return-void
.end method
