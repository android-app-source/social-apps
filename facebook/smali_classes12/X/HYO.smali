.class public LX/HYO;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/HYO;


# instance fields
.field public final a:Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetcher;

.field public final b:LX/HYL;

.field public final c:LX/3Cm;

.field public final d:LX/3DB;

.field private final e:LX/0wq;

.field public final f:LX/19j;

.field public final g:LX/1b4;


# direct methods
.method public constructor <init>(Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetcher;LX/HYL;LX/3Cm;LX/3DB;LX/0wq;LX/19j;LX/1b4;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2480593
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2480594
    iput-object p1, p0, LX/HYO;->a:Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetcher;

    .line 2480595
    iput-object p2, p0, LX/HYO;->b:LX/HYL;

    .line 2480596
    iput-object p3, p0, LX/HYO;->c:LX/3Cm;

    .line 2480597
    iput-object p4, p0, LX/HYO;->d:LX/3DB;

    .line 2480598
    iput-object p5, p0, LX/HYO;->e:LX/0wq;

    .line 2480599
    iput-object p6, p0, LX/HYO;->f:LX/19j;

    .line 2480600
    iput-object p7, p0, LX/HYO;->g:LX/1b4;

    .line 2480601
    return-void
.end method

.method public static a(LX/0QB;)LX/HYO;
    .locals 11

    .prologue
    .line 2480602
    sget-object v0, LX/HYO;->h:LX/HYO;

    if-nez v0, :cond_1

    .line 2480603
    const-class v1, LX/HYO;

    monitor-enter v1

    .line 2480604
    :try_start_0
    sget-object v0, LX/HYO;->h:LX/HYO;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2480605
    if-eqz v2, :cond_0

    .line 2480606
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2480607
    new-instance v3, LX/HYO;

    invoke-static {v0}, Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetcher;->a(LX/0QB;)Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetcher;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetcher;

    invoke-static {v0}, LX/HYL;->a(LX/0QB;)LX/HYL;

    move-result-object v5

    check-cast v5, LX/HYL;

    invoke-static {v0}, LX/3Cm;->a(LX/0QB;)LX/3Cm;

    move-result-object v6

    check-cast v6, LX/3Cm;

    invoke-static {v0}, LX/3DB;->a(LX/0QB;)LX/3DB;

    move-result-object v7

    check-cast v7, LX/3DB;

    invoke-static {v0}, LX/0wq;->b(LX/0QB;)LX/0wq;

    move-result-object v8

    check-cast v8, LX/0wq;

    invoke-static {v0}, LX/19j;->a(LX/0QB;)LX/19j;

    move-result-object v9

    check-cast v9, LX/19j;

    invoke-static {v0}, LX/1b4;->b(LX/0QB;)LX/1b4;

    move-result-object v10

    check-cast v10, LX/1b4;

    invoke-direct/range {v3 .. v10}, LX/HYO;-><init>(Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetcher;LX/HYL;LX/3Cm;LX/3DB;LX/0wq;LX/19j;LX/1b4;)V

    .line 2480608
    move-object v0, v3

    .line 2480609
    sput-object v0, LX/HYO;->h:LX/HYO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2480610
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2480611
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2480612
    :cond_1
    sget-object v0, LX/HYO;->h:LX/HYO;

    return-object v0

    .line 2480613
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2480614
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
