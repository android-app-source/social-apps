.class public final LX/JLh;
.super LX/8KR;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/8KR",
        "<",
        "Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/JLi;


# direct methods
.method public constructor <init>(LX/JLi;)V
    .locals 0

    .prologue
    .line 2681427
    iput-object p1, p0, LX/JLh;->a:LX/JLi;

    invoke-direct {p0}, LX/8KR;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2681428
    const-class v0, Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 5

    .prologue
    .line 2681429
    check-cast p1, Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;

    .line 2681430
    iget-object v0, p0, LX/JLh;->a:LX/JLi;

    iget-object v0, v0, LX/JLi;->a:LX/JLU;

    .line 2681431
    iget-object v1, p1, Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2681432
    new-instance v2, Lcom/facebook/react/bridge/WritableNativeMap;

    invoke-direct {v2}, Lcom/facebook/react/bridge/WritableNativeMap;-><init>()V

    .line 2681433
    const-string v3, "legacyApiStoryID"

    invoke-virtual {v2, v3, v1}, Lcom/facebook/react/bridge/WritableNativeMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2681434
    iget-object v1, v0, LX/JLU;->a:Lcom/facebook/react/bridge/Callback;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    invoke-interface {v1, v3}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    .line 2681435
    iget-object v0, p0, LX/JLh;->a:LX/JLi;

    invoke-static {v0}, LX/JLi;->a$redex0(LX/JLi;)V

    .line 2681436
    return-void
.end method
