.class public abstract enum LX/IoL;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IoL;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IoL;

.field public static final enum CONFIRMED:LX/IoL;

.field public static final enum DISABLED:LX/IoL;

.field public static final enum HIDDEN:LX/IoL;

.field public static final enum NORMAL:LX/IoL;

.field public static final enum SELECTED:LX/IoL;


# instance fields
.field public mAlpha:F

.field public mButtonColorResId:I
    .annotation build Landroid/support/annotation/ColorRes;
    .end annotation
.end field

.field public mLayoutWeight:F

.field public mShowProgressBar:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 16

    .prologue
    const/4 v15, 0x2

    const/4 v3, 0x0

    const/4 v7, 0x1

    const/4 v2, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    .line 2611780
    new-instance v0, LX/IoM;

    const-string v1, "DISABLED"

    const v5, 0x7f0a019a

    move v6, v2

    invoke-direct/range {v0 .. v6}, LX/IoM;-><init>(Ljava/lang/String;IFFIZ)V

    sput-object v0, LX/IoL;->DISABLED:LX/IoL;

    .line 2611781
    new-instance v5, LX/IoN;

    const-string v6, "HIDDEN"

    const v10, 0x7f0a019a

    move v8, v3

    move v9, v4

    move v11, v2

    invoke-direct/range {v5 .. v11}, LX/IoN;-><init>(Ljava/lang/String;IFFIZ)V

    sput-object v5, LX/IoL;->HIDDEN:LX/IoL;

    .line 2611782
    new-instance v8, LX/IoO;

    const-string v9, "NORMAL"

    const/high16 v11, 0x3f000000    # 0.5f

    const v13, 0x7f0a019a

    move v10, v15

    move v12, v4

    move v14, v2

    invoke-direct/range {v8 .. v14}, LX/IoO;-><init>(Ljava/lang/String;IFFIZ)V

    sput-object v8, LX/IoL;->NORMAL:LX/IoL;

    .line 2611783
    new-instance v8, LX/IoP;

    const-string v9, "SELECTED"

    const/4 v10, 0x3

    const v13, 0x7f0a080f

    move v11, v4

    move v12, v4

    move v14, v2

    invoke-direct/range {v8 .. v14}, LX/IoP;-><init>(Ljava/lang/String;IFFIZ)V

    sput-object v8, LX/IoL;->SELECTED:LX/IoL;

    .line 2611784
    new-instance v8, LX/IoQ;

    const-string v9, "CONFIRMED"

    const/4 v10, 0x4

    const v12, 0x3f19999a    # 0.6f

    const v13, 0x7f0a080f

    move v11, v4

    move v14, v7

    invoke-direct/range {v8 .. v14}, LX/IoQ;-><init>(Ljava/lang/String;IFFIZ)V

    sput-object v8, LX/IoL;->CONFIRMED:LX/IoL;

    .line 2611785
    const/4 v0, 0x5

    new-array v0, v0, [LX/IoL;

    sget-object v1, LX/IoL;->DISABLED:LX/IoL;

    aput-object v1, v0, v2

    sget-object v1, LX/IoL;->HIDDEN:LX/IoL;

    aput-object v1, v0, v7

    sget-object v1, LX/IoL;->NORMAL:LX/IoL;

    aput-object v1, v0, v15

    const/4 v1, 0x3

    sget-object v2, LX/IoL;->SELECTED:LX/IoL;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, LX/IoL;->CONFIRMED:LX/IoL;

    aput-object v2, v0, v1

    sput-object v0, LX/IoL;->$VALUES:[LX/IoL;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IFFIZ)V
    .locals 0
    .param p5    # I
        .annotation build Landroid/support/annotation/ColorRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FFIZ)V"
        }
    .end annotation

    .prologue
    .line 2611774
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2611775
    iput p3, p0, LX/IoL;->mLayoutWeight:F

    .line 2611776
    iput p4, p0, LX/IoL;->mAlpha:F

    .line 2611777
    iput p5, p0, LX/IoL;->mButtonColorResId:I

    .line 2611778
    iput-boolean p6, p0, LX/IoL;->mShowProgressBar:Z

    .line 2611779
    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;IFFIZLX/IoI;)V
    .locals 0

    .prologue
    .line 2611769
    invoke-direct/range {p0 .. p6}, LX/IoL;-><init>(Ljava/lang/String;IFFIZ)V

    return-void
.end method

.method public static synthetic access$100(LX/IoL;)I
    .locals 1

    .prologue
    .line 2611773
    iget v0, p0, LX/IoL;->mButtonColorResId:I

    return v0
.end method

.method public static synthetic access$200(LX/IoL;)F
    .locals 1

    .prologue
    .line 2611772
    iget v0, p0, LX/IoL;->mAlpha:F

    return v0
.end method

.method public static synthetic access$300(LX/IoL;)Z
    .locals 1

    .prologue
    .line 2611786
    iget-boolean v0, p0, LX/IoL;->mShowProgressBar:Z

    return v0
.end method

.method public static synthetic access$400(LX/IoL;)F
    .locals 1

    .prologue
    .line 2611771
    iget v0, p0, LX/IoL;->mLayoutWeight:F

    return v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/IoL;
    .locals 1

    .prologue
    .line 2611770
    const-class v0, LX/IoL;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IoL;

    return-object v0
.end method

.method public static values()[LX/IoL;
    .locals 1

    .prologue
    .line 2611768
    sget-object v0, LX/IoL;->$VALUES:[LX/IoL;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IoL;

    return-object v0
.end method


# virtual methods
.method public abstract getButtonText(Landroid/content/res/Resources;LX/IoK;Ljava/lang/String;)Ljava/lang/String;
.end method
