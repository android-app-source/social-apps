.class public LX/HVq;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/9XE;

.field private final b:Landroid/content/Context;

.field private final c:LX/EAc;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/EAc;LX/9XE;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2475419
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2475420
    iput-object p1, p0, LX/HVq;->b:Landroid/content/Context;

    .line 2475421
    iput-object p2, p0, LX/HVq;->c:LX/EAc;

    .line 2475422
    iput-object p3, p0, LX/HVq;->a:LX/9XE;

    .line 2475423
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;)V
    .locals 8

    .prologue
    .line 2475424
    iget-object v0, p0, LX/HVq;->a:LX/9XE;

    sget-object v1, LX/9XI;->EVENT_TAPPED_REVIEWS_CONTEXT_ITEM:LX/9XI;

    iget-wide v2, p3, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->a:J

    invoke-virtual {v0, v1, v2, v3}, LX/9XE;->a(LX/9X2;J)V

    .line 2475425
    iget-object v1, p0, LX/HVq;->c:LX/EAc;

    iget-wide v2, p3, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->a:J

    const/4 v4, 0x0

    iget-object v5, p3, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->e:Ljava/lang/String;

    iget-object v6, p3, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->e:Ljava/lang/String;

    iget-object v7, p0, LX/HVq;->b:Landroid/content/Context;

    invoke-virtual/range {v1 .. v7}, LX/EAc;->a(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 2475426
    return-void
.end method
