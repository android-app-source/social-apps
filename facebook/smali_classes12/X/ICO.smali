.class public LX/ICO;
.super LX/0ht;
.source ""

# interfaces
.implements LX/02k;


# instance fields
.field public a:LX/0Uh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public l:LX/ICK;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 2549345
    invoke-direct {p0, p1}, LX/0ht;-><init>(Landroid/content/Context;)V

    .line 2549346
    const-class v0, LX/ICO;

    invoke-static {v0, p0}, LX/ICO;->a(Ljava/lang/Class;LX/02k;)V

    .line 2549347
    const/4 p1, 0x0

    .line 2549348
    invoke-virtual {p0}, LX/0ht;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0313d8

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2549349
    invoke-virtual {p0, v1}, LX/0ht;->d(Landroid/view/View;)V

    .line 2549350
    const v0, 0x7f0d2dcc

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 2549351
    if-eqz v0, :cond_0

    .line 2549352
    iget-object v2, p0, LX/ICO;->a:LX/0Uh;

    const/16 v3, 0x320

    invoke-virtual {v2, v3, p1}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2549353
    const v2, 0x7f083914

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 2549354
    :cond_0
    :goto_0
    const v0, 0x7f0d2dcd

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2549355
    new-instance v1, LX/ICN;

    invoke-direct {v1, p0}, LX/ICN;-><init>(LX/ICO;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2549356
    return-void

    .line 2549357
    :cond_1
    const v2, 0x7f083913

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/ICO;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object p0

    check-cast p0, LX/0Uh;

    iput-object p0, p1, LX/ICO;->a:LX/0Uh;

    return-void
.end method
