.class public LX/I4T;
.super LX/CnT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CnT",
        "<",
        "Lcom/facebook/events/eventcollections/view/impl/block/EventCollectionHeaderBlockView;",
        "Lcom/facebook/events/eventcollections/model/data/EventCollectionHeaderBlockData;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/facebook/events/eventcollections/view/impl/EventCollectionHeaderBlockViewImpl;)V
    .locals 0

    .prologue
    .line 2534111
    invoke-direct {p0, p1}, LX/CnT;-><init>(LX/CnG;)V

    .line 2534112
    return-void
.end method


# virtual methods
.method public final a(LX/Clr;)V
    .locals 5

    .prologue
    .line 2534113
    check-cast p1, LX/I4M;

    .line 2534114
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2534115
    check-cast v0, Lcom/facebook/events/eventcollections/view/impl/EventCollectionHeaderBlockViewImpl;

    .line 2534116
    iget-object v1, p1, LX/I4M;->a:LX/8Z4;

    if-eqz v1, :cond_0

    iget-object v1, p1, LX/I4M;->a:LX/8Z4;

    invoke-interface {v1}, LX/8Z4;->d()Ljava/lang/String;

    move-result-object v1

    :goto_0
    move-object v1, v1

    .line 2534117
    iget-object v2, p1, LX/I4M;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2534118
    iget-object v3, p1, LX/I4M;->c:LX/8Yr;

    if-eqz v3, :cond_1

    iget-object v3, p1, LX/I4M;->c:LX/8Yr;

    invoke-interface {v3}, LX/8Yr;->eg_()LX/1Fb;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v3, p1, LX/I4M;->c:LX/8Yr;

    invoke-interface {v3}, LX/8Yr;->eg_()LX/1Fb;

    move-result-object v3

    invoke-interface {v3}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v3

    :goto_1
    move-object v3, v3

    .line 2534119
    iget-object v4, v0, Lcom/facebook/events/eventcollections/view/impl/EventCollectionHeaderBlockViewImpl;->b:Landroid/widget/TextView;

    invoke-static {v4, v1}, Lcom/facebook/events/eventcollections/view/impl/EventCollectionHeaderBlockViewImpl;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 2534120
    iget-object v4, v0, Lcom/facebook/events/eventcollections/view/impl/EventCollectionHeaderBlockViewImpl;->c:Landroid/widget/TextView;

    invoke-static {v4, v2}, Lcom/facebook/events/eventcollections/view/impl/EventCollectionHeaderBlockViewImpl;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 2534121
    iget-object p0, v0, Lcom/facebook/events/eventcollections/view/impl/EventCollectionHeaderBlockViewImpl;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz v3, :cond_2

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    :goto_2
    sget-object p1, Lcom/facebook/events/eventcollections/view/impl/EventCollectionHeaderBlockViewImpl;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p0, v4, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2534122
    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    goto :goto_1

    .line 2534123
    :cond_2
    const/4 v4, 0x0

    goto :goto_2
.end method
