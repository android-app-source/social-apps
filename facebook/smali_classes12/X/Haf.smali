.class public final LX/Haf;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/Hag;


# direct methods
.method public constructor <init>(LX/Hag;)V
    .locals 0

    .prologue
    .line 2484258
    iput-object p1, p0, LX/Haf;->a:LX/Hag;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(ID)V
    .locals 4

    .prologue
    .line 2484259
    const-wide v0, 0x3feccccccccccccdL    # 0.9

    cmpl-double v0, p2, v0

    if-nez v0, :cond_1

    .line 2484260
    iget-object v0, p0, LX/Haf;->a:LX/Hag;

    iget-object v1, p0, LX/Haf;->a:LX/Hag;

    iget v1, v1, LX/Hag;->h:I

    invoke-static {v0, v1}, LX/Hag;->a(LX/Hag;I)LX/HbN;

    move-result-object v1

    .line 2484261
    iget-object v0, p0, LX/Haf;->a:LX/Hag;

    invoke-static {v0, p1}, LX/Hag;->a(LX/Hag;I)LX/HbN;

    move-result-object v0

    .line 2484262
    instance-of v2, v0, LX/HbY;

    if-eqz v2, :cond_0

    .line 2484263
    check-cast v0, LX/HbY;

    invoke-virtual {v0}, LX/HbY;->y()V

    .line 2484264
    :cond_0
    instance-of v0, v1, LX/HbY;

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 2484265
    check-cast v0, LX/HbY;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/HbY;->b(Z)V

    .line 2484266
    :cond_1
    return-void
.end method

.method public final b(ID)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 2484267
    const-wide v0, 0x3fa999999999999aL    # 0.05

    cmpl-double v0, p2, v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/Haf;->a:LX/Hag;

    iget-object v0, v0, LX/Hag;->d:LX/HbJ;

    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    if-ne p1, v0, :cond_0

    .line 2484268
    iget-object v0, p0, LX/Haf;->a:LX/Hag;

    const v1, 0x7f0a00fb

    .line 2484269
    packed-switch v3, :pswitch_data_0

    .line 2484270
    :goto_0
    iget-object v0, p0, LX/Haf;->a:LX/Hag;

    iget-object v1, p0, LX/Haf;->a:LX/Hag;

    iget-object v1, v1, LX/Hag;->d:LX/HbJ;

    invoke-virtual {v1}, LX/1OM;->ij_()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, LX/Hag;->a(LX/Hag;I)LX/HbN;

    move-result-object v0

    .line 2484271
    instance-of v1, v0, LX/HbO;

    if-eqz v1, :cond_0

    .line 2484272
    check-cast v0, LX/HbO;

    invoke-virtual {v0}, LX/HbO;->x()V

    .line 2484273
    :cond_0
    const-wide v0, 0x3fb999999999999aL    # 0.1

    cmpl-double v0, p2, v0

    if-nez v0, :cond_2

    .line 2484274
    iget-object v0, p0, LX/Haf;->a:LX/Hag;

    iget-object v1, p0, LX/Haf;->a:LX/Hag;

    iget v1, v1, LX/Hag;->h:I

    invoke-static {v0, v1}, LX/Hag;->a(LX/Hag;I)LX/HbN;

    move-result-object v1

    .line 2484275
    iget-object v0, p0, LX/Haf;->a:LX/Hag;

    invoke-static {v0, p1}, LX/Hag;->a(LX/Hag;I)LX/HbN;

    move-result-object v0

    .line 2484276
    instance-of v2, v0, LX/HbY;

    if-eqz v2, :cond_1

    .line 2484277
    check-cast v0, LX/HbY;

    invoke-virtual {v0}, LX/HbY;->y()V

    .line 2484278
    :cond_1
    instance-of v0, v1, LX/HbY;

    if-eqz v0, :cond_2

    move-object v0, v1

    .line 2484279
    check-cast v0, LX/HbY;

    invoke-virtual {v0, v3}, LX/HbY;->b(Z)V

    .line 2484280
    :cond_2
    return-void

    .line 2484281
    :pswitch_0
    iget-object v2, v0, LX/Hag;->f:Lcom/facebook/fbui/glyph/GlyphButton;

    iget-object v4, v0, LX/Hag;->b:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v2, v4}, Lcom/facebook/fbui/glyph/GlyphButton;->setGlyphColor(I)V

    goto :goto_0

    .line 2484282
    :pswitch_1
    iget-object v2, v0, LX/Hag;->f:Lcom/facebook/fbui/glyph/GlyphButton;

    iget-object v4, v0, LX/Hag;->b:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/facebook/fbui/glyph/GlyphButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2484283
    iget-object v2, v0, LX/Hag;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 2484284
    invoke-static {v2, v3}, LX/Hbr;->a(Landroid/util/DisplayMetrics;I)I

    move-result v2

    .line 2484285
    iget-object v4, v0, LX/Hag;->f:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v4, v2, v2, v2, v2}, Lcom/facebook/fbui/glyph/GlyphButton;->setPadding(IIII)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final c(ID)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2484286
    const-wide v0, 0x3fa999999999999aL    # 0.05

    cmpl-double v0, p2, v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/Haf;->a:LX/Hag;

    iget-object v0, v0, LX/Hag;->d:LX/HbJ;

    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    if-ne p1, v0, :cond_0

    .line 2484287
    iget-object v0, p0, LX/Haf;->a:LX/Hag;

    const v1, 0x7f0a00d5

    .line 2484288
    packed-switch v2, :pswitch_data_0

    .line 2484289
    :goto_0
    iget-object v0, p0, LX/Haf;->a:LX/Hag;

    iget-object v1, p0, LX/Haf;->a:LX/Hag;

    iget-object v1, v1, LX/Hag;->d:LX/HbJ;

    invoke-virtual {v1}, LX/1OM;->ij_()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, LX/Hag;->a(LX/Hag;I)LX/HbN;

    move-result-object v0

    .line 2484290
    instance-of v1, v0, LX/HbO;

    if-eqz v1, :cond_0

    .line 2484291
    iget-object v1, p0, LX/Haf;->a:LX/Hag;

    iget-object v1, v1, LX/Hag;->k:LX/HaZ;

    const-string v2, "FINALE_SHOWN"

    invoke-virtual {v1, v2}, LX/HaZ;->a(Ljava/lang/String;)V

    .line 2484292
    check-cast v0, LX/HbO;

    .line 2484293
    const/4 v1, 0x1

    invoke-static {v0, v1}, LX/HbO;->c(LX/HbO;I)V

    .line 2484294
    :cond_0
    return-void

    .line 2484295
    :pswitch_0
    iget-object v3, v0, LX/Hag;->f:Lcom/facebook/fbui/glyph/GlyphButton;

    iget-object p1, v0, LX/Hag;->b:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    invoke-virtual {v3, p1}, Lcom/facebook/fbui/glyph/GlyphButton;->setGlyphColor(I)V

    goto :goto_0

    .line 2484296
    :pswitch_1
    iget-object v3, v0, LX/Hag;->f:Lcom/facebook/fbui/glyph/GlyphButton;

    iget-object p1, v0, LX/Hag;->b:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-virtual {v3, p1}, Lcom/facebook/fbui/glyph/GlyphButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2484297
    iget-object v3, v0, LX/Hag;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    .line 2484298
    invoke-static {v3, v2}, LX/Hbr;->a(Landroid/util/DisplayMetrics;I)I

    move-result v3

    .line 2484299
    iget-object p1, v0, LX/Hag;->f:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {p1, v3, v3, v3, v3}, Lcom/facebook/fbui/glyph/GlyphButton;->setPadding(IIII)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
