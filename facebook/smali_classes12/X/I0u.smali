.class public final enum LX/I0u;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/I0u;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/I0u;

.field public static final enum LOADING_COMPLETE:LX/I0u;

.field public static final enum LOADING_FUTURE:LX/I0u;

.field public static final enum LOADING_PAST:LX/I0u;

.field public static final enum LOADING_PAST_AND_FUTURE:LX/I0u;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2528132
    new-instance v0, LX/I0u;

    const-string v1, "LOADING_PAST"

    invoke-direct {v0, v1, v2}, LX/I0u;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I0u;->LOADING_PAST:LX/I0u;

    .line 2528133
    new-instance v0, LX/I0u;

    const-string v1, "LOADING_FUTURE"

    invoke-direct {v0, v1, v3}, LX/I0u;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I0u;->LOADING_FUTURE:LX/I0u;

    .line 2528134
    new-instance v0, LX/I0u;

    const-string v1, "LOADING_PAST_AND_FUTURE"

    invoke-direct {v0, v1, v4}, LX/I0u;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I0u;->LOADING_PAST_AND_FUTURE:LX/I0u;

    .line 2528135
    new-instance v0, LX/I0u;

    const-string v1, "LOADING_COMPLETE"

    invoke-direct {v0, v1, v5}, LX/I0u;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I0u;->LOADING_COMPLETE:LX/I0u;

    .line 2528136
    const/4 v0, 0x4

    new-array v0, v0, [LX/I0u;

    sget-object v1, LX/I0u;->LOADING_PAST:LX/I0u;

    aput-object v1, v0, v2

    sget-object v1, LX/I0u;->LOADING_FUTURE:LX/I0u;

    aput-object v1, v0, v3

    sget-object v1, LX/I0u;->LOADING_PAST_AND_FUTURE:LX/I0u;

    aput-object v1, v0, v4

    sget-object v1, LX/I0u;->LOADING_COMPLETE:LX/I0u;

    aput-object v1, v0, v5

    sput-object v0, LX/I0u;->$VALUES:[LX/I0u;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2528138
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/I0u;
    .locals 1

    .prologue
    .line 2528139
    const-class v0, LX/I0u;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/I0u;

    return-object v0
.end method

.method public static values()[LX/I0u;
    .locals 1

    .prologue
    .line 2528137
    sget-object v0, LX/I0u;->$VALUES:[LX/I0u;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/I0u;

    return-object v0
.end method
