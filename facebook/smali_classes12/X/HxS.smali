.class public LX/HxS;
.super LX/98h;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/98h",
        "<",
        "Ljava/lang/Object;",
        "LX/Hzd;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Object;

.field private static volatile g:LX/HxS;


# instance fields
.field public final b:LX/0TD;

.field public final c:LX/Bky;

.field private final d:LX/0Uh;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Hze;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2522580
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/HxS;->a:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0TD;LX/Bky;LX/0Uh;LX/0Or;LX/0ad;)V
    .locals 0
    .param p1    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TD;",
            "LX/Bky;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Or",
            "<",
            "LX/Hze;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2522573
    invoke-direct {p0}, LX/98h;-><init>()V

    .line 2522574
    iput-object p1, p0, LX/HxS;->b:LX/0TD;

    .line 2522575
    iput-object p2, p0, LX/HxS;->c:LX/Bky;

    .line 2522576
    iput-object p3, p0, LX/HxS;->d:LX/0Uh;

    .line 2522577
    iput-object p4, p0, LX/HxS;->e:LX/0Or;

    .line 2522578
    iput-object p5, p0, LX/HxS;->f:LX/0ad;

    .line 2522579
    return-void
.end method

.method public static a(LX/0QB;)LX/HxS;
    .locals 9

    .prologue
    .line 2522560
    sget-object v0, LX/HxS;->g:LX/HxS;

    if-nez v0, :cond_1

    .line 2522561
    const-class v1, LX/HxS;

    monitor-enter v1

    .line 2522562
    :try_start_0
    sget-object v0, LX/HxS;->g:LX/HxS;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2522563
    if-eqz v2, :cond_0

    .line 2522564
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2522565
    new-instance v3, LX/HxS;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, LX/0TD;

    invoke-static {v0}, LX/Bky;->b(LX/0QB;)LX/Bky;

    move-result-object v5

    check-cast v5, LX/Bky;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v6

    check-cast v6, LX/0Uh;

    const/16 v7, 0x1b05

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-direct/range {v3 .. v8}, LX/HxS;-><init>(LX/0TD;LX/Bky;LX/0Uh;LX/0Or;LX/0ad;)V

    .line 2522566
    move-object v0, v3

    .line 2522567
    sput-object v0, LX/HxS;->g:LX/HxS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2522568
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2522569
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2522570
    :cond_1
    sget-object v0, LX/HxS;->g:LX/HxS;

    return-object v0

    .line 2522571
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2522572
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/content/Intent;)LX/98g;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/content/Intent;",
            ")",
            "LX/98g",
            "<",
            "Ljava/lang/Object;",
            "LX/Hzd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2522538
    const-string v0, "extra_dashboard_filter_type"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2522539
    if-eqz v0, :cond_0

    sget-object v1, LX/Hx6;->UPCOMING:LX/Hx6;

    invoke-virtual {v1}, LX/Hx6;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2522540
    if-nez v0, :cond_1

    .line 2522541
    const/4 v0, 0x0

    .line 2522542
    :goto_1
    return-object v0

    .line 2522543
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0f74

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2522544
    new-instance v1, LX/98g;

    sget-object v3, LX/HxS;->a:Ljava/lang/Object;

    iget-object v0, p0, LX/HxS;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hze;

    .line 2522545
    iget-object v4, p0, LX/HxS;->f:LX/0ad;

    sget-short v5, LX/347;->o:S

    const/4 p2, 0x0

    invoke-interface {v4, v5, p2}, LX/0ad;->a(SZ)Z

    move-result v4

    if-nez v4, :cond_3

    .line 2522546
    const/4 v4, 0x0

    .line 2522547
    :goto_2
    move-object v4, v4

    .line 2522548
    iget-object v5, v0, LX/Hze;->a:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v11

    .line 2522549
    invoke-static {v0, v2}, LX/Hze;->a$redex0(LX/Hze;I)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v10

    .line 2522550
    new-instance v5, LX/Hzd;

    iget-object v6, v0, LX/Hze;->f:LX/0TD;

    move-object v7, v4

    move-object v8, v0

    move v9, v2

    invoke-direct/range {v5 .. v12}, LX/Hzd;-><init>(LX/0TD;Lcom/google/common/util/concurrent/ListenableFuture;LX/Hze;ILcom/google/common/util/concurrent/ListenableFuture;J)V

    move-object v0, v5

    .line 2522551
    invoke-direct {v1, v3, v0}, LX/98g;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v0, v1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    iget-object v4, p0, LX/HxS;->b:LX/0TD;

    new-instance v5, LX/HxR;

    invoke-direct {v5, p0, p1}, LX/HxR;-><init>(LX/HxS;Landroid/content/Context;)V

    invoke-interface {v4, v5}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    goto :goto_2
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2522553
    check-cast p1, LX/Hzd;

    .line 2522554
    if-eqz p1, :cond_1

    .line 2522555
    iget-object v0, p1, LX/Hzd;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 2522556
    iget-object v0, p1, LX/Hzd;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, Lcom/facebook/events/dashboard/EventsPager$EventsPagerEarlyFetchFutureHolder$1;

    invoke-direct {v1, p1}, Lcom/facebook/events/dashboard/EventsPager$EventsPagerEarlyFetchFutureHolder$1;-><init>(LX/Hzd;)V

    iget-object p0, p1, LX/Hzd;->f:LX/0TD;

    invoke-interface {v0, v1, p0}, Lcom/google/common/util/concurrent/ListenableFuture;->addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 2522557
    :cond_0
    iget-object v0, p1, LX/Hzd;->d:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_1

    .line 2522558
    iget-object v0, p1, LX/Hzd;->d:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2522559
    :cond_1
    return-void
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 2522552
    iget-object v0, p0, LX/HxS;->d:LX/0Uh;

    const/16 v1, 0x39f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method
