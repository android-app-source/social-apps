.class public LX/Hq4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0iK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0iz;",
        ":",
        "LX/0j0;",
        ":",
        "LX/0jF;",
        "DerivedData::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesIsCustomPublishModeSupported;",
        "Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;>",
        "Ljava/lang/Object;",
        "LX/0iK",
        "<TModelData;TDerivedData;>;"
    }
.end annotation


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/actionitem/ActionItemController;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/Hq3;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Hq3",
            "<TModelData;TServices;>;"
        }
    .end annotation
.end field

.field private final d:LX/0gd;

.field public e:LX/62V;

.field public f:Lcom/facebook/widget/recyclerview/BetterRecyclerView;


# direct methods
.method public constructor <init>(LX/0il;LX/HrE;LX/Hq8;Landroid/content/Context;LX/0gd;)V
    .locals 3
    .param p1    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/HrE;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;",
            "Lcom/facebook/composer/actionitem/ActionItemController$Delegate;",
            "LX/Hq8;",
            "Landroid/content/Context;",
            "LX/0gd;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2510204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2510205
    iput-object p4, p0, LX/Hq4;->a:Landroid/content/Context;

    .line 2510206
    iput-object p5, p0, LX/Hq4;->d:LX/0gd;

    .line 2510207
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 2510208
    invoke-virtual {p3, p1, p2}, LX/Hq8;->a(Ljava/lang/Object;LX/HrE;)LX/Hq7;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2510209
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/Hq4;->b:LX/0Px;

    .line 2510210
    new-instance v0, LX/Hq3;

    iget-object v1, p0, LX/Hq4;->a:Landroid/content/Context;

    iget-object v2, p0, LX/Hq4;->d:LX/0gd;

    invoke-direct {v0, v1, v2, p1}, LX/Hq3;-><init>(Landroid/content/Context;LX/0gd;LX/0il;)V

    iput-object v0, p0, LX/Hq4;->c:LX/Hq3;

    .line 2510211
    invoke-direct {p0}, LX/Hq4;->b()V

    .line 2510212
    return-void
.end method

.method private b()V
    .locals 7

    .prologue
    .line 2510213
    iget-object v0, p0, LX/Hq4;->c:LX/Hq3;

    const/4 v3, 0x0

    .line 2510214
    iget-object v1, p0, LX/Hq4;->c:LX/Hq3;

    .line 2510215
    iget-object v2, v1, LX/Hq3;->d:LX/0Px;

    move-object v2, v2

    .line 2510216
    iget-object v1, p0, LX/Hq4;->b:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v5

    move v4, v3

    :goto_0
    if-ge v4, v5, :cond_4

    iget-object v1, p0, LX/Hq4;->b:LX/0Px;

    invoke-virtual {v1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Hq7;

    .line 2510217
    invoke-virtual {v1}, LX/Hq7;->b()Z

    move-result v6

    invoke-virtual {v2, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eq v6, v1, :cond_0

    .line 2510218
    const/4 v1, 0x1

    .line 2510219
    :goto_1
    if-nez v1, :cond_1

    move-object v1, v2

    .line 2510220
    :goto_2
    move-object v1, v1

    .line 2510221
    iput-object v1, v0, LX/Hq3;->d:LX/0Px;

    .line 2510222
    iget-object v0, p0, LX/Hq4;->c:LX/Hq3;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2510223
    return-void

    .line 2510224
    :cond_0
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_0

    .line 2510225
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 2510226
    iget-object v1, p0, LX/Hq4;->b:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v5

    move v2, v3

    :goto_3
    if-ge v2, v5, :cond_3

    iget-object v1, p0, LX/Hq4;->b:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Hq7;

    .line 2510227
    invoke-virtual {v1}, LX/Hq7;->b()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2510228
    invoke-virtual {v4, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2510229
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    .line 2510230
    :cond_3
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    goto :goto_2

    :cond_4
    move v1, v3

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/5L2;)V
    .locals 0

    .prologue
    .line 2510231
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2510232
    invoke-direct {p0}, LX/Hq4;->b()V

    .line 2510233
    return-void
.end method
