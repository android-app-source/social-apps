.class public LX/HnK;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/HnK;


# instance fields
.field public final a:LX/0if;


# direct methods
.method public constructor <init>(LX/0if;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2501679
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2501680
    iput-object p1, p0, LX/HnK;->a:LX/0if;

    .line 2501681
    return-void
.end method

.method public static a(LX/0QB;)LX/HnK;
    .locals 4

    .prologue
    .line 2501682
    sget-object v0, LX/HnK;->b:LX/HnK;

    if-nez v0, :cond_1

    .line 2501683
    const-class v1, LX/HnK;

    monitor-enter v1

    .line 2501684
    :try_start_0
    sget-object v0, LX/HnK;->b:LX/HnK;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2501685
    if-eqz v2, :cond_0

    .line 2501686
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2501687
    new-instance p0, LX/HnK;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v3

    check-cast v3, LX/0if;

    invoke-direct {p0, v3}, LX/HnK;-><init>(LX/0if;)V

    .line 2501688
    move-object v0, p0

    .line 2501689
    sput-object v0, LX/HnK;->b:LX/HnK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2501690
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2501691
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2501692
    :cond_1
    sget-object v0, LX/HnK;->b:LX/HnK;

    return-object v0

    .line 2501693
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2501694
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/HnK;LX/HnJ;)V
    .locals 3

    .prologue
    .line 2501695
    iget-object v0, p0, LX/HnK;->a:LX/0if;

    sget-object v1, LX/0ig;->aY:LX/0ih;

    iget-object v2, p1, LX/HnJ;->actionName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2501696
    return-void
.end method

.method public static a(LX/HnK;LX/HnJ;LX/1rQ;)V
    .locals 4

    .prologue
    .line 2501697
    iget-object v0, p0, LX/HnK;->a:LX/0if;

    sget-object v1, LX/0ig;->aY:LX/0ih;

    iget-object v2, p1, LX/HnJ;->actionName:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3, p2}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2501698
    return-void
.end method
