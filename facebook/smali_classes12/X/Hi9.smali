.class public LX/Hi9;
.super Landroid/support/v4/view/ViewPager;
.source ""


# instance fields
.field private final a:Ljava/util/Map;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/0hc;",
            "LX/Hi8;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/database/DataSetObserver;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2496890
    invoke-direct {p0, p1}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;)V

    .line 2496891
    new-instance v0, LX/026;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, LX/026;-><init>(I)V

    iput-object v0, p0, LX/Hi9;->a:Ljava/util/Map;

    .line 2496892
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 2496893
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2496894
    new-instance v0, LX/026;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, LX/026;-><init>(I)V

    iput-object v0, p0, LX/Hi9;->a:Ljava/util/Map;

    .line 2496895
    return-void
.end method

.method private a(LX/0gG;)V
    .locals 3

    .prologue
    .line 2496896
    instance-of v0, p1, LX/Hi7;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Hi9;->b:Landroid/database/DataSetObserver;

    if-nez v0, :cond_0

    .line 2496897
    new-instance v1, LX/Hi6;

    move-object v0, p1

    check-cast v0, LX/Hi7;

    invoke-direct {v1, v0}, LX/Hi6;-><init>(LX/Hi7;)V

    iput-object v1, p0, LX/Hi9;->b:Landroid/database/DataSetObserver;

    .line 2496898
    iget-object v0, p0, LX/Hi9;->b:Landroid/database/DataSetObserver;

    invoke-virtual {p1, v0}, LX/0gG;->a(Landroid/database/DataSetObserver;)V

    .line 2496899
    check-cast p1, LX/Hi7;

    invoke-static {p1}, LX/Hi7;->e(LX/Hi7;)V

    .line 2496900
    :cond_0
    return-void
.end method

.method private b(I)I
    .locals 1

    .prologue
    .line 2496907
    if-ltz p1, :cond_0

    invoke-static {}, LX/Hi9;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2496908
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 p1, 0x0

    .line 2496909
    :cond_0
    :goto_0
    return p1

    .line 2496910
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v0

    invoke-virtual {v0}, LX/0gG;->b()I

    move-result v0

    sub-int/2addr v0, p1

    add-int/lit8 p1, v0, -0x1

    goto :goto_0
.end method

.method private g()V
    .locals 2

    .prologue
    .line 2496901
    invoke-super {p0}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v0

    .line 2496902
    instance-of v1, v0, LX/Hi7;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/Hi9;->b:Landroid/database/DataSetObserver;

    if-eqz v1, :cond_0

    .line 2496903
    iget-object v1, p0, LX/Hi9;->b:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, LX/0gG;->b(Landroid/database/DataSetObserver;)V

    .line 2496904
    const/4 v0, 0x0

    iput-object v0, p0, LX/Hi9;->b:Landroid/database/DataSetObserver;

    .line 2496905
    :cond_0
    return-void
.end method

.method private static h()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2496906
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-static {v1}, LX/3rK;->a(Ljava/util/Locale;)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(F)V
    .locals 1

    .prologue
    .line 2496885
    invoke-static {}, LX/Hi9;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    invoke-super {p0, p1}, Landroid/support/v4/view/ViewPager;->a(F)V

    .line 2496886
    return-void

    .line 2496887
    :cond_0
    neg-float p1, p1

    goto :goto_0
.end method

.method public a(IZ)V
    .locals 1

    .prologue
    .line 2496888
    invoke-direct {p0, p1}, LX/Hi9;->b(I)I

    move-result v0

    invoke-super {p0, v0, p2}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 2496889
    return-void
.end method

.method public getAdapter()LX/0gG;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 2496881
    invoke-super {p0}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v0

    .line 2496882
    instance-of v1, v0, LX/Hi7;

    if-eqz v1, :cond_0

    check-cast v0, LX/Hi7;

    .line 2496883
    iget-object v1, v0, LX/Hi5;->a:LX/0gG;

    move-object v0, v1

    .line 2496884
    :cond_0
    return-object v0
.end method

.method public getCurrentItem()I
    .locals 1

    .prologue
    .line 2496880
    invoke-super {p0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    invoke-direct {p0, v0}, LX/Hi9;->b(I)I

    move-result v0

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x40fff53d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2496877
    invoke-super {p0}, Landroid/support/v4/view/ViewPager;->onAttachedToWindow()V

    .line 2496878
    invoke-super {p0}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v1

    invoke-direct {p0, v1}, LX/Hi9;->a(LX/0gG;)V

    .line 2496879
    const/16 v1, 0x2d

    const v2, 0x5896f19f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x67480a8a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2496874
    invoke-direct {p0}, LX/Hi9;->g()V

    .line 2496875
    invoke-super {p0}, Landroid/support/v4/view/ViewPager;->onDetachedFromWindow()V

    .line 2496876
    const/16 v1, 0x2d

    const v2, 0x1c747136

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setAdapter(LX/0gG;)V
    .locals 3
    .param p1    # LX/0gG;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 2496861
    invoke-direct {p0}, LX/Hi9;->g()V

    .line 2496862
    if-eqz p1, :cond_2

    invoke-static {}, LX/Hi9;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    move v1, v0

    .line 2496863
    :goto_0
    if-eqz v1, :cond_0

    .line 2496864
    new-instance v0, LX/Hi7;

    invoke-direct {v0, p0, p1}, LX/Hi7;-><init>(LX/Hi9;LX/0gG;)V

    .line 2496865
    invoke-direct {p0, v0}, LX/Hi9;->a(LX/0gG;)V

    move-object p1, v0

    .line 2496866
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2496867
    if-eqz v1, :cond_1

    .line 2496868
    const/4 v1, 0x0

    .line 2496869
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Hi9;->c:Z

    .line 2496870
    invoke-virtual {p0, v2, v1}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 2496871
    iput-boolean v1, p0, LX/Hi9;->c:Z

    .line 2496872
    :cond_1
    return-void

    :cond_2
    move v1, v2

    .line 2496873
    goto :goto_0
.end method

.method public setCurrentItem(I)V
    .locals 1

    .prologue
    .line 2496859
    invoke-direct {p0, p1}, LX/Hi9;->b(I)I

    move-result v0

    invoke-super {p0, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 2496860
    return-void
.end method

.method public setOnPageChangeListener(LX/0hc;)V
    .locals 2

    .prologue
    .line 2496854
    invoke-static {}, LX/Hi9;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2496855
    new-instance v0, LX/Hi8;

    invoke-direct {v0, p0, p1}, LX/Hi8;-><init>(LX/Hi9;LX/0hc;)V

    .line 2496856
    iget-object v1, p0, LX/Hi9;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object p1, v0

    .line 2496857
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 2496858
    return-void
.end method
