.class public LX/ItV;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation

.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# static fields
.field private static final m:Ljava/lang/Object;


# instance fields
.field private final a:LX/0SI;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/messaging/send/client/SendMessageManager;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/concurrent/ScheduledExecutorService;

.field public final d:LX/0TD;

.field private final e:LX/0SG;

.field private final f:LX/0ad;

.field public final g:LX/2N4;

.field public final h:LX/03V;

.field public final i:LX/01T;

.field private final j:LX/It7;

.field public k:LX/ItU;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "uiThread"
    .end annotation
.end field

.field public final l:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2623460
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/ItV;->m:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0SI;LX/0Or;Ljava/util/concurrent/ScheduledExecutorService;LX/0TD;LX/0SG;LX/0ad;LX/2N4;LX/03V;LX/01T;LX/It7;)V
    .locals 2
    .param p3    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p4    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p5    # LX/0SG;
        .annotation runtime Lcom/facebook/messaging/database/threads/NeedsDbClock;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0SI;",
            "LX/0Or",
            "<",
            "Lcom/facebook/messaging/send/client/SendMessageManager;",
            ">;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "LX/0TD;",
            "LX/0SG;",
            "LX/0ad;",
            "LX/2N4;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/01T;",
            "LX/It7;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2623446
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2623447
    sget-object v0, LX/ItU;->NOT_STARTED:LX/ItU;

    iput-object v0, p0, LX/ItV;->k:LX/ItU;

    .line 2623448
    iput-object p1, p0, LX/ItV;->a:LX/0SI;

    .line 2623449
    iput-object p2, p0, LX/ItV;->b:LX/0Or;

    .line 2623450
    iput-object p3, p0, LX/ItV;->c:Ljava/util/concurrent/ScheduledExecutorService;

    .line 2623451
    iput-object p4, p0, LX/ItV;->d:LX/0TD;

    .line 2623452
    iput-object p5, p0, LX/ItV;->e:LX/0SG;

    .line 2623453
    iput-object p6, p0, LX/ItV;->f:LX/0ad;

    .line 2623454
    iput-object p7, p0, LX/ItV;->g:LX/2N4;

    .line 2623455
    iput-object p8, p0, LX/ItV;->h:LX/03V;

    .line 2623456
    iput-object p9, p0, LX/ItV;->i:LX/01T;

    .line 2623457
    iput-object p10, p0, LX/ItV;->j:LX/It7;

    .line 2623458
    iget-object v0, p0, LX/ItV;->e:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/ItV;->l:J

    .line 2623459
    return-void
.end method

.method public static a(LX/0QB;)LX/ItV;
    .locals 7

    .prologue
    .line 2623393
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2623394
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2623395
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2623396
    if-nez v1, :cond_0

    .line 2623397
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2623398
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2623399
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2623400
    sget-object v1, LX/ItV;->m:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2623401
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2623402
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2623403
    :cond_1
    if-nez v1, :cond_4

    .line 2623404
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2623405
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2623406
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/ItV;->b(LX/0QB;)LX/ItV;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 2623407
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2623408
    if-nez v1, :cond_2

    .line 2623409
    sget-object v0, LX/ItV;->m:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ItV;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2623410
    :goto_1
    if-eqz v0, :cond_3

    .line 2623411
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2623412
    :goto_3
    check-cast v0, LX/ItV;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2623413
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2623414
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2623415
    :catchall_1
    move-exception v0

    .line 2623416
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2623417
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2623418
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2623419
    :cond_2
    :try_start_8
    sget-object v0, LX/ItV;->m:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ItV;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static b(LX/0QB;)LX/ItV;
    .locals 11

    .prologue
    .line 2623444
    new-instance v0, LX/ItV;

    invoke-static {p0}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v1

    check-cast v1, LX/0SI;

    const/16 v2, 0x2923

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, LX/0TD;

    invoke-static {p0}, LX/2N6;->a(LX/0QB;)LX/2N6;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-static {p0}, LX/2N4;->a(LX/0QB;)LX/2N4;

    move-result-object v7

    check-cast v7, LX/2N4;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-static {p0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v9

    check-cast v9, LX/01T;

    invoke-static {p0}, LX/It7;->a(LX/0QB;)LX/It7;

    move-result-object v10

    check-cast v10, LX/It7;

    invoke-direct/range {v0 .. v10}, LX/ItV;-><init>(LX/0SI;LX/0Or;Ljava/util/concurrent/ScheduledExecutorService;LX/0TD;LX/0SG;LX/0ad;LX/2N4;LX/03V;LX/01T;LX/It7;)V

    .line 2623445
    return-object v0
.end method

.method public static b(LX/ItV;Ljava/util/LinkedHashMap;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2623435
    sget-object v0, LX/6fP;->PENDING_SEND_ON_STARTUP:LX/6fP;

    invoke-static {v0}, Lcom/facebook/messaging/model/send/SendError;->a(LX/6fP;)Lcom/facebook/messaging/model/send/SendError;

    move-result-object v0

    .line 2623436
    iget-object v1, p0, LX/ItV;->j:LX/It7;

    invoke-virtual {p1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    iget-wide v4, p0, LX/ItV;->l:J

    .line 2623437
    iget-object v3, v1, LX/It7;->c:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/messaging/send/client/SendMessageManager;

    invoke-virtual {v3, v2}, Lcom/facebook/messaging/send/client/SendMessageManager;->a(LX/0Px;)V

    .line 2623438
    iget-object v3, v1, LX/It7;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/FDt;

    invoke-virtual {v3, v0, v4, v5}, LX/FDt;->a(Lcom/facebook/messaging/model/send/SendError;J)V

    .line 2623439
    iget-object v3, v1, LX/It7;->a:LX/2Og;

    sget-object p0, LX/6en;->FB:LX/6en;

    .line 2623440
    invoke-static {v3, p0}, LX/2Og;->b(LX/2Og;LX/6en;)LX/2OQ;

    move-result-object p1

    .line 2623441
    if-eqz p1, :cond_0

    .line 2623442
    invoke-virtual {p1, v2}, LX/2OQ;->b(LX/0Px;)V

    .line 2623443
    :cond_0
    return-void
.end method

.method public static e(LX/ItV;)V
    .locals 5

    .prologue
    .line 2623429
    sget-object v0, LX/6fP;->PENDING_SEND_ON_STARTUP:LX/6fP;

    invoke-static {v0}, Lcom/facebook/messaging/model/send/SendError;->a(LX/6fP;)Lcom/facebook/messaging/model/send/SendError;

    move-result-object v0

    .line 2623430
    iget-object v1, p0, LX/ItV;->j:LX/It7;

    iget-wide v2, p0, LX/ItV;->l:J

    .line 2623431
    iget-object v4, v1, LX/It7;->c:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/messaging/send/client/SendMessageManager;

    invoke-virtual {v4}, Lcom/facebook/messaging/send/client/SendMessageManager;->a()V

    .line 2623432
    iget-object v4, v1, LX/It7;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/FDt;

    invoke-virtual {v4, v0, v2, v3}, LX/FDt;->a(Lcom/facebook/messaging/model/send/SendError;J)V

    .line 2623433
    iget-object v4, v1, LX/It7;->a:LX/2Og;

    sget-object p0, LX/6en;->FB:LX/6en;

    invoke-virtual {v4, p0}, LX/2Og;->a(LX/6en;)V

    .line 2623434
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/LinkedHashMap;)V
    .locals 9
    .param p1    # Ljava/util/LinkedHashMap;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2623420
    iget-object v0, p0, LX/ItV;->k:LX/ItU;

    sget-object v1, LX/ItU;->ABORTED:LX/ItU;

    if-ne v0, v1, :cond_1

    .line 2623421
    :cond_0
    :goto_0
    return-void

    .line 2623422
    :cond_1
    sget-object v0, LX/ItU;->SUCCEEDED:LX/ItU;

    iput-object v0, p0, LX/ItV;->k:LX/ItU;

    .line 2623423
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2623424
    iget-object v0, p0, LX/ItV;->a:LX/0SI;

    iget-object v1, p0, LX/ItV;->a:LX/0SI;

    invoke-interface {v1}, LX/0SI;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0SI;->b(Lcom/facebook/auth/viewercontext/ViewerContext;)LX/1mW;

    move-result-object v3

    .line 2623425
    :try_start_0
    invoke-virtual {p1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 2623426
    iget-object v1, p0, LX/ItV;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/send/client/SendMessageManager;

    const-string v5, "startup_retry"

    const-string v6, "startup_retry"

    invoke-static {v6}, Lcom/facebook/messaging/send/trigger/NavigationTrigger;->a(Ljava/lang/String;)Lcom/facebook/messaging/send/trigger/NavigationTrigger;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v1, v0, v5, v6, v7}, Lcom/facebook/messaging/send/client/SendMessageManager;->a(Lcom/facebook/messaging/model/messages/Message;Ljava/lang/String;Lcom/facebook/messaging/send/trigger/NavigationTrigger;LX/9VZ;)Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_1

    .line 2623427
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2623428
    :catchall_0
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    :goto_2
    if-eqz v3, :cond_2

    if-eqz v1, :cond_4

    :try_start_2
    invoke-interface {v3}, LX/1mW;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_2
    :goto_3
    throw v0

    :cond_3
    if-eqz v3, :cond_0

    invoke-interface {v3}, LX/1mW;->close()V

    goto :goto_0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_4
    invoke-interface {v3}, LX/1mW;->close()V

    goto :goto_3

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_2
.end method
