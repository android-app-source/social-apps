.class public LX/Him;
.super LX/A8X;
.source ""


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Landroid/location/Address;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/view/LayoutInflater;

.field public c:LX/Hit;

.field public d:LX/Hil;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Px;LX/Hil;LX/Hit;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Px",
            "<",
            "Landroid/location/Address;",
            ">;",
            "LX/Hil;",
            "Lcom/facebook/addresstypeahead/view/AddressNullStateSectionAdapter$Listener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2497950
    invoke-direct {p0}, LX/A8X;-><init>()V

    .line 2497951
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, LX/Him;->b:Landroid/view/LayoutInflater;

    .line 2497952
    iput-object p2, p0, LX/Him;->a:LX/0Px;

    .line 2497953
    iput-object p3, p0, LX/Him;->d:LX/Hil;

    .line 2497954
    iput-object p4, p0, LX/Him;->c:LX/Hit;

    .line 2497955
    return-void
.end method

.method private static a(LX/Hil;)I
    .locals 2

    .prologue
    .line 2497946
    sget-object v0, LX/Hik;->a:[I

    invoke-virtual {p0}, LX/Hil;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2497947
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown or non-public guest list type."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2497948
    :pswitch_0
    const v0, 0x7f083a2c

    .line 2497949
    :goto_0
    return v0

    :pswitch_1
    const v0, 0x7f083a2d

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static f(I)I
    .locals 2

    .prologue
    .line 2497942
    packed-switch p0, :pswitch_data_0

    .line 2497943
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown View Type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2497944
    :pswitch_0
    const v0, 0x7f0300ab

    .line 2497945
    :goto_0
    return v0

    :pswitch_1
    const v0, 0x7f0300ad

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 2497928
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2497929
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown View Type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2497930
    :pswitch_0
    check-cast p1, Landroid/widget/TextView;

    .line 2497931
    iget-object v0, p0, LX/Him;->d:LX/Hil;

    invoke-static {v0}, LX/Him;->a(LX/Hil;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 2497932
    :goto_0
    return-void

    .line 2497933
    :pswitch_1
    check-cast p1, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2497934
    if-lez p2, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2497935
    iget-object v0, p0, LX/Him;->a:LX/0Px;

    add-int/lit8 v1, p2, -0x1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Address;

    move-object v0, v0

    .line 2497936
    invoke-virtual {v0}, Landroid/location/Address;->getMaxAddressLineIndex()I

    move-result v1

    if-ltz v1, :cond_0

    .line 2497937
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/location/Address;->getAddressLine(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2497938
    invoke-virtual {v0}, Landroid/location/Address;->getLocality()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2497939
    :goto_2
    new-instance v1, LX/Hij;

    invoke-direct {v1, p0, p2, v0}, LX/Hij;-><init>(LX/Him;ILandroid/location/Address;)V

    invoke-virtual {p1, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 2497940
    :cond_0
    invoke-virtual {v0}, Landroid/location/Address;->getLocality()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 2497941
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final d(Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 3

    .prologue
    .line 2497927
    iget-object v0, p0, LX/Him;->b:Landroid/view/LayoutInflater;

    invoke-static {p2}, LX/Him;->f(I)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2497925
    iget-object v0, p0, LX/Him;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    .line 2497926
    if-lez v0, :cond_0

    add-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2497922
    if-nez p1, :cond_0

    .line 2497923
    iget-object v0, p0, LX/Him;->d:LX/Hil;

    .line 2497924
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/Him;->a:LX/0Px;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2497920
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2497921
    const/4 v0, 0x2

    return v0
.end method
