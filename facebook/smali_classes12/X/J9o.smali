.class public LX/J9o;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/J9l;


# instance fields
.field private final a:LX/JDL;

.field private final b:LX/J94;

.field private final c:LX/9lP;

.field private final d:Landroid/view/LayoutInflater;

.field private final e:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/JDK;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/JDL;LX/J94;LX/9lP;Landroid/view/LayoutInflater;Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;)V
    .locals 0
    .param p3    # LX/9lP;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Landroid/view/LayoutInflater;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2653560
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2653561
    iput-object p1, p0, LX/J9o;->a:LX/JDL;

    .line 2653562
    iput-object p2, p0, LX/J9o;->b:LX/J94;

    .line 2653563
    iput-object p3, p0, LX/J9o;->c:LX/9lP;

    .line 2653564
    iput-object p4, p0, LX/J9o;->d:Landroid/view/LayoutInflater;

    .line 2653565
    iput-object p5, p0, LX/J9o;->e:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    .line 2653566
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2653559
    const/16 v0, 0x24

    return v0
.end method

.method public final a(LX/JBL;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;)LX/0us;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "updateCollection"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2653551
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2653552
    invoke-interface {p1}, LX/JBL;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2653553
    invoke-interface {p1}, LX/JBL;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;->d()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2653554
    iget-object v0, p0, LX/J9o;->f:Ljava/util/List;

    if-nez v0, :cond_0

    .line 2653555
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/J9o;->f:Ljava/util/List;

    .line 2653556
    :cond_0
    invoke-static {p1, p2}, LX/JDL;->a(LX/JBL;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;)LX/0Px;

    move-result-object v0

    .line 2653557
    iget-object v1, p0, LX/J9o;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2653558
    invoke-interface {p1}, LX/JBL;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;->b()LX/0us;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Context;LX/J9V;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2653542
    sget-object v0, LX/J9n;->a:[I

    invoke-virtual {p2}, LX/J9V;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2653543
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown type in TableCollectionSubAdapter"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2653544
    :pswitch_0
    new-instance v0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;

    invoke-direct {v0, p1}, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;-><init>(Landroid/content/Context;)V

    .line 2653545
    iget-object v2, p0, LX/J9o;->d:Landroid/view/LayoutInflater;

    invoke-static {v0, v2}, LX/J94;->c(Landroid/view/View;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    .line 2653546
    :goto_0
    return-object v0

    .line 2653547
    :pswitch_1
    new-instance v0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;

    invoke-direct {v0, p1}, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;-><init>(Landroid/content/Context;)V

    .line 2653548
    const v1, 0x7f0d0963

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 2653549
    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2653550
    iget-object v2, p0, LX/J9o;->d:Landroid/view/LayoutInflater;

    invoke-static {v0, v2}, LX/J94;->d(Landroid/view/View;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2653567
    iget-object v0, p0, LX/J9o;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Landroid/view/View;LX/9lP;)V
    .locals 2

    .prologue
    .line 2653539
    invoke-static {p2}, LX/J94;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;

    .line 2653540
    check-cast p1, LX/JDK;

    iget-object v1, p0, LX/J9o;->c:LX/9lP;

    invoke-virtual {v0, p1, v1}, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->a(LX/JDK;LX/9lP;)V

    .line 2653541
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2653538
    const/4 v0, 0x1

    return v0
.end method

.method public final b(I)LX/J9V;
    .locals 1

    .prologue
    .line 2653535
    invoke-virtual {p0}, LX/J9o;->d()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    .line 2653536
    sget-object v0, LX/J9V;->SUB_ADAPTER_ITEM_BOTTOM:LX/J9V;

    .line 2653537
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/J9V;->SUB_ADAPTER_ITEM_MIDDLE:LX/J9V;

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 2653533
    const/4 v0, 0x0

    iput-object v0, p0, LX/J9o;->f:Ljava/util/List;

    .line 2653534
    return-void
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 2653530
    iget-object v0, p0, LX/J9o;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 2653531
    iget-object v0, p0, LX/J9o;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 2653532
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;
    .locals 1

    .prologue
    .line 2653529
    iget-object v0, p0, LX/J9o;->e:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    return-object v0
.end method
