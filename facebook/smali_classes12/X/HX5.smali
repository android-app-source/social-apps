.class public final LX/HX5;
.super LX/2s5;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

.field public final b:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/base/fragment/FbFragment;",
            ">;>;"
        }
    .end annotation
.end field

.field private c:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

.field public d:J

.field public e:Z


# direct methods
.method public constructor <init>(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;LX/0gc;JLcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V
    .locals 1

    .prologue
    .line 2477343
    iput-object p1, p0, LX/HX5;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    .line 2477344
    invoke-direct {p0, p2}, LX/2s5;-><init>(LX/0gc;)V

    .line 2477345
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/HX5;->e:Z

    .line 2477346
    iput-wide p3, p0, LX/HX5;->d:J

    .line 2477347
    iput-object p5, p0, LX/HX5;->c:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    .line 2477348
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/HX5;->b:Landroid/util/SparseArray;

    .line 2477349
    return-void
.end method

.method private a(Landroid/support/v4/app/Fragment;)V
    .locals 5

    .prologue
    .line 2477326
    instance-of v0, p1, LX/8EK;

    if-nez v0, :cond_0

    .line 2477327
    iget-object v0, p0, LX/HX5;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->E:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "First card perf logging not supported by fragment "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2477328
    :goto_0
    return-void

    .line 2477329
    :cond_0
    iget-object v0, p0, LX/HX5;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aq:LX/8EI;

    const-string v1, "SuperCategoryType"

    iget-object v3, p0, LX/HX5;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v3, v3, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    .line 2477330
    invoke-static {v3}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->a(LX/CZd;)Ljava/lang/String;

    move-result-object v4

    move-object v2, v4

    .line 2477331
    invoke-virtual {v0, v1, v2}, LX/8EI;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2477332
    iget-object v0, p0, LX/HX5;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ab:LX/HQB;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/HQB;->a(I)LX/9Y7;

    move-result-object v0

    .line 2477333
    if-eqz v0, :cond_1

    invoke-interface {v0}, LX/9Y7;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2477334
    iget-object v1, p0, LX/HX5;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v1, v1, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aq:LX/8EI;

    const-string v2, "FirstTabType"

    invoke-interface {v0}, LX/9Y7;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/8EI;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2477335
    :cond_1
    check-cast p1, LX/8EK;

    .line 2477336
    const/4 v0, 0x1

    invoke-interface {p1, v0}, LX/8EK;->a(Z)V

    .line 2477337
    iget-object v0, p0, LX/HX5;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aq:LX/8EI;

    invoke-interface {p1, v0}, LX/8EK;->a(LX/8EI;)V

    .line 2477338
    iget-object v0, p0, LX/HX5;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aq:LX/8EI;

    .line 2477339
    iget-object v1, v0, LX/8EI;->b:LX/0id;

    const v2, 0x13007d

    iget-object v3, v0, LX/8EI;->g:Ljava/util/List;

    invoke-virtual {v1, v2, v3}, LX/0id;->a(ILjava/util/List;)V

    .line 2477340
    const-string v1, "FirstCardInitFetchNum"

    iget-object v2, v0, LX/8EI;->d:LX/8Do;

    invoke-virtual {v2}, LX/8Do;->j()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/8EI;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2477341
    const-string v1, "WrapperOnHomeTab"

    iget-object v2, v0, LX/8EI;->d:LX/8Do;

    invoke-virtual {v2}, LX/8Do;->k()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/8EI;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2477342
    goto :goto_0
.end method

.method private g(I)Lcom/facebook/base/fragment/FbFragment;
    .locals 12

    .prologue
    .line 2477278
    iget-object v0, p0, LX/HX5;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ab:LX/HQB;

    invoke-virtual {v0, p1}, LX/HQB;->a(I)LX/9Y7;

    move-result-object v0

    .line 2477279
    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2477280
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2477281
    iget-object v4, p0, LX/HX5;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v4, v4, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    .line 2477282
    iget-object v8, v4, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v4, v8

    .line 2477283
    if-eqz v4, :cond_5

    iget-object v4, p0, LX/HX5;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v4, v4, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    .line 2477284
    iget-object v8, v4, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v4, v8

    .line 2477285
    invoke-virtual {v4}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->x()Ljava/lang/String;

    move-result-object v4

    .line 2477286
    :goto_0
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 2477287
    const-string v9, "com.facebook.katana.profile.id"

    iget-wide v10, p0, LX/HX5;->d:J

    invoke-virtual {v8, v9, v10, v11}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2477288
    const-string v9, "profile_name"

    invoke-virtual {v8, v9, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2477289
    invoke-interface {v0}, LX/9Y7;->e()Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;

    move-result-object v4

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;->REACTION_SURFACE:Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;

    if-ne v4, v9, :cond_a

    .line 2477290
    invoke-interface {v0}, LX/9Y7;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v4

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_HOME:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-ne v4, v9, :cond_6

    move v4, v6

    .line 2477291
    :goto_1
    invoke-interface {v0}, LX/9Y7;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v9

    sget-object v10, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_GROUPS:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-ne v9, v10, :cond_7

    .line 2477292
    :goto_2
    const-string v7, "extra_should_enable_related_pages_like_chaining"

    invoke-virtual {v8, v7, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2477293
    const-string v7, "arg_should_support_cache"

    invoke-virtual {v8, v7, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2477294
    const-string v7, "arg_pages_surface_reaction_surface"

    invoke-static {v0}, LX/HQB;->a(LX/9Y7;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v7, v9}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2477295
    const-string v9, "arg_precreated_reaction_session_id"

    if-eqz v4, :cond_8

    iget-object v7, p0, LX/HX5;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v7, v7, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aI:Ljava/lang/String;

    :goto_3
    invoke-virtual {v8, v9, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2477296
    const-string v7, "arg_precreated_cached_reaction_session_id"

    if-eqz v4, :cond_0

    iget-object v5, p0, LX/HX5;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v5, v5, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aJ:Ljava/lang/String;

    :cond_0
    invoke-virtual {v8, v7, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2477297
    const-string v5, "page_fragment_uuid"

    iget-object v7, p0, LX/HX5;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    invoke-virtual {v7}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->k()Landroid/os/ParcelUuid;

    move-result-object v7

    invoke-virtual {v8, v5, v7}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2477298
    const-string v5, "extra_is_landing_fragment"

    invoke-interface {v0}, LX/9Y7;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v7

    iget-object v9, p0, LX/HX5;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v9, v9, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ab:LX/HQB;

    invoke-virtual {v9}, LX/HQB;->c()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v9

    invoke-virtual {v7, v9}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->equals(Ljava/lang/Object;)Z

    move-result v7

    invoke-virtual {v8, v5, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2477299
    if-eqz v4, :cond_1

    .line 2477300
    const-string v4, "extra_page_view_referrer"

    iget-object v5, p0, LX/HX5;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v5, v5, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2477301
    iget-object v7, v5, LX/HX9;->f:LX/89z;

    move-object v5, v7

    .line 2477302
    invoke-virtual {v5}, LX/89z;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v8, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2477303
    :cond_1
    const-string v5, "empty_view"

    if-eqz v6, :cond_9

    sget-object v4, LX/8YB;->GROUPS:LX/8YB;

    :goto_4
    invoke-virtual {v8, v5, v4}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2477304
    :cond_2
    :goto_5
    move-object v1, v8

    .line 2477305
    iget-object v2, p0, LX/HX5;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v2, v2, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->w:LX/HXC;

    const/4 v3, 0x1

    invoke-virtual {v2, v0, v1, v3}, LX/HXC;->a(LX/9Y7;Landroid/os/Bundle;Z)Lcom/facebook/base/fragment/FbFragment;

    move-result-object v1

    .line 2477306
    invoke-interface {v0}, LX/9Y7;->e()Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;->REACTION_SURFACE:Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;

    if-ne v0, v2, :cond_3

    instance-of v0, v1, LX/HPB;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/HX5;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-boolean v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->O:Z

    if-eqz v0, :cond_3

    move-object v0, v1

    .line 2477307
    check-cast v0, LX/HPB;

    invoke-interface {v0}, LX/HPB;->b()V

    .line 2477308
    :cond_3
    instance-of v0, v1, LX/HPA;

    if-eqz v0, :cond_4

    move-object v0, v1

    .line 2477309
    check-cast v0, LX/HPA;

    iget-object v2, p0, LX/HX5;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v2, v2, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    invoke-interface {v0, v2}, LX/HPA;->a(LX/CZd;)V

    .line 2477310
    :cond_4
    return-object v1

    :cond_5
    move-object v4, v5

    .line 2477311
    goto/16 :goto_0

    :cond_6
    move v4, v7

    .line 2477312
    goto/16 :goto_1

    :cond_7
    move v6, v7

    .line 2477313
    goto/16 :goto_2

    :cond_8
    move-object v7, v5

    .line 2477314
    goto/16 :goto_3

    .line 2477315
    :cond_9
    sget-object v4, LX/8YB;->DEFAULT:LX/8YB;

    goto :goto_4

    .line 2477316
    :cond_a
    sget-object v4, LX/HWw;->b:[I

    invoke-interface {v0}, LX/9Y7;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    goto :goto_5

    .line 2477317
    :pswitch_0
    const-string v4, "extra_ref_module"

    const-string v5, "pages_identity"

    invoke-virtual {v8, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2477318
    const-string v4, "event_ref_mechanism"

    sget-object v5, Lcom/facebook/events/common/ActionMechanism;->PAGES_SURFACE_EVENTS_TAB:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v5}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v8, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 2477319
    :pswitch_1
    iget-object v4, p0, LX/HX5;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v4, v4, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    if-eqz v4, :cond_2

    .line 2477320
    const-string v4, "extra_page_user_location"

    iget-object v5, p0, LX/HX5;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v5, v5, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    .line 2477321
    iget-object v6, v5, LX/CZd;->b:Landroid/location/Location;

    move-object v5, v6

    .line 2477322
    invoke-virtual {v8, v4, v5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_5

    .line 2477323
    :pswitch_2
    const-string v4, "page_fragment_uuid"

    iget-object v5, p0, LX/HX5;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    invoke-virtual {v5}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->k()Landroid/os/ParcelUuid;

    move-result-object v5

    invoke-virtual {v8, v4, v5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto/16 :goto_5

    .line 2477324
    :pswitch_3
    const-string v4, "product_ref_type"

    sget-object v5, LX/7iP;->PAGE:LX/7iP;

    invoke-virtual {v8, v4, v5}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2477325
    const-string v4, "product_ref_id"

    iget-wide v6, p0, LX/HX5;->d:J

    invoke-virtual {v8, v4, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto/16 :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public final G_(I)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 2477274
    invoke-virtual {p0}, LX/HX5;->b()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 2477275
    invoke-virtual {p0}, LX/0gG;->kV_()V

    .line 2477276
    const-string v0, ""

    .line 2477277
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/HX5;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ab:LX/HQB;

    invoke-virtual {v0, p1}, LX/HQB;->a(I)LX/9Y7;

    move-result-object v0

    invoke-interface {v0}, LX/9Y7;->k()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NameUppercaseModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NameUppercaseModel;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 2477273
    iget-boolean v0, p0, LX/HX5;->e:Z

    if-eqz v0, :cond_0

    const/4 v0, -0x2

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, LX/2s5;->a(Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 3

    .prologue
    .line 2477226
    invoke-direct {p0, p1}, LX/HX5;->g(I)Lcom/facebook/base/fragment/FbFragment;

    move-result-object v1

    .line 2477227
    iget-object v0, p0, LX/HX5;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    .line 2477228
    iget-boolean v2, v0, LX/CZd;->l:Z

    move v0, v2

    .line 2477229
    if-eqz v0, :cond_1

    .line 2477230
    :cond_0
    :goto_0
    return-object v1

    .line 2477231
    :cond_1
    iget-object v0, p0, LX/HX5;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ab:LX/HQB;

    invoke-virtual {v0}, LX/HQB;->d()I

    move-result v0

    if-ne p1, v0, :cond_2

    iget-object v0, p0, LX/HX5;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->f:LX/8Do;

    invoke-virtual {v0}, LX/8Do;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2477232
    :cond_2
    iget-object v0, p0, LX/HX5;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    invoke-static {v0, p1}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->g(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;I)Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;

    move-result-object v0

    .line 2477233
    if-nez v0, :cond_3

    .line 2477234
    iget-object v0, p0, LX/HX5;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->S:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;

    .line 2477235
    check-cast v1, LX/GZf;

    invoke-virtual {v0, v1}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->a(LX/GZf;)V

    .line 2477236
    iget-object v1, p0, LX/HX5;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v1, v1, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ae:Ljava/util/Map;

    iget-object v2, p0, LX/HX5;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v2, v2, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ab:LX/HQB;

    invoke-virtual {v2, p1}, LX/HQB;->a(I)LX/9Y7;

    move-result-object v2

    invoke-interface {v2}, LX/9Y7;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    move-object v1, v0

    .line 2477237
    goto :goto_0
.end method

.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2477259
    invoke-super {p0, p1, p2}, LX/2s5;->a(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/base/fragment/FbFragment;

    .line 2477260
    instance-of v1, v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;

    .line 2477261
    iget-boolean v2, v1, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->a:Z

    if-eqz v2, :cond_2

    iget-object v2, v1, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->c:LX/GZf;

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :goto_0
    move v1, v2

    .line 2477262
    if-nez v1, :cond_0

    move-object v1, v0

    .line 2477263
    check-cast v1, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;

    invoke-direct {p0, p2}, LX/HX5;->g(I)Lcom/facebook/base/fragment/FbFragment;

    move-result-object v2

    check-cast v2, LX/GZf;

    invoke-virtual {v1, v2}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->a(LX/GZf;)V

    move-object v1, v0

    .line 2477264
    check-cast v1, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;

    iget-object v2, p0, LX/HX5;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-boolean v2, v2, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->O:Z

    invoke-virtual {v1, v2}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->b(Z)V

    .line 2477265
    :cond_0
    iget-object v1, p0, LX/HX5;->b:Landroid/util/SparseArray;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, p2, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    move-object v1, v0

    .line 2477266
    check-cast v1, LX/GZf;

    iget-object v2, p0, LX/HX5;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget v2, v2, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->az:I

    invoke-interface {v1, v2}, LX/GZf;->a(I)V

    move-object v1, v0

    .line 2477267
    check-cast v1, LX/GZf;

    iget-object v2, p0, LX/HX5;->c:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    invoke-interface {v1, v2}, LX/GZf;->a(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V

    move-object v1, v0

    .line 2477268
    check-cast v1, LX/GZf;

    iget-object v2, p0, LX/HX5;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v2, v2, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->Z:Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;

    invoke-interface {v1, v2}, LX/GZf;->a(LX/E8t;)V

    move-object v1, v0

    .line 2477269
    check-cast v1, LX/GZf;

    iget-object v2, p0, LX/HX5;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v2, v2, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->b:LX/0hB;

    invoke-virtual {v2}, LX/0hB;->d()I

    move-result v2

    invoke-interface {v1, v2}, LX/GZf;->E_(I)V

    .line 2477270
    iget-object v1, p0, LX/HX5;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v1, v1, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ab:LX/HQB;

    invoke-virtual {v1}, LX/HQB;->d()I

    move-result v1

    if-ne p2, v1, :cond_1

    .line 2477271
    invoke-direct {p0, v0}, LX/HX5;->a(Landroid/support/v4/app/Fragment;)V

    .line 2477272
    :cond_1
    return-object v0

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2477256
    iget-object v0, p0, LX/HX5;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->remove(I)V

    .line 2477257
    invoke-super {p0, p1, p2, p3}, LX/2s5;->a(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 2477258
    return-void
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    .line 2477247
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/HX5;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2477248
    iget-object v0, p0, LX/HX5;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    .line 2477249
    iget-object v0, p0, LX/HX5;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 2477250
    if-eqz v0, :cond_1

    .line 2477251
    if-nez p1, :cond_0

    iget-object v3, p0, LX/HX5;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v3, v3, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->Y:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v3

    if-eq v3, v2, :cond_1

    .line 2477252
    :cond_0
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/base/fragment/FbFragment;

    .line 2477253
    check-cast v0, LX/GZf;

    iget-object v2, p0, LX/HX5;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget v2, v2, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->az:I

    invoke-interface {v0, v2}, LX/GZf;->a(I)V

    .line 2477254
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2477255
    :cond_2
    return-void
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 2477244
    iget-object v0, p0, LX/HX5;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    .line 2477245
    iget-boolean v1, v0, LX/CZd;->l:Z

    move v0, v1

    .line 2477246
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HX5;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ab:LX/HQB;

    invoke-virtual {v0}, LX/HQB;->b()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/HX5;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ab:LX/HQB;

    invoke-virtual {v0}, LX/HQB;->b()I

    move-result v0

    goto :goto_0
.end method

.method public final b(I)J
    .locals 2

    .prologue
    .line 2477240
    invoke-virtual {p0}, LX/HX5;->b()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 2477241
    invoke-virtual {p0}, LX/0gG;->kV_()V

    .line 2477242
    const-wide/16 v0, 0x0

    .line 2477243
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, LX/HX5;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ab:LX/HQB;

    invoke-virtual {v0, p1}, LX/HQB;->a(I)LX/9Y7;

    move-result-object v0

    invoke-interface {v0}, LX/9Y7;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->hashCode()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0
.end method

.method public final e(I)Lcom/facebook/base/fragment/FbFragment;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2477238
    iget-object v0, p0, LX/HX5;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 2477239
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/base/fragment/FbFragment;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
