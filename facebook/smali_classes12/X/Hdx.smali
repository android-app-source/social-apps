.class public final LX/Hdx;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Hdy;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;

.field public final synthetic b:LX/Hdy;


# direct methods
.method public constructor <init>(LX/Hdy;)V
    .locals 1

    .prologue
    .line 2489488
    iput-object p1, p0, LX/Hdx;->b:LX/Hdy;

    .line 2489489
    move-object v0, p1

    .line 2489490
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2489491
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2489476
    const-string v0, "TopicTitleComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2489477
    if-ne p0, p1, :cond_1

    .line 2489478
    :cond_0
    :goto_0
    return v0

    .line 2489479
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2489480
    goto :goto_0

    .line 2489481
    :cond_3
    check-cast p1, LX/Hdx;

    .line 2489482
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2489483
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2489484
    if-eq v2, v3, :cond_0

    .line 2489485
    iget-object v2, p0, LX/Hdx;->a:Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/Hdx;->a:Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;

    iget-object v3, p1, LX/Hdx;->a:Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2489486
    goto :goto_0

    .line 2489487
    :cond_4
    iget-object v2, p1, LX/Hdx;->a:Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
