.class public final LX/HMk;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lcom/facebook/pages/common/services/PagesServicesFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/services/PagesServicesFragment;Z)V
    .locals 0

    .prologue
    .line 2457596
    iput-object p1, p0, LX/HMk;->b:Lcom/facebook/pages/common/services/PagesServicesFragment;

    iput-boolean p2, p0, LX/HMk;->a:Z

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2457557
    iget-object v0, p0, LX/HMk;->b:Lcom/facebook/pages/common/services/PagesServicesFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/services/PagesServicesFragment;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "PagesServicesFragment"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2457558
    iget-object v0, p0, LX/HMk;->b:Lcom/facebook/pages/common/services/PagesServicesFragment;

    invoke-static {v0}, Lcom/facebook/pages/common/services/PagesServicesFragment;->p$redex0(Lcom/facebook/pages/common/services/PagesServicesFragment;)V

    .line 2457559
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2457560
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x1

    .line 2457561
    if-eqz p1, :cond_0

    .line 2457562
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2457563
    if-eqz v0, :cond_0

    .line 2457564
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2457565
    check-cast v0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel;->a()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 2457566
    :cond_0
    iget-object v0, p0, LX/HMk;->b:Lcom/facebook/pages/common/services/PagesServicesFragment;

    iget-object v1, p0, LX/HMk;->b:Lcom/facebook/pages/common/services/PagesServicesFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081815    # 1.8090005E38f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/pages/common/services/PagesServicesFragment;->a$redex0(Lcom/facebook/pages/common/services/PagesServicesFragment;Ljava/lang/String;)V

    .line 2457567
    :cond_1
    :goto_0
    return-void

    .line 2457568
    :cond_2
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2457569
    check-cast v0, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel;

    .line 2457570
    iget-object v2, p0, LX/HMk;->b:Lcom/facebook/pages/common/services/PagesServicesFragment;

    iget-boolean v2, v2, Lcom/facebook/pages/common/services/PagesServicesFragment;->x:Z

    .line 2457571
    iget-object v3, p0, LX/HMk;->b:Lcom/facebook/pages/common/services/PagesServicesFragment;

    new-instance v4, LX/8A4;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel;->b()LX/0Px;

    move-result-object v5

    invoke-direct {v4, v5}, LX/8A4;-><init>(Ljava/util/List;)V

    sget-object v5, LX/8A3;->CREATE_CONTENT:LX/8A3;

    invoke-virtual {v4, v5}, LX/8A4;->a(LX/8A3;)Z

    move-result v4

    .line 2457572
    iput-boolean v4, v3, Lcom/facebook/pages/common/services/PagesServicesFragment;->x:Z

    .line 2457573
    iget-object v3, p0, LX/HMk;->b:Lcom/facebook/pages/common/services/PagesServicesFragment;

    iget-boolean v3, v3, Lcom/facebook/pages/common/services/PagesServicesFragment;->x:Z

    if-eq v3, v2, :cond_3

    .line 2457574
    iget-object v2, p0, LX/HMk;->b:Lcom/facebook/pages/common/services/PagesServicesFragment;

    invoke-static {v2}, Lcom/facebook/pages/common/services/PagesServicesFragment;->k$redex0(Lcom/facebook/pages/common/services/PagesServicesFragment;)V

    .line 2457575
    :cond_3
    iget-object v2, p0, LX/HMk;->b:Lcom/facebook/pages/common/services/PagesServicesFragment;

    iget-boolean v2, v2, Lcom/facebook/pages/common/services/PagesServicesFragment;->x:Z

    if-eqz v2, :cond_4

    .line 2457576
    iget-object v2, p0, LX/HMk;->b:Lcom/facebook/pages/common/services/PagesServicesFragment;

    iget-object v2, v2, Lcom/facebook/pages/common/services/PagesServicesFragment;->j:LX/0if;

    sget-object v3, LX/0ig;->ah:LX/0ih;

    const-string v4, "admin_can_edit"

    invoke-virtual {v2, v3, v4}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 2457577
    :cond_4
    iget-object v2, p0, LX/HMk;->b:Lcom/facebook/pages/common/services/PagesServicesFragment;

    iget-object v3, p0, LX/HMk;->b:Lcom/facebook/pages/common/services/PagesServicesFragment;

    iget-object v3, v3, Lcom/facebook/pages/common/services/PagesServicesFragment;->f:LX/HMx;

    iget-object v4, p0, LX/HMk;->b:Lcom/facebook/pages/common/services/PagesServicesFragment;

    iget-object v4, v4, Lcom/facebook/pages/common/services/PagesServicesFragment;->q:LX/HN5;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/HMx;->a(LX/HN5;Ljava/lang/String;)LX/HN5;

    move-result-object v3

    .line 2457578
    iput-object v3, v2, Lcom/facebook/pages/common/services/PagesServicesFragment;->q:LX/HN5;

    .line 2457579
    iget-object v2, p0, LX/HMk;->b:Lcom/facebook/pages/common/services/PagesServicesFragment;

    invoke-static {v2}, Lcom/facebook/pages/common/services/PagesServicesFragment;->l(Lcom/facebook/pages/common/services/PagesServicesFragment;)V

    .line 2457580
    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel;->c()Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;

    move-result-object v2

    .line 2457581
    if-eqz v2, :cond_5

    invoke-virtual {v2}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v2}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;->c()Ljava/lang/String;

    move-result-object v0

    const-string v3, "published"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, LX/HMk;->b:Lcom/facebook/pages/common/services/PagesServicesFragment;

    iget-boolean v0, v0, Lcom/facebook/pages/common/services/PagesServicesFragment;->x:Z

    if-nez v0, :cond_6

    .line 2457582
    :cond_5
    iget-object v0, p0, LX/HMk;->b:Lcom/facebook/pages/common/services/PagesServicesFragment;

    invoke-static {v0}, Lcom/facebook/pages/common/services/PagesServicesFragment;->p$redex0(Lcom/facebook/pages/common/services/PagesServicesFragment;)V

    goto :goto_0

    .line 2457583
    :cond_6
    invoke-virtual {v2}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;->b()Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel$ProductCatalogModel;

    move-result-object v0

    if-nez v0, :cond_7

    move v0, v1

    :goto_1
    if-eqz v0, :cond_9

    .line 2457584
    iget-object v0, p0, LX/HMk;->b:Lcom/facebook/pages/common/services/PagesServicesFragment;

    invoke-static {v0}, Lcom/facebook/pages/common/services/PagesServicesFragment;->p$redex0(Lcom/facebook/pages/common/services/PagesServicesFragment;)V

    goto/16 :goto_0

    .line 2457585
    :cond_7
    invoke-virtual {v2}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;->b()Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel$ProductCatalogModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel$ProductCatalogModel;->b()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2457586
    if-nez v0, :cond_8

    move v0, v1

    goto :goto_1

    :cond_8
    const/4 v0, 0x0

    goto :goto_1

    .line 2457587
    :cond_9
    iget-object v0, p0, LX/HMk;->b:Lcom/facebook/pages/common/services/PagesServicesFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/services/PagesServicesFragment;->t:LX/HN4;

    invoke-virtual {v0}, LX/HN4;->getCount()I

    move-result v0

    .line 2457588
    iget-object v1, p0, LX/HMk;->b:Lcom/facebook/pages/common/services/PagesServicesFragment;

    const/4 v3, 0x0

    invoke-static {v1, v3}, Lcom/facebook/pages/common/services/PagesServicesFragment;->a$redex0(Lcom/facebook/pages/common/services/PagesServicesFragment;Ljava/lang/String;)V

    .line 2457589
    invoke-virtual {v2}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;->b()Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel$ProductCatalogModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel$ProductCatalogModel;->b()LX/1vs;

    move-result-object v1

    iget-object v3, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2457590
    iget-object v4, p0, LX/HMk;->b:Lcom/facebook/pages/common/services/PagesServicesFragment;

    invoke-virtual {v2}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;->d()LX/0Px;

    move-result-object v5

    invoke-static {v5}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v5

    invoke-virtual {v2}, Lcom/facebook/pages/identity/protocol/graphql/ServicesListGraphQLModels$PageServicesDataModel$ServicesCardModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v3, v1, v5, v2}, Lcom/facebook/pages/common/services/PagesServicesFragment;->a$redex0(Lcom/facebook/pages/common/services/PagesServicesFragment;LX/15i;ILjava/util/List;Ljava/lang/String;)V

    .line 2457591
    iget-boolean v1, p0, LX/HMk;->a:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/HMk;->b:Lcom/facebook/pages/common/services/PagesServicesFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/services/PagesServicesFragment;->t:LX/HN4;

    invoke-virtual {v1}, LX/HN4;->getCount()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 2457592
    iget-object v0, p0, LX/HMk;->b:Lcom/facebook/pages/common/services/PagesServicesFragment;

    .line 2457593
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/facebook/pages/common/services/PagesServicesFragment;->J:Z

    .line 2457594
    iget-object v1, v0, Lcom/facebook/pages/common/services/PagesServicesFragment;->g:LX/0hB;

    invoke-virtual {v1}, LX/0hB;->d()I

    move-result v1

    invoke-static {v0, v1}, Lcom/facebook/pages/common/services/PagesServicesFragment;->d(Lcom/facebook/pages/common/services/PagesServicesFragment;I)V

    .line 2457595
    goto/16 :goto_0
.end method
