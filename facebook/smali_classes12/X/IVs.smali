.class public LX/IVs;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/common/endoffeed/EndOfFeedPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/DP0;


# direct methods
.method public constructor <init>(LX/0Ot;LX/DP0;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/common/endoffeed/EndOfFeedPartDefinition;",
            ">;",
            "LX/DP0;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2582682
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2582683
    iput-object p1, p0, LX/IVs;->a:LX/0Ot;

    .line 2582684
    iput-object p2, p0, LX/IVs;->b:LX/DP0;

    .line 2582685
    return-void
.end method

.method public static a(LX/0QB;)LX/IVs;
    .locals 5

    .prologue
    .line 2582671
    const-class v1, LX/IVs;

    monitor-enter v1

    .line 2582672
    :try_start_0
    sget-object v0, LX/IVs;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2582673
    sput-object v2, LX/IVs;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2582674
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2582675
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2582676
    new-instance v4, LX/IVs;

    const/16 v3, 0x6f4

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    const-class v3, LX/DP0;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/DP0;

    invoke-direct {v4, p0, v3}, LX/IVs;-><init>(LX/0Ot;LX/DP0;)V

    .line 2582677
    move-object v0, v4

    .line 2582678
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2582679
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IVs;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2582680
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2582681
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0Ot;LX/0Ot;)LX/0Ot;
    .locals 1
    .param p2    # LX/0Ot;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<+",
            "Lcom/facebook/multirow/api/MultiRowGroupPartDefinition",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;*-",
            "LX/1Pf;",
            ">;>;",
            "LX/0Ot",
            "<+",
            "Lcom/facebook/multirow/api/MultiRowGroupPartDefinition",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;*-",
            "LX/1Pf;",
            ">;>;)",
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/feed/rows/partdefinitions/GenericGroupsFeedRootPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2582670
    new-instance v0, LX/IVr;

    invoke-direct {v0, p0, p1, p2}, LX/IVr;-><init>(LX/IVs;LX/0Ot;LX/0Ot;)V

    return-object v0
.end method
