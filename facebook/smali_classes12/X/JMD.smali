.class public LX/JMD;
.super Lcom/facebook/widget/SwitchCompat;
.source ""


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2683123
    invoke-direct {p0, p1}, Lcom/facebook/widget/SwitchCompat;-><init>(Landroid/content/Context;)V

    .line 2683124
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/JMD;->a:Z

    .line 2683125
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 1

    .prologue
    .line 2683126
    invoke-virtual {p0}, LX/JMD;->isChecked()Z

    move-result v0

    if-eq v0, p1, :cond_0

    .line 2683127
    invoke-super {p0, p1}, Lcom/facebook/widget/SwitchCompat;->setChecked(Z)V

    .line 2683128
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/JMD;->a:Z

    .line 2683129
    return-void
.end method

.method public final setChecked(Z)V
    .locals 1

    .prologue
    .line 2683130
    iget-boolean v0, p0, LX/JMD;->a:Z

    if-eqz v0, :cond_0

    .line 2683131
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/JMD;->a:Z

    .line 2683132
    invoke-super {p0, p1}, Lcom/facebook/widget/SwitchCompat;->setChecked(Z)V

    .line 2683133
    :cond_0
    return-void
.end method
