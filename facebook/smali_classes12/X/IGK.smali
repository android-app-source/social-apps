.class public final LX/IGK;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 12

    .prologue
    .line 2555858
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2555859
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2555860
    if-eqz v0, :cond_7

    .line 2555861
    const-string v1, "nodes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2555862
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2555863
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v3

    if-ge v2, v3, :cond_6

    .line 2555864
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result v3

    const-wide/16 v6, 0x0

    .line 2555865
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2555866
    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, LX/15i;->g(II)I

    move-result v4

    .line 2555867
    if-eqz v4, :cond_2

    .line 2555868
    const-string v5, "accuracy"

    invoke-virtual {p2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2555869
    const-wide/16 v10, 0x0

    .line 2555870
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2555871
    const/4 v8, 0x0

    invoke-virtual {p0, v4, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v8

    .line 2555872
    if-eqz v8, :cond_0

    .line 2555873
    const-string v9, "unit"

    invoke-virtual {p2, v9}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2555874
    invoke-virtual {p2, v8}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2555875
    :cond_0
    const/4 v8, 0x1

    invoke-virtual {p0, v4, v8, v10, v11}, LX/15i;->a(IID)D

    move-result-wide v8

    .line 2555876
    cmpl-double v10, v8, v10

    if-eqz v10, :cond_1

    .line 2555877
    const-string v10, "value"

    invoke-virtual {p2, v10}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2555878
    invoke-virtual {p2, v8, v9}, LX/0nX;->a(D)V

    .line 2555879
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2555880
    :cond_2
    const/4 v4, 0x1

    invoke-virtual {p0, v3, v4}, LX/15i;->g(II)I

    move-result v4

    .line 2555881
    if-eqz v4, :cond_3

    .line 2555882
    const-string v5, "location"

    invoke-virtual {p2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2555883
    invoke-static {p0, v4, p2}, LX/4aX;->a(LX/15i;ILX/0nX;)V

    .line 2555884
    :cond_3
    const/4 v4, 0x2

    invoke-virtual {p0, v3, v4, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v4

    .line 2555885
    cmp-long v6, v4, v6

    if-eqz v6, :cond_4

    .line 2555886
    const-string v6, "location_ts"

    invoke-virtual {p2, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2555887
    invoke-virtual {p2, v4, v5}, LX/0nX;->a(J)V

    .line 2555888
    :cond_4
    const/4 v4, 0x3

    invoke-virtual {p0, v3, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 2555889
    if-eqz v4, :cond_5

    .line 2555890
    const-string v5, "message"

    invoke-virtual {p2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2555891
    invoke-virtual {p2, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2555892
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2555893
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2555894
    :cond_6
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2555895
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2555896
    return-void
.end method
