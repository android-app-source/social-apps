.class public final LX/J5m;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;)V
    .locals 0

    .prologue
    .line 2648661
    iput-object p1, p0, LX/J5m;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x6c7fdbe0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2648662
    iget-object v1, p0, LX/J5m;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;

    iget-object v1, v1, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->y:LX/J5r;

    sget-object v2, LX/J5r;->INTRODUCTION:LX/J5r;

    if-ne v1, v2, :cond_0

    .line 2648663
    iget-object v1, p0, LX/J5m;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;

    iget-object v1, v1, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->q:LX/J45;

    sget-object v2, LX/J5h;->PRIVACY_CHECKUP_INTRO_STEP_CONTINUE:LX/J5h;

    invoke-virtual {v1, v2}, LX/J45;->a(LX/J5h;)V

    .line 2648664
    :cond_0
    iget-object v1, p0, LX/J5m;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;

    invoke-static {v1}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;->p(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupActivity;)V

    .line 2648665
    const v1, -0xc598661

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
