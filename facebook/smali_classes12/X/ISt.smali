.class public final LX/ISt;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;)V
    .locals 0

    .prologue
    .line 2578458
    iput-object p1, p0, LX/ISt;->a:Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2578459
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2578460
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2578461
    iget-object v0, p0, LX/ISt;->a:Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;

    iget-object v1, v0, Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;->a:LX/ISx;

    .line 2578462
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2578463
    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;

    .line 2578464
    iput-object v0, v1, LX/ISx;->g:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;

    .line 2578465
    invoke-static {v1}, LX/ISx;->f(LX/ISx;)V

    .line 2578466
    iget-object v2, v1, LX/ISx;->f:LX/DTZ;

    if-nez v2, :cond_0

    .line 2578467
    iget-object v2, v1, LX/ISx;->c:LX/DTa;

    iget-object v3, v1, LX/ISx;->g:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;

    invoke-virtual {v3}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->l()Ljava/lang/String;

    move-result-object v3

    iget-object p1, v1, LX/ISx;->g:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;

    invoke-virtual {p1}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->p()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object p1

    invoke-virtual {v2, v3, p1}, LX/DTa;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupAdminType;)LX/DTZ;

    move-result-object v2

    iput-object v2, v1, LX/ISx;->f:LX/DTZ;

    .line 2578468
    :cond_0
    iget-object v0, p0, LX/ISt;->a:Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;

    iget-object v1, p0, LX/ISt;->a:Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;

    iget-object v1, v1, Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;->a:LX/ISx;

    invoke-virtual {v1}, LX/ISx;->a()Ljava/lang/String;

    move-result-object v1

    .line 2578469
    iput-object v1, v0, Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;->n:Ljava/lang/String;

    .line 2578470
    iget-object v0, p0, LX/ISt;->a:Lcom/facebook/groups/feed/ui/GroupsMemberBioFragment;

    .line 2578471
    const-class v1, LX/1ZF;

    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2578472
    if-eqz v1, :cond_1

    .line 2578473
    invoke-virtual {v0}, Lcom/facebook/groups/feed/ui/GroupsFeedSimpleBaseFragment;->e()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2578474
    :cond_1
    return-void
.end method
