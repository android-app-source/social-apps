.class public final LX/IHq;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const-wide/16 v4, 0x0

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 2561111
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_8

    .line 2561112
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2561113
    :goto_0
    return v1

    .line 2561114
    :cond_0
    const-string v12, "location_ts"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 2561115
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    move v0, v6

    .line 2561116
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_6

    .line 2561117
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 2561118
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2561119
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 2561120
    const-string v12, "accuracy"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 2561121
    invoke-static {p0, p1}, LX/IHo;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 2561122
    :cond_2
    const-string v12, "location"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 2561123
    invoke-static {p0, p1}, LX/4aX;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 2561124
    :cond_3
    const-string v12, "message"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 2561125
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 2561126
    :cond_4
    const-string v12, "sender"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 2561127
    invoke-static {p0, p1}, LX/IHp;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 2561128
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2561129
    :cond_6
    const/4 v11, 0x5

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 2561130
    invoke-virtual {p1, v1, v10}, LX/186;->b(II)V

    .line 2561131
    invoke-virtual {p1, v6, v9}, LX/186;->b(II)V

    .line 2561132
    if-eqz v0, :cond_7

    .line 2561133
    const/4 v1, 0x2

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2561134
    :cond_7
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 2561135
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2561136
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_8
    move v0, v1

    move v7, v1

    move v8, v1

    move-wide v2, v4

    move v9, v1

    move v10, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 2561137
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2561138
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2561139
    if-eqz v0, :cond_0

    .line 2561140
    const-string v1, "accuracy"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2561141
    invoke-static {p0, v0, p2}, LX/IHo;->a(LX/15i;ILX/0nX;)V

    .line 2561142
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2561143
    if-eqz v0, :cond_1

    .line 2561144
    const-string v1, "location"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2561145
    invoke-static {p0, v0, p2}, LX/4aX;->a(LX/15i;ILX/0nX;)V

    .line 2561146
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 2561147
    cmp-long v2, v0, v2

    if-eqz v2, :cond_2

    .line 2561148
    const-string v2, "location_ts"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2561149
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 2561150
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2561151
    if-eqz v0, :cond_3

    .line 2561152
    const-string v1, "message"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2561153
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2561154
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2561155
    if-eqz v0, :cond_4

    .line 2561156
    const-string v1, "sender"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2561157
    invoke-static {p0, v0, p2, p3}, LX/IHp;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2561158
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2561159
    return-void
.end method
