.class public LX/Ii2;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/2Mi;

.field private final b:Landroid/content/Context;

.field private final c:LX/0gh;

.field private final d:LX/FGW;


# direct methods
.method public constructor <init>(LX/2Mi;Landroid/content/Context;LX/0gh;LX/FGW;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2603211
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2603212
    iput-object p1, p0, LX/Ii2;->a:LX/2Mi;

    .line 2603213
    iput-object p2, p0, LX/Ii2;->b:Landroid/content/Context;

    .line 2603214
    iput-object p3, p0, LX/Ii2;->c:LX/0gh;

    .line 2603215
    iput-object p4, p0, LX/Ii2;->d:LX/FGW;

    .line 2603216
    return-void
.end method

.method public static a(Lcom/facebook/ui/media/attachments/MediaResource;)Z
    .locals 4

    .prologue
    .line 2603217
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2603218
    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2603219
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    const-wide/16 v2, 0x400

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 2603220
    const/4 v0, 0x1

    .line 2603221
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
