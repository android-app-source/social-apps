.class public LX/JJL;
.super Landroid/content/AbstractThreadedSyncAdapter;
.source ""


# instance fields
.field private final a:LX/0WJ;

.field private final b:LX/JJJ;

.field private c:LX/0tX;

.field private d:Ljava/util/concurrent/ExecutorService;

.field private e:LX/0SG;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0WJ;LX/JJJ;LX/0tX;LX/0SG;Ljava/util/concurrent/ExecutorService;)V
    .locals 1
    .param p6    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/idleexecutor/DefaultIdleExecutor;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2679296
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Landroid/content/AbstractThreadedSyncAdapter;-><init>(Landroid/content/Context;Z)V

    .line 2679297
    iput-object p2, p0, LX/JJL;->a:LX/0WJ;

    .line 2679298
    iput-object p3, p0, LX/JJL;->b:LX/JJJ;

    .line 2679299
    iput-object p4, p0, LX/JJL;->c:LX/0tX;

    .line 2679300
    iput-object p5, p0, LX/JJL;->e:LX/0SG;

    .line 2679301
    iput-object p6, p0, LX/JJL;->d:Ljava/util/concurrent/ExecutorService;

    .line 2679302
    return-void
.end method

.method private static a(LX/JJH;Z)Ljava/util/HashMap;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/JJH;",
            "Z)",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2679303
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v1

    .line 2679304
    invoke-virtual {p0}, LX/JJH;->a()Ljava/util/List;

    move-result-object v0

    .line 2679305
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JJG;

    .line 2679306
    if-eqz p1, :cond_0

    .line 2679307
    iget-wide v6, v0, LX/JJG;->a:J

    move-wide v4, v6

    .line 2679308
    invoke-virtual {p0, v4, v5}, LX/JJH;->a(J)V

    .line 2679309
    :cond_0
    iget-wide v6, v0, LX/JJG;->b:J

    move-wide v4, v6

    .line 2679310
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    .line 2679311
    iget-wide v6, v0, LX/JJG;->a:J

    move-wide v4, v6

    .line 2679312
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2679313
    :cond_1
    return-object v1
.end method

.method public static a$redex0(LX/JJL;Landroid/content/SyncResult;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;)V
    .locals 10

    .prologue
    .line 2679314
    iget-object v0, p0, LX/JJL;->b:LX/JJJ;

    invoke-virtual {v0}, LX/JJJ;->a()LX/JJH;

    move-result-object v2

    .line 2679315
    if-nez v2, :cond_1

    .line 2679316
    :cond_0
    return-void

    .line 2679317
    :cond_1
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 2679318
    iget-boolean v0, p1, Landroid/content/SyncResult;->fullSyncRequested:Z

    invoke-static {v2, v0}, LX/JJL;->a(LX/JJH;Z)Ljava/util/HashMap;

    move-result-object v4

    .line 2679319
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v5

    .line 2679320
    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel$AllEventsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel$AllEventsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel$AllEventsModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2679321
    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel$AllEventsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel$AllEventsModel;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v7, :cond_2

    invoke-virtual {v6, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7oa;

    .line 2679322
    new-instance v8, LX/JJI;

    invoke-direct {v8, v0}, LX/JJI;-><init>(LX/7oa;)V

    invoke-interface {v5, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2679323
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2679324
    :cond_2
    move-object v0, v5

    .line 2679325
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v5

    .line 2679326
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_3
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JJI;

    .line 2679327
    iget-object v1, v0, LX/JJI;->e:Ljava/lang/String;

    move-object v1, v1

    .line 2679328
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2679329
    iget-object v1, v0, LX/JJI;->e:Ljava/lang/String;

    move-object v1, v1

    .line 2679330
    invoke-virtual {v4, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2679331
    iget-object v1, v0, LX/JJI;->e:Ljava/lang/String;

    move-object v1, v1

    .line 2679332
    invoke-virtual {v4, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 2679333
    sget-object v1, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v7, "caller_is_syncadapter"

    sget-object p0, LX/JJH;->b:Ljava/lang/String;

    invoke-virtual {v1, v7, p0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v7, "account_name"

    iget-object p0, v2, LX/JJH;->e:Landroid/accounts/Account;

    iget-object p0, p0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v7, p0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v7, "account_type"

    iget-object p0, v2, LX/JJH;->e:Landroid/accounts/Account;

    iget-object p0, p0, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v1, v7, p0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    .line 2679334
    new-instance v7, Ljava/lang/StringBuilder;

    const-string p0, "_id = \'"

    invoke-direct {v7, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string p0, "\'"

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 p0, 0x0

    invoke-virtual {v1, v7, p0}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    .line 2679335
    invoke-static {v2, v0, v1}, LX/JJH;->a(LX/JJH;LX/JJI;Landroid/content/ContentProviderOperation$Builder;)Landroid/content/ContentProviderOperation;

    move-result-object v1

    move-object v0, v1

    .line 2679336
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2679337
    :goto_2
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/16 v1, 0x32

    if-lt v0, v1, :cond_3

    .line 2679338
    invoke-virtual {v2, v5}, LX/JJH;->a(Ljava/util/ArrayList;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2679339
    invoke-virtual {p1}, Landroid/content/SyncResult;->madeSomeProgress()Z

    .line 2679340
    :cond_4
    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    goto/16 :goto_1

    .line 2679341
    :cond_5
    sget-object v1, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v7, "caller_is_syncadapter"

    sget-object v8, LX/JJH;->b:Ljava/lang/String;

    invoke-virtual {v1, v7, v8}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v7, "account_name"

    iget-object v8, v2, LX/JJH;->e:Landroid/accounts/Account;

    iget-object v8, v8, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v7, v8}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v7, "account_type"

    iget-object v8, v2, LX/JJH;->e:Landroid/accounts/Account;

    iget-object v8, v8, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v1, v7, v8}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    .line 2679342
    invoke-static {v2, v0, v1}, LX/JJH;->a(LX/JJH;LX/JJI;Landroid/content/ContentProviderOperation$Builder;)Landroid/content/ContentProviderOperation;

    move-result-object v1

    move-object v0, v1

    .line 2679343
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2679344
    :cond_6
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 2679345
    invoke-virtual {v2, v5}, LX/JJH;->a(Ljava/util/ArrayList;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2679346
    invoke-virtual {p1}, Landroid/content/SyncResult;->madeSomeProgress()Z

    .line 2679347
    :cond_7
    invoke-virtual {v4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_8
    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2679348
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2679349
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 2679350
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {v2, v0, v1}, LX/JJH;->a(J)V

    goto :goto_3
.end method


# virtual methods
.method public final onPerformSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 4

    .prologue
    .line 2679351
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 2679352
    iget-object v0, p0, LX/JJL;->b:LX/JJJ;

    .line 2679353
    iput-object p1, v0, LX/JJJ;->e:Landroid/accounts/Account;

    .line 2679354
    iget-object v0, p0, LX/JJL;->b:LX/JJJ;

    .line 2679355
    iput-object p4, v0, LX/JJJ;->d:Landroid/content/ContentProviderClient;

    .line 2679356
    const-string v0, "com.android.calendar"

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-wide/16 v2, 0x2a30

    invoke-static {p1, v0, v1, v2, v3}, Landroid/content/ContentResolver;->addPeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;J)V

    .line 2679357
    iget-object v0, p0, LX/JJL;->a:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2679358
    :goto_0
    return-void

    .line 2679359
    :cond_0
    iget-object v0, p0, LX/JJL;->e:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    .line 2679360
    invoke-static {}, LX/7oV;->g()LX/7oS;

    move-result-object v0

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    new-instance v1, LX/7oS;

    invoke-direct {v1}, LX/7oS;-><init>()V

    .line 2679361
    iget-object v2, v1, LX/0gW;->e:LX/0w7;

    move-object v1, v2

    .line 2679362
    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0w7;)LX/0zO;

    move-result-object v0

    .line 2679363
    iget-object v1, p0, LX/JJL;->c:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2679364
    new-instance v1, LX/JJK;

    invoke-direct {v1, p0, p5}, LX/JJK;-><init>(LX/JJL;Landroid/content/SyncResult;)V

    iget-object v2, p0, LX/JJL;->d:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
