.class public LX/IlN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static j:LX/0Xm;


# instance fields
.field private final b:LX/IkB;

.field private final c:LX/IkC;

.field private final d:LX/IkP;

.field private final e:Lcom/facebook/messaging/payment/method/verification/PaymentCardsFetcher;

.field private final f:LX/Ijy;

.field private final g:LX/Ijz;

.field private final h:LX/03V;

.field private final i:Ljava/util/concurrent/Executor;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2607704
    const-class v0, LX/IlN;

    sput-object v0, LX/IlN;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/IkB;LX/IkC;LX/IkP;Lcom/facebook/messaging/payment/method/verification/PaymentCardsFetcher;LX/Ijy;LX/Ijz;LX/03V;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p8    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2607705
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2607706
    iput-object p1, p0, LX/IlN;->b:LX/IkB;

    .line 2607707
    iput-object p2, p0, LX/IlN;->c:LX/IkC;

    .line 2607708
    iput-object p3, p0, LX/IlN;->d:LX/IkP;

    .line 2607709
    iput-object p4, p0, LX/IlN;->e:Lcom/facebook/messaging/payment/method/verification/PaymentCardsFetcher;

    .line 2607710
    iput-object p5, p0, LX/IlN;->f:LX/Ijy;

    .line 2607711
    iput-object p6, p0, LX/IlN;->g:LX/Ijz;

    .line 2607712
    iput-object p7, p0, LX/IlN;->h:LX/03V;

    .line 2607713
    iput-object p8, p0, LX/IlN;->i:Ljava/util/concurrent/Executor;

    .line 2607714
    return-void
.end method

.method public static a(LX/0QB;)LX/IlN;
    .locals 12

    .prologue
    .line 2607715
    const-class v1, LX/IlN;

    monitor-enter v1

    .line 2607716
    :try_start_0
    sget-object v0, LX/IlN;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2607717
    sput-object v2, LX/IlN;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2607718
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2607719
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2607720
    new-instance v3, LX/IlN;

    invoke-static {v0}, LX/IkB;->a(LX/0QB;)LX/IkB;

    move-result-object v4

    check-cast v4, LX/IkB;

    invoke-static {v0}, LX/IkC;->a(LX/0QB;)LX/IkC;

    move-result-object v5

    check-cast v5, LX/IkC;

    invoke-static {v0}, LX/IkP;->a(LX/0QB;)LX/IkP;

    move-result-object v6

    check-cast v6, LX/IkP;

    invoke-static {v0}, Lcom/facebook/messaging/payment/method/verification/PaymentCardsFetcher;->a(LX/0QB;)Lcom/facebook/messaging/payment/method/verification/PaymentCardsFetcher;

    move-result-object v7

    check-cast v7, Lcom/facebook/messaging/payment/method/verification/PaymentCardsFetcher;

    invoke-static {v0}, LX/Ijy;->b(LX/0QB;)LX/Ijy;

    move-result-object v8

    check-cast v8, LX/Ijy;

    const-class v9, LX/Ijz;

    invoke-interface {v0, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/Ijz;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v10

    check-cast v10, LX/03V;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v11

    check-cast v11, Ljava/util/concurrent/Executor;

    invoke-direct/range {v3 .. v11}, LX/IlN;-><init>(LX/IkB;LX/IkC;LX/IkP;Lcom/facebook/messaging/payment/method/verification/PaymentCardsFetcher;LX/Ijy;LX/Ijz;LX/03V;Ljava/util/concurrent/Executor;)V

    .line 2607721
    move-object v0, v3

    .line 2607722
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2607723
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IlN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2607724
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2607725
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
