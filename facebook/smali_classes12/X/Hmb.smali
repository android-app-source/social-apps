.class public final enum LX/Hmb;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Hmb;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Hmb;

.field public static final enum COMMUNICATION_ERROR:LX/Hmb;

.field public static final enum FAILED_TO_READ_SENDER_ERROR:LX/Hmb;

.field public static final enum FLOW_UNHANDLED_EXCEPTION:LX/Hmb;

.field public static final enum INITIALIZE:LX/Hmb;

.field public static final enum INSTALL_APK:LX/Hmb;

.field public static final enum READ_PREFLIGHT_EXCEPTION:LX/Hmb;

.field public static final enum RECEIVER_ERROR:LX/Hmb;

.field public static final enum RECEIVE_APK:LX/Hmb;

.field public static final enum RECEIVE_INTEND_TO_SEND:LX/Hmb;

.field public static final enum SENDER_ERROR:LX/Hmb;

.field public static final enum SEND_RECEIVE_PREFLIGHT:LX/Hmb;

.field public static final enum SOCKET_IO_ERROR:LX/Hmb;

.field public static final enum VERIFY_APK:LX/Hmb;

.field public static final enum VERIFY_APK_FAILURE:LX/Hmb;

.field public static final enum WRITE_ERROR_TO_SENDER:LX/Hmb;

.field public static final enum WRITE_PREFLIGHT_EXCEPTION:LX/Hmb;

.field public static final enum WRITE_TRANSFER_SUCCESS_EXCEPTION:LX/Hmb;


# instance fields
.field private final eventName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2500866
    new-instance v0, LX/Hmb;

    const-string v1, "INITIALIZE"

    const-string v2, "initialize"

    invoke-direct {v0, v1, v4, v2}, LX/Hmb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hmb;->INITIALIZE:LX/Hmb;

    .line 2500867
    new-instance v0, LX/Hmb;

    const-string v1, "SEND_RECEIVE_PREFLIGHT"

    const-string v2, "send_receive_preflight"

    invoke-direct {v0, v1, v5, v2}, LX/Hmb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hmb;->SEND_RECEIVE_PREFLIGHT:LX/Hmb;

    .line 2500868
    new-instance v0, LX/Hmb;

    const-string v1, "RECEIVE_INTEND_TO_SEND"

    const-string v2, "receive_intend_to_send"

    invoke-direct {v0, v1, v6, v2}, LX/Hmb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hmb;->RECEIVE_INTEND_TO_SEND:LX/Hmb;

    .line 2500869
    new-instance v0, LX/Hmb;

    const-string v1, "RECEIVE_APK"

    const-string v2, "receive_apk"

    invoke-direct {v0, v1, v7, v2}, LX/Hmb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hmb;->RECEIVE_APK:LX/Hmb;

    .line 2500870
    new-instance v0, LX/Hmb;

    const-string v1, "VERIFY_APK"

    const-string v2, "verify_apk"

    invoke-direct {v0, v1, v8, v2}, LX/Hmb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hmb;->VERIFY_APK:LX/Hmb;

    .line 2500871
    new-instance v0, LX/Hmb;

    const-string v1, "VERIFY_APK_FAILURE"

    const/4 v2, 0x5

    const-string v3, "verify_apk_failure"

    invoke-direct {v0, v1, v2, v3}, LX/Hmb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hmb;->VERIFY_APK_FAILURE:LX/Hmb;

    .line 2500872
    new-instance v0, LX/Hmb;

    const-string v1, "INSTALL_APK"

    const/4 v2, 0x6

    const-string v3, "install_apk"

    invoke-direct {v0, v1, v2, v3}, LX/Hmb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hmb;->INSTALL_APK:LX/Hmb;

    .line 2500873
    new-instance v0, LX/Hmb;

    const-string v1, "SENDER_ERROR"

    const/4 v2, 0x7

    const-string v3, "sender_error"

    invoke-direct {v0, v1, v2, v3}, LX/Hmb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hmb;->SENDER_ERROR:LX/Hmb;

    .line 2500874
    new-instance v0, LX/Hmb;

    const-string v1, "FAILED_TO_READ_SENDER_ERROR"

    const/16 v2, 0x8

    const-string v3, "failed_to_read_sender_error"

    invoke-direct {v0, v1, v2, v3}, LX/Hmb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hmb;->FAILED_TO_READ_SENDER_ERROR:LX/Hmb;

    .line 2500875
    new-instance v0, LX/Hmb;

    const-string v1, "RECEIVER_ERROR"

    const/16 v2, 0x9

    const-string v3, "receiver_error"

    invoke-direct {v0, v1, v2, v3}, LX/Hmb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hmb;->RECEIVER_ERROR:LX/Hmb;

    .line 2500876
    new-instance v0, LX/Hmb;

    const-string v1, "WRITE_ERROR_TO_SENDER"

    const/16 v2, 0xa

    const-string v3, "write_error_to_sender"

    invoke-direct {v0, v1, v2, v3}, LX/Hmb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hmb;->WRITE_ERROR_TO_SENDER:LX/Hmb;

    .line 2500877
    new-instance v0, LX/Hmb;

    const-string v1, "COMMUNICATION_ERROR"

    const/16 v2, 0xb

    const-string v3, "communication_error"

    invoke-direct {v0, v1, v2, v3}, LX/Hmb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hmb;->COMMUNICATION_ERROR:LX/Hmb;

    .line 2500878
    new-instance v0, LX/Hmb;

    const-string v1, "SOCKET_IO_ERROR"

    const/16 v2, 0xc

    const-string v3, "socket_io_error"

    invoke-direct {v0, v1, v2, v3}, LX/Hmb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hmb;->SOCKET_IO_ERROR:LX/Hmb;

    .line 2500879
    new-instance v0, LX/Hmb;

    const-string v1, "FLOW_UNHANDLED_EXCEPTION"

    const/16 v2, 0xd

    const-string v3, "flow_unhandled_exception"

    invoke-direct {v0, v1, v2, v3}, LX/Hmb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hmb;->FLOW_UNHANDLED_EXCEPTION:LX/Hmb;

    .line 2500880
    new-instance v0, LX/Hmb;

    const-string v1, "READ_PREFLIGHT_EXCEPTION"

    const/16 v2, 0xe

    const-string v3, "read_preflight_exception"

    invoke-direct {v0, v1, v2, v3}, LX/Hmb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hmb;->READ_PREFLIGHT_EXCEPTION:LX/Hmb;

    .line 2500881
    new-instance v0, LX/Hmb;

    const-string v1, "WRITE_PREFLIGHT_EXCEPTION"

    const/16 v2, 0xf

    const-string v3, "write_preflight_exception"

    invoke-direct {v0, v1, v2, v3}, LX/Hmb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hmb;->WRITE_PREFLIGHT_EXCEPTION:LX/Hmb;

    .line 2500882
    new-instance v0, LX/Hmb;

    const-string v1, "WRITE_TRANSFER_SUCCESS_EXCEPTION"

    const/16 v2, 0x10

    const-string v3, "write_transfer_success_exception"

    invoke-direct {v0, v1, v2, v3}, LX/Hmb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hmb;->WRITE_TRANSFER_SUCCESS_EXCEPTION:LX/Hmb;

    .line 2500883
    const/16 v0, 0x11

    new-array v0, v0, [LX/Hmb;

    sget-object v1, LX/Hmb;->INITIALIZE:LX/Hmb;

    aput-object v1, v0, v4

    sget-object v1, LX/Hmb;->SEND_RECEIVE_PREFLIGHT:LX/Hmb;

    aput-object v1, v0, v5

    sget-object v1, LX/Hmb;->RECEIVE_INTEND_TO_SEND:LX/Hmb;

    aput-object v1, v0, v6

    sget-object v1, LX/Hmb;->RECEIVE_APK:LX/Hmb;

    aput-object v1, v0, v7

    sget-object v1, LX/Hmb;->VERIFY_APK:LX/Hmb;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/Hmb;->VERIFY_APK_FAILURE:LX/Hmb;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/Hmb;->INSTALL_APK:LX/Hmb;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/Hmb;->SENDER_ERROR:LX/Hmb;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/Hmb;->FAILED_TO_READ_SENDER_ERROR:LX/Hmb;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/Hmb;->RECEIVER_ERROR:LX/Hmb;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/Hmb;->WRITE_ERROR_TO_SENDER:LX/Hmb;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/Hmb;->COMMUNICATION_ERROR:LX/Hmb;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/Hmb;->SOCKET_IO_ERROR:LX/Hmb;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/Hmb;->FLOW_UNHANDLED_EXCEPTION:LX/Hmb;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/Hmb;->READ_PREFLIGHT_EXCEPTION:LX/Hmb;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/Hmb;->WRITE_PREFLIGHT_EXCEPTION:LX/Hmb;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/Hmb;->WRITE_TRANSFER_SUCCESS_EXCEPTION:LX/Hmb;

    aput-object v2, v0, v1

    sput-object v0, LX/Hmb;->$VALUES:[LX/Hmb;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2500884
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2500885
    iput-object p3, p0, LX/Hmb;->eventName:Ljava/lang/String;

    .line 2500886
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Hmb;
    .locals 1

    .prologue
    .line 2500887
    const-class v0, LX/Hmb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Hmb;

    return-object v0
.end method

.method public static values()[LX/Hmb;
    .locals 1

    .prologue
    .line 2500888
    sget-object v0, LX/Hmb;->$VALUES:[LX/Hmb;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Hmb;

    return-object v0
.end method
