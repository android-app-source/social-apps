.class public final LX/IIq;
.super LX/IIn;
.source ""


# instance fields
.field public final synthetic b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V
    .locals 1

    .prologue
    .line 2562128
    iput-object p1, p0, LX/IIq;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-direct {p0, p1}, LX/IIn;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    return-void
.end method


# virtual methods
.method public final d()V
    .locals 1

    .prologue
    .line 2562129
    iget-object v0, p0, LX/IIq;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-static {v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->D(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    .line 2562130
    return-void
.end method

.method public final f()V
    .locals 7

    .prologue
    .line 2562131
    iget-object v0, p0, LX/IIq;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    .line 2562132
    iget-object v1, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->p:LX/IG7;

    sget-object v2, LX/IG6;->DASHBOARD_FETCH_DATA2:LX/IG6;

    invoke-virtual {v1, v2}, LX/IG7;->a(LX/IG6;)V

    .line 2562133
    invoke-static {v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->E$redex0(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    .line 2562134
    iget-object v1, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->u:LX/IFv;

    iget-object v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->an:Ljava/lang/String;

    iget-object v3, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ac:LX/0am;

    new-instance v4, LX/IIR;

    invoke-direct {v4, v0, p0}, LX/IIR;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;LX/IIm;)V

    .line 2562135
    iget-object v5, v1, LX/IFv;->d:LX/IFg;

    .line 2562136
    const/4 v6, 0x0

    invoke-static {v5, v6, v2, v3}, LX/IFg;->a(LX/IFg;ZLjava/lang/String;LX/0am;)LX/0zO;

    move-result-object v6

    move-object v5, v6

    .line 2562137
    iget-object v6, v1, LX/IFv;->a:LX/1Ck;

    sget-object v0, LX/IFy;->f:LX/IFy;

    iget-object p0, v1, LX/IFv;->c:LX/0tX;

    invoke-virtual {p0, v5}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v5

    new-instance p0, LX/IFq;

    invoke-direct {p0, v1, v4}, LX/IFq;-><init>(LX/IFv;LX/IIR;)V

    invoke-virtual {v6, v0, v5, p0}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2562138
    return-void
.end method

.method public final i()V
    .locals 3

    .prologue
    .line 2562139
    iget-object v0, p0, LX/IIq;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    const/4 v1, 0x0

    .line 2562140
    iput-boolean v1, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ae:Z

    .line 2562141
    iget-object v0, p0, LX/IIq;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, p0, LX/IIq;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aE:LX/IIm;

    .line 2562142
    iget-boolean v2, v0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v2, v2

    .line 2562143
    if-eqz v2, :cond_0

    .line 2562144
    invoke-static {v0, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->a$redex1(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;LX/IIm;)V

    .line 2562145
    :goto_0
    return-void

    .line 2562146
    :cond_0
    iget-object v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->p:LX/IG7;

    sget-object p0, LX/IG6;->DASHBOARD_TTI:LX/IG6;

    invoke-virtual {v2, p0}, LX/IG7;->c(LX/IG6;)V

    .line 2562147
    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ae:Z

    goto :goto_0
.end method

.method public final j()V
    .locals 3

    .prologue
    .line 2562148
    iget-object v0, p0, LX/IIq;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, p0, LX/IIq;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aH:LX/IIm;

    .line 2562149
    iget-boolean v2, v0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v2, v2

    .line 2562150
    if-eqz v2, :cond_0

    .line 2562151
    invoke-static {v0, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->a$redex1(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;LX/IIm;)V

    .line 2562152
    :goto_0
    return-void

    .line 2562153
    :cond_0
    iget-object v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->p:LX/IG7;

    sget-object p0, LX/IG6;->DASHBOARD_TTI:LX/IG6;

    invoke-virtual {v2, p0}, LX/IG7;->c(LX/IG6;)V

    .line 2562154
    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ae:Z

    goto :goto_0
.end method

.method public final k()V
    .locals 3

    .prologue
    .line 2562155
    iget-object v0, p0, LX/IIq;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, p0, LX/IIq;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aF:LX/IIm;

    .line 2562156
    iget-boolean v2, v0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v2, v2

    .line 2562157
    if-eqz v2, :cond_0

    .line 2562158
    invoke-static {v0, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->a$redex1(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;LX/IIm;)V

    .line 2562159
    :goto_0
    return-void

    .line 2562160
    :cond_0
    iget-object v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->p:LX/IG7;

    sget-object p0, LX/IG6;->DASHBOARD_TTI:LX/IG6;

    invoke-virtual {v2, p0}, LX/IG7;->c(LX/IG6;)V

    .line 2562161
    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ae:Z

    goto :goto_0
.end method

.method public final l()V
    .locals 3

    .prologue
    .line 2562162
    iget-object v0, p0, LX/IIq;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, p0, LX/IIq;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aG:LX/IIm;

    .line 2562163
    iget-boolean v2, v0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v2, v2

    .line 2562164
    if-eqz v2, :cond_0

    .line 2562165
    invoke-static {v0, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->a$redex1(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;LX/IIm;)V

    .line 2562166
    :goto_0
    return-void

    .line 2562167
    :cond_0
    iget-object v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->p:LX/IG7;

    sget-object p0, LX/IG6;->DASHBOARD_TTI:LX/IG6;

    invoke-virtual {v2, p0}, LX/IG7;->c(LX/IG6;)V

    .line 2562168
    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ae:Z

    goto :goto_0
.end method

.method public final m()V
    .locals 3

    .prologue
    .line 2562169
    iget-object v0, p0, LX/IIq;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, p0, LX/IIq;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aI:LX/IIm;

    .line 2562170
    iget-boolean v2, v0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v2, v2

    .line 2562171
    if-eqz v2, :cond_0

    .line 2562172
    invoke-static {v0, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->a$redex1(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;LX/IIm;)V

    .line 2562173
    :goto_0
    return-void

    .line 2562174
    :cond_0
    iget-object v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->p:LX/IG7;

    sget-object p0, LX/IG6;->DASHBOARD_TTI:LX/IG6;

    invoke-virtual {v2, p0}, LX/IG7;->c(LX/IG6;)V

    .line 2562175
    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ae:Z

    goto :goto_0
.end method
