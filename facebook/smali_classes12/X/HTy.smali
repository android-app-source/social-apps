.class public final LX/HTy;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 2471323
    const/4 v10, 0x0

    .line 2471324
    const/4 v9, 0x0

    .line 2471325
    const/4 v8, 0x0

    .line 2471326
    const/4 v7, 0x0

    .line 2471327
    const/4 v6, 0x0

    .line 2471328
    const/4 v5, 0x0

    .line 2471329
    const/4 v4, 0x0

    .line 2471330
    const/4 v3, 0x0

    .line 2471331
    const/4 v2, 0x0

    .line 2471332
    const/4 v1, 0x0

    .line 2471333
    const/4 v0, 0x0

    .line 2471334
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->START_OBJECT:LX/15z;

    if-eq v11, v12, :cond_1

    .line 2471335
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2471336
    const/4 v0, 0x0

    .line 2471337
    :goto_0
    return v0

    .line 2471338
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2471339
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_b

    .line 2471340
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 2471341
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2471342
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 2471343
    const-string v12, "friend_endorsements_url"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 2471344
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_1

    .line 2471345
    :cond_2
    const-string v12, "friend_endorsers"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 2471346
    invoke-static {p0, p1}, LX/HTt;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 2471347
    :cond_3
    const-string v12, "friend_posters_count"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 2471348
    const/4 v0, 0x1

    .line 2471349
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v8

    goto :goto_1

    .line 2471350
    :cond_4
    const-string v12, "help_center_url"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 2471351
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 2471352
    :cond_5
    const-string v12, "pending_endorsements_url"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 2471353
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 2471354
    :cond_6
    const-string v12, "pending_endorsers"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 2471355
    invoke-static {p0, p1}, LX/HTt;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 2471356
    :cond_7
    const-string v12, "politician"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 2471357
    invoke-static {p0, p1}, LX/HTx;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 2471358
    :cond_8
    const-string v12, "running_office"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 2471359
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_1

    .line 2471360
    :cond_9
    const-string v12, "social_context_text"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 2471361
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto/16 :goto_1

    .line 2471362
    :cond_a
    const-string v12, "url"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 2471363
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto/16 :goto_1

    .line 2471364
    :cond_b
    const/16 v11, 0xa

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 2471365
    const/4 v11, 0x0

    invoke-virtual {p1, v11, v10}, LX/186;->b(II)V

    .line 2471366
    const/4 v10, 0x1

    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 2471367
    if-eqz v0, :cond_c

    .line 2471368
    const/4 v0, 0x2

    const/4 v9, 0x0

    invoke-virtual {p1, v0, v8, v9}, LX/186;->a(III)V

    .line 2471369
    :cond_c
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2471370
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2471371
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2471372
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2471373
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2471374
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2471375
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2471376
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2471377
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2471378
    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2471379
    if-eqz v0, :cond_0

    .line 2471380
    const-string v1, "friend_endorsements_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2471381
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2471382
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2471383
    if-eqz v0, :cond_1

    .line 2471384
    const-string v1, "friend_endorsers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2471385
    invoke-static {p0, v0, p2, p3}, LX/HTt;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2471386
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2471387
    if-eqz v0, :cond_2

    .line 2471388
    const-string v1, "friend_posters_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2471389
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2471390
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2471391
    if-eqz v0, :cond_3

    .line 2471392
    const-string v1, "help_center_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2471393
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2471394
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2471395
    if-eqz v0, :cond_4

    .line 2471396
    const-string v1, "pending_endorsements_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2471397
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2471398
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2471399
    if-eqz v0, :cond_5

    .line 2471400
    const-string v1, "pending_endorsers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2471401
    invoke-static {p0, v0, p2, p3}, LX/HTt;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2471402
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2471403
    if-eqz v0, :cond_6

    .line 2471404
    const-string v1, "politician"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2471405
    invoke-static {p0, v0, p2, p3}, LX/HTx;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2471406
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2471407
    if-eqz v0, :cond_7

    .line 2471408
    const-string v1, "running_office"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2471409
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2471410
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2471411
    if-eqz v0, :cond_8

    .line 2471412
    const-string v1, "social_context_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2471413
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2471414
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2471415
    if-eqz v0, :cond_9

    .line 2471416
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2471417
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2471418
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2471419
    return-void
.end method
