.class public final enum LX/JLG;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/JLG;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/JLG;

.field public static final enum LARGE:LX/JLG;

.field public static final enum MEDIUM:LX/JLG;

.field public static final enum SMALL:LX/JLG;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2680777
    new-instance v0, LX/JLG;

    const-string v1, "SMALL"

    invoke-direct {v0, v1, v2}, LX/JLG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JLG;->SMALL:LX/JLG;

    .line 2680778
    new-instance v0, LX/JLG;

    const-string v1, "MEDIUM"

    invoke-direct {v0, v1, v3}, LX/JLG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JLG;->MEDIUM:LX/JLG;

    .line 2680779
    new-instance v0, LX/JLG;

    const-string v1, "LARGE"

    invoke-direct {v0, v1, v4}, LX/JLG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JLG;->LARGE:LX/JLG;

    .line 2680780
    const/4 v0, 0x3

    new-array v0, v0, [LX/JLG;

    sget-object v1, LX/JLG;->SMALL:LX/JLG;

    aput-object v1, v0, v2

    sget-object v1, LX/JLG;->MEDIUM:LX/JLG;

    aput-object v1, v0, v3

    sget-object v1, LX/JLG;->LARGE:LX/JLG;

    aput-object v1, v0, v4

    sput-object v0, LX/JLG;->$VALUES:[LX/JLG;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2680781
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/JLG;
    .locals 1

    .prologue
    .line 2680782
    const-class v0, LX/JLG;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/JLG;

    return-object v0
.end method

.method public static values()[LX/JLG;
    .locals 1

    .prologue
    .line 2680783
    sget-object v0, LX/JLG;->$VALUES:[LX/JLG;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/JLG;

    return-object v0
.end method
