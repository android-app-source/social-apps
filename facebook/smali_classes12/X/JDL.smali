.class public LX/JDL;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/JDL;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2663706
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2663707
    return-void
.end method

.method public static a(LX/JBL;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;)LX/0Px;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getListCollectionItems"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/JBL;",
            "Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;",
            ")",
            "LX/0Px",
            "<",
            "LX/JDK;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2663708
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2663709
    invoke-interface {p0}, LX/JBL;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, LX/JBL;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;->d()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2663710
    invoke-interface {p0}, LX/JBL;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;->d()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;

    .line 2663711
    invoke-static {v0, p1}, LX/JDK;->a(Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;)LX/JDK;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2663712
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2663713
    :cond_0
    invoke-interface {p0}, LX/JBK;->r()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, LX/JBK;->r()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2663714
    invoke-interface {p0}, LX/JBK;->r()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsEligibleSuggestionsFieldsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v0

    .line 2663715
    :goto_1
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionSuggestionFieldsModel;

    .line 2663716
    invoke-static {v0, p1}, LX/JDK;->a(Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionSuggestionFieldsModel;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;)LX/JDK;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2663717
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2663718
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/JDL;
    .locals 3

    .prologue
    .line 2663719
    sget-object v0, LX/JDL;->a:LX/JDL;

    if-nez v0, :cond_1

    .line 2663720
    const-class v1, LX/JDL;

    monitor-enter v1

    .line 2663721
    :try_start_0
    sget-object v0, LX/JDL;->a:LX/JDL;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2663722
    if-eqz v2, :cond_0

    .line 2663723
    :try_start_1
    new-instance v0, LX/JDL;

    invoke-direct {v0}, LX/JDL;-><init>()V

    .line 2663724
    move-object v0, v0

    .line 2663725
    sput-object v0, LX/JDL;->a:LX/JDL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2663726
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2663727
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2663728
    :cond_1
    sget-object v0, LX/JDL;->a:LX/JDL;

    return-object v0

    .line 2663729
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2663730
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
