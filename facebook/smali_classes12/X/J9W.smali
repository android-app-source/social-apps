.class public LX/J9W;
.super Landroid/widget/BaseAdapter;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final l:Ljava/lang/Object;

.field private static final m:Ljava/lang/Object;


# instance fields
.field public final b:Landroid/content/Context;

.field private final c:LX/9lP;

.field private final d:LX/JDA;

.field public final e:LX/JCx;

.field private final f:LX/JDL;

.field public final g:LX/J94;

.field private final h:LX/J8p;

.field public final i:LX/0hx;

.field private final j:LX/J93;

.field public final k:Landroid/view/LayoutInflater;

.field public n:Z

.field private o:LX/JAc;

.field private p:LX/JBL;

.field public q:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

.field public r:LX/J9l;

.field public s:LX/0us;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2652941
    const-class v0, LX/J9W;

    sput-object v0, LX/J9W;->a:Ljava/lang/Class;

    .line 2652942
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/J9W;->l:Ljava/lang/Object;

    .line 2652943
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/J9W;->m:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/9lP;LX/JDA;LX/JCx;LX/JDL;LX/J94;LX/J8p;LX/0hx;LX/J93;Landroid/view/LayoutInflater;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/9lP;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/J8p;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p10    # Landroid/view/LayoutInflater;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2652952
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 2652953
    const/4 v0, 0x0

    iput-object v0, p0, LX/J9W;->s:LX/0us;

    .line 2652954
    iput-object p1, p0, LX/J9W;->b:Landroid/content/Context;

    .line 2652955
    iput-object p2, p0, LX/J9W;->c:LX/9lP;

    .line 2652956
    iput-object p3, p0, LX/J9W;->d:LX/JDA;

    .line 2652957
    iput-object p4, p0, LX/J9W;->e:LX/JCx;

    .line 2652958
    iput-object p5, p0, LX/J9W;->f:LX/JDL;

    .line 2652959
    iput-object p6, p0, LX/J9W;->g:LX/J94;

    .line 2652960
    iput-object p7, p0, LX/J9W;->h:LX/J8p;

    .line 2652961
    iput-object p8, p0, LX/J9W;->i:LX/0hx;

    .line 2652962
    iput-object p9, p0, LX/J9W;->j:LX/J93;

    .line 2652963
    iput-object p10, p0, LX/J9W;->k:Landroid/view/LayoutInflater;

    .line 2652964
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/J9W;->n:Z

    .line 2652965
    return-void
.end method

.method private f()LX/JD9;
    .locals 18

    .prologue
    .line 2652944
    const/4 v15, 0x0

    .line 2652945
    const/4 v14, 0x0

    .line 2652946
    move-object/from16 v0, p0

    iget-object v1, v0, LX/J9W;->o:LX/JAc;

    if-eqz v1, :cond_0

    .line 2652947
    move-object/from16 v0, p0

    iget-object v1, v0, LX/J9W;->o:LX/JAc;

    invoke-interface {v1}, LX/JAb;->e()Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    move-result-object v15

    .line 2652948
    move-object/from16 v0, p0

    iget-object v2, v0, LX/J9W;->p:LX/JBL;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/J9W;->o:LX/JAc;

    invoke-interface {v3}, LX/JAb;->e()Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, LX/J9W;->q:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    invoke-static {v2, v3, v4}, LX/J93;->a(LX/JBL;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;)Ljava/lang/String;

    move-result-object v14

    .line 2652949
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, LX/J9W;->p:LX/JBL;

    invoke-interface {v1}, LX/JBL;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;->a()I

    move-result v1

    if-nez v1, :cond_1

    const/4 v5, 0x0

    .line 2652950
    :goto_0
    new-instance v1, LX/JD9;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/J9W;->p:LX/JBL;

    invoke-interface {v2}, LX/JAV;->c()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, LX/J9W;->p:LX/JBL;

    invoke-interface {v3}, LX/JBK;->mW_()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, LX/J9W;->p:LX/JBL;

    invoke-interface {v7}, LX/JBK;->p()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, LX/J9W;->p:LX/JBL;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/J9W;->c:LX/9lP;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-direct/range {v1 .. v17}, LX/JD9;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/1Fb;Ljava/lang/String;LX/JBL;Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionMediasetModel;Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldsModel;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;LX/9lP;Z)V

    return-object v1

    .line 2652951
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, LX/J9W;->p:LX/JBL;

    invoke-interface {v1}, LX/JBL;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2652932
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/J9W;->n:Z

    .line 2652933
    iput-object v1, p0, LX/J9W;->o:LX/JAc;

    .line 2652934
    iput-object v1, p0, LX/J9W;->p:LX/JBL;

    .line 2652935
    iput-object v1, p0, LX/J9W;->s:LX/0us;

    .line 2652936
    iget-object v0, p0, LX/J9W;->r:LX/J9l;

    if-eqz v0, :cond_0

    .line 2652937
    iget-object v0, p0, LX/J9W;->r:LX/J9l;

    invoke-interface {v0}, LX/J9k;->c()V

    .line 2652938
    iput-object v1, p0, LX/J9W;->r:LX/J9l;

    .line 2652939
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    iput-object v0, p0, LX/J9W;->q:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    .line 2652940
    return-void
.end method

.method public final a(LX/JBL;)V
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "updateCollection"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2652927
    iget-object v0, p0, LX/J9W;->o:LX/JAc;

    const-string v1, "Must initialize section first"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2652928
    if-eqz p1, :cond_0

    invoke-interface {p1}, LX/JBL;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, LX/JBL;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;->d()LX/0Px;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2652929
    :cond_0
    :goto_0
    return-void

    .line 2652930
    :cond_1
    iget-object v0, p0, LX/J9W;->r:LX/J9l;

    iget-object v1, p0, LX/J9W;->o:LX/JAc;

    invoke-interface {v1}, LX/JAb;->e()Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LX/J9l;->a(LX/JBL;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;)LX/0us;

    move-result-object v0

    iput-object v0, p0, LX/J9W;->s:LX/0us;

    .line 2652931
    const v0, 0x266e55c6

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel;)V
    .locals 7
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "setAppSection"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2652911
    iget-object v0, p0, LX/J9W;->o:LX/JAc;

    if-eqz v0, :cond_0

    .line 2652912
    invoke-virtual {p0}, LX/J9W;->a()V

    .line 2652913
    :cond_0
    iput-object p1, p0, LX/J9W;->o:LX/JAc;

    .line 2652914
    if-eqz p1, :cond_5

    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel;->k()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel$CollectionsModel;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel;->k()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel$CollectionsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel$CollectionsModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel;->k()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel$CollectionsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel$CollectionsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2652915
    if-nez v0, :cond_1

    .line 2652916
    :goto_1
    return-void

    .line 2652917
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel;->k()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel$CollectionsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel$CollectionsModel;->a()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, LX/JBL;

    .line 2652918
    iput-object v6, p0, LX/J9W;->p:LX/JBL;

    .line 2652919
    invoke-interface {v6}, LX/JAW;->d()LX/0Px;

    move-result-object v0

    .line 2652920
    invoke-static {v0}, LX/JCx;->a(Ljava/lang/Iterable;)Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    move-result-object v1

    iput-object v1, p0, LX/J9W;->q:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    .line 2652921
    iget-object v0, p0, LX/J9W;->q:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->LIST:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2652922
    new-instance v0, LX/J9o;

    iget-object v1, p0, LX/J9W;->f:LX/JDL;

    iget-object v2, p0, LX/J9W;->g:LX/J94;

    iget-object v3, p0, LX/J9W;->c:LX/9lP;

    iget-object v4, p0, LX/J9W;->k:Landroid/view/LayoutInflater;

    iget-object v5, p0, LX/J9W;->q:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    invoke-direct/range {v0 .. v5}, LX/J9o;-><init>(LX/JDL;LX/J94;LX/9lP;Landroid/view/LayoutInflater;Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;)V

    iput-object v0, p0, LX/J9W;->r:LX/J9l;

    .line 2652923
    :goto_2
    invoke-virtual {p0, v6}, LX/J9W;->a(LX/JBL;)V

    goto :goto_1

    .line 2652924
    :cond_2
    iget-object v0, p0, LX/J9W;->q:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->GRID:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, LX/J9W;->q:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2652925
    :cond_3
    new-instance v0, LX/J9q;

    iget-object v1, p0, LX/J9W;->d:LX/JDA;

    iget-object v2, p0, LX/J9W;->g:LX/J94;

    iget-object v3, p0, LX/J9W;->q:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    iget-object v4, p0, LX/J9W;->k:Landroid/view/LayoutInflater;

    invoke-direct {v0, v1, v2, v3, v4}, LX/J9q;-><init>(LX/JDA;LX/J94;Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;Landroid/view/LayoutInflater;)V

    iput-object v0, p0, LX/J9W;->r:LX/J9l;

    goto :goto_2

    .line 2652926
    :cond_4
    new-instance v0, LX/J9m;

    iget-object v1, p0, LX/J9W;->d:LX/JDA;

    iget-object v2, p0, LX/J9W;->g:LX/J94;

    iget-object v3, p0, LX/J9W;->q:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    iget-object v4, p0, LX/J9W;->k:Landroid/view/LayoutInflater;

    invoke-direct {v0, v1, v2, v3, v4}, LX/J9m;-><init>(LX/JDA;LX/J94;Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;Landroid/view/LayoutInflater;)V

    iput-object v0, p0, LX/J9W;->r:LX/J9l;

    goto :goto_2

    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 2652966
    iput-boolean p1, p0, LX/J9W;->n:Z

    .line 2652967
    const v0, -0x47b25de

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2652968
    return-void
.end method

.method public final getCount()I
    .locals 2

    .prologue
    .line 2652905
    iget-object v0, p0, LX/J9W;->o:LX/JAc;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/J9W;->r:LX/J9l;

    if-nez v0, :cond_2

    .line 2652906
    :cond_0
    const/4 v0, 0x0

    .line 2652907
    :cond_1
    :goto_0
    return v0

    .line 2652908
    :cond_2
    iget-object v0, p0, LX/J9W;->r:LX/J9l;

    invoke-interface {v0}, LX/J9k;->d()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    .line 2652909
    iget-boolean v1, p0, LX/J9W;->n:Z

    if-eqz v1, :cond_1

    .line 2652910
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2652891
    if-nez p1, :cond_0

    .line 2652892
    sget-object v0, LX/J9W;->l:Ljava/lang/Object;

    .line 2652893
    :goto_0
    return-object v0

    .line 2652894
    :cond_0
    add-int/lit8 v0, p1, -0x1

    .line 2652895
    if-nez v0, :cond_1

    .line 2652896
    iget-object v0, p0, LX/J9W;->p:LX/JBL;

    goto :goto_0

    .line 2652897
    :cond_1
    add-int/lit8 v0, v0, -0x1

    .line 2652898
    iget-object v1, p0, LX/J9W;->r:LX/J9l;

    invoke-interface {v1}, LX/J9k;->d()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 2652899
    iget-object v1, p0, LX/J9W;->r:LX/J9l;

    invoke-interface {v1, v0}, LX/J9k;->a(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 2652900
    :cond_2
    iget-object v1, p0, LX/J9W;->r:LX/J9l;

    invoke-interface {v1}, LX/J9k;->d()I

    move-result v1

    sub-int/2addr v0, v1

    .line 2652901
    if-nez v0, :cond_3

    .line 2652902
    iget-boolean v0, p0, LX/J9W;->n:Z

    if-eqz v0, :cond_3

    .line 2652903
    sget-object v0, LX/J9W;->m:Ljava/lang/Object;

    goto :goto_0

    .line 2652904
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Invalid adapter position. Probably loading state not maintained properly"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2652890
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 2652843
    invoke-virtual {p0, p1}, LX/J9W;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 2652844
    sget-object v1, LX/J9W;->l:Ljava/lang/Object;

    if-ne v0, v1, :cond_0

    .line 2652845
    sget-object v0, LX/J9V;->SECTION_HEADER:LX/J9V;

    invoke-virtual {v0}, LX/J9V;->ordinal()I

    move-result v0

    .line 2652846
    :goto_0
    return v0

    .line 2652847
    :cond_0
    instance-of v1, v0, LX/JAX;

    if-eqz v1, :cond_1

    .line 2652848
    sget-object v0, LX/J9V;->COLLECTION_HEADER:LX/J9V;

    invoke-virtual {v0}, LX/J9V;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2652849
    :cond_1
    sget-object v1, LX/J9W;->m:Ljava/lang/Object;

    if-ne v0, v1, :cond_2

    .line 2652850
    sget-object v0, LX/J9V;->LOADING_INDICATOR:LX/J9V;

    invoke-virtual {v0}, LX/J9V;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2652851
    :cond_2
    add-int/lit8 v0, p1, -0x2

    .line 2652852
    iget-object v1, p0, LX/J9W;->r:LX/J9l;

    invoke-interface {v1, v0}, LX/J9k;->b(I)LX/J9V;

    move-result-object v0

    invoke-virtual {v0}, LX/J9V;->ordinal()I

    move-result v0

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2652854
    invoke-virtual {p0, p1}, LX/J9W;->getItemViewType(I)I

    move-result v1

    .line 2652855
    invoke-static {}, LX/J9V;->values()[LX/J9V;

    move-result-object v2

    aget-object v1, v2, v1

    .line 2652856
    if-eqz p2, :cond_0

    .line 2652857
    :try_start_0
    instance-of v0, p2, Landroid/widget/TextView;

    if-eqz v0, :cond_6

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    const-string v2, "error_view"

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_0
    move v2, v0

    .line 2652858
    if-eqz v2, :cond_5

    .line 2652859
    :cond_0
    const/4 p2, 0x0

    .line 2652860
    sget-object v0, LX/J9U;->a:[I

    invoke-virtual {v1}, LX/J9V;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 2652861
    iget-object v0, p0, LX/J9W;->r:LX/J9l;

    iget-object v2, p0, LX/J9W;->b:Landroid/content/Context;

    invoke-interface {v0, v2, v1, p3}, LX/J9k;->a(Landroid/content/Context;LX/J9V;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    :goto_1
    move-object v2, v0

    .line 2652862
    :goto_2
    sget-object v5, LX/J9U;->a:[I

    invoke-virtual {v1}, LX/J9V;->ordinal()I

    move-result v1

    aget v1, v5, v1

    packed-switch v1, :pswitch_data_1

    .line 2652863
    invoke-virtual {p0, p1}, LX/J9W;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    .line 2652864
    iget-object v3, p0, LX/J9W;->r:LX/J9l;

    iget-object v5, p0, LX/J9W;->c:LX/9lP;

    invoke-interface {v3, v1, v2, v5}, LX/J9k;->a(Ljava/lang/Object;Landroid/view/View;LX/9lP;)V

    .line 2652865
    :cond_1
    :goto_3
    :pswitch_0
    return-object v2

    .line 2652866
    :pswitch_1
    move-object v0, v2

    check-cast v0, Lcom/facebook/timeline/aboutpage/views/header/CollectionSectionHeader;

    move-object v1, v0

    iget-object v3, p0, LX/J9W;->o:LX/JAc;

    invoke-virtual {v1, v3}, Lcom/facebook/timeline/aboutpage/views/header/CollectionSectionHeader;->a(LX/JAc;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 2652867
    :catch_0
    move-exception v1

    move-object v2, v1

    .line 2652868
    sget-object v1, LX/J9W;->a:Ljava/lang/Class;

    const-string v3, "getView"

    invoke-static {v1, v3, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2652869
    if-nez p1, :cond_3

    .line 2652870
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "section_header: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/J9W;->o:LX/JAc;

    invoke-interface {v3}, LX/JAb;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2652871
    :goto_4
    iget-object v3, p0, LX/J9W;->d:LX/JDA;

    iget-object v4, p0, LX/J9W;->b:Landroid/content/Context;

    const-string v5, "CollectionsCollectionAdapter.getView"

    invoke-virtual {v3, v2, v4, v1, v5}, LX/JDA;->a(Ljava/lang/Exception;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/widget/TextView;

    move-result-object v2

    goto :goto_3

    .line 2652872
    :pswitch_2
    :try_start_1
    invoke-direct {p0}, LX/J9W;->f()LX/JD9;

    move-result-object v5

    .line 2652873
    invoke-static {v2}, LX/J94;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v1

    check-cast v1, LX/JDe;

    .line 2652874
    const/4 v6, 0x0

    iget-object v7, v5, LX/JD9;->m:Ljava/lang/String;

    if-eqz v7, :cond_2

    move v3, v4

    :cond_2
    const/4 v7, 0x0

    invoke-virtual {v1, v5, v6, v3, v7}, LX/JDe;->a(LX/JD9;ZZZ)V

    .line 2652875
    iget-object v1, p0, LX/J9W;->o:LX/JAc;

    invoke-interface {v1}, LX/JAb;->mV_()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2652876
    iget-object v1, p0, LX/J9W;->h:LX/J8p;

    iget-object v3, p0, LX/J9W;->c:LX/9lP;

    .line 2652877
    iget-object v0, v3, LX/9lP;->a:Ljava/lang/String;

    move-object v3, v0

    .line 2652878
    iget-object v5, p0, LX/J9W;->c:LX/9lP;

    invoke-static {v5}, LX/J8p;->a(LX/9lP;)LX/9lQ;

    move-result-object v5

    iget-object v6, p0, LX/J9W;->o:LX/JAc;

    invoke-interface {v6}, LX/JAb;->mV_()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, LX/J9W;->p:LX/JBL;

    invoke-interface {v7}, LX/JBK;->j()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v3, v5, v6, v7}, LX/J8p;->a(Ljava/lang/String;LX/9lQ;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    .line 2652879
    :cond_3
    if-ne p1, v4, :cond_4

    .line 2652880
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "collection_header: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/J9W;->p:LX/JBL;

    invoke-interface {v3}, LX/JBK;->mW_()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_4

    .line 2652881
    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "item_view, type: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/J9W;->q:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_4

    :cond_5
    move-object v2, p2

    goto/16 :goto_2

    :cond_6
    :try_start_2
    const/4 v0, 0x0

    goto/16 :goto_0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 2652882
    :pswitch_3
    new-instance v0, Lcom/facebook/timeline/aboutpage/views/header/CollectionSectionHeader;

    iget-object v2, p0, LX/J9W;->b:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/facebook/timeline/aboutpage/views/header/CollectionSectionHeader;-><init>(Landroid/content/Context;)V

    .line 2652883
    iget-object v2, p0, LX/J9W;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 2652884
    const v5, 0x7f0b249c

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    const v6, 0x7f0b249a

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    add-int/2addr v5, v6

    .line 2652885
    const v6, 0x7f0b248d

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    const v7, 0x7f0b249c

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0, v6, v2, p2, v5}, Landroid/view/View;->setPadding(IIII)V

    goto/16 :goto_1

    .line 2652886
    :pswitch_4
    new-instance v0, LX/JDe;

    iget-object v2, p0, LX/J9W;->b:Landroid/content/Context;

    invoke-direct {v0, v2}, LX/JDe;-><init>(Landroid/content/Context;)V

    .line 2652887
    iget-object v5, p0, LX/J9W;->k:Landroid/view/LayoutInflater;

    invoke-static {v0, v5}, LX/J94;->b(Landroid/view/View;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_1

    .line 2652888
    :pswitch_5
    iget-object v0, p0, LX/J9W;->i:LX/0hx;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, LX/0hx;->a(Z)V

    .line 2652889
    iget-object v0, p0, LX/J9W;->k:Landroid/view/LayoutInflater;

    const v2, 0x7f0314f0

    invoke-virtual {v0, v2, p3, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2652853
    sget v0, LX/J9V;->NUM_VIEW_TYPES:I

    return v0
.end method
