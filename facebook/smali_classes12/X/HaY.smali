.class public LX/HaY;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/Hba;

.field public b:Landroid/widget/FrameLayout;

.field public c:Landroid/graphics/drawable/TransitionDrawable;

.field public d:LX/HaX;


# direct methods
.method public constructor <init>(Landroid/widget/FrameLayout;LX/Hba;)V
    .locals 4

    .prologue
    .line 2483956
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2483957
    iput-object p1, p0, LX/HaY;->b:Landroid/widget/FrameLayout;

    .line 2483958
    iput-object p2, p0, LX/HaY;->a:LX/Hba;

    .line 2483959
    sget-object v0, LX/HaX;->INITIAL:LX/HaX;

    iput-object v0, p0, LX/HaY;->d:LX/HaX;

    .line 2483960
    sget-object v0, LX/HaW;->a:[I

    iget-object v1, p0, LX/HaY;->a:LX/Hba;

    invoke-virtual {v1}, LX/Hba;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2483961
    :goto_0
    return-void

    .line 2483962
    :pswitch_0
    iget-object v0, p0, LX/HaY;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0312d1

    iget-object p1, p0, LX/HaY;->b:Landroid/widget/FrameLayout;

    const/4 p2, 0x1

    invoke-virtual {v0, v1, p1, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 2483963
    goto :goto_0

    .line 2483964
    :pswitch_1
    const/4 p2, 0x1

    .line 2483965
    iget-object v0, p0, LX/HaY;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0312d5

    iget-object v2, p0, LX/HaY;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1, v2, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2483966
    const/4 v1, 0x2

    new-array v1, v1, [Landroid/graphics/drawable/Drawable;

    .line 2483967
    const/4 v2, 0x0

    iget-object v3, p0, LX/HaY;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const p1, 0x7f0217ae

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    aput-object v3, v1, v2

    .line 2483968
    iget-object v2, p0, LX/HaY;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0217a9    # 1.729225E38f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aput-object v2, v1, p2

    .line 2483969
    new-instance v2, Landroid/graphics/drawable/TransitionDrawable;

    invoke-direct {v2, v1}, Landroid/graphics/drawable/TransitionDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    iput-object v2, p0, LX/HaY;->c:Landroid/graphics/drawable/TransitionDrawable;

    .line 2483970
    const v1, 0x7f0d2be1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 2483971
    iget-object v1, p0, LX/HaY;->c:Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2483972
    goto :goto_0

    .line 2483973
    :pswitch_2
    const/4 p2, 0x1

    .line 2483974
    iget-object v0, p0, LX/HaY;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0312d9

    iget-object v2, p0, LX/HaY;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1, v2, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2483975
    const/4 v1, 0x2

    new-array v1, v1, [Landroid/graphics/drawable/Drawable;

    .line 2483976
    const/4 v2, 0x0

    iget-object v3, p0, LX/HaY;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const p1, 0x7f0217ae

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    aput-object v3, v1, v2

    .line 2483977
    iget-object v2, p0, LX/HaY;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0217a9    # 1.729225E38f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aput-object v2, v1, p2

    .line 2483978
    new-instance v2, Landroid/graphics/drawable/TransitionDrawable;

    invoke-direct {v2, v1}, Landroid/graphics/drawable/TransitionDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    iput-object v2, p0, LX/HaY;->c:Landroid/graphics/drawable/TransitionDrawable;

    .line 2483979
    const v1, 0x7f0d2bee

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 2483980
    iget-object v1, p0, LX/HaY;->c:Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2483981
    goto/16 :goto_0

    .line 2483982
    :pswitch_3
    const/4 p2, 0x1

    .line 2483983
    iget-object v0, p0, LX/HaY;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0312db

    iget-object v2, p0, LX/HaY;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1, v2, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2483984
    const/4 v1, 0x2

    new-array v1, v1, [Landroid/graphics/drawable/Drawable;

    .line 2483985
    const/4 v2, 0x0

    iget-object v3, p0, LX/HaY;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const p1, 0x7f0217ae

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    aput-object v3, v1, v2

    .line 2483986
    iget-object v2, p0, LX/HaY;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0217a9    # 1.729225E38f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aput-object v2, v1, p2

    .line 2483987
    new-instance v2, Landroid/graphics/drawable/TransitionDrawable;

    invoke-direct {v2, v1}, Landroid/graphics/drawable/TransitionDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    iput-object v2, p0, LX/HaY;->c:Landroid/graphics/drawable/TransitionDrawable;

    .line 2483988
    const v1, 0x7f0d2bf7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 2483989
    iget-object v1, p0, LX/HaY;->c:Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2483990
    goto/16 :goto_0

    .line 2483991
    :pswitch_4
    iget-object v0, p0, LX/HaY;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0312cb

    iget-object v2, p0, LX/HaY;->b:Landroid/widget/FrameLayout;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 2483992
    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static a(LX/HaY;IZ)V
    .locals 1

    .prologue
    .line 2483993
    iget-object v0, p0, LX/HaY;->c:Landroid/graphics/drawable/TransitionDrawable;

    if-eqz v0, :cond_0

    .line 2483994
    if-eqz p2, :cond_1

    .line 2483995
    iget-object v0, p0, LX/HaY;->c:Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 2483996
    :cond_0
    :goto_0
    return-void

    .line 2483997
    :cond_1
    iget-object v0, p0, LX/HaY;->c:Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/TransitionDrawable;->reverseTransition(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2483998
    iget-object v0, p0, LX/HaY;->d:LX/HaX;

    sget-object v1, LX/HaX;->INITIAL:LX/HaX;

    if-eq v0, v1, :cond_0

    .line 2483999
    :goto_0
    return-void

    .line 2484000
    :cond_0
    sget-object v0, LX/HaW;->a:[I

    iget-object v1, p0, LX/HaY;->a:LX/Hba;

    invoke-virtual {v1}, LX/Hba;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2484001
    :goto_1
    sget-object v0, LX/HaX;->ANIMATED:LX/HaX;

    iput-object v0, p0, LX/HaY;->d:LX/HaX;

    goto :goto_0

    .line 2484002
    :pswitch_0
    const/16 v0, 0x1f4

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, LX/HaY;->a(LX/HaY;IZ)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
