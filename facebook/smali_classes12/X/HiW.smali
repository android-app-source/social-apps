.class public final LX/HiW;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/HiX;


# direct methods
.method public constructor <init>(LX/HiX;)V
    .locals 0

    .prologue
    .line 2497441
    iput-object p1, p0, LX/HiW;->a:LX/HiX;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/location/Address;)V
    .locals 4

    .prologue
    .line 2497442
    iget-object v0, p0, LX/HiW;->a:LX/HiX;

    iget-object v0, v0, LX/HiX;->d:Lcom/facebook/addresstypeahead/helper/AddressTypeAheadParams;

    if-nez v0, :cond_2

    const/4 v0, 0x0

    move-object v1, v0

    .line 2497443
    :goto_0
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2497444
    iget-object v0, p0, LX/HiW;->a:LX/HiX;

    iget-object v0, v0, LX/HiX;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ibp;

    .line 2497445
    const-string v3, "RideDestUpdateAddressTypeAheadResultHandler"

    move-object v3, v3

    .line 2497446
    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2497447
    new-instance v1, LX/HiV;

    invoke-direct {v1, p0}, LX/HiV;-><init>(LX/HiW;)V

    .line 2497448
    iput-object v1, v0, LX/Ibp;->e:LX/HiV;

    .line 2497449
    iget-object v1, p0, LX/HiW;->a:LX/HiX;

    iget-object v1, v1, LX/HiX;->d:Lcom/facebook/addresstypeahead/helper/AddressTypeAheadParams;

    invoke-virtual {v0, p1, v1}, LX/Ibp;->a(Landroid/location/Address;Lcom/facebook/addresstypeahead/helper/AddressTypeAheadParams;)V

    .line 2497450
    :cond_1
    :goto_1
    return-void

    .line 2497451
    :cond_2
    iget-object v0, p0, LX/HiW;->a:LX/HiX;

    iget-object v0, v0, LX/HiX;->d:Lcom/facebook/addresstypeahead/helper/AddressTypeAheadParams;

    .line 2497452
    iget-object v1, v0, Lcom/facebook/addresstypeahead/helper/AddressTypeAheadParams;->b:Ljava/lang/String;

    move-object v0, v1

    .line 2497453
    move-object v1, v0

    goto :goto_0

    .line 2497454
    :cond_3
    iget-object v0, p0, LX/HiW;->a:LX/HiX;

    iget-object v0, v0, LX/HiX;->c:LX/HiJ;

    if-eqz v0, :cond_1

    .line 2497455
    iget-object v0, p0, LX/HiW;->a:LX/HiX;

    iget-object v0, v0, LX/HiX;->c:LX/HiJ;

    invoke-virtual {v0, p1}, LX/HiJ;->a(Landroid/location/Address;)V

    goto :goto_1
.end method
