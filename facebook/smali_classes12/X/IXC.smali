.class public LX/IXC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/48R;


# static fields
.field private static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/11i;

.field private final c:LX/0So;

.field public final d:LX/IXG;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8bL;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0yH;

.field private i:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2585591
    new-instance v0, LX/IXB;

    invoke-direct {v0}, LX/IXB;-><init>()V

    sput-object v0, LX/IXC;->a:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(LX/11i;LX/0So;LX/IXG;LX/0Ot;LX/0Ot;LX/0Ot;LX/0yH;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/11i;",
            "LX/0So;",
            "LX/IXG;",
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/8bL;",
            ">;",
            "Lcom/facebook/iorg/common/zero/interfaces/ZeroFeatureVisibilityHelper;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2585592
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2585593
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/IXC;->i:J

    .line 2585594
    iput-object p1, p0, LX/IXC;->b:LX/11i;

    .line 2585595
    iput-object p2, p0, LX/IXC;->c:LX/0So;

    .line 2585596
    iput-object p3, p0, LX/IXC;->d:LX/IXG;

    .line 2585597
    iput-object p4, p0, LX/IXC;->e:LX/0Ot;

    .line 2585598
    iput-object p5, p0, LX/IXC;->f:LX/0Ot;

    .line 2585599
    iput-object p6, p0, LX/IXC;->g:LX/0Ot;

    .line 2585600
    iput-object p7, p0, LX/IXC;->h:LX/0yH;

    .line 2585601
    return-void
.end method

.method public static a(Landroid/content/Intent;)Ljava/lang/String;
    .locals 1
    .param p0    # Landroid/content/Intent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2585602
    if-nez p0, :cond_0

    .line 2585603
    const/4 v0, 0x0

    .line 2585604
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "extra_instant_articles_id"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private c(Landroid/content/Intent;Landroid/content/Context;)Z
    .locals 8
    .param p1    # Landroid/content/Intent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2585605
    iget-object v0, p0, LX/IXC;->h:LX/0yH;

    sget-object v1, LX/0yY;->EXTERNAL_URLS_INTERSTITIAL:LX/0yY;

    invoke-virtual {v0, v1}, LX/0yH;->a(LX/0yY;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/IXC;->h:LX/0yH;

    sget-object v1, LX/0yY;->INSTANT_ARTICLE_SETTING:LX/0yY;

    invoke-virtual {v0, v1}, LX/0yH;->a(LX/0yY;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2585606
    :cond_0
    :goto_0
    return v3

    .line 2585607
    :cond_1
    const/4 v1, 0x0

    .line 2585608
    if-eqz p1, :cond_2

    if-nez p2, :cond_b

    :cond_2
    move v0, v1

    .line 2585609
    :goto_1
    move v0, v0

    .line 2585610
    if-eqz v0, :cond_0

    .line 2585611
    const-string v0, "RichDocumentIntentHandler#launchInstantArticle"

    const v1, -0x52a53b6f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2585612
    iget-object v0, p0, LX/IXC;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iget-wide v4, p0, LX/IXC;->i:J

    sub-long/2addr v0, v4

    const-wide/16 v4, 0x5dc

    cmp-long v0, v0, v4

    if-gez v0, :cond_3

    move v3, v2

    .line 2585613
    goto :goto_0

    .line 2585614
    :cond_3
    const-string v0, "external_click_time"

    iget-object v1, p0, LX/IXC;->c:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v4

    invoke-virtual {p1, v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2585615
    iget-object v0, p0, LX/IXC;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, LX/IXC;->i:J

    .line 2585616
    const-string v0, "open_action"

    const-string v1, "clicked"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2585617
    iget-object v0, p0, LX/IXC;->b:LX/11i;

    sget-object v1, LX/IXZ;->a:LX/IXY;

    invoke-interface {v0, v1}, LX/11i;->a(LX/0Pq;)LX/11o;

    .line 2585618
    const v0, -0x34d2d2fc    # -1.1349252E7f

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2585619
    const-class v0, LX/0ew;

    invoke-static {p2, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ew;

    .line 2585620
    if-eqz v0, :cond_9

    invoke-interface {v0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->c()Z

    move-result v1

    if-eqz v1, :cond_9

    const/4 v5, 0x0

    .line 2585621
    iget-object v1, p0, LX/IXC;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Uh;

    const/16 v4, 0x3da

    invoke-virtual {v1, v4, v5}, LX/0Uh;->a(IZ)Z

    move-result v1

    .line 2585622
    if-eqz v1, :cond_e

    .line 2585623
    const/4 v1, 0x1

    .line 2585624
    :goto_2
    move v1, v1

    .line 2585625
    if-eqz v1, :cond_9

    .line 2585626
    new-instance v4, Lcom/facebook/instantarticles/InstantArticlesFragment;

    invoke-direct {v4}, Lcom/facebook/instantarticles/InstantArticlesFragment;-><init>()V

    .line 2585627
    invoke-static {p1}, LX/IXC;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v1

    .line 2585628
    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_f

    invoke-virtual {p1}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_f

    .line 2585629
    iget-object v1, p0, LX/IXC;->d:LX/IXG;

    invoke-virtual {p1}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, p2, v5}, LX/398;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2585630
    invoke-static {v1}, LX/IXC;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_11

    .line 2585631
    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 2585632
    const-string v5, "extra_instant_articles_click_url"

    const-string v6, "extra_instant_articles_canonical_url"

    invoke-virtual {v1, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2585633
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    .line 2585634
    if-eqz v5, :cond_4

    .line 2585635
    invoke-virtual {v1, v5}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 2585636
    :cond_4
    :goto_3
    move-object v1, v1

    .line 2585637
    const-string v5, "extra_instant_articles_can_log_open_link_on_settle"

    invoke-virtual {v1, v5, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2585638
    if-eqz v1, :cond_9

    .line 2585639
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2585640
    const-string v6, "richdocument_fragment_tag"

    invoke-virtual {v1, v6, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2585641
    invoke-virtual {v4, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2585642
    iget-object v1, p0, LX/IXC;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8bL;

    invoke-virtual {v1}, LX/8bL;->b()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 2585643
    invoke-interface {v0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v1

    .line 2585644
    invoke-static {v1}, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->b(LX/0gc;)Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;

    move-result-object v5

    .line 2585645
    if-nez v5, :cond_5

    .line 2585646
    new-instance v5, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;

    invoke-direct {v5}, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;-><init>()V

    .line 2585647
    :cond_5
    move-object v5, v5

    .line 2585648
    invoke-virtual {v5}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v1

    if-nez v1, :cond_6

    .line 2585649
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 2585650
    const-string v1, "extra_parent_article_click_source"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2585651
    sget-object v7, LX/IXC;->a:Ljava/util/Set;

    invoke-interface {v7, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    move v1, v2

    .line 2585652
    :goto_4
    const-string v3, "extra_instant_articles_featured_articles_launch_ok"

    invoke-virtual {v6, v3, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2585653
    invoke-virtual {v5, v6}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2585654
    invoke-interface {v0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v1

    sget-object v3, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->m:Ljava/lang/String;

    invoke-virtual {v5, v1, v3}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2585655
    :cond_6
    invoke-virtual {v5, v4}, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->a(Lcom/facebook/richdocument/view/carousel/PageableFragment;)V

    .line 2585656
    :goto_5
    invoke-interface {v0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->b()Z

    move v3, v2

    .line 2585657
    goto/16 :goto_0

    :cond_7
    move v1, v3

    .line 2585658
    goto :goto_4

    .line 2585659
    :cond_8
    invoke-interface {v0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v4, v1, v5}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    goto :goto_5

    .line 2585660
    :cond_9
    new-instance v0, Landroid/content/ComponentName;

    const-class v1, Lcom/facebook/instantarticles/InstantArticlesActivity;

    invoke-direct {v0, p2, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 2585661
    if-eqz p1, :cond_a

    if-eqz p2, :cond_a

    invoke-virtual {p1}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 2585662
    :cond_a
    :goto_6
    goto/16 :goto_0

    .line 2585663
    :cond_b
    invoke-static {p1}, LX/IXC;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    .line 2585664
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_c

    invoke-virtual {p1}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_c

    .line 2585665
    iget-object v0, p0, LX/IXC;->d:LX/IXG;

    invoke-virtual {p1}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, p2, v4}, LX/398;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2585666
    invoke-static {v0}, LX/IXC;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    .line 2585667
    :cond_c
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_d

    const/4 v0, 0x1

    goto/16 :goto_1

    :cond_d
    move v0, v1

    goto/16 :goto_1

    .line 2585668
    :cond_e
    iget-object v1, p0, LX/IXC;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0ad;

    sget-short v4, LX/2yD;->C:S

    invoke-interface {v1, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v1

    goto/16 :goto_2

    .line 2585669
    :cond_f
    invoke-virtual {p1}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 2585670
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 2585671
    const-string v5, "extra_instant_articles_click_url"

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "extra_instant_articles_canonical_url"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 2585672
    :cond_10
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 2585673
    const-string v5, "extra_instant_articles_click_url"

    invoke-virtual {p1}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 2585674
    :cond_11
    const/4 v1, 0x0

    goto/16 :goto_3

    .line 2585675
    :cond_12
    invoke-virtual {p1}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v0

    .line 2585676
    iget-object v1, p0, LX/IXC;->d:LX/IXG;

    invoke-virtual {v1, p2, v0}, LX/398;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2585677
    if-eqz v0, :cond_a

    .line 2585678
    const-string v1, "extra_instant_articles_canonical_url"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2585679
    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 2585680
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2585681
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto/16 :goto_6
.end method


# virtual methods
.method public final a(Landroid/content/Intent;ILandroid/app/Activity;)Z
    .locals 1

    .prologue
    .line 2585682
    invoke-direct {p0, p1, p3}, LX/IXC;->c(Landroid/content/Intent;Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)Z
    .locals 1

    .prologue
    .line 2585683
    invoke-virtual {p3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/IXC;->c(Landroid/content/Intent;Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/content/Intent;Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 2585684
    invoke-direct {p0, p1, p2}, LX/IXC;->c(Landroid/content/Intent;Landroid/content/Context;)Z

    move-result v0

    return v0
.end method
