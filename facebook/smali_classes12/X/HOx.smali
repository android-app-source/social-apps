.class public LX/HOx;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/HOw;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2461209
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 2461210
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;LX/1PT;LX/1SX;Ljava/lang/Boolean;LX/0o8;LX/8YB;J)LX/HOw;
    .locals 29

    .prologue
    .line 2461211
    new-instance v3, LX/HOw;

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v12

    check-cast v12, LX/0SG;

    const-class v2, LX/E2N;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v13

    check-cast v13, LX/E2N;

    const-class v2, LX/3Tp;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v14

    check-cast v14, LX/3Tp;

    invoke-static/range {p0 .. p0}, LX/E1j;->a(LX/0QB;)LX/E1j;

    move-result-object v15

    check-cast v15, LX/E1j;

    invoke-static/range {p0 .. p0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v16

    check-cast v16, LX/0bH;

    invoke-static/range {p0 .. p0}, LX/1Db;->a(LX/0QB;)LX/1Db;

    move-result-object v17

    check-cast v17, LX/1Db;

    invoke-static/range {p0 .. p0}, LX/AjP;->a(LX/0QB;)LX/AjP;

    move-result-object v18

    check-cast v18, LX/AjP;

    invoke-static/range {p0 .. p0}, LX/1My;->a(LX/0QB;)LX/1My;

    move-result-object v19

    check-cast v19, LX/1My;

    invoke-static/range {p0 .. p0}, LX/1Db;->a(LX/0QB;)LX/1Db;

    move-result-object v20

    check-cast v20, LX/1Db;

    invoke-static/range {p0 .. p0}, LX/E1l;->a(LX/0QB;)LX/E1l;

    move-result-object v21

    check-cast v21, LX/E1l;

    invoke-static/range {p0 .. p0}, LX/CfF;->a(LX/0QB;)LX/CfF;

    move-result-object v22

    check-cast v22, LX/CfF;

    invoke-static/range {p0 .. p0}, LX/CvY;->a(LX/0QB;)LX/CvY;

    move-result-object v23

    check-cast v23, LX/CvY;

    invoke-static/range {p0 .. p0}, LX/Cfw;->a(LX/0QB;)LX/Cfw;

    move-result-object v24

    check-cast v24, LX/Cfw;

    invoke-static/range {p0 .. p0}, LX/3mH;->a(LX/0QB;)LX/3mH;

    move-result-object v25

    check-cast v25, LX/3mH;

    invoke-static/range {p0 .. p0}, LX/967;->a(LX/0QB;)LX/967;

    move-result-object v26

    check-cast v26, LX/967;

    invoke-static/range {p0 .. p0}, LX/16I;->a(LX/0QB;)LX/16I;

    move-result-object v27

    check-cast v27, LX/16I;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v28

    check-cast v28, LX/0ad;

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    move-wide/from16 v10, p7

    invoke-direct/range {v3 .. v28}, LX/HOw;-><init>(Landroid/content/Context;LX/1PT;LX/1SX;Ljava/lang/Boolean;LX/0o8;LX/8YB;JLX/0SG;LX/E2N;LX/3Tp;LX/E1j;LX/0bH;LX/1Db;LX/AjP;LX/1My;LX/1Db;LX/E1l;LX/CfF;LX/CvY;LX/Cfw;LX/3mH;LX/967;LX/16I;LX/0ad;)V

    .line 2461212
    return-object v3
.end method
