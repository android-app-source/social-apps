.class public LX/Hma;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field public static final e:LX/0Tn;

.field private static volatile f:LX/Hma;


# instance fields
.field public final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final b:Landroid/content/pm/PackageInfo;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2500863
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "beam/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 2500864
    sput-object v0, LX/Hma;->c:LX/0Tn;

    const-string v1, "version_code"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Hma;->d:LX/0Tn;

    .line 2500865
    sget-object v0, LX/Hma;->c:LX/0Tn;

    const-string v1, "beam_transaction_id"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Hma;->e:LX/0Tn;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;Landroid/content/pm/PackageInfo;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2500859
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2500860
    iput-object p1, p0, LX/Hma;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2500861
    iput-object p2, p0, LX/Hma;->b:Landroid/content/pm/PackageInfo;

    .line 2500862
    return-void
.end method

.method public static a(LX/0QB;)LX/Hma;
    .locals 5

    .prologue
    .line 2500846
    sget-object v0, LX/Hma;->f:LX/Hma;

    if-nez v0, :cond_1

    .line 2500847
    const-class v1, LX/Hma;

    monitor-enter v1

    .line 2500848
    :try_start_0
    sget-object v0, LX/Hma;->f:LX/Hma;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2500849
    if-eqz v2, :cond_0

    .line 2500850
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2500851
    new-instance p0, LX/Hma;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0WE;->a(LX/0QB;)Landroid/content/pm/PackageInfo;

    move-result-object v4

    check-cast v4, Landroid/content/pm/PackageInfo;

    invoke-direct {p0, v3, v4}, LX/Hma;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;Landroid/content/pm/PackageInfo;)V

    .line 2500852
    move-object v0, p0

    .line 2500853
    sput-object v0, LX/Hma;->f:LX/Hma;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2500854
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2500855
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2500856
    :cond_1
    sget-object v0, LX/Hma;->f:LX/Hma;

    return-object v0

    .line 2500857
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2500858
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2500843
    iget-object v0, p0, LX/Hma;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/Hma;->d:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    sget-object v1, LX/Hma;->e:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2500844
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2500845
    iget-object v0, p0, LX/Hma;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/Hma;->e:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
