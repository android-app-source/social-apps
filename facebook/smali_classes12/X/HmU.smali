.class public final enum LX/HmU;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HmU;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/HmU;

.field public static final enum INITIALIZE:LX/HmU;

.field public static final enum RECEIVER_ERROR:LX/HmU;

.field public static final enum RECEIVE_APK:LX/HmU;

.field public static final enum RECEIVE_INTEND_TO_SEND:LX/HmU;

.field public static final enum SENDER_ERROR:LX/HmU;

.field public static final enum SEND_RECEIVE_PREFLIGHT:LX/HmU;

.field public static final enum VERIFY_APK:LX/HmU;

.field public static final enum WAIT_INITIALIZE:LX/HmU;

.field public static final enum WAIT_INSTALL_APK:LX/HmU;

.field public static final enum WAIT_RECEIVE_APK:LX/HmU;

.field public static final enum WAIT_RECEIVE_INTEND_TO_SEND:LX/HmU;

.field public static final enum WAIT_SEND_RECEIVE_PREFLIGHT:LX/HmU;

.field public static final enum WAIT_VERIFY_APK:LX/HmU;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2500725
    new-instance v0, LX/HmU;

    const-string v1, "WAIT_INITIALIZE"

    invoke-direct {v0, v1, v3}, LX/HmU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HmU;->WAIT_INITIALIZE:LX/HmU;

    .line 2500726
    new-instance v0, LX/HmU;

    const-string v1, "INITIALIZE"

    invoke-direct {v0, v1, v4}, LX/HmU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HmU;->INITIALIZE:LX/HmU;

    .line 2500727
    new-instance v0, LX/HmU;

    const-string v1, "WAIT_SEND_RECEIVE_PREFLIGHT"

    invoke-direct {v0, v1, v5}, LX/HmU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HmU;->WAIT_SEND_RECEIVE_PREFLIGHT:LX/HmU;

    .line 2500728
    new-instance v0, LX/HmU;

    const-string v1, "SEND_RECEIVE_PREFLIGHT"

    invoke-direct {v0, v1, v6}, LX/HmU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HmU;->SEND_RECEIVE_PREFLIGHT:LX/HmU;

    .line 2500729
    new-instance v0, LX/HmU;

    const-string v1, "WAIT_RECEIVE_INTEND_TO_SEND"

    invoke-direct {v0, v1, v7}, LX/HmU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HmU;->WAIT_RECEIVE_INTEND_TO_SEND:LX/HmU;

    .line 2500730
    new-instance v0, LX/HmU;

    const-string v1, "RECEIVE_INTEND_TO_SEND"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/HmU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HmU;->RECEIVE_INTEND_TO_SEND:LX/HmU;

    .line 2500731
    new-instance v0, LX/HmU;

    const-string v1, "WAIT_RECEIVE_APK"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/HmU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HmU;->WAIT_RECEIVE_APK:LX/HmU;

    .line 2500732
    new-instance v0, LX/HmU;

    const-string v1, "RECEIVE_APK"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/HmU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HmU;->RECEIVE_APK:LX/HmU;

    .line 2500733
    new-instance v0, LX/HmU;

    const-string v1, "WAIT_VERIFY_APK"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/HmU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HmU;->WAIT_VERIFY_APK:LX/HmU;

    .line 2500734
    new-instance v0, LX/HmU;

    const-string v1, "VERIFY_APK"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/HmU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HmU;->VERIFY_APK:LX/HmU;

    .line 2500735
    new-instance v0, LX/HmU;

    const-string v1, "WAIT_INSTALL_APK"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/HmU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HmU;->WAIT_INSTALL_APK:LX/HmU;

    .line 2500736
    new-instance v0, LX/HmU;

    const-string v1, "SENDER_ERROR"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/HmU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HmU;->SENDER_ERROR:LX/HmU;

    .line 2500737
    new-instance v0, LX/HmU;

    const-string v1, "RECEIVER_ERROR"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/HmU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HmU;->RECEIVER_ERROR:LX/HmU;

    .line 2500738
    const/16 v0, 0xd

    new-array v0, v0, [LX/HmU;

    sget-object v1, LX/HmU;->WAIT_INITIALIZE:LX/HmU;

    aput-object v1, v0, v3

    sget-object v1, LX/HmU;->INITIALIZE:LX/HmU;

    aput-object v1, v0, v4

    sget-object v1, LX/HmU;->WAIT_SEND_RECEIVE_PREFLIGHT:LX/HmU;

    aput-object v1, v0, v5

    sget-object v1, LX/HmU;->SEND_RECEIVE_PREFLIGHT:LX/HmU;

    aput-object v1, v0, v6

    sget-object v1, LX/HmU;->WAIT_RECEIVE_INTEND_TO_SEND:LX/HmU;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/HmU;->RECEIVE_INTEND_TO_SEND:LX/HmU;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/HmU;->WAIT_RECEIVE_APK:LX/HmU;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/HmU;->RECEIVE_APK:LX/HmU;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/HmU;->WAIT_VERIFY_APK:LX/HmU;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/HmU;->VERIFY_APK:LX/HmU;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/HmU;->WAIT_INSTALL_APK:LX/HmU;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/HmU;->SENDER_ERROR:LX/HmU;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/HmU;->RECEIVER_ERROR:LX/HmU;

    aput-object v2, v0, v1

    sput-object v0, LX/HmU;->$VALUES:[LX/HmU;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2500739
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HmU;
    .locals 1

    .prologue
    .line 2500740
    const-class v0, LX/HmU;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HmU;

    return-object v0
.end method

.method public static values()[LX/HmU;
    .locals 1

    .prologue
    .line 2500741
    sget-object v0, LX/HmU;->$VALUES:[LX/HmU;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HmU;

    return-object v0
.end method
