.class public final LX/JQp;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JQr;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1Pm;

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/JQk;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/lang/String;

.field public d:Z

.field public final synthetic e:LX/JQr;


# direct methods
.method public constructor <init>(LX/JQr;)V
    .locals 1

    .prologue
    .line 2692259
    iput-object p1, p0, LX/JQp;->e:LX/JQr;

    .line 2692260
    move-object v0, p1

    .line 2692261
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2692262
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2692263
    const-string v0, "ActiveNowHScrollComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2692264
    if-ne p0, p1, :cond_1

    .line 2692265
    :cond_0
    :goto_0
    return v0

    .line 2692266
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2692267
    goto :goto_0

    .line 2692268
    :cond_3
    check-cast p1, LX/JQp;

    .line 2692269
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2692270
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2692271
    if-eq v2, v3, :cond_0

    .line 2692272
    iget-object v2, p0, LX/JQp;->a:LX/1Pm;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JQp;->a:LX/1Pm;

    iget-object v3, p1, LX/JQp;->a:LX/1Pm;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2692273
    goto :goto_0

    .line 2692274
    :cond_5
    iget-object v2, p1, LX/JQp;->a:LX/1Pm;

    if-nez v2, :cond_4

    .line 2692275
    :cond_6
    iget-object v2, p0, LX/JQp;->b:LX/0Px;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/JQp;->b:LX/0Px;

    iget-object v3, p1, LX/JQp;->b:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2692276
    goto :goto_0

    .line 2692277
    :cond_8
    iget-object v2, p1, LX/JQp;->b:LX/0Px;

    if-nez v2, :cond_7

    .line 2692278
    :cond_9
    iget-object v2, p0, LX/JQp;->c:Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/JQp;->c:Ljava/lang/String;

    iget-object v3, p1, LX/JQp;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2692279
    goto :goto_0

    .line 2692280
    :cond_b
    iget-object v2, p1, LX/JQp;->c:Ljava/lang/String;

    if-nez v2, :cond_a

    .line 2692281
    :cond_c
    iget-boolean v2, p0, LX/JQp;->d:Z

    iget-boolean v3, p1, LX/JQp;->d:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 2692282
    goto :goto_0
.end method
