.class public LX/I8k;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field private A:LX/4yZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4yZ",
            "<",
            "Ljava/lang/Integer;",
            "LX/I8N",
            "<",
            "LX/1Cw;",
            ">;>;"
        }
    .end annotation
.end field

.field private B:Ljava/util/IdentityHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/IdentityHashMap",
            "<",
            "LX/1Cw;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public C:I

.field public D:LX/0g8;

.field public E:Z

.field public F:LX/IBq;

.field private G:LX/Cg4;

.field public H:LX/2ja;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public I:LX/I8j;

.field private J:LX/0ad;

.field public K:LX/I7G;

.field public L:LX/I7H;

.field private M:LX/0Uh;

.field public N:Ljava/lang/String;

.field public O:Z

.field public final a:LX/0Zb;

.field public final b:Landroid/content/Context;

.field public final c:LX/I8R;

.field public final d:LX/I8W;

.field public final e:LX/I8O;

.field public final f:LX/0kv;

.field public final g:LX/1Qq;

.field private final h:LX/1Qq;

.field public final i:LX/I8m;

.field private final j:LX/I8o;

.field public final k:LX/I8V;

.field public final l:LX/I8T;

.field private final m:LX/1Db;

.field private n:I

.field public final o:Lcom/facebook/events/common/EventAnalyticsParams;

.field public p:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field public q:LX/E93;

.field private r:LX/I8r;

.field public s:Lcom/facebook/events/model/Event;

.field public final t:LX/0So;

.field public final u:LX/1P0;

.field private final v:LX/1vi;

.field public final w:LX/I5f;

.field public x:LX/I8N;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private y:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/I8N;",
            ">;"
        }
    .end annotation
.end field

.field private z:LX/4yZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4yZ",
            "<",
            "Ljava/lang/Integer;",
            "LX/I8N",
            "<*>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/I5f;LX/I7i;LX/DBC;Lcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/events/permalink/EventPermalinkFragment;Landroid/content/Context;LX/E93;LX/I91;LX/I98;LX/1P0;LX/0Zb;LX/I89;LX/1DS;LX/0Ot;LX/0Ot;LX/0kv;LX/1Db;LX/IBJ;LX/I8T;LX/I8u;LX/0So;LX/1vi;LX/IBk;LX/0ad;LX/0Uh;)V
    .locals 13
    .param p1    # LX/I5f;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/I7i;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/DBC;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/events/common/EventAnalyticsParams;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/events/permalink/EventPermalinkFragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/E93;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # LX/I91;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # LX/I98;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p10    # LX/1P0;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/I5f;",
            "LX/I7i;",
            "LX/DBC;",
            "Lcom/facebook/events/common/EventAnalyticsParams;",
            "Lcom/facebook/events/feed/ui/EventFeedStoryPinMutator$StoryUpdater;",
            "Landroid/content/Context;",
            "LX/E93;",
            "LX/I91;",
            "LX/I98;",
            "LX/1P0;",
            "LX/0Zb;",
            "LX/I89;",
            "LX/1DS;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/events/permalink/multirow/EventPermalinkRootPartSelector;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/events/permalink/multirow/EventFeedComposerRootPartDefinition;",
            ">;",
            "LX/0kv;",
            "LX/1Db;",
            "LX/IBJ;",
            "LX/I8T;",
            "LX/I8u;",
            "LX/0So;",
            "LX/1vi;",
            "LX/IBk;",
            "LX/0ad;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2541821
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2541822
    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v2

    iput-object v2, p0, LX/I8k;->y:LX/0Px;

    .line 2541823
    const/4 v2, 0x0

    iput-object v2, p0, LX/I8k;->N:Ljava/lang/String;

    .line 2541824
    const/4 v2, 0x1

    iput-boolean v2, p0, LX/I8k;->O:Z

    .line 2541825
    move-object/from16 v0, p25

    iput-object v0, p0, LX/I8k;->M:LX/0Uh;

    .line 2541826
    iput-object p1, p0, LX/I8k;->w:LX/I5f;

    .line 2541827
    move-object/from16 v0, p11

    iput-object v0, p0, LX/I8k;->a:LX/0Zb;

    .line 2541828
    move-object/from16 v0, p6

    iput-object v0, p0, LX/I8k;->b:Landroid/content/Context;

    .line 2541829
    move-object/from16 v0, p4

    iput-object v0, p0, LX/I8k;->o:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2541830
    move-object/from16 v0, p16

    iput-object v0, p0, LX/I8k;->f:LX/0kv;

    .line 2541831
    move-object/from16 v0, p17

    iput-object v0, p0, LX/I8k;->m:LX/1Db;

    .line 2541832
    move-object/from16 v0, p21

    iput-object v0, p0, LX/I8k;->t:LX/0So;

    .line 2541833
    move-object/from16 v0, p10

    iput-object v0, p0, LX/I8k;->u:LX/1P0;

    .line 2541834
    move-object/from16 v0, p7

    iput-object v0, p0, LX/I8k;->q:LX/E93;

    .line 2541835
    move-object/from16 v0, p22

    iput-object v0, p0, LX/I8k;->v:LX/1vi;

    .line 2541836
    move-object/from16 v0, p24

    iput-object v0, p0, LX/I8k;->J:LX/0ad;

    .line 2541837
    new-instance v2, LX/I8X;

    invoke-direct {v2, p0}, LX/I8X;-><init>(LX/I8k;)V

    iput-object v2, p0, LX/I8k;->G:LX/Cg4;

    .line 2541838
    iget-object v2, p0, LX/I8k;->v:LX/1vi;

    iget-object v3, p0, LX/I8k;->G:LX/Cg4;

    invoke-virtual {v2, v3}, LX/0b4;->a(LX/0b2;)Z

    .line 2541839
    new-instance v2, LX/I8R;

    iget-object v3, p0, LX/I8k;->b:Landroid/content/Context;

    move-object/from16 v4, p12

    move-object v5, p2

    move-object/from16 v6, p3

    move-object/from16 v7, p20

    move-object/from16 v8, p4

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p23

    move-object/from16 v12, p24

    invoke-direct/range {v2 .. v12}, LX/I8R;-><init>(Landroid/content/Context;LX/I89;LX/I7i;LX/DBC;LX/I8u;Lcom/facebook/events/common/EventAnalyticsParams;LX/I91;LX/I98;LX/IBk;LX/0ad;)V

    iput-object v2, p0, LX/I8k;->c:LX/I8R;

    .line 2541840
    new-instance v2, LX/I8W;

    invoke-direct {v2}, LX/I8W;-><init>()V

    iput-object v2, p0, LX/I8k;->d:LX/I8W;

    .line 2541841
    new-instance v2, LX/I8O;

    invoke-direct {v2}, LX/I8O;-><init>()V

    iput-object v2, p0, LX/I8k;->e:LX/I8O;

    .line 2541842
    new-instance v2, LX/I8o;

    iget-object v3, p0, LX/I8k;->b:Landroid/content/Context;

    invoke-direct {v2, v3}, LX/I8o;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, LX/I8k;->j:LX/I8o;

    .line 2541843
    new-instance v2, LX/I8V;

    invoke-direct {v2}, LX/I8V;-><init>()V

    iput-object v2, p0, LX/I8k;->k:LX/I8V;

    .line 2541844
    move-object/from16 v0, p19

    iput-object v0, p0, LX/I8k;->l:LX/I8T;

    .line 2541845
    new-instance v2, LX/I8r;

    iget-object v3, p0, LX/I8k;->b:Landroid/content/Context;

    invoke-direct {v2, v3}, LX/I8r;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, LX/I8k;->r:LX/I8r;

    .line 2541846
    move-object/from16 v0, p5

    move-object/from16 v1, p18

    invoke-direct {p0, v0, v1}, LX/I8k;->a(Lcom/facebook/events/permalink/EventPermalinkFragment;LX/IBJ;)LX/IBI;

    move-result-object v2

    .line 2541847
    move-object/from16 v0, p13

    move-object/from16 v1, p14

    invoke-direct {p0, p1, v0, v1, v2}, LX/I8k;->a(LX/I5f;LX/1DS;LX/0Ot;LX/IBI;)LX/1Qq;

    move-result-object v3

    iput-object v3, p0, LX/I8k;->g:LX/1Qq;

    .line 2541848
    move-object/from16 v0, p13

    move-object/from16 v1, p15

    invoke-direct {p0, v0, v1, v2}, LX/I8k;->a(LX/1DS;LX/0Ot;LX/IBI;)LX/1Qq;

    move-result-object v2

    iput-object v2, p0, LX/I8k;->h:LX/1Qq;

    .line 2541849
    new-instance v2, LX/I8m;

    invoke-direct {v2}, LX/I8m;-><init>()V

    iput-object v2, p0, LX/I8k;->i:LX/I8m;

    .line 2541850
    invoke-direct {p0}, LX/I8k;->o()V

    .line 2541851
    return-void
.end method

.method private a(LX/1DS;LX/0Ot;LX/IBI;)LX/1Qq;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1DS;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/events/permalink/multirow/EventFeedComposerRootPartDefinition;",
            ">;",
            "Lcom/facebook/events/permalink/multirow/environment/EventPermalinkEnvironment;",
            ")",
            "LX/1Qq;"
        }
    .end annotation

    .prologue
    .line 2541852
    iget-object v0, p0, LX/I8k;->e:LX/I8O;

    iget-object v1, p0, LX/I8k;->s:Lcom/facebook/events/model/Event;

    invoke-virtual {v0, v1}, LX/I8O;->a(Lcom/facebook/events/model/Event;)V

    .line 2541853
    invoke-static {p2}, LX/5On;->a(LX/0Ot;)LX/0Ot;

    move-result-object v0

    iget-object v1, p0, LX/I8k;->e:LX/I8O;

    invoke-virtual {p1, v0, v1}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v0

    .line 2541854
    iput-object p3, v0, LX/1Ql;->f:LX/1PW;

    .line 2541855
    move-object v0, v0

    .line 2541856
    invoke-virtual {v0}, LX/1Ql;->e()LX/1Qq;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/I5f;LX/1DS;LX/0Ot;LX/IBI;)LX/1Qq;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/I5f;",
            "LX/1DS;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/events/permalink/multirow/EventPermalinkRootPartSelector;",
            ">;",
            "Lcom/facebook/events/permalink/multirow/environment/EventPermalinkEnvironment;",
            ")",
            "LX/1Qq;"
        }
    .end annotation

    .prologue
    .line 2541857
    new-instance v0, LX/I8Z;

    invoke-direct {v0, p0, p1}, LX/I8Z;-><init>(LX/I8k;LX/I5f;)V

    .line 2541858
    iget-object v1, p0, LX/I8k;->d:LX/I8W;

    .line 2541859
    iget-object v2, p1, LX/I5f;->c:LX/0fz;

    move-object v2, v2

    .line 2541860
    iput-object v2, v1, LX/I8W;->e:LX/0fz;

    .line 2541861
    iget-object v1, p0, LX/I8k;->d:LX/I8W;

    invoke-virtual {p2, p3, v1}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/1Ql;->a(LX/99g;)LX/1Ql;

    move-result-object v0

    .line 2541862
    iput-object p4, v0, LX/1Ql;->f:LX/1PW;

    .line 2541863
    move-object v0, v0

    .line 2541864
    invoke-virtual {v0}, LX/1Ql;->e()LX/1Qq;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/I8k;Ljava/lang/Object;)LX/I8N;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2541865
    iget-object v0, p0, LX/I8k;->y:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, LX/I8k;->y:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/I8N;

    .line 2541866
    iget-object v3, v0, LX/I8N;->a:Ljava/lang/Object;

    invoke-static {v3, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2541867
    :goto_1
    return-object v0

    .line 2541868
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2541869
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(Lcom/facebook/events/permalink/EventPermalinkFragment;LX/IBJ;)LX/IBI;
    .locals 7

    .prologue
    .line 2541870
    new-instance v0, LX/I8a;

    invoke-direct {v0, p0}, LX/I8a;-><init>(LX/I8k;)V

    iput-object v0, p0, LX/I8k;->p:LX/0QK;

    .line 2541871
    const-string v2, "event_permalink"

    iget-object v3, p0, LX/I8k;->b:Landroid/content/Context;

    .line 2541872
    sget-object v0, LX/I6Y;->a:LX/I6Y;

    move-object v4, v0

    .line 2541873
    new-instance v5, Lcom/facebook/events/permalink/adapters/EventPermalinkRecyclerViewAdapter$5;

    invoke-direct {v5, p0}, Lcom/facebook/events/permalink/adapters/EventPermalinkRecyclerViewAdapter$5;-><init>(LX/I8k;)V

    .line 2541874
    new-instance v0, LX/I8c;

    invoke-direct {v0, p0}, LX/I8c;-><init>(LX/I8k;)V

    move-object v6, v0

    .line 2541875
    move-object v0, p2

    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, LX/IBJ;->a(Lcom/facebook/events/permalink/EventPermalinkFragment;Ljava/lang/String;Landroid/content/Context;LX/1PT;Ljava/lang/Runnable;LX/1PY;)LX/IBI;

    move-result-object v0

    return-object v0
.end method

.method public static a$redex0(LX/I8k;Landroid/os/Parcelable;Z)V
    .locals 2

    .prologue
    .line 2541876
    if-eqz p2, :cond_1

    .line 2541877
    iget-object v0, p0, LX/I8k;->F:LX/IBq;

    sget-object v1, LX/IBr;->ABOUT:LX/IBr;

    invoke-virtual {v0, v1}, LX/IBq;->b(LX/IBr;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2541878
    iget-object v0, p0, LX/I8k;->I:LX/I8j;

    .line 2541879
    iput-object p1, v0, LX/I8j;->b:Landroid/os/Parcelable;

    .line 2541880
    :goto_0
    return-void

    .line 2541881
    :cond_0
    iget-object v0, p0, LX/I8k;->I:LX/I8j;

    .line 2541882
    iput-object p1, v0, LX/I8j;->a:Landroid/os/Parcelable;

    .line 2541883
    goto :goto_0

    .line 2541884
    :cond_1
    iget-object v0, p0, LX/I8k;->F:LX/IBq;

    sget-object v1, LX/IBr;->ABOUT:LX/IBr;

    invoke-virtual {v0, v1}, LX/IBq;->b(LX/IBr;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2541885
    iget-object v0, p0, LX/I8k;->I:LX/I8j;

    .line 2541886
    iput-object p1, v0, LX/I8j;->a:Landroid/os/Parcelable;

    .line 2541887
    goto :goto_0

    .line 2541888
    :cond_2
    iget-object v0, p0, LX/I8k;->I:LX/I8j;

    .line 2541889
    iput-object p1, v0, LX/I8j;->b:Landroid/os/Parcelable;

    .line 2541890
    goto :goto_0
.end method

.method public static e(LX/I8k;Z)V
    .locals 3

    .prologue
    .line 2541891
    iget-object v0, p0, LX/I8k;->d:LX/I8W;

    .line 2541892
    sget-object v2, LX/IBD;->e:LX/IBD;

    if-eqz p1, :cond_0

    sget-object v1, LX/I8W;->a:Ljava/lang/Object;

    :goto_0
    invoke-static {v2, v1}, LX/IBE;->a(LX/IBD;Ljava/lang/Object;)LX/IBE;

    move-result-object v1

    iput-object v1, v0, LX/I8W;->d:LX/IBE;

    .line 2541893
    invoke-static {v0}, LX/I8W;->c(LX/I8W;)V

    .line 2541894
    invoke-virtual {p0}, LX/I8k;->j()V

    .line 2541895
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 2541896
    return-void

    .line 2541897
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static f(LX/I8k;II)LX/I8N;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2541898
    :goto_0
    if-gt p1, p2, :cond_1

    .line 2541899
    iget-object v0, p0, LX/I8k;->z:LX/4yZ;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4yZ;->a(Ljava/lang/Comparable;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/I8k;->z:LX/4yZ;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4yZ;->a(Ljava/lang/Comparable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/I8N;

    iget-object v0, v0, LX/I8N;->a:Ljava/lang/Object;

    iget-object v1, p0, LX/I8k;->q:LX/E93;

    if-ne v0, v1, :cond_0

    .line 2541900
    iget-object v0, p0, LX/I8k;->z:LX/4yZ;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4yZ;->a(Ljava/lang/Comparable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/I8N;

    .line 2541901
    :goto_1
    return-object v0

    .line 2541902
    :cond_0
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 2541903
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private o()V
    .locals 10

    .prologue
    const/4 v8, 0x0

    .line 2541904
    iget-object v0, p0, LX/I8k;->c:LX/I8R;

    iget-object v1, p0, LX/I8k;->j:LX/I8o;

    iget-object v2, p0, LX/I8k;->h:LX/1Qq;

    iget-object v3, p0, LX/I8k;->l:LX/I8T;

    iget-object v4, p0, LX/I8k;->r:LX/I8r;

    iget-object v5, p0, LX/I8k;->g:LX/1Qq;

    iget-object v6, p0, LX/I8k;->k:LX/I8V;

    iget-object v7, p0, LX/I8k;->i:LX/I8m;

    invoke-static/range {v0 .. v7}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 2541905
    new-instance v0, Ljava/util/IdentityHashMap;

    invoke-direct {v0}, Ljava/util/IdentityHashMap;-><init>()V

    iput-object v0, p0, LX/I8k;->B:Ljava/util/IdentityHashMap;

    .line 2541906
    new-instance v3, LX/4yW;

    invoke-direct {v3}, LX/4yW;-><init>()V

    .line 2541907
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    move v1, v8

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Cw;

    .line 2541908
    invoke-interface {v0}, LX/1Cw;->getViewTypeCount()I

    move-result v5

    .line 2541909
    new-instance v6, LX/I8N;

    invoke-direct {v6, v0, v8}, LX/I8N;-><init>(Ljava/lang/Object;I)V

    .line 2541910
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    add-int v9, v5, v8

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-static {v7, v9}, LX/50M;->b(Ljava/lang/Comparable;Ljava/lang/Comparable;)LX/50M;

    move-result-object v7

    invoke-virtual {v3, v7, v6}, LX/4yW;->a(LX/50M;Ljava/lang/Object;)LX/4yW;

    .line 2541911
    iget-object v6, p0, LX/I8k;->B:Ljava/util/IdentityHashMap;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v0, v7}, Ljava/util/IdentityHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2541912
    add-int/2addr v8, v5

    .line 2541913
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2541914
    :cond_0
    invoke-virtual {v3}, LX/4yW;->a()LX/4yZ;

    move-result-object v0

    iput-object v0, p0, LX/I8k;->A:LX/4yZ;

    .line 2541915
    iput v8, p0, LX/I8k;->n:I

    .line 2541916
    return-void
.end method

.method public static p(LX/I8k;)Z
    .locals 1

    .prologue
    .line 2541917
    iget-object v0, p0, LX/I8k;->d:LX/I8W;

    .line 2541918
    iget-object p0, v0, LX/I8W;->e:LX/0fz;

    move-object v0, p0

    .line 2541919
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0fz;->x()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private r()LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2541920
    iget-object v0, p0, LX/I8k;->s:Lcom/facebook/events/model/Event;

    .line 2541921
    iget-object v3, v0, Lcom/facebook/events/model/Event;->m:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-object v0, v3

    .line 2541922
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->INTERESTED:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    if-ne v0, v3, :cond_3

    move v0, v1

    .line 2541923
    :goto_0
    iget-object v3, p0, LX/I8k;->r:LX/I8r;

    .line 2541924
    iput-boolean v0, v3, LX/I8r;->c:Z

    .line 2541925
    if-nez v0, :cond_5

    .line 2541926
    new-array v0, v6, [LX/IBr;

    sget-object v3, LX/IBr;->EVENT:LX/IBr;

    aput-object v3, v0, v2

    sget-object v3, LX/IBr;->ACTIVITY:LX/IBr;

    aput-object v3, v0, v1

    .line 2541927
    iget-object v3, p0, LX/I8k;->j:LX/I8o;

    invoke-virtual {v3, v0}, LX/I8o;->a([LX/IBr;)V

    .line 2541928
    iget-object v3, p0, LX/I8k;->F:LX/IBq;

    .line 2541929
    iput-object v0, v3, LX/IBq;->k:[LX/IBr;

    .line 2541930
    iget-object v0, p0, LX/I8k;->J:LX/0ad;

    sget-short v3, LX/347;->O:S

    invoke-interface {v0, v3, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 2541931
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 2541932
    iget-object v4, p0, LX/I8k;->c:LX/I8R;

    invoke-virtual {v3, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2541933
    iget-object v4, p0, LX/I8k;->s:Lcom/facebook/events/model/Event;

    sget-object v5, LX/7vK;->ADMIN:LX/7vK;

    invoke-virtual {v4, v5}, Lcom/facebook/events/model/Event;->a(LX/7vK;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, LX/I8k;->M:LX/0Uh;

    const/16 v5, 0x3a7

    invoke-virtual {v4, v5, v2}, LX/0Uh;->a(IZ)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2541934
    iget-object v4, p0, LX/I8k;->j:LX/I8o;

    invoke-virtual {v3, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2541935
    :cond_0
    iget-object v4, p0, LX/I8k;->F:LX/IBq;

    sget-object v5, LX/IBr;->EVENT:LX/IBr;

    invoke-virtual {v4, v5}, LX/IBq;->b(LX/IBr;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2541936
    iget-object v4, p0, LX/I8k;->j:LX/I8o;

    .line 2541937
    iput-boolean v1, v4, LX/I8o;->d:Z

    .line 2541938
    new-array v4, v6, [Ljava/lang/Object;

    iget-object v5, p0, LX/I8k;->h:LX/1Qq;

    aput-object v5, v4, v2

    iget-object v2, p0, LX/I8k;->q:LX/E93;

    aput-object v2, v4, v1

    invoke-virtual {v3, v4}, LX/0Pz;->b([Ljava/lang/Object;)LX/0Pz;

    .line 2541939
    if-eqz v0, :cond_1

    .line 2541940
    iget-object v0, p0, LX/I8k;->r:LX/I8r;

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2541941
    :cond_1
    iget-object v0, p0, LX/I8k;->g:LX/1Qq;

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2541942
    :cond_2
    :goto_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2541943
    :goto_2
    return-object v0

    :cond_3
    move v0, v2

    .line 2541944
    goto :goto_0

    .line 2541945
    :cond_4
    iget-object v0, p0, LX/I8k;->F:LX/IBq;

    sget-object v1, LX/IBr;->ACTIVITY:LX/IBr;

    invoke-virtual {v0, v1}, LX/IBq;->b(LX/IBr;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2541946
    iget-object v0, p0, LX/I8k;->j:LX/I8o;

    .line 2541947
    iput-boolean v2, v0, LX/I8o;->d:Z

    .line 2541948
    iget-object v0, p0, LX/I8k;->k:LX/I8V;

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 2541949
    :cond_5
    new-array v0, v6, [LX/IBr;

    sget-object v3, LX/IBr;->ABOUT:LX/IBr;

    aput-object v3, v0, v2

    sget-object v2, LX/IBr;->DISCUSSION:LX/IBr;

    aput-object v2, v0, v1

    .line 2541950
    iget-object v1, p0, LX/I8k;->j:LX/I8o;

    invoke-virtual {v1, v0}, LX/I8o;->a([LX/IBr;)V

    .line 2541951
    iget-object v1, p0, LX/I8k;->F:LX/IBq;

    .line 2541952
    iput-object v0, v1, LX/IBq;->k:[LX/IBr;

    .line 2541953
    iget-object v0, p0, LX/I8k;->F:LX/IBq;

    sget-object v1, LX/IBr;->ABOUT:LX/IBr;

    invoke-virtual {v0, v1}, LX/IBq;->b(LX/IBr;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2541954
    iget-object v0, p0, LX/I8k;->s:Lcom/facebook/events/model/Event;

    .line 2541955
    iget-boolean v1, v0, Lcom/facebook/events/model/Event;->z:Z

    move v0, v1

    .line 2541956
    if-nez v0, :cond_6

    .line 2541957
    iget-object v0, p0, LX/I8k;->c:LX/I8R;

    iget-object v1, p0, LX/I8k;->j:LX/I8o;

    iget-object v2, p0, LX/I8k;->h:LX/1Qq;

    iget-object v3, p0, LX/I8k;->q:LX/E93;

    iget-object v4, p0, LX/I8k;->r:LX/I8r;

    iget-object v5, p0, LX/I8k;->i:LX/I8m;

    invoke-static/range {v0 .. v5}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_2

    .line 2541958
    :cond_6
    iget-object v0, p0, LX/I8k;->c:LX/I8R;

    iget-object v1, p0, LX/I8k;->j:LX/I8o;

    iget-object v2, p0, LX/I8k;->q:LX/E93;

    iget-object v3, p0, LX/I8k;->r:LX/I8r;

    iget-object v4, p0, LX/I8k;->i:LX/I8m;

    invoke-static {v0, v1, v2, v3, v4}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_2

    .line 2541959
    :cond_7
    iget-object v0, p0, LX/I8k;->s:Lcom/facebook/events/model/Event;

    .line 2541960
    iget-boolean v1, v0, Lcom/facebook/events/model/Event;->z:Z

    move v0, v1

    .line 2541961
    if-nez v0, :cond_8

    .line 2541962
    iget-object v0, p0, LX/I8k;->c:LX/I8R;

    iget-object v1, p0, LX/I8k;->j:LX/I8o;

    iget-object v2, p0, LX/I8k;->h:LX/1Qq;

    iget-object v3, p0, LX/I8k;->l:LX/I8T;

    iget-object v4, p0, LX/I8k;->g:LX/1Qq;

    iget-object v5, p0, LX/I8k;->i:LX/I8m;

    invoke-static/range {v0 .. v5}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_2

    .line 2541963
    :cond_8
    iget-object v0, p0, LX/I8k;->c:LX/I8R;

    iget-object v1, p0, LX/I8k;->j:LX/I8o;

    iget-object v2, p0, LX/I8k;->l:LX/I8T;

    iget-object v3, p0, LX/I8k;->g:LX/1Qq;

    iget-object v4, p0, LX/I8k;->i:LX/I8m;

    invoke-static {v0, v1, v2, v3, v4}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto/16 :goto_2
.end method

.method public static s(LX/I8k;)V
    .locals 4

    .prologue
    .line 2541730
    iget-object v0, p0, LX/I8k;->j:LX/I8o;

    invoke-static {p0, v0}, LX/I8k;->a(LX/I8k;Ljava/lang/Object;)LX/I8N;

    move-result-object v0

    iget-object v1, p0, LX/I8k;->F:LX/IBq;

    invoke-virtual {v1}, LX/IBq;->c()I

    move-result v1

    .line 2541731
    iget-object v2, p0, LX/I8k;->F:LX/IBq;

    if-eqz v2, :cond_0

    if-nez v0, :cond_1

    .line 2541732
    :cond_0
    :goto_0
    return-void

    .line 2541733
    :cond_1
    iget-object v2, p0, LX/I8k;->D:LX/0g8;

    iget v3, v0, LX/I8N;->b:I

    invoke-interface {v2, v3, v1}, LX/0g8;->c(II)V

    .line 2541734
    iget-object v2, p0, LX/I8k;->F:LX/IBq;

    .line 2541735
    iget-object v3, v2, LX/IBq;->b:Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;

    if-eqz v3, :cond_2

    .line 2541736
    iget-object v3, v2, LX/IBq;->b:Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;

    const/4 p0, 0x0

    invoke-virtual {v3, p0}, Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;->setVisibility(I)V

    .line 2541737
    :cond_2
    goto :goto_0
.end method


# virtual methods
.method public final C_(I)J
    .locals 3

    .prologue
    .line 2541813
    iget-object v0, p0, LX/I8k;->z:LX/4yZ;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4yZ;->a(Ljava/lang/Comparable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/I8N;

    .line 2541814
    if-nez v0, :cond_0

    .line 2541815
    const-wide/16 v0, 0x0

    .line 2541816
    :goto_0
    return-wide v0

    .line 2541817
    :cond_0
    invoke-virtual {v0, p1}, LX/I8N;->a(I)I

    move-result v1

    .line 2541818
    invoke-virtual {v0}, LX/I8N;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2541819
    iget-object v0, p0, LX/I8k;->q:LX/E93;

    invoke-virtual {v0, v1}, LX/1OM;->C_(I)J

    move-result-wide v0

    goto :goto_0

    .line 2541820
    :cond_1
    invoke-virtual {v0}, LX/I8N;->a()LX/1Cw;

    move-result-object v0

    invoke-interface {v0, v1}, LX/1Cw;->getItemId(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 5

    .prologue
    .line 2541964
    const v0, 0xffffff

    if-le p2, v0, :cond_0

    .line 2541965
    iget-object v0, p0, LX/I8k;->q:LX/E93;

    invoke-virtual {v0, p1, p2}, LX/1OM;->a(Landroid/view/ViewGroup;I)LX/1a1;

    move-result-object v0

    .line 2541966
    :goto_0
    return-object v0

    .line 2541967
    :cond_0
    iget v0, p0, LX/I8k;->n:I

    if-lt p2, v0, :cond_1

    .line 2541968
    iget-object v0, p0, LX/I8k;->q:LX/E93;

    iget v1, p0, LX/I8k;->n:I

    sub-int v1, p2, v1

    invoke-virtual {v0, p1, v1}, LX/1OM;->a(Landroid/view/ViewGroup;I)LX/1a1;

    move-result-object v0

    goto :goto_0

    .line 2541969
    :cond_1
    iget-object v0, p0, LX/I8k;->A:LX/4yZ;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4yZ;->a(Ljava/lang/Comparable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/I8N;

    .line 2541970
    if-nez v0, :cond_2

    .line 2541971
    const/4 v0, 0x0

    goto :goto_0

    .line 2541972
    :cond_2
    iget-object v1, v0, LX/I8N;->a:Ljava/lang/Object;

    check-cast v1, LX/1Cw;

    .line 2541973
    iget v2, v0, LX/I8N;->c:I

    sub-int v2, p2, v2

    move v2, v2

    .line 2541974
    invoke-interface {v1, v2, p1}, LX/1Cw;->a(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 2541975
    if-eqz v3, :cond_4

    const/4 v0, 0x1

    :goto_1
    const-string v4, "Unexpected null view from getView()"

    invoke-static {v0, v4}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2541976
    iget-object v0, p0, LX/I8k;->g:LX/1Qq;

    if-eq v1, v0, :cond_3

    iget-object v0, p0, LX/I8k;->h:LX/1Qq;

    if-ne v1, v0, :cond_5

    .line 2541977
    :cond_3
    new-instance v0, LX/I8h;

    invoke-direct {v0, v3, p1, v2}, LX/I8h;-><init>(Landroid/view/View;Landroid/view/ViewGroup;I)V

    goto :goto_0

    .line 2541978
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 2541979
    :cond_5
    iget-object v0, p0, LX/I8k;->j:LX/I8o;

    if-ne v1, v0, :cond_6

    sget-object v0, LX/I8n;->TAB_BAR:LX/I8n;

    invoke-virtual {v0}, LX/I8n;->ordinal()I

    move-result v0

    if-ne v2, v0, :cond_6

    .line 2541980
    new-instance v0, LX/I8i;

    invoke-direct {v0, v3, p1, v2}, LX/I8i;-><init>(Landroid/view/View;Landroid/view/ViewGroup;I)V

    .line 2541981
    iget-object v1, p0, LX/I8k;->F:LX/IBq;

    .line 2541982
    iput-object v0, v1, LX/IBq;->c:LX/I8i;

    .line 2541983
    iget-object v4, v1, LX/IBq;->c:LX/I8i;

    const/4 p0, 0x0

    invoke-virtual {v4, p0}, LX/1a1;->a(Z)V

    .line 2541984
    invoke-static {v1}, LX/IBq;->g(LX/IBq;)Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;

    move-result-object v4

    .line 2541985
    iput-object v1, v4, Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;->b:LX/I8d;

    .line 2541986
    :cond_6
    new-instance v0, LX/I8g;

    invoke-direct {v0, v3, p1, v2}, LX/I8g;-><init>(Landroid/view/View;Landroid/view/ViewGroup;I)V

    goto :goto_0
.end method

.method public final a(LX/1a1;)V
    .locals 1

    .prologue
    .line 2541724
    invoke-super {p0, p1}, LX/1OM;->a(LX/1a1;)V

    .line 2541725
    instance-of v0, p1, LX/I8h;

    if-eqz v0, :cond_1

    .line 2541726
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    invoke-static {v0}, LX/1Db;->a(Landroid/view/View;)Ljava/lang/Void;

    .line 2541727
    :cond_0
    :goto_0
    return-void

    .line 2541728
    :cond_1
    instance-of v0, p1, LX/I8g;

    if-nez v0, :cond_0

    .line 2541729
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    invoke-static {v0}, LX/1Db;->a(Landroid/view/View;)Ljava/lang/Void;

    goto :goto_0
.end method

.method public final a(LX/1a1;I)V
    .locals 6

    .prologue
    .line 2541738
    iget-object v0, p0, LX/I8k;->z:LX/4yZ;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4yZ;->a(Ljava/lang/Comparable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/I8N;

    .line 2541739
    if-nez v0, :cond_0

    .line 2541740
    :goto_0
    return-void

    .line 2541741
    :cond_0
    invoke-virtual {v0, p2}, LX/I8N;->a(I)I

    move-result v1

    .line 2541742
    invoke-virtual {v0}, LX/I8N;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2541743
    invoke-virtual {v0}, LX/I8N;->b()LX/1OM;

    move-result-object v0

    invoke-virtual {v0, p1, v1}, LX/1OM;->a(LX/1a1;I)V

    goto :goto_0

    .line 2541744
    :cond_1
    check-cast p1, LX/I8g;

    .line 2541745
    invoke-virtual {v0}, LX/I8N;->a()LX/1Cw;

    move-result-object v0

    .line 2541746
    invoke-interface {v0, v1}, LX/1Cw;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p1, LX/1a1;->a:Landroid/view/View;

    iget v4, p1, LX/I8g;->m:I

    iget-object v5, p1, LX/I8g;->l:Landroid/view/ViewGroup;

    invoke-interface/range {v0 .. v5}, LX/1Cw;->a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V

    goto :goto_0
.end method

.method public final a(LX/IBr;I)V
    .locals 3

    .prologue
    .line 2541747
    iget-object v0, p0, LX/I8k;->j:LX/I8o;

    .line 2541748
    iget-object v1, v0, LX/I8o;->b:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2541749
    iget-object v0, p0, LX/I8k;->F:LX/IBq;

    .line 2541750
    iget-object v1, v0, LX/IBq;->a:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2541751
    invoke-virtual {p0}, LX/I8k;->j()V

    .line 2541752
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 2541753
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 2

    .prologue
    .line 2541754
    iget-object v0, p0, LX/I8k;->d:LX/I8W;

    .line 2541755
    sget-object v1, LX/IBD;->c:LX/IBD;

    invoke-static {v1, p1}, LX/IBE;->a(LX/IBD;Ljava/lang/Object;)LX/IBE;

    move-result-object v1

    iput-object v1, v0, LX/I8W;->c:LX/IBE;

    .line 2541756
    invoke-static {v0}, LX/I8W;->c(LX/I8W;)V

    .line 2541757
    invoke-virtual {p0}, LX/I8k;->j()V

    .line 2541758
    return-void
.end method

.method public final b(Landroid/support/v7/widget/RecyclerView;)V
    .locals 2

    .prologue
    .line 2541759
    invoke-super {p0, p1}, LX/1OM;->b(Landroid/support/v7/widget/RecyclerView;)V

    .line 2541760
    iget-object v0, p0, LX/I8k;->v:LX/1vi;

    iget-object v1, p0, LX/I8k;->G:LX/Cg4;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 2541761
    return-void
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 2541762
    iget-object v0, p0, LX/I8k;->r:LX/I8r;

    .line 2541763
    iput-boolean p1, v0, LX/I8r;->b:Z

    .line 2541764
    invoke-virtual {p0}, LX/I8k;->j()V

    .line 2541765
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 2541766
    return-void
.end method

.method public final e()V
    .locals 4

    .prologue
    .line 2541767
    iget-object v0, p0, LX/I8k;->F:LX/IBq;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/I8k;->D:LX/0g8;

    if-eqz v0, :cond_0

    .line 2541768
    iget-object v0, p0, LX/I8k;->F:LX/IBq;

    sget-object v1, LX/IBr;->DISCUSSION:LX/IBr;

    iget-object v2, p0, LX/I8k;->F:LX/IBq;

    sget-object v3, LX/IBr;->ABOUT:LX/IBr;

    invoke-virtual {v2, v3}, LX/IBq;->b(LX/IBr;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/IBq;->a(LX/IBr;Z)V

    .line 2541769
    invoke-static {p0}, LX/I8k;->s(LX/I8k;)V

    .line 2541770
    :cond_0
    return-void
.end method

.method public final f()V
    .locals 0

    .prologue
    .line 2541771
    invoke-virtual {p0}, LX/I8k;->j()V

    .line 2541772
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 2541773
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 3

    .prologue
    .line 2541774
    iget-object v0, p0, LX/I8k;->z:LX/4yZ;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4yZ;->a(Ljava/lang/Comparable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/I8N;

    .line 2541775
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2541776
    invoke-virtual {v0, p1}, LX/I8N;->a(I)I

    move-result v1

    .line 2541777
    invoke-virtual {v0}, LX/I8N;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, LX/I8N;->b()LX/1OM;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/1OM;->getItemViewType(I)I

    move-result v1

    .line 2541778
    :goto_0
    const v2, 0xffffff

    if-le v1, v2, :cond_1

    move v0, v1

    .line 2541779
    :goto_1
    return v0

    .line 2541780
    :cond_0
    invoke-virtual {v0}, LX/I8N;->a()LX/1Cw;

    move-result-object v2

    invoke-interface {v2, v1}, LX/1Cw;->getItemViewType(I)I

    move-result v1

    goto :goto_0

    .line 2541781
    :cond_1
    iget v2, v0, LX/I8N;->c:I

    add-int/2addr v2, v1

    move v0, v2

    .line 2541782
    goto :goto_1
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2541783
    iget v0, p0, LX/I8k;->C:I

    return v0
.end method

.method public final j()V
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 2541784
    iget-object v1, p0, LX/I8k;->s:Lcom/facebook/events/model/Event;

    if-nez v1, :cond_0

    .line 2541785
    :goto_0
    return-void

    .line 2541786
    :cond_0
    iget-object v1, p0, LX/I8k;->h:LX/1Qq;

    invoke-interface {v1}, LX/1Cw;->notifyDataSetChanged()V

    .line 2541787
    iget-object v1, p0, LX/I8k;->g:LX/1Qq;

    invoke-interface {v1}, LX/1Cw;->notifyDataSetChanged()V

    .line 2541788
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 2541789
    new-instance v5, LX/4yW;

    invoke-direct {v5}, LX/4yW;-><init>()V

    .line 2541790
    invoke-direct {p0}, LX/I8k;->r()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v3, v0

    move v2, v0

    :goto_1
    if-ge v3, v7, :cond_4

    invoke-virtual {v6, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 2541791
    instance-of v1, v0, LX/1Cw;

    if-eqz v1, :cond_3

    .line 2541792
    check-cast v0, LX/1Cw;

    .line 2541793
    invoke-interface {v0}, LX/1Cw;->getCount()I

    move-result v8

    .line 2541794
    if-lez v8, :cond_5

    .line 2541795
    new-instance v9, LX/I8N;

    iget-object v1, p0, LX/I8k;->B:Ljava/util/IdentityHashMap;

    invoke-virtual {v1, v0}, Ljava/util/IdentityHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {v9, v0, v2, v1}, LX/I8N;-><init>(Ljava/lang/Object;II)V

    .line 2541796
    iget-object v1, p0, LX/I8k;->g:LX/1Qq;

    if-ne v0, v1, :cond_1

    .line 2541797
    iput-object v9, p0, LX/I8k;->x:LX/I8N;

    .line 2541798
    :cond_1
    invoke-virtual {v4, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2541799
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    add-int v1, v2, v8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, LX/50M;->b(Ljava/lang/Comparable;Ljava/lang/Comparable;)LX/50M;

    move-result-object v0

    invoke-virtual {v5, v0, v9}, LX/4yW;->a(LX/50M;Ljava/lang/Object;)LX/4yW;

    .line 2541800
    add-int v0, v2, v8

    :goto_2
    move v2, v0

    .line 2541801
    :cond_2
    :goto_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 2541802
    :cond_3
    iget-boolean v1, p0, LX/I8k;->E:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/I8k;->q:LX/E93;

    if-ne v0, v1, :cond_2

    .line 2541803
    iget-object v1, p0, LX/I8k;->q:LX/E93;

    invoke-virtual {v1}, LX/1OM;->ij_()I

    move-result v1

    .line 2541804
    new-instance v8, LX/I8N;

    iget v9, p0, LX/I8k;->n:I

    invoke-direct {v8, v0, v2, v9}, LX/I8N;-><init>(Ljava/lang/Object;II)V

    .line 2541805
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    add-int v9, v2, v1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-static {v0, v9}, LX/50M;->b(Ljava/lang/Comparable;Ljava/lang/Comparable;)LX/50M;

    move-result-object v0

    invoke-virtual {v5, v0, v8}, LX/4yW;->a(LX/50M;Ljava/lang/Object;)LX/4yW;

    .line 2541806
    invoke-virtual {v4, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2541807
    add-int/2addr v2, v1

    goto :goto_3

    .line 2541808
    :cond_4
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/I8k;->y:LX/0Px;

    .line 2541809
    invoke-virtual {v5}, LX/4yW;->a()LX/4yZ;

    move-result-object v0

    iput-object v0, p0, LX/I8k;->z:LX/4yZ;

    .line 2541810
    iput v2, p0, LX/I8k;->C:I

    goto/16 :goto_0

    :cond_5
    move v0, v2

    goto :goto_2
.end method

.method public final m()V
    .locals 1

    .prologue
    .line 2541811
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/I8k;->e(LX/I8k;Z)V

    .line 2541812
    return-void
.end method
