.class public final LX/Ir0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field public final synthetic a:Landroid/view/View;

.field public final synthetic b:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2617378
    iput-object p1, p0, LX/Ir0;->b:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    iput-object p2, p0, LX/Ir0;->a:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 2

    .prologue
    .line 2617379
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 2617380
    iget-object v1, p0, LX/Ir0;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 2617381
    return-void
.end method
