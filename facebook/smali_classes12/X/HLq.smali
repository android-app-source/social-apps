.class public LX/HLq;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private final a:I

.field public final b:I

.field public c:LX/0Uh;

.field public d:Lcom/facebook/content/SecureContextHelper;

.field public e:Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;

.field public f:Lcom/facebook/widget/text/BetterTextView;

.field public g:Lcom/facebook/widget/text/BetterTextView;

.field public h:Lcom/facebook/widget/text/BetterTextView;

.field public i:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 2456417
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2456418
    invoke-virtual {p0}, LX/HLq;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2456419
    const v1, 0x7f030e3f

    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2456420
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, LX/HLq;->setOrientation(I)V

    .line 2456421
    const v1, 0x7f0b0d5e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, LX/HLq;->a:I

    .line 2456422
    const v1, 0x7f0b0d5f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, LX/HLq;->b:I

    .line 2456423
    const v1, 0x7f0213ab

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {p0, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2456424
    iget v0, p0, LX/HLq;->a:I

    iget v1, p0, LX/HLq;->b:I

    div-int/lit8 v1, v1, 0x2

    iget v2, p0, LX/HLq;->a:I

    iget v3, p0, LX/HLq;->b:I

    div-int/lit8 v3, v3, 0x2

    invoke-virtual {p0, v0, v1, v2, v3}, LX/HLq;->setPadding(IIII)V

    .line 2456425
    const v0, 0x7f0d22bc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;

    iput-object v0, p0, LX/HLq;->e:Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;

    .line 2456426
    const v0, 0x7f0d22bd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/HLq;->f:Lcom/facebook/widget/text/BetterTextView;

    .line 2456427
    const v0, 0x7f0d22be

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/HLq;->g:Lcom/facebook/widget/text/BetterTextView;

    .line 2456428
    const v0, 0x7f0d22bf

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/HLq;->h:Lcom/facebook/widget/text/BetterTextView;

    .line 2456429
    const v0, 0x7f0d22c0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/HLq;->i:Lcom/facebook/widget/text/BetterTextView;

    .line 2456430
    iget-object v0, p0, LX/HLq;->e:Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;

    sget-object v1, LX/0xq;->ROBOTO:LX/0xq;

    sget-object v2, LX/0xr;->REGULAR:LX/0xr;

    .line 2456431
    invoke-virtual {v0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, LX/0xs;->a(Landroid/widget/TextView;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)V

    .line 2456432
    return-void
.end method

.method public static a(LX/HLq;Landroid/widget/TextView;Ljava/lang/String;Landroid/view/View$OnClickListener;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2456433
    if-eqz p2, :cond_1

    const/4 v1, 0x1

    .line 2456434
    :goto_0
    if-eqz v1, :cond_0

    .line 2456435
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2456436
    invoke-virtual {p1, p3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2456437
    iget v2, p0, LX/HLq;->b:I

    div-int/lit8 v2, v2, 0x2

    iget v3, p0, LX/HLq;->b:I

    div-int/lit8 v3, v3, 0x2

    invoke-virtual {p1, v0, v2, v0, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 2456438
    :cond_0
    if-eqz v1, :cond_2

    :goto_1
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2456439
    return-void

    :cond_1
    move v1, v0

    .line 2456440
    goto :goto_0

    .line 2456441
    :cond_2
    const/16 v0, 0x8

    goto :goto_1
.end method
