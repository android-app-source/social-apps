.class public final LX/Hq0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Hq7;

.field public final synthetic b:LX/Hq3;


# direct methods
.method public constructor <init>(LX/Hq3;LX/Hq7;)V
    .locals 0

    .prologue
    .line 2510161
    iput-object p1, p0, LX/Hq0;->b:LX/Hq3;

    iput-object p2, p0, LX/Hq0;->a:LX/Hq7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x2ad40c1c

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2510162
    iget-object v0, p0, LX/Hq0;->a:LX/Hq7;

    .line 2510163
    iget-object v2, v0, LX/Hq7;->b:LX/HrE;

    .line 2510164
    iget-object v3, v2, LX/HrE;->a:Lcom/facebook/composer/activity/ComposerFragment;

    .line 2510165
    iget-object v0, v3, Lcom/facebook/composer/activity/ComposerFragment;->aB:LX/0gd;

    sget-object p1, LX/0ge;->COMPOSER_PUBLISH_MODE_SELECTOR_CLICK:LX/0ge;

    iget-object v5, v3, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v5}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v5}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, p1, v5}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 2510166
    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v5, v3, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v5}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v5}, LX/0jF;->getPublishMode()LX/5Rn;

    move-result-object p1

    iget-object v5, v3, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v5}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v5}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v2

    iget-object v5, v3, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v5}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-interface {v5}, LX/0iz;->getPublishScheduleTime()Ljava/lang/Long;

    move-result-object v5

    invoke-static {v0, p1, v2, v5}, Lcom/facebook/composer/ui/publishmode/PublishModeSelectorActivity;->a(Landroid/content/Context;LX/5Rn;LX/0Px;Ljava/lang/Long;)Landroid/content/Intent;

    move-result-object v5

    const/16 v0, 0xe

    invoke-virtual {v3, v5, v0}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 2510167
    iget-object v0, p0, LX/Hq0;->b:LX/Hq3;

    iget-object v2, v0, LX/Hq3;->b:LX/0gd;

    iget-object v0, p0, LX/Hq0;->b:LX/Hq3;

    iget-object v0, v0, LX/Hq3;->c:LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0j0;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    .line 2510168
    const-string v5, "publish_mode"

    move-object v3, v5

    .line 2510169
    sget-object v5, LX/0ge;->COMPOSER_ACTION_ITEM_CLICK:LX/0ge;

    invoke-static {v5, v0}, LX/324;->a(LX/0ge;Ljava/lang/String;)LX/324;

    move-result-object v5

    .line 2510170
    iget-object p0, v5, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "action_item_type"

    invoke-virtual {p0, v0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2510171
    move-object v5, v5

    .line 2510172
    iget-object p0, v2, LX/0gd;->a:LX/0Zb;

    .line 2510173
    iget-object v2, v5, LX/324;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-object v5, v2

    .line 2510174
    invoke-interface {p0, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2510175
    const v0, -0x272874f0

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
