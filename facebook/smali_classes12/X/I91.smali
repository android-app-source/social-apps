.class public LX/I91;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/03V;

.field private final c:LX/1Kf;

.field private final d:LX/1nQ;

.field private final e:LX/0ad;

.field private f:Landroid/view/View$OnClickListener;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/ui/dialogs/ProgressDialogFragment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Z

.field private j:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2542183
    const-class v0, LX/I91;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/I91;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/1Kf;LX/1nQ;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2542184
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2542185
    iput-object p1, p0, LX/I91;->b:LX/03V;

    .line 2542186
    iput-object p2, p0, LX/I91;->c:LX/1Kf;

    .line 2542187
    iput-object p3, p0, LX/I91;->d:LX/1nQ;

    .line 2542188
    iput-object p4, p0, LX/I91;->e:LX/0ad;

    .line 2542189
    return-void
.end method

.method public static a(LX/0QB;)LX/I91;
    .locals 5

    .prologue
    .line 2542190
    new-instance v4, LX/I91;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-static {p0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v1

    check-cast v1, LX/1Kf;

    invoke-static {p0}, LX/1nQ;->b(LX/0QB;)LX/1nQ;

    move-result-object v2

    check-cast v2, LX/1nQ;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {v4, v0, v1, v2, v3}, LX/I91;-><init>(LX/03V;LX/1Kf;LX/1nQ;LX/0ad;)V

    .line 2542191
    move-object v0, v4

    .line 2542192
    return-object v0
.end method

.method public static a$redex0(LX/I91;Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 2542193
    iget-object v0, p0, LX/I91;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    if-nez v0, :cond_1

    .line 2542194
    iget-boolean v0, p0, LX/I91;->i:Z

    if-nez v0, :cond_0

    .line 2542195
    const/4 v3, 0x1

    .line 2542196
    const-class v0, Landroid/support/v4/app/FragmentActivity;

    invoke-static {p1, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    .line 2542197
    if-nez v0, :cond_4

    .line 2542198
    iget-object v0, p0, LX/I91;->b:LX/03V;

    sget-object v1, LX/I91;->a:Ljava/lang/String;

    const-string v2, "Failed to find a fragment activity"

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    .line 2542199
    iput-boolean v3, v1, LX/0VK;->d:Z

    .line 2542200
    move-object v1, v1

    .line 2542201
    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 2542202
    :cond_0
    :goto_0
    return-void

    .line 2542203
    :cond_1
    iget-object v0, p0, LX/I91;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->am()Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel;

    move-result-object v0

    .line 2542204
    if-nez v0, :cond_5

    .line 2542205
    const/4 v1, 0x0

    .line 2542206
    :goto_1
    move-object v0, v1

    .line 2542207
    new-instance v1, LX/4X0;

    invoke-direct {v1}, LX/4X0;-><init>()V

    .line 2542208
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2542209
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLInlineActivity;->j()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/4X0;->b:Ljava/lang/String;

    .line 2542210
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLInlineActivity;->k()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    iput-object v2, v1, LX/4X0;->c:Lcom/facebook/graphql/model/GraphQLNode;

    .line 2542211
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLInlineActivity;->l()Lcom/facebook/graphql/model/GraphQLTaggableActivity;

    move-result-object v2

    iput-object v2, v1, LX/4X0;->d:Lcom/facebook/graphql/model/GraphQLTaggableActivity;

    .line 2542212
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLInlineActivity;->m()Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    move-result-object v2

    iput-object v2, v1, LX/4X0;->e:Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    .line 2542213
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLInlineActivity;->n()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/4X0;->f:Ljava/lang/String;

    .line 2542214
    invoke-static {v1, v0}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 2542215
    move-object v0, v1

    .line 2542216
    new-instance v1, LX/4XR;

    invoke-direct {v1}, LX/4XR;-><init>()V

    iget-object v2, p0, LX/I91;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->e()Ljava/lang/String;

    move-result-object v2

    .line 2542217
    iput-object v2, v1, LX/4XR;->fO:Ljava/lang/String;

    .line 2542218
    move-object v1, v1

    .line 2542219
    iget-object v2, p0, LX/I91;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->eR_()Ljava/lang/String;

    move-result-object v2

    .line 2542220
    iput-object v2, v1, LX/4XR;->iB:Ljava/lang/String;

    .line 2542221
    move-object v1, v1

    .line 2542222
    new-instance v2, LX/39x;

    invoke-direct {v2}, LX/39x;-><init>()V

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->EVENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    .line 2542223
    iput-object v3, v2, LX/39x;->p:LX/0Px;

    .line 2542224
    move-object v2, v2

    .line 2542225
    new-instance v3, LX/4XR;

    invoke-direct {v3}, LX/4XR;-><init>()V

    iget-object v4, p0, LX/I91;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-virtual {v4}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->e()Ljava/lang/String;

    move-result-object v4

    .line 2542226
    iput-object v4, v3, LX/4XR;->fO:Ljava/lang/String;

    .line 2542227
    move-object v3, v3

    .line 2542228
    invoke-virtual {v3}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v3

    .line 2542229
    iput-object v3, v2, LX/39x;->s:Lcom/facebook/graphql/model/GraphQLNode;

    .line 2542230
    move-object v2, v2

    .line 2542231
    invoke-virtual {v2}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    .line 2542232
    iput-object v2, v1, LX/4XR;->iO:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2542233
    move-object v1, v1

    .line 2542234
    invoke-virtual {v1}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    .line 2542235
    iput-object v1, v0, LX/4X0;->c:Lcom/facebook/graphql/model/GraphQLNode;

    .line 2542236
    move-object v0, v0

    .line 2542237
    invoke-virtual {v0}, LX/4X0;->a()Lcom/facebook/graphql/model/GraphQLInlineActivity;

    move-result-object v0

    .line 2542238
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLInlineActivity;->k()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->fP()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->a(Lcom/facebook/graphql/model/GraphQLInlineActivity;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v0

    .line 2542239
    if-nez v0, :cond_2

    .line 2542240
    iget-object v0, p0, LX/I91;->b:LX/03V;

    sget-object v1, LX/I91;->a:Ljava/lang/String;

    const-string v2, "Failed to create an inline activity model for an event"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2542241
    :cond_2
    sget-object v1, LX/21D;->EVENT:LX/21D;

    const-string v2, "eventMinutiaePrefilled"

    invoke-static {v1, v2}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    sget-object v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setMinutiaeObjectTag(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    iget-object v1, p0, LX/I91;->e:LX/0ad;

    sget-short v2, LX/1EB;->an:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setUsePublishExperiment(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    .line 2542242
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2542243
    iget-object v3, p0, LX/I91;->c:LX/1Kf;

    const/16 v4, 0x1f8

    const-class v0, Landroid/app/Activity;

    invoke-static {p1, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-interface {v3, v2, v1, v4, v0}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 2542244
    iget-object v0, p0, LX/I91;->d:LX/1nQ;

    iget-object v1, p0, LX/I91;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->e()Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/facebook/events/common/ActionMechanism;->HEADER:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v3}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2542245
    iget-object v4, v0, LX/1nQ;->i:LX/0Zb;

    const-string p0, "event_checkin_button_click"

    const/4 p1, 0x0

    invoke-interface {v4, p0, p1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v4

    .line 2542246
    invoke-virtual {v4}, LX/0oG;->a()Z

    move-result p0

    if-eqz p0, :cond_3

    .line 2542247
    const-string p0, "event_permalink"

    invoke-virtual {v4, p0}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2542248
    iget-object p0, v0, LX/1nQ;->j:LX/0kv;

    iget-object p1, v0, LX/1nQ;->g:Landroid/content/Context;

    invoke-virtual {p0, p1}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v4, p0}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    .line 2542249
    const-string p0, "Event"

    invoke-virtual {v4, p0}, LX/0oG;->b(Ljava/lang/String;)LX/0oG;

    .line 2542250
    invoke-virtual {v4, v1}, LX/0oG;->c(Ljava/lang/String;)LX/0oG;

    .line 2542251
    const-string p0, "mechanism"

    invoke-virtual {v4, p0, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2542252
    const-string p0, "composer_session_id"

    invoke-virtual {v4, p0, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2542253
    const-string p0, "event_id"

    invoke-virtual {v4, p0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2542254
    invoke-virtual {v4}, LX/0oG;->d()V

    .line 2542255
    :cond_3
    goto/16 :goto_0

    .line 2542256
    :cond_4
    const v1, 0x7f081f77

    invoke-static {v1, v3, v3}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a(IZZ)Landroid/support/v4/app/DialogFragment;

    move-result-object v1

    check-cast v1, Lcom/facebook/ui/dialogs/ProgressDialogFragment;

    iput-object v1, p0, LX/I91;->h:Lcom/facebook/ui/dialogs/ProgressDialogFragment;

    .line 2542257
    iget-object v1, p0, LX/I91;->h:Lcom/facebook/ui/dialogs/ProgressDialogFragment;

    new-instance v2, LX/I8z;

    invoke-direct {v2, p0}, LX/I8z;-><init>(LX/I91;)V

    invoke-virtual {v1, v2}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 2542258
    iget-object v1, p0, LX/I91;->h:Lcom/facebook/ui/dialogs/ProgressDialogFragment;

    new-instance v2, LX/I90;

    invoke-direct {v2, p0}, LX/I90;-><init>(LX/I91;)V

    invoke-virtual {v1, v2}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 2542259
    iget-object v1, p0, LX/I91;->h:Lcom/facebook/ui/dialogs/ProgressDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v2, "checkinDialog"

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2542260
    :cond_5
    new-instance v1, LX/4X0;

    invoke-direct {v1}, LX/4X0;-><init>()V

    .line 2542261
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel;->b()Ljava/lang/String;

    move-result-object v2

    .line 2542262
    iput-object v2, v1, LX/4X0;->b:Ljava/lang/String;

    .line 2542263
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel;->c()Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;

    move-result-object v2

    .line 2542264
    if-nez v2, :cond_6

    .line 2542265
    const/4 v3, 0x0

    .line 2542266
    :goto_2
    move-object v2, v3

    .line 2542267
    iput-object v2, v1, LX/4X0;->d:Lcom/facebook/graphql/model/GraphQLTaggableActivity;

    .line 2542268
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel;->d()Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel;

    move-result-object v2

    .line 2542269
    if-nez v2, :cond_7

    .line 2542270
    const/4 v3, 0x0

    .line 2542271
    :goto_3
    move-object v2, v3

    .line 2542272
    iput-object v2, v1, LX/4X0;->e:Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    .line 2542273
    invoke-virtual {v1}, LX/4X0;->a()Lcom/facebook/graphql/model/GraphQLInlineActivity;

    move-result-object v1

    goto/16 :goto_1

    .line 2542274
    :cond_6
    new-instance v3, LX/4Z3;

    invoke-direct {v3}, LX/4Z3;-><init>()V

    .line 2542275
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->b()Ljava/lang/String;

    move-result-object v4

    .line 2542276
    iput-object v4, v3, LX/4Z3;->e:Ljava/lang/String;

    .line 2542277
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->c()Ljava/lang/String;

    move-result-object v4

    .line 2542278
    iput-object v4, v3, LX/4Z3;->g:Ljava/lang/String;

    .line 2542279
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->d()I

    move-result v4

    .line 2542280
    iput v4, v3, LX/4Z3;->h:I

    .line 2542281
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->e()Ljava/lang/String;

    move-result-object v4

    .line 2542282
    iput-object v4, v3, LX/4Z3;->i:Ljava/lang/String;

    .line 2542283
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->y()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v4

    invoke-static {v4}, LX/7u9;->a(Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;)Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v4

    .line 2542284
    iput-object v4, v3, LX/4Z3;->j:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 2542285
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->x()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v4

    invoke-static {v4}, LX/7u9;->a(Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;)Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v4

    .line 2542286
    iput-object v4, v3, LX/4Z3;->k:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 2542287
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->w()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v4

    invoke-static {v4}, LX/7u9;->a(Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;)Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v4

    .line 2542288
    iput-object v4, v3, LX/4Z3;->n:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 2542289
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->v()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v4

    invoke-static {v4}, LX/7u9;->a(Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;)Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v4

    .line 2542290
    iput-object v4, v3, LX/4Z3;->o:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 2542291
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->u()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v4

    invoke-static {v4}, LX/7u9;->a(Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;)Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v4

    .line 2542292
    iput-object v4, v3, LX/4Z3;->p:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 2542293
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->t()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;

    move-result-object v4

    invoke-static {v4}, LX/7u9;->a(Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaePreviewTemplateModel;)Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v4

    .line 2542294
    iput-object v4, v3, LX/4Z3;->q:Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    .line 2542295
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityModel;->eO_()Ljava/lang/String;

    move-result-object v4

    .line 2542296
    iput-object v4, v3, LX/4Z3;->u:Ljava/lang/String;

    .line 2542297
    invoke-virtual {v3}, LX/4Z3;->a()Lcom/facebook/graphql/model/GraphQLTaggableActivity;

    move-result-object v3

    goto :goto_2

    .line 2542298
    :cond_7
    new-instance v3, LX/4Z5;

    invoke-direct {v3}, LX/4Z5;-><init>()V

    .line 2542299
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel;->b()Ljava/lang/String;

    move-result-object v4

    .line 2542300
    iput-object v4, v3, LX/4Z5;->c:Ljava/lang/String;

    .line 2542301
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel;->c()Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel$ImageModel;

    move-result-object v4

    .line 2542302
    if-nez v4, :cond_8

    .line 2542303
    const/4 v0, 0x0

    .line 2542304
    :goto_4
    move-object v4, v0

    .line 2542305
    iput-object v4, v3, LX/4Z5;->d:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2542306
    invoke-virtual {v3}, LX/4Z5;->a()Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    move-result-object v3

    goto/16 :goto_3

    .line 2542307
    :cond_8
    new-instance v0, LX/2dc;

    invoke-direct {v0}, LX/2dc;-><init>()V

    .line 2542308
    invoke-virtual {v4}, Lcom/facebook/events/graphql/EventsGraphQLModels$AttendingInlineActivityModel$TaggableActivityIconModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v2

    .line 2542309
    iput-object v2, v0, LX/2dc;->h:Ljava/lang/String;

    .line 2542310
    invoke-virtual {v0}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    goto :goto_4
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Z)V
    .locals 5
    .param p3    # Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2542311
    iget-boolean v0, p0, LX/I91;->j:Z

    if-nez v0, :cond_1

    .line 2542312
    iget-object v0, p0, LX/I91;->d:LX/1nQ;

    .line 2542313
    iget-object v1, p2, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2542314
    sget-object v2, Lcom/facebook/events/common/ActionMechanism;->HEADER:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v2}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2542315
    iget-object v3, v0, LX/1nQ;->i:LX/0Zb;

    const-string v4, "event_checkin_button_impression"

    const/4 p2, 0x0

    invoke-interface {v3, v4, p2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v3

    .line 2542316
    invoke-virtual {v3}, LX/0oG;->a()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2542317
    const-string v4, "event_permalink"

    invoke-virtual {v3, v4}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2542318
    iget-object v4, v0, LX/1nQ;->j:LX/0kv;

    iget-object p2, v0, LX/1nQ;->g:Landroid/content/Context;

    invoke-virtual {v4, p2}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    .line 2542319
    const-string v4, "Event"

    invoke-virtual {v3, v4}, LX/0oG;->b(Ljava/lang/String;)LX/0oG;

    .line 2542320
    invoke-virtual {v3, v1}, LX/0oG;->c(Ljava/lang/String;)LX/0oG;

    .line 2542321
    const-string v4, "mechanism"

    invoke-virtual {v3, v4, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2542322
    const-string v4, "event_id"

    invoke-virtual {v3, v4, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2542323
    invoke-virtual {v3}, LX/0oG;->d()V

    .line 2542324
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/I91;->j:Z

    .line 2542325
    :cond_1
    iput-object p3, p0, LX/I91;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 2542326
    iput-boolean p4, p0, LX/I91;->i:Z

    .line 2542327
    iget-object v0, p0, LX/I91;->h:Lcom/facebook/ui/dialogs/ProgressDialogFragment;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/I91;->h:Lcom/facebook/ui/dialogs/ProgressDialogFragment;

    invoke-virtual {v0}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2542328
    iget-object v0, p0, LX/I91;->h:Lcom/facebook/ui/dialogs/ProgressDialogFragment;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2542329
    iget-boolean v0, p0, LX/I91;->i:Z

    if-eqz v0, :cond_2

    .line 2542330
    iget-object v0, p0, LX/I91;->h:Lcom/facebook/ui/dialogs/ProgressDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2542331
    :cond_2
    iget-object v0, p0, LX/I91;->g:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    if-eqz v0, :cond_3

    .line 2542332
    iget-object v0, p0, LX/I91;->h:Lcom/facebook/ui/dialogs/ProgressDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2542333
    iget-object v0, p0, LX/I91;->h:Lcom/facebook/ui/dialogs/ProgressDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p0, v0}, LX/I91;->a$redex0(LX/I91;Landroid/content/Context;)V

    .line 2542334
    :cond_3
    iget-object v0, p0, LX/I91;->f:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_4

    .line 2542335
    new-instance v0, LX/I8y;

    invoke-direct {v0, p0}, LX/I8y;-><init>(LX/I91;)V

    iput-object v0, p0, LX/I91;->f:Landroid/view/View$OnClickListener;

    .line 2542336
    :cond_4
    iget-object v0, p0, LX/I91;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2542337
    return-void
.end method
