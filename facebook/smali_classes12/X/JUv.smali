.class public LX/JUv;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JUs;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JUx;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2699906
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/JUv;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JUx;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2699907
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2699908
    iput-object p1, p0, LX/JUv;->b:LX/0Ot;

    .line 2699909
    return-void
.end method

.method public static a(LX/0QB;)LX/JUv;
    .locals 4

    .prologue
    .line 2699910
    const-class v1, LX/JUv;

    monitor-enter v1

    .line 2699911
    :try_start_0
    sget-object v0, LX/JUv;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2699912
    sput-object v2, LX/JUv;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2699913
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2699914
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2699915
    new-instance v3, LX/JUv;

    const/16 p0, 0x2087

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JUv;-><init>(LX/0Ot;)V

    .line 2699916
    move-object v0, v3

    .line 2699917
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2699918
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JUv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2699919
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2699920
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Landroid/view/View;LX/1De;ZLX/1X1;)V
    .locals 6

    .prologue
    .line 2699921
    check-cast p4, LX/JUt;

    .line 2699922
    iget-object v0, p0, LX/JUv;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JUx;

    iget-object v4, p4, LX/JUt;->b:Lcom/facebook/graphql/model/GraphQLNode;

    iget-object v5, p4, LX/JUt;->c:LX/JVP;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-virtual/range {v0 .. v5}, LX/JUx;->onSaveClick(Landroid/view/View;LX/1De;ZLcom/facebook/graphql/model/GraphQLNode;LX/JVP;)V

    .line 2699923
    return-void
.end method

.method public static onSaveClick(LX/1X1;LX/1De;Z)LX/1dQ;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            "LX/1De;",
            "Z)",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2699924
    const v0, 0x2412ca5b

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 2699925
    check-cast p2, LX/JUt;

    .line 2699926
    iget-object v0, p0, LX/JUv;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JUx;

    iget-object v1, p2, LX/JUt;->a:Ljava/lang/Boolean;

    iget-object v2, p2, LX/JUt;->b:Lcom/facebook/graphql/model/GraphQLNode;

    .line 2699927
    if-nez v1, :cond_1

    .line 2699928
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->kT()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v3

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-eq v3, p0, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->kT()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v3

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLSavedState;->ARCHIVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne v3, p0, :cond_2

    :cond_0
    const/4 v3, 0x1

    :goto_0
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 2699929
    :cond_1
    iget-object v3, v0, LX/JUx;->a:LX/2g9;

    invoke-virtual {v3, p1}, LX/2g9;->c(LX/1De;)LX/2gA;

    move-result-object p0

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_3

    const/16 v3, 0x24

    :goto_1
    invoke-virtual {p0, v3}, LX/2gA;->h(I)LX/2gA;

    move-result-object p0

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_4

    const v3, 0x7f080d28

    :goto_2
    invoke-virtual {p0, v3}, LX/2gA;->i(I)LX/2gA;

    move-result-object v3

    const p0, 0x7f020784

    invoke-virtual {v3, p0}, LX/2gA;->k(I)LX/2gA;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    .line 2699930
    const p2, 0x2412ca5b

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {p1, p2, v0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p2

    move-object p0, p2

    .line 2699931
    invoke-interface {v3, p0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2699932
    return-object v0

    .line 2699933
    :cond_2
    const/4 v3, 0x0

    goto :goto_0

    .line 2699934
    :cond_3
    const/16 v3, 0x44

    goto :goto_1

    :cond_4
    const v3, 0x7f080d27

    goto :goto_2
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2699935
    invoke-static {}, LX/1dS;->b()V

    .line 2699936
    iget v0, p1, LX/1dQ;->b:I

    .line 2699937
    packed-switch v0, :pswitch_data_0

    .line 2699938
    :goto_0
    return-object v4

    .line 2699939
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2699940
    iget-object v2, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    check-cast v0, LX/1De;

    iget-object v1, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v3, 0x1

    aget-object v1, v1, v3

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iget-object v3, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v2, v0, v1, v3}, LX/JUv;->a(Landroid/view/View;LX/1De;ZLX/1X1;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2412ca5b
        :pswitch_0
    .end packed-switch
.end method

.method public final a(LX/1X1;LX/1X1;)V
    .locals 1

    .prologue
    .line 2699941
    check-cast p1, LX/JUt;

    .line 2699942
    check-cast p2, LX/JUt;

    .line 2699943
    iget-object v0, p1, LX/JUt;->a:Ljava/lang/Boolean;

    iput-object v0, p2, LX/JUt;->a:Ljava/lang/Boolean;

    .line 2699944
    return-void
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 2699945
    const/4 v0, 0x1

    return v0
.end method
