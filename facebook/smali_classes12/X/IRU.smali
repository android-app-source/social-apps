.class public LX/IRU;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/DaM;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/widget/CustomFrameLayout;",
        "LX/DaM",
        "<",
        "Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/DOL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2576510
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/IRU;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2576511
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2576512
    const v0, 0x7f0e0786

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2576513
    const-class v0, LX/IRU;

    invoke-static {v0, p0}, LX/IRU;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2576514
    const v0, 0x7f03084a

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2576515
    const v0, 0x7f0d159c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    .line 2576516
    new-instance p2, LX/IRT;

    invoke-direct {p2, p0}, LX/IRT;-><init>(LX/IRU;)V

    .line 2576517
    invoke-virtual {v0, p2}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2576518
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/IRU;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/DOL;->a(LX/0QB;)LX/DOL;

    move-result-object p0

    check-cast p0, LX/DOL;

    iput-object v1, p1, LX/IRU;->a:Lcom/facebook/content/SecureContextHelper;

    iput-object p0, p1, LX/IRU;->b:LX/DOL;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2576519
    check-cast p1, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 2576520
    iput-object p1, p0, LX/IRU;->c:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 2576521
    return-void
.end method
