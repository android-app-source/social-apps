.class public LX/JCJ;
.super LX/J8p;
.source ""


# instance fields
.field public final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionItemType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2661858
    invoke-direct {p0, p1}, LX/J8p;-><init>(LX/0Zb;)V

    .line 2661859
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/JCJ;->b:Ljava/util/Set;

    .line 2661860
    return-void
.end method

.method public static b(LX/0QB;)LX/JCJ;
    .locals 2

    .prologue
    .line 2661861
    new-instance v1, LX/JCJ;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-direct {v1, v0}, LX/JCJ;-><init>(LX/0Zb;)V

    .line 2661862
    return-object v1
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2661863
    const-string v0, "collections_overview"

    return-object v0
.end method
