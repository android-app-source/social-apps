.class public final LX/IhL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/IhK;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;)V
    .locals 0

    .prologue
    .line 2602412
    iput-object p1, p0, LX/IhL;->a:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;)V
    .locals 2

    .prologue
    .line 2602413
    iget-object v0, p0, LX/IhL;->a:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;

    iget-object v0, v0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->D:Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;

    .line 2602414
    iget-boolean v1, v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->b:Z

    move v0, v1

    .line 2602415
    if-eqz v0, :cond_0

    .line 2602416
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 2602417
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2602418
    iget-object v1, p0, LX/IhL;->a:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;

    invoke-static {v1, v0}, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->a$redex0(Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;Ljava/util/ArrayList;)V

    .line 2602419
    :goto_0
    return-void

    .line 2602420
    :cond_0
    iget-object v0, p0, LX/IhL;->a:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;

    iget-object v0, v0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->C:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2602421
    iget-object v0, p0, LX/IhL;->a:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;

    iget-object v0, v0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->C:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 2602422
    iget-object v0, p0, LX/IhL;->a:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;

    .line 2602423
    invoke-static {v0, p1}, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->b$redex0(Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;Lcom/facebook/ui/media/attachments/MediaResource;)V

    .line 2602424
    :goto_1
    iget-object v0, p0, LX/IhL;->a:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;

    invoke-static {v0}, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->n(Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;)V

    goto :goto_0

    .line 2602425
    :cond_1
    iget-object v0, p0, LX/IhL;->a:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;

    iget-object v0, v0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->C:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2602426
    iget-object v0, p0, LX/IhL;->a:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;

    .line 2602427
    invoke-static {v0, p1}, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->a$redex0(Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;Lcom/facebook/ui/media/attachments/MediaResource;)V

    .line 2602428
    goto :goto_1
.end method

.method public final b(Lcom/facebook/ui/media/attachments/MediaResource;)V
    .locals 9

    .prologue
    .line 2602429
    iget-object v0, p0, LX/IhL;->a:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;

    .line 2602430
    invoke-static {v0, p1}, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->b$redex0(Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;Lcom/facebook/ui/media/attachments/MediaResource;)V

    .line 2602431
    iget-object v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v1, LX/2MK;->PHOTO:LX/2MK;

    if-ne v0, v1, :cond_0

    .line 2602432
    iget-object v0, p0, LX/IhL;->a:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;

    .line 2602433
    iget-object v1, v0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->s:LX/Ihz;

    iget-object v2, p1, Lcom/facebook/ui/media/attachments/MediaResource;->r:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/Ihz;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2602434
    :goto_0
    return-void

    .line 2602435
    :cond_0
    iget-object v0, p0, LX/IhL;->a:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;

    const/4 v7, 0x0

    .line 2602436
    invoke-static {p1}, LX/Ii2;->a(Lcom/facebook/ui/media/attachments/MediaResource;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2602437
    new-instance v2, LX/0ju;

    invoke-direct {v2, v0}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v3, 0x7f082e11

    invoke-virtual {v2, v3}, LX/0ju;->a(I)LX/0ju;

    move-result-object v2

    const v3, 0x7f082e12

    invoke-virtual {v2, v3}, LX/0ju;->b(I)LX/0ju;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v2

    const v3, 0x7f080036

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v2

    invoke-virtual {v2}, LX/0ju;->b()LX/2EJ;

    .line 2602438
    :cond_1
    :goto_1
    goto :goto_0

    .line 2602439
    :cond_2
    invoke-static {p1}, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->a(Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    move-result-object v1

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v2

    const-string p0, "photo_edit_dialog_fragment_tag"

    invoke-virtual {v1, v2, p0}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    goto :goto_0

    .line 2602440
    :cond_3
    iget-object v2, v0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->t:LX/Ii1;

    iget-object v3, p1, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    const-string v4, "messenger_video_edit"

    invoke-virtual {v2, v3, v4}, LX/Ii1;->a(Landroid/net/Uri;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2602441
    iget-object v2, v0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->q:LX/Iht;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v5

    const-string v6, "VIDEO_EDIT"

    move-object v3, v0

    move-object v4, p1

    move-object v8, v7

    invoke-virtual/range {v2 .. v8}, LX/Iht;->a(Landroid/content/Context;Lcom/facebook/ui/media/attachments/MediaResource;LX/0gc;Ljava/lang/String;LX/6eE;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    goto :goto_1
.end method
