.class public LX/HbO;
.super LX/HbN;
.source ""


# instance fields
.field private final o:Landroid/widget/LinearLayout;

.field public final p:Lcom/facebook/resources/ui/FbTextView;

.field public final q:Lcom/facebook/resources/ui/FbTextView;

.field public final r:Lcom/facebook/resources/ui/FbButton;

.field public final s:Lcom/facebook/resources/ui/FbButton;

.field public final t:LX/Hbq;

.field public final u:Landroid/widget/FrameLayout;

.field private v:LX/HaY;

.field public w:LX/HaZ;


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/content/Context;LX/Hbq;LX/HaZ;)V
    .locals 2
    .param p1    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2485436
    invoke-direct {p0, p1, p2}, LX/HbN;-><init>(Landroid/view/View;Landroid/content/Context;)V

    .line 2485437
    const v0, 0x7f0d2bb9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/HbO;->o:Landroid/widget/LinearLayout;

    .line 2485438
    const v0, 0x7f0d2bbb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/HbO;->p:Lcom/facebook/resources/ui/FbTextView;

    .line 2485439
    const v0, 0x7f0d2bbc

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/HbO;->q:Lcom/facebook/resources/ui/FbTextView;

    .line 2485440
    const v0, 0x7f0d2bbd

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, LX/HbO;->r:Lcom/facebook/resources/ui/FbButton;

    .line 2485441
    const v0, 0x7f0d2bbe

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, LX/HbO;->s:Lcom/facebook/resources/ui/FbButton;

    .line 2485442
    iget-object v0, p0, LX/HbO;->s:Lcom/facebook/resources/ui/FbButton;

    const v1, 0x7f08363a

    invoke-virtual {p2, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2485443
    iput-object p3, p0, LX/HbO;->t:LX/Hbq;

    .line 2485444
    const v0, 0x7f0d2bba

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, LX/HbO;->u:Landroid/widget/FrameLayout;

    .line 2485445
    iput-object p4, p0, LX/HbO;->w:LX/HaZ;

    .line 2485446
    return-void
.end method

.method public static c(LX/HbO;I)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x190

    .line 2485449
    iget-object v0, p0, LX/HbO;->u:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 2485450
    iget-object v0, p0, LX/HbO;->p:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 2485451
    iget-object v0, p0, LX/HbO;->q:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 2485452
    iget-object v0, p0, LX/HbO;->r:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbButton;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 2485453
    iget-object v0, p0, LX/HbO;->s:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbButton;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 2485454
    return-void
.end method


# virtual methods
.method public final a(LX/Hbb;I)V
    .locals 3

    .prologue
    .line 2485455
    invoke-super {p0, p1, p2}, LX/HbN;->a(LX/Hbb;I)V

    .line 2485456
    iget-object v0, p0, LX/HbO;->o:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1a3;

    .line 2485457
    iget-object v1, p0, LX/HbN;->m:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v1, v0, LX/1a3;->height:I

    .line 2485458
    iget-object v1, p0, LX/HbO;->o:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2485459
    iget-object v0, p0, LX/HbO;->p:Lcom/facebook/resources/ui/FbTextView;

    .line 2485460
    iget-object v1, p1, LX/Hbb;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2485461
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2485462
    iget-object v0, p0, LX/HbO;->q:Lcom/facebook/resources/ui/FbTextView;

    .line 2485463
    iget-object v1, p1, LX/Hbb;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2485464
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2485465
    iget-object v0, p0, LX/HbO;->r:Lcom/facebook/resources/ui/FbButton;

    .line 2485466
    iget-object v1, p1, LX/Hbb;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2485467
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2485468
    iget-object v0, p0, LX/HbO;->r:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/HbL;

    invoke-direct {v1, p0}, LX/HbL;-><init>(LX/HbO;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2485469
    iget-object v0, p0, LX/HbO;->s:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/HbM;

    invoke-direct {v1, p0}, LX/HbM;-><init>(LX/HbO;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2485470
    iget-object v0, p0, LX/HbO;->u:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 2485471
    new-instance v0, LX/HaY;

    iget-object v1, p0, LX/HbO;->u:Landroid/widget/FrameLayout;

    iget-object v2, p0, LX/HbN;->l:LX/Hbb;

    .line 2485472
    iget-object p1, v2, LX/Hbb;->d:LX/Hba;

    move-object v2, p1

    .line 2485473
    invoke-direct {v0, v1, v2}, LX/HaY;-><init>(Landroid/widget/FrameLayout;LX/Hba;)V

    iput-object v0, p0, LX/HbO;->v:LX/HaY;

    .line 2485474
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    new-instance v1, Lcom/facebook/securitycheckup/items/SecurityCheckupConclusionViewHolder$3;

    invoke-direct {v1, p0}, Lcom/facebook/securitycheckup/items/SecurityCheckupConclusionViewHolder$3;-><init>(LX/HbO;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 2485475
    invoke-virtual {p0}, LX/HbO;->x()V

    .line 2485476
    return-void
.end method

.method public final x()V
    .locals 1

    .prologue
    .line 2485447
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/HbO;->c(LX/HbO;I)V

    .line 2485448
    return-void
.end method
