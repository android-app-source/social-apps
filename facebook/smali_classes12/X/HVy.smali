.class public LX/HVy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2475502
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2475503
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 4

    .prologue
    .line 2475504
    const-string v0, "com.facebook.katana.profile.id"

    const-wide/16 v2, -0x1

    invoke-virtual {p1, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 2475505
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2475506
    const-string v3, "arg_page_id"

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2475507
    new-instance v3, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;

    invoke-direct {v3}, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;-><init>()V

    .line 2475508
    invoke-virtual {v3, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2475509
    move-object v0, v3

    .line 2475510
    return-object v0
.end method
