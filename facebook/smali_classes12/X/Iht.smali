.class public LX/Iht;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2603047
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2603048
    iput-object p1, p0, LX/Iht;->a:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    .line 2603049
    return-void
.end method

.method public static b(LX/0QB;)LX/Iht;
    .locals 2

    .prologue
    .line 2603050
    new-instance v1, LX/Iht;

    invoke-static {p0}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a(LX/0QB;)Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    invoke-direct {v1, v0}, LX/Iht;-><init>(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;)V

    .line 2603051
    return-object v1
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/facebook/ui/media/attachments/MediaResource;LX/0gc;Ljava/lang/String;LX/6eE;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 5

    .prologue
    .line 2603052
    iget-object v0, p0, LX/Iht;->a:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    .line 2603053
    iget-object v1, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->i:LX/2MR;

    invoke-virtual {v1}, LX/2MR;->a()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 2603054
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6ed;

    .line 2603055
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FGc;

    .line 2603056
    iget-object v2, v2, LX/6ed;->b:LX/2MK;

    sget-object v4, LX/2MK;->VIDEO:LX/2MK;

    if-ne v2, v4, :cond_0

    iget-object v2, v1, LX/FGc;->b:LX/FGb;

    sget-object v4, LX/FGb;->IN_PROGRESS:LX/FGb;

    if-ne v2, v4, :cond_0

    iget-object v1, v1, LX/FGc;->e:LX/FGa;

    sget-object v2, LX/FGa;->TRANSCODING:LX/FGa;

    if-ne v1, v2, :cond_0

    .line 2603057
    const/4 v1, 0x1

    .line 2603058
    :goto_0
    move v0, v1

    .line 2603059
    if-eqz v0, :cond_1

    .line 2603060
    new-instance v0, LX/31Y;

    invoke-direct {v0, p1}, LX/31Y;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f083a16

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f083a17

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    const v1, 0x104000a

    new-instance v2, LX/Ihs;

    invoke-direct {v2, p0}, LX/Ihs;-><init>(LX/Iht;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 2603061
    :goto_1
    return-void

    .line 2603062
    :cond_1
    if-eqz p5, :cond_3

    .line 2603063
    invoke-static {p2, p5}, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;->a(Lcom/facebook/ui/media/attachments/MediaResource;LX/6eE;)Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;

    move-result-object v0

    .line 2603064
    :goto_2
    if-eqz p6, :cond_2

    .line 2603065
    iput-object p6, v0, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;->t:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2603066
    :cond_2
    invoke-virtual {v0, p3, p4}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    goto :goto_1

    .line 2603067
    :cond_3
    invoke-static {}, LX/6eE;->a()LX/6eE;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;->a(Lcom/facebook/ui/media/attachments/MediaResource;LX/6eE;)Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;

    move-result-object v0

    move-object v0, v0

    .line 2603068
    goto :goto_2

    :cond_4
    const/4 v1, 0x0

    goto :goto_0
.end method
