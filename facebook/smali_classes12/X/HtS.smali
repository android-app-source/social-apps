.class public final LX/HtS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/graphql/model/GraphQLProfile;",
        ">;",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/HtT;


# direct methods
.method public constructor <init>(LX/HtT;)V
    .locals 0

    .prologue
    .line 2516190
    iput-object p1, p0, LX/HtS;->a:LX/HtT;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2516191
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2516192
    if-nez p1, :cond_0

    .line 2516193
    const/4 v0, 0x0

    .line 2516194
    :goto_0
    return-object v0

    .line 2516195
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2516196
    check-cast v0, Lcom/facebook/graphql/model/GraphQLProfile;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->E()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    goto :goto_0
.end method
