.class public final LX/Hhi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$FetchZeroIPTestModel;",
        ">;",
        "LX/HhX;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Hhj;


# direct methods
.method public constructor <init>(LX/Hhj;)V
    .locals 0

    .prologue
    .line 2496112
    iput-object p1, p0, LX/Hhi;->a:LX/Hhj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 12
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2496113
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2496114
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2496115
    check-cast v0, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$FetchZeroIPTestModel;

    invoke-virtual {v0}, Lcom/facebook/zero/iptest/graphql/ZeroIPTestGraphQLModels$FetchZeroIPTestModel;->a()LX/1vs;

    move-result-object v0

    iget-object v8, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2496116
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2496117
    if-nez v0, :cond_0

    .line 2496118
    sget-object v0, LX/HhX;->a:LX/HhX;

    .line 2496119
    :goto_0
    return-object v0

    .line 2496120
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2496121
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v8, v0, v1}, LX/15i;->g(II)I

    move-result v9

    .line 2496122
    if-nez v9, :cond_1

    .line 2496123
    sget-object v0, LX/HhX;->a:LX/HhX;

    goto :goto_0

    .line 2496124
    :cond_1
    const/4 v0, 0x0

    const v1, -0x448b7ff

    invoke-static {v8, v9, v0, v1}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    .line 2496125
    :goto_1
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 2496126
    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v11

    :goto_2
    invoke-interface {v11}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v11}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v5, v0, LX/1vs;->b:I

    .line 2496127
    const/4 v0, 0x4

    :try_start_2
    invoke-virtual {v4, v5, v0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/HhL;->valueOfName(Ljava/lang/String;)LX/HhL;
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v1

    .line 2496128
    const/4 v0, 0x0

    invoke-virtual {v4, v5, v0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 2496129
    if-nez v0, :cond_4

    .line 2496130
    const-string v0, "0"

    move-object v7, v0

    .line 2496131
    :goto_3
    new-instance v0, LX/HhM;

    const/4 v2, 0x1

    invoke-virtual {v4, v5, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {v4, v5, v3}, LX/15i;->j(II)I

    move-result v3

    const/4 v6, 0x2

    invoke-virtual {v4, v5, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    const/4 v6, 0x1

    invoke-virtual {v8, v9, v6}, LX/15i;->j(II)I

    move-result v6

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    invoke-direct/range {v0 .. v7}, LX/HhM;-><init>(LX/HhL;Ljava/lang/String;ILjava/lang/String;Ljava/util/Collection;II)V

    .line 2496132
    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2496133
    :cond_2
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_1

    .line 2496134
    :catch_0
    move-exception v0

    .line 2496135
    const-class v1, LX/HhW;

    const-string v2, "TestType %s is unsupported, skipping"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    const/4 v7, 0x4

    invoke-virtual {v4, v5, v7}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    const/4 v4, 0x1

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 2496136
    :cond_3
    new-instance v0, LX/HhX;

    invoke-direct {v0}, LX/HhX;-><init>()V

    .line 2496137
    iput-object v10, v0, LX/HhX;->b:Ljava/util/List;

    .line 2496138
    goto/16 :goto_0

    :cond_4
    move-object v7, v0

    goto :goto_3
.end method
