.class public final LX/Hp5;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 2508057
    const/4 v8, 0x0

    .line 2508058
    const/4 v5, 0x0

    .line 2508059
    const-wide/16 v6, 0x0

    .line 2508060
    const/4 v4, 0x0

    .line 2508061
    const/4 v3, 0x0

    .line 2508062
    const/4 v2, 0x0

    .line 2508063
    const/4 v1, 0x0

    .line 2508064
    const/4 v0, 0x0

    .line 2508065
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v9, v10, :cond_a

    .line 2508066
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2508067
    const/4 v0, 0x0

    .line 2508068
    :goto_0
    return v0

    .line 2508069
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v0, v5, :cond_6

    .line 2508070
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v0

    .line 2508071
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2508072
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v11, :cond_0

    if-eqz v0, :cond_0

    .line 2508073
    const-string v5, "height"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2508074
    const/4 v0, 0x1

    .line 2508075
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    move v10, v4

    move v4, v0

    goto :goto_1

    .line 2508076
    :cond_1
    const-string v5, "name"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2508077
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    move v9, v0

    goto :goto_1

    .line 2508078
    :cond_2
    const-string v5, "scale"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2508079
    const/4 v0, 0x1

    .line 2508080
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v2

    move v1, v0

    goto :goto_1

    .line 2508081
    :cond_3
    const-string v5, "uri"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 2508082
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    move v8, v0

    goto :goto_1

    .line 2508083
    :cond_4
    const-string v5, "width"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2508084
    const/4 v0, 0x1

    .line 2508085
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v5

    move v6, v0

    move v7, v5

    goto :goto_1

    .line 2508086
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2508087
    :cond_6
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 2508088
    if-eqz v4, :cond_7

    .line 2508089
    const/4 v0, 0x0

    const/4 v4, 0x0

    invoke-virtual {p1, v0, v10, v4}, LX/186;->a(III)V

    .line 2508090
    :cond_7
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 2508091
    if-eqz v1, :cond_8

    .line 2508092
    const/4 v1, 0x2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 2508093
    :cond_8
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 2508094
    if-eqz v6, :cond_9

    .line 2508095
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v7, v1}, LX/186;->a(III)V

    .line 2508096
    :cond_9
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_a
    move v9, v5

    move v10, v8

    move v8, v4

    move v4, v2

    move-wide v12, v6

    move v6, v0

    move v7, v3

    move-wide v2, v12

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 2508097
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2508098
    invoke-virtual {p0, p1, v3, v3}, LX/15i;->a(III)I

    move-result v0

    .line 2508099
    if-eqz v0, :cond_0

    .line 2508100
    const-string v1, "height"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2508101
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2508102
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2508103
    if-eqz v0, :cond_1

    .line 2508104
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2508105
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2508106
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 2508107
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_2

    .line 2508108
    const-string v2, "scale"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2508109
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 2508110
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2508111
    if-eqz v0, :cond_3

    .line 2508112
    const-string v1, "uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2508113
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2508114
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 2508115
    if-eqz v0, :cond_4

    .line 2508116
    const-string v1, "width"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2508117
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2508118
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2508119
    return-void
.end method
