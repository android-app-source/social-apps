.class public final LX/I3E;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Hxd;

.field public final synthetic b:LX/I3F;


# direct methods
.method public constructor <init>(LX/I3F;LX/Hxd;)V
    .locals 0

    .prologue
    .line 2531187
    iput-object p1, p0, LX/I3E;->b:LX/I3F;

    iput-object p2, p0, LX/I3E;->a:LX/Hxd;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2531185
    iget-object v0, p0, LX/I3E;->a:LX/Hxd;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v3, v2, v3}, LX/Hxd;->a(Ljava/util/List;ILjava/lang/String;Z)V

    .line 2531186
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 2531160
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x0

    .line 2531161
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2531162
    if-eqz v0, :cond_0

    .line 2531163
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2531164
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel$SubscribedProfileCalendarEventsModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2531165
    :cond_0
    :goto_0
    return-void

    .line 2531166
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2531167
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel$SubscribedProfileCalendarEventsModel;

    move-result-object v3

    .line 2531168
    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel$SubscribedProfileCalendarEventsModel;->a()LX/0Px;

    move-result-object v4

    .line 2531169
    new-instance v5, Ljava/util/ArrayList;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v0

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 2531170
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v6

    move v2, v1

    :goto_1
    if-ge v2, v6, :cond_4

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;

    .line 2531171
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->q()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v7

    .line 2531172
    sget-object v8, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-eq v7, v8, :cond_2

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->SUBSCRIBED_ADMIN_CALENDAR:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-eq v7, v8, :cond_2

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne v7, v8, :cond_3

    :cond_2
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->Z()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v7

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-eq v7, v8, :cond_3

    .line 2531173
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2531174
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 2531175
    :cond_4
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2531176
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2531177
    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2531178
    if-nez v0, :cond_6

    move v0, v1

    .line 2531179
    :goto_2
    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchSubscribedEventsModel$SubscribedProfileCalendarEventsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v3

    .line 2531180
    iget-object v4, p0, LX/I3E;->a:LX/Hxd;

    if-nez v3, :cond_7

    const/4 v2, 0x0

    :goto_3
    if-eqz v3, :cond_5

    invoke-interface {v3}, LX/0ut;->b()Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v1, 0x1

    :cond_5
    invoke-interface {v4, v5, v0, v2, v1}, LX/Hxd;->a(Ljava/util/List;ILjava/lang/String;Z)V

    .line 2531181
    iget-object v0, p0, LX/I3E;->b:LX/I3F;

    invoke-static {v0, v5, p1}, LX/I3F;->a$redex0(LX/I3F;Ljava/util/List;Lcom/facebook/graphql/executor/GraphQLResult;)V

    goto :goto_0

    .line 2531182
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2531183
    :cond_6
    invoke-virtual {v2, v0, v1}, LX/15i;->j(II)I

    move-result v0

    goto :goto_2

    .line 2531184
    :cond_7
    invoke-interface {v3}, LX/0ut;->a()Ljava/lang/String;

    move-result-object v2

    goto :goto_3
.end method
