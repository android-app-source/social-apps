.class public final LX/Igb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/6eR;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;

.field private final b:Lcom/facebook/ui/media/attachments/MediaResource;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;Lcom/facebook/ui/media/attachments/MediaResource;)V
    .locals 0

    .prologue
    .line 2601295
    iput-object p1, p0, LX/Igb;->a:Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2601296
    iput-object p2, p0, LX/Igb;->b:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2601297
    return-void
.end method

.method private a(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 2601298
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/Igb;->a(Ljava/lang/String;ILjava/lang/Throwable;)V

    .line 2601299
    return-void
.end method

.method private a(Ljava/lang/String;ILjava/lang/Throwable;)V
    .locals 3
    .param p3    # Ljava/lang/Throwable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2601300
    iget-object v0, p0, LX/Igb;->a:Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;

    iget-object v0, v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->H:LX/Igt;

    iget-object v1, p0, LX/Igb;->b:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {v0, v1}, LX/Igt;->b(Lcom/facebook/ui/media/attachments/MediaResource;)V

    .line 2601301
    iget-object v0, p0, LX/Igb;->a:Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;

    iget-object v0, v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->c:LX/03V;

    const-string v1, "MediaPicker"

    invoke-virtual {v0, v1, p1, p3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2601302
    iget-object v0, p0, LX/Igb;->a:Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;

    invoke-virtual {v0, p2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2601303
    iget-object v1, p0, LX/Igb;->a:Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;

    iget-object v1, v1, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->j:LX/0kL;

    new-instance v2, LX/27k;

    invoke-direct {v2, v0}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    const/16 v0, 0x11

    .line 2601304
    iput v0, v2, LX/27k;->b:I

    .line 2601305
    move-object v0, v2

    .line 2601306
    invoke-virtual {v1, v0}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2601307
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2601308
    const-string v0, "Unknown media error"

    const v1, 0x7f082e0e

    invoke-direct {p0, v0, v1, p1}, LX/Igb;->a(Ljava/lang/String;ILjava/lang/Throwable;)V

    .line 2601309
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2601310
    check-cast p1, LX/6eR;

    .line 2601311
    sget-object v0, LX/Iga;->a:[I

    invoke-virtual {p1}, LX/6eR;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2601312
    :cond_0
    :goto_0
    return-void

    .line 2601313
    :pswitch_0
    iget-object v0, p0, LX/Igb;->a:Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;

    iget-object v0, v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->e:LX/FGd;

    iget-object v1, p0, LX/Igb;->b:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v1, v1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    invoke-virtual {v0, v1}, LX/FGd;->a(LX/2MK;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2601314
    invoke-static {}, Lcom/facebook/ui/media/attachments/MediaResource;->a()LX/5zn;

    move-result-object v0

    iget-object v1, p0, LX/Igb;->b:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {v0, v1}, LX/5zn;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5zn;

    move-result-object v0

    iget-object v1, p0, LX/Igb;->a:Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;

    iget-object v1, v1, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->F:Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;

    iget-object v1, v1, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->g:Ljava/lang/String;

    .line 2601315
    iput-object v1, v0, LX/5zn;->o:Ljava/lang/String;

    .line 2601316
    move-object v0, v0

    .line 2601317
    iget-object v1, p0, LX/Igb;->a:Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;

    iget-object v1, v1, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->F:Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;

    iget-object v1, v1, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->f:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2601318
    iput-object v1, v0, LX/5zn;->p:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2601319
    move-object v0, v0

    .line 2601320
    sget-object v1, LX/5zj;->MEDIA_PICKER_GALLERY:LX/5zj;

    .line 2601321
    iput-object v1, v0, LX/5zn;->d:LX/5zj;

    .line 2601322
    move-object v0, v0

    .line 2601323
    invoke-virtual {v0}, LX/5zn;->G()Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v0

    .line 2601324
    iget-object v1, p0, LX/Igb;->a:Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;

    iget-object v1, v1, Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;->e:LX/FGd;

    iget-object v2, p0, LX/Igb;->a:Lcom/facebook/messaging/media/mediapicker/MediaPickerFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    invoke-virtual {v1, v0}, LX/FGd;->a(Lcom/facebook/ui/media/attachments/MediaResource;)V

    goto :goto_0

    .line 2601325
    :pswitch_1
    const-string v0, "File does not exist"

    const v1, 0x7f082e0d

    invoke-direct {p0, v0, v1}, LX/Igb;->a(Ljava/lang/String;I)V

    goto :goto_0

    .line 2601326
    :pswitch_2
    const-string v0, "File not accessible"

    const v1, 0x7f082e0c

    invoke-direct {p0, v0, v1}, LX/Igb;->a(Ljava/lang/String;I)V

    goto :goto_0

    .line 2601327
    :pswitch_3
    const-string v0, "Media corrupted"

    const v1, 0x7f082e0e

    invoke-direct {p0, v0, v1}, LX/Igb;->a(Ljava/lang/String;I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
