.class public LX/IeF;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2598527
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 2598528
    return-void
.end method

.method public static a(LX/0SF;Ljava/util/concurrent/ScheduledExecutorService;LX/03V;LX/3Km;Landroid/content/res/Resources;LX/3Mk;LX/IeW;LX/3NE;LX/3NH;LX/3NI;Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;)LX/3Mi;
    .locals 10
    .param p1    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p7    # LX/3NE;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # LX/3NH;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # LX/3NI;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/annotations/ForMessengerBlendedSearchList;
    .end annotation

    .prologue
    .line 2598529
    const/4 v0, 0x1

    invoke-virtual {p5, v0}, LX/3Mk;->e(Z)V

    .line 2598530
    invoke-virtual {p3}, LX/3Km;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v6, 0x0

    :goto_0
    invoke-virtual {p3}, LX/3Km;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    move-object/from16 v7, p8

    :goto_1
    invoke-virtual {p3}, LX/3Km;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    move-object/from16 v8, p9

    :goto_2
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p5

    move-object/from16 v5, p6

    move-object/from16 v9, p10

    invoke-static/range {v0 .. v9}, LX/Ieb;->a(LX/0SF;Ljava/util/concurrent/ScheduledExecutorService;LX/03V;Landroid/content/res/Resources;LX/3Mk;LX/IeW;LX/3NE;LX/3NH;LX/3NI;Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;)LX/3Mi;

    move-result-object v0

    return-object v0

    :cond_0
    move-object/from16 v6, p7

    goto :goto_0

    :cond_1
    const/4 v7, 0x0

    goto :goto_1

    :cond_2
    const/4 v8, 0x0

    goto :goto_2
.end method

.method public static a(LX/0SF;Ljava/util/concurrent/ScheduledExecutorService;LX/03V;LX/3Km;Landroid/content/res/Resources;LX/3Mk;LX/IeW;LX/3NE;LX/3NH;Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;)LX/3Mi;
    .locals 11
    .param p1    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p7    # LX/3NE;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # LX/3NH;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/annotations/ForIdentityMatching;
    .end annotation

    .prologue
    .line 2598512
    const/4 v1, 0x1

    move-object/from16 v0, p5

    invoke-virtual {v0, v1}, LX/3Mk;->d(Z)V

    .line 2598513
    const/4 v1, 0x1

    move-object/from16 v0, p5

    invoke-virtual {v0, v1}, LX/3Mk;->b(Z)V

    .line 2598514
    const/4 v1, 0x1

    move-object/from16 v0, p6

    invoke-virtual {v0, v1}, LX/IeW;->b(Z)V

    .line 2598515
    invoke-virtual {p3}, LX/3Km;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v7, 0x0

    :goto_0
    invoke-virtual {p3}, LX/3Km;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    move-object/from16 v8, p8

    :goto_1
    const/4 v9, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v10, p9

    invoke-static/range {v1 .. v10}, LX/Ieb;->a(LX/0SF;Ljava/util/concurrent/ScheduledExecutorService;LX/03V;Landroid/content/res/Resources;LX/3Mk;LX/IeW;LX/3NE;LX/3NH;LX/3NI;Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;)LX/3Mi;

    move-result-object v1

    return-object v1

    :cond_0
    move-object/from16 v7, p7

    goto :goto_0

    :cond_1
    const/4 v8, 0x0

    goto :goto_1
.end method

.method public static a(LX/0SF;Ljava/util/concurrent/ScheduledExecutorService;LX/03V;LX/3Mk;Lcom/facebook/messaging/contacts/picker/filters/ContactPickerNonFriendUsersFilter;LX/3Lw;)LX/3Mi;
    .locals 6
    .param p1    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/annotations/ForFacebookList;
    .end annotation

    .prologue
    .line 2598516
    invoke-virtual {p5}, LX/3Lw;->a()Z

    move-result v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    .line 2598517
    new-instance p0, LX/3NS;

    const/4 p5, 0x0

    const/4 p2, 0x1

    const/4 p3, 0x0

    .line 2598518
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object p4

    .line 2598519
    if-nez v5, :cond_0

    move p1, p2

    .line 2598520
    :goto_0
    iput-boolean p1, v3, LX/3Ml;->d:Z

    .line 2598521
    new-instance p1, LX/3NU;

    invoke-direct {p1, v3, p5, p2}, LX/3NU;-><init>(LX/3Mi;Ljava/lang/String;Z)V

    invoke-virtual {p4, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2598522
    new-instance p1, LX/3NU;

    invoke-direct {p1, v4, p5, p3}, LX/3NU;-><init>(LX/3Mi;Ljava/lang/String;Z)V

    invoke-virtual {p4, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2598523
    invoke-virtual {p4}, LX/0Pz;->b()LX/0Px;

    move-result-object p1

    move-object p1, p1

    .line 2598524
    invoke-direct {p0, p1, v0, v1, v2}, LX/3NS;-><init>(LX/0Px;LX/0SG;Ljava/util/concurrent/ScheduledExecutorService;LX/03V;)V

    move-object v0, p0

    .line 2598525
    return-object v0

    :cond_0
    move p1, p3

    .line 2598526
    goto :goto_0
.end method

.method public static a(LX/0SF;Ljava/util/concurrent/ScheduledExecutorService;LX/03V;LX/3Mk;Ljava/lang/Boolean;)LX/3Mi;
    .locals 5
    .param p1    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/rtc/annotations/IsVoipVideoEnabled;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/annotations/ForVoipSearchList;
    .end annotation

    .prologue
    .line 2598502
    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 2598503
    new-instance v1, LX/3NS;

    const/4 p4, 0x1

    .line 2598504
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2598505
    iput-boolean p4, p3, LX/3Mk;->p:Z

    .line 2598506
    iput-boolean p4, p3, LX/3Mk;->s:Z

    .line 2598507
    iput-boolean v0, p3, LX/3Mk;->t:Z

    .line 2598508
    new-instance v3, LX/3NU;

    const/4 v4, 0x0

    invoke-direct {v3, p3, v4, p4}, LX/3NU;-><init>(LX/3Mi;Ljava/lang/String;Z)V

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2598509
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    move-object v2, v2

    .line 2598510
    invoke-direct {v1, v2, p0, p1, p2}, LX/3NS;-><init>(LX/0Px;LX/0SG;Ljava/util/concurrent/ScheduledExecutorService;LX/03V;)V

    move-object v0, v1

    .line 2598511
    return-object v0
.end method

.method public static a(LX/0SF;Ljava/util/concurrent/ScheduledExecutorService;LX/03V;Landroid/content/res/Resources;LX/3Mk;LX/3N9;LX/3NA;)LX/3Mi;
    .locals 7
    .param p1    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/annotations/ForVoipGroupCallList;
    .end annotation

    .prologue
    .line 2598501
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p3

    move-object v5, p5

    move-object v6, p6

    invoke-static/range {v0 .. v6}, LX/Ieh;->a(LX/0SF;Ljava/util/concurrent/ScheduledExecutorService;LX/03V;LX/3Mk;Landroid/content/res/Resources;LX/3N9;LX/3NA;)LX/3Mi;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 2598500
    return-void
.end method
