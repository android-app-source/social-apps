.class public final LX/HMy;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/services/PagesServicesItemFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/services/PagesServicesItemFragment;)V
    .locals 0

    .prologue
    .line 2457903
    iput-object p1, p0, LX/HMy;->a:Lcom/facebook/pages/common/services/PagesServicesItemFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2457904
    iget-object v0, p0, LX/HMy;->a:Lcom/facebook/pages/common/services/PagesServicesItemFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "PagesServicesItemFragment"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2457905
    iget-object v0, p0, LX/HMy;->a:Lcom/facebook/pages/common/services/PagesServicesItemFragment;

    const v1, 0x7f081816    # 1.8090007E38f

    invoke-static {v0, v1}, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->a$redex0(Lcom/facebook/pages/common/services/PagesServicesItemFragment;I)V

    .line 2457906
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 11

    .prologue
    .line 2457907
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2457908
    if-eqz p1, :cond_0

    .line 2457909
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2457910
    if-nez v0, :cond_1

    .line 2457911
    :cond_0
    iget-object v0, p0, LX/HMy;->a:Lcom/facebook/pages/common/services/PagesServicesItemFragment;

    const v1, 0x7f081816    # 1.8090007E38f

    invoke-static {v0, v1}, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->a$redex0(Lcom/facebook/pages/common/services/PagesServicesItemFragment;I)V

    .line 2457912
    :goto_0
    return-void

    .line 2457913
    :cond_1
    iget-object v1, p0, LX/HMy;->a:Lcom/facebook/pages/common/services/PagesServicesItemFragment;

    .line 2457914
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2457915
    check-cast v0, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;

    .line 2457916
    iput-object v0, v1, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->q:Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;

    .line 2457917
    iget-object v0, p0, LX/HMy;->a:Lcom/facebook/pages/common/services/PagesServicesItemFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->q:Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/HMy;->a:Lcom/facebook/pages/common/services/PagesServicesItemFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->q:Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->j()Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/HMy;->a:Lcom/facebook/pages/common/services/PagesServicesItemFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->q:Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->j()Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2457918
    iget-object v0, p0, LX/HMy;->a:Lcom/facebook/pages/common/services/PagesServicesItemFragment;

    iget-object v1, p0, LX/HMy;->a:Lcom/facebook/pages/common/services/PagesServicesItemFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->q:Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;

    invoke-virtual {v1}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->j()Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;->k()Ljava/lang/String;

    move-result-object v1

    .line 2457919
    iput-object v1, v0, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->n:Ljava/lang/String;

    .line 2457920
    iget-object v0, p0, LX/HMy;->a:Lcom/facebook/pages/common/services/PagesServicesItemFragment;

    invoke-static {v0}, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->b$redex0(Lcom/facebook/pages/common/services/PagesServicesItemFragment;)V

    .line 2457921
    :cond_2
    iget-object v0, p0, LX/HMy;->a:Lcom/facebook/pages/common/services/PagesServicesItemFragment;

    const/4 v10, 0x0

    .line 2457922
    iget-object v2, v0, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->j:Lcom/facebook/pages/common/services/PagesServiceItemLarge;

    if-eqz v2, :cond_6

    iget-object v2, v0, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->q:Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;

    if-eqz v2, :cond_6

    iget-object v2, v0, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->q:Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;

    invoke-virtual {v2}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->j()Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 2457923
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    .line 2457924
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->CALL_NOW:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    invoke-virtual {v7, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2457925
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->MESSAGE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    invoke-virtual {v7, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2457926
    iget-object v2, v0, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->g:LX/0Uh;

    const/16 v3, 0x249

    invoke-virtual {v2, v3, v10}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2457927
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->REQUEST_APPOINTMENT:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    invoke-virtual {v7, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2457928
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->BOOK_NOW:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    invoke-virtual {v7, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2457929
    :cond_3
    iget-object v2, v0, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->j:Lcom/facebook/pages/common/services/PagesServiceItemLarge;

    iget-object v3, v0, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->q:Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;

    invoke-static {v3}, LX/HMg;->a(LX/9Zu;)Landroid/net/Uri;

    move-result-object v3

    iget-object v4, v0, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->q:Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;

    invoke-virtual {v4}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->e()Ljava/lang/String;

    move-result-object v4

    iget-object v5, v0, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->q:Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;

    invoke-virtual {v5}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->c()Ljava/lang/String;

    move-result-object v5

    iget-object v6, v0, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->q:Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;

    invoke-virtual {v6}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->b()Ljava/lang/String;

    move-result-object v6

    iget-object v8, v0, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->q:Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;

    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v7

    .line 2457930
    if-eqz v8, :cond_7

    invoke-virtual {v8}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->j()Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;

    move-result-object v9

    if-eqz v9, :cond_7

    invoke-virtual {v8}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->j()Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;->l()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    move-result-object v9

    if-eqz v9, :cond_7

    invoke-virtual {v8}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->j()Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;->l()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->dT_()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v9

    if-eqz v9, :cond_7

    invoke-virtual {v8}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->j()Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;->l()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->dT_()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v9

    invoke-interface {v7, v9}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    const/4 v9, 0x1

    :goto_1
    move v7, v9

    .line 2457931
    iget-object v8, v0, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->o:Ljava/lang/String;

    iget-object v9, v0, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->q:Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;

    invoke-virtual/range {v2 .. v9}, Lcom/facebook/pages/common/services/PagesServiceItemLarge;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;)V

    .line 2457932
    iget-object v2, v0, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->k:Landroid/widget/ScrollView;

    invoke-virtual {v2, v10}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 2457933
    iget-object v2, v0, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->l:Landroid/widget/ProgressBar;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2457934
    iget-boolean v2, v0, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->p:Z

    if-nez v2, :cond_6

    iget-object v2, v0, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->q:Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;

    if-eqz v2, :cond_6

    iget-object v2, v0, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->q:Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;

    invoke-virtual {v2}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->j()Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 2457935
    iget-object v2, v0, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->q:Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;

    invoke-virtual {v2}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->j()Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;->m()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    if-eqz v2, :cond_4

    .line 2457936
    iget-object v2, v0, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->q:Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;

    invoke-virtual {v2}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->j()Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;->m()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2457937
    iget-object v4, v0, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->i:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v3, v2, v10}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 2457938
    :cond_4
    iget-object v2, v0, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->i:Lcom/facebook/fbui/widget/contentview/ContentView;

    iget-object v3, v0, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->q:Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;

    invoke-virtual {v3}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->j()Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2457939
    iget-object v2, v0, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->q:Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;

    invoke-virtual {v2}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->j()Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;->n()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2457940
    iget-object v2, v0, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->i:Lcom/facebook/fbui/widget/contentview/ContentView;

    iget-object v3, v0, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->q:Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;

    invoke-virtual {v3}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel;->j()Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/pages/identity/protocol/graphql/SingleServiceGraphQLModels$SingleServiceGraphQLModel$PageModel;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2457941
    :cond_5
    iget-object v2, v0, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->i:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v2, v10}, Lcom/facebook/fbui/widget/contentview/ContentView;->setVisibility(I)V

    .line 2457942
    iget-object v2, v0, Lcom/facebook/pages/common/services/PagesServicesItemFragment;->i:Lcom/facebook/fbui/widget/contentview/ContentView;

    new-instance v3, LX/HMz;

    invoke-direct {v3, v0}, LX/HMz;-><init>(Lcom/facebook/pages/common/services/PagesServicesItemFragment;)V

    invoke-virtual {v2, v3}, Lcom/facebook/fbui/widget/contentview/ContentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2457943
    :cond_6
    goto/16 :goto_0

    :cond_7
    const/4 v9, 0x0

    goto/16 :goto_1
.end method
