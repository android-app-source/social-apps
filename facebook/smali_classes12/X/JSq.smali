.class public LX/JSq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/24J;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;",
        ">",
        "Ljava/lang/Object;",
        "LX/24J",
        "<",
        "LX/JSO;",
        "TV;>;"
    }
.end annotation


# instance fields
.field private final a:LX/JSp;


# direct methods
.method public constructor <init>(LX/JSp;)V
    .locals 0

    .prologue
    .line 2696053
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2696054
    iput-object p1, p0, LX/JSq;->a:LX/JSp;

    .line 2696055
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Object;Landroid/view/View;)Ljava/lang/Runnable;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 2696056
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/util/List;Ljava/lang/Object;Ljava/lang/Object;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2696057
    check-cast p3, LX/JSO;

    check-cast p4, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;

    .line 2696058
    if-eqz p3, :cond_0

    iget-object v0, p3, LX/JSO;->a:LX/JSK;

    if-nez v0, :cond_1

    .line 2696059
    :cond_0
    :goto_0
    return-void

    .line 2696060
    :cond_1
    iget-object v1, p3, LX/JSO;->a:LX/JSK;

    iget v2, p3, LX/JSO;->b:I

    .line 2696061
    sget-object p0, LX/JSo;->a:[I

    invoke-virtual {v1}, LX/JSK;->ordinal()I

    move-result p1

    aget p0, p0, p1

    packed-switch p0, :pswitch_data_0

    .line 2696062
    iget-object p0, p4, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->c:LX/JSs;

    move-object p0, p0

    .line 2696063
    iget-boolean p1, p0, LX/JSs;->g:Z

    move p0, p1

    .line 2696064
    if-eqz p0, :cond_2

    .line 2696065
    iget-object p0, p4, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->c:LX/JSs;

    move-object p0, p0

    .line 2696066
    const/4 p1, 0x0

    iput-boolean p1, p0, LX/JSs;->g:Z

    .line 2696067
    iget-object p1, p0, LX/JSs;->d:LX/JSt;

    invoke-virtual {p1}, LX/JSt;->a()V

    .line 2696068
    invoke-static {p0}, LX/JSs;->e(LX/JSs;)V

    .line 2696069
    invoke-virtual {p4}, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->b()V

    .line 2696070
    invoke-virtual {p4}, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->f()V

    .line 2696071
    const/4 p0, 0x0

    invoke-virtual {p4, p0}, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->setPlayButtonVisibility(I)V

    .line 2696072
    :cond_2
    :goto_1
    goto :goto_0

    .line 2696073
    :pswitch_0
    invoke-static {p4}, LX/JSp;->a(Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;)V

    goto :goto_1

    .line 2696074
    :pswitch_1
    invoke-virtual {p4}, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->b()V

    .line 2696075
    iget-object p0, p4, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->c:LX/JSs;

    move-object p0, p0

    .line 2696076
    iget-boolean p1, p0, LX/JSs;->g:Z

    move p0, p1

    .line 2696077
    if-nez p0, :cond_3

    .line 2696078
    invoke-virtual {p4}, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->e()V

    .line 2696079
    const/4 p0, 0x0

    invoke-virtual {p4, p0}, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->setPlayButtonVisibility(I)V

    .line 2696080
    iget-object p0, p4, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->c:LX/JSs;

    move-object p0, p0

    .line 2696081
    iget-boolean p1, p0, LX/JSs;->g:Z

    if-eqz p1, :cond_4

    .line 2696082
    :goto_2
    goto :goto_1

    .line 2696083
    :pswitch_2
    iget-object p0, p4, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->c:LX/JSs;

    move-object p0, p0

    .line 2696084
    invoke-virtual {p0}, LX/JSs;->b()V

    .line 2696085
    invoke-virtual {p4}, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->b()V

    .line 2696086
    const/4 p0, 0x0

    invoke-virtual {p4, p0}, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->setPlayButtonVisibility(I)V

    .line 2696087
    goto :goto_1

    .line 2696088
    :pswitch_3
    invoke-static {p4}, LX/JSp;->a(Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;)V

    goto :goto_1

    .line 2696089
    :cond_3
    iget-object p0, p4, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->c:LX/JSs;

    move-object p0, p0

    .line 2696090
    const/4 p1, 0x1

    iput-boolean p1, p0, LX/JSs;->g:Z

    .line 2696091
    iget-object p1, p0, LX/JSs;->d:LX/JSt;

    invoke-virtual {p1, v2}, LX/JSt;->b(I)V

    .line 2696092
    iget-object p1, p0, LX/JSs;->c:LX/JSP;

    invoke-interface {p1}, LX/JSP;->g()V

    .line 2696093
    iget-object p1, p0, LX/JSs;->c:LX/JSP;

    invoke-interface {p1}, LX/JSP;->getVinylView()Lcom/facebook/feedplugins/musicstory/animations/VinylView;

    move-result-object p1

    iget-object p4, p0, LX/JSs;->f:LX/JSu;

    invoke-virtual {p1, p4}, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2696094
    goto :goto_2

    .line 2696095
    :cond_4
    const/4 p1, 0x1

    iput-boolean p1, p0, LX/JSs;->g:Z

    .line 2696096
    iget-object p1, p0, LX/JSs;->d:LX/JSt;

    invoke-virtual {p1, v2}, LX/JSt;->a(I)V

    .line 2696097
    iget-object p1, p0, LX/JSs;->c:LX/JSP;

    invoke-interface {p1}, LX/JSP;->g()V

    .line 2696098
    iget-object p1, p0, LX/JSs;->c:LX/JSP;

    invoke-interface {p1}, LX/JSP;->getVinylView()Lcom/facebook/feedplugins/musicstory/animations/VinylView;

    move-result-object p1

    iget-object p4, p0, LX/JSs;->b:Landroid/view/animation/AnimationSet;

    invoke-virtual {p1, p4}, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
