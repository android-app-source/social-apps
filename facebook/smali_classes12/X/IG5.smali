.class public LX/IG5;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/IG5;


# instance fields
.field private final a:LX/IFO;

.field public final b:LX/IFa;

.field private final c:LX/0ad;


# direct methods
.method public constructor <init>(LX/IFO;LX/IFa;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2555272
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2555273
    iput-object p1, p0, LX/IG5;->a:LX/IFO;

    .line 2555274
    iput-object p2, p0, LX/IG5;->b:LX/IFa;

    .line 2555275
    iput-object p3, p0, LX/IG5;->c:LX/0ad;

    .line 2555276
    return-void
.end method

.method public static a(LX/IFd;Ljava/lang/String;)LX/IFR;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2555266
    sget-object v0, LX/IFd;->c:LX/IFd;

    if-ne p0, v0, :cond_0

    move-object v0, v1

    .line 2555267
    :goto_0
    return-object v0

    .line 2555268
    :cond_0
    invoke-virtual {p0}, LX/622;->e()Ljava/util/List;

    move-result-object v0

    .line 2555269
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IFR;

    .line 2555270
    invoke-interface {v0}, LX/IFR;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 2555271
    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/IG5;
    .locals 6

    .prologue
    .line 2555253
    sget-object v0, LX/IG5;->d:LX/IG5;

    if-nez v0, :cond_1

    .line 2555254
    const-class v1, LX/IG5;

    monitor-enter v1

    .line 2555255
    :try_start_0
    sget-object v0, LX/IG5;->d:LX/IG5;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2555256
    if-eqz v2, :cond_0

    .line 2555257
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2555258
    new-instance p0, LX/IG5;

    invoke-static {v0}, LX/IFO;->a(LX/0QB;)LX/IFO;

    move-result-object v3

    check-cast v3, LX/IFO;

    const-class v4, LX/IFa;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/IFa;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-direct {p0, v3, v4, v5}, LX/IG5;-><init>(LX/IFO;LX/IFa;LX/0ad;)V

    .line 2555259
    move-object v0, p0

    .line 2555260
    sput-object v0, LX/IG5;->d:LX/IG5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2555261
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2555262
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2555263
    :cond_1
    sget-object v0, LX/IG5;->d:LX/IG5;

    return-object v0

    .line 2555264
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2555265
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static final a(LX/IFd;)Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/IFd;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2555241
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v1

    .line 2555242
    invoke-virtual {p0}, LX/622;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IFR;

    .line 2555243
    instance-of v3, v0, LX/IFZ;

    if-eqz v3, :cond_0

    .line 2555244
    invoke-interface {v0}, LX/IFR;->a()Ljava/lang/String;

    move-result-object v3

    .line 2555245
    invoke-interface {v0}, LX/IFR;->c()Ljava/lang/String;

    move-result-object v4

    .line 2555246
    new-instance v5, LX/0XI;

    invoke-direct {v5}, LX/0XI;-><init>()V

    sget-object p0, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-virtual {v5, p0, v3}, LX/0XI;->a(LX/0XG;Ljava/lang/String;)LX/0XI;

    move-result-object v3

    .line 2555247
    iput-object v4, v3, LX/0XI;->h:Ljava/lang/String;

    .line 2555248
    move-object v3, v3

    .line 2555249
    invoke-virtual {v3}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v3

    move-object v0, v3

    .line 2555250
    iget-object v3, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v3, v3

    .line 2555251
    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2555252
    :cond_1
    return-object v1
.end method

.method public static a(LX/IFd;LX/IG2;)V
    .locals 3

    .prologue
    .line 2555208
    sget-object v0, LX/IFd;->c:LX/IFd;

    if-ne p0, v0, :cond_1

    .line 2555209
    :cond_0
    return-void

    .line 2555210
    :cond_1
    invoke-virtual {p0}, LX/622;->e()Ljava/util/List;

    move-result-object v0

    .line 2555211
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IFR;

    .line 2555212
    check-cast v0, LX/IFZ;

    .line 2555213
    invoke-virtual {v0}, LX/IFZ;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/IG2;->b(Ljava/lang/String;)Z

    move-result v2

    .line 2555214
    iput-boolean v2, v0, LX/IFZ;->a:Z

    .line 2555215
    goto :goto_0
.end method

.method public static final a(LX/IFd;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/location/ImmutableLocation;)V
    .locals 1

    .prologue
    .line 2555238
    sget-object v0, LX/IFd;->c:LX/IFd;

    if-ne p0, v0, :cond_0

    .line 2555239
    :goto_0
    return-void

    .line 2555240
    :cond_0
    invoke-virtual {p0}, LX/622;->e()Ljava/util/List;

    move-result-object v0

    invoke-static {v0, p1, p2, p3, p4}, LX/IG5;->a(Ljava/lang/Iterable;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/location/ImmutableLocation;)V

    goto :goto_0
.end method

.method public static a(LX/IFd;Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 2555230
    sget-object v0, LX/IFd;->c:LX/IFd;

    if-ne p0, v0, :cond_1

    .line 2555231
    :cond_0
    return-void

    .line 2555232
    :cond_1
    invoke-virtual {p0}, LX/622;->e()Ljava/util/List;

    move-result-object v0

    .line 2555233
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IFR;

    .line 2555234
    invoke-interface {v0}, LX/IFR;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2555235
    check-cast v0, LX/IFZ;

    .line 2555236
    iput-boolean p2, v0, LX/IFZ;->a:Z

    .line 2555237
    goto :goto_0
.end method

.method public static a(Ljava/lang/Iterable;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/location/ImmutableLocation;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "LX/IFR;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/location/ImmutableLocation;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2555216
    if-nez p0, :cond_1

    .line 2555217
    :cond_0
    return-void

    .line 2555218
    :cond_1
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IFR;

    .line 2555219
    invoke-interface {v0}, LX/IFR;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2555220
    instance-of v1, v0, LX/IFZ;

    if-eqz v1, :cond_2

    move-object v1, v0

    .line 2555221
    check-cast v1, LX/IFZ;

    .line 2555222
    iput-object p2, v1, LX/IFZ;->f:Ljava/lang/String;

    .line 2555223
    move-object v1, v0

    .line 2555224
    check-cast v1, LX/IFZ;

    .line 2555225
    iput-object p3, v1, LX/IFZ;->g:Ljava/lang/String;

    .line 2555226
    instance-of v1, v0, LX/IFb;

    if-eqz v1, :cond_2

    .line 2555227
    check-cast v0, LX/IFb;

    .line 2555228
    iput-object p4, v0, LX/IFb;->a:Lcom/facebook/location/ImmutableLocation;

    .line 2555229
    goto :goto_0
.end method
