.class public LX/JMi;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2684585
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;",
            ">;)",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 2684601
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2684602
    check-cast v0, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;

    .line 2684603
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->o()LX/0Px;

    move-result-object v1

    .line 2684604
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_1

    :cond_0
    move-object v0, v2

    .line 2684605
    :goto_0
    return-object v0

    .line 2684606
    :cond_1
    invoke-virtual {v1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnitItem;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    .line 2684607
    if-eqz v1, :cond_2

    instance-of v3, v1, Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v3, :cond_3

    :cond_2
    move-object v0, v2

    .line 2684608
    goto :goto_0

    .line 2684609
    :cond_3
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    move-object v0, v1

    .line 2684610
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0, v2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;LX/0Px;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2684586
    invoke-static {p0}, LX/JMi;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 2684587
    if-nez v0, :cond_0

    move-object v0, v1

    .line 2684588
    :goto_0
    return-object v0

    .line 2684589
    :cond_0
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 2684590
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v4, 0x0

    .line 2684591
    invoke-static {v0}, LX/17E;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-nez v2, :cond_4

    move-object v2, v4

    .line 2684592
    :cond_1
    :goto_1
    move-object v0, v2

    .line 2684593
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    if-nez v2, :cond_3

    :cond_2
    move-object v0, v1

    .line 2684594
    goto :goto_0

    .line 2684595
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2684596
    :cond_4
    const/4 v2, 0x0

    move v3, v2

    :goto_2
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ge v3, v2, :cond_5

    .line 2684597
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2684598
    invoke-static {v2}, LX/1VO;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result p0

    if-nez p0, :cond_1

    .line 2684599
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    :cond_5
    move-object v2, v4

    .line 2684600
    goto :goto_1
.end method
