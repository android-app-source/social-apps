.class public final LX/HyG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Bni;


# instance fields
.field public final synthetic a:Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;)V
    .locals 0

    .prologue
    .line 2523772
    iput-object p1, p0, LX/HyG;->a:Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V
    .locals 2

    .prologue
    .line 2523773
    iget-object v0, p0, LX/HyG;->a:Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->a:LX/I7w;

    sget-object v1, Lcom/facebook/events/common/ActionMechanism;->DASHBOARD_ROW_GUEST_STATUS:Lcom/facebook/events/common/ActionMechanism;

    .line 2523774
    iget-object p0, v0, LX/I7w;->d:Lcom/facebook/events/model/Event;

    invoke-static {p0}, LX/Bm1;->a(Lcom/facebook/events/model/Event;)Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    move-result-object p0

    .line 2523775
    invoke-virtual {v0, p0, p2, v1}, LX/I7w;->a(LX/7oa;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/events/common/ActionMechanism;)V

    .line 2523776
    return-void
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V
    .locals 2

    .prologue
    .line 2523777
    iget-object v0, p0, LX/HyG;->a:Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->a:LX/I7w;

    sget-object v1, Lcom/facebook/events/common/ActionMechanism;->DASHBOARD_ROW_GUEST_STATUS:Lcom/facebook/events/common/ActionMechanism;

    .line 2523778
    iget-object p0, v0, LX/I7w;->d:Lcom/facebook/events/model/Event;

    invoke-static {p0}, LX/Bm1;->a(Lcom/facebook/events/model/Event;)Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    move-result-object p0

    .line 2523779
    invoke-virtual {v0, p0, p2, v1}, LX/I7w;->a(LX/7oa;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Lcom/facebook/events/common/ActionMechanism;)V

    .line 2523780
    return-void
.end method
