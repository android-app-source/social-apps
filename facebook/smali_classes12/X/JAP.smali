.class public final LX/JAP;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    .line 2655496
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2655497
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 2655498
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 2655499
    const/4 v2, 0x0

    .line 2655500
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_5

    .line 2655501
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2655502
    :goto_1
    move v1, v2

    .line 2655503
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2655504
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0

    .line 2655505
    :cond_1
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2655506
    :cond_2
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_4

    .line 2655507
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2655508
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2655509
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 2655510
    const-string v4, "list_items"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2655511
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2655512
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_ARRAY:LX/15z;

    if-ne v3, v4, :cond_3

    .line 2655513
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_ARRAY:LX/15z;

    if-eq v3, v4, :cond_3

    .line 2655514
    const/4 v4, 0x0

    .line 2655515
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v5, :cond_a

    .line 2655516
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2655517
    :goto_4
    move v3, v4

    .line 2655518
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 2655519
    :cond_3
    invoke-static {v1, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 2655520
    goto :goto_2

    .line 2655521
    :cond_4
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2655522
    invoke-virtual {p1, v2, v1}, LX/186;->b(II)V

    .line 2655523
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto :goto_1

    :cond_5
    move v1, v2

    goto :goto_2

    .line 2655524
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2655525
    :cond_7
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_9

    .line 2655526
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 2655527
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2655528
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_7

    if-eqz v6, :cond_7

    .line 2655529
    const-string v7, "heading_type"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 2655530
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/graphql/enums/GraphQLProfileFieldTextListItemHeadingType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLProfileFieldTextListItemHeadingType;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    goto :goto_5

    .line 2655531
    :cond_8
    const-string v7, "text"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 2655532
    invoke-static {p0, p1}, LX/JAS;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_5

    .line 2655533
    :cond_9
    const/4 v6, 0x2

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 2655534
    invoke-virtual {p1, v4, v5}, LX/186;->b(II)V

    .line 2655535
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, LX/186;->b(II)V

    .line 2655536
    invoke-virtual {p1}, LX/186;->d()I

    move-result v4

    goto :goto_4

    :cond_a
    move v3, v4

    move v5, v4

    goto :goto_5
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 10

    .prologue
    .line 2655537
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2655538
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_a

    .line 2655539
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    .line 2655540
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2655541
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2655542
    if-eqz v2, :cond_9

    .line 2655543
    const-string v3, "list_items"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2655544
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2655545
    const/4 v3, 0x0

    :goto_1
    invoke-virtual {p0, v2}, LX/15i;->c(I)I

    move-result v4

    if-ge v3, v4, :cond_8

    .line 2655546
    invoke-virtual {p0, v2, v3}, LX/15i;->q(II)I

    move-result v4

    const/4 v6, 0x0

    .line 2655547
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2655548
    invoke-virtual {p0, v4, v6}, LX/15i;->g(II)I

    move-result v5

    .line 2655549
    if-eqz v5, :cond_0

    .line 2655550
    const-string v5, "heading_type"

    invoke-virtual {p2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2655551
    invoke-virtual {p0, v4, v6}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2, v5}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2655552
    :cond_0
    const/4 v5, 0x1

    invoke-virtual {p0, v4, v5}, LX/15i;->g(II)I

    move-result v5

    .line 2655553
    if-eqz v5, :cond_7

    .line 2655554
    const-string v6, "text"

    invoke-virtual {p2, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2655555
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2655556
    const/4 v6, 0x0

    invoke-virtual {p0, v5, v6}, LX/15i;->g(II)I

    move-result v6

    .line 2655557
    if-eqz v6, :cond_5

    .line 2655558
    const-string v7, "ranges"

    invoke-virtual {p2, v7}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2655559
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2655560
    const/4 v7, 0x0

    :goto_2
    invoke-virtual {p0, v6}, LX/15i;->c(I)I

    move-result v8

    if-ge v7, v8, :cond_4

    .line 2655561
    invoke-virtual {p0, v6, v7}, LX/15i;->q(II)I

    move-result v8

    const/4 v4, 0x0

    .line 2655562
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2655563
    invoke-virtual {p0, v8, v4}, LX/15i;->g(II)I

    move-result v9

    .line 2655564
    if-eqz v9, :cond_1

    .line 2655565
    const-string v1, "entity"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2655566
    invoke-static {p0, v9, p2}, LX/JAR;->a(LX/15i;ILX/0nX;)V

    .line 2655567
    :cond_1
    const/4 v9, 0x1

    invoke-virtual {p0, v8, v9, v4}, LX/15i;->a(III)I

    move-result v9

    .line 2655568
    if-eqz v9, :cond_2

    .line 2655569
    const-string v1, "length"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2655570
    invoke-virtual {p2, v9}, LX/0nX;->b(I)V

    .line 2655571
    :cond_2
    const/4 v9, 0x2

    invoke-virtual {p0, v8, v9, v4}, LX/15i;->a(III)I

    move-result v9

    .line 2655572
    if-eqz v9, :cond_3

    .line 2655573
    const-string v1, "offset"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2655574
    invoke-virtual {p2, v9}, LX/0nX;->b(I)V

    .line 2655575
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2655576
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 2655577
    :cond_4
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2655578
    :cond_5
    const/4 v6, 0x1

    invoke-virtual {p0, v5, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v6

    .line 2655579
    if-eqz v6, :cond_6

    .line 2655580
    const-string v7, "text"

    invoke-virtual {p2, v7}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2655581
    invoke-virtual {p2, v6}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2655582
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2655583
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2655584
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    .line 2655585
    :cond_8
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2655586
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2655587
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 2655588
    :cond_a
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2655589
    return-void
.end method
