.class public final LX/IS8;
.super LX/DMC;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/DMC",
        "<",
        "LX/INf;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/ISI;


# direct methods
.method public constructor <init>(LX/ISI;LX/DML;)V
    .locals 0

    .prologue
    .line 2577486
    iput-object p1, p0, LX/IS8;->a:LX/ISI;

    invoke-direct {p0, p2}, LX/DMC;-><init>(LX/DML;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 9

    .prologue
    .line 2577487
    check-cast p1, LX/INf;

    .line 2577488
    iget-object v0, p0, LX/IS8;->a:LX/ISI;

    iget-object v0, v0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->Q()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel$SubgroupsFieldsModel;

    move-result-object v0

    iget-object v1, p0, LX/IS8;->a:LX/ISI;

    sget-object v2, LX/5QS;->ALL_SUB_GROUPS:LX/5QS;

    iget-object v3, p0, LX/IS8;->a:LX/ISI;

    iget-object v3, v3, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-static {v1, v2, v3}, LX/ISI;->a$redex0(LX/ISI;LX/5QS;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Landroid/view/View$OnClickListener;

    move-result-object v1

    const v6, 0x14954af1

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2577489
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel$SubgroupsFieldsModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$SubgroupsRelatedInformationModel$ChildGroupsModel;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel$SubgroupsFieldsModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$SubgroupsRelatedInformationModel$ChildGroupsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$SubgroupsRelatedInformationModel$ChildGroupsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel$SubgroupsFieldsModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$SubgroupsRelatedInformationModel$ChildGroupsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$SubgroupsRelatedInformationModel$ChildGroupsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2577490
    iget-object v2, p1, LX/INf;->c:Lcom/facebook/fig/footer/FigFooter;

    invoke-virtual {v2, v4}, Lcom/facebook/fig/footer/FigFooter;->setVisibility(I)V

    .line 2577491
    iget-object v2, p1, LX/INf;->c:Lcom/facebook/fig/footer/FigFooter;

    invoke-virtual {v2, v1}, Lcom/facebook/fig/footer/FigFooter;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2577492
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel$SubgroupsFieldsModel;->b()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    if-eqz v2, :cond_6

    .line 2577493
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel$SubgroupsFieldsModel;->b()LX/1vs;

    move-result-object v2

    iget-object v5, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    invoke-static {v5, v2, v4, v6}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v2

    .line 2577494
    if-eqz v2, :cond_4

    invoke-static {v2}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v2

    :goto_0
    if-eqz v2, :cond_5

    move v2, v3

    :goto_1
    if-eqz v2, :cond_9

    .line 2577495
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel$SubgroupsFieldsModel;->b()LX/1vs;

    move-result-object v2

    iget-object v5, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    invoke-static {v5, v2, v4, v6}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v2

    .line 2577496
    if-eqz v2, :cond_7

    invoke-static {v2}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v2

    :goto_2
    invoke-virtual {v2}, LX/39O;->a()Z

    move-result v2

    if-nez v2, :cond_8

    move v2, v3

    :goto_3
    if-eqz v2, :cond_2

    .line 2577497
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel$SubgroupsFieldsModel;->b()LX/1vs;

    move-result-object v2

    iget-object v5, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    invoke-static {v5, v2, v4, v6}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v2

    if-eqz v2, :cond_a

    invoke-static {v2}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v2

    :goto_4
    invoke-virtual {v2}, LX/3Sa;->e()LX/3Sh;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, LX/2sN;->a()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, LX/2sN;->b()LX/1vs;

    move-result-object v5

    iget-object v6, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    .line 2577498
    sget-object v7, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->SUB_GROUPS:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    const-class v8, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    invoke-virtual {v6, v5, v3, v8, p0}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 2577499
    iget-object v2, p1, LX/INf;->d:Lcom/facebook/fig/header/FigHeader;

    invoke-virtual {v6, v5, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/fig/header/FigHeader;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2577500
    :cond_2
    iget-object v2, p1, LX/INf;->a:LX/IL3;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel$SubgroupsFieldsModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$SubgroupsRelatedInformationModel$ChildGroupsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$SubgroupsRelatedInformationModel$ChildGroupsModel;->a()LX/0Px;

    move-result-object v3

    .line 2577501
    iput-object v3, v2, LX/IL3;->a:LX/0Px;

    .line 2577502
    const/4 v4, 0x1

    iput-boolean v4, v2, LX/IL3;->c:Z

    .line 2577503
    iget-object v2, p1, LX/INf;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2577504
    iget-object v3, v2, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v2, v3

    .line 2577505
    if-nez v2, :cond_3

    .line 2577506
    iget-object v2, p1, LX/INf;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v3, p1, LX/INf;->a:LX/IL3;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2577507
    :cond_3
    iget-object v2, p1, LX/INf;->a:LX/IL3;

    invoke-virtual {v2}, LX/1OM;->notifyDataSetChanged()V

    .line 2577508
    return-void

    .line 2577509
    :cond_4
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v2

    goto/16 :goto_0

    :cond_5
    move v2, v4

    goto/16 :goto_1

    :cond_6
    move v2, v4

    goto/16 :goto_1

    :cond_7
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v2

    goto :goto_2

    :cond_8
    move v2, v4

    goto :goto_3

    :cond_9
    move v2, v4

    goto :goto_3

    .line 2577510
    :cond_a
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v2

    goto :goto_4
.end method
