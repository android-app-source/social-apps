.class public final LX/Ho9;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/composer/minutiae/model/MinutiaeObject;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment$1;


# direct methods
.method public constructor <init>(Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment$1;)V
    .locals 0

    .prologue
    .line 2505294
    iput-object p1, p0, LX/Ho9;->a:Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment$1;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2505295
    iget-object v0, p0, LX/Ho9;->a:Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment$1;

    iget-object v0, v0, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment$1;->b:Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;

    iget-object v0, v0, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "fetchElectionHubMinutiae"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2505296
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2505297
    check-cast p1, Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 2505298
    iget-object v0, p0, LX/Ho9;->a:Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment$1;

    iget-object v0, v0, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment$1;->b:Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;

    .line 2505299
    iput-object p1, v0, Lcom/facebook/civicengagement/elections/ui/ElectionHubDiscussionFragment;->s:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 2505300
    return-void
.end method
