.class public LX/Iql;
.super LX/Iqk;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcom/facebook/common/callercontext/CallerContext;

.field public final c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private final d:LX/1Ad;

.field public final e:LX/Iqh;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2617257
    const-class v0, LX/Iql;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Iql;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/Iqh;Lcom/facebook/drawee/fbpipeline/FbDraweeView;Lcom/facebook/common/callercontext/CallerContext;LX/0wW;LX/1Ad;)V
    .locals 0
    .param p1    # LX/Iqh;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/drawee/fbpipeline/FbDraweeView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2617251
    invoke-direct {p0, p1, p2, p4}, LX/Iqk;-><init>(LX/Iqg;Landroid/view/View;LX/0wW;)V

    .line 2617252
    iput-object p1, p0, LX/Iql;->e:LX/Iqh;

    .line 2617253
    iput-object p2, p0, LX/Iql;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2617254
    iput-object p3, p0, LX/Iql;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 2617255
    iput-object p5, p0, LX/Iql;->d:LX/1Ad;

    .line 2617256
    return-void
.end method

.method private t()V
    .locals 3

    .prologue
    .line 2617248
    iget-object v1, p0, LX/Iql;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v0, p0, LX/Iql;->d:LX/1Ad;

    iget-object v2, p0, LX/Iql;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    iget-object v2, p0, LX/Iql;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    iget-object v2, p0, LX/Iql;->e:LX/Iqh;

    iget-object v2, v2, LX/Iqh;->a:Landroid/net/Uri;

    invoke-static {v2}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    new-instance v2, LX/Iqi;

    invoke-direct {v2, p0}, LX/Iqi;-><init>(LX/Iql;)V

    invoke-virtual {v0, v2}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2617249
    iget-object v0, p0, LX/Iql;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->invalidate()V

    .line 2617250
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 2617245
    invoke-super {p0}, LX/Iqk;->a()V

    .line 2617246
    invoke-direct {p0}, LX/Iql;->t()V

    .line 2617247
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2617235
    invoke-super {p0, p1}, LX/Iqk;->a(Ljava/lang/Object;)V

    .line 2617236
    instance-of v0, p1, LX/Iqn;

    if-eqz v0, :cond_2

    .line 2617237
    sget-object v0, LX/Iqj;->a:[I

    check-cast p1, LX/Iqn;

    invoke-virtual {p1}, LX/Iqn;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2617238
    :cond_0
    :goto_0
    return-void

    .line 2617239
    :pswitch_0
    iget-object v1, p0, LX/Iql;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v0, p0, LX/Iql;->e:LX/Iqh;

    .line 2617240
    iget-boolean p0, v0, LX/Iqg;->l:Z

    move v0, p0

    .line 2617241
    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x4

    goto :goto_1

    .line 2617242
    :cond_2
    instance-of v0, p1, LX/Iqf;

    if-eqz v0, :cond_0

    .line 2617243
    sget-object v0, LX/Iqj;->b:[I

    check-cast p1, LX/Iqf;

    invoke-virtual {p1}, LX/Iqf;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 2617244
    :pswitch_1
    invoke-direct {p0}, LX/Iql;->t()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
    .end packed-switch
.end method
