.class public LX/ICC;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ICD;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/ICC",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/ICD;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2549002
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2549003
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/ICC;->b:LX/0Zi;

    .line 2549004
    iput-object p1, p0, LX/ICC;->a:LX/0Ot;

    .line 2549005
    return-void
.end method

.method public static a(LX/0QB;)LX/ICC;
    .locals 4

    .prologue
    .line 2549006
    const-class v1, LX/ICC;

    monitor-enter v1

    .line 2549007
    :try_start_0
    sget-object v0, LX/ICC;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2549008
    sput-object v2, LX/ICC;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2549009
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2549010
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2549011
    new-instance v3, LX/ICC;

    const/16 p0, 0x216a

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/ICC;-><init>(LX/0Ot;)V

    .line 2549012
    move-object v0, v3

    .line 2549013
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2549014
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/ICC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2549015
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2549016
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Landroid/view/View;LX/1X1;)V
    .locals 10

    .prologue
    .line 2549017
    check-cast p2, LX/ICB;

    .line 2549018
    iget-object v0, p0, LX/ICC;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ICD;

    iget-object v1, p2, LX/ICB;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/ICB;->b:LX/1Po;

    const/4 v6, 0x0

    .line 2549019
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2549020
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const v4, -0x4bb9a0ed

    invoke-static {v3, v4}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v5

    .line 2549021
    if-nez v5, :cond_0

    .line 2549022
    :goto_0
    return-void

    .line 2549023
    :cond_0
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2549024
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2549025
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v4

    if-nez v4, :cond_2

    .line 2549026
    :cond_1
    :goto_1
    iget-object v3, v0, LX/ICD;->b:LX/17W;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v5

    const/16 v8, 0x3e7

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v7

    check-cast v7, Landroid/content/ContextWrapper;

    invoke-virtual {v7}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v7

    check-cast v7, Landroid/app/Activity;

    move-object v9, v7

    check-cast v9, Landroid/app/Activity;

    move-object v7, v6

    invoke-virtual/range {v3 .. v9}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;ILandroid/app/Activity;)Z

    goto :goto_0

    .line 2549027
    :cond_2
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v3

    .line 2549028
    iget-object v4, v0, LX/ICD;->c:LX/0Zb;

    const-string v7, "for_sale_item_message_seller_button_clicked"

    const/4 v8, 0x0

    invoke-interface {v4, v7, v8}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v4

    .line 2549029
    invoke-virtual {v4}, LX/0oG;->a()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 2549030
    const-string v7, "for_sale_item_id"

    invoke-virtual {v4, v7, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v3

    const-string v4, "surface"

    invoke-interface {v2}, LX/1Po;->c()LX/1PT;

    move-result-object v7

    invoke-static {v7}, LX/217;->a(LX/1PT;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v4, v7}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v3

    invoke-virtual {v3}, LX/0oG;->d()V

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 2549031
    check-cast p2, LX/ICB;

    .line 2549032
    iget-object v0, p0, LX/ICC;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ICD;

    iget-object v1, p2, LX/ICB;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2549033
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 2549034
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const p0, -0x4bb9a0ed

    invoke-static {v2, p0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v2

    .line 2549035
    if-nez v2, :cond_0

    .line 2549036
    const/4 v2, 0x0

    .line 2549037
    :goto_0
    move-object v0, v2

    .line 2549038
    return-object v0

    :cond_0
    iget-object p0, v0, LX/ICD;->a:LX/2g9;

    invoke-virtual {p0, p1}, LX/2g9;->c(LX/1De;)LX/2gA;

    move-result-object p0

    const/16 p2, 0x11

    invoke-virtual {p0, p2}, LX/2gA;->h(I)LX/2gA;

    move-result-object p0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/2gA;->a(Ljava/lang/CharSequence;)LX/2gA;

    move-result-object v2

    const p0, 0x7f020742

    invoke-virtual {v2, p0}, LX/2gA;->k(I)LX/2gA;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    .line 2549039
    const p0, 0x11157745

    const/4 p2, 0x0

    invoke-static {p1, p0, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 2549040
    invoke-interface {v2, p0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2549041
    invoke-static {}, LX/1dS;->b()V

    .line 2549042
    iget v0, p1, LX/1dQ;->b:I

    .line 2549043
    packed-switch v0, :pswitch_data_0

    .line 2549044
    :goto_0
    return-object v2

    .line 2549045
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2549046
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0, v1}, LX/ICC;->a(Landroid/view/View;LX/1X1;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x11157745
        :pswitch_0
    .end packed-switch
.end method
