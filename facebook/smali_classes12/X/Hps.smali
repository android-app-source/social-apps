.class public final LX/Hps;
.super LX/BcO;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/BcO",
        "<",
        "LX/Hpt;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public b:LX/Hpa;

.field public c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public d:[Ljava/lang/String;

.field public e:LX/Hpf;

.field public f:LX/2kW;

.field public g:LX/BcQ;

.field public final synthetic h:LX/Hpt;


# direct methods
.method public constructor <init>(LX/Hpt;)V
    .locals 1

    .prologue
    .line 2510012
    iput-object p1, p0, LX/Hps;->h:LX/Hpt;

    .line 2510013
    move-object v0, p1

    .line 2510014
    invoke-direct {p0, v0}, LX/BcO;-><init>(LX/BcS;)V

    .line 2510015
    return-void
.end method


# virtual methods
.method public final a(Z)LX/BcO;
    .locals 2

    .prologue
    .line 2510016
    invoke-super {p0, p1}, LX/BcO;->a(Z)LX/BcO;

    move-result-object v0

    check-cast v0, LX/Hps;

    .line 2510017
    if-nez p1, :cond_0

    .line 2510018
    const/4 v1, 0x0

    iput-object v1, v0, LX/Hps;->f:LX/2kW;

    .line 2510019
    :cond_0
    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2510020
    if-ne p0, p1, :cond_1

    .line 2510021
    :cond_0
    :goto_0
    return v0

    .line 2510022
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2510023
    goto :goto_0

    .line 2510024
    :cond_3
    check-cast p1, LX/Hps;

    .line 2510025
    iget-object v2, p0, LX/Hps;->b:LX/Hpa;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Hps;->b:LX/Hpa;

    iget-object v3, p1, LX/Hps;->b:LX/Hpa;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2510026
    goto :goto_0

    .line 2510027
    :cond_5
    iget-object v2, p1, LX/Hps;->b:LX/Hpa;

    if-nez v2, :cond_4

    .line 2510028
    :cond_6
    iget-object v2, p0, LX/Hps;->c:Ljava/util/Map;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/Hps;->c:Ljava/util/Map;

    iget-object v3, p1, LX/Hps;->c:Ljava/util/Map;

    invoke-interface {v2, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2510029
    goto :goto_0

    .line 2510030
    :cond_8
    iget-object v2, p1, LX/Hps;->c:Ljava/util/Map;

    if-nez v2, :cond_7

    .line 2510031
    :cond_9
    iget-object v2, p0, LX/Hps;->d:[Ljava/lang/String;

    iget-object v3, p1, LX/Hps;->d:[Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 2510032
    goto :goto_0

    .line 2510033
    :cond_a
    iget-object v2, p0, LX/Hps;->e:LX/Hpf;

    if-eqz v2, :cond_c

    iget-object v2, p0, LX/Hps;->e:LX/Hpf;

    iget-object v3, p1, LX/Hps;->e:LX/Hpf;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    :cond_b
    move v0, v1

    .line 2510034
    goto :goto_0

    .line 2510035
    :cond_c
    iget-object v2, p1, LX/Hps;->e:LX/Hpf;

    if-nez v2, :cond_b

    .line 2510036
    :cond_d
    iget-object v2, p0, LX/Hps;->f:LX/2kW;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/Hps;->f:LX/2kW;

    iget-object v3, p1, LX/Hps;->f:LX/2kW;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2510037
    goto :goto_0

    .line 2510038
    :cond_e
    iget-object v2, p1, LX/Hps;->f:LX/2kW;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
