.class public final LX/HnR;
.super LX/0DV;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

.field private b:LX/6Cz;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;)V
    .locals 0

    .prologue
    .line 2502161
    iput-object p1, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    invoke-direct {p0}, LX/0DV;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;B)V
    .locals 0

    .prologue
    .line 2502162
    invoke-direct {p0, p1}, LX/HnR;-><init>(Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;)V

    return-void
.end method

.method private a(Ljava/lang/String;LX/6Cx;)V
    .locals 6
    .param p2    # LX/6Cx;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 2502163
    invoke-static {p2}, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->a(LX/6Cx;)Ljava/lang/String;

    move-result-object v2

    .line 2502164
    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->c:LX/0gh;

    const-string v3, "system_page_load"

    invoke-virtual {v0, v3}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    move-result-object v0

    invoke-static {p1}, LX/D3T;->a(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v5

    move-object v3, v2

    move-object v4, v1

    invoke-virtual/range {v0 .. v5}, LX/0gh;->a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 2502165
    return-void
.end method

.method private c()V
    .locals 14

    .prologue
    .line 2502166
    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->E:LX/Hni;

    .line 2502167
    iget-object v1, v0, LX/Hni;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2502168
    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->E:LX/Hni;

    .line 2502169
    iget-object v10, v0, LX/Hni;->b:Ljava/util/Map;

    invoke-interface {v10, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_5

    .line 2502170
    const/4 v10, 0x0

    .line 2502171
    :goto_0
    move-object v2, v10

    .line 2502172
    if-nez v2, :cond_1

    .line 2502173
    :cond_0
    :goto_1
    return-void

    .line 2502174
    :cond_1
    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->F:LX/2nD;

    iget-boolean v3, v2, LX/Hnh;->a:Z

    iget-boolean v4, v2, LX/Hnh;->b:Z

    iget-wide v6, v2, LX/Hnh;->c:J

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    long-to-int v5, v6

    const/4 v9, -0x1

    const/4 v8, 0x0

    .line 2502175
    iget-object v6, v0, LX/2nD;->a:Ljava/util/Map;

    invoke-interface {v6, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map;

    .line 2502176
    if-eqz v6, :cond_2

    invoke-interface {v6}, Ljava/util/Map;->size()I

    move-result v7

    if-nez v7, :cond_6

    :cond_2
    move-object v6, v8

    .line 2502177
    :goto_2
    move-object v3, v6

    .line 2502178
    if-eqz v3, :cond_0

    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->F:LX/2nD;

    .line 2502179
    iget-object v4, v0, LX/2nD;->b:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    invoke-static {v3}, LX/7Fv;->b(Ljava/lang/String;)Z

    move-result v4

    move v0, v4

    .line 2502180
    if-eqz v0, :cond_0

    .line 2502181
    iget-boolean v0, v2, LX/Hnh;->b:Z

    if-eqz v0, :cond_3

    .line 2502182
    const-string v0, "FULL_LOAD"

    .line 2502183
    :goto_3
    sget-object v4, Lcom/facebook/browser/liteclient/rapidfeedback/BrowserLiteRapidFeedbackActivity;->p:Ljava/lang/String;

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v3, v5, v6

    const/4 v3, 0x1

    aput-object v1, v5, v3

    const/4 v1, 0x2

    aput-object v0, v5, v1

    const/4 v0, 0x3

    iget-wide v2, v2, LX/Hnh;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v0

    invoke-static {v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2502184
    iget-object v1, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v1, v1, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->G:LX/17Y;

    iget-object v2, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    invoke-interface {v1, v2, v0}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2502185
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2502186
    iget-object v1, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v1, v1, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->h:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_1

    .line 2502187
    :cond_3
    iget-boolean v0, v2, LX/Hnh;->a:Z

    if-eqz v0, :cond_4

    .line 2502188
    const-string v0, "TTI"

    goto :goto_3

    .line 2502189
    :cond_4
    const-string v0, "NONE"

    goto :goto_3

    .line 2502190
    :cond_5
    new-instance v11, LX/Hnh;

    invoke-direct {v11}, LX/Hnh;-><init>()V

    .line 2502191
    iget-object v10, v0, LX/Hni;->b:Ljava/util/Map;

    invoke-interface {v10, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, LX/Hnh;

    .line 2502192
    iget-boolean v12, v10, LX/Hnh;->a:Z

    iput-boolean v12, v11, LX/Hnh;->a:Z

    .line 2502193
    iget-boolean v12, v10, LX/Hnh;->b:Z

    iput-boolean v12, v11, LX/Hnh;->b:Z

    .line 2502194
    iget-wide v12, v10, LX/Hnh;->c:J

    iput-wide v12, v11, LX/Hnh;->c:J

    .line 2502195
    move-object v10, v11

    .line 2502196
    goto/16 :goto_0

    .line 2502197
    :cond_6
    invoke-interface {v6}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->size()I

    move-result v7

    new-array v10, v7, [Ljava/lang/Integer;

    .line 2502198
    invoke-interface {v6}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7, v10}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 2502199
    invoke-static {v10}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 2502200
    const/4 v7, 0x0

    :goto_4
    array-length v11, v10

    add-int/lit8 v11, v11, -0x1

    if-ge v7, v11, :cond_d

    .line 2502201
    aget-object v11, v10, v7

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    if-le v5, v11, :cond_8

    add-int/lit8 v11, v7, 0x1

    aget-object v11, v10, v11

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    if-ge v5, v11, :cond_8

    .line 2502202
    aget-object v7, v10, v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 2502203
    :goto_5
    if-ne v7, v9, :cond_7

    .line 2502204
    array-length v7, v10

    add-int/lit8 v7, v7, -0x1

    aget-object v7, v10, v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-le v5, v7, :cond_9

    .line 2502205
    array-length v7, v10

    add-int/lit8 v7, v7, -0x1

    aget-object v7, v10, v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 2502206
    :cond_7
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/Bag;

    .line 2502207
    if-nez v6, :cond_a

    move-object v6, v8

    .line 2502208
    goto/16 :goto_2

    .line 2502209
    :cond_8
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    :cond_9
    move-object v6, v8

    .line 2502210
    goto/16 :goto_2

    .line 2502211
    :cond_a
    if-eqz v4, :cond_b

    .line 2502212
    iget-object v6, v6, LX/Bag;->c:Ljava/lang/String;

    goto/16 :goto_2

    .line 2502213
    :cond_b
    if-eqz v3, :cond_c

    .line 2502214
    iget-object v6, v6, LX/Bag;->b:Ljava/lang/String;

    goto/16 :goto_2

    .line 2502215
    :cond_c
    iget-object v6, v6, LX/Bag;->a:Ljava/lang/String;

    goto/16 :goto_2

    :cond_d
    move v7, v9

    goto :goto_5
.end method

.method private d()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2502216
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v1, v1, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->o:LX/1Bn;

    .line 2502217
    iget-boolean p0, v1, LX/1Bn;->w:Z

    if-eqz p0, :cond_0

    .line 2502218
    iget-object p0, v1, LX/1Bn;->j:Ljava/util/Map;

    invoke-interface {p0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object p0

    .line 2502219
    :goto_0
    move-object v1, p0

    .line 2502220
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0

    :cond_0
    new-instance p0, Ljava/util/HashSet;

    invoke-direct {p0}, Ljava/util/HashSet;-><init>()V

    goto :goto_0
.end method

.method private d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2502221
    sget-object v0, LX/21D;->IN_APP_BROWSER:LX/21D;

    const-string v1, "browserLiteCallback"

    invoke-static {p2}, LX/89G;->a(Ljava/lang/String;)LX/89G;

    move-result-object v2

    .line 2502222
    iput-object p1, v2, LX/89G;->e:Ljava/lang/String;

    .line 2502223
    move-object v2, v2

    .line 2502224
    invoke-virtual {v2}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/1nC;->a(LX/21D;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    sget-object v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    .line 2502225
    iget-object v1, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v1, v1, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->n:LX/1Kf;

    const/4 v2, 0x0

    iget-object v3, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    invoke-interface {v1, v2, v0, v3}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Landroid/content/Context;)V

    .line 2502226
    return-void
.end method

.method private e()Z
    .locals 3

    .prologue
    .line 2502227
    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->y:LX/0ad;

    sget-short v1, LX/1Bm;->a:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2502228
    if-eqz p1, :cond_2

    const-string v1, "fb://close/"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 2502229
    if-eqz v1, :cond_1

    .line 2502230
    const/4 v0, 0x2

    .line 2502231
    :cond_0
    :goto_1
    return v0

    .line 2502232
    :cond_1
    iget-object v1, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v1, v1, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->s:LX/0Uh;

    const/16 v2, 0xc7

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2502233
    const/4 v0, 0x3

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final synthetic a()Ljava/util/List;
    .locals 1

    .prologue
    .line 2502234
    invoke-direct {p0}, LX/HnR;->d()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 2502235
    const/16 v0, 0x64

    if-ne p1, v0, :cond_1

    .line 2502236
    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/1C0;->i:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2502237
    :cond_0
    :goto_0
    return-void

    .line 2502238
    :cond_1
    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->y:LX/0ad;

    sget-short v1, LX/1Bm;->U:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2502239
    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->v:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/1C0;->i:LX/0Tn;

    invoke-interface {v0, v1, p1}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2502006
    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->q:LX/H5w;

    iget-object v1, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    invoke-virtual {v0, v1, p1}, LX/H5w;->a(Landroid/content/Context;Landroid/os/Bundle;)V

    .line 2502007
    return-void
.end method

.method public final a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V
    .locals 2

    .prologue
    .line 2502240
    iget-object v0, p0, LX/HnR;->b:LX/6Cz;

    if-nez v0, :cond_0

    .line 2502241
    :goto_0
    return-void

    .line 2502242
    :cond_0
    iget-object v0, p0, LX/HnR;->b:LX/6Cz;

    const-string v1, "browser_extensions_autofill_dialog_accepted"

    invoke-virtual {v0, p1, v1}, LX/6Cz;->a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;LX/0Da;)V
    .locals 13

    .prologue
    .line 2502243
    iget-object v0, p0, LX/HnR;->b:LX/6Cz;

    if-eqz v0, :cond_2

    .line 2502244
    iget-object v0, p0, LX/HnR;->b:LX/6Cz;

    iget-object v1, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    invoke-virtual {v1}, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 2502245
    iget-object v2, v0, LX/6Cz;->g:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6CG;

    .line 2502246
    iget-object v4, v0, LX/6Cz;->i:Landroid/os/Bundle;

    invoke-interface {v2, v4}, LX/6C9;->a(Landroid/os/Bundle;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2502247
    invoke-interface {v2, p1}, LX/6CG;->a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V

    goto :goto_0

    .line 2502248
    :cond_1
    iget-object v2, v0, LX/6Cz;->j:LX/6D3;

    .line 2502249
    iget-object v5, p1, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->b:Ljava/lang/String;

    move-object v5, v5

    .line 2502250
    if-eqz v5, :cond_2

    .line 2502251
    iget-object v5, p1, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->d:Ljava/lang/String;

    move-object v5, v5

    .line 2502252
    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 2502253
    iget-object v5, p1, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->b:Ljava/lang/String;

    move-object v5, v5

    .line 2502254
    const-string v7, "_FBExtensions"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 2502255
    :cond_2
    :goto_1
    return-void

    .line 2502256
    :cond_3
    const-string v5, "JS_BRIDGE_WHITELISTED_DOMAINS"

    invoke-virtual {p1, v5}, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    .line 2502257
    iget-object v7, v2, LX/6D3;->d:LX/6D1;

    .line 2502258
    iget-object v8, p1, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->f:Ljava/lang/String;

    move-object v8, v8

    .line 2502259
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 2502260
    invoke-static {v8}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 2502261
    :cond_4
    :goto_2
    move v7, v9

    .line 2502262
    if-nez v7, :cond_5

    iget-object v7, v2, LX/6D3;->d:LX/6D1;

    .line 2502263
    iget-object v8, p1, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->c:Landroid/os/Bundle;

    move-object v8, v8

    .line 2502264
    iget-object v9, p1, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->f:Ljava/lang/String;

    move-object v9, v9

    .line 2502265
    invoke-virtual {v7, v8, v9, v5}, LX/6D1;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/util/List;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 2502266
    iget-object v5, v2, LX/6D3;->c:LX/03V;

    const-string v7, "BrowserExtensionsJSBridge"

    const-string v8, "Call %s not allowed due to unsafe url %s"

    .line 2502267
    iget-object v9, p1, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->d:Ljava/lang/String;

    move-object v9, v9

    .line 2502268
    iget-object v10, p1, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->f:Ljava/lang/String;

    move-object v10, v10

    .line 2502269
    invoke-static {v8, v9, v10}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2502270
    goto :goto_1

    .line 2502271
    :cond_5
    iget-object v5, v2, LX/6D3;->b:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_6
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_8

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    move-object v11, v5

    check-cast v11, LX/6CR;

    .line 2502272
    iget-object v5, p1, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->d:Ljava/lang/String;

    move-object v5, v5

    .line 2502273
    invoke-interface {v11}, LX/6CR;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 2502274
    iget-object v5, p1, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->d:Ljava/lang/String;

    move-object v5, v5

    .line 2502275
    sget-object v8, LX/6D3;->a:LX/0P1;

    if-nez v8, :cond_7

    .line 2502276
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v8

    const-string v9, "requestCredentials"

    sget-object v10, Lcom/facebook/browserextensions/ipc/RequestCredentialsJSBridgeCall;->CREATOR:LX/0EY;

    invoke-virtual {v8, v9, v10}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v8

    const-string v9, "requestAuthorizedCredentials"

    sget-object v10, Lcom/facebook/browserextensions/ipc/RequestAuthorizedCredentialsJSBridgeCall;->CREATOR:LX/0EY;

    invoke-virtual {v8, v9, v10}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v8

    const-string v9, "processPayment"

    sget-object v10, Lcom/facebook/browserextensions/ipc/ProcessPaymentJSBridgeCall;->CREATOR:LX/0EY;

    invoke-virtual {v8, v9, v10}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v8

    const-string v9, "updateCart"

    sget-object v10, Lcom/facebook/browserextensions/ipc/commerce/UpdateCartJSBridgeCall;->CREATOR:LX/0EY;

    invoke-virtual {v8, v9, v10}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v8

    const-string v9, "resetCart"

    sget-object v10, Lcom/facebook/browserextensions/ipc/commerce/ResetCartJSBridgeCall;->CREATOR:LX/0EY;

    invoke-virtual {v8, v9, v10}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v8

    const-string v9, "purchase_complete"

    sget-object v10, Lcom/facebook/browserextensions/ipc/commerce/PurchaseCompleteJSBridgeCall;->CREATOR:LX/0EY;

    invoke-virtual {v8, v9, v10}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v8

    const-string v9, "requestUserInfoField"

    sget-object v10, Lcom/facebook/browserextensions/ipc/RequestUserInfoFieldJSBridgeCall;->CREATOR:LX/0EY;

    invoke-virtual {v8, v9, v10}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v8

    const-string v9, "requestCurrentPosition"

    sget-object v10, Lcom/facebook/browserextensions/ipc/RequestCurrentPositionJSBridgeCall;->CREATOR:LX/0EY;

    invoke-virtual {v8, v9, v10}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v8

    const-string v9, "getUserID"

    sget-object v10, Lcom/facebook/browserextensions/ipc/GetUserIDJSBridgeCall;->CREATOR:LX/0EY;

    invoke-virtual {v8, v9, v10}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v8

    const-string v9, "begin_share_flow"

    sget-object v10, Lcom/facebook/browserextensions/ipc/BeginShareFlowJSBridgeCall;->CREATOR:LX/0EY;

    invoke-virtual {v8, v9, v10}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v8

    const-string v9, "requestCloseBrowser"

    sget-object v10, Lcom/facebook/browserextensions/ipc/RequestCloseBrowserJSBridgeCall;->CREATOR:LX/0EY;

    invoke-virtual {v8, v9, v10}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v8

    const-string v9, "hasCapability"

    sget-object v10, Lcom/facebook/browserextensions/ipc/HasCapabilityJSBridgeCall;->CREATOR:LX/0EY;

    invoke-virtual {v8, v9, v10}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v8

    const-string v9, "requestAutoFill"

    sget-object v10, Lcom/facebook/browserextensions/ipc/RequestAutoFillJSBridgeCall;->CREATOR:LX/0EY;

    invoke-virtual {v8, v9, v10}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v8

    const-string v9, "requestFillOfferCode"

    sget-object v10, Lcom/facebook/browserextensions/ipc/RequestOfferCodeJSBridgeCall;->CREATOR:LX/0EY;

    invoke-virtual {v8, v9, v10}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v8

    const-string v9, "saveAutofillData"

    sget-object v10, Lcom/facebook/browserextensions/ipc/SaveAutofillDataJSBridgeCall;->CREATOR:LX/0EY;

    invoke-virtual {v8, v9, v10}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v8

    const-string v9, "paymentsCheckout"

    sget-object v10, Lcom/facebook/browserextensions/ipc/PaymentsCheckoutJSBridgeCall;->CREATOR:LX/0EY;

    invoke-virtual {v8, v9, v10}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v8

    const-string v9, "paymentsCheckoutShippingAddressReturn"

    sget-object v10, Lcom/facebook/browserextensions/ipc/PaymentsCheckoutShippingAddressReturnJSBridgeCall;->CREATOR:LX/0EY;

    invoke-virtual {v8, v9, v10}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v8

    const-string v9, "requestProductUrl"

    sget-object v10, Lcom/facebook/browserextensions/ipc/RequestUpdateProductHistoryJSBridgeCall;->CREATOR:LX/0EY;

    invoke-virtual {v8, v9, v10}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v8

    invoke-virtual {v8}, LX/0P2;->b()LX/0P1;

    move-result-object v8

    sput-object v8, LX/6D3;->a:LX/0P1;

    .line 2502277
    :cond_7
    sget-object v8, LX/6D3;->a:LX/0P1;

    invoke-virtual {v8, v5}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/0EY;

    move-object v5, v8

    .line 2502278
    if-eqz v5, :cond_6

    .line 2502279
    iget-object v6, p1, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->b:Ljava/lang/String;

    move-object v7, v6

    .line 2502280
    iget-object v6, p1, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->c:Landroid/os/Bundle;

    move-object v8, v6

    .line 2502281
    iget-object v6, p1, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->f:Ljava/lang/String;

    move-object v9, v6

    .line 2502282
    iget-object v6, p1, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->e:Landroid/os/Bundle;

    move-object v10, v6

    .line 2502283
    move-object v6, v1

    invoke-interface/range {v5 .. v10}, LX/0EY;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Landroid/os/Bundle;)Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;

    move-result-object v5

    .line 2502284
    new-instance v6, LX/6D2;

    invoke-direct {v6, v2, p2}, LX/6D2;-><init>(LX/6D3;LX/0Da;)V

    move-object v6, v6

    .line 2502285
    iput-object v6, v5, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->g:LX/0DZ;

    .line 2502286
    move-object v5, v5

    .line 2502287
    invoke-interface {v11, v5}, LX/6CR;->a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V

    .line 2502288
    goto/16 :goto_1

    .line 2502289
    :cond_8
    goto/16 :goto_1

    .line 2502290
    :cond_9
    iget-object v11, v7, LX/6D1;->a:LX/0Uh;

    const/16 v12, 0x542

    invoke-virtual {v11, v12, v9}, LX/0Uh;->a(IZ)Z

    move-result v11

    if-eqz v11, :cond_a

    move v9, v10

    .line 2502291
    goto/16 :goto_2

    .line 2502292
    :cond_a
    iget-object v11, p1, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->d:Ljava/lang/String;

    move-object v11, v11

    .line 2502293
    const-string v12, "requestAutoFill"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 2502294
    iget-object v11, v7, LX/6D1;->a:LX/0Uh;

    const/16 v12, 0x50c

    invoke-virtual {v11, v12, v9}, LX/0Uh;->a(IZ)Z

    move-result v11

    if-eqz v11, :cond_4

    move v9, v10

    .line 2502295
    goto/16 :goto_2
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2502296
    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->s:LX/0Uh;

    const/16 v1, 0xc8

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_2

    if-le p2, v3, :cond_2

    .line 2502297
    invoke-direct {p0}, LX/HnR;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2502298
    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->b:LX/D3V;

    const-string v1, ""

    invoke-virtual {v0, v3, v1}, LX/D3V;->a(ZLjava/lang/String;)V

    .line 2502299
    :goto_0
    iget-object v0, p0, LX/HnR;->b:LX/6Cz;

    if-eqz v0, :cond_0

    .line 2502300
    iget-object v0, p0, LX/HnR;->b:LX/6Cz;

    .line 2502301
    iget-object v1, v0, LX/6Cz;->b:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_1

    .line 2502302
    :cond_0
    return-void

    .line 2502303
    :cond_1
    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->b:LX/D3V;

    const/4 v1, 0x0

    invoke-virtual {v0, v3, v1}, LX/D3V;->a(ZLjava/lang/String;)V

    goto :goto_0

    .line 2502304
    :cond_2
    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->b:LX/D3V;

    invoke-virtual {v0, v3, p1}, LX/D3V;->a(ZLjava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;JJJJJIZZZLjava/util/Map;Z)V
    .locals 24
    .param p16    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2502305
    move-object/from16 v0, p0

    iget-object v2, v0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v2, v2, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->E:LX/Hni;

    invoke-virtual {v2}, LX/Hni;->b()Ljava/lang/String;

    move-result-object v4

    .line 2502306
    move-object/from16 v0, p0

    iget-object v2, v0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v3, v2, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->j:LX/2nC;

    move/from16 v0, p12

    int-to-long v0, v0

    move-wide/from16 v16, v0

    move-object/from16 v5, p1

    move-wide/from16 v6, p2

    move-wide/from16 v8, p4

    move-wide/from16 v10, p6

    move-wide/from16 v12, p8

    move-wide/from16 v14, p10

    move/from16 v18, p13

    move/from16 v19, p14

    move/from16 v20, p15

    move-object/from16 v21, p16

    move/from16 v22, p17

    invoke-virtual/range {v3 .. v22}, LX/2nC;->a(Ljava/lang/String;Ljava/lang/String;JJJJJJZZZLjava/util/Map;Z)V

    .line 2502307
    move-object/from16 v0, p0

    iget-object v2, v0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v3, v2, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->E:LX/Hni;

    const-wide/16 v6, -0x1

    cmp-long v2, p6, v6

    if-nez v2, :cond_0

    const-wide/16 v6, -0x1

    cmp-long v2, p10, v6

    if-eqz v2, :cond_1

    :cond_0
    const/4 v2, 0x1

    :goto_0
    invoke-virtual {v3, v4, v2}, LX/Hni;->a(Ljava/lang/String;Z)V

    .line 2502308
    move-object/from16 v0, p0

    iget-object v2, v0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v3, v2, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->E:LX/Hni;

    const-wide/16 v6, -0x1

    cmp-long v2, p8, v6

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :goto_1
    invoke-virtual {v3, v4, v2}, LX/Hni;->b(Ljava/lang/String;Z)V

    .line 2502309
    return-void

    .line 2502310
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 2502311
    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 8
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 2502312
    if-nez p2, :cond_0

    .line 2502313
    iput-object v2, p0, LX/HnR;->b:LX/6Cz;

    .line 2502314
    :goto_0
    return-void

    .line 2502315
    :cond_0
    const-string v0, "session_id"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2502316
    iget-object v1, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v1, v1, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->A:LX/6C4;

    .line 2502317
    iput-object v0, v1, LX/6C4;->b:Ljava/lang/String;

    .line 2502318
    iget-object v1, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v1, v1, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->A:LX/6C4;

    .line 2502319
    iget-object v3, v1, LX/6C4;->b:Ljava/lang/String;

    if-nez v3, :cond_5

    .line 2502320
    :goto_1
    iget-object v1, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v1, v1, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->E:LX/Hni;

    .line 2502321
    iget-object v3, v1, LX/Hni;->b:Ljava/util/Map;

    new-instance v4, LX/Hnh;

    invoke-direct {v4}, LX/Hnh;-><init>()V

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2502322
    invoke-static {p2}, LX/6D3;->a(Landroid/os/Bundle;)LX/6Cx;

    move-result-object v0

    sget-object v1, LX/6Cx;->NONE:LX/6Cx;

    if-eq v0, v1, :cond_4

    .line 2502323
    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->B:LX/6D0;

    invoke-virtual {v0, p2}, LX/6D0;->a(Landroid/os/Bundle;)LX/6Cz;

    move-result-object v0

    iput-object v0, p0, LX/HnR;->b:LX/6Cz;

    .line 2502324
    iget-object v0, p0, LX/HnR;->b:LX/6Cz;

    .line 2502325
    iget-object v1, v0, LX/6Cz;->a:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6CA;

    .line 2502326
    iget-object v3, v0, LX/6Cz;->i:Landroid/os/Bundle;

    invoke-interface {v1, v3}, LX/6C9;->a(Landroid/os/Bundle;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2502327
    iget-object v3, v0, LX/6Cz;->i:Landroid/os/Bundle;

    invoke-interface {v1, p1, v3}, LX/6CA;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_2

    .line 2502328
    :cond_2
    iget-object v1, v0, LX/6Cz;->c:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6Co;

    .line 2502329
    iget-object v3, v0, LX/6Cz;->i:Landroid/os/Bundle;

    invoke-interface {v1, v3}, LX/6Co;->a(Landroid/os/Bundle;)V

    goto :goto_3

    .line 2502330
    :cond_3
    goto :goto_0

    .line 2502331
    :cond_4
    iput-object v2, p0, LX/HnR;->b:LX/6Cz;

    goto :goto_0

    .line 2502332
    :cond_5
    iget-object v3, v1, LX/6C4;->d:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v3

    .line 2502333
    iget-object v5, v1, LX/6C4;->c:Ljava/util/Map;

    iget-object v6, v1, LX/6C4;->b:Ljava/lang/String;

    new-instance v7, LX/6C3;

    invoke-direct {v7, v3, v4}, LX/6C3;-><init>(J)V

    invoke-interface {v5, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2502334
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v4, "browser_article_opened"

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2502335
    const-string v4, "article_chaining_id"

    iget-object v5, v1, LX/6C4;->b:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2502336
    const-string v4, "user_url"

    invoke-virtual {v3, v4, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2502337
    const-string v4, "time_delta"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2502338
    iget-object v4, v1, LX/6C4;->a:LX/0Zb;

    invoke-interface {v4, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto/16 :goto_1
.end method

.method public final a(Ljava/lang/String;Landroid/os/Bundle;I)V
    .locals 4
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x1

    .line 2502106
    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->s:LX/0Uh;

    const/16 v1, 0xc8

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    if-le p3, v3, :cond_1

    .line 2502107
    invoke-direct {p0}, LX/HnR;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2502108
    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->b:LX/D3V;

    const-string v1, ""

    invoke-virtual {v0, v3, v1}, LX/D3V;->a(ZLjava/lang/String;)V

    .line 2502109
    :goto_0
    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->t:LX/15W;

    iget-object v1, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    new-instance v2, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v3, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->BUILT_IN_BROWSER:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v2, v3}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    .line 2502110
    const-class v3, LX/0i1;

    const/4 p1, 0x0

    invoke-virtual {v0, v1, v2, v3, p1}, LX/15W;->a(Landroid/content/Context;Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;Ljava/lang/Object;)Z

    .line 2502111
    if-nez p2, :cond_2

    .line 2502112
    :goto_1
    return-void

    .line 2502113
    :cond_0
    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->b:LX/D3V;

    const/4 v1, 0x0

    invoke-virtual {v0, v3, v1}, LX/D3V;->a(ZLjava/lang/String;)V

    goto :goto_0

    .line 2502114
    :cond_1
    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->b:LX/D3V;

    invoke-virtual {v0, v3, p1}, LX/D3V;->a(ZLjava/lang/String;)V

    goto :goto_0

    .line 2502115
    :cond_2
    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->A:LX/6C4;

    const-string v1, "session_id"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2502116
    iput-object v1, v0, LX/6C4;->b:Ljava/lang/String;

    .line 2502117
    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->A:LX/6C4;

    const/4 p0, 0x1

    .line 2502118
    invoke-static {v0}, LX/6C4;->g(LX/6C4;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 2502119
    :cond_3
    :goto_2
    goto :goto_1

    .line 2502120
    :cond_4
    iget-object v1, v0, LX/6C4;->c:Ljava/util/Map;

    iget-object v2, v0, LX/6C4;->b:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6C3;

    .line 2502121
    iget-boolean v2, v1, LX/6C3;->b:Z

    if-nez v2, :cond_3

    iget-boolean v2, v1, LX/6C3;->c:Z

    if-nez v2, :cond_3

    .line 2502122
    iput-boolean p0, v1, LX/6C3;->e:Z

    .line 2502123
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "browser_article_ready_to_interact"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2502124
    iget-object v3, v0, LX/6C4;->b:Ljava/lang/String;

    invoke-static {v0, v2, v3, v1}, LX/6C4;->a(LX/6C4;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/6C3;)V

    .line 2502125
    iget-object v3, v0, LX/6C4;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2502126
    iput-boolean p0, v1, LX/6C3;->b:Z

    goto :goto_2
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;III)V
    .locals 8

    .prologue
    .line 2502127
    packed-switch p3, :pswitch_data_0

    .line 2502128
    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->c:LX/0gh;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 2502129
    :goto_0
    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->A:LX/6C4;

    .line 2502130
    invoke-static {v0}, LX/6C4;->g(LX/6C4;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2502131
    :goto_1
    invoke-direct {p0}, LX/HnR;->c()V

    .line 2502132
    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->F:LX/2nD;

    iget-object v1, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v1, v1, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->E:LX/Hni;

    .line 2502133
    iget-object v2, v1, LX/Hni;->c:Ljava/lang/String;

    move-object v1, v2

    .line 2502134
    iget-object v2, v0, LX/2nD;->a:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2502135
    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->E:LX/Hni;

    .line 2502136
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "fb4a_iab_long_click"

    invoke-direct {v3, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2502137
    const-string v2, "clicks"

    invoke-virtual {v3, v2, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2502138
    const-string v2, "original_clicks"

    invoke-virtual {v3, v2, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2502139
    const-string v2, "is_iab"

    const/4 v4, 0x1

    invoke-virtual {v3, v2, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2502140
    iget-object v2, v0, LX/Hni;->b:Ljava/util/Map;

    iget-object v4, v0, LX/Hni;->c:Ljava/lang/String;

    invoke-interface {v2, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2502141
    const-string v4, "foreground_time"

    iget-object v2, v0, LX/Hni;->b:Ljava/util/Map;

    iget-object v5, v0, LX/Hni;->c:Ljava/lang/String;

    invoke-interface {v2, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Hnh;

    iget-wide v6, v2, LX/Hnh;->c:J

    invoke-virtual {v3, v4, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2502142
    :cond_0
    iget-object v2, v0, LX/Hni;->e:LX/0Zb;

    invoke-interface {v2, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2502143
    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->E:LX/Hni;

    .line 2502144
    iget-object v1, v0, LX/Hni;->c:Ljava/lang/String;

    .line 2502145
    iget-object v2, v0, LX/Hni;->b:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2502146
    iget-object v0, p0, LX/HnR;->b:LX/6Cz;

    if-eqz v0, :cond_2

    .line 2502147
    iget-object v0, p0, LX/HnR;->b:LX/6Cz;

    .line 2502148
    iget-object v1, v0, LX/6Cz;->i:Landroid/os/Bundle;

    const-string v2, "JS_BRIDGE_WEB_TITLE"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2502149
    iget-object v1, v0, LX/6Cz;->a:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6CA;

    .line 2502150
    iget-object v3, v0, LX/6Cz;->i:Landroid/os/Bundle;

    invoke-interface {v1, v3}, LX/6C9;->a(Landroid/os/Bundle;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2502151
    iget-object v3, v0, LX/6Cz;->i:Landroid/os/Bundle;

    invoke-interface {v1, p1, v3}, LX/6CA;->c(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_2

    .line 2502152
    :cond_2
    return-void

    .line 2502153
    :pswitch_0
    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->c:LX/0gh;

    const-string v1, "tap_back_button"

    invoke-virtual {v0, v1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    goto/16 :goto_0

    .line 2502154
    :pswitch_1
    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->c:LX/0gh;

    const-string v1, "tap_top_left_nav"

    invoke-virtual {v0, v1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    goto/16 :goto_0

    .line 2502155
    :cond_3
    iget-object v1, v0, LX/6C4;->c:Ljava/util/Map;

    iget-object v2, v0, LX/6C4;->b:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6C3;

    .line 2502156
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p3, "browser_article_closed"

    invoke-direct {v2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2502157
    iget-object p3, v0, LX/6C4;->b:Ljava/lang/String;

    invoke-static {v0, v2, p3, v1}, LX/6C4;->a(LX/6C4;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/6C3;)V

    .line 2502158
    iget-object v1, v0, LX/6C4;->a:LX/0Zb;

    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2502159
    iget-object v1, v0, LX/6C4;->c:Ljava/util/Map;

    iget-object v2, v0, LX/6C4;->b:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2502160
    const/4 v1, 0x0

    iput-object v1, v0, LX/6C4;->b:Ljava/lang/String;

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2501919
    if-nez p3, :cond_3

    const/4 v0, 0x0

    .line 2501920
    :goto_0
    invoke-direct {p0, p2, v0}, LX/HnR;->a(Ljava/lang/String;LX/6Cx;)V

    .line 2501921
    invoke-direct {p0}, LX/HnR;->e()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2501922
    :cond_0
    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->b:LX/D3V;

    .line 2501923
    const/4 p1, 0x1

    invoke-static {v0, p1}, LX/D3V;->b(LX/D3V;Z)V

    .line 2501924
    :cond_1
    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->A:LX/6C4;

    .line 2501925
    invoke-static {v0}, LX/6C4;->g(LX/6C4;)Z

    move-result p0

    if-nez p0, :cond_4

    .line 2501926
    :cond_2
    :goto_1
    return-void

    .line 2501927
    :cond_3
    invoke-static {p3}, LX/6D3;->a(Landroid/os/Bundle;)LX/6Cx;

    move-result-object v0

    goto :goto_0

    .line 2501928
    :cond_4
    iget-object p0, v0, LX/6C4;->c:Ljava/util/Map;

    iget-object p1, v0, LX/6C4;->b:Ljava/lang/String;

    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/6C3;

    .line 2501929
    iget-boolean p0, p0, LX/6C3;->e:Z

    if-eqz p0, :cond_2

    .line 2501930
    invoke-static {v0}, LX/6C4;->g(LX/6C4;)Z

    move-result p0

    if-nez p0, :cond_5

    .line 2501931
    :goto_2
    goto :goto_1

    .line 2501932
    :cond_5
    iget-object p0, v0, LX/6C4;->c:Ljava/util/Map;

    iget-object p1, v0, LX/6C4;->b:Ljava/lang/String;

    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/6C3;

    const/4 p1, 0x1

    iput-boolean p1, p0, LX/6C3;->c:Z

    goto :goto_2
.end method

.method public final a(Ljava/lang/String;Ljava/util/List;)V
    .locals 3

    .prologue
    .line 2501933
    invoke-interface {p2}, Ljava/util/List;->size()I

    .line 2501934
    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->D:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Be;

    .line 2501935
    new-instance v1, Lcom/facebook/browser/prefetch/BrowserPrefetcher$1;

    invoke-direct {v1, v0, p1, p2}, Lcom/facebook/browser/prefetch/BrowserPrefetcher$1;-><init>(LX/1Be;Ljava/lang/String;Ljava/util/List;)V

    .line 2501936
    iget-object v2, v0, LX/1Be;->j:Landroid/os/Handler;

    const p0, -0x4451b0ea

    invoke-static {v2, v1, p0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 2501937
    return-void
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 10

    .prologue
    .line 2501938
    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->w:LX/0Sh;

    new-instance v1, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService$BrowserLiteCallbackImpl$2;

    invoke-direct {v1, p0}, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService$BrowserLiteCallbackImpl$2;-><init>(LX/HnR;)V

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 2501939
    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->b:LX/D3V;

    const/4 v1, 0x0

    .line 2501940
    invoke-static {v0, v1}, LX/D3V;->a(LX/D3V;Z)V

    .line 2501941
    invoke-static {v0, v1}, LX/D3V;->c(LX/D3V;Z)V

    .line 2501942
    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->A:LX/6C4;

    .line 2501943
    invoke-static {v0}, LX/6C4;->g(LX/6C4;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2501944
    :cond_0
    :goto_0
    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->E:LX/Hni;

    .line 2501945
    iget-object v2, v0, LX/Hni;->b:Ljava/util/Map;

    iget-object v3, v0, LX/Hni;->c:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 2501946
    :goto_1
    iget-object v0, p0, LX/HnR;->b:LX/6Cz;

    if-eqz v0, :cond_1

    .line 2501947
    iget-object v0, p0, LX/HnR;->b:LX/6Cz;

    .line 2501948
    iget-object v1, v0, LX/6Cz;->a:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6CA;

    .line 2501949
    iget-object v3, v0, LX/6Cz;->i:Landroid/os/Bundle;

    invoke-interface {v1, v3}, LX/6C9;->a(Landroid/os/Bundle;)Z

    goto :goto_2

    .line 2501950
    :cond_1
    return-void

    .line 2501951
    :cond_2
    iget-object v1, v0, LX/6C4;->c:Ljava/util/Map;

    iget-object v2, v0, LX/6C4;->b:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6C3;

    .line 2501952
    iget-boolean v2, v1, LX/6C3;->d:Z

    if-nez v2, :cond_0

    .line 2501953
    const/4 v2, 0x1

    iput-boolean v2, v1, LX/6C3;->d:Z

    .line 2501954
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p2, "browser_article_backgrounded"

    invoke-direct {v2, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2501955
    iget-object p2, v0, LX/6C4;->b:Ljava/lang/String;

    invoke-static {v0, v2, p2, v1}, LX/6C4;->a(LX/6C4;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/6C3;)V

    .line 2501956
    iget-object v1, v0, LX/6C4;->a:LX/0Zb;

    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0

    .line 2501957
    :cond_3
    iget-object v2, v0, LX/Hni;->b:Ljava/util/Map;

    iget-object v3, v0, LX/Hni;->c:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Hnh;

    iget-wide v4, v2, LX/Hnh;->c:J

    iget-object v3, v0, LX/Hni;->a:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0SF;

    invoke-virtual {v3}, LX/0SF;->a()J

    move-result-wide v6

    iget-wide v8, v0, LX/Hni;->d:J

    sub-long/2addr v6, v8

    add-long/2addr v4, v6

    iput-wide v4, v2, LX/Hnh;->c:J

    goto :goto_1
.end method

.method public final a(Ljava/util/List;Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;)V
    .locals 5

    .prologue
    .line 2501958
    iget-object v0, p0, LX/HnR;->b:LX/6Cz;

    if-nez v0, :cond_0

    .line 2501959
    :goto_0
    return-void

    .line 2501960
    :cond_0
    iget-object v0, p0, LX/HnR;->b:LX/6Cz;

    .line 2501961
    iget-object v1, v0, LX/6Cz;->e:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6Db;

    .line 2501962
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;

    .line 2501963
    iget-object p0, v1, LX/6Db;->a:LX/6DO;

    invoke-virtual {p0, v3}, LX/6DO;->a(Lcom/facebook/browser/lite/ipc/browserextensions/autofill/BrowserExtensionsAutofillData;)V

    goto :goto_2

    .line 2501964
    :cond_1
    goto :goto_1

    .line 2501965
    :cond_2
    iget-object v1, v0, LX/6Cz;->f:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6CF;

    .line 2501966
    iget-object v3, p2, Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;->c:Landroid/os/Bundle;

    move-object v3, v3

    .line 2501967
    invoke-interface {v1, v3}, LX/6C9;->a(Landroid/os/Bundle;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2501968
    const-string v3, "browser_extensions_save_autofill_dialog_accepted"

    invoke-interface {v1, p2, v3}, LX/6CF;->a(Lcom/facebook/browser/lite/ipc/BrowserLiteJSBridgeCall;Ljava/lang/String;)V

    goto :goto_3

    .line 2501969
    :cond_4
    goto :goto_0
.end method

.method public final a(Ljava/util/Map;)V
    .locals 10

    .prologue
    .line 2501970
    const-string v0, "screenshot_uri"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 2501971
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2501972
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2501973
    const-string v0, "debug_info_map"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 2501974
    new-instance v7, LX/0P2;

    invoke-direct {v7}, LX/0P2;-><init>()V

    .line 2501975
    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v1, v2

    .line 2501976
    check-cast v1, Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v7, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_0

    .line 2501977
    :cond_0
    const-string v0, "raw_view_description_file_uri"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/Uri;

    .line 2501978
    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->k:LX/6G2;

    iget-object v1, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    sget-object v2, LX/6Fb;->DEFAULT:LX/6Fb;

    .line 2501979
    sget-object v3, LX/0Re;->a:LX/0Re;

    move-object v3, v3

    .line 2501980
    const-wide v8, 0xbf148a6a18adL

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v4}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v4

    invoke-virtual {v7}, LX/0P2;->b()LX/0P1;

    move-result-object v7

    invoke-virtual/range {v0 .. v7}, LX/6G2;->a(Landroid/content/Context;LX/6Fb;LX/0Rf;LX/0am;Ljava/util/List;Landroid/net/Uri;LX/0P1;)V

    .line 2501981
    return-void
.end method

.method public final a(Ljava/util/Map;Landroid/os/Bundle;)V
    .locals 7
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2501982
    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->r:LX/HnT;

    iget-object v1, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    invoke-virtual {v0, v1, p1}, LX/HnT;->a(Landroid/content/Context;Ljava/util/Map;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2501983
    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->l:LX/6C5;

    invoke-virtual {v0, p1}, LX/6C5;->a(Ljava/util/Map;)V

    .line 2501984
    :cond_0
    :goto_0
    return-void

    .line 2501985
    :cond_1
    iget-object v0, p0, LX/HnR;->b:LX/6Cz;

    if-eqz v0, :cond_0

    .line 2501986
    iget-object v0, p0, LX/HnR;->b:LX/6Cz;

    .line 2501987
    const-string v1, "action"

    invoke-interface {p1, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "url"

    invoke-interface {p1, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "url"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Ljava/lang/String;

    if-nez v1, :cond_3

    .line 2501988
    :cond_2
    :goto_1
    goto :goto_0

    .line 2501989
    :cond_3
    const-string v1, "action"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2501990
    iget-object v1, v0, LX/6Cz;->c:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6Co;

    .line 2501991
    invoke-interface {v1}, LX/6Co;->a()LX/6Eo;

    move-result-object v4

    invoke-virtual {v4}, LX/6Eo;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2501992
    const-string v2, "url"

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, LX/6Cz;->i:Landroid/os/Bundle;

    invoke-interface {v1, v2, v3}, LX/6Co;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_1

    .line 2501993
    :cond_5
    iget-object v1, v0, LX/6Cz;->h:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7i5;

    .line 2501994
    iget-object v3, v1, LX/7i5;->a:LX/0if;

    iget-object v4, v1, LX/7i5;->b:LX/0ih;

    const-string v5, "browser_extensions_on_user_action"

    const/4 v6, 0x0

    .line 2501995
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v0

    .line 2501996
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result p0

    if-eqz p0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/Map$Entry;

    .line 2501997
    invoke-interface {p0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    invoke-interface {p0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    invoke-virtual {v0, p2, p0}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    goto :goto_3

    .line 2501998
    :cond_6
    move-object p0, v0

    .line 2501999
    invoke-virtual {v3, v4, v5, v6, p0}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2502000
    goto :goto_2
.end method

.method public final a([J)V
    .locals 4

    .prologue
    .line 2502001
    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    .line 2502002
    iget-object v1, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v1, v1, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->e:LX/168;

    aget-wide v2, p1, v0

    invoke-virtual {v1, v2, v3}, LX/168;->a(J)V

    .line 2502003
    iget-object v1, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v1, v1, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->d:LX/1rD;

    aget-wide v2, p1, v0

    invoke-virtual {v1, v2, v3}, LX/1rD;->a(J)V

    .line 2502004
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2502005
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 2502008
    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->i:LX/BWU;

    iget-object v1, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    invoke-virtual {v0, v1, p1, p2}, LX/BWU;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2502009
    if-nez v0, :cond_0

    .line 2502010
    const/4 v0, 0x0

    .line 2502011
    :goto_0
    return v0

    .line 2502012
    :cond_0
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2502013
    iget-object v1, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v1, v1, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->h:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2502014
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 2502015
    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->p:LX/H6T;

    iget-object v1, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, p3, v2}, LX/H6T;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2502016
    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->C:LX/HnQ;

    invoke-virtual {v0}, LX/HnQ;->a()Z

    .line 2502017
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 2502018
    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->p:LX/H6T;

    .line 2502019
    const-string v1, "offer_view_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2502020
    iget-object v3, v0, LX/H6T;->l:LX/H60;

    iget-object v1, v0, LX/H6T;->r:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2502021
    new-instance v4, LX/4HR;

    invoke-direct {v4}, LX/4HR;-><init>()V

    .line 2502022
    const-string p0, "offer_view_id"

    invoke-virtual {v4, p0, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2502023
    const-string p0, "actor_id"

    invoke-virtual {v4, p0, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2502024
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object p0

    invoke-virtual {p0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p0

    .line 2502025
    const-string p1, "client_mutation_id"

    invoke-virtual {v4, p1, p0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2502026
    new-instance p0, LX/H6n;

    invoke-direct {p0}, LX/H6n;-><init>()V

    move-object p0, p0

    .line 2502027
    const-string p1, "input"

    invoke-virtual {p0, p1, v4}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2502028
    invoke-static {p0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v4

    .line 2502029
    iget-object p0, v3, LX/H60;->a:LX/0tX;

    invoke-virtual {p0, v4}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v1, v4

    .line 2502030
    new-instance v2, LX/H6S;

    invoke-direct {v2, v0}, LX/H6S;-><init>(LX/H6T;)V

    invoke-static {v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2502031
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 11

    .prologue
    .line 2502032
    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->m:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;

    sget-object v1, LX/7BH;->LIGHT:LX/7BH;

    sget-object v2, LX/103;->URL:LX/103;

    const-string v3, ""

    .line 2502033
    const/4 v4, 0x0

    .line 2502034
    const-string v6, ""

    move-object v5, v1

    move-object v7, v2

    move-object v8, v3

    move-object v9, p1

    move-object v10, v4

    invoke-static/range {v5 .. v10}, Lcom/facebook/search/api/GraphSearchQuery;->a(LX/7BH;Ljava/lang/String;LX/103;Ljava/lang/String;Ljava/lang/String;LX/7B5;)Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v5

    move-object v4, v5

    .line 2502035
    move-object v1, v4

    .line 2502036
    invoke-virtual {v0, v1}, Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;->b(Lcom/facebook/search/api/GraphSearchQuery;)V

    .line 2502037
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2502038
    const-string v1, "action"

    const-string v2, "SEARCH"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2502039
    const-string v1, "url"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2502040
    iget-object v1, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v1, v1, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->l:LX/6C5;

    invoke-virtual {v1, v0}, LX/6C5;->a(Ljava/util/Map;)V

    .line 2502041
    return-void
.end method

.method public final b(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 5
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2502042
    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->f:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService$BrowserLiteActivity;

    .line 2502043
    iput-object p1, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService$BrowserLiteActivity;->a:Ljava/lang/String;

    .line 2502044
    if-nez p2, :cond_1

    const/4 v0, 0x0

    .line 2502045
    :goto_0
    iget-object v1, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v1, v1, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->f:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService$BrowserLiteActivity;

    .line 2502046
    iput-object v0, v1, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService$BrowserLiteActivity;->b:LX/6Cx;

    .line 2502047
    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->w:LX/0Sh;

    new-instance v1, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService$BrowserLiteCallbackImpl$1;

    invoke-direct {v1, p0}, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService$BrowserLiteCallbackImpl$1;-><init>(LX/HnR;)V

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 2502048
    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->b:LX/D3V;

    .line 2502049
    const/4 v1, 0x1

    invoke-static {v0, v1}, LX/D3V;->a(LX/D3V;Z)V

    .line 2502050
    if-nez p2, :cond_2

    .line 2502051
    :cond_0
    :goto_1
    return-void

    .line 2502052
    :cond_1
    invoke-static {p2}, LX/6D3;->a(Landroid/os/Bundle;)LX/6Cx;

    move-result-object v0

    goto :goto_0

    .line 2502053
    :cond_2
    const-string v0, "session_id"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2502054
    iget-object v1, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v1, v1, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->A:LX/6C4;

    .line 2502055
    iput-object v0, v1, LX/6C4;->b:Ljava/lang/String;

    .line 2502056
    iget-object v1, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v1, v1, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->A:LX/6C4;

    .line 2502057
    invoke-static {v1}, LX/6C4;->g(LX/6C4;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 2502058
    :cond_3
    :goto_2
    iget-object v1, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v1, v1, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->E:LX/Hni;

    .line 2502059
    iput-object v0, v1, LX/Hni;->c:Ljava/lang/String;

    .line 2502060
    iget-object v2, v1, LX/Hni;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0SF;

    invoke-virtual {v2}, LX/0SF;->a()J

    move-result-wide v2

    iput-wide v2, v1, LX/Hni;->d:J

    .line 2502061
    iget-object v0, p0, LX/HnR;->b:LX/6Cz;

    if-nez v0, :cond_4

    invoke-static {p2}, LX/6D3;->a(Landroid/os/Bundle;)LX/6Cx;

    move-result-object v0

    sget-object v1, LX/6Cx;->NONE:LX/6Cx;

    if-eq v0, v1, :cond_4

    .line 2502062
    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->B:LX/6D0;

    invoke-virtual {v0, p2}, LX/6D0;->a(Landroid/os/Bundle;)LX/6Cz;

    move-result-object v0

    iput-object v0, p0, LX/HnR;->b:LX/6Cz;

    .line 2502063
    :cond_4
    iget-object v0, p0, LX/HnR;->b:LX/6Cz;

    if-eqz v0, :cond_0

    .line 2502064
    iget-object v0, p0, LX/HnR;->b:LX/6Cz;

    .line 2502065
    iget-object v1, v0, LX/6Cz;->a:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_5
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6CA;

    .line 2502066
    iget-object v3, v0, LX/6Cz;->i:Landroid/os/Bundle;

    invoke-interface {v1, v3}, LX/6C9;->a(Landroid/os/Bundle;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 2502067
    iget-object v3, v0, LX/6Cz;->i:Landroid/os/Bundle;

    invoke-interface {v1, p1, v3}, LX/6CA;->b(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_3

    .line 2502068
    :cond_6
    goto :goto_1

    .line 2502069
    :cond_7
    iget-object v2, v1, LX/6C4;->c:Ljava/util/Map;

    iget-object v3, v1, LX/6C4;->b:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6C3;

    .line 2502070
    iget-boolean v3, v2, LX/6C3;->d:Z

    if-eqz v3, :cond_3

    .line 2502071
    const/4 v3, 0x0

    iput-boolean v3, v2, LX/6C3;->d:Z

    .line 2502072
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v4, "browser_article_foregrounded"

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2502073
    iget-object v4, v1, LX/6C4;->b:Ljava/lang/String;

    invoke-static {v1, v3, v4, v2}, LX/6C4;->a(LX/6C4;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/6C3;)V

    .line 2502074
    iget-object v2, v1, LX/6C4;->a:LX/0Zb;

    invoke-interface {v2, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_2
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2502075
    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->u:LX/CK7;

    const-string v1, "browser"

    invoke-virtual {v0, v1, p1, p2}, LX/CK7;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2502076
    return-void
.end method

.method public final c(Ljava/lang/String;)Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2502077
    iget-object v0, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v0, v0, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->o:LX/1Bn;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, LX/1Bn;->a(Ljava/lang/String;Z)Lcom/facebook/browser/lite/ipc/PrefetchCacheEntry;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2502078
    invoke-direct {p0, p1, p2}, LX/HnR;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2502079
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2502080
    const-string v1, "action"

    const-string v2, "QUOTE_SHARE"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2502081
    iget-object v1, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    iget-object v1, v1, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->l:LX/6C5;

    invoke-virtual {v1, v0}, LX/6C5;->a(Ljava/util/Map;)V

    .line 2502082
    return-void
.end method

.method public final d(Ljava/lang/String;)Z
    .locals 7

    .prologue
    .line 2502083
    iget-object v0, p0, LX/HnR;->b:LX/6Cz;

    if-nez v0, :cond_0

    .line 2502084
    const/4 v0, 0x0

    .line 2502085
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/HnR;->b:LX/6Cz;

    iget-object v1, p0, LX/HnR;->a:Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;

    invoke-virtual {v1}, Lcom/facebook/browser/liteclient/BrowserLiteCallbackService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 2502086
    iget-object v2, v0, LX/6Cz;->d:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6Er;

    .line 2502087
    iget-object v4, v0, LX/6Cz;->i:Landroid/os/Bundle;

    const/4 v5, 0x0

    .line 2502088
    const-string v6, "JS_BRIDGE_EXTENSION_TYPE"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2502089
    sget-object p0, LX/6Cx;->INSTANT_EXPERIENCE:LX/6Cx;

    iget-object p0, p0, LX/6Cx;->value:Ljava/lang/String;

    invoke-virtual {p0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 2502090
    :cond_2
    :goto_1
    move v2, v5

    .line 2502091
    if-eqz v2, :cond_1

    .line 2502092
    const/4 v2, 0x1

    .line 2502093
    :goto_2
    move v0, v2

    .line 2502094
    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    goto :goto_2

    .line 2502095
    :cond_4
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 2502096
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 2502097
    if-eqz v6, :cond_5

    .line 2502098
    invoke-static {v6}, LX/2yp;->d(Landroid/net/Uri;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 2502099
    :cond_5
    const-string v6, "JS_BRIDGE_WHITELISTED_DOMAINS"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    .line 2502100
    iget-object p0, v2, LX/6Er;->b:LX/6D1;

    invoke-virtual {p0, v4, p1, v6}, LX/6D1;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/util/List;)Z

    move-result v6

    if-nez v6, :cond_2

    iget-object v6, v2, LX/6Er;->d:LX/0Uh;

    const/16 p0, 0x50d

    invoke-virtual {v6, p0, v5}, LX/0Uh;->a(IZ)Z

    move-result v6

    if-nez v6, :cond_2

    .line 2502101
    new-instance v5, Landroid/content/Intent;

    const-string v6, "android.intent.action.VIEW"

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    invoke-direct {v5, v6, p0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 2502102
    const-string v6, "iab_click_source"

    const-string p0, "fbbrowser_instant_experience"

    invoke-virtual {v5, v6, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2502103
    const/high16 v6, 0x10000000

    invoke-virtual {v5, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2502104
    iget-object v6, v2, LX/6Er;->c:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v6, v5, v1}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2502105
    const/4 v5, 0x1

    goto :goto_1
.end method
