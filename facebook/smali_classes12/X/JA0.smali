.class public LX/JA0;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsFirstFiveGroupMembersFieldsModel$GroupMembersModel;

.field public final e:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

.field public final f:Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;

.field public final g:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "address"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation
.end field

.field public final h:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "address"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation
.end field

.field public final i:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field private j:LX/J9z;

.field private k:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

.field private l:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

.field public m:Z

.field public final n:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "LX/1oP;",
            ">;"
        }
    .end annotation
.end field

.field private o:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "LX/1oP;",
            ">;"
        }
    .end annotation
.end field

.field private p:LX/JAY;

.field private q:LX/JAZ;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;)V
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "<init>"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2653771
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->m()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;

    move-result-object v1

    invoke-direct {p0, v0, v1, p1}, LX/JA0;-><init>(Ljava/lang/String;Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;LX/JAY;)V

    .line 2653772
    return-void
.end method

.method public constructor <init>(Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionSuggestionFieldsModel;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 2653751
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2653752
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2653753
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionSuggestionFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/JA0;->a:Ljava/lang/String;

    .line 2653754
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionSuggestionFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/JA0;->b:Ljava/lang/String;

    .line 2653755
    iput-object v1, p0, LX/JA0;->c:Ljava/lang/String;

    .line 2653756
    iput-object v1, p0, LX/JA0;->d:Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsFirstFiveGroupMembersFieldsModel$GroupMembersModel;

    .line 2653757
    iput-object v1, p0, LX/JA0;->j:LX/J9z;

    .line 2653758
    iput-object v1, p0, LX/JA0;->k:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2653759
    iput-object v1, p0, LX/JA0;->l:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 2653760
    iput-boolean v2, p0, LX/JA0;->m:Z

    .line 2653761
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionSuggestionFieldsModel;->m()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/JA0;->n:LX/0Px;

    .line 2653762
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionSuggestionFieldsModel;->l()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/JA0;->o:LX/0Px;

    .line 2653763
    iput-object v1, p0, LX/JA0;->p:LX/JAY;

    .line 2653764
    invoke-static {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionSuggestionFieldsModel;->a(Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionSuggestionFieldsModel;)Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionSuggestionFieldsModel;

    move-result-object v0

    iput-object v0, p0, LX/JA0;->q:LX/JAZ;

    .line 2653765
    iput-object v1, p0, LX/JA0;->e:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    .line 2653766
    iput-object v1, p0, LX/JA0;->f:Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;

    .line 2653767
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, LX/JA0;->g:LX/15i;

    const/4 v0, 0x0

    iput v0, p0, LX/JA0;->h:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2653768
    invoke-virtual {p1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionSuggestionFieldsModel;->d()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    iput-object v0, p0, LX/JA0;->i:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2653769
    return-void

    .line 2653770
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private constructor <init>(Ljava/lang/String;Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;LX/JAY;)V
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "<init>"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2653731
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2653732
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2653733
    iput-object p1, p0, LX/JA0;->a:Ljava/lang/String;

    .line 2653734
    invoke-virtual {p2}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;->mS_()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/JA0;->b:Ljava/lang/String;

    .line 2653735
    invoke-virtual {p2}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/JA0;->c:Ljava/lang/String;

    .line 2653736
    invoke-virtual {p2}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;->e()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsFirstFiveGroupMembersFieldsModel$GroupMembersModel;

    move-result-object v0

    iput-object v0, p0, LX/JA0;->d:Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionsFirstFiveGroupMembersFieldsModel$GroupMembersModel;

    .line 2653737
    invoke-virtual {p2}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;->n()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/J9z;->fromString(Ljava/lang/String;)LX/J9z;

    move-result-object v0

    iput-object v0, p0, LX/JA0;->j:LX/J9z;

    .line 2653738
    invoke-virtual {p2}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;->d()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    iput-object v0, p0, LX/JA0;->k:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2653739
    invoke-virtual {p2}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;->m()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v0

    iput-object v0, p0, LX/JA0;->l:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 2653740
    invoke-virtual {p2}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;->mT_()Z

    move-result v0

    iput-boolean v0, p0, LX/JA0;->m:Z

    .line 2653741
    invoke-virtual {p2}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;->p()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/JA0;->n:LX/0Px;

    .line 2653742
    invoke-virtual {p2}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;->o()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/JA0;->o:LX/0Px;

    .line 2653743
    if-nez p3, :cond_0

    new-instance v0, LX/JAe;

    invoke-direct {v0}, LX/JAe;-><init>()V

    invoke-virtual {v0}, LX/JAe;->a()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemContainingMutationFieldsModel;

    move-result-object p3

    :cond_0
    iput-object p3, p0, LX/JA0;->p:LX/JAY;

    .line 2653744
    const/4 v0, 0x0

    iput-object v0, p0, LX/JA0;->q:LX/JAZ;

    .line 2653745
    invoke-virtual {p2}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;->l()Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    move-result-object v0

    iput-object v0, p0, LX/JA0;->e:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    .line 2653746
    invoke-virtual {p2}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;->j()Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;

    move-result-object v0

    iput-object v0, p0, LX/JA0;->f:Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;

    .line 2653747
    invoke-virtual {p2}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;->q()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iput-object v1, p0, LX/JA0;->g:LX/15i;

    iput v0, p0, LX/JA0;->h:I

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2653748
    invoke-virtual {p2}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    iput-object v0, p0, LX/JA0;->i:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2653749
    return-void

    .line 2653750
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2653713
    iget-object v0, p0, LX/JA0;->p:LX/JAY;

    if-eqz v0, :cond_0

    .line 2653714
    iget-object v0, p0, LX/JA0;->p:LX/JAY;

    invoke-static {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemContainingMutationFieldsModel;->a(LX/JAY;)Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemContainingMutationFieldsModel;

    move-result-object v0

    .line 2653715
    new-instance v1, LX/JAe;

    invoke-direct {v1}, LX/JAe;-><init>()V

    .line 2653716
    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemContainingMutationFieldsModel;->d()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/JAe;->a:Ljava/lang/String;

    .line 2653717
    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemContainingMutationFieldsModel;->mO_()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/JAe;->b:Ljava/lang/String;

    .line 2653718
    move-object v0, v1

    .line 2653719
    iput-object p1, v0, LX/JAe;->b:Ljava/lang/String;

    .line 2653720
    move-object v0, v0

    .line 2653721
    invoke-virtual {v0}, LX/JAe;->a()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemContainingMutationFieldsModel;

    move-result-object v0

    iput-object v0, p0, LX/JA0;->p:LX/JAY;

    .line 2653722
    :goto_0
    return-void

    .line 2653723
    :cond_0
    iget-object v0, p0, LX/JA0;->q:LX/JAZ;

    invoke-static {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionSuggestionContainingMutationFieldsModel;->a(LX/JAZ;)Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionSuggestionContainingMutationFieldsModel;

    move-result-object v0

    .line 2653724
    new-instance v1, LX/JAm;

    invoke-direct {v1}, LX/JAm;-><init>()V

    .line 2653725
    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionSuggestionContainingMutationFieldsModel;->b()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/JAm;->a:Ljava/lang/String;

    .line 2653726
    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionSuggestionContainingMutationFieldsModel;->c()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/JAm;->b:Ljava/lang/String;

    .line 2653727
    move-object v0, v1

    .line 2653728
    iput-object p1, v0, LX/JAm;->b:Ljava/lang/String;

    .line 2653729
    move-object v0, v0

    .line 2653730
    invoke-virtual {v0}, LX/JAm;->a()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionSuggestionContainingMutationFieldsModel;

    move-result-object v0

    iput-object v0, p0, LX/JA0;->q:LX/JAZ;

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1oP;)V
    .locals 5
    .param p1    # LX/1oP;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 2653704
    if-nez p1, :cond_0

    .line 2653705
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/JA0;->a(Ljava/lang/String;)V

    .line 2653706
    :goto_0
    return-void

    .line 2653707
    :cond_0
    iget-object v0, p0, LX/JA0;->n:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_2

    iget-object v0, p0, LX/JA0;->n:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1oP;

    .line 2653708
    invoke-interface {p1}, LX/1oP;->b()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0}, LX/1oP;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2653709
    const/4 v0, 0x1

    .line 2653710
    :goto_2
    const-string v1, "Trying to set a non-supported collection!"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2653711
    invoke-interface {p1}, LX/1oP;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/JA0;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 2653712
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public final a(LX/J9z;)V
    .locals 1

    .prologue
    .line 2653700
    if-nez p1, :cond_0

    .line 2653701
    sget-object v0, LX/J9z;->UNKNOWN:LX/J9z;

    iput-object v0, p0, LX/JA0;->j:LX/J9z;

    .line 2653702
    :goto_0
    return-void

    .line 2653703
    :cond_0
    iput-object p1, p0, LX/JA0;->j:LX/J9z;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 1

    .prologue
    .line 2653773
    if-nez p1, :cond_0

    .line 2653774
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, LX/JA0;->k:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2653775
    :goto_0
    return-void

    .line 2653776
    :cond_0
    iput-object p1, p0, LX/JA0;->k:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V
    .locals 1

    .prologue
    .line 2653696
    if-nez p1, :cond_0

    .line 2653697
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iput-object v0, p0, LX/JA0;->l:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 2653698
    :goto_0
    return-void

    .line 2653699
    :cond_0
    iput-object p1, p0, LX/JA0;->l:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    goto :goto_0
.end method

.method public final b()LX/1oP;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2653685
    iget-object v0, p0, LX/JA0;->p:LX/JAY;

    if-eqz v0, :cond_0

    .line 2653686
    iget-object v0, p0, LX/JA0;->p:LX/JAY;

    invoke-interface {v0}, LX/JAY;->mO_()Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 2653687
    :goto_0
    if-eqz v3, :cond_2

    .line 2653688
    iget-object v0, p0, LX/JA0;->n:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move v1, v2

    :goto_1
    if-ge v1, v4, :cond_2

    iget-object v0, p0, LX/JA0;->n:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1oP;

    .line 2653689
    invoke-interface {v0}, LX/1oP;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2653690
    :goto_2
    return-object v0

    .line 2653691
    :cond_0
    iget-object v0, p0, LX/JA0;->q:LX/JAZ;

    invoke-interface {v0}, LX/JAZ;->c()Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    goto :goto_0

    .line 2653692
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2653693
    :cond_2
    iget-object v0, p0, LX/JA0;->o:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2653694
    iget-object v0, p0, LX/JA0;->o:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1oP;

    goto :goto_2

    .line 2653695
    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final c()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2653680
    iget-object v2, p0, LX/JA0;->p:LX/JAY;

    if-eqz v2, :cond_2

    .line 2653681
    iget-object v2, p0, LX/JA0;->p:LX/JAY;

    invoke-interface {v2}, LX/JAY;->mO_()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2653682
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 2653683
    goto :goto_0

    .line 2653684
    :cond_2
    iget-object v2, p0, LX/JA0;->q:LX/JAZ;

    invoke-interface {v2}, LX/JAZ;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .locals 1

    .prologue
    .line 2653679
    iget-object v0, p0, LX/JA0;->k:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/JA0;->k:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    goto :goto_0
.end method

.method public final g()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .locals 1

    .prologue
    .line 2653678
    iget-object v0, p0, LX/JA0;->l:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/JA0;->l:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    goto :goto_0
.end method

.method public final i()LX/J9z;
    .locals 1

    .prologue
    .line 2653677
    iget-object v0, p0, LX/JA0;->j:LX/J9z;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/JA0;->j:LX/J9z;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/J9z;->UNKNOWN:LX/J9z;

    goto :goto_0
.end method
