.class public final LX/JOZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/feed/protocol/HPPMutationModels$HPPAYMTLogEventMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

.field public final synthetic c:Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;)V
    .locals 0

    .prologue
    .line 2687840
    iput-object p1, p0, LX/JOZ;->c:Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;

    iput-object p2, p0, LX/JOZ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p3, p0, LX/JOZ;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 2687841
    iget-object v0, p0, LX/JOZ;->c:Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;

    iget-object v0, v0, Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JPN;

    iget-object v1, p0, LX/JOZ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p0, LX/JOZ;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "HPPAYMTLogEventMutation failed: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/JPN;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;Ljava/lang/String;)V

    .line 2687842
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2687843
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2687844
    if-eqz p1, :cond_0

    .line 2687845
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2687846
    if-nez v0, :cond_1

    .line 2687847
    :cond_0
    iget-object v0, p0, LX/JOZ;->c:Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;

    iget-object v0, v0, Lcom/facebook/feedplugins/hpp/AYMTCardComponentSpec;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JPN;

    iget-object v1, p0, LX/JOZ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p0, LX/JOZ;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    const-string v3, "HPPAYMTLogEventMutation success but result is null"

    invoke-virtual {v0, v1, v2, v3}, LX/JPN;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;Ljava/lang/String;)V

    .line 2687848
    :cond_1
    return-void
.end method
