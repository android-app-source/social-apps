.class public final LX/HUX;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 2473153
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 2473154
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2473155
    :goto_0
    return v1

    .line 2473156
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2473157
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_4

    .line 2473158
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2473159
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2473160
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 2473161
    const-string v4, "edges"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2473162
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2473163
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_ARRAY:LX/15z;

    if-ne v3, v4, :cond_2

    .line 2473164
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_ARRAY:LX/15z;

    if-eq v3, v4, :cond_2

    .line 2473165
    invoke-static {p0, p1}, LX/HUW;->b(LX/15w;LX/186;)I

    move-result v3

    .line 2473166
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2473167
    :cond_2
    invoke-static {v2, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v2

    move v2, v2

    .line 2473168
    goto :goto_1

    .line 2473169
    :cond_3
    const-string v4, "page_info"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2473170
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2473171
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v5, :cond_e

    .line 2473172
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2473173
    :goto_3
    move v0, v3

    .line 2473174
    goto :goto_1

    .line 2473175
    :cond_4
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2473176
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2473177
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2473178
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 2473179
    :cond_6
    const-string v11, "has_next_page"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 2473180
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v5

    move v8, v5

    move v5, v4

    .line 2473181
    :cond_7
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_b

    .line 2473182
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 2473183
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2473184
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_7

    if-eqz v10, :cond_7

    .line 2473185
    const-string v11, "end_cursor"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 2473186
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_4

    .line 2473187
    :cond_8
    const-string v11, "has_previous_page"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 2473188
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v7, v0

    move v0, v4

    goto :goto_4

    .line 2473189
    :cond_9
    const-string v11, "start_cursor"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_a

    .line 2473190
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_4

    .line 2473191
    :cond_a
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_4

    .line 2473192
    :cond_b
    const/4 v10, 0x4

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 2473193
    invoke-virtual {p1, v3, v9}, LX/186;->b(II)V

    .line 2473194
    if-eqz v5, :cond_c

    .line 2473195
    invoke-virtual {p1, v4, v8}, LX/186;->a(IZ)V

    .line 2473196
    :cond_c
    if-eqz v0, :cond_d

    .line 2473197
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v7}, LX/186;->a(IZ)V

    .line 2473198
    :cond_d
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2473199
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_3

    :cond_e
    move v0, v3

    move v5, v3

    move v6, v3

    move v7, v3

    move v8, v3

    move v9, v3

    goto :goto_4
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2473200
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2473201
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2473202
    if-eqz v0, :cond_1

    .line 2473203
    const-string v1, "edges"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2473204
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2473205
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 2473206
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/HUW;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2473207
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2473208
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2473209
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2473210
    if-eqz v0, :cond_6

    .line 2473211
    const-string v1, "page_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2473212
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2473213
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2473214
    if-eqz v1, :cond_2

    .line 2473215
    const-string v2, "end_cursor"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2473216
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2473217
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->b(II)Z

    move-result v1

    .line 2473218
    if-eqz v1, :cond_3

    .line 2473219
    const-string v2, "has_next_page"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2473220
    invoke-virtual {p2, v1}, LX/0nX;->a(Z)V

    .line 2473221
    :cond_3
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, LX/15i;->b(II)Z

    move-result v1

    .line 2473222
    if-eqz v1, :cond_4

    .line 2473223
    const-string v2, "has_previous_page"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2473224
    invoke-virtual {p2, v1}, LX/0nX;->a(Z)V

    .line 2473225
    :cond_4
    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2473226
    if-eqz v1, :cond_5

    .line 2473227
    const-string v2, "start_cursor"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2473228
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2473229
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2473230
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2473231
    return-void
.end method
