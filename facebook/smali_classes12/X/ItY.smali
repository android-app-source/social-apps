.class public final enum LX/ItY;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ItY;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ItY;

.field public static final enum THRIFT:LX/ItY;

.field public static final enum THRIFT_BATCH:LX/ItY;

.field private static TOPIC_PREFIX:Ljava/lang/String;


# instance fields
.field private requestTopicType:I

.field private responseTopicType:I


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2623474
    new-instance v0, LX/ItY;

    const-string v1, "THRIFT"

    const/16 v2, 0x45

    const/16 v3, 0x46

    invoke-direct {v0, v1, v4, v2, v3}, LX/ItY;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/ItY;->THRIFT:LX/ItY;

    .line 2623475
    new-instance v0, LX/ItY;

    const-string v1, "THRIFT_BATCH"

    const/16 v2, 0x5f

    const/16 v3, 0x60

    invoke-direct {v0, v1, v5, v2, v3}, LX/ItY;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/ItY;->THRIFT_BATCH:LX/ItY;

    .line 2623476
    const/4 v0, 0x2

    new-array v0, v0, [LX/ItY;

    sget-object v1, LX/ItY;->THRIFT:LX/ItY;

    aput-object v1, v0, v4

    sget-object v1, LX/ItY;->THRIFT_BATCH:LX/ItY;

    aput-object v1, v0, v5

    sput-object v0, LX/ItY;->$VALUES:[LX/ItY;

    .line 2623477
    const-string v0, "/"

    sput-object v0, LX/ItY;->TOPIC_PREFIX:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 2623478
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2623479
    iput p3, p0, LX/ItY;->requestTopicType:I

    .line 2623480
    iput p4, p0, LX/ItY;->responseTopicType:I

    .line 2623481
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/ItY;
    .locals 1

    .prologue
    .line 2623482
    const-class v0, LX/ItY;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ItY;

    return-object v0
.end method

.method public static values()[LX/ItY;
    .locals 1

    .prologue
    .line 2623483
    sget-object v0, LX/ItY;->$VALUES:[LX/ItY;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ItY;

    return-object v0
.end method


# virtual methods
.method public final getRequestTopic()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2623484
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LX/ItY;->TOPIC_PREFIX:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v0, LX/2Ua;->b:Ljava/util/Map;

    iget v2, p0, LX/ItY;->requestTopicType:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getResponseTopic()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2623485
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LX/ItY;->TOPIC_PREFIX:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v0, LX/2Ua;->b:Ljava/util/Map;

    iget v2, p0, LX/ItY;->responseTopicType:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
