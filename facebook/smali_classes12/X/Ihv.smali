.class public final LX/Ihv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;)V
    .locals 0

    .prologue
    .line 2603086
    iput-object p1, p0, LX/Ihv;->a:Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v7, 0x2

    const/4 v0, 0x1

    const v1, 0x191ec2c1

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2603087
    iget-object v0, p0, LX/Ihv;->a:Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;->q:LX/G7C;

    iget-object v1, p0, LX/Ihv;->a:Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;

    iget-object v1, v1, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;->t:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    sget-object v2, LX/0yY;->VIDEO_UPLOAD_INTERSTITIAL:LX/0yY;

    iget-object v3, p0, LX/Ihv;->a:Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v3

    iget-object v4, p0, LX/Ihv;->a:Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;

    iget-object v4, v4, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;->w:LX/39A;

    const/4 v5, 0x0

    .line 2603088
    invoke-static {v0, v1, v2}, LX/G7C;->a(LX/G7C;Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/0yY;)LX/G7B;

    move-result-object v8

    .line 2603089
    sget-object v9, LX/G7B;->DIRECT_CALL:LX/G7B;

    if-ne v8, v9, :cond_1

    .line 2603090
    if-eqz v4, :cond_0

    .line 2603091
    invoke-interface {v4, v5}, LX/39A;->a(Ljava/lang/Object;)V

    .line 2603092
    :cond_0
    :goto_0
    const v0, -0x3fb316d1

    invoke-static {v7, v7, v0, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2603093
    :cond_1
    sget-object v9, LX/G7B;->STANDARD_ZERO_DIALOG:LX/G7B;

    if-ne v8, v9, :cond_2

    .line 2603094
    invoke-static {v2}, LX/G7C;->a(LX/0yY;)I

    move-result v9

    .line 2603095
    invoke-static {v2}, LX/G7C;->b(LX/0yY;)LX/0yY;

    move-result-object p0

    .line 2603096
    iget-object v8, v0, LX/G7C;->d:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/121;

    iget-object p1, v0, LX/G7C;->a:Landroid/content/Context;

    invoke-virtual {p1, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, p0, v9, v4}, LX/121;->a(LX/0yY;Ljava/lang/String;LX/39A;)LX/121;

    .line 2603097
    iget-object v8, v0, LX/G7C;->d:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/121;

    invoke-virtual {v8, p0, v3, v5}, LX/121;->a(LX/0yY;LX/0gc;Ljava/lang/Object;)Landroid/support/v4/app/DialogFragment;

    goto :goto_0

    .line 2603098
    :cond_2
    sget-object v9, LX/0yY;->SMS_THREAD_INTERSTITIAL:LX/0yY;

    .line 2603099
    iget-object v8, v0, LX/G7C;->e:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/G7G;

    iget-object p0, v0, LX/G7C;->a:Landroid/content/Context;

    .line 2603100
    const p1, 0x7f080e51

    move p1, p1

    .line 2603101
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v8, v9, p0, v4}, LX/121;->a(LX/0yY;Ljava/lang/String;LX/39A;)LX/121;

    .line 2603102
    iget-object v8, v0, LX/G7C;->e:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/G7G;

    invoke-virtual {v8, v9, v3, v5}, LX/121;->a(LX/0yY;LX/0gc;Ljava/lang/Object;)Landroid/support/v4/app/DialogFragment;

    goto :goto_0
.end method
