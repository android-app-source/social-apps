.class public final LX/JRd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3J0;


# instance fields
.field public final synthetic a:LX/3hx;

.field public final synthetic b:LX/3i8;

.field public final synthetic c:Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;LX/3hx;LX/3i8;)V
    .locals 0

    .prologue
    .line 2693544
    iput-object p1, p0, LX/JRd;->c:Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;

    iput-object p2, p0, LX/JRd;->a:LX/3hx;

    iput-object p3, p0, LX/JRd;->b:LX/3i8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/7Jv;)V
    .locals 3

    .prologue
    .line 2693545
    iget-object v0, p0, LX/JRd;->a:LX/3hx;

    iget-object v0, v0, LX/3hx;->l:LX/2oO;

    iget-boolean v1, p1, LX/7Jv;->b:Z

    iget-boolean v2, p1, LX/7Jv;->a:Z

    invoke-virtual {v0, v1, v2}, LX/2oO;->a(ZZ)V

    .line 2693546
    iget-object v0, p0, LX/JRd;->a:LX/3hx;

    .line 2693547
    if-eqz p1, :cond_2

    iget-boolean v1, p1, LX/7Jv;->a:Z

    if-nez v1, :cond_2

    iget-boolean v1, p1, LX/7Jv;->b:Z

    if-nez v1, :cond_2

    iget v1, p1, LX/7Jv;->c:I

    if-ltz v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 2693548
    iput-boolean v1, v0, LX/3hx;->i:Z

    .line 2693549
    iget-object v0, p0, LX/JRd;->a:LX/3hx;

    iget-object v0, v0, LX/3hx;->j:LX/2oL;

    iget v1, p1, LX/7Jv;->c:I

    invoke-virtual {v0, v1}, LX/2oL;->a(I)V

    .line 2693550
    iget-object v0, p0, LX/JRd;->a:LX/3hx;

    iget-object v0, v0, LX/3hx;->j:LX/2oL;

    iget-boolean v1, p1, LX/7Jv;->b:Z

    .line 2693551
    iput-boolean v1, v0, LX/2oL;->a:Z

    .line 2693552
    iget-object v0, p0, LX/JRd;->a:LX/3hx;

    iget-object v1, v0, LX/3hx;->j:LX/2oL;

    iget-boolean v0, p1, LX/7Jv;->a:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 2693553
    :goto_1
    iput-boolean v0, v1, LX/2oL;->d:Z

    .line 2693554
    iget-boolean v0, p1, LX/7Jv;->b:Z

    if-eqz v0, :cond_0

    .line 2693555
    iget-object v0, p0, LX/JRd;->a:LX/3hx;

    iget-object v0, v0, LX/3hx;->e:Lcom/facebook/feedplugins/multishare/MultiShareInlineVideoView;

    .line 2693556
    iget-object v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    move-object v0, v1

    .line 2693557
    invoke-virtual {v0}, LX/2pb;->c()V

    .line 2693558
    :cond_0
    iget-object v1, p0, LX/JRd;->a:LX/3hx;

    iget-object v2, p0, LX/JRd;->b:LX/3i8;

    .line 2693559
    invoke-static {v1, v2}, Lcom/facebook/feedplugins/multishare/MultiSharePagerItemViewPartDefinition;->b(LX/3hx;LX/3i8;)V

    .line 2693560
    return-void

    .line 2693561
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method
