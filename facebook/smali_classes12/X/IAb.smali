.class public final LX/IAb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/Set;

.field public final synthetic b:LX/IAd;


# direct methods
.method public constructor <init>(LX/IAd;Ljava/util/Set;)V
    .locals 0

    .prologue
    .line 2545903
    iput-object p1, p0, LX/IAb;->b:LX/IAd;

    iput-object p2, p0, LX/IAb;->a:Ljava/util/Set;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2545904
    iget-object v0, p0, LX/IAb;->b:LX/IAd;

    iget-object v1, p0, LX/IAb;->a:Ljava/util/Set;

    .line 2545905
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v3

    .line 2545906
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 2545907
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2545908
    new-instance p0, Lcom/facebook/user/model/UserFbidIdentifier;

    invoke-direct {p0, v2}, Lcom/facebook/user/model/UserFbidIdentifier;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 2545909
    :cond_0
    iget-object v2, v0, LX/IAd;->e:Ljava/util/concurrent/ExecutorService;

    new-instance v5, Lcom/facebook/events/permalink/messagefriends/EventCreateGroupHandler$3;

    invoke-direct {v5, v0, v4, v3}, Lcom/facebook/events/permalink/messagefriends/EventCreateGroupHandler$3;-><init>(LX/IAd;LX/0Pz;Lcom/google/common/util/concurrent/SettableFuture;)V

    const v4, -0x3897678d

    invoke-static {v2, v5, v4}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2545910
    move-object v0, v3

    .line 2545911
    return-object v0
.end method
