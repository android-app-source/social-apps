.class public final LX/Iqi;
.super LX/1cC;
.source ""


# instance fields
.field public final synthetic a:LX/Iql;


# direct methods
.method public constructor <init>(LX/Iql;)V
    .locals 0

    .prologue
    .line 2617162
    iput-object p1, p0, LX/Iqi;->a:LX/Iql;

    invoke-direct {p0}, LX/1cC;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/Object;Landroid/graphics/drawable/Animatable;)V
    .locals 2
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/drawable/Animatable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2617163
    iget-object v0, p0, LX/Iqi;->a:LX/Iql;

    iget-object v0, v0, LX/Iql;->e:LX/Iqh;

    .line 2617164
    iget-boolean v1, v0, LX/Iqg;->m:Z

    move v0, v1

    .line 2617165
    if-eqz v0, :cond_0

    .line 2617166
    :goto_0
    return-void

    .line 2617167
    :cond_0
    iget-object v0, p0, LX/Iqi;->a:LX/Iql;

    iget-object v0, v0, LX/Iql;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2617168
    iget-object v0, p0, LX/Iqi;->a:LX/Iql;

    iget-object v0, v0, LX/Iql;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, LX/Iqi;->a:LX/Iql;

    iget-object v1, v1, LX/Iql;->e:LX/Iqh;

    .line 2617169
    iget p1, v1, LX/Iqg;->g:F

    move v1, p1

    .line 2617170
    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setAlpha(F)V

    .line 2617171
    iget-object v0, p0, LX/Iqi;->a:LX/Iql;

    iget-object v0, v0, LX/Iql;->e:LX/Iqh;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/Iqg;->c(Z)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 2617172
    sget-object v0, LX/Iql;->a:Ljava/lang/String;

    const-string v1, "Failed to load image: %s"

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/Iqi;->a:LX/Iql;

    iget-object v4, v4, LX/Iql;->e:LX/Iqh;

    iget-object v4, v4, LX/Iqh;->a:Landroid/net/Uri;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2617173
    iget-object v0, p0, LX/Iqi;->a:LX/Iql;

    iget-object v0, v0, LX/Iql;->e:LX/Iqh;

    invoke-virtual {v0, v5}, LX/Iqg;->c(Z)V

    .line 2617174
    return-void
.end method
