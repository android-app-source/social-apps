.class public final LX/JIP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryCardPhotoProtileQueryModel;",
        ">;",
        "Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLInterfaces$DiscoveryPhotoProtileSectionFields;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/DiscoveryCardPhotoProtileFetcher;


# direct methods
.method public constructor <init>(Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/DiscoveryCardPhotoProtileFetcher;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2677875
    iput-object p1, p0, LX/JIP;->b:Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/DiscoveryCardPhotoProtileFetcher;

    iput-object p2, p0, LX/JIP;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2677876
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 2677877
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2677878
    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryCardPhotoProtileQueryModel;

    .line 2677879
    if-nez v0, :cond_0

    move v2, v1

    :goto_0
    if-eqz v2, :cond_2

    :goto_1
    if-eqz v1, :cond_4

    .line 2677880
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Null protile result from server for profile ID: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/JIP;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2677881
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryCardPhotoProtileQueryModel;->a()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    .line 2677882
    if-nez v2, :cond_1

    move v2, v1

    goto :goto_0

    :cond_1
    move v2, v3

    goto :goto_0

    .line 2677883
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryCardPhotoProtileQueryModel;->a()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const-class v4, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileSectionFieldsModel;

    invoke-virtual {v2, v1, v3, v4}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v1

    .line 2677884
    if-eqz v1, :cond_3

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    :goto_2
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    goto :goto_1

    .line 2677885
    :cond_3
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2677886
    goto :goto_2

    .line 2677887
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryCardPhotoProtileQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v2, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileSectionFieldsModel;

    invoke-virtual {v1, v0, v3, v2}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_3
    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileSectionFieldsModel;

    return-object v0

    .line 2677888
    :cond_5
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2677889
    goto :goto_3
.end method
