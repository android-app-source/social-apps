.class public LX/Ihj;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Z

.field public final b:Z

.field public final c:Z

.field public final d:Z

.field public final e:I

.field public final f:I


# direct methods
.method public constructor <init>(LX/Ihi;)V
    .locals 1

    .prologue
    .line 2602955
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2602956
    iget-boolean v0, p1, LX/Ihi;->a:Z

    iput-boolean v0, p0, LX/Ihj;->a:Z

    .line 2602957
    iget-boolean v0, p1, LX/Ihi;->c:Z

    iput-boolean v0, p0, LX/Ihj;->c:Z

    .line 2602958
    iget-boolean v0, p1, LX/Ihi;->d:Z

    iput-boolean v0, p0, LX/Ihj;->d:Z

    .line 2602959
    iget v0, p1, LX/Ihi;->e:I

    iput v0, p0, LX/Ihj;->e:I

    .line 2602960
    iget-boolean v0, p1, LX/Ihi;->b:Z

    iput-boolean v0, p0, LX/Ihj;->b:Z

    .line 2602961
    iget v0, p1, LX/Ihi;->f:I

    iput v0, p0, LX/Ihj;->f:I

    .line 2602962
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2602964
    if-eqz p1, :cond_0

    instance-of v1, p1, LX/Ihj;

    if-nez v1, :cond_1

    .line 2602965
    :cond_0
    :goto_0
    return v0

    .line 2602966
    :cond_1
    check-cast p1, LX/Ihj;

    .line 2602967
    iget-boolean v1, p0, LX/Ihj;->a:Z

    iget-boolean v2, p1, LX/Ihj;->a:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, LX/Ihj;->b:Z

    iget-boolean v2, p1, LX/Ihj;->b:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, LX/Ihj;->c:Z

    iget-boolean v2, p1, LX/Ihj;->c:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, LX/Ihj;->d:Z

    iget-boolean v2, p1, LX/Ihj;->d:Z

    if-ne v1, v2, :cond_0

    iget v1, p0, LX/Ihj;->e:I

    iget v2, p1, LX/Ihj;->e:I

    if-ne v1, v2, :cond_0

    iget v1, p0, LX/Ihj;->f:I

    iget v2, p1, LX/Ihj;->f:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2602963
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-boolean v2, p0, LX/Ihj;->a:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, LX/Ihj;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, LX/Ihj;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-boolean v2, p0, LX/Ihj;->d:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget v2, p0, LX/Ihj;->e:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget v2, p0, LX/Ihj;->f:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
