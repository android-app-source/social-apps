.class public LX/IfP;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final f:Ljava/lang/Object;


# instance fields
.field private final b:LX/0UI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0UI",
            "<",
            "Ljava/lang/String;",
            "LX/IfO;",
            "LX/If7;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Sh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/Ieu;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2599678
    const-string v0, "ContactsYouMayKnowListenerManager"

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/IfP;->a:LX/0Px;

    .line 2599679
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/IfP;->f:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2599675
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2599676
    new-instance v0, LX/IfM;

    invoke-direct {v0, p0}, LX/IfM;-><init>(LX/IfP;)V

    iput-object v0, p0, LX/IfP;->b:LX/0UI;

    .line 2599677
    return-void
.end method

.method public static a(LX/0QB;)LX/IfP;
    .locals 9

    .prologue
    .line 2599644
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2599645
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2599646
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2599647
    if-nez v1, :cond_0

    .line 2599648
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2599649
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2599650
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2599651
    sget-object v1, LX/IfP;->f:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2599652
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2599653
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2599654
    :cond_1
    if-nez v1, :cond_4

    .line 2599655
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2599656
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2599657
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2599658
    new-instance p0, LX/IfP;

    invoke-direct {p0}, LX/IfP;-><init>()V

    .line 2599659
    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v1

    check-cast v1, LX/0Sh;

    invoke-static {v0}, LX/Ieu;->a(LX/0QB;)LX/Ieu;

    move-result-object v7

    check-cast v7, LX/Ieu;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/ExecutorService;

    .line 2599660
    iput-object v1, p0, LX/IfP;->c:LX/0Sh;

    iput-object v7, p0, LX/IfP;->d:LX/Ieu;

    iput-object v8, p0, LX/IfP;->e:Ljava/util/concurrent/ExecutorService;

    .line 2599661
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2599662
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2599663
    if-nez v1, :cond_2

    .line 2599664
    sget-object v0, LX/IfP;->f:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IfP;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2599665
    :goto_1
    if-eqz v0, :cond_3

    .line 2599666
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2599667
    :goto_3
    check-cast v0, LX/IfP;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2599668
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2599669
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2599670
    :catchall_1
    move-exception v0

    .line 2599671
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2599672
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2599673
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2599674
    :cond_2
    :try_start_8
    sget-object v0, LX/IfP;->f:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IfP;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;LX/IfN;)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CYMKSuggestionSurface;
        .end annotation
    .end param

    .prologue
    .line 2599639
    iget-object v0, p0, LX/IfP;->c:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 2599640
    iget-object v0, p0, LX/IfP;->d:LX/Ieu;

    invoke-virtual {v0, p1, p2}, LX/Ieu;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;

    move-result-object v0

    .line 2599641
    if-eqz v0, :cond_0

    .line 2599642
    iget-object v1, p0, LX/IfP;->b:LX/0UI;

    sget-object v2, LX/IfP;->a:LX/0Px;

    new-instance v3, LX/IfO;

    invoke-direct {v3, p3, v0, p1}, LX/IfO;-><init>(LX/IfN;Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;Ljava/lang/String;)V

    iget-object v0, p0, LX/IfP;->e:Ljava/util/concurrent/ExecutorService;

    invoke-virtual {v1, v2, v3, v0}, LX/0UI;->a(Ljava/util/Collection;Ljava/lang/Object;Ljava/util/concurrent/Executor;)V

    .line 2599643
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/If7;)V
    .locals 2

    .prologue
    .line 2599637
    iget-object v0, p0, LX/IfP;->b:LX/0UI;

    const-string v1, "ContactsYouMayKnowListenerManager"

    invoke-virtual {v0, v1, p1}, LX/0UI;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2599638
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CYMKSuggestionSurface;
        .end annotation
    .end param

    .prologue
    .line 2599631
    sget-object v0, LX/IfN;->HIDDEN:LX/IfN;

    invoke-direct {p0, p1, p2, v0}, LX/IfP;->a(Ljava/lang/String;Ljava/lang/String;LX/IfN;)V

    .line 2599632
    return-void
.end method

.method public final b(LX/If7;)V
    .locals 2

    .prologue
    .line 2599635
    iget-object v0, p0, LX/IfP;->b:LX/0UI;

    const-string v1, "ContactsYouMayKnowListenerManager"

    invoke-virtual {v0, v1, p1}, LX/0UI;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2599636
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CYMKSuggestionSurface;
        .end annotation
    .end param

    .prologue
    .line 2599633
    sget-object v0, LX/IfN;->ADDED:LX/IfN;

    invoke-direct {p0, p1, p2, v0}, LX/IfP;->a(Ljava/lang/String;Ljava/lang/String;LX/IfN;)V

    .line 2599634
    return-void
.end method
