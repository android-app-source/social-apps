.class public LX/JVY;
.super LX/An9;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "LX/An9",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponent;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JVS;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponent;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/JVS;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2701090
    invoke-direct {p0}, LX/An9;-><init>()V

    .line 2701091
    iput-object p1, p0, LX/JVY;->a:LX/0Ot;

    .line 2701092
    iput-object p2, p0, LX/JVY;->b:LX/0Ot;

    .line 2701093
    return-void
.end method

.method public static a(LX/0QB;)LX/JVY;
    .locals 5

    .prologue
    .line 2701079
    const-class v1, LX/JVY;

    monitor-enter v1

    .line 2701080
    :try_start_0
    sget-object v0, LX/JVY;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2701081
    sput-object v2, LX/JVY;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2701082
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2701083
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2701084
    new-instance v3, LX/JVY;

    const/16 v4, 0x2097

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0x2095

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, p0}, LX/JVY;-><init>(LX/0Ot;LX/0Ot;)V

    .line 2701085
    move-object v0, v3

    .line 2701086
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2701087
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JVY;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2701088
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2701089
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X1;
    .locals 3
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2701066
    iget-object v0, p0, LX/JVY;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JVS;

    const/4 v1, 0x0

    .line 2701067
    new-instance v2, LX/JVR;

    invoke-direct {v2, v0}, LX/JVR;-><init>(LX/JVS;)V

    .line 2701068
    sget-object p0, LX/JVS;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/JVQ;

    .line 2701069
    if-nez p0, :cond_0

    .line 2701070
    new-instance p0, LX/JVQ;

    invoke-direct {p0}, LX/JVQ;-><init>()V

    .line 2701071
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/JVQ;->a$redex0(LX/JVQ;LX/1De;IILX/JVR;)V

    .line 2701072
    move-object v2, p0

    .line 2701073
    move-object v1, v2

    .line 2701074
    move-object v0, v1

    .line 2701075
    iget-object v1, v0, LX/JVQ;->a:LX/JVR;

    iput-object p2, v1, LX/JVR;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2701076
    iget-object v1, v0, LX/JVQ;->d:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2701077
    move-object v0, v0

    .line 2701078
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/3mj;Ljava/lang/Object;Ljava/lang/Object;)LX/1X1;
    .locals 2

    .prologue
    .line 2701046
    check-cast p4, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2701047
    iget-object v0, p0, LX/JVY;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponent;

    const/4 v1, 0x0

    .line 2701048
    new-instance p0, LX/JVV;

    invoke-direct {p0, v0}, LX/JVV;-><init>(Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponent;)V

    .line 2701049
    sget-object p5, Lcom/facebook/feedplugins/photochaining/components/PhotoChainingItemComponent;->a:LX/0Zi;

    invoke-virtual {p5}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p5

    check-cast p5, LX/JVU;

    .line 2701050
    if-nez p5, :cond_0

    .line 2701051
    new-instance p5, LX/JVU;

    invoke-direct {p5}, LX/JVU;-><init>()V

    .line 2701052
    :cond_0
    invoke-static {p5, p1, v1, v1, p0}, LX/JVU;->a$redex0(LX/JVU;LX/1De;IILX/JVV;)V

    .line 2701053
    move-object p0, p5

    .line 2701054
    move-object v1, p0

    .line 2701055
    move-object v0, v1

    .line 2701056
    iget-object v1, v0, LX/JVU;->a:LX/JVV;

    iput-object p2, v1, LX/JVV;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2701057
    iget-object v1, v0, LX/JVU;->d:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v1, p0}, Ljava/util/BitSet;->set(I)V

    .line 2701058
    move-object v0, v0

    .line 2701059
    iget-object v1, v0, LX/JVU;->a:LX/JVV;

    iput-object p4, v1, LX/JVV;->a:Lcom/facebook/graphql/model/GraphQLStory;

    .line 2701060
    iget-object v1, v0, LX/JVU;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, Ljava/util/BitSet;->set(I)V

    .line 2701061
    move-object v0, v0

    .line 2701062
    iget-object v1, v0, LX/JVU;->a:LX/JVV;

    iput-object p3, v1, LX/JVV;->c:LX/3mj;

    .line 2701063
    iget-object v1, v0, LX/JVU;->d:Ljava/util/BitSet;

    const/4 p0, 0x2

    invoke-virtual {v1, p0}, Ljava/util/BitSet;->set(I)V

    .line 2701064
    move-object v0, v0

    .line 2701065
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2701044
    const-class v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2701045
    const/4 v0, 0x6

    return v0
.end method
