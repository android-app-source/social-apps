.class public LX/HIb;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "LX/1S3;"
    }
.end annotation


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/pages/common/reaction/components/OfferOnPagesOfferCardComponentComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/HIb",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/pages/common/reaction/components/OfferOnPagesOfferCardComponentComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2451408
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2451409
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/HIb;->b:LX/0Zi;

    .line 2451410
    iput-object p1, p0, LX/HIb;->a:LX/0Ot;

    .line 2451411
    return-void
.end method

.method private b(LX/1X1;)V
    .locals 10

    .prologue
    .line 2451433
    check-cast p1, LX/HIa;

    .line 2451434
    iget-object v0, p0, LX/HIb;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/reaction/components/OfferOnPagesOfferCardComponentComponentSpec;

    iget-object v1, p1, LX/HIa;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v2, p1, LX/HIa;->b:LX/2km;

    .line 2451435
    iget-object v3, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v3

    .line 2451436
    invoke-interface {v3}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v3

    if-nez v3, :cond_0

    .line 2451437
    :goto_0
    return-void

    .line 2451438
    :cond_0
    iget-object v3, v0, Lcom/facebook/pages/common/reaction/components/OfferOnPagesOfferCardComponentComponentSpec;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/E1f;

    .line 2451439
    iget-object v4, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v4, v4

    .line 2451440
    invoke-interface {v4}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v4

    move-object v5, v2

    check-cast v5, LX/1Pn;

    invoke-interface {v5}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v5

    const/4 v6, 0x0

    move-object v7, v2

    check-cast v7, LX/2kp;

    invoke-interface {v7}, LX/2kp;->t()LX/2jY;

    move-result-object v7

    .line 2451441
    iget-object v8, v7, LX/2jY;->a:Ljava/lang/String;

    move-object v7, v8

    .line 2451442
    move-object v8, v2

    check-cast v8, LX/2kp;

    invoke-interface {v8}, LX/2kp;->t()LX/2jY;

    move-result-object v8

    .line 2451443
    iget-object v9, v8, LX/2jY;->b:Ljava/lang/String;

    move-object v8, v9

    .line 2451444
    iget-object v9, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v9, v9

    .line 2451445
    invoke-virtual/range {v3 .. v9}, LX/E1f;->a(LX/9rk;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v3

    .line 2451446
    iget-object v4, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v4, v4

    .line 2451447
    iget-object v5, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v5, v5

    .line 2451448
    invoke-interface {v2, v4, v5, v3}, LX/2km;->a(Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V

    goto :goto_0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2451449
    const v0, -0x36376d27

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 2451417
    check-cast p2, LX/HIa;

    .line 2451418
    iget-object v0, p0, LX/HIb;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/reaction/components/OfferOnPagesOfferCardComponentComponentSpec;

    iget-object v1, p2, LX/HIa;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    const/4 p2, 0x1

    const/4 p0, 0x0

    .line 2451419
    iget-object v2, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v2

    .line 2451420
    invoke-interface {v2}, LX/9uc;->aT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$ImageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$ImageModel;->b()Ljava/lang/String;

    move-result-object v2

    .line 2451421
    iget-object v3, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v3

    .line 2451422
    invoke-interface {v3}, LX/9uc;->dc()LX/174;

    move-result-object v3

    invoke-interface {v3}, LX/174;->a()Ljava/lang/String;

    move-result-object v3

    .line 2451423
    iget-object v4, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v4, v4

    .line 2451424
    invoke-interface {v4}, LX/9uc;->au()LX/174;

    move-result-object v4

    invoke-interface {v4}, LX/174;->a()Ljava/lang/String;

    move-result-object v4

    .line 2451425
    iget-object v5, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v5, v5

    .line 2451426
    invoke-interface {v5}, LX/9uc;->O()LX/174;

    move-result-object v5

    invoke-interface {v5}, LX/174;->a()Ljava/lang/String;

    move-result-object v5

    .line 2451427
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    const v7, 0x7f0a0097

    invoke-interface {v6, v7}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v6

    .line 2451428
    const v7, -0x36376d27

    const/4 v1, 0x0

    invoke-static {p1, v7, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v7

    move-object v7, v7

    .line 2451429
    invoke-interface {v6, v7}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v6

    const v7, 0x7f0b231b

    invoke-interface {v6, v7}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v6

    .line 2451430
    iget-object v7, v0, Lcom/facebook/pages/common/reaction/components/OfferOnPagesOfferCardComponentComponentSpec;->c:LX/1nu;

    invoke-virtual {v7, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v7

    sget-object v1, Lcom/facebook/pages/common/reaction/components/OfferOnPagesOfferCardComponentComponentSpec;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v7, v1}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v7

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v7, v1}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v7

    invoke-virtual {v7}, LX/1X5;->c()LX/1Di;

    move-result-object v7

    const v1, 0x7f0b231a

    invoke-interface {v7, v1}, LX/1Di;->q(I)LX/1Di;

    move-result-object v7

    move-object v2, v7

    .line 2451431
    invoke-interface {v6, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    const v6, 0x7f0a00ca

    const v7, 0x7f0b0050

    invoke-static {p1, v3, v6, p2, v7}, Lcom/facebook/pages/common/reaction/components/OfferOnPagesOfferCardComponentComponentSpec;->a(LX/1De;Ljava/lang/String;III)LX/1Di;

    move-result-object v3

    const v6, 0x7f0b0069

    invoke-interface {v3, p2, v6}, LX/1Di;->g(II)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0a00bd

    const v6, 0x7f0b004e

    invoke-static {p1, v4, v3, p0, v6}, Lcom/facebook/pages/common/reaction/components/OfferOnPagesOfferCardComponentComponentSpec;->a(LX/1De;Ljava/lang/String;III)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0a00a3

    const v4, 0x7f0b004e

    invoke-static {p1, v5, v3, p0, v4}, Lcom/facebook/pages/common/reaction/components/OfferOnPagesOfferCardComponentComponentSpec;->a(LX/1De;Ljava/lang/String;III)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2451432
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2451412
    invoke-static {}, LX/1dS;->b()V

    .line 2451413
    iget v0, p1, LX/1dQ;->b:I

    .line 2451414
    packed-switch v0, :pswitch_data_0

    .line 2451415
    :goto_0
    return-object v1

    .line 2451416
    :pswitch_0
    iget-object v0, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0}, LX/HIb;->b(LX/1X1;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x36376d27
        :pswitch_0
    .end packed-switch
.end method
