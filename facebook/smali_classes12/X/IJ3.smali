.class public final LX/IJ3;
.super LX/IIn;
.source ""


# instance fields
.field public final synthetic b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V
    .locals 1

    .prologue
    .line 2562448
    iput-object p1, p0, LX/IJ3;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-direct {p0, p1}, LX/IIn;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    return-void
.end method


# virtual methods
.method public final f()V
    .locals 3

    .prologue
    .line 2562424
    iget-object v0, p0, LX/IJ3;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-static {v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->I(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    .line 2562425
    iget-object v0, p0, LX/IJ3;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-static {v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->J(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    .line 2562426
    iget-object v0, p0, LX/IJ3;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-static {v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->D(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    .line 2562427
    iget-object v0, p0, LX/IJ3;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->b(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;Z)V

    .line 2562428
    iget-object v0, p0, LX/IJ3;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->n:LX/IID;

    .line 2562429
    iget-object v1, v0, LX/IID;->d:LX/IIC;

    sget-object v2, LX/IIC;->UPSELL:LX/IIC;

    if-ne v1, v2, :cond_0

    .line 2562430
    :goto_0
    return-void

    .line 2562431
    :cond_0
    sget-object v1, LX/IIC;->UPSELL:LX/IIC;

    iput-object v1, v0, LX/IID;->d:LX/IIC;

    .line 2562432
    const-string v1, "friends_nearby_dashboard_impression"

    invoke-static {v0, v1}, LX/IID;->i(LX/IID;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 2562433
    const-string v2, "view"

    sget-object p0, LX/IIC;->UPSELL:LX/IIC;

    iget-object p0, p0, LX/IIC;->name:Ljava/lang/String;

    invoke-virtual {v1, v2, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2562434
    iget-object v2, v0, LX/IID;->a:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0
.end method

.method public final r()V
    .locals 11

    .prologue
    .line 2562435
    iget-object v0, p0, LX/IJ3;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->S:Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;

    invoke-virtual {v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFeatureDisabledView;->getSelectedPrivacy()Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;

    move-result-object v0

    .line 2562436
    if-nez v0, :cond_0

    .line 2562437
    :goto_0
    return-void

    .line 2562438
    :cond_0
    iget-object v1, p0, LX/IJ3;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    const/4 v5, 0x1

    .line 2562439
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2562440
    const-string v2, "BackgroundLocationUpdateSettingsParams"

    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;->a(ZLjava/lang/String;)Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;

    move-result-object v3

    invoke-virtual {v4, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2562441
    const v2, 0x7f080024

    const/4 v3, 0x0

    invoke-static {v2, v5, v3, v5}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a(IZZZ)Landroid/support/v4/app/DialogFragment;

    move-result-object v8

    .line 2562442
    invoke-virtual {v1}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v2

    const-string v3, "turn_on_progress"

    invoke-virtual {v8, v2, v3}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2562443
    iget-object v9, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->f:LX/1Ck;

    sget-object v10, LX/IFy;->m:LX/IFy;

    iget-object v2, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->h:LX/0aG;

    const-string v3, "background_location_update_settings"

    sget-object v5, LX/1ME;->BY_EXCEPTION:LX/1ME;

    sget-object v6, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->b:Lcom/facebook/common/callercontext/CallerContext;

    const v7, 0x3996b64e

    invoke-static/range {v2 .. v7}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v2

    invoke-interface {v2}, LX/1MF;->start()LX/1ML;

    move-result-object v2

    new-instance v3, LX/IIU;

    invoke-direct {v3, v1, v8}, LX/IIU;-><init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;Landroid/support/v4/app/DialogFragment;)V

    invoke-virtual {v9, v10, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2562444
    iget-object v0, p0, LX/IJ3;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->n:LX/IID;

    .line 2562445
    const-string v1, "friends_nearby_dashboard_upsell_turn_on"

    invoke-static {v0, v1}, LX/IID;->i(LX/IID;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 2562446
    iget-object v2, v0, LX/IID;->a:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2562447
    goto :goto_0
.end method

.method public final s()V
    .locals 3

    .prologue
    .line 2562414
    iget-object v0, p0, LX/IJ3;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-static {v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->K(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    .line 2562415
    iget-object v0, p0, LX/IJ3;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, p0, LX/IJ3;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aA:LX/IIm;

    .line 2562416
    iget-boolean v2, v0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v2, v2

    .line 2562417
    if-eqz v2, :cond_0

    .line 2562418
    invoke-static {v0, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->a$redex1(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;LX/IIm;)V

    .line 2562419
    :goto_0
    return-void

    .line 2562420
    :cond_0
    iget-object v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->p:LX/IG7;

    sget-object p0, LX/IG6;->DASHBOARD_TTI:LX/IG6;

    invoke-virtual {v2, p0}, LX/IG7;->c(LX/IG6;)V

    .line 2562421
    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->ae:Z

    goto :goto_0
.end method

.method public final t()V
    .locals 2

    .prologue
    .line 2562422
    iget-object v0, p0, LX/IJ3;->b:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->a(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;Z)V

    .line 2562423
    return-void
.end method
