.class public LX/Iws;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/Iws;


# instance fields
.field public final a:LX/0if;

.field public b:J

.field public c:J


# direct methods
.method public constructor <init>(LX/0if;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2631036
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2631037
    iput-object p1, p0, LX/Iws;->a:LX/0if;

    .line 2631038
    return-void
.end method

.method public static a(LX/0QB;)LX/Iws;
    .locals 4

    .prologue
    .line 2631041
    sget-object v0, LX/Iws;->d:LX/Iws;

    if-nez v0, :cond_1

    .line 2631042
    const-class v1, LX/Iws;

    monitor-enter v1

    .line 2631043
    :try_start_0
    sget-object v0, LX/Iws;->d:LX/Iws;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2631044
    if-eqz v2, :cond_0

    .line 2631045
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2631046
    new-instance p0, LX/Iws;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v3

    check-cast v3, LX/0if;

    invoke-direct {p0, v3}, LX/Iws;-><init>(LX/0if;)V

    .line 2631047
    move-object v0, p0

    .line 2631048
    sput-object v0, LX/Iws;->d:LX/Iws;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2631049
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2631050
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2631051
    :cond_1
    sget-object v0, LX/Iws;->d:LX/Iws;

    return-object v0

    .line 2631052
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2631053
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static h(LX/Iws;)LX/1rQ;
    .locals 4

    .prologue
    .line 2631054
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v0

    const-string v1, "page_id"

    iget-wide v2, p0, LX/Iws;->b:J

    invoke-virtual {v0, v1, v2, v3}, LX/1rQ;->a(Ljava/lang/String;J)LX/1rQ;

    move-result-object v0

    const-string v1, "profile_id"

    iget-wide v2, p0, LX/Iws;->c:J

    invoke-virtual {v0, v1, v2, v3}, LX/1rQ;->a(Ljava/lang/String;J)LX/1rQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final g()V
    .locals 2

    .prologue
    .line 2631039
    iget-object v0, p0, LX/Iws;->a:LX/0if;

    sget-object v1, LX/0ig;->S:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->c(LX/0ih;)V

    .line 2631040
    return-void
.end method
