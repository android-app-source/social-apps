.class public LX/Ic5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/IZp;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/facebook/content/SecureContextHelper;

.field private final c:LX/Ic4;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/Ic4;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2595128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2595129
    iput-object p1, p0, LX/Ic5;->a:Landroid/content/Context;

    .line 2595130
    iput-object p2, p0, LX/Ic5;->b:Lcom/facebook/content/SecureContextHelper;

    .line 2595131
    iput-object p3, p0, LX/Ic5;->c:LX/Ic4;

    .line 2595132
    return-void
.end method

.method public static b(LX/0QB;)LX/Ic5;
    .locals 4

    .prologue
    .line 2595133
    new-instance v3, LX/Ic5;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/Ic4;->b(LX/0QB;)LX/Ic4;

    move-result-object v2

    check-cast v2, LX/Ic4;

    invoke-direct {v3, v0, v1, v2}, LX/Ic5;-><init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/Ic4;)V

    .line 2595134
    return-object v3
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2595135
    const-string v0, "RideNativeSignUpResultHandler"

    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;)Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 2595136
    iget-object v1, p1, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2595137
    iget-object v2, p1, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->e:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v2, v2

    .line 2595138
    iget-object v3, p1, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->g:Landroid/os/Bundle;

    move-object v3, v3

    .line 2595139
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    if-eqz v2, :cond_0

    if-nez v3, :cond_1

    .line 2595140
    :cond_0
    :goto_0
    return v0

    .line 2595141
    :cond_1
    const-string v4, "entry_point"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2595142
    const-string v5, "provider_display_name"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2595143
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 2595144
    iget-object v6, p0, LX/Ic5;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v7, p0, LX/Ic5;->a:Landroid/content/Context;

    .line 2595145
    new-instance v0, LX/CK3;

    invoke-direct {v0}, LX/CK3;-><init>()V

    move-object v0, v0

    .line 2595146
    iput-object v1, v0, LX/CK3;->e:Ljava/lang/String;

    .line 2595147
    move-object v0, v0

    .line 2595148
    iput-object v5, v0, LX/CK3;->f:Ljava/lang/String;

    .line 2595149
    move-object v0, v0

    .line 2595150
    iput-object v2, v0, LX/CK3;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2595151
    move-object v0, v0

    .line 2595152
    iget-object v1, p1, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->f:Ljava/lang/String;

    move-object v1, v1

    .line 2595153
    iput-object v1, v0, LX/CK3;->c:Ljava/lang/String;

    .line 2595154
    move-object v0, v0

    .line 2595155
    iput-object v4, v0, LX/CK3;->a:Ljava/lang/String;

    .line 2595156
    move-object v0, v0

    .line 2595157
    const-string v1, "request_tag"

    invoke-virtual {v3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2595158
    iput-object v1, v0, LX/CK3;->g:Ljava/lang/String;

    .line 2595159
    move-object v0, v0

    .line 2595160
    const-string v1, "address"

    invoke-virtual {v3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2595161
    iput-object v1, v0, LX/CK3;->d:Ljava/lang/String;

    .line 2595162
    move-object v0, v0

    .line 2595163
    iget-object v1, p1, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->h:Ljava/lang/String;

    move-object v1, v1

    .line 2595164
    iput-object v1, v0, LX/CK3;->i:Ljava/lang/String;

    .line 2595165
    move-object v1, v0

    .line 2595166
    const-string v0, "dest_coordinates"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/location/Coordinates;

    .line 2595167
    iput-object v0, v1, LX/CK3;->h:Lcom/facebook/location/Coordinates;

    .line 2595168
    move-object v0, v1

    .line 2595169
    new-instance v1, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;

    invoke-direct {v1, v0}, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;-><init>(LX/CK3;)V

    move-object v0, v1

    .line 2595170
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2595171
    const-string v2, "ride_service_params"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2595172
    const-string v2, "RideRequestFragment"

    invoke-static {v7, v2, v1}, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    move-object v0, v1

    .line 2595173
    iget-object v1, p0, LX/Ic5;->a:Landroid/content/Context;

    invoke-interface {v6, v0, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2595174
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;)Z
    .locals 3

    .prologue
    .line 2595175
    iget-object v0, p0, LX/Ic5;->c:LX/Ic4;

    .line 2595176
    iget-object v1, p1, Lcom/facebook/messaging/business/nativesignup/model/NativeSignUpParams;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2595177
    iget-object v2, v0, LX/Ic4;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v2, :cond_0

    .line 2595178
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 2595179
    :cond_0
    new-instance v2, LX/4JQ;

    invoke-direct {v2}, LX/4JQ;-><init>()V

    .line 2595180
    const-string p0, "provider_name"

    invoke-virtual {v2, p0, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2595181
    new-instance p0, LX/Ib8;

    invoke-direct {p0}, LX/Ib8;-><init>()V

    move-object p0, p0

    .line 2595182
    const-string p1, "input"

    invoke-virtual {p0, p1, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2595183
    iget-object v2, v0, LX/Ic4;->d:LX/0tX;

    invoke-static {p0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object p0

    invoke-virtual {v2, p0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    iput-object v2, v0, LX/Ic4;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2595184
    iget-object v2, v0, LX/Ic4;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance p0, LX/Iby;

    invoke-direct {p0, v0}, LX/Iby;-><init>(LX/Ic4;)V

    iget-object p1, v0, LX/Ic4;->e:Ljava/util/concurrent/ExecutorService;

    invoke-static {v2, p0, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
