.class public final LX/IdB;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)V
    .locals 0

    .prologue
    .line 2596094
    iput-object p1, p0, LX/IdB;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/location/Address;)V
    .locals 8
    .param p1    # Landroid/location/Address;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2596095
    iget-object v0, p0, LX/IdB;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2596096
    :goto_0
    return-void

    .line 2596097
    :cond_0
    if-eqz p1, :cond_1

    .line 2596098
    iget-object v0, p0, LX/IdB;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v1, p0, LX/IdB;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v1, v1, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->w:Lcom/facebook/messaging/business/ride/utils/RideServiceParams;

    .line 2596099
    iget-object v2, v1, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->d:Ljava/lang/String;

    move-object v1, v2

    .line 2596100
    sget-object v2, LX/Ib6;->DESTINATION:LX/Ib6;

    invoke-static {v0, v1, v2}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->a$redex0(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;Ljava/lang/String;LX/Ib6;)V

    .line 2596101
    iget-object v1, p0, LX/IdB;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    invoke-virtual {p1}, Landroid/location/Address;->getLatitude()D

    move-result-wide v2

    invoke-virtual {p1}, Landroid/location/Address;->getLongitude()D

    move-result-wide v4

    invoke-static {p1}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->b(Landroid/location/Address;)Ljava/lang/String;

    move-result-object v6

    .line 2596102
    invoke-static/range {v1 .. v6}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->b$redex0(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;DDLjava/lang/String;)V

    .line 2596103
    iget-object v0, p0, LX/IdB;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    invoke-static {v0}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->k(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)V

    goto :goto_0

    .line 2596104
    :cond_1
    iget-object v0, p0, LX/IdB;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v0, v0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->E:Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;

    iget-object v1, p0, LX/IdB;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    const v2, 0x7f082d7e

    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/business/ride/view/RideMultiOptionsView;->setActionText(Ljava/lang/String;)V

    goto :goto_0
.end method
