.class public final LX/JUt;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JUv;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/Boolean;

.field public b:Lcom/facebook/graphql/model/GraphQLNode;

.field public c:LX/JVP;

.field public final synthetic d:LX/JUv;


# direct methods
.method public constructor <init>(LX/JUv;)V
    .locals 1

    .prologue
    .line 2699867
    iput-object p1, p0, LX/JUt;->d:LX/JUv;

    .line 2699868
    move-object v0, p1

    .line 2699869
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2699870
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2699871
    const-string v0, "CommerceSaveButtonComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2699872
    if-ne p0, p1, :cond_1

    .line 2699873
    :cond_0
    :goto_0
    return v0

    .line 2699874
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2699875
    goto :goto_0

    .line 2699876
    :cond_3
    check-cast p1, LX/JUt;

    .line 2699877
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2699878
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2699879
    if-eq v2, v3, :cond_0

    .line 2699880
    iget-object v2, p0, LX/JUt;->a:Ljava/lang/Boolean;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JUt;->a:Ljava/lang/Boolean;

    iget-object v3, p1, LX/JUt;->a:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2699881
    goto :goto_0

    .line 2699882
    :cond_5
    iget-object v2, p1, LX/JUt;->a:Ljava/lang/Boolean;

    if-nez v2, :cond_4

    .line 2699883
    :cond_6
    iget-object v2, p0, LX/JUt;->b:Lcom/facebook/graphql/model/GraphQLNode;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/JUt;->b:Lcom/facebook/graphql/model/GraphQLNode;

    iget-object v3, p1, LX/JUt;->b:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2699884
    goto :goto_0

    .line 2699885
    :cond_8
    iget-object v2, p1, LX/JUt;->b:Lcom/facebook/graphql/model/GraphQLNode;

    if-nez v2, :cond_7

    .line 2699886
    :cond_9
    iget-object v2, p0, LX/JUt;->c:LX/JVP;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/JUt;->c:LX/JVP;

    iget-object v3, p1, LX/JUt;->c:LX/JVP;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2699887
    goto :goto_0

    .line 2699888
    :cond_a
    iget-object v2, p1, LX/JUt;->c:LX/JVP;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 2699889
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/JUt;

    .line 2699890
    const/4 v1, 0x0

    iput-object v1, v0, LX/JUt;->a:Ljava/lang/Boolean;

    .line 2699891
    return-object v0
.end method
