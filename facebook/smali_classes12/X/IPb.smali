.class public final LX/IPb;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/IPc;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/BcO;

.field public b:LX/Bdb;

.field public c:LX/1X1;

.field public d:LX/1X1;

.field public e:Z

.field public f:LX/IPe;

.field public g:LX/5K7;

.field public final synthetic h:LX/IPc;


# direct methods
.method public constructor <init>(LX/IPc;)V
    .locals 1

    .prologue
    .line 2574353
    iput-object p1, p0, LX/IPb;->h:LX/IPc;

    .line 2574354
    move-object v0, p1

    .line 2574355
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2574356
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2574357
    const-string v0, "GroupMallDrawerComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2574358
    if-ne p0, p1, :cond_1

    .line 2574359
    :cond_0
    :goto_0
    return v0

    .line 2574360
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2574361
    goto :goto_0

    .line 2574362
    :cond_3
    check-cast p1, LX/IPb;

    .line 2574363
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2574364
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2574365
    if-eq v2, v3, :cond_0

    .line 2574366
    iget-object v2, p0, LX/IPb;->a:LX/BcO;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/IPb;->a:LX/BcO;

    iget-object v3, p1, LX/IPb;->a:LX/BcO;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2574367
    goto :goto_0

    .line 2574368
    :cond_5
    iget-object v2, p1, LX/IPb;->a:LX/BcO;

    if-nez v2, :cond_4

    .line 2574369
    :cond_6
    iget-object v2, p0, LX/IPb;->b:LX/Bdb;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/IPb;->b:LX/Bdb;

    iget-object v3, p1, LX/IPb;->b:LX/Bdb;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2574370
    goto :goto_0

    .line 2574371
    :cond_8
    iget-object v2, p1, LX/IPb;->b:LX/Bdb;

    if-nez v2, :cond_7

    .line 2574372
    :cond_9
    iget-object v2, p0, LX/IPb;->c:LX/1X1;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/IPb;->c:LX/1X1;

    iget-object v3, p1, LX/IPb;->c:LX/1X1;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2574373
    goto :goto_0

    .line 2574374
    :cond_b
    iget-object v2, p1, LX/IPb;->c:LX/1X1;

    if-nez v2, :cond_a

    .line 2574375
    :cond_c
    iget-object v2, p0, LX/IPb;->d:LX/1X1;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/IPb;->d:LX/1X1;

    iget-object v3, p1, LX/IPb;->d:LX/1X1;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 2574376
    goto :goto_0

    .line 2574377
    :cond_e
    iget-object v2, p1, LX/IPb;->d:LX/1X1;

    if-nez v2, :cond_d

    .line 2574378
    :cond_f
    iget-boolean v2, p0, LX/IPb;->e:Z

    iget-boolean v3, p1, LX/IPb;->e:Z

    if-eq v2, v3, :cond_10

    move v0, v1

    .line 2574379
    goto :goto_0

    .line 2574380
    :cond_10
    iget-object v2, p0, LX/IPb;->f:LX/IPe;

    if-eqz v2, :cond_12

    iget-object v2, p0, LX/IPb;->f:LX/IPe;

    iget-object v3, p1, LX/IPb;->f:LX/IPe;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13

    :cond_11
    move v0, v1

    .line 2574381
    goto :goto_0

    .line 2574382
    :cond_12
    iget-object v2, p1, LX/IPb;->f:LX/IPe;

    if-nez v2, :cond_11

    .line 2574383
    :cond_13
    iget-object v2, p0, LX/IPb;->g:LX/5K7;

    if-eqz v2, :cond_14

    iget-object v2, p0, LX/IPb;->g:LX/5K7;

    iget-object v3, p1, LX/IPb;->g:LX/5K7;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2574384
    goto/16 :goto_0

    .line 2574385
    :cond_14
    iget-object v2, p1, LX/IPb;->g:LX/5K7;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 3

    .prologue
    .line 2574347
    const/4 v2, 0x0

    .line 2574348
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/IPb;

    .line 2574349
    iget-object v1, v0, LX/IPb;->c:LX/1X1;

    if-eqz v1, :cond_1

    iget-object v1, v0, LX/IPb;->c:LX/1X1;

    invoke-virtual {v1}, LX/1X1;->g()LX/1X1;

    move-result-object v1

    :goto_0
    iput-object v1, v0, LX/IPb;->c:LX/1X1;

    .line 2574350
    iget-object v1, v0, LX/IPb;->d:LX/1X1;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/IPb;->d:LX/1X1;

    invoke-virtual {v1}, LX/1X1;->g()LX/1X1;

    move-result-object v2

    :cond_0
    iput-object v2, v0, LX/IPb;->d:LX/1X1;

    .line 2574351
    return-object v0

    :cond_1
    move-object v1, v2

    .line 2574352
    goto :goto_0
.end method
