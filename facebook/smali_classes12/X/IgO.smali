.class public LX/IgO;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/messaging/media/folder/Folder;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/content/ContentResolver;

.field public c:Lcom/facebook/messaging/media/folder/LoadFolderParams;


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2600995
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2600996
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/IgO;->a:Ljava/util/HashMap;

    .line 2600997
    iput-object p1, p0, LX/IgO;->b:Landroid/content/ContentResolver;

    .line 2600998
    return-void
.end method

.method public static a(Landroid/database/Cursor;LX/IgK;)J
    .locals 2

    .prologue
    .line 2600999
    invoke-interface {p1}, LX/IgK;->g()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 2601000
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public static a(LX/IgO;LX/IgK;)V
    .locals 14

    .prologue
    .line 2601001
    iget-object v0, p0, LX/IgO;->b:Landroid/content/ContentResolver;

    invoke-interface {p1}, LX/IgK;->a()Landroid/net/Uri;

    move-result-object v1

    invoke-interface {p1}, LX/IgK;->b()[Ljava/lang/String;

    move-result-object v2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2601002
    iget-object v3, p0, LX/IgO;->c:Lcom/facebook/messaging/media/folder/LoadFolderParams;

    iget-boolean v3, v3, Lcom/facebook/messaging/media/folder/LoadFolderParams;->b:Z

    if-eqz v3, :cond_4

    const-string v3, "%s !=?) GROUP BY (%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-interface {p1}, LX/IgK;->h()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-interface {p1}, LX/IgK;->c()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    :goto_0
    move-object v3, v3

    .line 2601003
    iget-object v4, p0, LX/IgO;->c:Lcom/facebook/messaging/media/folder/LoadFolderParams;

    iget-boolean v4, v4, Lcom/facebook/messaging/media/folder/LoadFolderParams;->b:Z

    if-eqz v4, :cond_5

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    sget-object v6, Lcom/facebook/ipc/media/data/MimeType;->d:Lcom/facebook/ipc/media/data/MimeType;

    invoke-virtual {v6}, Lcom/facebook/ipc/media/data/MimeType;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    :goto_1
    move-object v4, v4

    .line 2601004
    invoke-interface {p1}, LX/IgK;->g()Ljava/lang/String;

    move-result-object v5

    .line 2601005
    const-string v6, "MAX(%s) DESC"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v5, v7, v8

    invoke-static {v6, v7}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    move-object v5, v6

    .line 2601006
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2601007
    if-eqz v1, :cond_3

    .line 2601008
    :try_start_0
    invoke-interface {p1}, LX/IgK;->c()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 2601009
    :goto_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2601010
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2601011
    iget-object v3, p0, LX/IgO;->a:Ljava/util/HashMap;

    invoke-virtual {v3, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2601012
    invoke-interface {p1}, LX/IgK;->d()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 2601013
    invoke-static {v1, p1}, LX/IgO;->a(Landroid/database/Cursor;LX/IgK;)J

    move-result-wide v8

    .line 2601014
    invoke-interface {v1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 2601015
    invoke-static {v1, p1}, LX/IgO;->b(Landroid/database/Cursor;LX/IgK;)Landroid/net/Uri;

    move-result-object v7

    .line 2601016
    invoke-static {}, Lcom/facebook/messaging/media/folder/Folder;->newBuilder()LX/IgI;

    move-result-object v10

    .line 2601017
    iput-object v6, v10, LX/IgI;->a:Ljava/lang/String;

    .line 2601018
    move-object v6, v10

    .line 2601019
    iput-object v2, v6, LX/IgI;->b:Ljava/lang/String;

    .line 2601020
    move-object v6, v6

    .line 2601021
    iput-object v7, v6, LX/IgI;->c:Landroid/net/Uri;

    .line 2601022
    move-object v6, v6

    .line 2601023
    iput-wide v8, v6, LX/IgI;->e:J

    .line 2601024
    move-object v6, v6

    .line 2601025
    invoke-static {v1, p1}, LX/IgO;->c(Landroid/database/Cursor;LX/IgK;)I

    move-result v7

    .line 2601026
    iput v7, v6, LX/IgI;->d:I

    .line 2601027
    move-object v6, v6

    .line 2601028
    invoke-virtual {v6}, LX/IgI;->a()Lcom/facebook/messaging/media/folder/Folder;

    move-result-object v6

    .line 2601029
    iget-object v7, p0, LX/IgO;->a:Ljava/util/HashMap;

    invoke-virtual {v7, v2, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2601030
    goto :goto_2

    .line 2601031
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 2601032
    :cond_0
    :try_start_1
    iget-object v6, p0, LX/IgO;->a:Ljava/util/HashMap;

    invoke-virtual {v6, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/messaging/media/folder/Folder;

    .line 2601033
    invoke-static {v1, p1}, LX/IgO;->a(Landroid/database/Cursor;LX/IgK;)J

    move-result-wide v8

    .line 2601034
    iget-wide v12, v6, Lcom/facebook/messaging/media/folder/Folder;->e:J

    move-wide v10, v12

    .line 2601035
    cmp-long v7, v8, v10

    if-lez v7, :cond_1

    .line 2601036
    invoke-static {}, Lcom/facebook/messaging/media/folder/Folder;->newBuilder()LX/IgI;

    move-result-object v7

    invoke-virtual {v7, v6}, LX/IgI;->a(Lcom/facebook/messaging/media/folder/Folder;)LX/IgI;

    move-result-object v6

    invoke-static {v1, p1}, LX/IgO;->b(Landroid/database/Cursor;LX/IgK;)Landroid/net/Uri;

    move-result-object v7

    .line 2601037
    iput-object v7, v6, LX/IgI;->c:Landroid/net/Uri;

    .line 2601038
    move-object v6, v6

    .line 2601039
    invoke-virtual {v6}, LX/IgI;->a()Lcom/facebook/messaging/media/folder/Folder;

    move-result-object v6

    .line 2601040
    iget-object v7, p0, LX/IgO;->a:Ljava/util/HashMap;

    invoke-virtual {v7, v2, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2601041
    :cond_1
    invoke-direct {p0, v1, p1, v2}, LX/IgO;->c(Landroid/database/Cursor;LX/IgK;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 2601042
    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2601043
    :cond_3
    return-void

    :cond_4
    const-string v3, "1) GROUP BY (%s"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-interface {p1}, LX/IgK;->c()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    :cond_5
    const/4 v4, 0x0

    goto/16 :goto_1
.end method

.method public static b(Landroid/database/Cursor;LX/IgK;)Landroid/net/Uri;
    .locals 4

    .prologue
    .line 2601044
    invoke-interface {p1}, LX/IgK;->e()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 2601045
    instance-of v1, p1, LX/IgL;

    if-eqz v1, :cond_0

    .line 2601046
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 2601047
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p1}, LX/IgK;->i()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2601048
    :goto_0
    return-object v0

    .line 2601049
    :cond_0
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2601050
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public static c(Landroid/database/Cursor;LX/IgK;)I
    .locals 1

    .prologue
    .line 2601051
    invoke-interface {p1}, LX/IgK;->f()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 2601052
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method private c(Landroid/database/Cursor;LX/IgK;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2601053
    iget-object v0, p0, LX/IgO;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/media/folder/Folder;

    .line 2601054
    invoke-static {p1, p2}, LX/IgO;->c(Landroid/database/Cursor;LX/IgK;)I

    move-result v1

    .line 2601055
    invoke-static {}, Lcom/facebook/messaging/media/folder/Folder;->newBuilder()LX/IgI;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/IgI;->a(Lcom/facebook/messaging/media/folder/Folder;)LX/IgI;

    move-result-object v2

    .line 2601056
    iget p1, v0, Lcom/facebook/messaging/media/folder/Folder;->d:I

    move v0, p1

    .line 2601057
    add-int/2addr v0, v1

    .line 2601058
    iput v0, v2, LX/IgI;->d:I

    .line 2601059
    move-object v0, v2

    .line 2601060
    invoke-virtual {v0}, LX/IgI;->a()Lcom/facebook/messaging/media/folder/Folder;

    move-result-object v0

    .line 2601061
    iget-object v1, p0, LX/IgO;->a:Ljava/util/HashMap;

    invoke-virtual {v1, p3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2601062
    return-void
.end method
