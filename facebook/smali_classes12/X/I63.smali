.class public final LX/I63;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2537016
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 2537017
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2537018
    :goto_0
    return v1

    .line 2537019
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2537020
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_5

    .line 2537021
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2537022
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2537023
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 2537024
    const-string v6, "boostable_story"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2537025
    invoke-static {p0, p1}, LX/I5x;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 2537026
    :cond_2
    const-string v6, "event_creator"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2537027
    invoke-static {p0, p1}, LX/I5y;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 2537028
    :cond_3
    const-string v6, "event_pinned_stories"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2537029
    invoke-static {p0, p1}, LX/I60;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 2537030
    :cond_4
    const-string v6, "event_stories"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2537031
    invoke-static {p0, p1}, LX/I62;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 2537032
    :cond_5
    const/4 v5, 0x4

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2537033
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 2537034
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 2537035
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2537036
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2537037
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2537038
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2537039
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2537040
    if-eqz v0, :cond_0

    .line 2537041
    const-string v1, "boostable_story"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2537042
    invoke-static {p0, v0, p2}, LX/I5x;->a(LX/15i;ILX/0nX;)V

    .line 2537043
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2537044
    if-eqz v0, :cond_1

    .line 2537045
    const-string v1, "event_creator"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2537046
    invoke-static {p0, v0, p2, p3}, LX/I5y;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2537047
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2537048
    if-eqz v0, :cond_2

    .line 2537049
    const-string v1, "event_pinned_stories"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2537050
    invoke-static {p0, v0, p2, p3}, LX/I60;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2537051
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2537052
    if-eqz v0, :cond_3

    .line 2537053
    const-string v1, "event_stories"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2537054
    invoke-static {p0, v0, p2, p3}, LX/I62;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2537055
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2537056
    return-void
.end method
