.class public LX/HVu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2475459
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2475460
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 6

    .prologue
    .line 2475461
    const-string v0, "com.facebook.katana.profile.id"

    const-wide/16 v2, -0x1

    invoke-virtual {p1, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    const-string v2, "extra_page_name"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "ANDROID_PAGE_POSTS"

    .line 2475462
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2475463
    new-instance v5, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;

    invoke-direct {v5}, Lcom/facebook/pages/common/surface/fragments/reaction/PageFullscreenReactionFragment;-><init>()V

    .line 2475464
    const-string p0, "ptr_enabled"

    const/4 p1, 0x1

    invoke-virtual {v4, p0, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2475465
    const-string p0, "com.facebook.katana.profile.id"

    invoke-virtual {v4, p0, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2475466
    const-string p0, "extra_page_name"

    invoke-virtual {v4, p0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2475467
    const-string p0, "extra_reaction_surface"

    invoke-virtual {v4, p0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2475468
    invoke-virtual {v5, v4}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2475469
    move-object v0, v5

    .line 2475470
    return-object v0
.end method
