.class public LX/Imk;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/Imm;

.field private final b:LX/5fv;

.field private final c:Ljava/util/regex/Pattern;


# direct methods
.method public constructor <init>(LX/Imm;LX/5fv;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2610139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2610140
    iput-object p1, p0, LX/Imk;->a:LX/Imm;

    .line 2610141
    iput-object p2, p0, LX/Imk;->b:LX/5fv;

    .line 2610142
    const-string v0, "\\$((\\d{1,3}(,\\d{3})*)|\\d+)(\\.\\d{1,2})?(?=\\s|\\.\\s|\\,\\s|$|\\.$|\\,$|\\?|\\!)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, LX/Imk;->c:Ljava/util/regex/Pattern;

    .line 2610143
    return-void
.end method
