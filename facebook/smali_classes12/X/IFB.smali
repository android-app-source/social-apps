.class public final LX/IFB;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;)V
    .locals 0

    .prologue
    .line 2553617
    iput-object p1, p0, LX/IFB;->a:Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2553618
    if-eqz p1, :cond_0

    .line 2553619
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2553620
    if-nez v0, :cond_1

    .line 2553621
    :cond_0
    iget-object v0, p0, LX/IFB;->a:Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;

    iget-object v0, v0, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;->t:LX/03V;

    const-string v1, "friends_nearby_detail_view_friend_info_abnormal"

    const-string v2, "friend info is empty"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2553622
    :goto_0
    return-void

    .line 2553623
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2553624
    check-cast v0, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;

    .line 2553625
    invoke-virtual {v0}, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;->a()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2553626
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2553627
    if-nez v1, :cond_2

    .line 2553628
    iget-object v0, p0, LX/IFB;->a:Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;

    iget-object v0, v0, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;->t:LX/03V;

    const-string v1, "friends_nearby_detail_view_friend_approximate_location_abnormal"

    const-string v2, "friend approximate location is empty"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2553629
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2553630
    :cond_2
    new-instance v3, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    const-string v4, "friends_nearby_detail_view"

    invoke-direct {v3, v4}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;-><init>(Ljava/lang/String;)V

    const/4 v4, 0x0

    invoke-virtual {v2, v1, v4}, LX/15i;->l(II)D

    move-result-wide v4

    const/4 v6, 0x1

    invoke-virtual {v2, v1, v6}, LX/15i;->l(II)D

    move-result-wide v6

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(DD)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    move-result-object v1

    const/16 v2, 0xd

    invoke-virtual {v1, v2}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(I)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    move-result-object v1

    .line 2553631
    iget-object v2, p0, LX/IFB;->a:Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;

    iget-object v2, v2, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;->x:Lcom/facebook/maps/FbStaticMapView;

    invoke-virtual {v2, v1}, LX/3BP;->setMapOptions(Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;)V

    .line 2553632
    invoke-virtual {v0}, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_4

    .line 2553633
    :cond_3
    iget-object v0, p0, LX/IFB;->a:Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;

    iget-object v0, v0, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;->t:LX/03V;

    const-string v1, "friends_nearby_detail_view_friend_profile_picture_info_abnormal"

    const-string v2, "friend profile picture info is empty"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2553634
    :cond_4
    iget-object v1, p0, LX/IFB;->a:Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;

    iget-object v1, v1, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;->y:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;->u:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2553635
    iget-object v1, p0, LX/IFB;->a:Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;

    iget-object v1, v1, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;->z:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2553636
    invoke-virtual {v0}, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel;->k()Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel$NearbyFriendsContactsSetItemModel;

    move-result-object v0

    .line 2553637
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel$NearbyFriendsContactsSetItemModel;->j()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {v0}, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel$NearbyFriendsContactsSetItemModel;->a()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v1

    if-nez v1, :cond_6

    .line 2553638
    :cond_5
    iget-object v0, p0, LX/IFB;->a:Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;

    iget-object v0, v0, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;->t:LX/03V;

    const-string v1, "friends_nearby_detail_view_friend_location_context_abnormal"

    const-string v2, "friend location context is empty"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2553639
    :cond_6
    iget-object v1, p0, LX/IFB;->a:Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;

    iget-object v1, v1, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;->A:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel$NearbyFriendsContactsSetItemModel;->j()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2553640
    iget-object v1, p0, LX/IFB;->a:Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;

    iget-object v1, v1, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;->B:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/facebook/friendsnearby/detailview/graphql/FriendsNearbyDetailViewQueryModels$FriendsNearbyDetailViewQueryModel$NearbyFriendsContactsSetItemModel;->a()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2553641
    iget-object v0, p0, LX/IFB;->a:Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;

    iget-object v0, v0, Lcom/facebook/friendsnearby/detailview/FriendsNearbyDetailViewActivity;->t:LX/03V;

    const-string v1, "friends_nearby_detail_view_friend_info_fetch_fail"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2553642
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2553643
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-direct {p0, p1}, LX/IFB;->a(Lcom/facebook/graphql/executor/GraphQLResult;)V

    return-void
.end method
