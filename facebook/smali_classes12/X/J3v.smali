.class public final LX/J3v;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel;",
        ">;",
        "LX/J4L;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/J3w;


# direct methods
.method public constructor <init>(LX/J3w;)V
    .locals 0

    .prologue
    .line 2643390
    iput-object p1, p0, LX/J3v;->a:LX/J3w;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2643391
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2643392
    iget-object v0, p0, LX/J3v;->a:LX/J3w;

    iget-object v0, v0, LX/J3w;->a:LX/J45;

    iget-object v0, v0, LX/J45;->f:LX/J4N;

    .line 2643393
    if-eqz p1, :cond_0

    .line 2643394
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2643395
    if-eqz v1, :cond_0

    .line 2643396
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2643397
    check-cast v1, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel;

    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel$AudienceInfoModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2643398
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2643399
    check-cast v1, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel;

    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel$AudienceInfoModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel$AudienceInfoModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2643400
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2643401
    check-cast v1, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel;

    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel$AudienceInfoModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel$AudienceInfoModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ComposerReviewModel;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2643402
    :cond_0
    const/4 v1, 0x0

    .line 2643403
    :goto_0
    move-object v0, v1

    .line 2643404
    return-object v0

    .line 2643405
    :cond_1
    new-instance v2, LX/J4L;

    sget-object v1, LX/J4K;->COMPOSER_STEP:LX/J4K;

    invoke-direct {v2, v1}, LX/J4L;-><init>(LX/J4K;)V

    .line 2643406
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2643407
    check-cast v1, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel;

    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel$AudienceInfoModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel$AudienceInfoModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ComposerReviewModel;

    move-result-object v1

    .line 2643408
    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ComposerReviewModel;->c()Z

    move-result p0

    if-nez p0, :cond_2

    move-object v1, v2

    .line 2643409
    goto :goto_0

    .line 2643410
    :cond_2
    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ComposerReviewModel;->c()Z

    move-result p0

    .line 2643411
    iput-boolean p0, v2, LX/J4L;->i:Z

    .line 2643412
    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ComposerReviewModel;->b()LX/0Px;

    move-result-object p0

    invoke-virtual {v2, p0}, LX/J4L;->a(LX/0Px;)V

    .line 2643413
    invoke-virtual {v1}, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchPrivacyCheckupComposerStepQueryModel$AudienceInfoModel$PrivacyCheckupInfoModel$ComposerReviewModel;->a()LX/J5E;

    move-result-object v1

    sget-object p0, LX/J4K;->COMPOSER_STEP:LX/J4K;

    invoke-static {v0, v1, p0}, LX/J4N;->a(LX/J4N;LX/J5E;LX/J4K;)LX/0Px;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/J4L;->a(Ljava/util/Collection;)V

    .line 2643414
    const/4 v1, 0x0

    .line 2643415
    iput-boolean v1, v2, LX/J4L;->k:Z

    .line 2643416
    move-object v1, v2

    .line 2643417
    goto :goto_0
.end method
