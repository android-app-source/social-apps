.class public final enum LX/HX6;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HX6;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/HX6;

.field public static final enum NO_SCROLL:LX/HX6;

.field public static final enum SCROLL_TO_TAB:LX/HX6;

.field public static final enum SCROLL_TO_TOP:LX/HX6;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2477352
    new-instance v0, LX/HX6;

    const-string v1, "SCROLL_TO_TAB"

    invoke-direct {v0, v1, v2}, LX/HX6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HX6;->SCROLL_TO_TAB:LX/HX6;

    .line 2477353
    new-instance v0, LX/HX6;

    const-string v1, "SCROLL_TO_TOP"

    invoke-direct {v0, v1, v3}, LX/HX6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HX6;->SCROLL_TO_TOP:LX/HX6;

    .line 2477354
    new-instance v0, LX/HX6;

    const-string v1, "NO_SCROLL"

    invoke-direct {v0, v1, v4}, LX/HX6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HX6;->NO_SCROLL:LX/HX6;

    .line 2477355
    const/4 v0, 0x3

    new-array v0, v0, [LX/HX6;

    sget-object v1, LX/HX6;->SCROLL_TO_TAB:LX/HX6;

    aput-object v1, v0, v2

    sget-object v1, LX/HX6;->SCROLL_TO_TOP:LX/HX6;

    aput-object v1, v0, v3

    sget-object v1, LX/HX6;->NO_SCROLL:LX/HX6;

    aput-object v1, v0, v4

    sput-object v0, LX/HX6;->$VALUES:[LX/HX6;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2477356
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HX6;
    .locals 1

    .prologue
    .line 2477351
    const-class v0, LX/HX6;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HX6;

    return-object v0
.end method

.method public static values()[LX/HX6;
    .locals 1

    .prologue
    .line 2477350
    sget-object v0, LX/HX6;->$VALUES:[LX/HX6;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HX6;

    return-object v0
.end method
