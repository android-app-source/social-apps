.class public final LX/JPW;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JPX;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Landroid/net/Uri;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Z

.field public e:Ljava/lang/String;

.field public final synthetic f:LX/JPX;


# direct methods
.method public constructor <init>(LX/JPX;)V
    .locals 1

    .prologue
    .line 2689892
    iput-object p1, p0, LX/JPW;->f:LX/JPX;

    .line 2689893
    move-object v0, p1

    .line 2689894
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2689895
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2689896
    const-string v0, "PostHeaderComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2689897
    if-ne p0, p1, :cond_1

    .line 2689898
    :cond_0
    :goto_0
    return v0

    .line 2689899
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2689900
    goto :goto_0

    .line 2689901
    :cond_3
    check-cast p1, LX/JPW;

    .line 2689902
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2689903
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2689904
    if-eq v2, v3, :cond_0

    .line 2689905
    iget-object v2, p0, LX/JPW;->a:Landroid/net/Uri;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JPW;->a:Landroid/net/Uri;

    iget-object v3, p1, LX/JPW;->a:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2689906
    goto :goto_0

    .line 2689907
    :cond_5
    iget-object v2, p1, LX/JPW;->a:Landroid/net/Uri;

    if-nez v2, :cond_4

    .line 2689908
    :cond_6
    iget-object v2, p0, LX/JPW;->b:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/JPW;->b:Ljava/lang/String;

    iget-object v3, p1, LX/JPW;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2689909
    goto :goto_0

    .line 2689910
    :cond_8
    iget-object v2, p1, LX/JPW;->b:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 2689911
    :cond_9
    iget-object v2, p0, LX/JPW;->c:Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/JPW;->c:Ljava/lang/String;

    iget-object v3, p1, LX/JPW;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2689912
    goto :goto_0

    .line 2689913
    :cond_b
    iget-object v2, p1, LX/JPW;->c:Ljava/lang/String;

    if-nez v2, :cond_a

    .line 2689914
    :cond_c
    iget-boolean v2, p0, LX/JPW;->d:Z

    iget-boolean v3, p1, LX/JPW;->d:Z

    if-eq v2, v3, :cond_d

    move v0, v1

    .line 2689915
    goto :goto_0

    .line 2689916
    :cond_d
    iget-object v2, p0, LX/JPW;->e:Ljava/lang/String;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/JPW;->e:Ljava/lang/String;

    iget-object v3, p1, LX/JPW;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2689917
    goto :goto_0

    .line 2689918
    :cond_e
    iget-object v2, p1, LX/JPW;->e:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
