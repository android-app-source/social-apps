.class public final LX/HW2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;)V
    .locals 0

    .prologue
    .line 2475538
    iput-object p1, p0, LX/HW2;->a:Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2475539
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 8
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2475540
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const-wide/16 v2, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2475541
    if-eqz p1, :cond_0

    .line 2475542
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2475543
    if-nez v0, :cond_1

    .line 2475544
    :cond_0
    :goto_0
    return-void

    .line 2475545
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2475546
    check-cast v0, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel;->k()I

    move-result v0

    int-to-long v0, v0

    .line 2475547
    iget-object v6, p0, LX/HW2;->a:Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;

    iget-object v6, v6, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->d:Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;

    if-eqz v6, :cond_2

    .line 2475548
    iget-object v6, p0, LX/HW2;->a:Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;

    iget-object v6, v6, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->d:Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;

    invoke-virtual {v6, v0, v1}, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->setInsightsBadgeCount(J)V

    .line 2475549
    :cond_2
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2475550
    check-cast v0, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-nez v0, :cond_4

    move v0, v4

    :goto_1
    if-nez v0, :cond_0

    .line 2475551
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2475552
    check-cast v0, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v6, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v6, v0, v5}, LX/15i;->g(II)I

    move-result v7

    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2475553
    invoke-virtual {v6, v7, v5}, LX/15i;->g(II)I

    move-result v0

    if-eqz v0, :cond_6

    .line 2475554
    invoke-virtual {v6, v7, v5}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {v6, v0, v4}, LX/15i;->j(II)I

    move-result v0

    int-to-long v0, v0

    add-long/2addr v0, v2

    .line 2475555
    :goto_2
    invoke-virtual {v6, v7, v4}, LX/15i;->g(II)I

    move-result v2

    if-eqz v2, :cond_3

    .line 2475556
    invoke-virtual {v6, v7, v4}, LX/15i;->g(II)I

    move-result v2

    invoke-virtual {v6, v2, v5}, LX/15i;->j(II)I

    move-result v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    .line 2475557
    :cond_3
    iget-object v2, p0, LX/HW2;->a:Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;

    iget-object v2, v2, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->d:Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;

    if-eqz v2, :cond_0

    .line 2475558
    iget-object v2, p0, LX/HW2;->a:Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;

    iget-object v2, v2, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;->d:Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;

    invoke-virtual {v2, v0, v1}, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->setActivityBadgeCount(J)V

    goto :goto_0

    .line 2475559
    :cond_4
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2475560
    check-cast v0, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2475561
    invoke-virtual {v1, v0, v5}, LX/15i;->g(II)I

    move-result v0

    if-nez v0, :cond_5

    move v0, v4

    goto :goto_1

    :cond_5
    move v0, v5

    goto :goto_1

    .line 2475562
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_6
    move-wide v0, v2

    goto :goto_2
.end method
