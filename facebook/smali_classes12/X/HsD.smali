.class public LX/HsD;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "Lcom/facebook/composer/collage/creators/CollageDraweeControllerCreator;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2514142
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 2514143
    return-void
.end method


# virtual methods
.method public final a(LX/0il;)Lcom/facebook/composer/collage/creators/CollageDraweeControllerCreator;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData::",
            "LX/0io;",
            "Services::",
            "LX/0il",
            "<TModelData;>;>(TServices;)",
            "Lcom/facebook/composer/collage/creators/CollageDraweeControllerCreator",
            "<TModelData;TServices;>;"
        }
    .end annotation

    .prologue
    .line 2514144
    new-instance v0, Lcom/facebook/composer/collage/creators/CollageDraweeControllerCreator;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/1VL;->b(LX/0QB;)LX/1VL;

    move-result-object v2

    check-cast v2, LX/1VL;

    invoke-static {p0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v3

    check-cast v3, LX/1Ad;

    const/16 v4, 0x2e3c

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x2ec8

    invoke-static {p0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x2e13

    invoke-static {p0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x2e3d

    invoke-static {p0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    move-object v8, p1

    invoke-direct/range {v0 .. v8}, Lcom/facebook/composer/collage/creators/CollageDraweeControllerCreator;-><init>(Landroid/content/Context;LX/1VL;LX/1Ad;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0il;)V

    .line 2514145
    return-object v0
.end method
