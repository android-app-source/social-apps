.class public LX/IqN;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1FZ;

.field private b:LX/1FJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1FZ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2616862
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2616863
    iput-object p1, p0, LX/IqN;->a:LX/1FZ;

    .line 2616864
    return-void
.end method

.method public static a(LX/0QB;)LX/IqN;
    .locals 2

    .prologue
    .line 2616865
    new-instance v1, LX/IqN;

    invoke-static {p0}, LX/1Et;->a(LX/0QB;)LX/1FZ;

    move-result-object v0

    check-cast v0, LX/1FZ;

    invoke-direct {v1, v0}, LX/IqN;-><init>(LX/1FZ;)V

    .line 2616866
    move-object v0, v1

    .line 2616867
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;LX/IqP;)Landroid/graphics/Bitmap;
    .locals 10

    .prologue
    const/high16 v8, 0x40000000    # 2.0f

    .line 2616868
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 2616869
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2616870
    instance-of v2, v0, Landroid/widget/EditText;

    if-eqz v2, :cond_0

    .line 2616871
    check-cast v0, Landroid/widget/EditText;

    .line 2616872
    invoke-virtual {v0}, Landroid/widget/EditText;->clearComposingText()V

    .line 2616873
    invoke-virtual {v0}, Landroid/widget/EditText;->getInputType()I

    move-result v2

    const/high16 v3, 0x80000

    or-int/2addr v2, v3

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setInputType(I)V

    .line 2616874
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 2616875
    :cond_1
    iget-object v0, p2, LX/IqP;->a:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    .line 2616876
    new-instance v0, LX/IqK;

    iget-object v1, p2, LX/IqP;->a:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, LX/IqK;-><init>(Landroid/graphics/Bitmap;)V

    move-object v6, v0

    .line 2616877
    :goto_1
    invoke-interface {v6}, LX/IqJ;->a()I

    move-result v1

    .line 2616878
    invoke-interface {v6}, LX/IqJ;->b()I

    move-result v2

    .line 2616879
    if-eqz v1, :cond_2

    if-nez v2, :cond_5

    .line 2616880
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Width and height for output must be > 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2616881
    :cond_3
    iget-object v0, p2, LX/IqP;->b:Landroid/view/View;

    if-eqz v0, :cond_4

    .line 2616882
    new-instance v0, LX/IqM;

    iget-object v1, p2, LX/IqP;->b:Landroid/view/View;

    invoke-direct {v0, v1}, LX/IqM;-><init>(Landroid/view/View;)V

    move-object v6, v0

    goto :goto_1

    .line 2616883
    :cond_4
    new-instance v0, LX/IqL;

    iget v1, p2, LX/IqP;->c:I

    iget v2, p2, LX/IqP;->d:I

    invoke-direct {v0, v1, v2}, LX/IqL;-><init>(II)V

    move-object v6, v0

    goto :goto_1

    .line 2616884
    :cond_5
    invoke-interface {v6}, LX/IqJ;->c()Landroid/graphics/Bitmap$Config;

    move-result-object v4

    .line 2616885
    iget v3, p2, LX/IqP;->f:I

    if-nez v4, :cond_6

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    :cond_6
    iget-object v0, p2, LX/IqP;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    iget-object v0, p2, LX/IqP;->e:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    const/16 v5, 0xff

    if-ge v0, v5, :cond_c

    :cond_7
    const/4 v5, 0x1

    :goto_2
    move-object v0, p0

    const/4 v7, 0x2

    .line 2616886
    if-eqz v5, :cond_f

    .line 2616887
    if-eqz v3, :cond_8

    if-ne v3, v7, :cond_e

    .line 2616888
    :cond_8
    invoke-static {v1, v2, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 2616889
    :goto_3
    new-instance v9, LX/IqI;

    invoke-direct {v9, v0}, LX/IqI;-><init>(LX/IqN;)V

    invoke-static {v7, v9}, LX/1FJ;->a(Ljava/lang/Object;LX/1FN;)LX/1FJ;

    move-result-object v7

    .line 2616890
    :goto_4
    move-object v0, v7

    .line 2616891
    iput-object v0, p0, LX/IqN;->b:LX/1FJ;

    .line 2616892
    iget-object v0, p0, LX/IqN;->b:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 2616893
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2616894
    iget-object v4, p2, LX/IqP;->e:Ljava/lang/Integer;

    if-eqz v4, :cond_9

    iget-object v4, p2, LX/IqP;->e:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-eqz v4, :cond_9

    .line 2616895
    iget-object v4, p2, LX/IqP;->e:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 2616896
    :cond_9
    iget v4, p2, LX/IqP;->f:I

    if-eqz v4, :cond_a

    .line 2616897
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 2616898
    iget v5, p2, LX/IqP;->f:I

    mul-int/lit8 v5, v5, -0x5a

    int-to-float v5, v5

    int-to-float v7, v4

    div-float/2addr v7, v8

    int-to-float v4, v4

    div-float/2addr v4, v8

    invoke-virtual {v3, v5, v7, v4}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 2616899
    :cond_a
    invoke-interface {v6, v3}, LX/IqJ;->a(Landroid/graphics/Canvas;)V

    .line 2616900
    invoke-virtual {v3}, Landroid/graphics/Canvas;->save()I

    .line 2616901
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getWidth()I

    move-result v4

    sub-int/2addr v1, v4

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getHeight()I

    move-result v4

    sub-int/2addr v2, v4

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {v3, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2616902
    invoke-virtual {p1, v3}, Landroid/view/ViewGroup;->draw(Landroid/graphics/Canvas;)V

    .line 2616903
    invoke-virtual {v3}, Landroid/graphics/Canvas;->restore()V

    .line 2616904
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v2, v1

    :goto_5
    if-ltz v2, :cond_d

    .line 2616905
    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2616906
    instance-of v3, v1, Landroid/widget/EditText;

    if-eqz v3, :cond_b

    .line 2616907
    check-cast v1, Landroid/widget/EditText;

    .line 2616908
    invoke-virtual {v1}, Landroid/widget/EditText;->getInputType()I

    move-result v3

    const v4, -0x80001

    and-int/2addr v3, v4

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setInputType(I)V

    .line 2616909
    :cond_b
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    goto :goto_5

    .line 2616910
    :cond_c
    const/4 v5, 0x0

    goto/16 :goto_2

    .line 2616911
    :cond_d
    return-object v0

    .line 2616912
    :cond_e
    invoke-static {v2, v1, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v7

    goto/16 :goto_3

    .line 2616913
    :cond_f
    if-eqz v3, :cond_10

    if-ne v3, v7, :cond_11

    .line 2616914
    :cond_10
    iget-object v7, v0, LX/IqN;->a:LX/1FZ;

    invoke-virtual {v7, v1, v2, v4}, LX/1FZ;->a(IILandroid/graphics/Bitmap$Config;)LX/1FJ;

    move-result-object v7

    goto/16 :goto_4

    .line 2616915
    :cond_11
    iget-object v7, v0, LX/IqN;->a:LX/1FZ;

    invoke-virtual {v7, v2, v1, v4}, LX/1FZ;->a(IILandroid/graphics/Bitmap$Config;)LX/1FJ;

    move-result-object v7

    goto/16 :goto_4
.end method
