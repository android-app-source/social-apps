.class public final LX/Icq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Zz;


# instance fields
.field public final synthetic a:Landroid/location/Location;

.field public final synthetic b:Landroid/location/Location;

.field public final synthetic c:Lcom/facebook/messaging/business/ride/view/RideMapView;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/business/ride/view/RideMapView;Landroid/location/Location;Landroid/location/Location;)V
    .locals 0

    .prologue
    .line 2595793
    iput-object p1, p0, LX/Icq;->c:Lcom/facebook/messaging/business/ride/view/RideMapView;

    iput-object p2, p0, LX/Icq;->a:Landroid/location/Location;

    iput-object p3, p0, LX/Icq;->b:Landroid/location/Location;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/6al;)V
    .locals 7

    .prologue
    .line 2595794
    iget-object v0, p0, LX/Icq;->c:Lcom/facebook/messaging/business/ride/view/RideMapView;

    iget-object v0, v0, Lcom/facebook/messaging/business/ride/view/RideMapView;->a:LX/Ibv;

    iget-object v1, p0, LX/Icq;->a:Landroid/location/Location;

    invoke-static {v1}, Lcom/facebook/messaging/business/ride/view/RideMapView;->c(Landroid/location/Location;)Lcom/facebook/android/maps/model/LatLng;

    move-result-object v1

    iget-object v2, p0, LX/Icq;->b:Landroid/location/Location;

    invoke-static {v2}, Lcom/facebook/messaging/business/ride/view/RideMapView;->c(Landroid/location/Location;)Lcom/facebook/android/maps/model/LatLng;

    move-result-object v2

    const/high16 p0, 0x3f800000    # 1.0f

    .line 2595795
    const/4 v3, 0x1

    .line 2595796
    invoke-virtual {p1}, LX/6al;->a()V

    .line 2595797
    invoke-static {}, LX/697;->a()LX/696;

    move-result-object v4

    .line 2595798
    new-instance v5, LX/699;

    invoke-direct {v5}, LX/699;-><init>()V

    const v6, 0x7f020ff0

    invoke-static {v6}, LX/690;->a(I)LX/68w;

    move-result-object v6

    .line 2595799
    iput-object v6, v5, LX/699;->c:LX/68w;

    .line 2595800
    move-object v5, v5

    .line 2595801
    iput-object v1, v5, LX/699;->b:Lcom/facebook/android/maps/model/LatLng;

    .line 2595802
    move-object v5, v5

    .line 2595803
    iput p0, v5, LX/699;->j:F

    .line 2595804
    move-object v5, v5

    .line 2595805
    invoke-virtual {p1, v5}, LX/6al;->a(LX/699;)LX/6ax;

    .line 2595806
    invoke-virtual {v4, v1}, LX/696;->a(Lcom/facebook/android/maps/model/LatLng;)LX/696;

    .line 2595807
    if-eqz v2, :cond_0

    .line 2595808
    new-instance v3, LX/699;

    invoke-direct {v3}, LX/699;-><init>()V

    const v5, 0x7f021023

    invoke-static {v5}, LX/690;->a(I)LX/68w;

    move-result-object v5

    .line 2595809
    iput-object v5, v3, LX/699;->c:LX/68w;

    .line 2595810
    move-object v3, v3

    .line 2595811
    iput-object v2, v3, LX/699;->b:Lcom/facebook/android/maps/model/LatLng;

    .line 2595812
    move-object v3, v3

    .line 2595813
    iput p0, v3, LX/699;->j:F

    .line 2595814
    move-object v3, v3

    .line 2595815
    invoke-virtual {p1, v3}, LX/6al;->a(LX/699;)LX/6ax;

    .line 2595816
    invoke-virtual {v4, v2}, LX/696;->a(Lcom/facebook/android/maps/model/LatLng;)LX/696;

    .line 2595817
    const/4 v3, 0x0

    .line 2595818
    :cond_0
    if-eqz v3, :cond_1

    const/high16 v3, 0x41600000    # 14.0f

    invoke-static {v1, v3}, LX/6aN;->a(Lcom/facebook/android/maps/model/LatLng;F)LX/6aM;

    move-result-object v3

    :goto_0
    invoke-virtual {p1, v3}, LX/6al;->a(LX/6aM;)V

    .line 2595819
    return-void

    .line 2595820
    :cond_1
    invoke-virtual {v4}, LX/696;->a()LX/697;

    move-result-object v3

    iget v4, v0, LX/Ibv;->a:I

    invoke-static {v3, v4}, LX/6aN;->a(LX/697;I)LX/6aM;

    move-result-object v3

    goto :goto_0
.end method
