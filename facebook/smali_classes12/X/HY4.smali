.class public LX/HY4;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/HY4;


# instance fields
.field public final a:LX/11i;

.field public final b:LX/HY0;

.field public final c:LX/4i1;


# direct methods
.method public constructor <init>(LX/11i;LX/HY0;LX/4i1;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2480085
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2480086
    iput-object p1, p0, LX/HY4;->a:LX/11i;

    .line 2480087
    iput-object p2, p0, LX/HY4;->b:LX/HY0;

    .line 2480088
    iput-object p3, p0, LX/HY4;->c:LX/4i1;

    .line 2480089
    return-void
.end method

.method public static a(LX/0QB;)LX/HY4;
    .locals 6

    .prologue
    .line 2480106
    sget-object v0, LX/HY4;->d:LX/HY4;

    if-nez v0, :cond_1

    .line 2480107
    const-class v1, LX/HY4;

    monitor-enter v1

    .line 2480108
    :try_start_0
    sget-object v0, LX/HY4;->d:LX/HY4;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2480109
    if-eqz v2, :cond_0

    .line 2480110
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2480111
    new-instance p0, LX/HY4;

    invoke-static {v0}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v3

    check-cast v3, LX/11i;

    invoke-static {v0}, LX/HY0;->a(LX/0QB;)LX/HY0;

    move-result-object v4

    check-cast v4, LX/HY0;

    invoke-static {v0}, LX/4i1;->a(LX/0QB;)LX/4i1;

    move-result-object v5

    check-cast v5, LX/4i1;

    invoke-direct {p0, v3, v4, v5}, LX/HY4;-><init>(LX/11i;LX/HY0;LX/4i1;)V

    .line 2480112
    move-object v0, p0

    .line 2480113
    sput-object v0, LX/HY4;->d:LX/HY4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2480114
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2480115
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2480116
    :cond_1
    sget-object v0, LX/HY4;->d:LX/HY4;

    return-object v0

    .line 2480117
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2480118
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/HY4;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2480104
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, LX/HY4;->b(LX/HY4;Ljava/lang/String;LX/0P1;)V

    .line 2480105
    return-void
.end method

.method public static a(LX/HY4;Ljava/lang/String;LX/0P1;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2480100
    iget-object v0, p0, LX/HY4;->a:LX/11i;

    iget-object v1, p0, LX/HY4;->b:LX/HY0;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 2480101
    if-eqz v0, :cond_0

    .line 2480102
    const/4 v1, 0x0

    const v2, 0x39cd2d73

    invoke-static {v0, p1, v1, p2, v2}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 2480103
    :cond_0
    return-void
.end method

.method public static b(LX/HY4;Ljava/lang/String;LX/0P1;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2480096
    iget-object v0, p0, LX/HY4;->a:LX/11i;

    iget-object v1, p0, LX/HY4;->b:LX/HY0;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 2480097
    if-eqz v0, :cond_0

    .line 2480098
    const/4 v1, 0x0

    const v2, -0x63f4fc0d

    invoke-static {v0, p1, v1, p2, v2}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    .line 2480099
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;ZZ)V
    .locals 7

    .prologue
    .line 2480092
    if-eqz p3, :cond_0

    const-string v5, "dialog_not_cached"

    .line 2480093
    :goto_0
    const-string v6, "PlatformWebDialogs_Fragment_fetchManifest"

    const-string v0, "action_name"

    invoke-virtual {p1}, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "dialog_version"

    invoke-virtual {p1}, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;->b()Ljava/lang/String;

    move-result-object v3

    const-string v4, "dialog_cache_status"

    invoke-static/range {v0 .. v5}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    invoke-static {p0, v6, v0}, LX/HY4;->b(LX/HY4;Ljava/lang/String;LX/0P1;)V

    .line 2480094
    return-void

    .line 2480095
    :cond_0
    if-eqz p2, :cond_1

    const-string v5, "dialog_cached_not_loaded"

    goto :goto_0

    :cond_1
    const-string v5, "dialog_loaded"

    goto :goto_0
.end method

.method public final l()V
    .locals 2

    .prologue
    .line 2480090
    iget-object v0, p0, LX/HY4;->a:LX/11i;

    iget-object v1, p0, LX/HY4;->b:LX/HY0;

    invoke-interface {v0, v1}, LX/11i;->c(LX/0Pq;)V

    .line 2480091
    return-void
.end method
