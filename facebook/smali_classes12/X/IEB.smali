.class public final enum LX/IEB;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IEB;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IEB;

.field public static final enum FRIEND_LIST_ALL_FRIENDS_SEEN:LX/IEB;

.field public static final enum FRIEND_LIST_MUTUAL_FRIENDS_SEEN:LX/IEB;

.field public static final enum FRIEND_LIST_OPENED:LX/IEB;

.field public static final enum FRIEND_LIST_PYMK_SEEN:LX/IEB;

.field public static final enum FRIEND_LIST_RECENTLY_ADDED_FRIENDS_SEEN:LX/IEB;

.field public static final enum FRIEND_LIST_SEARCH_BAR_CLICK:LX/IEB;

.field public static final enum FRIEND_LIST_SUGGESTIONS_SEEN:LX/IEB;

.field public static final enum FRIEND_LIST_TAB_SELECTED:LX/IEB;

.field public static final enum FRIEND_LIST_WITH_NEW_POSTS_SEEN:LX/IEB;


# instance fields
.field public final analyticsName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2551626
    new-instance v0, LX/IEB;

    const-string v1, "FRIEND_LIST_OPENED"

    const-string v2, "friend_list_opened"

    invoke-direct {v0, v1, v4, v2}, LX/IEB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/IEB;->FRIEND_LIST_OPENED:LX/IEB;

    .line 2551627
    new-instance v0, LX/IEB;

    const-string v1, "FRIEND_LIST_SEARCH_BAR_CLICK"

    const-string v2, "friend_list_search_bar_click"

    invoke-direct {v0, v1, v5, v2}, LX/IEB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/IEB;->FRIEND_LIST_SEARCH_BAR_CLICK:LX/IEB;

    .line 2551628
    new-instance v0, LX/IEB;

    const-string v1, "FRIEND_LIST_TAB_SELECTED"

    const-string v2, "friend_list_tab_selected"

    invoke-direct {v0, v1, v6, v2}, LX/IEB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/IEB;->FRIEND_LIST_TAB_SELECTED:LX/IEB;

    .line 2551629
    new-instance v0, LX/IEB;

    const-string v1, "FRIEND_LIST_ALL_FRIENDS_SEEN"

    const-string v2, "friend_list_all_friends_seen"

    invoke-direct {v0, v1, v7, v2}, LX/IEB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/IEB;->FRIEND_LIST_ALL_FRIENDS_SEEN:LX/IEB;

    .line 2551630
    new-instance v0, LX/IEB;

    const-string v1, "FRIEND_LIST_MUTUAL_FRIENDS_SEEN"

    const-string v2, "friend_list_mutual_friends_seen"

    invoke-direct {v0, v1, v8, v2}, LX/IEB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/IEB;->FRIEND_LIST_MUTUAL_FRIENDS_SEEN:LX/IEB;

    .line 2551631
    new-instance v0, LX/IEB;

    const-string v1, "FRIEND_LIST_PYMK_SEEN"

    const/4 v2, 0x5

    const-string v3, "friend_list_people_you_may_know_seen"

    invoke-direct {v0, v1, v2, v3}, LX/IEB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/IEB;->FRIEND_LIST_PYMK_SEEN:LX/IEB;

    .line 2551632
    new-instance v0, LX/IEB;

    const-string v1, "FRIEND_LIST_RECENTLY_ADDED_FRIENDS_SEEN"

    const/4 v2, 0x6

    const-string v3, "friend_list_recently_added_friends_seen"

    invoke-direct {v0, v1, v2, v3}, LX/IEB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/IEB;->FRIEND_LIST_RECENTLY_ADDED_FRIENDS_SEEN:LX/IEB;

    .line 2551633
    new-instance v0, LX/IEB;

    const-string v1, "FRIEND_LIST_SUGGESTIONS_SEEN"

    const/4 v2, 0x7

    const-string v3, "friend_list_suggestions_seen"

    invoke-direct {v0, v1, v2, v3}, LX/IEB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/IEB;->FRIEND_LIST_SUGGESTIONS_SEEN:LX/IEB;

    .line 2551634
    new-instance v0, LX/IEB;

    const-string v1, "FRIEND_LIST_WITH_NEW_POSTS_SEEN"

    const/16 v2, 0x8

    const-string v3, "friend_list_with_new_posts_seen"

    invoke-direct {v0, v1, v2, v3}, LX/IEB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/IEB;->FRIEND_LIST_WITH_NEW_POSTS_SEEN:LX/IEB;

    .line 2551635
    const/16 v0, 0x9

    new-array v0, v0, [LX/IEB;

    sget-object v1, LX/IEB;->FRIEND_LIST_OPENED:LX/IEB;

    aput-object v1, v0, v4

    sget-object v1, LX/IEB;->FRIEND_LIST_SEARCH_BAR_CLICK:LX/IEB;

    aput-object v1, v0, v5

    sget-object v1, LX/IEB;->FRIEND_LIST_TAB_SELECTED:LX/IEB;

    aput-object v1, v0, v6

    sget-object v1, LX/IEB;->FRIEND_LIST_ALL_FRIENDS_SEEN:LX/IEB;

    aput-object v1, v0, v7

    sget-object v1, LX/IEB;->FRIEND_LIST_MUTUAL_FRIENDS_SEEN:LX/IEB;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/IEB;->FRIEND_LIST_PYMK_SEEN:LX/IEB;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/IEB;->FRIEND_LIST_RECENTLY_ADDED_FRIENDS_SEEN:LX/IEB;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/IEB;->FRIEND_LIST_SUGGESTIONS_SEEN:LX/IEB;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/IEB;->FRIEND_LIST_WITH_NEW_POSTS_SEEN:LX/IEB;

    aput-object v2, v0, v1

    sput-object v0, LX/IEB;->$VALUES:[LX/IEB;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2551636
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2551637
    iput-object p3, p0, LX/IEB;->analyticsName:Ljava/lang/String;

    .line 2551638
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IEB;
    .locals 1

    .prologue
    .line 2551639
    const-class v0, LX/IEB;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IEB;

    return-object v0
.end method

.method public static values()[LX/IEB;
    .locals 1

    .prologue
    .line 2551640
    sget-object v0, LX/IEB;->$VALUES:[LX/IEB;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IEB;

    return-object v0
.end method
