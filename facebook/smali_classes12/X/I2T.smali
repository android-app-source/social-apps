.class public final LX/I2T;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/events/common/EventAnalyticsParams;

.field public final synthetic b:Lcom/facebook/events/dashboard/multirow/EventsDashboardEventRowPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/multirow/EventsDashboardEventRowPartDefinition;Lcom/facebook/events/common/EventAnalyticsParams;)V
    .locals 0

    .prologue
    .line 2530338
    iput-object p1, p0, LX/I2T;->b:Lcom/facebook/events/dashboard/multirow/EventsDashboardEventRowPartDefinition;

    iput-object p2, p0, LX/I2T;->a:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x6c16b865

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    move-object v0, p1

    .line 2530339
    check-cast v0, Lcom/facebook/events/dashboard/EventsDashboardRowView;

    .line 2530340
    iget-object v2, v0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->m:Lcom/facebook/events/model/Event;

    move-object v2, v2

    .line 2530341
    if-nez v2, :cond_0

    .line 2530342
    const v0, 0x1635fb6f

    invoke-static {v3, v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2530343
    :goto_0
    return-void

    .line 2530344
    :cond_0
    iget-object v2, p0, LX/I2T;->b:Lcom/facebook/events/dashboard/multirow/EventsDashboardEventRowPartDefinition;

    iget-object v2, v2, Lcom/facebook/events/dashboard/multirow/EventsDashboardEventRowPartDefinition;->b:LX/I76;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 2530345
    iget-object v4, v0, Lcom/facebook/events/dashboard/EventsDashboardRowView;->m:Lcom/facebook/events/model/Event;

    move-object v0, v4

    .line 2530346
    iget-object v4, p0, LX/I2T;->a:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v4, v4, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    invoke-virtual {v2, v3, v0, v4}, LX/I76;->a(Landroid/content/Context;Lcom/facebook/events/model/Event;Lcom/facebook/events/common/EventActionContext;)V

    .line 2530347
    const v0, 0x6037033d

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method
