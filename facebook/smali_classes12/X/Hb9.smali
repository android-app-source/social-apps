.class public final LX/Hb9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/HbE;


# direct methods
.method public constructor <init>(LX/HbE;)V
    .locals 0

    .prologue
    .line 2485154
    iput-object p1, p0, LX/Hb9;->a:LX/HbE;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x2

    const/4 v2, 0x1

    const v3, 0x2f8d8ec0

    invoke-static {v0, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2485155
    iget-object v0, p0, LX/Hb9;->a:LX/HbE;

    iget-object v0, v0, LX/HbE;->x:LX/HaZ;

    const-string v3, "LA_TURN_OFF"

    invoke-virtual {v0, v3}, LX/HaZ;->a(Ljava/lang/String;)V

    .line 2485156
    iget-object v0, p0, LX/Hb9;->a:LX/HbE;

    iget-object v0, v0, LX/HbE;->v:LX/Hai;

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 2485157
    iget-object v3, v0, LX/Hai;->b:LX/HbG;

    if-eqz v3, :cond_2

    iget-object v3, v0, LX/Hai;->b:LX/HbG;

    .line 2485158
    iget-boolean p1, v3, LX/HbG;->e:Z

    move v3, p1

    .line 2485159
    if-eqz v3, :cond_2

    move v3, v4

    :goto_0
    iput-boolean v3, v0, LX/Hai;->e:Z

    .line 2485160
    iget-object v3, v0, LX/Hai;->c:LX/HbG;

    if-eqz v3, :cond_3

    iget-object v3, v0, LX/Hai;->c:LX/HbG;

    .line 2485161
    iget-boolean p1, v3, LX/HbG;->e:Z

    move v3, p1

    .line 2485162
    if-eqz v3, :cond_3

    move v3, v4

    :goto_1
    iput-boolean v3, v0, LX/Hai;->f:Z

    .line 2485163
    iget-object v3, v0, LX/Hai;->d:LX/HbG;

    if-eqz v3, :cond_0

    iget-object v3, v0, LX/Hai;->d:LX/HbG;

    .line 2485164
    iget-boolean p1, v3, LX/HbG;->e:Z

    move v3, p1

    .line 2485165
    if-eqz v3, :cond_0

    move v5, v4

    :cond_0
    iput-boolean v5, v0, LX/Hai;->g:Z

    .line 2485166
    iput-boolean v4, v0, LX/Hai;->h:Z

    .line 2485167
    move v0, v1

    .line 2485168
    :goto_2
    iget-object v3, p0, LX/Hb9;->a:LX/HbE;

    iget-object v3, v3, LX/HbE;->s:Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;

    invoke-virtual {v3}, LX/1OM;->ij_()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 2485169
    iget-object v3, p0, LX/Hb9;->a:LX/HbE;

    iget-object v3, v3, LX/HbE;->s:Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;

    invoke-virtual {v3, v0}, Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;->e(I)LX/HbG;

    move-result-object v3

    .line 2485170
    iput-boolean v1, v3, LX/HbG;->e:Z

    .line 2485171
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2485172
    :cond_1
    iget-object v0, p0, LX/Hb9;->a:LX/HbE;

    iget-object v0, v0, LX/HbE;->s:Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2485173
    iget-object v0, p0, LX/Hb9;->a:LX/HbE;

    iget-object v0, v0, LX/HbE;->g:Lcom/facebook/resources/ui/FbButton;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2485174
    iget-object v0, p0, LX/Hb9;->a:LX/HbE;

    iget-object v0, v0, LX/HbE;->f:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2485175
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerController$4$1;

    invoke-direct {v1, p0}, Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerController$4$1;-><init>(LX/Hb9;)V

    const-wide/16 v4, 0x2bc

    const v3, -0x627cee0a

    invoke-static {v0, v1, v4, v5, v3}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2485176
    const v0, -0x5d9363d3

    invoke-static {v0, v2}, LX/02F;->a(II)V

    return-void

    :cond_2
    move v3, v5

    .line 2485177
    goto :goto_0

    :cond_3
    move v3, v5

    .line 2485178
    goto :goto_1
.end method
