.class public LX/ISI;
.super LX/ISH;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:I

.field public final c:LX/IRb;

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0ad;

.field public final g:LX/0Uh;

.field public final h:LX/DRd;

.field public final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field public final j:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

.field private l:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/DMB;",
            ">;"
        }
    .end annotation
.end field

.field private m:Z

.field public n:Z

.field public o:LX/DRc;

.field public p:LX/DMZ;

.field public q:LX/DOL;

.field private r:LX/3my;

.field public s:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2577731
    const-class v0, LX/ISI;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/ISI;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/IRb;LX/DRc;LX/0ad;LX/0Uh;LX/DRd;LX/0Or;LX/DMZ;LX/DOL;LX/3my;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 1
    .param p3    # LX/IRb;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/DRc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/IRb;",
            "LX/DRc;",
            "LX/0ad;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/DRd;",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;",
            "LX/DMZ;",
            "LX/DOL;",
            "LX/3my;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2577732
    invoke-direct {p0}, LX/ISH;-><init>()V

    .line 2577733
    const/4 v0, 0x2

    iput v0, p0, LX/ISI;->b:I

    .line 2577734
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2577735
    iput-object v0, p0, LX/ISI;->l:LX/0Px;

    .line 2577736
    iput-object p1, p0, LX/ISI;->d:LX/0Ot;

    .line 2577737
    iput-object p2, p0, LX/ISI;->e:LX/0Ot;

    .line 2577738
    iput-object p3, p0, LX/ISI;->c:LX/IRb;

    .line 2577739
    iput-object p4, p0, LX/ISI;->o:LX/DRc;

    .line 2577740
    iput-object p5, p0, LX/ISI;->f:LX/0ad;

    .line 2577741
    iput-object p6, p0, LX/ISI;->g:LX/0Uh;

    .line 2577742
    iput-object p7, p0, LX/ISI;->h:LX/DRd;

    .line 2577743
    iput-object p8, p0, LX/ISI;->i:LX/0Or;

    .line 2577744
    iput-object p9, p0, LX/ISI;->p:LX/DMZ;

    .line 2577745
    iput-object p10, p0, LX/ISI;->q:LX/DOL;

    .line 2577746
    iput-object p11, p0, LX/ISI;->r:LX/3my;

    .line 2577747
    iput-object p12, p0, LX/ISI;->j:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2577748
    return-void
.end method

.method public static a$redex0(LX/ISI;LX/5QS;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2577598
    new-instance v0, LX/IS4;

    invoke-direct {v0, p0, p2, p1}, LX/IS4;-><init>(LX/ISI;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;LX/5QS;)V

    return-object v0
.end method

.method public static a$redex0(LX/ISI;LX/7vF;Z)V
    .locals 7

    .prologue
    .line 2577749
    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->v()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityEventsFragmentModel$GroupEventsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->v()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityEventsFragmentModel$GroupEventsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityEventsFragmentModel$GroupEventsModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2577750
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2577751
    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->v()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityEventsFragmentModel$GroupEventsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityEventsFragmentModel$GroupEventsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    .line 2577752
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityEventsFragmentModel$GroupEventsModel$EdgesModel;

    .line 2577753
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityEventsFragmentModel$GroupEventsModel$EdgesModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityEventsFragmentModel$GroupEventsModel$EdgesModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, LX/7vF;->a()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2577754
    new-instance v5, LX/9NT;

    invoke-direct {v5}, LX/9NT;-><init>()V

    .line 2577755
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityEventsFragmentModel$GroupEventsModel$EdgesModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    move-result-object v6

    iput-object v6, v5, LX/9NT;->a:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    .line 2577756
    move-object v5, v5

    .line 2577757
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityEventsFragmentModel$GroupEventsModel$EdgesModel;->j()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/7vF;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;)Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    move-result-object v0

    .line 2577758
    iput-object v0, v5, LX/9NT;->a:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    .line 2577759
    move-object v0, v5

    .line 2577760
    invoke-virtual {v0}, LX/9NT;->a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityEventsFragmentModel$GroupEventsModel$EdgesModel;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2577761
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2577762
    :cond_0
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 2577763
    :cond_1
    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->v()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityEventsFragmentModel$GroupEventsModel;

    move-result-object v0

    .line 2577764
    new-instance v1, LX/9NS;

    invoke-direct {v1}, LX/9NS;-><init>()V

    .line 2577765
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityEventsFragmentModel$GroupEventsModel;->a()LX/0Px;

    move-result-object v3

    iput-object v3, v1, LX/9NS;->a:LX/0Px;

    .line 2577766
    move-object v0, v1

    .line 2577767
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 2577768
    iput-object v1, v0, LX/9NS;->a:LX/0Px;

    .line 2577769
    move-object v0, v0

    .line 2577770
    iget-object v1, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-static {v1}, LX/9OQ;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)LX/9OQ;

    move-result-object v1

    new-instance v2, LX/9OG;

    invoke-direct {v2}, LX/9OG;-><init>()V

    invoke-virtual {v0}, LX/9NS;->a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityEventsFragmentModel$GroupEventsModel;

    move-result-object v0

    .line 2577771
    iput-object v0, v2, LX/9OG;->i:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityEventsFragmentModel$GroupEventsModel;

    .line 2577772
    move-object v0, v2

    .line 2577773
    invoke-virtual {v0}, LX/9OG;->a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    .line 2577774
    iput-object v0, v1, LX/9OQ;->e:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    .line 2577775
    move-object v0, v1

    .line 2577776
    invoke-virtual {v0}, LX/9OQ;->a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-result-object v0

    iput-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 2577777
    if-eqz p2, :cond_2

    .line 2577778
    const v0, 0x27de8059

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2577779
    :cond_2
    return-void
.end method

.method private d()Z
    .locals 4

    .prologue
    .line 2577780
    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/ISI;->r:LX/3my;

    iget-object v1, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v1

    const/4 v2, 0x0

    .line 2577781
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->INFORMAL:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    if-ne v1, v3, :cond_0

    iget-object v3, v0, LX/3my;->a:LX/0Uh;

    const/16 p0, 0x292

    invoke-virtual {v3, p0, v2}, LX/0Uh;->a(IZ)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    :cond_0
    move v0, v2

    .line 2577782
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static f(LX/ISI;)Z
    .locals 2

    .prologue
    .line 2577783
    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static g(LX/ISI;)Z
    .locals 1

    .prologue
    .line 2577784
    invoke-direct {p0}, LX/ISI;->w()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->O()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->O()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel$GroupStoriesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->O()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel$GroupStoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$ForumFieldsModel$GroupStoriesModel;->a()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()Z
    .locals 4

    .prologue
    .line 2577785
    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->v()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static r(LX/ISI;)Z
    .locals 2

    .prologue
    .line 2577786
    invoke-static {p0}, LX/ISI;->v(LX/ISI;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->r()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->r()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$ButtonTextModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->r()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;->l()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$TitleModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->r()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;->c()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->r()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->r()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel;->k()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityCreationUpsellModel$CommunityCreationUnitModel$CoverImageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static v(LX/ISI;)Z
    .locals 2

    .prologue
    .line 2577787
    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ISI;->r:LX/3my;

    iget-object v1, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3my;->a(Lcom/facebook/graphql/enums/GraphQLGroupCategory;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private w()Z
    .locals 2

    .prologue
    .line 2577728
    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ISI;->r:LX/3my;

    iget-object v1, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3my;->b(Lcom/facebook/graphql/enums/GraphQLGroupCategory;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2577729
    sget-object v0, LX/IUI;->I:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DML;

    invoke-interface {v0, p2}, LX/DML;->a(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 2577730
    return-object v0
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 2577727
    return-void
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 2577724
    check-cast p2, LX/DMB;

    .line 2577725
    invoke-interface {p2, p3}, LX/DMB;->a(Landroid/view/View;)V

    .line 2577726
    return-void
.end method

.method public final a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;ZZZLX/IVp;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;",
            "ZZZ",
            "LX/IVp",
            "<",
            "LX/DMB;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2577609
    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-ne v0, p1, :cond_0

    iget-boolean v0, p0, LX/ISI;->m:Z

    if-ne v0, p2, :cond_0

    iget-boolean v0, p0, LX/ISI;->n:Z

    if-ne v0, p3, :cond_0

    if-nez p4, :cond_0

    .line 2577610
    :goto_0
    return-void

    .line 2577611
    :cond_0
    iput-object p1, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 2577612
    iput-boolean p2, p0, LX/ISI;->m:Z

    .line 2577613
    if-eqz p2, :cond_1

    if-eqz p3, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, LX/ISI;->n:Z

    .line 2577614
    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 2577615
    iget-object v0, p0, LX/ISI;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v1, LX/ISI;->a:Ljava/lang/String;

    const-string v2, "Tried to bind a group without a group id"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2577616
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 2577617
    :cond_2
    const/4 v0, 0x1

    .line 2577618
    invoke-static {p0}, LX/ISI;->g(LX/ISI;)Z

    move-result v1

    if-eqz v1, :cond_28

    .line 2577619
    :cond_3
    :goto_2
    move v0, v0

    .line 2577620
    if-eqz v0, :cond_4

    .line 2577621
    new-instance v0, LX/DaN;

    sget-object v1, LX/IUI;->i:LX/DML;

    new-instance v2, LX/B1U;

    iget-object v3, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-direct {p0}, LX/ISI;->w()Z

    move-result v4

    invoke-direct {v2, v3, v4}, LX/B1U;-><init>(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;Z)V

    invoke-direct {v0, v1, v2}, LX/DaN;-><init>(LX/DML;Ljava/lang/Object;)V

    sget-object v1, LX/IVo;->COMPOSER_BAR:LX/IVo;

    invoke-virtual {p5, v0, v1}, LX/IVp;->a(Ljava/lang/Object;Ljava/lang/Enum;)LX/IVp;

    .line 2577622
    :cond_4
    invoke-direct {p0}, LX/ISI;->d()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2577623
    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->Q()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel$SubgroupsFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->Q()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel$SubgroupsFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel$SubgroupsFieldsModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$SubgroupsRelatedInformationModel$ChildGroupsModel;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->Q()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel$SubgroupsFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel$SubgroupsFieldsModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$SubgroupsRelatedInformationModel$ChildGroupsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$SubgroupsRelatedInformationModel$ChildGroupsModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->Q()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel$SubgroupsFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel$SubgroupsFieldsModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$SubgroupsRelatedInformationModel$ChildGroupsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$SubgroupsRelatedInformationModel$ChildGroupsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 2577624
    :cond_5
    :goto_3
    invoke-direct {p0}, LX/ISI;->j()Z

    move-result v0

    if-nez v0, :cond_6

    .line 2577625
    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v0, :cond_30

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->R()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel;

    move-result-object v0

    if-eqz v0, :cond_30

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->R()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupSuggestionTipsModel$TipsChannelModel;->b()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_30

    const/4 v0, 0x1

    :goto_4
    move v0, v0

    .line 2577626
    if-nez v0, :cond_7

    :cond_6
    invoke-static {p0}, LX/ISI;->r(LX/ISI;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2577627
    :cond_7
    new-instance v0, LX/IS6;

    sget-object v1, LX/IUI;->p:LX/DML;

    invoke-direct {v0, p0, v1, p1}, LX/IS6;-><init>(LX/ISI;LX/DML;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V

    sget-object v1, LX/IVo;->SUGGESTIONS_CARD:LX/IVo;

    invoke-virtual {p5, v0, v1}, LX/IVp;->a(Ljava/lang/Object;Ljava/lang/Enum;)LX/IVp;

    .line 2577628
    :cond_8
    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v0, :cond_31

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    if-eqz v0, :cond_31

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-static {v0}, LX/DZD;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)LX/DZC;

    move-result-object v0

    invoke-static {v0}, LX/DJw;->a(LX/DZC;)Z

    move-result v0

    if-eqz v0, :cond_31

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->R()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupOwnerAuthoredStoriesModel;

    move-result-object v0

    if-eqz v0, :cond_31

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->R()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupOwnerAuthoredStoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$GroupOwnerAuthoredStoriesModel;->b()I

    move-result v0

    if-lez v0, :cond_31

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-static {v0}, LX/37Q;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Z

    move-result v0

    if-eqz v0, :cond_31

    const/4 v0, 0x1

    :goto_5
    move v0, v0

    .line 2577629
    if-eqz v0, :cond_9

    .line 2577630
    iget-object v0, p0, LX/ISI;->f:LX/0ad;

    sget-short v1, LX/DIp;->a:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    move v0, v0

    .line 2577631
    if-nez v0, :cond_9

    .line 2577632
    new-instance v0, LX/DaN;

    sget-object v1, LX/IUI;->c:LX/DML;

    iget-object v2, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-direct {v0, v1, v2}, LX/DaN;-><init>(LX/DML;Ljava/lang/Object;)V

    sget-object v1, LX/IVo;->YOUR_SALE_POST:LX/IVo;

    invoke-virtual {p5, v0, v1}, LX/IVp;->a(Ljava/lang/Object;Ljava/lang/Enum;)LX/IVp;

    .line 2577633
    :cond_9
    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v0, :cond_32

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-static {v0}, LX/DZD;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)LX/DZC;

    move-result-object v0

    invoke-static {v0}, LX/DJw;->a(LX/DZC;)Z

    move-result v0

    if-eqz v0, :cond_32

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-static {v0}, LX/37Q;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Z

    move-result v0

    if-eqz v0, :cond_32

    const/4 v0, 0x1

    :goto_6
    move v0, v0

    .line 2577634
    if-eqz v0, :cond_a

    .line 2577635
    new-instance v0, LX/DaN;

    sget-object v1, LX/IUI;->e:LX/DML;

    iget-object v2, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-direct {v0, v1, v2}, LX/DaN;-><init>(LX/DML;Ljava/lang/Object;)V

    sget-object v1, LX/IVo;->FOR_SALE_POSTS_BAR:LX/IVo;

    invoke-virtual {p5, v0, v1}, LX/IVp;->a(Ljava/lang/Object;Ljava/lang/Enum;)LX/IVp;

    .line 2577636
    :cond_a
    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v0, :cond_33

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    if-eqz v0, :cond_33

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-static {v0}, LX/DZD;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)LX/DZC;

    move-result-object v0

    const/4 v1, 0x0

    .line 2577637
    invoke-static {v0}, LX/DJw;->a(LX/DZC;)Z

    move-result v2

    if-nez v2, :cond_34

    .line 2577638
    :cond_b
    :goto_7
    move v0, v1

    .line 2577639
    if-eqz v0, :cond_33

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-static {v0}, LX/37Q;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Z

    move-result v0

    if-eqz v0, :cond_33

    const/4 v0, 0x1

    :goto_8
    move v0, v0

    .line 2577640
    if-eqz v0, :cond_c

    .line 2577641
    new-instance v0, LX/DaN;

    sget-object v1, LX/IUI;->d:LX/DML;

    iget-object v2, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-direct {v0, v1, v2}, LX/DaN;-><init>(LX/DML;Ljava/lang/Object;)V

    sget-object v1, LX/IVo;->BROWSE_CATEGORIES_BAR:LX/IVo;

    invoke-virtual {p5, v0, v1}, LX/IVp;->a(Ljava/lang/Object;Ljava/lang/Enum;)LX/IVp;

    .line 2577642
    :cond_c
    iget-boolean v0, p0, LX/ISI;->n:Z

    move v0, v0

    .line 2577643
    if-eqz v0, :cond_d

    .line 2577644
    new-instance v0, LX/DaP;

    sget-object v1, LX/IUI;->t:LX/DaO;

    invoke-direct {v0, v1}, LX/DaP;-><init>(LX/DML;)V

    sget-object v1, LX/IVo;->LOADING_BAR:LX/IVo;

    invoke-virtual {p5, v0, v1}, LX/IVp;->a(Ljava/lang/Object;Ljava/lang/Enum;)LX/IVp;

    .line 2577645
    :cond_d
    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v0, :cond_36

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2577646
    iget-object v2, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v2

    if-nez v2, :cond_37

    .line 2577647
    :cond_e
    :goto_9
    move v0, v0

    .line 2577648
    if-nez v0, :cond_36

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    if-eqz v0, :cond_36

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->COLLEGE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    iget-object v1, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_36

    const/4 v0, 0x1

    :goto_a
    move v0, v0

    .line 2577649
    if-nez v0, :cond_f

    invoke-static {p0}, LX/ISI;->v(LX/ISI;)Z

    move-result v0

    if-eqz v0, :cond_35

    :cond_f
    const/4 v0, 0x1

    :goto_b
    move v0, v0

    .line 2577650
    if-eqz v0, :cond_23

    .line 2577651
    const p2, 0x14954af1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2577652
    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->N()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CommunityLocationModel;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 2577653
    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->N()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CommunityLocationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel$CommunityLocationModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2577654
    if-eqz v0, :cond_12

    move v0, v1

    :goto_c
    if-eqz v0, :cond_10

    .line 2577655
    new-instance v0, LX/IS9;

    sget-object v3, LX/IUI;->s:LX/DML;

    invoke-direct {v0, p0, v3}, LX/IS9;-><init>(LX/ISI;LX/DML;)V

    sget-object v3, LX/IVo;->WEATHER:LX/IVo;

    invoke-virtual {p5, v0, v3}, LX/IVp;->a(Ljava/lang/Object;Ljava/lang/Enum;)LX/IVp;

    .line 2577656
    :cond_10
    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    if-eqz v0, :cond_11

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->e()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_11

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->e()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_11

    iget-object v3, p0, LX/ISI;->j:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v0, LX/ILq;->d:LX/0Tn;

    iget-object v4, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-interface {v3, v0, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-nez v0, :cond_11

    .line 2577657
    new-instance v0, LX/ISA;

    sget-object v3, LX/IUI;->B:LX/DML;

    invoke-direct {v0, p0, v3}, LX/ISA;-><init>(LX/ISI;LX/DML;)V

    sget-object v3, LX/IVo;->QUESTIONS:LX/IVo;

    invoke-virtual {p5, v0, v3}, LX/IVp;->a(Ljava/lang/Object;Ljava/lang/Enum;)LX/IVp;

    .line 2577658
    :cond_11
    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 2577659
    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2577660
    if-eqz v0, :cond_14

    move v0, v1

    :goto_d
    if-eqz v0, :cond_18

    .line 2577661
    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->m()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v3, v0, v2, p2}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 2577662
    if-eqz v0, :cond_16

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_e
    if-eqz v0, :cond_17

    move v0, v1

    :goto_f
    if-eqz v0, :cond_23

    .line 2577663
    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->m()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v3, v0, v2, p2}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_19

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_10
    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v3

    :goto_11
    :pswitch_0
    invoke-interface {v3}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_23

    invoke-interface {v3}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget p2, v0, LX/1vs;->b:I

    .line 2577664
    sget-object p3, LX/IS5;->a:[I

    const-class v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    sget-object p4, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    invoke-virtual {v4, p2, v1, v0, p4}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->ordinal()I

    move-result v0

    aget v0, p3, v0

    packed-switch v0, :pswitch_data_0

    .line 2577665
    iget-object v0, p0, LX/ISI;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v4, LX/ISI;->a:Ljava/lang/String;

    const-string p2, "unrecognized group content section type"

    invoke-virtual {v0, v4, p2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_11

    :cond_12
    move v0, v2

    .line 2577666
    goto/16 :goto_c

    :cond_13
    move v0, v2

    goto/16 :goto_c

    :cond_14
    move v0, v2

    .line 2577667
    goto :goto_d

    :cond_15
    move v0, v2

    goto :goto_d

    :cond_16
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_e

    :cond_17
    move v0, v2

    goto :goto_f

    :cond_18
    move v0, v2

    goto :goto_f

    .line 2577668
    :cond_19
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_10

    .line 2577669
    :pswitch_1
    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    if-eqz v0, :cond_1b

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->t()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForumChildGroupsFragmentModel$CommunityForumChildGroupsModel;

    move-result-object v0

    if-eqz v0, :cond_1b

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->t()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForumChildGroupsFragmentModel$CommunityForumChildGroupsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForumChildGroupsFragmentModel$CommunityForumChildGroupsModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1b

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->t()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForumChildGroupsFragmentModel$CommunityForumChildGroupsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForumChildGroupsFragmentModel$CommunityForumChildGroupsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1b

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->CITY:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    iget-object v4, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1a

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->COLLEGE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    iget-object v4, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 2577670
    :cond_1a
    new-instance v0, LX/IRz;

    sget-object v4, LX/IUI;->x:LX/DML;

    invoke-direct {v0, p0, v4}, LX/IRz;-><init>(LX/ISI;LX/DML;)V

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->COMMUNITY_FORUM_GROUPS:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    invoke-virtual {p5, v0, v4}, LX/IVp;->a(Ljava/lang/Object;Ljava/lang/Enum;)LX/IVp;

    .line 2577671
    :cond_1b
    goto/16 :goto_11

    .line 2577672
    :pswitch_2
    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    if-eqz v0, :cond_1c

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->z()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityViewerChildGroupsFragmentModel$ViewerChildGroupsModel;

    move-result-object v0

    if-eqz v0, :cond_1c

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->z()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityViewerChildGroupsFragmentModel$ViewerChildGroupsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityViewerChildGroupsFragmentModel$ViewerChildGroupsModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1c

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->z()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityViewerChildGroupsFragmentModel$ViewerChildGroupsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityViewerChildGroupsFragmentModel$ViewerChildGroupsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1c

    invoke-static {p0}, LX/ISI;->f(LX/ISI;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 2577673
    new-instance v0, LX/IRy;

    sget-object v4, LX/IUI;->z:LX/DML;

    invoke-direct {v0, p0, v4}, LX/IRy;-><init>(LX/ISI;LX/DML;)V

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->JOINED_CHILD_GROUPS:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    invoke-virtual {p5, v0, v4}, LX/IVp;->a(Ljava/lang/Object;Ljava/lang/Enum;)LX/IVp;

    .line 2577674
    :cond_1c
    goto/16 :goto_11

    .line 2577675
    :pswitch_3
    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    if-eqz v0, :cond_1d

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->x()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunitySuggestedGroupsFragmentModel$RelatedGroupsModel;

    move-result-object v0

    if-eqz v0, :cond_1d

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->x()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunitySuggestedGroupsFragmentModel$RelatedGroupsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunitySuggestedGroupsFragmentModel$RelatedGroupsModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1d

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->x()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunitySuggestedGroupsFragmentModel$RelatedGroupsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunitySuggestedGroupsFragmentModel$RelatedGroupsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1d

    invoke-static {p0}, LX/ISI;->f(LX/ISI;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 2577676
    new-instance v0, LX/ISG;

    sget-object v4, LX/IUI;->y:LX/DML;

    invoke-direct {v0, p0, v4}, LX/ISG;-><init>(LX/ISI;LX/DML;)V

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->SUGGESTED_CHILD_GROUPS:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    invoke-virtual {p5, v0, v4}, LX/IVp;->a(Ljava/lang/Object;Ljava/lang/Enum;)LX/IVp;

    .line 2577677
    :cond_1d
    goto/16 :goto_11

    .line 2577678
    :pswitch_4
    invoke-virtual {v4, p2, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 2577679
    iget-object v4, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v4

    if-eqz v4, :cond_39

    iget-object v4, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->w()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityTrendingStoriesFragmentModel$GroupTrendingStoriesModel;

    move-result-object v4

    if-eqz v4, :cond_39

    iget-object v4, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->w()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityTrendingStoriesFragmentModel$GroupTrendingStoriesModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityTrendingStoriesFragmentModel$GroupTrendingStoriesModel;->a()LX/0Px;

    move-result-object v4

    if-eqz v4, :cond_39

    iget-object v4, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->w()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityTrendingStoriesFragmentModel$GroupTrendingStoriesModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityTrendingStoriesFragmentModel$GroupTrendingStoriesModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_39

    const/4 v4, 0x1

    :goto_12
    move v4, v4

    .line 2577680
    if-eqz v4, :cond_1e

    invoke-static {p0}, LX/ISI;->f(LX/ISI;)Z

    move-result v4

    if-eqz v4, :cond_1e

    .line 2577681
    new-instance v4, LX/IS0;

    sget-object p2, LX/IUI;->D:LX/DML;

    invoke-direct {v4, p0, p2, v0}, LX/IS0;-><init>(LX/ISI;LX/DML;Ljava/lang/String;)V

    sget-object p2, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->TRENDING_STORIES:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    invoke-virtual {p5, v4, p2}, LX/IVp;->a(Ljava/lang/Object;Ljava/lang/Enum;)LX/IVp;

    .line 2577682
    :cond_1e
    goto/16 :goto_11

    .line 2577683
    :pswitch_5
    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    if-eqz v0, :cond_1f

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->s()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel;

    move-result-object v0

    if-eqz v0, :cond_1f

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->s()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1f

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->s()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityForSaleStoriesFragmentModel$CommunityForsaleStoriesModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    const/4 v4, 0x6

    if-lt v0, v4, :cond_1f

    invoke-static {p0}, LX/ISI;->f(LX/ISI;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 2577684
    new-instance v0, LX/ISD;

    sget-object v4, LX/IUI;->A:LX/DML;

    invoke-direct {v0, p0, v4}, LX/ISD;-><init>(LX/ISI;LX/DML;)V

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->FOR_SALE_ITEMS:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    invoke-virtual {p5, v0, v4}, LX/IVp;->a(Ljava/lang/Object;Ljava/lang/Enum;)LX/IVp;

    .line 2577685
    :cond_1f
    goto/16 :goto_11

    .line 2577686
    :pswitch_6
    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    if-eqz v0, :cond_20

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->v()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityEventsFragmentModel$GroupEventsModel;

    move-result-object v0

    if-eqz v0, :cond_20

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->v()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityEventsFragmentModel$GroupEventsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityEventsFragmentModel$GroupEventsModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_20

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->v()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityEventsFragmentModel$GroupEventsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityEventsFragmentModel$GroupEventsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_20

    invoke-static {p0}, LX/ISI;->f(LX/ISI;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 2577687
    new-instance v0, LX/ISF;

    sget-object v4, LX/IUI;->C:LX/DML;

    invoke-direct {v0, p0, v4}, LX/ISF;-><init>(LX/ISI;LX/DML;)V

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->EVENTS:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    invoke-virtual {p5, v0, v4}, LX/IVp;->a(Ljava/lang/Object;Ljava/lang/Enum;)LX/IVp;

    .line 2577688
    :cond_20
    goto/16 :goto_11

    .line 2577689
    :pswitch_7
    invoke-virtual {v4, p2, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 2577690
    iget-object v4, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v4

    if-eqz v4, :cond_21

    iget-object v4, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->y()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel;

    move-result-object v4

    if-eqz v4, :cond_21

    iget-object v4, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->y()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityInviteFriendsFragmentModel$SuggestedMembersModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_21

    iget-object v4, p0, LX/ISI;->g:LX/0Uh;

    const/16 p2, 0xe6

    const/4 p3, 0x0

    invoke-virtual {v4, p2, p3}, LX/0Uh;->a(IZ)Z

    move-result v4

    if-eqz v4, :cond_21

    invoke-static {p0}, LX/ISI;->f(LX/ISI;)Z

    move-result v4

    if-eqz v4, :cond_21

    .line 2577691
    new-instance v4, LX/IS2;

    sget-object p2, LX/IUI;->G:LX/DML;

    invoke-direct {v4, p0, p2, v0}, LX/IS2;-><init>(LX/ISI;LX/DML;Ljava/lang/String;)V

    sget-object p2, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->INVITE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    invoke-virtual {p5, v4, p2}, LX/IVp;->a(Ljava/lang/Object;Ljava/lang/Enum;)LX/IVp;

    .line 2577692
    :cond_21
    goto/16 :goto_11

    .line 2577693
    :pswitch_8
    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    if-eqz v0, :cond_22

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->u()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel;

    move-result-object v0

    if-eqz v0, :cond_22

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->u()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_22

    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->I()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityRelatedInformationModel;->u()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityLocalNewsFragmentModel$CommunityNewsCollectionModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_22

    .line 2577694
    new-instance v0, LX/ISB;

    sget-object v4, LX/IUI;->H:LX/DML;

    invoke-direct {v0, p0, v4}, LX/ISB;-><init>(LX/ISI;LX/DML;)V

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;->LOCAL_NEWS_STORIES:Lcom/facebook/graphql/enums/GraphQLGroupContentSectionType;

    invoke-virtual {p5, v0, v4}, LX/IVp;->a(Ljava/lang/Object;Ljava/lang/Enum;)LX/IVp;

    .line 2577695
    :cond_22
    goto/16 :goto_11

    .line 2577696
    :cond_23
    invoke-direct {p0}, LX/ISI;->j()Z

    move-result v0

    if-eqz v0, :cond_24

    .line 2577697
    new-instance v0, LX/DaN;

    sget-object v1, LX/IUI;->r:LX/DML;

    iget-object v2, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-direct {v0, v1, v2}, LX/DaN;-><init>(LX/DML;Ljava/lang/Object;)V

    sget-object v1, LX/IVo;->ARCHIVED:LX/IVo;

    invoke-virtual {p5, v0, v1}, LX/IVp;->a(Ljava/lang/Object;Ljava/lang/Enum;)LX/IVp;

    .line 2577698
    :cond_24
    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-static {v0}, LX/IQV;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 2577699
    new-instance v0, LX/DaP;

    sget-object v1, LX/IUI;->E:LX/DML;

    invoke-direct {v0, v1}, LX/DaP;-><init>(LX/DML;)V

    sget-object v1, LX/IVo;->RELIABLE_EXPECTATIONS:LX/IVo;

    invoke-virtual {p5, v0, v1}, LX/IVp;->a(Ljava/lang/Object;Ljava/lang/Enum;)LX/IVp;

    .line 2577700
    :cond_25
    iget-object v0, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-static {v0}, LX/DZD;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)LX/DZC;

    move-result-object v0

    invoke-static {v0}, LX/DJw;->c(LX/DZC;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_26

    .line 2577701
    new-instance v0, LX/DaN;

    sget-object v1, LX/IUI;->F:LX/DML;

    iget-object v2, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-direct {v0, v1, v2}, LX/DaN;-><init>(LX/DML;Ljava/lang/Object;)V

    sget-object v1, LX/IVo;->CLIENT_HEADER_COMPONENT:LX/IVo;

    invoke-virtual {p5, v0, v1}, LX/IVp;->a(Ljava/lang/Object;Ljava/lang/Enum;)LX/IVp;

    .line 2577702
    :cond_26
    invoke-static {p0}, LX/ISI;->g(LX/ISI;)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 2577703
    new-instance v0, LX/IS7;

    sget-object v1, LX/IUI;->o:LX/DML;

    invoke-direct {v0, p0, v1, p1}, LX/IS7;-><init>(LX/ISI;LX/DML;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V

    sget-object v1, LX/IVo;->EMPTY_COMMUNITY:LX/IVo;

    invoke-virtual {p5, v0, v1}, LX/IVp;->a(Ljava/lang/Object;Ljava/lang/Enum;)LX/IVp;

    .line 2577704
    :cond_27
    invoke-virtual {p5}, LX/IVp;->a()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/ISI;->l:LX/0Px;

    .line 2577705
    const v0, -0x6c426134

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto/16 :goto_0

    :cond_28
    iget-object v1, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v1, :cond_2b

    iget-object v1, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v1

    if-eqz v1, :cond_2b

    iget-object v1, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->x()Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;->CAN_POST_AFTER_APPROVAL:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    if-eq v1, v2, :cond_29

    iget-object v1, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->x()Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;->CAN_POST_WITHOUT_APPROVAL:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    if-eq v1, v2, :cond_29

    .line 2577706
    iget-object v1, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->J()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;

    move-result-object v1

    if-eqz v1, :cond_2c

    iget-object v1, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->J()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2c

    const/4 v1, 0x1

    :goto_13
    move v1, v1

    .line 2577707
    if-eqz v1, :cond_2b

    .line 2577708
    :cond_29
    const/4 v1, 0x0

    .line 2577709
    iget-object v2, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v2, :cond_2a

    iget-object v2, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->N()Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    move-result-object v2

    if-eqz v2, :cond_2a

    iget-object v2, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->N()Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;->a()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_2a

    iget-object v2, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->N()Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2a

    .line 2577710
    iget-object v2, p0, LX/ISI;->h:LX/DRd;

    iget-object v3, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->N()Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposeFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposeFragmentModel;->c()Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/DRd;->a(Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;)Z

    move-result v1

    .line 2577711
    :cond_2a
    move v1, v1

    .line 2577712
    if-eqz v1, :cond_2e

    iget-object v1, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-static {v1}, LX/DRd;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Z

    move-result v1

    if-eqz v1, :cond_2e

    const/4 v1, 0x1

    :goto_14
    move v1, v1

    .line 2577713
    if-eqz v1, :cond_2d

    iget-object v1, p0, LX/ISI;->o:LX/DRc;

    if-eqz v1, :cond_2d

    iget-object v1, p0, LX/ISI;->o:LX/DRc;

    invoke-virtual {v1}, LX/DRc;->c()Z

    move-result v1

    if-eqz v1, :cond_2d

    const/4 v1, 0x1

    :goto_15
    move v1, v1

    .line 2577714
    if-eqz v1, :cond_3

    :cond_2b
    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_2c
    const/4 v1, 0x0

    goto :goto_13

    :cond_2d
    const/4 v1, 0x0

    goto :goto_15

    :cond_2e
    const/4 v1, 0x0

    goto :goto_14

    .line 2577715
    :cond_2f
    new-instance v0, LX/IS8;

    sget-object v1, LX/IUI;->z:LX/DML;

    invoke-direct {v0, p0, v1}, LX/IS8;-><init>(LX/ISI;LX/DML;)V

    sget-object v1, LX/IVo;->CHILD_GROUPS:LX/IVo;

    invoke-virtual {p5, v0, v1}, LX/IVp;->a(Ljava/lang/Object;Ljava/lang/Enum;)LX/IVp;

    goto/16 :goto_3

    :cond_30
    const/4 v0, 0x0

    goto/16 :goto_4

    :cond_31
    const/4 v0, 0x0

    goto/16 :goto_5

    :cond_32
    const/4 v0, 0x0

    goto/16 :goto_6

    :cond_33
    const/4 v0, 0x0

    goto/16 :goto_8

    .line 2577716
    :cond_34
    invoke-static {v0}, LX/DJw;->k(LX/DZC;)LX/0Px;

    move-result-object v2

    .line 2577717
    if-eqz v2, :cond_b

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_b

    const/4 v1, 0x1

    goto/16 :goto_7

    :cond_35
    const/4 v0, 0x0

    goto/16 :goto_b

    :cond_36
    const/4 v0, 0x0

    goto/16 :goto_a

    .line 2577718
    :cond_37
    iget-object v2, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->S()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupRelatedInformationModel;

    move-result-object v2

    if-nez v2, :cond_38

    .line 2577719
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->COLLEGE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    iget-object v3, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 2577720
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CANNOT_JOIN_OR_REQUEST:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    iget-object v3, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    iget-object v3, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    move v0, v1

    goto/16 :goto_9

    .line 2577721
    :cond_38
    iget-object v2, p0, LX/ISI;->k:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->S()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupRelatedInformationModel;

    move-result-object v2

    .line 2577722
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->COLLEGE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupRelatedInformationModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 2577723
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupRelatedInformationModel;->hI_()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_e

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CANNOT_JOIN_OR_REQUEST:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupRelatedInformationModel;->hI_()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    move v0, v1

    goto/16 :goto_9

    :cond_39
    const/4 v4, 0x0

    goto/16 :goto_12

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;ZZZZ)V
    .locals 6

    .prologue
    .line 2577606
    new-instance v5, LX/IVp;

    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    invoke-direct {v5, v0}, LX/IVp;-><init>(LX/0Pz;)V

    move-object v0, p0

    move-object v1, p1

    move v2, p3

    move v3, p4

    move v4, p5

    .line 2577607
    invoke-virtual/range {v0 .. v5}, LX/ISI;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;ZZZLX/IVp;)V

    .line 2577608
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2577604
    iput-object p1, p0, LX/ISI;->s:Ljava/lang/String;

    .line 2577605
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2577603
    iget-object v0, p0, LX/ISI;->l:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2577602
    iget-object v0, p0, LX/ISI;->l:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2577601
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 2577600
    sget-object v1, LX/IUI;->I:LX/0Px;

    iget-object v0, p0, LX/ISI;->l:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DMB;

    invoke-interface {v0}, LX/DMB;->a()LX/DML;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2577599
    sget-object v0, LX/IUI;->I:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
