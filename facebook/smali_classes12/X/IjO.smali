.class public LX/IjO;
.super Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;
.source ""

# interfaces
.implements LX/6E6;


# instance fields
.field public a:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/resources/ui/FbTextView;

.field public c:Lcom/facebook/resources/ui/FbTextView;

.field public d:Lcom/facebook/resources/ui/FbTextView;

.field public e:Lcom/facebook/payments/p2p/model/PaymentCard;

.field public f:Z

.field public g:LX/6qh;

.field public h:Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2605696
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2605697
    const-class v0, LX/IjO;

    invoke-static {v0, p0}, LX/IjO;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2605698
    const v0, 0x7f030afd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2605699
    const v0, 0x7f0d1bc9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/IjO;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 2605700
    const v0, 0x7f0d1bca

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/IjO;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 2605701
    const v0, 0x7f0d1bcb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/IjO;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 2605702
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/IjO;->setOrientation(I)V

    .line 2605703
    invoke-virtual {p0}, LX/IjO;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const p1, 0x7f0a0670

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDivider(Landroid/graphics/drawable/Drawable;)V

    .line 2605704
    invoke-virtual {p0}, LX/IjO;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const p1, 0x7f0b1e8a

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDividerThickness(I)V

    .line 2605705
    invoke-static {p0}, LX/IjO;->e(LX/IjO;)V

    .line 2605706
    return-void
.end method

.method public static a(LX/IjO;Lcom/facebook/resources/ui/FbTextView;I)V
    .locals 4
    .param p1    # Lcom/facebook/resources/ui/FbTextView;
        .annotation build Landroid/support/annotation/ColorRes;
        .end annotation
    .end param

    .prologue
    .line 2605693
    invoke-virtual {p0}, LX/IjO;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 2605694
    iget-object v1, p0, LX/IjO;->a:LX/0wM;

    invoke-virtual {p1}, Lcom/facebook/resources/ui/FbTextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v2

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-virtual {v1, v2, v0}, LX/0wM;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    .line 2605695
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/IjO;

    invoke-static {p0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object p0

    check-cast p0, LX/0wM;

    iput-object p0, p1, LX/IjO;->a:LX/0wM;

    return-void
.end method

.method public static e(LX/IjO;)V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 2605689
    iget-object v0, p0, LX/IjO;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getVisibility()I

    move-result v0

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/IjO;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getVisibility()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 2605690
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setShowSegmentedDividers(I)V

    .line 2605691
    :goto_0
    return-void

    .line 2605692
    :cond_0
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setShowSegmentedDividers(I)V

    goto :goto_0
.end method


# virtual methods
.method public setVisibilityOfMakePrimaryButton(I)V
    .locals 1

    .prologue
    .line 2605685
    iget-object v0, p0, LX/IjO;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2605686
    iget-object v0, p0, LX/IjO;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2605687
    invoke-static {p0}, LX/IjO;->e(LX/IjO;)V

    .line 2605688
    return-void
.end method
