.class public final LX/ItS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/util/LinkedHashMap",
        "<",
        "Ljava/lang/String;",
        "Lcom/facebook/messaging/model/messages/Message;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:J

.field public final synthetic b:LX/ItV;


# direct methods
.method public constructor <init>(LX/ItV;J)V
    .locals 0

    .prologue
    .line 2623353
    iput-object p1, p0, LX/ItS;->b:LX/ItV;

    iput-wide p2, p0, LX/ItS;->a:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2623354
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 2623355
    iget-object v0, p0, LX/ItS;->b:LX/ItV;

    iget-object v0, v0, LX/ItV;->g:LX/2N4;

    const/4 v1, 0x0

    iget-object v2, p0, LX/ItS;->b:LX/ItV;

    iget-wide v2, v2, LX/ItV;->l:J

    const/16 v4, 0x15

    new-array v5, v6, [LX/2uW;

    sget-object v8, LX/2uW;->PENDING_SEND:LX/2uW;

    aput-object v8, v5, v7

    invoke-virtual/range {v0 .. v5}, LX/2N4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;JI[LX/2uW;)Ljava/util/LinkedHashMap;

    move-result-object v1

    .line 2623356
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 2623357
    :goto_0
    return-object v0

    .line 2623358
    :cond_1
    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->size()I

    move-result v0

    int-to-long v2, v0

    const-wide/16 v4, 0x14

    cmp-long v0, v2, v4

    if-lez v0, :cond_2

    .line 2623359
    iget-object v0, p0, LX/ItS;->b:LX/ItV;

    invoke-static {v0}, LX/ItV;->e(LX/ItV;)V

    .line 2623360
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    goto :goto_0

    .line 2623361
    :cond_2
    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 2623362
    iget-wide v4, v0, Lcom/facebook/messaging/model/messages/Message;->c:J

    iget-wide v8, p0, LX/ItS;->a:J

    cmp-long v0, v4, v8

    if-gez v0, :cond_3

    move v0, v6

    .line 2623363
    :goto_1
    if-eqz v0, :cond_4

    .line 2623364
    iget-object v0, p0, LX/ItS;->b:LX/ItV;

    invoke-static {v0, v1}, LX/ItV;->b(LX/ItV;Ljava/util/LinkedHashMap;)V

    .line 2623365
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    goto :goto_0

    :cond_4
    move-object v0, v1

    .line 2623366
    goto :goto_0

    :cond_5
    move v0, v7

    goto :goto_1
.end method
