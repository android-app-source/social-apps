.class public LX/Hvy;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/11R;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/11R;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2520084
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2520085
    iput-object p1, p0, LX/Hvy;->a:Landroid/content/Context;

    .line 2520086
    iput-object p2, p0, LX/Hvy;->b:LX/11R;

    .line 2520087
    return-void
.end method

.method public static b(LX/0QB;)LX/Hvy;
    .locals 3

    .prologue
    .line 2520082
    new-instance v2, LX/Hvy;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v1

    check-cast v1, LX/11R;

    invoke-direct {v2, v0, v1}, LX/Hvy;-><init>(Landroid/content/Context;LX/11R;)V

    .line 2520083
    return-object v2
.end method


# virtual methods
.method public final a(J)Ljava/lang/String;
    .locals 7

    .prologue
    .line 2520077
    const-wide/16 v0, 0x3e8

    mul-long v2, p1, v0

    .line 2520078
    iget-object v0, p0, LX/Hvy;->b:LX/11R;

    invoke-virtual {v0, v2, v3}, LX/11R;->a(J)J

    move-result-wide v0

    .line 2520079
    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-eqz v4, :cond_0

    const-wide/16 v4, 0x1

    cmp-long v0, v0, v4

    if-nez v0, :cond_1

    :cond_0
    const v0, 0x7f081450

    .line 2520080
    :goto_0
    iget-object v1, p0, LX/Hvy;->a:Landroid/content/Context;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p0, v2, v3}, LX/Hvy;->b(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {p0, v2, v3}, LX/Hvy;->c(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v5

    invoke-virtual {v1, v0, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2520081
    :cond_1
    const v0, 0x7f081451

    goto :goto_0
.end method

.method public final a(LX/5Rn;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 2520071
    sget-object v0, LX/Hvx;->a:[I

    invoke-virtual {p1}, LX/5Rn;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2520072
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown publish mode: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2520073
    :pswitch_0
    iget-object v0, p0, LX/Hvy;->a:Landroid/content/Context;

    const v1, 0x7f081456

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2520074
    :goto_0
    return-object v0

    .line 2520075
    :pswitch_1
    iget-object v0, p0, LX/Hvy;->a:Landroid/content/Context;

    const v1, 0x7f081457

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2520076
    :pswitch_2
    iget-object v0, p0, LX/Hvy;->a:Landroid/content/Context;

    const v1, 0x7f081458

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b(J)Ljava/lang/String;
    .locals 3

    .prologue
    .line 2520070
    iget-object v0, p0, LX/Hvy;->b:LX/11R;

    sget-object v1, LX/1lB;->EVENTS_RELATIVE_DATE_STYLE:LX/1lB;

    invoke-virtual {v0, v1, p1, p2}, LX/11R;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c(J)Ljava/lang/String;
    .locals 3

    .prologue
    .line 2520069
    iget-object v0, p0, LX/Hvy;->b:LX/11R;

    sget-object v1, LX/1lB;->HOUR_MINUTE_STYLE:LX/1lB;

    invoke-virtual {v0, v1, p1, p2}, LX/11R;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
