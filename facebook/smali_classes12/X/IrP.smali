.class public LX/IrP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private A:Z

.field public B:Z

.field public C:Z

.field public D:Z

.field public E:Z

.field public F:Ljava/lang/Integer;
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field public final G:LX/IrD;

.field private a:LX/Irs;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:LX/IqE;

.field public final c:LX/IqN;

.field private final d:LX/1zC;

.field private final e:LX/1Ad;

.field private final f:LX/2My;

.field private final g:LX/0wW;

.field public final h:LX/Is9;

.field public final i:Lcom/facebook/messaging/photos/editing/LayerGroupLayout;

.field public final j:Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public final k:Lcom/facebook/messaging/photos/editing/TextStylesLayout;

.field public final l:Landroid/view/ViewGroup;

.field public final m:Landroid/view/View;

.field public final n:LX/4ob;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4ob",
            "<",
            "LX/IqZ;",
            ">;"
        }
    .end annotation
.end field

.field public final o:LX/IqH;

.field public final p:LX/Iqq;

.field public q:LX/Ird;

.field public r:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

.field public s:LX/IqW;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public t:LX/Iqd;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private u:LX/IsC;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public v:LX/Ir4;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public w:LX/Iqe;

.field public x:F

.field public y:Ljava/lang/String;

.field public z:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/IrD;LX/Is9;Landroid/view/ViewGroup;Lcom/facebook/messaging/photos/editing/LayerGroupLayout;Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;Lcom/facebook/messaging/photos/editing/TextStylesLayout;Landroid/view/View;LX/4ob;LX/IqE;LX/IqN;LX/1zC;LX/1Ad;LX/2My;LX/0wW;LX/IqH;LX/Irs;)V
    .locals 3
    .param p1    # LX/IrD;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/Is9;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation

        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Landroid/view/ViewGroup;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/messaging/photos/editing/LayerGroupLayout;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation

        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Lcom/facebook/messaging/photos/editing/TextStylesLayout;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # LX/4ob;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation

        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/IrD;",
            "Lcom/facebook/messaging/photos/editing/StickerPickerFactory;",
            "Landroid/view/ViewGroup;",
            "Lcom/facebook/messaging/photos/editing/LayerGroupLayout;",
            "Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;",
            "Lcom/facebook/messaging/photos/editing/TextStylesLayout;",
            "Landroid/view/View;",
            "LX/4ob",
            "<",
            "LX/IqZ;",
            ">;",
            "LX/IqE;",
            "LX/IqN;",
            "LX/1zC;",
            "LX/1Ad;",
            "LX/2My;",
            "LX/0wW;",
            "LX/IqH;",
            "LX/Irs;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2617877
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2617878
    sget-object v1, LX/Iqe;->IDLE:LX/Iqe;

    iput-object v1, p0, LX/IrP;->w:LX/Iqe;

    .line 2617879
    iput-object p1, p0, LX/IrP;->G:LX/IrD;

    .line 2617880
    if-nez p2, :cond_0

    new-instance p2, LX/Is9;

    invoke-direct {p2}, LX/Is9;-><init>()V

    :cond_0
    iput-object p2, p0, LX/IrP;->h:LX/Is9;

    .line 2617881
    iput-object p3, p0, LX/IrP;->l:Landroid/view/ViewGroup;

    .line 2617882
    iput-object p4, p0, LX/IrP;->i:Lcom/facebook/messaging/photos/editing/LayerGroupLayout;

    .line 2617883
    iput-object p5, p0, LX/IrP;->j:Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;

    .line 2617884
    iput-object p6, p0, LX/IrP;->k:Lcom/facebook/messaging/photos/editing/TextStylesLayout;

    .line 2617885
    iput-object p7, p0, LX/IrP;->m:Landroid/view/View;

    .line 2617886
    iput-object p8, p0, LX/IrP;->n:LX/4ob;

    .line 2617887
    iput-object p9, p0, LX/IrP;->b:LX/IqE;

    .line 2617888
    iput-object p10, p0, LX/IrP;->c:LX/IqN;

    .line 2617889
    iput-object p11, p0, LX/IrP;->d:LX/1zC;

    .line 2617890
    iput-object p12, p0, LX/IrP;->e:LX/1Ad;

    .line 2617891
    move-object/from16 v0, p13

    iput-object v0, p0, LX/IrP;->f:LX/2My;

    .line 2617892
    move-object/from16 v0, p14

    iput-object v0, p0, LX/IrP;->g:LX/0wW;

    .line 2617893
    move-object/from16 v0, p15

    iput-object v0, p0, LX/IrP;->o:LX/IqH;

    .line 2617894
    move-object/from16 v0, p16

    iput-object v0, p0, LX/IrP;->a:LX/Irs;

    .line 2617895
    iget-object v1, p0, LX/IrP;->j:Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;

    if-eqz v1, :cond_1

    .line 2617896
    iget-object v1, p0, LX/IrP;->j:Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;

    iget-object v2, p0, LX/IrP;->G:LX/IrD;

    invoke-virtual {v1, v2}, Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;->a(LX/IrD;)V

    .line 2617897
    iget-object v1, p0, LX/IrP;->j:Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;

    new-instance v2, LX/IrF;

    invoke-direct {v2, p0}, LX/IrF;-><init>(LX/IrP;)V

    invoke-virtual {v1, v2}, Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;->setListener(LX/IrE;)V

    .line 2617898
    :cond_1
    iget-object v1, p0, LX/IrP;->o:LX/IqH;

    new-instance v2, LX/IrG;

    invoke-direct {v2, p0}, LX/IrG;-><init>(LX/IrP;)V

    invoke-virtual {v1, v2}, LX/IqH;->a(LX/IqF;)V

    .line 2617899
    new-instance v1, LX/IrH;

    invoke-direct {v1, p0}, LX/IrH;-><init>(LX/IrP;)V

    iput-object v1, p0, LX/IrP;->p:LX/Iqq;

    .line 2617900
    iget-object v1, p0, LX/IrP;->k:Lcom/facebook/messaging/photos/editing/TextStylesLayout;

    new-instance v2, LX/IrJ;

    invoke-direct {v2, p0}, LX/IrJ;-><init>(LX/IrP;)V

    invoke-virtual {v1, v2}, Lcom/facebook/messaging/photos/editing/TextStylesLayout;->setListener(LX/IrI;)V

    .line 2617901
    invoke-direct {p0}, LX/IrP;->s()V

    .line 2617902
    return-void
.end method

.method public static a(LX/IrP;LX/Iqe;Z)V
    .locals 8

    .prologue
    const/4 v3, 0x1

    .line 2617904
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2617905
    iget-object v0, p0, LX/IrP;->w:LX/Iqe;

    if-ne v0, p1, :cond_1

    .line 2617906
    :cond_0
    :goto_0
    return-void

    .line 2617907
    :cond_1
    iget-object v1, p0, LX/IrP;->w:LX/Iqe;

    .line 2617908
    iput-object p1, p0, LX/IrP;->w:LX/Iqe;

    .line 2617909
    sget-object v0, LX/Iqe;->STICKER_COLLAPSED:LX/Iqe;

    if-ne v1, v0, :cond_2

    .line 2617910
    iget-object v0, p0, LX/IrP;->s:LX/IqW;

    invoke-interface {v0}, LX/IqW;->a()Z

    .line 2617911
    :cond_2
    iget-object v0, p0, LX/IrP;->w:LX/Iqe;

    sget-object v2, LX/Iqe;->TEXT:LX/Iqe;

    if-ne v0, v2, :cond_c

    .line 2617912
    iget-object v0, p0, LX/IrP;->r:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    invoke-virtual {v0}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->c()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 2617913
    iget-object v0, p0, LX/IrP;->r:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    .line 2617914
    iget-object v2, v0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->B:LX/Iqk;

    .line 2617915
    iget-object v0, v2, LX/Iqk;->b:LX/Iqg;

    move-object v2, v0

    .line 2617916
    move-object v0, v2

    .line 2617917
    check-cast v0, LX/IsC;

    iput-object v0, p0, LX/IrP;->u:LX/IsC;

    .line 2617918
    :cond_3
    :goto_1
    iget-object v0, p0, LX/IrP;->w:LX/Iqe;

    sget-object v2, LX/Iqe;->TRANSFORMING:LX/Iqe;

    if-eq v0, v2, :cond_4

    iget-object v0, p0, LX/IrP;->w:LX/Iqe;

    sget-object v2, LX/Iqe;->TEXT:LX/Iqe;

    if-ne v0, v2, :cond_5

    .line 2617919
    :cond_4
    iput-boolean v3, p0, LX/IrP;->A:Z

    .line 2617920
    :cond_5
    const/4 p1, 0x2

    const/4 v5, 0x1

    const/16 v4, 0x8

    const/4 v2, 0x0

    .line 2617921
    iget-object v0, p0, LX/IrP;->w:LX/Iqe;

    sget-object v6, LX/Iqe;->TEXT:LX/Iqe;

    if-ne v0, v6, :cond_13

    .line 2617922
    iget-object v0, p0, LX/IrP;->k:Lcom/facebook/messaging/photos/editing/TextStylesLayout;

    invoke-virtual {v0, v2}, Lcom/facebook/messaging/photos/editing/TextStylesLayout;->setVisibility(I)V

    .line 2617923
    :goto_2
    invoke-static {p0}, LX/IrP;->n(LX/IrP;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, LX/IrP;->w:LX/Iqe;

    new-array v6, p1, [LX/Iqe;

    sget-object v7, LX/Iqe;->DOODLE:LX/Iqe;

    aput-object v7, v6, v2

    sget-object v7, LX/Iqe;->DOODLING:LX/Iqe;

    aput-object v7, v6, v5

    invoke-virtual {v0, v6}, LX/Iqe;->isOneOf([LX/Iqe;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 2617924
    iget-object v0, p0, LX/IrP;->t:LX/Iqd;

    invoke-virtual {v0}, LX/Iqd;->h()V

    .line 2617925
    :cond_6
    invoke-static {p0}, LX/IrP;->o(LX/IrP;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, LX/IrP;->w:LX/Iqe;

    new-array v6, p1, [LX/Iqe;

    sget-object v7, LX/Iqe;->STICKER:LX/Iqe;

    aput-object v7, v6, v2

    sget-object v7, LX/Iqe;->STICKER_COLLAPSED:LX/Iqe;

    aput-object v7, v6, v5

    invoke-virtual {v0, v6}, LX/Iqe;->isOneOf([LX/Iqe;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 2617926
    iget-object v0, p0, LX/IrP;->s:LX/IqW;

    invoke-interface {v0}, LX/IqW;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2617927
    :cond_7
    iget-object v0, p0, LX/IrP;->j:Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;

    if-eqz v0, :cond_9

    .line 2617928
    iget-object v6, p0, LX/IrP;->j:Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;

    .line 2617929
    iget-object v0, p0, LX/IrP;->w:LX/Iqe;

    sget-object v7, LX/Iqe;->DISABLED:LX/Iqe;

    if-eq v0, v7, :cond_17

    iget-object v0, p0, LX/IrP;->w:LX/Iqe;

    invoke-static {v0}, LX/Iqe;->isUserInteracting(LX/Iqe;)Z

    move-result v0

    if-nez v0, :cond_17

    invoke-static {p0}, LX/IrP;->m(LX/IrP;)Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, LX/IrP;->G:LX/IrD;

    invoke-virtual {v0}, LX/IrD;->a()Z

    move-result v0

    if-eqz v0, :cond_17

    :cond_8
    const/4 v0, 0x1

    :goto_3
    move v0, v0

    .line 2617930
    if-eqz v0, :cond_14

    move v0, v2

    :goto_4
    invoke-virtual {v6, v0}, Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;->setVisibility(I)V

    .line 2617931
    :cond_9
    iget-object v0, p0, LX/IrP;->w:LX/Iqe;

    sget-object v6, LX/Iqe;->TRANSFORMING:LX/Iqe;

    if-ne v0, v6, :cond_15

    move v0, v5

    .line 2617932
    :goto_5
    iget-object v5, p0, LX/IrP;->m:Landroid/view/View;

    if-eqz v0, :cond_16

    :goto_6
    invoke-virtual {v5, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2617933
    if-eqz p2, :cond_0

    iget-object v0, p0, LX/IrP;->v:LX/Ir4;

    if-eqz v0, :cond_0

    .line 2617934
    iget-object v0, p0, LX/IrP;->v:LX/Ir4;

    .line 2617935
    iget-object v2, v0, LX/Ir4;->a:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    const/16 v5, 0x8

    const/4 v6, 0x0

    .line 2617936
    iget-object v4, v2, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->D:LX/IrP;

    if-nez v4, :cond_18

    .line 2617937
    :goto_7
    iget-object v0, p0, LX/IrP;->w:LX/Iqe;

    sget-object v2, LX/Iqe;->IDLE:LX/Iqe;

    if-ne v0, v2, :cond_0

    sget-object v0, LX/Iqe;->TEXT:LX/Iqe;

    if-ne v1, v0, :cond_0

    iget-object v0, p0, LX/IrP;->q:LX/Ird;

    invoke-virtual {v0}, LX/Ird;->h()I

    move-result v0

    if-ne v0, v3, :cond_0

    iget-object v0, p0, LX/IrP;->f:LX/2My;

    const/4 v5, 0x0

    .line 2617938
    iget-object v4, v0, LX/2My;->d:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0W3;

    sget-wide v6, LX/0X5;->fW:J

    invoke-interface {v4, v6, v7}, LX/0W4;->b(J)Z

    move-result v4

    if-nez v4, :cond_a

    iget-object v4, v0, LX/2My;->a:LX/0Uh;

    const/16 v6, 0x14a

    invoke-virtual {v4, v6, v5}, LX/0Uh;->a(IZ)Z

    move-result v4

    if-eqz v4, :cond_1c

    :cond_a
    const/4 v4, 0x1

    :goto_8
    move v0, v4

    .line 2617939
    if-eqz v0, :cond_0

    .line 2617940
    iget-object v0, p0, LX/IrP;->u:LX/IsC;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2617941
    iget-object v0, p0, LX/IrP;->u:LX/IsC;

    .line 2617942
    iget-boolean v1, v0, LX/Iqg;->k:Z

    move v0, v1

    .line 2617943
    if-nez v0, :cond_0

    iget-object v0, p0, LX/IrP;->u:LX/IsC;

    .line 2617944
    iget-object v1, v0, LX/IsC;->g:Ljava/lang/CharSequence;

    move-object v0, v1

    .line 2617945
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2617946
    sget-object v0, LX/Iqe;->STICKER_COLLAPSED:LX/Iqe;

    invoke-static {p0, v0}, LX/IrP;->b(LX/IrP;LX/Iqe;)V

    goto/16 :goto_0

    .line 2617947
    :cond_b
    new-instance v0, LX/IsC;

    invoke-direct {v0}, LX/IsC;-><init>()V

    .line 2617948
    iget-object v2, p0, LX/IrP;->q:LX/Ird;

    invoke-virtual {v2, v0}, LX/Ird;->a(LX/Iqg;)V

    .line 2617949
    iput-object v0, p0, LX/IrP;->u:LX/IsC;

    goto/16 :goto_1

    .line 2617950
    :cond_c
    iget-object v0, p0, LX/IrP;->w:LX/Iqe;

    sget-object v2, LX/Iqe;->STICKER:LX/Iqe;

    if-eq v0, v2, :cond_d

    iget-object v0, p0, LX/IrP;->w:LX/Iqe;

    sget-object v2, LX/Iqe;->STICKER_COLLAPSED:LX/Iqe;

    if-ne v0, v2, :cond_f

    .line 2617951
    :cond_d
    iget-object v0, p0, LX/IrP;->l:Landroid/view/ViewGroup;

    .line 2617952
    iget-object v2, p0, LX/IrP;->s:LX/IqW;

    if-nez v2, :cond_e

    .line 2617953
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v5

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getHeight()I

    move-result v6

    .line 2617954
    new-instance v7, LX/IqX;

    invoke-direct {v7, v4, v5, v6}, LX/IqX;-><init>(Landroid/content/Context;II)V

    move-object v2, v7

    .line 2617955
    iput-object v2, p0, LX/IrP;->s:LX/IqW;

    .line 2617956
    iget-object v2, p0, LX/IrP;->s:LX/IqW;

    invoke-interface {v2}, LX/IqW;->getView()Landroid/view/View;

    move-result-object v2

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2617957
    iget-object v2, p0, LX/IrP;->s:LX/IqW;

    invoke-interface {v2}, LX/IqW;->getView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2617958
    :cond_e
    iget-object v0, p0, LX/IrP;->s:LX/IqW;

    iget v2, p0, LX/IrP;->x:F

    invoke-interface {v0, v2}, LX/IqW;->setGlobalRotation(F)V

    .line 2617959
    iget-object v0, p0, LX/IrP;->s:LX/IqW;

    new-instance v2, LX/IrK;

    invoke-direct {v2, p0}, LX/IrK;-><init>(LX/IrP;)V

    invoke-interface {v0, v2}, LX/IqW;->setOnStickerClickListener(LX/IrK;)V

    .line 2617960
    iget-object v0, p0, LX/IrP;->s:LX/IqW;

    invoke-interface {v0}, LX/IqW;->getView()Landroid/view/View;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2617961
    goto/16 :goto_1

    .line 2617962
    :cond_f
    iget-object v0, p0, LX/IrP;->w:LX/Iqe;

    sget-object v2, LX/Iqe;->DOODLE:LX/Iqe;

    if-ne v0, v2, :cond_3

    sget-object v0, LX/Iqe;->DOODLING:LX/Iqe;

    if-eq v1, v0, :cond_3

    .line 2617963
    iget-object v0, p0, LX/IrP;->l:Landroid/view/ViewGroup;

    .line 2617964
    iget-object v2, p0, LX/IrP;->t:LX/Iqd;

    if-nez v2, :cond_10

    .line 2617965
    new-instance v2, LX/Iqd;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v2, v4}, LX/Iqd;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, LX/IrP;->t:LX/Iqd;

    .line 2617966
    iget-object v2, p0, LX/IrP;->n:LX/4ob;

    if-nez v2, :cond_1d

    .line 2617967
    new-instance v2, LX/Irv;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v2, v4}, LX/Irv;-><init>(Landroid/content/Context;)V

    .line 2617968
    iget-object v4, p0, LX/IrP;->t:LX/Iqd;

    invoke-virtual {v4, v2}, LX/Iqd;->addView(Landroid/view/View;)V

    .line 2617969
    :goto_9
    iget-object v4, p0, LX/IrP;->t:LX/Iqd;

    .line 2617970
    iget-object v5, v4, LX/Iqd;->a:LX/IqZ;

    if-nez v5, :cond_1e

    const/4 v5, 0x1

    :goto_a
    invoke-static {v5}, LX/0PB;->checkState(Z)V

    .line 2617971
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/IqZ;

    iput-object v5, v4, LX/Iqd;->a:LX/IqZ;

    .line 2617972
    iget-object v5, v4, LX/Iqd;->a:LX/IqZ;

    new-instance v6, LX/Iqa;

    invoke-direct {v6, v4}, LX/Iqa;-><init>(LX/Iqd;)V

    .line 2617973
    iput-object v6, v5, LX/IqZ;->a:LX/Iqa;

    .line 2617974
    iget-object v2, p0, LX/IrP;->t:LX/Iqd;

    new-instance v4, LX/IrN;

    invoke-direct {v4, p0}, LX/IrN;-><init>(LX/IrP;)V

    .line 2617975
    iput-object v4, v2, LX/Iqd;->c:LX/IrN;

    .line 2617976
    iget-object v2, p0, LX/IrP;->t:LX/Iqd;

    .line 2617977
    iget-object v4, p0, LX/IrP;->i:Lcom/facebook/messaging/photos/editing/LayerGroupLayout;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v4

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    move v4, v4

    .line 2617978
    invoke-virtual {v0, v2, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 2617979
    :cond_10
    iget-object v0, p0, LX/IrP;->t:LX/Iqd;

    const/4 v4, 0x1

    .line 2617980
    invoke-static {v0}, LX/Iqd;->i(LX/Iqd;)V

    .line 2617981
    iget-object v2, v0, LX/Iqd;->b:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v2, v4}, Lcom/facebook/drawingview/DrawingView;->setEnabled(Z)V

    .line 2617982
    iget-object v2, v0, LX/Iqd;->a:LX/IqZ;

    if-eqz v2, :cond_11

    .line 2617983
    iget-object v2, v0, LX/Iqd;->a:LX/IqZ;

    invoke-virtual {v2}, LX/IqZ;->a()V

    .line 2617984
    :cond_11
    iget-object v2, v0, LX/Iqd;->c:LX/IrN;

    if-eqz v2, :cond_12

    invoke-virtual {v0}, LX/Iqd;->e()Z

    move-result v2

    if-eqz v2, :cond_12

    .line 2617985
    iget-object v2, v0, LX/Iqd;->c:LX/IrN;

    invoke-virtual {v2, v4}, LX/IrN;->a(Z)V

    .line 2617986
    iget-object v2, v0, LX/Iqd;->c:LX/IrN;

    iget-object v4, v0, LX/Iqd;->b:Lcom/facebook/drawingview/DrawingView;

    .line 2617987
    iget v0, v4, Lcom/facebook/drawingview/DrawingView;->l:I

    move v4, v0

    .line 2617988
    invoke-virtual {v2, v4}, LX/IrN;->a(I)V

    .line 2617989
    :cond_12
    goto/16 :goto_1

    .line 2617990
    :cond_13
    iget-object v0, p0, LX/IrP;->k:Lcom/facebook/messaging/photos/editing/TextStylesLayout;

    invoke-virtual {v0, v4}, Lcom/facebook/messaging/photos/editing/TextStylesLayout;->setVisibility(I)V

    .line 2617991
    iget-object v0, p0, LX/IrP;->r:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    invoke-virtual {v0}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->b()V

    goto/16 :goto_2

    :cond_14
    move v0, v4

    .line 2617992
    goto/16 :goto_4

    :cond_15
    move v0, v2

    .line 2617993
    goto/16 :goto_5

    :cond_16
    move v2, v4

    .line 2617994
    goto/16 :goto_6

    :cond_17
    const/4 v0, 0x0

    goto/16 :goto_3

    .line 2617995
    :cond_18
    iget-object v4, v2, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->D:LX/IrP;

    .line 2617996
    iget-object v7, v4, LX/IrP;->w:LX/Iqe;

    invoke-static {v7}, LX/Iqe;->isUserInteracting(LX/Iqe;)Z

    move-result v7

    move v7, v7

    .line 2617997
    iget-object v0, v2, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->K:Lcom/facebook/fbui/glyph/GlyphView;

    if-eqz v7, :cond_19

    move v4, v5

    :goto_b
    invoke-virtual {v0, v4}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2617998
    iget-object v0, v2, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->I:Lcom/facebook/fbui/glyph/GlyphView;

    if-eqz v7, :cond_1a

    move v4, v5

    :goto_c
    invoke-virtual {v0, v4}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2617999
    iget-object v4, v2, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->L:Landroid/view/View;

    if-eqz v7, :cond_1b

    :goto_d
    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_7

    :cond_19
    move v4, v6

    .line 2618000
    goto :goto_b

    :cond_1a
    move v4, v6

    .line 2618001
    goto :goto_c

    :cond_1b
    move v5, v6

    .line 2618002
    goto :goto_d

    :cond_1c
    move v4, v5

    goto/16 :goto_8

    .line 2618003
    :cond_1d
    iget-object v2, p0, LX/IrP;->n:LX/4ob;

    invoke-virtual {v2}, LX/4ob;->a()Landroid/view/View;

    move-result-object v2

    check-cast v2, LX/IqZ;

    goto/16 :goto_9

    .line 2618004
    :cond_1e
    const/4 v5, 0x0

    goto/16 :goto_a
.end method

.method public static a(LX/IrP;LX/Irs;)V
    .locals 0

    .prologue
    .line 2617903
    iput-object p1, p0, LX/IrP;->a:LX/Irs;

    return-void
.end method

.method public static b(LX/IrP;LX/Iqe;)V
    .locals 1

    .prologue
    .line 2617874
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, LX/IrP;->a(LX/IrP;LX/Iqe;Z)V

    .line 2617875
    return-void
.end method

.method public static m(LX/IrP;)Z
    .locals 1

    .prologue
    .line 2617876
    iget-object v0, p0, LX/IrP;->t:LX/Iqd;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/IrP;->t:LX/Iqd;

    invoke-virtual {v0}, LX/Iqd;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static n(LX/IrP;)Z
    .locals 1

    .prologue
    .line 2617873
    iget-object v0, p0, LX/IrP;->t:LX/Iqd;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/IrP;->t:LX/Iqd;

    invoke-virtual {v0}, LX/Iqd;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static o(LX/IrP;)Z
    .locals 1

    .prologue
    .line 2617872
    iget-object v0, p0, LX/IrP;->s:LX/IqW;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/IrP;->s:LX/IqW;

    invoke-interface {v0}, LX/IqW;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private s()V
    .locals 3

    .prologue
    .line 2617864
    new-instance v0, LX/Ird;

    invoke-direct {v0}, LX/Ird;-><init>()V

    iput-object v0, p0, LX/IrP;->q:LX/Ird;

    .line 2617865
    iget-object v0, p0, LX/IrP;->a:LX/Irs;

    iget-object v1, p0, LX/IrP;->i:Lcom/facebook/messaging/photos/editing/LayerGroupLayout;

    iget-object v2, p0, LX/IrP;->q:LX/Ird;

    invoke-virtual {v0, v1, v2}, LX/Irs;->a(Lcom/facebook/messaging/photos/editing/LayerGroupLayout;LX/Ird;)Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    move-result-object v0

    iput-object v0, p0, LX/IrP;->r:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    .line 2617866
    iget-object v0, p0, LX/IrP;->r:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    new-instance v1, LX/IrL;

    invoke-direct {v1, p0}, LX/IrL;-><init>(LX/IrP;)V

    .line 2617867
    iput-object v1, v0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->n:LX/IrL;

    .line 2617868
    iget-object v0, p0, LX/IrP;->r:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    new-instance v1, LX/IrM;

    invoke-direct {v1, p0}, LX/IrM;-><init>(LX/IrP;)V

    .line 2617869
    iput-object v1, v0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->o:LX/IrM;

    .line 2617870
    iget-object v0, p0, LX/IrP;->r:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    invoke-virtual {v0}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->a()V

    .line 2617871
    return-void
.end method


# virtual methods
.method public final b()Z
    .locals 1

    .prologue
    .line 2617863
    iget-object v0, p0, LX/IrP;->q:LX/Ird;

    invoke-virtual {v0}, LX/Ird;->h()I

    move-result v0

    if-gtz v0, :cond_0

    invoke-static {p0}, LX/IrP;->m(LX/IrP;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()V
    .locals 5

    .prologue
    .line 2617823
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2617824
    iget-object v0, p0, LX/IrP;->q:LX/Ird;

    .line 2617825
    iget v3, v0, LX/Ird;->d:I

    move v0, v3

    .line 2617826
    if-lez v0, :cond_4

    move v0, v1

    :goto_0
    iput-boolean v0, p0, LX/IrP;->B:Z

    .line 2617827
    iget-object v0, p0, LX/IrP;->q:LX/Ird;

    .line 2617828
    iget v3, v0, LX/Ird;->e:I

    move v0, v3

    .line 2617829
    if-lez v0, :cond_5

    move v0, v1

    :goto_1
    iput-boolean v0, p0, LX/IrP;->C:Z

    .line 2617830
    iget-object v0, p0, LX/IrP;->q:LX/Ird;

    .line 2617831
    iget v3, v0, LX/Ird;->f:I

    move v0, v3

    .line 2617832
    if-lez v0, :cond_6

    :goto_2
    iput-boolean v1, p0, LX/IrP;->D:Z

    .line 2617833
    invoke-static {p0}, LX/IrP;->m(LX/IrP;)Z

    move-result v0

    iput-boolean v0, p0, LX/IrP;->E:Z

    .line 2617834
    iget-object v0, p0, LX/IrP;->q:LX/Ird;

    .line 2617835
    iget-object v1, v0, LX/Ird;->g:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2617836
    iput-object v0, p0, LX/IrP;->y:Ljava/lang/String;

    .line 2617837
    iget-object v0, p0, LX/IrP;->q:LX/Ird;

    .line 2617838
    iget-object v1, v0, LX/Ird;->h:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2617839
    iput-object v0, p0, LX/IrP;->z:Ljava/lang/String;

    .line 2617840
    iget-object v0, p0, LX/IrP;->i:Lcom/facebook/messaging/photos/editing/LayerGroupLayout;

    invoke-virtual {v0}, Lcom/facebook/messaging/photos/editing/LayerGroupLayout;->getChildCount()I

    move-result v1

    .line 2617841
    const/4 v0, 0x0

    :goto_3
    if-ge v0, v1, :cond_0

    .line 2617842
    iget-object v2, p0, LX/IrP;->q:LX/Ird;

    iget-object v3, p0, LX/IrP;->q:LX/Ird;

    .line 2617843
    iget-object v4, v3, LX/Ird;->c:LX/Iqg;

    move-object v3, v4

    .line 2617844
    invoke-virtual {v2, v3}, LX/Ird;->c(LX/Iqg;)V

    .line 2617845
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 2617846
    :cond_0
    sget-object v0, LX/Iqe;->IDLE:LX/Iqe;

    .line 2617847
    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/IrP;->a(LX/IrP;LX/Iqe;Z)V

    .line 2617848
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/IrP;->A:Z

    .line 2617849
    iget-object v0, p0, LX/IrP;->t:LX/Iqd;

    if-eqz v0, :cond_3

    .line 2617850
    iget-object v0, p0, LX/IrP;->t:LX/Iqd;

    .line 2617851
    iget-object v1, v0, LX/Iqd;->b:Lcom/facebook/drawingview/DrawingView;

    move-object v0, v1

    .line 2617852
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/drawingview/DrawingView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    iget-object v2, p0, LX/IrP;->i:Lcom/facebook/messaging/photos/editing/LayerGroupLayout;

    if-ne v1, v2, :cond_1

    .line 2617853
    iget-object v1, p0, LX/IrP;->i:Lcom/facebook/messaging/photos/editing/LayerGroupLayout;

    invoke-virtual {v1, v0}, Lcom/facebook/messaging/photos/editing/LayerGroupLayout;->removeView(Landroid/view/View;)V

    .line 2617854
    iget-object v1, p0, LX/IrP;->l:Landroid/view/ViewGroup;

    iget-object v2, p0, LX/IrP;->l:Landroid/view/ViewGroup;

    iget-object v3, p0, LX/IrP;->t:LX/Iqd;

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 2617855
    :cond_1
    iget-object v0, p0, LX/IrP;->t:LX/Iqd;

    .line 2617856
    iget-object v1, v0, LX/Iqd;->b:Lcom/facebook/drawingview/DrawingView;

    if-eqz v1, :cond_2

    .line 2617857
    iget-object v1, v0, LX/Iqd;->b:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v1}, Lcom/facebook/drawingview/DrawingView;->a()V

    .line 2617858
    :cond_2
    invoke-virtual {v0}, LX/Iqd;->h()V

    .line 2617859
    :cond_3
    return-void

    :cond_4
    move v0, v2

    .line 2617860
    goto/16 :goto_0

    :cond_5
    move v0, v2

    .line 2617861
    goto :goto_1

    :cond_6
    move v1, v2

    .line 2617862
    goto :goto_2
.end method
