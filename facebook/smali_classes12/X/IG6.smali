.class public final enum LX/IG6;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IG6;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IG6;

.field public static final enum DASHBOARD_FETCH_DATA:LX/IG6;

.field public static final enum DASHBOARD_FETCH_DATA1:LX/IG6;

.field public static final enum DASHBOARD_FETCH_DATA2:LX/IG6;

.field public static final enum DASHBOARD_INIT:LX/IG6;

.field public static final enum DASHBOARD_INIT_LOCATION:LX/IG6;

.field public static final enum DASHBOARD_REFRESH_LOCATION:LX/IG6;

.field public static final enum DASHBOARD_TTI:LX/IG6;

.field public static final enum DASHBOARD_VIEW_RENDER:LX/IG6;


# instance fields
.field public final markerId:I

.field public final markerName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2555277
    new-instance v0, LX/IG6;

    const-string v1, "DASHBOARD_INIT_LOCATION"

    const v2, 0x300001

    const-string v3, "FriendsNearbyDashboardInitLocation"

    invoke-direct {v0, v1, v5, v2, v3}, LX/IG6;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/IG6;->DASHBOARD_INIT_LOCATION:LX/IG6;

    .line 2555278
    new-instance v0, LX/IG6;

    const-string v1, "DASHBOARD_REFRESH_LOCATION"

    const v2, 0x300002

    const-string v3, "FriendsNearbyDashboardRefreshLocation"

    invoke-direct {v0, v1, v6, v2, v3}, LX/IG6;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/IG6;->DASHBOARD_REFRESH_LOCATION:LX/IG6;

    .line 2555279
    new-instance v0, LX/IG6;

    const-string v1, "DASHBOARD_FETCH_DATA"

    const v2, 0x300003

    const-string v3, "FriendsNearbyDashboardFetchData"

    invoke-direct {v0, v1, v7, v2, v3}, LX/IG6;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/IG6;->DASHBOARD_FETCH_DATA:LX/IG6;

    .line 2555280
    new-instance v0, LX/IG6;

    const-string v1, "DASHBOARD_FETCH_DATA1"

    const v2, 0x300004

    const-string v3, "FriendsNearbyDashboardFetchData1"

    invoke-direct {v0, v1, v8, v2, v3}, LX/IG6;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/IG6;->DASHBOARD_FETCH_DATA1:LX/IG6;

    .line 2555281
    new-instance v0, LX/IG6;

    const-string v1, "DASHBOARD_FETCH_DATA2"

    const v2, 0x300005

    const-string v3, "FriendsNearbyDashboardFetchData2"

    invoke-direct {v0, v1, v9, v2, v3}, LX/IG6;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/IG6;->DASHBOARD_FETCH_DATA2:LX/IG6;

    .line 2555282
    new-instance v0, LX/IG6;

    const-string v1, "DASHBOARD_VIEW_RENDER"

    const/4 v2, 0x5

    const v3, 0x300006

    const-string v4, "FriendsNearbyDashboardViewRender"

    invoke-direct {v0, v1, v2, v3, v4}, LX/IG6;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/IG6;->DASHBOARD_VIEW_RENDER:LX/IG6;

    .line 2555283
    new-instance v0, LX/IG6;

    const-string v1, "DASHBOARD_INIT"

    const/4 v2, 0x6

    const v3, 0x300007

    const-string v4, "FriendsNearbyDashboardInit"

    invoke-direct {v0, v1, v2, v3, v4}, LX/IG6;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/IG6;->DASHBOARD_INIT:LX/IG6;

    .line 2555284
    new-instance v0, LX/IG6;

    const-string v1, "DASHBOARD_TTI"

    const/4 v2, 0x7

    const v3, 0x300008

    const-string v4, "FriendsNearbyDashboardTTI"

    invoke-direct {v0, v1, v2, v3, v4}, LX/IG6;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/IG6;->DASHBOARD_TTI:LX/IG6;

    .line 2555285
    const/16 v0, 0x8

    new-array v0, v0, [LX/IG6;

    sget-object v1, LX/IG6;->DASHBOARD_INIT_LOCATION:LX/IG6;

    aput-object v1, v0, v5

    sget-object v1, LX/IG6;->DASHBOARD_REFRESH_LOCATION:LX/IG6;

    aput-object v1, v0, v6

    sget-object v1, LX/IG6;->DASHBOARD_FETCH_DATA:LX/IG6;

    aput-object v1, v0, v7

    sget-object v1, LX/IG6;->DASHBOARD_FETCH_DATA1:LX/IG6;

    aput-object v1, v0, v8

    sget-object v1, LX/IG6;->DASHBOARD_FETCH_DATA2:LX/IG6;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, LX/IG6;->DASHBOARD_VIEW_RENDER:LX/IG6;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/IG6;->DASHBOARD_INIT:LX/IG6;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/IG6;->DASHBOARD_TTI:LX/IG6;

    aput-object v2, v0, v1

    sput-object v0, LX/IG6;->$VALUES:[LX/IG6;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2555286
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2555287
    iput p3, p0, LX/IG6;->markerId:I

    .line 2555288
    iput-object p4, p0, LX/IG6;->markerName:Ljava/lang/String;

    .line 2555289
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IG6;
    .locals 1

    .prologue
    .line 2555290
    const-class v0, LX/IG6;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IG6;

    return-object v0
.end method

.method public static values()[LX/IG6;
    .locals 1

    .prologue
    .line 2555291
    sget-object v0, LX/IG6;->$VALUES:[LX/IG6;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IG6;

    return-object v0
.end method
