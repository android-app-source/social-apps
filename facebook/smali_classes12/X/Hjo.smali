.class public LX/Hjo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/HjW;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final A:Ljava/lang/String;

.field private final B:LX/Hj9;

.field private final C:Ljava/lang/String;

.field public final D:Ljava/lang/String;

.field public final E:LX/Hjn;

.field public final F:LX/Hk3;

.field private final G:LX/HjI;

.field public final H:Ljava/lang/String;

.field public I:Z

.field public J:Z

.field private K:Z

.field public L:J

.field public M:LX/Hkf;

.field public final b:Landroid/net/Uri;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public final h:LX/Hj9;

.field public final i:LX/Hj9;

.field private final j:LX/HjB;

.field public final k:Ljava/lang/String;

.field public final l:Ljava/lang/String;

.field public final m:Ljava/lang/String;

.field private final n:Ljava/lang/String;

.field private final o:LX/Hkk;

.field private final p:Ljava/lang/String;

.field private final q:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final r:Z

.field public final s:Z

.field public final t:Z

.field public final u:I

.field public final v:I

.field public final w:I

.field public final x:I

.field public final y:Ljava/lang/String;

.field public final z:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    const-class v0, LX/Hjo;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Hjo;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Hj9;LX/Hj9;LX/HjB;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Hkk;Ljava/lang/String;Ljava/util/Collection;ZLX/Hj9;Ljava/lang/String;Ljava/lang/String;LX/Hjn;Ljava/lang/String;ZZIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IILX/Hk3;LX/HjI;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/Hj9;",
            "LX/Hj9;",
            "LX/HjB;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/Hkk;",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;Z",
            "LX/Hj9;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/Hjn;",
            "Ljava/lang/String;",
            "ZZII",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "II",
            "LX/Hk3;",
            "LX/HjI;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v2, 0x0

    iput-wide v2, p0, LX/Hjo;->L:J

    const/4 v2, 0x0

    iput-object v2, p0, LX/Hjo;->M:LX/Hkf;

    iput-object p1, p0, LX/Hjo;->b:Landroid/net/Uri;

    iput-object p2, p0, LX/Hjo;->c:Ljava/lang/String;

    iput-object p3, p0, LX/Hjo;->d:Ljava/lang/String;

    iput-object p4, p0, LX/Hjo;->e:Ljava/lang/String;

    iput-object p5, p0, LX/Hjo;->f:Ljava/lang/String;

    iput-object p6, p0, LX/Hjo;->g:Ljava/lang/String;

    iput-object p7, p0, LX/Hjo;->h:LX/Hj9;

    iput-object p8, p0, LX/Hjo;->i:LX/Hj9;

    iput-object p9, p0, LX/Hjo;->j:LX/HjB;

    iput-object p10, p0, LX/Hjo;->k:Ljava/lang/String;

    iput-object p11, p0, LX/Hjo;->m:Ljava/lang/String;

    move-object/from16 v0, p12

    iput-object v0, p0, LX/Hjo;->n:Ljava/lang/String;

    move-object/from16 v0, p13

    iput-object v0, p0, LX/Hjo;->o:LX/Hkk;

    move-object/from16 v0, p14

    iput-object v0, p0, LX/Hjo;->p:Ljava/lang/String;

    move-object/from16 v0, p15

    iput-object v0, p0, LX/Hjo;->q:Ljava/util/Collection;

    move/from16 v0, p16

    iput-boolean v0, p0, LX/Hjo;->r:Z

    move/from16 v0, p22

    iput-boolean v0, p0, LX/Hjo;->s:Z

    move/from16 v0, p23

    iput-boolean v0, p0, LX/Hjo;->t:Z

    move-object/from16 v0, p17

    iput-object v0, p0, LX/Hjo;->B:LX/Hj9;

    move-object/from16 v0, p18

    iput-object v0, p0, LX/Hjo;->C:Ljava/lang/String;

    move-object/from16 v0, p19

    iput-object v0, p0, LX/Hjo;->D:Ljava/lang/String;

    move-object/from16 v0, p20

    iput-object v0, p0, LX/Hjo;->E:LX/Hjn;

    move-object/from16 v0, p21

    iput-object v0, p0, LX/Hjo;->l:Ljava/lang/String;

    move/from16 v0, p24

    iput v0, p0, LX/Hjo;->u:I

    move/from16 v0, p25

    iput v0, p0, LX/Hjo;->v:I

    move/from16 v0, p29

    iput v0, p0, LX/Hjo;->w:I

    move/from16 v0, p30

    iput v0, p0, LX/Hjo;->x:I

    move-object/from16 v0, p26

    iput-object v0, p0, LX/Hjo;->y:Ljava/lang/String;

    move-object/from16 v0, p27

    iput-object v0, p0, LX/Hjo;->z:Ljava/lang/String;

    move-object/from16 v0, p28

    iput-object v0, p0, LX/Hjo;->A:Ljava/lang/String;

    move-object/from16 v0, p31

    iput-object v0, p0, LX/Hjo;->F:LX/Hk3;

    move-object/from16 v0, p32

    iput-object v0, p0, LX/Hjo;->G:LX/HjI;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, LX/Hjo;->H:Ljava/lang/String;

    return-void
.end method

.method public static a(Lorg/json/JSONObject;)LX/Hjo;
    .locals 35

    if-nez p0, :cond_1

    const/4 v2, 0x0

    :cond_0
    :goto_0
    return-object v2

    :cond_1
    const-string v2, "fbad_command"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const-string v2, "title"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v2, "subtitle"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v2, "body"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v2, "call_to_action"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v2, "social_context"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v2, "icon"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    invoke-static {v2}, LX/Hj9;->a(Lorg/json/JSONObject;)LX/Hj9;

    move-result-object v9

    const-string v2, "image"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    invoke-static {v2}, LX/Hj9;->a(Lorg/json/JSONObject;)LX/Hj9;

    move-result-object v10

    const-string v2, "star_rating"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    invoke-static {v2}, LX/HjB;->a(Lorg/json/JSONObject;)LX/HjB;

    move-result-object v11

    const-string v2, "impression_report_url"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    const-string v2, "native_view_report_url"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    const-string v2, "click_report_url"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    new-instance v2, LX/Hk3;

    invoke-direct {v2}, LX/Hk3;-><init>()V

    const-string v14, "is_organic"

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v14

    invoke-virtual {v2, v14}, LX/Hk3;->a(Z)LX/Hk3;

    move-result-object v33

    const-string v2, "used_report_url"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    const-string v2, "manual_imp"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v18

    const-string v2, "enable_view_log"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v24

    const-string v2, "enable_snapshot_log"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v25

    const-string v2, "snapshot_log_delay_second"

    const/4 v15, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v15}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v26

    const-string v2, "snapshot_compress_quality"

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v15}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v27

    const-string v2, "viewability_check_initial_delay"

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v15}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v31

    const-string v2, "viewability_check_interval"

    const/16 v15, 0x3e8

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v15}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v32

    const/16 v19, 0x0

    const-string v2, "ad_choices_icon"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    const-string v15, "native_ui_config"

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v15

    if-nez v15, :cond_3

    const/16 v34, 0x0

    :goto_1
    if-eqz v2, :cond_2

    invoke-static {v2}, LX/Hj9;->a(Lorg/json/JSONObject;)LX/Hj9;

    move-result-object v19

    :cond_2
    const-string v2, "ad_choices_link_url"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    const-string v2, "request_id"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    const-string v2, "invalidation_behavior"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/Hkk;->a(Ljava/lang/String;)LX/Hkk;

    move-result-object v15

    const-string v2, "invalidation_report_url"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    const/16 v17, 0x0

    :try_start_0
    new-instance v2, Lorg/json/JSONArray;

    const-string v22, "detection_strings"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-direct {v2, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    invoke-static {v2}, LX/Hkl;->a(Lorg/json/JSONArray;)Ljava/util/Collection;

    move-result-object v17

    const-string v2, "trackers"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    new-instance v22, LX/Hjn;

    move-object/from16 v0, v22

    invoke-direct {v0, v2}, LX/Hjn;-><init>(Lorg/json/JSONArray;)V

    const-string v2, "video_url"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    const-string v2, "video_play_report_url"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    const-string v2, "video_time_report_url"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    new-instance v2, LX/Hjo;

    invoke-direct/range {v2 .. v34}, LX/Hjo;-><init>(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Hj9;LX/Hj9;LX/HjB;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Hkk;Ljava/lang/String;Ljava/util/Collection;ZLX/Hj9;Ljava/lang/String;Ljava/lang/String;LX/Hjn;Ljava/lang/String;ZZIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IILX/Hk3;LX/HjI;)V

    invoke-static {v2}, LX/Hjo;->v(LX/Hjo;)Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_3
    new-instance v34, LX/HjI;

    move-object/from16 v0, v34

    invoke-direct {v0, v15}, LX/HjI;-><init>(Lorg/json/JSONObject;)V

    goto :goto_1

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    move-object/from16 v2, v17

    goto :goto_2
.end method

.method private static a(Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p2, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public static a(Ljava/util/Map;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string v0, "mil"

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "mil"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const-string v1, "mil"

    invoke-interface {p1, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v0, "mil"

    const-string v1, "true"

    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static b(Ljava/util/Map;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string v0, "nti"

    invoke-static {v0, p0, p1}, LX/Hjo;->a(Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;)V

    const-string v0, "nhs"

    invoke-static {v0, p0, p1}, LX/Hjo;->a(Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;)V

    const-string v0, "nmv"

    invoke-static {v0, p0, p1}, LX/Hjo;->a(Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;)V

    return-void
.end method

.method public static u(LX/Hjo;)V
    .locals 5

    const/4 v4, 0x1

    iget-boolean v0, p0, LX/Hjo;->K:Z

    if-nez v0, :cond_0

    new-instance v0, LX/Hkv;

    invoke-direct {v0}, LX/Hkv;-><init>()V

    new-array v1, v4, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, LX/Hjo;->n:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, LX/Hkv;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    iput-boolean v4, p0, LX/Hjo;->K:Z

    :cond_0
    return-void
.end method

.method private static v(LX/Hjo;)Z
    .locals 1

    iget-object v0, p0, LX/Hjo;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Hjo;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, LX/Hjo;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Hjo;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, LX/Hjo;->h:LX/Hj9;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Hjo;->i:LX/Hj9;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/Hkk;
    .locals 1

    iget-object v0, p0, LX/Hjo;->o:LX/Hkk;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LX/Hjo;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, LX/Hjo;->q:Ljava/util/Collection;

    return-object v0
.end method
