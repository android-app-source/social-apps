.class public LX/IB6;
.super Lcom/facebook/fbui/widget/contentview/CheckedContentView;
.source ""


# static fields
.field public static l:LX/IAI;


# instance fields
.field public j:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2546610
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;-><init>(Landroid/content/Context;)V

    .line 2546611
    const-class v0, LX/IB6;

    invoke-static {v0, p0}, LX/IB6;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2546612
    iget-object v0, p0, LX/IB6;->k:Landroid/content/res/Resources;

    const v1, 0x7f0b145b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2546613
    iget-object v1, p0, LX/IB6;->k:Landroid/content/res/Resources;

    const v2, 0x7f0b1463

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2546614
    sget-object v2, LX/6VF;->SMALL:LX/6VF;

    invoke-virtual {p0, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setThumbnailSize(LX/6VF;)V

    .line 2546615
    invoke-virtual {p0, v4}, Lcom/facebook/fbui/widget/contentview/ContentView;->setMaxLinesFromThumbnailSize(Z)V

    .line 2546616
    invoke-virtual {p0, v3, v3}, Lcom/facebook/fbui/widget/contentview/ContentView;->e(II)V

    .line 2546617
    invoke-virtual {p0, v1, v0, v4, v0}, LX/IB6;->setPadding(IIII)V

    .line 2546618
    sget-object v0, LX/6V9;->START:LX/6V9;

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setCheckMarkPosition(LX/6V9;)V

    .line 2546619
    iget-object v0, p0, LX/IB6;->j:LX/0wM;

    const v1, 0x7f0207d6

    const v2, -0x6e685d

    invoke-virtual {v0, v1, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2546620
    iget-object v1, p0, LX/IB6;->k:Landroid/content/res/Resources;

    const v2, 0x7f0b1437

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iget-object v2, p0, LX/IB6;->k:Landroid/content/res/Resources;

    const v3, 0x7f0b1437

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0, v4, v4, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2546621
    new-instance v1, LX/IAI;

    iget-object v2, p0, LX/IB6;->k:Landroid/content/res/Resources;

    const v3, 0x7f0b1436

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-direct {v1, v0, v2}, LX/IAI;-><init>(Landroid/graphics/drawable/Drawable;I)V

    sput-object v1, LX/IB6;->l:LX/IAI;

    .line 2546622
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/IB6;

    invoke-static {p0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v1

    check-cast v1, LX/0wM;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object p0

    check-cast p0, Landroid/content/res/Resources;

    iput-object v1, p1, LX/IB6;->j:LX/0wM;

    iput-object p0, p1, LX/IB6;->k:Landroid/content/res/Resources;

    return-void
.end method
