.class public final LX/HRL;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2465062
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_a

    .line 2465063
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2465064
    :goto_0
    return v1

    .line 2465065
    :cond_0
    const-string v11, "insights_badge_count"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 2465066
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v6, v3

    move v3, v2

    .line 2465067
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_7

    .line 2465068
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 2465069
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2465070
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 2465071
    const-string v11, "admin_info"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 2465072
    invoke-static {p0, p1}, LX/HRI;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 2465073
    :cond_2
    const-string v11, "comms_hub_config"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 2465074
    invoke-static {p0, p1}, LX/HRJ;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 2465075
    :cond_3
    const-string v11, "id"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 2465076
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 2465077
    :cond_4
    const-string v11, "is_published"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 2465078
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v5, v0

    move v0, v2

    goto :goto_1

    .line 2465079
    :cond_5
    const-string v11, "page_likers"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 2465080
    invoke-static {p0, p1}, LX/HRK;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 2465081
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2465082
    :cond_7
    const/4 v10, 0x6

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 2465083
    invoke-virtual {p1, v1, v9}, LX/186;->b(II)V

    .line 2465084
    invoke-virtual {p1, v2, v8}, LX/186;->b(II)V

    .line 2465085
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v7}, LX/186;->b(II)V

    .line 2465086
    if-eqz v3, :cond_8

    .line 2465087
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v6, v1}, LX/186;->a(III)V

    .line 2465088
    :cond_8
    if-eqz v0, :cond_9

    .line 2465089
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v5}, LX/186;->a(IZ)V

    .line 2465090
    :cond_9
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2465091
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_a
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2465092
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2465093
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2465094
    if-eqz v0, :cond_0

    .line 2465095
    const-string v1, "admin_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2465096
    invoke-static {p0, v0, p2, p3}, LX/HRI;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2465097
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2465098
    if-eqz v0, :cond_1

    .line 2465099
    const-string v1, "comms_hub_config"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2465100
    invoke-static {p0, v0, p2}, LX/HRJ;->a(LX/15i;ILX/0nX;)V

    .line 2465101
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2465102
    if-eqz v0, :cond_2

    .line 2465103
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2465104
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2465105
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2465106
    if-eqz v0, :cond_3

    .line 2465107
    const-string v1, "insights_badge_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2465108
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2465109
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2465110
    if-eqz v0, :cond_4

    .line 2465111
    const-string v1, "is_published"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2465112
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2465113
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2465114
    if-eqz v0, :cond_5

    .line 2465115
    const-string v1, "page_likers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2465116
    invoke-static {p0, v0, p2}, LX/HRK;->a(LX/15i;ILX/0nX;)V

    .line 2465117
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2465118
    return-void
.end method
