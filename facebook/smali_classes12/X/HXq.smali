.class public LX/HXq;
.super Lcom/facebook/widget/ListViewFriendlyViewPager;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2479440
    invoke-direct {p0, p1}, Lcom/facebook/widget/ListViewFriendlyViewPager;-><init>(Landroid/content/Context;)V

    .line 2479441
    invoke-direct {p0}, LX/HXq;->i()V

    .line 2479442
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2479428
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/ListViewFriendlyViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2479429
    invoke-direct {p0}, LX/HXq;->i()V

    .line 2479430
    return-void
.end method

.method private i()V
    .locals 3

    .prologue
    .line 2479435
    invoke-virtual {p0}, LX/HXq;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 2479436
    int-to-float v1, v0

    const v2, 0x3f638e39

    mul-float/2addr v1, v2

    float-to-int v1, v1

    .line 2479437
    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    .line 2479438
    invoke-virtual {p0}, LX/HXq;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, LX/HXq;->getPaddingBottom()I

    move-result v2

    invoke-virtual {p0, v0, v1, v0, v2}, LX/HXq;->setPadding(IIII)V

    .line 2479439
    return-void
.end method


# virtual methods
.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 2479431
    invoke-super {p0, p1}, Lcom/facebook/widget/ListViewFriendlyViewPager;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2479432
    invoke-direct {p0}, LX/HXq;->i()V

    .line 2479433
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2479434
    return-void
.end method
