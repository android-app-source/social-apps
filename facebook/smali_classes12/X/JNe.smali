.class public LX/JNe;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JNh;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JNe",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JNh;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2686305
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2686306
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JNe;->b:LX/0Zi;

    .line 2686307
    iput-object p1, p0, LX/JNe;->a:LX/0Ot;

    .line 2686308
    return-void
.end method

.method public static a(LX/0QB;)LX/JNe;
    .locals 4

    .prologue
    .line 2686294
    const-class v1, LX/JNe;

    monitor-enter v1

    .line 2686295
    :try_start_0
    sget-object v0, LX/JNe;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2686296
    sput-object v2, LX/JNe;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2686297
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2686298
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2686299
    new-instance v3, LX/JNe;

    const/16 p0, 0x1f3c

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JNe;-><init>(LX/0Ot;)V

    .line 2686300
    move-object v0, v3

    .line 2686301
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2686302
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JNe;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2686303
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2686304
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 13

    .prologue
    .line 2686207
    check-cast p2, LX/JNd;

    .line 2686208
    iget-object v0, p0, LX/JNe;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JNh;

    iget-object v1, p2, LX/JNd;->a:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;

    iget v2, p2, LX/JNd;->c:I

    const/16 v4, 0x101

    const/16 v3, 0x81

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v5, 0x0

    .line 2686209
    const/4 v6, 0x0

    .line 2686210
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v7

    .line 2686211
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v8

    const v9, 0x25d6af

    if-ne v8, v9, :cond_2

    .line 2686212
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLProfile;->s()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2686213
    iget-object v4, v0, LX/JNh;->c:Landroid/content/res/Resources;

    const v6, 0x7f081a47

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2686214
    :goto_0
    const v6, 0x7f0208fa

    .line 2686215
    :goto_1
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v7

    const v8, 0x7f0b2558

    invoke-interface {v7, v10, v8}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, v2}, LX/1Dh;->F(I)LX/1Dh;

    move-result-object v7

    const v8, 0x7f0b2559

    invoke-interface {v7, v8}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, v11}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v7

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v8

    const/4 v2, 0x2

    const/4 p2, 0x1

    .line 2686216
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v9

    invoke-interface {v9, v2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v9

    invoke-interface {v9, v2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v9

    invoke-interface {v9, p2}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v9

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v12

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLProfile;->D()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v12, p0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v12

    sget-object p0, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v12, p0}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v12

    const/4 p0, 0x0

    invoke-virtual {v12, p0}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v12

    const p0, 0x7f0b0052

    invoke-virtual {v12, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v12

    const p0, 0x7f0a0099

    invoke-virtual {v12, p0}, LX/1ne;->n(I)LX/1ne;

    move-result-object v12

    invoke-virtual {v12, p2}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v12

    sget-object p0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v12, p0}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v12

    const p0, 0x7f0b255a

    invoke-virtual {v12, p0}, LX/1ne;->s(I)LX/1ne;

    move-result-object v12

    invoke-virtual {v12}, LX/1X5;->c()LX/1Di;

    move-result-object v12

    invoke-interface {v12, v2, v2}, LX/1Di;->h(II)LX/1Di;

    move-result-object v12

    invoke-interface {v9, v12}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v9

    const v12, 0x7f0b255e

    invoke-interface {v9, p2, v12}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v9

    .line 2686217
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLProfile;->A()Z

    move-result v12

    if-eqz v12, :cond_5

    .line 2686218
    iget-object v12, v0, LX/JNh;->d:LX/1vg;

    invoke-virtual {v12, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v12

    const p0, 0x7f0207dc

    invoke-virtual {v12, p0}, LX/2xv;->h(I)LX/2xv;

    move-result-object v12

    const p0, 0x7f0a008d

    invoke-virtual {v12, p0}, LX/2xv;->j(I)LX/2xv;

    move-result-object v12

    .line 2686219
    :goto_2
    move-object v12, v12

    .line 2686220
    if-eqz v12, :cond_0

    .line 2686221
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object p0

    invoke-virtual {p0, v12}, LX/1o5;->a(LX/1n6;)LX/1o5;

    move-result-object v12

    invoke-virtual {v12}, LX/1X5;->c()LX/1Di;

    move-result-object v12

    const p0, 0x7f0b2562

    invoke-interface {v12, p0}, LX/1Di;->i(I)LX/1Di;

    move-result-object v12

    const p0, 0x7f0b2563

    invoke-interface {v12, p0}, LX/1Di;->q(I)LX/1Di;

    move-result-object v12

    const p0, 0x7f0b2564

    invoke-interface {v12, p2, p0}, LX/1Di;->c(II)LX/1Di;

    move-result-object v12

    const/4 p0, 0x3

    const p2, 0x7f0b2565

    invoke-interface {v12, p0, p2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v12

    invoke-interface {v9, v12}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2686222
    :cond_0
    move-object v8, v9

    .line 2686223
    invoke-interface {v7, v8}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v7

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v8

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v8

    sget-object v9, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v8, v9}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v8

    invoke-virtual {v8, v5}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v8

    const v9, 0x7f0b0050

    invoke-virtual {v8, v9}, LX/1ne;->q(I)LX/1ne;

    move-result-object v8

    const v9, 0x7f0a00a8

    invoke-virtual {v8, v9}, LX/1ne;->n(I)LX/1ne;

    move-result-object v8

    const v9, 0x7f0b255b

    invoke-virtual {v8, v9}, LX/1ne;->s(I)LX/1ne;

    move-result-object v8

    invoke-virtual {v8}, LX/1X5;->c()LX/1Di;

    move-result-object v8

    const v9, 0x7f0b255f

    invoke-interface {v8, v10, v9}, LX/1Di;->c(II)LX/1Di;

    move-result-object v8

    const v9, 0x7f0b255d

    invoke-interface {v8, v5, v9}, LX/1Di;->c(II)LX/1Di;

    move-result-object v8

    const v9, 0x7f0b255d

    invoke-interface {v8, v11, v9}, LX/1Di;->c(II)LX/1Di;

    move-result-object v8

    invoke-interface {v7, v8}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v7

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v8

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v8

    sget-object v9, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v8, v9}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v8

    invoke-virtual {v8, v5}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v5

    const v8, 0x7f0b004e

    invoke-virtual {v5, v8}, LX/1ne;->q(I)LX/1ne;

    move-result-object v5

    const v8, 0x7f0a00a4

    invoke-virtual {v5, v8}, LX/1ne;->n(I)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v10}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v5

    sget-object v8, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v5, v8}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v5

    const v8, 0x7f0b255c

    invoke-virtual {v5, v8}, LX/1ne;->s(I)LX/1ne;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const v8, 0x7f0b2560

    invoke-interface {v5, v10, v8}, LX/1Di;->c(II)LX/1Di;

    move-result-object v5

    invoke-interface {v7, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v7

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-interface {v7, v8}, LX/1Dh;->e(F)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, v10}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v7

    const/4 v8, 0x3

    const v9, 0x7f0b2561

    invoke-interface {v7, v8, v9}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v7

    iget-object v8, v0, LX/JNh;->b:LX/2g9;

    invoke-virtual {v8, p1}, LX/2g9;->c(LX/1De;)LX/2gA;

    move-result-object v8

    invoke-virtual {v8, v3}, LX/2gA;->h(I)LX/2gA;

    move-result-object v3

    invoke-virtual {v3, v4}, LX/2gA;->a(Ljava/lang/CharSequence;)LX/2gA;

    move-result-object v3

    invoke-virtual {v3, v6}, LX/2gA;->k(I)LX/2gA;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    .line 2686224
    const v4, 0x193eb55b

    const/4 v6, 0x0

    invoke-static {p1, v4, v6}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v4, v4

    .line 2686225
    invoke-interface {v3, v4}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v3

    invoke-interface {v7, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v5, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    .line 2686226
    const v4, 0x55a7fdb

    const/4 v5, 0x0

    invoke-static {p1, v4, v5}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v4, v4

    .line 2686227
    invoke-interface {v3, v4}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2686228
    return-object v0

    .line 2686229
    :cond_1
    iget-object v3, v0, LX/JNh;->c:Landroid/content/res/Resources;

    const v6, 0x7f081a46

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    move v12, v4

    move-object v4, v3

    move v3, v12

    .line 2686230
    goto/16 :goto_0

    .line 2686231
    :cond_2
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v8

    const v9, 0x285feb

    if-ne v8, v9, :cond_4

    .line 2686232
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLProfile;->K()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v6

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-ne v6, v7, :cond_3

    .line 2686233
    iget-object v4, v0, LX/JNh;->c:Landroid/content/res/Resources;

    const v6, 0x7f081a45

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2686234
    :goto_3
    const v6, 0x7f02086f

    goto/16 :goto_1

    .line 2686235
    :cond_3
    iget-object v3, v0, LX/JNh;->c:Landroid/content/res/Resources;

    const v6, 0x7f081a44

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    move v12, v4

    move-object v4, v3

    move v3, v12

    .line 2686236
    goto :goto_3

    :cond_4
    move v3, v5

    move-object v4, v6

    move v6, v5

    goto/16 :goto_1

    .line 2686237
    :cond_5
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v12

    invoke-virtual {v12}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v12

    const p0, 0x25d6af

    if-ne v12, p0, :cond_6

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLProfile;->Y()Z

    move-result v12

    if-eqz v12, :cond_6

    .line 2686238
    iget-object v12, v0, LX/JNh;->d:LX/1vg;

    invoke-virtual {v12, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v12

    const p0, 0x7f0207d9

    invoke-virtual {v12, p0}, LX/2xv;->h(I)LX/2xv;

    move-result-object v12

    const p0, 0x7f0a00ba

    invoke-virtual {v12, p0}, LX/2xv;->j(I)LX/2xv;

    move-result-object v12

    goto/16 :goto_2

    .line 2686239
    :cond_6
    const/4 v12, 0x0

    goto/16 :goto_2
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 2686240
    invoke-static {}, LX/1dS;->b()V

    .line 2686241
    iget v0, p1, LX/1dQ;->b:I

    .line 2686242
    sparse-switch v0, :sswitch_data_0

    .line 2686243
    :goto_0
    return-object v2

    .line 2686244
    :sswitch_0
    check-cast p2, LX/3Ae;

    .line 2686245
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2686246
    check-cast v1, LX/JNd;

    .line 2686247
    iget-object v3, p0, LX/JNe;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JNh;

    iget-object p1, v1, LX/JNd;->d:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;

    iget-object p2, v1, LX/JNd;->a:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;

    .line 2686248
    iget-object p0, v3, LX/JNh;->a:LX/JNi;

    invoke-virtual {p0, p1, p2, v0}, LX/JNi;->a(Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;Landroid/view/View;)V

    .line 2686249
    goto :goto_0

    .line 2686250
    :sswitch_1
    iget-object v0, p1, LX/1dQ;->a:LX/1X1;

    .line 2686251
    check-cast v0, LX/JNd;

    .line 2686252
    iget-object v1, p0, LX/JNe;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/JNh;

    iget-object v3, v0, LX/JNd;->d:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;

    iget-object v4, v0, LX/JNd;->a:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;

    iget-object v5, v0, LX/JNd;->e:LX/3mj;

    iget-object v6, v0, LX/JNd;->b:LX/1Pb;

    const/4 v7, 0x1

    .line 2686253
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object p1

    .line 2686254
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object p2

    .line 2686255
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result p0

    const v0, 0x25d6af

    if-ne p0, v0, :cond_3

    .line 2686256
    invoke-static {v4, v3}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object p0

    .line 2686257
    invoke-static {p0}, LX/17Q;->F(LX/0lF;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2686258
    const/4 v0, 0x0

    .line 2686259
    :goto_1
    move-object p0, v0

    .line 2686260
    iget-object v0, v1, LX/JNh;->h:LX/0Zb;

    invoke-interface {v0, p0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2686261
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->s()Z

    move-result p0

    if-nez p0, :cond_0

    .line 2686262
    invoke-virtual {v5, v7}, LX/3mj;->a(Z)V

    .line 2686263
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->s()Z

    move-result p1

    if-nez p1, :cond_2

    .line 2686264
    :goto_2
    invoke-static {}, Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;->a()LX/5H8;

    move-result-object p1

    .line 2686265
    iput-object p2, p1, LX/5H8;->a:Ljava/lang/String;

    .line 2686266
    move-object p1, p1

    .line 2686267
    iput-boolean v7, p1, LX/5H8;->b:Z

    .line 2686268
    move-object p1, p1

    .line 2686269
    iget-object p0, v1, LX/JNh;->e:LX/20i;

    invoke-virtual {p0}, LX/20i;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object p0

    .line 2686270
    iput-object p0, p1, LX/5H8;->c:Lcom/facebook/graphql/model/GraphQLActor;

    .line 2686271
    move-object p1, p1

    .line 2686272
    invoke-virtual {p1}, LX/5H8;->a()Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;

    move-result-object p1

    .line 2686273
    iget-object p0, v1, LX/JNh;->f:LX/1Ck;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "task_key_toggle_page_like"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v3, LX/JNf;

    invoke-direct {v3, v1, p1}, LX/JNf;-><init>(LX/JNh;Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;)V

    new-instance p1, LX/JNg;

    invoke-direct {p1, v1}, LX/JNg;-><init>(LX/JNh;)V

    invoke-virtual {p0, v0, v3, p1}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2686274
    :cond_1
    :goto_3
    goto/16 :goto_0

    .line 2686275
    :cond_2
    const/4 v7, 0x0

    goto :goto_2

    .line 2686276
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result p0

    const v0, 0x285feb

    if-ne p0, v0, :cond_1

    .line 2686277
    invoke-static {v4, v3}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object p0

    .line 2686278
    invoke-static {p0}, LX/17Q;->F(LX/0lF;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2686279
    const/4 v0, 0x0

    .line 2686280
    :goto_4
    move-object p0, v0

    .line 2686281
    iget-object v0, v1, LX/JNh;->h:LX/0Zb;

    invoke-interface {v0, p0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2686282
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->K()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object p1

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-ne p1, p0, :cond_4

    .line 2686283
    const-string v7, "LMAF_NETEGO"

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, p2, v7}, LX/1Pb;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 2686284
    :cond_4
    invoke-virtual {v5, v7}, LX/3mj;->a(Z)V

    .line 2686285
    const-string v7, "LMAF_NETEGO"

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, p2, v7}, LX/1Pb;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 2686286
    :cond_5
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "connect_with_facebook_fan"

    invoke-direct {v0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "tracking"

    invoke-virtual {v0, v3, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v3, "native_newsfeed"

    .line 2686287
    iput-object v3, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2686288
    move-object v0, v0

    .line 2686289
    goto/16 :goto_1

    .line 2686290
    :cond_6
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "connect_with_facebook_follow"

    invoke-direct {v0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "tracking"

    invoke-virtual {v0, v3, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v3, "native_newsfeed"

    .line 2686291
    iput-object v3, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2686292
    move-object v0, v0

    .line 2686293
    goto :goto_4

    nop

    :sswitch_data_0
    .sparse-switch
        0x55a7fdb -> :sswitch_0
        0x193eb55b -> :sswitch_1
    .end sparse-switch
.end method
