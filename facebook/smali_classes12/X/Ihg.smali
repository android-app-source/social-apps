.class public LX/Ihg;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/widget/ImageView;

.field public b:Z

.field public c:Z

.field public d:LX/IhC;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2602943
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2602944
    const v0, 0x7f0d1b2b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/Ihg;->a:Landroid/widget/ImageView;

    .line 2602945
    iget-object v0, p0, LX/Ihg;->a:Landroid/widget/ImageView;

    new-instance v1, LX/Ihf;

    invoke-direct {v1, p0}, LX/Ihf;-><init>(LX/Ihg;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2602946
    return-void
.end method

.method public static a(LX/Ihg;)V
    .locals 2

    .prologue
    .line 2602947
    iget-object v1, p0, LX/Ihg;->a:Landroid/widget/ImageView;

    iget-boolean v0, p0, LX/Ihg;->c:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/Ihg;->b:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2602948
    return-void

    .line 2602949
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
