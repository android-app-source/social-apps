.class public LX/IBq;
.super LX/1OX;
.source ""

# interfaces
.implements LX/I8d;


# instance fields
.field public final a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "LX/IBr;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;

.field public c:LX/I8i;

.field public d:LX/I8d;

.field private e:Z

.field private f:Landroid/content/Context;

.field private g:LX/0zG;

.field private h:Landroid/view/ViewStub;

.field private i:Ljava/lang/Runnable;

.field private j:I

.field public k:[LX/IBr;

.field public l:LX/IBr;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0zG;Landroid/view/ViewStub;Ljava/lang/Boolean;I)V
    .locals 1

    .prologue
    .line 2547788
    invoke-direct {p0}, LX/1OX;-><init>()V

    .line 2547789
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/IBq;->a:Ljava/util/HashMap;

    .line 2547790
    iput-object p1, p0, LX/IBq;->f:Landroid/content/Context;

    .line 2547791
    iput-object p2, p0, LX/IBq;->g:LX/0zG;

    .line 2547792
    iput-object p3, p0, LX/IBq;->h:Landroid/view/ViewStub;

    .line 2547793
    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/IBq;->e:Z

    .line 2547794
    iput p5, p0, LX/IBq;->j:I

    .line 2547795
    return-void
.end method

.method public static g(LX/IBq;)Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2547787
    iget-object v0, p0, LX/IBq;->c:LX/I8i;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/IBq;->c:LX/I8i;

    iget-object v0, v0, LX/1a1;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;

    goto :goto_0
.end method

.method public static h(LX/IBq;)Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2547774
    iget-object v0, p0, LX/IBq;->b:Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;

    if-eqz v0, :cond_0

    .line 2547775
    iget-object v0, p0, LX/IBq;->b:Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;

    iget-object v1, p0, LX/IBq;->a:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;->setBadges(Ljava/util/HashMap;)V

    .line 2547776
    iget-object v0, p0, LX/IBq;->b:Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;

    .line 2547777
    :goto_0
    return-object v0

    .line 2547778
    :cond_0
    iget-object v0, p0, LX/IBq;->h:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;

    iput-object v0, p0, LX/IBq;->b:Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;

    .line 2547779
    iget-object v0, p0, LX/IBq;->b:Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;->setVisibility(I)V

    .line 2547780
    iget-object v0, p0, LX/IBq;->b:Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;

    invoke-virtual {p0}, LX/IBq;->c()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;->setTranslationY(F)V

    .line 2547781
    iget-object v0, p0, LX/IBq;->b:Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;

    .line 2547782
    iput-object p0, v0, Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;->b:LX/I8d;

    .line 2547783
    iget-object v0, p0, LX/IBq;->b:Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;

    iget-object v1, p0, LX/IBq;->k:[LX/IBr;

    invoke-virtual {v0, v1}, Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;->setTabTypes([LX/IBr;)V

    .line 2547784
    iget-object v0, p0, LX/IBq;->b:Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;

    iget-object v1, p0, LX/IBq;->l:LX/IBr;

    invoke-virtual {v0, v1}, Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;->setSelected(LX/IBr;)V

    .line 2547785
    iget-object v0, p0, LX/IBq;->b:Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;

    iget-object v1, p0, LX/IBq;->a:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;->setBadges(Ljava/util/HashMap;)V

    .line 2547786
    iget-object v0, p0, LX/IBq;->b:Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/IBr;)V
    .locals 1

    .prologue
    .line 2547771
    iget-object v0, p0, LX/IBq;->l:LX/IBr;

    if-nez v0, :cond_0

    .line 2547772
    iput-object p1, p0, LX/IBq;->l:LX/IBr;

    .line 2547773
    :cond_0
    return-void
.end method

.method public final a(LX/IBr;Z)V
    .locals 2

    .prologue
    .line 2547796
    iput-object p1, p0, LX/IBq;->l:LX/IBr;

    .line 2547797
    invoke-static {p0}, LX/IBq;->g(LX/IBq;)Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;

    move-result-object v0

    .line 2547798
    if-eqz v0, :cond_0

    .line 2547799
    invoke-virtual {v0, p1}, Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;->setSelected(LX/IBr;)V

    .line 2547800
    :cond_0
    iget-object v0, p0, LX/IBq;->b:Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;

    if-eqz v0, :cond_1

    .line 2547801
    iget-object v0, p0, LX/IBq;->b:Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;

    invoke-virtual {v0, p1}, Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;->setSelected(LX/IBr;)V

    .line 2547802
    :cond_1
    iget-object v0, p0, LX/IBq;->d:LX/I8d;

    if-eqz v0, :cond_2

    .line 2547803
    iget-object v0, p0, LX/IBq;->d:LX/I8d;

    invoke-interface {v0, p1, p2}, LX/I8d;->a(LX/IBr;Z)V

    .line 2547804
    :cond_2
    iget-object v0, p0, LX/IBq;->i:Ljava/lang/Runnable;

    if-nez v0, :cond_3

    .line 2547805
    new-instance v0, Lcom/facebook/events/permalink/tabbar/StickyTabBarController$1;

    invoke-direct {v0, p0}, Lcom/facebook/events/permalink/tabbar/StickyTabBarController$1;-><init>(LX/IBq;)V

    iput-object v0, p0, LX/IBq;->i:Ljava/lang/Runnable;

    .line 2547806
    :cond_3
    iget-object v0, p0, LX/IBq;->b:Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;

    if-eqz v0, :cond_4

    .line 2547807
    iget-object v0, p0, LX/IBq;->b:Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;

    iget-object v1, p0, LX/IBq;->i:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;->post(Ljava/lang/Runnable;)Z

    .line 2547808
    :cond_4
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 2

    .prologue
    .line 2547761
    invoke-static {p0}, LX/IBq;->g(LX/IBq;)Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;

    move-result-object v0

    .line 2547762
    if-eqz v0, :cond_0

    .line 2547763
    if-lez p3, :cond_1

    invoke-virtual {p0}, LX/IBq;->d()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2547764
    invoke-virtual {v0}, Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;->getTop()I

    move-result v0

    invoke-virtual {p0}, LX/IBq;->c()I

    move-result v1

    if-gt v0, v1, :cond_0

    .line 2547765
    invoke-static {p0}, LX/IBq;->h(LX/IBq;)Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;->setVisibility(I)V

    .line 2547766
    :cond_0
    :goto_0
    return-void

    .line 2547767
    :cond_1
    if-gez p3, :cond_0

    invoke-virtual {p0}, LX/IBq;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2547768
    invoke-virtual {v0}, Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;->getTop()I

    move-result v0

    invoke-virtual {p0}, LX/IBq;->c()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 2547769
    iget-object v0, p0, LX/IBq;->b:Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;

    if-eqz v0, :cond_0

    .line 2547770
    iget-object v0, p0, LX/IBq;->b:Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;->setVisibility(I)V

    goto :goto_0
.end method

.method public final b(LX/IBr;)Z
    .locals 1

    .prologue
    .line 2547758
    iget-object v0, p0, LX/IBq;->b:Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;

    if-eqz v0, :cond_0

    .line 2547759
    iget-object v0, p0, LX/IBq;->b:Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;

    invoke-virtual {v0, p1}, Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;->a(LX/IBr;)Z

    move-result v0

    .line 2547760
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/IBq;->l:LX/IBr;

    if-ne v0, p1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2547754
    iget-boolean v0, p0, LX/IBq;->e:Z

    if-eqz v0, :cond_0

    move v0, v1

    .line 2547755
    :goto_0
    return v0

    .line 2547756
    :cond_0
    iget-object v0, p0, LX/IBq;->g:LX/0zG;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 2547757
    instance-of v0, v0, LX/63T;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/IBq;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0413

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iget v1, p0, LX/IBq;->j:I

    add-int/2addr v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 2547753
    iget-object v0, p0, LX/IBq;->b:Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/IBq;->b:Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;

    invoke-virtual {v0}, Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
