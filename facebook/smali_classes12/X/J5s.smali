.class public LX/J5s;
.super Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;
.source ""


# instance fields
.field public c:Landroid/view/View$OnClickListener;

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;Landroid/content/res/Resources;Landroid/content/Context;LX/J5k;LX/J64;LX/J4L;)V
    .locals 0
    .param p5    # LX/J64;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/J4L;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2648990
    invoke-direct/range {p0 .. p6}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;-><init>(Landroid/view/LayoutInflater;Landroid/content/res/Resources;Landroid/content/Context;LX/J5k;LX/J64;LX/J4L;)V

    .line 2648991
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/J4J;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 2648992
    invoke-virtual {p0, p1, p2}, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->b(Landroid/view/View;LX/J4J;)V

    .line 2648993
    const v0, 0x7f0d2694

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/spinner/AudienceSpinner;

    .line 2648994
    const v1, 0x7f0d2695

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/ui/PrivacyOptionView;

    .line 2648995
    invoke-virtual {v1, v4}, Lcom/facebook/privacy/ui/PrivacyOptionView;->setVisibility(I)V

    .line 2648996
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/facebook/privacy/spinner/AudienceSpinner;->setVisibility(I)V

    .line 2648997
    invoke-virtual {v1, v4}, Lcom/facebook/privacy/ui/PrivacyOptionView;->setBackgroundResource(I)V

    .line 2648998
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->a:LX/J64;

    invoke-interface {v0}, LX/J64;->c()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->b:LX/J4L;

    iget-object v0, v0, LX/J4L;->d:LX/0Px;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;->CHANGE_PRIVACY:Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;

    invoke-virtual {v0, v2}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2648999
    const/4 v0, 0x1

    .line 2649000
    iput-boolean v0, v1, Lcom/facebook/privacy/ui/PrivacyOptionView;->c:Z

    .line 2649001
    iget-object v0, p0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;->a:LX/J64;

    invoke-interface {v0}, LX/J64;->c()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v0

    .line 2649002
    iget-object v2, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v2

    .line 2649003
    invoke-virtual {v1, v0}, Lcom/facebook/privacy/ui/PrivacyOptionView;->setPrivacyOption(LX/1oT;)V

    .line 2649004
    iget-object v0, p0, LX/J5s;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v0}, Lcom/facebook/privacy/ui/PrivacyOptionView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2649005
    iget-object v0, p2, LX/J4J;->a:Ljava/lang/String;

    iput-object v0, p0, LX/J5s;->d:Ljava/lang/String;

    .line 2649006
    iget-object v0, p2, LX/J4J;->b:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    iput-object v0, p0, LX/J5s;->e:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    .line 2649007
    :goto_0
    return-void

    .line 2649008
    :cond_0
    iget-object v0, p2, LX/J4J;->g:Ljava/lang/String;

    iget-object v2, p2, LX/J4J;->h:LX/1Fd;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/privacy/ui/PrivacyOptionView;->a(Ljava/lang/String;LX/1Fd;)V

    .line 2649009
    invoke-virtual {v1, v3}, Lcom/facebook/privacy/ui/PrivacyOptionView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2649010
    iput-object v3, p0, LX/J5s;->d:Ljava/lang/String;

    .line 2649011
    iput-object v3, p0, LX/J5s;->e:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    goto :goto_0
.end method
