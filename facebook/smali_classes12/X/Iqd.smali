.class public LX/Iqd;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/IqZ;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/drawingview/DrawingView;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public c:LX/IrN;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2617069
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2617070
    return-void
.end method

.method public static i(LX/Iqd;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2617083
    iget-object v0, p0, LX/Iqd;->a:LX/IqZ;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2617084
    iget-object v0, p0, LX/Iqd;->b:Lcom/facebook/drawingview/DrawingView;

    if-nez v0, :cond_0

    .line 2617085
    invoke-virtual {p0}, LX/Iqd;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2617086
    new-instance v2, Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/facebook/drawingview/DrawingView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, LX/Iqd;->b:Lcom/facebook/drawingview/DrawingView;

    .line 2617087
    iget-object v2, p0, LX/Iqd;->b:Lcom/facebook/drawingview/DrawingView;

    new-instance v3, LX/Iqb;

    invoke-direct {v3, p0}, LX/Iqb;-><init>(LX/Iqd;)V

    .line 2617088
    iput-object v3, v2, Lcom/facebook/drawingview/DrawingView;->d:LX/5Ne;

    .line 2617089
    iget-object v2, p0, LX/Iqd;->b:Lcom/facebook/drawingview/DrawingView;

    new-instance v3, LX/Iqc;

    invoke-direct {v3, p0}, LX/Iqc;-><init>(LX/Iqd;)V

    .line 2617090
    iput-object v3, v2, Lcom/facebook/drawingview/DrawingView;->n:LX/5Ng;

    .line 2617091
    iget-object v2, p0, LX/Iqd;->b:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v2, v1}, Lcom/facebook/drawingview/DrawingView;->setEnabled(Z)V

    .line 2617092
    iget-object v1, p0, LX/Iqd;->b:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 2617093
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 2617094
    goto :goto_0
.end method


# virtual methods
.method public final e()Z
    .locals 1

    .prologue
    .line 2617082
    iget-object v0, p0, LX/Iqd;->b:Lcom/facebook/drawingview/DrawingView;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Iqd;->b:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v0}, Lcom/facebook/drawingview/DrawingView;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getStrokes()LX/0Px;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2617079
    iget-object v0, p0, LX/Iqd;->b:Lcom/facebook/drawingview/DrawingView;

    if-nez v0, :cond_0

    .line 2617080
    const/4 v0, 0x0

    .line 2617081
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/Iqd;->b:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v0}, Lcom/facebook/drawingview/DrawingView;->getStrokes()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final h()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2617072
    iget-object v0, p0, LX/Iqd;->b:Lcom/facebook/drawingview/DrawingView;

    if-eqz v0, :cond_0

    .line 2617073
    iget-object v0, p0, LX/Iqd;->b:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v0, v1}, Lcom/facebook/drawingview/DrawingView;->setEnabled(Z)V

    .line 2617074
    :cond_0
    iget-object v0, p0, LX/Iqd;->a:LX/IqZ;

    if-eqz v0, :cond_1

    .line 2617075
    iget-object v0, p0, LX/Iqd;->a:LX/IqZ;

    invoke-virtual {v0}, LX/IqZ;->b()V

    .line 2617076
    :cond_1
    iget-object v0, p0, LX/Iqd;->c:LX/IrN;

    if-eqz v0, :cond_2

    .line 2617077
    iget-object v0, p0, LX/Iqd;->c:LX/IrN;

    invoke-virtual {v0, v1}, LX/IrN;->a(Z)V

    .line 2617078
    :cond_2
    return-void
.end method

.method public final isEnabled()Z
    .locals 1

    .prologue
    .line 2617071
    iget-object v0, p0, LX/Iqd;->b:Lcom/facebook/drawingview/DrawingView;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Iqd;->b:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v0}, Lcom/facebook/drawingview/DrawingView;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
