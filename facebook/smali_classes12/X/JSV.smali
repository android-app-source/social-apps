.class public final LX/JSV;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/JSX;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/JSW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/JSX",
            "<TE;>.SimpleMusicStoryComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/JSX;

.field private c:[Ljava/lang/String;

.field private d:I

.field public e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/JSX;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 2695418
    iput-object p1, p0, LX/JSV;->b:LX/JSX;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 2695419
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "attachmentProps"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "environment"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/JSV;->c:[Ljava/lang/String;

    .line 2695420
    iput v3, p0, LX/JSV;->d:I

    .line 2695421
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/JSV;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/JSV;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/JSV;LX/1De;IILX/JSW;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/JSX",
            "<TE;>.SimpleMusicStoryComponentImpl;)V"
        }
    .end annotation

    .prologue
    .line 2695422
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 2695423
    iput-object p4, p0, LX/JSV;->a:LX/JSW;

    .line 2695424
    iget-object v0, p0, LX/JSV;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 2695425
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2695426
    invoke-super {p0}, LX/1X5;->a()V

    .line 2695427
    const/4 v0, 0x0

    iput-object v0, p0, LX/JSV;->a:LX/JSW;

    .line 2695428
    iget-object v0, p0, LX/JSV;->b:LX/JSX;

    iget-object v0, v0, LX/JSX;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 2695429
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/JSX;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2695430
    iget-object v1, p0, LX/JSV;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/JSV;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/JSV;->d:I

    if-ge v1, v2, :cond_2

    .line 2695431
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2695432
    :goto_0
    iget v2, p0, LX/JSV;->d:I

    if-ge v0, v2, :cond_1

    .line 2695433
    iget-object v2, p0, LX/JSV;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2695434
    iget-object v2, p0, LX/JSV;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2695435
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2695436
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2695437
    :cond_2
    iget-object v0, p0, LX/JSV;->a:LX/JSW;

    .line 2695438
    invoke-virtual {p0}, LX/JSV;->a()V

    .line 2695439
    return-object v0
.end method
