.class public final LX/HO9;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionMutationsModels$PageCallToActionContactUsFormSubmitMutationModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/HOA;


# direct methods
.method public constructor <init>(LX/HOA;)V
    .locals 0

    .prologue
    .line 2459664
    iput-object p1, p0, LX/HO9;->a:LX/HOA;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2459661
    iget-object v0, p0, LX/HO9;->a:LX/HOA;

    iget-object v0, v0, LX/HOA;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageUserCallToActionFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageUserCallToActionFragment;->g:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->a(Z)V

    .line 2459662
    iget-object v0, p0, LX/HO9;->a:LX/HOA;

    iget-object v0, v0, LX/HOA;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageUserCallToActionFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageUserCallToActionFragment;->g:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;

    const v1, 0x7f081685

    invoke-virtual {v0, v1}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->c(I)V

    .line 2459663
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2459647
    iget-object v0, p0, LX/HO9;->a:LX/HOA;

    iget-object v0, v0, LX/HOA;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageUserCallToActionFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageUserCallToActionFragment;->g:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->a(Z)V

    .line 2459648
    new-instance v0, LX/0ht;

    iget-object v1, p0, LX/HO9;->a:LX/HOA;

    iget-object v1, v1, LX/HOA;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageUserCallToActionFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0ht;-><init>(Landroid/content/Context;)V

    .line 2459649
    new-instance v1, LX/HOT;

    iget-object v2, p0, LX/HO9;->a:LX/HOA;

    iget-object v2, v2, LX/HOA;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageUserCallToActionFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/HOT;-><init>(Landroid/content/Context;)V

    .line 2459650
    const v2, 0x3f59999a    # 0.85f

    invoke-virtual {v0, v2}, LX/0ht;->b(F)V

    .line 2459651
    iput-object v0, v1, LX/HOT;->c:LX/0ht;

    .line 2459652
    new-instance v2, LX/HO8;

    invoke-direct {v2, p0}, LX/HO8;-><init>(LX/HO9;)V

    .line 2459653
    iput-object v2, v1, LX/HOT;->d:LX/HO8;

    .line 2459654
    invoke-virtual {v0, v1}, LX/0ht;->d(Landroid/view/View;)V

    .line 2459655
    iget-object v1, p0, LX/HO9;->a:LX/HOA;

    iget-object v1, v1, LX/HOA;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageUserCallToActionFragment;

    .line 2459656
    iget-object v2, v1, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v2

    .line 2459657
    invoke-virtual {v0, v1}, LX/0ht;->c(Landroid/view/View;)V

    .line 2459658
    sget-object v1, LX/3AV;->CENTER:LX/3AV;

    invoke-virtual {v0, v1}, LX/0ht;->a(LX/3AV;)V

    .line 2459659
    invoke-virtual {v0}, LX/0ht;->d()V

    .line 2459660
    return-void
.end method
