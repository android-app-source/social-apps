.class public final enum LX/J6K;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/J6K;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/J6K;

.field public static final enum CHECKUP_LIST_SECTION_HEADER_TYPE:LX/J6K;

.field public static final enum CHECKUP_LIST_SECTION_ITEM_TYPE:LX/J6K;

.field public static final enum LOADING_INDICATOR:LX/J6K;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2649527
    new-instance v0, LX/J6K;

    const-string v1, "CHECKUP_LIST_SECTION_HEADER_TYPE"

    invoke-direct {v0, v1, v2}, LX/J6K;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J6K;->CHECKUP_LIST_SECTION_HEADER_TYPE:LX/J6K;

    .line 2649528
    new-instance v0, LX/J6K;

    const-string v1, "CHECKUP_LIST_SECTION_ITEM_TYPE"

    invoke-direct {v0, v1, v3}, LX/J6K;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J6K;->CHECKUP_LIST_SECTION_ITEM_TYPE:LX/J6K;

    .line 2649529
    new-instance v0, LX/J6K;

    const-string v1, "LOADING_INDICATOR"

    invoke-direct {v0, v1, v4}, LX/J6K;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J6K;->LOADING_INDICATOR:LX/J6K;

    .line 2649530
    const/4 v0, 0x3

    new-array v0, v0, [LX/J6K;

    sget-object v1, LX/J6K;->CHECKUP_LIST_SECTION_HEADER_TYPE:LX/J6K;

    aput-object v1, v0, v2

    sget-object v1, LX/J6K;->CHECKUP_LIST_SECTION_ITEM_TYPE:LX/J6K;

    aput-object v1, v0, v3

    sget-object v1, LX/J6K;->LOADING_INDICATOR:LX/J6K;

    aput-object v1, v0, v4

    sput-object v0, LX/J6K;->$VALUES:[LX/J6K;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2649531
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/J6K;
    .locals 1

    .prologue
    .line 2649532
    const-class v0, LX/J6K;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/J6K;

    return-object v0
.end method

.method public static values()[LX/J6K;
    .locals 1

    .prologue
    .line 2649533
    sget-object v0, LX/J6K;->$VALUES:[LX/J6K;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/J6K;

    return-object v0
.end method
