.class public final LX/JCd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;)V
    .locals 0

    .prologue
    .line 2662670
    iput-object p1, p0, LX/JCd;->a:Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, -0x2e72be80

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2662671
    iget-object v0, p0, LX/JCd;->a:Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;

    iget-object v0, v0, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/J7A;

    new-instance v2, LX/J7E;

    iget-object v3, p0, LX/JCd;->a:Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;

    iget-object v3, v3, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->t:LX/9lP;

    .line 2662672
    iget-object v4, v3, LX/9lP;->e:Landroid/os/ParcelUuid;

    move-object v3, v4

    .line 2662673
    iget-object v4, p0, LX/JCd;->a:Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;

    iget-object v4, v4, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->s:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;

    invoke-virtual {v4}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;->b()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/JCd;->a:Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;

    iget-object v5, v5, Lcom/facebook/timeline/aboutpage/views/AboutCollectionItemView;->s:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;

    invoke-virtual {v5}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldModel;->c()Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5}, LX/J7E;-><init>(Landroid/os/ParcelUuid;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;)V

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b7;)V

    .line 2662674
    const v0, -0x3d9a0c21

    invoke-static {v6, v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
