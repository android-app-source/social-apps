.class public final LX/INS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

.field public final synthetic c:Z

.field public final synthetic d:Lcom/facebook/groups/community/units/CommunityInviteFriendsUnit;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/community/units/CommunityInviteFriendsUnit;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;Z)V
    .locals 0

    .prologue
    .line 2570814
    iput-object p1, p0, LX/INS;->d:Lcom/facebook/groups/community/units/CommunityInviteFriendsUnit;

    iput-object p2, p0, LX/INS;->a:Ljava/lang/String;

    iput-object p3, p0, LX/INS;->b:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    iput-boolean p4, p0, LX/INS;->c:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, 0x676b2e32

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2570815
    iget-object v0, p0, LX/INS;->d:Lcom/facebook/groups/community/units/CommunityInviteFriendsUnit;

    const-string v2, "community_invite_friends_see_all"

    iget-object v3, p0, LX/INS;->a:Ljava/lang/String;

    invoke-static {v0, v2, v3}, Lcom/facebook/groups/community/units/CommunityInviteFriendsUnit;->a$redex0(Lcom/facebook/groups/community/units/CommunityInviteFriendsUnit;Ljava/lang/String;Ljava/lang/String;)V

    .line 2570816
    iget-object v0, p0, LX/INS;->d:Lcom/facebook/groups/community/units/CommunityInviteFriendsUnit;

    iget-object v0, v0, Lcom/facebook/groups/community/units/CommunityInviteFriendsUnit;->d:LX/DVF;

    iget-object v2, p0, LX/INS;->a:Ljava/lang/String;

    iget-object v3, p0, LX/INS;->b:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    iget-boolean v4, p0, LX/INS;->c:Z

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-interface {v0, v2, v3, v4, v5}, LX/DVF;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;ZLandroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    .line 2570817
    iget-object v0, p0, LX/INS;->d:Lcom/facebook/groups/community/units/CommunityInviteFriendsUnit;

    iget-object v0, v0, Lcom/facebook/groups/community/units/CommunityInviteFriendsUnit;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2570818
    const v0, 0x41b59b48

    invoke-static {v6, v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
