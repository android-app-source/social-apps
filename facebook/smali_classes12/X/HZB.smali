.class public final LX/HZB;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;)V
    .locals 0

    .prologue
    .line 2481690
    iput-object p1, p0, LX/HZB;->a:Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 1

    .prologue
    .line 2481691
    iget-object v0, p0, LX/HZB;->a:Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;

    invoke-static {v0, p1}, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->b(Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;Lcom/facebook/fbservice/service/ServiceException;)V

    .line 2481692
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 11

    .prologue
    .line 2481693
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 2481694
    iget-object v0, p0, LX/HZB;->a:Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;

    const/4 v4, 0x1

    .line 2481695
    if-nez p1, :cond_0

    .line 2481696
    const-string v1, "Malformed success response"

    invoke-static {v0, v1}, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->b(Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;Ljava/lang/String;)V

    .line 2481697
    :goto_0
    return-void

    .line 2481698
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/registration/protocol/RegisterAccountMethod$Result;

    .line 2481699
    iget-object v2, v0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->e:Lcom/facebook/registration/model/SimpleRegFormData;

    .line 2481700
    iput-object v1, v2, Lcom/facebook/registration/model/SimpleRegFormData;->e:Lcom/facebook/registration/protocol/RegisterAccountMethod$Result;

    .line 2481701
    if-nez v1, :cond_1

    .line 2481702
    const-string v1, "Malformed success result"

    invoke-static {v0, v1}, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->b(Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;Ljava/lang/String;)V

    goto :goto_0

    .line 2481703
    :cond_1
    iget-object v2, v1, Lcom/facebook/registration/protocol/RegisterAccountMethod$Result;->completionUrl:Ljava/lang/String;

    invoke-static {v2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 2481704
    iget-object v2, v0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->c:LX/HZv;

    .line 2481705
    iget-object v5, v2, LX/HZv;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v6, 0x400001

    const-string v7, "AccountCreationTime"

    const/4 v8, 0x0

    const-string v9, "result"

    const-string v10, "completion_url"

    invoke-interface/range {v5 .. v10}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2481706
    iget-object v2, v0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->f:LX/HZt;

    .line 2481707
    iget-object v3, v2, LX/HZt;->a:LX/0Zb;

    sget-object v5, LX/HZu;->ACCOUNT_CREATION_NEEDS_COMPLETION:LX/HZu;

    invoke-static {v2, v5}, LX/HZt;->a(LX/HZt;LX/HZu;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-interface {v3, v5}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2481708
    const-string v3, "create_needs_completion"

    invoke-static {v2, v3}, LX/HZt;->i(LX/HZt;Ljava/lang/String;)V

    .line 2481709
    iget-object v2, v0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->e:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v2, v4}, Lcom/facebook/registration/model/RegistrationFormData;->d(Z)V

    .line 2481710
    iget-object v1, v1, Lcom/facebook/registration/protocol/RegisterAccountMethod$Result;->completionUrl:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->a(Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;Ljava/lang/String;)V

    goto :goto_0

    .line 2481711
    :cond_2
    iget-object v2, v1, Lcom/facebook/registration/protocol/RegisterAccountMethod$Result;->userId:Ljava/lang/String;

    invoke-static {v2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 2481712
    iget-object v2, v1, Lcom/facebook/registration/protocol/RegisterAccountMethod$Result;->userId:Ljava/lang/String;

    .line 2481713
    :goto_1
    invoke-static {v2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2481714
    const-string v1, "Create without uid/contactpoint"

    invoke-static {v0, v1}, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->b(Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;Ljava/lang/String;)V

    goto :goto_0

    .line 2481715
    :cond_3
    iget-object v2, v0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->e:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v2}, Lcom/facebook/registration/model/RegistrationFormData;->d()Lcom/facebook/growth/model/ContactpointType;

    move-result-object v2

    sget-object v3, Lcom/facebook/growth/model/ContactpointType;->PHONE:Lcom/facebook/growth/model/ContactpointType;

    if-ne v2, v3, :cond_4

    iget-object v2, v0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->e:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v2}, Lcom/facebook/registration/model/RegistrationFormData;->e()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_4
    iget-object v2, v0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->e:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v2}, Lcom/facebook/registration/model/RegistrationFormData;->getEmail()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 2481716
    :cond_5
    iget-object v2, v1, Lcom/facebook/registration/protocol/RegisterAccountMethod$Result;->accountType:Ljava/lang/String;

    iget-object v3, v1, Lcom/facebook/registration/protocol/RegisterAccountMethod$Result;->userId:Ljava/lang/String;

    .line 2481717
    const-string v5, "full_bypass"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_6

    const-string v5, "bypass_without_login"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 2481718
    :cond_6
    iget-object v5, v0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->e:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v5}, Lcom/facebook/registration/model/RegistrationFormData;->d()Lcom/facebook/growth/model/ContactpointType;

    move-result-object v5

    sget-object v6, Lcom/facebook/growth/model/ContactpointType;->PHONE:Lcom/facebook/growth/model/ContactpointType;

    if-ne v5, v6, :cond_b

    .line 2481719
    iget-object v5, v0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->e:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v5}, Lcom/facebook/registration/model/RegistrationFormData;->e()Ljava/lang/String;

    move-result-object v5

    iget-object v6, v0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->e:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v6}, Lcom/facebook/registration/model/RegistrationFormData;->getPhoneIsoCountryCode()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/facebook/growth/model/Contactpoint;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/growth/model/Contactpoint;

    move-result-object v6

    .line 2481720
    iget-object v5, v0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->b:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/2U8;

    sget-object v7, LX/2UD;->EXPERIMENTAL_SMS_CONFIRMATION:LX/2UD;

    invoke-virtual {v5, v7, v6, v3}, LX/2U8;->a(LX/2UD;Lcom/facebook/growth/model/Contactpoint;Ljava/lang/String;)Z

    .line 2481721
    iget-object v5, v0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->e:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v5}, Lcom/facebook/registration/model/RegistrationFormData;->j()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_7

    .line 2481722
    iget-object v5, v0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->b:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/2U8;

    sget-object v6, LX/2UD;->OPENID_CONNECT_EMAIL_CONFIRMATION:LX/2UD;

    iget-object v7, v0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->e:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v7}, Lcom/facebook/registration/model/RegistrationFormData;->j()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/facebook/growth/model/Contactpoint;->a(Ljava/lang/String;)Lcom/facebook/growth/model/Contactpoint;

    move-result-object v7

    invoke-virtual {v5, v6, v7, v3}, LX/2U8;->a(LX/2UD;Lcom/facebook/growth/model/Contactpoint;Ljava/lang/String;)Z

    .line 2481723
    :cond_7
    :goto_2
    iget-object v2, v0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->e:Lcom/facebook/registration/model/SimpleRegFormData;

    .line 2481724
    iget-object v3, v2, Lcom/facebook/registration/model/SimpleRegFormData;->e:Lcom/facebook/registration/protocol/RegisterAccountMethod$Result;

    move-object v2, v3

    .line 2481725
    iget-object v2, v2, Lcom/facebook/registration/protocol/RegisterAccountMethod$Result;->userId:Ljava/lang/String;

    .line 2481726
    iget-object v3, v0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->l:LX/HaQ;

    invoke-virtual {v3}, LX/HaQ;->a()Z

    move-result v3

    if-nez v3, :cond_8

    invoke-static {v2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 2481727
    iget-object v3, v0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->k:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    invoke-static {v2}, LX/1nR;->b(Ljava/lang/String;)LX/0Tn;

    move-result-object v5

    iget-object v6, v0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->e:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v6}, Lcom/facebook/registration/model/RegistrationFormData;->c()Z

    move-result v6

    invoke-interface {v3, v5, v6}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v3

    invoke-interface {v3}, LX/0hN;->commit()V

    .line 2481728
    :cond_8
    invoke-static {v2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_9

    .line 2481729
    iget-object v3, v0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->k:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    invoke-static {v2}, LX/0uQ;->a(Ljava/lang/String;)LX/0Tn;

    move-result-object v2

    const/4 v5, 0x1

    invoke-interface {v3, v2, v5}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 2481730
    :cond_9
    iget-object v2, v0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->c:LX/HZv;

    .line 2481731
    iget-object v5, v2, LX/HZv;->a:Lcom/facebook/performancelogger/PerformanceLogger;

    const v6, 0x400001

    const-string v7, "AccountCreationTime"

    const/4 v8, 0x0

    const-string v9, "result"

    const-string v10, "success"

    invoke-interface/range {v5 .. v10}, Lcom/facebook/performancelogger/PerformanceLogger;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2481732
    iget-object v2, v0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->f:LX/HZt;

    iget-object v3, v1, Lcom/facebook/registration/protocol/RegisterAccountMethod$Result;->accountType:Ljava/lang/String;

    iget-object v1, v1, Lcom/facebook/registration/protocol/RegisterAccountMethod$Result;->userId:Ljava/lang/String;

    .line 2481733
    iget-object v5, v2, LX/HZt;->a:LX/0Zb;

    sget-object v6, LX/HZu;->ACCOUNT_CREATION_SUCCESS:LX/HZu;

    invoke-static {v2, v6}, LX/HZt;->a(LX/HZt;LX/HZu;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "created_account_type"

    invoke-virtual {v6, v7, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "userId"

    invoke-virtual {v6, v7, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    invoke-interface {v5, v6}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2481734
    sget-object v5, LX/0ig;->e:LX/0ih;

    const/16 v6, 0x258

    .line 2481735
    iput v6, v5, LX/0ih;->c:I

    .line 2481736
    iget-object v5, v2, LX/HZt;->f:LX/0if;

    sget-object v6, LX/0ig;->e:LX/0ih;

    invoke-virtual {v5, v6}, LX/0if;->a(LX/0ih;)V

    .line 2481737
    iget-object v1, v0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->e:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v1, v4}, Lcom/facebook/registration/model/RegistrationFormData;->d(Z)V

    .line 2481738
    iget-object v1, v0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->e:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v1}, Lcom/facebook/registration/model/RegistrationFormData;->d()Lcom/facebook/growth/model/ContactpointType;

    move-result-object v1

    sget-object v2, Lcom/facebook/growth/model/ContactpointType;->PHONE:Lcom/facebook/growth/model/ContactpointType;

    if-ne v1, v2, :cond_a

    .line 2481739
    iget-object v1, v0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->u:LX/0i5;

    sget-object v2, LX/HYk;->b:[Ljava/lang/String;

    new-instance v3, LX/HZC;

    invoke-direct {v3, v0}, LX/HZC;-><init>(Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;)V

    invoke-virtual {v1, v2, v3}, LX/0i5;->a([Ljava/lang/String;LX/6Zx;)V

    .line 2481740
    goto/16 :goto_0

    .line 2481741
    :cond_a
    invoke-static {v0}, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->o(Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;)V

    goto/16 :goto_0

    .line 2481742
    :cond_b
    iget-object v5, v0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->e:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v5}, Lcom/facebook/registration/model/RegistrationFormData;->d()Lcom/facebook/growth/model/ContactpointType;

    move-result-object v5

    sget-object v6, Lcom/facebook/growth/model/ContactpointType;->EMAIL:Lcom/facebook/growth/model/ContactpointType;

    if-ne v5, v6, :cond_7

    .line 2481743
    iget-object v5, v0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->e:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v5}, Lcom/facebook/registration/model/RegistrationFormData;->getEmail()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/growth/model/Contactpoint;->a(Ljava/lang/String;)Lcom/facebook/growth/model/Contactpoint;

    move-result-object v6

    .line 2481744
    iget-object v5, v0, Lcom/facebook/registration/fragment/RegistrationCreateAccountFragment;->b:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/2U8;

    sget-object v7, LX/2UD;->OPENID_CONNECT_EMAIL_CONFIRMATION:LX/2UD;

    invoke-virtual {v5, v7, v6, v3}, LX/2U8;->a(LX/2UD;Lcom/facebook/growth/model/Contactpoint;Ljava/lang/String;)Z

    goto/16 :goto_2
.end method
