.class public final LX/HWp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V
    .locals 0

    .prologue
    .line 2477080
    iput-object p1, p0, LX/HWp;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 2477081
    const/4 v0, 0x0

    .line 2477082
    iget-object v2, p0, LX/HWp;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v2, v2, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->Z:Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;

    invoke-virtual {v2}, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->getMeasuredHeight()I

    move-result v2

    iget-object v3, p0, LX/HWp;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget v3, v3, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ax:I

    if-eq v2, v3, :cond_0

    .line 2477083
    iget-object v0, p0, LX/HWp;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v2, p0, LX/HWp;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v2, v2, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->Z:Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;

    invoke-virtual {v2}, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->getMeasuredHeight()I

    move-result v2

    .line 2477084
    iput v2, v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ax:I

    .line 2477085
    move v0, v1

    .line 2477086
    :cond_0
    iget-object v2, p0, LX/HWp;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v2, v2, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aa:LX/63T;

    if-eqz v2, :cond_1

    iget-object v2, p0, LX/HWp;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v2, v2, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aa:LX/63T;

    invoke-interface {v2}, LX/63T;->getHeight()I

    move-result v2

    iget-object v3, p0, LX/HWp;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget v3, v3, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aB:I

    if-eq v2, v3, :cond_1

    .line 2477087
    iget-object v0, p0, LX/HWp;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v2, p0, LX/HWp;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v2, v2, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aa:LX/63T;

    invoke-interface {v2}, LX/63T;->getHeight()I

    move-result v2

    .line 2477088
    iput v2, v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aB:I

    .line 2477089
    move v0, v1

    .line 2477090
    :cond_1
    if-eqz v0, :cond_2

    .line 2477091
    iget-object v0, p0, LX/HWp;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v1, p0, LX/HWp;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget v1, v1, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->az:I

    invoke-static {v0, v1}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->e$redex0(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;I)V

    .line 2477092
    iget-object v0, p0, LX/HWp;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v1, p0, LX/HWp;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget v1, v1, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->az:I

    invoke-static {v0, v1}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->h(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;I)V

    .line 2477093
    :cond_2
    return-void
.end method
