.class public final LX/HOn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8uH;


# instance fields
.field public final synthetic a:LX/HOU;

.field public final synthetic b:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithEntitiesView;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithEntitiesView;LX/HOU;)V
    .locals 0

    .prologue
    .line 2460881
    iput-object p1, p0, LX/HOn;->b:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithEntitiesView;

    iput-object p2, p0, LX/HOn;->a:LX/HOU;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/1W5;)V
    .locals 4

    .prologue
    .line 2460873
    if-eqz p1, :cond_0

    invoke-interface {p1}, LX/1W5;->a()LX/171;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2460874
    :cond_0
    :goto_0
    return-void

    .line 2460875
    :cond_1
    iget-object v0, p0, LX/HOn;->b:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithEntitiesView;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithEntitiesView;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1nG;

    invoke-interface {p1}, LX/1W5;->a()LX/171;

    move-result-object v1

    invoke-static {v1}, LX/2yc;->a(LX/171;)LX/1yA;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1nG;->a(LX/1yA;)Ljava/lang/String;

    move-result-object v0

    .line 2460876
    iget-object v1, p0, LX/HOn;->a:LX/HOU;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-interface {v1, v2}, LX/HOU;->a([Ljava/lang/String;)V

    .line 2460877
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2460878
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2460879
    const/high16 v0, 0x10000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2460880
    iget-object v0, p0, LX/HOn;->b:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithEntitiesView;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithEntitiesView;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/HOn;->b:Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithEntitiesView;

    invoke-virtual {v2}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithEntitiesView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method
