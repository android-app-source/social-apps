.class public final LX/INY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/INZ;


# direct methods
.method public constructor <init>(LX/INZ;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2570912
    iput-object p1, p0, LX/INY;->d:LX/INZ;

    iput-object p2, p0, LX/INY;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;

    iput-object p3, p0, LX/INY;->b:Ljava/lang/String;

    iput-object p4, p0, LX/INY;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x2

    const v0, 0x45e4f03c

    invoke-static {v6, v7, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2570913
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v0

    const-string v2, "question_id"

    iget-object v3, p0, LX/INY;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    const-string v2, "community_id"

    iget-object v3, p0, LX/INY;->b:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    .line 2570914
    iget-object v2, p0, LX/INY;->d:LX/INZ;

    iget-object v2, v2, LX/INZ;->g:LX/0if;

    sget-object v3, LX/0ig;->aS:LX/0ih;

    const-string v4, "did_tap_on_question"

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5, v0}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2570915
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 2570916
    iget-object v0, p0, LX/INY;->d:LX/INZ;

    iget-object v0, v0, LX/INZ;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 2570917
    const-string v0, "target_fragment"

    sget-object v3, LX/0cQ;->COMMUNITY_SEARCH_FRAGMENT:LX/0cQ;

    invoke-virtual {v3}, LX/0cQ;->ordinal()I

    move-result v3

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2570918
    const-string v0, "group_id"

    iget-object v3, p0, LX/INY;->b:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2570919
    const-string v0, "group_name"

    iget-object v3, p0, LX/INY;->c:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2570920
    const-string v0, "community_nux_question"

    iget-object v3, p0, LX/INY;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;

    invoke-static {v2, v0, v3}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2570921
    iget-object v0, p0, LX/INY;->d:LX/INZ;

    iget-object v0, v0, LX/INZ;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    sget-object v0, LX/ILq;->c:LX/0Tn;

    iget-object v4, p0, LX/INY;->b:Ljava/lang/String;

    invoke-virtual {v0, v4}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iget-object v4, p0, LX/INY;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-interface {v3, v0, v7}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2570922
    iget-object v0, p0, LX/INY;->d:LX/INZ;

    iget-object v3, v0, LX/INZ;->b:Lcom/facebook/content/SecureContextHelper;

    const/16 v4, 0x12c

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-interface {v3, v2, v4, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2570923
    const v0, 0x50e9f803

    invoke-static {v6, v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
