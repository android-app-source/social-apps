.class public LX/HVE;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2474867
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;Ljava/lang/String;Z)Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;
    .locals 9

    .prologue
    .line 2474868
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->m()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->m()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2474869
    :cond_0
    :goto_0
    return-object p0

    .line 2474870
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->bV_()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel;

    move-result-object v1

    .line 2474871
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel;->d()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel;->d()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel;->cb_()Z

    move-result v0

    if-eq v0, p2, :cond_0

    .line 2474872
    if-eqz p2, :cond_2

    const/4 v0, 0x1

    .line 2474873
    :goto_1
    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel;->d()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel;->k()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel$LikersModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel$LikersModel;->a()I

    move-result v1

    add-int/2addr v0, v1

    .line 2474874
    invoke-static {p0}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->a(Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;)Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

    move-result-object v1

    .line 2474875
    new-instance v5, LX/5hV;

    invoke-direct {v5}, LX/5hV;-><init>()V

    .line 2474876
    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->b()Z

    move-result v6

    iput-boolean v6, v5, LX/5hV;->a:Z

    .line 2474877
    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->c()Z

    move-result v6

    iput-boolean v6, v5, LX/5hV;->b:Z

    .line 2474878
    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->d()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, LX/5hV;->c:Ljava/lang/String;

    .line 2474879
    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->e()J

    move-result-wide v7

    iput-wide v7, v5, LX/5hV;->d:J

    .line 2474880
    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->O()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel;

    move-result-object v6

    iput-object v6, v5, LX/5hV;->e:Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel;

    .line 2474881
    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->bU_()Z

    move-result v6

    iput-boolean v6, v5, LX/5hV;->f:Z

    .line 2474882
    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->j()D

    move-result-wide v7

    iput-wide v7, v5, LX/5hV;->g:D

    .line 2474883
    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->P()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    move-result-object v6

    iput-object v6, v5, LX/5hV;->h:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    .line 2474884
    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->l()I

    move-result v6

    iput v6, v5, LX/5hV;->i:I

    .line 2474885
    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->m()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, LX/5hV;->j:Ljava/lang/String;

    .line 2474886
    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->n()I

    move-result v6

    iput v6, v5, LX/5hV;->k:I

    .line 2474887
    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->o()I

    move-result v6

    iput v6, v5, LX/5hV;->l:I

    .line 2474888
    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->p()I

    move-result v6

    iput v6, v5, LX/5hV;->m:I

    .line 2474889
    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->q()Z

    move-result v6

    iput-boolean v6, v5, LX/5hV;->n:Z

    .line 2474890
    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->r()Z

    move-result v6

    iput-boolean v6, v5, LX/5hV;->o:Z

    .line 2474891
    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->s()Z

    move-result v6

    iput-boolean v6, v5, LX/5hV;->p:Z

    .line 2474892
    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->t()Z

    move-result v6

    iput-boolean v6, v5, LX/5hV;->q:Z

    .line 2474893
    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->u()I

    move-result v6

    iput v6, v5, LX/5hV;->r:I

    .line 2474894
    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->Q()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v6

    iput-object v6, v5, LX/5hV;->s:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2474895
    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->w()D

    move-result-wide v7

    iput-wide v7, v5, LX/5hV;->t:D

    .line 2474896
    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->x()I

    move-result v6

    iput v6, v5, LX/5hV;->u:I

    .line 2474897
    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->y()I

    move-result v6

    iput v6, v5, LX/5hV;->v:I

    .line 2474898
    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->z()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, LX/5hV;->w:Ljava/lang/String;

    .line 2474899
    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->A()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, LX/5hV;->x:Ljava/lang/String;

    .line 2474900
    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->B()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, LX/5hV;->y:Ljava/lang/String;

    .line 2474901
    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->C()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, LX/5hV;->z:Ljava/lang/String;

    .line 2474902
    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->D()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, LX/5hV;->A:Ljava/lang/String;

    .line 2474903
    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->E()D

    move-result-wide v7

    iput-wide v7, v5, LX/5hV;->B:D

    .line 2474904
    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->F()D

    move-result-wide v7

    iput-wide v7, v5, LX/5hV;->C:D

    .line 2474905
    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->G()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, LX/5hV;->D:Ljava/lang/String;

    .line 2474906
    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->H()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, LX/5hV;->E:Ljava/lang/String;

    .line 2474907
    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->I()I

    move-result v6

    iput v6, v5, LX/5hV;->F:I

    .line 2474908
    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->J()Z

    move-result v6

    iput-boolean v6, v5, LX/5hV;->G:Z

    .line 2474909
    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->R()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v6

    iput-object v6, v5, LX/5hV;->H:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2474910
    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->S()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v6

    iput-object v6, v5, LX/5hV;->I:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2474911
    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->M()LX/0Px;

    move-result-object v6

    iput-object v6, v5, LX/5hV;->J:LX/0Px;

    .line 2474912
    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->N()I

    move-result v6

    iput v6, v5, LX/5hV;->K:I

    .line 2474913
    move-object v2, v5

    .line 2474914
    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->O()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel;

    move-result-object v3

    .line 2474915
    new-instance v4, LX/5hi;

    invoke-direct {v4}, LX/5hi;-><init>()V

    .line 2474916
    invoke-virtual {v3}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel;->b()LX/0Px;

    move-result-object v5

    iput-object v5, v4, LX/5hi;->a:LX/0Px;

    .line 2474917
    invoke-virtual {v3}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel;->c()LX/0Px;

    move-result-object v5

    iput-object v5, v4, LX/5hi;->b:LX/0Px;

    .line 2474918
    invoke-virtual {v3}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel;->l()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel;

    move-result-object v5

    iput-object v5, v4, LX/5hi;->c:Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel;

    .line 2474919
    invoke-virtual {v3}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel;->e()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, LX/5hi;->d:Ljava/lang/String;

    .line 2474920
    invoke-virtual {v3}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel;->m()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v5

    iput-object v5, v4, LX/5hi;->e:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2474921
    invoke-virtual {v3}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel;->n()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$ShareableModel;

    move-result-object v5

    iput-object v5, v4, LX/5hi;->f:Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$ShareableModel;

    .line 2474922
    invoke-virtual {v3}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel;->o()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v5

    iput-object v5, v4, LX/5hi;->g:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2474923
    invoke-virtual {v3}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel;->p()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v5

    iput-object v5, v4, LX/5hi;->h:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2474924
    move-object v3, v4

    .line 2474925
    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->O()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel;->l()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel;

    move-result-object v4

    .line 2474926
    new-instance v5, LX/5hj;

    invoke-direct {v5}, LX/5hj;-><init>()V

    .line 2474927
    invoke-virtual {v4}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel;->b()Z

    move-result v6

    iput-boolean v6, v5, LX/5hj;->a:Z

    .line 2474928
    invoke-virtual {v4}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel;->c()Z

    move-result v6

    iput-boolean v6, v5, LX/5hj;->b:Z

    .line 2474929
    invoke-virtual {v4}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel;->d()Z

    move-result v6

    iput-boolean v6, v5, LX/5hj;->c:Z

    .line 2474930
    invoke-virtual {v4}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel;->l()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel$CommentsModel;

    move-result-object v6

    iput-object v6, v5, LX/5hj;->d:Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel$CommentsModel;

    .line 2474931
    invoke-virtual {v4}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel;->cb_()Z

    move-result v6

    iput-boolean v6, v5, LX/5hj;->e:Z

    .line 2474932
    invoke-virtual {v4}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel;->cc_()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, LX/5hj;->f:Ljava/lang/String;

    .line 2474933
    invoke-virtual {v4}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel;->j()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, LX/5hj;->g:Ljava/lang/String;

    .line 2474934
    invoke-virtual {v4}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel;->m()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel$LikersModel;

    move-result-object v6

    iput-object v6, v5, LX/5hj;->h:Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel$LikersModel;

    .line 2474935
    move-object v4, v5

    .line 2474936
    iput-boolean p2, v4, LX/5hj;->e:Z

    .line 2474937
    move-object v4, v4

    .line 2474938
    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->O()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel;->l()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel;->m()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel$LikersModel;

    move-result-object v1

    .line 2474939
    new-instance v5, LX/5hl;

    invoke-direct {v5}, LX/5hl;-><init>()V

    .line 2474940
    invoke-virtual {v1}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel$LikersModel;->a()I

    move-result v6

    iput v6, v5, LX/5hl;->a:I

    .line 2474941
    move-object v1, v5

    .line 2474942
    iput v0, v1, LX/5hl;->a:I

    .line 2474943
    move-object v0, v1

    .line 2474944
    invoke-virtual {v0}, LX/5hl;->a()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel$LikersModel;

    move-result-object v0

    .line 2474945
    iput-object v0, v4, LX/5hj;->h:Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel$LikersModel;

    .line 2474946
    move-object v0, v4

    .line 2474947
    invoke-virtual {v0}, LX/5hj;->a()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel;

    move-result-object v0

    .line 2474948
    iput-object v0, v3, LX/5hi;->c:Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel$FeedbackModel;

    .line 2474949
    move-object v0, v3

    .line 2474950
    invoke-virtual {v0}, LX/5hi;->a()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel;

    move-result-object v0

    .line 2474951
    iput-object v0, v2, LX/5hV;->e:Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel$CreationStoryModel;

    .line 2474952
    move-object v0, v2

    .line 2474953
    invoke-virtual {v0}, LX/5hV;->a()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

    move-result-object p0

    goto/16 :goto_0

    .line 2474954
    :cond_2
    const/4 v0, -0x1

    goto/16 :goto_1
.end method
