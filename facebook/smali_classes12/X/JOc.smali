.class public final LX/JOc;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JOe;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

.field public c:LX/1Pn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public d:LX/2dx;

.field public final synthetic e:LX/JOe;


# direct methods
.method public constructor <init>(LX/JOe;)V
    .locals 1

    .prologue
    .line 2687891
    iput-object p1, p0, LX/JOc;->e:LX/JOe;

    .line 2687892
    move-object v0, p1

    .line 2687893
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2687894
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2687915
    const-string v0, "AdsMultiCampaignsInsightsCardComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2687895
    if-ne p0, p1, :cond_1

    .line 2687896
    :cond_0
    :goto_0
    return v0

    .line 2687897
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2687898
    goto :goto_0

    .line 2687899
    :cond_3
    check-cast p1, LX/JOc;

    .line 2687900
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2687901
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2687902
    if-eq v2, v3, :cond_0

    .line 2687903
    iget-object v2, p0, LX/JOc;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JOc;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/JOc;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2687904
    goto :goto_0

    .line 2687905
    :cond_5
    iget-object v2, p1, LX/JOc;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 2687906
    :cond_6
    iget-object v2, p0, LX/JOc;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/JOc;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    iget-object v3, p1, LX/JOc;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2687907
    goto :goto_0

    .line 2687908
    :cond_8
    iget-object v2, p1, LX/JOc;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    if-nez v2, :cond_7

    .line 2687909
    :cond_9
    iget-object v2, p0, LX/JOc;->c:LX/1Pn;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/JOc;->c:LX/1Pn;

    iget-object v3, p1, LX/JOc;->c:LX/1Pn;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2687910
    goto :goto_0

    .line 2687911
    :cond_b
    iget-object v2, p1, LX/JOc;->c:LX/1Pn;

    if-nez v2, :cond_a

    .line 2687912
    :cond_c
    iget-object v2, p0, LX/JOc;->d:LX/2dx;

    if-eqz v2, :cond_d

    iget-object v2, p0, LX/JOc;->d:LX/2dx;

    iget-object v3, p1, LX/JOc;->d:LX/2dx;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2687913
    goto :goto_0

    .line 2687914
    :cond_d
    iget-object v2, p1, LX/JOc;->d:LX/2dx;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
