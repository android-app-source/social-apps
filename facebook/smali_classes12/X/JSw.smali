.class public LX/JSw;
.super Landroid/view/animation/Animation;
.source ""


# instance fields
.field private final a:Lcom/facebook/feedplugins/musicstory/animations/VinylView;

.field private final b:LX/JSv;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/musicstory/animations/VinylView;LX/JSv;)V
    .locals 1

    .prologue
    .line 2696212
    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    .line 2696213
    iput-object p1, p0, LX/JSw;->a:Lcom/facebook/feedplugins/musicstory/animations/VinylView;

    .line 2696214
    iput-object p2, p0, LX/JSw;->b:LX/JSv;

    .line 2696215
    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {p0, v0}, LX/JSw;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 2696216
    return-void
.end method


# virtual methods
.method public final applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 2

    .prologue
    .line 2696217
    iget-object v0, p0, LX/JSw;->b:LX/JSv;

    sget-object v1, LX/JSv;->SQUARE_TO_CIRCLE:LX/JSv;

    if-ne v0, v1, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    sub-float p1, v0, p1

    .line 2696218
    :cond_0
    iget-object v0, p0, LX/JSw;->a:Lcom/facebook/feedplugins/musicstory/animations/VinylView;

    invoke-virtual {v0, p1}, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->setRectangularity(F)V

    .line 2696219
    iget-object v0, p0, LX/JSw;->a:Lcom/facebook/feedplugins/musicstory/animations/VinylView;

    invoke-virtual {v0}, Lcom/facebook/feedplugins/musicstory/animations/VinylView;->requestLayout()V

    .line 2696220
    return-void
.end method
