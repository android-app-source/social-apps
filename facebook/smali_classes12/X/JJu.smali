.class public LX/JJu;
.super LX/5pb;
.source ""

# interfaces
.implements LX/5on;


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "FBProfileNativeModule"
.end annotation


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field private b:LX/5pW;


# direct methods
.method public constructor <init>(LX/0Or;LX/5pY;)V
    .locals 0
    .param p2    # LX/5pY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/5pY;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2679912
    invoke-direct {p0, p2}, LX/5pb;-><init>(LX/5pY;)V

    .line 2679913
    iput-object p1, p0, LX/JJu;->a:LX/0Or;

    .line 2679914
    invoke-virtual {p2, p0}, LX/5pX;->a(LX/5on;)V

    .line 2679915
    return-void
.end method

.method private static a(Ljava/util/List;Ljava/util/List;)LX/5pC;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/5pC;"
        }
    .end annotation

    .prologue
    .line 2679903
    new-instance v2, Lcom/facebook/react/bridge/WritableNativeArray;

    invoke-direct {v2}, Lcom/facebook/react/bridge/WritableNativeArray;-><init>()V

    .line 2679904
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2679905
    new-instance v3, Lcom/facebook/react/bridge/WritableNativeMap;

    invoke-direct {v3}, Lcom/facebook/react/bridge/WritableNativeMap;-><init>()V

    .line 2679906
    const-string v4, "key"

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, Lcom/facebook/react/bridge/WritableNativeMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2679907
    const-string v4, "text"

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, Lcom/facebook/react/bridge/WritableNativeMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2679908
    const-string v0, "selected"

    const/4 v4, 0x1

    invoke-virtual {v3, v0, v4}, Lcom/facebook/react/bridge/WritableNativeMap;->putBoolean(Ljava/lang/String;Z)V

    .line 2679909
    invoke-virtual {v2, v3}, Lcom/facebook/react/bridge/WritableNativeArray;->a(LX/5pH;)V

    .line 2679910
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2679911
    :cond_0
    return-object v2
.end method


# virtual methods
.method public final a(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2679888
    iget-object v0, p0, LX/JJu;->b:LX/5pW;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    .line 2679889
    :cond_0
    :goto_0
    return-void

    .line 2679890
    :cond_1
    const/4 v0, -0x1

    if-eq p2, v0, :cond_2

    .line 2679891
    iget-object v0, p0, LX/JJu;->b:LX/5pW;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v1, v2}, LX/JJu;->a(Ljava/util/List;Ljava/util/List;)LX/5pC;

    move-result-object v1

    invoke-interface {v0, v1}, LX/5pW;->a(Ljava/lang/Object;)V

    .line 2679892
    iput-object v3, p0, LX/JJu;->b:LX/5pW;

    goto :goto_0

    .line 2679893
    :cond_2
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 2679894
    :pswitch_0
    iget-object v0, p0, LX/JJu;->b:LX/5pW;

    const-string v1, "typeahead_result_ids_extra_key"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    const-string v2, "typeahead_result_names_extra_key"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {v1, v2}, LX/JJu;->a(Ljava/util/List;Ljava/util/List;)LX/5pC;

    move-result-object v1

    invoke-interface {v0, v1}, LX/5pW;->a(Ljava/lang/Object;)V

    .line 2679895
    iput-object v3, p0, LX/JJu;->b:LX/5pW;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2679902
    const-string v0, "FBProfileNativeModule"

    return-object v0
.end method

.method public openTagsTypeaheadViewWithReactTag(ILX/5pW;)V
    .locals 4
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2679896
    iput-object p2, p0, LX/JJu;->b:LX/5pW;

    .line 2679897
    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v1

    .line 2679898
    new-instance v2, Landroid/content/Intent;

    const-class v0, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadActivity;

    invoke-direct {v2, v1, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2679899
    const-string v0, "typeahead_launcher_extra_key"

    const-string v3, "react_native_launcher"

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2679900
    iget-object v0, p0, LX/JJu;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2679901
    return-void
.end method
