.class public final LX/Ic3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/widget/text/BetterEditTextView;

.field public final synthetic c:LX/IcK;

.field public final synthetic d:LX/15i;

.field public final synthetic e:I

.field public final synthetic f:LX/Ic4;


# direct methods
.method public constructor <init>(LX/Ic4;Ljava/lang/String;Lcom/facebook/widget/text/BetterEditTextView;LX/IcK;LX/15i;I)V
    .locals 0

    .prologue
    .line 2595036
    iput-object p1, p0, LX/Ic3;->f:LX/Ic4;

    iput-object p2, p0, LX/Ic3;->a:Ljava/lang/String;

    iput-object p3, p0, LX/Ic3;->b:Lcom/facebook/widget/text/BetterEditTextView;

    iput-object p4, p0, LX/Ic3;->c:LX/IcK;

    iput-object p5, p0, LX/Ic3;->d:LX/15i;

    iput p6, p0, LX/Ic3;->e:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 2595037
    iget-object v0, p0, LX/Ic3;->f:LX/Ic4;

    iget-object v0, v0, LX/Ic4;->h:LX/Ib2;

    const-string v1, "click_confirm_in_surge_dialog"

    invoke-virtual {v0, v1}, LX/Ib2;->b(Ljava/lang/String;)V

    .line 2595038
    iget-object v0, p0, LX/Ic3;->a:Ljava/lang/String;

    iget-object v1, p0, LX/Ic3;->b:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2595039
    iget-object v0, p0, LX/Ic3;->c:LX/IcK;

    iget-object v1, p0, LX/Ic3;->d:LX/15i;

    iget v2, p0, LX/Ic3;->e:I

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    .line 2595040
    iput-object v1, v0, LX/IcK;->m:Ljava/lang/String;

    .line 2595041
    iget-object v0, p0, LX/Ic3;->f:LX/Ic4;

    iget-object v1, p0, LX/Ic3;->c:LX/IcK;

    invoke-virtual {v0, v1}, LX/Ic4;->a(LX/IcK;)V

    .line 2595042
    :goto_0
    return-void

    .line 2595043
    :cond_0
    iget-object v0, p0, LX/Ic3;->f:LX/Ic4;

    iget-object v0, v0, LX/Ic4;->f:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, LX/Ic3;->b:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/text/BetterEditTextView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 2595044
    iget-object v0, p0, LX/Ic3;->f:LX/Ic4;

    iget-object v0, v0, LX/Ic4;->g:LX/IZK;

    iget-object v1, p0, LX/Ic3;->f:LX/Ic4;

    iget-object v1, v1, LX/Ic4;->b:Landroid/content/Context;

    const v2, 0x7f082d70

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/IZK;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
