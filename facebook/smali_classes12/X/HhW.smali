.class public LX/HhW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/HhW;


# instance fields
.field public final a:LX/0kb;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/Hhj;

.field private final d:LX/Hhl;

.field private final e:LX/HhZ;

.field public final f:LX/Hhh;

.field public final g:LX/0qK;


# direct methods
.method public constructor <init>(LX/0kb;LX/0Ot;LX/Hhj;LX/Hhl;LX/HhZ;LX/Hhh;LX/0SG;)V
    .locals 4
    .param p2    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0kb;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;",
            "LX/Hhj;",
            "LX/Hhl;",
            "LX/HhZ;",
            "LX/Hhh;",
            "LX/0SG;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2495604
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2495605
    iput-object p1, p0, LX/HhW;->a:LX/0kb;

    .line 2495606
    iput-object p2, p0, LX/HhW;->b:LX/0Ot;

    .line 2495607
    iput-object p3, p0, LX/HhW;->c:LX/Hhj;

    .line 2495608
    iput-object p4, p0, LX/HhW;->d:LX/Hhl;

    .line 2495609
    iput-object p5, p0, LX/HhW;->e:LX/HhZ;

    .line 2495610
    iput-object p6, p0, LX/HhW;->f:LX/Hhh;

    .line 2495611
    new-instance v0, LX/0qK;

    const/4 v1, 0x1

    const-wide/32 v2, 0x36ee80

    invoke-direct {v0, p7, v1, v2, v3}, LX/0qK;-><init>(LX/0SG;IJ)V

    iput-object v0, p0, LX/HhW;->g:LX/0qK;

    .line 2495612
    return-void
.end method

.method public static a(LX/0QB;)LX/HhW;
    .locals 12

    .prologue
    .line 2495613
    sget-object v0, LX/HhW;->h:LX/HhW;

    if-nez v0, :cond_1

    .line 2495614
    const-class v1, LX/HhW;

    monitor-enter v1

    .line 2495615
    :try_start_0
    sget-object v0, LX/HhW;->h:LX/HhW;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2495616
    if-eqz v2, :cond_0

    .line 2495617
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2495618
    new-instance v3, LX/HhW;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v4

    check-cast v4, LX/0kb;

    const/16 v5, 0x140d

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    .line 2495619
    new-instance v7, LX/Hhj;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v6

    check-cast v6, LX/0tX;

    const/16 v8, 0x140d

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-direct {v7, v6, v8}, LX/Hhj;-><init>(LX/0tX;LX/0Ot;)V

    .line 2495620
    move-object v6, v7

    .line 2495621
    check-cast v6, LX/Hhj;

    .line 2495622
    new-instance v9, LX/Hhl;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v7

    check-cast v7, LX/0tX;

    const/16 v8, 0x140d

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static {v0}, LX/1rd;->b(LX/0QB;)LX/1rd;

    move-result-object v8

    check-cast v8, LX/1rd;

    invoke-direct {v9, v7, v10, v8}, LX/Hhl;-><init>(LX/0tX;LX/0Ot;LX/1rd;)V

    .line 2495623
    move-object v7, v9

    .line 2495624
    check-cast v7, LX/Hhl;

    .line 2495625
    new-instance v8, LX/HhZ;

    const/16 v9, 0x140d

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-direct {v8, v9}, LX/HhZ;-><init>(LX/0Ot;)V

    .line 2495626
    move-object v8, v8

    .line 2495627
    check-cast v8, LX/HhZ;

    .line 2495628
    new-instance v10, LX/Hhh;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v9

    check-cast v9, LX/0W3;

    const/16 v11, 0x140d

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-direct {v10, v9, v11}, LX/Hhh;-><init>(LX/0W3;LX/0Ot;)V

    .line 2495629
    move-object v9, v10

    .line 2495630
    check-cast v9, LX/Hhh;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v10

    check-cast v10, LX/0SG;

    invoke-direct/range {v3 .. v10}, LX/HhW;-><init>(LX/0kb;LX/0Ot;LX/Hhj;LX/Hhl;LX/HhZ;LX/Hhh;LX/0SG;)V

    .line 2495631
    move-object v0, v3

    .line 2495632
    sput-object v0, LX/HhW;->h:LX/HhW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2495633
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2495634
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2495635
    :cond_1
    sget-object v0, LX/HhW;->h:LX/HhW;

    return-object v0

    .line 2495636
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2495637
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b()V
    .locals 5

    .prologue
    .line 2495638
    iget-object v0, p0, LX/HhW;->a:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2495639
    :goto_0
    return-void

    .line 2495640
    :cond_0
    iget-object v0, p0, LX/HhW;->c:LX/Hhj;

    const/4 v1, 0x5

    .line 2495641
    new-instance v2, LX/Hha;

    invoke-direct {v2}, LX/Hha;-><init>()V

    move-object v2, v2

    .line 2495642
    const-string v3, "num_tests"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2495643
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    sget-object v3, LX/0zS;->c:LX/0zS;

    invoke-virtual {v2, v3}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v2

    .line 2495644
    iget-object v3, v0, LX/Hhj;->a:LX/0tX;

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    .line 2495645
    iget-object v4, v0, LX/Hhj;->c:LX/0QK;

    iget-object v2, v0, LX/Hhj;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Executor;

    invoke-static {v3, v4, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v0, v2

    .line 2495646
    iget-object v1, p0, LX/HhW;->e:LX/HhZ;

    .line 2495647
    iget-object v3, v1, LX/HhZ;->b:LX/0QK;

    iget-object v2, v1, LX/HhZ;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Executor;

    invoke-static {v0, v3, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v0, v2

    .line 2495648
    iget-object v1, p0, LX/HhW;->d:LX/Hhl;

    .line 2495649
    iget-object v3, v1, LX/Hhl;->d:LX/0Vj;

    iget-object v2, v1, LX/Hhl;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Executor;

    invoke-static {v0, v3, v2}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v1, v2

    .line 2495650
    new-instance v2, LX/HhV;

    invoke-direct {v2, p0}, LX/HhV;-><init>(LX/HhW;)V

    iget-object v0, p0, LX/HhW;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
