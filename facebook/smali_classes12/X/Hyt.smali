.class public final enum LX/Hyt;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Hyt;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Hyt;

.field public static final enum DB_FETCH:LX/Hyt;

.field public static final enum FETCH_EVENT_COUNTS:LX/Hyt;

.field public static final enum FETCH_SINGLE_EVENT:LX/Hyt;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2524569
    new-instance v0, LX/Hyt;

    const-string v1, "DB_FETCH"

    invoke-direct {v0, v1, v2}, LX/Hyt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hyt;->DB_FETCH:LX/Hyt;

    .line 2524570
    new-instance v0, LX/Hyt;

    const-string v1, "FETCH_SINGLE_EVENT"

    invoke-direct {v0, v1, v3}, LX/Hyt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hyt;->FETCH_SINGLE_EVENT:LX/Hyt;

    .line 2524571
    new-instance v0, LX/Hyt;

    const-string v1, "FETCH_EVENT_COUNTS"

    invoke-direct {v0, v1, v4}, LX/Hyt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hyt;->FETCH_EVENT_COUNTS:LX/Hyt;

    .line 2524572
    const/4 v0, 0x3

    new-array v0, v0, [LX/Hyt;

    sget-object v1, LX/Hyt;->DB_FETCH:LX/Hyt;

    aput-object v1, v0, v2

    sget-object v1, LX/Hyt;->FETCH_SINGLE_EVENT:LX/Hyt;

    aput-object v1, v0, v3

    sget-object v1, LX/Hyt;->FETCH_EVENT_COUNTS:LX/Hyt;

    aput-object v1, v0, v4

    sput-object v0, LX/Hyt;->$VALUES:[LX/Hyt;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2524568
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Hyt;
    .locals 1

    .prologue
    .line 2524566
    const-class v0, LX/Hyt;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Hyt;

    return-object v0
.end method

.method public static values()[LX/Hyt;
    .locals 1

    .prologue
    .line 2524567
    sget-object v0, LX/Hyt;->$VALUES:[LX/Hyt;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Hyt;

    return-object v0
.end method
