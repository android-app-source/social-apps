.class public LX/JEs;
.super LX/4sY;
.source ""

# interfaces
.implements LX/2NW;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/4sY",
        "<",
        "LX/JEr;",
        ">;",
        "LX/2NW;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 0

    invoke-direct {p0, p1}, LX/4sY;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/common/api/Status;
    .locals 1

    iget-object v0, p0, LX/4sY;->a:Lcom/google/android/gms/common/data/DataHolder;

    iget p0, v0, Lcom/google/android/gms/common/data/DataHolder;->h:I

    move v0, p0

    invoke-static {v0}, LX/JF6;->b(I)Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Ljava/lang/Object;
    .locals 2

    new-instance v0, LX/JFA;

    iget-object v1, p0, LX/4sY;->a:Lcom/google/android/gms/common/data/DataHolder;

    invoke-direct {v0, v1, p1}, LX/JFA;-><init>(Lcom/google/android/gms/common/data/DataHolder;I)V

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    invoke-static {p0}, LX/2wy;->a(Ljava/lang/Object;)LX/4sw;

    move-result-object v0

    const-string v1, "status"

    invoke-virtual {p0}, LX/JEs;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4sw;->a(Ljava/lang/String;Ljava/lang/Object;)LX/4sw;

    move-result-object v0

    invoke-virtual {v0}, LX/4sw;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
