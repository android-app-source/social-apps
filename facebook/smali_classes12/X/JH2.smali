.class public final LX/JH2;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:[F

.field public static final b:Landroid/util/SparseIntArray;

.field private static final c:[I


# instance fields
.field public final d:LX/JGq;

.field private final e:LX/JGb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/JGb",
            "<",
            "LX/JGM;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/JGb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/JGb",
            "<",
            "LX/JGQ;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/JGb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/JGb",
            "<",
            "LX/JGu;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/JGb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/JGb",
            "<",
            "Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;",
            ">;"
        }
    .end annotation
.end field

.field public final k:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final l:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final m:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/5r1;",
            ">;"
        }
    .end annotation
.end field

.field public final n:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/5rT;",
            ">;"
        }
    .end annotation
.end field

.field public final o:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/5rT;",
            ">;"
        }
    .end annotation
.end field

.field public p:LX/JGf;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2672146
    new-array v0, v1, [F

    sput-object v0, LX/JH2;->a:[F

    .line 2672147
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    sput-object v0, LX/JH2;->b:Landroid/util/SparseIntArray;

    .line 2672148
    new-array v0, v1, [I

    sput-object v0, LX/JH2;->c:[I

    return-void
.end method

.method public constructor <init>(LX/JGq;)V
    .locals 2

    .prologue
    .line 2672132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2672133
    new-instance v0, LX/JGb;

    sget-object v1, LX/JGM;->b:[LX/JGM;

    invoke-direct {v0, v1}, LX/JGb;-><init>([Ljava/lang/Object;)V

    iput-object v0, p0, LX/JH2;->e:LX/JGb;

    .line 2672134
    new-instance v0, LX/JGb;

    sget-object v1, LX/JGQ;->a:[LX/JGQ;

    invoke-direct {v0, v1}, LX/JGb;-><init>([Ljava/lang/Object;)V

    iput-object v0, p0, LX/JH2;->f:LX/JGb;

    .line 2672135
    new-instance v0, LX/JGb;

    sget-object v1, LX/JGu;->a:[LX/JGu;

    invoke-direct {v0, v1}, LX/JGb;-><init>([Ljava/lang/Object;)V

    iput-object v0, p0, LX/JH2;->g:LX/JGb;

    .line 2672136
    new-instance v0, LX/JGb;

    sget-object v1, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->a:[Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;

    invoke-direct {v0, v1}, LX/JGb;-><init>([Ljava/lang/Object;)V

    iput-object v0, p0, LX/JH2;->h:LX/JGb;

    .line 2672137
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/JH2;->i:Ljava/util/ArrayList;

    .line 2672138
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/JH2;->j:Ljava/util/ArrayList;

    .line 2672139
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/JH2;->k:Ljava/util/ArrayList;

    .line 2672140
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/JH2;->l:Ljava/util/ArrayList;

    .line 2672141
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/JH2;->m:Ljava/util/ArrayList;

    .line 2672142
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/JH2;->n:Ljava/util/ArrayList;

    .line 2672143
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/JH2;->o:Ljava/util/ArrayList;

    .line 2672144
    iput-object p1, p0, LX/JH2;->d:LX/JGq;

    .line 2672145
    return-void
.end method

.method private static a(F)F
    .locals 2

    .prologue
    .line 2672131
    const/high16 v0, 0x3f000000    # 0.5f

    add-float/2addr v0, p0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method private a(LX/JGP;I)V
    .locals 6

    .prologue
    .line 2672127
    invoke-interface {p1}, LX/JGP;->ni_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2672128
    iget-object v0, p0, LX/JH2;->d:LX/JGq;

    const/4 v1, 0x0

    invoke-interface {p1, v1}, LX/JGP;->e(I)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v2

    const/4 v1, 0x1

    invoke-interface {p1, v1}, LX/JGP;->e(I)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v3

    const/4 v1, 0x2

    invoke-interface {p1, v1}, LX/JGP;->e(I)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v4

    const/4 v1, 0x3

    invoke-interface {p1, v1}, LX/JGP;->e(I)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v5

    move v1, p2

    invoke-virtual/range {v0 .. v5}, LX/JGq;->b(IIIII)V

    .line 2672129
    invoke-interface {p1}, LX/JGP;->nj_()V

    .line 2672130
    :cond_0
    return-void
.end method

.method public static a(LX/JH2;Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;FFFF)V
    .locals 7

    .prologue
    .line 2672111
    invoke-static {p2}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 2672112
    invoke-static {p3}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 2672113
    invoke-static {p4}, Ljava/lang/Math;->round(F)I

    move-result v4

    .line 2672114
    invoke-static {p5}, Ljava/lang/Math;->round(F)I

    move-result v5

    .line 2672115
    iget v0, p1, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->l:I

    move v0, v0

    .line 2672116
    if-ne v0, v2, :cond_0

    .line 2672117
    iget v0, p1, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->m:I

    move v0, v0

    .line 2672118
    if-ne v0, v3, :cond_0

    .line 2672119
    iget v0, p1, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->n:I

    move v0, v0

    .line 2672120
    if-ne v0, v4, :cond_0

    .line 2672121
    iget v0, p1, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->o:I

    move v0, v0

    .line 2672122
    if-ne v0, v5, :cond_0

    .line 2672123
    :goto_0
    return-void

    .line 2672124
    :cond_0
    invoke-virtual {p1, v2, v3, v4, v5}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->a(IIII)V

    .line 2672125
    iget v0, p1, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    move v1, v0

    .line 2672126
    iget-object v6, p0, LX/JH2;->n:Ljava/util/ArrayList;

    iget-object v0, p0, LX/JH2;->d:LX/JGq;

    invoke-virtual/range {v0 .. v5}, LX/JGq;->a(IIIII)LX/JGn;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private static a(LX/JH2;Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;FFFFZ)V
    .locals 2

    .prologue
    .line 2672105
    cmpl-float v0, p2, p4

    if-eqz v0, :cond_0

    cmpl-float v0, p3, p5

    if-nez v0, :cond_1

    .line 2672106
    :cond_0
    :goto_0
    return-void

    .line 2672107
    :cond_1
    invoke-virtual/range {p1 .. p6}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->a(FFFFZ)V

    .line 2672108
    iget-object v0, p0, LX/JH2;->g:LX/JGb;

    .line 2672109
    iget-object v1, p1, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->j:LX/JGu;

    move-object v1, v1

    .line 2672110
    invoke-virtual {v0, v1}, LX/JGb;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private a(Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;[Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;[Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;)V
    .locals 9

    .prologue
    const/4 v8, -0x1

    const/4 v1, 0x0

    .line 2672072
    iput-object p3, p1, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->i:[Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;

    .line 2672073
    iget-object v0, p0, LX/JH2;->p:LX/JGf;

    if-nez v0, :cond_0

    .line 2672074
    iget-object v0, p0, LX/JH2;->d:LX/JGq;

    invoke-virtual {v0}, LX/JGq;->g()LX/JGf;

    move-result-object v0

    iput-object v0, p0, LX/JH2;->p:LX/JGf;

    .line 2672075
    :cond_0
    array-length v0, p2

    if-eqz v0, :cond_1

    .line 2672076
    iget-object v0, p0, LX/JH2;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2672077
    :cond_1
    iget v0, p1, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    move v4, v0

    .line 2672078
    array-length v0, p3

    .line 2672079
    if-nez v0, :cond_4

    .line 2672080
    sget-object v0, LX/JH2;->c:[I

    .line 2672081
    :cond_2
    array-length v3, p2

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_6

    aget-object v5, p2, v2

    .line 2672082
    iget v6, v5, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->k:I

    move v6, v6

    .line 2672083
    if-ne v6, v4, :cond_3

    .line 2672084
    iget-object v6, p0, LX/JH2;->j:Ljava/util/ArrayList;

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2672085
    iput v8, v5, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->k:I

    .line 2672086
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2672087
    :cond_4
    new-array v0, v0, [I

    .line 2672088
    array-length v5, p3

    move v2, v1

    move v3, v1

    :goto_1
    if-ge v2, v5, :cond_2

    aget-object v6, p3, v2

    .line 2672089
    iget v7, v6, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->k:I

    move v7, v7

    .line 2672090
    if-ne v7, v4, :cond_5

    .line 2672091
    iget v7, v6, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    move v7, v7

    .line 2672092
    neg-int v7, v7

    aput v7, v0, v3

    .line 2672093
    :goto_2
    iput v8, v6, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->k:I

    .line 2672094
    add-int/lit8 v3, v3, 0x1

    .line 2672095
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2672096
    :cond_5
    iget v7, v6, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    move v7, v7

    .line 2672097
    aput v7, v0, v3

    goto :goto_2

    .line 2672098
    :cond_6
    iget-object v2, p0, LX/JH2;->j:Ljava/util/ArrayList;

    invoke-static {v2}, LX/JH2;->a(Ljava/util/ArrayList;)[I

    move-result-object v2

    .line 2672099
    iget-object v3, p0, LX/JH2;->j:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 2672100
    array-length v3, p3

    :goto_3
    if-ge v1, v3, :cond_7

    aget-object v5, p3, v1

    .line 2672101
    iput v4, v5, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->k:I

    .line 2672102
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 2672103
    :cond_7
    iget-object v1, p0, LX/JH2;->d:LX/JGq;

    invoke-virtual {v1, v4, v0, v2}, LX/JGq;->a(I[I[I)V

    .line 2672104
    return-void
.end method

.method public static a(LX/JH2;Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;FFFFFFFF)Z
    .locals 19

    .prologue
    .line 2672149
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->r()Z

    move-result v5

    .line 2672150
    if-nez v5, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->K()Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->g()Z

    move-result v5

    if-nez v5, :cond_0

    move-object/from16 v0, p1

    move/from16 v1, p6

    move/from16 v2, p7

    move/from16 v3, p8

    move/from16 v4, p9

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->a(FFFF)Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    const/4 v5, 0x1

    .line 2672151
    :goto_0
    if-nez v5, :cond_2

    .line 2672152
    const/4 v5, 0x0

    .line 2672153
    :goto_1
    return v5

    .line 2672154
    :cond_1
    const/4 v5, 0x0

    goto :goto_0

    .line 2672155
    :cond_2
    move-object/from16 v0, p1

    move/from16 v1, p6

    move/from16 v2, p7

    move/from16 v3, p8

    move/from16 v4, p9

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->b(FFFF)V

    .line 2672156
    move-object/from16 v0, p0

    iget-object v5, v0, LX/JH2;->e:LX/JGb;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->M()[LX/JGM;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/JGb;->a([Ljava/lang/Object;)V

    .line 2672157
    move-object/from16 v0, p0

    iget-object v5, v0, LX/JH2;->f:LX/JGb;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->N()[LX/JGQ;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/JGb;->a([Ljava/lang/Object;)V

    .line 2672158
    move-object/from16 v0, p0

    iget-object v5, v0, LX/JH2;->g:LX/JGb;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->Q()[LX/JGu;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/JGb;->a([Ljava/lang/Object;)V

    .line 2672159
    move-object/from16 v0, p0

    iget-object v5, v0, LX/JH2;->h:LX/JGb;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->O()[Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/JGb;->a([Ljava/lang/Object;)V

    .line 2672160
    const/4 v15, 0x0

    .line 2672161
    const/16 v16, 0x0

    .line 2672162
    move-object/from16 v0, p1

    instance-of v5, v0, LX/JGP;

    if-eqz v5, :cond_13

    move-object/from16 v5, p1

    .line 2672163
    check-cast v5, LX/JGP;

    .line 2672164
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->l()I

    move-result v6

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v6}, LX/JH2;->a(LX/JGP;I)V

    .line 2672165
    const/4 v15, 0x1

    .line 2672166
    invoke-interface {v5}, LX/JGP;->nh_()Z

    move-result v16

    .line 2672167
    const/high16 p6, -0x800000    # Float.NEGATIVE_INFINITY

    .line 2672168
    const/high16 v12, -0x800000    # Float.NEGATIVE_INFINITY

    .line 2672169
    const/high16 v13, 0x7f800000    # Float.POSITIVE_INFINITY

    .line 2672170
    const/high16 v14, 0x7f800000    # Float.POSITIVE_INFINITY

    .line 2672171
    :goto_2
    if-nez v15, :cond_3

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->b()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2672172
    const/4 v11, 0x1

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move/from16 v7, p2

    move/from16 v8, p3

    move/from16 v9, p4

    move/from16 v10, p5

    invoke-static/range {v5 .. v11}, LX/JH2;->a(LX/JH2;Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;FFFFZ)V

    :cond_3
    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move/from16 v7, p2

    move/from16 v8, p3

    move/from16 v9, p4

    move/from16 v10, p5

    move/from16 v11, p6

    .line 2672173
    invoke-static/range {v5 .. v16}, LX/JH2;->a(LX/JH2;Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;FFFFFFFFZZ)Z

    move-result v18

    .line 2672174
    const/4 v5, 0x0

    .line 2672175
    move-object/from16 v0, p0

    iget-object v6, v0, LX/JH2;->e:LX/JGb;

    invoke-virtual {v6}, LX/JGb;->a()[Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [LX/JGM;

    .line 2672176
    if-eqz v7, :cond_4

    .line 2672177
    const/4 v5, 0x1

    .line 2672178
    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->a([LX/JGM;)V

    .line 2672179
    :cond_4
    move-object/from16 v0, p0

    iget-object v6, v0, LX/JH2;->f:LX/JGb;

    invoke-virtual {v6}, LX/JGb;->a()[Ljava/lang/Object;

    move-result-object v11

    check-cast v11, [LX/JGQ;

    .line 2672180
    if-eqz v11, :cond_5

    .line 2672181
    const/4 v5, 0x1

    .line 2672182
    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->a([LX/JGQ;)V

    .line 2672183
    :cond_5
    move-object/from16 v0, p0

    iget-object v6, v0, LX/JH2;->g:LX/JGb;

    invoke-virtual {v6}, LX/JGb;->a()[Ljava/lang/Object;

    move-result-object v12

    check-cast v12, [LX/JGu;

    .line 2672184
    if-eqz v12, :cond_c

    .line 2672185
    const/4 v5, 0x1

    .line 2672186
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->a([LX/JGu;)V

    move/from16 v17, v5

    .line 2672187
    :goto_3
    move-object/from16 v0, p0

    iget-object v5, v0, LX/JH2;->h:LX/JGb;

    invoke-virtual {v5}, LX/JGb;->a()[Ljava/lang/Object;

    move-result-object v5

    move-object/from16 v16, v5

    check-cast v16, [Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;

    .line 2672188
    if-eqz v17, :cond_8

    .line 2672189
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->ab()Z

    move-result v5

    if-eqz v5, :cond_11

    .line 2672190
    sget-object v9, LX/JH2;->a:[F

    .line 2672191
    sget-object v10, LX/JH2;->a:[F

    .line 2672192
    sget-object v8, LX/JH2;->b:Landroid/util/SparseIntArray;

    .line 2672193
    if-eqz v7, :cond_6

    .line 2672194
    new-instance v8, Landroid/util/SparseIntArray;

    invoke-direct {v8}, Landroid/util/SparseIntArray;-><init>()V

    .line 2672195
    array-length v5, v7

    new-array v9, v5, [F

    .line 2672196
    array-length v5, v7

    new-array v10, v5, [F

    .line 2672197
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->ac()Z

    move-result v5

    if-eqz v5, :cond_e

    .line 2672198
    invoke-static {v7, v9, v10, v8}, LX/JGw;->a([LX/JGM;[F[FLandroid/util/SparseIntArray;)V

    .line 2672199
    :cond_6
    :goto_4
    sget-object v13, LX/JH2;->a:[F

    .line 2672200
    sget-object v14, LX/JH2;->a:[F

    .line 2672201
    if-eqz v12, :cond_7

    .line 2672202
    array-length v5, v12

    new-array v13, v5, [F

    .line 2672203
    array-length v5, v12

    new-array v14, v5, [F

    .line 2672204
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->ac()Z

    move-result v5

    if-eqz v5, :cond_f

    .line 2672205
    invoke-static {v12, v13, v14}, LX/JGw;->b([LX/JGu;[F[F)V

    .line 2672206
    :cond_7
    :goto_5
    if-eqz v16, :cond_10

    const/4 v15, 0x1

    .line 2672207
    :goto_6
    move-object/from16 v0, p0

    iget-object v5, v0, LX/JH2;->d:LX/JGq;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->l()I

    move-result v6

    invoke-virtual/range {v5 .. v15}, LX/JGq;->a(I[LX/JGM;Landroid/util/SparseIntArray;[F[F[LX/JGQ;[LX/JGu;[F[FZ)V

    .line 2672208
    :cond_8
    :goto_7
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->g()Z

    move-result v5

    if-eqz v5, :cond_9

    .line 2672209
    move-object/from16 v0, p0

    iget-object v5, v0, LX/JH2;->d:LX/JGq;

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/facebook/react/uimanager/ReactShadowNode;->a(LX/5rl;)V

    .line 2672210
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->e()V

    .line 2672211
    :cond_9
    if-eqz v16, :cond_a

    .line 2672212
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->O()[Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v16

    invoke-direct {v0, v1, v5, v2}, LX/JH2;->a(Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;[Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;[Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;)V

    .line 2672213
    :cond_a
    if-nez v17, :cond_b

    if-nez v16, :cond_b

    if-eqz v18, :cond_12

    :cond_b
    const/4 v5, 0x1

    goto/16 :goto_1

    .line 2672214
    :cond_c
    if-eqz v18, :cond_d

    .line 2672215
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->R()V

    :cond_d
    move/from16 v17, v5

    goto/16 :goto_3

    .line 2672216
    :cond_e
    invoke-static {v7, v9, v10, v8}, LX/JH5;->a([LX/JGM;[F[FLandroid/util/SparseIntArray;)V

    goto :goto_4

    .line 2672217
    :cond_f
    invoke-static {v12, v13, v14}, LX/JH5;->b([LX/JGu;[F[F)V

    goto :goto_5

    .line 2672218
    :cond_10
    const/4 v15, 0x0

    goto :goto_6

    .line 2672219
    :cond_11
    move-object/from16 v0, p0

    iget-object v5, v0, LX/JH2;->d:LX/JGq;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->l()I

    move-result v6

    invoke-virtual {v5, v6, v7, v11, v12}, LX/JGq;->a(I[LX/JGM;[LX/JGQ;[LX/JGu;)V

    goto :goto_7

    .line 2672220
    :cond_12
    const/4 v5, 0x0

    goto/16 :goto_1

    :cond_13
    move/from16 v14, p9

    move/from16 v13, p8

    move/from16 v12, p7

    goto/16 :goto_2
.end method

.method private static a(LX/JH2;Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;FFFFFFFFZZ)Z
    .locals 16

    .prologue
    .line 2672048
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->r()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2672049
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->s()V

    .line 2672050
    :cond_0
    invoke-static/range {p2 .. p2}, LX/JH2;->a(F)F

    move-result v4

    .line 2672051
    invoke-static/range {p3 .. p3}, LX/JH2;->a(F)F

    move-result v5

    .line 2672052
    invoke-static/range {p4 .. p4}, LX/JH2;->a(F)F

    move-result v6

    .line 2672053
    invoke-static/range {p5 .. p5}, LX/JH2;->a(F)F

    move-result v7

    .line 2672054
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->p()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2672055
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->x()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->y()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    sub-float v8, v6, v4

    float-to-int v8, v8

    sub-float v9, v7, v5

    float-to-int v9, v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v8, v9}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->b(IIII)LX/5r1;

    move-result-object v2

    .line 2672056
    if-eqz v2, :cond_1

    .line 2672057
    move-object/from16 v0, p0

    iget-object v3, v0, LX/JH2;->m:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2672058
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->I()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2672059
    move/from16 v0, p2

    move/from16 v1, p6

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result p6

    .line 2672060
    move/from16 v0, p3

    move/from16 v1, p7

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result p7

    .line 2672061
    move/from16 v0, p4

    move/from16 v1, p8

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result p8

    .line 2672062
    move/from16 v0, p5

    move/from16 v1, p9

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v11

    .line 2672063
    :goto_0
    invoke-static/range {p6 .. p6}, LX/JH2;->a(F)F

    move-result v8

    invoke-static/range {p7 .. p7}, LX/JH2;->a(F)F

    move-result v9

    invoke-static/range {p8 .. p8}, LX/JH2;->a(F)F

    move-result v10

    move-object/from16 v2, p1

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v11}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->a(LX/JH2;FFFFFFFF)V

    .line 2672064
    const/4 v3, 0x0

    .line 2672065
    const/4 v2, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->i()I

    move-result v14

    move v15, v2

    move v2, v3

    move v3, v15

    :goto_1
    if-eq v3, v14, :cond_3

    .line 2672066
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/facebook/react/uimanager/ReactShadowNode;->b(I)Lcom/facebook/react/uimanager/ReactShadowNode;

    move-result-object v5

    .line 2672067
    invoke-virtual {v5}, Lcom/facebook/react/uimanager/ReactShadowNode;->a()Z

    move-result v4

    if-nez v4, :cond_2

    .line 2672068
    check-cast v5, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;

    move-object/from16 v4, p0

    move/from16 v6, p2

    move/from16 v7, p3

    move/from16 v8, p6

    move/from16 v9, p7

    move/from16 v10, p8

    move/from16 v12, p10

    move/from16 v13, p11

    invoke-direct/range {v4 .. v13}, LX/JH2;->a(Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;FFFFFFZZ)Z

    move-result v4

    or-int/2addr v2, v4

    .line 2672069
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 2672070
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->L()V

    .line 2672071
    return v2

    :cond_4
    move/from16 v11, p9

    goto :goto_0
.end method

.method private a(Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;FFFFFFZZ)Z
    .locals 17

    .prologue
    .line 2672028
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->z()F

    move-result v1

    .line 2672029
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->A()F

    move-result v2

    .line 2672030
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->x()F

    move-result v3

    add-float v3, v3, p2

    .line 2672031
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->y()F

    move-result v4

    add-float v4, v4, p3

    .line 2672032
    add-float v5, v3, v1

    .line 2672033
    add-float v6, v4, v2

    .line 2672034
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->Y()Z

    move-result v8

    .line 2672035
    if-nez p8, :cond_0

    .line 2672036
    if-nez v8, :cond_2

    const/4 v7, 0x1

    :goto_0
    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-static/range {v1 .. v7}, LX/JH2;->a(LX/JH2;Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;FFFFZ)V

    .line 2672037
    :cond_0
    if-eqz v8, :cond_3

    .line 2672038
    invoke-virtual/range {p0 .. p1}, LX/JH2;->b(Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;)V

    .line 2672039
    invoke-static/range {p0 .. p1}, LX/JH2;->c(LX/JH2;Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;)V

    .line 2672040
    const/4 v9, 0x0

    const/4 v10, 0x0

    sub-float v11, v5, v3

    sub-float v12, v6, v4

    sub-float v13, p4, v3

    sub-float v14, p5, v4

    sub-float v15, p6, v3

    sub-float v16, p7, v4

    move-object/from16 v7, p0

    move-object/from16 v8, p1

    invoke-static/range {v7 .. v16}, LX/JH2;->a(LX/JH2;Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;FFFFFFFF)Z

    move-result v11

    .line 2672041
    if-nez p8, :cond_1

    .line 2672042
    move-object/from16 v0, p0

    iget-object v1, v0, LX/JH2;->e:LX/JGb;

    move-object/from16 v2, p1

    move/from16 v7, p4

    move/from16 v8, p5

    move/from16 v9, p6

    move/from16 v10, p7

    invoke-virtual/range {v2 .. v10}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->a(FFFFFFFF)LX/JGY;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/JGb;->a(Ljava/lang/Object;)V

    .line 2672043
    :cond_1
    if-nez p9, :cond_4

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    .line 2672044
    invoke-static/range {v1 .. v6}, LX/JH2;->a(LX/JH2;Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;FFFF)V

    move v1, v11

    .line 2672045
    :goto_1
    return v1

    .line 2672046
    :cond_2
    const/4 v7, 0x0

    goto :goto_0

    .line 2672047
    :cond_3
    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move/from16 v7, p4

    move/from16 v8, p5

    move/from16 v9, p6

    move/from16 v10, p7

    invoke-static/range {v1 .. v12}, LX/JH2;->a(LX/JH2;Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;FFFFFFFFZZ)Z

    move-result v1

    goto :goto_1

    :cond_4
    move v1, v11

    goto :goto_1
.end method

.method public static a(Ljava/util/ArrayList;)[I
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;",
            ">;)[I"
        }
    .end annotation

    .prologue
    .line 2672017
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 2672018
    if-nez v3, :cond_0

    .line 2672019
    sget-object v0, LX/JH2;->c:[I

    .line 2672020
    :goto_0
    return-object v0

    .line 2672021
    :cond_0
    new-array v1, v3, [I

    .line 2672022
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_1

    .line 2672023
    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;

    .line 2672024
    iget v4, v0, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    move v0, v4

    .line 2672025
    aput v0, v1, v2

    .line 2672026
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 2672027
    goto :goto_0
.end method

.method private static c(LX/JH2;Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;)V
    .locals 1

    .prologue
    .line 2671995
    iget-object v0, p0, LX/JH2;->h:LX/JGb;

    invoke-virtual {v0, p1}, LX/JGb;->a(Ljava/lang/Object;)V

    .line 2671996
    return-void
.end method


# virtual methods
.method public final a(LX/JGN;)V
    .locals 1

    .prologue
    .line 2672015
    iget-object v0, p0, LX/JH2;->e:LX/JGb;

    invoke-virtual {v0, p1}, LX/JGb;->a(Ljava/lang/Object;)V

    .line 2672016
    return-void
.end method

.method public final a(LX/JGQ;)V
    .locals 1

    .prologue
    .line 2672013
    iget-object v0, p0, LX/JH2;->f:LX/JGb;

    invoke-virtual {v0, p1}, LX/JGb;->a(Ljava/lang/Object;)V

    .line 2672014
    return-void
.end method

.method public final a(Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;LX/5rC;)V
    .locals 4
    .param p2    # LX/5rC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2672003
    iget-boolean v0, p1, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->p:Z

    move v0, v0

    .line 2672004
    if-eqz v0, :cond_0

    .line 2672005
    iget-object v0, p0, LX/JH2;->d:LX/JGq;

    .line 2672006
    iget v1, p1, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    move v1, v1

    .line 2672007
    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->c()Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, LX/5rl;->a(ILX/5rC;)V

    .line 2672008
    :goto_0
    return-void

    .line 2672009
    :cond_0
    iget-object v0, p0, LX/JH2;->d:LX/JGq;

    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->o()LX/5rJ;

    move-result-object v1

    .line 2672010
    iget v2, p1, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    move v2, v2

    .line 2672011
    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3, p2}, LX/5rl;->a(LX/5rJ;ILjava/lang/String;LX/5rC;)V

    .line 2672012
    invoke-virtual {p1}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->aa()V

    goto :goto_0
.end method

.method public final b(Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;)V
    .locals 5

    .prologue
    .line 2671997
    iget-boolean v0, p1, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->p:Z

    move v0, v0

    .line 2671998
    if-eqz v0, :cond_0

    .line 2671999
    :goto_0
    return-void

    .line 2672000
    :cond_0
    iget v0, p1, Lcom/facebook/react/uimanager/ReactShadowNode;->a:I

    move v0, v0

    .line 2672001
    iget-object v1, p0, LX/JH2;->d:LX/JGq;

    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->o()LX/5rJ;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/react/uimanager/ReactShadowNode;->c()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v3, v4}, LX/5rl;->a(LX/5rJ;ILjava/lang/String;LX/5rC;)V

    .line 2672002
    invoke-virtual {p1}, Lcom/facebook/catalyst/shadow/flat/FlatShadowNode;->aa()V

    goto :goto_0
.end method
