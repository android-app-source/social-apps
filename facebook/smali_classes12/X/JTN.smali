.class public final LX/JTN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/feedplugins/musicstory/providers/protocol/SpotifySaveSongMutationModels$SpotifySaveSongMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/JTK;

.field public final synthetic b:LX/JTF;

.field public final synthetic c:LX/JTO;


# direct methods
.method public constructor <init>(LX/JTO;LX/JTK;LX/JTF;)V
    .locals 0

    .prologue
    .line 2696633
    iput-object p1, p0, LX/JTN;->c:LX/JTO;

    iput-object p2, p0, LX/JTN;->a:LX/JTK;

    iput-object p3, p0, LX/JTN;->b:LX/JTF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2696632
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2696621
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2696622
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2696623
    check-cast v0, Lcom/facebook/feedplugins/musicstory/providers/protocol/SpotifySaveSongMutationModels$SpotifySaveSongMutationModel;

    invoke-virtual {v0}, Lcom/facebook/feedplugins/musicstory/providers/protocol/SpotifySaveSongMutationModels$SpotifySaveSongMutationModel;->a()Z

    move-result v0

    .line 2696624
    if-eqz v0, :cond_0

    .line 2696625
    iget-object v0, p0, LX/JTN;->a:LX/JTK;

    .line 2696626
    iget-object v1, v0, LX/JTK;->a:LX/JTL;

    iget-object v1, v1, LX/JTL;->d:LX/JTY;

    sget-object p0, LX/JTV;->spotify_save:LX/JTV;

    invoke-virtual {v1, p0}, LX/JTY;->a(LX/JTV;)V

    .line 2696627
    iget-object v1, v0, LX/JTK;->a:LX/JTL;

    iget-object v1, v1, LX/JTL;->f:LX/JSf;

    const/4 p0, 0x1

    iput-boolean p0, v1, LX/JSf;->a:Z

    .line 2696628
    iget-object v1, v0, LX/JTK;->a:LX/JTL;

    .line 2696629
    iget-object p0, v1, LX/JTL;->a:LX/JTM;

    iget-object p0, p0, LX/JTM;->c:Ljava/util/concurrent/ExecutorService;

    new-instance p1, Lcom/facebook/feedplugins/musicstory/providers/SpotifyBuilder$Spotify$2;

    invoke-direct {p1, v1}, Lcom/facebook/feedplugins/musicstory/providers/SpotifyBuilder$Spotify$2;-><init>(LX/JTL;)V

    const v0, 0x46b1d400    # 22762.0f

    invoke-static {p0, p1, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2696630
    :goto_0
    return-void

    .line 2696631
    :cond_0
    iget-object v0, p0, LX/JTN;->b:LX/JTF;

    invoke-interface {v0}, LX/JTF;->a()V

    goto :goto_0
.end method
