.class public LX/HXw;
.super LX/4hY;
.source ""


# instance fields
.field public final a:LX/HY4;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0lC;

.field private final d:Lcom/facebook/content/SecureContextHelper;

.field public final e:Landroid/app/Activity;

.field private final f:Landroid/content/Intent;

.field public final g:Lcom/facebook/platform/common/action/PlatformAppCall;

.field public final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private j:Z


# direct methods
.method public constructor <init>(LX/HY4;LX/0Or;LX/0lC;Lcom/facebook/content/SecureContextHelper;Landroid/app/Activity;Landroid/content/Intent;Lcom/facebook/platform/common/action/PlatformAppCall;LX/0Or;LX/0Or;)V
    .locals 0
    .param p8    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p9    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/AuthTokenString;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/HY4;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "LX/0lC;",
            "Lcom/facebook/content/SecureContextHelper;",
            "Landroid/app/Activity;",
            "Landroid/content/Intent;",
            "Lcom/facebook/platform/common/action/PlatformAppCall;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2479565
    invoke-direct {p0}, LX/4hY;-><init>()V

    .line 2479566
    iput-object p1, p0, LX/HXw;->a:LX/HY4;

    .line 2479567
    iput-object p2, p0, LX/HXw;->b:LX/0Or;

    .line 2479568
    iput-object p3, p0, LX/HXw;->c:LX/0lC;

    .line 2479569
    iput-object p4, p0, LX/HXw;->d:Lcom/facebook/content/SecureContextHelper;

    .line 2479570
    iput-object p5, p0, LX/HXw;->e:Landroid/app/Activity;

    .line 2479571
    iput-object p6, p0, LX/HXw;->f:Landroid/content/Intent;

    .line 2479572
    iput-object p7, p0, LX/HXw;->g:Lcom/facebook/platform/common/action/PlatformAppCall;

    .line 2479573
    iput-object p8, p0, LX/HXw;->h:LX/0Or;

    .line 2479574
    iput-object p9, p0, LX/HXw;->i:LX/0Or;

    .line 2479575
    return-void
.end method


# virtual methods
.method public final a(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 2479576
    const/16 v0, 0xc3

    if-ne p1, v0, :cond_0

    .line 2479577
    if-eqz p3, :cond_1

    .line 2479578
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 2479579
    :goto_0
    const/4 v1, -0x1

    if-ne p2, v1, :cond_2

    .line 2479580
    invoke-virtual {p0, v0}, LX/4hY;->d(Landroid/os/Bundle;)V

    .line 2479581
    :cond_0
    :goto_1
    return-void

    .line 2479582
    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    goto :goto_0

    .line 2479583
    :cond_2
    iget-object v1, p0, LX/HXw;->g:Lcom/facebook/platform/common/action/PlatformAppCall;

    .line 2479584
    iget-boolean v2, v1, Lcom/facebook/platform/common/action/PlatformAppCall;->c:Z

    move v1, v2

    .line 2479585
    if-nez v1, :cond_3

    .line 2479586
    iget-object v0, p0, LX/HXw;->g:Lcom/facebook/platform/common/action/PlatformAppCall;

    const-string v1, "ApplicationError"

    const-string v2, "Error occurred in dialog\'s operation"

    invoke-static {v0, v1, v2}, LX/4hW;->a(Lcom/facebook/platform/common/action/PlatformAppCall;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2479587
    :cond_3
    invoke-virtual {p0, v0}, LX/4hY;->c(Landroid/os/Bundle;)V

    goto :goto_1
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 2479588
    if-eqz p1, :cond_0

    .line 2479589
    const-string v0, "is_ui_showing"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, LX/HXw;->j:Z

    .line 2479590
    :cond_0
    iget-boolean v0, p0, LX/HXw;->j:Z

    if-eqz v0, :cond_1

    .line 2479591
    :goto_0
    return-void

    .line 2479592
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/HXw;->j:Z

    .line 2479593
    iget-object v0, p0, LX/HXw;->a:LX/HY4;

    iget-object v1, p0, LX/HXw;->g:Lcom/facebook/platform/common/action/PlatformAppCall;

    .line 2479594
    iget-object v2, v0, LX/HY4;->c:LX/4i1;

    iget-object v3, v0, LX/HY4;->b:LX/HY0;

    invoke-virtual {v2, v3}, LX/4i1;->a(LX/4i0;)V

    .line 2479595
    const-string v2, "PlatformWebDialogs_onCreateExecutor"

    const-string v3, "call_id"

    .line 2479596
    iget-object v4, v1, Lcom/facebook/platform/common/action/PlatformAppCall;->a:Ljava/lang/String;

    move-object v4, v4

    .line 2479597
    const-string v5, "action_name"

    .line 2479598
    iget-object p1, v1, Lcom/facebook/platform/common/action/PlatformAppCall;->i:Ljava/lang/String;

    move-object p1, p1

    .line 2479599
    invoke-static {v3, v4, v5, p1}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v3

    invoke-static {v0, v2, v3}, LX/HY4;->a(LX/HY4;Ljava/lang/String;LX/0P1;)V

    .line 2479600
    new-instance v2, LX/HXv;

    invoke-direct {v2}, LX/HXv;-><init>()V

    .line 2479601
    iget-object v0, p0, LX/HXw;->g:Lcom/facebook/platform/common/action/PlatformAppCall;

    iget-object v1, p0, LX/HXw;->f:Landroid/content/Intent;

    invoke-virtual {v2, v0, v1}, LX/4hh;->a(Lcom/facebook/platform/common/action/PlatformAppCall;Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2479602
    iget-object v0, v2, LX/4hh;->b:Landroid/os/Bundle;

    move-object v0, v0

    .line 2479603
    if-nez v0, :cond_2

    .line 2479604
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2479605
    const-string v1, "com.facebook.platform.status.ERROR_CODE"

    const-string v2, "UnknownError"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2479606
    :cond_2
    invoke-virtual {p0, v0}, LX/4hY;->c(Landroid/os/Bundle;)V

    .line 2479607
    iget-object v0, p0, LX/HXw;->a:LX/HY4;

    invoke-virtual {v0}, LX/HY4;->l()V

    goto :goto_0

    .line 2479608
    :cond_3
    new-instance v3, Landroid/content/Intent;

    iget-object v0, p0, LX/HXw;->e:Landroid/app/Activity;

    const-class v1, Lcom/facebook/platform/webdialogs/PlatformWebDialogsActivity;

    invoke-direct {v3, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2479609
    iget-object v0, p0, LX/HXw;->f:Landroid/content/Intent;

    invoke-static {v0}, Lcom/facebook/platform/common/action/PlatformAppCall;->a(Landroid/content/Intent;)Landroid/os/Bundle;

    move-result-object v1

    .line 2479610
    iget-object v0, v2, LX/HXv;->b:Landroid/os/Bundle;

    if-nez v0, :cond_6

    .line 2479611
    const/4 v0, 0x0

    .line 2479612
    :goto_1
    move-object v0, v0

    .line 2479613
    iget-object v4, p0, LX/HXw;->g:Lcom/facebook/platform/common/action/PlatformAppCall;

    .line 2479614
    iget-boolean v5, v4, Lcom/facebook/platform/common/action/PlatformAppCall;->c:Z

    move v4, v5

    .line 2479615
    if-nez v4, :cond_4

    .line 2479616
    iget-object v0, v2, LX/HXv;->a:Landroid/os/Bundle;

    if-nez v0, :cond_7

    .line 2479617
    const/4 v0, 0x0

    .line 2479618
    :goto_2
    move-object v0, v0

    .line 2479619
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2479620
    const-string v2, "app_name"

    iget-object v4, p0, LX/HXw;->g:Lcom/facebook/platform/common/action/PlatformAppCall;

    .line 2479621
    iget-object v5, v4, Lcom/facebook/platform/common/action/PlatformAppCall;->f:Ljava/lang/String;

    move-object v4, v5

    .line 2479622
    invoke-virtual {v1, v2, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2479623
    const-string v2, "com.facebook.platform.extra.APPLICATION_NAME"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 2479624
    const-string v2, "action_id"

    iget-object v4, p0, LX/HXw;->g:Lcom/facebook/platform/common/action/PlatformAppCall;

    .line 2479625
    iget-object v5, v4, Lcom/facebook/platform/common/action/PlatformAppCall;->a:Ljava/lang/String;

    move-object v4, v5

    .line 2479626
    invoke-virtual {v1, v2, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2479627
    const-string v2, "com.facebook.platform.protocol.CALL_ID"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 2479628
    move-object v1, v1

    .line 2479629
    :cond_4
    const-string v2, "com.facebook.platform.webdialogs.APPCALL_PARCEL"

    iget-object v4, p0, LX/HXw;->g:Lcom/facebook/platform/common/action/PlatformAppCall;

    invoke-virtual {v3, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2479630
    const-string v2, "com.facebook.platform.protocol.BRIDGE_ARGS"

    invoke-virtual {v3, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2479631
    const-string v1, "com.facebook.platform.protocol.METHOD_ARGS"

    invoke-virtual {v3, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2479632
    const-string v0, "com.facebook.platform.webdialogs.HOST_ARGS"

    .line 2479633
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2479634
    const-string v4, "uid"

    iget-object v1, p0, LX/HXw;->h:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v4, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2479635
    const-string v4, "access_token"

    iget-object v1, p0, LX/HXw;->i:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v4, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2479636
    const-string v1, "android_calling_package"

    iget-object v4, p0, LX/HXw;->g:Lcom/facebook/platform/common/action/PlatformAppCall;

    .line 2479637
    iget-object v5, v4, Lcom/facebook/platform/common/action/PlatformAppCall;->d:Ljava/lang/String;

    move-object v4, v5

    .line 2479638
    invoke-virtual {v2, v1, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2479639
    const-string v1, "android_key_hash"

    iget-object v4, p0, LX/HXw;->g:Lcom/facebook/platform/common/action/PlatformAppCall;

    .line 2479640
    iget-object v5, v4, Lcom/facebook/platform/common/action/PlatformAppCall;->g:Ljava/lang/String;

    move-object v4, v5

    .line 2479641
    invoke-virtual {v2, v1, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2479642
    move-object v1, v2

    .line 2479643
    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2479644
    const-string v0, "com.facebook.platform.protocol.PROTOCOL_VERSION"

    iget-object v1, p0, LX/HXw;->g:Lcom/facebook/platform/common/action/PlatformAppCall;

    .line 2479645
    iget v2, v1, Lcom/facebook/platform/common/action/PlatformAppCall;->b:I

    move v1, v2

    .line 2479646
    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2479647
    iget-object v0, p0, LX/HXw;->a:LX/HY4;

    .line 2479648
    const-string v1, "PlatformWebDialogs_setCookies"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, LX/HY4;->a(LX/HY4;Ljava/lang/String;LX/0P1;)V

    .line 2479649
    iget-object v0, p0, LX/HXw;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2479650
    if-eqz v0, :cond_5

    .line 2479651
    iget-object v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->c:Ljava/lang/String;

    move-object v0, v1

    .line 2479652
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 2479653
    iget-object v1, p0, LX/HXw;->c:LX/0lC;

    invoke-static {v1, v0}, Lcom/facebook/auth/credentials/SessionCookie;->a(LX/0lC;Ljava/lang/String;)LX/0Px;

    move-result-object v0

    .line 2479654
    if-eqz v0, :cond_5

    .line 2479655
    iget-object v1, p0, LX/HXw;->e:Landroid/app/Activity;

    const-string v2, "https://m.%s"

    invoke-static {v1, v2}, LX/14Z;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2479656
    iget-object v2, p0, LX/HXw;->e:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1, v0}, Lcom/facebook/webview/FacebookWebView;->a(Landroid/content/Context;Ljava/lang/String;Ljava/util/Collection;)V

    .line 2479657
    :cond_5
    iget-object v0, p0, LX/HXw;->a:LX/HY4;

    .line 2479658
    const-string v1, "PlatformWebDialogs_setCookies"

    invoke-static {v0, v1}, LX/HY4;->a(LX/HY4;Ljava/lang/String;)V

    .line 2479659
    iget-object v0, p0, LX/HXw;->a:LX/HY4;

    .line 2479660
    const-string v1, "PlatformWebDialogs_onCreateExecutor"

    invoke-static {v0, v1}, LX/HY4;->a(LX/HY4;Ljava/lang/String;)V

    .line 2479661
    const-string v1, "PlatformWebDialogs_startActivity"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, LX/HY4;->a(LX/HY4;Ljava/lang/String;LX/0P1;)V

    .line 2479662
    iget-object v0, p0, LX/HXw;->d:Lcom/facebook/content/SecureContextHelper;

    const/16 v1, 0xc3

    iget-object v2, p0, LX/HXw;->e:Landroid/app/Activity;

    invoke-interface {v0, v3, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    goto/16 :goto_0

    :cond_6
    new-instance v0, Landroid/os/Bundle;

    iget-object v4, v2, LX/HXv;->b:Landroid/os/Bundle;

    invoke-direct {v0, v4}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    goto/16 :goto_1

    :cond_7
    new-instance v0, Landroid/os/Bundle;

    iget-object v1, v2, LX/HXv;->a:Landroid/os/Bundle;

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    goto/16 :goto_2
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2479663
    invoke-super {p0, p1}, LX/4hY;->b(Landroid/os/Bundle;)V

    .line 2479664
    const-string v0, "is_ui_showing"

    iget-boolean v1, p0, LX/HXw;->j:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2479665
    return-void
.end method
