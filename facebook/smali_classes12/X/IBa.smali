.class public final LX/IBa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:LX/IBg;


# direct methods
.method public constructor <init>(LX/IBg;I)V
    .locals 0

    .prologue
    .line 2547307
    iput-object p1, p0, LX/IBa;->b:LX/IBg;

    iput p2, p0, LX/IBa;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 8

    .prologue
    .line 2547308
    iget-object v0, p0, LX/IBa;->b:LX/IBg;

    invoke-virtual {v0}, LX/IBg;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2547309
    iget-object v0, p0, LX/IBa;->b:LX/IBg;

    iget-object v0, v0, LX/IBg;->c:LX/1nQ;

    const-string v2, "event_location_summary_open_page"

    iget-object v3, p0, LX/IBa;->b:LX/IBg;

    iget-object v3, v3, LX/IBg;->k:Lcom/facebook/events/model/Event;

    .line 2547310
    iget-object v4, v3, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v3, v4

    .line 2547311
    iget-object v4, p0, LX/IBa;->b:LX/IBg;

    iget-object v4, v4, LX/IBg;->l:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v4, v4, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 2547312
    iget-object v5, v4, Lcom/facebook/events/common/EventActionContext;->e:Lcom/facebook/events/common/ActionSource;

    move-object v4, v5

    .line 2547313
    invoke-virtual {v4}, Lcom/facebook/events/common/ActionSource;->getParamValue()I

    move-result v4

    iget-object v5, p0, LX/IBa;->b:LX/IBg;

    iget-object v5, v5, LX/IBg;->l:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v5, v5, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 2547314
    iget-object p1, v5, Lcom/facebook/events/common/EventActionContext;->f:Lcom/facebook/events/common/ActionSource;

    move-object v5, p1

    .line 2547315
    invoke-virtual {v5}, Lcom/facebook/events/common/ActionSource;->getParamValue()I

    move-result v5

    invoke-virtual {v0, v2, v3, v4, v5}, LX/1nQ;->c(Ljava/lang/String;Ljava/lang/String;II)V

    .line 2547316
    iget v0, p0, LX/IBa;->a:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/IBa;->b:LX/IBg;

    iget-object v4, v4, LX/IBg;->k:Lcom/facebook/events/model/Event;

    .line 2547317
    iget-wide v6, v4, Lcom/facebook/events/model/Event;->P:J

    move-wide v4, v6

    .line 2547318
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v2}, LX/1nG;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2547319
    iget-object v0, p0, LX/IBa;->b:LX/IBg;

    iget-object v0, v0, LX/IBg;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-interface {v0, v1, v2}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2547320
    iget-object v2, p0, LX/IBa;->b:LX/IBg;

    iget-object v2, v2, LX/IBg;->h:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v2, v0, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2547321
    return-void
.end method
