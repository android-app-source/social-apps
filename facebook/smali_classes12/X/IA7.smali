.class public final LX/IA7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

.field public final synthetic b:Lcom/facebook/events/common/EventActionContext;

.field public final synthetic c:Lcom/facebook/events/permalink/guestlist/EventGuestListView;


# direct methods
.method public constructor <init>(Lcom/facebook/events/permalink/guestlist/EventGuestListView;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;Lcom/facebook/events/common/EventActionContext;)V
    .locals 0

    .prologue
    .line 2544683
    iput-object p1, p0, LX/IA7;->c:Lcom/facebook/events/permalink/guestlist/EventGuestListView;

    iput-object p2, p0, LX/IA7;->a:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    iput-object p3, p0, LX/IA7;->b:Lcom/facebook/events/common/EventActionContext;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x27f39fa7

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2544684
    new-instance v1, LX/Blg;

    iget-object v2, p0, LX/IA7;->a:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/Blg;-><init>(Ljava/lang/String;)V

    .line 2544685
    iget-object v2, p0, LX/IA7;->b:Lcom/facebook/events/common/EventActionContext;

    .line 2544686
    iput-object v2, v1, LX/Blg;->a:Lcom/facebook/events/common/EventActionContext;

    .line 2544687
    move-object v2, v1

    .line 2544688
    iget-object v3, p0, LX/IA7;->a:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->eR_()Ljava/lang/String;

    move-result-object v3

    .line 2544689
    iput-object v3, v2, LX/Blg;->c:Ljava/lang/String;

    .line 2544690
    move-object v2, v2

    .line 2544691
    iget-object v3, p0, LX/IA7;->a:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->G()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v3

    .line 2544692
    iput-object v3, v2, LX/Blg;->d:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    .line 2544693
    move-object v2, v2

    .line 2544694
    iget-object v3, p0, LX/IA7;->a:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->l()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v3

    .line 2544695
    iput-object v3, v2, LX/Blg;->e:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    .line 2544696
    move-object v2, v2

    .line 2544697
    iget-object v3, p0, LX/IA7;->a:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->q()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v3

    .line 2544698
    iput-object v3, v2, LX/Blg;->f:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 2544699
    move-object v2, v2

    .line 2544700
    iget-object v3, p0, LX/IA7;->c:Lcom/facebook/events/permalink/guestlist/EventGuestListView;

    invoke-static {v3}, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->b(Lcom/facebook/events/permalink/guestlist/EventGuestListView;)Z

    move-result v3

    .line 2544701
    iput-boolean v3, v2, LX/Blg;->g:Z

    .line 2544702
    move-object v2, v2

    .line 2544703
    iget-object v3, p0, LX/IA7;->c:Lcom/facebook/events/permalink/guestlist/EventGuestListView;

    iget-object v4, p0, LX/IA7;->a:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-static {v3, v4}, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->a(Lcom/facebook/events/permalink/guestlist/EventGuestListView;Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)LX/0Px;

    move-result-object v3

    .line 2544704
    iput-object v3, v2, LX/Blg;->h:LX/0Px;

    .line 2544705
    move-object v2, v2

    .line 2544706
    iget-object v3, p0, LX/IA7;->a:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-static {v3}, LX/I9i;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)Z

    move-result v3

    .line 2544707
    iput-boolean v3, v2, LX/Blg;->i:Z

    .line 2544708
    iget-object v2, p0, LX/IA7;->c:Lcom/facebook/events/permalink/guestlist/EventGuestListView;

    iget-object v2, v2, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->c:LX/Ble;

    iget-object v3, p0, LX/IA7;->c:Lcom/facebook/events/permalink/guestlist/EventGuestListView;

    invoke-virtual {v3}, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v1}, LX/Blg;->a()Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;

    move-result-object v1

    .line 2544709
    invoke-static {v1}, LX/Ble;->a(Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;)Landroid/os/Bundle;

    move-result-object v4

    invoke-static {v2, v3, v4}, LX/Ble;->a(LX/Ble;Landroid/content/Context;Landroid/os/Bundle;)V

    .line 2544710
    const v1, -0x5c7e4998

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
