.class public LX/JMA;
.super Landroid/view/View;
.source ""


# instance fields
.field public final a:LX/JKh;

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:LX/5pG;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/JKh;)V
    .locals 0

    .prologue
    .line 2682898
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2682899
    iput-object p2, p0, LX/JMA;->a:LX/JKh;

    .line 2682900
    return-void
.end method


# virtual methods
.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x1c9d0277

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2682901
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 2682902
    invoke-virtual {p0}, LX/JMA;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    .line 2682903
    new-instance v2, LX/JM9;

    invoke-direct {v2, p0, v1}, LX/JM9;-><init>(LX/JMA;Landroid/view/ViewTreeObserver;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 2682904
    const/16 v1, 0x2d

    const v2, -0x7d4195eb

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
