.class public final LX/Hzd;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:LX/Hze;

.field public final c:I

.field public final d:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchAllUpcomingEventsQueryModel;",
            ">;>;"
        }
    .end annotation
.end field

.field public final e:J

.field public final f:LX/0TD;


# direct methods
.method public constructor <init>(LX/0TD;Lcom/google/common/util/concurrent/ListenableFuture;LX/Hze;ILcom/google/common/util/concurrent/ListenableFuture;J)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TD;",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "LX/Hze;",
            "I",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchAllUpcomingEventsQueryModel;",
            ">;>;J)V"
        }
    .end annotation

    .prologue
    .line 2525828
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2525829
    iput-object p1, p0, LX/Hzd;->f:LX/0TD;

    .line 2525830
    iput-object p2, p0, LX/Hzd;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2525831
    iput-object p3, p0, LX/Hzd;->b:LX/Hze;

    .line 2525832
    iput p4, p0, LX/Hzd;->c:I

    .line 2525833
    iput-object p5, p0, LX/Hzd;->d:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2525834
    iput-wide p6, p0, LX/Hzd;->e:J

    .line 2525835
    return-void
.end method
