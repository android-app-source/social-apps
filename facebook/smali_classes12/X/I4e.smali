.class public final LX/I4e;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;


# direct methods
.method public constructor <init>(Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;)V
    .locals 0

    .prologue
    .line 2534274
    iput-object p1, p0, LX/I4e;->a:Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x5a05ac61

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2534275
    const/4 v0, 0x0

    .line 2534276
    iget-object v2, p0, LX/I4e;->a:Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;

    iget-object v2, v2, Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;->d:Lcom/facebook/widget/CustomFrameLayout;

    if-ne p1, v2, :cond_1

    .line 2534277
    iget-object v0, p0, LX/I4e;->a:Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;

    iget-object v0, v0, Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;->k:Ljava/lang/String;

    .line 2534278
    :cond_0
    :goto_0
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2534279
    const v0, -0x36316320    # -1692572.0f

    invoke-static {v0, v1}, LX/02F;->a(II)V

    .line 2534280
    :goto_1
    return-void

    .line 2534281
    :cond_1
    iget-object v2, p0, LX/I4e;->a:Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;

    iget-object v2, v2, Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;->g:Lcom/facebook/widget/CustomFrameLayout;

    if-ne p1, v2, :cond_0

    .line 2534282
    iget-object v0, p0, LX/I4e;->a:Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;

    iget-object v0, v0, Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;->l:Ljava/lang/String;

    goto :goto_0

    .line 2534283
    :cond_2
    iget-object v2, p0, LX/I4e;->a:Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 2534284
    iget-object v4, v2, Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;->b:LX/17Y;

    sget-object p0, LX/0ax;->cG:Ljava/lang/String;

    invoke-interface {v4, v3, p0}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    .line 2534285
    const-string p0, "extra_event_collection_id"

    invoke-virtual {v4, p0, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2534286
    iget-object p0, v2, Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;->j:Lcom/facebook/events/common/EventAnalyticsParams;

    if-eqz p0, :cond_3

    .line 2534287
    const-string p0, "extras_event_analytics_params"

    iget-object p1, v2, Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;->j:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-virtual {v4, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2534288
    :cond_3
    iget-object p0, v2, Lcom/facebook/events/eventcollections/view/impl/RelatedEventCollectionsBlockViewImpl;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {p0, v4, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2534289
    const v0, 0xd2d3a8d

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_1
.end method
