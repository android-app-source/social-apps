.class public final LX/HXd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/pages/identity/timeline/storymenu/graphql/PagePinStoryModels$PagePinStoryMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:LX/4At;

.field public final synthetic c:Landroid/view/View;

.field public final synthetic d:LX/HXi;


# direct methods
.method public constructor <init>(LX/HXi;Lcom/facebook/graphql/model/GraphQLStory;LX/4At;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2479174
    iput-object p1, p0, LX/HXd;->d:LX/HXi;

    iput-object p2, p0, LX/HXd;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object p3, p0, LX/HXd;->b:LX/4At;

    iput-object p4, p0, LX/HXd;->c:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2479175
    sget-object v0, LX/HXi;->g:Ljava/lang/Class;

    const-string v1, "Failed to pin post."

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2479176
    iget-object v0, p0, LX/HXd;->c:Landroid/view/View;

    iget-object v1, p0, LX/HXd;->c:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f081844

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/HQz;->a(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, LX/3oW;->a(Landroid/view/View;Ljava/lang/CharSequence;I)LX/3oW;

    move-result-object v0

    invoke-virtual {v0}, LX/3oW;->b()V

    .line 2479177
    iget-object v0, p0, LX/HXd;->b:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 2479178
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2479179
    iget-object v0, p0, LX/HXd;->d:LX/HXi;

    iget-object v1, p0, LX/HXd;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    .line 2479180
    iput-object v1, v0, LX/HXi;->n:Ljava/lang/String;

    .line 2479181
    iget-object v0, p0, LX/HXd;->d:LX/HXi;

    iget-object v0, v0, LX/D2z;->b:LX/BPq;

    new-instance v1, LX/BPX;

    iget-object v2, p0, LX/HXd;->d:LX/HXi;

    iget-object v2, v2, LX/D2z;->a:LX/5SB;

    .line 2479182
    iget-object v3, v2, LX/5SB;->d:Landroid/os/ParcelUuid;

    move-object v2, v3

    .line 2479183
    iget-object v3, p0, LX/HXd;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, LX/BPX;-><init>(Landroid/os/ParcelUuid;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2479184
    iget-object v0, p0, LX/HXd;->b:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 2479185
    return-void
.end method
