.class public LX/HYU;
.super Landroid/preference/Preference;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2480742
    invoke-direct {p0, p1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2480743
    iput-object p1, p0, LX/HYU;->a:Landroid/content/Context;

    .line 2480744
    iput-object p2, p0, LX/HYU;->b:Lcom/facebook/content/SecureContextHelper;

    .line 2480745
    const-string v0, "Rapid Feedback Settings"

    invoke-virtual {p0, v0}, LX/HYU;->setTitle(Ljava/lang/CharSequence;)V

    .line 2480746
    return-void
.end method

.method public static a(LX/0QB;)LX/HYU;
    .locals 3

    .prologue
    .line 2480747
    new-instance v2, LX/HYU;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {v2, v0, v1}, LX/HYU;-><init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;)V

    .line 2480748
    move-object v0, v2

    .line 2480749
    return-object v0
.end method


# virtual methods
.method public final onBindView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2480750
    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    .line 2480751
    new-instance v0, LX/HYT;

    invoke-direct {v0, p0}, LX/HYT;-><init>(LX/HYU;)V

    invoke-virtual {p0, v0}, LX/HYU;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2480752
    return-void
.end method
