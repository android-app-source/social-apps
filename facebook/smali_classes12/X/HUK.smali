.class public final LX/HUK;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;)V
    .locals 0

    .prologue
    .line 2471950
    iput-object p1, p0, LX/HUK;->a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2471979
    iget-object v0, p0, LX/HUK;->a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->a$redex0(Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;Z)V

    .line 2471980
    iget-object v0, p0, LX/HUK;->a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "fetchPagesPoliticalIssuesList"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2471981
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 2471951
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v2, 0x0

    .line 2471952
    if-nez p1, :cond_0

    .line 2471953
    :goto_0
    return-void

    .line 2471954
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2471955
    check-cast v0, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel;

    .line 2471956
    iget-object v1, p0, LX/HUK;->a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;

    iget-object v1, v1, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->r:Ljava/lang/String;

    if-nez v1, :cond_8

    const/4 v1, 0x1

    .line 2471957
    :goto_1
    if-eqz v1, :cond_1

    .line 2471958
    iget-object v1, p0, LX/HUK;->a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;

    iget-object v1, v1, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->j:LX/HU3;

    .line 2471959
    iget-object v3, v1, LX/HU3;->d:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 2471960
    :cond_1
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel;->k()Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 2471961
    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel;->k()Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel;->j()LX/1vs;

    move-result-object v1

    iget-object v3, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    iget-object v4, p0, LX/HUK;->a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;

    invoke-virtual {v3, v1, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    .line 2471962
    iput-object v1, v4, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->r:Ljava/lang/String;

    .line 2471963
    iget-object v1, p0, LX/HUK;->a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;

    iget-object v1, v1, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->j:LX/HU3;

    const/4 v5, 0x0

    .line 2471964
    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel;->k()Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 2471965
    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel;->k()Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v6, v5

    :goto_2
    if-ge v6, v8, :cond_5

    invoke-virtual {v7, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel;

    .line 2471966
    invoke-virtual {v3}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel;->a()Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-virtual {v3}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel;->a()Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel;->j()LX/1vs;

    move-result-object v4

    iget v4, v4, LX/1vs;->b:I

    if-eqz v4, :cond_3

    const/4 v4, 0x1

    :goto_3
    if-eqz v4, :cond_2

    .line 2471967
    iget-object v4, v1, LX/HU3;->d:Ljava/util/ArrayList;

    invoke-virtual {v3}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel;->a()Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2471968
    :cond_2
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    goto :goto_2

    :cond_3
    move v4, v5

    .line 2471969
    goto :goto_3

    :cond_4
    move v4, v5

    goto :goto_3

    .line 2471970
    :cond_5
    iget-object v1, p0, LX/HUK;->a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;

    iget-object v1, v1, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->j:LX/HU3;

    invoke-virtual {v1}, LX/1OM;->notifyDataSetChanged()V

    .line 2471971
    iget-object v1, p0, LX/HUK;->a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel;->a()Z

    move-result v3

    .line 2471972
    iput-boolean v3, v1, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->q:Z

    .line 2471973
    iget-object v1, p0, LX/HUK;->a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;

    iget-boolean v1, v1, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->q:Z

    if-eqz v1, :cond_6

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel;->j()LX/2uF;

    move-result-object v1

    invoke-virtual {v1}, LX/39O;->c()I

    move-result v1

    if-eqz v1, :cond_6

    .line 2471974
    iget-object v1, p0, LX/HUK;->a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;

    iget-object v1, v1, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->p:LX/3Sb;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel;->j()LX/2uF;

    move-result-object v0

    invoke-interface {v1, v0}, LX/3Sb;->a(LX/39P;)Z

    .line 2471975
    :cond_6
    iget-object v0, p0, LX/HUK;->a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->k:LX/HME;

    if-eqz v0, :cond_7

    .line 2471976
    iget-object v0, p0, LX/HUK;->a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->k:LX/HME;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2471977
    :cond_7
    iget-object v0, p0, LX/HUK;->a:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;

    invoke-static {v0, v2}, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->a$redex0(Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;Z)V

    goto/16 :goto_0

    :cond_8
    move v1, v2

    .line 2471978
    goto/16 :goto_1
.end method
