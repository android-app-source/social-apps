.class public final LX/JEy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/google/android/gms/location/places/internal/PlaceLikelihoodEntity;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 2

    check-cast p1, Lcom/google/android/gms/location/places/internal/PlaceLikelihoodEntity;

    check-cast p2, Lcom/google/android/gms/location/places/internal/PlaceLikelihoodEntity;

    iget v0, p1, Lcom/google/android/gms/location/places/internal/PlaceLikelihoodEntity;->c:F

    move v0, v0

    iget v1, p2, Lcom/google/android/gms/location/places/internal/PlaceLikelihoodEntity;->c:F

    move v1, v1

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    neg-int v0, v0

    return v0
.end method
