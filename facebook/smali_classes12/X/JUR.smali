.class public final LX/JUR;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2698826
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLAlbum;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2698808
    if-nez p1, :cond_0

    .line 2698809
    :goto_0
    return v0

    .line 2698810
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2698811
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 2698812
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 2698813
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 2698814
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLCommerceStoreCollection;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2698827
    if-nez p1, :cond_0

    .line 2698828
    :goto_0
    return v0

    .line 2698829
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLCommerceStoreCollection;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2698830
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 2698831
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 2698832
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 2698833
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLEntity;)I
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 2698834
    if-nez p1, :cond_0

    .line 2698835
    :goto_0
    return v0

    .line 2698836
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v1

    .line 2698837
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->u()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2698838
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2698839
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntity;->v()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v4

    const/4 v5, 0x0

    .line 2698840
    if-nez v4, :cond_1

    .line 2698841
    :goto_1
    move v4, v5

    .line 2698842
    const/4 v5, 0x5

    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 2698843
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 2698844
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 2698845
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 2698846
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v4}, LX/186;->b(II)V

    .line 2698847
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 2698848
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 2698849
    :cond_1
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    invoke-virtual {p0, v6}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v6

    .line 2698850
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 2698851
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v8

    invoke-static {p0, v8}, LX/JUR;->a(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v8

    .line 2698852
    const/4 p1, 0x3

    invoke-virtual {p0, p1}, LX/186;->c(I)V

    .line 2698853
    invoke-virtual {p0, v5, v6}, LX/186;->b(II)V

    .line 2698854
    const/4 v5, 0x1

    invoke-virtual {p0, v5, v7}, LX/186;->b(II)V

    .line 2698855
    const/4 v5, 0x2

    invoke-virtual {p0, v5, v8}, LX/186;->b(II)V

    .line 2698856
    invoke-virtual {p0}, LX/186;->d()I

    move-result v5

    .line 2698857
    invoke-virtual {p0, v5}, LX/186;->d(I)V

    goto :goto_1
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLEvent;)I
    .locals 11

    .prologue
    const/4 v0, 0x0

    .line 2698858
    if-nez p1, :cond_0

    .line 2698859
    :goto_0
    return v0

    .line 2698860
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEvent;->C()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 2698861
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEvent;->W()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 2698862
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEvent;->Z()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v3

    const/4 v4, 0x0

    .line 2698863
    if-nez v3, :cond_1

    .line 2698864
    :goto_1
    move v3, v4

    .line 2698865
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEvent;->ah()Lcom/facebook/graphql/model/GraphQLEventViewerCapability;

    move-result-object v4

    const/4 v5, 0x0

    .line 2698866
    if-nez v4, :cond_2

    .line 2698867
    :goto_2
    move v4, v5

    .line 2698868
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEvent;->ax()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 2698869
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEvent;->aF()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 2698870
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEvent;->aI()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 2698871
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEvent;->bh()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v8

    invoke-static {p0, v8}, LX/JUR;->a(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v8

    .line 2698872
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEvent;->by()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v9

    invoke-virtual {p0, v9}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v9

    .line 2698873
    const/16 v10, 0x9

    invoke-virtual {p0, v10}, LX/186;->c(I)V

    .line 2698874
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 2698875
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 2698876
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 2698877
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v4}, LX/186;->b(II)V

    .line 2698878
    const/4 v0, 0x4

    invoke-virtual {p0, v0, v5}, LX/186;->b(II)V

    .line 2698879
    const/4 v0, 0x5

    invoke-virtual {p0, v0, v6}, LX/186;->b(II)V

    .line 2698880
    const/4 v0, 0x6

    invoke-virtual {p0, v0, v7}, LX/186;->b(II)V

    .line 2698881
    const/4 v0, 0x7

    invoke-virtual {p0, v0, v8}, LX/186;->b(II)V

    .line 2698882
    const/16 v0, 0x8

    invoke-virtual {p0, v0, v9}, LX/186;->b(II)V

    .line 2698883
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 2698884
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 2698885
    :cond_1
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPlace;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v5

    .line 2698886
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPlace;->A()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 2698887
    const/4 v7, 0x2

    invoke-virtual {p0, v7}, LX/186;->c(I)V

    .line 2698888
    invoke-virtual {p0, v4, v5}, LX/186;->b(II)V

    .line 2698889
    const/4 v4, 0x1

    invoke-virtual {p0, v4, v6}, LX/186;->b(II)V

    .line 2698890
    invoke-virtual {p0}, LX/186;->d()I

    move-result v4

    .line 2698891
    invoke-virtual {p0, v4}, LX/186;->d(I)V

    goto/16 :goto_1

    .line 2698892
    :cond_2
    const/4 v6, 0x1

    invoke-virtual {p0, v6}, LX/186;->c(I)V

    .line 2698893
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLEventViewerCapability;->w()Z

    move-result v6

    invoke-virtual {p0, v5, v6}, LX/186;->a(IZ)V

    .line 2698894
    invoke-virtual {p0}, LX/186;->d()I

    move-result v5

    .line 2698895
    invoke-virtual {p0, v5}, LX/186;->d(I)V

    goto/16 :goto_2
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLFundraiser;)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2698896
    if-nez p1, :cond_0

    .line 2698897
    :goto_0
    return v0

    .line 2698898
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFundraiser;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v1

    .line 2698899
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFundraiser;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2698900
    const/4 v3, 0x2

    invoke-virtual {p0, v3}, LX/186;->c(I)V

    .line 2698901
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 2698902
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 2698903
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 2698904
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLGroup;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2698905
    if-nez p1, :cond_0

    .line 2698906
    :goto_0
    return v0

    .line 2698907
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2698908
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 2698909
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 2698910
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 2698911
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method public static a(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2698912
    if-nez p1, :cond_0

    .line 2698913
    :goto_0
    return v0

    .line 2698914
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2698915
    const/4 v2, 0x3

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 2698916
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v2

    invoke-virtual {p0, v0, v2, v0}, LX/186;->a(III)V

    .line 2698917
    const/4 v2, 0x1

    invoke-virtual {p0, v2, v1}, LX/186;->b(II)V

    .line 2698918
    const/4 v1, 0x2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v2

    invoke-virtual {p0, v1, v2, v0}, LX/186;->a(III)V

    .line 2698919
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 2698920
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLInlineActivity;)I
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 2699007
    if-nez p1, :cond_0

    .line 2699008
    :goto_0
    return v0

    .line 2699009
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLInlineActivity;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2699010
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLInlineActivity;->k()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    const/4 v3, 0x0

    .line 2699011
    if-nez v2, :cond_1

    .line 2699012
    :goto_1
    move v2, v3

    .line 2699013
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLInlineActivity;->l()Lcom/facebook/graphql/model/GraphQLTaggableActivity;

    move-result-object v3

    invoke-static {p0, v3}, LX/JUR;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTaggableActivity;)I

    move-result v3

    .line 2699014
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLInlineActivity;->m()Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    move-result-object v4

    const/4 v5, 0x0

    .line 2699015
    if-nez v4, :cond_2

    .line 2699016
    :goto_2
    move v4, v5

    .line 2699017
    const/4 v5, 0x4

    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 2699018
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 2699019
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 2699020
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 2699021
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v4}, LX/186;->b(II)V

    .line 2699022
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 2699023
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 2699024
    :cond_1
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v4

    .line 2699025
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 2699026
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 2699027
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->kn()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 2699028
    const/4 v8, 0x4

    invoke-virtual {p0, v8}, LX/186;->c(I)V

    .line 2699029
    invoke-virtual {p0, v3, v4}, LX/186;->b(II)V

    .line 2699030
    const/4 v3, 0x1

    invoke-virtual {p0, v3, v5}, LX/186;->b(II)V

    .line 2699031
    const/4 v3, 0x2

    invoke-virtual {p0, v3, v6}, LX/186;->b(II)V

    .line 2699032
    const/4 v3, 0x3

    invoke-virtual {p0, v3, v7}, LX/186;->b(II)V

    .line 2699033
    invoke-virtual {p0}, LX/186;->d()I

    move-result v3

    .line 2699034
    invoke-virtual {p0, v3}, LX/186;->d(I)V

    goto :goto_1

    .line 2699035
    :cond_2
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    const/4 v7, 0x0

    .line 2699036
    if-nez v6, :cond_3

    .line 2699037
    :goto_3
    move v6, v7

    .line 2699038
    const/4 v7, 0x1

    invoke-virtual {p0, v7}, LX/186;->c(I)V

    .line 2699039
    invoke-virtual {p0, v5, v6}, LX/186;->b(II)V

    .line 2699040
    invoke-virtual {p0}, LX/186;->d()I

    move-result v5

    .line 2699041
    invoke-virtual {p0, v5}, LX/186;->d(I)V

    goto :goto_2

    .line 2699042
    :cond_3
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 2699043
    const/4 v4, 0x1

    invoke-virtual {p0, v4}, LX/186;->c(I)V

    .line 2699044
    invoke-virtual {p0, v7, v8}, LX/186;->b(II)V

    .line 2699045
    invoke-virtual {p0}, LX/186;->d()I

    move-result v7

    .line 2699046
    invoke-virtual {p0, v7}, LX/186;->d(I)V

    goto :goto_3
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLJobOpening;)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2698921
    if-nez p1, :cond_0

    .line 2698922
    :goto_0
    return v0

    .line 2698923
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLJobOpening;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2698924
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLJobOpening;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2698925
    const/4 v3, 0x2

    invoke-virtual {p0, v3}, LX/186;->c(I)V

    .line 2698926
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 2698927
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 2698928
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 2698929
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLLocation;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    .line 2699047
    if-nez p1, :cond_0

    .line 2699048
    :goto_0
    return v1

    .line 2699049
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, LX/186;->c(I)V

    .line 2699050
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLocation;->a()D

    move-result-wide v2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 2699051
    const/4 v1, 0x1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLocation;->b()D

    move-result-wide v2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 2699052
    invoke-virtual {p0}, LX/186;->d()I

    move-result v1

    .line 2699053
    invoke-virtual {p0, v1}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLMessageThreadKey;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2699000
    if-nez p1, :cond_0

    .line 2699001
    :goto_0
    return v0

    .line 2699002
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMessageThreadKey;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2699003
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 2699004
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 2699005
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 2699006
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLOfferView;)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2699054
    if-nez p1, :cond_0

    .line 2699055
    :goto_0
    return v0

    .line 2699056
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLOfferView;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2699057
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLOfferView;->k()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    const/4 v3, 0x0

    .line 2699058
    if-nez v2, :cond_1

    .line 2699059
    :goto_1
    move v2, v3

    .line 2699060
    const/4 v3, 0x2

    invoke-virtual {p0, v3}, LX/186;->c(I)V

    .line 2699061
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 2699062
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 2699063
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 2699064
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 2699065
    :cond_1
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 2699066
    const/4 p1, 0x1

    invoke-virtual {p0, p1}, LX/186;->c(I)V

    .line 2699067
    invoke-virtual {p0, v3, v4}, LX/186;->b(II)V

    .line 2699068
    invoke-virtual {p0}, LX/186;->d()I

    move-result v3

    .line 2699069
    invoke-virtual {p0, v3}, LX/186;->d(I)V

    goto :goto_1
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLOpenGraphObject;)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2698989
    if-nez p1, :cond_0

    .line 2698990
    :goto_0
    return v0

    .line 2698991
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2698992
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->s()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2698993
    const/4 v3, 0x4

    invoke-virtual {p0, v3}, LX/186;->c(I)V

    .line 2698994
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->l()Z

    move-result v3

    invoke-virtual {p0, v0, v3}, LX/186;->a(IZ)V

    .line 2698995
    const/4 v0, 0x1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLOpenGraphObject;->m()Z

    move-result v3

    invoke-virtual {p0, v0, v3}, LX/186;->a(IZ)V

    .line 2698996
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 2698997
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 2698998
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 2698999
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLPage;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2698982
    if-nez p1, :cond_0

    .line 2698983
    :goto_0
    return v0

    .line 2698984
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2698985
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 2698986
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 2698987
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 2698988
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLPageStatusCard;)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2698966
    if-nez p1, :cond_0

    .line 2698967
    :goto_0
    return v0

    .line 2698968
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageStatusCard;->j()Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;

    move-result-object v1

    const/4 v2, 0x0

    .line 2698969
    if-nez v1, :cond_1

    .line 2698970
    :goto_1
    move v1, v2

    .line 2698971
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPageStatusCard;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2698972
    const/4 v3, 0x2

    invoke-virtual {p0, v3}, LX/186;->c(I)V

    .line 2698973
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 2698974
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 2698975
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 2698976
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 2698977
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNativeComponentFlowBookingRequest;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2698978
    const/4 v4, 0x1

    invoke-virtual {p0, v4}, LX/186;->c(I)V

    .line 2698979
    invoke-virtual {p0, v2, v3}, LX/186;->b(II)V

    .line 2698980
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 2698981
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_1
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLProductItem;)I
    .locals 13

    .prologue
    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 2698930
    if-nez p1, :cond_0

    .line 2698931
    :goto_0
    return v2

    .line 2698932
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProductItem;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2698933
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProductItem;->q()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 2698934
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProductItem;->t()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 2698935
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProductItem;->y()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 2698936
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProductItem;->Q()LX/0Px;

    move-result-object v7

    .line 2698937
    if-eqz v7, :cond_2

    .line 2698938
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v0

    new-array v8, v0, [I

    move v1, v2

    .line 2698939
    :goto_1
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2698940
    invoke-virtual {v7, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLProductImage;

    const/4 v10, 0x0

    .line 2698941
    if-nez v0, :cond_3

    .line 2698942
    :goto_2
    move v0, v10

    .line 2698943
    aput v0, v8, v1

    .line 2698944
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2698945
    :cond_1
    invoke-virtual {p0, v8, v9}, LX/186;->a([IZ)I

    move-result v0

    .line 2698946
    :goto_3
    const/4 v1, 0x5

    invoke-virtual {p0, v1}, LX/186;->c(I)V

    .line 2698947
    invoke-virtual {p0, v2, v3}, LX/186;->b(II)V

    .line 2698948
    invoke-virtual {p0, v9, v4}, LX/186;->b(II)V

    .line 2698949
    const/4 v1, 0x2

    invoke-virtual {p0, v1, v5}, LX/186;->b(II)V

    .line 2698950
    const/4 v1, 0x3

    invoke-virtual {p0, v1, v6}, LX/186;->b(II)V

    .line 2698951
    const/4 v1, 0x4

    invoke-virtual {p0, v1, v0}, LX/186;->b(II)V

    .line 2698952
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 2698953
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_3

    .line 2698954
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProductImage;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v11

    const/4 v12, 0x0

    .line 2698955
    if-nez v11, :cond_4

    .line 2698956
    :goto_4
    move v11, v12

    .line 2698957
    const/4 v12, 0x1

    invoke-virtual {p0, v12}, LX/186;->c(I)V

    .line 2698958
    invoke-virtual {p0, v10, v11}, LX/186;->b(II)V

    .line 2698959
    invoke-virtual {p0}, LX/186;->d()I

    move-result v10

    .line 2698960
    invoke-virtual {p0, v10}, LX/186;->d(I)V

    goto :goto_2

    .line 2698961
    :cond_4
    invoke-virtual {v11}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/186;->b(Ljava/lang/String;)I

    move-result p1

    .line 2698962
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/186;->c(I)V

    .line 2698963
    invoke-virtual {p0, v12, p1}, LX/186;->b(II)V

    .line 2698964
    invoke-virtual {p0}, LX/186;->d()I

    move-result v12

    .line 2698965
    invoke-virtual {p0, v12}, LX/186;->d(I)V

    goto :goto_4
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLProfile;)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2698815
    if-nez p1, :cond_0

    .line 2698816
    :goto_0
    return v0

    .line 2698817
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v1

    .line 2698818
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2698819
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    invoke-static {p0, v3}, LX/JUR;->a(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v3

    .line 2698820
    const/4 v4, 0x3

    invoke-virtual {p0, v4}, LX/186;->c(I)V

    .line 2698821
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 2698822
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 2698823
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 2698824
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 2698825
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLReactionUnit;)I
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 2698273
    if-nez p1, :cond_0

    .line 2698274
    :goto_0
    return v2

    .line 2698275
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLReactionUnit;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    .line 2698276
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLReactionUnit;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 2698277
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLReactionUnit;->n()LX/0Px;

    move-result-object v5

    .line 2698278
    if-eqz v5, :cond_2

    .line 2698279
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v0

    new-array v6, v0, [I

    move v1, v2

    .line 2698280
    :goto_1
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2698281
    invoke-virtual {v5, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;

    invoke-static {p0, v0}, LX/JUR;->a(LX/186;Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;)I

    move-result v0

    aput v0, v6, v1

    .line 2698282
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2698283
    :cond_1
    invoke-virtual {p0, v6, v7}, LX/186;->a([IZ)I

    move-result v0

    .line 2698284
    :goto_2
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLReactionUnit;->l()Lcom/facebook/graphql/enums/GraphQLReactionUnitStyle;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 2698285
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLReactionUnit;->m()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 2698286
    const/4 v6, 0x5

    invoke-virtual {p0, v6}, LX/186;->c(I)V

    .line 2698287
    invoke-virtual {p0, v2, v3}, LX/186;->b(II)V

    .line 2698288
    invoke-virtual {p0, v7, v4}, LX/186;->b(II)V

    .line 2698289
    const/4 v2, 0x2

    invoke-virtual {p0, v2, v0}, LX/186;->b(II)V

    .line 2698290
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 2698291
    const/4 v0, 0x4

    invoke-virtual {p0, v0, v5}, LX/186;->b(II)V

    .line 2698292
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 2698293
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_2
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;)I
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 2698294
    if-nez p1, :cond_0

    .line 2698295
    :goto_0
    return v0

    .line 2698296
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v1

    .line 2698297
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->u()Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    move-result-object v2

    const/4 v3, 0x0

    .line 2698298
    if-nez v2, :cond_1

    .line 2698299
    :goto_1
    move v2, v3

    .line 2698300
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->l()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 2698301
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    invoke-static {p0, v4}, LX/JUR;->f(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v4

    .line 2698302
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    invoke-static {p0, v5}, LX/JUR;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)I

    move-result v5

    .line 2698303
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->t()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    invoke-static {p0, v6}, LX/JUR;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)I

    move-result v6

    .line 2698304
    const/16 v7, 0x9

    invoke-virtual {p0, v7}, LX/186;->c(I)V

    .line 2698305
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 2698306
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v2}, LX/186;->b(II)V

    .line 2698307
    const/4 v1, 0x2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLReactionUnitComponent;->k()I

    move-result v2

    invoke-virtual {p0, v1, v2, v0}, LX/186;->a(III)V

    .line 2698308
    const/4 v0, 0x4

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 2698309
    const/4 v0, 0x6

    invoke-virtual {p0, v0, v4}, LX/186;->b(II)V

    .line 2698310
    const/4 v0, 0x7

    invoke-virtual {p0, v0, v5}, LX/186;->b(II)V

    .line 2698311
    const/16 v0, 0x8

    invoke-virtual {p0, v0, v6}, LX/186;->b(II)V

    .line 2698312
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 2698313
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 2698314
    :cond_1
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v4

    .line 2698315
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->m()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    .line 2698316
    const/4 v6, 0x2

    invoke-virtual {p0, v6}, LX/186;->c(I)V

    .line 2698317
    invoke-virtual {p0, v3, v4}, LX/186;->b(II)V

    .line 2698318
    const/4 v3, 0x1

    invoke-virtual {p0, v3, v5}, LX/186;->b(II)V

    .line 2698319
    invoke-virtual {p0}, LX/186;->d()I

    move-result v3

    .line 2698320
    invoke-virtual {p0, v3}, LX/186;->d(I)V

    goto :goto_1
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLTaggableActivity;)I
    .locals 14

    .prologue
    .line 2698321
    if-nez p1, :cond_0

    .line 2698322
    const/4 v0, 0x0

    .line 2698323
    :goto_0
    return v0

    .line 2698324
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->j()Lcom/facebook/graphql/model/GraphQLTaggableActivityAllIconsConnection;

    move-result-object v0

    const/4 v1, 0x0

    .line 2698325
    if-nez v0, :cond_1

    .line 2698326
    :goto_1
    move v0, v1

    .line 2698327
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    const/4 v2, 0x0

    .line 2698328
    if-nez v1, :cond_2

    .line 2698329
    :goto_2
    move v1, v2

    .line 2698330
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    const/4 v3, 0x0

    .line 2698331
    if-nez v2, :cond_3

    .line 2698332
    :goto_3
    move v2, v3

    .line 2698333
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2698334
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 2698335
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->q()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 2698336
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->r()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v6

    invoke-static {p0, v6}, LX/JUR;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;)I

    move-result v6

    .line 2698337
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->s()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v7

    invoke-static {p0, v7}, LX/JUR;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;)I

    move-result v7

    .line 2698338
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->t()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v8

    invoke-static {p0, v8}, LX/JUR;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;)I

    move-result v8

    .line 2698339
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->u()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v9

    invoke-static {p0, v9}, LX/JUR;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;)I

    move-result v9

    .line 2698340
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->v()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v10

    invoke-static {p0, v10}, LX/JUR;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;)I

    move-result v10

    .line 2698341
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->w()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v11

    invoke-static {p0, v11}, LX/JUR;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;)I

    move-result v11

    .line 2698342
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->x()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {p0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 2698343
    const/16 v13, 0x12

    invoke-virtual {p0, v13}, LX/186;->c(I)V

    .line 2698344
    const/4 v13, 0x0

    invoke-virtual {p0, v13, v0}, LX/186;->b(II)V

    .line 2698345
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 2698346
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 2698347
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 2698348
    const/4 v0, 0x4

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->n()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/186;->a(IZ)V

    .line 2698349
    const/4 v0, 0x5

    invoke-virtual {p0, v0, v4}, LX/186;->b(II)V

    .line 2698350
    const/4 v0, 0x6

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->p()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, LX/186;->a(III)V

    .line 2698351
    const/4 v0, 0x7

    invoke-virtual {p0, v0, v5}, LX/186;->b(II)V

    .line 2698352
    const/16 v0, 0x8

    invoke-virtual {p0, v0, v6}, LX/186;->b(II)V

    .line 2698353
    const/16 v0, 0x9

    invoke-virtual {p0, v0, v7}, LX/186;->b(II)V

    .line 2698354
    const/16 v0, 0xa

    invoke-virtual {p0, v0, v8}, LX/186;->b(II)V

    .line 2698355
    const/16 v0, 0xb

    invoke-virtual {p0, v0, v9}, LX/186;->b(II)V

    .line 2698356
    const/16 v0, 0xc

    invoke-virtual {p0, v0, v10}, LX/186;->b(II)V

    .line 2698357
    const/16 v0, 0xd

    invoke-virtual {p0, v0, v11}, LX/186;->b(II)V

    .line 2698358
    const/16 v0, 0xe

    invoke-virtual {p0, v0, v12}, LX/186;->b(II)V

    .line 2698359
    const/16 v0, 0xf

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->y()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/186;->a(IZ)V

    .line 2698360
    const/16 v0, 0x10

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->z()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/186;->a(IZ)V

    .line 2698361
    const/16 v0, 0x11

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->A()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/186;->a(IZ)V

    .line 2698362
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 2698363
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto/16 :goto_0

    .line 2698364
    :cond_1
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 2698365
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTaggableActivityAllIconsConnection;->a()I

    move-result v2

    invoke-virtual {p0, v1, v2, v1}, LX/186;->a(III)V

    .line 2698366
    invoke-virtual {p0}, LX/186;->d()I

    move-result v1

    .line 2698367
    invoke-virtual {p0, v1}, LX/186;->d(I)V

    goto/16 :goto_1

    .line 2698368
    :cond_2
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2698369
    const/4 v4, 0x1

    invoke-virtual {p0, v4}, LX/186;->c(I)V

    .line 2698370
    invoke-virtual {p0, v2, v3}, LX/186;->b(II)V

    .line 2698371
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 2698372
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto/16 :goto_2

    .line 2698373
    :cond_3
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 2698374
    const/4 v5, 0x1

    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 2698375
    invoke-virtual {p0, v3, v4}, LX/186;->b(II)V

    .line 2698376
    invoke-virtual {p0}, LX/186;->d()I

    move-result v3

    .line 2698377
    invoke-virtual {p0, v3}, LX/186;->d(I)V

    goto/16 :goto_3
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;)I
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 2698378
    if-nez p1, :cond_0

    .line 2698379
    :goto_0
    return v2

    .line 2698380
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2698381
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;->j()LX/0Px;

    move-result-object v4

    .line 2698382
    if-eqz v4, :cond_2

    .line 2698383
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v0

    new-array v5, v0, [I

    move v1, v2

    .line 2698384
    :goto_1
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2698385
    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActivityTemplateToken;

    const/4 v7, 0x0

    .line 2698386
    if-nez v0, :cond_3

    .line 2698387
    :goto_2
    move v0, v7

    .line 2698388
    aput v0, v5, v1

    .line 2698389
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2698390
    :cond_1
    invoke-virtual {p0, v5, v6}, LX/186;->a([IZ)I

    move-result v0

    .line 2698391
    :goto_3
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, LX/186;->c(I)V

    .line 2698392
    invoke-virtual {p0, v2, v3}, LX/186;->b(II)V

    .line 2698393
    invoke-virtual {p0, v6, v0}, LX/186;->b(II)V

    .line 2698394
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 2698395
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_3

    .line 2698396
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActivityTemplateToken;->j()Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    .line 2698397
    const/4 p1, 0x2

    invoke-virtual {p0, p1}, LX/186;->c(I)V

    .line 2698398
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActivityTemplateToken;->a()I

    move-result p1

    invoke-virtual {p0, v7, p1, v7}, LX/186;->a(III)V

    .line 2698399
    const/4 v7, 0x1

    invoke-virtual {p0, v7, v8}, LX/186;->b(II)V

    .line 2698400
    invoke-virtual {p0}, LX/186;->d()I

    move-result v7

    .line 2698401
    invoke-virtual {p0, v7}, LX/186;->d(I)V

    goto :goto_2
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2698402
    if-nez p1, :cond_0

    .line 2698403
    :goto_0
    return v0

    .line 2698404
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2698405
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 2698406
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 2698407
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 2698408
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLUser;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2698409
    if-nez p1, :cond_0

    .line 2698410
    :goto_0
    return v0

    .line 2698411
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLUser;->v()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2698412
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 2698413
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 2698414
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 2698415
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLVideoChannel;)I
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 2698416
    if-nez p1, :cond_0

    .line 2698417
    :goto_0
    return v0

    .line 2698418
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v1

    .line 2698419
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2698420
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    const/4 v4, 0x0

    .line 2698421
    if-nez v3, :cond_1

    .line 2698422
    :goto_1
    move v3, v4

    .line 2698423
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->s()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v4

    const/4 v5, 0x0

    .line 2698424
    if-nez v4, :cond_2

    .line 2698425
    :goto_2
    move v4, v5

    .line 2698426
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    const/4 v6, 0x0

    .line 2698427
    if-nez v5, :cond_3

    .line 2698428
    :goto_3
    move v5, v6

    .line 2698429
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    const/4 v7, 0x0

    .line 2698430
    if-nez v6, :cond_4

    .line 2698431
    :goto_4
    move v6, v7

    .line 2698432
    const/16 v7, 0xa

    invoke-virtual {p0, v7}, LX/186;->c(I)V

    .line 2698433
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 2698434
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v2}, LX/186;->b(II)V

    .line 2698435
    const/4 v1, 0x2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->l()I

    move-result v2

    invoke-virtual {p0, v1, v2, v0}, LX/186;->a(III)V

    .line 2698436
    const/4 v1, 0x3

    invoke-virtual {p0, v1, v3}, LX/186;->b(II)V

    .line 2698437
    const/4 v1, 0x4

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->n()Z

    move-result v2

    invoke-virtual {p0, v1, v2}, LX/186;->a(IZ)V

    .line 2698438
    const/4 v1, 0x5

    invoke-virtual {p0, v1, v4}, LX/186;->b(II)V

    .line 2698439
    const/4 v1, 0x6

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->o()Z

    move-result v2

    invoke-virtual {p0, v1, v2}, LX/186;->a(IZ)V

    .line 2698440
    const/4 v1, 0x7

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLVideoChannel;->p()I

    move-result v2

    invoke-virtual {p0, v1, v2, v0}, LX/186;->a(III)V

    .line 2698441
    const/16 v0, 0x8

    invoke-virtual {p0, v0, v5}, LX/186;->b(II)V

    .line 2698442
    const/16 v0, 0x9

    invoke-virtual {p0, v0, v6}, LX/186;->b(II)V

    .line 2698443
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 2698444
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 2698445
    :cond_1
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 2698446
    const/4 v6, 0x1

    invoke-virtual {p0, v6}, LX/186;->c(I)V

    .line 2698447
    invoke-virtual {p0, v4, v5}, LX/186;->b(II)V

    .line 2698448
    invoke-virtual {p0}, LX/186;->d()I

    move-result v4

    .line 2698449
    invoke-virtual {p0, v4}, LX/186;->d(I)V

    goto :goto_1

    .line 2698450
    :cond_2
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    invoke-virtual {p0, v6}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v6

    .line 2698451
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 2698452
    const/16 v8, 0x9

    invoke-virtual {p0, v8}, LX/186;->c(I)V

    .line 2698453
    invoke-virtual {p0, v5, v6}, LX/186;->b(II)V

    .line 2698454
    const/4 v5, 0x1

    invoke-virtual {p0, v5, v7}, LX/186;->b(II)V

    .line 2698455
    const/4 v5, 0x6

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->lo()Z

    move-result v6

    invoke-virtual {p0, v5, v6}, LX/186;->a(IZ)V

    .line 2698456
    invoke-virtual {p0}, LX/186;->d()I

    move-result v5

    .line 2698457
    invoke-virtual {p0, v5}, LX/186;->d(I)V

    goto/16 :goto_2

    .line 2698458
    :cond_3
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 2698459
    const/4 v8, 0x1

    invoke-virtual {p0, v8}, LX/186;->c(I)V

    .line 2698460
    invoke-virtual {p0, v6, v7}, LX/186;->b(II)V

    .line 2698461
    invoke-virtual {p0}, LX/186;->d()I

    move-result v6

    .line 2698462
    invoke-virtual {p0, v6}, LX/186;->d(I)V

    goto/16 :goto_3

    .line 2698463
    :cond_4
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 2698464
    const/4 v9, 0x1

    invoke-virtual {p0, v9}, LX/186;->c(I)V

    .line 2698465
    invoke-virtual {p0, v7, v8}, LX/186;->b(II)V

    .line 2698466
    invoke-virtual {p0}, LX/186;->d()I

    move-result v7

    .line 2698467
    invoke-virtual {p0, v7}, LX/186;->d(I)V

    goto/16 :goto_4
.end method

.method private static b(LX/186;Lcom/facebook/graphql/model/GraphQLComment;)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2698468
    if-nez p1, :cond_0

    .line 2698469
    :goto_0
    return v0

    .line 2698470
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->v()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v1

    const/4 v2, 0x0

    .line 2698471
    if-nez v1, :cond_1

    .line 2698472
    :goto_1
    move v1, v2

    .line 2698473
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2698474
    const/4 v3, 0x2

    invoke-virtual {p0, v3}, LX/186;->c(I)V

    .line 2698475
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 2698476
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 2698477
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 2698478
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 2698479
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2698480
    const/4 v4, 0x1

    invoke-virtual {p0, v4}, LX/186;->c(I)V

    .line 2698481
    invoke-virtual {p0, v2, v3}, LX/186;->b(II)V

    .line 2698482
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 2698483
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_1
.end method

.method private static b(LX/186;Lcom/facebook/graphql/model/GraphQLEvent;)I
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 2698484
    if-nez p1, :cond_0

    .line 2698485
    :goto_0
    return v0

    .line 2698486
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEvent;->ax()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2698487
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEvent;->aI()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2698488
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEvent;->aW()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v3

    const/4 v4, 0x0

    .line 2698489
    if-nez v3, :cond_1

    .line 2698490
    :goto_1
    move v3, v4

    .line 2698491
    const/4 v4, 0x3

    invoke-virtual {p0, v4}, LX/186;->c(I)V

    .line 2698492
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 2698493
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 2698494
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 2698495
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 2698496
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 2698497
    :cond_1
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->j()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 2698498
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->l()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 2698499
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    const/4 v8, 0x0

    .line 2698500
    if-nez v7, :cond_2

    .line 2698501
    :goto_2
    move v7, v8

    .line 2698502
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 2698503
    const/4 v9, 0x4

    invoke-virtual {p0, v9}, LX/186;->c(I)V

    .line 2698504
    invoke-virtual {p0, v4, v5}, LX/186;->b(II)V

    .line 2698505
    const/4 v4, 0x1

    invoke-virtual {p0, v4, v6}, LX/186;->b(II)V

    .line 2698506
    const/4 v4, 0x2

    invoke-virtual {p0, v4, v7}, LX/186;->b(II)V

    .line 2698507
    const/4 v4, 0x3

    invoke-virtual {p0, v4, v8}, LX/186;->b(II)V

    .line 2698508
    invoke-virtual {p0}, LX/186;->d()I

    move-result v4

    .line 2698509
    invoke-virtual {p0, v4}, LX/186;->d(I)V

    goto :goto_1

    .line 2698510
    :cond_2
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLImage;->d()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 2698511
    const/4 p1, 0x1

    invoke-virtual {p0, p1}, LX/186;->c(I)V

    .line 2698512
    invoke-virtual {p0, v8, v9}, LX/186;->b(II)V

    .line 2698513
    invoke-virtual {p0}, LX/186;->d()I

    move-result v8

    .line 2698514
    invoke-virtual {p0, v8}, LX/186;->d(I)V

    goto :goto_2
.end method

.method private static b(LX/186;Lcom/facebook/graphql/model/GraphQLFundraiser;)I
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 2698515
    if-nez p1, :cond_0

    .line 2698516
    :goto_0
    return v0

    .line 2698517
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFundraiser;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v1

    .line 2698518
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFundraiser;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2698519
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFundraiser;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    const/4 v4, 0x0

    .line 2698520
    if-nez v3, :cond_1

    .line 2698521
    :goto_1
    move v3, v4

    .line 2698522
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFundraiser;->m()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 2698523
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFundraiser;->n()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v5

    const/4 v6, 0x0

    .line 2698524
    if-nez v5, :cond_2

    .line 2698525
    :goto_2
    move v5, v6

    .line 2698526
    const/4 v6, 0x5

    invoke-virtual {p0, v6}, LX/186;->c(I)V

    .line 2698527
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 2698528
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 2698529
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 2698530
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v4}, LX/186;->b(II)V

    .line 2698531
    const/4 v0, 0x4

    invoke-virtual {p0, v0, v5}, LX/186;->b(II)V

    .line 2698532
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 2698533
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 2698534
    :cond_1
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 2698535
    const/4 v6, 0x1

    invoke-virtual {p0, v6}, LX/186;->c(I)V

    .line 2698536
    invoke-virtual {p0, v4, v5}, LX/186;->b(II)V

    .line 2698537
    invoke-virtual {p0}, LX/186;->d()I

    move-result v4

    .line 2698538
    invoke-virtual {p0, v4}, LX/186;->d(I)V

    goto :goto_1

    .line 2698539
    :cond_2
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v7

    .line 2698540
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v8

    invoke-static {p0, v8}, LX/JUR;->a(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v8

    .line 2698541
    const/4 p1, 0x2

    invoke-virtual {p0, p1}, LX/186;->c(I)V

    .line 2698542
    invoke-virtual {p0, v6, v7}, LX/186;->b(II)V

    .line 2698543
    const/4 v6, 0x1

    invoke-virtual {p0, v6, v8}, LX/186;->b(II)V

    .line 2698544
    invoke-virtual {p0}, LX/186;->d()I

    move-result v6

    .line 2698545
    invoke-virtual {p0, v6}, LX/186;->d(I)V

    goto :goto_2
.end method

.method private static b(LX/186;Lcom/facebook/graphql/model/GraphQLNode;)I
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 2698546
    if-nez p1, :cond_1

    .line 2698547
    :cond_0
    :goto_0
    return v0

    .line 2698548
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    .line 2698549
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v2, 0x403827a

    if-ne v1, v2, :cond_0

    .line 2698550
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->cG()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 2698551
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->cN()Lcom/facebook/graphql/model/GraphQLEventViewerCapability;

    move-result-object v2

    const/4 v3, 0x0

    .line 2698552
    if-nez v2, :cond_2

    .line 2698553
    :goto_1
    move v2, v3

    .line 2698554
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2698555
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->gg()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v4

    const/4 v5, 0x0

    .line 2698556
    if-nez v4, :cond_3

    .line 2698557
    :goto_2
    move v4, v5

    .line 2698558
    const/4 v5, 0x4

    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 2698559
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 2698560
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 2698561
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 2698562
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v4}, LX/186;->b(II)V

    .line 2698563
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 2698564
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 2698565
    :cond_2
    const/4 v4, 0x1

    invoke-virtual {p0, v4}, LX/186;->c(I)V

    .line 2698566
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLEventViewerCapability;->w()Z

    move-result v4

    invoke-virtual {p0, v3, v4}, LX/186;->a(IZ)V

    .line 2698567
    invoke-virtual {p0}, LX/186;->d()I

    move-result v3

    .line 2698568
    invoke-virtual {p0, v3}, LX/186;->d(I)V

    goto :goto_1

    .line 2698569
    :cond_3
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 2698570
    const/4 p1, 0x1

    invoke-virtual {p0, p1}, LX/186;->c(I)V

    .line 2698571
    invoke-virtual {p0, v5, v6}, LX/186;->b(II)V

    .line 2698572
    invoke-virtual {p0}, LX/186;->d()I

    move-result v5

    .line 2698573
    invoke-virtual {p0, v5}, LX/186;->d(I)V

    goto :goto_2
.end method

.method private static b(LX/186;Lcom/facebook/graphql/model/GraphQLPage;)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2698574
    if-nez p1, :cond_0

    .line 2698575
    :goto_0
    return v0

    .line 2698576
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2698577
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2698578
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    invoke-static {p0, v3}, LX/JUR;->f(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v3

    .line 2698579
    const/4 v4, 0x4

    invoke-virtual {p0, v4}, LX/186;->c(I)V

    .line 2698580
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 2698581
    const/4 v0, 0x1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPage;->I()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/186;->a(IZ)V

    .line 2698582
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 2698583
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 2698584
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 2698585
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static b(LX/186;Lcom/facebook/graphql/model/GraphQLProfile;)I
    .locals 14

    .prologue
    .line 2698586
    if-nez p1, :cond_0

    .line 2698587
    const/4 v0, 0x0

    .line 2698588
    :goto_0
    return v0

    .line 2698589
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v0

    .line 2698590
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->p()LX/0Px;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v1

    .line 2698591
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->q()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 2698592
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->r()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v3

    const/4 v4, 0x0

    .line 2698593
    if-nez v3, :cond_1

    .line 2698594
    :goto_1
    move v3, v4

    .line 2698595
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->t()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    .line 2698596
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 2698597
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->D()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 2698598
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    invoke-static {p0, v7}, LX/JUR;->f(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v7

    .line 2698599
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->M()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 2698600
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->N()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v9

    invoke-virtual {p0, v9}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v9

    .line 2698601
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->P()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v10

    invoke-virtual {p0, v10}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v10

    .line 2698602
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->Q()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v11

    invoke-virtual {p0, v11}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v11

    .line 2698603
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->R()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v12

    invoke-virtual {p0, v12}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v12

    .line 2698604
    const/16 v13, 0x12

    invoke-virtual {p0, v13}, LX/186;->c(I)V

    .line 2698605
    const/4 v13, 0x0

    invoke-virtual {p0, v13, v0}, LX/186;->b(II)V

    .line 2698606
    const/4 v0, 0x1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->l()Z

    move-result v13

    invoke-virtual {p0, v0, v13}, LX/186;->a(IZ)V

    .line 2698607
    const/4 v0, 0x2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->m()Z

    move-result v13

    invoke-virtual {p0, v0, v13}, LX/186;->a(IZ)V

    .line 2698608
    const/4 v0, 0x3

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->n()Z

    move-result v13

    invoke-virtual {p0, v0, v13}, LX/186;->a(IZ)V

    .line 2698609
    const/4 v0, 0x4

    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 2698610
    const/4 v0, 0x5

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 2698611
    const/4 v0, 0x6

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 2698612
    const/4 v0, 0x7

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->s()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/186;->a(IZ)V

    .line 2698613
    const/16 v0, 0x8

    invoke-virtual {p0, v0, v4}, LX/186;->b(II)V

    .line 2698614
    const/16 v0, 0x9

    invoke-virtual {p0, v0, v5}, LX/186;->b(II)V

    .line 2698615
    const/16 v0, 0xa

    invoke-virtual {p0, v0, v6}, LX/186;->b(II)V

    .line 2698616
    const/16 v0, 0xb

    invoke-virtual {p0, v0, v7}, LX/186;->b(II)V

    .line 2698617
    const/16 v0, 0xc

    invoke-virtual {p0, v0, v8}, LX/186;->b(II)V

    .line 2698618
    const/16 v0, 0xd

    invoke-virtual {p0, v0, v9}, LX/186;->b(II)V

    .line 2698619
    const/16 v0, 0xe

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProfile;->O()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/186;->a(IZ)V

    .line 2698620
    const/16 v0, 0xf

    invoke-virtual {p0, v0, v10}, LX/186;->b(II)V

    .line 2698621
    const/16 v0, 0x10

    invoke-virtual {p0, v0, v11}, LX/186;->b(II)V

    .line 2698622
    const/16 v0, 0x11

    invoke-virtual {p0, v0, v12}, LX/186;->b(II)V

    .line 2698623
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 2698624
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto/16 :goto_0

    .line 2698625
    :cond_1
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v5

    const/4 v6, 0x0

    .line 2698626
    if-nez v5, :cond_2

    .line 2698627
    :goto_2
    move v5, v6

    .line 2698628
    const/4 v6, 0x1

    invoke-virtual {p0, v6}, LX/186;->c(I)V

    .line 2698629
    invoke-virtual {p0, v4, v5}, LX/186;->b(II)V

    .line 2698630
    invoke-virtual {p0}, LX/186;->d()I

    move-result v4

    .line 2698631
    invoke-virtual {p0, v4}, LX/186;->d(I)V

    goto/16 :goto_1

    .line 2698632
    :cond_2
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    const/4 v8, 0x0

    .line 2698633
    if-nez v7, :cond_3

    .line 2698634
    :goto_3
    move v7, v8

    .line 2698635
    const/4 v8, 0x1

    invoke-virtual {p0, v8}, LX/186;->c(I)V

    .line 2698636
    invoke-virtual {p0, v6, v7}, LX/186;->b(II)V

    .line 2698637
    invoke-virtual {p0}, LX/186;->d()I

    move-result v6

    .line 2698638
    invoke-virtual {p0, v6}, LX/186;->d(I)V

    goto :goto_2

    .line 2698639
    :cond_3
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2698640
    const/4 v5, 0x1

    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 2698641
    invoke-virtual {p0, v8, v3}, LX/186;->b(II)V

    .line 2698642
    invoke-virtual {p0}, LX/186;->d()I

    move-result v8

    .line 2698643
    invoke-virtual {p0, v8}, LX/186;->d(I)V

    goto :goto_3
.end method

.method public static b(LX/186;Lcom/facebook/graphql/model/GraphQLReactionStoryAction;)I
    .locals 55

    .prologue
    .line 2698644
    if-nez p1, :cond_0

    .line 2698645
    const/4 v2, 0x0

    .line 2698646
    :goto_0
    return v2

    .line 2698647
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v4

    .line 2698648
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/JUR;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)I

    move-result v5

    .line 2698649
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/JUR;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)I

    move-result v6

    .line 2698650
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->l()Lcom/facebook/graphql/model/GraphQLOpenGraphObject;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/JUR;->a(LX/186;Lcom/facebook/graphql/model/GraphQLOpenGraphObject;)I

    move-result v7

    .line 2698651
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->m()Lcom/facebook/graphql/enums/GraphQLReactionStoryActionStyle;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    .line 2698652
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->n()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/JUR;->a(LX/186;Lcom/facebook/graphql/model/GraphQLAlbum;)I

    move-result v9

    .line 2698653
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->o()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/JUR;->a(LX/186;Lcom/facebook/graphql/model/GraphQLProfile;)I

    move-result v10

    .line 2698654
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ag()Lcom/facebook/graphql/model/GraphQLCommerceStoreCollection;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/JUR;->a(LX/186;Lcom/facebook/graphql/model/GraphQLCommerceStoreCollection;)I

    move-result v11

    .line 2698655
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->q()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/JUR;->b(LX/186;Lcom/facebook/graphql/model/GraphQLComment;)I

    move-result v12

    .line 2698656
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->r()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 2698657
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->s()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 2698658
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->t()Lcom/facebook/graphql/model/GraphQLInlineActivity;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/JUR;->a(LX/186;Lcom/facebook/graphql/model/GraphQLInlineActivity;)I

    move-result v15

    .line 2698659
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->u()Lcom/facebook/graphql/enums/GraphQLFundraiserSupportersConnectionType;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v16

    .line 2698660
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->v()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    .line 2698661
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->w()Lcom/facebook/graphql/enums/GraphQLPhotosByCategoryEntryPoint;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v18

    .line 2698662
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->x()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/JUR;->a(LX/186;Lcom/facebook/graphql/model/GraphQLEvent;)I

    move-result v19

    .line 2698663
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->y()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/JUR;->b(LX/186;Lcom/facebook/graphql/model/GraphQLEvent;)I

    move-result v20

    .line 2698664
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->z()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/JUR;->a(LX/186;Lcom/facebook/graphql/model/GraphQLUser;)I

    move-result v21

    .line 2698665
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->A()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v22

    .line 2698666
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->B()Lcom/facebook/graphql/model/GraphQLFundraiser;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/JUR;->a(LX/186;Lcom/facebook/graphql/model/GraphQLFundraiser;)I

    move-result v23

    .line 2698667
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->C()Lcom/facebook/graphql/model/GraphQLFundraiser;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/JUR;->b(LX/186;Lcom/facebook/graphql/model/GraphQLFundraiser;)I

    move-result v24

    .line 2698668
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->D()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/JUR;->a(LX/186;Lcom/facebook/graphql/model/GraphQLGroup;)I

    move-result v25

    .line 2698669
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->E()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v26

    .line 2698670
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->F()Lcom/facebook/graphql/model/GraphQLJobOpening;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/JUR;->a(LX/186;Lcom/facebook/graphql/model/GraphQLJobOpening;)I

    move-result v27

    .line 2698671
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->G()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/JUR;->a(LX/186;Lcom/facebook/graphql/model/GraphQLLocation;)I

    move-result v28

    .line 2698672
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->H()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/JUR;->a(LX/186;Lcom/facebook/graphql/model/GraphQLPage;)I

    move-result v29

    .line 2698673
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->I()Lcom/facebook/graphql/enums/GraphQLNearbyFriendsNuxType;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v30

    .line 2698674
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ah()Lcom/facebook/graphql/model/GraphQLOfferView;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/JUR;->a(LX/186;Lcom/facebook/graphql/model/GraphQLOfferView;)I

    move-result v31

    .line 2698675
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->J()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/JUR;->b(LX/186;Lcom/facebook/graphql/model/GraphQLPage;)I

    move-result v32

    .line 2698676
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->K()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v33

    .line 2698677
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->L()Lcom/facebook/graphql/enums/GraphQLPagePhotoSourceType;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v34

    .line 2698678
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->M()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v35

    .line 2698679
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->N()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/JUR;->c(LX/186;Lcom/facebook/graphql/model/GraphQLPage;)I

    move-result v36

    .line 2698680
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->O()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v37

    .line 2698681
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->P()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v38

    .line 2698682
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->Q()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/JUR;->a(LX/186;Lcom/facebook/graphql/model/GraphQLEntity;)I

    move-result v39

    .line 2698683
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->R()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/JUR;->b(LX/186;Lcom/facebook/graphql/model/GraphQLProfile;)I

    move-result v40

    .line 2698684
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->S()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v41

    .line 2698685
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->T()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v42

    .line 2698686
    const/4 v2, 0x0

    .line 2698687
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->U()LX/0Px;

    move-result-object v43

    .line 2698688
    if-eqz v43, :cond_2

    .line 2698689
    invoke-virtual/range {v43 .. v43}, LX/0Px;->size()I

    move-result v2

    new-array v0, v2, [I

    move-object/from16 v44, v0

    .line 2698690
    const/4 v2, 0x0

    move v3, v2

    :goto_1
    invoke-virtual/range {v43 .. v43}, LX/0Px;->size()I

    move-result v2

    if-ge v3, v2, :cond_1

    .line 2698691
    move-object/from16 v0, v43

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLUser;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/JUR;->b(LX/186;Lcom/facebook/graphql/model/GraphQLUser;)I

    move-result v2

    aput v2, v44, v3

    .line 2698692
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 2698693
    :cond_1
    const/4 v2, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v44

    invoke-virtual {v0, v1, v2}, LX/186;->a([IZ)I

    move-result v2

    .line 2698694
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->V()Lcom/facebook/graphql/model/GraphQLReactionUnit;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/JUR;->a(LX/186;Lcom/facebook/graphql/model/GraphQLReactionUnit;)I

    move-result v3

    .line 2698695
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->W()Lcom/facebook/graphql/model/GraphQLProductItem;

    move-result-object v43

    move-object/from16 v0, p0

    move-object/from16 v1, v43

    invoke-static {v0, v1}, LX/JUR;->a(LX/186;Lcom/facebook/graphql/model/GraphQLProductItem;)I

    move-result v43

    .line 2698696
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->X()Ljava/lang/String;

    move-result-object v44

    move-object/from16 v0, p0

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v44

    .line 2698697
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->Y()Ljava/lang/String;

    move-result-object v45

    move-object/from16 v0, p0

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v45

    .line 2698698
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ai()Lcom/facebook/graphql/model/GraphQLPageStatusCard;

    move-result-object v46

    move-object/from16 v0, p0

    move-object/from16 v1, v46

    invoke-static {v0, v1}, LX/JUR;->a(LX/186;Lcom/facebook/graphql/model/GraphQLPageStatusCard;)I

    move-result v46

    .line 2698699
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->Z()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v47

    move-object/from16 v0, p0

    move-object/from16 v1, v47

    invoke-static {v0, v1}, LX/JUR;->b(LX/186;Lcom/facebook/graphql/model/GraphQLStory;)I

    move-result v47

    .line 2698700
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ab()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, p0

    move-object/from16 v1, v48

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v48

    .line 2698701
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ac()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v49

    move-object/from16 v0, p0

    move-object/from16 v1, v49

    invoke-static {v0, v1}, LX/JUR;->b(LX/186;Lcom/facebook/graphql/model/GraphQLNode;)I

    move-result v49

    .line 2698702
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->aj()Lcom/facebook/graphql/model/GraphQLMessageThreadKey;

    move-result-object v50

    move-object/from16 v0, p0

    move-object/from16 v1, v50

    invoke-static {v0, v1}, LX/JUR;->a(LX/186;Lcom/facebook/graphql/model/GraphQLMessageThreadKey;)I

    move-result v50

    .line 2698703
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ad()Ljava/lang/String;

    move-result-object v51

    move-object/from16 v0, p0

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v51

    .line 2698704
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->ae()Ljava/lang/String;

    move-result-object v52

    move-object/from16 v0, p0

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v52

    .line 2698705
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->af()Lcom/facebook/graphql/model/GraphQLVideoChannel;

    move-result-object v53

    move-object/from16 v0, p0

    move-object/from16 v1, v53

    invoke-static {v0, v1}, LX/JUR;->a(LX/186;Lcom/facebook/graphql/model/GraphQLVideoChannel;)I

    move-result v53

    .line 2698706
    const/16 v54, 0x3a

    move-object/from16 v0, p0

    move/from16 v1, v54

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 2698707
    const/16 v54, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v54

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 2698708
    const/4 v4, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, LX/186;->b(II)V

    .line 2698709
    const/4 v4, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v6}, LX/186;->b(II)V

    .line 2698710
    const/4 v4, 0x7

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v7}, LX/186;->b(II)V

    .line 2698711
    const/16 v4, 0x8

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v8}, LX/186;->b(II)V

    .line 2698712
    const/16 v4, 0x9

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v9}, LX/186;->b(II)V

    .line 2698713
    const/16 v4, 0xa

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v10}, LX/186;->b(II)V

    .line 2698714
    const/16 v4, 0xb

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->p()Z

    move-result v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 2698715
    const/16 v4, 0xc

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v11}, LX/186;->b(II)V

    .line 2698716
    const/16 v4, 0xd

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v12}, LX/186;->b(II)V

    .line 2698717
    const/16 v4, 0xe

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v13}, LX/186;->b(II)V

    .line 2698718
    const/16 v4, 0xf

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v14}, LX/186;->b(II)V

    .line 2698719
    const/16 v4, 0x10

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v15}, LX/186;->b(II)V

    .line 2698720
    const/16 v4, 0x11

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2698721
    const/16 v4, 0x12

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2698722
    const/16 v4, 0x13

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2698723
    const/16 v4, 0x14

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2698724
    const/16 v4, 0x15

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2698725
    const/16 v4, 0x16

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2698726
    const/16 v4, 0x17

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2698727
    const/16 v4, 0x18

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2698728
    const/16 v4, 0x19

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2698729
    const/16 v4, 0x1a

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2698730
    const/16 v4, 0x1b

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2698731
    const/16 v4, 0x1c

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2698732
    const/16 v4, 0x1d

    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2698733
    const/16 v4, 0x1e

    move-object/from16 v0, p0

    move/from16 v1, v29

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2698734
    const/16 v4, 0x1f

    move-object/from16 v0, p0

    move/from16 v1, v30

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2698735
    const/16 v4, 0x20

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2698736
    const/16 v4, 0x21

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2698737
    const/16 v4, 0x22

    move-object/from16 v0, p0

    move/from16 v1, v33

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2698738
    const/16 v4, 0x23

    move-object/from16 v0, p0

    move/from16 v1, v34

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2698739
    const/16 v4, 0x24

    move-object/from16 v0, p0

    move/from16 v1, v35

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2698740
    const/16 v4, 0x25

    move-object/from16 v0, p0

    move/from16 v1, v36

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2698741
    const/16 v4, 0x26

    move-object/from16 v0, p0

    move/from16 v1, v37

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2698742
    const/16 v4, 0x27

    move-object/from16 v0, p0

    move/from16 v1, v38

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2698743
    const/16 v4, 0x28

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2698744
    const/16 v4, 0x29

    move-object/from16 v0, p0

    move/from16 v1, v40

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2698745
    const/16 v4, 0x2a

    move-object/from16 v0, p0

    move/from16 v1, v41

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2698746
    const/16 v4, 0x2b

    move-object/from16 v0, p0

    move/from16 v1, v42

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 2698747
    const/16 v4, 0x2c

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v2}, LX/186;->b(II)V

    .line 2698748
    const/16 v2, 0x2d

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 2698749
    const/16 v2, 0x2e

    move-object/from16 v0, p0

    move/from16 v1, v43

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2698750
    const/16 v2, 0x2f

    move-object/from16 v0, p0

    move/from16 v1, v44

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2698751
    const/16 v2, 0x30

    move-object/from16 v0, p0

    move/from16 v1, v45

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2698752
    const/16 v2, 0x31

    move-object/from16 v0, p0

    move/from16 v1, v46

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2698753
    const/16 v2, 0x32

    move-object/from16 v0, p0

    move/from16 v1, v47

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2698754
    const/16 v2, 0x33

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLReactionStoryAction;->aa()Z

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 2698755
    const/16 v2, 0x34

    move-object/from16 v0, p0

    move/from16 v1, v48

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2698756
    const/16 v2, 0x35

    move-object/from16 v0, p0

    move/from16 v1, v49

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2698757
    const/16 v2, 0x36

    move-object/from16 v0, p0

    move/from16 v1, v50

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2698758
    const/16 v2, 0x37

    move-object/from16 v0, p0

    move/from16 v1, v51

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2698759
    const/16 v2, 0x38

    move-object/from16 v0, p0

    move/from16 v1, v52

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2698760
    const/16 v2, 0x39

    move-object/from16 v0, p0

    move/from16 v1, v53

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2698761
    invoke-virtual/range {p0 .. p0}, LX/186;->d()I

    move-result v2

    .line 2698762
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->d(I)V

    goto/16 :goto_0
.end method

.method private static b(LX/186;Lcom/facebook/graphql/model/GraphQLStory;)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2698763
    if-nez p1, :cond_0

    .line 2698764
    :goto_0
    return v0

    .line 2698765
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2698766
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    const/4 v3, 0x0

    .line 2698767
    if-nez v2, :cond_1

    .line 2698768
    :goto_1
    move v2, v3

    .line 2698769
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2698770
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 2698771
    const/4 v5, 0x4

    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 2698772
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 2698773
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 2698774
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 2698775
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v4}, LX/186;->b(II)V

    .line 2698776
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 2698777
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 2698778
    :cond_1
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->u()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 2698779
    const/4 v5, 0x1

    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 2698780
    invoke-virtual {p0, v3, v4}, LX/186;->b(II)V

    .line 2698781
    invoke-virtual {p0}, LX/186;->d()I

    move-result v3

    .line 2698782
    invoke-virtual {p0, v3}, LX/186;->d(I)V

    goto :goto_1
.end method

.method private static b(LX/186;Lcom/facebook/graphql/model/GraphQLUser;)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2698783
    if-nez p1, :cond_0

    .line 2698784
    :goto_0
    return v0

    .line 2698785
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLUser;->v()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2698786
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLUser;->F()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2698787
    const/4 v3, 0x2

    invoke-virtual {p0, v3}, LX/186;->c(I)V

    .line 2698788
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 2698789
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 2698790
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 2698791
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static c(LX/186;Lcom/facebook/graphql/model/GraphQLPage;)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2698792
    if-nez p1, :cond_0

    .line 2698793
    :goto_0
    return v0

    .line 2698794
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2698795
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2698796
    const/4 v3, 0x2

    invoke-virtual {p0, v3}, LX/186;->c(I)V

    .line 2698797
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 2698798
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 2698799
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 2698800
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static f(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2698801
    if-nez p1, :cond_0

    .line 2698802
    :goto_0
    return v0

    .line 2698803
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2698804
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 2698805
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 2698806
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 2698807
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method
