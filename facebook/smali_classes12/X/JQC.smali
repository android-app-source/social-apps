.class public LX/JQC;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/3mL;

.field public final b:LX/JQ7;


# direct methods
.method public constructor <init>(LX/3mL;LX/JQ7;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2691182
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2691183
    iput-object p1, p0, LX/JQC;->a:LX/3mL;

    .line 2691184
    iput-object p2, p0, LX/JQC;->b:LX/JQ7;

    .line 2691185
    return-void
.end method

.method public static a(LX/0QB;)LX/JQC;
    .locals 5

    .prologue
    .line 2691186
    const-class v1, LX/JQC;

    monitor-enter v1

    .line 2691187
    :try_start_0
    sget-object v0, LX/JQC;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2691188
    sput-object v2, LX/JQC;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2691189
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2691190
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2691191
    new-instance p0, LX/JQC;

    invoke-static {v0}, LX/3mL;->a(LX/0QB;)LX/3mL;

    move-result-object v3

    check-cast v3, LX/3mL;

    const-class v4, LX/JQ7;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/JQ7;

    invoke-direct {p0, v3, v4}, LX/JQC;-><init>(LX/3mL;LX/JQ7;)V

    .line 2691192
    move-object v0, p0

    .line 2691193
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2691194
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JQC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2691195
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2691196
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
