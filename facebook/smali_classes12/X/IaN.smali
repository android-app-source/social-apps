.class public final LX/IaN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;)V
    .locals 0

    .prologue
    .line 2590891
    iput-object p1, p0, LX/IaN;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 2590892
    iget-object v0, p0, LX/IaN;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;

    iget-object v0, v0, Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;->i:LX/IZw;

    const-string v1, "click_log_in_dialog_log_in_button"

    invoke-virtual {v0, v1}, LX/IZw;->a(Ljava/lang/String;)V

    .line 2590893
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 2590894
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2590895
    const-string v1, "next_step"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2590896
    iget-object v1, p0, LX/IaN;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;

    invoke-virtual {v1}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2590897
    iget-object v0, p0, LX/IaN;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessCreateAccountFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2590898
    return-void
.end method
