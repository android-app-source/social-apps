.class public LX/IPn;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/IPl;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/feed/drawer/GroupMallDrawerItemComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2574607
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/IPn;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/feed/drawer/GroupMallDrawerItemComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2574608
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2574609
    iput-object p1, p0, LX/IPn;->b:LX/0Ot;

    .line 2574610
    return-void
.end method

.method public static a(LX/0QB;)LX/IPn;
    .locals 4

    .prologue
    .line 2574611
    const-class v1, LX/IPn;

    monitor-enter v1

    .line 2574612
    :try_start_0
    sget-object v0, LX/IPn;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2574613
    sput-object v2, LX/IPn;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2574614
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2574615
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2574616
    new-instance v3, LX/IPn;

    const/16 p0, 0x23f9

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/IPn;-><init>(LX/0Ot;)V

    .line 2574617
    move-object v0, v3

    .line 2574618
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2574619
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IPn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2574620
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2574621
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 13

    .prologue
    .line 2574622
    check-cast p2, LX/IPm;

    .line 2574623
    iget-object v0, p0, LX/IPn;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/drawer/GroupMallDrawerItemComponentSpec;

    iget-object v1, p2, LX/IPm;->a:Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel;

    iget v2, p2, LX/IPm;->b:I

    iget-boolean v3, p2, LX/IPm;->c:Z

    const/4 v5, 0x0

    const/4 p2, 0x1

    .line 2574624
    iget-object v4, v0, Lcom/facebook/groups/feed/drawer/GroupMallDrawerItemComponentSpec;->b:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1Ad;

    .line 2574625
    const-string v7, ""

    .line 2574626
    const/4 v6, 0x0

    .line 2574627
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel;

    move-result-object v8

    if-eqz v8, :cond_2

    .line 2574628
    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v8

    .line 2574629
    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel;->l()Ljava/lang/String;

    move-result-object v7

    .line 2574630
    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel;->m()I

    move-result v6

    .line 2574631
    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel$GroupCoverPhotoModel;

    move-result-object v9

    if-eqz v9, :cond_1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel$GroupCoverPhotoModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel$GroupCoverPhotoModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel$GroupCoverPhotoModel$PhotoModel;

    move-result-object v9

    if-eqz v9, :cond_1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel$GroupCoverPhotoModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel$GroupCoverPhotoModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel$GroupCoverPhotoModel$PhotoModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel$GroupCoverPhotoModel$PhotoModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel$GroupCoverPhotoModel$PhotoModel$ImageModel;

    move-result-object v9

    if-eqz v9, :cond_1

    .line 2574632
    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel$GroupCoverPhotoModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel$GroupCoverPhotoModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel$GroupCoverPhotoModel$PhotoModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel$GroupCoverPhotoModel$PhotoModel;->a()Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel$GroupCoverPhotoModel$PhotoModel$ImageModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/groups/feed/protocol/FetchGroupMallDrawerGraphQLModels$GroupMallDrawerQueryModel$AccountUserModel$GroupsModel$EdgesModel$NodeModel$GroupCoverPhotoModel$PhotoModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v9

    .line 2574633
    :goto_0
    if-nez v3, :cond_0

    if-lez v6, :cond_0

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v5

    const v6, 0x7f020c40

    invoke-virtual {v5, v6}, LX/1o5;->h(I)LX/1o5;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    invoke-interface {v5, p2}, LX/1Di;->c(I)LX/1Di;

    move-result-object v5

    const v6, 0x7f0b2414

    invoke-interface {v5, p2, v6}, LX/1Di;->l(II)LX/1Di;

    move-result-object v5

    const/4 v6, 0x5

    const v10, 0x7f0b2415

    invoke-interface {v5, v6, v10}, LX/1Di;->l(II)LX/1Di;

    move-result-object v5

    const v6, 0x7f0b2417

    invoke-interface {v5, v6}, LX/1Di;->i(I)LX/1Di;

    move-result-object v5

    const v6, 0x7f0b2417

    invoke-interface {v5, v6}, LX/1Di;->q(I)LX/1Di;

    move-result-object v5

    .line 2574634
    :cond_0
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    const v10, 0x7f0b240e

    invoke-interface {v6, v10}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v6

    const/4 v10, 0x4

    const v11, 0x7f0b2412

    invoke-interface {v6, v10, v11}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v6

    const/4 v10, 0x7

    const v11, 0x7f0b2413    # 1.8495E38f

    invoke-interface {v6, v10, v11}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v6

    .line 2574635
    const v10, -0x5b963754

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v8, v11, v12

    const/4 v12, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v11, v12

    invoke-static {p1, v10, v11}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v10

    move-object v8, v10

    .line 2574636
    invoke-interface {v6, v8}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v6

    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v8

    const v10, 0x7f0219a5

    invoke-virtual {v8, v10}, LX/1up;->h(I)LX/1up;

    move-result-object v8

    invoke-static {}, LX/4Ab;->e()LX/4Ab;

    move-result-object v10

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0a0118

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getColor(I)I

    move-result v11

    const/high16 v12, 0x3f800000    # 1.0f

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p0

    invoke-static {p2, v12, p0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v12

    invoke-virtual {v10, v11, v12}, LX/4Ab;->a(IF)LX/4Ab;

    move-result-object v10

    invoke-virtual {v8, v10}, LX/1up;->a(LX/4Ab;)LX/1up;

    move-result-object v8

    invoke-virtual {v4, v9}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v4

    sget-object v9, Lcom/facebook/groups/feed/drawer/GroupMallDrawerItemComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v4, v9}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v4

    invoke-virtual {v4}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v4

    invoke-virtual {v8, v4}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const v8, 0x7f0b240f

    invoke-interface {v4, v8}, LX/1Di;->q(I)LX/1Di;

    move-result-object v4

    invoke-interface {v6, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v7}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    const v6, 0x7f0b2411

    invoke-virtual {v5, v6}, LX/1ne;->q(I)LX/1ne;

    move-result-object v5

    const/4 v6, 0x2

    invoke-virtual {v5, v6}, LX/1ne;->j(I)LX/1ne;

    move-result-object v5

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v5, v6}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v5

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v5, v6}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const v6, 0x7f0b2410

    invoke-interface {v5, v6}, LX/1Di;->q(I)LX/1Di;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 2574637
    return-object v0

    :cond_1
    move-object v9, v5

    goto/16 :goto_0

    :cond_2
    move-object v8, v5

    move-object v9, v5

    goto/16 :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2574638
    invoke-static {}, LX/1dS;->b()V

    .line 2574639
    iget v0, p1, LX/1dQ;->b:I

    .line 2574640
    packed-switch v0, :pswitch_data_0

    .line 2574641
    :goto_0
    return-object v3

    .line 2574642
    :pswitch_0
    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    check-cast v0, Ljava/lang/String;

    iget-object v1, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 2574643
    iget-object p1, p0, LX/IPn;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/facebook/groups/feed/drawer/GroupMallDrawerItemComponentSpec;

    .line 2574644
    if-nez v0, :cond_1

    .line 2574645
    :cond_0
    :goto_1
    goto :goto_0

    .line 2574646
    :cond_1
    iget-object p2, p1, Lcom/facebook/groups/feed/drawer/GroupMallDrawerItemComponentSpec;->g:LX/0Ot;

    invoke-interface {p2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/0if;

    sget-object p0, LX/0ig;->aL:LX/0ih;

    const-string v2, "click_group"

    invoke-virtual {p2, p0, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2574647
    iget-object p2, p1, Lcom/facebook/groups/feed/drawer/GroupMallDrawerItemComponentSpec;->c:LX/0Ot;

    invoke-interface {p2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/DOL;

    const/4 p0, 0x0

    invoke-virtual {p2, v0, p0}, LX/DOL;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p0

    .line 2574648
    const-string p2, "drawer_position"

    invoke-virtual {p0, p2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2574649
    iget-object p2, p1, Lcom/facebook/groups/feed/drawer/GroupMallDrawerItemComponentSpec;->d:LX/0Ot;

    invoke-interface {p2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p1, Lcom/facebook/groups/feed/drawer/GroupMallDrawerItemComponentSpec;->e:Landroid/content/Context;

    invoke-interface {p2, p0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2574650
    iget-object p2, p1, Lcom/facebook/groups/feed/drawer/GroupMallDrawerItemComponentSpec;->e:Landroid/content/Context;

    instance-of p2, p2, Landroid/app/Activity;

    if-eqz p2, :cond_0

    iget-object p2, p1, Lcom/facebook/groups/feed/drawer/GroupMallDrawerItemComponentSpec;->f:LX/0Ot;

    invoke-interface {p2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/0Uh;

    const/16 p0, 0x4f0

    const/4 v2, 0x0

    invoke-virtual {p2, p0, v2}, LX/0Uh;->a(IZ)Z

    move-result p2

    if-nez p2, :cond_0

    .line 2574651
    iget-object p2, p1, Lcom/facebook/groups/feed/drawer/GroupMallDrawerItemComponentSpec;->e:Landroid/content/Context;

    check-cast p2, Landroid/app/Activity;

    invoke-virtual {p2}, Landroid/app/Activity;->finish()V

    goto :goto_1

    :pswitch_data_0
    .packed-switch -0x5b963754
        :pswitch_0
    .end packed-switch
.end method
