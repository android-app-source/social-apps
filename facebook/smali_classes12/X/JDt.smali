.class public final LX/JDt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AQ7;


# instance fields
.field private final a:LX/JDv;


# direct methods
.method public constructor <init>(LX/JDv;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2664749
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2664750
    iput-object p1, p0, LX/JDt;->a:LX/JDv;

    .line 2664751
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;LX/B5j;LX/B5f;)LX/AQ9;
    .locals 9
    .param p3    # LX/B5f;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData::",
            "Lcom/facebook/ipc/composer/plugin/ComposerPluginDataProvider;",
            "DerivedData::",
            "Lcom/facebook/ipc/composer/plugin/ComposerPluginDerivedDataProvider;",
            "Mutation::",
            "Lcom/facebook/ipc/composer/plugin/ComposerPluginMutation",
            "<TMutation;>;>(",
            "Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;",
            "LX/B5j",
            "<TModelData;TDerivedData;TMutation;>;",
            "LX/B5f;",
            ")",
            "Lcom/facebook/ipc/composer/plugin/ComposerPlugin",
            "<TModelData;TDerivedData;TMutation;>;"
        }
    .end annotation

    .prologue
    .line 2664752
    iget-object v0, p0, LX/JDt;->a:LX/JDv;

    .line 2664753
    new-instance v1, LX/JDu;

    const-class v2, Landroid/content/Context;

    invoke-interface {v0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    const-class v2, LX/AR9;

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/AR9;

    const-class v2, LX/Hu3;

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/Hu3;

    const-class v2, LX/AQx;

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/AQx;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v7

    check-cast v7, LX/0ad;

    invoke-static {v0}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v8

    check-cast v8, Landroid/view/inputmethod/InputMethodManager;

    move-object v2, p2

    invoke-direct/range {v1 .. v8}, LX/JDu;-><init>(LX/B5j;Landroid/content/Context;LX/AR9;LX/Hu3;LX/AQx;LX/0ad;Landroid/view/inputmethod/InputMethodManager;)V

    .line 2664754
    move-object v0, v1

    .line 2664755
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2664756
    const-string v0, "LifeEventComposerPluginConfig"

    return-object v0
.end method
