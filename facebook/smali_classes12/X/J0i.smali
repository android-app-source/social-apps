.class public LX/J0i;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/payments/p2p/service/model/transactions/SendCampaignPaymentMessageParams;",
        "Lcom/facebook/payments/p2p/service/model/transactions/SendCampaignPaymentMessageResult;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2637693
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2637694
    iput-object p1, p0, LX/J0i;->a:LX/0Or;

    .line 2637695
    return-void
.end method

.method public static a(LX/0QB;)LX/J0i;
    .locals 2

    .prologue
    .line 2637690
    new-instance v0, LX/J0i;

    const/16 v1, 0x12cc

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-direct {v0, v1}, LX/J0i;-><init>(LX/0Or;)V

    .line 2637691
    move-object v0, v0

    .line 2637692
    return-object v0
.end method

.method private static a(LX/0Px;)Ljava/lang/String;
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 2637641
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ","

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-static {v1, v2}, LX/0YN;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 5

    .prologue
    .line 2637654
    check-cast p1, Lcom/facebook/payments/p2p/service/model/transactions/SendCampaignPaymentMessageParams;

    .line 2637655
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2637656
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "receiver_ids"

    .line 2637657
    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/transactions/SendCampaignPaymentMessageParams;->b:LX/0Px;

    move-object v3, v3

    .line 2637658
    invoke-static {v3}, LX/J0i;->a(LX/0Px;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2637659
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "campaign_name"

    .line 2637660
    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/transactions/SendCampaignPaymentMessageParams;->c:Ljava/lang/String;

    move-object v3, v3

    .line 2637661
    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2637662
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "external_request_id"

    .line 2637663
    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/transactions/SendCampaignPaymentMessageParams;->d:Ljava/lang/String;

    move-object v3, v3

    .line 2637664
    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2637665
    iget-object v0, p1, Lcom/facebook/payments/p2p/service/model/transactions/SendCampaignPaymentMessageParams;->e:Ljava/lang/String;

    move-object v0, v0

    .line 2637666
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2637667
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "message"

    .line 2637668
    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/transactions/SendCampaignPaymentMessageParams;->e:Ljava/lang/String;

    move-object v3, v3

    .line 2637669
    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2637670
    :cond_0
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "format"

    const-string v3, "json"

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2637671
    iget-object v0, p0, LX/J0i;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2637672
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "null ViewerContextUser found sending payments via campaign flow."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2637673
    :cond_1
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    const-string v2, "p2p_campaign_transfers"

    .line 2637674
    iput-object v2, v0, LX/14O;->b:Ljava/lang/String;

    .line 2637675
    move-object v0, v0

    .line 2637676
    const-string v2, "POST"

    .line 2637677
    iput-object v2, v0, LX/14O;->c:Ljava/lang/String;

    .line 2637678
    move-object v2, v0

    .line 2637679
    const-string v3, "/%s/%s"

    iget-object v0, p0, LX/J0i;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2637680
    iget-object v4, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v4

    .line 2637681
    const-string v4, "p2p_campaign_transfers"

    invoke-static {v3, v0, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2637682
    iput-object v0, v2, LX/14O;->d:Ljava/lang/String;

    .line 2637683
    move-object v0, v2

    .line 2637684
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 2637685
    move-object v0, v0

    .line 2637686
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2637687
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2637688
    move-object v0, v0

    .line 2637689
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2637642
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2637643
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 2637644
    invoke-virtual {v0}, LX/0lF;->k()LX/0nH;

    move-result-object v1

    sget-object v2, LX/0nH;->ARRAY:LX/0nH;

    if-eq v1, v2, :cond_0

    .line 2637645
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Invalid response type %s received from server"

    invoke-virtual {v0}, LX/0lF;->k()LX/0nH;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2637646
    :cond_0
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 2637647
    invoke-virtual {v0}, LX/0lF;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 2637648
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2637649
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 2637650
    const-string v3, "id"

    invoke-virtual {v0, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    if-nez v3, :cond_1

    .line 2637651
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid response received from server - \'id\' not found."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2637652
    :cond_1
    const-string v3, "id"

    invoke-virtual {v0, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 2637653
    :cond_2
    new-instance v0, Lcom/facebook/payments/p2p/service/model/transactions/SendCampaignPaymentMessageResult;

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/payments/p2p/service/model/transactions/SendCampaignPaymentMessageResult;-><init>(LX/0Px;)V

    return-object v0
.end method
