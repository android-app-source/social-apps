.class public final LX/IQm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesAdapter;

.field public final b:LX/IQn;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesAdapter;LX/IQn;)V
    .locals 0

    .prologue
    .line 2575681
    iput-object p1, p0, LX/IQm;->a:Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2575682
    iput-object p2, p0, LX/IQm;->b:LX/IQn;

    .line 2575683
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x2

    const v0, -0x150f3231

    invoke-static {v4, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2575669
    iget-object v1, p0, LX/IQm;->b:LX/IQn;

    iget-object v1, v1, LX/IQn;->p:LX/IQj;

    invoke-static {v1, v2}, LX/IQj;->a$redex0(LX/IQj;Z)V

    .line 2575670
    iget-object v1, p0, LX/IQm;->b:LX/IQn;

    invoke-virtual {v1}, LX/1a1;->e()I

    move-result v1

    .line 2575671
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 2575672
    iget-object v2, p0, LX/IQm;->a:Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesAdapter;

    invoke-virtual {v2, v1}, LX/1OM;->i_(I)V

    .line 2575673
    :cond_0
    iget-object v1, p0, LX/IQm;->b:LX/IQn;

    iget-object v1, v1, LX/IQn;->p:LX/IQj;

    iget-object v1, v1, LX/IQj;->b:Ljava/lang/String;

    iget-object v2, p0, LX/IQm;->a:Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesAdapter;

    iget-object v2, v2, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesAdapter;->f:Ljava/lang/String;

    .line 2575674
    new-instance v3, LX/4Eu;

    invoke-direct {v3}, LX/4Eu;-><init>()V

    invoke-virtual {v3, v1}, LX/4Eu;->a(Ljava/lang/String;)LX/4Eu;

    move-result-object v3

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v5

    invoke-virtual {v3, v5}, LX/4Eu;->a(Ljava/util/List;)LX/4Eu;

    move-result-object v3

    const-string v5, "GROUP_MALL_SUGGESTIONS"

    invoke-virtual {v3, v5}, LX/4Eu;->b(Ljava/lang/String;)LX/4Eu;

    move-result-object v3

    .line 2575675
    invoke-static {}, LX/9KI;->a()LX/9KH;

    move-result-object v5

    .line 2575676
    const-string p1, "input"

    invoke-virtual {v5, p1, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2575677
    invoke-static {v5}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v3

    move-object v1, v3

    .line 2575678
    iget-object v2, p0, LX/IQm;->a:Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesAdapter;

    iget-object v2, v2, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesAdapter;->a:LX/0tX;

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2575679
    new-instance v2, LX/IQl;

    invoke-direct {v2, p0}, LX/IQl;-><init>(LX/IQm;)V

    iget-object v3, p0, LX/IQm;->a:Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesAdapter;

    iget-object v3, v3, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesAdapter;->b:Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2575680
    const v1, 0x743d083e

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
