.class public final enum LX/Hc5;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Hc5;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Hc5;

.field public static final enum Closed:LX/Hc5;

.field public static final enum Open:LX/Hc5;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2486753
    new-instance v0, LX/Hc5;

    const-string v1, "Open"

    invoke-direct {v0, v1, v2}, LX/Hc5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hc5;->Open:LX/Hc5;

    .line 2486754
    new-instance v0, LX/Hc5;

    const-string v1, "Closed"

    invoke-direct {v0, v1, v3}, LX/Hc5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hc5;->Closed:LX/Hc5;

    .line 2486755
    const/4 v0, 0x2

    new-array v0, v0, [LX/Hc5;

    sget-object v1, LX/Hc5;->Open:LX/Hc5;

    aput-object v1, v0, v2

    sget-object v1, LX/Hc5;->Closed:LX/Hc5;

    aput-object v1, v0, v3

    sput-object v0, LX/Hc5;->$VALUES:[LX/Hc5;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2486750
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Hc5;
    .locals 1

    .prologue
    .line 2486752
    const-class v0, LX/Hc5;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Hc5;

    return-object v0
.end method

.method public static values()[LX/Hc5;
    .locals 1

    .prologue
    .line 2486751
    sget-object v0, LX/Hc5;->$VALUES:[LX/Hc5;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Hc5;

    return-object v0
.end method
