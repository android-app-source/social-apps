.class public final LX/I7h;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:I

.field public final synthetic b:LX/I7i;


# direct methods
.method public constructor <init>(LX/I7i;I)V
    .locals 0

    .prologue
    .line 2540079
    iput-object p1, p0, LX/I7h;->b:LX/I7i;

    iput p2, p0, LX/I7h;->a:I

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2540080
    iget v0, p0, LX/I7h;->a:I

    const/16 v1, 0x1f6

    if-ne v0, v1, :cond_0

    .line 2540081
    iget-object v0, p0, LX/I7h;->b:LX/I7i;

    iget-object v0, v0, LX/I7i;->i:LX/Bl6;

    new-instance v1, LX/BlN;

    sget-object v2, LX/BlI;->FAILURE:LX/BlI;

    invoke-direct {v1, v2}, LX/BlN;-><init>(LX/BlI;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2540082
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2540071
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 2540072
    iget v0, p0, LX/I7h;->a:I

    const/16 v1, 0x1f6

    if-ne v0, v1, :cond_0

    .line 2540073
    iget-boolean v0, p1, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v0, v0

    .line 2540074
    if-eqz v0, :cond_1

    .line 2540075
    iget-object v0, p0, LX/I7h;->b:LX/I7i;

    iget-object v0, v0, LX/I7i;->g:LX/3RX;

    const-string v1, "post_main"

    invoke-virtual {v0, v1}, LX/3RX;->a(Ljava/lang/String;)V

    .line 2540076
    iget-object v0, p0, LX/I7h;->b:LX/I7i;

    iget-object v0, v0, LX/I7i;->i:LX/Bl6;

    new-instance v1, LX/BlN;

    sget-object v2, LX/BlI;->SUCCESS:LX/BlI;

    invoke-direct {v1, v2}, LX/BlN;-><init>(LX/BlI;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2540077
    :cond_0
    :goto_0
    return-void

    .line 2540078
    :cond_1
    iget-object v0, p0, LX/I7h;->b:LX/I7i;

    iget-object v0, v0, LX/I7i;->i:LX/Bl6;

    new-instance v1, LX/BlN;

    sget-object v2, LX/BlI;->FAILURE:LX/BlI;

    invoke-direct {v1, v2}, LX/BlN;-><init>(LX/BlI;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    goto :goto_0
.end method
