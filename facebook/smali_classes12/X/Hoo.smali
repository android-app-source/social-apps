.class public LX/Hoo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final b:Ljava/lang/String;

.field private static volatile d:LX/Hoo;


# instance fields
.field public a:LX/0Uh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:Landroid/content/Context;
    .annotation build Lcom/facebook/inject/ForAppContext;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2507062
    const-class v0, LX/Hoo;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Hoo;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2507063
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/Hoo;
    .locals 5

    .prologue
    .line 2507064
    sget-object v0, LX/Hoo;->d:LX/Hoo;

    if-nez v0, :cond_1

    .line 2507065
    const-class v1, LX/Hoo;

    monitor-enter v1

    .line 2507066
    :try_start_0
    sget-object v0, LX/Hoo;->d:LX/Hoo;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2507067
    if-eqz v2, :cond_0

    .line 2507068
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2507069
    new-instance p0, LX/Hoo;

    invoke-direct {p0}, LX/Hoo;-><init>()V

    .line 2507070
    const-class v3, Landroid/content/Context;

    const-class v4, Lcom/facebook/inject/ForAppContext;

    invoke-interface {v0, v3, v4}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    .line 2507071
    iput-object v3, p0, LX/Hoo;->c:Landroid/content/Context;

    iput-object v4, p0, LX/Hoo;->a:LX/0Uh;

    .line 2507072
    move-object v0, p0

    .line 2507073
    sput-object v0, LX/Hoo;->d:LX/Hoo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2507074
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2507075
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2507076
    :cond_1
    sget-object v0, LX/Hoo;->d:LX/Hoo;

    return-object v0

    .line 2507077
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2507078
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final init()V
    .locals 3

    .prologue
    .line 2507079
    iget-object v0, p0, LX/Hoo;->a:LX/0Uh;

    const/16 v1, 0x44a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    .line 2507080
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 2507081
    iget-object v1, p0, LX/Hoo;->c:Landroid/content/Context;

    .line 2507082
    const-string v2, "fb4a_should_delay_service_gk_enabled"

    invoke-static {v1, v2, v0}, LX/00f;->a(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 2507083
    return-void
.end method
