.class public LX/ICk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2lq;


# instance fields
.field private final a:I

.field private final b:J

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Z

.field public f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FriendingSuggestionUserFieldsModel;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2549744
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2549745
    invoke-virtual {p1}, Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FriendingSuggestionUserFieldsModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, LX/ICk;->b:J

    .line 2549746
    invoke-virtual {p1}, Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FriendingSuggestionUserFieldsModel;->n()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, LX/ICk;->d:Ljava/lang/String;

    .line 2549747
    invoke-virtual {p1}, Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FriendingSuggestionUserFieldsModel;->m()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/ICk;->c:Ljava/lang/String;

    .line 2549748
    invoke-virtual {p1}, Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FriendingSuggestionUserFieldsModel;->l()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v2}, LX/15i;->j(II)I

    move-result v0

    iput v0, p0, LX/ICk;->a:I

    .line 2549749
    invoke-virtual {p1}, Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FriendingSuggestionUserFieldsModel;->j()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    iput-object v0, p0, LX/ICk;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2549750
    iput-boolean v2, p0, LX/ICk;->e:Z

    .line 2549751
    return-void

    .line 2549752
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FriendingSuggestionUserFieldsModel;->n()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 2549743
    iget-wide v0, p0, LX/ICk;->b:J

    return-wide v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2549742
    iget-object v0, p0, LX/ICk;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2549741
    iget-object v0, p0, LX/ICk;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 2549739
    iget v0, p0, LX/ICk;->a:I

    return v0
.end method

.method public final f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .locals 1

    .prologue
    .line 2549740
    iget-object v0, p0, LX/ICk;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    return-object v0
.end method
