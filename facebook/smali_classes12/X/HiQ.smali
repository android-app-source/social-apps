.class public final LX/HiQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "LX/2wX;",
        "LX/0Px",
        "<",
        "Landroid/location/Address;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/google/android/gms/maps/model/LatLngBounds;

.field public final synthetic c:LX/HiS;


# direct methods
.method public constructor <init>(LX/HiS;Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLngBounds;)V
    .locals 0

    .prologue
    .line 2497371
    iput-object p1, p0, LX/HiQ;->c:LX/HiS;

    iput-object p2, p0, LX/HiQ;->a:Ljava/lang/String;

    iput-object p3, p0, LX/HiQ;->b:Lcom/google/android/gms/maps/model/LatLngBounds;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2497372
    check-cast p1, LX/2wX;

    const/4 v2, 0x0

    const/4 v9, 0x0

    .line 2497373
    sget-object v0, LX/JF3;->e:LX/JEt;

    iget-object v1, p0, LX/HiQ;->a:Ljava/lang/String;

    iget-object v3, p0, LX/HiQ;->b:Lcom/google/android/gms/maps/model/LatLngBounds;

    invoke-interface {v0, p1, v1, v3, v9}, LX/JEt;->a(LX/2wX;Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLngBounds;Lcom/google/android/gms/location/places/AutocompleteFilter;)LX/2wg;

    move-result-object v0

    .line 2497374
    const-wide/16 v4, 0xa

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v4, v5, v1}, LX/2wg;->a(JLjava/util/concurrent/TimeUnit;)LX/2NW;

    move-result-object v0

    check-cast v0, LX/JEs;

    .line 2497375
    invoke-virtual {v0}, LX/JEs;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    .line 2497376
    invoke-virtual {v1}, Lcom/google/android/gms/common/api/Status;->e()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2497377
    iget-object v1, p0, LX/HiQ;->c:LX/HiS;

    iget-object v1, v1, LX/HiS;->c:LX/03V;

    const-string v2, "AddressTypeAheadFetcher"

    const-string v3, "Error getting autocomplete prediction API call"

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2497378
    invoke-virtual {v0}, LX/4sY;->nc_()V

    .line 2497379
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2497380
    :goto_0
    return-object v0

    .line 2497381
    :cond_0
    invoke-static {v0}, LX/4sZ;->a(LX/4sX;)Ljava/util/ArrayList;

    move-result-object v3

    .line 2497382
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 2497383
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v1, v2

    :goto_1
    if-ge v1, v5, :cond_1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JEr;

    .line 2497384
    new-instance v6, Landroid/location/Address;

    iget-object v7, p0, LX/HiQ;->c:LX/HiS;

    iget-object v7, v7, LX/HiS;->f:Ljava/util/Locale;

    invoke-direct {v6, v7}, Landroid/location/Address;-><init>(Ljava/util/Locale;)V

    .line 2497385
    invoke-interface {v0, v9}, LX/JEr;->a(Landroid/text/style/CharacterStyle;)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v2, v7}, Landroid/location/Address;->setAddressLine(ILjava/lang/String;)V

    .line 2497386
    invoke-interface {v0, v9}, LX/JEr;->b(Landroid/text/style/CharacterStyle;)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/location/Address;->setLocality(Ljava/lang/String;)V

    .line 2497387
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 2497388
    const-string v8, "google_place_id"

    invoke-interface {v0}, LX/JEr;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v8, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2497389
    invoke-virtual {v6, v7}, Landroid/location/Address;->setExtras(Landroid/os/Bundle;)V

    .line 2497390
    invoke-virtual {v4, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2497391
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2497392
    :cond_1
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method
