.class public final LX/Hw5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Hw7;


# direct methods
.method public constructor <init>(LX/Hw7;)V
    .locals 0

    .prologue
    .line 2520112
    iput-object p1, p0, LX/Hw5;->a:LX/Hw7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x54e8ad2b

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2520113
    iget-object v1, p0, LX/Hw5;->a:LX/Hw7;

    iget-object v1, v1, LX/Hw7;->b:LX/Hw8;

    .line 2520114
    new-instance v3, Landroid/app/DatePickerDialog;

    iget-object v4, v1, LX/Hw8;->b:Landroid/content/Context;

    new-instance v5, LX/Hvz;

    invoke-direct {v5, v1}, LX/Hvz;-><init>(LX/Hw8;)V

    iget-object v6, v1, LX/Hw8;->d:Ljava/util/Calendar;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Ljava/util/Calendar;->get(I)I

    move-result v6

    iget-object v7, v1, LX/Hw8;->d:Ljava/util/Calendar;

    const/4 v8, 0x2

    invoke-virtual {v7, v8}, Ljava/util/Calendar;->get(I)I

    move-result v7

    iget-object v8, v1, LX/Hw8;->d:Ljava/util/Calendar;

    const/4 v9, 0x5

    invoke-virtual {v8, v9}, Ljava/util/Calendar;->get(I)I

    move-result v8

    invoke-direct/range {v3 .. v8}, Landroid/app/DatePickerDialog;-><init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;III)V

    .line 2520115
    iget-object v4, v1, LX/Hw8;->f:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v5

    const-wide/32 v7, 0x927c0

    sub-long/2addr v5, v7

    iget-object v4, v1, LX/Hw8;->d:Ljava/util/Calendar;

    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v7

    invoke-static {v5, v6, v7, v8}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v5

    .line 2520116
    invoke-virtual {v3}, Landroid/app/DatePickerDialog;->getDatePicker()Landroid/widget/DatePicker;

    move-result-object v4

    invoke-virtual {v4, v5, v6}, Landroid/widget/DatePicker;->setMinDate(J)V

    .line 2520117
    invoke-virtual {v3}, Landroid/app/DatePickerDialog;->show()V

    .line 2520118
    const v1, 0x34ab29ef

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
