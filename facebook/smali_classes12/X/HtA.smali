.class public final LX/HtA;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/HtB;


# direct methods
.method public constructor <init>(LX/HtB;)V
    .locals 0

    .prologue
    .line 2515451
    iput-object p1, p0, LX/HtA;->a:LX/HtB;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 3

    .prologue
    .line 2515452
    iget-object v0, p0, LX/HtA;->a:LX/HtB;

    .line 2515453
    iget-object v1, v0, LX/HtB;->h:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0il;

    .line 2515454
    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v2, LX/0j5;

    invoke-interface {v2}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v2

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    .line 2515455
    iget-object p0, v2, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->reshareContext:Lcom/facebook/ipc/composer/model/ComposerReshareContext;

    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2515456
    iget-object p0, v2, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->reshareContext:Lcom/facebook/ipc/composer/model/ComposerReshareContext;

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerReshareContext;->shouldIncludeReshareContext()Z

    move-result p0

    if-ne p0, p1, :cond_0

    .line 2515457
    :goto_0
    return-void

    .line 2515458
    :cond_0
    check-cast v1, LX/0im;

    invoke-interface {v1}, LX/0im;->c()LX/0jJ;

    move-result-object v1

    sget-object p0, LX/HtB;->a:LX/0jK;

    invoke-virtual {v1, p0}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v1

    check-cast v1, LX/0jL;

    new-instance p0, LX/89G;

    invoke-direct {p0, v2}, LX/89G;-><init>(Lcom/facebook/ipc/composer/intent/ComposerShareParams;)V

    iget-object v2, v2, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->reshareContext:Lcom/facebook/ipc/composer/model/ComposerReshareContext;

    invoke-static {v2}, Lcom/facebook/ipc/composer/model/ComposerReshareContext;->a(Lcom/facebook/ipc/composer/model/ComposerReshareContext;)Lcom/facebook/ipc/composer/model/ComposerReshareContext$Builder;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/facebook/ipc/composer/model/ComposerReshareContext$Builder;->setShouldIncludeReshareContext(Z)Lcom/facebook/ipc/composer/model/ComposerReshareContext$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/model/ComposerReshareContext$Builder;->a()Lcom/facebook/ipc/composer/model/ComposerReshareContext;

    move-result-object v2

    .line 2515459
    iput-object v2, p0, LX/89G;->f:Lcom/facebook/ipc/composer/model/ComposerReshareContext;

    .line 2515460
    move-object v2, p0

    .line 2515461
    invoke-virtual {v2}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0jL;->a(Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jL;

    invoke-virtual {v1}, LX/0jL;->a()V

    goto :goto_0
.end method
