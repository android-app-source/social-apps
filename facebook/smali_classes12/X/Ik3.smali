.class public LX/Ik3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2Jw;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2606346
    const-class v0, LX/Ik3;

    sput-object v0, LX/Ik3;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2606347
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2606348
    iput-object p1, p0, LX/Ik3;->b:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    .line 2606349
    return-void
.end method


# virtual methods
.method public final a(LX/2Jy;)Z
    .locals 3

    .prologue
    .line 2606350
    invoke-virtual {p1}, LX/2Jy;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2606351
    const/4 v0, 0x0

    .line 2606352
    :goto_0
    return v0

    .line 2606353
    :cond_0
    iget-object v0, p0, LX/Ik3;->b:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    sget-object v1, LX/J1G;->INCOMING:LX/J1G;

    .line 2606354
    new-instance v2, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsParams;

    invoke-direct {v2, v1}, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsParams;-><init>(LX/J1G;)V

    .line 2606355
    new-instance p0, Landroid/os/Bundle;

    invoke-direct {p0}, Landroid/os/Bundle;-><init>()V

    .line 2606356
    sget-object p1, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsParams;->a:Ljava/lang/String;

    invoke-virtual {p0, p1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2606357
    const-string v2, "fetch_payment_requests"

    invoke-static {v0, p0, v2}, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->a(Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 2606358
    new-instance p0, LX/J04;

    invoke-direct {p0, v0}, LX/J04;-><init>(Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object p1

    invoke-static {v2, p0, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2606359
    const/4 v0, 0x1

    goto :goto_0
.end method
