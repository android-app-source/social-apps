.class public LX/JNi;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/1nA;

.field private final b:LX/0Zb;


# direct methods
.method public constructor <init>(LX/1nA;LX/0Zb;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2686357
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2686358
    iput-object p1, p0, LX/JNi;->a:LX/1nA;

    .line 2686359
    iput-object p2, p0, LX/JNi;->b:LX/0Zb;

    .line 2686360
    return-void
.end method

.method public static a(LX/0QB;)LX/JNi;
    .locals 5

    .prologue
    .line 2686346
    const-class v1, LX/JNi;

    monitor-enter v1

    .line 2686347
    :try_start_0
    sget-object v0, LX/JNi;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2686348
    sput-object v2, LX/JNi;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2686349
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2686350
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2686351
    new-instance p0, LX/JNi;

    invoke-static {v0}, LX/1nA;->a(LX/0QB;)LX/1nA;

    move-result-object v3

    check-cast v3, LX/1nA;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-direct {p0, v3, v4}, LX/JNi;-><init>(LX/1nA;LX/0Zb;)V

    .line 2686352
    move-object v0, p0

    .line 2686353
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2686354
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JNi;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2686355
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2686356
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2686335
    invoke-static {p2, p1}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v0

    .line 2686336
    invoke-static {v0}, LX/17Q;->F(LX/0lF;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2686337
    const/4 v1, 0x0

    .line 2686338
    :goto_0
    move-object v0, v1

    .line 2686339
    iget-object v1, p0, LX/JNi;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2686340
    iget-object v0, p0, LX/JNi;->a:LX/1nA;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    invoke-static {v1}, LX/3Bd;->a(Lcom/facebook/graphql/model/GraphQLProfile;)LX/1y5;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, p3, v1, v2}, LX/1nA;->a(Landroid/view/View;LX/1y5;Landroid/os/Bundle;)V

    .line 2686341
    return-void

    .line 2686342
    :cond_0
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "connect_with_facebook_profile"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "tracking"

    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "native_newsfeed"

    .line 2686343
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2686344
    move-object v1, v1

    .line 2686345
    goto :goto_0
.end method
