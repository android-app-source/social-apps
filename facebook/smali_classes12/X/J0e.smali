.class public LX/J0e;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListParams;",
        "Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListResult;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/J0e;


# instance fields
.field private final b:LX/J0h;


# direct methods
.method public constructor <init>(LX/J0h;LX/0sO;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2637425
    invoke-direct {p0, p2}, LX/0ro;-><init>(LX/0sO;)V

    .line 2637426
    iput-object p1, p0, LX/J0e;->b:LX/J0h;

    .line 2637427
    return-void
.end method

.method public static a(LX/0QB;)LX/J0e;
    .locals 5

    .prologue
    .line 2637428
    sget-object v0, LX/J0e;->c:LX/J0e;

    if-nez v0, :cond_1

    .line 2637429
    const-class v1, LX/J0e;

    monitor-enter v1

    .line 2637430
    :try_start_0
    sget-object v0, LX/J0e;->c:LX/J0e;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2637431
    if-eqz v2, :cond_0

    .line 2637432
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2637433
    new-instance p0, LX/J0e;

    invoke-static {v0}, LX/J0h;->b(LX/0QB;)LX/J0h;

    move-result-object v3

    check-cast v3, LX/J0h;

    invoke-static {v0}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v4

    check-cast v4, LX/0sO;

    invoke-direct {p0, v3, v4}, LX/J0e;-><init>(LX/J0h;LX/0sO;)V

    .line 2637434
    move-object v0, p0

    .line 2637435
    sput-object v0, LX/J0e;->c:LX/J0e;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2637436
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2637437
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2637438
    :cond_1
    sget-object v0, LX/J0e;->c:LX/J0e;

    return-object v0

    .line 2637439
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2637440
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 2637441
    check-cast p1, Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListParams;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2637442
    const/4 v3, 0x0

    .line 2637443
    sget-object v0, LX/J0d;->a:[I

    .line 2637444
    iget-object v4, p1, Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListParams;->c:LX/DtK;

    move-object v4, v4

    .line 2637445
    invoke-virtual {v4}, LX/DtK;->ordinal()I

    move-result v4

    aget v0, v0, v4

    packed-switch v0, :pswitch_data_0

    .line 2637446
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown queryType seen "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2637447
    iget-object v2, p1, Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListParams;->c:LX/DtK;

    move-object v2, v2

    .line 2637448
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2637449
    :pswitch_0
    const-class v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchAllTransactionListQueryModel;

    invoke-virtual {p3, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchAllTransactionListQueryModel;

    .line 2637450
    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchAllTransactionListQueryModel;->a()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchAllTransactionListQueryModel$AllMessengerPaymentsModel;

    move-result-object v4

    .line 2637451
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchAllTransactionListQueryModel$AllMessengerPaymentsModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_9

    .line 2637452
    invoke-virtual {v4}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchAllTransactionListQueryModel$AllMessengerPaymentsModel;->a()LX/0Px;

    move-result-object v3

    .line 2637453
    invoke-virtual {v4}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchAllTransactionListQueryModel$AllMessengerPaymentsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v4, v0, v2}, LX/15i;->h(II)Z

    move-result v0

    move v4, v0

    move-object v5, v3

    .line 2637454
    :goto_1
    if-nez v5, :cond_6

    .line 2637455
    new-instance v0, Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListResult;

    .line 2637456
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2637457
    invoke-direct {v0, v2, v1}, Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListResult;-><init>(LX/0Px;Z)V

    .line 2637458
    :goto_2
    return-object v0

    :cond_0
    move v0, v2

    .line 2637459
    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_0

    .line 2637460
    :pswitch_1
    const-class v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingTransactionListQueryModel;

    invoke-virtual {p3, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingTransactionListQueryModel;

    .line 2637461
    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingTransactionListQueryModel;->a()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingTransactionListQueryModel$IncomingMessengerPaymentsModel;

    move-result-object v4

    .line 2637462
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingTransactionListQueryModel$IncomingMessengerPaymentsModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    move v0, v1

    :goto_3
    if-eqz v0, :cond_9

    .line 2637463
    invoke-virtual {v4}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingTransactionListQueryModel$IncomingMessengerPaymentsModel;->a()LX/0Px;

    move-result-object v3

    .line 2637464
    invoke-virtual {v4}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchIncomingTransactionListQueryModel$IncomingMessengerPaymentsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v4, v0, v2}, LX/15i;->h(II)Z

    move-result v0

    move v4, v0

    move-object v5, v3

    .line 2637465
    goto :goto_1

    :cond_2
    move v0, v2

    .line 2637466
    goto :goto_3

    :cond_3
    move v0, v2

    goto :goto_3

    .line 2637467
    :pswitch_2
    const-class v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchOutgoingTransactionListQueryModel;

    invoke-virtual {p3, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchOutgoingTransactionListQueryModel;

    .line 2637468
    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchOutgoingTransactionListQueryModel;->a()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchOutgoingTransactionListQueryModel$OutgoingMessengerPaymentsModel;

    move-result-object v4

    .line 2637469
    if-eqz v4, :cond_5

    invoke-virtual {v4}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchOutgoingTransactionListQueryModel$OutgoingMessengerPaymentsModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    if-eqz v0, :cond_9

    .line 2637470
    invoke-virtual {v4}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchOutgoingTransactionListQueryModel$OutgoingMessengerPaymentsModel;->a()LX/0Px;

    move-result-object v3

    .line 2637471
    invoke-virtual {v4}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$FetchOutgoingTransactionListQueryModel$OutgoingMessengerPaymentsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v4, v0, v2}, LX/15i;->h(II)Z

    move-result v0

    move v4, v0

    move-object v5, v3

    .line 2637472
    goto :goto_1

    :cond_4
    move v0, v2

    .line 2637473
    goto :goto_4

    :cond_5
    move v0, v2

    goto :goto_4

    .line 2637474
    :cond_6
    new-instance v6, LX/0Pz;

    invoke-direct {v6}, LX/0Pz;-><init>()V

    .line 2637475
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v7

    move v3, v2

    :goto_5
    if-ge v3, v7, :cond_7

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;

    .line 2637476
    iget-object v8, p0, LX/J0e;->b:LX/J0h;

    invoke-virtual {v8, v0}, LX/J0h;->a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentTransactionModel;)Lcom/facebook/payments/p2p/model/PaymentTransaction;

    move-result-object v0

    invoke-virtual {v6, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2637477
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_5

    .line 2637478
    :cond_7
    new-instance v0, Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListResult;

    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    if-nez v4, :cond_8

    :goto_6
    invoke-direct {v0, v3, v1}, Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListResult;-><init>(LX/0Px;Z)V

    goto/16 :goto_2

    :cond_8
    move v1, v2

    goto :goto_6

    :cond_9
    move v4, v2

    move-object v5, v3

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 2637479
    const/4 v0, 0x1

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 3

    .prologue
    .line 2637480
    check-cast p1, Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListParams;

    .line 2637481
    sget-object v0, LX/J0d;->a:[I

    .line 2637482
    iget-object v1, p1, Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListParams;->c:LX/DtK;

    move-object v1, v1

    .line 2637483
    invoke-virtual {v1}, LX/DtK;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2637484
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown queryType seen "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2637485
    iget-object v2, p1, Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListParams;->c:LX/DtK;

    move-object v2, v2

    .line 2637486
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2637487
    :pswitch_0
    new-instance v0, LX/DtV;

    invoke-direct {v0}, LX/DtV;-><init>()V

    move-object v0, v0

    .line 2637488
    :goto_0
    const-string v1, "max_transactions"

    .line 2637489
    iget v2, p1, Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListParams;->d:I

    move v2, v2

    .line 2637490
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2637491
    return-object v0

    .line 2637492
    :pswitch_1
    new-instance v0, LX/DtY;

    invoke-direct {v0}, LX/DtY;-><init>()V

    move-object v0, v0

    .line 2637493
    goto :goto_0

    .line 2637494
    :pswitch_2
    new-instance v0, LX/Dtb;

    invoke-direct {v0}, LX/Dtb;-><init>()V

    move-object v0, v0

    .line 2637495
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
