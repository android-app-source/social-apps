.class public final LX/HVA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

.field public final synthetic b:Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;)V
    .locals 0

    .prologue
    .line 2474756
    iput-object p1, p0, LX/HVA;->b:Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;

    iput-object p2, p0, LX/HVA;->a:Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, -0x46d76e56

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2474757
    iget-object v0, p0, LX/HVA;->b:Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;

    iget-object v2, v0, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->c:LX/D3w;

    iget-object v3, p0, LX/HVA;->a:Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

    iget-object v0, p0, LX/HVA;->b:Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->getContext()Landroid/content/Context;

    move-result-object v4

    sget-object v5, LX/04D;->PAGE_TIMELINE:LX/04D;

    iget-object v0, p0, LX/HVA;->b:Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2474758
    invoke-static {v3}, LX/5hK;->a(Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v8

    const/4 p0, 0x0

    move-object v7, v2

    move-object v9, v4

    move-object v10, v5

    move-object p1, v0

    .line 2474759
    invoke-static {v9, v10}, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->a(Landroid/content/Context;LX/04D;)Landroid/content/Intent;

    move-result-object v2

    .line 2474760
    const-string v3, "video_graphql_object"

    invoke-static {v2, v3, v8}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2474761
    const-string v3, "video_player_allow_looping"

    invoke-virtual {v2, v3, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2474762
    const-string v3, "com.facebook.orca.auth.OVERRIDDEN_VIEWER_CONTEXT"

    invoke-virtual {v2, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2474763
    invoke-static {v7, v9, v2}, LX/D3w;->a(LX/D3w;Landroid/content/Context;Landroid/content/Intent;)V

    .line 2474764
    const v0, -0x7df4a28d

    invoke-static {v6, v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
