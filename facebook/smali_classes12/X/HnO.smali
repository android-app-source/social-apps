.class public LX/HnO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/45M;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Or;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "LX/0Zb;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/45M;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2501788
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2501789
    iput-object p1, p0, LX/HnO;->a:Landroid/content/Context;

    .line 2501790
    iput-object p2, p0, LX/HnO;->b:LX/0Or;

    .line 2501791
    iput-object p3, p0, LX/HnO;->c:LX/0Ot;

    .line 2501792
    return-void
.end method


# virtual methods
.method public final init()V
    .locals 3

    .prologue
    .line 2501793
    iget-object v0, p0, LX/HnO;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/45M;

    new-instance v1, Lcom/facebook/bluetoothsupportreporter/BluetoothSupportReporter$1;

    invoke-direct {v1, p0}, Lcom/facebook/bluetoothsupportreporter/BluetoothSupportReporter$1;-><init>(LX/HnO;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/1SG;->addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 2501794
    return-void
.end method
