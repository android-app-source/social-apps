.class public final LX/IdL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;

.field public final synthetic b:LX/IdM;


# direct methods
.method public constructor <init>(LX/IdM;Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;)V
    .locals 0

    .prologue
    .line 2596726
    iput-object p1, p0, LX/IdL;->b:LX/IdM;

    iput-object p2, p0, LX/IdL;->a:Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x50519592

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2596727
    iget-object v0, p0, LX/IdL;->b:LX/IdM;

    iget-object v0, v0, LX/IdM;->a:Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;

    iget-object v0, v0, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->b:Landroid/widget/LinearLayout;

    iget-object v2, p0, LX/IdL;->b:LX/IdM;

    iget-object v2, v2, LX/IdM;->a:Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;

    iget v2, v2, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->e:I

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/IdM;

    invoke-virtual {v0}, LX/IdM;->c()V

    .line 2596728
    iget-object v0, p0, LX/IdL;->b:LX/IdM;

    invoke-virtual {v0}, LX/IdM;->b()V

    .line 2596729
    iget-object v0, p0, LX/IdL;->b:LX/IdM;

    iget-object v0, v0, LX/IdM;->a:Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;

    iget-object v2, p0, LX/IdL;->b:LX/IdM;

    iget-object v2, v2, LX/IdM;->c:Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;

    .line 2596730
    iput-object v2, v0, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->f:Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;

    .line 2596731
    iget-object v0, p0, LX/IdL;->b:LX/IdM;

    iget-object v0, v0, LX/IdM;->a:Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;

    iget-object v2, p0, LX/IdL;->b:LX/IdM;

    iget v2, v2, LX/IdM;->b:I

    .line 2596732
    iput v2, v0, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->e:I

    .line 2596733
    iget-object v0, p0, LX/IdL;->b:LX/IdM;

    iget-object v0, v0, LX/IdM;->a:Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;

    iget-object v0, v0, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->g:LX/IdD;

    if-eqz v0, :cond_0

    .line 2596734
    iget-object v0, p0, LX/IdL;->b:LX/IdM;

    iget-object v0, v0, LX/IdM;->a:Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;

    iget-object v0, v0, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->g:LX/IdD;

    iget-object v2, p0, LX/IdL;->b:LX/IdM;

    iget-object v2, v2, LX/IdM;->c:Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;

    invoke-interface {v0, v2}, LX/IdD;->onClick(Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;)V

    .line 2596735
    :cond_0
    const v0, 0x152d765a

    invoke-static {v3, v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
