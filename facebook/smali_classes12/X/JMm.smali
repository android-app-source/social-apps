.class public final LX/JMm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/feedplugins/aymt/protocol/ActionYouMayTakeMutationModels$PageSlideshowPostMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0bH;

.field public final synthetic b:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic c:LX/0kL;

.field public final synthetic d:Landroid/view/View;

.field public final synthetic e:LX/JMn;


# direct methods
.method public constructor <init>(LX/JMn;LX/0bH;Lcom/facebook/feed/rows/core/props/FeedProps;LX/0kL;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2684707
    iput-object p1, p0, LX/JMm;->e:LX/JMn;

    iput-object p2, p0, LX/JMm;->a:LX/0bH;

    iput-object p3, p0, LX/JMm;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p4, p0, LX/JMm;->c:LX/0kL;

    iput-object p5, p0, LX/JMm;->d:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2684717
    iget-object v0, p0, LX/JMm;->c:LX/0kL;

    new-instance v1, LX/27k;

    iget-object v2, p0, LX/JMm;->d:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f083a79

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2684718
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 7
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2684708
    const/4 v2, 0x0

    .line 2684709
    iget-object v6, p0, LX/JMm;->a:LX/0bH;

    new-instance v0, LX/1Nd;

    iget-object v1, p0, LX/JMm;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2684710
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v3

    .line 2684711
    check-cast v1, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->g()Ljava/lang/String;

    move-result-object v1

    sget-object v4, Lcom/facebook/graphql/enums/StoryVisibility;->HIDDEN:Lcom/facebook/graphql/enums/StoryVisibility;

    iget-object v3, p0, LX/JMm;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2684712
    iget-object v5, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v5

    .line 2684713
    check-cast v3, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLAYMTPageSlideshowFeedUnit;->H_()I

    move-result v5

    move-object v3, v2

    invoke-direct/range {v0 .. v5}, LX/1Nd;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/StoryVisibility;I)V

    invoke-virtual {v6, v0}, LX/0b4;->a(LX/0b7;)V

    .line 2684714
    iget-object v0, p0, LX/JMm;->a:LX/0bH;

    new-instance v1, LX/1YM;

    invoke-direct {v1}, LX/1YM;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2684715
    iget-object v0, p0, LX/JMm;->c:LX/0kL;

    new-instance v1, LX/27k;

    iget-object v2, p0, LX/JMm;->d:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f083a78

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2684716
    return-void
.end method
