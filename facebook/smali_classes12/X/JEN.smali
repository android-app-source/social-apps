.class public LX/JEN;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2665936
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;)I
    .locals 1

    .prologue
    .line 2665937
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;->MINUTES_VIEWED:Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    invoke-virtual {v0, p0}, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2665938
    const v0, 0x7f0207fd

    .line 2665939
    :goto_0
    return v0

    .line 2665940
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;->TOTAL_ENGAGEMENT:Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    invoke-virtual {v0, p0}, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2665941
    const v0, 0x7f0208fa

    goto :goto_0

    .line 2665942
    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;->TOTAL_FOLLOWERS:Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    invoke-virtual {v0, p0}, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2665943
    const v0, 0x7f020881

    goto :goto_0

    .line 2665944
    :cond_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;->VIDEO_VIEWS:Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    invoke-virtual {v0, p0}, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2665945
    const v0, 0x7f020874

    goto :goto_0

    .line 2665946
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;->BROADCAST_TIME:Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    invoke-virtual {v0, p0}, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2665947
    const v0, 0x7f02090a

    goto :goto_0

    .line 2665948
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;->PEAK_LIVE_VIEWERS:Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;

    invoke-virtual {v0, p0}, Lcom/facebook/graphql/enums/GraphQLVideoInsightsSummaryContext;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2665949
    const v0, 0x7f020862

    goto :goto_0

    .line 2665950
    :cond_5
    const v0, 0x7f02090e

    goto :goto_0
.end method
