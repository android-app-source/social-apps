.class public final LX/J6P;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;

.field private b:Z


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;Z)V
    .locals 0

    .prologue
    .line 2649618
    iput-object p1, p0, LX/J6P;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2649619
    iput-boolean p2, p0, LX/J6P;->b:Z

    .line 2649620
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2649621
    iget-object v0, p0, LX/J6P;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->h:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;

    const v1, -0x49cf23d4

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2649622
    iget-boolean v0, p0, LX/J6P;->b:Z

    if-eqz v0, :cond_0

    .line 2649623
    sget-object v0, LX/J6O;->a:[I

    iget-object v1, p0, LX/J6P;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;

    iget-object v1, v1, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->e:LX/J4K;

    invoke-virtual {v1}, LX/J4K;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2649624
    :cond_0
    :goto_0
    return-void

    .line 2649625
    :pswitch_0
    iget-object v0, p0, LX/J6P;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->d:LX/J3m;

    .line 2649626
    iget-object v1, v0, LX/J3m;->d:LX/11i;

    sget-object v2, LX/J3m;->a:LX/J3k;

    invoke-interface {v1, v2}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v1

    .line 2649627
    if-eqz v1, :cond_1

    .line 2649628
    const-string v2, "PrivacyCheckupComposerDataFetch"

    const p0, 0x3ef5939d

    invoke-static {v1, v2, p0}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 2649629
    :cond_1
    iget-object v1, v0, LX/J3m;->d:LX/11i;

    sget-object v2, LX/J3m;->a:LX/J3k;

    invoke-interface {v1, v2}, LX/11i;->b(LX/0Pq;)V

    .line 2649630
    goto :goto_0

    .line 2649631
    :pswitch_1
    iget-object v0, p0, LX/J6P;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->d:LX/J3m;

    .line 2649632
    iget-object v1, v0, LX/J3m;->d:LX/11i;

    sget-object v2, LX/J3m;->b:LX/J3l;

    invoke-interface {v1, v2}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v1

    .line 2649633
    if-eqz v1, :cond_2

    .line 2649634
    const-string v2, "PrivacyCheckupProfileDataFetch"

    const p0, 0x103c32e

    invoke-static {v1, v2, p0}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 2649635
    :cond_2
    iget-object v1, v0, LX/J3m;->d:LX/11i;

    sget-object v2, LX/J3m;->b:LX/J3l;

    invoke-interface {v1, v2}, LX/11i;->b(LX/0Pq;)V

    .line 2649636
    goto :goto_0

    .line 2649637
    :pswitch_2
    iget-object v0, p0, LX/J6P;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->d:LX/J3m;

    .line 2649638
    iget-object v1, v0, LX/J3m;->d:LX/11i;

    sget-object v2, LX/J3m;->c:LX/J3j;

    invoke-interface {v1, v2}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v1

    .line 2649639
    if-eqz v1, :cond_3

    .line 2649640
    const-string v2, "PrivacyCheckupAppsDataFetch"

    const p0, -0x80c3dcd

    invoke-static {v1, v2, p0}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 2649641
    :cond_3
    iget-object v1, v0, LX/J3m;->d:LX/11i;

    sget-object v2, LX/J3m;->c:LX/J3j;

    invoke-interface {v1, v2}, LX/11i;->b(LX/0Pq;)V

    .line 2649642
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
