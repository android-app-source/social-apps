.class public LX/HaA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0dC;


# direct methods
.method public constructor <init>(LX/0dC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2483438
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2483439
    iput-object p1, p0, LX/HaA;->a:LX/0dC;

    .line 2483440
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 2483441
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2483442
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "d"

    iget-object v3, p0, LX/HaA;->a:LX/0dC;

    invoke-virtual {v3}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2483443
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    sget-object v2, LX/11I;->HEADER_PREFILL_KICKOFF:LX/11I;

    iget-object v2, v2, LX/11I;->requestNameString:Ljava/lang/String;

    .line 2483444
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 2483445
    move-object v1, v1

    .line 2483446
    const-string v2, "GET"

    .line 2483447
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 2483448
    move-object v1, v1

    .line 2483449
    const-string v2, "hr/dc"

    .line 2483450
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 2483451
    move-object v1, v1

    .line 2483452
    sget-object v2, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v1, v2}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v1

    .line 2483453
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 2483454
    move-object v0, v1

    .line 2483455
    const/4 v1, 0x1

    .line 2483456
    iput-boolean v1, v0, LX/14O;->q:Z

    .line 2483457
    move-object v0, v0

    .line 2483458
    sget-object v1, LX/14S;->STRING:LX/14S;

    .line 2483459
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2483460
    move-object v0, v0

    .line 2483461
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2483462
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2483463
    const/4 v0, 0x0

    return-object v0
.end method
