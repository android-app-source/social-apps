.class public final enum LX/HZu;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HZu;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/HZu;

.field public static final enum ACCEPT_TERMS:LX/HZu;

.field public static final enum ACCOUNT_CREATION_ATTEMPT:LX/HZu;

.field public static final enum ACCOUNT_CREATION_ERROR:LX/HZu;

.field public static final enum ACCOUNT_CREATION_NEEDS_COMPLETION:LX/HZu;

.field public static final enum ACCOUNT_CREATION_SUCCESS:LX/HZu;

.field public static final enum ENTER_FLOW:LX/HZu;

.field public static final enum EXIT_FLOW:LX/HZu;

.field public static final enum EXPOSURE:LX/HZu;

.field public static final enum FB4A_REGISTRATION_UPSELL_CAMPAIGN:LX/HZu;

.field public static final enum FINISH_REGISTRATION_NOTIF_CLICKED:LX/HZu;

.field public static final enum FINISH_REGISTRATION_NOTIF_CREATED:LX/HZu;

.field public static final enum FINISH_REGISTRATION_NOTIF_SCHEDULED:LX/HZu;

.field public static final enum FLOW_STATE:LX/HZu;

.field public static final enum NAME_PREFILL:LX/HZu;

.field public static final enum PREFILL:LX/HZu;

.field public static final enum REGISTRATION_ADDITIONAL_EMAIL_STATE:LX/HZu;

.field public static final enum REGISTRATION_CONTACTS_TERMS_ACCEPT:LX/HZu;

.field public static final enum REGISTRATION_CP_SUGGESTION_CALL_ATTEMPT:LX/HZu;

.field public static final enum REGISTRATION_CP_SUGGESTION_CALL_ERROR:LX/HZu;

.field public static final enum REGISTRATION_CP_SUGGESTION_CALL_SUCCESS:LX/HZu;

.field public static final enum REGISTRATION_CP_SUGGESTION_READY:LX/HZu;

.field public static final enum REGISTRATION_EXISTING_ACCOUNT_STEP_WATERFALL:LX/HZu;

.field public static final enum REGISTRATION_HEADER_PREFILL_KICKOFF_STATE:LX/HZu;

.field public static final enum REGISTRATION_LOGIN_FAILURE:LX/HZu;

.field public static final enum REGISTRATION_LOGIN_START:LX/HZu;

.field public static final enum REGISTRATION_LOGIN_SUCCESS:LX/HZu;

.field public static final enum REJECT_TERMS:LX/HZu;

.field public static final enum STEP_BACK:LX/HZu;

.field public static final enum STEP_ERROR:LX/HZu;

.field public static final enum STEP_SUBMIT:LX/HZu;

.field public static final enum STEP_VALIDATION_ERROR:LX/HZu;

.field public static final enum STEP_VALIDATION_SUCCESS:LX/HZu;

.field public static final enum STEP_VIEW:LX/HZu;

.field public static final enum SWITCH_CONTACTPOINT_TYPE:LX/HZu;


# instance fields
.field private final mAnalyticsName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2482859
    new-instance v0, LX/HZu;

    const-string v1, "ENTER_FLOW"

    const-string v2, "registration_flow_enter"

    invoke-direct {v0, v1, v4, v2}, LX/HZu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HZu;->ENTER_FLOW:LX/HZu;

    .line 2482860
    new-instance v0, LX/HZu;

    const-string v1, "FLOW_STATE"

    const-string v2, "registration_flow_state"

    invoke-direct {v0, v1, v5, v2}, LX/HZu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HZu;->FLOW_STATE:LX/HZu;

    .line 2482861
    new-instance v0, LX/HZu;

    const-string v1, "EXIT_FLOW"

    const-string v2, "registration_flow_exit"

    invoke-direct {v0, v1, v6, v2}, LX/HZu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HZu;->EXIT_FLOW:LX/HZu;

    .line 2482862
    new-instance v0, LX/HZu;

    const-string v1, "ACCEPT_TERMS"

    const-string v2, "registration_step_accept_terms"

    invoke-direct {v0, v1, v7, v2}, LX/HZu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HZu;->ACCEPT_TERMS:LX/HZu;

    .line 2482863
    new-instance v0, LX/HZu;

    const-string v1, "REJECT_TERMS"

    const-string v2, "registration_step_reject_terms"

    invoke-direct {v0, v1, v8, v2}, LX/HZu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HZu;->REJECT_TERMS:LX/HZu;

    .line 2482864
    new-instance v0, LX/HZu;

    const-string v1, "SWITCH_CONTACTPOINT_TYPE"

    const/4 v2, 0x5

    const-string v3, "registration_contact_switch_type"

    invoke-direct {v0, v1, v2, v3}, LX/HZu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HZu;->SWITCH_CONTACTPOINT_TYPE:LX/HZu;

    .line 2482865
    new-instance v0, LX/HZu;

    const-string v1, "STEP_VIEW"

    const/4 v2, 0x6

    const-string v3, "registration_step_view"

    invoke-direct {v0, v1, v2, v3}, LX/HZu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HZu;->STEP_VIEW:LX/HZu;

    .line 2482866
    new-instance v0, LX/HZu;

    const-string v1, "STEP_SUBMIT"

    const/4 v2, 0x7

    const-string v3, "registration_step_submit"

    invoke-direct {v0, v1, v2, v3}, LX/HZu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HZu;->STEP_SUBMIT:LX/HZu;

    .line 2482867
    new-instance v0, LX/HZu;

    const-string v1, "STEP_BACK"

    const/16 v2, 0x8

    const-string v3, "registration_step_back"

    invoke-direct {v0, v1, v2, v3}, LX/HZu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HZu;->STEP_BACK:LX/HZu;

    .line 2482868
    new-instance v0, LX/HZu;

    const-string v1, "STEP_ERROR"

    const/16 v2, 0x9

    const-string v3, "registration_step_error"

    invoke-direct {v0, v1, v2, v3}, LX/HZu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HZu;->STEP_ERROR:LX/HZu;

    .line 2482869
    new-instance v0, LX/HZu;

    const-string v1, "STEP_VALIDATION_ERROR"

    const/16 v2, 0xa

    const-string v3, "registration_step_validation_error"

    invoke-direct {v0, v1, v2, v3}, LX/HZu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HZu;->STEP_VALIDATION_ERROR:LX/HZu;

    .line 2482870
    new-instance v0, LX/HZu;

    const-string v1, "STEP_VALIDATION_SUCCESS"

    const/16 v2, 0xb

    const-string v3, "registration_step_validation_success"

    invoke-direct {v0, v1, v2, v3}, LX/HZu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HZu;->STEP_VALIDATION_SUCCESS:LX/HZu;

    .line 2482871
    new-instance v0, LX/HZu;

    const-string v1, "ACCOUNT_CREATION_ATTEMPT"

    const/16 v2, 0xc

    const-string v3, "registration_account_creation_attempt"

    invoke-direct {v0, v1, v2, v3}, LX/HZu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HZu;->ACCOUNT_CREATION_ATTEMPT:LX/HZu;

    .line 2482872
    new-instance v0, LX/HZu;

    const-string v1, "ACCOUNT_CREATION_ERROR"

    const/16 v2, 0xd

    const-string v3, "registration_account_creation_error"

    invoke-direct {v0, v1, v2, v3}, LX/HZu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HZu;->ACCOUNT_CREATION_ERROR:LX/HZu;

    .line 2482873
    new-instance v0, LX/HZu;

    const-string v1, "ACCOUNT_CREATION_SUCCESS"

    const/16 v2, 0xe

    const-string v3, "registration_account_creation_success"

    invoke-direct {v0, v1, v2, v3}, LX/HZu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HZu;->ACCOUNT_CREATION_SUCCESS:LX/HZu;

    .line 2482874
    new-instance v0, LX/HZu;

    const-string v1, "ACCOUNT_CREATION_NEEDS_COMPLETION"

    const/16 v2, 0xf

    const-string v3, "registration_account_creation_needs_completion"

    invoke-direct {v0, v1, v2, v3}, LX/HZu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HZu;->ACCOUNT_CREATION_NEEDS_COMPLETION:LX/HZu;

    .line 2482875
    new-instance v0, LX/HZu;

    const-string v1, "NAME_PREFILL"

    const/16 v2, 0x10

    const-string v3, "registration_name_prefill"

    invoke-direct {v0, v1, v2, v3}, LX/HZu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HZu;->NAME_PREFILL:LX/HZu;

    .line 2482876
    new-instance v0, LX/HZu;

    const-string v1, "PREFILL"

    const/16 v2, 0x11

    const-string v3, "registration_prefill"

    invoke-direct {v0, v1, v2, v3}, LX/HZu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HZu;->PREFILL:LX/HZu;

    .line 2482877
    new-instance v0, LX/HZu;

    const-string v1, "EXPOSURE"

    const/16 v2, 0x12

    const-string v3, "initial_app_launch_experiment_exposure"

    invoke-direct {v0, v1, v2, v3}, LX/HZu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HZu;->EXPOSURE:LX/HZu;

    .line 2482878
    new-instance v0, LX/HZu;

    const-string v1, "FINISH_REGISTRATION_NOTIF_SCHEDULED"

    const/16 v2, 0x13

    const-string v3, "finish_registration_notif_scheduled"

    invoke-direct {v0, v1, v2, v3}, LX/HZu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HZu;->FINISH_REGISTRATION_NOTIF_SCHEDULED:LX/HZu;

    .line 2482879
    new-instance v0, LX/HZu;

    const-string v1, "FINISH_REGISTRATION_NOTIF_CREATED"

    const/16 v2, 0x14

    const-string v3, "finish_registration_notif_created"

    invoke-direct {v0, v1, v2, v3}, LX/HZu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HZu;->FINISH_REGISTRATION_NOTIF_CREATED:LX/HZu;

    .line 2482880
    new-instance v0, LX/HZu;

    const-string v1, "FINISH_REGISTRATION_NOTIF_CLICKED"

    const/16 v2, 0x15

    const-string v3, "finish_registration_notif_clicked"

    invoke-direct {v0, v1, v2, v3}, LX/HZu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HZu;->FINISH_REGISTRATION_NOTIF_CLICKED:LX/HZu;

    .line 2482881
    new-instance v0, LX/HZu;

    const-string v1, "FB4A_REGISTRATION_UPSELL_CAMPAIGN"

    const/16 v2, 0x16

    const-string v3, "fb4a_registration_upsell_campaign"

    invoke-direct {v0, v1, v2, v3}, LX/HZu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HZu;->FB4A_REGISTRATION_UPSELL_CAMPAIGN:LX/HZu;

    .line 2482882
    new-instance v0, LX/HZu;

    const-string v1, "REGISTRATION_CONTACTS_TERMS_ACCEPT"

    const/16 v2, 0x17

    const-string v3, "registration_contacts_terms_accept"

    invoke-direct {v0, v1, v2, v3}, LX/HZu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HZu;->REGISTRATION_CONTACTS_TERMS_ACCEPT:LX/HZu;

    .line 2482883
    new-instance v0, LX/HZu;

    const-string v1, "REGISTRATION_LOGIN_START"

    const/16 v2, 0x18

    const-string v3, "registration_login_start"

    invoke-direct {v0, v1, v2, v3}, LX/HZu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HZu;->REGISTRATION_LOGIN_START:LX/HZu;

    .line 2482884
    new-instance v0, LX/HZu;

    const-string v1, "REGISTRATION_LOGIN_SUCCESS"

    const/16 v2, 0x19

    const-string v3, "registration_login_success"

    invoke-direct {v0, v1, v2, v3}, LX/HZu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HZu;->REGISTRATION_LOGIN_SUCCESS:LX/HZu;

    .line 2482885
    new-instance v0, LX/HZu;

    const-string v1, "REGISTRATION_LOGIN_FAILURE"

    const/16 v2, 0x1a

    const-string v3, "registration_login_failure"

    invoke-direct {v0, v1, v2, v3}, LX/HZu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HZu;->REGISTRATION_LOGIN_FAILURE:LX/HZu;

    .line 2482886
    new-instance v0, LX/HZu;

    const-string v1, "REGISTRATION_CP_SUGGESTION_CALL_SUCCESS"

    const/16 v2, 0x1b

    const-string v3, "registration_cp_suggestion_call_success"

    invoke-direct {v0, v1, v2, v3}, LX/HZu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HZu;->REGISTRATION_CP_SUGGESTION_CALL_SUCCESS:LX/HZu;

    .line 2482887
    new-instance v0, LX/HZu;

    const-string v1, "REGISTRATION_CP_SUGGESTION_CALL_ERROR"

    const/16 v2, 0x1c

    const-string v3, "registration_cp_suggestion_call_error"

    invoke-direct {v0, v1, v2, v3}, LX/HZu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HZu;->REGISTRATION_CP_SUGGESTION_CALL_ERROR:LX/HZu;

    .line 2482888
    new-instance v0, LX/HZu;

    const-string v1, "REGISTRATION_CP_SUGGESTION_CALL_ATTEMPT"

    const/16 v2, 0x1d

    const-string v3, "registration_cp_suggestion_call_attempt"

    invoke-direct {v0, v1, v2, v3}, LX/HZu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HZu;->REGISTRATION_CP_SUGGESTION_CALL_ATTEMPT:LX/HZu;

    .line 2482889
    new-instance v0, LX/HZu;

    const-string v1, "REGISTRATION_CP_SUGGESTION_READY"

    const/16 v2, 0x1e

    const-string v3, "registration_cp_suggestion_ready"

    invoke-direct {v0, v1, v2, v3}, LX/HZu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HZu;->REGISTRATION_CP_SUGGESTION_READY:LX/HZu;

    .line 2482890
    new-instance v0, LX/HZu;

    const-string v1, "REGISTRATION_ADDITIONAL_EMAIL_STATE"

    const/16 v2, 0x1f

    const-string v3, "registration_additional_email_state"

    invoke-direct {v0, v1, v2, v3}, LX/HZu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HZu;->REGISTRATION_ADDITIONAL_EMAIL_STATE:LX/HZu;

    .line 2482891
    new-instance v0, LX/HZu;

    const-string v1, "REGISTRATION_HEADER_PREFILL_KICKOFF_STATE"

    const/16 v2, 0x20

    const-string v3, "registration_header_prefill_kickoff_state"

    invoke-direct {v0, v1, v2, v3}, LX/HZu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HZu;->REGISTRATION_HEADER_PREFILL_KICKOFF_STATE:LX/HZu;

    .line 2482892
    new-instance v0, LX/HZu;

    const-string v1, "REGISTRATION_EXISTING_ACCOUNT_STEP_WATERFALL"

    const/16 v2, 0x21

    const-string v3, "registration_existing_account_step_waterfall"

    invoke-direct {v0, v1, v2, v3}, LX/HZu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/HZu;->REGISTRATION_EXISTING_ACCOUNT_STEP_WATERFALL:LX/HZu;

    .line 2482893
    const/16 v0, 0x22

    new-array v0, v0, [LX/HZu;

    sget-object v1, LX/HZu;->ENTER_FLOW:LX/HZu;

    aput-object v1, v0, v4

    sget-object v1, LX/HZu;->FLOW_STATE:LX/HZu;

    aput-object v1, v0, v5

    sget-object v1, LX/HZu;->EXIT_FLOW:LX/HZu;

    aput-object v1, v0, v6

    sget-object v1, LX/HZu;->ACCEPT_TERMS:LX/HZu;

    aput-object v1, v0, v7

    sget-object v1, LX/HZu;->REJECT_TERMS:LX/HZu;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/HZu;->SWITCH_CONTACTPOINT_TYPE:LX/HZu;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/HZu;->STEP_VIEW:LX/HZu;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/HZu;->STEP_SUBMIT:LX/HZu;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/HZu;->STEP_BACK:LX/HZu;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/HZu;->STEP_ERROR:LX/HZu;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/HZu;->STEP_VALIDATION_ERROR:LX/HZu;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/HZu;->STEP_VALIDATION_SUCCESS:LX/HZu;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/HZu;->ACCOUNT_CREATION_ATTEMPT:LX/HZu;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/HZu;->ACCOUNT_CREATION_ERROR:LX/HZu;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/HZu;->ACCOUNT_CREATION_SUCCESS:LX/HZu;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/HZu;->ACCOUNT_CREATION_NEEDS_COMPLETION:LX/HZu;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/HZu;->NAME_PREFILL:LX/HZu;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/HZu;->PREFILL:LX/HZu;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/HZu;->EXPOSURE:LX/HZu;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/HZu;->FINISH_REGISTRATION_NOTIF_SCHEDULED:LX/HZu;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/HZu;->FINISH_REGISTRATION_NOTIF_CREATED:LX/HZu;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/HZu;->FINISH_REGISTRATION_NOTIF_CLICKED:LX/HZu;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/HZu;->FB4A_REGISTRATION_UPSELL_CAMPAIGN:LX/HZu;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/HZu;->REGISTRATION_CONTACTS_TERMS_ACCEPT:LX/HZu;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/HZu;->REGISTRATION_LOGIN_START:LX/HZu;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/HZu;->REGISTRATION_LOGIN_SUCCESS:LX/HZu;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LX/HZu;->REGISTRATION_LOGIN_FAILURE:LX/HZu;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, LX/HZu;->REGISTRATION_CP_SUGGESTION_CALL_SUCCESS:LX/HZu;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, LX/HZu;->REGISTRATION_CP_SUGGESTION_CALL_ERROR:LX/HZu;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, LX/HZu;->REGISTRATION_CP_SUGGESTION_CALL_ATTEMPT:LX/HZu;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, LX/HZu;->REGISTRATION_CP_SUGGESTION_READY:LX/HZu;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, LX/HZu;->REGISTRATION_ADDITIONAL_EMAIL_STATE:LX/HZu;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, LX/HZu;->REGISTRATION_HEADER_PREFILL_KICKOFF_STATE:LX/HZu;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, LX/HZu;->REGISTRATION_EXISTING_ACCOUNT_STEP_WATERFALL:LX/HZu;

    aput-object v2, v0, v1

    sput-object v0, LX/HZu;->$VALUES:[LX/HZu;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2482856
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2482857
    iput-object p3, p0, LX/HZu;->mAnalyticsName:Ljava/lang/String;

    .line 2482858
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HZu;
    .locals 1

    .prologue
    .line 2482894
    const-class v0, LX/HZu;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HZu;

    return-object v0
.end method

.method public static values()[LX/HZu;
    .locals 1

    .prologue
    .line 2482855
    sget-object v0, LX/HZu;->$VALUES:[LX/HZu;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HZu;

    return-object v0
.end method


# virtual methods
.method public final getAnalyticsName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2482854
    iget-object v0, p0, LX/HZu;->mAnalyticsName:Ljava/lang/String;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2482853
    iget-object v0, p0, LX/HZu;->mAnalyticsName:Ljava/lang/String;

    return-object v0
.end method
