.class public final LX/IHf;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 2560675
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 2560676
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2560677
    :goto_0
    return v1

    .line 2560678
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2560679
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_5

    .line 2560680
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2560681
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2560682
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 2560683
    const-string v6, "action_type"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2560684
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLContactsSetItemAction;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    goto :goto_1

    .line 2560685
    :cond_2
    const-string v6, "additional_item_description"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2560686
    const/4 v5, 0x0

    .line 2560687
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v6, :cond_a

    .line 2560688
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2560689
    :goto_2
    move v3, v5

    .line 2560690
    goto :goto_1

    .line 2560691
    :cond_3
    const-string v6, "item_description"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2560692
    const/4 v5, 0x0

    .line 2560693
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v6, :cond_e

    .line 2560694
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2560695
    :goto_3
    move v2, v5

    .line 2560696
    goto :goto_1

    .line 2560697
    :cond_4
    const-string v6, "item_explanation"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2560698
    const/4 v5, 0x0

    .line 2560699
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v6, :cond_12

    .line 2560700
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2560701
    :goto_4
    move v0, v5

    .line 2560702
    goto :goto_1

    .line 2560703
    :cond_5
    const/4 v5, 0x4

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2560704
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 2560705
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 2560706
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2560707
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2560708
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_6
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto/16 :goto_1

    .line 2560709
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2560710
    :cond_8
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_9

    .line 2560711
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 2560712
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2560713
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_8

    if-eqz v6, :cond_8

    .line 2560714
    const-string v7, "text"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 2560715
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_5

    .line 2560716
    :cond_9
    const/4 v6, 0x1

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 2560717
    invoke-virtual {p1, v5, v3}, LX/186;->b(II)V

    .line 2560718
    invoke-virtual {p1}, LX/186;->d()I

    move-result v5

    goto/16 :goto_2

    :cond_a
    move v3, v5

    goto :goto_5

    .line 2560719
    :cond_b
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2560720
    :cond_c
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_d

    .line 2560721
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 2560722
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2560723
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_c

    if-eqz v6, :cond_c

    .line 2560724
    const-string v7, "text"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 2560725
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_6

    .line 2560726
    :cond_d
    const/4 v6, 0x1

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 2560727
    invoke-virtual {p1, v5, v2}, LX/186;->b(II)V

    .line 2560728
    invoke-virtual {p1}, LX/186;->d()I

    move-result v5

    goto/16 :goto_3

    :cond_e
    move v2, v5

    goto :goto_6

    .line 2560729
    :cond_f
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2560730
    :cond_10
    :goto_7
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_11

    .line 2560731
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 2560732
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2560733
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_10

    if-eqz v6, :cond_10

    .line 2560734
    const-string v7, "text"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_f

    .line 2560735
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_7

    .line 2560736
    :cond_11
    const/4 v6, 0x1

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 2560737
    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 2560738
    invoke-virtual {p1}, LX/186;->d()I

    move-result v5

    goto/16 :goto_4

    :cond_12
    move v0, v5

    goto :goto_7
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2560739
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2560740
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 2560741
    if-eqz v0, :cond_0

    .line 2560742
    const-string v0, "action_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2560743
    invoke-virtual {p0, p1, v1}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2560744
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2560745
    if-eqz v0, :cond_2

    .line 2560746
    const-string v1, "additional_item_description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2560747
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2560748
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2560749
    if-eqz v1, :cond_1

    .line 2560750
    const-string p3, "text"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2560751
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2560752
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2560753
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2560754
    if-eqz v0, :cond_4

    .line 2560755
    const-string v1, "item_description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2560756
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2560757
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2560758
    if-eqz v1, :cond_3

    .line 2560759
    const-string p3, "text"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2560760
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2560761
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2560762
    :cond_4
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2560763
    if-eqz v0, :cond_6

    .line 2560764
    const-string v1, "item_explanation"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2560765
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2560766
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2560767
    if-eqz v1, :cond_5

    .line 2560768
    const-string p1, "text"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2560769
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2560770
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2560771
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2560772
    return-void
.end method
