.class public final enum LX/HQX;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HQX;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/HQX;

.field public static final enum HAS_DATA:LX/HQX;

.field public static final enum NO_DATA:LX/HQX;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2463682
    new-instance v0, LX/HQX;

    const-string v1, "HAS_DATA"

    invoke-direct {v0, v1, v2}, LX/HQX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HQX;->HAS_DATA:LX/HQX;

    .line 2463683
    new-instance v0, LX/HQX;

    const-string v1, "NO_DATA"

    invoke-direct {v0, v1, v3}, LX/HQX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HQX;->NO_DATA:LX/HQX;

    .line 2463684
    const/4 v0, 0x2

    new-array v0, v0, [LX/HQX;

    sget-object v1, LX/HQX;->HAS_DATA:LX/HQX;

    aput-object v1, v0, v2

    sget-object v1, LX/HQX;->NO_DATA:LX/HQX;

    aput-object v1, v0, v3

    sput-object v0, LX/HQX;->$VALUES:[LX/HQX;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2463679
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HQX;
    .locals 1

    .prologue
    .line 2463681
    const-class v0, LX/HQX;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HQX;

    return-object v0
.end method

.method public static values()[LX/HQX;
    .locals 1

    .prologue
    .line 2463680
    sget-object v0, LX/HQX;->$VALUES:[LX/HQX;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HQX;

    return-object v0
.end method
