.class public LX/JC2;
.super LX/3tK;
.source ""

# interfaces
.implements LX/J9E;


# static fields
.field public static final j:I

.field private static final s:I


# instance fields
.field private final k:LX/9lP;

.field private final l:Landroid/view/LayoutInflater;

.field private final m:LX/J8p;

.field private final n:LX/JDA;

.field private final o:LX/J94;

.field public final p:LX/JCx;

.field private final q:LX/JDL;

.field private final r:[Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

.field public t:LX/JAc;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2661432
    invoke-static {}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->values()[Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    move-result-object v0

    array-length v0, v0

    .line 2661433
    sput v0, LX/JC2;->s:I

    sput v0, LX/JC2;->j:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/9lP;Landroid/view/LayoutInflater;LX/J8p;LX/J94;LX/JDA;LX/JCx;LX/JDL;)V
    .locals 3
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/9lP;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Landroid/view/LayoutInflater;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/J8p;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2661434
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, LX/3tK;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    .line 2661435
    invoke-static {}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->values()[Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    move-result-object v0

    iput-object v0, p0, LX/JC2;->r:[Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    .line 2661436
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9lP;

    iput-object v0, p0, LX/JC2;->k:LX/9lP;

    .line 2661437
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, LX/JC2;->l:Landroid/view/LayoutInflater;

    .line 2661438
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/J8p;

    iput-object v0, p0, LX/JC2;->m:LX/J8p;

    .line 2661439
    iput-object p6, p0, LX/JC2;->n:LX/JDA;

    .line 2661440
    iput-object p5, p0, LX/JC2;->o:LX/J94;

    .line 2661441
    iput-object p7, p0, LX/JC2;->p:LX/JCx;

    .line 2661442
    iput-object p8, p0, LX/JC2;->q:LX/JDL;

    .line 2661443
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    .line 2661415
    :try_start_0
    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    invoke-virtual {p0, v0}, LX/JC2;->getItemViewType(I)I

    move-result v0

    .line 2661416
    sget v1, LX/JC2;->j:I

    if-ne v0, v1, :cond_0

    .line 2661417
    new-instance v0, Lcom/facebook/timeline/aboutpage/views/header/CollectionSectionHeader;

    iget-object v1, p0, LX/3tK;->d:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/facebook/timeline/aboutpage/views/header/CollectionSectionHeader;-><init>(Landroid/content/Context;)V

    .line 2661418
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 2661419
    const v2, 0x7f0b249c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2661420
    const v3, 0x7f0b248d

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3, v2}, Landroid/view/View;->setPadding(IIII)V

    .line 2661421
    :goto_0
    return-object v0

    .line 2661422
    :cond_0
    iget-object v1, p0, LX/JC2;->n:LX/JDA;

    iget-object v2, p0, LX/JC2;->r:[Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    aget-object v0, v2, v0

    iget-object v2, p0, LX/JC2;->l:Landroid/view/LayoutInflater;

    iget-object v3, p0, LX/3tK;->d:Landroid/content/Context;

    invoke-virtual {v1, v0, v2, v3}, LX/JDA;->a(Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;Landroid/view/LayoutInflater;Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    .line 2661423
    iget-object v2, p0, LX/JC2;->l:Landroid/view/LayoutInflater;

    invoke-static {v0, v2}, LX/J94;->a(Landroid/view/View;Landroid/view/LayoutInflater;)Landroid/view/View;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 2661424
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2661425
    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    invoke-virtual {p0, v0}, LX/JC2;->getItemViewType(I)I

    move-result v0

    sget v2, LX/JC2;->j:I

    if-ne v0, v2, :cond_1

    .line 2661426
    const-string v0, "section_header"

    .line 2661427
    :goto_1
    iget-object v2, p0, LX/JC2;->n:LX/JDA;

    iget-object v3, p0, LX/3tK;->d:Landroid/content/Context;

    const-string v4, "CollectionsSectionCursorAdapter.getView"

    invoke-virtual {v2, v1, v3, v0, v4}, LX/JDA;->a(Ljava/lang/Exception;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/widget/TextView;

    move-result-object v0

    goto :goto_0

    .line 2661428
    :cond_1
    check-cast p2, LX/2nf;

    .line 2661429
    invoke-interface {p2}, LX/2nf;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, LX/JBL;

    .line 2661430
    invoke-interface {v0}, LX/JAW;->d()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/JCx;->a(Ljava/lang/Iterable;)Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    move-result-object v0

    .line 2661431
    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(Landroid/view/View;Landroid/database/Cursor;)V
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 2661444
    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    invoke-virtual {p0, v0}, LX/JC2;->getItemViewType(I)I

    move-result v1

    .line 2661445
    check-cast p2, LX/2nf;

    invoke-interface {p2}, LX/2nf;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    .line 2661446
    sget v2, LX/JC2;->j:I

    if-ne v1, v2, :cond_0

    .line 2661447
    check-cast v0, LX/JAc;

    .line 2661448
    check-cast p1, Lcom/facebook/timeline/aboutpage/views/header/CollectionSectionHeader;

    invoke-virtual {p1, v0}, Lcom/facebook/timeline/aboutpage/views/header/CollectionSectionHeader;->a(LX/JAc;)V

    .line 2661449
    :goto_0
    return-void

    :cond_0
    move-object v1, v0

    .line 2661450
    check-cast v1, LX/JBL;

    .line 2661451
    iget-object v0, p0, LX/JC2;->n:LX/JDA;

    .line 2661452
    invoke-interface {v1}, LX/JAW;->d()LX/0Px;

    move-result-object v3

    invoke-static {v3}, LX/JCx;->a(Ljava/lang/Iterable;)Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    move-result-object v2

    .line 2661453
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->LIST:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    invoke-virtual {v3, v2}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2661454
    iget-object v2, p0, LX/JC2;->t:LX/JAc;

    invoke-interface {v2}, LX/JAb;->e()Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    move-result-object v2

    invoke-static {v1, v2}, LX/JDL;->a(LX/JBL;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;)LX/0Px;

    move-result-object v2

    .line 2661455
    :goto_1
    move-object v2, v2

    .line 2661456
    iget-object v4, p0, LX/JC2;->k:LX/9lP;

    iget-object v6, p0, LX/JC2;->t:LX/JAc;

    move-object v3, p1

    move-object v7, v5

    move-object v8, v5

    invoke-virtual/range {v0 .. v8}, LX/JDA;->a(LX/JBL;Ljava/util/List;Landroid/view/View;LX/9lP;LX/J9F;LX/JAc;Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionMediasetModel;Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldsModel;)V

    .line 2661457
    iget-object v0, p0, LX/JC2;->m:LX/J8p;

    iget-object v2, p0, LX/JC2;->k:LX/9lP;

    .line 2661458
    iget-object v3, v2, LX/9lP;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2661459
    iget-object v3, p0, LX/JC2;->k:LX/9lP;

    invoke-static {v3}, LX/J8p;->a(LX/9lP;)LX/9lQ;

    move-result-object v3

    iget-object v4, p0, LX/JC2;->t:LX/JAc;

    invoke-interface {v4}, LX/JAb;->mV_()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1}, LX/JBK;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v3, v4, v1}, LX/J8p;->a(Ljava/lang/String;LX/9lQ;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2661460
    :cond_1
    invoke-interface {v1}, LX/JBL;->k()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;->d()LX/0Px;

    move-result-object v2

    goto :goto_1
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 2661414
    return-void
.end method

.method public final b(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 2661407
    if-nez p1, :cond_0

    .line 2661408
    const/4 v0, 0x0

    invoke-super {p0, v0}, LX/3tK;->b(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    .line 2661409
    :goto_0
    return-object v0

    .line 2661410
    :cond_0
    iget-object v0, p0, LX/JC2;->t:LX/JAc;

    if-nez v0, :cond_1

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 2661411
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    move-object v0, p1

    .line 2661412
    check-cast v0, LX/2nf;

    invoke-interface {v0}, LX/2nf;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, LX/JAc;

    iput-object v0, p0, LX/JC2;->t:LX/JAc;

    .line 2661413
    :cond_1
    invoke-super {p0, p1}, LX/3tK;->b(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 2661399
    if-nez p1, :cond_0

    .line 2661400
    sget v0, LX/JC2;->j:I

    .line 2661401
    :goto_0
    return v0

    .line 2661402
    :cond_0
    invoke-virtual {p0}, LX/3tK;->a()Landroid/database/Cursor;

    move-result-object v0

    check-cast v0, LX/2nf;

    .line 2661403
    invoke-interface {v0, p1}, LX/2nf;->moveToPosition(I)Z

    .line 2661404
    invoke-interface {v0}, LX/2nf;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, LX/JBL;

    .line 2661405
    invoke-interface {v0}, LX/JAW;->d()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/JCx;->a(Ljava/lang/Iterable;)Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->ordinal()I

    move-result v0

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2661406
    sget v0, LX/JC2;->j:I

    add-int/lit8 v0, v0, 0x1

    return v0
.end method
