.class public final LX/HU4;
.super LX/1a1;
.source ""


# instance fields
.field public l:Landroid/widget/TextView;

.field public m:Landroid/widget/TextView;

.field public n:Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;

.field public o:Lcom/facebook/pages/fb4a/politics/ShareOpinionButtonView;

.field public final synthetic p:LX/HU5;


# direct methods
.method public constructor <init>(LX/HU5;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2471533
    iput-object p1, p0, LX/HU4;->p:LX/HU5;

    .line 2471534
    invoke-direct {p0, p2}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 2471535
    const v0, 0x7f0d188f

    invoke-static {p2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/HU4;->l:Landroid/widget/TextView;

    .line 2471536
    const v0, 0x7f0d1895

    invoke-static {p2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/HU4;->m:Landroid/widget/TextView;

    .line 2471537
    const v0, 0x7f0d1890

    invoke-static {p2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;

    iput-object v0, p0, LX/HU4;->n:Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;

    .line 2471538
    const v0, 0x7f0d1891

    invoke-static {p2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/politics/ShareOpinionButtonView;

    iput-object v0, p0, LX/HU4;->o:Lcom/facebook/pages/fb4a/politics/ShareOpinionButtonView;

    .line 2471539
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel$OpinionsEdgesModel$OpinionsEdgesNodeModel;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const v6, 0x4f3d3985

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2471540
    if-eqz p1, :cond_3

    .line 2471541
    invoke-virtual {p1}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel$OpinionsEdgesModel$OpinionsEdgesNodeModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2471542
    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    if-eqz v0, :cond_6

    .line 2471543
    invoke-virtual {p1}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel$OpinionsEdgesModel$OpinionsEdgesNodeModel;->l()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v6}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 2471544
    if-eqz v0, :cond_4

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_5

    move v0, v1

    :goto_2
    if-eqz v0, :cond_9

    .line 2471545
    invoke-virtual {p1}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel$OpinionsEdgesModel$OpinionsEdgesNodeModel;->l()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v6}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_3
    invoke-virtual {v0, v2}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2471546
    if-eqz v0, :cond_8

    move v0, v1

    :goto_4
    if-eqz v0, :cond_c

    .line 2471547
    invoke-virtual {p1}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel$OpinionsEdgesModel$OpinionsEdgesNodeModel;->l()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v6}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_a

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_5
    invoke-virtual {v0, v2}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2471548
    const-class v5, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel$OpinionsEdgesModel$OpinionsEdgesNodeModel$OpinionTopicModel$OpinionTopicEdgesModel$OpinionTopicEdgesNodeModel;

    invoke-virtual {v4, v0, v2, v5}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    if-eqz v0, :cond_b

    move v0, v1

    :goto_6
    if-eqz v0, :cond_f

    .line 2471549
    invoke-virtual {p1}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel$OpinionsEdgesModel$OpinionsEdgesNodeModel;->l()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v6}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_d

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_7
    invoke-virtual {v0, v2}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2471550
    const-class v5, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel$OpinionsEdgesModel$OpinionsEdgesNodeModel$OpinionTopicModel$OpinionTopicEdgesModel$OpinionTopicEdgesNodeModel;

    invoke-virtual {v4, v0, v2, v5}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel$OpinionsEdgesModel$OpinionsEdgesNodeModel$OpinionTopicModel$OpinionTopicEdgesModel$OpinionTopicEdgesNodeModel;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel$OpinionsEdgesModel$OpinionsEdgesNodeModel$OpinionTopicModel$OpinionTopicEdgesModel$OpinionTopicEdgesNodeModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_e

    move v0, v1

    :goto_8
    if-eqz v0, :cond_11

    .line 2471551
    invoke-virtual {p1}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel$OpinionsEdgesModel$OpinionsEdgesNodeModel;->l()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v6}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_10

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_9
    invoke-virtual {v0, v2}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2471552
    const-class v5, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel$OpinionsEdgesModel$OpinionsEdgesNodeModel$OpinionTopicModel$OpinionTopicEdgesModel$OpinionTopicEdgesNodeModel;

    invoke-virtual {v4, v0, v2, v5}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel$OpinionsEdgesModel$OpinionsEdgesNodeModel$OpinionTopicModel$OpinionTopicEdgesModel$OpinionTopicEdgesNodeModel;

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel$OpinionsEdgesModel$OpinionsEdgesNodeModel$OpinionTopicModel$OpinionTopicEdgesModel$OpinionTopicEdgesNodeModel;->j()Ljava/lang/String;

    move-result-object v0

    .line 2471553
    :goto_a
    invoke-virtual {p1}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel$OpinionsEdgesModel$OpinionsEdgesNodeModel;->n()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {p1}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel$OpinionsEdgesModel$OpinionsEdgesNodeModel;->n()Ljava/lang/String;

    move-result-object v3

    .line 2471554
    :cond_0
    iget-object v4, p0, LX/HU4;->l:Landroid/widget/TextView;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2471555
    if-eqz p1, :cond_12

    invoke-virtual {p1}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel$OpinionsEdgesModel$OpinionsEdgesNodeModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_12

    invoke-virtual {p1}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel$OpinionsEdgesModel$OpinionsEdgesNodeModel;->k()Ljava/lang/String;

    move-result-object v0

    .line 2471556
    :goto_b
    iget-object v4, p0, LX/HU4;->m:Landroid/widget/TextView;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2471557
    iget-object v0, p0, LX/HU4;->p:LX/HU5;

    iget-object v0, v0, LX/HU5;->e:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;

    .line 2471558
    iget-boolean v4, v0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;->q:Z

    move v0, v4

    .line 2471559
    if-eqz v0, :cond_13

    iget-object v0, p0, LX/HU4;->p:LX/HU5;

    iget-object v0, v0, LX/HU5;->d:LX/0Uh;

    const/16 v4, 0x519

    invoke-virtual {v0, v4, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_13

    if-eqz v3, :cond_13

    .line 2471560
    iget-object v0, p0, LX/HU4;->o:Lcom/facebook/pages/fb4a/politics/ShareOpinionButtonView;

    invoke-virtual {p1}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel$OpinionsEdgesModel$OpinionsEdgesNodeModel;->j()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, LX/HU4;->p:LX/HU5;

    iget-object v4, v4, LX/HU5;->e:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;

    invoke-virtual {v0, v3, v2, v4}, Lcom/facebook/pages/fb4a/politics/ShareOpinionButtonView;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;)V

    .line 2471561
    :goto_c
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel$OpinionsEdgesModel$OpinionsEdgesNodeModel;->m()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2471562
    iget-object v0, p0, LX/HU4;->n:Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;

    invoke-virtual {v0, v1}, Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;->setClickable(Z)V

    .line 2471563
    iget-object v0, p0, LX/HU4;->n:Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;

    invoke-virtual {p1}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel$OpinionsEdgesModel$OpinionsEdgesNodeModel;->m()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel$OpinionsEdgesModel$OpinionsEdgesNodeModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;->a(Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;Ljava/lang/String;)V

    .line 2471564
    iget-object v0, p0, LX/HU4;->m:Landroid/widget/TextView;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2471565
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 2471566
    goto/16 :goto_0

    :cond_3
    move v0, v2

    goto/16 :goto_0

    :cond_4
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto/16 :goto_1

    :cond_5
    move v0, v2

    goto/16 :goto_2

    :cond_6
    move v0, v2

    goto/16 :goto_2

    .line 2471567
    :cond_7
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto/16 :goto_3

    :cond_8
    move v0, v2

    .line 2471568
    goto/16 :goto_4

    :cond_9
    move v0, v2

    goto/16 :goto_4

    .line 2471569
    :cond_a
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto/16 :goto_5

    :cond_b
    move v0, v2

    .line 2471570
    goto/16 :goto_6

    :cond_c
    move v0, v2

    goto/16 :goto_6

    .line 2471571
    :cond_d
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto/16 :goto_7

    :cond_e
    move v0, v2

    .line 2471572
    goto/16 :goto_8

    :cond_f
    move v0, v2

    goto/16 :goto_8

    .line 2471573
    :cond_10
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto/16 :goto_9

    :cond_11
    move-object v0, v3

    .line 2471574
    goto/16 :goto_a

    .line 2471575
    :cond_12
    const-string v0, ""

    goto/16 :goto_b

    .line 2471576
    :cond_13
    iget-object v0, p0, LX/HU4;->o:Lcom/facebook/pages/fb4a/politics/ShareOpinionButtonView;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lcom/facebook/pages/fb4a/politics/ShareOpinionButtonView;->setVisibility(I)V

    goto :goto_c
.end method
