.class public final enum LX/I7V;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/I7V;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/I7V;

.field public static final enum FETCH_EVENT_FROM_DB:LX/I7V;

.field public static final enum FETCH_PERMALINK_GRAPHQL:LX/I7V;

.field public static final enum FETCH_PERMALINK_STORIES_GRAPHQL:LX/I7V;

.field public static final enum FETC_PERMALINK_ACTIVITIES_GRAPHQL:LX/I7V;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2538967
    new-instance v0, LX/I7V;

    const-string v1, "FETCH_PERMALINK_GRAPHQL"

    invoke-direct {v0, v1, v2}, LX/I7V;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I7V;->FETCH_PERMALINK_GRAPHQL:LX/I7V;

    .line 2538968
    new-instance v0, LX/I7V;

    const-string v1, "FETCH_EVENT_FROM_DB"

    invoke-direct {v0, v1, v3}, LX/I7V;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I7V;->FETCH_EVENT_FROM_DB:LX/I7V;

    .line 2538969
    new-instance v0, LX/I7V;

    const-string v1, "FETCH_PERMALINK_STORIES_GRAPHQL"

    invoke-direct {v0, v1, v4}, LX/I7V;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I7V;->FETCH_PERMALINK_STORIES_GRAPHQL:LX/I7V;

    .line 2538970
    new-instance v0, LX/I7V;

    const-string v1, "FETC_PERMALINK_ACTIVITIES_GRAPHQL"

    invoke-direct {v0, v1, v5}, LX/I7V;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I7V;->FETC_PERMALINK_ACTIVITIES_GRAPHQL:LX/I7V;

    .line 2538971
    const/4 v0, 0x4

    new-array v0, v0, [LX/I7V;

    sget-object v1, LX/I7V;->FETCH_PERMALINK_GRAPHQL:LX/I7V;

    aput-object v1, v0, v2

    sget-object v1, LX/I7V;->FETCH_EVENT_FROM_DB:LX/I7V;

    aput-object v1, v0, v3

    sget-object v1, LX/I7V;->FETCH_PERMALINK_STORIES_GRAPHQL:LX/I7V;

    aput-object v1, v0, v4

    sget-object v1, LX/I7V;->FETC_PERMALINK_ACTIVITIES_GRAPHQL:LX/I7V;

    aput-object v1, v0, v5

    sput-object v0, LX/I7V;->$VALUES:[LX/I7V;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2538965
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/I7V;
    .locals 1

    .prologue
    .line 2538972
    const-class v0, LX/I7V;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/I7V;

    return-object v0
.end method

.method public static values()[LX/I7V;
    .locals 1

    .prologue
    .line 2538966
    sget-object v0, LX/I7V;->$VALUES:[LX/I7V;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/I7V;

    return-object v0
.end method
