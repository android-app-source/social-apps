.class public LX/HQS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/HQL;


# instance fields
.field public final a:LX/HQc;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/BA0;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Landroid/content/Context;

.field public final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/17Y;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/8Do;

.field private final i:LX/00H;

.field private j:LX/CZd;

.field public k:LX/HQa;

.field private l:Ljava/lang/String;

.field public m:LX/HQN;

.field private n:LX/0gc;

.field private o:Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;

.field private p:Landroid/widget/FrameLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/HQc;LX/0Or;LX/0Or;LX/0Ot;LX/0Or;LX/0Or;LX/8Do;LX/00H;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/HQc;",
            "LX/0Or",
            "<",
            "LX/BA0;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;",
            "LX/0Or",
            "<",
            "LX/17Y;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/8Do;",
            "LX/00H;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2463600
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2463601
    iput-object p1, p0, LX/HQS;->e:Landroid/content/Context;

    .line 2463602
    iput-object p2, p0, LX/HQS;->a:LX/HQc;

    .line 2463603
    iput-object p3, p0, LX/HQS;->c:LX/0Or;

    .line 2463604
    iput-object p4, p0, LX/HQS;->d:LX/0Or;

    .line 2463605
    iput-object p5, p0, LX/HQS;->b:LX/0Ot;

    .line 2463606
    iput-object p6, p0, LX/HQS;->f:LX/0Or;

    .line 2463607
    iput-object p7, p0, LX/HQS;->g:LX/0Or;

    .line 2463608
    iput-object p8, p0, LX/HQS;->h:LX/8Do;

    .line 2463609
    iput-object p9, p0, LX/HQS;->i:LX/00H;

    .line 2463610
    return-void
.end method

.method private a(Ljava/lang/String;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2463622
    new-instance v0, LX/HQQ;

    invoke-direct {v0, p0, p1}, LX/HQQ;-><init>(LX/HQS;Ljava/lang/String;)V

    return-object v0
.end method

.method private a(LX/CZd;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2463611
    iget-object v0, p1, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v0

    .line 2463612
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->q()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2463613
    const/4 v1, 0x0

    .line 2463614
    iget-object v0, p1, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v0

    .line 2463615
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->v()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz v3, :cond_1

    const-class v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v2, v3, v4, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    if-eqz v0, :cond_1

    const-class v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v2, v3, v4, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel$CoverPhotoModel$PhotoModel;->n()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2463616
    const-class v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v2, v3, v4, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel$CoverPhotoModel$PhotoModel;->n()Ljava/lang/String;

    move-result-object v0

    .line 2463617
    :goto_0
    if-eqz v0, :cond_0

    iget-object v1, p0, LX/HQS;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2463618
    iput-object v0, p0, LX/HQS;->l:Ljava/lang/String;

    .line 2463619
    invoke-direct {p0}, LX/HQS;->c()V

    .line 2463620
    :cond_0
    return-void

    .line 2463621
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public static a(Landroid/view/View;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2463439
    new-instance v0, LX/0hs;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 2463440
    invoke-virtual {v0, p1}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 2463441
    sget-object v1, LX/3AV;->BELOW:LX/3AV;

    invoke-virtual {v0, v1}, LX/0ht;->a(LX/3AV;)V

    .line 2463442
    const/4 v1, -0x1

    .line 2463443
    iput v1, v0, LX/0hs;->t:I

    .line 2463444
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v1

    neg-int v1, v1

    div-int/lit8 v1, v1, 0x2

    add-int/lit8 v2, v1, 0x1e

    const/4 v3, 0x0

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v4

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v5

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, LX/0ht;->a(Landroid/view/View;IIII)V

    .line 2463445
    invoke-virtual {v0}, LX/0ht;->d()V

    .line 2463446
    return-void
.end method

.method private b(Ljava/lang/String;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2463599
    new-instance v0, LX/HQR;

    invoke-direct {v0, p0, p1}, LX/HQR;-><init>(LX/HQS;Ljava/lang/String;)V

    return-object v0
.end method

.method private b(LX/CZd;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 2463588
    iget-object v0, p0, LX/HQS;->n:LX/0gc;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HQS;->e:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 2463589
    :cond_0
    :goto_0
    return-void

    .line 2463590
    :cond_1
    invoke-static {p1}, LX/HQS;->e(LX/CZd;)Z

    move-result v0

    .line 2463591
    iget-object v1, p0, LX/HQS;->a:LX/HQc;

    invoke-interface {v1, v0}, LX/HQc;->a(Z)V

    .line 2463592
    if-eqz v0, :cond_2

    .line 2463593
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderViewController$2;

    invoke-direct {v1, p0}, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderViewController$2;-><init>(LX/HQS;)V

    const-wide/16 v2, 0x3e8

    const v4, 0x1458b5de

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_0

    .line 2463594
    :cond_2
    iget-object v0, p0, LX/HQS;->o:Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;

    if-eqz v0, :cond_3

    .line 2463595
    iget-object v0, p0, LX/HQS;->n:LX/0gc;

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    iget-object v1, p0, LX/HQS;->o:Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;

    invoke-virtual {v0, v1}, LX/0hH;->a(Landroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->c()I

    .line 2463596
    :cond_3
    iget-object v0, p0, LX/HQS;->m:LX/HQN;

    const/4 v1, 0x0

    iget-object v2, p0, LX/HQS;->p:Landroid/widget/FrameLayout;

    invoke-interface {v0, v1, v2}, LX/HQN;->a(ZLandroid/view/View;)V

    .line 2463597
    iput-object v3, p0, LX/HQS;->o:Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;

    .line 2463598
    iput-object v3, p0, LX/HQS;->p:Landroid/widget/FrameLayout;

    goto :goto_0
.end method

.method private static c(LX/CZd;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 2463579
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 2463580
    iget-object v0, p0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v0

    .line 2463581
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->p()LX/0Px;

    move-result-object v3

    .line 2463582
    if-eqz v3, :cond_0

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2463583
    const/4 v0, 0x0

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2463584
    const/4 v0, 0x1

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2463585
    const-string v0, " \u00b7 "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2463586
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2463587
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private c()V
    .locals 3

    .prologue
    .line 2463576
    iget-object v0, p0, LX/HQS;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BA0;

    iget-object v1, p0, LX/HQS;->l:Ljava/lang/String;

    iget-object v2, p0, LX/HQS;->e:Landroid/content/Context;

    invoke-static {v2}, LX/BQ3;->a(Landroid/content/Context;)F

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/BA0;->a(Ljava/lang/String;F)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2463577
    new-instance v2, LX/HQO;

    invoke-direct {v2, p0}, LX/HQO;-><init>(LX/HQS;)V

    iget-object v0, p0, LX/HQS;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2463578
    return-void
.end method

.method private static d(LX/CZd;)LX/HQZ;
    .locals 6

    .prologue
    .line 2463553
    invoke-virtual {p0}, LX/CZd;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2463554
    iget-object v0, p0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v0

    .line 2463555
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->v()Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v1, LX/HQY;->NOT_VISIBLE:LX/HQY;

    .line 2463556
    :goto_0
    iget-object v0, p0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v0

    .line 2463557
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->H()Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, LX/8A3;->EDIT_PROFILE:LX/8A3;

    invoke-virtual {p0, v0}, LX/CZd;->a(LX/8A3;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v2, LX/HQW;->VISIBLE:LX/HQW;

    .line 2463558
    :goto_1
    iget-object v0, p0, LX/CZd;->k:Ljava/lang/String;

    move-object v4, v0

    .line 2463559
    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2463560
    iget-object v0, p0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v0

    .line 2463561
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->F()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderDataProfilePhotoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2463562
    iget-object v0, p0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v0

    .line 2463563
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->F()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderDataProfilePhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderDataProfilePhotoModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2463564
    iget-object v0, p0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v0

    .line 2463565
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->F()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderDataProfilePhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageHeaderDataProfilePhotoModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v4

    .line 2463566
    :cond_0
    const/4 v5, 0x0

    .line 2463567
    iget-object v0, p0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v0

    .line 2463568
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->G()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2463569
    iget-object v0, p0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v0

    .line 2463570
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->G()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v5

    .line 2463571
    :cond_1
    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v3, LX/HQX;->NO_DATA:LX/HQX;

    .line 2463572
    :goto_2
    new-instance v0, LX/HQZ;

    invoke-direct/range {v0 .. v5}, LX/HQZ;-><init>(LX/HQY;LX/HQW;LX/HQX;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    .line 2463573
    :cond_2
    sget-object v1, LX/HQY;->VISIBLE:LX/HQY;

    goto :goto_0

    .line 2463574
    :cond_3
    sget-object v2, LX/HQW;->NOT_VISIBLE:LX/HQW;

    goto :goto_1

    .line 2463575
    :cond_4
    sget-object v3, LX/HQX;->HAS_DATA:LX/HQX;

    goto :goto_2
.end method

.method public static d(LX/HQS;)V
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 2463623
    iget-object v0, p0, LX/HQS;->e:Landroid/content/Context;

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 2463624
    if-eqz v0, :cond_0

    iget-object v1, p0, LX/HQS;->n:LX/0gc;

    invoke-virtual {v1}, LX/0gc;->g()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2463625
    :cond_0
    :goto_0
    return-void

    .line 2463626
    :cond_1
    iget-object v0, p0, LX/HQS;->o:Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;

    if-nez v0, :cond_0

    .line 2463627
    new-instance v0, Landroid/widget/FrameLayout;

    iget-object v1, p0, LX/HQS;->e:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/HQS;->p:Landroid/widget/FrameLayout;

    .line 2463628
    iget-object v0, p0, LX/HQS;->p:Landroid/widget/FrameLayout;

    invoke-static {}, LX/473;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setId(I)V

    .line 2463629
    new-instance v0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;

    invoke-direct {v0}, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;-><init>()V

    iput-object v0, p0, LX/HQS;->o:Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;

    .line 2463630
    iget-object v0, p0, LX/HQS;->o:Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;

    sget-object v1, LX/CdT;->FINCH_PROFILE:LX/CdT;

    .line 2463631
    iput-object v1, v0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->n:LX/CdT;

    .line 2463632
    iget-object v0, p0, LX/HQS;->o:Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;

    const-string v1, "android_page_header_add_photo_button"

    .line 2463633
    iput-object v1, v0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->o:Ljava/lang/String;

    .line 2463634
    iget-object v0, p0, LX/HQS;->o:Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;

    const-string v1, "android_suggest_profile_picture"

    .line 2463635
    iput-object v1, v0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->p:Ljava/lang/String;

    .line 2463636
    iget-object v0, p0, LX/HQS;->o:Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;

    iget-object v1, p0, LX/HQS;->k:LX/HQa;

    iget-wide v2, v1, LX/HQa;->f:J

    .line 2463637
    iput-wide v2, v0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->l:J

    .line 2463638
    iget-object v0, p0, LX/HQS;->o:Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;

    .line 2463639
    iput-boolean v4, v0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->i:Z

    .line 2463640
    iget-object v0, p0, LX/HQS;->o:Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;

    .line 2463641
    iput-boolean v4, v0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->h:Z

    .line 2463642
    iget-object v0, p0, LX/HQS;->n:LX/0gc;

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    iget-object v1, p0, LX/HQS;->p:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getId()I

    move-result v1

    iget-object v2, p0, LX/HQS;->o:Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;

    invoke-virtual {v0, v1, v2}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->c()I

    .line 2463643
    iget-object v0, p0, LX/HQS;->m:LX/HQN;

    iget-object v1, p0, LX/HQS;->p:Landroid/widget/FrameLayout;

    invoke-interface {v0, v4, v1}, LX/HQN;->a(ZLandroid/view/View;)V

    goto :goto_0
.end method

.method private e()LX/HQV;
    .locals 13

    .prologue
    const/4 v4, 0x0

    const/4 v9, 0x0

    const/4 v6, 0x1

    .line 2463487
    iget-object v0, p0, LX/HQS;->j:LX/CZd;

    .line 2463488
    iget-object v1, v0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v1

    .line 2463489
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->v()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, LX/HQS;->j:LX/CZd;

    .line 2463490
    iget-object v1, v0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v1

    .line 2463491
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->s()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v6

    .line 2463492
    :goto_0
    iget-object v1, p0, LX/HQS;->j:LX/CZd;

    .line 2463493
    iget-object v2, v1, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v1, v2

    .line 2463494
    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->v()Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, LX/HQS;->j:LX/CZd;

    .line 2463495
    iget-object v2, v1, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v1, v2

    .line 2463496
    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->s()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2463497
    iget-object v1, p0, LX/HQS;->j:LX/CZd;

    .line 2463498
    iget-object v2, v1, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v1, v2

    .line 2463499
    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->q()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    .line 2463500
    if-nez v1, :cond_3

    move v1, v6

    .line 2463501
    :goto_1
    if-nez v0, :cond_0

    if-eqz v1, :cond_5

    :cond_0
    iget-object v0, p0, LX/HQS;->j:LX/CZd;

    invoke-virtual {v0}, LX/CZd;->c()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2463502
    iget-object v0, p0, LX/HQS;->j:LX/CZd;

    .line 2463503
    iget-object v1, v0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v1

    .line 2463504
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->D()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_11

    .line 2463505
    iget-object v0, p0, LX/HQS;->j:LX/CZd;

    .line 2463506
    iget-object v1, v0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v1

    .line 2463507
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->D()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 2463508
    :goto_2
    new-array v3, v6, [Ljava/lang/String;

    iget-object v1, p0, LX/HQS;->j:LX/CZd;

    .line 2463509
    iget-object v2, v1, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v1, v2

    .line 2463510
    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->x()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v9

    move-object v2, v4

    move-object v1, v0

    .line 2463511
    :goto_3
    iget-object v0, p0, LX/HQS;->j:LX/CZd;

    .line 2463512
    iget-object v5, v0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v5

    .line 2463513
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->N()Ljava/lang/String;

    move-result-object v7

    .line 2463514
    sget-object v5, LX/HQU;->CATEGORY:LX/HQU;

    .line 2463515
    iget-object v0, p0, LX/HQS;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    sget v8, LX/FQe;->f:I

    invoke-virtual {v0, v8, v9}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/HQS;->j:LX/CZd;

    .line 2463516
    iget-object v8, v0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v8

    .line 2463517
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->K()Z

    move-result v0

    if-nez v0, :cond_9

    .line 2463518
    :cond_1
    iget-object v0, p0, LX/HQS;->i:LX/00H;

    .line 2463519
    iget-object v7, v0, LX/00H;->j:LX/01T;

    move-object v0, v7

    .line 2463520
    sget-object v7, LX/01T;->PAA:LX/01T;

    if-ne v0, v7, :cond_e

    .line 2463521
    iget-object v0, p0, LX/HQS;->j:LX/CZd;

    invoke-static {v0}, LX/HQS;->c(LX/CZd;)Ljava/lang/String;

    move-result-object v0

    move-object v8, v4

    move-object v4, v5

    move-object v5, v0

    .line 2463522
    :goto_4
    iget-object v0, p0, LX/HQS;->h:LX/8Do;

    invoke-virtual {v0}, LX/8Do;->a()Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, LX/HQS;->j:LX/CZd;

    invoke-virtual {v0}, LX/CZd;->c()Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, LX/HQS;->j:LX/CZd;

    .line 2463523
    iget-object v7, v0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v7

    .line 2463524
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->v()Z

    move-result v0

    if-nez v0, :cond_d

    move v7, v6

    .line 2463525
    :goto_5
    new-instance v0, LX/HQV;

    iget-object v10, p0, LX/HQS;->h:LX/8Do;

    invoke-virtual {v10}, LX/8Do;->a()Z

    move-result v10

    if-nez v10, :cond_c

    :goto_6
    invoke-direct/range {v0 .. v8}, LX/HQV;-><init>(Ljava/lang/String;Landroid/graphics/PointF;[Ljava/lang/String;LX/HQU;Ljava/lang/String;ZZLandroid/view/View$OnClickListener;)V

    return-object v0

    :cond_2
    move v0, v9

    .line 2463526
    goto/16 :goto_0

    :cond_3
    move v1, v9

    .line 2463527
    goto/16 :goto_1

    :cond_4
    move v1, v9

    goto/16 :goto_1

    .line 2463528
    :cond_5
    iget-object v0, p0, LX/HQS;->j:LX/CZd;

    .line 2463529
    iget-object v1, v0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v1

    .line 2463530
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->q()LX/1vs;

    move-result-object v0

    iget-object v5, v0, LX/1vs;->a:LX/15i;

    iget v7, v0, LX/1vs;->b:I

    .line 2463531
    if-eqz v7, :cond_10

    const-class v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v5, v7, v6, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    if-eqz v0, :cond_10

    const-class v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v5, v7, v6, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel$CoverPhotoModel$PhotoModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_10

    const-class v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v5, v7, v6, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel$CoverPhotoModel$PhotoModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_10

    .line 2463532
    const-class v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v5, v7, v6, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel$CoverPhotoModel$PhotoModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v1

    .line 2463533
    :goto_7
    if-eqz v7, :cond_7

    invoke-virtual {v5, v7, v9}, LX/15i;->g(II)I

    move-result v0

    if-eqz v0, :cond_6

    move v0, v6

    :goto_8
    if-eqz v0, :cond_f

    .line 2463534
    invoke-virtual {v5, v7, v9}, LX/15i;->g(II)I

    move-result v0

    .line 2463535
    invoke-virtual {v5, v7, v9}, LX/15i;->g(II)I

    move-result v3

    .line 2463536
    new-instance v2, Landroid/graphics/PointF;

    invoke-virtual {v5, v0, v9}, LX/15i;->l(II)D

    move-result-wide v10

    double-to-float v0, v10

    invoke-virtual {v5, v3, v6}, LX/15i;->l(II)D

    move-result-wide v10

    double-to-float v3, v10

    invoke-direct {v2, v0, v3}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2463537
    :goto_9
    if-eqz v7, :cond_8

    const-class v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v5, v7, v6, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    if-eqz v0, :cond_8

    const-class v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v5, v7, v6, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel$CoverPhotoModel$PhotoModel;->j()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 2463538
    const/4 v0, 0x2

    new-array v3, v0, [Ljava/lang/String;

    iget-object v0, p0, LX/HQS;->j:LX/CZd;

    .line 2463539
    iget-object v8, v0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v8

    .line 2463540
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->x()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v9

    const-class v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v5, v7, v6, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageProfileCoverPhotosDataModel$CoverPhotoModel$PhotoModel;->j()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v6

    goto/16 :goto_3

    :cond_6
    move v0, v9

    .line 2463541
    goto :goto_8

    :cond_7
    move v0, v9

    goto :goto_8

    .line 2463542
    :cond_8
    new-array v3, v6, [Ljava/lang/String;

    iget-object v0, p0, LX/HQS;->j:LX/CZd;

    .line 2463543
    iget-object v5, v0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v5

    .line 2463544
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->x()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v9

    goto/16 :goto_3

    .line 2463545
    :cond_9
    invoke-static {v7}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 2463546
    const-string v0, "@%s"

    invoke-static {v0, v7}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 2463547
    sget-object v4, LX/HQU;->USERNAME:LX/HQU;

    .line 2463548
    iget-object v0, p0, LX/HQS;->j:LX/CZd;

    sget-object v8, LX/8A3;->ADMINISTER:LX/8A3;

    invoke-virtual {v0, v8}, LX/CZd;->a(LX/8A3;)Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-direct {p0, v7}, LX/HQS;->a(Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v0

    :goto_a
    move-object v8, v0

    goto/16 :goto_4

    :cond_a
    invoke-direct {p0, v7}, LX/HQS;->b(Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v0

    goto :goto_a

    .line 2463549
    :cond_b
    iget-object v0, p0, LX/HQS;->j:LX/CZd;

    sget-object v7, LX/8A3;->ADMINISTER:LX/8A3;

    invoke-virtual {v0, v7}, LX/CZd;->a(LX/8A3;)Z

    move-result v0

    if-eqz v0, :cond_e

    iget-object v0, p0, LX/HQS;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    sget v7, LX/FQe;->g:I

    invoke-virtual {v0, v7, v9}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 2463550
    sget-object v0, LX/HQU;->CREATE_USERNAME:LX/HQU;

    .line 2463551
    invoke-direct {p0}, LX/HQS;->f()Landroid/view/View$OnClickListener;

    move-result-object v8

    move-object v5, v4

    move-object v4, v0

    goto/16 :goto_4

    :cond_c
    move v6, v9

    .line 2463552
    goto/16 :goto_6

    :cond_d
    move v7, v9

    goto/16 :goto_5

    :cond_e
    move-object v8, v4

    move-object v12, v5

    move-object v5, v4

    move-object v4, v12

    goto/16 :goto_4

    :cond_f
    move-object v2, v4

    goto/16 :goto_9

    :cond_10
    move-object v1, v4

    goto/16 :goto_7

    :cond_11
    move-object v0, v4

    goto/16 :goto_2
.end method

.method private static e(LX/CZd;)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2463471
    invoke-virtual {p0}, LX/CZd;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2463472
    iget-object v2, p0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v2, v2

    .line 2463473
    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->v()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2463474
    iget-object v2, p0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v2, v2

    .line 2463475
    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->H()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2463476
    :cond_0
    :goto_0
    return v0

    .line 2463477
    :cond_1
    iget-object v2, p0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v2, v2

    .line 2463478
    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->B()Lcom/facebook/graphql/enums/GraphQLPlaceType;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPlaceType;->RESIDENCE:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    if-eq v2, v3, :cond_0

    .line 2463479
    iget-object v2, p0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v2, v2

    .line 2463480
    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->o()LX/2uF;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 2463481
    iget-object v2, p0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v2, v2

    .line 2463482
    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->o()LX/2uF;

    move-result-object v2

    invoke-virtual {v2}, LX/39O;->a()Z

    move-result v2

    if-nez v2, :cond_3

    .line 2463483
    iget-object v2, p0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v2, v2

    .line 2463484
    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->o()LX/2uF;

    move-result-object v2

    invoke-virtual {v2}, LX/3Sa;->e()LX/3Sh;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, LX/2sN;->a()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, LX/2sN;->b()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    .line 2463485
    const-class v5, Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLAttributionSource;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    invoke-virtual {v4, v3, v1, v5, v6}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLAttributionSource;->WIKIPEDIA:Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    if-ne v3, v4, :cond_2

    goto :goto_0

    :cond_3
    move v0, v1

    .line 2463486
    goto :goto_0
.end method

.method private f()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2463470
    new-instance v0, LX/HQP;

    invoke-direct {v0, p0}, LX/HQP;-><init>(LX/HQS;)V

    return-object v0
.end method


# virtual methods
.method public final a()LX/1cC;
    .locals 1

    .prologue
    .line 2463469
    iget-object v0, p0, LX/HQS;->a:LX/HQc;

    invoke-interface {v0}, LX/HQc;->a()LX/1cC;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/CZd;LX/HQN;)LX/HQa;
    .locals 12

    .prologue
    .line 2463452
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2463453
    iput-object p2, p0, LX/HQS;->m:LX/HQN;

    .line 2463454
    sget-object v0, LX/8A3;->EDIT_PROFILE:LX/8A3;

    invoke-virtual {p1, v0}, LX/CZd;->a(LX/8A3;)Z

    move-result v1

    .line 2463455
    sget-object v0, LX/8A3;->BASIC_ADMIN:LX/8A3;

    invoke-virtual {p1, v0}, LX/CZd;->a(LX/8A3;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v2, 0x1

    .line 2463456
    :goto_0
    iput-object p1, p0, LX/HQS;->j:LX/CZd;

    .line 2463457
    new-instance v0, LX/HQa;

    invoke-direct {p0}, LX/HQS;->e()LX/HQV;

    move-result-object v3

    invoke-virtual {p1}, LX/CZd;->c()Z

    move-result v4

    .line 2463458
    iget-object v5, p1, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v5, v5

    .line 2463459
    invoke-virtual {v5}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->x()Ljava/lang/String;

    move-result-object v5

    .line 2463460
    iget-wide v10, p1, LX/CZd;->a:J

    move-wide v6, v10

    .line 2463461
    invoke-static {p1}, LX/HQS;->d(LX/CZd;)LX/HQZ;

    move-result-object v8

    .line 2463462
    iget-object v9, p1, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v9, v9

    .line 2463463
    invoke-virtual {v9}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->H()Z

    move-result v9

    invoke-direct/range {v0 .. v9}, LX/HQa;-><init>(ZZLX/HQV;ZLjava/lang/String;JLX/HQZ;Z)V

    iput-object v0, p0, LX/HQS;->k:LX/HQa;

    .line 2463464
    iget-object v0, p0, LX/HQS;->a:LX/HQc;

    iget-object v1, p0, LX/HQS;->j:LX/CZd;

    iget-object v2, p0, LX/HQS;->k:LX/HQa;

    invoke-interface {v0, v1, v2}, LX/HQc;->a(LX/CZd;LX/HQa;)V

    .line 2463465
    invoke-direct {p0, p1}, LX/HQS;->a(LX/CZd;)V

    .line 2463466
    invoke-direct {p0, p1}, LX/HQS;->b(LX/CZd;)V

    .line 2463467
    iget-object v0, p0, LX/HQS;->k:LX/HQa;

    return-object v0

    .line 2463468
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final a(LX/0gc;)V
    .locals 0

    .prologue
    .line 2463450
    iput-object p1, p0, LX/HQS;->n:LX/0gc;

    .line 2463451
    return-void
.end method

.method public final a(Landroid/os/ParcelUuid;)V
    .locals 1

    .prologue
    .line 2463448
    iget-object v0, p0, LX/HQS;->a:LX/HQc;

    invoke-interface {v0, p1}, LX/HQc;->a(Landroid/os/ParcelUuid;)V

    .line 2463449
    return-void
.end method

.method public final b()LX/1cC;
    .locals 1

    .prologue
    .line 2463447
    iget-object v0, p0, LX/HQS;->a:LX/HQc;

    invoke-interface {v0}, LX/HQc;->b()LX/1cC;

    move-result-object v0

    return-object v0
.end method
