.class public final LX/Hzs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLInterfaces$ReactionUnitFragment;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Hzt;


# direct methods
.method public constructor <init>(LX/Hzt;)V
    .locals 0

    .prologue
    .line 2526248
    iput-object p1, p0, LX/Hzs;->a:LX/Hzt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2526249
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2526250
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2526251
    if-eqz p1, :cond_0

    .line 2526252
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2526253
    if-nez v0, :cond_1

    .line 2526254
    :cond_0
    :goto_0
    return-void

    .line 2526255
    :cond_1
    iget-object v0, p0, LX/Hzs;->a:LX/Hzt;

    iget-object v1, v0, LX/Hzt;->f:LX/I2i;

    .line 2526256
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2526257
    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    .line 2526258
    iget-object v2, v1, LX/I2i;->g:Ljava/util/HashMap;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 2526259
    if-eqz v2, :cond_2

    iget-object v3, v1, LX/I2i;->d:Ljava/util/List;

    if-eqz v3, :cond_2

    .line 2526260
    iget-object v3, v1, LX/I2i;->d:Ljava/util/List;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-interface {v3, p1, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2526261
    iget-object v3, v1, LX/I2i;->f:Ljava/util/List;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    sget-object p1, LX/I2b;->i:LX/I2b;

    invoke-static {p1, v0}, LX/I2V;->a(LX/I2b;Ljava/lang/Object;)LX/I2V;

    move-result-object p1

    invoke-interface {v3, v2, p1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2526262
    :cond_2
    iget-object v0, p0, LX/Hzs;->a:LX/Hzt;

    invoke-static {v0}, LX/Hzt;->k(LX/Hzt;)V

    goto :goto_0
.end method
