.class public LX/IW6;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static l:LX/0Xm;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/IW5;

.field public final c:LX/IRJ;

.field public final d:LX/DOp;

.field public final e:LX/IVs;

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/feed/ui/partdefinitions/GroupsGraphQLStorySelectorPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/feed/rows/partdefinitions/HidePinnedStoryPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/ITk;

.field public final i:LX/ISJ;

.field public j:LX/ISH;

.field public k:LX/1Qq;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/IW5;LX/IRJ;LX/DOp;LX/IVs;LX/0Ot;LX/0Ot;LX/ITk;LX/ISJ;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/IW5;",
            "LX/IRJ;",
            "LX/DOp;",
            "LX/IVs;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/feed/ui/partdefinitions/GroupsGraphQLStorySelectorPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/feed/rows/partdefinitions/HidePinnedStoryPartDefinition;",
            ">;",
            "LX/ITk;",
            "LX/ISJ;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2583443
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2583444
    iput-object p1, p0, LX/IW6;->a:Landroid/content/Context;

    .line 2583445
    iput-object p2, p0, LX/IW6;->b:LX/IW5;

    .line 2583446
    iput-object p3, p0, LX/IW6;->c:LX/IRJ;

    .line 2583447
    iput-object p4, p0, LX/IW6;->d:LX/DOp;

    .line 2583448
    iput-object p5, p0, LX/IW6;->e:LX/IVs;

    .line 2583449
    iput-object p6, p0, LX/IW6;->f:LX/0Ot;

    .line 2583450
    iput-object p7, p0, LX/IW6;->g:LX/0Ot;

    .line 2583451
    iput-object p8, p0, LX/IW6;->h:LX/ITk;

    .line 2583452
    iput-object p9, p0, LX/IW6;->i:LX/ISJ;

    .line 2583453
    return-void
.end method

.method public static a(LX/0QB;)LX/IW6;
    .locals 13

    .prologue
    .line 2583454
    const-class v1, LX/IW6;

    monitor-enter v1

    .line 2583455
    :try_start_0
    sget-object v0, LX/IW6;->l:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2583456
    sput-object v2, LX/IW6;->l:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2583457
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2583458
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2583459
    new-instance v3, LX/IW6;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/IW5;->a(LX/0QB;)LX/IW5;

    move-result-object v5

    check-cast v5, LX/IW5;

    const-class v6, LX/IRJ;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/IRJ;

    invoke-static {v0}, LX/DOp;->b(LX/0QB;)LX/DOp;

    move-result-object v7

    check-cast v7, LX/DOp;

    invoke-static {v0}, LX/IVs;->a(LX/0QB;)LX/IVs;

    move-result-object v8

    check-cast v8, LX/IVs;

    const/16 v9, 0x2438

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x2414

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const-class v11, LX/ITk;

    invoke-interface {v0, v11}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/ITk;

    const-class v12, LX/ISJ;

    invoke-interface {v0, v12}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/ISJ;

    invoke-direct/range {v3 .. v12}, LX/IW6;-><init>(Landroid/content/Context;LX/IW5;LX/IRJ;LX/DOp;LX/IVs;LX/0Ot;LX/0Ot;LX/ITk;LX/ISJ;)V

    .line 2583460
    move-object v0, v3

    .line 2583461
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2583462
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IW6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2583463
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2583464
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
