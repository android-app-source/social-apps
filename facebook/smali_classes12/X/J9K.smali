.class public LX/J9K;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/J9K;


# instance fields
.field public final a:LX/17W;


# direct methods
.method public constructor <init>(LX/17W;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2652702
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2652703
    iput-object p1, p0, LX/J9K;->a:LX/17W;

    .line 2652704
    return-void
.end method

.method public static a(LX/0QB;)LX/J9K;
    .locals 4

    .prologue
    .line 2652705
    sget-object v0, LX/J9K;->b:LX/J9K;

    if-nez v0, :cond_1

    .line 2652706
    const-class v1, LX/J9K;

    monitor-enter v1

    .line 2652707
    :try_start_0
    sget-object v0, LX/J9K;->b:LX/J9K;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2652708
    if-eqz v2, :cond_0

    .line 2652709
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2652710
    new-instance p0, LX/J9K;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v3

    check-cast v3, LX/17W;

    invoke-direct {p0, v3}, LX/J9K;-><init>(LX/17W;)V

    .line 2652711
    move-object v0, p0

    .line 2652712
    sput-object v0, LX/J9K;->b:LX/J9K;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2652713
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2652714
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2652715
    :cond_1
    sget-object v0, LX/J9K;->b:LX/J9K;

    return-object v0

    .line 2652716
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2652717
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
