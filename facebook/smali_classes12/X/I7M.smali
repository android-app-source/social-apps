.class public final LX/I7M;
.super LX/1OX;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/events/permalink/EventPermalinkFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/permalink/EventPermalinkFragment;)V
    .locals 0

    .prologue
    .line 2538848
    iput-object p1, p0, LX/I7M;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    invoke-direct {p0}, LX/1OX;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 12

    .prologue
    .line 2538849
    if-lez p3, :cond_1

    .line 2538850
    iget-object v0, p0, LX/I7M;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    iget-boolean v0, v0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aR:Z

    if-nez v0, :cond_1

    .line 2538851
    iget-object v0, p0, LX/I7M;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ak:Lcom/facebook/events/common/EventAnalyticsParams;

    if-nez v0, :cond_7

    sget-object v0, Lcom/facebook/events/common/ActionSource;->UNKNOWN:Lcom/facebook/events/common/ActionSource;

    invoke-virtual {v0}, Lcom/facebook/events/common/ActionSource;->getParamValue()I

    move-result v0

    .line 2538852
    :goto_0
    iget-object v1, p0, LX/I7M;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    iget-object v1, v1, Lcom/facebook/events/permalink/EventPermalinkFragment;->ak:Lcom/facebook/events/common/EventAnalyticsParams;

    if-nez v1, :cond_8

    sget-object v1, Lcom/facebook/events/common/ActionSource;->UNKNOWN:Lcom/facebook/events/common/ActionSource;

    invoke-virtual {v1}, Lcom/facebook/events/common/ActionSource;->getParamValue()I

    move-result v1

    .line 2538853
    :goto_1
    iget-object v2, p0, LX/I7M;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    iget-object v2, v2, Lcom/facebook/events/permalink/EventPermalinkFragment;->m:LX/1nQ;

    iget-object v3, p0, LX/I7M;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    .line 2538854
    iget-object v4, v3, Lcom/facebook/events/permalink/EventPermalinkFragment;->aG:Ljava/lang/String;

    move-object v3, v4

    .line 2538855
    iget-object v4, v2, LX/1nQ;->i:LX/0Zb;

    const-string v5, "event_permalink_first_scroll"

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v4

    .line 2538856
    invoke-virtual {v4}, LX/0oG;->a()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2538857
    const-string v5, "event_permalink"

    invoke-virtual {v4, v5}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2538858
    iget-object v5, v2, LX/1nQ;->j:LX/0kv;

    iget-object v6, v2, LX/1nQ;->g:Landroid/content/Context;

    invoke-virtual {v5, v6}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    .line 2538859
    const-string v5, "Event"

    invoke-virtual {v4, v5}, LX/0oG;->b(Ljava/lang/String;)LX/0oG;

    .line 2538860
    invoke-virtual {v4, v3}, LX/0oG;->c(Ljava/lang/String;)LX/0oG;

    .line 2538861
    const-string v5, "action_source"

    invoke-virtual {v4, v5, v0}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 2538862
    const-string v5, "action_ref"

    invoke-virtual {v4, v5, v1}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 2538863
    invoke-virtual {v4}, LX/0oG;->d()V

    .line 2538864
    :cond_0
    iget-object v0, p0, LX/I7M;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    const/4 v1, 0x1

    .line 2538865
    iput-boolean v1, v0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aR:Z

    .line 2538866
    :cond_1
    iget-object v0, p0, LX/I7M;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    invoke-static {v0}, Lcom/facebook/events/permalink/EventPermalinkFragment;->O(Lcom/facebook/events/permalink/EventPermalinkFragment;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/I7M;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aL:LX/E93;

    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v0

    if-gtz v0, :cond_4

    :cond_2
    iget-object v0, p0, LX/I7M;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2538867
    iget-object v3, v0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aQ:LX/IBq;

    .line 2538868
    iget-object v4, v3, LX/IBq;->k:[LX/IBr;

    move-object v3, v4

    .line 2538869
    if-nez v3, :cond_9

    .line 2538870
    :cond_3
    :goto_2
    move v0, v1

    .line 2538871
    if-eqz v0, :cond_6

    .line 2538872
    :cond_4
    iget-object v0, p0, LX/I7M;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aK:LX/I8k;

    .line 2538873
    iget-object v4, v0, LX/I8k;->u:LX/1P0;

    invoke-virtual {v4}, LX/1P1;->l()I

    move-result v4

    .line 2538874
    iget-object v5, v0, LX/I8k;->u:LX/1P0;

    invoke-virtual {v5}, LX/1P1;->n()I

    move-result v5

    .line 2538875
    invoke-static {v0, v4, v5}, LX/I8k;->f(LX/I8k;II)LX/I8N;

    move-result-object v6

    .line 2538876
    if-nez v6, :cond_a

    .line 2538877
    :cond_5
    :goto_3
    iget-object v0, p0, LX/I7M;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aQ:LX/IBq;

    invoke-virtual {v0, p1, p2, p3}, LX/1OX;->a(Landroid/support/v7/widget/RecyclerView;II)V

    .line 2538878
    :cond_6
    return-void

    .line 2538879
    :cond_7
    iget-object v0, p0, LX/I7M;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/events/permalink/EventPermalinkFragment;->ak:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v0, v0, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 2538880
    iget-object v1, v0, Lcom/facebook/events/common/EventActionContext;->e:Lcom/facebook/events/common/ActionSource;

    move-object v0, v1

    .line 2538881
    invoke-virtual {v0}, Lcom/facebook/events/common/ActionSource;->getParamValue()I

    move-result v0

    goto/16 :goto_0

    .line 2538882
    :cond_8
    iget-object v1, p0, LX/I7M;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    iget-object v1, v1, Lcom/facebook/events/permalink/EventPermalinkFragment;->ak:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v1, v1, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 2538883
    iget-object v2, v1, Lcom/facebook/events/common/EventActionContext;->f:Lcom/facebook/events/common/ActionSource;

    move-object v1, v2

    .line 2538884
    invoke-virtual {v1}, Lcom/facebook/events/common/ActionSource;->getParamValue()I

    move-result v1

    goto/16 :goto_1

    :cond_9
    aget-object v4, v3, v1

    sget-object v5, LX/IBr;->EVENT:LX/IBr;

    if-ne v4, v5, :cond_3

    aget-object v3, v3, v2

    sget-object v4, LX/IBr;->ACTIVITY:LX/IBr;

    if-ne v3, v4, :cond_3

    move v1, v2

    goto :goto_2

    .line 2538885
    :cond_a
    if-lez p3, :cond_b

    iget-object v7, v0, LX/I8k;->F:LX/IBq;

    sget-object v8, LX/IBr;->ABOUT:LX/IBr;

    invoke-virtual {v7, v8}, LX/IBq;->b(LX/IBr;)Z

    move-result v7

    if-eqz v7, :cond_b

    iget-object v7, v0, LX/I8k;->H:LX/2ja;

    if-eqz v7, :cond_b

    .line 2538886
    invoke-virtual {v6, v4, v5}, LX/I8N;->a(II)Landroid/util/Pair;

    move-result-object v7

    .line 2538887
    if-eqz v7, :cond_b

    .line 2538888
    iget-object v8, v0, LX/I8k;->H:LX/2ja;

    iget-object v4, v0, LX/I8k;->t:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v10

    iget-object v4, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v9

    iget-object v4, v7, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 2538889
    invoke-static {v8, v10, v11}, LX/2ja;->f(LX/2ja;J)V

    .line 2538890
    invoke-static {v8, v9, v4}, LX/2ja;->a(LX/2ja;II)V

    .line 2538891
    :cond_b
    if-lez p3, :cond_c

    iget-object v4, v0, LX/I8k;->F:LX/IBq;

    sget-object v7, LX/IBr;->ACTIVITY:LX/IBr;

    invoke-virtual {v4, v7}, LX/IBq;->b(LX/IBr;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 2538892
    iget-object v4, v0, LX/I8k;->k:LX/I8V;

    invoke-static {v0, v4}, LX/I8k;->a(LX/I8k;Ljava/lang/Object;)LX/I8N;

    move-result-object v4

    .line 2538893
    if-eqz v4, :cond_c

    .line 2538894
    iget-object v7, v0, LX/I8k;->k:LX/I8V;

    invoke-virtual {v7}, LX/I8V;->getCount()I

    move-result v7

    iget v4, v4, LX/I8N;->b:I

    add-int/2addr v4, v7

    .line 2538895
    sub-int/2addr v4, v5

    const/4 v7, 0x4

    if-ge v4, v7, :cond_c

    .line 2538896
    iget-object v4, v0, LX/I8k;->L:LX/I7H;

    iget-object v7, v0, LX/I8k;->N:Ljava/lang/String;

    iget-boolean v8, v0, LX/I8k;->O:Z

    invoke-virtual {v4, v7, v8}, LX/I7H;->a(Ljava/lang/String;Z)V

    .line 2538897
    :cond_c
    iget-object v4, v0, LX/I8k;->q:LX/E93;

    invoke-virtual {v4}, LX/1OM;->ij_()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    .line 2538898
    if-ltz v4, :cond_5

    .line 2538899
    invoke-virtual {v6, v5}, LX/I8N;->a(I)I

    move-result v5

    invoke-static {v5, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 2538900
    iget-object v5, v0, LX/I8k;->q:LX/E93;

    invoke-virtual {v5}, LX/E8m;->f()I

    move-result v5

    .line 2538901
    iget-object v6, v0, LX/I8k;->q:LX/E93;

    sub-int/2addr v4, v5

    invoke-virtual {v6, v4}, LX/E8m;->e(I)I

    move-result v4

    .line 2538902
    iget-object v5, v0, LX/I8k;->q:LX/E93;

    iget-object v6, v0, LX/I8k;->q:LX/E93;

    invoke-virtual {v6}, LX/E8m;->d()I

    move-result v6

    invoke-virtual {v5, v6}, LX/E8m;->e(I)I

    move-result v5

    .line 2538903
    add-int/lit8 v5, v5, -0x2

    if-lt v4, v5, :cond_5

    .line 2538904
    iget-object v4, v0, LX/I8k;->q:LX/E93;

    invoke-virtual {v4}, LX/E8m;->h()V

    goto/16 :goto_3
.end method
