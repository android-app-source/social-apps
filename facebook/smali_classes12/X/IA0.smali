.class public final LX/IA0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:LX/Blc;

.field public final synthetic b:LX/IA1;


# direct methods
.method public constructor <init>(LX/IA1;LX/Blc;)V
    .locals 0

    .prologue
    .line 2544369
    iput-object p1, p0, LX/IA0;->b:LX/IA1;

    iput-object p2, p0, LX/IA0;->a:LX/Blc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 7

    .prologue
    .line 2544370
    iget-object v0, p0, LX/IA0;->b:LX/IA1;

    iget-object v0, v0, LX/IA1;->b:LX/IA4;

    invoke-static {v0}, LX/IA4;->j(LX/IA4;)V

    .line 2544371
    iget-object v0, p0, LX/IA0;->b:LX/IA1;

    iget-object v0, v0, LX/IA1;->b:LX/IA4;

    iget-object v1, p0, LX/IA0;->a:LX/Blc;

    invoke-static {v0, v1}, LX/IA4;->a$redex0(LX/IA4;LX/Blc;)V

    .line 2544372
    iget-object v0, p0, LX/IA0;->b:LX/IA1;

    iget-object v0, v0, LX/IA1;->b:LX/IA4;

    iget-object v0, v0, LX/IA4;->A:LX/I9f;

    iget-object v1, p0, LX/IA0;->b:LX/IA1;

    iget-object v1, v1, LX/IA1;->b:LX/IA4;

    iget-object v1, v1, LX/IA4;->v:Lcom/facebook/events/model/EventUser;

    iget-object v2, p0, LX/IA0;->a:LX/Blc;

    .line 2544373
    new-instance v3, LX/4EI;

    invoke-direct {v3}, LX/4EI;-><init>()V

    .line 2544374
    new-instance v4, LX/4EG;

    invoke-direct {v4}, LX/4EG;-><init>()V

    .line 2544375
    iget-object v5, v0, LX/I9f;->c:Lcom/facebook/events/common/EventActionContext;

    .line 2544376
    iget-object v6, v5, Lcom/facebook/events/common/EventActionContext;->f:Lcom/facebook/events/common/ActionSource;

    move-object v5, v6

    .line 2544377
    invoke-virtual {v5}, Lcom/facebook/events/common/ActionSource;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    .line 2544378
    new-instance v5, LX/4EG;

    invoke-direct {v5}, LX/4EG;-><init>()V

    .line 2544379
    iget-object v6, v0, LX/I9f;->c:Lcom/facebook/events/common/EventActionContext;

    .line 2544380
    iget-object p1, v6, Lcom/facebook/events/common/EventActionContext;->e:Lcom/facebook/events/common/ActionSource;

    move-object v6, p1

    .line 2544381
    invoke-virtual {v6}, Lcom/facebook/events/common/ActionSource;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    .line 2544382
    iget-object v6, v0, LX/I9f;->e:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v6}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/4EG;->b(Ljava/lang/String;)LX/4EG;

    .line 2544383
    new-instance v6, LX/4EL;

    invoke-direct {v6}, LX/4EL;-><init>()V

    .line 2544384
    invoke-static {v4, v5}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    invoke-virtual {v6, v4}, LX/4EL;->a(Ljava/util/List;)LX/4EL;

    .line 2544385
    move-object v4, v6

    .line 2544386
    const-string v5, "context"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 2544387
    move-object v3, v3

    .line 2544388
    iget-object v4, v0, LX/I9f;->a:Ljava/lang/String;

    .line 2544389
    const-string v5, "event_id"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2544390
    move-object v3, v3

    .line 2544391
    iget-object v4, v1, Lcom/facebook/events/model/EventUser;->b:Ljava/lang/String;

    move-object v4, v4

    .line 2544392
    const-string v5, "target_id"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2544393
    move-object v3, v3

    .line 2544394
    invoke-static {v2}, LX/I9f;->a(LX/Blc;)Ljava/lang/String;

    move-result-object v4

    .line 2544395
    const-string v5, "guest_status"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2544396
    move-object v3, v3

    .line 2544397
    invoke-static {v0, v2}, LX/I9f;->b(LX/I9f;LX/Blc;)Lcom/facebook/events/graphql/EventsMutationsModels$EventGuestCountsMutationFragmentModel;

    move-result-object v4

    .line 2544398
    new-instance v5, LX/7uD;

    invoke-direct {v5}, LX/7uD;-><init>()V

    move-object v5, v5

    .line 2544399
    const-string v6, "input"

    invoke-virtual {v5, v6, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2544400
    invoke-static {v5}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v3

    invoke-virtual {v3, v4}, LX/399;->a(LX/0jT;)LX/399;

    move-result-object v3

    .line 2544401
    iget-object v4, v0, LX/I9f;->g:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 2544402
    iget-object v4, v0, LX/I9f;->f:LX/1Ck;

    .line 2544403
    iget-object v5, v1, Lcom/facebook/events/model/EventUser;->b:Ljava/lang/String;

    move-object v5, v5

    .line 2544404
    new-instance v6, Ljava/lang/StringBuilder;

    const-string p1, "tasks-adminRsvpEvent:"

    invoke-direct {v6, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object p1, v0, LX/I9f;->a:Ljava/lang/String;

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string p1, ":"

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string p1, ":"

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, LX/Blc;->name()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object v5, v6

    .line 2544405
    new-instance v6, LX/I9c;

    invoke-direct {v6, v0}, LX/I9c;-><init>(LX/I9f;)V

    invoke-virtual {v4, v5, v3, v6}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2544406
    iget-object v0, p0, LX/IA0;->b:LX/IA1;

    iget-object v0, v0, LX/IA1;->b:LX/IA4;

    iget-object v0, v0, LX/IA4;->y:Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;

    iget-object v1, p0, LX/IA0;->b:LX/IA1;

    iget-object v1, v1, LX/IA1;->b:LX/IA4;

    iget-object v1, v1, LX/IA4;->v:Lcom/facebook/events/model/EventUser;

    iget-object v2, p0, LX/IA0;->a:LX/Blc;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->a(Lcom/facebook/events/model/EventUser;LX/Blc;)V

    .line 2544407
    const/4 v0, 0x1

    return v0
.end method
