.class public LX/J7V;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/quickinvite/protocol/methods/SendInviteMethod$Params;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2650944
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 7

    .prologue
    .line 2650911
    check-cast p1, Lcom/facebook/quickinvite/protocol/methods/SendInviteMethod$Params;

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2650912
    iget-object v0, p1, Lcom/facebook/quickinvite/protocol/methods/SendInviteMethod$Params;->a:LX/J7U;

    const-string v1, "Product required"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2650913
    iget-object v0, p1, Lcom/facebook/quickinvite/protocol/methods/SendInviteMethod$Params;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/facebook/quickinvite/protocol/methods/SendInviteMethod$Params;->e:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/facebook/quickinvite/protocol/methods/SendInviteMethod$Params;->d:Ljava/lang/String;

    if-eqz v0, :cond_5

    :cond_0
    move v0, v3

    :goto_0
    const-string v1, "Recipient required"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2650914
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 2650915
    iget-object v0, p1, Lcom/facebook/quickinvite/protocol/methods/SendInviteMethod$Params;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2650916
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "recipient_id"

    iget-object v5, p1, Lcom/facebook/quickinvite/protocol/methods/SendInviteMethod$Params;->b:Ljava/lang/String;

    invoke-direct {v0, v1, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2650917
    :cond_1
    iget-object v0, p1, Lcom/facebook/quickinvite/protocol/methods/SendInviteMethod$Params;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 2650918
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "message"

    iget-object v5, p1, Lcom/facebook/quickinvite/protocol/methods/SendInviteMethod$Params;->c:Ljava/lang/String;

    invoke-direct {v0, v1, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2650919
    :cond_2
    iget-object v0, p1, Lcom/facebook/quickinvite/protocol/methods/SendInviteMethod$Params;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 2650920
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "phone"

    iget-object v5, p1, Lcom/facebook/quickinvite/protocol/methods/SendInviteMethod$Params;->d:Ljava/lang/String;

    invoke-direct {v0, v1, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2650921
    :cond_3
    iget-object v0, p1, Lcom/facebook/quickinvite/protocol/methods/SendInviteMethod$Params;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 2650922
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "email"

    iget-object v5, p1, Lcom/facebook/quickinvite/protocol/methods/SendInviteMethod$Params;->e:Ljava/lang/String;

    invoke-direct {v0, v1, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2650923
    :cond_4
    iget-object v0, p1, Lcom/facebook/quickinvite/protocol/methods/SendInviteMethod$Params;->f:LX/0P1;

    if-eqz v0, :cond_7

    iget-object v0, p1, Lcom/facebook/quickinvite/protocol/methods/SendInviteMethod$Params;->f:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 2650924
    new-instance v5, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v5, v0}, LX/0m9;-><init>(LX/0mC;)V

    .line 2650925
    iget-object v0, p1, Lcom/facebook/quickinvite/protocol/methods/SendInviteMethod$Params;->f:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2650926
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v1, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    goto :goto_1

    :cond_5
    move v0, v2

    .line 2650927
    goto/16 :goto_0

    .line 2650928
    :cond_6
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "context"

    invoke-virtual {v5}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v1, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2650929
    :cond_7
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    const-string v1, "graphQuickInviteSendInvite"

    .line 2650930
    iput-object v1, v0, LX/14O;->b:Ljava/lang/String;

    .line 2650931
    move-object v0, v0

    .line 2650932
    const-string v1, "POST"

    .line 2650933
    iput-object v1, v0, LX/14O;->c:Ljava/lang/String;

    .line 2650934
    move-object v0, v0

    .line 2650935
    const-string v1, "%s/invites"

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v5, p1, Lcom/facebook/quickinvite/protocol/methods/SendInviteMethod$Params;->a:LX/J7U;

    iget-object v5, v5, LX/J7U;->appId:Ljava/lang/String;

    invoke-static {v5}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v2

    invoke-static {v1, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2650936
    iput-object v1, v0, LX/14O;->d:Ljava/lang/String;

    .line 2650937
    move-object v0, v0

    .line 2650938
    iput-object v4, v0, LX/14O;->g:Ljava/util/List;

    .line 2650939
    move-object v0, v0

    .line 2650940
    sget-object v1, LX/14S;->JSONPARSER:LX/14S;

    .line 2650941
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2650942
    move-object v0, v0

    .line 2650943
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2650909
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2650910
    const/4 v0, 0x0

    return-object v0
.end method
