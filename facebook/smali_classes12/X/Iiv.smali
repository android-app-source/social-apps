.class public final enum LX/Iiv;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Iiv;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Iiv;

.field public static final enum GROUP_COMMERCE:LX/Iiv;

.field public static final enum ORION_C2C_THREAD_BUYER_SEND:LX/Iiv;

.field public static final enum ORION_C2C_THREAD_SELLER_SEND:LX/Iiv;

.field public static final enum ORION_SEND:LX/Iiv;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2605159
    new-instance v0, LX/Iiv;

    const-string v1, "GROUP_COMMERCE"

    invoke-direct {v0, v1, v2}, LX/Iiv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Iiv;->GROUP_COMMERCE:LX/Iiv;

    .line 2605160
    new-instance v0, LX/Iiv;

    const-string v1, "ORION_SEND"

    invoke-direct {v0, v1, v3}, LX/Iiv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Iiv;->ORION_SEND:LX/Iiv;

    .line 2605161
    new-instance v0, LX/Iiv;

    const-string v1, "ORION_C2C_THREAD_BUYER_SEND"

    invoke-direct {v0, v1, v4}, LX/Iiv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Iiv;->ORION_C2C_THREAD_BUYER_SEND:LX/Iiv;

    .line 2605162
    new-instance v0, LX/Iiv;

    const-string v1, "ORION_C2C_THREAD_SELLER_SEND"

    invoke-direct {v0, v1, v5}, LX/Iiv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Iiv;->ORION_C2C_THREAD_SELLER_SEND:LX/Iiv;

    .line 2605163
    const/4 v0, 0x4

    new-array v0, v0, [LX/Iiv;

    sget-object v1, LX/Iiv;->GROUP_COMMERCE:LX/Iiv;

    aput-object v1, v0, v2

    sget-object v1, LX/Iiv;->ORION_SEND:LX/Iiv;

    aput-object v1, v0, v3

    sget-object v1, LX/Iiv;->ORION_C2C_THREAD_BUYER_SEND:LX/Iiv;

    aput-object v1, v0, v4

    sget-object v1, LX/Iiv;->ORION_C2C_THREAD_SELLER_SEND:LX/Iiv;

    aput-object v1, v0, v5

    sput-object v0, LX/Iiv;->$VALUES:[LX/Iiv;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2605164
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Iiv;
    .locals 1

    .prologue
    .line 2605165
    const-class v0, LX/Iiv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Iiv;

    return-object v0
.end method

.method public static values()[LX/Iiv;
    .locals 1

    .prologue
    .line 2605166
    sget-object v0, LX/Iiv;->$VALUES:[LX/Iiv;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Iiv;

    return-object v0
.end method
