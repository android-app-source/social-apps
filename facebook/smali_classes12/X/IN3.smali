.class public final LX/IN3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel;",
        ">;",
        "Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsInterfaces$CommunityGroupsSearchQuery;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/search/api/GraphSearchQuery;

.field public final synthetic b:LX/IN4;


# direct methods
.method public constructor <init>(LX/IN4;Lcom/facebook/search/api/GraphSearchQuery;)V
    .locals 0

    .prologue
    .line 2570512
    iput-object p1, p0, LX/IN3;->b:LX/IN4;

    iput-object p2, p0, LX/IN3;->a:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2570513
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2570514
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2570515
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2570516
    if-eqz v0, :cond_0

    .line 2570517
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2570518
    check-cast v0, Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel;

    invoke-virtual {v0}, Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel;->j()Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel$CombinedResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel$CombinedResultsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2570519
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2570520
    check-cast v0, Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel;

    invoke-virtual {v0}, Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel;->j()Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel$CombinedResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel$CombinedResultsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2570521
    iget-object v1, p0, LX/IN3;->b:LX/IN4;

    .line 2570522
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2570523
    check-cast v0, Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel;

    invoke-virtual {v0}, Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel;->j()Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel$CombinedResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel$CombinedResultsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2570524
    iput-object v0, v1, LX/IN4;->d:Ljava/lang/String;

    .line 2570525
    :cond_0
    iget-object v0, p0, LX/IN3;->b:LX/IN4;

    iget-object v1, p0, LX/IN3;->a:Lcom/facebook/search/api/GraphSearchQuery;

    .line 2570526
    iput-object v1, v0, LX/IN4;->e:Lcom/facebook/search/api/GraphSearchQuery;

    .line 2570527
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2570528
    check-cast v0, Lcom/facebook/groups/community/protocol/FetchCommunityGroupsSearchResultsModels$CommunityGroupsSearchQueryModel;

    return-object v0
.end method
