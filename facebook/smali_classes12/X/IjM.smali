.class public final LX/IjM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/IjO;


# direct methods
.method public constructor <init>(LX/IjO;)V
    .locals 0

    .prologue
    .line 2605669
    iput-object p1, p0, LX/IjM;->a:LX/IjO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x3d88587f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2605670
    iget-object v1, p0, LX/IjM;->a:LX/IjO;

    iget-object v1, v1, LX/IjO;->g:LX/6qh;

    if-eqz v1, :cond_0

    .line 2605671
    iget-object v1, p0, LX/IjM;->a:LX/IjO;

    iget-object v1, v1, LX/IjO;->e:Lcom/facebook/payments/p2p/model/PaymentCard;

    invoke-virtual {v1}, Lcom/facebook/payments/p2p/model/PaymentCard;->a()Ljava/lang/String;

    move-result-object v1

    .line 2605672
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2605673
    const-string v4, "extra_mutation"

    const-string p1, "action_set_primary"

    invoke-virtual {v2, v4, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2605674
    const-string v4, "payment_card_id"

    invoke-virtual {v2, v4, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2605675
    new-instance v4, LX/73T;

    sget-object p1, LX/73S;->MUTATION:LX/73S;

    invoke-direct {v4, p1, v2}, LX/73T;-><init>(LX/73S;Landroid/os/Bundle;)V

    move-object v1, v4

    .line 2605676
    iget-object v2, p0, LX/IjM;->a:LX/IjO;

    iget-object v2, v2, LX/IjO;->g:LX/6qh;

    invoke-virtual {v2, v1}, LX/6qh;->a(LX/73T;)V

    .line 2605677
    :cond_0
    const v1, -0x6cbe728b

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
