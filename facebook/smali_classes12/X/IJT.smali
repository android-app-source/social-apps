.class public final LX/IJT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;)V
    .locals 0

    .prologue
    .line 2563543
    iput-object p1, p0, LX/IJT;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x2ee

    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x3d639f06

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2563544
    iget-object v1, p0, LX/IJT;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->h:LX/IID;

    const-string v2, "location_button"

    invoke-virtual {v1, v2}, LX/IID;->g(Ljava/lang/String;)V

    .line 2563545
    iget-object v1, p0, LX/IJT;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;

    invoke-static {v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->p(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;)LX/6al;

    move-result-object v1

    invoke-virtual {v1}, LX/6al;->d()Landroid/location/Location;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/location/ImmutableLocation;->b(Landroid/location/Location;)Lcom/facebook/location/ImmutableLocation;

    move-result-object v1

    .line 2563546
    if-nez v1, :cond_0

    .line 2563547
    iget-object v1, p0, LX/IJT;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->f:LX/0kL;

    new-instance v2, LX/27k;

    const v3, 0x7f083853

    invoke-direct {v2, v3}, LX/27k;-><init>(I)V

    invoke-virtual {v1, v2}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2563548
    const v1, 0x786b285b

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2563549
    :goto_0
    return-void

    .line 2563550
    :cond_0
    iget-object v2, p0, LX/IJT;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;

    invoke-static {v2, v8, v9}, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->a$redex0(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;J)V

    .line 2563551
    iget-object v2, p0, LX/IJT;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;

    new-instance v3, Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {v1}, Lcom/facebook/location/ImmutableLocation;->a()D

    move-result-wide v4

    invoke-virtual {v1}, Lcom/facebook/location/ImmutableLocation;->b()D

    move-result-wide v6

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    const/4 v1, 0x0

    invoke-static {v2, v3, v8, v9, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->a$redex0(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;Lcom/facebook/android/maps/model/LatLng;JLjava/lang/Runnable;)V

    .line 2563552
    const v1, 0x5a38e82d

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0
.end method
