.class public LX/IGk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/friendsnearby/server/FriendsNearbyInviteParams;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2556505
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2556506
    return-void
.end method

.method private static a(Ljava/util/List;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 2556507
    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-virtual {v0}, LX/0mC;->b()LX/162;

    move-result-object v1

    .line 2556508
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2556509
    invoke-static {v0}, LX/0mC;->a(Ljava/lang/String;)LX/0mD;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/162;->a(LX/0lF;)LX/162;

    goto :goto_0

    .line 2556510
    :cond_0
    invoke-virtual {v1}, LX/162;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 5

    .prologue
    .line 2556511
    check-cast p1, Lcom/facebook/friendsnearby/server/FriendsNearbyInviteParams;

    .line 2556512
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 2556513
    const-string v1, "%s/info_requests"

    iget-object v2, p1, Lcom/facebook/friendsnearby/server/FriendsNearbyInviteParams;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2556514
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "field_types"

    iget-object v4, p1, Lcom/facebook/friendsnearby/server/FriendsNearbyInviteParams;->c:LX/0Px;

    invoke-static {v4}, LX/IGk;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2556515
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "additional_recipients"

    iget-object v4, p1, Lcom/facebook/friendsnearby/server/FriendsNearbyInviteParams;->b:LX/0Px;

    invoke-static {v4}, LX/IGk;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2556516
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v2

    const-string v3, "FRIENDS_NEARBY_INVITE"

    .line 2556517
    iput-object v3, v2, LX/14O;->b:Ljava/lang/String;

    .line 2556518
    move-object v2, v2

    .line 2556519
    const-string v3, "POST"

    .line 2556520
    iput-object v3, v2, LX/14O;->c:Ljava/lang/String;

    .line 2556521
    move-object v2, v2

    .line 2556522
    iput-object v1, v2, LX/14O;->d:Ljava/lang/String;

    .line 2556523
    move-object v1, v2

    .line 2556524
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 2556525
    move-object v0, v1

    .line 2556526
    sget-object v1, LX/14S;->STRING:LX/14S;

    .line 2556527
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2556528
    move-object v0, v0

    .line 2556529
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2556530
    invoke-virtual {p2}, LX/1pN;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
