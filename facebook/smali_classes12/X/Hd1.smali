.class public LX/Hd1;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "LX/Hcr;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/2U5;

.field public final c:LX/0tX;

.field public final d:LX/1My;

.field public final e:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(LX/0Px;LX/2U5;LX/0tX;LX/1My;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p1    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "LX/Hcr;",
            ">;",
            "LX/2U5;",
            "LX/0tX;",
            "LX/1My;",
            "Ljava/util/concurrent/Executor;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2487773
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2487774
    iput-object p1, p0, LX/Hd1;->a:LX/0Px;

    .line 2487775
    iput-object p2, p0, LX/Hd1;->b:LX/2U5;

    .line 2487776
    iput-object p3, p0, LX/Hd1;->c:LX/0tX;

    .line 2487777
    iput-object p4, p0, LX/Hd1;->d:LX/1My;

    .line 2487778
    iput-object p5, p0, LX/Hd1;->e:Ljava/util/concurrent/Executor;

    .line 2487779
    return-void
.end method

.method public static c(LX/Hd1;Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;)V
    .locals 3

    .prologue
    .line 2487780
    iget-object v0, p0, LX/Hd1;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/Hd1;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hcr;

    .line 2487781
    invoke-virtual {v0, p1}, LX/Hcr;->b(Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;)V

    .line 2487782
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2487783
    :cond_0
    return-void
.end method
