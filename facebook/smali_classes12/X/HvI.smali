.class public final LX/HvI;
.super LX/1Mt;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;

.field public final synthetic b:Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;)V
    .locals 0

    .prologue
    .line 2518823
    iput-object p1, p0, LX/HvI;->b:Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;

    iput-object p2, p0, LX/HvI;->a:Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;

    invoke-direct {p0}, LX/1Mt;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2518816
    iget-object v0, p0, LX/HvI;->b:Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;

    iget-object v0, v0, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;->g:LX/03V;

    const-string v1, "composer_session_save_failed"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2518817
    iget-object v0, p0, LX/HvI;->b:Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;->b(Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;)V

    .line 2518818
    return-void
.end method

.method public final onSuccessfulResult(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 2

    .prologue
    .line 2518819
    iget-object v0, p0, LX/HvI;->b:Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;

    iget-boolean v0, v0, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;->h:Z

    if-nez v0, :cond_0

    .line 2518820
    iget-object v0, p0, LX/HvI;->b:Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;

    iget-object v1, p0, LX/HvI;->a:Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;

    invoke-static {v0, v1}, Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;->b(Lcom/facebook/composer/system/savedsession/memsync/ComposerSavedSessionStore;Lcom/facebook/composer/system/savedsession/model/ComposerSavedSession;)V

    .line 2518821
    :cond_0
    return-void
.end method

.method public final bridge synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2518822
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    invoke-virtual {p0, p1}, LX/HvI;->onSuccessfulResult(Lcom/facebook/fbservice/service/OperationResult;)V

    return-void
.end method
