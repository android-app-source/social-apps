.class public LX/HhM;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/HhL;

.field public final c:Ljava/lang/String;

.field public final d:I

.field public final e:Ljava/lang/String;

.field public final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final g:I

.field private final h:I

.field public i:J

.field public j:J

.field public k:Ljava/lang/Throwable;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2495453
    const-class v0, LX/HhM;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/HhM;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/HhL;Ljava/lang/String;ILjava/lang/String;Ljava/util/Collection;II)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/HhL;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;II)V"
        }
    .end annotation

    .prologue
    .line 2495454
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2495455
    iput-object p1, p0, LX/HhM;->b:LX/HhL;

    .line 2495456
    iput-object p2, p0, LX/HhM;->c:Ljava/lang/String;

    .line 2495457
    iput p3, p0, LX/HhM;->d:I

    .line 2495458
    iput-object p4, p0, LX/HhM;->e:Ljava/lang/String;

    .line 2495459
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/HhM;->f:Ljava/util/Map;

    .line 2495460
    if-eqz p5, :cond_0

    .line 2495461
    invoke-interface {p5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 2495462
    iget-object v2, p0, LX/HhM;->f:Ljava/util/Map;

    iget-object v3, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2495463
    :cond_0
    iput p6, p0, LX/HhM;->g:I

    .line 2495464
    iput p7, p0, LX/HhM;->h:I

    .line 2495465
    return-void
.end method

.method public static f(LX/HhM;)V
    .locals 4

    .prologue
    .line 2495466
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, LX/HhM;->i:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, LX/HhM;->j:J

    .line 2495467
    return-void
.end method

.method public static i(LX/HhM;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/HhN;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2495468
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2495469
    iget v1, p0, LX/HhM;->h:I

    if-lez v1, :cond_0

    .line 2495470
    new-instance v1, LX/HhO;

    iget v2, p0, LX/HhM;->h:I

    invoke-direct {v1, v2}, LX/HhO;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2495471
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final d()V
    .locals 7

    .prologue
    .line 2495472
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iput-wide v1, p0, LX/HhM;->i:J

    .line 2495473
    const/16 v2, 0x50

    const/16 v1, 0x1bb

    .line 2495474
    sget-object v3, LX/HhK;->a:[I

    iget-object v4, p0, LX/HhM;->b:LX/HhL;

    invoke-virtual {v4}, LX/HhL;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 2495475
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/HhM;->k:Ljava/lang/Throwable;

    .line 2495476
    invoke-static {p0}, LX/HhM;->f(LX/HhM;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2495477
    :goto_1
    return-void

    .line 2495478
    :catch_0
    move-exception v0

    .line 2495479
    iput-object v0, p0, LX/HhM;->k:Ljava/lang/Throwable;

    .line 2495480
    invoke-static {p0}, LX/HhM;->f(LX/HhM;)V

    .line 2495481
    goto :goto_1

    .line 2495482
    :pswitch_0
    iget-object v3, p0, LX/HhM;->c:Ljava/lang/String;

    iget v1, p0, LX/HhM;->d:I

    if-lez v1, :cond_0

    iget v1, p0, LX/HhM;->d:I

    :goto_2
    iget v2, p0, LX/HhM;->g:I

    const-string v4, ""

    invoke-static {v3, v1, v2, v4}, LX/HhJ;->a(Ljava/lang/String;IILjava/lang/String;)Ljava/lang/String;

    goto :goto_0

    :cond_0
    move v1, v2

    goto :goto_2

    .line 2495483
    :pswitch_1
    iget-object v2, p0, LX/HhM;->c:Ljava/lang/String;

    iget v3, p0, LX/HhM;->d:I

    if-lez v3, :cond_1

    iget v1, p0, LX/HhM;->d:I

    :cond_1
    iget v3, p0, LX/HhM;->g:I

    const-string v4, ""

    invoke-static {v2, v1, v3, v4}, LX/HhJ;->a(Ljava/lang/String;IILjava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 2495484
    :pswitch_2
    iget-object v2, p0, LX/HhM;->c:Ljava/lang/String;

    iget v3, p0, LX/HhM;->d:I

    if-lez v3, :cond_2

    iget v1, p0, LX/HhM;->d:I

    :cond_2
    iget v3, p0, LX/HhM;->g:I

    const-string v4, ""

    invoke-static {v2, v1, v3, v4}, LX/HhJ;->b(Ljava/lang/String;IILjava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 2495485
    :pswitch_3
    iget-object v2, p0, LX/HhM;->c:Ljava/lang/String;

    iget v1, p0, LX/HhM;->d:I

    if-lez v1, :cond_3

    iget v1, p0, LX/HhM;->d:I

    :goto_3
    iget v3, p0, LX/HhM;->g:I

    invoke-static {v2, v1, v3}, LX/HhJ;->a(Ljava/lang/String;II)V

    goto :goto_0

    :cond_3
    const/16 v1, 0x22b3

    goto :goto_3

    .line 2495486
    :pswitch_4
    iget-object v2, p0, LX/HhM;->c:Ljava/lang/String;

    iget v3, p0, LX/HhM;->d:I

    if-lez v3, :cond_4

    iget v1, p0, LX/HhM;->d:I

    :cond_4
    iget v3, p0, LX/HhM;->g:I

    invoke-static {v2, v1, v3}, LX/HhJ;->b(Ljava/lang/String;II)V

    goto :goto_0

    .line 2495487
    :pswitch_5
    iget-object v2, p0, LX/HhM;->c:Ljava/lang/String;

    iget v1, p0, LX/HhM;->d:I

    if-lez v1, :cond_5

    iget v1, p0, LX/HhM;->d:I

    :goto_4
    iget v3, p0, LX/HhM;->g:I

    invoke-static {v2, v1, v3}, LX/HhJ;->c(Ljava/lang/String;II)V

    goto :goto_0

    :cond_5
    const/16 v1, 0xd96

    goto :goto_4

    .line 2495488
    :pswitch_6
    iget-object v2, p0, LX/HhM;->c:Ljava/lang/String;

    iget v3, p0, LX/HhM;->d:I

    if-lez v3, :cond_6

    iget v1, p0, LX/HhM;->d:I

    :cond_6
    iget v3, p0, LX/HhM;->g:I

    iget-object v4, p0, LX/HhM;->f:Ljava/util/Map;

    invoke-static {p0}, LX/HhM;->i(LX/HhM;)Ljava/util/List;

    move-result-object v5

    invoke-static {v2, v1, v3, v4, v5}, LX/HhJ;->a(Ljava/lang/String;IILjava/util/Map;Ljava/util/List;)V

    goto/16 :goto_0

    .line 2495489
    :pswitch_7
    invoke-static {p0}, LX/HhM;->i(LX/HhM;)Ljava/util/List;

    move-result-object v6

    .line 2495490
    iget-object v1, p0, LX/HhM;->b:LX/HhL;

    sget-object v3, LX/HhL;->HTTP_AKAMAI:LX/HhL;

    if-ne v1, v3, :cond_7

    .line 2495491
    new-instance v1, LX/HhQ;

    const-string v3, "akamai"

    invoke-direct {v1, v3}, LX/HhQ;-><init>(Ljava/lang/String;)V

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2495492
    :cond_7
    iget-object v1, p0, LX/HhM;->c:Ljava/lang/String;

    iget v3, p0, LX/HhM;->d:I

    if-lez v3, :cond_8

    iget v2, p0, LX/HhM;->d:I

    :cond_8
    iget-object v3, p0, LX/HhM;->e:Ljava/lang/String;

    iget-object v4, p0, LX/HhM;->f:Ljava/util/Map;

    iget v5, p0, LX/HhM;->g:I

    invoke-static/range {v1 .. v6}, LX/HhJ;->a(Ljava/lang/String;ILjava/lang/String;Ljava/util/Map;ILjava/util/List;)I

    goto/16 :goto_0

    .line 2495493
    :pswitch_8
    invoke-static {p0}, LX/HhM;->i(LX/HhM;)Ljava/util/List;

    move-result-object v2

    .line 2495494
    new-instance v1, LX/HhQ;

    const-string v3, "X-FB"

    invoke-direct {v1, v3}, LX/HhQ;-><init>(Ljava/lang/String;)V

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2495495
    iget-object v3, p0, LX/HhM;->c:Ljava/lang/String;

    iget v1, p0, LX/HhM;->d:I

    if-lez v1, :cond_9

    iget v1, p0, LX/HhM;->d:I

    :goto_5
    iget v4, p0, LX/HhM;->g:I

    invoke-static {v3, v1, v4, v2}, LX/HhJ;->a(Ljava/lang/String;IILjava/util/List;)I

    goto/16 :goto_0

    :cond_9
    const/16 v1, 0x1f90

    goto :goto_5

    .line 2495496
    :pswitch_9
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "IorgCarrierToolkitOperaEchoTest"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/util/Random;

    invoke-direct {v3}, Ljava/util/Random;-><init>()V

    const/16 v4, 0x64

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2495497
    iget-object v3, p0, LX/HhM;->f:Ljava/util/Map;

    const-string v4, "x-iorg-test-keyword"

    invoke-interface {v3, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2495498
    invoke-static {p0}, LX/HhM;->i(LX/HhM;)Ljava/util/List;

    move-result-object v6

    .line 2495499
    new-instance v3, LX/HhP;

    invoke-direct {v3, v1}, LX/HhP;-><init>(Ljava/lang/String;)V

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2495500
    iget-object v1, p0, LX/HhM;->c:Ljava/lang/String;

    iget v3, p0, LX/HhM;->d:I

    if-lez v3, :cond_a

    iget v2, p0, LX/HhM;->d:I

    :cond_a
    const-string v3, "/echo"

    iget-object v4, p0, LX/HhM;->f:Ljava/util/Map;

    iget v5, p0, LX/HhM;->g:I

    invoke-static/range {v1 .. v6}, LX/HhJ;->a(Ljava/lang/String;ILjava/lang/String;Ljava/util/Map;ILjava/util/List;)I

    goto/16 :goto_0

    .line 2495501
    :pswitch_a
    iget-object v2, p0, LX/HhM;->c:Ljava/lang/String;

    iget v3, p0, LX/HhM;->d:I

    if-lez v3, :cond_b

    iget v1, p0, LX/HhM;->d:I

    :cond_b
    iget-object v3, p0, LX/HhM;->e:Ljava/lang/String;

    iget-object v4, p0, LX/HhM;->f:Ljava/util/Map;

    iget v5, p0, LX/HhM;->g:I

    invoke-static {p0}, LX/HhM;->i(LX/HhM;)Ljava/util/List;

    invoke-static {v2, v1, v3, v4, v5}, LX/HhJ;->a(Ljava/lang/String;ILjava/lang/String;Ljava/util/Map;I)I

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_a
    .end packed-switch
.end method
