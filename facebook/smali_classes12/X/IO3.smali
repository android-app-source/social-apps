.class public LX/IO3;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/DP5;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/3mF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Or;
    .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/DK3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:Lcom/facebook/widget/text/BetterTextView;

.field public i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public j:Lcom/facebook/widget/text/BetterTextView;

.field public k:Lcom/facebook/fbui/facepile/FacepileView;

.field public l:Lcom/facebook/fig/button/FigButton;

.field public m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;

.field public n:Z

.field public o:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 9

    .prologue
    .line 2571422
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2571423
    const/4 v3, 0x0

    .line 2571424
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, LX/IO3;

    invoke-static {v0}, LX/DP5;->b(LX/0QB;)LX/DP5;

    move-result-object v4

    check-cast v4, LX/DP5;

    invoke-static {v0}, LX/3mF;->b(LX/0QB;)LX/3mF;

    move-result-object v5

    check-cast v5, LX/3mF;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v7

    check-cast v7, LX/0kL;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v8

    check-cast v8, Lcom/facebook/content/SecureContextHelper;

    const/16 p1, 0xc

    invoke-static {v0, p1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p1

    invoke-static {v0}, LX/DK3;->a(LX/0QB;)LX/DK3;

    move-result-object v0

    check-cast v0, LX/DK3;

    iput-object v4, v2, LX/IO3;->a:LX/DP5;

    iput-object v5, v2, LX/IO3;->b:LX/3mF;

    iput-object v6, v2, LX/IO3;->c:Ljava/util/concurrent/ExecutorService;

    iput-object v7, v2, LX/IO3;->d:LX/0kL;

    iput-object v8, v2, LX/IO3;->e:Lcom/facebook/content/SecureContextHelper;

    iput-object p1, v2, LX/IO3;->f:LX/0Or;

    iput-object v0, v2, LX/IO3;->g:LX/DK3;

    .line 2571425
    const v0, 0x7f0302f2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2571426
    invoke-virtual {p0, v3}, LX/IO3;->setOrientation(I)V

    .line 2571427
    invoke-virtual {p0}, LX/IO3;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0045

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0}, LX/IO3;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0046

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p0, v3, v0, v3, v1}, LX/IO3;->setPadding(IIII)V

    .line 2571428
    const v0, 0x7f0d0a24

    invoke-virtual {p0, v0}, LX/IO3;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/IO3;->h:Lcom/facebook/widget/text/BetterTextView;

    .line 2571429
    const v0, 0x7f0d0a25

    invoke-virtual {p0, v0}, LX/IO3;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/IO3;->j:Lcom/facebook/widget/text/BetterTextView;

    .line 2571430
    const v0, 0x7f0d0a23

    invoke-virtual {p0, v0}, LX/IO3;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/IO3;->i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2571431
    const v0, 0x7f0d0a26

    invoke-virtual {p0, v0}, LX/IO3;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/facepile/FacepileView;

    iput-object v0, p0, LX/IO3;->k:Lcom/facebook/fbui/facepile/FacepileView;

    .line 2571432
    const v0, 0x7f0d0a27

    invoke-virtual {p0, v0}, LX/IO3;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    iput-object v0, p0, LX/IO3;->l:Lcom/facebook/fig/button/FigButton;

    .line 2571433
    return-void
.end method

.method public static setJoinButtonState(LX/IO3;Lcom/facebook/graphql/enums/GraphQLGroupJoinState;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2571362
    sget-object v0, LX/IO2;->a:[I

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2571363
    iget-object v0, p0, LX/IO3;->l:Lcom/facebook/fig/button/FigButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    .line 2571364
    :cond_0
    :goto_0
    return-void

    .line 2571365
    :pswitch_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/IO3;->n:Z

    .line 2571366
    iget-object v0, p0, LX/IO3;->l:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0, v3}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    .line 2571367
    iget-object v0, p0, LX/IO3;->l:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {p0}, LX/IO3;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f081be3

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2571368
    :pswitch_1
    iput-boolean v3, p0, LX/IO3;->n:Z

    .line 2571369
    iget-object v0, p0, LX/IO3;->l:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0, v3}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    .line 2571370
    iget-object v0, p0, LX/IO3;->l:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {p0}, LX/IO3;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f081be5

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setText(Ljava/lang/CharSequence;)V

    .line 2571371
    iget-boolean v0, p0, LX/IO3;->o:Z

    if-eqz v0, :cond_0

    .line 2571372
    iget-object v0, p0, LX/IO3;->l:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0, v3}, Lcom/facebook/fig/button/FigButton;->setEnabled(Z)V

    goto :goto_0

    .line 2571373
    :pswitch_2
    iput-boolean v3, p0, LX/IO3;->n:Z

    .line 2571374
    iget-object v0, p0, LX/IO3;->l:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0, v3}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    .line 2571375
    iget-object v0, p0, LX/IO3;->l:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {p0}, LX/IO3;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f081be4

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setText(Ljava/lang/CharSequence;)V

    .line 2571376
    iget-boolean v0, p0, LX/IO3;->o:Z

    if-eqz v0, :cond_0

    .line 2571377
    iget-object v0, p0, LX/IO3;->l:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0, v3}, Lcom/facebook/fig/button/FigButton;->setEnabled(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;Ljava/lang/String;ZLX/IN0;)V
    .locals 8
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/GroupMemberActionSourceValue;
        .end annotation
    .end param
    .param p4    # LX/IN0;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 2571378
    iput-object p1, p0, LX/IO3;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;

    .line 2571379
    iget-object v0, p0, LX/IO3;->h:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2571380
    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-nez v0, :cond_5

    move v0, v1

    .line 2571381
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;->l()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel$GroupFriendMembersModel;

    move-result-object v2

    if-nez v2, :cond_6

    :goto_1
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2571382
    if-lez v1, :cond_9

    .line 2571383
    sget-object v2, LX/DP5;->a:Landroid/content/res/Resources;

    const v3, 0x7f0830c5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2571384
    :goto_2
    move-object v0, v2

    .line 2571385
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2571386
    iget-object v1, p0, LX/IO3;->j:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2571387
    :cond_0
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2571388
    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;->hG_()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    if-nez v2, :cond_a

    :cond_1
    :goto_3
    if-eqz v0, :cond_b

    .line 2571389
    const/4 v0, 0x0

    .line 2571390
    :goto_4
    move-object v0, v0

    .line 2571391
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 2571392
    iget-object v1, p0, LX/IO3;->i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageURI(Landroid/net/Uri;)V

    .line 2571393
    :goto_5
    const/4 v2, 0x0

    .line 2571394
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2571395
    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;->l()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel$GroupFriendMembersModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel$GroupFriendMembersModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v1, v2

    .line 2571396
    :goto_6
    if-ge v1, v5, :cond_3

    .line 2571397
    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel$GroupFriendMembersModel$NodesModel;

    .line 2571398
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel$GroupFriendMembersModel$NodesModel;->b()LX/1vs;

    move-result-object v6

    iget v6, v6, LX/1vs;->b:I

    if-eqz v6, :cond_2

    .line 2571399
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel$GroupFriendMembersModel$NodesModel;->b()LX/1vs;

    move-result-object v0

    iget-object v6, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v6, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2571400
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 2571401
    :cond_3
    move-object v0, v3

    .line 2571402
    iget-object v1, p0, LX/IO3;->k:Lcom/facebook/fbui/facepile/FacepileView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/facepile/FacepileView;->setFaces(Ljava/util/List;)V

    .line 2571403
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2571404
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_7
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2571405
    new-instance v4, LX/6UY;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v4, v1}, LX/6UY;-><init>(Landroid/net/Uri;)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 2571406
    :cond_4
    iget-object v1, p0, LX/IO3;->k:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/facepile/FacepileView;->setFaces(Ljava/util/List;)V

    .line 2571407
    iget-object v3, p0, LX/IO3;->k:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_c

    const/16 v1, 0x8

    :goto_8
    invoke-virtual {v3, v1}, Lcom/facebook/fbui/facepile/FacepileView;->setVisibility(I)V

    .line 2571408
    new-instance v0, LX/INz;

    invoke-direct {v0, p0}, LX/INz;-><init>(LX/IO3;)V

    invoke-virtual {p0, v0}, LX/IO3;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2571409
    if-eqz p3, :cond_8

    .line 2571410
    iget-object v0, p0, LX/IO3;->l:Lcom/facebook/fig/button/FigButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    .line 2571411
    :goto_9
    return-void

    .line 2571412
    :cond_5
    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;->k()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2571413
    invoke-virtual {v2, v0, v1}, LX/15i;->j(II)I

    move-result v0

    goto/16 :goto_0

    .line 2571414
    :cond_6
    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;->l()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel$GroupFriendMembersModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel$GroupFriendMembersModel;->b()I

    move-result v1

    goto/16 :goto_1

    .line 2571415
    :cond_7
    iget-object v0, p0, LX/IO3;->i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    new-instance v1, LX/Daf;

    invoke-virtual {p0}, LX/IO3;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, LX/Daf;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_5

    .line 2571416
    :cond_8
    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;->e()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    invoke-static {p0, v0}, LX/IO3;->setJoinButtonState(LX/IO3;Lcom/facebook/graphql/enums/GraphQLGroupJoinState;)V

    .line 2571417
    iget-object v0, p0, LX/IO3;->l:Lcom/facebook/fig/button/FigButton;

    new-instance v1, LX/IO1;

    invoke-direct {v1, p0, p2, p4}, LX/IO1;-><init>(LX/IO3;Ljava/lang/String;LX/IN0;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2571418
    goto :goto_9

    :cond_9
    sget-object v2, LX/DP5;->a:Landroid/content/res/Resources;

    const v3, 0x7f0830c6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_2

    .line 2571419
    :cond_a
    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;->hG_()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    invoke-virtual {v3, v2, v1}, LX/15i;->g(II)I

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    goto/16 :goto_3

    .line 2571420
    :cond_b
    invoke-virtual {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;->hG_()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v2, v0, v1}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {v2, v0, v1}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {v2, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4

    .line 2571421
    :cond_c
    const/4 v1, 0x0

    goto/16 :goto_8
.end method
