.class public abstract LX/Iur;
.super Landroid/widget/LinearLayout;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2627400
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 2627401
    return-void
.end method


# virtual methods
.method public a(LX/HjF;)V
    .locals 0

    .prologue
    .line 2627402
    return-void
.end method

.method public a(LX/Iv7;ZZ)V
    .locals 0

    .prologue
    .line 2627403
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 2627404
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-virtual {p0}, LX/Iur;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 2627405
    const-string v3, "window"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/WindowManager;

    .line 2627406
    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    .line 2627407
    new-instance v4, Landroid/graphics/Point;

    invoke-direct {v4}, Landroid/graphics/Point;-><init>()V

    .line 2627408
    invoke-virtual {v3, v4}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 2627409
    iget v3, v4, Landroid/graphics/Point;->x:I

    int-to-float v3, v3

    const v4, 0x3f4ccccd    # 0.8f

    mul-float/2addr v3, v4

    float-to-int v3, v3

    move v2, v3

    .line 2627410
    int-to-double v2, v2

    const-wide v4, 0x4000e147ae147ae1L    # 2.11

    div-double/2addr v2, v4

    double-to-int v2, v2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2627411
    return-void
.end method

.method public final onMeasure(II)V
    .locals 2

    .prologue
    .line 2627412
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 2627413
    invoke-virtual {p0}, LX/Iur;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, LX/Iur;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/Iur;->setMeasuredDimension(II)V

    .line 2627414
    return-void
.end method
