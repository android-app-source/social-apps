.class public final LX/Igv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/media/mediapicker/VideoItemController;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/media/mediapicker/VideoItemController;)V
    .locals 0

    .prologue
    .line 2601832
    iput-object p1, p0, LX/Igv;->a:Lcom/facebook/messaging/media/mediapicker/VideoItemController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x15c2dd9d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2601833
    iget-object v1, p0, LX/Igv;->a:Lcom/facebook/messaging/media/mediapicker/VideoItemController;

    iget-object v1, v1, Lcom/facebook/messaging/media/mediapicker/VideoItemController;->r:LX/Igm;

    if-eqz v1, :cond_0

    .line 2601834
    iget-object v1, p0, LX/Igv;->a:Lcom/facebook/messaging/media/mediapicker/VideoItemController;

    iget-object v1, v1, Lcom/facebook/messaging/media/mediapicker/VideoItemController;->s:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2601835
    iget-object v1, p0, LX/Igv;->a:Lcom/facebook/messaging/media/mediapicker/VideoItemController;

    iget-object v1, v1, Lcom/facebook/messaging/media/mediapicker/VideoItemController;->r:LX/Igm;

    iget-object v2, p0, LX/Igv;->a:Lcom/facebook/messaging/media/mediapicker/VideoItemController;

    iget-object v2, v2, Lcom/facebook/messaging/media/mediapicker/VideoItemController;->s:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2601836
    iget-object p0, v1, LX/Igm;->a:LX/Ign;

    iget-object p0, p0, LX/Ign;->i:LX/Igc;

    if-eqz p0, :cond_0

    .line 2601837
    iget-object p0, v1, LX/Igm;->a:LX/Ign;

    iget-object p0, p0, LX/Ign;->i:LX/Igc;

    invoke-virtual {p0, v2}, LX/Igc;->a(Lcom/facebook/ui/media/attachments/MediaResource;)V

    .line 2601838
    :cond_0
    const v1, 0x43da95ec

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
