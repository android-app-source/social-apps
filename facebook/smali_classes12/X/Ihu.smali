.class public final LX/Ihu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/39A;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;)V
    .locals 0

    .prologue
    .line 2603085
    iput-object p1, p0, LX/Ihu;->a:Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 2603084
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2603069
    iget-object v0, p0, LX/Ihu;->a:Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;

    .line 2603070
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 2603071
    iget-object p0, v0, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;->v:LX/IgW;

    if-eqz p0, :cond_0

    .line 2603072
    invoke-static {}, Lcom/facebook/ui/media/attachments/MediaResource;->a()LX/5zn;

    move-result-object p0

    iget-object p1, v0, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;->u:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {p0, p1}, LX/5zn;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5zn;

    move-result-object p0

    iget-object p1, v0, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;->u:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2603073
    iput-object p1, p0, LX/5zn;->g:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2603074
    move-object p0, p0

    .line 2603075
    iget-object p1, v0, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;->m:Lcom/facebook/videocodec/trimming/VideoPreviewFragment;

    invoke-virtual {p1}, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->b()I

    move-result p1

    .line 2603076
    iput p1, p0, LX/5zn;->u:I

    .line 2603077
    move-object p0, p0

    .line 2603078
    iget-object p1, v0, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;->m:Lcom/facebook/videocodec/trimming/VideoPreviewFragment;

    invoke-virtual {p1}, Lcom/facebook/videocodec/trimming/VideoPreviewFragment;->c()I

    move-result p1

    .line 2603079
    iput p1, p0, LX/5zn;->v:I

    .line 2603080
    move-object p0, p0

    .line 2603081
    invoke-virtual {p0}, LX/5zn;->G()Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object p0

    .line 2603082
    iget-object p1, v0, Lcom/facebook/messaging/media/picking/MessengerVideoEditDialogFragment;->v:LX/IgW;

    invoke-interface {p1, p0}, LX/IgW;->a(Lcom/facebook/ui/media/attachments/MediaResource;)V

    .line 2603083
    :cond_0
    return-void
.end method
