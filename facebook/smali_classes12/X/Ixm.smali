.class public LX/Ixm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1rs;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1rs",
        "<",
        "Lcom/facebook/pages/launchpoint/components/DiscoverPageItemGraphQLModels$DiscoverPageItemGraphQLModel;",
        "Ljava/lang/Void;",
        "Lcom/facebook/pages/launchpoint/connectioncontroller/PagesLaunchpointDiscoverGraphQLModels$PagesLaunchpointDiscoverGraphQLModel;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/0hB;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2632299
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2632300
    return-void
.end method


# virtual methods
.method public final a(LX/3DR;Ljava/lang/Object;)LX/0gW;
    .locals 3

    .prologue
    .line 2632301
    new-instance v0, LX/Ixn;

    invoke-direct {v0}, LX/Ixn;-><init>()V

    move-object v0, v0

    .line 2632302
    const-string v1, "count"

    .line 2632303
    iget v2, p1, LX/3DR;->e:I

    move v2, v2

    .line 2632304
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "profile_pic_size"

    iget-object v2, p0, LX/Ixm;->a:LX/0hB;

    invoke-virtual {v2}, LX/0hB;->c()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "cursor"

    .line 2632305
    iget-object v2, p1, LX/3DR;->d:Ljava/lang/String;

    move-object v2, v2

    .line 2632306
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;)LX/5Mb;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/pages/launchpoint/connectioncontroller/PagesLaunchpointDiscoverGraphQLModels$PagesLaunchpointDiscoverGraphQLModel;",
            ">;)",
            "LX/5Mb",
            "<",
            "Lcom/facebook/pages/launchpoint/components/DiscoverPageItemGraphQLModels$DiscoverPageItemGraphQLModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2632307
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2632308
    check-cast v0, Lcom/facebook/pages/launchpoint/connectioncontroller/PagesLaunchpointDiscoverGraphQLModels$PagesLaunchpointDiscoverGraphQLModel;

    invoke-virtual {v0}, Lcom/facebook/pages/launchpoint/connectioncontroller/PagesLaunchpointDiscoverGraphQLModels$PagesLaunchpointDiscoverGraphQLModel;->a()Lcom/facebook/pages/launchpoint/connectioncontroller/PagesLaunchpointDiscoverGraphQLModels$PagesLaunchpointDiscoverGraphQLModel$PagesYouMayLikeModel;

    move-result-object v0

    .line 2632309
    invoke-virtual {v0}, Lcom/facebook/pages/launchpoint/connectioncontroller/PagesLaunchpointDiscoverGraphQLModels$PagesLaunchpointDiscoverGraphQLModel$PagesYouMayLikeModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    new-instance v3, LX/5Mb;

    invoke-virtual {v0}, Lcom/facebook/pages/launchpoint/connectioncontroller/PagesLaunchpointDiscoverGraphQLModels$PagesLaunchpointDiscoverGraphQLModel$PagesYouMayLikeModel;->a()LX/0Px;

    move-result-object v0

    invoke-direct {v3, v0, v2, v1}, LX/5Mb;-><init>(LX/0Px;LX/15i;I)V

    return-object v3
.end method
