.class public final LX/IPK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LX/1Zp",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FetchSuggestedGroupsModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0zS;

.field public final synthetic b:LX/IPM;


# direct methods
.method public constructor <init>(LX/IPM;LX/0zS;)V
    .locals 0

    .prologue
    .line 2573978
    iput-object p1, p0, LX/IPK;->b:LX/IPM;

    iput-object p2, p0, LX/IPK;->a:LX/0zS;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2573979
    iget-object v0, p0, LX/IPK;->b:LX/IPM;

    .line 2573980
    new-instance v1, LX/IOK;

    invoke-direct {v1}, LX/IOK;-><init>()V

    move-object v1, v1

    .line 2573981
    iput-object v1, v0, LX/IPM;->m:LX/IOK;

    .line 2573982
    iget-object v1, v0, LX/IPM;->k:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2573983
    iget-object v1, v0, LX/IPM;->m:LX/IOK;

    const-string v2, "after_cursor"

    iget-object v3, v0, LX/IPM;->k:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2573984
    :cond_0
    iget-object v1, v0, LX/IPM;->m:LX/IOK;

    const-string v2, "profile_image_size"

    iget v3, v0, LX/IPM;->q:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2573985
    iget-object v1, v0, LX/IPM;->m:LX/IOK;

    const-string v2, "item_count"

    const-string v3, "7"

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2573986
    iget-object v1, v0, LX/IPM;->m:LX/IOK;

    const-string v2, "member_count"

    const/4 v3, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2573987
    iget-object v1, v0, LX/IPM;->m:LX/IOK;

    const-string v2, "cover_photo_size"

    iget v3, v0, LX/IPM;->q:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2573988
    iget-object v0, p0, LX/IPK;->b:LX/IPM;

    iget-object v0, v0, LX/IPM;->c:LX/0tX;

    iget-object v1, p0, LX/IPK;->b:LX/IPM;

    iget-object v1, v1, LX/IPM;->m:LX/IOK;

    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    iget-object v2, p0, LX/IPK;->a:LX/0zS;

    invoke-virtual {v1, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2573989
    new-instance v1, LX/IPJ;

    invoke-direct {v1, p0}, LX/IPJ;-><init>(LX/IPK;)V

    iget-object v2, p0, LX/IPK;->b:LX/IPM;

    iget-object v2, v2, LX/IPM;->d:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2573990
    return-object v0
.end method
