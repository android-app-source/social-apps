.class public LX/IXc;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;)V
    .locals 0

    .prologue
    .line 2586315
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2586316
    iput-object p1, p0, LX/IXc;->a:Landroid/content/Context;

    .line 2586317
    iput-object p2, p0, LX/IXc;->b:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    .line 2586318
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)LX/Clo;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlInterfaces$RichDocumentSlide;",
            ">;)",
            "Lcom/facebook/richdocument/model/block/v2/RichDocumentBlocks;"
        }
    .end annotation

    .prologue
    .line 2586319
    new-instance v1, LX/I4B;

    iget-object v0, p0, LX/IXc;->a:Landroid/content/Context;

    invoke-direct {v1, v0}, LX/I4B;-><init>(Landroid/content/Context;)V

    .line 2586320
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideModel;

    .line 2586321
    iget-object v3, p0, LX/IXc;->b:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    .line 2586322
    sget-object v4, LX/IXb;->a:[I

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideModel;->eo_()Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 2586323
    iget-object v4, v1, LX/I4B;->e:LX/03V;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, LX/I4B;->H:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".addSlideBlock"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Error attempting to add slide block of type "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideModel;->eo_()Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v5

    invoke-virtual {v5}, LX/0VK;->g()LX/0VG;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/03V;->a(LX/0VG;)V

    .line 2586324
    :goto_1
    goto :goto_0

    .line 2586325
    :cond_0
    invoke-virtual {v1}, LX/I4B;->b()LX/Clo;

    move-result-object v0

    return-object v0

    .line 2586326
    :pswitch_0
    iget-object v4, v1, LX/I4B;->z:Ljava/util/List;

    iget-object v5, v1, LX/I4B;->s:Ljava/lang/String;

    iget-object v6, v1, LX/I4B;->n:LX/0Ot;

    .line 2586327
    new-instance v7, LX/CmG;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideModel;->em_()LX/8Yr;

    move-result-object v8

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideModel;->p()Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    move-result-object v9

    invoke-direct {v7, v8, v9, v6}, LX/CmG;-><init>(LX/8Yr;Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;LX/0Ot;)V

    const/4 v8, 0x1

    .line 2586328
    iput-boolean v8, v7, LX/CmG;->f:Z

    .line 2586329
    move-object v7, v7

    .line 2586330
    iput-object v3, v7, LX/CmG;->c:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    .line 2586331
    move-object v7, v7

    .line 2586332
    iput-object v5, v7, LX/CmG;->g:Ljava/lang/String;

    .line 2586333
    move-object v7, v7

    .line 2586334
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideModel;->e()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v8

    .line 2586335
    iput-object v8, v7, LX/Cm8;->c:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    .line 2586336
    move-object v7, v7

    .line 2586337
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideModel;->n()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;

    move-result-object v8

    .line 2586338
    iput-object v8, v7, LX/Cm8;->d:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;

    .line 2586339
    move-object v7, v7

    .line 2586340
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideModel;->r()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v8

    .line 2586341
    iput-object v8, v7, LX/Cm8;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    .line 2586342
    move-object v7, v7

    .line 2586343
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideModel;->q()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v8

    .line 2586344
    iput-object v8, v7, LX/Cm8;->b:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    .line 2586345
    move-object v7, v7

    .line 2586346
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideModel;->d()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideModel;->b()Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    move-result-object v9

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideModel;->c()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v10

    invoke-virtual {v7, v8, v9, v10}, LX/Cm8;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;)LX/Cm8;

    move-result-object v7

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideModel;->l()Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    move-result-object v8

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideModel;->k()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, LX/Cm8;->a(Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;Lcom/facebook/graphql/model/GraphQLFeedback;)LX/Cm8;

    move-result-object v7

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideModel;->m()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/Cm7;->a(Ljava/lang/String;)LX/Cm7;

    move-result-object v7

    invoke-virtual {v7}, LX/Cm7;->b()LX/Clr;

    move-result-object v7

    check-cast v7, LX/Clw;

    move-object v5, v7

    .line 2586347
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2586348
    :pswitch_1
    iget-object v4, v1, LX/I4B;->z:Ljava/util/List;

    .line 2586349
    new-instance v8, LX/Cmf;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideModel;->j()LX/8Ys;

    move-result-object v9

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideModel;->p()Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    move-result-object v10

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideModel;->s()Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    move-result-object v11

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideModel;->t()Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    move-result-object v12

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideModel;->u()Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    move-result-object v13

    invoke-direct/range {v8 .. v13}, LX/Cmf;-><init>(LX/8Ys;Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;)V

    const/4 v9, 0x1

    .line 2586350
    iput-boolean v9, v8, LX/Cmf;->h:Z

    .line 2586351
    move-object v8, v8

    .line 2586352
    iput-object v3, v8, LX/Cmf;->i:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    .line 2586353
    move-object v8, v8

    .line 2586354
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideModel;->o()LX/8Yr;

    move-result-object v9

    .line 2586355
    iput-object v9, v8, LX/Cmf;->b:LX/8Yr;

    .line 2586356
    move-object v8, v8

    .line 2586357
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideModel;->e()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v9

    .line 2586358
    iput-object v9, v8, LX/Cm8;->c:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    .line 2586359
    move-object v8, v8

    .line 2586360
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideModel;->n()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;

    move-result-object v9

    .line 2586361
    iput-object v9, v8, LX/Cm8;->d:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLocationAnnotationModel;

    .line 2586362
    move-object v8, v8

    .line 2586363
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideModel;->r()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v9

    .line 2586364
    iput-object v9, v8, LX/Cm8;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    .line 2586365
    move-object v8, v8

    .line 2586366
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideModel;->q()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v9

    .line 2586367
    iput-object v9, v8, LX/Cm8;->b:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    .line 2586368
    move-object v8, v8

    .line 2586369
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideModel;->d()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideModel;->b()Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    move-result-object v10

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideModel;->c()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;

    move-result-object v11

    invoke-virtual {v8, v9, v10, v11}, LX/Cm8;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextAnnotationModel;)LX/Cm8;

    move-result-object v8

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideModel;->l()Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    move-result-object v9

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideModel;->k()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, LX/Cm8;->a(Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;Lcom/facebook/graphql/model/GraphQLFeedback;)LX/Cm8;

    move-result-object v8

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSlideModel;->m()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, LX/Cm7;->a(Ljava/lang/String;)LX/Cm7;

    move-result-object v8

    invoke-virtual {v8}, LX/Cm7;->b()LX/Clr;

    move-result-object v8

    check-cast v8, LX/Cm4;

    move-object v5, v8

    .line 2586370
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
