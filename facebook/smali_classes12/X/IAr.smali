.class public final enum LX/IAr;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IAr;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IAr;

.field public static final enum CHILD:LX/IAr;

.field public static final enum HEADER:LX/IAr;

.field public static final enum LOADING:LX/IAr;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2546247
    new-instance v0, LX/IAr;

    const-string v1, "HEADER"

    invoke-direct {v0, v1, v2}, LX/IAr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IAr;->HEADER:LX/IAr;

    .line 2546248
    new-instance v0, LX/IAr;

    const-string v1, "CHILD"

    invoke-direct {v0, v1, v3}, LX/IAr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IAr;->CHILD:LX/IAr;

    .line 2546249
    new-instance v0, LX/IAr;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v4}, LX/IAr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IAr;->LOADING:LX/IAr;

    .line 2546250
    const/4 v0, 0x3

    new-array v0, v0, [LX/IAr;

    sget-object v1, LX/IAr;->HEADER:LX/IAr;

    aput-object v1, v0, v2

    sget-object v1, LX/IAr;->CHILD:LX/IAr;

    aput-object v1, v0, v3

    sget-object v1, LX/IAr;->LOADING:LX/IAr;

    aput-object v1, v0, v4

    sput-object v0, LX/IAr;->$VALUES:[LX/IAr;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2546251
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IAr;
    .locals 1

    .prologue
    .line 2546252
    const-class v0, LX/IAr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IAr;

    return-object v0
.end method

.method public static values()[LX/IAr;
    .locals 1

    .prologue
    .line 2546253
    sget-object v0, LX/IAr;->$VALUES:[LX/IAr;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IAr;

    return-object v0
.end method
