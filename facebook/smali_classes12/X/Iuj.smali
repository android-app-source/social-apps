.class public LX/Iuj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Iua;


# instance fields
.field private final a:LX/297;


# direct methods
.method public constructor <init>(LX/297;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2627234
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2627235
    iput-object p1, p0, LX/Iuj;->a:LX/297;

    .line 2627236
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/service/model/NewMessageResult;)LX/Iuf;
    .locals 2

    .prologue
    .line 2627237
    iget-object v0, p1, Lcom/facebook/messaging/service/model/NewMessageResult;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v0, v0

    .line 2627238
    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2627239
    iget-object v1, p0, LX/Iuj;->a:LX/297;

    invoke-virtual {v1}, LX/297;->a()Lcom/facebook/messaging/model/threads/NotificationSetting;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/model/threads/NotificationSetting;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/Iuj;->a:LX/297;

    invoke-virtual {v1, v0}, LX/297;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/NotificationSetting;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/NotificationSetting;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2627240
    :cond_0
    sget-object v0, LX/Iuf;->SUPPRESS:LX/Iuf;

    .line 2627241
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, LX/Iuf;->BUZZ:LX/Iuf;

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2627242
    const-string v0, "NotifySetRule"

    return-object v0
.end method
