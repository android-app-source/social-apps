.class public final enum LX/J2w;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/J2w;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/J2w;

.field public static final enum ADDITIONAL_INSTRUCTION:LX/J2w;

.field public static final enum IMAGE_VIEW_EXTENSION:LX/J2w;

.field public static final enum NO_EXTENSION_COMPONENT:LX/J2w;

.field public static final enum ORDER_SUMMARY_EXTENSION:LX/J2w;

.field public static final enum OTHER_PARTICIPANT:LX/J2w;

.field public static final enum PRICE_LIST_EXTENSION:LX/J2w;

.field public static final enum PROGRESS_EXTENSION:LX/J2w;

.field public static final enum SINGLE_DIVIDER:LX/J2w;

.field public static final enum SPACED_DOUBLE_DIVIDER:LX/J2w;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2641835
    new-instance v0, LX/J2w;

    const-string v1, "ADDITIONAL_INSTRUCTION"

    invoke-direct {v0, v1, v3}, LX/J2w;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J2w;->ADDITIONAL_INSTRUCTION:LX/J2w;

    .line 2641836
    new-instance v0, LX/J2w;

    const-string v1, "IMAGE_VIEW_EXTENSION"

    invoke-direct {v0, v1, v4}, LX/J2w;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J2w;->IMAGE_VIEW_EXTENSION:LX/J2w;

    .line 2641837
    new-instance v0, LX/J2w;

    const-string v1, "NO_EXTENSION_COMPONENT"

    invoke-direct {v0, v1, v5}, LX/J2w;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J2w;->NO_EXTENSION_COMPONENT:LX/J2w;

    .line 2641838
    new-instance v0, LX/J2w;

    const-string v1, "PRICE_LIST_EXTENSION"

    invoke-direct {v0, v1, v6}, LX/J2w;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J2w;->PRICE_LIST_EXTENSION:LX/J2w;

    .line 2641839
    new-instance v0, LX/J2w;

    const-string v1, "PROGRESS_EXTENSION"

    invoke-direct {v0, v1, v7}, LX/J2w;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J2w;->PROGRESS_EXTENSION:LX/J2w;

    .line 2641840
    new-instance v0, LX/J2w;

    const-string v1, "ORDER_SUMMARY_EXTENSION"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/J2w;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J2w;->ORDER_SUMMARY_EXTENSION:LX/J2w;

    .line 2641841
    new-instance v0, LX/J2w;

    const-string v1, "OTHER_PARTICIPANT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/J2w;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J2w;->OTHER_PARTICIPANT:LX/J2w;

    .line 2641842
    new-instance v0, LX/J2w;

    const-string v1, "SINGLE_DIVIDER"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/J2w;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J2w;->SINGLE_DIVIDER:LX/J2w;

    .line 2641843
    new-instance v0, LX/J2w;

    const-string v1, "SPACED_DOUBLE_DIVIDER"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/J2w;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J2w;->SPACED_DOUBLE_DIVIDER:LX/J2w;

    .line 2641844
    const/16 v0, 0x9

    new-array v0, v0, [LX/J2w;

    sget-object v1, LX/J2w;->ADDITIONAL_INSTRUCTION:LX/J2w;

    aput-object v1, v0, v3

    sget-object v1, LX/J2w;->IMAGE_VIEW_EXTENSION:LX/J2w;

    aput-object v1, v0, v4

    sget-object v1, LX/J2w;->NO_EXTENSION_COMPONENT:LX/J2w;

    aput-object v1, v0, v5

    sget-object v1, LX/J2w;->PRICE_LIST_EXTENSION:LX/J2w;

    aput-object v1, v0, v6

    sget-object v1, LX/J2w;->PROGRESS_EXTENSION:LX/J2w;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/J2w;->ORDER_SUMMARY_EXTENSION:LX/J2w;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/J2w;->OTHER_PARTICIPANT:LX/J2w;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/J2w;->SINGLE_DIVIDER:LX/J2w;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/J2w;->SPACED_DOUBLE_DIVIDER:LX/J2w;

    aput-object v2, v0, v1

    sput-object v0, LX/J2w;->$VALUES:[LX/J2w;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2641834
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/J2w;
    .locals 1

    .prologue
    .line 2641832
    const-class v0, LX/J2w;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/J2w;

    return-object v0
.end method

.method public static values()[LX/J2w;
    .locals 1

    .prologue
    .line 2641833
    sget-object v0, LX/J2w;->$VALUES:[LX/J2w;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/J2w;

    return-object v0
.end method
