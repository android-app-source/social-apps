.class public final enum LX/JSv;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/JSv;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/JSv;

.field public static final enum CIRCLE_TO_SQUARE:LX/JSv;

.field public static final enum SQUARE_TO_CIRCLE:LX/JSv;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2696209
    new-instance v0, LX/JSv;

    const-string v1, "SQUARE_TO_CIRCLE"

    invoke-direct {v0, v1, v2}, LX/JSv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JSv;->SQUARE_TO_CIRCLE:LX/JSv;

    .line 2696210
    new-instance v0, LX/JSv;

    const-string v1, "CIRCLE_TO_SQUARE"

    invoke-direct {v0, v1, v3}, LX/JSv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JSv;->CIRCLE_TO_SQUARE:LX/JSv;

    .line 2696211
    const/4 v0, 0x2

    new-array v0, v0, [LX/JSv;

    sget-object v1, LX/JSv;->SQUARE_TO_CIRCLE:LX/JSv;

    aput-object v1, v0, v2

    sget-object v1, LX/JSv;->CIRCLE_TO_SQUARE:LX/JSv;

    aput-object v1, v0, v3

    sput-object v0, LX/JSv;->$VALUES:[LX/JSv;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2696208
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/JSv;
    .locals 1

    .prologue
    .line 2696206
    const-class v0, LX/JSv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/JSv;

    return-object v0
.end method

.method public static values()[LX/JSv;
    .locals 1

    .prologue
    .line 2696207
    sget-object v0, LX/JSv;->$VALUES:[LX/JSv;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/JSv;

    return-object v0
.end method
