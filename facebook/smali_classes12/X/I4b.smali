.class public LX/I4b;
.super LX/Cod;
.source ""

# interfaces
.implements LX/CnG;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cod",
        "<",
        "Lcom/facebook/events/eventcollections/presenter/EventBlockPresenter;",
        ">;",
        "Lcom/facebook/events/eventcollections/view/impl/block/EventBlockView;"
    }
.end annotation


# instance fields
.field public final a:Lcom/facebook/events/widget/eventcard/EventsCardView;

.field public final b:LX/Blh;

.field public c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/view/View;LX/Blh;)V
    .locals 3

    .prologue
    .line 2534236
    invoke-direct {p0, p1}, LX/Cod;-><init>(Landroid/view/View;)V

    .line 2534237
    iput-object p2, p0, LX/I4b;->b:LX/Blh;

    .line 2534238
    check-cast p1, Lcom/facebook/events/widget/eventcard/EventsCardView;

    iput-object p1, p0, LX/I4b;->a:Lcom/facebook/events/widget/eventcard/EventsCardView;

    .line 2534239
    iget-object v0, p0, LX/I4b;->a:Lcom/facebook/events/widget/eventcard/EventsCardView;

    invoke-virtual {v0}, Lcom/facebook/events/widget/eventcard/EventsCardView;->b()V

    .line 2534240
    iget-object v0, p0, LX/I4b;->a:Lcom/facebook/events/widget/eventcard/EventsCardView;

    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020640

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/widget/eventcard/EventsCardView;->a(Landroid/graphics/drawable/Drawable;)V

    .line 2534241
    iget-object v0, p0, LX/I4b;->a:Lcom/facebook/events/widget/eventcard/EventsCardView;

    new-instance v1, LX/I4a;

    invoke-direct {v1, p0}, LX/I4a;-><init>(LX/I4b;)V

    invoke-virtual {v0, v1}, Lcom/facebook/events/widget/eventcard/EventsCardView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2534242
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/events/widget/eventcard/EventActionButtonView;
    .locals 1

    .prologue
    .line 2534243
    iget-object v0, p0, LX/I4b;->a:Lcom/facebook/events/widget/eventcard/EventsCardView;

    invoke-virtual {v0}, Lcom/facebook/events/widget/eventcard/EventsCardView;->getActionButton()Lcom/facebook/events/widget/eventcard/EventActionButtonView;

    move-result-object v0

    return-object v0
.end method
