.class public final LX/Ih8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/media/picker/FolderItemViewHolder;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/media/picker/FolderItemViewHolder;)V
    .locals 0

    .prologue
    .line 2602245
    iput-object p1, p0, LX/Ih8;->a:Lcom/facebook/messaging/media/picker/FolderItemViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x455cd4fd

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2602246
    iget-object v1, p0, LX/Ih8;->a:Lcom/facebook/messaging/media/picker/FolderItemViewHolder;

    iget-object v1, v1, Lcom/facebook/messaging/media/picker/FolderItemViewHolder;->t:LX/IhY;

    if-eqz v1, :cond_0

    .line 2602247
    iget-object v1, p0, LX/Ih8;->a:Lcom/facebook/messaging/media/picker/FolderItemViewHolder;

    iget-object v1, v1, Lcom/facebook/messaging/media/picker/FolderItemViewHolder;->u:Lcom/facebook/messaging/media/folder/Folder;

    if-eqz v1, :cond_1

    .line 2602248
    iget-object v1, p0, LX/Ih8;->a:Lcom/facebook/messaging/media/picker/FolderItemViewHolder;

    iget-object v1, v1, Lcom/facebook/messaging/media/picker/FolderItemViewHolder;->u:Lcom/facebook/messaging/media/folder/Folder;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2602249
    iget-object v1, p0, LX/Ih8;->a:Lcom/facebook/messaging/media/picker/FolderItemViewHolder;

    iget-object v1, v1, Lcom/facebook/messaging/media/picker/FolderItemViewHolder;->t:LX/IhY;

    iget-object v2, p0, LX/Ih8;->a:Lcom/facebook/messaging/media/picker/FolderItemViewHolder;

    iget-object v2, v2, Lcom/facebook/messaging/media/picker/FolderItemViewHolder;->u:Lcom/facebook/messaging/media/folder/Folder;

    .line 2602250
    iget-object v3, v1, LX/IhY;->a:LX/IhZ;

    iget-object v3, v3, LX/IhZ;->d:LX/IhO;

    if-eqz v3, :cond_0

    .line 2602251
    iget-object v3, v1, LX/IhY;->a:LX/IhZ;

    iget-object v3, v3, LX/IhZ;->d:LX/IhO;

    .line 2602252
    iget-object v4, v3, LX/IhO;->a:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersAlbumsFragment;

    iget-object v4, v4, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersAlbumsFragment;->d:LX/IhR;

    .line 2602253
    iget-object v5, v4, LX/IhR;->a:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;

    iget-object v5, v5, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersContainerFragment;->a:LX/IhJ;

    .line 2602254
    iget-object v6, v5, LX/IhJ;->a:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;

    iget-object p0, v5, LX/IhJ;->a:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;

    iget-object p0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->D:Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;

    iget-object p1, v5, LX/IhJ;->a:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;

    iget-object p1, p1, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->C:Ljava/util/ArrayList;

    .line 2602255
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2602256
    const-string v4, "environment"

    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Parcelable;

    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2602257
    const-string v4, "folder"

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Parcelable;

    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2602258
    const-string v1, "selected"

    invoke-virtual {v3, v1, p1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2602259
    new-instance v1, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;

    invoke-direct {v1}, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;-><init>()V

    .line 2602260
    invoke-virtual {v1, v3}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2602261
    move-object p0, v1

    .line 2602262
    iput-object p0, v6, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->z:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;

    .line 2602263
    iget-object v6, v5, LX/IhJ;->a:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;

    iget-object v6, v6, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->z:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;

    new-instance p0, LX/IhI;

    iget-object p1, v5, LX/IhJ;->a:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;

    invoke-direct {p0, p1}, LX/IhI;-><init>(Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;)V

    .line 2602264
    iput-object p0, v6, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;->h:LX/IhI;

    .line 2602265
    iget-object v6, v5, LX/IhJ;->a:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;

    iget-object v6, v6, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->z:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;

    iget-object p0, v5, LX/IhJ;->a:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;

    iget-object p0, p0, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->u:LX/IhL;

    .line 2602266
    iput-object p0, v6, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;->e:LX/IhK;

    .line 2602267
    iget-object v6, v5, LX/IhJ;->a:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v6

    invoke-virtual {v6}, LX/0gc;->a()LX/0hH;

    move-result-object v6

    const p0, 0x7f0d1b1f

    iget-object p1, v5, LX/IhJ;->a:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;

    iget-object p1, p1, Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersActivity;->z:Lcom/facebook/messaging/media/picker/MediaPickerWithFoldersMediaFragment;

    const-string v1, "album_contents_fragment_tag"

    invoke-virtual {v6, p0, p1, v1}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v6

    invoke-virtual {v6}, LX/0hH;->b()I

    .line 2602268
    :cond_0
    :goto_0
    const v1, -0x79242d89

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2602269
    :cond_1
    goto :goto_0
.end method
