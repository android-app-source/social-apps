.class public LX/I21;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/events/common/EventAnalyticsParams;

.field private b:Landroid/view/View$OnClickListener;

.field private c:Landroid/view/View$OnClickListener;

.field public final d:Landroid/content/Context;

.field public final e:LX/Bie;

.field private final f:LX/6RZ;

.field public final g:LX/I76;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/Bie;LX/6RZ;LX/I76;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2529675
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2529676
    iput-object p1, p0, LX/I21;->d:Landroid/content/Context;

    .line 2529677
    iput-object p2, p0, LX/I21;->e:LX/Bie;

    .line 2529678
    iput-object p3, p0, LX/I21;->f:LX/6RZ;

    .line 2529679
    iput-object p4, p0, LX/I21;->g:LX/I76;

    .line 2529680
    new-instance v0, LX/I1z;

    invoke-direct {v0, p0}, LX/I1z;-><init>(LX/I21;)V

    iput-object v0, p0, LX/I21;->b:Landroid/view/View$OnClickListener;

    .line 2529681
    new-instance v0, LX/I20;

    invoke-direct {v0, p0}, LX/I20;-><init>(LX/I21;)V

    iput-object v0, p0, LX/I21;->c:Landroid/view/View$OnClickListener;

    .line 2529682
    return-void
.end method

.method private a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;)Ljava/lang/String;
    .locals 8
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 2529660
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 2529661
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v3, v1, LX/1vs;->b:I

    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2529662
    if-eqz v3, :cond_1

    .line 2529663
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v2, v3, v7}, LX/15i;->j(II)I

    move-result v4

    invoke-virtual {v2, v3, v6}, LX/15i;->j(II)I

    move-result v5

    invoke-static {v0, v1, v4, v5}, LX/6RS;->a(Ljava/util/Date;Ljava/util/TimeZone;II)Ljava/util/Calendar;

    move-result-object v1

    .line 2529664
    invoke-virtual {v1}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    .line 2529665
    const/4 v4, -0x1

    invoke-virtual {v0, v6, v4}, Ljava/util/Calendar;->roll(II)V

    .line 2529666
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    .line 2529667
    const/4 v5, 0x2

    invoke-virtual {v2, v3, v5}, LX/15i;->j(II)I

    move-result v5

    invoke-virtual {v2, v3, v6}, LX/15i;->j(II)I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v2, v3, v7}, LX/15i;->j(II)I

    move-result v2

    invoke-virtual {v4, v5, v6, v2}, Ljava/util/Calendar;->set(III)V

    .line 2529668
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    invoke-static {v2, v4}, LX/6RS;->a(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v2

    .line 2529669
    if-eqz v2, :cond_0

    .line 2529670
    :goto_0
    iget-object v1, p0, LX/I21;->f:LX/6RZ;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/6RZ;->b(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 2529671
    :goto_1
    return-object v0

    .line 2529672
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move-object v0, v1

    .line 2529673
    goto :goto_0

    .line 2529674
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static b(LX/0QB;)LX/I21;
    .locals 5

    .prologue
    .line 2529645
    new-instance v4, LX/I21;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/Bie;->b(LX/0QB;)LX/Bie;

    move-result-object v1

    check-cast v1, LX/Bie;

    invoke-static {p0}, LX/6RZ;->a(LX/0QB;)LX/6RZ;

    move-result-object v2

    check-cast v2, LX/6RZ;

    invoke-static {p0}, LX/I76;->b(LX/0QB;)LX/I76;

    move-result-object v3

    check-cast v3, LX/I76;

    invoke-direct {v4, v0, v1, v2, v3}, LX/I21;-><init>(Landroid/content/Context;LX/Bie;LX/6RZ;LX/I76;)V

    .line 2529646
    return-object v4
.end method


# virtual methods
.method public final a(Lcom/facebook/fig/listitem/FigListItem;Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;Lcom/facebook/events/common/EventAnalyticsParams;)V
    .locals 3
    .param p2    # Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2529647
    if-nez p2, :cond_0

    .line 2529648
    :goto_0
    return-void

    .line 2529649
    :cond_0
    iput-object p3, p0, LX/I21;->a:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2529650
    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;->m()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/fig/listitem/FigListItem;->setTag(Ljava/lang/Object;)V

    .line 2529651
    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;->p()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 2529652
    iget-object v0, p0, LX/I21;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b15dc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailSize(I)V

    .line 2529653
    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2529654
    invoke-direct {p0, p2}, LX/I21;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/fig/listitem/FigListItem;->setBodyText(Ljava/lang/CharSequence;)V

    .line 2529655
    iget-object v0, p0, LX/I21;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08219e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/fig/listitem/FigListItem;->setActionText(Ljava/lang/CharSequence;)V

    .line 2529656
    iget-object v0, p0, LX/I21;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08219f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/fig/listitem/FigListItem;->setActionContentDescription(Ljava/lang/CharSequence;)V

    .line 2529657
    iget-object v0, p0, LX/I21;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Lcom/facebook/fig/listitem/FigListItem;->setActionOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2529658
    iget-object v0, p0, LX/I21;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Lcom/facebook/fig/listitem/FigListItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2529659
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p1, v0}, Lcom/facebook/fig/listitem/FigListItem;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method
