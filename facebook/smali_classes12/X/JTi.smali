.class public final LX/JTi;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JTj;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

.field public b:Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;

.field public c:LX/JTg;

.field public final synthetic d:LX/JTj;


# direct methods
.method public constructor <init>(LX/JTj;)V
    .locals 1

    .prologue
    .line 2697088
    iput-object p1, p0, LX/JTi;->d:LX/JTj;

    .line 2697089
    move-object v0, p1

    .line 2697090
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2697091
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2697092
    const-string v0, "FriendsLocationsContextComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2697093
    if-ne p0, p1, :cond_1

    .line 2697094
    :cond_0
    :goto_0
    return v0

    .line 2697095
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2697096
    goto :goto_0

    .line 2697097
    :cond_3
    check-cast p1, LX/JTi;

    .line 2697098
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2697099
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2697100
    if-eq v2, v3, :cond_0

    .line 2697101
    iget-object v2, p0, LX/JTi;->a:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JTi;->a:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    iget-object v3, p1, LX/JTi;->a:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2697102
    goto :goto_0

    .line 2697103
    :cond_5
    iget-object v2, p1, LX/JTi;->a:Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;

    if-nez v2, :cond_4

    .line 2697104
    :cond_6
    iget-object v2, p0, LX/JTi;->b:Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/JTi;->b:Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;

    iget-object v3, p1, LX/JTi;->b:Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2697105
    goto :goto_0

    .line 2697106
    :cond_8
    iget-object v2, p1, LX/JTi;->b:Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;

    if-nez v2, :cond_7

    .line 2697107
    :cond_9
    iget-object v2, p0, LX/JTi;->c:LX/JTg;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/JTi;->c:LX/JTg;

    iget-object v3, p1, LX/JTi;->c:LX/JTg;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2697108
    goto :goto_0

    .line 2697109
    :cond_a
    iget-object v2, p1, LX/JTi;->c:LX/JTg;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
