.class public final LX/IV2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;)V
    .locals 0

    .prologue
    .line 2581237
    iput-object p1, p0, LX/IV2;->a:Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x1cd5f853

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2581238
    iget-object v1, p0, LX/IV2;->a:Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;

    .line 2581239
    iget-object v3, v1, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->l:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;

    if-nez v3, :cond_1

    .line 2581240
    :cond_0
    :goto_0
    const v1, 0x47dcc8a8

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2581241
    :cond_1
    iget-object v3, v1, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->l:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;->n()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CAN_JOIN:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-eq v3, v4, :cond_2

    iget-object v3, v1, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->l:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;->n()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-ne v3, v4, :cond_3

    .line 2581242
    :cond_2
    iget-object v3, v1, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->d:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/3mF;

    iget-object v4, v1, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->l:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;->l()Ljava/lang/String;

    move-result-object v4

    const-string v5, "group_mall"

    invoke-virtual {v3, v4, v5}, LX/3mF;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v4, v3

    .line 2581243
    :goto_1
    iget-object v3, v1, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->l:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;

    invoke-static {v3}, LX/9RR;->a(Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;)LX/9RR;

    move-result-object v3

    iget-object v5, v1, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->l:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;

    invoke-virtual {v5}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;->n()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v5

    .line 2581244
    sget-object p0, LX/IV5;->a:[I

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->ordinal()I

    move-result p1

    aget p0, p0, p1

    packed-switch p0, :pswitch_data_0

    .line 2581245
    :goto_2
    move-object v5, v5

    .line 2581246
    iput-object v5, v3, LX/9RR;->g:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 2581247
    move-object v3, v3

    .line 2581248
    invoke-virtual {v3}, LX/9RR;->a()Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;

    move-result-object v3

    .line 2581249
    iget-object v5, v1, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->l:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;

    .line 2581250
    invoke-virtual {v1, v3}, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->a(Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;)V

    .line 2581251
    new-instance p0, LX/IV4;

    invoke-direct {p0, v1, v5}, LX/IV4;-><init>(Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;)V

    iget-object v3, v1, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->e:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/Executor;

    invoke-static {v4, p0, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0

    .line 2581252
    :cond_3
    iget-object v3, v1, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->l:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;->n()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-eq v3, v4, :cond_4

    iget-object v3, v1, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->l:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;

    invoke-virtual {v3}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;->n()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-ne v3, v4, :cond_0

    .line 2581253
    :cond_4
    iget-object v3, v1, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->d:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/3mF;

    iget-object v4, v1, Lcom/facebook/groups/feed/ui/chaining/GroupSuggestionsChainingUnitItem;->l:Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;

    invoke-virtual {v4}, Lcom/facebook/groups/feed/protocol/FetchGroupSuggestionsChainingUnitGraphQLModels$FetchGroupSuggestionsChainingUnitModel$RelatedGroupsModel$NodesModel;->l()Ljava/lang/String;

    move-result-object v4

    const-string v5, "group_mall"

    const-string p0, "ALLOW_READD"

    invoke-virtual {v3, v4, v5, p0}, LX/3mF;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v4, v3

    goto :goto_1

    .line 2581254
    :pswitch_0
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    goto :goto_2

    .line 2581255
    :pswitch_1
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    goto :goto_2

    .line 2581256
    :pswitch_2
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CAN_JOIN:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    goto :goto_2

    .line 2581257
    :pswitch_3
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
