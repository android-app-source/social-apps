.class public final LX/Ian;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Ia2;


# instance fields
.field public final synthetic a:LX/Iao;


# direct methods
.method public constructor <init>(LX/Iao;)V
    .locals 0

    .prologue
    .line 2591291
    iput-object p1, p0, LX/Ian;->a:LX/Iao;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2591292
    iget-object v0, p0, LX/Ian;->a:LX/Iao;

    iget-object v0, v0, LX/Iao;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;

    invoke-static {v0}, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->p(Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;)V

    .line 2591293
    iget-object v0, p0, LX/Ian;->a:LX/Iao;

    iget-object v0, v0, LX/Iao;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;

    iget-object v0, v0, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->f:LX/IZw;

    const-string v1, "error_request_sms_code"

    invoke-virtual {v0, v1}, LX/IZw;->a(Ljava/lang/String;)V

    .line 2591294
    iget-object v0, p0, LX/Ian;->a:LX/Iao;

    iget-object v0, v0, LX/Iao;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;

    iget-object v0, v0, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->e:LX/IZK;

    invoke-virtual {v0}, LX/IZK;->a()V

    .line 2591295
    return-void
.end method

.method public final a(Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpPhoneVerificationMutationsModels$NativeSignUpSendConfirmationMutationModel;)V
    .locals 14
    .param p1    # Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpPhoneVerificationMutationsModels$NativeSignUpSendConfirmationMutationModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2591296
    iget-object v0, p0, LX/Ian;->a:LX/Iao;

    iget-object v0, v0, LX/Iao;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;

    invoke-static {v0}, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->p(Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;)V

    .line 2591297
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpPhoneVerificationMutationsModels$NativeSignUpSendConfirmationMutationModel;->j()Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2591298
    :cond_0
    iget-object v0, p0, LX/Ian;->a:LX/Iao;

    iget-object v0, v0, LX/Iao;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;

    iget-object v0, v0, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->e:LX/IZK;

    invoke-virtual {v0}, LX/IZK;->a()V

    .line 2591299
    :goto_0
    return-void

    .line 2591300
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpPhoneVerificationMutationsModels$NativeSignUpSendConfirmationMutationModel;->j()Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

    move-result-object v0

    .line 2591301
    new-instance v1, LX/IZh;

    invoke-direct {v1}, LX/IZh;-><init>()V

    iget-object v2, p0, LX/Ian;->a:LX/Iao;

    iget-object v2, v2, LX/Iao;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;

    invoke-static {v2}, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->k(Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;)Ljava/lang/String;

    move-result-object v2

    .line 2591302
    iput-object v2, v1, LX/IZh;->a:Ljava/lang/String;

    .line 2591303
    move-object v1, v1

    .line 2591304
    iget-object v2, p0, LX/Ian;->a:LX/Iao;

    iget-object v2, v2, LX/Iao;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;

    invoke-static {v2}, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->l(Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;)Ljava/lang/String;

    move-result-object v2

    .line 2591305
    iput-object v2, v1, LX/IZh;->c:Ljava/lang/String;

    .line 2591306
    move-object v1, v1

    .line 2591307
    iget-object v2, p0, LX/Ian;->a:LX/Iao;

    iget-object v2, v2, LX/Iao;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;

    invoke-static {v2}, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->n(Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;)Ljava/lang/String;

    move-result-object v2

    .line 2591308
    iput-object v2, v1, LX/IZh;->b:Ljava/lang/String;

    .line 2591309
    move-object v1, v1

    .line 2591310
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, LX/Ian;->a:LX/Iao;

    iget-object v3, v3, LX/Iao;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;

    invoke-static {v3}, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->k(Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, LX/Ian;->a:LX/Iao;

    iget-object v3, v3, LX/Iao;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;

    invoke-static {v3}, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->l(Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2591311
    iput-object v2, v1, LX/IZh;->d:Ljava/lang/String;

    .line 2591312
    move-object v1, v1

    .line 2591313
    const/4 v9, 0x1

    const/4 v13, 0x0

    const/4 v7, 0x0

    .line 2591314
    new-instance v5, LX/186;

    const/16 v6, 0x80

    invoke-direct {v5, v6}, LX/186;-><init>(I)V

    .line 2591315
    iget-object v6, v1, LX/IZh;->a:Ljava/lang/String;

    invoke-virtual {v5, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 2591316
    iget-object v8, v1, LX/IZh;->b:Ljava/lang/String;

    invoke-virtual {v5, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 2591317
    iget-object v10, v1, LX/IZh;->c:Ljava/lang/String;

    invoke-virtual {v5, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 2591318
    iget-object v11, v1, LX/IZh;->d:Ljava/lang/String;

    invoke-virtual {v5, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 2591319
    const/4 v12, 0x4

    invoke-virtual {v5, v12}, LX/186;->c(I)V

    .line 2591320
    invoke-virtual {v5, v13, v6}, LX/186;->b(II)V

    .line 2591321
    invoke-virtual {v5, v9, v8}, LX/186;->b(II)V

    .line 2591322
    const/4 v6, 0x2

    invoke-virtual {v5, v6, v10}, LX/186;->b(II)V

    .line 2591323
    const/4 v6, 0x3

    invoke-virtual {v5, v6, v11}, LX/186;->b(II)V

    .line 2591324
    invoke-virtual {v5}, LX/186;->d()I

    move-result v6

    .line 2591325
    invoke-virtual {v5, v6}, LX/186;->d(I)V

    .line 2591326
    invoke-virtual {v5}, LX/186;->e()[B

    move-result-object v5

    invoke-static {v5}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v6

    .line 2591327
    invoke-virtual {v6, v13}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2591328
    new-instance v5, LX/15i;

    move-object v8, v7

    move-object v10, v7

    invoke-direct/range {v5 .. v10}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2591329
    new-instance v6, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$PhoneNumberInfoModel;

    invoke-direct {v6, v5}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$PhoneNumberInfoModel;-><init>(LX/15i;)V

    .line 2591330
    move-object v1, v6

    .line 2591331
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;->SUCCESS:Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

    invoke-virtual {v2, v0}, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2591332
    iget-object v2, p0, LX/Ian;->a:LX/Iao;

    iget-object v2, v2, LX/Iao;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;

    iget-object v2, v2, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/Ian;->a:LX/Iao;

    iget-object v3, v3, LX/Iao;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 2591333
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2591334
    const-string v5, "phone_number_to_confirm"

    invoke-static {v4, v5, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2591335
    const-string v5, "BusinessConfirmCodeFragment"

    invoke-static {v3, v5, v4}, Lcom/facebook/messaging/business/common/activity/BusinessActivity;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v4

    move-object v1, v4

    .line 2591336
    const/4 v3, 0x1

    iget-object v4, p0, LX/Ian;->a:LX/Iao;

    iget-object v4, v4, LX/Iao;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;

    invoke-interface {v2, v1, v3, v4}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2591337
    :goto_1
    iget-object v1, p0, LX/Ian;->a:LX/Iao;

    iget-object v1, v1, LX/Iao;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;

    iget-object v1, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->f:LX/IZw;

    const-string v2, "success_request_sms_code"

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/IZw;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2591338
    :cond_2
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;->PHONE_NUMBER_ALREADY_VERIFIED:Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;

    invoke-virtual {v2, v0}, Lcom/facebook/graphql/enums/GraphQLSendConfirmationCodeOutcome;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2591339
    iget-object v2, p0, LX/Ian;->a:LX/Iao;

    iget-object v2, v2, LX/Iao;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;

    invoke-static {v2, v1}, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->a$redex0(Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$PhoneNumberInfoModel;)V

    goto :goto_1

    .line 2591340
    :cond_3
    iget-object v1, p0, LX/Ian;->a:LX/Iao;

    iget-object v1, v1, LX/Iao;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;

    iget-object v1, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->e:LX/IZK;

    invoke-virtual {p1}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpPhoneVerificationMutationsModels$NativeSignUpSendConfirmationMutationModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/IZK;->a(Ljava/lang/String;)V

    goto :goto_1
.end method
