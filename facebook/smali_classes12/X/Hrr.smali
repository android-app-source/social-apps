.class public LX/Hrr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0iK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0j1;",
        "DerivedData::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesIsAttachToAlbumSupported;",
        "Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;>",
        "Ljava/lang/Object;",
        "LX/0iK",
        "<TModelData;TDerivedData;>;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:LX/0wM;

.field private final c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field private final d:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/resources/ui/FbTextView;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/Hqs;

.field private final f:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/0wM;LX/0il;LX/Hqs;Landroid/view/ViewStub;)V
    .locals 2
    .param p3    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/Hqs;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Landroid/view/ViewStub;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "LX/0wM;",
            "TServices;",
            "Lcom/facebook/composer/album/controller/AlbumPillController$PillClickedListener;",
            "Landroid/view/ViewStub;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2513709
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2513710
    new-instance v0, LX/Hrq;

    invoke-direct {v0, p0}, LX/Hrq;-><init>(LX/Hrr;)V

    iput-object v0, p0, LX/Hrr;->f:Landroid/view/View$OnClickListener;

    .line 2513711
    iput-object p1, p0, LX/Hrr;->a:Landroid/content/res/Resources;

    .line 2513712
    iput-object p2, p0, LX/Hrr;->b:LX/0wM;

    .line 2513713
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/Hrr;->c:Ljava/lang/ref/WeakReference;

    .line 2513714
    iput-object p4, p0, LX/Hrr;->e:LX/Hqs;

    .line 2513715
    new-instance v0, LX/0zw;

    invoke-direct {v0, p5}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v0, p0, LX/Hrr;->d:LX/0zw;

    .line 2513716
    invoke-direct {p0}, LX/Hrr;->b()V

    .line 2513717
    return-void
.end method

.method private b()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const v6, -0x6e685d

    .line 2513695
    iget-object v0, p0, LX/Hrr;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    move-object v1, v0

    .line 2513696
    check-cast v1, LX/0ik;

    invoke-interface {v1}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2zG;

    invoke-virtual {v1}, LX/2zG;->f()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2513697
    iget-object v0, p0, LX/Hrr;->d:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->c()V

    .line 2513698
    :goto_0
    return-void

    .line 2513699
    :cond_0
    iget-object v1, p0, LX/Hrr;->d:LX/0zw;

    invoke-virtual {v1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    .line 2513700
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2513701
    iget-object v2, p0, LX/Hrr;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2513702
    iget-object v2, p0, LX/Hrr;->b:LX/0wM;

    iget-object v3, p0, LX/Hrr;->a:Landroid/content/res/Resources;

    const v4, 0x7f020319

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3, v6}, LX/0wM;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 2513703
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0j1;

    invoke-interface {v2}, LX/0j1;->getTargetAlbum()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0j1;

    invoke-interface {v2}, LX/0j1;->getTargetAlbum()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    .line 2513704
    :goto_1
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0j1;

    invoke-interface {v0}, LX/0j1;->getTargetAlbum()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/Hrr;->b:LX/0wM;

    iget-object v4, p0, LX/Hrr;->a:Landroid/content/res/Resources;

    const v5, 0x7f0202ed

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v0, v4, v6}, LX/0wM;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2513705
    :goto_2
    invoke-static {v1, v0, v7, v3, v7}, LX/4lM;->a(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2513706
    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2513707
    :cond_1
    iget-object v2, p0, LX/Hrr;->a:Landroid/content/res/Resources;

    const v4, 0x7f0814ac

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 2513708
    :cond_2
    iget-object v0, p0, LX/Hrr;->b:LX/0wM;

    iget-object v4, p0, LX/Hrr;->a:Landroid/content/res/Resources;

    const v5, 0x7f02030e

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v0, v4, v6}, LX/0wM;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_2
.end method


# virtual methods
.method public final a(LX/5L2;)V
    .locals 0

    .prologue
    .line 2513694
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2513692
    invoke-direct {p0}, LX/Hrr;->b()V

    .line 2513693
    return-void
.end method
