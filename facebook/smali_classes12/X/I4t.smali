.class public LX/I4t;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""


# instance fields
.field public j:Lcom/facebook/fbui/glyph/GlyphView;

.field public k:Lcom/facebook/resources/ui/FbTextView;

.field public l:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 2534626
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;)V

    .line 2534627
    const/16 v1, 0x11

    const/4 p1, 0x0

    .line 2534628
    const v0, 0x7f03056d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 2534629
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setGravity(I)V

    .line 2534630
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailGravity(I)V

    .line 2534631
    new-instance v0, LX/1au;

    const/4 v1, -0x1

    invoke-virtual {p0}, LX/I4t;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b23d2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/1au;-><init>(II)V

    invoke-virtual {p0, v0}, LX/I4t;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2534632
    invoke-virtual {p0}, LX/I4t;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b23cb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2534633
    invoke-virtual {p0, v0, p1, v0, p1}, LX/I4t;->setPadding(IIII)V

    .line 2534634
    invoke-virtual {p0}, LX/I4t;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00d5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, LX/I4t;->setBackgroundColor(I)V

    .line 2534635
    invoke-virtual {p0}, LX/I4t;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0034

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, p1, v0, p1, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->b(IIII)V

    .line 2534636
    invoke-virtual {p0}, LX/I4t;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a009e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setBorderColor(I)V

    .line 2534637
    const v0, 0x7f0d0f15

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/I4t;->j:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2534638
    const v0, 0x7f0d0f16

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/I4t;->k:Lcom/facebook/resources/ui/FbTextView;

    .line 2534639
    const v0, 0x7f0d0f17

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/I4t;->l:Lcom/facebook/resources/ui/FbTextView;

    .line 2534640
    return-void
.end method
