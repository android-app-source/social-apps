.class public final LX/IdE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/IdD;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)V
    .locals 0

    .prologue
    .line 2596120
    iput-object p1, p0, LX/IdE;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;)V
    .locals 12

    .prologue
    .line 2596121
    iget-object v0, p0, LX/IdE;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v0, v0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->G:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iget-object v0, v0, Lcom/facebook/messaging/business/ride/utils/LocationParams;->a:Landroid/location/Location;

    if-nez v0, :cond_1

    .line 2596122
    :cond_0
    :goto_0
    return-void

    .line 2596123
    :cond_1
    iget-object v0, p0, LX/IdE;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v0, v0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->g:LX/Ib2;

    const-string v1, "click_ride_type_item"

    invoke-virtual {v0, v1}, LX/Ib2;->b(Ljava/lang/String;)V

    .line 2596124
    iget-object v0, p0, LX/IdE;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v0, v0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->c:LX/Ibs;

    iget-object v1, p0, LX/IdE;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v1, v1, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->w:Lcom/facebook/messaging/business/ride/utils/RideServiceParams;

    .line 2596125
    iget-object v2, v1, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->e:Ljava/lang/String;

    move-object v1, v2

    .line 2596126
    invoke-virtual {p1}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;->k()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/IdE;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v3, v3, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->B:Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;

    .line 2596127
    iget-object v4, v3, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->f:Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;

    move-object v3, v4

    .line 2596128
    if-eqz v3, :cond_4

    iget-object v3, p0, LX/IdE;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v3, v3, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->B:Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;

    .line 2596129
    iget-object v4, v3, Lcom/facebook/messaging/business/ride/view/RideTypeTabContainerView;->f:Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;

    move-object v3, v4

    .line 2596130
    invoke-virtual {v3}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;->l()I

    move-result v3

    :goto_1
    iget-object v4, p0, LX/IdE;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v4, v4, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->G:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iget-object v4, v4, Lcom/facebook/messaging/business/ride/utils/LocationParams;->a:Landroid/location/Location;

    iget-object v5, p0, LX/IdE;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v5, v5, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->H:Lcom/facebook/messaging/business/ride/utils/LocationParams;

    iget-object v5, v5, Lcom/facebook/messaging/business/ride/utils/LocationParams;->a:Landroid/location/Location;

    .line 2596131
    invoke-virtual {v0}, LX/Ibs;->a()V

    .line 2596132
    new-instance v6, LX/IbF;

    invoke-direct {v6}, LX/IbF;-><init>()V

    move-object v6, v6

    .line 2596133
    const-string v7, "provider"

    invoke-virtual {v6, v7, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v6

    const-string v7, "ride_type"

    invoke-virtual {v6, v7, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v6

    const-string v7, "seat_count"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v6

    const-string v7, "source_latitude"

    invoke-virtual {v4}, Landroid/location/Location;->getLatitude()D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v6

    const-string v7, "source_longitude"

    invoke-virtual {v4}, Landroid/location/Location;->getLongitude()D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v6

    check-cast v6, LX/IbF;

    .line 2596134
    if-eqz v5, :cond_2

    .line 2596135
    const-string v7, "destination_latitude"

    invoke-virtual {v5}, Landroid/location/Location;->getLatitude()D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v7

    const-string v8, "destination_longitude"

    invoke-virtual {v5}, Landroid/location/Location;->getLongitude()D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2596136
    :cond_2
    iget-object v7, v0, LX/Ibs;->c:LX/0tX;

    invoke-static {v6}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v6

    invoke-virtual {v7, v6}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v6

    iput-object v6, v0, LX/Ibs;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2596137
    iget-object v6, v0, LX/Ibs;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v7, LX/Ibr;

    invoke-direct {v7, v0}, LX/Ibr;-><init>(LX/Ibs;)V

    iget-object v8, v0, LX/Ibs;->d:Ljava/util/concurrent/ExecutorService;

    invoke-static {v6, v7, v8}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2596138
    iget-object v0, p0, LX/IdE;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    invoke-static {v0}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->x(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;)V

    .line 2596139
    iget-object v0, p0, LX/IdE;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2596140
    iget-object v0, p0, LX/IdE;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v0, v0, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->o:LX/Ib3;

    iget-object v1, p0, LX/IdE;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    iget-object v1, v1, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->w:Lcom/facebook/messaging/business/ride/utils/RideServiceParams;

    .line 2596141
    iget-object v2, v1, Lcom/facebook/messaging/business/ride/utils/RideServiceParams;->e:Ljava/lang/String;

    move-object v1, v2

    .line 2596142
    invoke-virtual {p1}, Lcom/facebook/messaging/business/ride/graphql/RideQueryFragmentsModels$RideTypeModel;->a()Ljava/lang/String;

    move-result-object v2

    .line 2596143
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 2596144
    :cond_3
    :goto_2
    goto/16 :goto_0

    .line 2596145
    :cond_4
    const/4 v3, 0x1

    goto/16 :goto_1

    .line 2596146
    :cond_5
    invoke-static {v0, v1}, LX/Ib3;->b(LX/Ib3;Ljava/lang/String;)LX/Ib4;

    move-result-object v3

    .line 2596147
    iput-object v2, v3, LX/Ib4;->b:Ljava/lang/String;

    .line 2596148
    invoke-static {v0, v3}, LX/Ib3;->a(LX/Ib3;LX/Ib4;)V

    goto :goto_2
.end method
