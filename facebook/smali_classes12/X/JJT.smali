.class public final LX/JJT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardExternalLinksMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/JJX;


# direct methods
.method public constructor <init>(LX/JJX;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2679542
    iput-object p1, p0, LX/JJT;->b:LX/JJX;

    iput-object p2, p0, LX/JJT;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    .line 2679543
    iget-object v0, p0, LX/JJT;->b:LX/JJX;

    iget-object v0, v0, LX/JJX;->e:LX/JJR;

    const/16 v2, 0x8

    .line 2679544
    iget-object v1, v0, LX/JJR;->f:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 2679545
    iget-object v1, v0, LX/JJR;->p:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2679546
    iget-object v1, v0, LX/JJR;->l:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2679547
    iget-object v1, v0, LX/JJR;->f:Landroid/view/View;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    .line 2679548
    iget-object v1, v0, LX/JJR;->f:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2679549
    new-instance v1, LX/6WI;

    invoke-virtual {v0}, LX/JJR;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/6WI;-><init>(Landroid/content/Context;)V

    const v2, 0x7f08003d

    invoke-virtual {v1, v2}, LX/6WI;->b(I)LX/6WI;

    move-result-object v1

    const v2, 0x7f080016

    new-instance v3, LX/JJP;

    invoke-direct {v3, v0}, LX/JJP;-><init>(LX/JJR;)V

    invoke-virtual {v1, v2, v3}, LX/6WI;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/6WI;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/6WI;->a(Z)LX/6WI;

    move-result-object v1

    invoke-virtual {v1}, LX/6WI;->b()LX/6WJ;

    .line 2679550
    iget-object v0, p0, LX/JJT;->b:LX/JJX;

    iget-object v0, v0, LX/JJX;->c:LX/JJZ;

    iget-object v1, p0, LX/JJT;->a:Ljava/lang/String;

    iget-object v2, p0, LX/JJT;->b:LX/JJX;

    iget-object v2, v2, LX/JJX;->g:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    iget-object v2, v2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->promotionId:Ljava/lang/String;

    iget-object v3, p0, LX/JJT;->b:LX/JJX;

    iget-object v3, v3, LX/JJX;->g:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    iget-object v3, v3, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->customRenderType:Lcom/facebook/quickpromotion/customrender/CustomRenderType;

    invoke-virtual {v3}, Lcom/facebook/quickpromotion/customrender/CustomRenderType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    .line 2679551
    iget-object v5, v0, LX/JJZ;->a:LX/0Zb;

    const-string p0, "family_bridges_qp_add_username_failure"

    invoke-static {p0, v1, v2, v3}, LX/JJZ;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "failure_message"

    invoke-virtual {p0, p1, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-interface {v5, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2679552
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 2679535
    iget-object v0, p0, LX/JJT;->b:LX/JJX;

    iget-object v0, v0, LX/JJX;->e:LX/JJR;

    .line 2679536
    iget-object v4, v0, LX/JJR;->f:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 2679537
    iget-object v4, v0, LX/JJR;->l:Landroid/view/View;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 2679538
    iget-object v4, v0, LX/JJR;->f:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    const-wide/16 v6, 0xc8

    invoke-virtual {v4, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    new-instance v5, LX/JJO;

    invoke-direct {v5, v0}, LX/JJO;-><init>(LX/JJR;)V

    invoke-virtual {v4, v5}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 2679539
    iget-object v0, p0, LX/JJT;->b:LX/JJX;

    iget-object v0, v0, LX/JJX;->c:LX/JJZ;

    iget-object v1, p0, LX/JJT;->a:Ljava/lang/String;

    iget-object v2, p0, LX/JJT;->b:LX/JJX;

    iget-object v2, v2, LX/JJX;->g:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    iget-object v2, v2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->promotionId:Ljava/lang/String;

    iget-object v3, p0, LX/JJT;->b:LX/JJX;

    iget-object v3, v3, LX/JJX;->g:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    iget-object v3, v3, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->customRenderType:Lcom/facebook/quickpromotion/customrender/CustomRenderType;

    invoke-virtual {v3}, Lcom/facebook/quickpromotion/customrender/CustomRenderType;->name()Ljava/lang/String;

    move-result-object v3

    .line 2679540
    iget-object v4, v0, LX/JJZ;->a:LX/0Zb;

    const-string v5, "family_bridges_qp_add_username_success"

    invoke-static {v5, v1, v2, v3}, LX/JJZ;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-interface {v4, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2679541
    return-void
.end method
