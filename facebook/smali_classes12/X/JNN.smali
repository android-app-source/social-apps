.class public final LX/JNN;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JNO;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;

.field public final synthetic b:LX/JNO;


# direct methods
.method public constructor <init>(LX/JNO;)V
    .locals 1

    .prologue
    .line 2685798
    iput-object p1, p0, LX/JNN;->b:LX/JNO;

    .line 2685799
    move-object v0, p1

    .line 2685800
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2685801
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2685786
    const-string v0, "EventsSuggestionItemFacepileComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2685787
    if-ne p0, p1, :cond_1

    .line 2685788
    :cond_0
    :goto_0
    return v0

    .line 2685789
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2685790
    goto :goto_0

    .line 2685791
    :cond_3
    check-cast p1, LX/JNN;

    .line 2685792
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2685793
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2685794
    if-eq v2, v3, :cond_0

    .line 2685795
    iget-object v2, p0, LX/JNN;->a:Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/JNN;->a:Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;

    iget-object v3, p1, LX/JNN;->a:Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2685796
    goto :goto_0

    .line 2685797
    :cond_4
    iget-object v2, p1, LX/JNN;->a:Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
