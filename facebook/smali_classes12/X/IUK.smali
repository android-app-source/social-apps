.class public final LX/IUK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;)V
    .locals 0

    .prologue
    .line 2579888
    iput-object p1, p0, LX/IUK;->a:Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, -0x325167ac    # -3.6615232E8f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2579889
    const-string v1, "extra_request_id"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2579890
    if-nez v1, :cond_0

    .line 2579891
    sget-object v1, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->N:Ljava/lang/String;

    const-string v2, "There is no composer session id"

    invoke-static {v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2579892
    const/16 v1, 0x27

    const v2, -0x3c524c59

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2579893
    :goto_0
    return-void

    .line 2579894
    :cond_0
    iget-object v2, p0, LX/IUK;->a:Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;

    iget-object v2, v2, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->L:LX/CGV;

    .line 2579895
    iget-object v3, v2, LX/CGV;->b:Ljava/lang/String;

    move-object v2, v3

    .line 2579896
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2579897
    iget-object v1, p0, LX/IUK;->a:Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/ui/GroupsStandAloneFeedFragment;->mJ_()V

    .line 2579898
    :cond_1
    const v1, -0x2fb4f3c5

    invoke-static {v1, v0}, LX/02F;->e(II)V

    goto :goto_0
.end method
