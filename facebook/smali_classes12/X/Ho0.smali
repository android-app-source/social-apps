.class public final LX/Ho0;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    .line 2504981
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2504982
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 2504983
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 2504984
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2504985
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_9

    .line 2504986
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2504987
    :goto_1
    move v1, v2

    .line 2504988
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2504989
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0

    .line 2504990
    :cond_1
    const-string v10, "is_nt"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 2504991
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v4

    move v7, v4

    move v4, v3

    .line 2504992
    :cond_2
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_6

    .line 2504993
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 2504994
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2504995
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_2

    if-eqz v9, :cond_2

    .line 2504996
    const-string v10, "display_name"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 2504997
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_2

    .line 2504998
    :cond_3
    const-string v10, "poll_interval"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 2504999
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v1

    move v6, v1

    move v1, v3

    goto :goto_2

    .line 2505000
    :cond_4
    const-string v10, "tab_id"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 2505001
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_2

    .line 2505002
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_2

    .line 2505003
    :cond_6
    const/4 v9, 0x4

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 2505004
    invoke-virtual {p1, v2, v8}, LX/186;->b(II)V

    .line 2505005
    if-eqz v4, :cond_7

    .line 2505006
    invoke-virtual {p1, v3, v7}, LX/186;->a(IZ)V

    .line 2505007
    :cond_7
    if-eqz v1, :cond_8

    .line 2505008
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v6, v2}, LX/186;->a(III)V

    .line 2505009
    :cond_8
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 2505010
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_1

    :cond_9
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    goto :goto_2
.end method
