.class public LX/JLc;
.super LX/5pb;
.source ""

# interfaces
.implements LX/5on;
.implements LX/5pQ;


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "FBMarketplaceNativeModule"
.end annotation


# instance fields
.field private final a:LX/0xB;

.field private final b:Lcom/facebook/content/SecureContextHelper;

.field private final c:LX/17Y;

.field private d:LX/2EJ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final e:LX/0Uh;

.field private final f:LX/0ad;


# direct methods
.method public constructor <init>(LX/5pY;LX/0xB;Lcom/facebook/content/SecureContextHelper;LX/17Y;LX/0Uh;LX/0ad;)V
    .locals 0
    .param p1    # LX/5pY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2681303
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 2681304
    invoke-virtual {p1, p0}, LX/5pX;->a(LX/5pQ;)V

    .line 2681305
    iput-object p2, p0, LX/JLc;->a:LX/0xB;

    .line 2681306
    iput-object p3, p0, LX/JLc;->b:Lcom/facebook/content/SecureContextHelper;

    .line 2681307
    iput-object p4, p0, LX/JLc;->c:LX/17Y;

    .line 2681308
    invoke-virtual {p1, p0}, LX/5pX;->a(LX/5on;)V

    .line 2681309
    iput-object p5, p0, LX/JLc;->e:LX/0Uh;

    .line 2681310
    iput-object p6, p0, LX/JLc;->f:LX/0ad;

    .line 2681311
    return-void
.end method

.method public static synthetic a(LX/JLc;)LX/5pY;
    .locals 1

    .prologue
    .line 2681383
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2681384
    return-object v0
.end method

.method public static synthetic b(LX/JLc;)LX/5pY;
    .locals 1

    .prologue
    .line 2681381
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2681382
    return-object v0
.end method


# virtual methods
.method public final a(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2681379
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 2681380
    :cond_0
    return-void
.end method

.method public final b()Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2681359
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 2681360
    const-string v3, "showAttentionGrabbingTags"

    iget-object v4, p0, LX/JLc;->e:LX/0Uh;

    const/16 v5, 0x529

    invoke-virtual {v4, v5, v1}, LX/0Uh;->a(IZ)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2681361
    const-string v3, "showGroupItemsLink"

    iget-object v4, p0, LX/JLc;->e:LX/0Uh;

    const/16 v5, 0x52a

    invoke-virtual {v4, v5, v1}, LX/0Uh;->a(IZ)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2681362
    const-string v3, "showTitleInBrowse"

    iget-object v4, p0, LX/JLc;->f:LX/0ad;

    sget-short v5, LX/0xb;->e:S

    invoke-interface {v4, v5, v1}, LX/0ad;->a(SZ)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2681363
    const-string v3, "useLowResInitialImageInBrowse"

    iget-object v4, p0, LX/JLc;->f:LX/0ad;

    sget-short v5, LX/0xb;->a:S

    invoke-interface {v4, v5, v1}, LX/0ad;->a(SZ)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2681364
    const-string v3, "usePreviewPayloadInBrowse"

    iget-object v4, p0, LX/JLc;->f:LX/0ad;

    sget-short v5, LX/0xb;->j:S

    invoke-interface {v4, v5, v1}, LX/0ad;->a(SZ)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2681365
    const-string v3, "showTitleInSearch"

    iget-object v4, p0, LX/JLc;->f:LX/0ad;

    sget-short v5, LX/0xb;->f:S

    invoke-interface {v4, v5, v0}, LX/0ad;->a(SZ)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2681366
    const-string v3, "showMessageInBrowse"

    iget-object v4, p0, LX/JLc;->f:LX/0ad;

    sget-short v5, LX/0xb;->b:S

    invoke-interface {v4, v5, v1}, LX/0ad;->a(SZ)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2681367
    const-string v3, "showTopCategoryUpsell"

    iget-object v4, p0, LX/JLc;->f:LX/0ad;

    sget-short v5, LX/0xb;->g:S

    invoke-interface {v4, v5, v1}, LX/0ad;->a(SZ)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2681368
    const-string v3, "showSearchUpsell"

    iget-object v4, p0, LX/JLc;->f:LX/0ad;

    sget-short v5, LX/0xb;->c:S

    invoke-interface {v4, v5, v1}, LX/0ad;->a(SZ)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2681369
    const-string v3, "useMarketplaceLite"

    iget-object v4, p0, LX/JLc;->f:LX/0ad;

    sget-short v5, LX/0xb;->h:S

    invoke-interface {v4, v5, v1}, LX/0ad;->a(SZ)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2681370
    const-string v3, "useStoryView"

    iget-object v4, p0, LX/JLc;->f:LX/0ad;

    sget-short v5, LX/0xb;->k:S

    invoke-interface {v4, v5, v1}, LX/0ad;->a(SZ)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2681371
    const-string v3, "showTitleAsImagePlaceholder"

    iget-object v4, p0, LX/JLc;->f:LX/0ad;

    sget-short v5, LX/0xb;->d:S

    invoke-interface {v4, v5, v1}, LX/0ad;->a(SZ)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2681372
    const-string v3, "useFixedHeightImages"

    iget-object v4, p0, LX/JLc;->f:LX/0ad;

    sget-short v5, LX/0xb;->i:S

    invoke-interface {v4, v5, v1}, LX/0ad;->a(SZ)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2681373
    sget-object v3, LX/1vX;->c:LX/1vX;

    move-object v3, v3

    .line 2681374
    iget-object v4, p0, LX/5pb;->a:LX/5pY;

    move-object v4, v4

    .line 2681375
    invoke-virtual {v3, v4}, LX/1od;->a(Landroid/content/Context;)I

    move-result v3

    .line 2681376
    const-string v4, "isGooglePlayAvailable"

    if-nez v3, :cond_0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v2, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2681377
    return-object v2

    :cond_0
    move v0, v1

    .line 2681378
    goto :goto_0
.end method

.method public final bM_()V
    .locals 0

    .prologue
    .line 2681358
    return-void
.end method

.method public final bN_()V
    .locals 1

    .prologue
    .line 2681355
    iget-object v0, p0, LX/JLc;->d:LX/2EJ;

    if-eqz v0, :cond_0

    .line 2681356
    iget-object v0, p0, LX/JLc;->d:LX/2EJ;

    invoke-virtual {v0}, LX/2EJ;->dismiss()V

    .line 2681357
    :cond_0
    return-void
.end method

.method public final bO_()V
    .locals 1

    .prologue
    .line 2681352
    iget-object v0, p0, LX/JLc;->d:LX/2EJ;

    if-eqz v0, :cond_0

    .line 2681353
    iget-object v0, p0, LX/JLc;->d:LX/2EJ;

    invoke-virtual {v0}, LX/2EJ;->dismiss()V

    .line 2681354
    :cond_0
    return-void
.end method

.method public clearLocalPreferredMarketplaceID()V
    .locals 0
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2681351
    return-void
.end method

.method public clearMarketplaceJewelBadgeCount()V
    .locals 3
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2681349
    iget-object v0, p0, LX/JLc;->a:LX/0xB;

    sget-object v1, LX/12j;->MARKETPLACE:LX/12j;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0xB;->a(LX/12j;I)V

    .line 2681350
    return-void
.end method

.method public getMarketplaceJewelBadgeCount(Lcom/facebook/react/bridge/Callback;)V
    .locals 4
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2681345
    if-eqz p1, :cond_0

    .line 2681346
    iget-object v0, p0, LX/JLc;->a:LX/0xB;

    sget-object v1, LX/12j;->MARKETPLACE:LX/12j;

    invoke-virtual {v0, v1}, LX/0xB;->a(LX/12j;)I

    move-result v0

    .line 2681347
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const/4 v3, 0x0

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v1, v2

    invoke-interface {p1, v1}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    .line 2681348
    :cond_0
    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2681344
    const-string v0, "FBMarketplaceNativeModule"

    return-object v0
.end method

.method public launchLocationEditDialog(Ljava/lang/String;)V
    .locals 6
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    const/16 v3, 0xa

    const/4 v5, 0x0

    .line 2681332
    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v0

    .line 2681333
    if-nez v0, :cond_0

    .line 2681334
    :goto_0
    return-void

    .line 2681335
    :cond_0
    new-instance v1, Landroid/widget/EditText;

    invoke-direct {v1, v0}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 2681336
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setInputType(I)V

    .line 2681337
    invoke-virtual {v1, p1}, Landroid/widget/EditText;->append(Ljava/lang/CharSequence;)V

    .line 2681338
    const v2, 0x7f08380b    # 1.81066E38f

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setHint(I)V

    .line 2681339
    const/16 v2, 0x32

    invoke-virtual {v1, v2, v3, v5, v3}, Landroid/widget/EditText;->setPadding(IIII)V

    .line 2681340
    const/4 v2, 0x1

    new-array v2, v2, [Landroid/text/InputFilter;

    new-instance v3, Landroid/text/InputFilter$LengthFilter;

    const/4 v4, 0x5

    invoke-direct {v3, v4}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v3, v2, v5

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 2681341
    new-instance v2, LX/0ju;

    invoke-direct {v2, v0}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v0, 0x7f083809

    invoke-virtual {v2, v0}, LX/0ju;->a(I)LX/0ju;

    move-result-object v0

    const v2, 0x7f08380a

    invoke-virtual {v0, v2}, LX/0ju;->b(I)LX/0ju;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    move-result-object v0

    const v2, 0x7f080017

    new-instance v3, LX/JLb;

    invoke-direct {v3, p0}, LX/JLb;-><init>(LX/JLc;)V

    invoke-virtual {v0, v2, v3}, LX/0ju;->c(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v2, 0x7f08380c

    new-instance v3, LX/JLa;

    invoke-direct {v3, p0, v1}, LX/JLa;-><init>(LX/JLc;Landroid/widget/EditText;)V

    invoke-virtual {v0, v2, v3}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    iput-object v0, p0, LX/JLc;->d:LX/2EJ;

    .line 2681342
    iget-object v0, p0, LX/JLc;->d:LX/2EJ;

    invoke-virtual {v0}, LX/2EJ;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 2681343
    iget-object v0, p0, LX/JLc;->d:LX/2EJ;

    invoke-virtual {v0}, LX/2EJ;->show()V

    goto :goto_0
.end method

.method public openSystemLocationSettings()V
    .locals 3
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2681326
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.LOCATION_SOURCE_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2681327
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2681328
    iget-object v1, p0, LX/JLc;->b:Lcom/facebook/content/SecureContextHelper;

    .line 2681329
    iget-object v2, p0, LX/5pb;->a:LX/5pY;

    move-object v2, v2

    .line 2681330
    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2681331
    return-void
.end method

.method public reportStoryURL(LX/5pG;I)V
    .locals 4
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2681313
    const-string v0, "storyGraphQLID"

    invoke-interface {p1, v0}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2681314
    const-string v1, "actionType"

    invoke-interface {p1, v1}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2681315
    const-string v2, "storyRenderLocation"

    invoke-interface {p1, v2}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2681316
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    .line 2681317
    sget-object v3, LX/0ax;->dw:Ljava/lang/String;

    invoke-static {v3, v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2681318
    iget-object v1, p0, LX/JLc;->c:LX/17Y;

    .line 2681319
    iget-object v2, p0, LX/5pb;->a:LX/5pY;

    move-object v2, v2

    .line 2681320
    invoke-interface {v1, v2, v0}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2681321
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2681322
    iget-object v1, p0, LX/JLc;->b:Lcom/facebook/content/SecureContextHelper;

    .line 2681323
    iget-object v2, p0, LX/5pb;->a:LX/5pY;

    move-object v2, v2

    .line 2681324
    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2681325
    :cond_0
    return-void
.end method

.method public saveLocalPreferredMarketplaceID(Ljava/lang/String;)V
    .locals 0
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2681312
    return-void
.end method
