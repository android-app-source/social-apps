.class public LX/JVK;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2700509
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/JVK;
    .locals 3

    .prologue
    .line 2700510
    const-class v1, LX/JVK;

    monitor-enter v1

    .line 2700511
    :try_start_0
    sget-object v0, LX/JVK;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2700512
    sput-object v2, LX/JVK;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2700513
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2700514
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2700515
    new-instance v0, LX/JVK;

    invoke-direct {v0}, LX/JVK;-><init>()V

    .line 2700516
    move-object v0, v0

    .line 2700517
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2700518
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JVK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2700519
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2700520
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
