.class public LX/HwZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/HwZ;


# instance fields
.field private final a:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2521084
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2521085
    iput-object p1, p0, LX/HwZ;->a:Landroid/content/ContentResolver;

    .line 2521086
    return-void
.end method

.method public static a(LX/0QB;)LX/HwZ;
    .locals 4

    .prologue
    .line 2521069
    sget-object v0, LX/HwZ;->b:LX/HwZ;

    if-nez v0, :cond_1

    .line 2521070
    const-class v1, LX/HwZ;

    monitor-enter v1

    .line 2521071
    :try_start_0
    sget-object v0, LX/HwZ;->b:LX/HwZ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2521072
    if-eqz v2, :cond_0

    .line 2521073
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2521074
    new-instance p0, LX/HwZ;

    invoke-static {v0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v3

    check-cast v3, Landroid/content/ContentResolver;

    invoke-direct {p0, v3}, LX/HwZ;-><init>(Landroid/content/ContentResolver;)V

    .line 2521075
    move-object v0, p0

    .line 2521076
    sput-object v0, LX/HwZ;->b:LX/HwZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2521077
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2521078
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2521079
    :cond_1
    sget-object v0, LX/HwZ;->b:LX/HwZ;

    return-object v0

    .line 2521080
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2521081
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final clearUserData()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2521082
    iget-object v0, p0, LX/HwZ;->a:Landroid/content/ContentResolver;

    sget-object v1, LX/0PE;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2521083
    return-void
.end method
