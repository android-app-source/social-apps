.class public LX/JCY;
.super LX/B0Z;
.source ""


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Z

.field private final c:LX/J90;

.field private final d:LX/JCx;

.field private final e:LX/J8z;

.field private final f:LX/JCV;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/Boolean;LX/J90;LX/JCx;LX/J8z;LX/JCV;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/timeline/aboutpage/annotations/IsTheWhoEnabled;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2662500
    invoke-direct {p0}, LX/B0Z;-><init>()V

    .line 2662501
    iput-object p1, p0, LX/JCY;->a:Ljava/lang/String;

    .line 2662502
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/JCY;->b:Z

    .line 2662503
    iput-object p3, p0, LX/JCY;->c:LX/J90;

    .line 2662504
    iput-object p4, p0, LX/JCY;->d:LX/JCx;

    .line 2662505
    iput-object p5, p0, LX/JCY;->e:LX/J8z;

    .line 2662506
    iput-object p6, p0, LX/JCY;->f:LX/JCV;

    .line 2662507
    return-void
.end method


# virtual methods
.method public final a(LX/4a1;)LX/0zO;
    .locals 5

    .prologue
    .line 2662531
    iget-object v0, p0, LX/JCY;->c:LX/J90;

    iget-object v1, p0, LX/JCY;->a:Ljava/lang/String;

    const/4 v2, 0x4

    iget-boolean v3, p0, LX/JCY;->b:Z

    iget-object v4, p1, LX/4a1;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/J90;->a(Ljava/lang/String;IZLjava/lang/String;)LX/JBI;

    move-result-object v0

    .line 2662532
    const-string v1, "collections_after"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;LX/4a1;)LX/0gW;

    .line 2662533
    iget-object v1, p0, LX/JCY;->e:LX/J8z;

    invoke-virtual {v1}, LX/J8z;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2662534
    const-string v1, "param"

    const-string v2, "collections_after"

    invoke-virtual {v0, v1, v2}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2662535
    const-string v1, "import"

    const-string v2, "collections_sections_end_cursor"

    invoke-virtual {v0, v1, v2}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2662536
    const-string v1, "max_runs"

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2662537
    :cond_0
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/B0d;)LX/0zO;
    .locals 5

    .prologue
    .line 2662538
    iget-object v0, p0, LX/JCY;->c:LX/J90;

    iget-object v1, p0, LX/JCY;->a:Ljava/lang/String;

    const/4 v2, 0x4

    iget-boolean v3, p0, LX/JCY;->b:Z

    iget-object v4, p1, LX/B0d;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/J90;->a(Ljava/lang/String;IZLjava/lang/String;)LX/JBI;

    move-result-object v0

    .line 2662539
    iget-object v1, p0, LX/JCY;->e:LX/J8z;

    invoke-virtual {v1}, LX/J8z;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2662540
    const-string v1, "param"

    const-string v2, "collections_after"

    invoke-virtual {v0, v1, v2}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2662541
    const-string v1, "import"

    const-string v2, "collections_sections_end_cursor"

    invoke-virtual {v0, v1, v2}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2662542
    const-string v1, "max_runs"

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2662543
    :cond_0
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/B0d;Lcom/facebook/graphql/executor/GraphQLResult;)LX/B0N;
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2662509
    iget-object v0, p2, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v0

    .line 2662510
    check-cast v1, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineCollectionAppSectionsModel;

    .line 2662511
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineCollectionAppSectionsModel;->a()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineCollectionAppSectionsFieldsModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2662512
    :cond_0
    const-class v0, LX/JCY;

    const-string v1, "Missing connection"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2662513
    new-instance v0, LX/B0f;

    invoke-direct {v0, p1}, LX/B0f;-><init>(LX/B0d;)V

    .line 2662514
    :goto_0
    return-object v0

    .line 2662515
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineCollectionAppSectionsModel;->a()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineCollectionAppSectionsFieldsModel;

    move-result-object v0

    .line 2662516
    new-instance v2, LX/9JH;

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineCollectionAppSectionsFieldsModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v5

    invoke-direct {v2, v5}, LX/9JH;-><init>(I)V

    .line 2662517
    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineCollectionAppSectionsFieldsModel;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v5, v4

    :goto_1
    if-ge v5, v7, :cond_3

    invoke-virtual {v6, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;

    .line 2662518
    iget-object v8, p0, LX/JCY;->f:LX/JCV;

    iget-object v9, p0, LX/JCY;->d:LX/JCx;

    invoke-virtual {v8, v9, v0}, LX/JCV;->a(LX/JCx;Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionWithItemsOrRequestablesModel;)Z

    move-result v8

    .line 2662519
    if-eqz v8, :cond_2

    .line 2662520
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v8

    invoke-static {v0}, LX/0w5;->a(Lcom/facebook/flatbuffers/Flattenable;)LX/0w5;

    move-result-object v9

    invoke-static {v0}, LX/11F;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-virtual {v2, v8, v9, v0, v4}, LX/9JH;->a(ILX/0w5;Ljava/util/Collection;I)V

    .line 2662521
    :cond_2
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_1

    .line 2662522
    :cond_3
    const/4 v0, 0x0

    .line 2662523
    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineCollectionAppSectionsModel;->a()Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineCollectionAppSectionsFieldsModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$TimelineCollectionAppSectionsFieldsModel;->j()LX/1vs;

    move-result-object v5

    iget-object v6, v5, LX/1vs;->a:LX/15i;

    iget v7, v5, LX/1vs;->b:I

    .line 2662524
    sget-object v5, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2662525
    if-nez v7, :cond_4

    .line 2662526
    const-class v3, LX/JCY;

    const-string v5, "Missing page info"

    invoke-static {v3, v5}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    move v5, v4

    move-object v4, v0

    .line 2662527
    :goto_2
    new-instance v0, LX/B0i;

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, LX/B0i;-><init>(Lcom/facebook/flatbuffers/MutableFlattenable;LX/9JH;LX/B0d;Ljava/lang/String;Z)V

    goto :goto_0

    .line 2662528
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2662529
    :cond_4
    invoke-virtual {v6, v7, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    .line 2662530
    invoke-virtual {v6, v7, v3}, LX/15i;->h(II)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v3

    :goto_3
    move-object v4, v5

    move v5, v0

    goto :goto_2

    :cond_5
    move v0, v4

    goto :goto_3
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2662508
    const-string v0, "CollectionsSummaryTail"

    return-object v0
.end method
