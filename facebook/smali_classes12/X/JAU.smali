.class public LX/JAU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/JAU;


# instance fields
.field private final a:Landroid/view/Display;

.field private final b:Landroid/content/res/Resources;

.field private final c:LX/J9L;

.field private final d:LX/0rq;


# direct methods
.method public constructor <init>(Landroid/view/WindowManager;Landroid/content/res/Resources;LX/J9L;LX/0rq;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2655758
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2655759
    invoke-interface {p1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iput-object v0, p0, LX/JAU;->a:Landroid/view/Display;

    .line 2655760
    iput-object p2, p0, LX/JAU;->b:Landroid/content/res/Resources;

    .line 2655761
    iput-object p3, p0, LX/JAU;->c:LX/J9L;

    .line 2655762
    iput-object p4, p0, LX/JAU;->d:LX/0rq;

    .line 2655763
    return-void
.end method

.method public static a(LX/0QB;)LX/JAU;
    .locals 7

    .prologue
    .line 2655745
    sget-object v0, LX/JAU;->e:LX/JAU;

    if-nez v0, :cond_1

    .line 2655746
    const-class v1, LX/JAU;

    monitor-enter v1

    .line 2655747
    :try_start_0
    sget-object v0, LX/JAU;->e:LX/JAU;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2655748
    if-eqz v2, :cond_0

    .line 2655749
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2655750
    new-instance p0, LX/JAU;

    invoke-static {v0}, LX/0q4;->b(LX/0QB;)Landroid/view/WindowManager;

    move-result-object v3

    check-cast v3, Landroid/view/WindowManager;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {v0}, LX/J9L;->a(LX/0QB;)LX/J9L;

    move-result-object v5

    check-cast v5, LX/J9L;

    invoke-static {v0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v6

    check-cast v6, LX/0rq;

    invoke-direct {p0, v3, v4, v5, v6}, LX/JAU;-><init>(Landroid/view/WindowManager;Landroid/content/res/Resources;LX/J9L;LX/0rq;)V

    .line 2655751
    move-object v0, p0

    .line 2655752
    sput-object v0, LX/JAU;->e:LX/JAU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2655753
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2655754
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2655755
    :cond_1
    sget-object v0, LX/JAU;->e:LX/JAU;

    return-object v0

    .line 2655756
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2655757
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 2655742
    iget-object v0, p0, LX/JAU;->a:Landroid/view/Display;

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v0

    iget-object v1, p0, LX/JAU;->c:LX/J9L;

    .line 2655743
    iget p0, v1, LX/J9L;->a:I

    move v1, p0

    .line 2655744
    div-int/2addr v0, v1

    return v0
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 2655741
    iget-object v0, p0, LX/JAU;->b:Landroid/content/res/Resources;

    const v1, 0x7f0b0dc6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 2655740
    iget-object v0, p0, LX/JAU;->b:Landroid/content/res/Resources;

    const v1, 0x7f0b0dc7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method
