.class public LX/Hd8;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/2g9;

.field public final b:LX/HdH;


# direct methods
.method public constructor <init>(LX/2g9;LX/HdH;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2487899
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2487900
    iput-object p1, p0, LX/Hd8;->a:LX/2g9;

    .line 2487901
    iput-object p2, p0, LX/Hd8;->b:LX/HdH;

    .line 2487902
    return-void
.end method

.method public static a(LX/0QB;)LX/Hd8;
    .locals 6

    .prologue
    .line 2487903
    const-class v1, LX/Hd8;

    monitor-enter v1

    .line 2487904
    :try_start_0
    sget-object v0, LX/Hd8;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2487905
    sput-object v2, LX/Hd8;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2487906
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2487907
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2487908
    new-instance v5, LX/Hd8;

    invoke-static {v0}, LX/2g9;->a(LX/0QB;)LX/2g9;

    move-result-object v3

    check-cast v3, LX/2g9;

    .line 2487909
    new-instance p0, LX/HdH;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-direct {p0, v4}, LX/HdH;-><init>(LX/0tX;)V

    .line 2487910
    move-object v4, p0

    .line 2487911
    check-cast v4, LX/HdH;

    invoke-direct {v5, v3, v4}, LX/Hd8;-><init>(LX/2g9;LX/HdH;)V

    .line 2487912
    move-object v0, v5

    .line 2487913
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2487914
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Hd8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2487915
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2487916
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
