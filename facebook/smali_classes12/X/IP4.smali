.class public final LX/IP4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;)V
    .locals 0

    .prologue
    .line 2573679
    iput-object p1, p0, LX/IP4;->a:Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x5797a213

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2573680
    iget-object v1, p0, LX/IP4;->a:Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;

    .line 2573681
    iget-boolean v3, v1, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->r:Z

    if-eqz v3, :cond_0

    .line 2573682
    iget-object v3, v1, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->b:LX/3mF;

    iget-object v4, v1, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->s:Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;

    invoke-virtual {v4}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;->a()Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;->m()Ljava/lang/String;

    move-result-object v4

    const-string v5, "suggested_groups"

    invoke-virtual {v3, v4, v5}, LX/3mF;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 2573683
    :goto_0
    iget-object v4, v1, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->s:Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;

    invoke-static {v4}, LX/IOM;->a(Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;)LX/IOM;

    move-result-object v4

    iget-object v5, v1, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->s:Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;

    invoke-virtual {v5}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;->a()Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;

    move-result-object v5

    invoke-static {v5}, LX/ION;->a(Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;)LX/ION;

    move-result-object v5

    iget-object v6, v1, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->s:Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;

    invoke-virtual {v6}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;->a()Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;->o()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v6

    .line 2573684
    sget-object p0, LX/IP7;->a:[I

    invoke-virtual {v6}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->ordinal()I

    move-result p1

    aget p0, p0, p1

    packed-switch p0, :pswitch_data_0

    .line 2573685
    :goto_1
    move-object v6, v6

    .line 2573686
    iput-object v6, v5, LX/ION;->g:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 2573687
    move-object v5, v5

    .line 2573688
    invoke-virtual {v5}, LX/ION;->a()Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;

    move-result-object v5

    .line 2573689
    iput-object v5, v4, LX/IOM;->a:Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;

    .line 2573690
    move-object v4, v4

    .line 2573691
    invoke-virtual {v4}, LX/IOM;->a()Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;

    move-result-object v4

    .line 2573692
    iget-object v5, v1, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->s:Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;

    .line 2573693
    iget-boolean v6, v1, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->r:Z

    .line 2573694
    const/4 p0, 0x0

    invoke-virtual {v1, v4, p0}, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->a(Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;Z)V

    .line 2573695
    new-instance v4, LX/IP6;

    invoke-direct {v4, v1, v5, v6}, LX/IP6;-><init>(Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;Z)V

    iget-object v5, v1, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->c:Ljava/util/concurrent/ExecutorService;

    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2573696
    const v1, -0x53beeb1c

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2573697
    :cond_0
    iget-object v3, v1, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->b:LX/3mF;

    iget-object v4, v1, Lcom/facebook/groups/fb4a/discover/FB4ADiscoverRowView;->s:Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;

    invoke-virtual {v4}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel;->a()Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/groups/discover/protocol/FetchSuggestedGroupsModels$FB4ADiscoverRowDataModel$NodeModel;->m()Ljava/lang/String;

    move-result-object v4

    const-string v5, "suggested_groups"

    const-string v6, "ALLOW_READD"

    invoke-virtual {v3, v4, v5, v6}, LX/3mF;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    goto :goto_0

    .line 2573698
    :pswitch_0
    sget-object v6, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    goto :goto_1

    .line 2573699
    :pswitch_1
    sget-object v6, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    goto :goto_1

    .line 2573700
    :pswitch_2
    sget-object v6, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CAN_JOIN:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    goto :goto_1

    .line 2573701
    :pswitch_3
    sget-object v6, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
