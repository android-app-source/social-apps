.class public LX/JIr;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/JIg;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/JIh;",
            ">;"
        }
    .end annotation
.end field

.field public volatile c:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/JIz;",
            ">;"
        }
    .end annotation
.end field

.field public volatile d:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/entitycardsplugins/discoverycuration/presenters/DiscoveryMediaGridBinder;",
            ">;"
        }
    .end annotation
.end field

.field public volatile e:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/JJ0;",
            ">;"
        }
    .end annotation
.end field

.field public volatile f:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Fxj;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/JIF;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public h:LX/JIC;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public i:LX/EpO;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private final j:LX/1De;

.field public final k:LX/EnL;

.field public final l:LX/Emj;

.field private final m:LX/JIw;

.field public n:LX/EpN;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/EpN",
            "<",
            "Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLInterfaces$DiscoveryCardActionFields;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Landroid/graphics/drawable/Drawable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileSectionFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1De;LX/EnL;LX/Emj;LX/JIw;)V
    .locals 0
    .param p1    # LX/1De;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/EnL;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/Emj;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/JIw;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2678390
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2678391
    iput-object p1, p0, LX/JIr;->j:LX/1De;

    .line 2678392
    iput-object p2, p0, LX/JIr;->k:LX/EnL;

    .line 2678393
    iput-object p3, p0, LX/JIr;->l:LX/Emj;

    .line 2678394
    iput-object p4, p0, LX/JIr;->m:LX/JIw;

    .line 2678395
    return-void
.end method

.method private a(Lcom/facebook/components/ComponentView;LX/1X1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/components/ComponentView;",
            "LX/1X1",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 2678396
    iget-object v0, p1, Lcom/facebook/components/ComponentView;->a:LX/1dV;

    move-object v0, v0

    .line 2678397
    if-nez v0, :cond_0

    .line 2678398
    iget-object v0, p0, LX/JIr;->j:LX/1De;

    invoke-static {v0, p2}, LX/1cy;->a(LX/1De;LX/1X1;)LX/1me;

    move-result-object v0

    invoke-virtual {v0}, LX/1me;->b()LX/1dV;

    move-result-object v0

    .line 2678399
    invoke-virtual {p1, v0}, Lcom/facebook/components/ComponentView;->setComponent(LX/1dV;)V

    .line 2678400
    :goto_0
    return-void

    .line 2678401
    :cond_0
    invoke-virtual {v0, p2}, LX/1dV;->a(LX/1X1;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2678402
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2678403
    packed-switch p2, :pswitch_data_0

    .line 2678404
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected viewType: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2678405
    :pswitch_0
    new-instance v1, Lcom/facebook/components/ComponentView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/facebook/components/ComponentView;-><init>(Landroid/content/Context;)V

    .line 2678406
    new-instance v0, LX/JIo;

    invoke-direct {v0, v1}, LX/JIo;-><init>(Lcom/facebook/components/ComponentView;)V

    .line 2678407
    :goto_0
    return-object v0

    .line 2678408
    :pswitch_1
    const v1, 0x7f03042d

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/entitycardsplugins/person/widget/actionbar/PersonCardActionBarView;

    .line 2678409
    invoke-virtual {v0, v3, v3, v2}, LX/AhO;->a(ZZI)V

    .line 2678410
    new-instance v1, LX/JIi;

    invoke-direct {v1, v0}, LX/JIi;-><init>(Lcom/facebook/entitycardsplugins/person/widget/actionbar/PersonCardActionBarView;)V

    move-object v0, v1

    goto :goto_0

    .line 2678411
    :pswitch_2
    const v1, 0x7f0314c7

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/header/intro/bio/TimelineHeaderBioView;

    .line 2678412
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0a00d5

    invoke-static {v2, v3}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {v0, v1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2678413
    new-instance v1, LX/JIj;

    invoke-direct {v1, v0}, LX/JIj;-><init>(Lcom/facebook/timeline/header/intro/bio/TimelineHeaderBioView;)V

    move-object v0, v1

    goto :goto_0

    .line 2678414
    :pswitch_3
    const v1, 0x7f030435

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2678415
    new-instance v0, LX/JIp;

    invoke-direct {v0, v1}, LX/JIp;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 2678416
    :pswitch_4
    const v1, 0x7f030430

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/components/ComponentView;

    .line 2678417
    new-instance v1, LX/JIk;

    invoke-direct {v1, v0}, LX/JIk;-><init>(Lcom/facebook/components/ComponentView;)V

    move-object v0, v1

    goto :goto_0

    .line 2678418
    :pswitch_5
    const v1, 0x7f0314de

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/mediagrid/FigMediaGrid;

    .line 2678419
    iget-object v1, p0, LX/JIr;->f:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Fxj;

    .line 2678420
    new-instance v2, LX/JIm;

    iget-object v3, p0, LX/JIr;->m:LX/JIw;

    invoke-direct {v2, v0, v3, v1}, LX/JIm;-><init>(Lcom/facebook/fig/mediagrid/FigMediaGrid;LX/JIw;LX/Fxj;)V

    move-object v0, v2

    goto :goto_0

    .line 2678421
    :pswitch_6
    const v1, 0x7f030436

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2678422
    iget-object v1, p0, LX/JIr;->e:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/JJ0;

    .line 2678423
    new-instance v2, LX/JIq;

    iget-object v3, p0, LX/JIr;->m:LX/JIw;

    invoke-direct {v2, v0, v3, v1}, LX/JIq;-><init>(Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;LX/JIw;LX/JJ0;)V

    move-object v0, v2

    goto/16 :goto_0

    .line 2678424
    :pswitch_7
    const v1, 0x7f030f31

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/entitycardsplugins/person/widget/footer/PersonCardFigButtonFooterView;

    .line 2678425
    new-instance v1, LX/JIn;

    iget-object v2, p0, LX/JIr;->m:LX/JIw;

    invoke-direct {v1, v0, v2}, LX/JIn;-><init>(Lcom/facebook/entitycardsplugins/person/widget/footer/PersonCardFigButtonFooterView;LX/JIw;)V

    move-object v0, v1

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public final a(LX/1a1;I)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 2678426
    iget-object v0, p0, LX/JIr;->o:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    const-string v1, "called onBindViewHolder before setCard"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2678427
    iget-object v0, p0, LX/JIr;->o:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    .line 2678428
    invoke-static {v0}, LX/JIT;->a(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;)I

    move-result v1

    .line 2678429
    invoke-static {p2}, LX/JIT;->a(I)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {p2}, LX/JIT;->b(I)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {p2, v1}, LX/JIT;->a(II)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 2678430
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->c()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    move-result-object v1

    .line 2678431
    :goto_0
    move-object v0, v1

    .line 2678432
    instance-of v1, v0, LX/FSx;

    if-eqz v1, :cond_7

    .line 2678433
    instance-of v1, p1, LX/JIo;

    if-eqz v1, :cond_3

    .line 2678434
    check-cast p1, LX/JIo;

    .line 2678435
    iget-object v0, p0, LX/JIr;->g:LX/JIF;

    iget-object v1, p0, LX/JIr;->j:LX/1De;

    const/4 v2, 0x0

    .line 2678436
    new-instance v3, LX/JIE;

    invoke-direct {v3, v0}, LX/JIE;-><init>(LX/JIF;)V

    .line 2678437
    sget-object v4, LX/JIF;->a:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/JID;

    .line 2678438
    if-nez v4, :cond_1

    .line 2678439
    new-instance v4, LX/JID;

    invoke-direct {v4}, LX/JID;-><init>()V

    .line 2678440
    :cond_1
    invoke-static {v4, v1, v2, v2, v3}, LX/JID;->a$redex0(LX/JID;LX/1De;IILX/JIE;)V

    .line 2678441
    move-object v3, v4

    .line 2678442
    move-object v2, v3

    .line 2678443
    move-object v0, v2

    .line 2678444
    iget-object v1, p0, LX/JIr;->o:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    .line 2678445
    iget-object v2, v0, LX/JID;->a:LX/JIE;

    iput-object v1, v2, LX/JIE;->a:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    .line 2678446
    iget-object v2, v0, LX/JID;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2678447
    move-object v0, v0

    .line 2678448
    iget-object v1, p0, LX/JIr;->p:Landroid/graphics/drawable/Drawable;

    .line 2678449
    iget-object v2, v0, LX/JID;->a:LX/JIE;

    iput-object v1, v2, LX/JIE;->b:Landroid/graphics/drawable/Drawable;

    .line 2678450
    iget-object v2, v0, LX/JID;->d:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2678451
    move-object v0, v0

    .line 2678452
    iget-object v1, p0, LX/JIr;->l:LX/Emj;

    .line 2678453
    iget-object v2, v0, LX/JID;->a:LX/JIE;

    iput-object v1, v2, LX/JIE;->c:LX/Emj;

    .line 2678454
    iget-object v2, v0, LX/JID;->d:Ljava/util/BitSet;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2678455
    move-object v0, v0

    .line 2678456
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 2678457
    iget-object v1, p1, LX/JIo;->l:Lcom/facebook/components/ComponentView;

    invoke-direct {p0, v1, v0}, LX/JIr;->a(Lcom/facebook/components/ComponentView;LX/1X1;)V

    .line 2678458
    :cond_2
    :goto_1
    return-void

    .line 2678459
    :cond_3
    instance-of v1, p1, LX/JIi;

    if-eqz v1, :cond_5

    .line 2678460
    check-cast p1, LX/JIi;

    .line 2678461
    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    .line 2678462
    iget-object v4, p0, LX/JIr;->n:LX/EpN;

    if-nez v4, :cond_4

    .line 2678463
    iget-object v4, p0, LX/JIr;->b:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/JIh;

    .line 2678464
    iget-object v4, p0, LX/JIr;->a:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, LX/JIg;

    .line 2678465
    iget-object v4, p0, LX/JIr;->i:LX/EpO;

    iget-object v5, p0, LX/JIr;->l:LX/Emj;

    iget-object v6, p0, LX/JIr;->k:LX/EnL;

    sget-object v7, LX/2h7;->FRIENDING_CARD:LX/2h7;

    sget-object v8, LX/5P2;->FRIENDING_CARD:LX/5P2;

    invoke-virtual/range {v4 .. v10}, LX/EpO;->a(LX/Emj;LX/EnL;LX/2h7;LX/5P2;LX/BRt;LX/EpB;)LX/EpN;

    move-result-object v4

    iput-object v4, p0, LX/JIr;->n:LX/EpN;

    .line 2678466
    :cond_4
    iget-object v4, p0, LX/JIr;->n:LX/EpN;

    move-object v1, v4

    .line 2678467
    iget-object v2, p1, LX/JIi;->l:Lcom/facebook/entitycardsplugins/person/widget/actionbar/PersonCardActionBarView;

    invoke-virtual {v1, v0, v2}, LX/EpN;->a(LX/5wM;Lcom/facebook/entitycardsplugins/person/widget/actionbar/PersonCardActionBarView;)V

    goto :goto_1

    .line 2678468
    :cond_5
    instance-of v0, p1, LX/JIn;

    if-eqz v0, :cond_6

    .line 2678469
    check-cast p1, LX/JIn;

    .line 2678470
    iget-object v0, p1, LX/JIn;->l:Lcom/facebook/entitycardsplugins/person/widget/footer/PersonCardFigButtonFooterView;

    invoke-static {v0, p1}, LX/Epp;->a(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 2678471
    :cond_6
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected ViewHolder "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " at position "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2678472
    :cond_7
    instance-of v1, v0, LX/174;

    if-eqz v1, :cond_8

    .line 2678473
    instance-of v1, p1, LX/JIj;

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 2678474
    check-cast v0, LX/174;

    .line 2678475
    check-cast p1, LX/JIj;

    .line 2678476
    iget-object v1, p1, LX/JIj;->l:Lcom/facebook/timeline/header/intro/bio/TimelineHeaderBioView;

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0, v2, v2}, Lcom/facebook/timeline/header/intro/bio/TimelineHeaderBioView;->a(Ljava/lang/CharSequence;ZZ)V

    goto :goto_1

    .line 2678477
    :cond_8
    sget-object v1, LX/JIT;->a:Ljava/lang/Object;

    if-eq v0, v1, :cond_2

    .line 2678478
    instance-of v1, v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryContextItemFieldsModel;

    if-eqz v1, :cond_a

    .line 2678479
    instance-of v1, p1, LX/JIk;

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 2678480
    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryContextItemFieldsModel;

    .line 2678481
    check-cast p1, LX/JIk;

    .line 2678482
    iget-object v1, p0, LX/JIr;->h:LX/JIC;

    iget-object v2, p0, LX/JIr;->j:LX/1De;

    const/4 v3, 0x0

    .line 2678483
    new-instance v4, LX/JIB;

    invoke-direct {v4, v1}, LX/JIB;-><init>(LX/JIC;)V

    .line 2678484
    sget-object v5, LX/JIC;->a:LX/0Zi;

    invoke-virtual {v5}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/JIA;

    .line 2678485
    if-nez v5, :cond_9

    .line 2678486
    new-instance v5, LX/JIA;

    invoke-direct {v5}, LX/JIA;-><init>()V

    .line 2678487
    :cond_9
    invoke-static {v5, v2, v3, v3, v4}, LX/JIA;->a$redex0(LX/JIA;LX/1De;IILX/JIB;)V

    .line 2678488
    move-object v4, v5

    .line 2678489
    move-object v3, v4

    .line 2678490
    move-object v1, v3

    .line 2678491
    iget-object v2, v1, LX/JIA;->a:LX/JIB;

    iput-object v0, v2, LX/JIB;->a:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryContextItemFieldsModel;

    .line 2678492
    iget-object v2, v1, LX/JIA;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2678493
    move-object v0, v1

    .line 2678494
    iget-object v1, p0, LX/JIr;->o:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->c()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    move-result-object v1

    .line 2678495
    iget-object v2, v0, LX/JIA;->a:LX/JIB;

    iput-object v1, v2, LX/JIB;->f:LX/Eoo;

    .line 2678496
    iget-object v2, v0, LX/JIA;->d:Ljava/util/BitSet;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2678497
    move-object v0, v0

    .line 2678498
    iget-object v1, p0, LX/JIr;->o:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    invoke-static {v1, p2}, LX/JIT;->c(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;I)Z

    move-result v1

    .line 2678499
    iget-object v2, v0, LX/JIA;->a:LX/JIB;

    iput-boolean v1, v2, LX/JIB;->b:Z

    .line 2678500
    iget-object v2, v0, LX/JIA;->d:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2678501
    move-object v0, v0

    .line 2678502
    iget-object v1, p0, LX/JIr;->o:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    .line 2678503
    invoke-static {v1, p2}, LX/JIT;->c(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;I)Z

    move-result v2

    if-eqz v2, :cond_17

    invoke-static {v1}, LX/JIT;->c(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;)I

    move-result v2

    sub-int v2, p2, v2

    invoke-static {v1}, LX/JIT;->e(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;)I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne v2, v3, :cond_17

    const/4 v2, 0x1

    :goto_2
    move v1, v2

    .line 2678504
    iget-object v2, v0, LX/JIA;->a:LX/JIB;

    iput-boolean v1, v2, LX/JIB;->c:Z

    .line 2678505
    iget-object v2, v0, LX/JIA;->d:Ljava/util/BitSet;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2678506
    move-object v0, v0

    .line 2678507
    iget-object v1, p0, LX/JIr;->o:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    .line 2678508
    invoke-static {v1, p2}, LX/JIT;->c(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;I)Z

    move-result v2

    if-nez v2, :cond_18

    invoke-static {v1}, LX/JIT;->b(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;)I

    move-result v2

    if-ne p2, v2, :cond_18

    const/4 v2, 0x1

    :goto_3
    move v1, v2

    .line 2678509
    iget-object v2, v0, LX/JIA;->a:LX/JIB;

    iput-boolean v1, v2, LX/JIB;->d:Z

    .line 2678510
    iget-object v2, v0, LX/JIA;->d:Ljava/util/BitSet;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2678511
    move-object v0, v0

    .line 2678512
    iget-object v1, p0, LX/JIr;->o:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    .line 2678513
    invoke-static {v1, p2}, LX/JIT;->c(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;I)Z

    move-result v2

    if-nez v2, :cond_19

    invoke-static {v1}, LX/JIT;->b(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;)I

    move-result v2

    invoke-static {v1}, LX/JIT;->f(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/lit8 v2, v2, -0x1

    if-ne p2, v2, :cond_19

    const/4 v2, 0x1

    :goto_4
    move v1, v2

    .line 2678514
    iget-object v2, v0, LX/JIA;->a:LX/JIB;

    iput-boolean v1, v2, LX/JIB;->e:Z

    .line 2678515
    iget-object v2, v0, LX/JIA;->d:Ljava/util/BitSet;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2678516
    move-object v0, v0

    .line 2678517
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 2678518
    iget-object v1, p1, LX/JIk;->l:Lcom/facebook/components/ComponentView;

    invoke-direct {p0, v1, v0}, LX/JIr;->a(Lcom/facebook/components/ComponentView;LX/1X1;)V

    .line 2678519
    iget-object v0, p0, LX/JIr;->l:LX/Emj;

    const-string v1, "ec_config_context_rows"

    sget-object v2, LX/EnC;->SUCCEEDED:LX/EnC;

    iget-object v3, p0, LX/JIr;->o:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    invoke-virtual {v3}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->c()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->c()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, LX/Emj;->a(Ljava/lang/String;LX/EnC;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2678520
    :cond_a
    instance-of v1, v0, Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotosFieldsModel$FavoritePhotosModel;

    if-eqz v1, :cond_b

    .line 2678521
    check-cast v0, Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotosFieldsModel$FavoritePhotosModel;

    .line 2678522
    check-cast p1, LX/JIm;

    .line 2678523
    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotosFieldsModel$FavoritePhotosModel;->a()LX/0Px;

    move-result-object v1

    .line 2678524
    iput-object v1, p1, LX/JIm;->o:LX/0Px;

    .line 2678525
    iget-object v1, p0, LX/JIr;->f:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Fxj;

    .line 2678526
    iget-object v2, p0, LX/JIr;->d:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/entitycardsplugins/discoverycuration/presenters/DiscoveryMediaGridBinder;

    .line 2678527
    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotosFieldsModel$FavoritePhotosModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v2, v0, v1, p1}, Lcom/facebook/entitycardsplugins/discoverycuration/presenters/DiscoveryMediaGridBinder;->a(LX/0Px;LX/Fx7;LX/JIl;)V

    goto/16 :goto_1

    .line 2678528
    :cond_b
    sget-object v1, LX/JIT;->b:Ljava/lang/Object;

    if-ne v0, v1, :cond_10

    .line 2678529
    check-cast p1, LX/JIq;

    .line 2678530
    iget-object v0, p0, LX/JIr;->q:Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileSectionFieldsModel;

    if-nez v0, :cond_c

    .line 2678531
    iget-object v0, p1, LX/JIq;->l:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 2678532
    iget-object v0, p0, LX/JIr;->m:LX/JIw;

    .line 2678533
    iget-boolean v4, v0, LX/JIw;->r:Z

    if-eqz v4, :cond_1a

    .line 2678534
    :goto_5
    goto/16 :goto_1

    .line 2678535
    :cond_c
    iget-boolean v0, p1, LX/JIq;->t:Z

    if-nez v0, :cond_d

    .line 2678536
    iget-object v0, p1, LX/JIq;->l:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const v1, 0x7f030438

    invoke-virtual {v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setContentLayout(I)V

    .line 2678537
    iget-object v0, p1, LX/JIq;->l:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const v1, 0x7f0d0cce

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;

    iput-object v0, p1, LX/JIq;->p:Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;

    .line 2678538
    iget-object v0, p1, LX/JIq;->l:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const v1, 0x7f0d0ccf

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/mediagrid/FigMediaGrid;

    iput-object v0, p1, LX/JIq;->q:Lcom/facebook/fig/mediagrid/FigMediaGrid;

    .line 2678539
    iget-object v0, p1, LX/JIq;->l:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const v1, 0x7f0d0cd0

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protiles/views/ProtilesFooterView;

    iput-object v0, p1, LX/JIq;->r:Lcom/facebook/timeline/protiles/views/ProtilesFooterView;

    .line 2678540
    iget-object v0, p1, LX/JIq;->r:Lcom/facebook/timeline/protiles/views/ProtilesFooterView;

    const v1, 0x7f0d279d

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, LX/JIq;->s:Landroid/widget/TextView;

    .line 2678541
    iget-object v0, p1, LX/JIq;->p:Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;

    invoke-virtual {v0, p1}, Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2678542
    iget-object v0, p1, LX/JIq;->q:Lcom/facebook/fig/mediagrid/FigMediaGrid;

    invoke-virtual {v0, p1}, Lcom/facebook/fig/mediagrid/FigMediaGrid;->a(LX/6WM;)Lcom/facebook/fig/mediagrid/FigMediaGrid;

    .line 2678543
    iget-object v0, p1, LX/JIq;->r:Lcom/facebook/timeline/protiles/views/ProtilesFooterView;

    invoke-virtual {v0, p1}, Lcom/facebook/timeline/protiles/views/ProtilesFooterView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2678544
    const/4 v0, 0x1

    iput-boolean v0, p1, LX/JIq;->t:Z

    .line 2678545
    :cond_d
    iget-object v0, p1, LX/JIq;->l:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 2678546
    iget-object v0, p0, LX/JIr;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JIz;

    .line 2678547
    iget-object v1, p0, LX/JIr;->q:Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileSectionFieldsModel;

    const/4 v3, 0x0

    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 2678548
    iput-object v1, p1, LX/JIq;->o:Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileSectionFieldsModel;

    .line 2678549
    iget-boolean v2, p1, LX/JIq;->t:Z

    const-string v4, "called getHeaderView before inflating"

    invoke-static {v2, v4}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2678550
    iget-object v2, p1, LX/JIq;->p:Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;

    move-object v4, v2

    .line 2678551
    iget-object v2, p1, LX/JIq;->u:Landroid/graphics/drawable/Drawable;

    move-object v2, v2

    .line 2678552
    if-nez v2, :cond_e

    .line 2678553
    iget-object v2, v0, LX/JIz;->c:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/G1G;

    .line 2678554
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    invoke-virtual {v2, v5}, LX/G1G;->a(Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 2678555
    iput-object v2, p1, LX/JIq;->u:Landroid/graphics/drawable/Drawable;

    .line 2678556
    :cond_e
    iget-object v2, p1, LX/JIq;->u:Landroid/graphics/drawable/Drawable;

    move-object v2, v2

    .line 2678557
    invoke-virtual {v4, v2}, Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2678558
    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileSectionFieldsModel;->d()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel$TitleModel;

    move-result-object v2

    if-eqz v2, :cond_1b

    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileSectionFieldsModel;->d()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel$TitleModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel$TitleModel;->a()Ljava/lang/String;

    move-result-object v2

    .line 2678559
    :goto_6
    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileSectionFieldsModel;->c()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel$SubtitleModel;

    move-result-object v5

    if-eqz v5, :cond_f

    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileSectionFieldsModel;->c()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel$SubtitleModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionHeaderFieldsModel$SubtitleModel;->a()Ljava/lang/String;

    move-result-object v3

    :cond_f
    invoke-virtual {v4, v2, v3}, Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2678560
    invoke-virtual {v4, v6}, Lcom/facebook/timeline/protiles/views/ProtilesHeaderView;->a(Z)V

    .line 2678561
    const/4 v3, 0x0

    .line 2678562
    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileSectionFieldsModel;->b()Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileSectionFieldsModel$ProfileTileViewsModel;

    move-result-object v2

    if-eqz v2, :cond_1e

    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileSectionFieldsModel;->b()Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileSectionFieldsModel$ProfileTileViewsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileSectionFieldsModel$ProfileTileViewsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1e

    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileSectionFieldsModel;->b()Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileSectionFieldsModel$ProfileTileViewsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileSectionFieldsModel$ProfileTileViewsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileSectionFieldsModel$ProfileTileViewsModel$NodesModel;

    invoke-virtual {v2}, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileSectionFieldsModel$ProfileTileViewsModel$NodesModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileSectionFieldsModel$ProfileTileViewsModel$NodesModel$ProfileTileItemsModel;

    move-result-object v2

    if-eqz v2, :cond_1e

    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileSectionFieldsModel;->b()Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileSectionFieldsModel$ProfileTileViewsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileSectionFieldsModel$ProfileTileViewsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileSectionFieldsModel$ProfileTileViewsModel$NodesModel;

    invoke-virtual {v2}, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileSectionFieldsModel$ProfileTileViewsModel$NodesModel;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileSectionFieldsModel$ProfileTileViewsModel$NodesModel$ProfileTileItemsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileSectionFieldsModel$ProfileTileViewsModel$NodesModel$ProfileTileItemsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1e

    const/4 v2, 0x1

    :goto_7
    move v4, v2

    .line 2678563
    if-eqz v4, :cond_1c

    .line 2678564
    iget-object v2, v0, LX/JIz;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/JJ0;

    .line 2678565
    iget-object v3, v0, LX/JIz;->a:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/entitycardsplugins/discoverycuration/presenters/DiscoveryMediaGridBinder;

    .line 2678566
    invoke-virtual {p1, v6}, LX/JIq;->c(I)V

    .line 2678567
    invoke-static {v1}, LX/JJ0;->a(Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileSectionFieldsModel;)LX/0Px;

    move-result-object v5

    invoke-virtual {v3, v5, v2, p1}, Lcom/facebook/entitycardsplugins/discoverycuration/presenters/DiscoveryMediaGridBinder;->a(LX/0Px;LX/Fx7;LX/JIl;)V

    .line 2678568
    :goto_8
    iget-boolean v2, p1, LX/JIq;->t:Z

    const-string v3, "called getFooterView before inflating"

    invoke-static {v2, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2678569
    iget-object v2, p1, LX/JIq;->r:Lcom/facebook/timeline/protiles/views/ProtilesFooterView;

    move-object v2, v2

    .line 2678570
    iput-boolean v6, v2, Lcom/facebook/timeline/protiles/views/ProtilesFooterView;->d:Z

    .line 2678571
    if-eqz v4, :cond_1d

    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileSectionFieldsModel;->a()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFooterFieldsModel$FooterModel;

    move-result-object v3

    if-eqz v3, :cond_1d

    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileSectionFieldsModel;->a()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFooterFieldsModel$FooterModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFooterFieldsModel$FooterModel;->a()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1d

    .line 2678572
    invoke-virtual {p1}, LX/JIq;->A()Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryPhotoProtileGraphQLModels$DiscoveryPhotoProtileSectionFieldsModel;->a()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFooterFieldsModel$FooterModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileSectionFooterFieldsModel$FooterModel;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2678573
    invoke-virtual {v2, v6}, Lcom/facebook/timeline/protiles/views/ProtilesFooterView;->setFooterVisibility(I)V

    .line 2678574
    :goto_9
    goto/16 :goto_1

    .line 2678575
    :cond_10
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected model: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2678576
    :cond_11
    invoke-static {v0, p2}, LX/JIT;->g(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;I)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 2678577
    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->c()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->q()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;->a()LX/174;

    move-result-object v1

    goto/16 :goto_0

    .line 2678578
    :cond_12
    invoke-static {v0, p2}, LX/JIT;->h(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;I)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 2678579
    sget-object v1, LX/JIT;->a:Ljava/lang/Object;

    goto/16 :goto_0

    .line 2678580
    :cond_13
    invoke-static {v0, p2}, LX/JIT;->i(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;I)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 2678581
    invoke-static {v0}, LX/JIT;->h(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;)Z

    move-result v1

    if-eqz v1, :cond_14

    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->c()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->q()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;->c()Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotosFieldsModel$FavoritePhotosModel;

    move-result-object v1

    goto/16 :goto_0

    :cond_14
    sget-object v1, LX/JIT;->b:Ljava/lang/Object;

    goto/16 :goto_0

    .line 2678582
    :cond_15
    invoke-static {v0}, LX/JIT;->c(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;)I

    move-result v1

    .line 2678583
    sub-int v3, p2, v1

    invoke-static {v0}, LX/JIT;->e(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;)I

    move-result v4

    if-ge v3, v4, :cond_16

    .line 2678584
    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->c()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->r()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryMutualityFieldsModel$TimelineContextItemsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryMutualityFieldsModel$TimelineContextItemsModel;->a()LX/0Px;

    move-result-object v3

    sub-int v1, p2, v1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    goto/16 :goto_0

    .line 2678585
    :cond_16
    invoke-virtual {v0}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->c()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->q()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;->b()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryIntroCardFieldsModel$ProfileIntroCardModel$ContextItemsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryIntroCardFieldsModel$ProfileIntroCardModel$ContextItemsModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v0}, LX/JIT;->b(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;)I

    move-result v3

    sub-int v3, p2, v3

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    goto/16 :goto_0

    :cond_17
    const/4 v2, 0x0

    goto/16 :goto_2

    :cond_18
    const/4 v2, 0x0

    goto/16 :goto_3

    :cond_19
    const/4 v2, 0x0

    goto/16 :goto_4

    .line 2678586
    :cond_1a
    const/4 v4, 0x1

    iput-boolean v4, v0, LX/JIw;->r:Z

    .line 2678587
    iget-object v4, v0, LX/JIw;->g:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/DiscoveryCardPhotoProtileFetcher;

    .line 2678588
    iget-object v5, v0, LX/JIw;->m:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    invoke-virtual {v5}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->k()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->c()Ljava/lang/String;

    move-result-object v5

    .line 2678589
    new-instance v7, LX/FU3;

    invoke-direct {v7}, LX/FU3;-><init>()V

    move-object v7, v7

    .line 2678590
    const-string v8, "profile_id"

    invoke-virtual {v7, v8, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v7

    check-cast v7, LX/FU3;

    .line 2678591
    iget-object v8, v4, Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/DiscoveryCardPhotoProtileFetcher;->c:LX/0Or;

    invoke-interface {v8}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/0se;

    .line 2678592
    iget-object v9, v4, Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/DiscoveryCardPhotoProtileFetcher;->b:LX/0Or;

    invoke-interface {v9}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/0rq;

    .line 2678593
    invoke-virtual {v9}, LX/0rq;->c()LX/0wF;

    move-result-object v9

    invoke-virtual {v8, v7, v9}, LX/0se;->a(LX/0gW;LX/0wF;)LX/0gW;

    .line 2678594
    invoke-static {v7}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v7

    sget-object v8, Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/DiscoveryCardPhotoProtileFetcher;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 2678595
    iput-object v8, v7, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2678596
    move-object v7, v7

    .line 2678597
    sget-object v8, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v7, v8}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v7

    sget-object v8, LX/0zS;->a:LX/0zS;

    invoke-virtual {v7, v8}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v7

    const-wide/32 v9, 0x15180

    invoke-virtual {v7, v9, v10}, LX/0zO;->a(J)LX/0zO;

    move-result-object v8

    .line 2678598
    iget-object v7, v4, Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/DiscoveryCardPhotoProtileFetcher;->a:LX/0Or;

    invoke-interface {v7}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0tX;

    .line 2678599
    invoke-virtual {v7, v8}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v7

    .line 2678600
    new-instance v8, LX/JIP;

    invoke-direct {v8, v4, v5}, LX/JIP;-><init>(Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/DiscoveryCardPhotoProtileFetcher;Ljava/lang/String;)V

    .line 2678601
    sget-object v9, LX/131;->INSTANCE:LX/131;

    move-object v9, v9

    .line 2678602
    invoke-static {v7, v8, v9}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    move-object v5, v7

    .line 2678603
    iget-object v4, v0, LX/JIw;->f:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/Executor;

    .line 2678604
    new-instance v6, LX/JIu;

    invoke-direct {v6, v0}, LX/JIu;-><init>(LX/JIw;)V

    invoke-static {v5, v6, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto/16 :goto_5

    :cond_1b
    move-object v2, v3

    .line 2678605
    goto/16 :goto_6

    .line 2678606
    :cond_1c
    invoke-virtual {p1, v7}, LX/JIq;->c(I)V

    goto/16 :goto_8

    .line 2678607
    :cond_1d
    invoke-virtual {p1}, LX/JIq;->A()Landroid/widget/TextView;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2678608
    invoke-virtual {v2, v7}, Lcom/facebook/timeline/protiles/views/ProtilesFooterView;->setFooterVisibility(I)V

    goto/16 :goto_9

    :cond_1e
    move v2, v3

    goto/16 :goto_7
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 2678609
    iget-object v0, p0, LX/JIr;->o:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    const-string v1, "called getItemViewType before setCard"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2678610
    iget-object v0, p0, LX/JIr;->o:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    .line 2678611
    invoke-static {v0}, LX/JIT;->a(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;)I

    move-result v1

    .line 2678612
    invoke-static {p1}, LX/JIT;->a(I)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 2678613
    const/4 v1, 0x0

    .line 2678614
    :goto_0
    move v0, v1

    .line 2678615
    return v0

    .line 2678616
    :cond_0
    invoke-static {p1}, LX/JIT;->b(I)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 2678617
    const/4 v1, 0x1

    goto :goto_0

    .line 2678618
    :cond_1
    invoke-static {v0, p1}, LX/JIT;->g(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;I)Z

    move-result p0

    if-eqz p0, :cond_2

    .line 2678619
    const/4 v1, 0x2

    goto :goto_0

    .line 2678620
    :cond_2
    invoke-static {v0, p1}, LX/JIT;->h(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;I)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 2678621
    const/4 v1, 0x3

    goto :goto_0

    .line 2678622
    :cond_3
    invoke-static {v0, p1}, LX/JIT;->i(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;I)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 2678623
    invoke-static {v0}, LX/JIT;->h(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;)Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    goto :goto_0

    :cond_4
    const/4 v1, 0x6

    goto :goto_0

    .line 2678624
    :cond_5
    invoke-static {p1, v1}, LX/JIT;->a(II)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2678625
    const/4 v1, 0x7

    goto :goto_0

    .line 2678626
    :cond_6
    const/4 v1, 0x4

    goto :goto_0
.end method

.method public final ij_()I
    .locals 2

    .prologue
    .line 2678627
    iget-object v0, p0, LX/JIr;->o:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    const-string v1, "called getItemCount before setCard"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2678628
    iget-object v0, p0, LX/JIr;->o:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    invoke-static {v0}, LX/JIT;->a(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;)I

    move-result v0

    return v0
.end method
