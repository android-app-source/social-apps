.class public final LX/HU0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

.field public final synthetic c:Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;Ljava/lang/String;Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;)V
    .locals 0

    .prologue
    .line 2471422
    iput-object p1, p0, LX/HU0;->c:Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;

    iput-object p2, p0, LX/HU0;->a:Ljava/lang/String;

    iput-object p3, p0, LX/HU0;->b:Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x51993be7

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2471423
    iget-object v1, p0, LX/HU0;->c:Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;

    iget-object v1, v1, Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;->c:LX/HU1;

    iget-object v2, p0, LX/HU0;->a:Ljava/lang/String;

    .line 2471424
    const-string v3, "pages_issues_tab_video_play"

    invoke-static {v1, v3, v2}, LX/HU1;->c(LX/HU1;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 2471425
    iget-object v4, v1, LX/HU1;->a:LX/0Zb;

    invoke-interface {v4, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2471426
    iget-object v1, p0, LX/HU0;->c:Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;

    iget-object v1, v1, Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;->b:LX/D3w;

    iget-object v2, p0, LX/HU0;->b:Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

    iget-object v3, p0, LX/HU0;->c:Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;

    invoke-virtual {v3}, Lcom/facebook/pages/fb4a/politics/PagePoliticalIssueOpinionVideoView;->getContext()Landroid/content/Context;

    move-result-object v3

    sget-object v4, LX/04D;->PAGE_POLITICAL_ISSUES_OPINION_VIDEO:LX/04D;

    invoke-virtual {v1, v2, v3, v4}, LX/D3w;->b(Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;Landroid/content/Context;LX/04D;)V

    .line 2471427
    const v1, -0x41eed768

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
