.class public LX/HSM;
.super LX/3Tf;
.source ""

# interfaces
.implements LX/2ht;


# static fields
.field private static final c:Ljava/lang/Object;


# instance fields
.field private final d:Landroid/content/Context;

.field public e:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "LX/HSK;",
            "LX/HSJ;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/facebook/events/common/EventAnalyticsParams;

.field private g:LX/DBW;

.field public h:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public i:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public j:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/HSK;",
            ">;"
        }
    .end annotation
.end field

.field private l:I

.field public m:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2466902
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/HSM;->c:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/events/common/EventAnalyticsParams;Landroid/content/Context;)V
    .locals 4
    .param p1    # Lcom/facebook/events/common/EventAnalyticsParams;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2466903
    invoke-direct {p0}, LX/3Tf;-><init>()V

    .line 2466904
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/HSM;->j:Ljava/util/Set;

    .line 2466905
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/HSM;->k:Ljava/util/List;

    .line 2466906
    iput v3, p0, LX/HSM;->l:I

    .line 2466907
    iput-object p2, p0, LX/HSM;->d:Landroid/content/Context;

    .line 2466908
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, LX/HSK;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, LX/HSM;->e:Ljava/util/EnumMap;

    .line 2466909
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/HSM;->h:Ljava/util/HashMap;

    .line 2466910
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/HSM;->i:Ljava/util/HashMap;

    .line 2466911
    iget-object v0, p0, LX/HSM;->k:Ljava/util/List;

    const/4 v1, 0x2

    new-array v1, v1, [LX/HSK;

    sget-object v2, LX/HSK;->UPCOMING:LX/HSK;

    aput-object v2, v1, v3

    const/4 v2, 0x1

    sget-object v3, LX/HSK;->PAST:LX/HSK;

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2466912
    iput-object p1, p0, LX/HSM;->f:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2466913
    new-instance v0, LX/HSH;

    invoke-direct {v0, p0}, LX/HSH;-><init>(LX/HSM;)V

    iput-object v0, p0, LX/HSM;->g:LX/DBW;

    .line 2466914
    return-void
.end method

.method private a(IILX/HSL;Landroid/view/View;)Landroid/view/View;
    .locals 7

    .prologue
    .line 2466915
    sget-object v0, LX/HSI;->a:[I

    invoke-virtual {p3}, LX/HSL;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2466916
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No child views for view type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3}, LX/HSL;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2466917
    :pswitch_0
    instance-of v0, p4, LX/DBX;

    if-nez v0, :cond_1

    .line 2466918
    new-instance v6, LX/DBX;

    iget-object v0, p0, LX/HSM;->d:Landroid/content/Context;

    invoke-direct {v6, v0}, LX/DBX;-><init>(Landroid/content/Context;)V

    .line 2466919
    :goto_0
    invoke-virtual {p0, p1, p2}, LX/HSM;->a(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/model/Event;

    move-object v0, v6

    .line 2466920
    check-cast v0, LX/DBX;

    iget-object v2, p0, LX/HSM;->f:Lcom/facebook/events/common/EventAnalyticsParams;

    const/4 v3, 0x0

    iget-object v4, p0, LX/HSM;->g:LX/DBW;

    invoke-virtual {v1}, Lcom/facebook/events/model/Event;->av()Z

    move-result v5

    invoke-virtual/range {v0 .. v5}, LX/DBX;->a(Lcom/facebook/events/model/Event;Lcom/facebook/events/common/EventAnalyticsParams;ZLX/DBW;Z)V

    move-object v0, v6

    .line 2466921
    check-cast v0, LX/DBX;

    iget-object v2, p0, LX/HSM;->j:Ljava/util/Set;

    .line 2466922
    iget-object v3, v1, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v1, v3

    .line 2466923
    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    .line 2466924
    iget-object v2, v0, LX/DBX;->t:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setSelected(Z)V

    .line 2466925
    :goto_1
    return-object v6

    .line 2466926
    :pswitch_1
    instance-of v0, p4, LX/HSa;

    if-nez v0, :cond_0

    .line 2466927
    new-instance v2, LX/HSa;

    iget-object v0, p0, LX/HSM;->d:Landroid/content/Context;

    invoke-direct {v2, v0}, LX/HSa;-><init>(Landroid/content/Context;)V

    .line 2466928
    :goto_2
    iget-object v0, p0, LX/HSM;->k:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HSK;

    move-object v1, v2

    .line 2466929
    check-cast v1, LX/HSa;

    check-cast v1, LX/HSa;

    invoke-direct {p0, v0}, LX/HSM;->b(LX/HSK;)Ljava/lang/String;

    move-result-object v0

    .line 2466930
    iget-object v3, v1, LX/HSa;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2466931
    move-object v6, v2

    .line 2466932
    goto :goto_1

    :cond_0
    move-object v2, p4

    goto :goto_2

    :cond_1
    move-object v6, p4

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(LX/HSL;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2466933
    sget-object v0, LX/HSI;->a:[I

    invoke-virtual {p1}, LX/HSL;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2466934
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid header view type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2466935
    :pswitch_0
    new-instance v0, LX/HSl;

    iget-object v1, p0, LX/HSM;->d:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/HSl;-><init>(Landroid/content/Context;)V

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/HSM;LX/HSK;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 2466936
    sget-object v0, LX/HSI;->b:[I

    invoke-virtual {p1}, LX/HSK;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2466937
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, LX/HSK;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " section type is not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2466938
    :pswitch_0
    iget-object v0, p0, LX/HSM;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0830fb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2466939
    :goto_0
    return-object v0

    .line 2466940
    :pswitch_1
    iget-object v0, p0, LX/HSM;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0830f1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2466941
    :pswitch_2
    iget-object v0, p0, LX/HSM;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0830f2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(LX/HSM;Ljava/lang/String;LX/HSK;)V
    .locals 3

    .prologue
    .line 2466961
    iget-object v0, p0, LX/HSM;->e:Ljava/util/EnumMap;

    invoke-virtual {v0, p2}, Ljava/util/EnumMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2466962
    :cond_0
    :goto_0
    return-void

    .line 2466963
    :cond_1
    iget-object v0, p0, LX/HSM;->e:Ljava/util/EnumMap;

    invoke-virtual {v0, p2}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HSJ;

    iget-object v0, v0, LX/HSJ;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2466964
    iget-object v0, p0, LX/HSM;->e:Ljava/util/EnumMap;

    invoke-virtual {v0, p2}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HSJ;

    iget-object v2, v0, LX/HSJ;->b:Ljava/util/List;

    .line 2466965
    sget-object v0, LX/HSK;->UPCOMING:LX/HSK;

    if-ne p2, v0, :cond_3

    iget-object v0, p0, LX/HSM;->h:Ljava/util/HashMap;

    move-object v1, v0

    .line 2466966
    :goto_1
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2466967
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2466968
    invoke-interface {v2, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 2466969
    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 2466970
    const/4 v0, 0x0

    move p0, v0

    :goto_2
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge p0, v0, :cond_2

    .line 2466971
    invoke-interface {v2, p0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/model/Event;

    .line 2466972
    iget-object p1, v0, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v0, p1

    .line 2466973
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v1, v0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2466974
    add-int/lit8 v0, p0, 0x1

    move p0, v0

    goto :goto_2

    .line 2466975
    :cond_2
    goto :goto_0

    .line 2466976
    :cond_3
    iget-object v0, p0, LX/HSM;->i:Ljava/util/HashMap;

    move-object v1, v0

    goto :goto_1
.end method

.method private b(LX/HSK;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 2466942
    sget-object v0, LX/HSI;->b:[I

    invoke-virtual {p1}, LX/HSK;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2466943
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, LX/HSK;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " section type is not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2466944
    :pswitch_0
    iget-object v0, p0, LX/HSM;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0830f3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2466945
    :goto_0
    return-object v0

    .line 2466946
    :pswitch_1
    iget-object v0, p0, LX/HSM;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0830f4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2466947
    :pswitch_2
    iget-object v0, p0, LX/HSM;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0830f5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a(I)I
    .locals 1

    .prologue
    .line 2466948
    sget-object v0, LX/HSL;->TEXT_HEADER_VIEW:LX/HSL;

    invoke-virtual {v0}, LX/HSL;->ordinal()I

    move-result v0

    return v0
.end method

.method public final a(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2466949
    invoke-virtual {p0, p1, p2}, LX/HSM;->c(II)I

    move-result v0

    .line 2466950
    invoke-static {}, LX/HSL;->values()[LX/HSL;

    move-result-object v1

    aget-object v0, v1, v0

    .line 2466951
    invoke-direct {p0, p1, p2, v0, p4}, LX/HSM;->a(IILX/HSL;Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2466952
    invoke-virtual {p0, p1}, LX/HSM;->a(I)I

    move-result v0

    .line 2466953
    invoke-static {}, LX/HSL;->values()[LX/HSL;

    move-result-object v1

    aget-object v0, v1, v0

    .line 2466954
    if-nez p2, :cond_1

    .line 2466955
    invoke-direct {p0, v0}, LX/HSM;->a(LX/HSL;)Landroid/view/View;

    move-result-object v2

    .line 2466956
    :goto_0
    sget-object v1, LX/HSL;->TEXT_HEADER_VIEW:LX/HSL;

    if-ne v0, v1, :cond_0

    .line 2466957
    invoke-virtual {p0, p1}, LX/HSM;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HSJ;

    move-object v1, v2

    .line 2466958
    check-cast v1, LX/HSl;

    iget-object v0, v0, LX/HSJ;->a:Ljava/lang/String;

    .line 2466959
    iget-object p0, v1, LX/HSl;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2466960
    :cond_0
    return-object v2

    :cond_1
    move-object v2, p2

    goto :goto_0
.end method

.method public final a(II)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2466888
    iget-object v0, p0, LX/HSM;->e:Ljava/util/EnumMap;

    iget-object v1, p0, LX/HSM;->k:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HSJ;

    .line 2466889
    iget-object v1, v0, LX/HSJ;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, v0, LX/HSJ;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/HSM;->c:Ljava/lang/Object;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/events/model/Event;)V
    .locals 2

    .prologue
    .line 2466890
    iget-object v0, p0, LX/HSM;->e:Ljava/util/EnumMap;

    sget-object v1, LX/HSK;->UPCOMING:LX/HSK;

    invoke-virtual {v0, v1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HSM;->e:Ljava/util/EnumMap;

    sget-object v1, LX/HSK;->UPCOMING:LX/HSK;

    invoke-virtual {v0, v1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HSJ;

    iget-object v0, v0, LX/HSJ;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2466891
    iget-object v0, p0, LX/HSM;->e:Ljava/util/EnumMap;

    sget-object v1, LX/HSK;->UPCOMING:LX/HSK;

    invoke-virtual {v0, v1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HSJ;

    iget-object v1, v0, LX/HSJ;->b:Ljava/util/List;

    .line 2466892
    iget-object v0, p0, LX/HSM;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2466893
    iget-object v0, p0, LX/HSM;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2466894
    invoke-interface {v1, v0, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2466895
    :cond_0
    iget-object v0, p0, LX/HSM;->e:Ljava/util/EnumMap;

    sget-object v1, LX/HSK;->NEW:LX/HSK;

    invoke-virtual {v0, v1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/HSM;->e:Ljava/util/EnumMap;

    sget-object v1, LX/HSK;->NEW:LX/HSK;

    invoke-virtual {v0, v1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HSJ;

    iget-object v0, v0, LX/HSJ;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2466896
    iget-object v0, p0, LX/HSM;->e:Ljava/util/EnumMap;

    sget-object v1, LX/HSK;->NEW:LX/HSK;

    invoke-virtual {v0, v1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HSJ;

    iget-object v1, v0, LX/HSJ;->b:Ljava/util/List;

    .line 2466897
    iget-object v0, p0, LX/HSM;->i:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2466898
    iget-object v0, p0, LX/HSM;->i:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2466899
    invoke-interface {v1, v0, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2466900
    :cond_1
    const v0, -0x7f664348

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2466901
    return-void
.end method

.method public final b(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2466854
    iget v0, p0, LX/HSM;->m:I

    if-ge p1, v0, :cond_0

    .line 2466855
    new-instance v0, LX/HSl;

    iget-object v1, p0, LX/HSM;->d:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/HSl;-><init>(Landroid/content/Context;)V

    .line 2466856
    :goto_0
    return-object v0

    .line 2466857
    :cond_0
    iget v0, p0, LX/HSM;->m:I

    sub-int v0, p1, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 2466858
    invoke-virtual {p0, v0}, LX/3Tf;->d(I)[I

    move-result-object v0

    aget v0, v0, v1

    .line 2466859
    invoke-virtual {p0, v0, p2, p3}, LX/HSM;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 2466860
    const/4 p3, 0x0

    .line 2466861
    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    const/4 p1, -0x1

    const/4 p2, -0x2

    invoke-direct {v1, p1, p2}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2466862
    invoke-static {p3, p3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {p3, p3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    invoke-virtual {v0, v1, p1}, Landroid/view/View;->measure(II)V

    .line 2466863
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    move v1, v1

    .line 2466864
    iput v1, p0, LX/HSM;->l:I

    goto :goto_0
.end method

.method public final b(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2466865
    iget-object v0, p0, LX/HSM;->e:Ljava/util/EnumMap;

    iget-object v1, p0, LX/HSM;->k:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2466866
    iget-object v0, p0, LX/HSM;->e:Ljava/util/EnumMap;

    invoke-virtual {v0}, Ljava/util/EnumMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HSK;

    .line 2466867
    iget-object v4, p0, LX/HSM;->e:Ljava/util/EnumMap;

    invoke-virtual {v4, v0}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HSJ;

    iget-object v0, v0, LX/HSJ;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 2466868
    goto :goto_0

    .line 2466869
    :cond_0
    if-nez v1, :cond_1

    const/4 v2, 0x1

    :cond_1
    return v2
.end method

.method public final b(II)Z
    .locals 1

    .prologue
    .line 2466870
    const/4 v0, 0x1

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 2466871
    iget-object v0, p0, LX/HSM;->e:Ljava/util/EnumMap;

    invoke-virtual {v0}, Ljava/util/EnumMap;->size()I

    move-result v0

    return v0
.end method

.method public final c(I)I
    .locals 2

    .prologue
    .line 2466872
    iget-object v0, p0, LX/HSM;->e:Ljava/util/EnumMap;

    iget-object v1, p0, LX/HSM;->k:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HSJ;

    .line 2466873
    iget-object v1, v0, LX/HSJ;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, v0, LX/HSJ;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final c(II)I
    .locals 2

    .prologue
    .line 2466874
    iget-object v0, p0, LX/HSM;->e:Ljava/util/EnumMap;

    iget-object v1, p0, LX/HSM;->k:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HSJ;

    .line 2466875
    iget-object v0, v0, LX/HSJ;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/HSL;->NULL_STATE_VIEW:LX/HSL;

    invoke-virtual {v0}, LX/HSL;->ordinal()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    sget-object v0, LX/HSL;->EVENT_ROW_VIEW:LX/HSL;

    invoke-virtual {v0}, LX/HSL;->ordinal()I

    move-result v0

    goto :goto_0
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 2466876
    iget-object v0, p0, LX/HSM;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00d5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    return v0
.end method

.method public final e(I)I
    .locals 1

    .prologue
    .line 2466877
    iget v0, p0, LX/HSM;->m:I

    if-ge p1, v0, :cond_0

    .line 2466878
    const/4 v0, 0x0

    .line 2466879
    :goto_0
    return v0

    :cond_0
    iget v0, p0, LX/HSM;->l:I

    goto :goto_0
.end method

.method public final f(I)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2466880
    iget v2, p0, LX/HSM;->m:I

    sub-int v2, p1, v2

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 2466881
    if-ltz v2, :cond_0

    invoke-virtual {p0}, LX/3Tf;->getCount()I

    move-result v3

    if-lt v2, v3, :cond_2

    :cond_0
    move v0, v1

    .line 2466882
    :cond_1
    :goto_0
    return v0

    .line 2466883
    :cond_2
    invoke-virtual {p0, v2}, LX/3Tf;->d(I)[I

    move-result-object v2

    .line 2466884
    aget v2, v2, v0

    .line 2466885
    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    move v0, v1

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2466886
    invoke-static {}, LX/HSL;->values()[LX/HSL;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public final o_(I)I
    .locals 1

    .prologue
    .line 2466887
    sget-object v0, LX/HSL;->TEXT_HEADER_VIEW:LX/HSL;

    invoke-virtual {v0}, LX/HSL;->ordinal()I

    move-result v0

    return v0
.end method
