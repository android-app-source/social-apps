.class public LX/JS9;
.super LX/3hi;
.source ""


# instance fields
.field private final b:I

.field private final c:LX/1bh;


# direct methods
.method public constructor <init>(I)V
    .locals 2
    .param p1    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2694780
    invoke-direct {p0}, LX/3hi;-><init>()V

    .line 2694781
    iput p1, p0, LX/JS9;->b:I

    .line 2694782
    new-instance v0, LX/1ed;

    iget v1, p0, LX/JS9;->b:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1ed;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/JS9;->c:LX/1bh;

    .line 2694783
    return-void
.end method


# virtual methods
.method public final a()LX/1bh;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2694776
    iget-object v0, p0, LX/JS9;->c:LX/1bh;

    return-object v0
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 2694778
    const/4 v0, 0x2

    iget v1, p0, LX/JS9;->b:I

    invoke-static {p1, v0, v1}, Lcom/facebook/imagepipeline/nativecode/NativeBlurFilter;->a(Landroid/graphics/Bitmap;II)V

    .line 2694779
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2694777
    const-class v0, LX/JS9;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
