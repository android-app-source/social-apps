.class public final LX/IaF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Ia2;


# instance fields
.field public final synthetic a:LX/IaG;


# direct methods
.method public constructor <init>(LX/IaG;)V
    .locals 0

    .prologue
    .line 2590731
    iput-object p1, p0, LX/IaF;->a:LX/IaG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2590732
    iget-object v0, p0, LX/IaF;->a:LX/IaG;

    iget-object v0, v0, LX/IaG;->b:Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;

    invoke-static {v0}, Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;->m(Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;)V

    .line 2590733
    iget-object v0, p0, LX/IaF;->a:LX/IaG;

    iget-object v0, v0, LX/IaG;->b:Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;

    iget-object v0, v0, Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;->d:LX/IZK;

    invoke-virtual {v0}, LX/IZK;->a()V

    .line 2590734
    iget-object v0, p0, LX/IaF;->a:LX/IaG;

    iget-object v0, v0, LX/IaG;->b:Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;

    iget-object v0, v0, Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;->e:LX/IZw;

    const-string v1, "error_resend_sms_code"

    invoke-virtual {v0, v1}, LX/IZw;->a(Ljava/lang/String;)V

    .line 2590735
    return-void
.end method

.method public final a(Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpPhoneVerificationMutationsModels$NativeSignUpSendConfirmationMutationModel;)V
    .locals 2
    .param p1    # Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpPhoneVerificationMutationsModels$NativeSignUpSendConfirmationMutationModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2590736
    iget-object v0, p0, LX/IaF;->a:LX/IaG;

    iget-object v0, v0, LX/IaG;->b:Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;

    invoke-static {v0}, Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;->m(Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;)V

    .line 2590737
    iget-object v0, p0, LX/IaF;->a:LX/IaG;

    iget-object v0, v0, LX/IaG;->b:Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;

    iget-object v0, v0, Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;->e:LX/IZw;

    const-string v1, "success_resend_sms_code"

    invoke-virtual {v0, v1}, LX/IZw;->a(Ljava/lang/String;)V

    .line 2590738
    return-void
.end method
