.class public final LX/Ifc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;

.field public final synthetic b:LX/Ife;


# direct methods
.method public constructor <init>(LX/Ife;Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)V
    .locals 0

    .prologue
    .line 2599879
    iput-object p1, p0, LX/Ifc;->b:LX/Ife;

    iput-object p2, p0, LX/Ifc;->a:Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2599880
    iget-object v0, p0, LX/Ifc;->b:LX/Ife;

    iget-object v0, v0, LX/Ife;->a:LX/Ies;

    iget-object v1, p0, LX/Ifc;->a:Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;

    invoke-virtual {v0, v1}, LX/Ies;->c(Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)V

    .line 2599881
    iget-object v0, p0, LX/Ifc;->b:LX/Ife;

    iget-object v0, v0, LX/Ife;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1CX;

    iget-object v1, p0, LX/Ifc;->b:LX/Ife;

    iget-object v1, v1, LX/Ife;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1CX;

    invoke-static {p1}, Lcom/facebook/fbservice/service/ServiceException;->forException(Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/ServiceException;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1CX;->a(Lcom/facebook/fbservice/service/ServiceException;)LX/4mm;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1CX;->a(LX/4mm;)LX/2EJ;

    .line 2599882
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2599883
    iget-object v0, p0, LX/Ifc;->b:LX/Ife;

    iget-object v0, v0, LX/Ife;->d:LX/IfP;

    const-string v1, "PEOPLE_TAB"

    iget-object v2, p0, LX/Ifc;->a:Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;

    iget-object v2, v2, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;->a:Lcom/facebook/user/model/User;

    .line 2599884
    iget-object p1, v2, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v2, p1

    .line 2599885
    invoke-virtual {v0, v1, v2}, LX/IfP;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2599886
    iget-object v0, p0, LX/Ifc;->b:LX/Ife;

    iget-object v0, v0, LX/Ife;->a:LX/Ies;

    iget-object v1, p0, LX/Ifc;->a:Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;

    invoke-virtual {v0, v1}, LX/Ies;->b(Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)V

    .line 2599887
    return-void
.end method
