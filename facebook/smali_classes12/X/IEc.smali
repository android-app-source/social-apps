.class public final LX/IEc;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 2553021
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2553022
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 2553023
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 2553024
    invoke-static {p0, p1}, LX/IEc;->b(LX/15w;LX/186;)I

    move-result v1

    .line 2553025
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2553026
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2553027
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2553028
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 2553029
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/IEc;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2553030
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2553031
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2553032
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 2553033
    const/4 v10, 0x0

    .line 2553034
    const/4 v9, 0x0

    .line 2553035
    const/4 v8, 0x0

    .line 2553036
    const/4 v7, 0x0

    .line 2553037
    const/4 v6, 0x0

    .line 2553038
    const/4 v5, 0x0

    .line 2553039
    const/4 v4, 0x0

    .line 2553040
    const/4 v3, 0x0

    .line 2553041
    const/4 v2, 0x0

    .line 2553042
    const/4 v1, 0x0

    .line 2553043
    const/4 v0, 0x0

    .line 2553044
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->START_OBJECT:LX/15z;

    if-eq v11, v12, :cond_1

    .line 2553045
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2553046
    const/4 v0, 0x0

    .line 2553047
    :goto_0
    return v0

    .line 2553048
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2553049
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_a

    .line 2553050
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 2553051
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2553052
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 2553053
    const-string v12, "friendship_status"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 2553054
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v10

    goto :goto_1

    .line 2553055
    :cond_2
    const-string v12, "id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 2553056
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 2553057
    :cond_3
    const-string v12, "local_is_pymk_blacklisted"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 2553058
    const/4 v1, 0x1

    .line 2553059
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v8

    goto :goto_1

    .line 2553060
    :cond_4
    const-string v12, "mutual_friends"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 2553061
    invoke-static {p0, p1}, LX/IEa;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 2553062
    :cond_5
    const-string v12, "name"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 2553063
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 2553064
    :cond_6
    const-string v12, "profile_picture"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 2553065
    invoke-static {p0, p1}, LX/3lU;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 2553066
    :cond_7
    const-string v12, "structured_name"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 2553067
    invoke-static {p0, p1}, LX/IEb;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 2553068
    :cond_8
    const-string v12, "subscribe_status"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 2553069
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    goto/16 :goto_1

    .line 2553070
    :cond_9
    const-string v12, "unread_count"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 2553071
    const/4 v0, 0x1

    .line 2553072
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v2

    goto/16 :goto_1

    .line 2553073
    :cond_a
    const/16 v11, 0x9

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 2553074
    const/4 v11, 0x0

    invoke-virtual {p1, v11, v10}, LX/186;->b(II)V

    .line 2553075
    const/4 v10, 0x1

    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 2553076
    if-eqz v1, :cond_b

    .line 2553077
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v8}, LX/186;->a(IZ)V

    .line 2553078
    :cond_b
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 2553079
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 2553080
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 2553081
    const/4 v1, 0x6

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 2553082
    const/4 v1, 0x7

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 2553083
    if-eqz v0, :cond_c

    .line 2553084
    const/16 v0, 0x8

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v2, v1}, LX/186;->a(III)V

    .line 2553085
    :cond_c
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const/4 v3, 0x7

    const/4 v2, 0x0

    .line 2553086
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2553087
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2553088
    if-eqz v0, :cond_0

    .line 2553089
    const-string v0, "friendship_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2553090
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2553091
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2553092
    if-eqz v0, :cond_1

    .line 2553093
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2553094
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2553095
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2553096
    if-eqz v0, :cond_2

    .line 2553097
    const-string v1, "local_is_pymk_blacklisted"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2553098
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2553099
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2553100
    if-eqz v0, :cond_4

    .line 2553101
    const-string v1, "mutual_friends"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2553102
    const/4 v1, 0x0

    .line 2553103
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2553104
    invoke-virtual {p0, v0, v1, v1}, LX/15i;->a(III)I

    move-result v1

    .line 2553105
    if-eqz v1, :cond_3

    .line 2553106
    const-string v4, "count"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2553107
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 2553108
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2553109
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2553110
    if-eqz v0, :cond_5

    .line 2553111
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2553112
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2553113
    :cond_5
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2553114
    if-eqz v0, :cond_6

    .line 2553115
    const-string v1, "profile_picture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2553116
    invoke-static {p0, v0, p2}, LX/3lU;->a(LX/15i;ILX/0nX;)V

    .line 2553117
    :cond_6
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2553118
    if-eqz v0, :cond_7

    .line 2553119
    const-string v1, "structured_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2553120
    invoke-static {p0, v0, p2, p3}, LX/IEb;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2553121
    :cond_7
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 2553122
    if-eqz v0, :cond_8

    .line 2553123
    const-string v0, "subscribe_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2553124
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2553125
    :cond_8
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2553126
    if-eqz v0, :cond_9

    .line 2553127
    const-string v1, "unread_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2553128
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2553129
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2553130
    return-void
.end method
