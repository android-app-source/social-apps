.class public LX/IRI;
.super LX/1Qj;
.source ""

# interfaces
.implements LX/1Pf;


# instance fields
.field private final n:LX/1SX;

.field private final o:LX/1PT;


# direct methods
.method public constructor <init>(LX/IRH;Landroid/content/Context;LX/1PT;Ljava/lang/Runnable;LX/1PY;LX/DOc;LX/DOn;LX/Al0;)V
    .locals 1
    .param p1    # LX/IRH;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/1PT;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Runnable;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/1PY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2576360
    invoke-direct {p0, p2, p4, p5}, LX/1Qj;-><init>(Landroid/content/Context;Ljava/lang/Runnable;LX/1PY;)V

    .line 2576361
    iput-object p3, p0, LX/IRI;->o:LX/1PT;

    .line 2576362
    sget-object v0, LX/IRH;->NORMAL:LX/IRH;

    if-ne p1, v0, :cond_0

    .line 2576363
    invoke-virtual {p6, p0}, LX/DOc;->a(LX/1Qj;)LX/DOb;

    move-result-object v0

    iput-object v0, p0, LX/IRI;->n:LX/1SX;

    .line 2576364
    :goto_0
    return-void

    .line 2576365
    :cond_0
    sget-object v0, LX/IRH;->PENDING:LX/IRH;

    if-ne p1, v0, :cond_1

    .line 2576366
    iput-object p8, p0, LX/IRI;->n:LX/1SX;

    goto :goto_0

    .line 2576367
    :cond_1
    invoke-virtual {p7, p0}, LX/DOn;->a(LX/1Qj;)LX/DOm;

    move-result-object v0

    iput-object v0, p0, LX/IRI;->n:LX/1SX;

    goto :goto_0
.end method


# virtual methods
.method public final c()LX/1PT;
    .locals 1

    .prologue
    .line 2576359
    iget-object v0, p0, LX/IRI;->o:LX/1PT;

    return-object v0
.end method

.method public final e()LX/1SX;
    .locals 1

    .prologue
    .line 2576358
    iget-object v0, p0, LX/IRI;->n:LX/1SX;

    return-object v0
.end method
