.class public final LX/IIV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V
    .locals 0

    .prologue
    .line 2561875
    iput-object p1, p0, LX/IIV;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x6f7cb14

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2561876
    iget-object v1, p0, LX/IIV;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aK:LX/IIm;

    instance-of v1, v1, LX/IIo;

    if-nez v1, :cond_0

    iget-object v1, p0, LX/IIV;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aK:LX/IIm;

    instance-of v1, v1, LX/IIz;

    if-nez v1, :cond_0

    iget-object v1, p0, LX/IIV;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aK:LX/IIm;

    instance-of v1, v1, LX/IJ0;

    if-eqz v1, :cond_1

    .line 2561877
    :cond_0
    iget-object v1, p0, LX/IIV;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aK:LX/IIm;

    invoke-virtual {v1}, LX/IIm;->g()V

    .line 2561878
    :goto_0
    const v1, 0x1ccf074a    # 1.3699995E-21f

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2561879
    :cond_1
    iget-object v1, p0, LX/IIV;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->j:LX/03V;

    const-string v2, "friends_nearby_error_button_handle_failed"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "error button is tapped in abnormal fragment state: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, LX/IIV;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v4, v4, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->aK:LX/IIm;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
