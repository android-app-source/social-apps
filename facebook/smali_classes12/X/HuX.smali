.class public LX/HuX;
.super LX/Hs6;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<DerivedData::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesIsBrandedContentSupported;",
        "Services::",
        "LX/0ik",
        "<TDerivedData;>;>",
        "LX/Hs6;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field private final b:Landroid/content/Context;

.field public final c:LX/Hr2;

.field public final d:LX/HuZ;

.field private e:LX/Hu0;


# direct methods
.method public constructor <init>(LX/0ik;LX/Hr2;LX/HuZ;Landroid/content/Context;)V
    .locals 2
    .param p1    # LX/0ik;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/Hr2;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;",
            "LX/Hr2;",
            "LX/HuZ;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2517726
    invoke-direct {p0}, LX/Hs6;-><init>()V

    .line 2517727
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/HuX;->a:Ljava/lang/ref/WeakReference;

    .line 2517728
    iput-object p2, p0, LX/HuX;->c:LX/Hr2;

    .line 2517729
    iput-object p4, p0, LX/HuX;->b:Landroid/content/Context;

    .line 2517730
    iput-object p3, p0, LX/HuX;->d:LX/HuZ;

    .line 2517731
    invoke-static {p0}, LX/HuX;->h(LX/HuX;)V

    .line 2517732
    return-void
.end method

.method public static h(LX/HuX;)V
    .locals 3

    .prologue
    .line 2517706
    invoke-static {}, LX/Hu0;->newBuilder()LX/Htz;

    move-result-object v0

    const v1, 0x7f0209dd

    .line 2517707
    iput v1, v0, LX/Htz;->a:I

    .line 2517708
    move-object v0, v0

    .line 2517709
    const v1, 0x7f0a097a

    .line 2517710
    iput v1, v0, LX/Htz;->f:I

    .line 2517711
    move-object v0, v0

    .line 2517712
    iget-object v1, p0, LX/HuX;->b:Landroid/content/Context;

    const v2, 0x7f083983

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2517713
    iput-object v1, v0, LX/Htz;->b:Ljava/lang/String;

    .line 2517714
    move-object v0, v0

    .line 2517715
    invoke-virtual {p0}, LX/HuX;->g()LX/Hty;

    move-result-object v1

    invoke-virtual {v1}, LX/Hty;->getAnalyticsName()Ljava/lang/String;

    move-result-object v1

    .line 2517716
    iput-object v1, v0, LX/Htz;->d:Ljava/lang/String;

    .line 2517717
    move-object v0, v0

    .line 2517718
    new-instance v1, LX/HuW;

    invoke-direct {v1, p0}, LX/HuW;-><init>(LX/HuX;)V

    .line 2517719
    iput-object v1, v0, LX/Htz;->e:LX/Hr2;

    .line 2517720
    move-object v0, v0

    .line 2517721
    iget-object v1, p0, LX/HuX;->d:LX/HuZ;

    .line 2517722
    iput-object v1, v0, LX/Htz;->g:LX/Hsa;

    .line 2517723
    move-object v0, v0

    .line 2517724
    invoke-virtual {v0}, LX/Htz;->a()LX/Hu0;

    move-result-object v0

    iput-object v0, p0, LX/HuX;->e:LX/Hu0;

    .line 2517725
    return-void
.end method


# virtual methods
.method public final d()LX/Hu0;
    .locals 1

    .prologue
    .line 2517685
    iget-object v0, p0, LX/HuX;->e:LX/Hu0;

    return-object v0
.end method

.method public final e()Z
    .locals 6

    .prologue
    .line 2517688
    iget-object v0, p0, LX/HuX;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ik;

    invoke-interface {v0}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2zG;

    .line 2517689
    iget-object v1, v0, LX/2zG;->l:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2sH;

    invoke-static {v0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v2

    invoke-interface {v2}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    invoke-static {v0}, LX/2zG;->J(LX/2zG;)Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v3

    invoke-interface {v3}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v3

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2517690
    sget-object p0, LX/2sU;->a:[I

    invoke-virtual {v2}, LX/2rw;->ordinal()I

    move-result v0

    aget p0, p0, v0

    packed-switch p0, :pswitch_data_0

    move v4, v5

    .line 2517691
    :cond_0
    :goto_0
    move v1, v4

    .line 2517692
    move v0, v1

    .line 2517693
    return v0

    .line 2517694
    :pswitch_0
    if-eqz v3, :cond_5

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->getIsPageVerified()Z

    move-result p0

    if-nez p0, :cond_1

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->getIsOptedInSponsorTags()Z

    move-result p0

    if-eqz p0, :cond_5

    :cond_1
    const/4 p0, 0x1

    :goto_1
    move p0, p0

    .line 2517695
    if-eqz p0, :cond_3

    const/4 p0, 0x0

    .line 2517696
    if-eqz v3, :cond_2

    .line 2517697
    invoke-virtual {v3}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->getIsPageVerified()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2517698
    iget-object v0, v1, LX/2sH;->b:LX/0Uh;

    sget v2, LX/7l1;->d:I

    invoke-virtual {v0, v2, p0}, LX/0Uh;->a(IZ)Z

    move-result p0

    .line 2517699
    :cond_2
    :goto_2
    move p0, p0

    .line 2517700
    if-nez p0, :cond_0

    :cond_3
    move v4, v5

    goto :goto_0

    .line 2517701
    :pswitch_1
    iget-object p0, v1, LX/2sH;->b:LX/0Uh;

    sget v0, LX/7l1;->e:I

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, LX/0Uh;->a(IZ)Z

    move-result p0

    move p0, p0

    .line 2517702
    if-eqz p0, :cond_4

    .line 2517703
    iget-object p0, v1, LX/2sH;->a:LX/0ad;

    sget-short v0, LX/1RY;->b:S

    const/4 v2, 0x0

    invoke-interface {p0, v0, v2}, LX/0ad;->a(SZ)Z

    move-result p0

    move p0, p0

    .line 2517704
    if-nez p0, :cond_0

    :cond_4
    move v4, v5

    goto :goto_0

    :cond_5
    const/4 p0, 0x0

    goto :goto_1

    .line 2517705
    :cond_6
    iget-object v0, v1, LX/2sH;->a:LX/0ad;

    sget-short v2, LX/1RY;->a:S

    invoke-interface {v0, v2, p0}, LX/0ad;->a(SZ)Z

    move-result p0

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 2517687
    const/4 v0, 0x0

    return v0
.end method

.method public final g()LX/Hty;
    .locals 1

    .prologue
    .line 2517686
    sget-object v0, LX/Hty;->BRANDED_CONTENT:LX/Hty;

    return-object v0
.end method
