.class public final LX/HFA;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateCategoryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/HEu;

.field public final synthetic b:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;LX/HEu;)V
    .locals 0

    .prologue
    .line 2443124
    iput-object p1, p0, LX/HFA;->b:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    iput-object p2, p0, LX/HFA;->a:LX/HEu;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2443140
    iget-object v0, p0, LX/HFA;->b:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f08003a

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2443141
    iget-object v0, p0, LX/HFA;->b:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->p:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0, v2}, Lcom/facebook/fig/button/FigButton;->setEnabled(Z)V

    .line 2443142
    iget-object v0, p0, LX/HFA;->b:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->b:LX/03V;

    sget-object v1, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->a:Ljava/lang/String;

    const-string v2, "Category update failed"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2443143
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2443125
    check-cast p1, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateCategoryModel;

    .line 2443126
    if-eqz p1, :cond_0

    .line 2443127
    invoke-virtual {p1}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateCategoryModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2443128
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2443129
    iget-object v0, p0, LX/HFA;->b:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    invoke-virtual {v0}, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->b()V

    .line 2443130
    :goto_0
    iget-object v0, p0, LX/HFA;->b:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->p:Lcom/facebook/fig/button/FigButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setEnabled(Z)V

    .line 2443131
    :cond_0
    return-void

    .line 2443132
    :cond_1
    iget-object v1, p0, LX/HFA;->a:LX/HEu;

    .line 2443133
    iget-object v2, v1, LX/HEu;->b:Ljava/lang/String;

    move-object v1, v2

    .line 2443134
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2443135
    iget-object v0, p0, LX/HFA;->b:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->l:LX/HFJ;

    iget-object v1, p0, LX/HFA;->a:LX/HEu;

    .line 2443136
    iput-object v1, v0, LX/HFJ;->d:LX/HEu;

    .line 2443137
    iget-object v0, p0, LX/HFA;->b:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->g:LX/HFf;

    sget-object v1, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "update_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/HFA;->a:LX/HEu;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/HFf;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2443138
    iget-object v0, p0, LX/HFA;->b:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    invoke-virtual {v0}, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->c()V

    goto :goto_0

    .line 2443139
    :cond_2
    iget-object v0, p0, LX/HFA;->b:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->b:LX/03V;

    sget-object v1, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->a:Ljava/lang/String;

    const-string v2, "error: Category update failed"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
