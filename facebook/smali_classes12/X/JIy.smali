.class public LX/JIy;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/JIy;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/23R;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/D2M;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2678722
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2678723
    return-void
.end method

.method public static a(LX/0QB;)LX/JIy;
    .locals 5

    .prologue
    .line 2678728
    sget-object v0, LX/JIy;->c:LX/JIy;

    if-nez v0, :cond_1

    .line 2678729
    const-class v1, LX/JIy;

    monitor-enter v1

    .line 2678730
    :try_start_0
    sget-object v0, LX/JIy;->c:LX/JIy;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2678731
    if-eqz v2, :cond_0

    .line 2678732
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2678733
    new-instance v3, LX/JIy;

    invoke-direct {v3}, LX/JIy;-><init>()V

    .line 2678734
    const/16 v4, 0xf2f

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 p0, 0x36b4

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    .line 2678735
    iput-object v4, v3, LX/JIy;->a:LX/0Or;

    iput-object p0, v3, LX/JIy;->b:LX/0Or;

    .line 2678736
    move-object v0, v3

    .line 2678737
    sput-object v0, LX/JIy;->c:LX/JIy;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2678738
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2678739
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2678740
    :cond_1
    sget-object v0, LX/JIy;->c:LX/JIy;

    return-object v0

    .line 2678741
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2678742
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/JIy;Landroid/content/Context;Ljava/lang/String;LX/1Fb;LX/9hE;LX/74S;)V
    .locals 3

    .prologue
    .line 2678724
    invoke-virtual {p4, p5}, LX/9hD;->a(LX/74S;)LX/9hD;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/9hD;->a(Ljava/lang/String;)LX/9hD;

    move-result-object v0

    invoke-interface {p3}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/9hD;->a(LX/1bf;)LX/9hD;

    move-result-object v0

    invoke-virtual {v0}, LX/9hD;->b()Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    move-result-object v1

    .line 2678725
    iget-object v0, p0, LX/JIy;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/23R;

    .line 2678726
    const/4 v2, 0x0

    invoke-interface {v0, p1, v1, v2}, LX/23R;->a(Landroid/content/Context;Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;LX/9hN;)V

    .line 2678727
    return-void
.end method
