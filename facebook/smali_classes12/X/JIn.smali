.class public final LX/JIn;
.super LX/1a1;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final l:Lcom/facebook/entitycardsplugins/person/widget/footer/PersonCardFigButtonFooterView;

.field private final m:LX/JIw;


# direct methods
.method public constructor <init>(Lcom/facebook/entitycardsplugins/person/widget/footer/PersonCardFigButtonFooterView;LX/JIw;)V
    .locals 0

    .prologue
    .line 2678345
    invoke-direct {p0, p1}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 2678346
    iput-object p1, p0, LX/JIn;->l:Lcom/facebook/entitycardsplugins/person/widget/footer/PersonCardFigButtonFooterView;

    .line 2678347
    iput-object p2, p0, LX/JIn;->m:LX/JIw;

    .line 2678348
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0xd86e68c

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2678349
    iget-object v1, p0, LX/JIn;->m:LX/JIw;

    .line 2678350
    invoke-virtual {v1}, LX/Eme;->a()LX/0am;

    move-result-object v9

    .line 2678351
    invoke-virtual {v9}, LX/0am;->isPresent()Z

    move-result v3

    if-nez v3, :cond_0

    .line 2678352
    :goto_0
    const v1, -0x290ed40

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2678353
    :cond_0
    iget-object v3, v1, LX/JIw;->o:LX/Emj;

    sget-object v4, LX/Emo;->DEFAULT_ACTION:LX/Emo;

    iget-object v5, v1, LX/JIw;->m:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    invoke-virtual {v5}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->a()Ljava/lang/String;

    move-result-object v5

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v6

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v7

    iget-object v8, v1, LX/JIw;->m:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    invoke-interface/range {v3 .. v8}, LX/Emj;->a(LX/Emo;Ljava/lang/String;LX/0am;LX/0am;Ljava/lang/Object;)V

    .line 2678354
    iget-object v3, v1, LX/JIw;->o:LX/Emj;

    iget-object v4, v1, LX/JIw;->m:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->k()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;->c()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, LX/Emj;->a(Ljava/lang/String;)V

    .line 2678355
    iget-object v3, v1, LX/JIw;->a:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EoZ;

    .line 2678356
    invoke-virtual {v9}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/JI3;

    invoke-virtual {v4}, LX/JI3;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, v1, LX/JIw;->m:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    invoke-virtual {v5}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->k()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/EoZ;->a(Landroid/content/Context;LX/Eon;)V

    goto :goto_0
.end method
