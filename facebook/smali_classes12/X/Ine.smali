.class public LX/Ine;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/IoD;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ioh;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/IpF;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/IpQ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/IoD;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Ioh;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/IpF;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/IpQ;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2610757
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2610758
    iput-object p1, p0, LX/Ine;->a:LX/0Ot;

    .line 2610759
    iput-object p2, p0, LX/Ine;->b:LX/0Ot;

    .line 2610760
    iput-object p3, p0, LX/Ine;->c:LX/0Ot;

    .line 2610761
    iput-object p4, p0, LX/Ine;->d:LX/0Ot;

    .line 2610762
    return-void
.end method

.method public static a(LX/0QB;)LX/Ine;
    .locals 1

    .prologue
    .line 2610745
    invoke-static {p0}, LX/Ine;->b(LX/0QB;)LX/Ine;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/Ine;
    .locals 5

    .prologue
    .line 2610746
    new-instance v0, LX/Ine;

    const/16 v1, 0x28c9

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x28cf

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x28d3

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x28d7

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, LX/Ine;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2610747
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/facebook/messaging/payment/value/input/MessengerPayData;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2610748
    const-string v0, "payment_flow_type"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/5g0;

    .line 2610749
    sget-object v1, LX/Ind;->a:[I

    invoke-virtual {v0}, LX/5g0;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2610750
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported paymentFlowType: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2610751
    :pswitch_0
    iget-object v0, p0, LX/Ine;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IoC;

    .line 2610752
    :goto_0
    invoke-interface {v0, p1, p2, p3}, LX/IoC;->a(Ljava/lang/String;Lcom/facebook/messaging/payment/value/input/MessengerPayData;Landroid/os/Bundle;)V

    .line 2610753
    return-void

    .line 2610754
    :pswitch_1
    iget-object v0, p0, LX/Ine;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IoC;

    goto :goto_0

    .line 2610755
    :pswitch_2
    iget-object v0, p0, LX/Ine;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IoC;

    goto :goto_0

    .line 2610756
    :pswitch_3
    iget-object v0, p0, LX/Ine;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IoC;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
