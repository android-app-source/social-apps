.class public LX/JPd;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/hpp/UnitContentHeaderComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JPd",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/hpp/UnitContentHeaderComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2690116
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2690117
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JPd;->b:LX/0Zi;

    .line 2690118
    iput-object p1, p0, LX/JPd;->a:LX/0Ot;

    .line 2690119
    return-void
.end method

.method public static a(LX/0QB;)LX/JPd;
    .locals 4

    .prologue
    .line 2690156
    const-class v1, LX/JPd;

    monitor-enter v1

    .line 2690157
    :try_start_0
    sget-object v0, LX/JPd;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2690158
    sput-object v2, LX/JPd;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2690159
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2690160
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2690161
    new-instance v3, LX/JPd;

    const/16 p0, 0x1fe6

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JPd;-><init>(LX/0Ot;)V

    .line 2690162
    move-object v0, v3

    .line 2690163
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2690164
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JPd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2690165
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2690166
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 2690133
    check-cast p2, LX/JPc;

    .line 2690134
    iget-object v0, p0, LX/JPd;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/hpp/UnitContentHeaderComponentSpec;

    iget-object v1, p2, LX/JPc;->b:LX/JPJ;

    const/4 p2, 0x6

    const/4 p0, 0x0

    const/4 v8, 0x1

    .line 2690135
    invoke-virtual {v1}, LX/JPJ;->g()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 2690136
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v3

    .line 2690137
    check-cast v2, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->s()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    .line 2690138
    if-nez v2, :cond_0

    .line 2690139
    const/4 v2, 0x0

    .line 2690140
    :goto_0
    move-object v0, v2

    .line 2690141
    return-object v0

    .line 2690142
    :cond_0
    const/4 v3, 0x0

    .line 2690143
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    .line 2690144
    if-nez v4, :cond_3

    .line 2690145
    :cond_1
    :goto_1
    move-object v3, v3

    .line 2690146
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v4

    .line 2690147
    invoke-virtual {v1}, LX/JPJ;->g()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 2690148
    iget-object v5, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v5

    .line 2690149
    check-cast v2, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-nez v2, :cond_2

    const-string v2, ""

    .line 2690150
    :goto_2
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x2

    invoke-interface {v5, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v8}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v5

    iget-object v6, v0, Lcom/facebook/feedplugins/hpp/UnitContentHeaderComponentSpec;->b:LX/1nu;

    invoke-virtual {v6, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v6

    sget-object v7, Lcom/facebook/feedplugins/hpp/UnitContentHeaderComponentSpec;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v6, v7}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v6

    invoke-virtual {v6, v3}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v3

    const v6, 0x7f0213f3

    invoke-virtual {v3, v6}, LX/1nw;->h(I)LX/1nw;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const v6, 0x7f0b2587

    invoke-interface {v3, v6}, LX/1Di;->i(I)LX/1Di;

    move-result-object v3

    const v6, 0x7f0b2587

    invoke-interface {v3, v6}, LX/1Di;->q(I)LX/1Di;

    move-result-object v3

    const/4 v6, 0x7

    const v7, 0x7f0b2581

    invoke-interface {v3, v6, v7}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    const v6, 0x7f0b2581

    invoke-interface {v3, p0, v6}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    invoke-interface {v5, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v8}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v5

    const v6, 0x7f0b257f

    invoke-interface {v5, v6}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v4, v6}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v8}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v4

    const v6, 0x7f0b2582

    invoke-virtual {v4, v6}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const v6, 0x7f0b2581

    invoke-interface {v4, p2, v6}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    const v6, 0x7f0b2578

    invoke-interface {v4, v8, v6}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    invoke-interface {v5, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    sget-object v5, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v5}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v8}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v2

    const v5, 0x7f0b2589

    invoke-virtual {v2, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    const v5, 0x7f0a010e

    invoke-virtual {v2, v5}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v8}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const v5, 0x7f0b2581

    invoke-interface {v2, p2, v5}, LX/1Di;->c(II)LX/1Di;

    move-result-object v2

    invoke-interface {v4, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v3, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    .line 2690151
    const v3, 0x4aad1753    # 5671849.5f

    const/4 v4, 0x0

    invoke-static {p1, v3, v4}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v3

    move-object v3, v3

    .line 2690152
    invoke-interface {v2, v3}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    goto/16 :goto_0

    .line 2690153
    :cond_2
    invoke-virtual {v1}, LX/JPJ;->g()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 2690154
    iget-object v5, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v5

    .line 2690155
    check-cast v2, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_2

    :cond_3
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    goto/16 :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2690120
    invoke-static {}, LX/1dS;->b()V

    .line 2690121
    iget v0, p1, LX/1dQ;->b:I

    .line 2690122
    packed-switch v0, :pswitch_data_0

    .line 2690123
    :goto_0
    return-object v2

    .line 2690124
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2690125
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2690126
    check-cast v1, LX/JPc;

    .line 2690127
    iget-object v3, p0, LX/JPd;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/hpp/UnitContentHeaderComponentSpec;

    iget-object p1, v1, LX/JPc;->b:LX/JPJ;

    .line 2690128
    invoke-virtual {p1}, LX/JPJ;->g()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object p2

    .line 2690129
    iget-object p0, v3, Lcom/facebook/feedplugins/hpp/UnitContentHeaderComponentSpec;->a:LX/1nA;

    .line 2690130
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p2, v1

    .line 2690131
    check-cast p2, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->s()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object p2

    invoke-static {p2}, LX/3Bd;->a(Lcom/facebook/graphql/model/GraphQLPage;)LX/1y5;

    move-result-object p2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p2, v1}, LX/1nA;->a(Landroid/view/View;LX/1y5;Landroid/os/Bundle;)V

    .line 2690132
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x4aad1753
        :pswitch_0
    .end packed-switch
.end method
