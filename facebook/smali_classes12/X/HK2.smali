.class public LX/HK2;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/HJu;


# direct methods
.method public constructor <init>(LX/HJu;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2453580
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2453581
    iput-object p1, p0, LX/HK2;->a:LX/HJu;

    .line 2453582
    return-void
.end method

.method public static a(LX/0QB;)LX/HK2;
    .locals 4

    .prologue
    .line 2453583
    const-class v1, LX/HK2;

    monitor-enter v1

    .line 2453584
    :try_start_0
    sget-object v0, LX/HK2;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2453585
    sput-object v2, LX/HK2;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2453586
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2453587
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2453588
    new-instance p0, LX/HK2;

    invoke-static {v0}, LX/HJu;->a(LX/0QB;)LX/HJu;

    move-result-object v3

    check-cast v3, LX/HJu;

    invoke-direct {p0, v3}, LX/HK2;-><init>(LX/HJu;)V

    .line 2453589
    move-object v0, p0

    .line 2453590
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2453591
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/HK2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2453592
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2453593
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
