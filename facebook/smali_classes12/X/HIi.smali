.class public final LX/HIi;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/HIj;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

.field public final synthetic b:LX/HIj;


# direct methods
.method public constructor <init>(LX/HIj;)V
    .locals 1

    .prologue
    .line 2451589
    iput-object p1, p0, LX/HIi;->b:LX/HIj;

    .line 2451590
    move-object v0, p1

    .line 2451591
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2451592
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2451577
    const-string v0, "PageAboutPaymentOptionsComponentComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2451578
    if-ne p0, p1, :cond_1

    .line 2451579
    :cond_0
    :goto_0
    return v0

    .line 2451580
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2451581
    goto :goto_0

    .line 2451582
    :cond_3
    check-cast p1, LX/HIi;

    .line 2451583
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2451584
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2451585
    if-eq v2, v3, :cond_0

    .line 2451586
    iget-object v2, p0, LX/HIi;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/HIi;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v3, p1, LX/HIi;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2451587
    goto :goto_0

    .line 2451588
    :cond_4
    iget-object v2, p1, LX/HIi;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
