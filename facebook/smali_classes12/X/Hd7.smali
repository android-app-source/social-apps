.class public LX/Hd7;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Hd5;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Hd8;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2487847
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Hd7;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Hd8;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2487896
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2487897
    iput-object p1, p0, LX/Hd7;->b:LX/0Ot;

    .line 2487898
    return-void
.end method

.method public static a(LX/0QB;)LX/Hd7;
    .locals 4

    .prologue
    .line 2487885
    const-class v1, LX/Hd7;

    monitor-enter v1

    .line 2487886
    :try_start_0
    sget-object v0, LX/Hd7;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2487887
    sput-object v2, LX/Hd7;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2487888
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2487889
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2487890
    new-instance v3, LX/Hd7;

    const/16 p0, 0x3715

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Hd7;-><init>(LX/0Ot;)V

    .line 2487891
    move-object v0, v3

    .line 2487892
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2487893
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Hd7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2487894
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2487895
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2487884
    const v0, -0x707b7ab6

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 2487876
    check-cast p2, LX/Hd6;

    .line 2487877
    iget-object v0, p0, LX/Hd7;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hd8;

    iget-object v1, p2, LX/Hd6;->a:Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;

    .line 2487878
    invoke-virtual {v1}, Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;->l()Z

    move-result v2

    if-eqz v2, :cond_0

    const v2, 0x7f083671

    .line 2487879
    :goto_0
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const p0, 0x7f0a0097

    invoke-interface {v3, p0}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v3

    iget-object p0, v0, LX/Hd8;->a:LX/2g9;

    invoke-virtual {p0, p1}, LX/2g9;->c(LX/1De;)LX/2gA;

    move-result-object p0

    const/16 p2, 0x18

    invoke-virtual {p0, p2}, LX/2gA;->h(I)LX/2gA;

    move-result-object p0

    invoke-virtual {p0, v2}, LX/2gA;->i(I)LX/2gA;

    move-result-object p0

    invoke-virtual {p0, v2}, LX/2gA;->j(I)LX/2gA;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    .line 2487880
    const p0, -0x707b7ab6

    const/4 p2, 0x0

    invoke-static {p1, p0, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 2487881
    invoke-interface {v2, p0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v2

    invoke-interface {v3, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x1

    const/4 p0, 0x5

    invoke-interface {v2, v3, p0}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x3

    const/16 p0, 0xc

    invoke-interface {v2, v3, p0}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x6

    const p0, 0x7f0b22f6

    invoke-interface {v2, v3, p0}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2487882
    return-object v0

    .line 2487883
    :cond_0
    const v2, 0x7f083670

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2487848
    invoke-static {}, LX/1dS;->b()V

    .line 2487849
    iget v0, p1, LX/1dQ;->b:I

    .line 2487850
    packed-switch v0, :pswitch_data_0

    .line 2487851
    :goto_0
    return-object v1

    .line 2487852
    :pswitch_0
    iget-object v0, p1, LX/1dQ;->a:LX/1X1;

    .line 2487853
    check-cast v0, LX/Hd6;

    .line 2487854
    iget-object v2, p0, LX/Hd7;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Hd8;

    iget-object p1, v0, LX/Hd6;->a:Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;

    .line 2487855
    invoke-virtual {p1}, Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;->l()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 2487856
    iget-object p2, v2, LX/Hd8;->b:LX/HdH;

    .line 2487857
    new-instance p0, LX/4Jv;

    invoke-direct {p0}, LX/4Jv;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;->j()Ljava/lang/String;

    move-result-object v0

    .line 2487858
    const-string v2, "topic_id"

    invoke-virtual {p0, v2, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2487859
    move-object p0, p0

    .line 2487860
    new-instance v0, LX/HdI;

    invoke-direct {v0}, LX/HdI;-><init>()V

    move-object v0, v0

    .line 2487861
    const-string v2, "input"

    invoke-virtual {v0, v2, p0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2487862
    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object p0

    .line 2487863
    const/4 v0, 0x0

    invoke-static {p1, v0}, LX/HdH;->a(Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;Z)Lcom/facebook/topics/sections/follow/TopicFollowMutationModels$TopicFollowMutationFieldsModel;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/399;->a(LX/0jT;)LX/399;

    .line 2487864
    iget-object v0, p2, LX/HdH;->a:LX/0tX;

    invoke-virtual {v0, p0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2487865
    :goto_1
    goto :goto_0

    .line 2487866
    :cond_0
    iget-object p2, v2, LX/Hd8;->b:LX/HdH;

    .line 2487867
    new-instance p0, LX/4Jt;

    invoke-direct {p0}, LX/4Jt;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;->j()Ljava/lang/String;

    move-result-object v0

    .line 2487868
    const-string v2, "topic_id"

    invoke-virtual {p0, v2, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2487869
    move-object p0, p0

    .line 2487870
    new-instance v0, LX/HdA;

    invoke-direct {v0}, LX/HdA;-><init>()V

    move-object v0, v0

    .line 2487871
    const-string v2, "input"

    invoke-virtual {v0, v2, p0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2487872
    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object p0

    .line 2487873
    const/4 v0, 0x1

    invoke-static {p1, v0}, LX/HdH;->a(Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;Z)Lcom/facebook/topics/sections/follow/TopicFollowMutationModels$TopicFollowMutationFieldsModel;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/399;->a(LX/0jT;)LX/399;

    .line 2487874
    iget-object v0, p2, LX/HdH;->a:LX/0tX;

    invoke-virtual {v0, p0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2487875
    goto :goto_1

    :pswitch_data_0
    .packed-switch -0x707b7ab6
        :pswitch_0
    .end packed-switch
.end method
