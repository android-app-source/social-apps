.class public final enum LX/IAN;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IAN;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IAN;

.field public static final enum HOSTED_BY_PAGE_OR_PEOPLE:LX/IAN;

.field public static final enum UNKNOWN:LX/IAN;

.field public static final enum WITH_ARTISTS:LX/IAN;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2545362
    new-instance v0, LX/IAN;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2}, LX/IAN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IAN;->UNKNOWN:LX/IAN;

    .line 2545363
    new-instance v0, LX/IAN;

    const-string v1, "HOSTED_BY_PAGE_OR_PEOPLE"

    invoke-direct {v0, v1, v3}, LX/IAN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IAN;->HOSTED_BY_PAGE_OR_PEOPLE:LX/IAN;

    .line 2545364
    new-instance v0, LX/IAN;

    const-string v1, "WITH_ARTISTS"

    invoke-direct {v0, v1, v4}, LX/IAN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IAN;->WITH_ARTISTS:LX/IAN;

    .line 2545365
    const/4 v0, 0x3

    new-array v0, v0, [LX/IAN;

    sget-object v1, LX/IAN;->UNKNOWN:LX/IAN;

    aput-object v1, v0, v2

    sget-object v1, LX/IAN;->HOSTED_BY_PAGE_OR_PEOPLE:LX/IAN;

    aput-object v1, v0, v3

    sget-object v1, LX/IAN;->WITH_ARTISTS:LX/IAN;

    aput-object v1, v0, v4

    sput-object v0, LX/IAN;->$VALUES:[LX/IAN;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2545361
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IAN;
    .locals 1

    .prologue
    .line 2545359
    const-class v0, LX/IAN;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IAN;

    return-object v0
.end method

.method public static values()[LX/IAN;
    .locals 1

    .prologue
    .line 2545360
    sget-object v0, LX/IAN;->$VALUES:[LX/IAN;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IAN;

    return-object v0
.end method
