.class public final LX/HTo;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2470936
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_3

    .line 2470937
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2470938
    :goto_0
    return v1

    .line 2470939
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2470940
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_2

    .line 2470941
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 2470942
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2470943
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 2470944
    const-string v3, "icon"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2470945
    const/4 v2, 0x0

    .line 2470946
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 2470947
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2470948
    :goto_2
    move v0, v2

    .line 2470949
    goto :goto_1

    .line 2470950
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2470951
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2470952
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    .line 2470953
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2470954
    :cond_5
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_6

    .line 2470955
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2470956
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2470957
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_5

    if-eqz v3, :cond_5

    .line 2470958
    const-string v4, "uri"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2470959
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_3

    .line 2470960
    :cond_6
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2470961
    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2470962
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto :goto_2

    :cond_7
    move v0, v2

    goto :goto_3
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2470963
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2470964
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2470965
    if-eqz v0, :cond_1

    .line 2470966
    const-string v1, "icon"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2470967
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2470968
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2470969
    if-eqz v1, :cond_0

    .line 2470970
    const-string p1, "uri"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2470971
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2470972
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2470973
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2470974
    return-void
.end method
