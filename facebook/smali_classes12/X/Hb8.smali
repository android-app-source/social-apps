.class public final LX/Hb8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/HbE;


# direct methods
.method public constructor <init>(LX/HbE;)V
    .locals 0

    .prologue
    .line 2485134
    iput-object p1, p0, LX/Hb8;->a:LX/HbE;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x2

    const v3, -0x263fdb9

    invoke-static {v0, v1, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 2485135
    iget-object v4, p0, LX/Hb8;->a:LX/HbE;

    iget-object v0, p0, LX/Hb8;->a:LX/HbE;

    iget-boolean v0, v0, LX/HbE;->r:Z

    if-nez v0, :cond_0

    move v0, v1

    .line 2485136
    :goto_0
    iput-boolean v0, v4, LX/HbE;->r:Z

    .line 2485137
    iget-object v0, p0, LX/Hb8;->a:LX/HbE;

    iget-object v0, v0, LX/HbE;->q:Lcom/facebook/securitycheckup/inner/InertCheckBox;

    iget-object v4, p0, LX/Hb8;->a:LX/HbE;

    iget-boolean v4, v4, LX/HbE;->r:Z

    invoke-virtual {v0, v4}, Lcom/facebook/securitycheckup/inner/InertCheckBox;->setChecked(Z)V

    move v0, v2

    .line 2485138
    :goto_1
    iget-object v4, p0, LX/Hb8;->a:LX/HbE;

    iget-object v4, v4, LX/HbE;->s:Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;

    invoke-virtual {v4}, LX/1OM;->ij_()I

    move-result v4

    if-ge v0, v4, :cond_1

    .line 2485139
    iget-object v4, p0, LX/Hb8;->a:LX/HbE;

    iget-object v4, v4, LX/HbE;->s:Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;

    invoke-virtual {v4, v0}, Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;->e(I)LX/HbG;

    move-result-object v4

    .line 2485140
    iget-object v5, p0, LX/Hb8;->a:LX/HbE;

    iget-boolean v5, v5, LX/HbE;->r:Z

    .line 2485141
    iput-boolean v5, v4, LX/HbG;->e:Z

    .line 2485142
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    move v0, v2

    .line 2485143
    goto :goto_0

    .line 2485144
    :cond_1
    iget-object v0, p0, LX/Hb8;->a:LX/HbE;

    iget-boolean v0, v0, LX/HbE;->r:Z

    if-eqz v0, :cond_2

    .line 2485145
    iget-object v0, p0, LX/Hb8;->a:LX/HbE;

    iget-object v0, v0, LX/HbE;->f:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setEnabled(Z)V

    .line 2485146
    iget-object v0, p0, LX/Hb8;->a:LX/HbE;

    iget-object v0, v0, LX/HbE;->f:Lcom/facebook/resources/ui/FbButton;

    iget-object v1, p0, LX/Hb8;->a:LX/HbE;

    iget-object v1, v1, LX/HbE;->i:Landroid/content/Context;

    const v2, 0x7f08363b

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2485147
    :goto_2
    iget-object v0, p0, LX/Hb8;->a:LX/HbE;

    iget-object v0, v0, LX/HbE;->s:Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2485148
    const v0, -0x178267e3

    invoke-static {v0, v3}, LX/02F;->a(II)V

    return-void

    .line 2485149
    :cond_2
    iget-object v0, p0, LX/Hb8;->a:LX/HbE;

    iget-object v0, v0, LX/HbE;->f:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbButton;->setEnabled(Z)V

    .line 2485150
    iget-object v0, p0, LX/Hb8;->a:LX/HbE;

    iget-object v0, v0, LX/HbE;->f:Lcom/facebook/resources/ui/FbButton;

    iget-object v1, p0, LX/Hb8;->a:LX/HbE;

    iget-object v1, v1, LX/HbE;->i:Landroid/content/Context;

    const v2, 0x7f08363c

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method
