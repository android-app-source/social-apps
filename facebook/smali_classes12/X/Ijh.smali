.class public final LX/Ijh;
.super LX/2h1;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2h1",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/payments/p2p/model/PaymentCard;

.field public final synthetic b:LX/Ijy;


# direct methods
.method public constructor <init>(LX/Ijy;Lcom/facebook/payments/p2p/model/PaymentCard;)V
    .locals 0

    .prologue
    .line 2606012
    iput-object p1, p0, LX/Ijh;->b:LX/Ijy;

    iput-object p2, p0, LX/Ijh;->a:Lcom/facebook/payments/p2p/model/PaymentCard;

    invoke-direct {p0}, LX/2h1;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 10

    .prologue
    .line 2606013
    iget-object v0, p0, LX/Ijh;->b:LX/Ijy;

    iget-object v0, v0, LX/Ijy;->g:LX/0Zb;

    iget-object v1, p0, LX/Ijh;->b:LX/Ijy;

    iget-object v1, v1, LX/Ijy;->o:LX/Ik2;

    iget-object v1, v1, LX/Ik2;->e:LX/5g0;

    iget-object v1, v1, LX/5g0;->analyticsModule:Ljava/lang/String;

    const-string v2, "p2p_csc_fail"

    invoke-static {v1, v2}, Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2606014
    iget-object v0, p0, LX/Ijh;->b:LX/Ijy;

    iget-object v1, p0, LX/Ijh;->a:Lcom/facebook/payments/p2p/model/PaymentCard;

    .line 2606015
    iget-object v3, p1, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v3, v3

    .line 2606016
    sget-object v4, LX/1nY;->API_ERROR:LX/1nY;

    if-eq v3, v4, :cond_0

    .line 2606017
    :goto_0
    return-void

    .line 2606018
    :cond_0
    iget-object v3, p1, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v3, v3

    .line 2606019
    invoke-virtual {v3}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v3

    move-object v5, v3

    check-cast v5, Lcom/facebook/http/protocol/ApiErrorResult;

    .line 2606020
    invoke-virtual {v5}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    .line 2606021
    iget-object v3, v0, LX/Ijy;->p:LX/Ijx;

    invoke-interface {v3}, LX/Ijx;->b()V

    .line 2606022
    iget-object v3, v0, LX/Ijy;->e:Landroid/content/Context;

    iget-object v4, v0, LX/Ijy;->e:Landroid/content/Context;

    const v6, 0x7f082c23

    invoke-virtual {v4, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5}, Lcom/facebook/http/protocol/ApiErrorResult;->c()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/http/protocol/ApiErrorResult;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, v0, LX/Ijy;->e:Landroid/content/Context;

    const v7, 0x7f080016

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v7, LX/Ijn;

    invoke-direct {v7, v0}, LX/Ijn;-><init>(LX/Ijy;)V

    invoke-static {v3, v4, v5, v6, v7}, LX/Gza;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)LX/2EJ;

    move-result-object v3

    invoke-virtual {v3}, LX/2EJ;->show()V

    goto :goto_0

    .line 2606023
    :sswitch_0
    iget-object v3, v0, LX/Ijy;->e:Landroid/content/Context;

    iget-object v4, v0, LX/Ijy;->e:Landroid/content/Context;

    const v6, 0x7f082c23

    invoke-virtual {v4, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5}, Lcom/facebook/http/protocol/ApiErrorResult;->c()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/http/protocol/ApiErrorResult;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, v0, LX/Ijy;->e:Landroid/content/Context;

    const v7, 0x7f080043

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v7, LX/Ijj;

    invoke-direct {v7, v0, v1}, LX/Ijj;-><init>(LX/Ijy;Lcom/facebook/payments/p2p/model/PaymentCard;)V

    iget-object v8, v0, LX/Ijy;->e:Landroid/content/Context;

    const v9, 0x7f080017

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    new-instance v9, LX/Ijk;

    invoke-direct {v9, v0}, LX/Ijk;-><init>(LX/Ijy;)V

    invoke-static/range {v3 .. v9}, LX/Gza;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)LX/2EJ;

    move-result-object v3

    .line 2606024
    new-instance v4, LX/Ijl;

    invoke-direct {v4, v0}, LX/Ijl;-><init>(LX/Ijy;)V

    invoke-virtual {v3, v4}, LX/2EJ;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 2606025
    invoke-virtual {v3}, LX/2EJ;->show()V

    goto :goto_0

    .line 2606026
    :sswitch_1
    iget-object v3, v0, LX/Ijy;->p:LX/Ijx;

    invoke-interface {v3}, LX/Ijx;->b()V

    .line 2606027
    iget-object v3, v0, LX/Ijy;->e:Landroid/content/Context;

    iget-object v4, v0, LX/Ijy;->e:Landroid/content/Context;

    const v6, 0x7f082c2c

    invoke-virtual {v4, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5}, Lcom/facebook/http/protocol/ApiErrorResult;->c()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/http/protocol/ApiErrorResult;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, v0, LX/Ijy;->e:Landroid/content/Context;

    const v7, 0x7f080016

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v7, LX/Ijm;

    invoke-direct {v7, v0}, LX/Ijm;-><init>(LX/Ijy;)V

    invoke-static {v3, v4, v5, v6, v7}, LX/Gza;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)LX/2EJ;

    move-result-object v3

    invoke-virtual {v3}, LX/2EJ;->show()V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x2746 -> :sswitch_0
        0x274b -> :sswitch_1
    .end sparse-switch
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 13

    .prologue
    .line 2606028
    iget-object v0, p0, LX/Ijh;->b:LX/Ijy;

    iget-object v0, v0, LX/Ijy;->g:LX/0Zb;

    iget-object v1, p0, LX/Ijh;->b:LX/Ijy;

    iget-object v1, v1, LX/Ijy;->o:LX/Ik2;

    iget-object v1, v1, LX/Ik2;->e:LX/5g0;

    iget-object v1, v1, LX/5g0;->analyticsModule:Ljava/lang/String;

    const-string v2, "p2p_csc_success"

    invoke-static {v1, v2}, Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2606029
    new-instance v1, Lcom/facebook/payments/p2p/model/PartialPaymentCard;

    iget-object v0, p0, LX/Ijh;->a:Lcom/facebook/payments/p2p/model/PaymentCard;

    .line 2606030
    iget-wide v11, v0, Lcom/facebook/payments/p2p/model/PaymentCard;->a:J

    move-wide v2, v11

    .line 2606031
    iget-object v0, p0, LX/Ijh;->a:Lcom/facebook/payments/p2p/model/PaymentCard;

    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/PaymentCard;->f()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, LX/Ijh;->a:Lcom/facebook/payments/p2p/model/PaymentCard;

    .line 2606032
    iget v5, v0, Lcom/facebook/payments/p2p/model/PaymentCard;->c:I

    move v5, v5

    .line 2606033
    iget-object v0, p0, LX/Ijh;->a:Lcom/facebook/payments/p2p/model/PaymentCard;

    .line 2606034
    iget v6, v0, Lcom/facebook/payments/p2p/model/PaymentCard;->d:I

    move v6, v6

    .line 2606035
    iget-object v0, p0, LX/Ijh;->a:Lcom/facebook/payments/p2p/model/PaymentCard;

    .line 2606036
    iget-object v7, v0, Lcom/facebook/payments/p2p/model/PaymentCard;->e:Lcom/facebook/payments/p2p/model/Address;

    move-object v7, v7

    .line 2606037
    iget-object v0, p0, LX/Ijh;->a:Lcom/facebook/payments/p2p/model/PaymentCard;

    .line 2606038
    iget-object v8, v0, Lcom/facebook/payments/p2p/model/PaymentCard;->f:Ljava/lang/String;

    move-object v8, v8

    .line 2606039
    const/4 v9, 0x1

    iget-object v0, p0, LX/Ijh;->a:Lcom/facebook/payments/p2p/model/PaymentCard;

    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/PaymentCard;->h()Z

    move-result v10

    invoke-direct/range {v1 .. v10}, Lcom/facebook/payments/p2p/model/PartialPaymentCard;-><init>(JLjava/lang/String;IILcom/facebook/payments/p2p/model/Address;Ljava/lang/String;ZZ)V

    .line 2606040
    iget-object v0, p0, LX/Ijh;->b:LX/Ijy;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, LX/Ijy;->a$redex0(LX/Ijy;Lcom/facebook/payments/p2p/model/PaymentCard;Lcom/facebook/payments/p2p/model/VerificationFollowUpAction;)V

    .line 2606041
    return-void
.end method
