.class public LX/JQW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/17W;

.field public final b:LX/0Zb;


# direct methods
.method public constructor <init>(LX/17W;LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2691877
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2691878
    iput-object p1, p0, LX/JQW;->a:LX/17W;

    .line 2691879
    iput-object p2, p0, LX/JQW;->b:LX/0Zb;

    .line 2691880
    return-void
.end method

.method public static a(LX/0QB;)LX/JQW;
    .locals 5

    .prologue
    .line 2691866
    const-class v1, LX/JQW;

    monitor-enter v1

    .line 2691867
    :try_start_0
    sget-object v0, LX/JQW;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2691868
    sput-object v2, LX/JQW;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2691869
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2691870
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2691871
    new-instance p0, LX/JQW;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v3

    check-cast v3, LX/17W;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-direct {p0, v3, v4}, LX/JQW;-><init>(LX/17W;LX/0Zb;)V

    .line 2691872
    move-object v0, p0

    .line 2691873
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2691874
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JQW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2691875
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2691876
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
