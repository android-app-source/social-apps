.class public final LX/JGi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5rT;


# instance fields
.field public final synthetic a:LX/JGq;

.field private final b:I

.field private final c:F

.field private final d:F

.field private final e:F

.field private final f:F

.field private final g:Lcom/facebook/react/bridge/Callback;

.field private final h:Z


# direct methods
.method private constructor <init>(LX/JGq;IFFFFZLcom/facebook/react/bridge/Callback;)V
    .locals 0

    .prologue
    .line 2668968
    iput-object p1, p0, LX/JGi;->a:LX/JGq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2668969
    iput p2, p0, LX/JGi;->b:I

    .line 2668970
    iput p3, p0, LX/JGi;->c:F

    .line 2668971
    iput p4, p0, LX/JGi;->d:F

    .line 2668972
    iput p5, p0, LX/JGi;->e:F

    .line 2668973
    iput p6, p0, LX/JGi;->f:F

    .line 2668974
    iput-object p8, p0, LX/JGi;->g:Lcom/facebook/react/bridge/Callback;

    .line 2668975
    iput-boolean p7, p0, LX/JGi;->h:Z

    .line 2668976
    return-void
.end method

.method public synthetic constructor <init>(LX/JGq;IFFFFZLcom/facebook/react/bridge/Callback;B)V
    .locals 0

    .prologue
    .line 2668977
    invoke-direct/range {p0 .. p8}, LX/JGi;-><init>(LX/JGq;IFFFFZLcom/facebook/react/bridge/Callback;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 12

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 2668978
    :try_start_0
    iget-boolean v0, p0, LX/JGi;->h:Z

    if-eqz v0, :cond_0

    .line 2668979
    iget-object v0, p0, LX/JGi;->a:LX/JGq;

    iget-object v0, v0, LX/JGq;->b:LX/JGd;

    iget v1, p0, LX/JGi;->b:I

    sget-object v2, LX/JGq;->a:[I

    invoke-virtual {v0, v1, v2}, LX/5qw;->b(I[I)V
    :try_end_0
    .catch LX/5qz; {:try_start_0 .. :try_end_0} :catch_0

    .line 2668980
    :goto_0
    sget-object v0, LX/JGq;->a:[I

    aget v0, v0, v7

    int-to-float v0, v0

    .line 2668981
    sget-object v1, LX/JGq;->a:[I

    aget v1, v1, v8

    int-to-float v1, v1

    .line 2668982
    sget-object v2, LX/JGq;->a:[I

    aget v2, v2, v9

    int-to-float v2, v2

    .line 2668983
    sget-object v3, LX/JGq;->a:[I

    aget v3, v3, v10

    int-to-float v3, v3

    .line 2668984
    iget v4, p0, LX/JGi;->c:F

    mul-float/2addr v4, v2

    add-float/2addr v0, v4

    invoke-static {v0}, LX/5r2;->c(F)F

    move-result v0

    .line 2668985
    iget v4, p0, LX/JGi;->d:F

    mul-float/2addr v4, v3

    add-float/2addr v1, v4

    invoke-static {v1}, LX/5r2;->c(F)F

    move-result v1

    .line 2668986
    iget v4, p0, LX/JGi;->e:F

    mul-float/2addr v2, v4

    invoke-static {v2}, LX/5r2;->c(F)F

    move-result v2

    .line 2668987
    iget v4, p0, LX/JGi;->f:F

    mul-float/2addr v3, v4

    invoke-static {v3}, LX/5r2;->c(F)F

    move-result v3

    .line 2668988
    iget-boolean v4, p0, LX/JGi;->h:Z

    if-eqz v4, :cond_1

    .line 2668989
    iget-object v4, p0, LX/JGi;->g:Lcom/facebook/react/bridge/Callback;

    new-array v5, v11, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    aput-object v0, v5, v7

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    aput-object v0, v5, v8

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    aput-object v0, v5, v9

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    aput-object v0, v5, v10

    invoke-interface {v4, v5}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    .line 2668990
    :goto_1
    return-void

    .line 2668991
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/JGi;->a:LX/JGq;

    iget-object v0, v0, LX/JGq;->b:LX/JGd;

    iget v1, p0, LX/JGi;->b:I

    sget-object v2, LX/JGq;->a:[I

    invoke-virtual {v0, v1, v2}, LX/5qw;->a(I[I)V
    :try_end_1
    .catch LX/5qz; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 2668992
    :catch_0
    iget-object v0, p0, LX/JGi;->g:Lcom/facebook/react/bridge/Callback;

    new-array v1, v7, [Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    goto :goto_1

    .line 2668993
    :cond_1
    iget-object v4, p0, LX/JGi;->g:Lcom/facebook/react/bridge/Callback;

    const/4 v5, 0x6

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v5, v9

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v5, v10

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    aput-object v0, v5, v11

    const/4 v0, 0x5

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v5, v0

    invoke-interface {v4, v5}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    goto :goto_1
.end method
