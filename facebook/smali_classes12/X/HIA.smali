.class public final LX/HIA;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 2450614
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 2450615
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2450616
    :goto_0
    return v1

    .line 2450617
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2450618
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 2450619
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 2450620
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2450621
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 2450622
    const-string v5, "all_draft_posts"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2450623
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2450624
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v6, :cond_a

    .line 2450625
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2450626
    :goto_2
    move v3, v4

    .line 2450627
    goto :goto_1

    .line 2450628
    :cond_2
    const-string v5, "all_scheduled_posts"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2450629
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2450630
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v6, :cond_f

    .line 2450631
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2450632
    :goto_3
    move v2, v4

    .line 2450633
    goto :goto_1

    .line 2450634
    :cond_3
    const-string v5, "page_contact_us_leads"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2450635
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2450636
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v6, :cond_14

    .line 2450637
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2450638
    :goto_4
    move v0, v4

    .line 2450639
    goto :goto_1

    .line 2450640
    :cond_4
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2450641
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 2450642
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2450643
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2450644
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1

    .line 2450645
    :cond_6
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_8

    .line 2450646
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 2450647
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2450648
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_6

    if-eqz v7, :cond_6

    .line 2450649
    const-string v8, "count"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 2450650
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v6, v3

    move v3, v5

    goto :goto_5

    .line 2450651
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_5

    .line 2450652
    :cond_8
    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2450653
    if-eqz v3, :cond_9

    .line 2450654
    invoke-virtual {p1, v4, v6, v4}, LX/186;->a(III)V

    .line 2450655
    :cond_9
    invoke-virtual {p1}, LX/186;->d()I

    move-result v4

    goto :goto_2

    :cond_a
    move v3, v4

    move v6, v4

    goto :goto_5

    .line 2450656
    :cond_b
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_d

    .line 2450657
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 2450658
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2450659
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_b

    if-eqz v7, :cond_b

    .line 2450660
    const-string v8, "count"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 2450661
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v2

    move v6, v2

    move v2, v5

    goto :goto_6

    .line 2450662
    :cond_c
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_6

    .line 2450663
    :cond_d
    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2450664
    if-eqz v2, :cond_e

    .line 2450665
    invoke-virtual {p1, v4, v6, v4}, LX/186;->a(III)V

    .line 2450666
    :cond_e
    invoke-virtual {p1}, LX/186;->d()I

    move-result v4

    goto/16 :goto_3

    :cond_f
    move v2, v4

    move v6, v4

    goto :goto_6

    .line 2450667
    :cond_10
    :goto_7
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_12

    .line 2450668
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 2450669
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2450670
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_10

    if-eqz v7, :cond_10

    .line 2450671
    const-string v8, "count"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_11

    .line 2450672
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v6, v0

    move v0, v5

    goto :goto_7

    .line 2450673
    :cond_11
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_7

    .line 2450674
    :cond_12
    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2450675
    if-eqz v0, :cond_13

    .line 2450676
    invoke-virtual {p1, v4, v6, v4}, LX/186;->a(III)V

    .line 2450677
    :cond_13
    invoke-virtual {p1}, LX/186;->d()I

    move-result v4

    goto/16 :goto_4

    :cond_14
    move v0, v4

    move v6, v4

    goto :goto_7
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2450581
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2450582
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2450583
    if-eqz v0, :cond_1

    .line 2450584
    const-string v1, "all_draft_posts"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2450585
    const/4 v1, 0x0

    .line 2450586
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2450587
    invoke-virtual {p0, v0, v1, v1}, LX/15i;->a(III)I

    move-result v1

    .line 2450588
    if-eqz v1, :cond_0

    .line 2450589
    const-string p3, "count"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2450590
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 2450591
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2450592
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2450593
    if-eqz v0, :cond_3

    .line 2450594
    const-string v1, "all_scheduled_posts"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2450595
    const/4 v1, 0x0

    .line 2450596
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2450597
    invoke-virtual {p0, v0, v1, v1}, LX/15i;->a(III)I

    move-result v1

    .line 2450598
    if-eqz v1, :cond_2

    .line 2450599
    const-string p3, "count"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2450600
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 2450601
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2450602
    :cond_3
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2450603
    if-eqz v0, :cond_5

    .line 2450604
    const-string v1, "page_contact_us_leads"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2450605
    const/4 v1, 0x0

    .line 2450606
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2450607
    invoke-virtual {p0, v0, v1, v1}, LX/15i;->a(III)I

    move-result v1

    .line 2450608
    if-eqz v1, :cond_4

    .line 2450609
    const-string p1, "count"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2450610
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 2450611
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2450612
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2450613
    return-void
.end method
