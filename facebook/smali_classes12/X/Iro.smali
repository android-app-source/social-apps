.class public final LX/Iro;
.super LX/IrX;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;)V
    .locals 0

    .prologue
    .line 2618731
    iput-object p1, p0, LX/Iro;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    invoke-direct {p0}, LX/IrX;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/IrY;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 2618721
    iget-object v0, p0, LX/Iro;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-object v0, v0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->f:LX/Ird;

    .line 2618722
    iget-object v1, v0, LX/Ird;->c:LX/Iqg;

    move-object v1, v1

    .line 2618723
    if-eqz v1, :cond_0

    .line 2618724
    iget-object v0, p0, LX/Iro;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    iget-object v0, v0, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->i:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Iqk;

    .line 2618725
    iput-boolean v2, v0, LX/Iqk;->k:Z

    .line 2618726
    iget v0, p1, LX/IrY;->r:F

    iget v3, p1, LX/IrY;->q:F

    iget v4, p1, LX/IrY;->n:F

    iget p0, p1, LX/IrY;->m:F

    invoke-static {v0, v3, v4, p0}, LX/IrY;->a(FFFF)F

    move-result v0

    move v0, v0

    .line 2618727
    neg-float v0, v0

    .line 2618728
    iget v3, v1, LX/Iqg;->e:F

    add-float/2addr v3, v0

    iput v3, v1, LX/Iqg;->e:F

    .line 2618729
    sget-object v3, LX/Iqn;->ROTATE:LX/Iqn;

    invoke-virtual {v1, v3}, LX/Iqg;->a(Ljava/lang/Object;)V

    .line 2618730
    :cond_0
    return v2
.end method

.method public final b(LX/IrY;)Z
    .locals 3

    .prologue
    .line 2618715
    iget v0, p1, LX/IrY;->d:F

    move v0, v0

    .line 2618716
    float-to-int v0, v0

    .line 2618717
    iget v1, p1, LX/IrY;->e:F

    move v1, v1

    .line 2618718
    float-to-int v1, v1

    .line 2618719
    iget-object v2, p0, LX/Iro;->a:Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;

    invoke-static {v2, v0, v1}, Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;->c(Lcom/facebook/messaging/photos/editing/SceneLayersPresenter;II)LX/Iqg;

    move-result-object v0

    .line 2618720
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
