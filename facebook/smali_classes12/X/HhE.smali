.class public LX/HhE;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:Landroid/widget/ImageView;

.field public b:Landroid/widget/TextView;

.field public c:Landroid/widget/TextView;

.field public d:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2495149
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2495150
    const p1, 0x7f030247

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2495151
    const p1, 0x7f0d08c9

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, LX/HhE;->d:Landroid/view/ViewGroup;

    .line 2495152
    const p1, 0x7f0d08ca

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, LX/HhE;->a:Landroid/widget/ImageView;

    .line 2495153
    const p1, 0x7f0d08cb

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, LX/HhE;->b:Landroid/widget/TextView;

    .line 2495154
    const p1, 0x7f0d08cc

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, LX/HhE;->c:Landroid/widget/TextView;

    .line 2495155
    return-void
.end method


# virtual methods
.method public final a(LX/Hh9;)V
    .locals 6

    .prologue
    .line 2495156
    iget-object v0, p0, LX/HhE;->a:Landroid/widget/ImageView;

    .line 2495157
    iget v1, p1, LX/Hh9;->a:I

    move v1, v1

    .line 2495158
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2495159
    iget-object v0, p0, LX/HhE;->b:Landroid/widget/TextView;

    .line 2495160
    iget-object v1, p1, LX/Hh9;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2495161
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2495162
    iget-object v0, p0, LX/HhE;->b:Landroid/widget/TextView;

    .line 2495163
    iget-object v1, p1, LX/Hh9;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2495164
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2495165
    iget-object v0, p0, LX/HhE;->c:Landroid/widget/TextView;

    .line 2495166
    iget-object v1, p1, LX/Hh9;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2495167
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2495168
    iget-object v0, p0, LX/HhE;->c:Landroid/widget/TextView;

    .line 2495169
    iget-object v1, p1, LX/Hh9;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2495170
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2495171
    iget-boolean v0, p1, LX/Hh9;->d:Z

    move v0, v0

    .line 2495172
    if-eqz v0, :cond_0

    .line 2495173
    iget-object v0, p0, LX/HhE;->b:Landroid/widget/TextView;

    invoke-virtual {p0}, LX/HhE;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0947

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2495174
    :cond_0
    invoke-virtual {p0}, LX/HhE;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 2495175
    iget-object v0, p1, LX/Hh9;->e:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 2495176
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HhA;

    .line 2495177
    const v1, 0x7f030248

    iget-object v4, p0, LX/HhE;->d:Landroid/view/ViewGroup;

    const/4 v5, 0x0

    invoke-virtual {v2, v1, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 2495178
    const v1, 0x7f0d08cd

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 2495179
    iget-object v5, v0, LX/HhA;->a:Ljava/lang/String;

    move-object v5, v5

    .line 2495180
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2495181
    iget-object v5, v0, LX/HhA;->a:Ljava/lang/String;

    move-object v5, v5

    .line 2495182
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2495183
    const v1, 0x7f0d08ce

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 2495184
    iget-object v5, v0, LX/HhA;->b:Ljava/lang/String;

    move-object v5, v5

    .line 2495185
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2495186
    iget-object v5, v0, LX/HhA;->b:Ljava/lang/String;

    move-object v0, v5

    .line 2495187
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2495188
    iget-object v0, p0, LX/HhE;->d:Landroid/view/ViewGroup;

    invoke-virtual {p0}, LX/HhE;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/HhD;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2495189
    iget-object v0, p0, LX/HhE;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 2495190
    :cond_1
    return-void
.end method
