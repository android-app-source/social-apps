.class public LX/IqH;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:I

.field private c:LX/IqG;

.field public d:LX/IqF;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2616824
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2616825
    sget-object v0, LX/IqG;->NOT_STARTED:LX/IqG;

    iput-object v0, p0, LX/IqH;->c:LX/IqG;

    .line 2616826
    return-void
.end method

.method public static a(LX/0QB;)LX/IqH;
    .locals 1

    .prologue
    .line 2616795
    new-instance v0, LX/IqH;

    invoke-direct {v0}, LX/IqH;-><init>()V

    .line 2616796
    move-object v0, v0

    .line 2616797
    return-object v0
.end method

.method private a(LX/IqG;)V
    .locals 2

    .prologue
    .line 2616819
    iget-object v0, p0, LX/IqH;->c:LX/IqG;

    if-ne p1, v0, :cond_1

    .line 2616820
    :cond_0
    :goto_0
    return-void

    .line 2616821
    :cond_1
    iput-object p1, p0, LX/IqH;->c:LX/IqG;

    .line 2616822
    iget-object v0, p0, LX/IqH;->d:LX/IqF;

    if-eqz v0, :cond_0

    .line 2616823
    iget-object v0, p0, LX/IqH;->d:LX/IqF;

    iget-object v1, p0, LX/IqH;->c:LX/IqG;

    invoke-interface {v0, v1}, LX/IqF;->a(LX/IqG;)V

    goto :goto_0
.end method

.method private f()V
    .locals 2

    .prologue
    .line 2616815
    iget v0, p0, LX/IqH;->b:I

    iget v1, p0, LX/IqH;->a:I

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2616816
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/IqH;->c:LX/IqG;

    sget-object v1, LX/IqG;->LOADING:LX/IqG;

    if-ne v0, v1, :cond_0

    .line 2616817
    sget-object v0, LX/IqG;->DONE:LX/IqG;

    invoke-direct {p0, v0}, LX/IqH;->a(LX/IqG;)V

    .line 2616818
    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2616812
    sget-object v0, LX/IqG;->LOADING:LX/IqG;

    invoke-direct {p0, v0}, LX/IqH;->a(LX/IqG;)V

    .line 2616813
    invoke-direct {p0}, LX/IqH;->f()V

    .line 2616814
    return-void
.end method

.method public final a(LX/IqF;)V
    .locals 0
    .param p1    # LX/IqF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2616810
    iput-object p1, p0, LX/IqH;->d:LX/IqF;

    .line 2616811
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2616808
    iget v0, p0, LX/IqH;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/IqH;->a:I

    .line 2616809
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 2616803
    iget v0, p0, LX/IqH;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/IqH;->b:I

    .line 2616804
    iget v0, p0, LX/IqH;->b:I

    iget v1, p0, LX/IqH;->a:I

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2616805
    invoke-direct {p0}, LX/IqH;->f()V

    .line 2616806
    return-void

    .line 2616807
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2616799
    iput v0, p0, LX/IqH;->a:I

    .line 2616800
    iput v0, p0, LX/IqH;->b:I

    .line 2616801
    sget-object v0, LX/IqG;->NOT_STARTED:LX/IqG;

    invoke-direct {p0, v0}, LX/IqH;->a(LX/IqG;)V

    .line 2616802
    return-void
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 2616798
    iget-object v0, p0, LX/IqH;->c:LX/IqG;

    sget-object v1, LX/IqG;->DONE:LX/IqG;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
