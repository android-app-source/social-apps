.class public final LX/HYG;
.super LX/HYB;
.source ""


# instance fields
.field private a:LX/4hz;


# direct methods
.method public constructor <init>(LX/4hz;Lorg/json/JSONObject;)V
    .locals 0

    .prologue
    .line 2480374
    invoke-direct {p0, p2}, LX/HYB;-><init>(Lorg/json/JSONObject;)V

    .line 2480375
    iput-object p1, p0, LX/HYG;->a:LX/4hz;

    .line 2480376
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2480377
    iget-object v1, p0, LX/HYB;->a:Lorg/json/JSONObject;

    move-object v1, v1

    .line 2480378
    if-eqz v1, :cond_0

    .line 2480379
    iget-object v2, p0, LX/HYG;->a:LX/4hz;

    invoke-virtual {v2, v1}, LX/4hz;->a(Lorg/json/JSONObject;)Landroid/os/Bundle;

    move-result-object v1

    .line 2480380
    const-string v2, "isDialogLoadingUI"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 2480381
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p1, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->p:Z

    .line 2480382
    const-string v1, "platform_webview_dialog_ready"

    invoke-static {p1, v1}, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->a$redex0(Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;Ljava/lang/String;)V

    .line 2480383
    iget-object v1, p1, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->m:Lcom/facebook/webview/FacebookWebView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/webview/FacebookWebView;->setVisibility(I)V

    .line 2480384
    iget-object v1, p1, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->l:Landroid/widget/ProgressBar;

    if-eqz v1, :cond_1

    .line 2480385
    iget-object v1, p1, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->l:Landroid/widget/ProgressBar;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2480386
    :cond_1
    iget-object v1, p1, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->g:LX/HY4;

    iget-object v2, p1, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->i:Lcom/facebook/platform/common/action/PlatformAppCall;

    .line 2480387
    iget-object v3, v2, Lcom/facebook/platform/common/action/PlatformAppCall;->i:Ljava/lang/String;

    move-object v3, v3

    .line 2480388
    const-string v4, "PlatformWebDialogs_Fragment_loadJSDialog"

    const-string p1, "action_name"

    invoke-static {p1, v3}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object p1

    invoke-static {v1, v4, p1}, LX/HY4;->b(LX/HY4;Ljava/lang/String;LX/0P1;)V

    .line 2480389
    if-nez v0, :cond_2

    .line 2480390
    iget-object v3, v1, LX/HY4;->a:LX/11i;

    iget-object v4, v1, LX/HY4;->b:LX/HY0;

    invoke-interface {v3, v4}, LX/11i;->b(LX/0Pq;)V

    .line 2480391
    :goto_0
    const/4 v0, 0x0

    invoke-virtual {p2, p0, v0}, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->a(LX/HYB;Landroid/os/Bundle;)V

    .line 2480392
    return-void

    .line 2480393
    :cond_2
    const-string v4, "PlatformWebDialogs_Fragment_JSDialog_uiloading"

    const-string p1, "action_name"

    invoke-static {p1, v3}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v3

    invoke-static {v1, v4, v3}, LX/HY4;->a(LX/HY4;Ljava/lang/String;LX/0P1;)V

    goto :goto_0
.end method
