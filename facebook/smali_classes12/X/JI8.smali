.class public LX/JI8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EoJ;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/EoJ",
        "<",
        "LX/JJF;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2677396
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2677397
    return-void
.end method

.method public static a(LX/0QB;)LX/JI8;
    .locals 3

    .prologue
    .line 2677398
    const-class v1, LX/JI8;

    monitor-enter v1

    .line 2677399
    :try_start_0
    sget-object v0, LX/JI8;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2677400
    sput-object v2, LX/JI8;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2677401
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2677402
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2677403
    new-instance v0, LX/JI8;

    invoke-direct {v0}, LX/JI8;-><init>()V

    .line 2677404
    move-object v0, v0

    .line 2677405
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2677406
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JI8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2677407
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2677408
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2677409
    new-instance v0, LX/JJF;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/JJF;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/JJF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2677410
    const-class v0, LX/JJF;

    return-object v0
.end method
