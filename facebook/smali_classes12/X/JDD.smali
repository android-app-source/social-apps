.class public final LX/JDD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;

.field public final synthetic b:Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;)V
    .locals 0

    .prologue
    .line 2663438
    iput-object p1, p0, LX/JDD;->b:Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;

    iput-object p2, p0, LX/JDD;->a:Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x2

    const v3, 0x540af772

    invoke-static {v2, v0, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 2663439
    iget-object v2, p0, LX/JDD;->a:Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;

    invoke-virtual {v2}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->m()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;->j()Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;

    move-result-object v2

    .line 2663440
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;->ADDRESS:Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;

    invoke-virtual {v4, v2}, Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2663441
    iget-object v2, p0, LX/JDD;->a:Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;

    invoke-virtual {v2}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->mP_()LX/174;

    move-result-object v2

    invoke-interface {v2}, LX/174;->a()Ljava/lang/String;

    move-result-object v2

    .line 2663442
    iget-object v4, p0, LX/JDD;->a:Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;

    invoke-virtual {v4}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->m()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;->q()LX/1vs;

    move-result-object v4

    iget v4, v4, LX/1vs;->b:I

    if-eqz v4, :cond_2

    .line 2663443
    iget-object v4, p0, LX/JDD;->a:Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;

    invoke-virtual {v4}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->m()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;->q()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    .line 2663444
    invoke-virtual {v5, v4, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    :goto_0
    if-eqz v0, :cond_5

    .line 2663445
    iget-object v0, p0, LX/JDD;->a:Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->m()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemNodeFieldsModel;->q()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v2, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 2663446
    :goto_1
    iget-object v1, p0, LX/JDD;->b:Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;

    invoke-static {v1, v0}, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->a(Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2663447
    iget-object v1, p0, LX/JDD;->b:Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;

    .line 2663448
    invoke-static {v1, v0}, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->b$redex0(Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;Ljava/lang/String;)V

    .line 2663449
    :cond_0
    :goto_2
    const v0, -0x5d1e19b1

    invoke-static {v0, v3}, LX/02F;->a(II)V

    return-void

    :cond_1
    move v0, v1

    .line 2663450
    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 2663451
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;->PHONE:Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;

    invoke-virtual {v0, v2}, Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2663452
    iget-object v0, p0, LX/JDD;->b:Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;

    iget-object v1, p0, LX/JDD;->a:Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;

    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->mP_()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    .line 2663453
    invoke-static {v0, v1}, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->c$redex0(Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;Ljava/lang/String;)V

    .line 2663454
    goto :goto_2

    .line 2663455
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;->EMAIL:Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;

    invoke-virtual {v0, v2}, Lcom/facebook/graphql/enums/GraphQLTimelineContactItemType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2663456
    iget-object v0, p0, LX/JDD;->b:Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;

    iget-object v1, p0, LX/JDD;->a:Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;

    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;->mP_()LX/174;

    move-result-object v1

    invoke-interface {v1}, LX/174;->a()Ljava/lang/String;

    move-result-object v1

    .line 2663457
    invoke-static {v0, v1}, Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;->d$redex0(Lcom/facebook/timeline/aboutpage/views/ContactListCollectionItemView;Ljava/lang/String;)V

    .line 2663458
    goto :goto_2

    :cond_5
    move-object v0, v2

    goto :goto_1
.end method
