.class public final LX/IzX;
.super LX/0Tz;
.source ""


# static fields
.field public static final a:LX/0U1;

.field public static final b:LX/0U1;

.field public static final c:LX/0U1;

.field public static final d:LX/0U1;

.field public static final e:LX/0U1;

.field public static final f:LX/0U1;

.field public static final g:LX/0U1;

.field public static final h:LX/0U1;

.field public static final i:LX/0U1;

.field public static final j:LX/0U1;

.field public static final k:LX/0U1;

.field public static final l:LX/0U1;

.field public static final m:LX/0U1;

.field private static final n:LX/0sv;

.field private static final o:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 15

    .prologue
    .line 2634651
    new-instance v0, LX/0U1;

    const-string v1, "request_id"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/IzX;->a:LX/0U1;

    .line 2634652
    new-instance v0, LX/0U1;

    const-string v1, "requester_id"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/IzX;->b:LX/0U1;

    .line 2634653
    new-instance v0, LX/0U1;

    const-string v1, "requestee_id"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/IzX;->c:LX/0U1;

    .line 2634654
    new-instance v0, LX/0U1;

    const-string v1, "request_status"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/IzX;->d:LX/0U1;

    .line 2634655
    new-instance v0, LX/0U1;

    const-string v1, "creation_time"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/IzX;->e:LX/0U1;

    .line 2634656
    new-instance v0, LX/0U1;

    const-string v1, "updated_time"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/IzX;->f:LX/0U1;

    .line 2634657
    new-instance v0, LX/0U1;

    const-string v1, "raw_amount"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/IzX;->g:LX/0U1;

    .line 2634658
    new-instance v0, LX/0U1;

    const-string v1, "amount_offset"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/IzX;->h:LX/0U1;

    .line 2634659
    new-instance v0, LX/0U1;

    const-string v1, "currency"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/IzX;->i:LX/0U1;

    .line 2634660
    new-instance v0, LX/0U1;

    const-string v1, "memo_text"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/IzX;->j:LX/0U1;

    .line 2634661
    new-instance v0, LX/0U1;

    const-string v1, "theme"

    const-string v2, "THEME"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/IzX;->k:LX/0U1;

    .line 2634662
    new-instance v0, LX/0U1;

    const-string v1, "group_thread_id"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/IzX;->l:LX/0U1;

    .line 2634663
    new-instance v0, LX/0U1;

    const-string v1, "transaction_id"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/IzX;->m:LX/0U1;

    .line 2634664
    new-instance v0, LX/0su;

    sget-object v1, LX/IzX;->a:LX/0U1;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0su;-><init>(LX/0Px;)V

    sput-object v0, LX/IzX;->n:LX/0sv;

    .line 2634665
    sget-object v0, LX/IzX;->a:LX/0U1;

    sget-object v1, LX/IzX;->b:LX/0U1;

    sget-object v2, LX/IzX;->c:LX/0U1;

    sget-object v3, LX/IzX;->d:LX/0U1;

    sget-object v4, LX/IzX;->e:LX/0U1;

    sget-object v5, LX/IzX;->f:LX/0U1;

    sget-object v6, LX/IzX;->g:LX/0U1;

    sget-object v7, LX/IzX;->h:LX/0U1;

    sget-object v8, LX/IzX;->i:LX/0U1;

    sget-object v9, LX/IzX;->j:LX/0U1;

    sget-object v10, LX/IzX;->k:LX/0U1;

    sget-object v11, LX/IzX;->l:LX/0U1;

    const/4 v12, 0x1

    new-array v12, v12, [LX/0U1;

    const/4 v13, 0x0

    sget-object v14, LX/IzX;->m:LX/0U1;

    aput-object v14, v12, v13

    invoke-static/range {v0 .. v12}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/IzX;->o:LX/0Px;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 2634649
    const-string v0, "requests"

    sget-object v1, LX/IzX;->o:LX/0Px;

    sget-object v2, LX/IzX;->n:LX/0sv;

    invoke-direct {p0, v0, v1, v2}, LX/0Tz;-><init>(Ljava/lang/String;LX/0Px;LX/0sv;)V

    .line 2634650
    return-void
.end method
