.class public LX/JR0;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pm;",
        ":",
        "LX/1Pk;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JR4;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JR0",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JR4;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2692545
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2692546
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JR0;->b:LX/0Zi;

    .line 2692547
    iput-object p1, p0, LX/JR0;->a:LX/0Ot;

    .line 2692548
    return-void
.end method

.method public static a(LX/0QB;)LX/JR0;
    .locals 4

    .prologue
    .line 2692549
    const-class v1, LX/JR0;

    monitor-enter v1

    .line 2692550
    :try_start_0
    sget-object v0, LX/JR0;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2692551
    sput-object v2, LX/JR0;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2692552
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2692553
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2692554
    new-instance v3, LX/JR0;

    const/16 p0, 0x201b

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JR0;-><init>(LX/0Ot;)V

    .line 2692555
    move-object v0, v3

    .line 2692556
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2692557
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JR0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2692558
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2692559
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 13

    .prologue
    .line 2692560
    check-cast p2, LX/JQz;

    .line 2692561
    iget-object v0, p0, LX/JR0;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JR4;

    iget-object v1, p2, LX/JQz;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/JQz;->b:LX/1Pm;

    const/4 v8, 0x1

    .line 2692562
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2692563
    check-cast v3, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;

    .line 2692564
    new-instance v4, LX/JR2;

    invoke-direct {v4, v3}, LX/JR2;-><init>(Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;)V

    invoke-interface {v2, v4, v3}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JR3;

    .line 2692565
    iget-object v4, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 2692566
    check-cast v4, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;

    .line 2692567
    iget-object v5, v3, LX/JR3;->c:LX/0Px;

    move-object v5, v5

    .line 2692568
    if-eqz v5, :cond_0

    .line 2692569
    iget-object v5, v3, LX/JR3;->c:LX/0Px;

    move-object v5, v5

    .line 2692570
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v5

    const/4 v6, 0x3

    if-ge v5, v6, :cond_3

    .line 2692571
    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 2692572
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->s()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    const/4 v5, 0x0

    move v6, v5

    :goto_0
    if-ge v6, v10, :cond_2

    invoke-virtual {v9, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLUser;

    .line 2692573
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v11

    if-eqz v11, :cond_1

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_1

    .line 2692574
    new-instance v11, LX/JQk;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v12

    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v12

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLUser;->v()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLUser;->F()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v11, v12, p0, v5}, LX/JQk;-><init>(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v11}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2692575
    :cond_1
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_0

    .line 2692576
    :cond_2
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    move-object v5, v5

    .line 2692577
    iput-object v5, v3, LX/JR3;->c:LX/0Px;

    .line 2692578
    const/4 v5, 0x0

    .line 2692579
    iput-boolean v5, v3, LX/JR3;->a:Z

    .line 2692580
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->t()I

    move-result v5

    .line 2692581
    iput v5, v3, LX/JR3;->d:I

    .line 2692582
    :cond_3
    move-object v4, v3

    .line 2692583
    iget-object v3, v0, LX/JR4;->f:LX/JQo;

    new-instance v5, LX/JR1;

    invoke-direct {v5, v0, v4, v2, v1}, LX/JR1;-><init>(LX/JR4;LX/JR3;LX/1Pm;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2692584
    iput-object v5, v3, LX/JQo;->k:LX/JR1;

    .line 2692585
    iget-object v6, v3, LX/JQo;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v7, LX/1qz;->a:LX/0Tn;

    iget-object v9, v3, LX/JQo;->j:LX/0dN;

    invoke-interface {v6, v7, v9}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;LX/0dN;)V

    .line 2692586
    iget-boolean v3, v4, LX/JR3;->a:Z

    move v5, v3

    .line 2692587
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const/4 v6, 0x0

    invoke-interface {v3, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v8}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v3

    .line 2692588
    iget-object v6, v0, LX/JR4;->e:LX/JR8;

    const/4 v7, 0x0

    .line 2692589
    new-instance v9, LX/JR7;

    invoke-direct {v9, v6}, LX/JR7;-><init>(LX/JR8;)V

    .line 2692590
    iget-object v10, v6, LX/JR8;->b:LX/0Zi;

    invoke-virtual {v10}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, LX/JR6;

    .line 2692591
    if-nez v10, :cond_4

    .line 2692592
    new-instance v10, LX/JR6;

    invoke-direct {v10, v6}, LX/JR6;-><init>(LX/JR8;)V

    .line 2692593
    :cond_4
    invoke-static {v10, p1, v7, v7, v9}, LX/JR6;->a$redex0(LX/JR6;LX/1De;IILX/JR7;)V

    .line 2692594
    move-object v9, v10

    .line 2692595
    move-object v7, v9

    .line 2692596
    move-object v6, v7

    .line 2692597
    iget-object v7, v6, LX/JR6;->a:LX/JR7;

    iput-object v1, v7, LX/JR7;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2692598
    iget-object v7, v6, LX/JR6;->e:Ljava/util/BitSet;

    const/4 v9, 0x0

    invoke-virtual {v7, v9}, Ljava/util/BitSet;->set(I)V

    .line 2692599
    move-object v6, v6

    .line 2692600
    iget-object v7, v6, LX/JR6;->a:LX/JR7;

    iput-object v2, v7, LX/JR7;->b:LX/1Pm;

    .line 2692601
    iget-object v7, v6, LX/JR6;->e:Ljava/util/BitSet;

    const/4 v9, 0x1

    invoke-virtual {v7, v9}, Ljava/util/BitSet;->set(I)V

    .line 2692602
    move-object v6, v6

    .line 2692603
    invoke-virtual {v6}, LX/1X5;->d()LX/1X1;

    move-result-object v6

    invoke-interface {v3, v6}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v6

    iget-object v3, v0, LX/JR4;->a:LX/JRM;

    invoke-virtual {v3, p1}, LX/JRM;->c(LX/1De;)LX/JRK;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/JRK;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/JRK;

    move-result-object v7

    move-object v3, v2

    check-cast v3, LX/1Pk;

    invoke-virtual {v7, v3}, LX/JRK;->a(LX/1Pk;)LX/JRK;

    move-result-object v7

    .line 2692604
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2692605
    check-cast v3, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;

    const/4 v9, 0x0

    .line 2692606
    if-eqz v5, :cond_9

    .line 2692607
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v10

    if-eqz v10, :cond_5

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v9

    .line 2692608
    :cond_5
    :goto_1
    move-object v3, v9

    .line 2692609
    invoke-virtual {v7, v3}, LX/JRK;->b(Ljava/lang/String;)LX/JRK;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->d()LX/1X1;

    move-result-object v3

    invoke-interface {v6, v3}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v6

    iget-object v3, v0, LX/JR4;->b:LX/JQr;

    const/4 v7, 0x0

    .line 2692610
    new-instance v9, LX/JQp;

    invoke-direct {v9, v3}, LX/JQp;-><init>(LX/JQr;)V

    .line 2692611
    sget-object v10, LX/JQr;->a:LX/0Zi;

    invoke-virtual {v10}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, LX/JQq;

    .line 2692612
    if-nez v10, :cond_6

    .line 2692613
    new-instance v10, LX/JQq;

    invoke-direct {v10}, LX/JQq;-><init>()V

    .line 2692614
    :cond_6
    invoke-static {v10, p1, v7, v7, v9}, LX/JQq;->a$redex0(LX/JQq;LX/1De;IILX/JQp;)V

    .line 2692615
    move-object v9, v10

    .line 2692616
    move-object v7, v9

    .line 2692617
    move-object v3, v7

    .line 2692618
    iget-object v7, v3, LX/JQq;->a:LX/JQp;

    iput-object v2, v7, LX/JQp;->a:LX/1Pm;

    .line 2692619
    iget-object v7, v3, LX/JQq;->d:Ljava/util/BitSet;

    const/4 v9, 0x0

    invoke-virtual {v7, v9}, Ljava/util/BitSet;->set(I)V

    .line 2692620
    move-object v3, v3

    .line 2692621
    iget-object v7, v4, LX/JR3;->c:LX/0Px;

    move-object v7, v7

    .line 2692622
    iget-object v9, v3, LX/JQq;->a:LX/JQp;

    iput-object v7, v9, LX/JQp;->b:LX/0Px;

    .line 2692623
    iget-object v9, v3, LX/JQq;->d:Ljava/util/BitSet;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Ljava/util/BitSet;->set(I)V

    .line 2692624
    move-object v7, v3

    .line 2692625
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2692626
    check-cast v3, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->c()Ljava/lang/String;

    move-result-object v3

    .line 2692627
    iget-object v9, v7, LX/JQq;->a:LX/JQp;

    iput-object v3, v9, LX/JQp;->c:Ljava/lang/String;

    .line 2692628
    iget-object v9, v7, LX/JQq;->d:Ljava/util/BitSet;

    const/4 v10, 0x2

    invoke-virtual {v9, v10}, Ljava/util/BitSet;->set(I)V

    .line 2692629
    move-object v3, v7

    .line 2692630
    iget-object v7, v3, LX/JQq;->a:LX/JQp;

    iput-boolean v5, v7, LX/JQp;->d:Z

    .line 2692631
    iget-object v7, v3, LX/JQq;->d:Ljava/util/BitSet;

    const/4 v9, 0x3

    invoke-virtual {v7, v9}, Ljava/util/BitSet;->set(I)V

    .line 2692632
    move-object v3, v3

    .line 2692633
    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const v7, 0x7f0b25a8

    invoke-interface {v3, v8, v7}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    invoke-interface {v6, v3}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v6

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const/4 v11, 0x0

    const/4 v2, 0x1

    const/4 p2, 0x0

    const/4 p0, 0x3

    .line 2692634
    iget-object v9, v4, LX/JR3;->c:LX/0Px;

    move-object v9, v9

    .line 2692635
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v9

    if-lt v9, p0, :cond_7

    .line 2692636
    iget v9, v4, LX/JR3;->d:I

    move v9, v9

    .line 2692637
    if-ge v9, p0, :cond_a

    :cond_7
    move-object v9, v11

    .line 2692638
    :goto_2
    move-object v4, v9

    .line 2692639
    invoke-virtual {v6, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    const v6, 0x7f0b0050

    invoke-virtual {v4, v6}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    const v6, 0x7f0a00d6

    invoke-virtual {v4, v6}, LX/1ne;->n(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const v6, 0x7f0b25a8

    invoke-interface {v4, v8, v6}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    const/4 v6, 0x6

    const v7, 0x7f0b010f

    invoke-interface {v4, v6, v7}, LX/1Di;->g(II)LX/1Di;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v4

    const v6, 0x7f0a098d

    invoke-virtual {v4, v6}, LX/25Q;->i(I)LX/25Q;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const v6, 0x7f0b25a7

    invoke-interface {v4, v6}, LX/1Di;->q(I)LX/1Di;

    move-result-object v4

    const/4 v6, 0x4

    invoke-interface {v4, v6}, LX/1Di;->b(I)LX/1Di;

    move-result-object v4

    const v6, 0x7f0b25a9

    invoke-interface {v4, v8, v6}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    iget-object v3, v0, LX/JR4;->c:LX/JRI;

    invoke-virtual {v3, p1}, LX/JRI;->c(LX/1De;)LX/JRG;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/JRG;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/JRG;

    move-result-object v6

    .line 2692640
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2692641
    check-cast v3, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;

    const/4 v7, 0x0

    .line 2692642
    if-eqz v5, :cond_e

    .line 2692643
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v8

    if-eqz v8, :cond_8

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v7

    .line 2692644
    :cond_8
    :goto_3
    move-object v3, v7

    .line 2692645
    invoke-virtual {v6, v3}, LX/JRG;->b(Ljava/lang/String;)LX/JRG;

    move-result-object v5

    iget-object v6, v0, LX/JR4;->d:LX/JR5;

    .line 2692646
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2692647
    check-cast v3, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;

    .line 2692648
    iget-object v7, v6, LX/JR5;->a:LX/23i;

    invoke-virtual {v7}, LX/23i;->f()Z

    move-result v7

    if-eqz v7, :cond_f

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->p()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    .line 2692649
    :goto_4
    if-eqz v7, :cond_10

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v7

    :goto_5
    move-object v3, v7

    .line 2692650
    invoke-virtual {v5, v3}, LX/JRG;->c(Ljava/lang/String;)LX/JRG;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->d()LX/1X1;

    move-result-object v3

    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2692651
    return-object v0

    :cond_9
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v10

    if-eqz v10, :cond_5

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->v()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_1

    .line 2692652
    :cond_a
    iget-object v9, v4, LX/JR3;->c:LX/0Px;

    move-object v9, v9

    .line 2692653
    invoke-virtual {v9, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/JQk;

    .line 2692654
    iget-object v10, v4, LX/JR3;->c:LX/0Px;

    move-object v10, v10

    .line 2692655
    invoke-virtual {v10, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, LX/JQk;

    .line 2692656
    iget-object v12, v9, LX/JQk;->c:Ljava/lang/String;

    move-object v12, v12

    .line 2692657
    if-eqz v12, :cond_b

    .line 2692658
    iget-object v12, v10, LX/JQk;->c:Ljava/lang/String;

    move-object v12, v12

    .line 2692659
    if-nez v12, :cond_c

    :cond_b
    move-object v9, v11

    .line 2692660
    goto/16 :goto_2

    .line 2692661
    :cond_c
    iget-boolean v11, v4, LX/JR3;->a:Z

    move v11, v11

    .line 2692662
    if-eqz v11, :cond_d

    const v11, 0x7f083a84

    :goto_6
    new-array v12, p0, [Ljava/lang/Object;

    .line 2692663
    iget-object p0, v9, LX/JQk;->c:Ljava/lang/String;

    move-object v9, p0

    .line 2692664
    aput-object v9, v12, p2

    .line 2692665
    iget-object v9, v10, LX/JQk;->c:Ljava/lang/String;

    move-object v9, v9

    .line 2692666
    aput-object v9, v12, v2

    const/4 v9, 0x2

    .line 2692667
    iget v10, v4, LX/JR3;->d:I

    move v10, v10

    .line 2692668
    add-int/lit8 v10, v10, -0x2

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v12, v9

    invoke-virtual {v7, v11, v12}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_2

    :cond_d
    const v11, 0x7f083a85

    goto :goto_6

    :cond_e
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v8

    if-eqz v8, :cond_8

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_3

    .line 2692669
    :cond_f
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMessengerActiveNowFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    goto/16 :goto_4

    .line 2692670
    :cond_10
    const/4 v7, 0x0

    goto/16 :goto_5
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2692671
    invoke-static {}, LX/1dS;->b()V

    .line 2692672
    const/4 v0, 0x0

    return-object v0
.end method
