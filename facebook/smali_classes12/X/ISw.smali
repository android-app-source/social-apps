.class public final enum LX/ISw;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ISw;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ISw;

.field public static final enum MEMBER_BIO_COMPOSER:LX/ISw;

.field public static final enum MEMBER_BIO_HEADER:LX/ISw;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2578534
    new-instance v0, LX/ISw;

    const-string v1, "MEMBER_BIO_HEADER"

    invoke-direct {v0, v1, v2}, LX/ISw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ISw;->MEMBER_BIO_HEADER:LX/ISw;

    .line 2578535
    new-instance v0, LX/ISw;

    const-string v1, "MEMBER_BIO_COMPOSER"

    invoke-direct {v0, v1, v3}, LX/ISw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/ISw;->MEMBER_BIO_COMPOSER:LX/ISw;

    .line 2578536
    const/4 v0, 0x2

    new-array v0, v0, [LX/ISw;

    sget-object v1, LX/ISw;->MEMBER_BIO_HEADER:LX/ISw;

    aput-object v1, v0, v2

    sget-object v1, LX/ISw;->MEMBER_BIO_COMPOSER:LX/ISw;

    aput-object v1, v0, v3

    sput-object v0, LX/ISw;->$VALUES:[LX/ISw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2578537
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/ISw;
    .locals 1

    .prologue
    .line 2578533
    const-class v0, LX/ISw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ISw;

    return-object v0
.end method

.method public static values()[LX/ISw;
    .locals 1

    .prologue
    .line 2578532
    sget-object v0, LX/ISw;->$VALUES:[LX/ISw;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ISw;

    return-object v0
.end method
