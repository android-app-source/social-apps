.class public final LX/Hp8;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 2508209
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2508210
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 2508211
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 2508212
    invoke-static {p0, p1}, LX/Hp8;->b(LX/15w;LX/186;)I

    move-result v1

    .line 2508213
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2508214
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2508215
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2508216
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 2508217
    if-eqz v0, :cond_0

    .line 2508218
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2508219
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2508220
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2508221
    if-eqz v0, :cond_1

    .line 2508222
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2508223
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2508224
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2508225
    if-eqz v0, :cond_2

    .line 2508226
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2508227
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2508228
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2508229
    return-void
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2508230
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2508231
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 2508232
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2}, LX/Hp8;->a(LX/15i;ILX/0nX;)V

    .line 2508233
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2508234
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2508235
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2508236
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 2508237
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2508238
    :goto_0
    return v1

    .line 2508239
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2508240
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_5

    .line 2508241
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 2508242
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2508243
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 2508244
    const-string v5, "__type__"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    const-string v5, "__typename"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2508245
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    goto :goto_1

    .line 2508246
    :cond_3
    const-string v5, "id"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 2508247
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 2508248
    :cond_4
    const-string v5, "name"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2508249
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 2508250
    :cond_5
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2508251
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 2508252
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2508253
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2508254
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1
.end method
