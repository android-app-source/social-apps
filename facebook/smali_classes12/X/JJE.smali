.class public LX/JJE;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2679080
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2679081
    const v0, 0x7f0303be

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2679082
    const v0, 0x7f0d02a7

    invoke-virtual {p0, v0}, LX/JJE;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/JJE;->a:Lcom/facebook/resources/ui/FbTextView;

    .line 2679083
    invoke-virtual {p0}, LX/JJE;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const p1, 0x7f0b2549

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2679084
    invoke-virtual {p0, v0, v0, v0, v0}, LX/JJE;->setPadding(IIII)V

    .line 2679085
    return-void
.end method


# virtual methods
.method public setEnabled(Z)V
    .locals 3

    .prologue
    .line 2679086
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->setEnabled(Z)V

    .line 2679087
    iget-object v1, p0, LX/JJE;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, LX/JJE;->getContext()Landroid/content/Context;

    move-result-object v2

    if-eqz p1, :cond_0

    const v0, 0x7f0a00a8

    :goto_0
    invoke-static {v2, v0}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 2679088
    return-void

    .line 2679089
    :cond_0
    const v0, 0x7f0a00a5

    goto :goto_0
.end method

.method public setSelected(Z)V
    .locals 3

    .prologue
    .line 2679090
    iget-object v1, p0, LX/JJE;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, LX/JJE;->getContext()Landroid/content/Context;

    move-result-object v2

    if-eqz p1, :cond_0

    const v0, 0x7f0a00d5

    :goto_0
    invoke-static {v2, v0}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 2679091
    invoke-virtual {p0}, LX/JJE;->getContext()Landroid/content/Context;

    move-result-object v1

    if-eqz p1, :cond_1

    const v0, 0x7f020372

    :goto_1
    invoke-static {v1, v0}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {p0, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2679092
    return-void

    .line 2679093
    :cond_0
    const v0, 0x7f0a00a8

    goto :goto_0

    .line 2679094
    :cond_1
    const v0, 0x7f02036a

    goto :goto_1
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2679095
    iget-object v0, p0, LX/JJE;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2679096
    return-void
.end method
