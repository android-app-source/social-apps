.class public LX/I4d;
.super LX/Cod;
.source ""

# interfaces
.implements LX/CnG;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cod",
        "<",
        "LX/I4U;",
        ">;",
        "Lcom/facebook/events/eventcollections/view/impl/block/MoreEventsLinkBlockView;"
    }
.end annotation


# instance fields
.field public a:LX/0Or;
    .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/events/common/EventAnalyticsParams;

.field private final d:Lcom/facebook/fig/button/FigButton;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2534269
    invoke-direct {p0, p1}, LX/Cod;-><init>(Landroid/view/View;)V

    .line 2534270
    const-class v0, LX/I4d;

    invoke-static {v0, p0}, LX/I4d;->a(Ljava/lang/Class;LX/02k;)V

    .line 2534271
    const v0, 0x7f0d0dbb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    iput-object v0, p0, LX/I4d;->d:Lcom/facebook/fig/button/FigButton;

    .line 2534272
    iget-object v0, p0, LX/I4d;->d:Lcom/facebook/fig/button/FigButton;

    new-instance v1, LX/I4c;

    invoke-direct {v1, p0}, LX/I4c;-><init>(LX/I4d;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2534273
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, LX/I4d;

    const/16 p0, 0xc

    invoke-static {v1, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    iput-object p0, p1, LX/I4d;->a:LX/0Or;

    iput-object v1, p1, LX/I4d;->b:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method
