.class public final LX/HY2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;

.field public final synthetic b:LX/2QQ;


# direct methods
.method public constructor <init>(LX/2QQ;Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;)V
    .locals 0

    .prologue
    .line 2480038
    iput-object p1, p0, LX/HY2;->b:LX/2QQ;

    iput-object p2, p0, LX/HY2;->a:Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 2480039
    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    if-eqz v0, :cond_0

    .line 2480040
    :goto_0
    return-void

    .line 2480041
    :cond_0
    iget-object v0, p0, LX/HY2;->b:LX/2QQ;

    const/4 v1, 0x0

    .line 2480042
    iput-object v1, v0, LX/2QQ;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2480043
    iget-object v0, p0, LX/HY2;->a:Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;

    sget-object v1, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest$FetchState;->NOT_FETCHED:Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest$FetchState;

    invoke-virtual {v0, v1}, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;->a(Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest$FetchState;)V

    .line 2480044
    const-string v0, "PlatformWebDialogsManifest"

    const-string v1, "Error when trying to download the dialog for : %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/HY2;->a:Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;

    invoke-virtual {v4}, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, p1, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2480045
    iget-object v0, p0, LX/HY2;->b:LX/2QQ;

    iget-object v0, v0, LX/2QQ;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HXy;

    .line 2480046
    iget-object v2, p0, LX/HY2;->a:Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;

    .line 2480047
    invoke-virtual {v2}, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, LX/HXy;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2480048
    iget-object v3, v0, LX/HXy;->b:Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;

    const-string v4, "platform_webview_method_refresh_failed"

    invoke-static {v3, v4}, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->a$redex0(Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;Ljava/lang/String;)V

    .line 2480049
    iget-object v3, v0, LX/HXy;->b:Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;

    invoke-static {v3}, Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;->o(Lcom/facebook/platform/webdialogs/PlatformWebDialogsFragment;)V

    .line 2480050
    :cond_1
    goto :goto_1

    .line 2480051
    :cond_2
    iget-object v0, p0, LX/HY2;->b:LX/2QQ;

    invoke-static {v0}, LX/2QQ;->f(LX/2QQ;)V

    goto :goto_0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2480052
    iget-object v0, p0, LX/HY2;->b:LX/2QQ;

    const/4 v1, 0x0

    .line 2480053
    iput-object v1, v0, LX/2QQ;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2480054
    iget-object v0, p0, LX/HY2;->a:Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;

    sget-object v1, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest$FetchState;->FETCHED:Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest$FetchState;

    invoke-virtual {v0, v1}, Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;->a(Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest$FetchState;)V

    .line 2480055
    iget-object v0, p0, LX/HY2;->b:LX/2QQ;

    iget-object v0, v0, LX/2QQ;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HXy;

    .line 2480056
    iget-object v2, p0, LX/HY2;->a:Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, LX/HXy;->a(Lcom/facebook/platform/webdialogs/PlatformWebViewActionManifest;Z)V

    goto :goto_0

    .line 2480057
    :cond_0
    iget-object v0, p0, LX/HY2;->b:LX/2QQ;

    invoke-static {v0}, LX/2QQ;->f(LX/2QQ;)V

    .line 2480058
    return-void
.end method
