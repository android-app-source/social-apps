.class public LX/IiC;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/IiC;


# instance fields
.field public a:LX/6cf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2603494
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/IiC;
    .locals 6

    .prologue
    .line 2603495
    sget-object v0, LX/IiC;->d:LX/IiC;

    if-nez v0, :cond_1

    .line 2603496
    const-class v1, LX/IiC;

    monitor-enter v1

    .line 2603497
    :try_start_0
    sget-object v0, LX/IiC;->d:LX/IiC;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2603498
    if-eqz v2, :cond_0

    .line 2603499
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2603500
    new-instance p0, LX/IiC;

    invoke-direct {p0}, LX/IiC;-><init>()V

    .line 2603501
    invoke-static {v0}, LX/6cf;->b(LX/0QB;)LX/6cf;

    move-result-object v3

    check-cast v3, LX/6cf;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    .line 2603502
    iput-object v3, p0, LX/IiC;->a:LX/6cf;

    iput-object v4, p0, LX/IiC;->b:LX/0Uh;

    iput-object v5, p0, LX/IiC;->c:LX/0ad;

    .line 2603503
    move-object v0, p0

    .line 2603504
    sput-object v0, LX/IiC;->d:LX/IiC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2603505
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2603506
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2603507
    :cond_1
    sget-object v0, LX/IiC;->d:LX/IiC;

    return-object v0

    .line 2603508
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2603509
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
