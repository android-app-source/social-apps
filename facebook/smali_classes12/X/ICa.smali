.class public final LX/ICa;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/ICk;

.field public final synthetic b:Landroid/view/View;

.field public final synthetic c:LX/ICb;


# direct methods
.method public constructor <init>(LX/ICb;LX/ICk;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2549573
    iput-object p1, p0, LX/ICa;->c:LX/ICb;

    iput-object p2, p0, LX/ICa;->a:LX/ICk;

    iput-object p3, p0, LX/ICa;->b:Landroid/view/View;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2549574
    iget-object v0, p0, LX/ICa;->c:LX/ICb;

    iget-object v0, v0, LX/ICb;->a:Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;

    iget-object v0, v0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->j:LX/0kL;

    new-instance v1, LX/27k;

    iget-object v2, p0, LX/ICa;->c:LX/ICb;

    iget-object v2, v2, LX/ICb;->a:Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;

    const v3, 0x7f08381b

    invoke-virtual {v2, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2549575
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2549576
    iget-object v0, p0, LX/ICa;->a:LX/ICk;

    const/4 v1, 0x1

    .line 2549577
    iput-boolean v1, v0, LX/ICk;->e:Z

    .line 2549578
    iget-object v0, p0, LX/ICa;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/facebook/friending/common/list/FriendListItemView;

    .line 2549579
    const v1, 0x7f08381a

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(I)V

    .line 2549580
    iget-object v1, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    move-object v0, v1

    .line 2549581
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2549582
    return-void
.end method
