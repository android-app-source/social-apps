.class public final enum LX/JTX;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/JTX;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/JTX;

.field public static final enum CTA_ACTION:LX/JTX;

.field public static final enum ERROR:LX/JTX;

.field public static final enum PLAYBACK_DURATION:LX/JTX;

.field public static final enum SONG_DURATION:LX/JTX;

.field public static final enum STALL_DURATION:LX/JTX;


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2696796
    new-instance v0, LX/JTX;

    const-string v1, "PLAYBACK_DURATION"

    const-string v2, "play_duration_ms"

    invoke-direct {v0, v1, v3, v2}, LX/JTX;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/JTX;->PLAYBACK_DURATION:LX/JTX;

    .line 2696797
    new-instance v0, LX/JTX;

    const-string v1, "SONG_DURATION"

    const-string v2, "audio_duration_ms"

    invoke-direct {v0, v1, v4, v2}, LX/JTX;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/JTX;->SONG_DURATION:LX/JTX;

    .line 2696798
    new-instance v0, LX/JTX;

    const-string v1, "STALL_DURATION"

    const-string v2, "stall_duration_ms"

    invoke-direct {v0, v1, v5, v2}, LX/JTX;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/JTX;->STALL_DURATION:LX/JTX;

    .line 2696799
    new-instance v0, LX/JTX;

    const-string v1, "ERROR"

    const-string v2, "error"

    invoke-direct {v0, v1, v6, v2}, LX/JTX;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/JTX;->ERROR:LX/JTX;

    .line 2696800
    new-instance v0, LX/JTX;

    const-string v1, "CTA_ACTION"

    const-string v2, "cta_action"

    invoke-direct {v0, v1, v7, v2}, LX/JTX;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/JTX;->CTA_ACTION:LX/JTX;

    .line 2696801
    const/4 v0, 0x5

    new-array v0, v0, [LX/JTX;

    sget-object v1, LX/JTX;->PLAYBACK_DURATION:LX/JTX;

    aput-object v1, v0, v3

    sget-object v1, LX/JTX;->SONG_DURATION:LX/JTX;

    aput-object v1, v0, v4

    sget-object v1, LX/JTX;->STALL_DURATION:LX/JTX;

    aput-object v1, v0, v5

    sget-object v1, LX/JTX;->ERROR:LX/JTX;

    aput-object v1, v0, v6

    sget-object v1, LX/JTX;->CTA_ACTION:LX/JTX;

    aput-object v1, v0, v7

    sput-object v0, LX/JTX;->$VALUES:[LX/JTX;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2696802
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2696803
    iput-object p3, p0, LX/JTX;->name:Ljava/lang/String;

    .line 2696804
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/JTX;
    .locals 1

    .prologue
    .line 2696805
    const-class v0, LX/JTX;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/JTX;

    return-object v0
.end method

.method public static values()[LX/JTX;
    .locals 1

    .prologue
    .line 2696806
    sget-object v0, LX/JTX;->$VALUES:[LX/JTX;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/JTX;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2696807
    iget-object v0, p0, LX/JTX;->name:Ljava/lang/String;

    return-object v0
.end method
