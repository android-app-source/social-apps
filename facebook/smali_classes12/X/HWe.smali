.class public final LX/HWe;
.super LX/CXt;
.source ""


# instance fields
.field public final synthetic b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;Landroid/os/ParcelUuid;)V
    .locals 0

    .prologue
    .line 2476903
    iput-object p1, p0, LX/HWe;->b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    invoke-direct {p0, p2}, LX/CXt;-><init>(Landroid/os/ParcelUuid;)V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 6

    .prologue
    .line 2476904
    iget-object v0, p0, LX/HWe;->b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    iget-wide v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->q:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 2476905
    iget-object v0, p0, LX/HWe;->b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    iget-object v1, p0, LX/HWe;->b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, LX/0ax;->aE:Ljava/lang/String;

    iget-object v3, p0, LX/HWe;->b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    iget-wide v4, v3, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->q:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2476906
    iget-object v0, p0, LX/HWe;->b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/HWe;->b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2476907
    :cond_0
    iget-object v0, p0, LX/HWe;->b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2476908
    return-void
.end method
