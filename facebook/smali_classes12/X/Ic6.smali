.class public LX/Ic6;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Ljava/lang/String;

.field private final d:LX/03V;

.field private final e:Lcom/facebook/content/SecureContextHelper;

.field private final f:LX/Ic5;

.field private final g:LX/0Uh;

.field private final h:LX/IZK;

.field private final i:LX/Ib2;

.field private final j:Lcom/facebook/messaging/business/ride/helper/RideProviderLoader;

.field private final k:LX/2Oi;

.field private final l:LX/0ad;

.field private final m:LX/0So;

.field private n:Landroid/os/Handler;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2595202
    new-instance v0, Ljava/lang/String;

    const v1, 0x1f697

    invoke-static {v1}, Ljava/lang/Character;->toChars(I)[C

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    sput-object v0, LX/Ic6;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/03V;Lcom/facebook/content/SecureContextHelper;LX/0Uh;LX/0Or;Lcom/facebook/messaging/business/ride/helper/RideProviderLoader;LX/IZK;LX/Ib2;LX/Ic4;LX/Ic5;LX/0ad;LX/2Oi;LX/0So;)V
    .locals 1
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/content/SecureContextHelper;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/messaging/business/ride/helper/RideProviderLoader;",
            "LX/IZK;",
            "LX/Ib2;",
            "LX/Ic4;",
            "LX/Ic5;",
            "LX/0ad;",
            "LX/2Oi;",
            "LX/0So;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2595185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2595186
    iput-object p1, p0, LX/Ic6;->b:Landroid/content/Context;

    .line 2595187
    iput-object p12, p0, LX/Ic6;->k:LX/2Oi;

    .line 2595188
    invoke-interface {p5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/Ic6;->c:Ljava/lang/String;

    .line 2595189
    iput-object p2, p0, LX/Ic6;->d:LX/03V;

    .line 2595190
    iput-object p3, p0, LX/Ic6;->e:Lcom/facebook/content/SecureContextHelper;

    .line 2595191
    iput-object p4, p0, LX/Ic6;->g:LX/0Uh;

    .line 2595192
    iput-object p6, p0, LX/Ic6;->j:Lcom/facebook/messaging/business/ride/helper/RideProviderLoader;

    .line 2595193
    iput-object p10, p0, LX/Ic6;->f:LX/Ic5;

    .line 2595194
    iput-object p7, p0, LX/Ic6;->h:LX/IZK;

    .line 2595195
    iput-object p8, p0, LX/Ic6;->i:LX/Ib2;

    .line 2595196
    iput-object p11, p0, LX/Ic6;->l:LX/0ad;

    .line 2595197
    iput-object p13, p0, LX/Ic6;->m:LX/0So;

    .line 2595198
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LX/Ic6;->n:Landroid/os/Handler;

    .line 2595199
    return-void
.end method

.method public static b(LX/0QB;)LX/Ic6;
    .locals 14

    .prologue
    .line 2595200
    new-instance v0, LX/Ic6;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    const/16 v5, 0x15e7

    invoke-static {p0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {p0}, Lcom/facebook/messaging/business/ride/helper/RideProviderLoader;->b(LX/0QB;)Lcom/facebook/messaging/business/ride/helper/RideProviderLoader;

    move-result-object v6

    check-cast v6, Lcom/facebook/messaging/business/ride/helper/RideProviderLoader;

    invoke-static {p0}, LX/IZK;->b(LX/0QB;)LX/IZK;

    move-result-object v7

    check-cast v7, LX/IZK;

    invoke-static {p0}, LX/Ib2;->b(LX/0QB;)LX/Ib2;

    move-result-object v8

    check-cast v8, LX/Ib2;

    invoke-static {p0}, LX/Ic4;->b(LX/0QB;)LX/Ic4;

    move-result-object v9

    check-cast v9, LX/Ic4;

    invoke-static {p0}, LX/Ic5;->b(LX/0QB;)LX/Ic5;

    move-result-object v10

    check-cast v10, LX/Ic5;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v11

    check-cast v11, LX/0ad;

    invoke-static {p0}, LX/2Oi;->a(LX/0QB;)LX/2Oi;

    move-result-object v12

    check-cast v12, LX/2Oi;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v13

    check-cast v13, LX/0So;

    invoke-direct/range {v0 .. v13}, LX/Ic6;-><init>(Landroid/content/Context;LX/03V;Lcom/facebook/content/SecureContextHelper;LX/0Uh;LX/0Or;Lcom/facebook/messaging/business/ride/helper/RideProviderLoader;LX/IZK;LX/Ib2;LX/Ic4;LX/Ic5;LX/0ad;LX/2Oi;LX/0So;)V

    .line 2595201
    return-object v0
.end method
