.class public LX/Itx;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:LX/Itw;

.field private final b:LX/Itv;

.field private final c:I

.field private final d:Ljava/lang/Exception;

.field private final e:Lcom/facebook/messaging/service/model/NewMessageResult;

.field private final f:Z

.field private final g:Ljava/lang/String;


# direct methods
.method private constructor <init>(LX/Itw;LX/Itv;ILjava/lang/Exception;Lcom/facebook/messaging/service/model/NewMessageResult;ZLjava/lang/String;)V
    .locals 4
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2625185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2625186
    sget-object v0, LX/Itu;->a:[I

    invoke-virtual {p1}, LX/Itw;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    .line 2625187
    :goto_0
    iput-object p1, p0, LX/Itx;->a:LX/Itw;

    .line 2625188
    iput-object p2, p0, LX/Itx;->b:LX/Itv;

    .line 2625189
    iput p3, p0, LX/Itx;->c:I

    .line 2625190
    iput-object p4, p0, LX/Itx;->d:Ljava/lang/Exception;

    .line 2625191
    iput-object p5, p0, LX/Itx;->e:Lcom/facebook/messaging/service/model/NewMessageResult;

    .line 2625192
    iput-boolean p6, p0, LX/Itx;->f:Z

    .line 2625193
    iput-object p7, p0, LX/Itx;->g:Ljava/lang/String;

    .line 2625194
    return-void

    .line 2625195
    :pswitch_0
    sget-object v0, LX/Itv;->NONE:LX/Itv;

    if-ne p2, v0, :cond_0

    move v0, v1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2625196
    if-nez p4, :cond_1

    :goto_2
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    goto :goto_0

    :cond_0
    move v0, v2

    .line 2625197
    goto :goto_1

    :cond_1
    move v1, v2

    .line 2625198
    goto :goto_2

    .line 2625199
    :pswitch_1
    sget-object v0, LX/Itv;->NONE:LX/Itv;

    if-eq p2, v0, :cond_2

    move v0, v1

    :goto_3
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2625200
    if-nez p5, :cond_3

    :goto_4
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 2625201
    goto :goto_3

    :cond_3
    move v1, v2

    .line 2625202
    goto :goto_4

    .line 2625203
    :pswitch_2
    sget-object v0, LX/Itv;->NONE:LX/Itv;

    if-eq p2, v0, :cond_4

    move v0, v1

    :goto_5
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2625204
    if-nez p5, :cond_5

    :goto_6
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    goto :goto_0

    :cond_4
    move v0, v2

    .line 2625205
    goto :goto_5

    :cond_5
    move v1, v2

    .line 2625206
    goto :goto_6

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(IZLjava/lang/String;)LX/Itx;
    .locals 8
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 2625184
    new-instance v0, LX/Itx;

    sget-object v1, LX/Itw;->FAILED:LX/Itw;

    sget-object v2, LX/Itv;->SEND_FAILED_NO_RETRY:LX/Itv;

    move v3, p0

    move-object v5, v4

    move v6, p1

    move-object v7, p2

    invoke-direct/range {v0 .. v7}, LX/Itx;-><init>(LX/Itw;LX/Itv;ILjava/lang/Exception;Lcom/facebook/messaging/service/model/NewMessageResult;ZLjava/lang/String;)V

    return-object v0
.end method

.method public static a(LX/Itv;)LX/Itx;
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 2625183
    new-instance v0, LX/Itx;

    sget-object v1, LX/Itw;->SKIPPED:LX/Itw;

    move-object v2, p0

    move-object v5, v4

    move v6, v3

    move-object v7, v4

    invoke-direct/range {v0 .. v7}, LX/Itx;-><init>(LX/Itw;LX/Itv;ILjava/lang/Exception;Lcom/facebook/messaging/service/model/NewMessageResult;ZLjava/lang/String;)V

    return-object v0
.end method

.method public static a(LX/Itv;ILjava/lang/String;Z)LX/Itx;
    .locals 8
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 2625182
    new-instance v0, LX/Itx;

    sget-object v1, LX/Itw;->FAILED:LX/Itw;

    move-object v2, p0

    move v3, p1

    move-object v5, v4

    move v6, p3

    move-object v7, p2

    invoke-direct/range {v0 .. v7}, LX/Itx;-><init>(LX/Itw;LX/Itv;ILjava/lang/Exception;Lcom/facebook/messaging/service/model/NewMessageResult;ZLjava/lang/String;)V

    return-object v0
.end method

.method public static a(LX/Itv;IZ)LX/Itx;
    .locals 1

    .prologue
    .line 2625181
    const/4 v0, 0x0

    invoke-static {p0, p1, v0, p2}, LX/Itx;->a(LX/Itv;ILjava/lang/String;Z)LX/Itx;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/messaging/service/model/NewMessageResult;Z)LX/Itx;
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 2625159
    new-instance v0, LX/Itx;

    sget-object v1, LX/Itw;->SUCCEEDED:LX/Itw;

    sget-object v2, LX/Itv;->NONE:LX/Itv;

    const/4 v3, 0x0

    move-object v5, p0

    move v6, p1

    move-object v7, v4

    invoke-direct/range {v0 .. v7}, LX/Itx;-><init>(LX/Itw;LX/Itv;ILjava/lang/Exception;Lcom/facebook/messaging/service/model/NewMessageResult;ZLjava/lang/String;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Exception;I)LX/Itx;
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 2625180
    new-instance v0, LX/Itx;

    sget-object v1, LX/Itw;->FAILED:LX/Itw;

    sget-object v2, LX/Itv;->SEND_FAILED_UNKNOWN_EXCEPTION:LX/Itv;

    const/4 v6, 0x0

    move v3, p1

    move-object v4, p0

    move-object v7, v5

    invoke-direct/range {v0 .. v7}, LX/Itx;-><init>(LX/Itw;LX/Itv;ILjava/lang/Exception;Lcom/facebook/messaging/service/model/NewMessageResult;ZLjava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final a()LX/Itw;
    .locals 1

    .prologue
    .line 2625179
    iget-object v0, p0, LX/Itx;->a:LX/Itw;

    return-object v0
.end method

.method public final b()Lcom/facebook/messaging/service/model/NewMessageResult;
    .locals 2

    .prologue
    .line 2625176
    iget-object v0, p0, LX/Itx;->a:LX/Itw;

    sget-object v1, LX/Itw;->SUCCEEDED:LX/Itw;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2625177
    iget-object v0, p0, LX/Itx;->e:Lcom/facebook/messaging/service/model/NewMessageResult;

    return-object v0

    .line 2625178
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2625173
    iget-object v0, p0, LX/Itx;->a:LX/Itw;

    sget-object v1, LX/Itw;->SKIPPED:LX/Itw;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2625174
    iget-object v0, p0, LX/Itx;->b:LX/Itv;

    iget-object v0, v0, LX/Itv;->message:Ljava/lang/String;

    return-object v0

    .line 2625175
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2625166
    iget-object v0, p0, LX/Itx;->a:LX/Itw;

    invoke-virtual {v0}, LX/Itw;->isFailure()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2625167
    iget-object v0, p0, LX/Itx;->b:LX/Itv;

    sget-object v1, LX/Itv;->SEND_FAILED_UNKNOWN_EXCEPTION:LX/Itv;

    if-ne v0, v1, :cond_0

    .line 2625168
    iget-object v0, p0, LX/Itx;->b:LX/Itv;

    iget-object v0, v0, LX/Itv;->message:Ljava/lang/String;

    iget-object v1, p0, LX/Itx;->d:Ljava/lang/Exception;

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2625169
    :goto_0
    return-object v0

    .line 2625170
    :cond_0
    iget-object v0, p0, LX/Itx;->b:LX/Itv;

    sget-object v1, LX/Itv;->SEND_FAILED_PUBLISH_FAILED_WITH_EXCEPTION:LX/Itv;

    if-ne v0, v1, :cond_1

    .line 2625171
    const-string v0, "%s %s"

    iget-object v1, p0, LX/Itx;->b:LX/Itv;

    iget-object v1, v1, LX/Itv;->message:Ljava/lang/String;

    iget-object v2, p0, LX/Itx;->g:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2625172
    :cond_1
    iget-object v0, p0, LX/Itx;->b:LX/Itv;

    iget-object v0, v0, LX/Itv;->message:Ljava/lang/String;

    goto :goto_0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 2625164
    iget-object v0, p0, LX/Itx;->a:LX/Itw;

    invoke-virtual {v0}, LX/Itw;->isFailure()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2625165
    iget v0, p0, LX/Itx;->c:I

    return v0
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 2625163
    iget-object v0, p0, LX/Itx;->a:LX/Itw;

    invoke-virtual {v0}, LX/Itw;->isFailure()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Itx;->b:LX/Itv;

    sget-object v1, LX/Itv;->SEND_FAILED_NO_RETRY:LX/Itv;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 2625162
    iget-boolean v0, p0, LX/Itx;->f:Z

    return v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2625160
    iget-object v0, p0, LX/Itx;->a:LX/Itw;

    invoke-virtual {v0}, LX/Itw;->isFailure()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2625161
    iget-object v0, p0, LX/Itx;->g:Ljava/lang/String;

    return-object v0
.end method
