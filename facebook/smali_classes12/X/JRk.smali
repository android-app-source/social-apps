.class public final LX/JRk;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/3he;

.field public final synthetic b:LX/3ho;


# direct methods
.method public constructor <init>(LX/3ho;LX/3he;)V
    .locals 0

    .prologue
    .line 2693721
    iput-object p1, p0, LX/JRk;->b:LX/3ho;

    iput-object p2, p0, LX/JRk;->a:LX/3he;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2693722
    iget-object v0, p0, LX/JRk;->a:LX/3he;

    invoke-static {v0}, LX/3ho;->g(LX/3he;)V

    .line 2693723
    iget-object v0, p0, LX/JRk;->a:LX/3he;

    invoke-static {v0}, LX/3ho;->d(LX/3he;)V

    .line 2693724
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 14
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2693725
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2693726
    iget-object v0, p0, LX/JRk;->a:LX/3he;

    invoke-static {v0}, LX/3ho;->g(LX/3he;)V

    .line 2693727
    if-nez p1, :cond_0

    .line 2693728
    iget-object v0, p0, LX/JRk;->a:LX/3he;

    invoke-static {v0}, LX/3ho;->d(LX/3he;)V

    .line 2693729
    :goto_0
    return-void

    .line 2693730
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2693731
    check-cast v0, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel;

    .line 2693732
    const/4 v4, 0x0

    .line 2693733
    invoke-virtual {v0}, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel;->a()Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel;->a()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    const/4 v0, 0x0

    move v7, v0

    :goto_1
    if-ge v7, v9, :cond_7

    invoke-virtual {v8, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel;

    .line 2693734
    iget-object v1, p0, LX/JRk;->a:LX/3he;

    invoke-virtual {v1}, LX/3he;->a()I

    move-result v1

    iget-object v2, p0, LX/JRk;->b:LX/3ho;

    iget v2, v2, LX/3ho;->f:I

    if-ge v1, v2, :cond_7

    .line 2693735
    invoke-virtual {v0}, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel;->a()Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel;->a()Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel;

    move-result-object v6

    .line 2693736
    if-eqz v6, :cond_9

    .line 2693737
    invoke-virtual {v6}, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel;->l()Ljava/lang/String;

    move-result-object v1

    .line 2693738
    invoke-virtual {v6}, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel;->m()Ljava/lang/String;

    move-result-object v2

    .line 2693739
    const/4 v3, 0x0

    .line 2693740
    const/4 v5, 0x0

    .line 2693741
    invoke-virtual {v6}, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2693742
    invoke-virtual {v6}, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel;->j()LX/1vs;

    move-result-object v0

    iget-object v5, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v10, 0x0

    invoke-virtual {v5, v0, v10}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    .line 2693743
    :cond_1
    invoke-virtual {v6}, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel;->k()Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel$MediaModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 2693744
    invoke-virtual {v6}, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel;->k()Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel$MediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel$MediaModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2693745
    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_2

    .line 2693746
    invoke-virtual {v6}, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel;->k()Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel$MediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel$MediaModel;->a()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v10, 0x0

    invoke-virtual {v3, v0, v10}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    .line 2693747
    :cond_2
    if-eqz v1, :cond_9

    if-eqz v2, :cond_9

    if-eqz v3, :cond_9

    .line 2693748
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 2693749
    iget-object v0, p0, LX/JRk;->a:LX/3he;

    invoke-virtual {v0}, LX/3he;->b()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 2693750
    const/4 v10, 0x0

    .line 2693751
    invoke-virtual {v6}, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel;->a()LX/2uF;

    move-result-object v11

    if-eqz v11, :cond_a

    if-eqz v0, :cond_a

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v11

    if-eqz v11, :cond_a

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v11

    invoke-virtual {v11}, LX/0Px;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_a

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v11

    invoke-virtual {v11, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v11}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v11

    if-eqz v11, :cond_a

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v11

    invoke-virtual {v11, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v11}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v11

    invoke-virtual {v11}, LX/0Px;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_a

    const/4 v11, 0x1

    :goto_3
    move v11, v11

    .line 2693752
    move v10, v11

    .line 2693753
    if-eqz v10, :cond_6

    .line 2693754
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v0

    const/4 v10, 0x0

    invoke-virtual {v0, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v0

    const/4 v10, 0x0

    invoke-virtual {v0, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 2693755
    invoke-virtual {v6}, Lcom/facebook/feedplugins/multishare/protocol/GraphQLMultiShareInfiniteCarouselModels$MultiShareQueryModel$CarouselInfiniteScrollModel$EdgesModel$NodeModel$SubattachmentModel;->a()LX/2uF;

    move-result-object v6

    invoke-virtual {v6}, LX/3Sa;->e()LX/3Sh;

    move-result-object v6

    :cond_3
    :goto_4
    invoke-interface {v6}, LX/2sN;->a()Z

    move-result v10

    if-eqz v10, :cond_6

    .line 2693756
    invoke-interface {v6}, LX/2sN;->b()LX/1vs;

    move-result-object v10

    .line 2693757
    iget-object v11, v10, LX/1vs;->a:LX/15i;

    iget v10, v10, LX/1vs;->b:I

    .line 2693758
    const/4 v12, 0x1

    invoke-virtual {v11, v10, v12}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_3

    const/4 v12, 0x2

    invoke-virtual {v11, v10, v12}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_3

    .line 2693759
    invoke-static {v0}, LX/4Ys;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)LX/4Ys;

    move-result-object v12

    const/4 v13, 0x1

    invoke-virtual {v11, v10, v13}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v13

    .line 2693760
    iput-object v13, v12, LX/4Ys;->bz:Ljava/lang/String;

    .line 2693761
    move-object v12, v12

    .line 2693762
    const/4 v13, 0x2

    invoke-virtual {v11, v10, v13}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v10

    .line 2693763
    iput-object v10, v12, LX/4Ys;->bD:Ljava/lang/String;

    .line 2693764
    move-object v10, v12

    .line 2693765
    invoke-virtual {v10}, LX/4Ys;->a()Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v10

    invoke-virtual {v4, v10}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_4

    .line 2693766
    :cond_4
    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 2693767
    :cond_6
    const/4 v6, 0x1

    .line 2693768
    iget-object v0, p0, LX/JRk;->a:LX/3he;

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    invoke-virtual/range {v0 .. v5}, LX/3he;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/String;)V

    move v0, v6

    .line 2693769
    :goto_5
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    move v4, v0

    goto/16 :goto_1

    .line 2693770
    :cond_7
    if-eqz v4, :cond_8

    .line 2693771
    iget-object v0, p0, LX/JRk;->a:LX/3he;

    invoke-virtual {v0}, LX/3he;->c()V

    .line 2693772
    :goto_6
    iget-object v0, p0, LX/JRk;->a:LX/3he;

    invoke-static {v0}, LX/3ho;->g(LX/3he;)V

    goto/16 :goto_0

    .line 2693773
    :cond_8
    iget-object v0, p0, LX/JRk;->a:LX/3he;

    invoke-static {v0}, LX/3ho;->d(LX/3he;)V

    goto :goto_6

    :cond_9
    move v0, v4

    goto :goto_5

    :cond_a
    move v11, v10

    goto/16 :goto_3
.end method
