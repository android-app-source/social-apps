.class public LX/JUp;
.super LX/An9;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ">",
        "LX/An9",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnitItem;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JUe;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JUe;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2699796
    invoke-direct {p0}, LX/An9;-><init>()V

    .line 2699797
    iput-object p1, p0, LX/JUp;->a:LX/0Ot;

    .line 2699798
    return-void
.end method

.method public static a(LX/0QB;)LX/JUp;
    .locals 4

    .prologue
    .line 2699799
    const-class v1, LX/JUp;

    monitor-enter v1

    .line 2699800
    :try_start_0
    sget-object v0, LX/JUp;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2699801
    sput-object v2, LX/JUp;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2699802
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2699803
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2699804
    new-instance v3, LX/JUp;

    const/16 p0, 0x207f

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JUp;-><init>(LX/0Ot;)V

    .line 2699805
    move-object v0, v3

    .line 2699806
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2699807
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JUp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2699808
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2699809
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/3mj;Ljava/lang/Object;Ljava/lang/Object;)LX/1X1;
    .locals 2

    .prologue
    .line 2699810
    check-cast p4, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnitItem;

    check-cast p5, LX/1Pr;

    .line 2699811
    iget-object v0, p0, LX/JUp;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JUe;

    const/4 v1, 0x0

    .line 2699812
    new-instance p0, LX/JUd;

    invoke-direct {p0, v0}, LX/JUd;-><init>(LX/JUe;)V

    .line 2699813
    iget-object p3, v0, LX/JUe;->b:LX/0Zi;

    invoke-virtual {p3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, LX/JUc;

    .line 2699814
    if-nez p3, :cond_0

    .line 2699815
    new-instance p3, LX/JUc;

    invoke-direct {p3, v0}, LX/JUc;-><init>(LX/JUe;)V

    .line 2699816
    :cond_0
    invoke-static {p3, p1, v1, v1, p0}, LX/JUc;->a$redex0(LX/JUc;LX/1De;IILX/JUd;)V

    .line 2699817
    move-object p0, p3

    .line 2699818
    move-object v1, p0

    .line 2699819
    move-object v0, v1

    .line 2699820
    iget-object v1, v0, LX/JUc;->a:LX/JUd;

    iput-object p2, v1, LX/JUd;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2699821
    iget-object v1, v0, LX/JUc;->e:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, Ljava/util/BitSet;->set(I)V

    .line 2699822
    move-object v0, v0

    .line 2699823
    iget-object v1, v0, LX/JUc;->a:LX/JUd;

    iput-object p4, v1, LX/JUd;->b:Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnitItem;

    .line 2699824
    iget-object v1, v0, LX/JUc;->e:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v1, p0}, Ljava/util/BitSet;->set(I)V

    .line 2699825
    move-object v0, v0

    .line 2699826
    iget-object v1, v0, LX/JUc;->a:LX/JUd;

    iput-object p5, v1, LX/JUd;->c:LX/1Pr;

    .line 2699827
    iget-object v1, v0, LX/JUc;->e:Ljava/util/BitSet;

    const/4 p0, 0x2

    invoke-virtual {v1, p0}, Ljava/util/BitSet;->set(I)V

    .line 2699828
    move-object v0, v0

    .line 2699829
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2699830
    const-class v0, Lcom/facebook/graphql/model/GraphQLPageContextualRecommendationsFeedUnit;

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2699831
    const/16 v0, 0x8

    return v0
.end method
