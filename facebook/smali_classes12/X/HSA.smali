.class public final LX/HSA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;)V
    .locals 0

    .prologue
    .line 2466629
    iput-object p1, p0, LX/HSA;->a:Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x445b96f6

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2466630
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HSF;

    .line 2466631
    iget-object v2, p0, LX/HSA;->a:Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;

    iget-object v2, v2, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->k:LX/HSE;

    iget-object v2, v2, LX/HSE;->g:LX/HSF;

    if-eq v0, v2, :cond_1

    .line 2466632
    iget-object v2, p0, LX/HSA;->a:Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;

    iget-object v2, v2, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->k:LX/HSE;

    iget-object v2, v2, LX/HSE;->g:LX/HSF;

    .line 2466633
    iget-object v3, p0, LX/HSA;->a:Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;

    iget v4, v2, LX/HSF;->tabButtonId:I

    invoke-static {v3, v4}, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->a(Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;I)Landroid/view/View;

    move-result-object v3

    .line 2466634
    iget-object v4, p0, LX/HSA;->a:Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;

    .line 2466635
    invoke-static {v4, v0}, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->a$redex0(Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;LX/HSF;)V

    .line 2466636
    sget-object v4, LX/HSF;->INSIGHTS:LX/HSF;

    invoke-virtual {v0, v4}, LX/HSF;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2466637
    iget-object v4, p0, LX/HSA;->a:Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;

    const-wide/16 v6, 0x0

    invoke-virtual {v4, v6, v7}, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->setInsightsBadgeCount(J)V

    .line 2466638
    :cond_0
    iget-object v4, p0, LX/HSA;->a:Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;

    invoke-static {v4, v2}, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->b(Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;LX/HSF;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2466639
    iget-object v2, p0, LX/HSA;->a:Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;

    invoke-static {v2, v0}, Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;->b(Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;LX/HSF;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2466640
    :cond_1
    const v0, -0x22754915

    invoke-static {v5, v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
