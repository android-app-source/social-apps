.class public LX/JLQ;
.super LX/5pb;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "JSCPerfRecorder"
.end annotation


# instance fields
.field private a:LX/30K;


# direct methods
.method public constructor <init>(LX/5pY;LX/30K;)V
    .locals 0
    .param p1    # LX/5pY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2681045
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 2681046
    iput-object p2, p0, LX/JLQ;->a:LX/30K;

    .line 2681047
    return-void
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2681048
    const-string v0, "JSCPerfRecorder"

    return-object v0
.end method

.method public recordJSCPerf(LX/5pG;)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2681043
    iget-object v0, p0, LX/JLQ;->a:LX/30K;

    new-instance v1, LX/30L;

    invoke-direct {v1, p1}, LX/30L;-><init>(LX/5pG;)V

    invoke-virtual {v0, v1}, LX/30K;->a(LX/30L;)V

    .line 2681044
    return-void
.end method
