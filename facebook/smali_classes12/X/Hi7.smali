.class public final LX/Hi7;
.super LX/Hi5;
.source ""


# instance fields
.field public final synthetic a:LX/Hi9;

.field private b:I


# direct methods
.method public constructor <init>(LX/Hi9;LX/0gG;)V
    .locals 1
    .param p2    # LX/0gG;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 2496814
    iput-object p1, p0, LX/Hi7;->a:LX/Hi9;

    .line 2496815
    invoke-direct {p0, p2}, LX/Hi5;-><init>(LX/0gG;)V

    .line 2496816
    invoke-virtual {p2}, LX/0gG;->b()I

    move-result v0

    iput v0, p0, LX/Hi7;->b:I

    .line 2496817
    return-void
.end method

.method private a(I)I
    .locals 1

    .prologue
    .line 2496818
    invoke-virtual {p0}, LX/Hi5;->b()I

    move-result v0

    sub-int/2addr v0, p1

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public static e(LX/Hi7;)V
    .locals 1

    .prologue
    .line 2496819
    invoke-virtual {p0}, LX/0gG;->b()I

    move-result v0

    iput v0, p0, LX/Hi7;->b:I

    .line 2496820
    return-void
.end method


# virtual methods
.method public final G_(I)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 2496821
    invoke-direct {p0, p1}, LX/Hi7;->a(I)I

    move-result v0

    invoke-super {p0, v0}, LX/Hi5;->G_(I)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 2496822
    invoke-super {p0, p1}, LX/Hi5;->a(Ljava/lang/Object;)I

    move-result v0

    .line 2496823
    if-gez v0, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, v0}, LX/Hi7;->a(I)I

    move-result v0

    goto :goto_0
.end method

.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2496824
    invoke-direct {p0, p2}, LX/Hi7;->a(I)I

    move-result v0

    invoke-super {p0, p1, v0}, LX/Hi5;->a(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2496825
    invoke-direct {p0, p2}, LX/Hi7;->a(I)I

    move-result v0

    invoke-super {p0, p1, v0, p3}, LX/Hi5;->a(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 2496826
    return-void
.end method

.method public final b(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2496827
    iget v0, p0, LX/Hi7;->b:I

    sub-int/2addr v0, p2

    add-int/lit8 v0, v0, -0x1

    invoke-super {p0, p1, v0, p3}, LX/Hi5;->b(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 2496828
    return-void
.end method

.method public final d(I)F
    .locals 1

    .prologue
    .line 2496829
    invoke-direct {p0, p1}, LX/Hi7;->a(I)I

    move-result v0

    invoke-super {p0, v0}, LX/Hi5;->d(I)F

    move-result v0

    return v0
.end method
