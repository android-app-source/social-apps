.class public LX/HEz;
.super LX/0ht;
.source ""

# interfaces
.implements LX/02k;


# instance fields
.field public a:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/HFc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/HEt;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field public p:Landroid/view/View;

.field public q:LX/HEy;

.field private r:Ljava/lang/String;

.field public s:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public final t:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2442951
    invoke-direct {p0, p1}, LX/0ht;-><init>(Landroid/content/Context;)V

    .line 2442952
    new-instance v0, LX/HEx;

    invoke-direct {v0, p0}, LX/HEx;-><init>(LX/HEz;)V

    iput-object v0, p0, LX/HEz;->t:Landroid/view/View$OnClickListener;

    .line 2442953
    const-class v0, LX/HEz;

    invoke-static {v0, p0}, LX/HEz;->a(Ljava/lang/Class;LX/02k;)V

    .line 2442954
    iput-object p2, p0, LX/HEz;->r:Ljava/lang/String;

    .line 2442955
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/HEz;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v1

    check-cast v1, LX/1Ck;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-static {p0}, LX/HFc;->b(LX/0QB;)LX/HFc;

    move-result-object v3

    check-cast v3, LX/HFc;

    new-instance v0, LX/HEt;

    invoke-direct {v0}, LX/HEt;-><init>()V

    move-object v0, v0

    move-object p0, v0

    check-cast p0, LX/HEt;

    iput-object v1, p1, LX/HEz;->a:LX/1Ck;

    iput-object v2, p1, LX/HEz;->l:LX/03V;

    iput-object v3, p1, LX/HEz;->m:LX/HFc;

    iput-object p0, p1, LX/HEz;->n:LX/HEt;

    return-void
.end method


# virtual methods
.method public final a(LX/HEy;)V
    .locals 6

    .prologue
    .line 2442923
    iput-object p1, p0, LX/HEz;->q:LX/HEy;

    .line 2442924
    invoke-virtual {p0}, LX/0ht;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030e3a

    iget-object p1, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v0, v1, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/HEz;->p:Landroid/view/View;

    .line 2442925
    iget-object v0, p0, LX/HEz;->p:Landroid/view/View;

    const v1, 0x7f0d05af

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, LX/HEz;->s:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2442926
    iget-object v0, p0, LX/HEz;->p:Landroid/view/View;

    const v1, 0x7f0d22ab

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, LX/HEz;->o:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2442927
    iget-object v0, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 2442928
    const/4 v1, -0x1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 2442929
    const/16 v0, 0x3eb

    .line 2442930
    iput v0, p0, LX/0ht;->s:I

    .line 2442931
    iget-object v0, p0, LX/HEz;->r:Ljava/lang/String;

    .line 2442932
    iget-object v1, p0, LX/HEz;->o:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 2442933
    iget-object v1, p0, LX/HEz;->o:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 2442934
    iget-object v1, p0, LX/HEz;->a:LX/1Ck;

    const-string v2, "fetch_category_gql_task_key"

    iget-object v3, p0, LX/HEz;->m:LX/HFc;

    .line 2442935
    new-instance v4, LX/HGT;

    invoke-direct {v4}, LX/HGT;-><init>()V

    move-object v4, v4

    .line 2442936
    const-string v5, "input"

    invoke-virtual {v4, v5, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    check-cast v4, LX/HGT;

    .line 2442937
    iget-object v5, v3, LX/HFc;->a:LX/0tX;

    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    invoke-virtual {v5, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v4

    new-instance v5, LX/HFZ;

    invoke-direct {v5, v3}, LX/HFZ;-><init>(LX/HFc;)V

    .line 2442938
    sget-object p1, LX/131;->INSTANCE:LX/131;

    move-object p1, p1

    .line 2442939
    invoke-static {v4, v5, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v3, v4

    .line 2442940
    new-instance v4, LX/HEw;

    invoke-direct {v4, p0}, LX/HEw;-><init>(LX/HEz;)V

    invoke-virtual {v1, v2, v3, v4}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2442941
    iget-object v0, p0, LX/HEz;->p:Landroid/view/View;

    const v1, 0x7f0d22ac

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    .line 2442942
    iget-object v1, p0, LX/HEz;->s:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 2442943
    iget-object v1, p0, LX/HEz;->n:LX/HEt;

    iget-object v2, p0, LX/HEz;->t:Landroid/view/View$OnClickListener;

    .line 2442944
    iput-object v2, v1, LX/HEt;->b:Landroid/view/View$OnClickListener;

    .line 2442945
    iget-object v1, p0, LX/HEz;->s:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v2, p0, LX/HEz;->n:LX/HEt;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2442946
    new-instance v1, LX/HEv;

    invoke-direct {v1, p0}, LX/HEv;-><init>(LX/HEz;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2442947
    :cond_0
    new-instance v0, LX/1P1;

    invoke-virtual {p0}, LX/0ht;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1P1;-><init>(Landroid/content/Context;)V

    .line 2442948
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/1P1;->b(I)V

    .line 2442949
    iget-object v1, p0, LX/HEz;->s:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2442950
    return-void
.end method
