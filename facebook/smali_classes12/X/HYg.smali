.class public LX/HYg;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public b:LX/0dC;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0dC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2480959
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2480960
    iput-object p1, p0, LX/HYg;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2480961
    iput-object p2, p0, LX/HYg;->b:LX/0dC;

    .line 2480962
    return-void
.end method

.method public static b(LX/0QB;)LX/HYg;
    .locals 3

    .prologue
    .line 2480965
    new-instance v2, LX/HYg;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0dB;->b(LX/0QB;)LX/0dC;

    move-result-object v1

    check-cast v1, LX/0dC;

    invoke-direct {v2, v0, v1}, LX/HYg;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0dC;)V

    .line 2480966
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2480963
    iget-object v0, p0, LX/HYg;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/Ha8;->b:LX/0Tn;

    invoke-interface {v0, v1, p1}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2480964
    return-void
.end method
