.class public LX/IBH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/IB8;


# instance fields
.field public final a:LX/1ay;

.field public final b:LX/1Kf;

.field private final c:LX/IBL;

.field public final d:LX/01T;

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1ay;LX/1Kf;LX/IBL;LX/01T;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ay;",
            "LX/1Kf;",
            "LX/IBL;",
            "LX/01T;",
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2546737
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2546738
    iput-object p2, p0, LX/IBH;->b:LX/1Kf;

    .line 2546739
    iput-object p1, p0, LX/IBH;->a:LX/1ay;

    .line 2546740
    iput-object p3, p0, LX/IBH;->c:LX/IBL;

    .line 2546741
    iput-object p4, p0, LX/IBH;->d:LX/01T;

    .line 2546742
    iput-object p5, p0, LX/IBH;->e:LX/0Ot;

    .line 2546743
    return-void
.end method

.method public static a(LX/0QB;)LX/IBH;
    .locals 1

    .prologue
    .line 2546791
    invoke-static {p0}, LX/IBH;->b(LX/0QB;)LX/IBH;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/events/model/Event;Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/facebook/ipc/composer/intent/ComposerPageData;
    .locals 2

    .prologue
    .line 2546786
    invoke-static {}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->newBuilder()Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v0

    .line 2546787
    iget-object v1, p0, Lcom/facebook/events/model/Event;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2546788
    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPageName(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v0

    .line 2546789
    iget-object v1, p0, Lcom/facebook/events/model/Event;->x:Ljava/lang/String;

    move-object v1, v1

    .line 2546790
    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPageProfilePicUrl(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPostAsPageViewerContext(Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/events/model/Event;Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;Lcom/facebook/auth/viewercontext/ViewerContext;)V
    .locals 1

    .prologue
    .line 2546782
    iget-object v0, p0, Lcom/facebook/events/model/Event;->w:Ljava/lang/String;

    move-object v0, v0

    .line 2546783
    if-eqz v0, :cond_0

    .line 2546784
    invoke-static {p0, p2}, LX/IBH;->a(Lcom/facebook/events/model/Event;Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialPageData(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 2546785
    :cond_0
    return-void
.end method

.method public static b(LX/0QB;)LX/IBH;
    .locals 6

    .prologue
    .line 2546780
    new-instance v0, LX/IBH;

    invoke-static {p0}, LX/1ay;->b(LX/0QB;)LX/1ay;

    move-result-object v1

    check-cast v1, LX/1ay;

    invoke-static {p0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v2

    check-cast v2, LX/1Kf;

    invoke-static {p0}, LX/IBL;->a(LX/0QB;)LX/IBL;

    move-result-object v3

    check-cast v3, LX/IBL;

    invoke-static {p0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v4

    check-cast v4, LX/01T;

    const/16 v5, 0x1032

    invoke-static {p0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, LX/IBH;-><init>(LX/1ay;LX/1Kf;LX/IBL;LX/01T;LX/0Ot;)V

    .line 2546781
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/auth/viewercontext/ViewerContext;Landroid/content/Context;Lcom/facebook/events/model/Event;)Landroid/content/Intent;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2546762
    new-instance v0, LX/89I;

    .line 2546763
    iget-object v1, p3, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2546764
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    sget-object v1, LX/2rw;->EVENT:LX/2rw;

    invoke-direct {v0, v2, v3, v1}, LX/89I;-><init>(JLX/2rw;)V

    .line 2546765
    iget-object v1, p3, Lcom/facebook/events/model/Event;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2546766
    iput-object v1, v0, LX/89I;->c:Ljava/lang/String;

    .line 2546767
    move-object v0, v0

    .line 2546768
    iget-object v1, p3, Lcom/facebook/events/model/Event;->x:Ljava/lang/String;

    move-object v1, v1

    .line 2546769
    iput-object v1, v0, LX/89I;->d:Ljava/lang/String;

    .line 2546770
    move-object v0, v0

    .line 2546771
    invoke-virtual {v0}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    .line 2546772
    sget-object v1, LX/21D;->EVENT:LX/21D;

    const-string v2, "eventWallPhoto"

    invoke-static {v1, v2}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    iget-object v0, p0, LX/IBH;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v2, LX/1EB;->an:S

    invoke-interface {v0, v2, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setUsePublishExperiment(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    .line 2546773
    invoke-static {p3, v0, p1}, LX/IBH;->a(Lcom/facebook/events/model/Event;Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;Lcom/facebook/auth/viewercontext/ViewerContext;)V

    .line 2546774
    new-instance v1, LX/8AA;

    sget-object v2, LX/8AB;->EVENT:LX/8AB;

    invoke-direct {v1, v2}, LX/8AA;-><init>(LX/8AB;)V

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    .line 2546775
    iput-object v0, v1, LX/8AA;->a:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 2546776
    move-object v1, v1

    .line 2546777
    iget-object v0, p0, LX/IBH;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v2, LX/1EB;->ah:S

    invoke-interface {v0, v2, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2546778
    invoke-virtual {v1}, LX/8AA;->b()LX/8AA;

    .line 2546779
    :cond_0
    invoke-static {p2, v1}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->a(Landroid/content/Context;LX/8AA;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lcom/facebook/events/model/Event;)V
    .locals 2

    .prologue
    .line 2546760
    iget-object v0, p0, LX/IBH;->c:LX/IBL;

    new-instance v1, LX/IBF;

    invoke-direct {v1, p0, p2, p1}, LX/IBF;-><init>(LX/IBH;Lcom/facebook/events/model/Event;Landroid/content/Context;)V

    invoke-virtual {v0, p2, v1}, LX/IBL;->a(Lcom/facebook/events/model/Event;LX/0QK;)V

    .line 2546761
    return-void
.end method

.method public final b(Lcom/facebook/auth/viewercontext/ViewerContext;Lcom/facebook/events/model/Event;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2546746
    sget-object v0, LX/21D;->EVENT:LX/21D;

    const-string v2, "eventWallStatus"

    invoke-static {v0, v2}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    new-instance v2, LX/89I;

    .line 2546747
    iget-object v3, p2, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v3, v3

    .line 2546748
    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    sget-object v3, LX/2rw;->EVENT:LX/2rw;

    invoke-direct {v2, v4, v5, v3}, LX/89I;-><init>(JLX/2rw;)V

    .line 2546749
    iget-object v3, p2, Lcom/facebook/events/model/Event;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2546750
    iput-object v3, v2, LX/89I;->c:Ljava/lang/String;

    .line 2546751
    move-object v2, v2

    .line 2546752
    iget-object v3, p2, Lcom/facebook/events/model/Event;->x:Ljava/lang/String;

    move-object v3, v3

    .line 2546753
    iput-object v3, v2, LX/89I;->d:Ljava/lang/String;

    .line 2546754
    move-object v2, v2

    .line 2546755
    iget-object v3, p2, Lcom/facebook/events/model/Event;->ap:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-object v3, v3

    .line 2546756
    invoke-virtual {v2, v3}, LX/89I;->a(LX/2rX;)LX/89I;

    move-result-object v2

    invoke-virtual {v2}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    iget-object v0, p0, LX/IBH;->d:LX/01T;

    sget-object v3, LX/01T;->PAA:LX/01T;

    if-ne v0, v3, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setDisableFriendTagging(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    iget-object v0, p0, LX/IBH;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v3, LX/1EB;->an:S

    invoke-interface {v0, v3, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-virtual {v2, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setUsePublishExperiment(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    .line 2546757
    invoke-static {p2, v0, p1}, LX/IBH;->a(Lcom/facebook/events/model/Event;Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;Lcom/facebook/auth/viewercontext/ViewerContext;)V

    .line 2546758
    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v1

    .line 2546759
    goto :goto_0
.end method

.method public final b(Landroid/content/Context;Lcom/facebook/events/model/Event;)V
    .locals 2

    .prologue
    .line 2546744
    iget-object v0, p0, LX/IBH;->c:LX/IBL;

    new-instance v1, LX/IBG;

    invoke-direct {v1, p0, p1, p2}, LX/IBG;-><init>(LX/IBH;Landroid/content/Context;Lcom/facebook/events/model/Event;)V

    invoke-virtual {v0, p2, v1}, LX/IBL;->a(Lcom/facebook/events/model/Event;LX/0QK;)V

    .line 2546745
    return-void
.end method
