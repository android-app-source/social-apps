.class public LX/IrQ;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/IrP;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2618005
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 2618006
    return-void
.end method


# virtual methods
.method public final a(LX/IrD;LX/Is9;Landroid/view/ViewGroup;Lcom/facebook/messaging/photos/editing/LayerGroupLayout;Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;Lcom/facebook/messaging/photos/editing/TextStylesLayout;Landroid/view/View;LX/4ob;)LX/IrP;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/IrD;",
            "Lcom/facebook/messaging/photos/editing/StickerPickerFactory;",
            "Landroid/view/ViewGroup;",
            "Lcom/facebook/messaging/photos/editing/LayerGroupLayout;",
            "Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;",
            "Lcom/facebook/messaging/photos/editing/TextStylesLayout;",
            "Landroid/view/View;",
            "LX/4ob",
            "<",
            "LX/IqZ;",
            ">;)",
            "LX/IrP;"
        }
    .end annotation

    .prologue
    .line 2618007
    new-instance v1, LX/IrP;

    invoke-static/range {p0 .. p0}, LX/IqE;->a(LX/0QB;)LX/IqE;

    move-result-object v10

    check-cast v10, LX/IqE;

    invoke-static/range {p0 .. p0}, LX/IqN;->a(LX/0QB;)LX/IqN;

    move-result-object v11

    check-cast v11, LX/IqN;

    invoke-static/range {p0 .. p0}, LX/1zC;->a(LX/0QB;)LX/1zC;

    move-result-object v12

    check-cast v12, LX/1zC;

    invoke-static/range {p0 .. p0}, LX/1Ad;->a(LX/0QB;)LX/1Ad;

    move-result-object v13

    check-cast v13, LX/1Ad;

    invoke-static/range {p0 .. p0}, LX/2My;->a(LX/0QB;)LX/2My;

    move-result-object v14

    check-cast v14, LX/2My;

    invoke-static/range {p0 .. p0}, LX/0wW;->a(LX/0QB;)LX/0wW;

    move-result-object v15

    check-cast v15, LX/0wW;

    invoke-static/range {p0 .. p0}, LX/IqH;->a(LX/0QB;)LX/IqH;

    move-result-object v16

    check-cast v16, LX/IqH;

    const-class v2, LX/Irs;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v17

    check-cast v17, LX/Irs;

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v1 .. v17}, LX/IrP;-><init>(LX/IrD;LX/Is9;Landroid/view/ViewGroup;Lcom/facebook/messaging/photos/editing/LayerGroupLayout;Lcom/facebook/messaging/photos/editing/PhotoEditingControlsLayout;Lcom/facebook/messaging/photos/editing/TextStylesLayout;Landroid/view/View;LX/4ob;LX/IqE;LX/IqN;LX/1zC;LX/1Ad;LX/2My;LX/0wW;LX/IqH;LX/Irs;)V

    .line 2618008
    const-class v2, LX/Irs;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/Irs;

    invoke-static {v1, v2}, LX/IrP;->a(LX/IrP;LX/Irs;)V

    .line 2618009
    return-object v1
.end method
