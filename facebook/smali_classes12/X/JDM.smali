.class public final LX/JDM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;)V
    .locals 0

    .prologue
    .line 2663731
    iput-object p1, p0, LX/JDM;->a:Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x58390dad

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2663732
    iget-object v1, p0, LX/JDM;->a:Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;

    iget-object v1, v1, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->j:LX/JDK;

    iget-object v1, v1, LX/JDK;->g:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, LX/JDM;->a:Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;

    iget-object v1, v1, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->j:LX/JDK;

    iget-object v1, v1, LX/JDK;->d:LX/1Fb;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/JDM;->a:Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;

    iget-object v1, v1, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->j:LX/JDK;

    iget-object v1, v1, LX/JDK;->b:LX/175;

    if-eqz v1, :cond_0

    .line 2663733
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    iget-object v2, p0, LX/JDM;->a:Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;

    iget-object v2, v2, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->j:LX/JDK;

    iget-object v2, v2, LX/JDK;->d:LX/1Fb;

    invoke-interface {v2}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/JDM;->a:Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;

    iget-object v3, v3, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->j:LX/JDK;

    iget-object v3, v3, LX/JDK;->b:LX/175;

    invoke-interface {v3}, LX/175;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v5, v2, v3}, LX/5ve;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2663734
    :cond_0
    iget-object v1, p0, LX/JDM;->a:Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;

    iget-object v1, v1, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->f:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iget-object v2, p0, LX/JDM;->a:Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;

    invoke-virtual {v2}, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/JDM;->a:Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;

    iget-object v3, v3, Lcom/facebook/timeline/aboutpage/views/ListCollectionItemView;->w:Ljava/lang/String;

    invoke-interface {v1, v2, v3, v5, v5}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;)Z

    .line 2663735
    const v1, -0x19c0d5df

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
