.class public LX/Hj0;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/Hj0;

.field public static final b:LX/Hj0;

.field public static final c:LX/Hj0;

.field public static final d:LX/Hj0;

.field public static final e:LX/Hj0;

.field public static final f:LX/Hj0;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# instance fields
.field public final g:I

.field public final h:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    new-instance v0, LX/Hj0;

    const/16 v1, 0x3e8

    const-string v2, "Network Error"

    invoke-direct {v0, v1, v2}, LX/Hj0;-><init>(ILjava/lang/String;)V

    sput-object v0, LX/Hj0;->a:LX/Hj0;

    new-instance v0, LX/Hj0;

    const/16 v1, 0x3e9

    const-string v2, "No Fill"

    invoke-direct {v0, v1, v2}, LX/Hj0;-><init>(ILjava/lang/String;)V

    sput-object v0, LX/Hj0;->b:LX/Hj0;

    new-instance v0, LX/Hj0;

    const/16 v1, 0x3ea

    const-string v2, "Ad was re-loaded too frequently"

    invoke-direct {v0, v1, v2}, LX/Hj0;-><init>(ILjava/lang/String;)V

    sput-object v0, LX/Hj0;->c:LX/Hj0;

    new-instance v0, LX/Hj0;

    const/16 v1, 0x7d0

    const-string v2, "Server Error"

    invoke-direct {v0, v1, v2}, LX/Hj0;-><init>(ILjava/lang/String;)V

    sput-object v0, LX/Hj0;->d:LX/Hj0;

    new-instance v0, LX/Hj0;

    const/16 v1, 0x7d1

    const-string v2, "Internal Error"

    invoke-direct {v0, v1, v2}, LX/Hj0;-><init>(ILjava/lang/String;)V

    sput-object v0, LX/Hj0;->e:LX/Hj0;

    new-instance v0, LX/Hj0;

    const/16 v1, 0x7d2

    const-string v2, "Native ad failed to load due to missing properties"

    invoke-direct {v0, v1, v2}, LX/Hj0;-><init>(ILjava/lang/String;)V

    sput-object v0, LX/Hj0;->f:LX/Hj0;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, LX/Hky;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p2, "unknown error"

    :cond_0
    iput p1, p0, LX/Hj0;->g:I

    iput-object p2, p0, LX/Hj0;->h:Ljava/lang/String;

    return-void
.end method
