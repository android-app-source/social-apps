.class public LX/Itt;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/2Hu;

.field private final c:LX/2Ow;

.field private final d:LX/0So;

.field private final e:LX/0SG;

.field private final f:LX/0SG;

.field private final g:LX/0kb;

.field private final h:LX/3QL;

.field public final i:LX/FCR;

.field private final j:LX/Ite;

.field private final k:LX/Itc;

.field private final l:LX/1sj;

.field private final m:LX/1tM;

.field private final n:LX/3Ed;

.field private final o:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2625114
    const-class v0, LX/Itt;

    sput-object v0, LX/Itt;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/2Hu;LX/2Ow;LX/0So;LX/0SG;LX/0SG;LX/0kb;LX/3QL;LX/FCR;LX/Ite;LX/Itc;LX/1sj;LX/1tM;LX/3Ed;LX/03V;)V
    .locals 0
    .param p4    # LX/0SG;
        .annotation runtime Lcom/facebook/messaging/database/threads/NeedsDbClock;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2625098
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2625099
    iput-object p1, p0, LX/Itt;->b:LX/2Hu;

    .line 2625100
    iput-object p2, p0, LX/Itt;->c:LX/2Ow;

    .line 2625101
    iput-object p3, p0, LX/Itt;->d:LX/0So;

    .line 2625102
    iput-object p4, p0, LX/Itt;->e:LX/0SG;

    .line 2625103
    iput-object p5, p0, LX/Itt;->f:LX/0SG;

    .line 2625104
    iput-object p6, p0, LX/Itt;->g:LX/0kb;

    .line 2625105
    iput-object p7, p0, LX/Itt;->h:LX/3QL;

    .line 2625106
    iput-object p8, p0, LX/Itt;->i:LX/FCR;

    .line 2625107
    iput-object p9, p0, LX/Itt;->j:LX/Ite;

    .line 2625108
    iput-object p10, p0, LX/Itt;->k:LX/Itc;

    .line 2625109
    iput-object p11, p0, LX/Itt;->l:LX/1sj;

    .line 2625110
    iput-object p12, p0, LX/Itt;->m:LX/1tM;

    .line 2625111
    iput-object p13, p0, LX/Itt;->n:LX/3Ed;

    .line 2625112
    iput-object p14, p0, LX/Itt;->o:LX/03V;

    .line 2625113
    return-void
.end method

.method public static a(LX/0QB;)LX/Itt;
    .locals 1

    .prologue
    .line 2624941
    invoke-static {p0}, LX/Itt;->b(LX/0QB;)LX/Itt;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/Itt;Lcom/facebook/messaging/service/model/SendMessageParams;LX/FCM;ILX/ItY;J)LX/Itx;
    .locals 9

    .prologue
    .line 2625081
    :try_start_0
    iget-object v0, p0, LX/Itt;->b:LX/2Hu;

    invoke-virtual {v0}, LX/2Hu;->a()LX/2gV;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, p3

    move-object v5, p4

    move-wide v6, p5

    .line 2625082
    :try_start_1
    invoke-static/range {v0 .. v7}, LX/Itt;->a(LX/Itt;Lcom/facebook/messaging/service/model/SendMessageParams;LX/FCM;LX/2gV;ILX/ItY;J)LX/Itx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 2625083
    :try_start_2
    invoke-virtual {v3}, LX/2gV;->f()V

    .line 2625084
    :goto_0
    return-object v0

    .line 2625085
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, LX/2gV;->f()V

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 2625086
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2625087
    invoke-static {v1}, LX/1Bz;->getCausalChain(Ljava/lang/Throwable;)Ljava/util/List;

    move-result-object v0

    .line 2625088
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    .line 2625089
    instance-of v3, v0, Ljava/lang/Exception;

    if-nez v3, :cond_1

    .line 2625090
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 2625091
    :cond_1
    check-cast v0, Ljava/lang/Exception;

    .line 2625092
    instance-of v3, v0, Landroid/os/RemoteException;

    if-eqz v3, :cond_2

    .line 2625093
    sget-object v0, LX/Itv;->MQTT_REMOTEEXCEPTION:LX/Itv;

    iget v0, v0, LX/Itv;->errorCode:I

    invoke-static {v1, v0}, LX/Itx;->a(Ljava/lang/Exception;I)LX/Itx;

    move-result-object v0

    goto :goto_0

    .line 2625094
    :cond_2
    instance-of v0, v0, Ljava/io/IOException;

    if-eqz v0, :cond_0

    .line 2625095
    sget-object v0, LX/Itv;->MQTT_IOEXCEPTION:LX/Itv;

    iget v0, v0, LX/Itv;->errorCode:I

    invoke-static {v1, v0}, LX/Itx;->a(Ljava/lang/Exception;I)LX/Itx;

    move-result-object v0

    goto :goto_0

    .line 2625096
    :cond_3
    sget-object v0, LX/Itt;->a:Ljava/lang/Class;

    const-string v2, "Exception while sending message over mqtt"

    invoke-static {v0, v2, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2625097
    sget-object v0, LX/Itv;->MQTT_EXCEPTION:LX/Itv;

    iget v0, v0, LX/Itv;->errorCode:I

    invoke-static {v1, v0}, LX/Itx;->a(Ljava/lang/Exception;I)LX/Itx;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(LX/Itt;Lcom/facebook/messaging/service/model/SendMessageParams;LX/FCM;LX/2gV;ILX/ItY;J)LX/Itx;
    .locals 20

    .prologue
    .line 2625028
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v4, v4, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 2625029
    move-object/from16 v0, p0

    iget-object v5, v0, LX/Itt;->k:LX/Itc;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/Itt;->o:LX/03V;

    move-object/from16 v8, p5

    move-object/from16 v9, p3

    invoke-virtual/range {v5 .. v10}, LX/Itc;->a(JLX/ItY;LX/2gV;LX/03V;)LX/76J;

    move-result-object v13

    .line 2625030
    invoke-virtual {v13}, LX/76J;->c()V

    .line 2625031
    :try_start_0
    move-object/from16 v0, p2

    iget-wide v7, v0, LX/FCM;->a:J

    .line 2625032
    move-object/from16 v0, p2

    iget-wide v14, v0, LX/FCM;->b:J

    .line 2625033
    move-object/from16 v0, p0

    iget-object v4, v0, LX/Itt;->d:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v16

    .line 2625034
    move-object/from16 v0, p0

    iget-object v4, v0, LX/Itt;->m:LX/1tM;

    invoke-virtual {v4}, LX/1tM;->get()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2625035
    :try_start_1
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/facebook/messaging/service/model/SendMessageParams;->c:Lcom/facebook/fbtrace/FbTraceNode;

    invoke-static {v4}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;)Lcom/facebook/fbtrace/FbTraceNode;

    move-result-object v18

    .line 2625036
    move-object/from16 v0, p0

    iget-object v4, v0, LX/Itt;->j:LX/Ite;

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move-object/from16 v2, p5

    invoke-virtual {v4, v0, v1, v2, v12}, LX/Ite;->a(Lcom/facebook/messaging/service/model/SendMessageParams;Lcom/facebook/fbtrace/FbTraceNode;LX/ItY;Ljava/lang/Integer;)[B

    move-result-object v6

    .line 2625037
    invoke-static/range {v18 .. v18}, LX/2b8;->a(Lcom/facebook/fbtrace/FbTraceNode;)LX/2gQ;

    move-result-object v4

    .line 2625038
    const-string v5, "op"

    const-string v9, "mqtt_send_attempt"

    invoke-interface {v4, v5, v9}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2625039
    const-string v5, "attempt_number"

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v4, v5, v9}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2625040
    move-object/from16 v0, p0

    iget-object v5, v0, LX/Itt;->l:LX/1sj;

    sget-object v9, LX/2gR;->REQUEST_SEND:LX/2gR;

    move-object/from16 v0, v18

    invoke-virtual {v5, v0, v9, v4}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;LX/2gR;LX/2gQ;)V
    :try_end_1
    .catch LX/Itq; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2625041
    :try_start_2
    new-instance v9, LX/Itr;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v9, v0, v1}, LX/Itr;-><init>(LX/Itt;Lcom/facebook/messaging/service/model/SendMessageParams;)V

    .line 2625042
    invoke-virtual/range {p5 .. p5}, LX/ItY;->getRequestTopic()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v4, p3

    move-wide/from16 v10, p6

    invoke-virtual/range {v4 .. v12}, LX/2gV;->a(Ljava/lang/String;[BJLX/76H;JLjava/lang/Integer;)Z

    move-result v4

    .line 2625043
    if-eqz v4, :cond_0

    .line 2625044
    invoke-virtual {v13}, LX/76J;->f()V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catch LX/Itq; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2625045
    :try_start_3
    move-object/from16 v0, p0

    iget-object v4, v0, LX/Itt;->l:LX/1sj;

    sget-object v5, LX/2gR;->RESPONSE_RECEIVE:LX/2gR;

    const/4 v6, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v4, v0, v5, v6}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;LX/2gR;LX/2gQ;)V
    :try_end_3
    .catch LX/Itq; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2625046
    :try_start_4
    move-object/from16 v0, p0

    iget-object v4, v0, LX/Itt;->c:LX/2Ow;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v5, v5, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v6, v6, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, LX/2Ow;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)V

    .line 2625047
    move-object/from16 v0, p0

    iget-object v4, v0, LX/Itt;->h:LX/3QL;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/Itt;->d:LX/0So;

    invoke-interface {v5}, LX/0So;->now()J

    move-result-wide v10

    sub-long v10, v10, v16

    move-object/from16 v0, p1

    invoke-virtual {v4, v0, v10, v11}, LX/3QL;->a(Lcom/facebook/messaging/service/model/SendMessageParams;J)V

    .line 2625048
    move-object/from16 v0, p0

    iget-object v4, v0, LX/Itt;->i:LX/FCR;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v5, v5, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v4, v5}, LX/FCR;->a(Ljava/lang/String;)V

    .line 2625049
    move-object/from16 v0, p0

    iget-object v4, v0, LX/Itt;->d:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v4

    sub-long v4, v4, v16

    .line 2625050
    add-long v10, v7, v14

    sub-long v4, v10, v4

    .line 2625051
    invoke-virtual {v13}, LX/76J;->e()Ljava/lang/Object;

    move-result-object v6

    if-nez v6, :cond_1

    const-wide/16 v10, 0x0

    cmp-long v6, v4, v10

    if-gez v6, :cond_1

    .line 2625052
    sget-object v4, LX/Itv;->SEND_FAILED_TIMED_OUT_AFTER_PUBLISH:LX/Itv;

    sget-object v5, LX/Itv;->SEND_FAILED_TIMED_OUT_AFTER_PUBLISH:LX/Itv;

    iget v5, v5, LX/Itv;->errorCode:I

    const/4 v6, 0x1

    invoke-static {v4, v5, v6}, LX/Itx;->a(LX/Itv;IZ)LX/Itx;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v4

    .line 2625053
    invoke-virtual {v13}, LX/76J;->d()V

    .line 2625054
    :goto_0
    return-object v4

    .line 2625055
    :cond_0
    :try_start_5
    sget-object v4, LX/Itv;->SEND_FAILED_PUBLISH_FAILED:LX/Itv;

    sget-object v5, LX/Itv;->SEND_FAILED_PUBLISH_FAILED:LX/Itv;

    iget v5, v5, LX/Itv;->errorCode:I

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v4, v5, v6, v7}, LX/Itx;->a(LX/Itv;ILjava/lang/String;Z)LX/Itx;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_0
    .catch LX/Itq; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v4

    .line 2625056
    invoke-virtual {v13}, LX/76J;->d()V

    goto :goto_0

    .line 2625057
    :catch_0
    move-exception v4

    .line 2625058
    :try_start_6
    sget-object v5, LX/Itv;->SEND_FAILED_PUBLISH_FAILED_WITH_EXCEPTION:LX/Itv;

    sget-object v6, LX/Itv;->SEND_FAILED_PUBLISH_FAILED_WITH_EXCEPTION:LX/Itv;

    iget v6, v6, LX/Itv;->errorCode:I

    invoke-virtual {v4}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v4

    const/4 v7, 0x0

    invoke-static {v5, v6, v4, v7}, LX/Itx;->a(LX/Itv;ILjava/lang/String;Z)LX/Itx;
    :try_end_6
    .catch LX/Itq; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v4

    .line 2625059
    invoke-virtual {v13}, LX/76J;->d()V

    goto :goto_0

    .line 2625060
    :catch_1
    move-exception v4

    .line 2625061
    :try_start_7
    iget-object v4, v4, LX/Itq;->mMqttResult:LX/Itx;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 2625062
    invoke-virtual {v13}, LX/76J;->d()V

    goto :goto_0

    .line 2625063
    :cond_1
    :try_start_8
    invoke-virtual {v13, v4, v5}, LX/76J;->a(J)Z

    move-result v4

    if-nez v4, :cond_3

    .line 2625064
    move-object/from16 v0, p0

    iget-object v4, v0, LX/Itt;->d:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v4

    sub-long v4, v4, v16

    .line 2625065
    add-long v6, v7, v14

    sub-long v4, v6, v4

    .line 2625066
    const-wide/16 v6, 0x0

    cmp-long v6, v4, v6

    if-lez v6, :cond_2

    .line 2625067
    move-object/from16 v0, p0

    iget-object v6, v0, LX/Itt;->g:LX/0kb;

    invoke-virtual {v6, v4, v5}, LX/0kb;->a(J)V

    .line 2625068
    :cond_2
    sget-object v4, LX/Itv;->SEND_FAILED_TIMED_OUT_WAITING_FOR_RESPONSE:LX/Itv;

    sget-object v5, LX/Itv;->SEND_FAILED_TIMED_OUT_WAITING_FOR_RESPONSE:LX/Itv;

    iget v5, v5, LX/Itv;->errorCode:I

    const/4 v6, 0x1

    invoke-static {v4, v5, v6}, LX/Itx;->a(LX/Itv;IZ)LX/Itx;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    move-result-object v4

    .line 2625069
    invoke-virtual {v13}, LX/76J;->d()V

    goto :goto_0

    .line 2625070
    :cond_3
    :try_start_9
    invoke-virtual {v13}, LX/76J;->e()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/765;

    iget-boolean v4, v4, LX/765;->a:Z

    if-nez v4, :cond_5

    .line 2625071
    invoke-virtual {v13}, LX/76J;->e()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/765;

    iget-boolean v4, v4, LX/765;->c:Z

    if-eqz v4, :cond_4

    .line 2625072
    sget-object v5, LX/Itv;->SEND_FAILED_SERVER_RETURNED_FAILURE:LX/Itv;

    invoke-virtual {v13}, LX/76J;->e()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/765;

    iget v4, v4, LX/765;->b:I

    const/4 v6, 0x1

    invoke-static {v5, v4, v6}, LX/Itx;->a(LX/Itv;IZ)LX/Itx;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move-result-object v4

    .line 2625073
    invoke-virtual {v13}, LX/76J;->d()V

    goto/16 :goto_0

    .line 2625074
    :cond_4
    :try_start_a
    invoke-virtual {v13}, LX/76J;->e()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/765;

    iget v5, v4, LX/765;->b:I

    const/4 v6, 0x1

    invoke-virtual {v13}, LX/76J;->e()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/765;

    iget-object v4, v4, LX/765;->d:Ljava/lang/String;

    invoke-static {v5, v6, v4}, LX/Itx;->a(IZLjava/lang/String;)LX/Itx;
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    move-result-object v4

    .line 2625075
    invoke-virtual {v13}, LX/76J;->d()V

    goto/16 :goto_0

    :cond_5
    invoke-virtual {v13}, LX/76J;->d()V

    .line 2625076
    move-object/from16 v0, p0

    iget-object v4, v0, LX/Itt;->e:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    .line 2625077
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v6

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    invoke-virtual {v6, v7}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6f7;

    move-result-object v6

    sget-object v7, LX/2uW;->REGULAR:LX/2uW;

    invoke-virtual {v6, v7}, LX/6f7;->a(LX/2uW;)LX/6f7;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, LX/6f7;->a(J)LX/6f7;

    move-result-object v6

    sget-object v7, LX/6f3;->MQTT:LX/6f3;

    invoke-virtual {v6, v7}, LX/6f7;->a(LX/6f3;)LX/6f7;

    move-result-object v6

    invoke-static {v4, v5}, LX/6fa;->a(J)J

    move-result-wide v4

    invoke-virtual {v6, v4, v5}, LX/6f7;->c(J)LX/6f7;

    move-result-object v4

    invoke-virtual {v4}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v7

    .line 2625078
    new-instance v5, Lcom/facebook/messaging/service/model/NewMessageResult;

    sget-object v6, LX/0ta;->FROM_SERVER:LX/0ta;

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, LX/Itt;->f:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v10

    invoke-direct/range {v5 .. v11}, Lcom/facebook/messaging/service/model/NewMessageResult;-><init>(LX/0ta;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2625079
    const/4 v4, 0x1

    invoke-static {v5, v4}, LX/Itx;->a(Lcom/facebook/messaging/service/model/NewMessageResult;Z)LX/Itx;

    move-result-object v4

    goto/16 :goto_0

    .line 2625080
    :catchall_0
    move-exception v4

    invoke-virtual {v13}, LX/76J;->d()V

    throw v4
.end method

.method private static a(LX/Itt;Ljava/util/List;LX/FCM;ILX/ItY;J)LX/Ity;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/service/model/SendMessageParams;",
            ">;",
            "LX/FCM;",
            "I",
            "LX/ItY;",
            "J)",
            "LX/Ity;"
        }
    .end annotation

    .prologue
    .line 2625011
    :try_start_0
    iget-object v0, p0, LX/Itt;->b:LX/2Hu;

    invoke-virtual {v0}, LX/2Hu;->a()LX/2gV;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, p3

    move-object v5, p4

    move-wide v6, p5

    .line 2625012
    :try_start_1
    invoke-static/range {v0 .. v7}, LX/Itt;->a(LX/Itt;Ljava/util/List;LX/FCM;LX/2gV;ILX/ItY;J)LX/Ity;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 2625013
    :try_start_2
    invoke-virtual {v3}, LX/2gV;->f()V

    .line 2625014
    :goto_0
    return-object v0

    .line 2625015
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, LX/2gV;->f()V

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 2625016
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2625017
    invoke-static {v1}, LX/1Bz;->getCausalChain(Ljava/lang/Throwable;)Ljava/util/List;

    move-result-object v0

    .line 2625018
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    .line 2625019
    instance-of v3, v0, Ljava/lang/Exception;

    if-nez v3, :cond_1

    .line 2625020
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 2625021
    :cond_1
    check-cast v0, Ljava/lang/Exception;

    .line 2625022
    instance-of v3, v0, Landroid/os/RemoteException;

    if-eqz v3, :cond_2

    .line 2625023
    new-instance v0, LX/Ity;

    invoke-direct {v0}, LX/Ity;-><init>()V

    sget-object v2, LX/Itv;->MQTT_REMOTEEXCEPTION:LX/Itv;

    iget v2, v2, LX/Itv;->errorCode:I

    invoke-static {v1, v2}, LX/Itx;->a(Ljava/lang/Exception;I)LX/Itx;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Ity;->a(LX/Itx;)LX/Ity;

    move-result-object v0

    goto :goto_0

    .line 2625024
    :cond_2
    instance-of v0, v0, Ljava/io/IOException;

    if-eqz v0, :cond_0

    .line 2625025
    new-instance v0, LX/Ity;

    invoke-direct {v0}, LX/Ity;-><init>()V

    sget-object v2, LX/Itv;->MQTT_IOEXCEPTION:LX/Itv;

    iget v2, v2, LX/Itv;->errorCode:I

    invoke-static {v1, v2}, LX/Itx;->a(Ljava/lang/Exception;I)LX/Itx;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Ity;->a(LX/Itx;)LX/Ity;

    move-result-object v0

    goto :goto_0

    .line 2625026
    :cond_3
    sget-object v0, LX/Itt;->a:Ljava/lang/Class;

    const-string v2, "Exception while sending message over mqtt"

    invoke-static {v0, v2, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2625027
    new-instance v0, LX/Ity;

    invoke-direct {v0}, LX/Ity;-><init>()V

    sget-object v2, LX/Itv;->MQTT_EXCEPTION:LX/Itv;

    iget v2, v2, LX/Itv;->errorCode:I

    invoke-static {v1, v2}, LX/Itx;->a(Ljava/lang/Exception;I)LX/Itx;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Ity;->a(LX/Itx;)LX/Ity;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(LX/Itt;Ljava/util/List;LX/FCM;LX/2gV;ILX/ItY;J)LX/Ity;
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/service/model/SendMessageParams;",
            ">;",
            "LX/FCM;",
            "Lcom/facebook/push/mqtt/service/MqttPushServiceClient;",
            "I",
            "LX/ItY;",
            "J)",
            "LX/Ity;"
        }
    .end annotation

    .prologue
    .line 2624952
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Itt;->n:LX/3Ed;

    invoke-virtual {v2}, LX/3Ed;->a()J

    move-result-wide v8

    .line 2624953
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Itt;->l:LX/1sj;

    invoke-virtual {v2}, LX/1sj;->a()Lcom/facebook/fbtrace/FbTraceNode;

    move-result-object v5

    .line 2624954
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Itt;->m:LX/1tM;

    invoke-virtual {v2}, LX/1tM;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    .line 2624955
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Itt;->k:LX/Itc;

    move-object/from16 v0, p5

    invoke-virtual {v2, v8, v9, v0}, LX/Itc;->a(JLX/ItY;)LX/76J;

    move-result-object v17

    .line 2624956
    invoke-virtual/range {v17 .. v17}, LX/76J;->c()V

    .line 2624957
    :try_start_0
    move-object/from16 v0, p2

    iget-wide v11, v0, LX/FCM;->a:J

    .line 2624958
    move-object/from16 v0, p2

    iget-wide v0, v0, LX/FCM;->b:J

    move-wide/from16 v18, v0

    .line 2624959
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Itt;->d:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v20

    .line 2624960
    :try_start_1
    move-object/from16 v0, p0

    iget-object v3, v0, LX/Itt;->j:LX/Ite;

    move-object/from16 v4, p1

    move-object/from16 v6, p5

    invoke-virtual/range {v3 .. v9}, LX/Ite;->a(Ljava/util/List;Lcom/facebook/fbtrace/FbTraceNode;LX/ItY;Ljava/lang/Integer;J)[B

    move-result-object v10

    .line 2624961
    invoke-static {v5}, LX/2b8;->a(Lcom/facebook/fbtrace/FbTraceNode;)LX/2gQ;

    move-result-object v2

    .line 2624962
    const-string v3, "op"

    const-string v4, "mqtt_send_attempt"

    invoke-interface {v2, v3, v4}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2624963
    const-string v3, "attempt_number"

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v3, v4}, LX/2gQ;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2624964
    move-object/from16 v0, p0

    iget-object v3, v0, LX/Itt;->l:LX/1sj;

    sget-object v4, LX/2gR;->REQUEST_SEND:LX/2gR;

    invoke-virtual {v3, v5, v4, v2}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;LX/2gR;LX/2gQ;)V
    :try_end_1
    .catch LX/Itq; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2624965
    :try_start_2
    new-instance v13, LX/Its;

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v8, v9}, LX/Its;-><init>(LX/Itt;J)V

    .line 2624966
    invoke-virtual/range {p5 .. p5}, LX/ItY;->getRequestTopic()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v8, p3

    move-wide/from16 v14, p6

    move-object/from16 v16, v7

    invoke-virtual/range {v8 .. v16}, LX/2gV;->a(Ljava/lang/String;[BJLX/76H;JLjava/lang/Integer;)Z

    move-result v2

    .line 2624967
    if-nez v2, :cond_0

    .line 2624968
    new-instance v2, LX/Ity;

    invoke-direct {v2}, LX/Ity;-><init>()V

    sget-object v3, LX/Itv;->SEND_FAILED_PUBLISH_FAILED:LX/Itv;

    sget-object v4, LX/Itv;->SEND_FAILED_PUBLISH_FAILED:LX/Itv;

    iget v4, v4, LX/Itv;->errorCode:I

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, LX/Itx;->a(LX/Itv;IZ)LX/Itx;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/Ity;->a(LX/Itx;)LX/Ity;
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catch LX/Itq; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    .line 2624969
    invoke-virtual/range {v17 .. v17}, LX/76J;->d()V

    .line 2624970
    :goto_0
    return-object v2

    .line 2624971
    :catch_0
    :try_start_3
    new-instance v2, LX/Ity;

    invoke-direct {v2}, LX/Ity;-><init>()V

    sget-object v3, LX/Itv;->SEND_FAILED_PUBLISH_FAILED_WITH_EXCEPTION:LX/Itv;

    sget-object v4, LX/Itv;->SEND_FAILED_PUBLISH_FAILED_WITH_EXCEPTION:LX/Itv;

    iget v4, v4, LX/Itv;->errorCode:I

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, LX/Itx;->a(LX/Itv;IZ)LX/Itx;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/Ity;->a(LX/Itx;)LX/Ity;
    :try_end_3
    .catch LX/Itq; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v2

    .line 2624972
    invoke-virtual/range {v17 .. v17}, LX/76J;->d()V

    goto :goto_0

    .line 2624973
    :cond_0
    :try_start_4
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Itt;->l:LX/1sj;

    sget-object v3, LX/2gR;->RESPONSE_RECEIVE:LX/2gR;

    const/4 v4, 0x0

    invoke-virtual {v2, v5, v3, v4}, LX/1sj;->a(Lcom/facebook/fbtrace/FbTraceNode;LX/2gR;LX/2gQ;)V
    :try_end_4
    .catch LX/Itq; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2624974
    :try_start_5
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/service/model/SendMessageParams;

    .line 2624975
    move-object/from16 v0, p0

    iget-object v4, v0, LX/Itt;->c:LX/2Ow;

    iget-object v5, v2, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v5, v5, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v6, v2, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v6, v6, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, LX/2Ow;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)V

    .line 2624976
    move-object/from16 v0, p0

    iget-object v4, v0, LX/Itt;->h:LX/3QL;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/Itt;->d:LX/0So;

    invoke-interface {v5}, LX/0So;->now()J

    move-result-wide v6

    sub-long v6, v6, v20

    invoke-virtual {v4, v2, v6, v7}, LX/3QL;->a(Lcom/facebook/messaging/service/model/SendMessageParams;J)V

    .line 2624977
    move-object/from16 v0, p0

    iget-object v4, v0, LX/Itt;->i:LX/FCR;

    iget-object v2, v2, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v2, v2, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-virtual {v4, v2}, LX/FCR;->a(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    .line 2624978
    :catchall_0
    move-exception v2

    invoke-virtual/range {v17 .. v17}, LX/76J;->d()V

    throw v2

    .line 2624979
    :catch_1
    move-exception v2

    .line 2624980
    :try_start_6
    new-instance v3, LX/Ity;

    invoke-direct {v3}, LX/Ity;-><init>()V

    iget-object v2, v2, LX/Itq;->mMqttResult:LX/Itx;

    invoke-virtual {v3, v2}, LX/Ity;->a(LX/Itx;)LX/Ity;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v2

    .line 2624981
    invoke-virtual/range {v17 .. v17}, LX/76J;->d()V

    goto :goto_0

    .line 2624982
    :cond_1
    :try_start_7
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Itt;->d:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    sub-long v2, v2, v20

    .line 2624983
    add-long v4, v11, v18

    sub-long v2, v4, v2

    .line 2624984
    invoke-virtual/range {v17 .. v17}, LX/76J;->e()Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_2

    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-gez v4, :cond_2

    .line 2624985
    new-instance v2, LX/Ity;

    invoke-direct {v2}, LX/Ity;-><init>()V

    sget-object v3, LX/Itv;->SEND_FAILED_TIMED_OUT_AFTER_PUBLISH:LX/Itv;

    sget-object v4, LX/Itv;->SEND_FAILED_TIMED_OUT_AFTER_PUBLISH:LX/Itv;

    iget v4, v4, LX/Itv;->errorCode:I

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, LX/Itx;->a(LX/Itv;IZ)LX/Itx;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/Ity;->a(LX/Itx;)LX/Ity;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-result-object v2

    .line 2624986
    invoke-virtual/range {v17 .. v17}, LX/76J;->d()V

    goto/16 :goto_0

    .line 2624987
    :cond_2
    :try_start_8
    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, LX/76J;->a(J)Z

    move-result v2

    if-nez v2, :cond_4

    .line 2624988
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Itt;->d:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    sub-long v2, v2, v20

    .line 2624989
    add-long v4, v11, v18

    sub-long v2, v4, v2

    .line 2624990
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-lez v4, :cond_3

    .line 2624991
    move-object/from16 v0, p0

    iget-object v4, v0, LX/Itt;->g:LX/0kb;

    invoke-virtual {v4, v2, v3}, LX/0kb;->a(J)V

    .line 2624992
    :cond_3
    new-instance v2, LX/Ity;

    invoke-direct {v2}, LX/Ity;-><init>()V

    sget-object v3, LX/Itv;->SEND_FAILED_TIMED_OUT_WAITING_FOR_RESPONSE:LX/Itv;

    sget-object v4, LX/Itv;->SEND_FAILED_TIMED_OUT_WAITING_FOR_RESPONSE:LX/Itv;

    iget v4, v4, LX/Itv;->errorCode:I

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, LX/Itx;->a(LX/Itv;IZ)LX/Itx;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/Ity;->a(LX/Itx;)LX/Ity;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    move-result-object v2

    .line 2624993
    invoke-virtual/range {v17 .. v17}, LX/76J;->d()V

    goto/16 :goto_0

    :cond_4
    invoke-virtual/range {v17 .. v17}, LX/76J;->d()V

    .line 2624994
    new-instance v11, LX/Ity;

    invoke-direct {v11}, LX/Ity;-><init>()V

    .line 2624995
    invoke-virtual/range {v17 .. v17}, LX/76J;->e()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 2624996
    const/4 v3, 0x0

    move v10, v3

    :goto_2
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-ge v10, v3, :cond_5

    .line 2624997
    invoke-interface {v2, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/765;

    .line 2624998
    move-object/from16 v0, p1

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/messaging/service/model/SendMessageParams;

    .line 2624999
    iget-wide v6, v3, LX/765;->f:J

    iget-object v5, v4, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v5, v5, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    cmp-long v5, v6, v8

    if-eqz v5, :cond_6

    .line 2625000
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Itt;->o:LX/03V;

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "responses in bathed response out of order"

    invoke-virtual {v2, v3, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    move-object v2, v11

    .line 2625001
    goto/16 :goto_0

    .line 2625002
    :cond_6
    iget-boolean v5, v3, LX/765;->a:Z

    if-eqz v5, :cond_7

    .line 2625003
    move-object/from16 v0, p0

    iget-object v3, v0, LX/Itt;->e:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v6

    .line 2625004
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v3

    iget-object v4, v4, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    invoke-virtual {v3, v4}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6f7;

    move-result-object v3

    sget-object v4, LX/2uW;->REGULAR:LX/2uW;

    invoke-virtual {v3, v4}, LX/6f7;->a(LX/2uW;)LX/6f7;

    move-result-object v3

    invoke-virtual {v3, v6, v7}, LX/6f7;->a(J)LX/6f7;

    move-result-object v3

    sget-object v4, LX/6f3;->MQTT:LX/6f3;

    invoke-virtual {v3, v4}, LX/6f7;->a(LX/6f3;)LX/6f7;

    move-result-object v3

    invoke-static {v6, v7}, LX/6fa;->a(J)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, LX/6f7;->c(J)LX/6f7;

    move-result-object v3

    invoke-virtual {v3}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v5

    .line 2625005
    new-instance v3, Lcom/facebook/messaging/service/model/NewMessageResult;

    sget-object v4, LX/0ta;->FROM_SERVER:LX/0ta;

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, LX/Itt;->f:LX/0SG;

    invoke-interface {v8}, LX/0SG;->a()J

    move-result-wide v8

    invoke-direct/range {v3 .. v9}, Lcom/facebook/messaging/service/model/NewMessageResult;-><init>(LX/0ta;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2625006
    const/4 v4, 0x1

    invoke-static {v3, v4}, LX/Itx;->a(Lcom/facebook/messaging/service/model/NewMessageResult;Z)LX/Itx;

    move-result-object v3

    invoke-virtual {v11, v3}, LX/Ity;->a(LX/Itx;)LX/Ity;

    .line 2625007
    :goto_3
    add-int/lit8 v3, v10, 0x1

    move v10, v3

    goto/16 :goto_2

    .line 2625008
    :cond_7
    iget-boolean v4, v3, LX/765;->c:Z

    if-eqz v4, :cond_8

    .line 2625009
    sget-object v4, LX/Itv;->SEND_FAILED_SERVER_RETURNED_FAILURE:LX/Itv;

    iget v3, v3, LX/765;->b:I

    const/4 v5, 0x1

    invoke-static {v4, v3, v5}, LX/Itx;->a(LX/Itv;IZ)LX/Itx;

    move-result-object v3

    invoke-virtual {v11, v3}, LX/Ity;->a(LX/Itx;)LX/Ity;

    goto :goto_3

    .line 2625010
    :cond_8
    iget v4, v3, LX/765;->b:I

    const/4 v5, 0x1

    iget-object v3, v3, LX/765;->d:Ljava/lang/String;

    invoke-static {v4, v5, v3}, LX/Itx;->a(IZLjava/lang/String;)LX/Itx;

    move-result-object v3

    invoke-virtual {v11, v3}, LX/Ity;->a(LX/Itx;)LX/Ity;

    goto :goto_3
.end method

.method private static b(LX/0QB;)LX/Itt;
    .locals 15

    .prologue
    .line 2624944
    new-instance v0, LX/Itt;

    invoke-static {p0}, LX/2Hu;->a(LX/0QB;)LX/2Hu;

    move-result-object v1

    check-cast v1, LX/2Hu;

    invoke-static {p0}, LX/2Ow;->a(LX/0QB;)LX/2Ow;

    move-result-object v2

    check-cast v2, LX/2Ow;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v3

    check-cast v3, LX/0So;

    invoke-static {p0}, LX/2N6;->a(LX/0QB;)LX/2N6;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-static {p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v6

    check-cast v6, LX/0kb;

    invoke-static {p0}, LX/3QL;->b(LX/0QB;)LX/3QL;

    move-result-object v7

    check-cast v7, LX/3QL;

    invoke-static {p0}, LX/FCR;->a(LX/0QB;)LX/FCR;

    move-result-object v8

    check-cast v8, LX/FCR;

    .line 2624945
    new-instance v13, LX/Ite;

    invoke-static {p0}, LX/2Mk;->a(LX/0QB;)LX/2Mk;

    move-result-object v9

    check-cast v9, LX/2Mk;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v10

    check-cast v10, LX/03V;

    invoke-static {p0}, LX/2Mv;->b(LX/0QB;)LX/2Mv;

    move-result-object v11

    check-cast v11, LX/2Mv;

    invoke-static {p0}, LX/1sj;->a(LX/0QB;)LX/1sj;

    move-result-object v12

    check-cast v12, LX/1sj;

    invoke-direct {v13, v9, v10, v11, v12}, LX/Ite;-><init>(LX/2Mk;LX/03V;LX/2Mv;LX/1sj;)V

    .line 2624946
    move-object v9, v13

    .line 2624947
    check-cast v9, LX/Ite;

    .line 2624948
    new-instance v13, LX/Itc;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v10

    check-cast v10, LX/03V;

    invoke-static {p0}, LX/2Hv;->a(LX/0QB;)LX/2Hv;

    move-result-object v11

    check-cast v11, LX/2Hv;

    invoke-static {p0}, LX/7GV;->a(LX/0QB;)LX/7GV;

    move-result-object v12

    check-cast v12, LX/7GV;

    invoke-direct {v13, v10, v11, v12}, LX/Itc;-><init>(LX/03V;LX/2Hv;LX/7GV;)V

    .line 2624949
    move-object v10, v13

    .line 2624950
    check-cast v10, LX/Itc;

    invoke-static {p0}, LX/1sj;->a(LX/0QB;)LX/1sj;

    move-result-object v11

    check-cast v11, LX/1sj;

    invoke-static {p0}, LX/1tM;->a(LX/0QB;)LX/1tM;

    move-result-object v12

    check-cast v12, LX/1tM;

    invoke-static {p0}, LX/3Ed;->a(LX/0QB;)LX/3Ed;

    move-result-object v13

    check-cast v13, LX/3Ed;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v14

    check-cast v14, LX/03V;

    invoke-direct/range {v0 .. v14}, LX/Itt;-><init>(LX/2Hu;LX/2Ow;LX/0So;LX/0SG;LX/0SG;LX/0kb;LX/3QL;LX/FCR;LX/Ite;LX/Itc;LX/1sj;LX/1tM;LX/3Ed;LX/03V;)V

    .line 2624951
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/service/model/SendMessageParams;LX/FCM;IJ)LX/Itx;
    .locals 8

    .prologue
    .line 2624943
    sget-object v5, LX/ItY;->THRIFT:LX/ItY;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-wide v6, p4

    invoke-static/range {v1 .. v7}, LX/Itt;->a(LX/Itt;Lcom/facebook/messaging/service/model/SendMessageParams;LX/FCM;ILX/ItY;J)LX/Itx;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/List;LX/FCM;IJ)LX/Ity;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/service/model/SendMessageParams;",
            ">;",
            "LX/FCM;",
            "IJ)",
            "LX/Ity;"
        }
    .end annotation

    .prologue
    .line 2624942
    sget-object v5, LX/ItY;->THRIFT_BATCH:LX/ItY;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-wide v6, p4

    invoke-static/range {v1 .. v7}, LX/Itt;->a(LX/Itt;Ljava/util/List;LX/FCM;ILX/ItY;J)LX/Ity;

    move-result-object v0

    return-object v0
.end method
