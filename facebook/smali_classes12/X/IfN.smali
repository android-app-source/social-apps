.class public final enum LX/IfN;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IfN;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IfN;

.field public static final enum ADDED:LX/IfN;

.field public static final enum HIDDEN:LX/IfN;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2599620
    new-instance v0, LX/IfN;

    const-string v1, "ADDED"

    invoke-direct {v0, v1, v2}, LX/IfN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IfN;->ADDED:LX/IfN;

    .line 2599621
    new-instance v0, LX/IfN;

    const-string v1, "HIDDEN"

    invoke-direct {v0, v1, v3}, LX/IfN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IfN;->HIDDEN:LX/IfN;

    .line 2599622
    const/4 v0, 0x2

    new-array v0, v0, [LX/IfN;

    sget-object v1, LX/IfN;->ADDED:LX/IfN;

    aput-object v1, v0, v2

    sget-object v1, LX/IfN;->HIDDEN:LX/IfN;

    aput-object v1, v0, v3

    sput-object v0, LX/IfN;->$VALUES:[LX/IfN;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2599623
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IfN;
    .locals 1

    .prologue
    .line 2599624
    const-class v0, LX/IfN;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IfN;

    return-object v0
.end method

.method public static values()[LX/IfN;
    .locals 1

    .prologue
    .line 2599625
    sget-object v0, LX/IfN;->$VALUES:[LX/IfN;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IfN;

    return-object v0
.end method
