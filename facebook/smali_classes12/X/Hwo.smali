.class public final LX/Hwo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2NM;


# direct methods
.method public constructor <init>(LX/2NM;)V
    .locals 0

    .prologue
    .line 2521433
    iput-object p1, p0, LX/Hwo;->a:LX/2NM;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2521421
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2521422
    check-cast p1, Ljava/util/List;

    .line 2521423
    if-nez p1, :cond_0

    .line 2521424
    :goto_0
    return-void

    .line 2521425
    :cond_0
    const/4 v0, 0x0

    .line 2521426
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2521427
    if-eqz v0, :cond_2

    .line 2521428
    add-int/lit8 v0, v1, 0x1

    :goto_2
    move v1, v0

    .line 2521429
    goto :goto_1

    .line 2521430
    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2521431
    const-string v2, "count"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2521432
    iget-object v1, p0, LX/Hwo;->a:LX/2NM;

    iget-object v1, v1, LX/2NM;->j:LX/10O;

    const-string v2, "login_openid_tokens_retrieved"

    invoke-virtual {v1, v2, v0}, LX/10O;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_2
.end method
