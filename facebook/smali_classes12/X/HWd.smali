.class public final LX/HWd;
.super LX/CXx;
.source ""


# instance fields
.field public final synthetic b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;Landroid/os/ParcelUuid;)V
    .locals 0

    .prologue
    .line 2476880
    iput-object p1, p0, LX/HWd;->b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    invoke-direct {p0, p2}, LX/CXx;-><init>(Landroid/os/ParcelUuid;)V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 8

    .prologue
    .line 2476881
    check-cast p1, LX/CXw;

    .line 2476882
    iget-object v0, p0, LX/HWd;->b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->C:Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;

    if-eqz v0, :cond_3

    .line 2476883
    iget-object v0, p0, LX/HWd;->b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->l:LX/HNE;

    if-nez v0, :cond_0

    .line 2476884
    iget-object v0, p0, LX/HWd;->b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    iget-object v1, p0, LX/HWd;->b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    iget-object v1, v1, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->h:LX/HNF;

    iget-object v2, p0, LX/HWd;->b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    iget-object v2, v2, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->o:Landroid/os/ParcelUuid;

    invoke-virtual {v1, v2}, LX/HNF;->a(Landroid/os/ParcelUuid;)LX/HNE;

    move-result-object v1

    .line 2476885
    iput-object v1, v0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->l:LX/HNE;

    .line 2476886
    :cond_0
    iget-object v0, p0, LX/HWd;->b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    iget-object v1, p1, LX/CXw;->b:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    iget-object v2, p1, LX/CXw;->c:Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;

    .line 2476887
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_SERVICES:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-ne v1, v3, :cond_1

    invoke-virtual {v2}, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;->c()Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel$ServicesCardModel;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;->c()Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel$ServicesCardModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel$ServicesCardModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/HN7;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 2476888
    :cond_1
    const/4 v3, 0x0

    .line 2476889
    :goto_0
    move-object v4, v3

    .line 2476890
    iget-object v0, p0, LX/HWd;->b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    iget-object v6, v0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->C:Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;

    iget-object v0, p0, LX/HWd;->b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->l:LX/HNE;

    iget-object v1, p0, LX/HWd;->b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p1, LX/CXw;->b:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    iget-object v3, p1, LX/CXw;->c:Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;

    const/4 v5, 0x1

    invoke-virtual/range {v0 .. v5}, LX/HNE;->a(Landroid/content/Context;Lcom/facebook/graphql/enums/GraphQLPageActionType;Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;LX/HN7;Z)LX/5fq;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;->a(LX/5fq;)V

    .line 2476891
    :cond_2
    :goto_1
    return-void

    .line 2476892
    :cond_3
    iget-object v0, p0, LX/HWd;->b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->B:Lcom/facebook/pages/common/deeplink/simplifiedheader/view/PagesSimplifiedHeader;

    if-eqz v0, :cond_2

    .line 2476893
    iget-object v0, p0, LX/HWd;->b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->m:LX/HBh;

    if-nez v0, :cond_4

    .line 2476894
    iget-object v0, p0, LX/HWd;->b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    iget-object v1, p0, LX/HWd;->b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    iget-object v1, v1, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->j:LX/HBi;

    iget-object v2, p0, LX/HWd;->b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    iget-object v2, v2, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->o:Landroid/os/ParcelUuid;

    .line 2476895
    new-instance v5, LX/HBh;

    const-class v3, LX/HNF;

    invoke-interface {v1, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/HNF;

    invoke-static {v1}, LX/HN9;->a(LX/0QB;)LX/HN9;

    move-result-object v4

    check-cast v4, LX/HN9;

    invoke-direct {v5, v3, v4, v2}, LX/HBh;-><init>(LX/HNF;LX/HN9;Landroid/os/ParcelUuid;)V

    .line 2476896
    move-object v1, v5

    .line 2476897
    iput-object v1, v0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->m:LX/HBh;

    .line 2476898
    :cond_4
    iget-object v0, p0, LX/HWd;->b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    iget-object v7, v0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->B:Lcom/facebook/pages/common/deeplink/simplifiedheader/view/PagesSimplifiedHeader;

    iget-object v0, p0, LX/HWd;->b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->m:LX/HBh;

    iget-object v1, p0, LX/HWd;->b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/HWd;->b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    iget-object v2, v2, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->t:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    iget-object v3, p0, LX/HWd;->b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    iget-object v3, v3, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->r:Ljava/lang/String;

    iget-object v4, p0, LX/HWd;->b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    iget-object v4, v4, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->s:Ljava/lang/String;

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    iget-object v5, p0, LX/HWd;->b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    iget-boolean v5, v5, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->v:Z

    iget-object v6, p1, LX/CXw;->c:Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;

    invoke-virtual/range {v0 .. v6}, LX/HBh;->a(Landroid/content/Context;Lcom/facebook/graphql/enums/GraphQLPageActionType;Ljava/lang/String;Landroid/net/Uri;ZLcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;)LX/5fp;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/facebook/pages/common/deeplink/simplifiedheader/view/PagesSimplifiedHeader;->a(LX/5fp;)V

    .line 2476899
    iget-object v0, p0, LX/HWd;->b:Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->B:Lcom/facebook/pages/common/deeplink/simplifiedheader/view/PagesSimplifiedHeader;

    invoke-virtual {v0}, Lcom/facebook/pages/common/deeplink/simplifiedheader/view/PagesSimplifiedHeader;->invalidate()V

    goto :goto_1

    .line 2476900
    :cond_5
    iget-object v3, v0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->n:LX/HN7;

    if-nez v3, :cond_6

    .line 2476901
    iget-object v3, v0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->i:LX/HN8;

    invoke-virtual {v2}, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;->c()Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel$ServicesCardModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel$ServicesCardModel;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/HN8;->a(Ljava/lang/String;)LX/HN7;

    move-result-object v3

    iput-object v3, v0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->n:LX/HN7;

    .line 2476902
    :cond_6
    iget-object v3, v0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->n:LX/HN7;

    goto/16 :goto_0
.end method
