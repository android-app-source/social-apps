.class public final enum LX/Hvi;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Hvi;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Hvi;

.field public static final enum ELIGIBLE:LX/Hvi;

.field public static final enum NEED_TO_CHECK:LX/Hvi;

.field public static final enum NOT_ELIGIBLE:LX/Hvi;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2519431
    new-instance v0, LX/Hvi;

    const-string v1, "ELIGIBLE"

    invoke-direct {v0, v1, v2}, LX/Hvi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hvi;->ELIGIBLE:LX/Hvi;

    .line 2519432
    new-instance v0, LX/Hvi;

    const-string v1, "NOT_ELIGIBLE"

    invoke-direct {v0, v1, v3}, LX/Hvi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hvi;->NOT_ELIGIBLE:LX/Hvi;

    .line 2519433
    new-instance v0, LX/Hvi;

    const-string v1, "NEED_TO_CHECK"

    invoke-direct {v0, v1, v4}, LX/Hvi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hvi;->NEED_TO_CHECK:LX/Hvi;

    .line 2519434
    const/4 v0, 0x3

    new-array v0, v0, [LX/Hvi;

    sget-object v1, LX/Hvi;->ELIGIBLE:LX/Hvi;

    aput-object v1, v0, v2

    sget-object v1, LX/Hvi;->NOT_ELIGIBLE:LX/Hvi;

    aput-object v1, v0, v3

    sget-object v1, LX/Hvi;->NEED_TO_CHECK:LX/Hvi;

    aput-object v1, v0, v4

    sput-object v0, LX/Hvi;->$VALUES:[LX/Hvi;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2519430
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Hvi;
    .locals 1

    .prologue
    .line 2519435
    const-class v0, LX/Hvi;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Hvi;

    return-object v0
.end method

.method public static values()[LX/Hvi;
    .locals 1

    .prologue
    .line 2519429
    sget-object v0, LX/Hvi;->$VALUES:[LX/Hvi;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Hvi;

    return-object v0
.end method
