.class public final LX/JPq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/JPv;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

.field public final synthetic c:Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;

.field public final synthetic d:Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;LX/JPv;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;)V
    .locals 0

    .prologue
    .line 2690500
    iput-object p1, p0, LX/JPq;->d:Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;

    iput-object p2, p0, LX/JPq;->a:LX/JPv;

    iput-object p3, p0, LX/JPq;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    iput-object p4, p0, LX/JPq;->c:Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v7, 0x1

    const/4 v0, 0x2

    const v1, 0x495e3bfe

    invoke-static {v0, v7, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v8

    .line 2690501
    iget-object v0, p0, LX/JPq;->a:LX/JPv;

    iget-object v0, v0, LX/JPv;->a:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->k()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v7, :cond_1

    move v6, v7

    .line 2690502
    :goto_0
    iget-object v0, p0, LX/JPq;->d:Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;

    iget-object v1, p0, LX/JPq;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    iget-object v2, p0, LX/JPq;->a:LX/JPv;

    iget-object v2, v2, LX/JPv;->a:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;

    invoke-static {v1, v2}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v1

    iget-object v2, p0, LX/JPq;->d:Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;

    iget-object v2, v2, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;->g:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iget-object v4, p0, LX/JPq;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v4

    if-eqz v4, :cond_2

    iget-object v4, p0, LX/JPq;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 2690503
    :goto_1
    iget-object v9, v0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;->d:LX/0Zb;

    .line 2690504
    new-instance v10, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v11, "page_admin_panel_context_item_tap"

    invoke-direct {v10, v11}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v11, "tracking"

    invoke-virtual {v10, v11, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    const-string v11, "admin_id"

    invoke-virtual {v10, v11, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    const-string v11, "event_name"

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->CLICK:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelEvent;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v11, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    const-string v11, "page_id"

    invoke-virtual {v10, v11, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    const-string v11, "is_ego_show_single_page_item"

    invoke-virtual {v10, v11, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    const-string v11, "source"

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;->CONTEXT_ROW:Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLMEgoPageAdminPanelSource;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v11, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    const-string v11, "page_admin_panel"

    .line 2690505
    iput-object v11, v10, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2690506
    move-object v10, v10

    .line 2690507
    move-object v10, v10

    .line 2690508
    invoke-interface {v9, v10}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2690509
    iget-object v0, p0, LX/JPq;->c:Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->k()Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    move-result-object v0

    .line 2690510
    sget-object v1, LX/JPs;->a:[I

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2690511
    iget-object v0, p0, LX/JPq;->d:Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;->c:LX/HVa;

    iget-object v1, p0, LX/JPq;->c:Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;

    .line 2690512
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->j()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 2690513
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->j()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v2, 0x0

    move v3, v2

    :goto_2
    if-ge v3, v5, :cond_4

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemLink;

    .line 2690514
    new-instance v6, LX/5No;

    invoke-direct {v6}, LX/5No;-><init>()V

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemLink;->a()LX/0Px;

    move-result-object v7

    .line 2690515
    iput-object v7, v6, LX/5No;->a:LX/0Px;

    .line 2690516
    move-object v6, v6

    .line 2690517
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemLink;->j()Ljava/lang/String;

    move-result-object v7

    .line 2690518
    iput-object v7, v6, LX/5No;->b:Ljava/lang/String;

    .line 2690519
    move-object v6, v6

    .line 2690520
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItemLink;->k()Ljava/lang/String;

    move-result-object v2

    .line 2690521
    iput-object v2, v6, LX/5No;->c:Ljava/lang/String;

    .line 2690522
    move-object v2, v6

    .line 2690523
    invoke-virtual {v2}, LX/5No;->a()Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel$ItemLinksModel;

    move-result-object v2

    invoke-static {v0, v2}, LX/HVa;->a(LX/HVa;Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel$ItemLinksModel;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2690524
    :cond_0
    :goto_3
    const v0, 0xd5ed3e3

    invoke-static {v0, v8}, LX/02F;->a(II)V

    return-void

    .line 2690525
    :cond_1
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 2690526
    :cond_2
    const-wide/16 v4, 0x0

    goto/16 :goto_1

    .line 2690527
    :pswitch_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2690528
    const-string v1, "extra_is_admin"

    invoke-virtual {v0, v1, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2690529
    const-string v1, "extra_page_tab"

    sget-object v2, LX/BEQ;->INSIGHTS:LX/BEQ;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2690530
    iget-object v1, p0, LX/JPq;->d:Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;->b:LX/1nA;

    iget-object v2, p0, LX/JPq;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    invoke-static {v2}, LX/3Bd;->a(Lcom/facebook/graphql/model/GraphQLPage;)LX/1y5;

    move-result-object v2

    invoke-virtual {v1, p1, v2, v0}, LX/1nA;->a(Landroid/view/View;LX/1y5;Landroid/os/Bundle;)V

    goto :goto_3

    .line 2690531
    :pswitch_1
    iget-object v0, p0, LX/JPq;->d:Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/hpp/ui/MobilePageAdminPanelPagePartDefinition;->b:LX/1nA;

    iget-object v1, p0, LX/JPq;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    invoke-static {v1}, LX/3Bd;->a(Lcom/facebook/graphql/model/GraphQLPage;)LX/1y5;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, LX/1nA;->a(Landroid/view/View;LX/1y5;Landroid/os/Bundle;)V

    goto :goto_3

    .line 2690532
    :cond_3
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    .line 2690533
    :cond_4
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntityCardContextItem;->a()Ljava/lang/String;

    move-result-object v2

    .line 2690534
    if-eqz v2, :cond_0

    .line 2690535
    iget-object v3, v0, LX/HVa;->b:LX/17Y;

    iget-object v4, v0, LX/HVa;->a:Landroid/content/Context;

    invoke-interface {v3, v4, v2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    .line 2690536
    if-eqz v3, :cond_5

    .line 2690537
    iget-object v2, v0, LX/HVa;->c:Lcom/facebook/content/SecureContextHelper;

    iget-object v4, v0, LX/HVa;->a:Landroid/content/Context;

    invoke-interface {v2, v3, v4}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_3

    .line 2690538
    :cond_5
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2690539
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2690540
    iget-object v2, v0, LX/HVa;->c:Lcom/facebook/content/SecureContextHelper;

    iget-object v4, v0, LX/HVa;->a:Landroid/content/Context;

    invoke-interface {v2, v3, v4}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
