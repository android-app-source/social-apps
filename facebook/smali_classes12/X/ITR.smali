.class public final LX/ITR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

.field public final synthetic b:Landroid/view/View;

.field public final synthetic c:LX/DPA;

.field public final synthetic d:LX/DPp;

.field public final synthetic e:LX/0Ot;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;Landroid/view/View;LX/DPA;LX/DPp;LX/0Ot;)V
    .locals 0

    .prologue
    .line 2579274
    iput-object p1, p0, LX/ITR;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    iput-object p2, p0, LX/ITR;->b:Landroid/view/View;

    iput-object p3, p0, LX/ITR;->c:LX/DPA;

    iput-object p4, p0, LX/ITR;->d:LX/DPp;

    iput-object p5, p0, LX/ITR;->e:LX/0Ot;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 14

    .prologue
    .line 2579275
    iget-object v0, p0, LX/ITR;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    iget-object v1, p0, LX/ITR;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/ITR;->c:LX/DPA;

    iget-object v3, p0, LX/ITR;->d:LX/DPp;

    iget-object v4, p0, LX/ITR;->e:LX/0Ot;

    .line 2579276
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->o()LX/1vs;

    move-result-object v5

    iget-object v8, v5, LX/1vs;->a:LX/15i;

    iget v9, v5, LX/1vs;->b:I

    .line 2579277
    new-instance v5, LX/DPD;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->d()Ljava/lang/String;

    move-result-object v7

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, LX/15i;->j(II)I

    move-result v8

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->K()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v9

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->I()Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;

    move-result-object v10

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->J()Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    move-result-object v11

    const-string v12, "group_mall"

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v13

    invoke-virtual {v13}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v13

    invoke-direct/range {v5 .. v13}, LX/DPD;-><init>(Ljava/lang/String;Ljava/lang/String;ILcom/facebook/graphql/enums/GraphQLGroupVisibility;Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupCategory;)V

    .line 2579278
    const-class v6, Landroid/support/v4/app/FragmentActivity;

    invoke-static {v1, v6}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v6

    .line 2579279
    new-instance v7, LX/ITS;

    invoke-direct {v7, v0, v3, v4}, LX/ITS;-><init>(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;LX/DPp;LX/0Ot;)V

    invoke-virtual {v2, v1, v6, v5, v7}, LX/DPA;->a(Landroid/content/Context;LX/0gc;LX/DPD;LX/DPC;)V

    .line 2579280
    const/4 v0, 0x1

    return v0
.end method
