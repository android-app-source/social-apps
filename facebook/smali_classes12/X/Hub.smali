.class public final LX/Hub;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AQ7;


# instance fields
.field private final a:LX/Hud;


# direct methods
.method public constructor <init>(LX/Hud;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2517757
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2517758
    iput-object p1, p0, LX/Hub;->a:LX/Hud;

    .line 2517759
    return-void
.end method

.method private static a(Ljava/lang/String;)V
    .locals 4
    .param p0    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2517760
    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 2517761
    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 2517762
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ComposerPluginMock.Factory: version change from "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " to 1"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2517763
    :catch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ComposerPluginMock.Factory: no support for instanceState"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2517764
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;LX/B5j;LX/B5f;)LX/AQ9;
    .locals 2
    .param p3    # LX/B5f;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData::",
            "Lcom/facebook/ipc/composer/plugin/ComposerPluginDataProvider;",
            "DerivedData::",
            "Lcom/facebook/ipc/composer/plugin/ComposerPluginDerivedDataProvider;",
            "Mutation::",
            "Lcom/facebook/ipc/composer/plugin/ComposerPluginMutation",
            "<TMutation;>;>(",
            "Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;",
            "LX/B5j",
            "<TModelData;TDerivedData;TMutation;>;",
            "LX/B5f;",
            ")",
            "Lcom/facebook/ipc/composer/plugin/ComposerPlugin",
            "<TModelData;TDerivedData;TMutation;>;"
        }
    .end annotation

    .prologue
    .line 2517747
    invoke-virtual {p2}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v0

    invoke-interface {v0}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getComposerType()LX/2rt;

    move-result-object v0

    .line 2517748
    sget-object v1, LX/2rt;->STATUS:LX/2rt;

    if-eq v0, v1, :cond_0

    sget-object v1, LX/2rt;->SHARE:LX/2rt;

    if-ne v0, v1, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2517749
    if-eqz p3, :cond_1

    .line 2517750
    iget-object v0, p3, LX/B5f;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2517751
    invoke-static {v0}, LX/Hub;->a(Ljava/lang/String;)V

    .line 2517752
    :cond_1
    iget-object v0, p0, LX/Hub;->a:LX/Hud;

    .line 2517753
    new-instance p0, LX/Huc;

    const-class v1, Landroid/content/Context;

    invoke-interface {v0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-direct {p0, v1, p2}, LX/Huc;-><init>(Landroid/content/Context;LX/B5j;)V

    .line 2517754
    move-object v0, p0

    .line 2517755
    return-object v0

    .line 2517756
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2517746
    const-string v0, "MOCK"

    return-object v0
.end method
