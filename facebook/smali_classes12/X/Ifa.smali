.class public final LX/Ifa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/Ie7;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;

.field public final synthetic b:LX/Ifb;


# direct methods
.method public constructor <init>(LX/Ifb;Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)V
    .locals 0

    .prologue
    .line 2599842
    iput-object p1, p0, LX/Ifa;->b:LX/Ifb;

    iput-object p2, p0, LX/Ifa;->a:Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2599843
    iget-object v0, p0, LX/Ifa;->b:LX/Ifb;

    iget-object v0, v0, LX/Ifb;->b:LX/Ife;

    iget-object v1, p0, LX/Ifa;->a:Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, LX/Ife;->b$redex0(LX/Ife;Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;Z)V

    .line 2599844
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2599845
    check-cast p1, LX/Ie7;

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2599846
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2599847
    sget-object v0, LX/Ie7;->NOTICE_SKIPPED:LX/Ie7;

    if-eq p1, v0, :cond_0

    .line 2599848
    iget-object v0, p0, LX/Ifa;->b:LX/Ifb;

    iget-object v0, v0, LX/Ifb;->b:LX/Ife;

    iget-object v0, v0, LX/Ife;->b:LX/IfT;

    const-string v1, "cymk_notice_shown"

    invoke-virtual {v0, v1}, LX/IfT;->a(Ljava/lang/String;)V

    .line 2599849
    :cond_0
    sget-object v0, LX/Ifd;->b:[I

    invoke-virtual {p1}, LX/Ie7;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2599850
    iget-object v0, p0, LX/Ifa;->b:LX/Ifb;

    iget-object v0, v0, LX/Ifb;->b:LX/Ife;

    iget-object v1, p0, LX/Ifa;->a:Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;

    invoke-static {v0, v1, v2}, LX/Ife;->b$redex0(LX/Ife;Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;Z)V

    .line 2599851
    :goto_0
    return-void

    .line 2599852
    :pswitch_0
    iget-object v0, p0, LX/Ifa;->b:LX/Ifb;

    iget-object v0, v0, LX/Ifb;->b:LX/Ife;

    iget-object v1, p0, LX/Ifa;->a:Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;

    invoke-static {v0, v1, v3}, LX/Ife;->a$redex0(LX/Ife;Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;Z)V

    goto :goto_0

    .line 2599853
    :pswitch_1
    iget-object v0, p0, LX/Ifa;->b:LX/Ifb;

    iget-object v0, v0, LX/Ifb;->b:LX/Ife;

    iget-object v1, p0, LX/Ifa;->a:Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;

    invoke-static {v0, v1, v2}, LX/Ife;->a$redex0(LX/Ife;Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;Z)V

    goto :goto_0

    .line 2599854
    :pswitch_2
    iget-object v0, p0, LX/Ifa;->b:LX/Ifb;

    iget-object v0, v0, LX/Ifb;->b:LX/Ife;

    iget-object v1, p0, LX/Ifa;->a:Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;

    invoke-static {v0, v1, v3}, LX/Ife;->b$redex0(LX/Ife;Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
