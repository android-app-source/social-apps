.class public final LX/I7m;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/events/feed/protocol/EventCreationStoryQueryModels$EventCreationStoryQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/I7n;


# direct methods
.method public constructor <init>(LX/I7n;)V
    .locals 0

    .prologue
    .line 2540129
    iput-object p1, p0, LX/I7m;->a:LX/I7n;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2540118
    iget-object v0, p0, LX/I7m;->a:LX/I7n;

    invoke-static {v0}, LX/I7n;->d(LX/I7n;)Z

    .line 2540119
    iget-object v0, p0, LX/I7m;->a:LX/I7n;

    iget-object v0, v0, LX/I7n;->d:Landroid/content/Context;

    iget-object v1, p0, LX/I7m;->a:LX/I7n;

    iget-object v1, v1, LX/I7n;->d:Landroid/content/Context;

    const v2, 0x7f080039

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    .line 2540120
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2540121
    check-cast p1, Lcom/facebook/events/feed/protocol/EventCreationStoryQueryModels$EventCreationStoryQueryModel;

    .line 2540122
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/events/feed/protocol/EventCreationStoryQueryModels$EventCreationStoryQueryModel;->a()Lcom/facebook/events/feed/protocol/EventCreationStoryQueryModels$EventCreationStoryQueryModel$CreationStoryModel;

    move-result-object v0

    if-nez v0, :cond_2

    .line 2540123
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null Graphql Result for %s"

    const-class v2, Lcom/facebook/events/feed/protocol/EventCreationStoryQueryModels$EventCreationStoryQueryModel;

    invoke-virtual {v2}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/I7m;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    .line 2540124
    :cond_1
    :goto_0
    return-void

    .line 2540125
    :cond_2
    iget-object v0, p0, LX/I7m;->a:LX/I7n;

    invoke-virtual {p1}, Lcom/facebook/events/feed/protocol/EventCreationStoryQueryModels$EventCreationStoryQueryModel;->a()Lcom/facebook/events/feed/protocol/EventCreationStoryQueryModels$EventCreationStoryQueryModel$CreationStoryModel;

    move-result-object v1

    .line 2540126
    iput-object v1, v0, LX/I7n;->i:Lcom/facebook/events/feed/protocol/EventCreationStoryQueryModels$EventCreationStoryQueryModel$CreationStoryModel;

    .line 2540127
    iget-object v0, p0, LX/I7m;->a:LX/I7n;

    invoke-static {v0}, LX/I7n;->d(LX/I7n;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2540128
    iget-object v0, p0, LX/I7m;->a:LX/I7n;

    invoke-virtual {v0}, LX/I7n;->a()V

    goto :goto_0
.end method
