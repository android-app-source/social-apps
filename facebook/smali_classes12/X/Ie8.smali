.class public LX/Ie8;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/6cf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2598324
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(LX/Ie8;)Z
    .locals 3

    .prologue
    .line 2598354
    iget-object v0, p0, LX/Ie8;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/Ie9;->a:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;LX/0gc;Lcom/facebook/user/model/User;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0gc;",
            "Lcom/facebook/user/model/User;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/Ie7;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2598327
    iget-object v0, p3, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v3, v0

    .line 2598328
    invoke-virtual {p3}, Lcom/facebook/user/model/User;->j()Ljava/lang/String;

    move-result-object v4

    .line 2598329
    iget-object v0, p3, Lcom/facebook/user/model/User;->e:Lcom/facebook/user/model/Name;

    move-object v0, v0

    .line 2598330
    invoke-virtual {v0}, Lcom/facebook/user/model/Name;->j()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    const/4 p3, 0x1

    const/4 p2, 0x0

    .line 2598331
    iget-object v6, v0, LX/Ie8;->a:LX/6cf;

    .line 2598332
    iget-object v7, v6, LX/6cf;->a:LX/0Uh;

    const/16 v8, 0xc

    const/4 p0, 0x0

    invoke-virtual {v7, v8, p0}, LX/0Uh;->a(IZ)Z

    move-result v7

    move v6, v7

    .line 2598333
    if-eqz v6, :cond_0

    .line 2598334
    invoke-static {v0}, LX/Ie8;->b(LX/Ie8;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2598335
    iget-object v6, v0, LX/Ie8;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v7, LX/Ie9;->b:LX/0Tn;

    const/4 v8, 0x0

    invoke-interface {v6, v7, v8}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v6

    move v6, v6

    .line 2598336
    if-eqz v6, :cond_2

    .line 2598337
    sget-object v6, LX/Ie7;->NOTICE_SKIPPED:LX/Ie7;

    invoke-static {v6}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    .line 2598338
    :goto_0
    move-object v6, v6

    .line 2598339
    :goto_1
    move-object v0, v6

    .line 2598340
    return-object v0

    .line 2598341
    :cond_0
    invoke-static {v0}, LX/Ie8;->b(LX/Ie8;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2598342
    sget-object v6, LX/Ie7;->NOTICE_SKIPPED:LX/Ie7;

    invoke-static {v6}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    goto :goto_1

    .line 2598343
    :cond_1
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v6

    .line 2598344
    new-instance v7, LX/31Y;

    invoke-direct {v7, v1}, LX/31Y;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const p0, 0x7f0839fd

    new-array p1, p3, [Ljava/lang/Object;

    aput-object v4, p1, p2

    invoke-virtual {v8, p0, p1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v7

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const p0, 0x7f0839fe

    new-array p1, p3, [Ljava/lang/Object;

    aput-object v5, p1, p2

    invoke-virtual {v8, p0, p1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v7

    const v8, 0x7f0839fc

    new-instance p0, LX/Ie5;

    invoke-direct {p0, v0, v6}, LX/Ie5;-><init>(LX/Ie8;Lcom/google/common/util/concurrent/SettableFuture;)V

    invoke-virtual {v7, v8, p0}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v7

    const v8, 0x7f080017

    new-instance p0, LX/Ie4;

    invoke-direct {p0, v0, v6}, LX/Ie4;-><init>(LX/Ie8;Lcom/google/common/util/concurrent/SettableFuture;)V

    invoke-virtual {v7, v8, p0}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v7

    invoke-virtual {v7}, LX/0ju;->b()LX/2EJ;

    goto :goto_1

    .line 2598345
    :cond_2
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v6

    .line 2598346
    new-instance v7, Lcom/facebook/messaging/contacts/addcontactnotice/AddOnMessengerNuxDialogFragment;

    invoke-direct {v7}, Lcom/facebook/messaging/contacts/addcontactnotice/AddOnMessengerNuxDialogFragment;-><init>()V

    .line 2598347
    invoke-static {v3}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v8

    .line 2598348
    iput-object v8, v7, Lcom/facebook/messaging/contacts/addcontactnotice/AddOnMessengerNuxDialogFragment;->m:Lcom/facebook/user/model/UserKey;

    .line 2598349
    iput-object v4, v7, Lcom/facebook/messaging/contacts/addcontactnotice/AddOnMessengerNuxDialogFragment;->n:Ljava/lang/String;

    .line 2598350
    iput-object v5, v7, Lcom/facebook/messaging/contacts/addcontactnotice/AddOnMessengerNuxDialogFragment;->o:Ljava/lang/String;

    .line 2598351
    iput-object v6, v7, Lcom/facebook/messaging/contacts/addcontactnotice/AddOnMessengerNuxDialogFragment;->p:Lcom/google/common/util/concurrent/SettableFuture;

    .line 2598352
    const-string v8, "add_on_messenger_nux"

    invoke-virtual {v7, v2, v8}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2598353
    new-instance v7, LX/Ie6;

    invoke-direct {v7, v0}, LX/Ie6;-><init>(LX/Ie8;)V

    iget-object v8, v0, LX/Ie8;->b:Ljava/util/concurrent/Executor;

    invoke-static {v6, v7, v8}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method

.method public final a()V
    .locals 3

    .prologue
    .line 2598325
    iget-object v0, p0, LX/Ie8;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/Ie9;->a:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2598326
    return-void
.end method
