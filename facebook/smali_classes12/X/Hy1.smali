.class public LX/Hy1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0kx;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/support/v4/app/Fragment;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/fbreact/interfaces/FbReactInstanceManager;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2523460
    const-class v0, LX/Hy1;

    sput-object v0, LX/Hy1;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0kx;LX/0Or;LX/0Ot;LX/0ad;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/events/annotation/EventsDashboardReactFragment;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0kx;",
            "LX/0Or",
            "<",
            "Landroid/support/v4/app/Fragment;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/fbreact/interfaces/FbReactInstanceManager;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2523461
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2523462
    iput-object p1, p0, LX/Hy1;->b:LX/0kx;

    .line 2523463
    iput-object p2, p0, LX/Hy1;->c:LX/0Or;

    .line 2523464
    iput-object p3, p0, LX/Hy1;->d:LX/0Ot;

    .line 2523465
    iput-object p4, p0, LX/Hy1;->e:LX/0ad;

    .line 2523466
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2523467
    const-string v0, "section_name"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2523468
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2523469
    const-string v2, "force_tabbed_dashboard"

    invoke-virtual {p1, v2, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 2523470
    const-string v3, "force_tabbed_dashboard"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2523471
    iget-object v3, p0, LX/Hy1;->e:LX/0ad;

    sget-short v4, LX/347;->e:S

    invoke-interface {v3, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2523472
    sget-object v0, LX/Hx7;->DISCOVER:LX/Hx7;

    invoke-virtual {v0}, LX/Hx7;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, LX/Hy1;->b:LX/0kx;

    const-string v3, "unknown"

    invoke-virtual {v2, v3}, LX/0kx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2523473
    new-instance v3, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;

    invoke-direct {v3}, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;-><init>()V

    .line 2523474
    const-string v4, "extra_key_dashboard_tab_type"

    invoke-virtual {v1, v4, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2523475
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2523476
    const-string v4, "extra_ref_module"

    invoke-virtual {v1, v4, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2523477
    :cond_0
    invoke-virtual {v3, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2523478
    move-object v0, v3

    .line 2523479
    :goto_0
    return-object v0

    .line 2523480
    :cond_1
    if-nez v0, :cond_2

    if-nez v2, :cond_2

    iget-object v2, p0, LX/Hy1;->e:LX/0ad;

    sget-object v3, LX/0c0;->Live:LX/0c0;

    sget-short v4, LX/347;->C:S

    invoke-interface {v2, v3, v4, v5}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2523481
    iget-object v0, p0, LX/Hy1;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JKf;

    invoke-virtual {v0}, LX/JKf;->b()V

    .line 2523482
    iget-object v0, p0, LX/Hy1;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    goto :goto_0

    .line 2523483
    :cond_2
    iget-object v2, p0, LX/Hy1;->e:LX/0ad;

    sget-short v3, LX/347;->M:S

    invoke-interface {v2, v3, v5}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2523484
    iget-object v0, p0, LX/Hy1;->b:LX/0kx;

    const-string v2, "unknown"

    invoke-virtual {v0, v2}, LX/0kx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2523485
    new-instance v2, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;

    invoke-direct {v2}, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;-><init>()V

    .line 2523486
    const-string v3, "extra_dashboard_tab_type"

    sget-object v4, LX/Hx7;->DISCOVER:LX/Hx7;

    invoke-virtual {v4}, LX/Hx7;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2523487
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 2523488
    const-string v3, "extra_ref_module"

    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2523489
    :cond_3
    invoke-virtual {v2, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2523490
    move-object v0, v2

    .line 2523491
    goto :goto_0

    .line 2523492
    :cond_4
    iget-object v2, p0, LX/Hy1;->b:LX/0kx;

    const-string v3, "unknown"

    invoke-virtual {v2, v3}, LX/0kx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2523493
    new-instance v3, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    invoke-direct {v3}, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;-><init>()V

    .line 2523494
    const-string v4, "extra_dashboard_filter_type"

    invoke-virtual {v1, v4, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2523495
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 2523496
    const-string v4, "extra_ref_module"

    invoke-virtual {v1, v4, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2523497
    :cond_5
    invoke-virtual {v3, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2523498
    move-object v0, v3

    .line 2523499
    goto :goto_0
.end method
