.class public LX/HtT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/HtM;


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:LX/0tX;

.field private final c:Lcom/facebook/composer/feedattachment/ComposerFeedAttachmentViewBinder;


# direct methods
.method public constructor <init>(LX/0tX;Landroid/content/res/Resources;Lcom/facebook/composer/feedattachment/ComposerFeedAttachmentViewBinder;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2516207
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2516208
    iput-object p1, p0, LX/HtT;->b:LX/0tX;

    .line 2516209
    iput-object p2, p0, LX/HtT;->a:Landroid/content/res/Resources;

    .line 2516210
    iput-object p3, p0, LX/HtT;->c:Lcom/facebook/composer/feedattachment/ComposerFeedAttachmentViewBinder;

    .line 2516211
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2516197
    new-instance v0, Lcom/facebook/attachments/angora/AngoraAttachmentView;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/attachments/angora/AngoraAttachmentView;-><init>(Landroid/content/Context;)V

    .line 2516198
    iget-object v1, p0, LX/HtT;->a:Landroid/content/res/Resources;

    const v2, 0x7f020a3d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/attachments/angora/AngoraAttachmentView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2516199
    iget-object v1, p0, LX/HtT;->c:Lcom/facebook/composer/feedattachment/ComposerFeedAttachmentViewBinder;

    invoke-virtual {v1, p1, v0}, Lcom/facebook/composer/feedattachment/ComposerFeedAttachmentViewBinder;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/attachments/angora/AngoraAttachmentView;)V

    .line 2516200
    return-object v0
.end method

.method public final a(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/composer/minutiae/model/MinutiaeObject;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2516201
    iget-object v0, p0, LX/HtT;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b03ee

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 2516202
    new-instance v1, LX/IC4;

    invoke-direct {v1}, LX/IC4;-><init>()V

    move-object v1, v1

    .line 2516203
    const-class v2, Lcom/facebook/graphql/model/GraphQLProfile;

    invoke-static {v2}, LX/0w5;->b(Ljava/lang/Class;)LX/0w5;

    move-result-object v2

    invoke-static {v1, v2}, LX/0zO;->a(LX/0gW;LX/0w5;)LX/0zO;

    move-result-object v1

    new-instance v2, LX/IC4;

    invoke-direct {v2}, LX/IC4;-><init>()V

    const-string v3, "object_id"

    iget-object v4, p1, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->object:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;

    invoke-virtual {v4}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel;->n()Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/composer/minutiae/graphql/MinutiaeDefaultsGraphQLModels$MinutiaeTaggableObjectFieldsModel;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "profile_image_size"

    invoke-virtual {v2, v3, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    .line 2516204
    iget-object v2, v0, LX/0gW;->e:LX/0w7;

    move-object v0, v2

    .line 2516205
    invoke-virtual {v1, v0}, LX/0zO;->a(LX/0w7;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    .line 2516206
    iget-object v1, p0, LX/HtT;->b:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    new-instance v1, LX/HtS;

    invoke-direct {v1, p0}, LX/HtS;-><init>(LX/HtT;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
