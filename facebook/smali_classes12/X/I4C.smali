.class public LX/I4C;
.super LX/I4B;
.source ""


# instance fields
.field public H:LX/Clr;

.field public I:LX/Clr;

.field private J:Landroid/content/Context;

.field public K:Lcom/facebook/events/common/EventAnalyticsParams;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2533825
    invoke-direct {p0, p1}, LX/I4B;-><init>(Landroid/content/Context;)V

    .line 2533826
    iput-object p1, p0, LX/I4C;->J:Landroid/content/Context;

    .line 2533827
    return-void
.end method


# virtual methods
.method public final synthetic a(I)LX/I4B;
    .locals 1

    .prologue
    .line 2533822
    invoke-super {p0, p1}, LX/I4B;->a(I)LX/I4B;

    .line 2533823
    move-object v0, p0

    .line 2533824
    return-object v0
.end method

.method public final a(LX/B5a;Landroid/content/Context;)LX/I4B;
    .locals 4

    .prologue
    .line 2533801
    invoke-interface {p1}, LX/B5a;->o()Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2533802
    :goto_0
    return-object p0

    .line 2533803
    :cond_0
    instance-of v0, p1, LX/I49;

    if-nez v0, :cond_1

    .line 2533804
    invoke-super {p0, p1, p2}, LX/I4B;->a(LX/B5a;Landroid/content/Context;)LX/I4B;

    move-result-object p0

    goto :goto_0

    .line 2533805
    :cond_1
    check-cast p1, LX/I49;

    .line 2533806
    invoke-virtual {p1}, LX/I49;->o()Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    move-result-object v0

    .line 2533807
    sget-object v1, LX/I4A;->a:[I

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2533808
    :cond_2
    :goto_1
    invoke-super {p0, p1, p2}, LX/I4B;->a(LX/B5a;Landroid/content/Context;)LX/I4B;

    move-result-object p0

    goto :goto_0

    .line 2533809
    :pswitch_0
    invoke-virtual {p1}, LX/I49;->f()LX/7oa;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2533810
    iget-object v0, p0, LX/I4B;->z:Ljava/util/List;

    iget-object v1, p0, LX/I4C;->J:Landroid/content/Context;

    invoke-virtual {p1}, LX/I49;->f()LX/7oa;

    move-result-object v2

    iget-object v3, p0, LX/I4C;->K:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-static {v1, v2, v3}, LX/I4F;->a(Landroid/content/Context;LX/7oa;Lcom/facebook/events/common/EventAnalyticsParams;)LX/Clr;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2533811
    :pswitch_1
    invoke-virtual {p1}, LX/I49;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2533812
    iget-object v0, p0, LX/I4B;->z:Ljava/util/List;

    .line 2533813
    new-instance v1, LX/I4H;

    invoke-virtual {p1}, LX/I49;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/I4H;-><init>(Ljava/lang/String;)V

    .line 2533814
    iget-object v2, p1, LX/I49;->a:Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;

    invoke-virtual {v2}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->l()Ljava/lang/String;

    move-result-object v2

    move-object v2, v2

    .line 2533815
    iput-object v2, v1, LX/I4H;->b:Ljava/lang/String;

    .line 2533816
    move-object v1, v1

    .line 2533817
    iget-object v2, p1, LX/I49;->a:Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;

    invoke-virtual {v2}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionSectionEdgeModel$EventCollectionSectionModel;->m()Ljava/lang/String;

    move-result-object v2

    move-object v2, v2

    .line 2533818
    iput-object v2, v1, LX/I4H;->c:Ljava/lang/String;

    .line 2533819
    move-object v1, v1

    .line 2533820
    invoke-virtual {v1}, LX/I4H;->c()LX/I4I;

    move-result-object v1

    move-object v1, v1

    .line 2533821
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final synthetic a(Ljava/lang/String;)LX/I4B;
    .locals 1

    .prologue
    .line 2533798
    invoke-super {p0, p1}, LX/I4B;->a(Ljava/lang/String;)LX/I4B;

    .line 2533799
    move-object v0, p0

    .line 2533800
    return-object v0
.end method

.method public final b()LX/Clo;
    .locals 4

    .prologue
    .line 2533782
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2533783
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2533784
    iget-object v2, p0, LX/I4C;->H:LX/Clr;

    if-eqz v2, :cond_0

    .line 2533785
    iget-object v2, p0, LX/I4C;->H:LX/Clr;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2533786
    :cond_0
    iget-object v2, p0, LX/I4C;->K:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2533787
    new-instance v3, LX/I4N;

    invoke-direct {v3, v2}, LX/I4N;-><init>(Lcom/facebook/events/common/EventAnalyticsParams;)V

    invoke-virtual {v3}, LX/I4N;->c()LX/I4O;

    move-result-object v3

    move-object v2, v3

    .line 2533788
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2533789
    iget-object v2, p0, LX/I4C;->I:LX/Clr;

    if-eqz v2, :cond_1

    .line 2533790
    iget-object v2, p0, LX/I4C;->I:LX/Clr;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2533791
    :cond_1
    new-instance v2, LX/Clo;

    iget-object v3, p0, LX/I4B;->s:Ljava/lang/String;

    invoke-direct {v2, v3}, LX/Clo;-><init>(Ljava/lang/String;)V

    .line 2533792
    invoke-virtual {v2, v0}, LX/Clo;->a(Ljava/util/Collection;)V

    .line 2533793
    iget-object v0, p0, LX/I4B;->z:Ljava/util/List;

    invoke-virtual {v2, v0}, LX/Clo;->a(Ljava/util/Collection;)V

    .line 2533794
    invoke-virtual {v2, v1}, LX/Clo;->a(Ljava/util/Collection;)V

    .line 2533795
    iget-object v0, v2, LX/Clo;->b:Landroid/os/Bundle;

    move-object v0, v0

    .line 2533796
    iget-object v1, p0, LX/I4B;->F:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 2533797
    return-object v2
.end method

.method public final b(LX/8Z4;)LX/I4B;
    .locals 1

    .prologue
    .line 2533828
    invoke-super {p0, p1}, LX/I4B;->b(LX/8Z4;)LX/I4B;

    .line 2533829
    return-object p0
.end method

.method public final synthetic b(Ljava/lang/String;)LX/I4B;
    .locals 1

    .prologue
    .line 2533779
    invoke-super {p0, p1}, LX/I4B;->b(Ljava/lang/String;)LX/I4B;

    .line 2533780
    move-object v0, p0

    .line 2533781
    return-object v0
.end method

.method public final b(I)LX/I4C;
    .locals 0

    .prologue
    .line 2533771
    invoke-super {p0, p1}, LX/I4B;->a(I)LX/I4B;

    .line 2533772
    return-object p0
.end method

.method public final c(Ljava/lang/String;)LX/I4B;
    .locals 1

    .prologue
    .line 2533777
    invoke-super {p0, p1}, LX/I4B;->c(Ljava/lang/String;)LX/I4B;

    .line 2533778
    return-object p0
.end method

.method public final f(Ljava/lang/String;)LX/I4C;
    .locals 0

    .prologue
    .line 2533775
    invoke-super {p0, p1}, LX/I4B;->a(Ljava/lang/String;)LX/I4B;

    .line 2533776
    return-object p0
.end method

.method public final g(Ljava/lang/String;)LX/I4C;
    .locals 0

    .prologue
    .line 2533773
    invoke-super {p0, p1}, LX/I4B;->b(Ljava/lang/String;)LX/I4B;

    .line 2533774
    return-object p0
.end method
