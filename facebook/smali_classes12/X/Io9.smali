.class public LX/Io9;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public b:LX/Iab;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final c:Landroid/support/v4/app/Fragment;

.field private final d:Ljava/util/concurrent/Executor;

.field public final e:LX/03V;

.field public final f:Lcom/facebook/messaging/payment/method/verification/PaymentCardsFetcher;

.field public final g:Landroid/content/Context;

.field public final h:Landroid/content/res/Resources;

.field public final i:LX/Ijy;

.field private final j:Z

.field public k:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/p2p/model/PaymentCard;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2611558
    const-class v0, LX/Io9;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Io9;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/support/v4/app/Fragment;ZLjava/util/concurrent/Executor;LX/03V;Lcom/facebook/messaging/payment/method/verification/PaymentCardsFetcher;Landroid/content/Context;Landroid/content/res/Resources;LX/Ijy;)V
    .locals 0
    .param p1    # Landroid/support/v4/app/Fragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2611559
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2611560
    iput-object p1, p0, LX/Io9;->c:Landroid/support/v4/app/Fragment;

    .line 2611561
    iput-boolean p2, p0, LX/Io9;->j:Z

    .line 2611562
    iput-object p3, p0, LX/Io9;->d:Ljava/util/concurrent/Executor;

    .line 2611563
    iput-object p4, p0, LX/Io9;->e:LX/03V;

    .line 2611564
    iput-object p5, p0, LX/Io9;->f:Lcom/facebook/messaging/payment/method/verification/PaymentCardsFetcher;

    .line 2611565
    iput-object p6, p0, LX/Io9;->g:Landroid/content/Context;

    .line 2611566
    iput-object p7, p0, LX/Io9;->h:Landroid/content/res/Resources;

    .line 2611567
    iput-object p8, p0, LX/Io9;->i:LX/Ijy;

    .line 2611568
    return-void
.end method

.method public static c(LX/Io9;LX/0Px;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/p2p/model/PaymentCard;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2611569
    new-instance v0, LX/Io7;

    invoke-direct {v0, p0}, LX/Io7;-><init>(LX/Io9;)V

    .line 2611570
    invoke-static {}, LX/Ik2;->newBuilder()LX/Ik0;

    move-result-object v1

    .line 2611571
    iput-object p1, v1, LX/Ik0;->b:LX/0Px;

    .line 2611572
    move-object v1, v1

    .line 2611573
    sget-object v2, LX/5g0;->MESSENGER_COMMERCE:LX/5g0;

    .line 2611574
    iput-object v2, v1, LX/Ik0;->e:LX/5g0;

    .line 2611575
    move-object v1, v1

    .line 2611576
    iget-object v2, p0, LX/Io9;->c:Landroid/support/v4/app/Fragment;

    .line 2611577
    iput-object v2, v1, LX/Ik0;->c:Landroid/support/v4/app/Fragment;

    .line 2611578
    move-object v1, v1

    .line 2611579
    iget-boolean v2, p0, LX/Io9;->j:Z

    .line 2611580
    iput-boolean v2, v1, LX/Ik0;->k:Z

    .line 2611581
    move-object v1, v1

    .line 2611582
    invoke-virtual {v1}, LX/Ik0;->a()LX/Ik2;

    move-result-object v1

    .line 2611583
    iget-object v2, p0, LX/Io9;->i:LX/Ijy;

    invoke-virtual {v2, v1, v0}, LX/Ijy;->a(LX/Ik2;LX/Ijx;)V

    .line 2611584
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .prologue
    .line 2611585
    iget-object v0, p0, LX/Io9;->f:Lcom/facebook/messaging/payment/method/verification/PaymentCardsFetcher;

    invoke-virtual {v0}, Lcom/facebook/messaging/payment/method/verification/PaymentCardsFetcher;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/Io9;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2611586
    iget-object v0, p0, LX/Io9;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, LX/Io6;

    invoke-direct {v1, p0}, LX/Io6;-><init>(LX/Io9;)V

    iget-object v2, p0, LX/Io9;->d:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2611587
    return-void
.end method
