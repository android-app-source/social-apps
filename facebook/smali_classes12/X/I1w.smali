.class public LX/I1w;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2529532
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2529533
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 2

    .prologue
    .line 2529534
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2529535
    const/4 v0, 0x0

    .line 2529536
    :goto_0
    return-object v0

    .line 2529537
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "extra_events_hosting_dashboard_section_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/I2H;

    .line 2529538
    sget-object v1, LX/I2H;->PAST:LX/I2H;

    if-ne v0, v1, :cond_1

    .line 2529539
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 2529540
    new-instance v1, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;

    invoke-direct {v1}, Lcom/facebook/events/dashboard/hosting/EventsDashboardPastEventsFragment;-><init>()V

    .line 2529541
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2529542
    move-object v0, v1

    .line 2529543
    goto :goto_0

    .line 2529544
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 2529545
    new-instance v1, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;

    invoke-direct {v1}, Lcom/facebook/events/dashboard/hosting/EventsDashboardHostingFragment;-><init>()V

    .line 2529546
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2529547
    move-object v0, v1

    .line 2529548
    goto :goto_0
.end method
