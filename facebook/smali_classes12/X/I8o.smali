.class public LX/I8o;
.super LX/1Cv;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "LX/IBr;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private c:[LX/IBr;

.field public d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2542039
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 2542040
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/I8o;->b:Ljava/util/HashMap;

    .line 2542041
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/I8o;->d:Z

    .line 2542042
    iput-object p1, p0, LX/I8o;->a:Landroid/content/Context;

    .line 2542043
    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2542037
    invoke-static {}, LX/I8n;->values()[LX/I8n;

    move-result-object v0

    aget-object v0, v0, p1

    .line 2542038
    iget-object v1, p0, LX/I8o;->a:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    sget-object v2, LX/I8n;->TAB_BAR:LX/I8n;

    if-ne v0, v2, :cond_0

    const v0, 0x7f030504

    :goto_0
    const/4 v2, 0x0

    invoke-virtual {v1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_0
    const v0, 0x7f0304f7

    goto :goto_0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 2542032
    invoke-static {}, LX/I8n;->values()[LX/I8n;

    move-result-object v0

    aget-object v0, v0, p4

    sget-object v1, LX/I8n;->TAB_BAR:LX/I8n;

    if-ne v0, v1, :cond_0

    .line 2542033
    check-cast p3, Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;

    .line 2542034
    iget-object v0, p0, LX/I8o;->c:[LX/IBr;

    invoke-virtual {p3, v0}, Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;->setTabTypes([LX/IBr;)V

    .line 2542035
    iget-object v0, p0, LX/I8o;->b:Ljava/util/HashMap;

    invoke-virtual {p3, v0}, Lcom/facebook/events/permalink/tabbar/EventPermalinkTabBar;->setBadges(Ljava/util/HashMap;)V

    .line 2542036
    :cond_0
    return-void
.end method

.method public final a([LX/IBr;)V
    .locals 1

    .prologue
    .line 2542029
    iput-object p1, p0, LX/I8o;->c:[LX/IBr;

    .line 2542030
    const v0, 0x631f4338

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2542031
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2542022
    iget-boolean v0, p0, LX/I8o;->d:Z

    if-eqz v0, :cond_0

    .line 2542023
    const/4 v0, 0x2

    .line 2542024
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2542028
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2542027
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2542026
    if-nez p1, :cond_0

    sget-object v0, LX/I8n;->TAB_BAR:LX/I8n;

    invoke-virtual {v0}, LX/I8n;->ordinal()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    sget-object v0, LX/I8n;->EVENT_PERMALINK_GAP_VIEW:LX/I8n;

    invoke-virtual {v0}, LX/I8n;->ordinal()I

    move-result v0

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2542025
    invoke-static {}, LX/I8n;->values()[LX/I8n;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
