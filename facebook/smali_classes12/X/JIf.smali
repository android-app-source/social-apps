.class public LX/JIf;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/JIe;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2678261
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 2678262
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;LX/Eny;LX/Emj;Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;)LX/JIe;
    .locals 13

    .prologue
    .line 2678259
    new-instance v0, LX/JIe;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    const/16 v2, 0x2eb

    invoke-static {p0, v2}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    const/16 v3, 0x455

    invoke-static {p0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {p0}, LX/JJt;->a(LX/0QB;)LX/JJt;

    move-result-object v5

    check-cast v5, LX/JJt;

    const/16 v6, 0x1ace

    invoke-static {p0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const-class v7, LX/JIO;

    invoke-interface {p0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/JIO;

    const/16 v8, 0x1acd

    invoke-static {p0, v8}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    move-object v9, p1

    move-object v10, p2

    move-object/from16 v11, p3

    move-object/from16 v12, p4

    invoke-direct/range {v0 .. v12}, LX/JIe;-><init>(Ljava/util/concurrent/Executor;LX/0Or;LX/0Or;LX/03V;LX/JJt;LX/0Or;LX/JIO;LX/0Or;Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;LX/Eny;LX/Emj;Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;)V

    .line 2678260
    return-object v0
.end method
