.class public LX/Htl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements LX/0iK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0io;",
        ":",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesIsKeyboardUp;",
        ":",
        "LX/0j0;",
        ":",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesMaxBottomOverlayHeight;",
        ":",
        "LX/0j8;",
        ":",
        "LX/0jD;",
        ":",
        "Lcom/facebook/composer/inlinesprouts/model/InlineSproutsStateSpec$ProvidesInlineSproutsState;",
        ":",
        "LX/0ip;",
        "DerivedData:",
        "Ljava/lang/Object;",
        "Mutation::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
        ":",
        "Lcom/facebook/composer/inlinesprouts/model/InlineSproutsStateSpec$SetsInlineSproutsState",
        "<TMutation;>;Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;:",
        "LX/0im",
        "<TMutation;>;>",
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "LX/0iK",
        "<TModelData;TDerivedData;>;"
    }
.end annotation


# static fields
.field public static final a:LX/0jK;


# instance fields
.field public final b:LX/0il;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TServices;"
        }
    .end annotation
.end field

.field public final c:Landroid/widget/TextView;

.field public final d:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Lcom/facebook/composer/inlinesprouts/InlineSproutsView;

.field public final f:LX/Hts;

.field private final g:LX/Hth;

.field private final h:LX/Hte;

.field public final i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/inlinesproutsinterfaces/InlineSproutItem;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public k:Z

.field public l:Ljava/lang/String;

.field public m:LX/Htp;

.field private final n:LX/Hti;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2516736
    const-class v0, LX/Htl;

    invoke-static {v0}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object v0

    sput-object v0, LX/Htl;->a:LX/0jK;

    return-void
.end method

.method public constructor <init>(Landroid/view/View$OnClickListener;Lcom/facebook/composer/inlinesprouts/InlineSproutsView;Landroid/widget/TextView;LX/0zw;LX/0zw;LX/0il;LX/0Px;Landroid/content/Context;LX/Htt;LX/Hth;LX/Hte;)V
    .locals 2
    .param p1    # Landroid/view/View$OnClickListener;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/composer/inlinesprouts/InlineSproutsView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Landroid/widget/TextView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/0zw;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/0zw;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View$OnClickListener;",
            "Lcom/facebook/composer/inlinesprouts/InlineSproutsView;",
            "Landroid/widget/TextView;",
            "LX/0zw",
            "<",
            "Landroid/view/View;",
            ">;",
            "LX/0zw",
            "<",
            "Landroid/view/View;",
            ">;TServices;",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/inlinesproutsinterfaces/InlineSproutItem;",
            ">;",
            "Landroid/content/Context;",
            "LX/Htt;",
            "LX/Hth;",
            "LX/Hte;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2516712
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2516713
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Htl;->k:Z

    .line 2516714
    const-string v0, ""

    iput-object v0, p0, LX/Htl;->l:Ljava/lang/String;

    .line 2516715
    sget-object v0, LX/Htp;->NO_ANIMATION:LX/Htp;

    iput-object v0, p0, LX/Htl;->m:LX/Htp;

    .line 2516716
    new-instance v0, LX/Htj;

    invoke-direct {v0, p0}, LX/Htj;-><init>(LX/Htl;)V

    iput-object v0, p0, LX/Htl;->n:LX/Hti;

    .line 2516717
    iput-object p2, p0, LX/Htl;->e:Lcom/facebook/composer/inlinesprouts/InlineSproutsView;

    .line 2516718
    iput-object p3, p0, LX/Htl;->c:Landroid/widget/TextView;

    .line 2516719
    iput-object p4, p0, LX/Htl;->d:LX/0zw;

    .line 2516720
    iput-object p6, p0, LX/Htl;->b:LX/0il;

    .line 2516721
    iput-object p10, p0, LX/Htl;->g:LX/Hth;

    .line 2516722
    iput-object p11, p0, LX/Htl;->h:LX/Hte;

    .line 2516723
    new-instance v1, LX/Hts;

    invoke-static {p9}, LX/Htv;->a(LX/0QB;)LX/Htv;

    move-result-object v0

    check-cast v0, LX/Htv;

    invoke-direct {v1, p8, v0}, LX/Hts;-><init>(Landroid/content/Context;LX/Htv;)V

    .line 2516724
    move-object v0, v1

    .line 2516725
    iput-object v0, p0, LX/Htl;->f:LX/Hts;

    .line 2516726
    iput-object p7, p0, LX/Htl;->i:LX/0Px;

    .line 2516727
    iget-object v0, p0, LX/Htl;->e:Lcom/facebook/composer/inlinesprouts/InlineSproutsView;

    iget-object v1, p0, LX/Htl;->f:LX/Hts;

    invoke-virtual {v0, v1}, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->setSproutAdapter(LX/Hts;)V

    .line 2516728
    iget-object v0, p0, LX/Htl;->e:Lcom/facebook/composer/inlinesprouts/InlineSproutsView;

    invoke-virtual {v0, p0}, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->setSproutItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2516729
    iget-object v0, p0, LX/Htl;->e:Lcom/facebook/composer/inlinesprouts/InlineSproutsView;

    iget-object v1, p0, LX/Htl;->n:LX/Hti;

    .line 2516730
    iput-object v1, v0, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->e:LX/Hti;

    .line 2516731
    iget-object v0, p0, LX/Htl;->e:Lcom/facebook/composer/inlinesprouts/InlineSproutsView;

    invoke-virtual {v0}, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, LX/Htk;

    invoke-direct {v1, p0}, LX/Htk;-><init>(LX/Htl;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 2516732
    iput-object p5, p0, LX/Htl;->j:LX/0zw;

    .line 2516733
    iget-object v0, p0, LX/Htl;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2516734
    invoke-direct {p0}, LX/Htl;->e()V

    .line 2516735
    return-void
.end method

.method public static a(LX/Htl;LX/Htp;)V
    .locals 4

    .prologue
    .line 2516704
    iget-object v0, p0, LX/Htl;->c:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2516705
    iget-object v0, p0, LX/Htl;->e:Lcom/facebook/composer/inlinesprouts/InlineSproutsView;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->a(LX/Htp;)V

    .line 2516706
    iget-object v1, p0, LX/Htl;->g:LX/Hth;

    iget-object v0, p0, LX/Htl;->b:LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0j0;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, LX/Htl;->l:Ljava/lang/String;

    .line 2516707
    const-string v3, "composer_tapped_collapsed_inline_sprout_event"

    invoke-static {v3, v0}, LX/Hth;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 2516708
    const-string p1, "available_items"

    invoke-virtual {v3, p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2516709
    iget-object p1, v1, LX/Hth;->a:LX/0Zb;

    invoke-interface {p1, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2516710
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Htl;->k:Z

    .line 2516711
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 2516701
    iget-object v1, p0, LX/Htl;->e:Lcom/facebook/composer/inlinesprouts/InlineSproutsView;

    iget-object v0, p0, LX/Htl;->b:LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->g()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->setExpandedMaxHeight(I)V

    .line 2516702
    invoke-direct {p0}, LX/Htl;->e()V

    .line 2516703
    return-void
.end method

.method private e()V
    .locals 9

    .prologue
    .line 2516605
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2516606
    iget-object v0, p0, LX/Htl;->i:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    iget-object v0, p0, LX/Htl;->i:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hs6;

    .line 2516607
    invoke-virtual {v0}, LX/Hs6;->e()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2516608
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2516609
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2516610
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 2516611
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2516612
    iget-object v0, p0, LX/Htl;->e:Lcom/facebook/composer/inlinesprouts/InlineSproutsView;

    invoke-virtual {v0}, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->a()V

    .line 2516613
    iget-object v0, p0, LX/Htl;->c:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2516614
    :goto_1
    return-void

    .line 2516615
    :cond_2
    iget-object v1, p0, LX/Htl;->h:LX/Hte;

    iget-object v2, p0, LX/Htl;->c:Landroid/widget/TextView;

    const/4 v3, 0x0

    const/4 v8, 0x0

    .line 2516616
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2516617
    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2516618
    invoke-static {v2, v8, v8, v8, v8}, LX/4lM;->a(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2516619
    :cond_3
    :goto_2
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2516620
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2516621
    iget-object v1, p0, LX/Htl;->f:LX/Hts;

    .line 2516622
    iget-object v6, v1, LX/Hts;->d:LX/0Px;

    move-object v7, v6

    .line 2516623
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v6

    if-eq v1, v6, :cond_11

    move v1, v4

    .line 2516624
    :goto_3
    move v1, v1

    .line 2516625
    if-nez v1, :cond_c

    .line 2516626
    :cond_4
    :goto_4
    goto :goto_1

    .line 2516627
    :cond_5
    const/4 v5, 0x0

    .line 2516628
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_6
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_b

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Hs6;

    .line 2516629
    invoke-virtual {v4}, LX/Hs6;->b()Z

    move-result v7

    if-eqz v7, :cond_6

    .line 2516630
    if-nez v5, :cond_7

    move-object v5, v4

    .line 2516631
    :cond_7
    invoke-virtual {v4}, LX/Hs6;->f()Z

    move-result v7

    if-nez v7, :cond_6

    .line 2516632
    :goto_5
    move-object v4, v4

    .line 2516633
    iget-object v5, v1, LX/Hte;->c:LX/Hs6;

    if-eq v4, v5, :cond_8

    .line 2516634
    iput-object v4, v1, LX/Hte;->c:LX/Hs6;

    .line 2516635
    invoke-virtual {v4}, LX/Hs6;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2516636
    :cond_8
    iget-object v4, v1, LX/Hte;->d:LX/0Px;

    if-eq v0, v4, :cond_3

    .line 2516637
    iput-object v0, v1, LX/Hte;->d:LX/0Px;

    .line 2516638
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    move v5, v3

    move v4, v3

    .line 2516639
    :goto_6
    const/4 v3, 0x4

    if-ge v4, v3, :cond_9

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    if-ge v5, v3, :cond_9

    .line 2516640
    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Hs6;

    invoke-virtual {v3}, LX/Hs6;->a()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 2516641
    iget-object v7, v1, LX/Hte;->b:LX/Htv;

    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Hs6;

    invoke-virtual {v3}, LX/Hs6;->d()LX/Hu0;

    move-result-object v3

    invoke-virtual {v7, v3}, LX/Htv;->a(LX/Hu0;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v6, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2516642
    add-int/lit8 v3, v4, 0x1

    .line 2516643
    :goto_7
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    move v4, v3

    goto :goto_6

    .line 2516644
    :cond_9
    new-instance v3, LX/ASa;

    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    iget-object v5, v1, LX/Hte;->a:Landroid/content/res/Resources;

    const v6, 0x7f0b0063

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-direct {v3, v4, v5}, LX/ASa;-><init>(LX/0Px;I)V

    invoke-static {v2, v8, v8, v3, v8}, LX/4lM;->a(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_2

    :cond_a
    move v3, v4

    goto :goto_7

    :cond_b
    move-object v4, v5

    goto :goto_5

    .line 2516645
    :cond_c
    new-instance v5, Ljava/util/ArrayList;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    invoke-direct {v5, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 2516646
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v6

    move v4, v3

    :goto_8
    if-ge v4, v6, :cond_d

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Hs6;

    .line 2516647
    invoke-virtual {v1}, LX/Hs6;->d()LX/Hu0;

    move-result-object v1

    .line 2516648
    iget-object v7, v1, LX/Hu0;->d:Ljava/lang/String;

    move-object v1, v7

    .line 2516649
    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2516650
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_8

    .line 2516651
    :cond_d
    invoke-static {v5}, LX/16N;->b(Ljava/util/List;)LX/162;

    move-result-object v1

    invoke-virtual {v1}, LX/162;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/Htl;->l:Ljava/lang/String;

    .line 2516652
    iget-object v1, p0, LX/Htl;->f:LX/Hts;

    .line 2516653
    iput-object v0, v1, LX/Hts;->d:LX/0Px;

    .line 2516654
    const v4, -0x2c855473

    invoke-static {v1, v4}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2516655
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    if-le v1, v2, :cond_f

    .line 2516656
    :goto_9
    if-eqz v2, :cond_10

    .line 2516657
    iget-object v1, p0, LX/Htl;->d:LX/0zw;

    invoke-virtual {v1}, LX/0zw;->c()V

    .line 2516658
    :cond_e
    :goto_a
    invoke-static {p0}, LX/Htl;->h(LX/Htl;)Z

    move-result v1

    if-eq v1, v2, :cond_4

    .line 2516659
    iget-object v1, p0, LX/Htl;->b:LX/0il;

    check-cast v1, LX/0im;

    invoke-interface {v1}, LX/0im;->c()LX/0jJ;

    move-result-object v1

    sget-object v3, LX/Htl;->a:LX/0jK;

    invoke-virtual {v1, v3}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v1

    check-cast v1, LX/0jL;

    invoke-static {p0}, LX/Htl;->f(LX/Htl;)Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState$Builder;->setIsCollapsible(Z)Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState$Builder;->a()Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0jL;->a(Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jL;

    invoke-virtual {v1}, LX/0jL;->a()V

    goto/16 :goto_4

    :cond_f
    move v2, v3

    .line 2516660
    goto :goto_9

    .line 2516661
    :cond_10
    iget-object v1, p0, LX/Htl;->d:LX/0zw;

    invoke-virtual {v1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v1

    .line 2516662
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-eqz v3, :cond_e

    .line 2516663
    new-instance v3, Lcom/facebook/composer/inlinesprouts/InlineSproutsController$4;

    invoke-direct {v3, p0, v1}, Lcom/facebook/composer/inlinesprouts/InlineSproutsController$4;-><init>(LX/Htl;Landroid/view/View;)V

    invoke-virtual {v1, v3}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    goto :goto_a

    .line 2516664
    :cond_11
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v8

    move v6, v5

    :goto_b
    if-ge v6, v8, :cond_13

    invoke-virtual {v0, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Hs6;

    .line 2516665
    invoke-virtual {v7, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_12

    move v1, v4

    .line 2516666
    goto/16 :goto_3

    .line 2516667
    :cond_12
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto :goto_b

    :cond_13
    move v1, v5

    .line 2516668
    goto/16 :goto_3
.end method

.method public static f(LX/Htl;)Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState$Builder;
    .locals 1

    .prologue
    .line 2516737
    iget-object v0, p0, LX/Htl;->b:LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->v()Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;->a(Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;)Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static h(LX/Htl;)Z
    .locals 1

    .prologue
    .line 2516700
    iget-object v0, p0, LX/Htl;->b:LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->v()Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;->isCollapsible()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2516691
    invoke-static {p0}, LX/Htl;->h(LX/Htl;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2516692
    :goto_0
    return-void

    .line 2516693
    :cond_0
    iput-boolean v2, p0, LX/Htl;->k:Z

    .line 2516694
    iget-object v0, p0, LX/Htl;->c:Landroid/widget/TextView;

    new-instance v1, Lcom/facebook/composer/inlinesprouts/InlineSproutsController$3;

    invoke-direct {v1, p0}, Lcom/facebook/composer/inlinesprouts/InlineSproutsController$3;-><init>(LX/Htl;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->post(Ljava/lang/Runnable;)Z

    .line 2516695
    iget-object v0, p0, LX/Htl;->e:Lcom/facebook/composer/inlinesprouts/InlineSproutsView;

    invoke-virtual {v0}, Lcom/facebook/composer/inlinesprouts/InlineSproutsView;->a()V

    .line 2516696
    iget-object v1, p0, LX/Htl;->g:LX/Hth;

    iget-object v0, p0, LX/Htl;->b:LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0j0;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    .line 2516697
    const-string v3, "composer_collapse_inline_sprout_event"

    invoke-static {v3, v0}, LX/Hth;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 2516698
    iget-object v4, v1, LX/Hth;->a:LX/0Zb;

    invoke-interface {v4, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2516699
    iget-object v0, p0, LX/Htl;->b:LX/0il;

    check-cast v0, LX/0im;

    invoke-interface {v0}, LX/0im;->c()LX/0jJ;

    move-result-object v0

    sget-object v1, LX/Htl;->a:LX/0jK;

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-static {p0}, LX/Htl;->f(LX/Htl;)Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState$Builder;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState$Builder;->setIsInlineSproutsOpen(Z)Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState$Builder;->a()Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0jL;->a(Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    goto :goto_0
.end method

.method public final a(LX/5L2;)V
    .locals 1

    .prologue
    .line 2516681
    sget-object v0, LX/5L2;->ON_INLINE_SPROUTS_STATE_CHANGE:LX/5L2;

    if-ne p1, v0, :cond_1

    .line 2516682
    invoke-direct {p0}, LX/Htl;->d()V

    .line 2516683
    :cond_0
    :goto_0
    return-void

    .line 2516684
    :cond_1
    sget-object v0, LX/5L2;->ON_KEYBOARD_STATE_CHANGED:LX/5L2;

    if-ne p1, v0, :cond_3

    .line 2516685
    iget-object v0, p0, LX/Htl;->b:LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2516686
    invoke-virtual {p0}, LX/Htl;->a()V

    goto :goto_0

    .line 2516687
    :cond_2
    iget-boolean v0, p0, LX/Htl;->k:Z

    if-eqz v0, :cond_0

    .line 2516688
    iget-object v0, p0, LX/Htl;->m:LX/Htp;

    invoke-static {p0, v0}, LX/Htl;->a(LX/Htl;LX/Htp;)V

    goto :goto_0

    .line 2516689
    :cond_3
    sget-object v0, LX/5L2;->ON_SCROLL_CHANGED:LX/5L2;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, LX/Htl;->b:LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->v()Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/composer/inlinesprouts/model/InlineSproutsState;->isInlineSproutsOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2516690
    invoke-virtual {p0}, LX/Htl;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2516679
    invoke-direct {p0}, LX/Htl;->d()V

    .line 2516680
    return-void
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 2516669
    iget-object v0, p0, LX/Htl;->f:LX/Hts;

    invoke-virtual {v0, p3}, LX/Hts;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hs6;

    .line 2516670
    invoke-virtual {v0}, LX/Hs6;->d()LX/Hu0;

    move-result-object v1

    .line 2516671
    iget-object v2, p0, LX/Htl;->g:LX/Hth;

    iget-object v0, p0, LX/Htl;->b:LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0j0;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    .line 2516672
    iget-object v3, v1, LX/Hu0;->d:Ljava/lang/String;

    move-object v3, v3

    .line 2516673
    const-string p0, "composer_opened_inline_sprout_event"

    invoke-static {p0, v0}, LX/Hth;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    .line 2516674
    const-string p1, "composer_opened_inline_sprout_data"

    invoke-virtual {p0, p1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2516675
    iget-object p1, v2, LX/Hth;->a:LX/0Zb;

    invoke-interface {p1, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2516676
    iget-object v0, v1, LX/Hu0;->e:LX/Hr2;

    move-object v0, v0

    .line 2516677
    invoke-interface {v0}, LX/Hr2;->onClick()V

    .line 2516678
    return-void
.end method
