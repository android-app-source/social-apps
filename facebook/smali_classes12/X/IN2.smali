.class public LX/IN2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DS5;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:I

.field private final c:I

.field private final d:LX/03V;

.field public final e:LX/IM6;

.field public final f:LX/0if;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2570466
    const-class v0, LX/IN2;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/IN2;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/IM6;LX/03V;LX/0if;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2570467
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2570468
    iput-object p3, p0, LX/IN2;->d:LX/03V;

    .line 2570469
    iput-object p2, p0, LX/IN2;->e:LX/IM6;

    .line 2570470
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b200c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/IN2;->b:I

    .line 2570471
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00d5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, LX/IN2;->c:I

    .line 2570472
    iput-object p4, p0, LX/IN2;->f:LX/0if;

    .line 2570473
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2570474
    iget v0, p0, LX/IN2;->b:I

    return v0
.end method

.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2570475
    sget-object v0, LX/IN1;->a:[I

    invoke-static {p1}, LX/IN8;->fromOrdinal(I)LX/IN8;

    move-result-object v1

    invoke-virtual {v1}, LX/IN8;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2570476
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown view type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2570477
    :pswitch_0
    new-instance v0, LX/IO3;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/IO3;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x1

    .line 2570478
    iput-boolean v1, v0, LX/IO3;->o:Z

    .line 2570479
    move-object v0, v0

    .line 2570480
    iget-object v1, v0, LX/IO3;->i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-virtual {v0}, LX/IO3;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p0, 0x7f0b008a

    invoke-virtual {v2, p0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2570481
    iget-object v1, v0, LX/IO3;->i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-virtual {v0}, LX/IO3;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p0, 0x7f0b008a

    invoke-virtual {v2, p0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2570482
    iget-object v1, v0, LX/IO3;->k:Lcom/facebook/fbui/facepile/FacepileView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/facepile/FacepileView;->setVisibility(I)V

    .line 2570483
    invoke-virtual {v0}, LX/IO3;->invalidate()V

    .line 2570484
    move-object v0, v0

    .line 2570485
    :goto_0
    return-object v0

    .line 2570486
    :pswitch_1
    new-instance v0, LX/IO4;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/IO4;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 2570487
    :pswitch_2
    new-instance v0, Lcom/facebook/fig/button/FigButton;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/fig/button/FigButton;-><init>(Landroid/content/Context;)V

    .line 2570488
    const/16 v1, 0x28

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setType(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Ljava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2570489
    invoke-static {p3}, LX/IN8;->fromOrdinal(I)LX/IN8;

    move-result-object v0

    .line 2570490
    sget-object v1, LX/IN8;->Header:LX/IN8;

    if-ne v0, v1, :cond_0

    instance-of v1, p2, LX/IO4;

    if-eqz v1, :cond_0

    .line 2570491
    check-cast p2, LX/IO4;

    check-cast p1, LX/IN7;

    invoke-virtual {p4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2570492
    iget v1, p1, LX/IN7;->a:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2570493
    iget-object v1, p2, LX/IO4;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2570494
    :goto_0
    return-void

    .line 2570495
    :cond_0
    sget-object v1, LX/IN8;->CreateGroupButton:LX/IN8;

    if-ne v0, v1, :cond_1

    instance-of v1, p2, Lcom/facebook/fig/button/FigButton;

    if-eqz v1, :cond_1

    move-object v0, p1

    .line 2570496
    check-cast v0, LX/IN6;

    .line 2570497
    iget-object v1, v0, LX/IN6;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2570498
    move-object v0, p2

    .line 2570499
    check-cast v0, Lcom/facebook/fig/button/FigButton;

    invoke-virtual {p4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f083004

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setText(Ljava/lang/CharSequence;)V

    .line 2570500
    check-cast p1, LX/IN6;

    .line 2570501
    iget-object v0, p1, LX/IN6;->b:Landroid/view/View$OnClickListener;

    move-object v0, v0

    .line 2570502
    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 2570503
    :cond_1
    sget-object v1, LX/IN8;->GroupRow:LX/IN8;

    if-ne v0, v1, :cond_2

    instance-of v1, p2, LX/IO3;

    if-eqz v1, :cond_2

    .line 2570504
    check-cast p2, LX/IO3;

    .line 2570505
    check-cast p1, LX/INB;

    .line 2570506
    iget-object v0, p1, LX/INB;->mModel:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;

    move-object v0, v0

    .line 2570507
    invoke-static {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;

    move-result-object v0

    const-string v1, "community_subgroups_search"

    new-instance v2, LX/IN0;

    invoke-direct {v2, p0, p1}, LX/IN0;-><init>(LX/IN2;LX/INB;)V

    invoke-virtual {p2, v0, v1, v5, v2}, LX/IO3;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;Ljava/lang/String;ZLX/IN0;)V

    goto :goto_0

    .line 2570508
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "bindView failed for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2570509
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " with item type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, LX/IN8;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2570510
    iget-object v0, p0, LX/IN2;->d:LX/03V;

    sget-object v2, LX/IN2;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2570511
    iget v0, p0, LX/IN2;->c:I

    return v0
.end method
