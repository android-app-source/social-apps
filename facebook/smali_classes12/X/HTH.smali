.class public final LX/HTH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/pages/fb4a/offers/PageOffersGraphQLModels$PageOffersQueryModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;)V
    .locals 0

    .prologue
    .line 2469263
    iput-object p1, p0, LX/HTH;->a:Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2469264
    new-instance v0, LX/HT2;

    invoke-direct {v0}, LX/HT2;-><init>()V

    move-object v0, v0

    .line 2469265
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2469266
    new-instance v1, LX/HT2;

    invoke-direct {v1}, LX/HT2;-><init>()V

    const-string v2, "page_id"

    iget-object v3, p0, LX/HTH;->a:Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;

    iget-object v3, v3, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->l:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "count"

    const-string v3, "5"

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "end_cursor"

    iget-object v3, p0, LX/HTH;->a:Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;

    iget-object v3, v3, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->m:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    .line 2469267
    iget-object v2, v1, LX/0gW;->e:LX/0w7;

    move-object v1, v2

    .line 2469268
    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0w7;)LX/0zO;

    .line 2469269
    iget-object v1, p0, LX/HTH;->a:Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;

    iget-object v1, v1, Lcom/facebook/pages/fb4a/offers/PageSurfaceOffersListFragment;->c:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0
.end method
