.class public LX/HIX;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/3U8;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kp;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/pages/common/reaction/components/HScrollPageCardFooterComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/HIX",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/pages/common/reaction/components/HScrollPageCardFooterComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2451254
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2451255
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/HIX;->b:LX/0Zi;

    .line 2451256
    iput-object p1, p0, LX/HIX;->a:LX/0Ot;

    .line 2451257
    return-void
.end method

.method public static a(LX/0QB;)LX/HIX;
    .locals 4

    .prologue
    .line 2451258
    const-class v1, LX/HIX;

    monitor-enter v1

    .line 2451259
    :try_start_0
    sget-object v0, LX/HIX;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2451260
    sput-object v2, LX/HIX;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2451261
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2451262
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2451263
    new-instance v3, LX/HIX;

    const/16 p0, 0x2b93

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/HIX;-><init>(LX/0Ot;)V

    .line 2451264
    move-object v0, v3

    .line 2451265
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2451266
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/HIX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2451267
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2451268
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private c(LX/1X1;)V
    .locals 8

    .prologue
    .line 2451269
    check-cast p1, LX/HIW;

    .line 2451270
    iget-object v0, p0, LX/HIX;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/reaction/components/HScrollPageCardFooterComponentSpec;

    iget-object v1, p1, LX/HIW;->a:LX/HIR;

    iget-object v2, p1, LX/HIW;->d:LX/HIS;

    .line 2451271
    if-eqz v2, :cond_0

    .line 2451272
    invoke-interface {v2, v1}, LX/HIS;->a(LX/HIR;)LX/CY7;

    move-result-object v3

    .line 2451273
    :goto_0
    if-nez v3, :cond_1

    .line 2451274
    :goto_1
    return-void

    .line 2451275
    :cond_0
    new-instance v3, LX/CY6;

    invoke-direct {v3}, LX/CY6;-><init>()V

    iget-object v4, v1, LX/HIR;->a:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    .line 2451276
    iput-wide v5, v3, LX/CY6;->a:J

    .line 2451277
    move-object v3, v3

    .line 2451278
    iget-object v4, v1, LX/HIR;->f:Ljava/lang/String;

    .line 2451279
    iput-object v4, v3, LX/CY6;->b:Ljava/lang/String;

    .line 2451280
    move-object v3, v3

    .line 2451281
    iget-object v4, v1, LX/HIR;->b:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    .line 2451282
    iput-object v4, v3, LX/CY6;->d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    .line 2451283
    move-object v3, v3

    .line 2451284
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    .line 2451285
    iput-object v4, v3, LX/CY6;->e:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    .line 2451286
    move-object v3, v3

    .line 2451287
    invoke-virtual {v3}, LX/CY6;->a()LX/CY7;

    move-result-object v3

    goto :goto_0

    .line 2451288
    :cond_1
    if-eqz v2, :cond_2

    .line 2451289
    invoke-interface {v2, v1, v3}, LX/HIS;->a(LX/HIR;LX/CY7;)V

    .line 2451290
    :cond_2
    iget-object v4, v0, Lcom/facebook/pages/common/reaction/components/HScrollPageCardFooterComponentSpec;->g:LX/CYK;

    invoke-virtual {v4, v3}, LX/CYK;->c(LX/CY7;)V

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 7

    .prologue
    .line 2451291
    check-cast p2, LX/HIW;

    .line 2451292
    iget-object v0, p0, LX/HIX;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/reaction/components/HScrollPageCardFooterComponentSpec;

    iget-object v1, p2, LX/HIW;->a:LX/HIR;

    iget-object v2, p2, LX/HIW;->b:LX/HIU;

    .line 2451293
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x2

    invoke-interface {v3, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    const/16 v4, 0x8

    const v5, 0x7f0b2329

    invoke-interface {v3, v4, v5}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    .line 2451294
    const v5, -0x15082128

    const/4 v6, 0x0

    invoke-static {p1, v5, v6}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v5, v5

    .line 2451295
    invoke-interface {v4, v5}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-interface {v4, v5}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v4

    iget-object v5, v1, LX/HIR;->f:Ljava/lang/String;

    const/4 p2, 0x1

    .line 2451296
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v6

    sget-object p0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v6, p0}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, p2}, LX/1ne;->j(I)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v5}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v6

    const p0, 0x7f0b0050

    invoke-virtual {v6, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, p2}, LX/1ne;->t(I)LX/1ne;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    move-object v5, v6

    .line 2451297
    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    iget-object v5, v1, LX/HIR;->g:Ljava/lang/String;

    iget v6, v1, LX/HIR;->e:I

    invoke-static {v0, p1, v5, v6}, Lcom/facebook/pages/common/reaction/components/HScrollPageCardFooterComponentSpec;->a(Lcom/facebook/pages/common/reaction/components/HScrollPageCardFooterComponentSpec;LX/1De;Ljava/lang/String;I)LX/1Di;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    .line 2451298
    iget-object v3, v1, LX/HIR;->b:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    if-nez v3, :cond_0

    iget-boolean v3, v1, LX/HIR;->c:Z

    if-eqz v3, :cond_2

    if-eqz v2, :cond_2

    :cond_0
    const/4 v3, 0x1

    :goto_0
    move v3, v3

    .line 2451299
    if-eqz v3, :cond_1

    .line 2451300
    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v3

    const v5, 0x7f0a015a

    invoke-virtual {v3, v5}, LX/25Q;->i(I)LX/25Q;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/4 v5, 0x4

    invoke-interface {v3, v5}, LX/1Di;->b(I)LX/1Di;

    move-result-object v3

    const/4 v5, 0x6

    const v6, 0x7f0b0918

    invoke-interface {v3, v5, v6}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    const v5, 0x7f0b0033

    invoke-interface {v3, v5}, LX/1Di;->i(I)LX/1Di;

    move-result-object v3

    move-object v3, v3

    .line 2451301
    :goto_1
    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    const/16 p0, 0x8

    const/4 v6, 0x2

    .line 2451302
    iget-object v4, v1, LX/HIR;->b:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    if-eqz v4, :cond_3

    .line 2451303
    iget-object v4, v0, Lcom/facebook/pages/common/reaction/components/HScrollPageCardFooterComponentSpec;->c:LX/1nu;

    invoke-virtual {v4, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v4

    sget-object v5, Lcom/facebook/pages/common/reaction/components/HScrollPageCardFooterComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v4, v5}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v4

    iget-object v5, v1, LX/HIR;->b:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    invoke-virtual {v5}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->l()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel;->a()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel$IconImageModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$IconInfoModel$IconImageModel;->a()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    invoke-interface {v4, v6}, LX/1Di;->b(I)LX/1Di;

    move-result-object v4

    .line 2451304
    const v5, -0x169bd3a5

    const/4 v6, 0x0

    invoke-static {p1, v5, v6}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v5, v5

    .line 2451305
    invoke-interface {v4, v5}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v4

    const v5, 0x7f0b232b

    invoke-interface {v4, v5}, LX/1Di;->q(I)LX/1Di;

    move-result-object v4

    const v5, 0x7f0b163c

    invoke-interface {v4, p0, v5}, LX/1Di;->g(II)LX/1Di;

    move-result-object v4

    .line 2451306
    :goto_2
    move-object v4, v4

    .line 2451307
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2451308
    return-object v0

    :cond_1
    const/4 v3, 0x0

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 2451309
    :cond_3
    iget-boolean v4, v1, LX/HIR;->c:Z

    if-eqz v4, :cond_6

    if-eqz v2, :cond_6

    .line 2451310
    iget-object v4, v0, Lcom/facebook/pages/common/reaction/components/HScrollPageCardFooterComponentSpec;->e:LX/1vg;

    invoke-virtual {v4, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v5

    iget-boolean v4, v1, LX/HIR;->d:Z

    if-eqz v4, :cond_4

    const v4, 0x7f0a00d2

    :goto_3
    invoke-virtual {v5, v4}, LX/2xv;->j(I)LX/2xv;

    move-result-object v5

    iget-boolean v4, v1, LX/HIR;->d:Z

    if-eqz v4, :cond_5

    const v4, 0x7f021886

    :goto_4
    invoke-virtual {v5, v4}, LX/2xv;->h(I)LX/2xv;

    move-result-object v4

    invoke-virtual {v4}, LX/1n6;->b()LX/1dc;

    move-result-object v4

    .line 2451311
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    invoke-interface {v4, v6}, LX/1Di;->b(I)LX/1Di;

    move-result-object v4

    .line 2451312
    const v5, 0xfc63dac

    const/4 v6, 0x0

    invoke-static {p1, v5, v6}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v5, v5

    .line 2451313
    invoke-interface {v4, v5}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v4

    const v5, 0x7f0b232b

    invoke-interface {v4, v5}, LX/1Di;->q(I)LX/1Di;

    move-result-object v4

    const v5, 0x7f0b163c

    invoke-interface {v4, p0, v5}, LX/1Di;->g(II)LX/1Di;

    move-result-object v4

    goto :goto_2

    .line 2451314
    :cond_4
    const v4, 0x7f0a010e

    goto :goto_3

    :cond_5
    const v4, 0x7f021885

    goto :goto_4

    .line 2451315
    :cond_6
    const/4 v4, 0x0

    goto :goto_2
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2451316
    invoke-static {}, LX/1dS;->b()V

    .line 2451317
    iget v0, p1, LX/1dQ;->b:I

    .line 2451318
    sparse-switch v0, :sswitch_data_0

    .line 2451319
    :goto_0
    return-object v1

    .line 2451320
    :sswitch_0
    iget-object v0, p1, LX/1dQ;->a:LX/1X1;

    .line 2451321
    check-cast v0, LX/HIW;

    .line 2451322
    iget-object v2, p0, LX/HIX;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/pages/common/reaction/components/HScrollPageCardFooterComponentSpec;

    iget-object v3, v0, LX/HIW;->a:LX/HIR;

    iget-object v4, v0, LX/HIW;->c:LX/HIT;

    .line 2451323
    if-eqz v4, :cond_0

    .line 2451324
    invoke-interface {v4, v3}, LX/HIT;->a(LX/HIR;)V

    .line 2451325
    :cond_0
    iget-object p1, v2, Lcom/facebook/pages/common/reaction/components/HScrollPageCardFooterComponentSpec;->d:LX/17W;

    iget-object p2, v2, Lcom/facebook/pages/common/reaction/components/HScrollPageCardFooterComponentSpec;->b:Landroid/content/Context;

    sget-object p0, LX/0ax;->aE:Ljava/lang/String;

    iget-object v0, v3, LX/HIR;->a:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p2, p0}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2451326
    goto :goto_0

    .line 2451327
    :sswitch_1
    iget-object v0, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0}, LX/HIX;->c(LX/1X1;)V

    goto :goto_0

    .line 2451328
    :sswitch_2
    iget-object v0, p1, LX/1dQ;->a:LX/1X1;

    .line 2451329
    check-cast v0, LX/HIW;

    .line 2451330
    iget-object v2, p0, LX/HIX;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v2, v0, LX/HIW;->a:LX/HIR;

    iget-object v3, v0, LX/HIW;->b:LX/HIU;

    .line 2451331
    if-eqz v3, :cond_1

    .line 2451332
    invoke-interface {v3, v2}, LX/HIU;->a(LX/HIR;)V

    .line 2451333
    :cond_1
    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x169bd3a5 -> :sswitch_1
        -0x15082128 -> :sswitch_0
        0xfc63dac -> :sswitch_2
    .end sparse-switch
.end method
