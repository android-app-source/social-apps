.class public LX/Huc;
.super LX/AQ9;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginDataProvider;",
        "DerivedData::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginDerivedDataProvider;",
        "Mutation::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginMutation",
        "<TMutation;>;>",
        "LX/AQ9",
        "<TModelData;TDerivedData;TMutation;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/B5j;)V
    .locals 0
    .param p2    # LX/B5j;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/B5j",
            "<TModelData;TDerivedData;TMutation;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2517768
    invoke-direct {p0, p1, p2}, LX/AQ9;-><init>(Landroid/content/Context;LX/B5j;)V

    .line 2517769
    return-void
.end method


# virtual methods
.method public final V()LX/ARN;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2517765
    sget-object v0, LX/Hua;->a:[I

    invoke-virtual {p0}, LX/AQ9;->R()LX/B5j;

    move-result-object v1

    invoke-virtual {v1}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getComposerType()LX/2rt;

    move-result-object v1

    invoke-virtual {v1}, LX/2rt;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2517766
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2517767
    :pswitch_0
    sget-object v0, LX/ARN;->b:LX/ARN;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final X()LX/ARN;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2517791
    sget-object v0, LX/Hua;->a:[I

    invoke-virtual {p0}, LX/AQ9;->R()LX/B5j;

    move-result-object v1

    invoke-virtual {v1}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getComposerType()LX/2rt;

    move-result-object v1

    invoke-virtual {v1}, LX/2rt;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2517792
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2517793
    :pswitch_0
    sget-object v0, LX/ARN;->b:LX/ARN;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a()LX/B5f;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2517797
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/B5f;->a(Ljava/lang/String;)LX/B5f;

    move-result-object v0

    return-object v0
.end method

.method public final aG()LX/ARN;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2517794
    sget-object v0, LX/Hua;->a:[I

    invoke-virtual {p0}, LX/AQ9;->R()LX/B5j;

    move-result-object v1

    invoke-virtual {v1}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getComposerType()LX/2rt;

    move-result-object v1

    invoke-virtual {v1}, LX/2rt;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2517795
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2517796
    :pswitch_0
    sget-object v0, LX/ARN;->b:LX/ARN;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final aI()LX/ARN;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2517785
    sget-object v0, LX/Hua;->a:[I

    invoke-virtual {p0}, LX/AQ9;->R()LX/B5j;

    move-result-object v1

    invoke-virtual {v1}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getComposerType()LX/2rt;

    move-result-object v1

    invoke-virtual {v1}, LX/2rt;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2517786
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2517787
    :pswitch_0
    sget-object v0, LX/ARN;->a:LX/ARN;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final aa()LX/ARN;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2517788
    sget-object v0, LX/Hua;->a:[I

    invoke-virtual {p0}, LX/AQ9;->R()LX/B5j;

    move-result-object v1

    invoke-virtual {v1}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getComposerType()LX/2rt;

    move-result-object v1

    invoke-virtual {v1}, LX/2rt;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2517789
    sget-object v0, LX/ARN;->a:LX/ARN;

    :goto_0
    return-object v0

    .line 2517790
    :pswitch_0
    sget-object v0, LX/ARN;->b:LX/ARN;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final ac()LX/ARN;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2517782
    sget-object v0, LX/Hua;->a:[I

    invoke-virtual {p0}, LX/AQ9;->R()LX/B5j;

    move-result-object v1

    invoke-virtual {v1}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getComposerType()LX/2rt;

    move-result-object v1

    invoke-virtual {v1}, LX/2rt;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2517783
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2517784
    :pswitch_0
    sget-object v0, LX/ARN;->a:LX/ARN;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final ae()LX/ARN;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2517779
    sget-object v0, LX/Hua;->a:[I

    invoke-virtual {p0}, LX/AQ9;->R()LX/B5j;

    move-result-object v1

    invoke-virtual {v1}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getComposerType()LX/2rt;

    move-result-object v1

    invoke-virtual {v1}, LX/2rt;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2517780
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2517781
    :pswitch_0
    sget-object v0, LX/ARN;->b:LX/ARN;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final ag()LX/ARN;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2517776
    sget-object v0, LX/Hua;->a:[I

    invoke-virtual {p0}, LX/AQ9;->R()LX/B5j;

    move-result-object v1

    invoke-virtual {v1}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getComposerType()LX/2rt;

    move-result-object v1

    invoke-virtual {v1}, LX/2rt;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2517777
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2517778
    :pswitch_0
    sget-object v0, LX/ARN;->b:LX/ARN;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final al()LX/ARN;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2517773
    sget-object v0, LX/Hua;->a:[I

    invoke-virtual {p0}, LX/AQ9;->R()LX/B5j;

    move-result-object v1

    invoke-virtual {v1}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getComposerType()LX/2rt;

    move-result-object v1

    invoke-virtual {v1}, LX/2rt;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2517774
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2517775
    :pswitch_0
    sget-object v0, LX/ARN;->a:LX/ARN;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final au()LX/ARN;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2517770
    sget-object v0, LX/Hua;->a:[I

    invoke-virtual {p0}, LX/AQ9;->R()LX/B5j;

    move-result-object v1

    invoke-virtual {v1}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-interface {v1}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getComposerType()LX/2rt;

    move-result-object v1

    invoke-virtual {v1}, LX/2rt;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2517771
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2517772
    :pswitch_0
    sget-object v0, LX/ARN;->a:LX/ARN;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
