.class public LX/IsC;
.super LX/Iqg;
.source ""


# instance fields
.field public final a:LX/Dhh;

.field public final b:Lcom/facebook/messaging/font/FontAsset;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public final c:Z

.field public final d:Z

.field public final e:Z

.field public final f:LX/Dhi;

.field public g:Ljava/lang/CharSequence;

.field public h:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field public i:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2619359
    invoke-direct {p0}, LX/Iqg;-><init>()V

    .line 2619360
    const/4 v0, -0x1

    iput v0, p0, LX/IsC;->h:I

    .line 2619361
    iput v1, p0, LX/IsC;->i:I

    .line 2619362
    sget-object v0, LX/Dhh;->CLEAR:LX/Dhh;

    iput-object v0, p0, LX/IsC;->a:LX/Dhh;

    .line 2619363
    const/4 v0, 0x0

    iput-object v0, p0, LX/IsC;->b:Lcom/facebook/messaging/font/FontAsset;

    .line 2619364
    iput-boolean v1, p0, LX/IsC;->c:Z

    .line 2619365
    iput-boolean v1, p0, LX/IsC;->d:Z

    .line 2619366
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/IsC;->e:Z

    .line 2619367
    sget-object v0, LX/Dhi;->REGULAR:LX/Dhi;

    iput-object v0, p0, LX/IsC;->f:LX/Dhi;

    .line 2619368
    return-void
.end method

.method public constructor <init>(Lcom/facebook/messaging/montage/model/art/ArtItem;FZFLX/Iqo;LX/Iqo;Ljava/lang/CharSequence;IILX/Dhh;Lcom/facebook/messaging/font/FontAsset;ZZZLX/Dhi;)V
    .locals 2
    .param p1    # Lcom/facebook/messaging/montage/model/art/ArtItem;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p11    # Lcom/facebook/messaging/font/FontAsset;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2619369
    invoke-direct/range {p0 .. p6}, LX/Iqg;-><init>(Lcom/facebook/messaging/montage/model/art/ArtItem;FZFLX/Iqo;LX/Iqo;)V

    .line 2619370
    const/4 v1, -0x1

    iput v1, p0, LX/IsC;->h:I

    .line 2619371
    const/4 v1, 0x0

    iput v1, p0, LX/IsC;->i:I

    .line 2619372
    iput-object p7, p0, LX/IsC;->g:Ljava/lang/CharSequence;

    .line 2619373
    iput p8, p0, LX/IsC;->h:I

    .line 2619374
    iput p9, p0, LX/IsC;->i:I

    .line 2619375
    iput-object p10, p0, LX/IsC;->a:LX/Dhh;

    .line 2619376
    iput-object p11, p0, LX/IsC;->b:Lcom/facebook/messaging/font/FontAsset;

    .line 2619377
    iput-boolean p12, p0, LX/IsC;->c:Z

    .line 2619378
    iput-boolean p13, p0, LX/IsC;->d:Z

    .line 2619379
    move/from16 v0, p14

    iput-boolean v0, p0, LX/IsC;->e:Z

    .line 2619380
    move-object/from16 v0, p15

    iput-object v0, p0, LX/IsC;->f:LX/Dhi;

    .line 2619381
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 2619382
    iget-object v0, p0, LX/IsC;->f:LX/Dhi;

    sget-object v1, LX/Dhi;->USER_PROMPT:LX/Dhi;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, LX/IsC;->g:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/IsC;->g:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
