.class public LX/Irf;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2618515
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2618516
    return-void
.end method

.method public static a(LX/Iqo;I)I
    .locals 2

    .prologue
    .line 2618517
    iget v0, p0, LX/Iqo;->e:F

    move v0, v0

    .line 2618518
    int-to-float v1, p1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public static a(LX/Iqo;III)I
    .locals 3

    .prologue
    .line 2618519
    iget-object v0, p0, LX/Iqo;->a:LX/DhY;

    move-object v0, v0

    .line 2618520
    if-nez v0, :cond_0

    .line 2618521
    const/4 v0, 0x0

    .line 2618522
    :goto_0
    return v0

    .line 2618523
    :cond_0
    sget-object v0, LX/Ire;->a:[I

    .line 2618524
    iget-object v1, p0, LX/Iqo;->a:LX/DhY;

    move-object v1, v1

    .line 2618525
    invoke-virtual {v1}, LX/DhY;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2618526
    :pswitch_0
    sub-int v0, p3, p1

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    .line 2618527
    iget v1, p0, LX/Iqo;->c:F

    move v1, v1

    .line 2618528
    int-to-float v2, p3

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    .line 2618529
    :goto_1
    sub-int v1, p3, p1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    .line 2618530
    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    goto :goto_0

    .line 2618531
    :pswitch_1
    iget v0, p0, LX/Iqo;->c:F

    move v0, v0

    .line 2618532
    int-to-float v1, p3

    mul-float/2addr v0, v1

    .line 2618533
    sub-int v1, p2, p1

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    .line 2618534
    goto :goto_1

    .line 2618535
    :pswitch_2
    const/high16 v0, 0x3f800000    # 1.0f

    .line 2618536
    iget v1, p0, LX/Iqo;->c:F

    move v1, v1

    .line 2618537
    sub-float/2addr v0, v1

    int-to-float v1, p3

    mul-float/2addr v0, v1

    int-to-float v1, p1

    sub-float/2addr v0, v1

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static b(LX/Iqo;I)I
    .locals 2

    .prologue
    .line 2618538
    iget v0, p0, LX/Iqo;->f:F

    move v0, v0

    .line 2618539
    int-to-float v1, p1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public static b(LX/Iqo;III)I
    .locals 3

    .prologue
    .line 2618540
    iget-object v0, p0, LX/Iqo;->b:LX/DhZ;

    move-object v0, v0

    .line 2618541
    if-nez v0, :cond_0

    .line 2618542
    const/4 v0, 0x0

    .line 2618543
    :goto_0
    return v0

    .line 2618544
    :cond_0
    sget-object v0, LX/Ire;->b:[I

    .line 2618545
    iget-object v1, p0, LX/Iqo;->b:LX/DhZ;

    move-object v1, v1

    .line 2618546
    invoke-virtual {v1}, LX/DhZ;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2618547
    :pswitch_0
    sub-int v0, p3, p1

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    .line 2618548
    iget v1, p0, LX/Iqo;->d:F

    move v1, v1

    .line 2618549
    int-to-float v2, p3

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    .line 2618550
    :goto_1
    sub-int v1, p3, p1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    .line 2618551
    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    goto :goto_0

    .line 2618552
    :pswitch_1
    iget v0, p0, LX/Iqo;->d:F

    move v0, v0

    .line 2618553
    int-to-float v1, p3

    mul-float/2addr v0, v1

    .line 2618554
    sub-int v1, p2, p1

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    .line 2618555
    goto :goto_1

    .line 2618556
    :pswitch_2
    const/high16 v0, 0x3f800000    # 1.0f

    .line 2618557
    iget v1, p0, LX/Iqo;->d:F

    move v1, v1

    .line 2618558
    sub-float/2addr v0, v1

    int-to-float v1, p3

    mul-float/2addr v0, v1

    int-to-float v1, p1

    sub-float/2addr v0, v1

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
