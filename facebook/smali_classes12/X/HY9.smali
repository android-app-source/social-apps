.class public LX/HY9;
.super Landroid/webkit/WebViewClient;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile k:LX/HY9;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:LX/2QR;

.field public final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/platform/webdialogs/bridgeapi/PlatformWebDialogsBridgeApiFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/content/SecureContextHelper;

.field private final f:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final g:LX/03R;

.field public h:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/facebook/webview/FacebookWebView;",
            "Lcom/facebook/platform/webdialogs/PlatformWebViewClient$PlatformWebViewBridgeHandler;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/BWE;

.field private j:LX/BWL;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2480302
    const-class v0, LX/HY9;

    sput-object v0, LX/HY9;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/2QR;Ljava/util/Set;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/03R;)V
    .locals 1
    .param p6    # LX/03R;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/2QR;",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/platform/webdialogs/bridgeapi/PlatformWebDialogsBridgeApiFactory;",
            ">;",
            "Lcom/facebook/content/SecureContextHelper;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/03R;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2480229
    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    .line 2480230
    iput-object p1, p0, LX/HY9;->b:Landroid/content/Context;

    .line 2480231
    iput-object p2, p0, LX/HY9;->c:LX/2QR;

    .line 2480232
    iput-object p3, p0, LX/HY9;->d:Ljava/util/Set;

    .line 2480233
    iput-object p4, p0, LX/HY9;->e:Lcom/facebook/content/SecureContextHelper;

    .line 2480234
    iput-object p5, p0, LX/HY9;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2480235
    iput-object p6, p0, LX/HY9;->g:LX/03R;

    .line 2480236
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/HY9;->h:Ljava/util/HashMap;

    .line 2480237
    new-instance v0, LX/HY8;

    invoke-direct {v0, p0}, LX/HY8;-><init>(LX/HY9;)V

    iput-object v0, p0, LX/HY9;->i:LX/BWE;

    .line 2480238
    new-instance v0, LX/HY7;

    invoke-direct {v0, p0}, LX/HY7;-><init>(LX/HY9;)V

    iput-object v0, p0, LX/HY9;->j:LX/BWL;

    .line 2480239
    return-void
.end method

.method public static a(LX/0QB;)LX/HY9;
    .locals 10

    .prologue
    .line 2480287
    sget-object v0, LX/HY9;->k:LX/HY9;

    if-nez v0, :cond_1

    .line 2480288
    const-class v1, LX/HY9;

    monitor-enter v1

    .line 2480289
    :try_start_0
    sget-object v0, LX/HY9;->k:LX/HY9;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2480290
    if-eqz v2, :cond_0

    .line 2480291
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2480292
    new-instance v3, LX/HY9;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/2QR;->a(LX/0QB;)LX/2QR;

    move-result-object v5

    check-cast v5, LX/2QR;

    .line 2480293
    new-instance v6, LX/0U8;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v7

    new-instance v8, LX/HYK;

    invoke-direct {v8, v0}, LX/HYK;-><init>(LX/0QB;)V

    invoke-direct {v6, v7, v8}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v6, v6

    .line 2480294
    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v8

    check-cast v8, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0XD;->b(LX/0QB;)LX/03R;

    move-result-object v9

    check-cast v9, LX/03R;

    invoke-direct/range {v3 .. v9}, LX/HY9;-><init>(Landroid/content/Context;LX/2QR;Ljava/util/Set;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/03R;)V

    .line 2480295
    move-object v0, v3

    .line 2480296
    sput-object v0, LX/HY9;->k:LX/HY9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2480297
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2480298
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2480299
    :cond_1
    sget-object v0, LX/HY9;->k:LX/HY9;

    return-object v0

    .line 2480300
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2480301
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static d(LX/HY9;Lcom/facebook/webview/FacebookWebView;)V
    .locals 2

    .prologue
    .line 2480284
    const-string v0, "window.__fbPlatDialogHost.%1$s()"

    const-string v1, "__dequeue"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2480285
    iget-object v1, p0, LX/HY9;->j:LX/BWL;

    invoke-virtual {p1, v0, v1}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWL;)Ljava/lang/String;

    .line 2480286
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/webview/FacebookWebView;Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 3

    .prologue
    .line 2480280
    const-string v1, "window.__fbPlatDialogHost.%1$s(\'%2$s\',%3$s)"

    const-string v2, "__dispatchEvent"

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v1, v2, p2, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2480281
    iget-object v1, p0, LX/HY9;->j:LX/BWL;

    invoke-virtual {p1, v0, v1}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWL;)Ljava/lang/String;

    .line 2480282
    return-void

    .line 2480283
    :cond_0
    const-string v0, "null"

    goto :goto_0
.end method

.method public final onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2480264
    move-object v0, p1

    check-cast v0, Lcom/facebook/webview/FacebookWebView;

    .line 2480265
    iget-object v1, p0, LX/HY9;->h:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2480266
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 2480267
    iget-object v1, p0, LX/HY9;->d:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HYA;

    .line 2480268
    invoke-virtual {v1}, LX/HYA;->a()Ljava/lang/String;

    move-result-object v4

    .line 2480269
    invoke-virtual {v1}, LX/HYA;->b()Ljava/lang/String;

    move-result-object v1

    .line 2480270
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0x3a

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v4, 0x2c

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v4, 0xa

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 2480271
    :cond_0
    const-string v1, "function() {  var queue=[];  function bridge(method,params,callback,errback){    if(callback||errback){      var token=\'f\'+(Math.random()*(1<<30)).toString(16).replace(\'.\',\'\');      window.addEventListener(token,function(event){        window.removeEventListener(token,arguments.callee);        if (event.data.error) {          errback(event.data);        } else {          callback(event.data);        }      });      params.%1$s=token;    }    queue.push(JSON.stringify({\'%2$s\':method,\'%3$s\':params}));    if(queue.length==1){window.prompt(\'%4$s:///\');}  }  function bridgeMethodImpl(name){    return function(params,callback,errback){bridge(name,params,callback,errback);};  }  function dispatchEvent(eventName, data){    var event = document.createEvent(\'Event\');    event.initEvent(eventName,true,true);    event.data = data;    document.dispatchEvent(event);  }  window.__fbPlatDialogHost = window.__fbPlatDialogHost || {    %5$s    %6$s:function(){return queue.shift();},    %7$s:function(eventName, data){dispatchEvent(eventName, data);}  };  dispatchEvent(\'fbPlatformReady\',null);}();"

    const/4 v3, 0x7

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "__callbackToken"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "method"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "params"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-string v5, "fbplatdialog"

    aput-object v5, v3, v4

    const/4 v4, 0x4

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v4

    const/4 v2, 0x5

    const-string v4, "__dequeue"

    aput-object v4, v3, v2

    const/4 v2, 0x6

    const-string v4, "__dispatchEvent"

    aput-object v4, v3, v2

    invoke-static {v1, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2480272
    move-object v1, v1

    .line 2480273
    const/4 v2, 0x0

    .line 2480274
    invoke-virtual {v0, v1, v2}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;LX/BWL;)Ljava/lang/String;

    .line 2480275
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 2480276
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 2480277
    new-instance v2, Lcom/facebook/platform/webdialogs/PlatformWebViewClient$1;

    invoke-direct {v2, p0, v1}, Lcom/facebook/platform/webdialogs/PlatformWebViewClient$1;-><init>(LX/HY9;Ljava/lang/ref/WeakReference;)V

    const-wide/16 v4, 0x1388

    const v1, 0x4af082e0    # 7881072.0f

    invoke-static {v0, v2, v4, v5, v1}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2480278
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 2480279
    return-void
.end method

.method public final onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 2480258
    move-object v0, p1

    check-cast v0, Lcom/facebook/webview/FacebookWebView;

    .line 2480259
    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 2480260
    new-instance v6, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v6, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 2480261
    new-instance v0, Lcom/facebook/platform/webdialogs/PlatformWebViewClient$2;

    move-object v1, p0

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/facebook/platform/webdialogs/PlatformWebViewClient$2;-><init>(LX/HY9;Ljava/lang/ref/WeakReference;ILjava/lang/String;Ljava/lang/String;)V

    const v1, -0x31f5e8cf    # -5.7919392E8f

    invoke-static {v6, v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 2480262
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 2480263
    return-void
.end method

.method public final onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V
    .locals 3

    .prologue
    .line 2480250
    iget-object v0, p0, LX/HY9;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0dU;->j:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    .line 2480251
    if-nez v0, :cond_0

    .line 2480252
    invoke-virtual {p2}, Landroid/webkit/SslErrorHandler;->proceed()V

    .line 2480253
    :goto_0
    return-void

    .line 2480254
    :cond_0
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 2480255
    if-nez v0, :cond_1

    sget-object v0, LX/03R;->YES:LX/03R;

    iget-object v1, p0, LX/HY9;->g:LX/03R;

    if-ne v0, v1, :cond_2

    .line 2480256
    :cond_1
    iget-object v0, p0, LX/HY9;->b:Landroid/content/Context;

    const v1, 0x7f082715

    invoke-static {v0, v1}, LX/0kL;->a(Landroid/content/Context;I)V

    goto :goto_0

    .line 2480257
    :cond_2
    iget-object v0, p0, LX/HY9;->b:Landroid/content/Context;

    const v1, 0x7f082714

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    goto :goto_0
.end method

.method public final shouldInterceptRequest(Landroid/webkit/WebView;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;
    .locals 4

    .prologue
    .line 2480245
    const-string v0, "platformurlversion"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2480246
    iget-object v0, p0, LX/HY9;->c:LX/2QR;

    invoke-virtual {v0, p2}, LX/2QR;->a(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    .line 2480247
    if-eqz v1, :cond_0

    .line 2480248
    new-instance v0, Landroid/webkit/WebResourceResponse;

    const-string v2, "text/html"

    const-string v3, "utf-8"

    invoke-direct {v0, v2, v3, v1}, Landroid/webkit/WebResourceResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;)V

    .line 2480249
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->shouldInterceptRequest(Landroid/webkit/WebView;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 5

    .prologue
    .line 2480240
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2480241
    sget-object v1, LX/0ax;->a:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "extbrowser"

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2480242
    iget-object v1, p0, LX/HY9;->e:Lcom/facebook/content/SecureContextHelper;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    const-string v4, "url"

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2480243
    const/4 v0, 0x1

    .line 2480244
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method
