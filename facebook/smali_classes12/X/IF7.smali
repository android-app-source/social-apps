.class public LX/IF7;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/IF7;


# direct methods
.method public constructor <init>()V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2553588
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2553589
    sget-object v0, LX/0ax;->dX:Ljava/lang/String;

    const-string v1, "{source unknown}"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v2, LX/0cQ;->NEARBY_FRIENDS_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    .line 2553590
    sget-object v0, LX/0ax;->dY:Ljava/lang/String;

    const-string v1, "{fbid []}"

    const-string v2, "{source unknown}"

    const-string v3, "{notif_id 0}"

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v2, LX/0cQ;->NEARBY_FRIENDS_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    .line 2553591
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2553592
    const-string v1, "title"

    const v2, 0x7f08386b

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2553593
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->NEARBY_FRIENDS_INVITE_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2553594
    sget-object v1, LX/0ax;->dZ:Ljava/lang/String;

    const-string v2, "{friendsSelectorSelected -1}"

    const-string v3, "{friendsSelectorExcluded -1}"

    invoke-static {v1, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/facebook/widget/friendselector/CaspianFriendSelectorActivity;

    invoke-virtual {p0, v1, v2, v0}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 2553595
    return-void
.end method

.method public static a(LX/0QB;)LX/IF7;
    .locals 3

    .prologue
    .line 2553596
    sget-object v0, LX/IF7;->a:LX/IF7;

    if-nez v0, :cond_1

    .line 2553597
    const-class v1, LX/IF7;

    monitor-enter v1

    .line 2553598
    :try_start_0
    sget-object v0, LX/IF7;->a:LX/IF7;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2553599
    if-eqz v2, :cond_0

    .line 2553600
    :try_start_1
    new-instance v0, LX/IF7;

    invoke-direct {v0}, LX/IF7;-><init>()V

    .line 2553601
    move-object v0, v0

    .line 2553602
    sput-object v0, LX/IF7;->a:LX/IF7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2553603
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2553604
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2553605
    :cond_1
    sget-object v0, LX/IF7;->a:LX/IF7;

    return-object v0

    .line 2553606
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2553607
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
