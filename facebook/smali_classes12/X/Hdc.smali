.class public LX/Hdc;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/3mL;

.field private final b:LX/Hde;

.field public final c:LX/Hdh;


# direct methods
.method public constructor <init>(LX/3mL;LX/Hde;LX/Hdh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2488830
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2488831
    iput-object p1, p0, LX/Hdc;->a:LX/3mL;

    .line 2488832
    iput-object p2, p0, LX/Hdc;->b:LX/Hde;

    .line 2488833
    iput-object p3, p0, LX/Hdc;->c:LX/Hdh;

    .line 2488834
    return-void
.end method

.method public static a(LX/Hdc;LX/1De;LX/1Pm;Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel;)LX/3mX;
    .locals 9

    .prologue
    .line 2488835
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b22f6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    .line 2488836
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v0, v1

    .line 2488837
    invoke-static {}, LX/3mP;->newBuilder()LX/3mP;

    move-result-object v1

    float-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 2488838
    iput-object v0, v1, LX/3mP;->c:Ljava/lang/Integer;

    .line 2488839
    move-object v0, v1

    .line 2488840
    const/4 v1, 0x2

    .line 2488841
    iput v1, v0, LX/3mP;->b:I

    .line 2488842
    move-object v0, v0

    .line 2488843
    invoke-virtual {v0}, LX/3mP;->a()LX/25M;

    move-result-object v0

    .line 2488844
    iget-object v1, p0, LX/Hdc;->b:LX/Hde;

    invoke-virtual {p3}, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel;->a()Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/topics/sections/sources/TopicSourcesGraphQLModels$TopicSourcesQueryModel$TopicContentPublishersModel;->j()LX/0Px;

    move-result-object v2

    .line 2488845
    new-instance v3, LX/Hdd;

    const-class v4, Landroid/content/Context;

    invoke-interface {v1, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v1}, LX/HdO;->a(LX/0QB;)LX/HdO;

    move-result-object v8

    check-cast v8, LX/HdO;

    move-object v5, v2

    move-object v6, p2

    move-object v7, v0

    invoke-direct/range {v3 .. v8}, LX/Hdd;-><init>(Landroid/content/Context;LX/0Px;LX/1Pm;LX/25M;LX/HdO;)V

    .line 2488846
    move-object v0, v3

    .line 2488847
    return-object v0
.end method

.method public static a(LX/0QB;)LX/Hdc;
    .locals 6

    .prologue
    .line 2488848
    const-class v1, LX/Hdc;

    monitor-enter v1

    .line 2488849
    :try_start_0
    sget-object v0, LX/Hdc;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2488850
    sput-object v2, LX/Hdc;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2488851
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2488852
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2488853
    new-instance p0, LX/Hdc;

    invoke-static {v0}, LX/3mL;->a(LX/0QB;)LX/3mL;

    move-result-object v3

    check-cast v3, LX/3mL;

    const-class v4, LX/Hde;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/Hde;

    invoke-static {v0}, LX/Hdh;->a(LX/0QB;)LX/Hdh;

    move-result-object v5

    check-cast v5, LX/Hdh;

    invoke-direct {p0, v3, v4, v5}, LX/Hdc;-><init>(LX/3mL;LX/Hde;LX/Hdh;)V

    .line 2488854
    move-object v0, p0

    .line 2488855
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2488856
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Hdc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2488857
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2488858
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
