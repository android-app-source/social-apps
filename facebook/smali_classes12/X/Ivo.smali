.class public LX/Ivo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# static fields
.field private static final a:LX/0wT;


# instance fields
.field private final b:Landroid/os/Handler;

.field private final c:Lcom/facebook/notifications/lockscreenservice/SpringyPressStateTouchHandler$DeferredTouchRunnable;

.field public final d:LX/0wc;

.field public e:F

.field public f:Z

.field public g:Z

.field public h:Z

.field public final i:Landroid/view/View;

.field public final j:LX/IvW;

.field private final k:Landroid/graphics/Rect;

.field private final l:[I

.field private final m:LX/0wW;

.field public final n:LX/0wd;

.field private final o:LX/Ivn;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2629218
    const-wide/high16 v0, 0x4034000000000000L    # 20.0

    const-wide/high16 v2, 0x402e000000000000L    # 15.0

    invoke-static {v0, v1, v2, v3}, LX/0wT;->b(DD)LX/0wT;

    move-result-object v0

    sput-object v0, LX/Ivo;->a:LX/0wT;

    return-void
.end method

.method private constructor <init>(Landroid/view/View;LX/IvW;LX/0wW;LX/0wT;DDLX/0wc;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 2629219
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2629220
    iput-boolean v4, p0, LX/Ivo;->g:Z

    .line 2629221
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, LX/Ivo;->l:[I

    .line 2629222
    const v0, 0x3f75c28f    # 0.96f

    iput v0, p0, LX/Ivo;->e:F

    .line 2629223
    iput-object p1, p0, LX/Ivo;->i:Landroid/view/View;

    .line 2629224
    iput-object p2, p0, LX/Ivo;->j:LX/IvW;

    .line 2629225
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/Ivo;->k:Landroid/graphics/Rect;

    .line 2629226
    iput-object p3, p0, LX/Ivo;->m:LX/0wW;

    .line 2629227
    iput-object p9, p0, LX/Ivo;->d:LX/0wc;

    .line 2629228
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LX/Ivo;->b:Landroid/os/Handler;

    .line 2629229
    new-instance v0, Lcom/facebook/notifications/lockscreenservice/SpringyPressStateTouchHandler$DeferredTouchRunnable;

    iget-object v1, p0, LX/Ivo;->i:Landroid/view/View;

    invoke-direct {v0, p0, v1, p0}, Lcom/facebook/notifications/lockscreenservice/SpringyPressStateTouchHandler$DeferredTouchRunnable;-><init>(LX/Ivo;Landroid/view/View;LX/Ivo;)V

    iput-object v0, p0, LX/Ivo;->c:Lcom/facebook/notifications/lockscreenservice/SpringyPressStateTouchHandler$DeferredTouchRunnable;

    .line 2629230
    iget-object v0, p0, LX/Ivo;->m:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    invoke-virtual {v0, p4}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    .line 2629231
    iput-wide p5, v0, LX/0wd;->k:D

    .line 2629232
    move-object v0, v0

    .line 2629233
    iput-wide p7, v0, LX/0wd;->l:D

    .line 2629234
    move-object v0, v0

    .line 2629235
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    move-result-object v0

    iput-object v0, p0, LX/Ivo;->n:LX/0wd;

    .line 2629236
    new-instance v0, LX/Ivn;

    invoke-direct {v0, p0}, LX/Ivn;-><init>(LX/Ivo;)V

    iput-object v0, p0, LX/Ivo;->o:LX/Ivn;

    .line 2629237
    iget-object v0, p0, LX/Ivo;->i:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2629238
    iget-object v0, p0, LX/Ivo;->n:LX/0wd;

    iget-object v1, p0, LX/Ivo;->o:LX/Ivn;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 2629239
    :cond_0
    iget-object v0, p0, LX/Ivo;->i:Landroid/view/View;

    iget-object v1, p0, LX/Ivo;->n:LX/0wd;

    iget-object v2, p0, LX/Ivo;->o:LX/Ivn;

    .line 2629240
    new-instance v3, LX/Ivm;

    invoke-direct {v3, v1, v2}, LX/Ivm;-><init>(LX/0wd;LX/Ivn;)V

    move-object v1, v3

    .line 2629241
    invoke-virtual {v0, v1}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 2629242
    return-void

    .line 2629243
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method public constructor <init>(Landroid/view/View;LX/IvW;LX/0wW;LX/0wc;)V
    .locals 11

    .prologue
    const-wide v6, 0x3f847ae147ae147bL    # 0.01

    .line 2629244
    sget-object v5, LX/Ivo;->a:LX/0wT;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-wide v8, v6

    move-object v10, p4

    invoke-direct/range {v1 .. v10}, LX/Ivo;-><init>(Landroid/view/View;LX/IvW;LX/0wW;LX/0wT;DDLX/0wc;)V

    .line 2629245
    return-void
.end method

.method private a(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2629246
    iget-object v2, p0, LX/Ivo;->i:Landroid/view/View;

    iget-object v3, p0, LX/Ivo;->l:[I

    invoke-virtual {v2, v3}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 2629247
    iget-object v2, p0, LX/Ivo;->l:[I

    aget v2, v2, v1

    .line 2629248
    iget-object v3, p0, LX/Ivo;->l:[I

    aget v3, v3, v0

    .line 2629249
    iget-object v4, p0, LX/Ivo;->k:Landroid/graphics/Rect;

    iget-object v5, p0, LX/Ivo;->i:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v5

    add-int/2addr v5, v2

    iget-object v6, p0, LX/Ivo;->i:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v6

    add-int/2addr v6, v3

    invoke-virtual {v4, v2, v3, v5, v6}, Landroid/graphics/Rect;->set(IIII)V

    .line 2629250
    iget-object v2, p0, LX/Ivo;->k:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    .line 2629251
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    move v0, v1

    .line 2629252
    :goto_0
    return v0

    .line 2629253
    :pswitch_0
    iput-boolean v1, p0, LX/Ivo;->f:Z

    .line 2629254
    iput-boolean v1, p0, LX/Ivo;->h:Z

    .line 2629255
    invoke-static {p0}, LX/Ivo;->b(LX/Ivo;)V

    goto :goto_0

    .line 2629256
    :pswitch_1
    if-eqz v2, :cond_0

    .line 2629257
    invoke-static {p0}, LX/Ivo;->b(LX/Ivo;)V

    goto :goto_0

    .line 2629258
    :cond_0
    invoke-direct {p0}, LX/Ivo;->c()V

    move v0, v1

    .line 2629259
    goto :goto_0

    .line 2629260
    :pswitch_2
    invoke-direct {p0}, LX/Ivo;->c()V

    move v0, v1

    .line 2629261
    goto :goto_0

    .line 2629262
    :pswitch_3
    iget-boolean v3, p0, LX/Ivo;->g:Z

    if-eqz v3, :cond_2

    .line 2629263
    iget-object v1, p0, LX/Ivo;->n:LX/0wd;

    invoke-virtual {v1}, LX/0wd;->i()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2629264
    invoke-direct {p0}, LX/Ivo;->c()V

    .line 2629265
    :goto_1
    iput-boolean v0, p0, LX/Ivo;->h:Z

    goto :goto_0

    .line 2629266
    :cond_1
    iput-boolean v0, p0, LX/Ivo;->f:Z

    goto :goto_1

    .line 2629267
    :cond_2
    invoke-direct {p0}, LX/Ivo;->c()V

    .line 2629268
    if-nez v2, :cond_3

    move v0, v1

    .line 2629269
    goto :goto_0

    .line 2629270
    :cond_3
    iget-object v0, p0, LX/Ivo;->j:LX/IvW;

    iget-object v1, p0, LX/Ivo;->i:Landroid/view/View;

    invoke-virtual {v0, v1}, LX/IvW;->a(Landroid/view/View;)Z

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static b(LX/Ivo;)V
    .locals 4

    .prologue
    .line 2629271
    iget-object v0, p0, LX/Ivo;->n:LX/0wd;

    iget v1, p0, LX/Ivo;->e:F

    float-to-double v2, v1

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 2629272
    return-void
.end method

.method private c()V
    .locals 4

    .prologue
    .line 2629273
    iget-object v0, p0, LX/Ivo;->n:LX/0wd;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 2629274
    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v2, 0x1

    .line 2629275
    iget-boolean v0, p0, LX/Ivo;->g:Z

    if-eqz v0, :cond_1

    .line 2629276
    invoke-direct {p0, p2}, LX/Ivo;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 2629277
    :cond_0
    :goto_0
    return v0

    .line 2629278
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v3, v0, 0xff

    .line 2629279
    if-nez v3, :cond_2

    .line 2629280
    iget-object v0, p0, LX/Ivo;->c:Lcom/facebook/notifications/lockscreenservice/SpringyPressStateTouchHandler$DeferredTouchRunnable;

    .line 2629281
    iput-object p2, v0, Lcom/facebook/notifications/lockscreenservice/SpringyPressStateTouchHandler$DeferredTouchRunnable;->d:Landroid/view/MotionEvent;

    .line 2629282
    iget-object v0, p0, LX/Ivo;->b:Landroid/os/Handler;

    invoke-virtual {v0, v7}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 2629283
    iget-object v0, p0, LX/Ivo;->b:Landroid/os/Handler;

    iget-object v1, p0, LX/Ivo;->c:Lcom/facebook/notifications/lockscreenservice/SpringyPressStateTouchHandler$DeferredTouchRunnable;

    const-wide/16 v4, 0x96

    const v6, -0x3fc0655a

    invoke-static {v0, v1, v4, v5, v6}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2629284
    :cond_2
    const/4 v0, 0x0

    .line 2629285
    iget-object v1, p0, LX/Ivo;->c:Lcom/facebook/notifications/lockscreenservice/SpringyPressStateTouchHandler$DeferredTouchRunnable;

    .line 2629286
    iget-boolean v4, v1, Lcom/facebook/notifications/lockscreenservice/SpringyPressStateTouchHandler$DeferredTouchRunnable;->e:Z

    move v1, v4

    .line 2629287
    if-eqz v1, :cond_4

    .line 2629288
    invoke-direct {p0, p2}, LX/Ivo;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    move v1, v2

    .line 2629289
    :goto_1
    const/4 v4, 0x3

    if-ne v3, v4, :cond_3

    .line 2629290
    iget-object v1, p0, LX/Ivo;->b:Landroid/os/Handler;

    invoke-virtual {v1, v7}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 2629291
    iget-object v1, p0, LX/Ivo;->c:Lcom/facebook/notifications/lockscreenservice/SpringyPressStateTouchHandler$DeferredTouchRunnable;

    invoke-virtual {v1}, Lcom/facebook/notifications/lockscreenservice/SpringyPressStateTouchHandler$DeferredTouchRunnable;->b()V

    goto :goto_0

    .line 2629292
    :cond_3
    if-ne v3, v2, :cond_0

    .line 2629293
    iget-object v2, p0, LX/Ivo;->b:Landroid/os/Handler;

    invoke-virtual {v2, v7}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 2629294
    iget-object v2, p0, LX/Ivo;->c:Lcom/facebook/notifications/lockscreenservice/SpringyPressStateTouchHandler$DeferredTouchRunnable;

    invoke-virtual {v2}, Lcom/facebook/notifications/lockscreenservice/SpringyPressStateTouchHandler$DeferredTouchRunnable;->b()V

    .line 2629295
    if-nez v1, :cond_0

    .line 2629296
    invoke-direct {p0, p2}, LX/Ivo;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 2629297
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/Ivo;->f:Z

    .line 2629298
    invoke-static {p0}, LX/Ivo;->b(LX/Ivo;)V

    .line 2629299
    goto :goto_0

    :cond_4
    move v1, v0

    move v0, v2

    goto :goto_1
.end method
