.class public final LX/Hz1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLInterfaces$ReactionUnitFragment;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Hz2;


# direct methods
.method public constructor <init>(LX/Hz2;)V
    .locals 0

    .prologue
    .line 2525137
    iput-object p1, p0, LX/Hz1;->a:LX/Hz2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2525136
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2525123
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2525124
    if-eqz p1, :cond_0

    .line 2525125
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2525126
    if-nez v0, :cond_1

    .line 2525127
    :cond_0
    :goto_0
    return-void

    .line 2525128
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2525129
    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    .line 2525130
    iget-object v1, p0, LX/Hz1;->a:LX/Hz2;

    iget-object v1, v1, LX/Hz2;->j:LX/I2Z;

    .line 2525131
    iget-object v2, v1, LX/I2Z;->l:Ljava/util/HashMap;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 2525132
    iget-object v2, v1, LX/I2Z;->l:Ljava/util/HashMap;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 2525133
    iget-object p1, v1, LX/I2Z;->k:Ljava/util/List;

    invoke-interface {p1, v2, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2525134
    invoke-static {v1}, LX/I2Z;->b(LX/I2Z;)V

    .line 2525135
    :cond_2
    iget-object v0, p0, LX/Hz1;->a:LX/Hz2;

    invoke-static {v0}, LX/Hz2;->m(LX/Hz2;)V

    goto :goto_0
.end method
