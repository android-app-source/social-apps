.class public LX/HJS;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/3U8;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kp;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityMoreFriendsComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/HJS",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityMoreFriendsComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2452742
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2452743
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/HJS;->b:LX/0Zi;

    .line 2452744
    iput-object p1, p0, LX/HJS;->a:LX/0Ot;

    .line 2452745
    return-void
.end method

.method public static a(LX/0QB;)LX/HJS;
    .locals 4

    .prologue
    .line 2452746
    const-class v1, LX/HJS;

    monitor-enter v1

    .line 2452747
    :try_start_0
    sget-object v0, LX/HJS;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2452748
    sput-object v2, LX/HJS;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2452749
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2452750
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2452751
    new-instance v3, LX/HJS;

    const/16 p0, 0x2bad

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/HJS;-><init>(LX/0Ot;)V

    .line 2452752
    move-object v0, v3

    .line 2452753
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2452754
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/HJS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2452755
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2452756
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2452757
    const v0, -0x79aaf69b

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 2452758
    iget-object v0, p0, LX/HJS;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityMoreFriendsComponentSpec;

    const/4 p2, 0x1

    .line 2452759
    invoke-static {}, LX/4Ab;->e()LX/4Ab;

    move-result-object v1

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b2323

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, LX/4Ab;->d(F)LX/4Ab;

    move-result-object v1

    .line 2452760
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x2

    invoke-interface {v2, v3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    iget-object v3, v0, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityMoreFriendsComponentSpec;->c:LX/1nu;

    invoke-virtual {v3, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v3

    sget-object v4, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityMoreFriendsComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v4}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v3

    invoke-static {p1}, LX/1nf;->a(LX/1De;)LX/1nh;

    move-result-object v4

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const p0, 0x7f0a008a

    invoke-virtual {v5, p0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v4, v5}, LX/1nh;->h(I)LX/1nh;

    move-result-object v4

    .line 2452761
    iget-object v5, v3, LX/1nw;->a:Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;

    invoke-virtual {v4}, LX/1n6;->b()LX/1dc;

    move-result-object p0

    iput-object p0, v5, Lcom/facebook/feedplugins/images/FbFeedFrescoComponent$FbFeedFrescoComponentImpl;->i:LX/1dc;

    .line 2452762
    move-object v3, v3

    .line 2452763
    invoke-virtual {v3, v1}, LX/1nw;->a(LX/4Ab;)LX/1nw;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const v3, 0x7f0b2327

    invoke-interface {v1, v3}, LX/1Di;->q(I)LX/1Di;

    move-result-object v1

    const v3, 0x7f0b2327

    invoke-interface {v1, v3}, LX/1Di;->i(I)LX/1Di;

    move-result-object v1

    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v2

    iget-object v3, v0, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityMoreFriendsComponentSpec;->e:LX/1vg;

    invoke-virtual {v3, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v3

    const v4, 0x7f0a0097

    invoke-virtual {v3, v4}, LX/2xv;->j(I)LX/2xv;

    move-result-object v3

    const v4, 0x7f020886

    invoke-virtual {v3, v4}, LX/2xv;->h(I)LX/2xv;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1o5;->a(LX/1n6;)LX/1o5;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const v3, 0x7f0b2326

    invoke-interface {v2, v3}, LX/1Di;->q(I)LX/1Di;

    move-result-object v2

    invoke-interface {v2, p2}, LX/1Di;->c(I)LX/1Di;

    move-result-object v2

    const/4 v3, 0x0

    const v4, 0x7f0b2325

    invoke-interface {v2, v3, v4}, LX/1Di;->l(II)LX/1Di;

    move-result-object v2

    const v3, 0x7f0b2325

    invoke-interface {v2, p2, v3}, LX/1Di;->l(II)LX/1Di;

    move-result-object v2

    const v3, 0x7f0b2326

    invoke-interface {v2, v3}, LX/1Di;->i(I)LX/1Di;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    .line 2452764
    const v2, -0x79aaf69b

    const/4 v3, 0x0

    invoke-static {p1, v2, v3}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v2

    move-object v2, v2

    .line 2452765
    invoke-interface {v1, v2}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 2452766
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2452767
    invoke-static {}, LX/1dS;->b()V

    .line 2452768
    iget v0, p1, LX/1dQ;->b:I

    .line 2452769
    packed-switch v0, :pswitch_data_0

    .line 2452770
    :goto_0
    return-object v1

    .line 2452771
    :pswitch_0
    iget-object v2, p0, LX/HJS;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityMoreFriendsComponentSpec;

    .line 2452772
    iget-object v3, v2, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityMoreFriendsComponentSpec;->d:LX/17W;

    iget-object v4, v2, Lcom/facebook/pages/common/reaction/components/PageFriendsCityActivityMoreFriendsComponentSpec;->b:Landroid/content/Context;

    sget-object p1, LX/0ax;->cW:Ljava/lang/String;

    const/4 p2, 0x2

    new-array p2, p2, [Ljava/lang/Object;

    const/4 p0, 0x0

    const-string v0, "unknown"

    aput-object v0, p2, p0

    const/4 p0, 0x1

    sget-object v0, LX/5P0;->SUGGESTIONS:LX/5P0;

    invoke-virtual {v0}, LX/5P0;->name()Ljava/lang/String;

    move-result-object v0

    aput-object v0, p2, p0

    invoke-static {p1, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, v4, p1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2452773
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x79aaf69b
        :pswitch_0
    .end packed-switch
.end method
