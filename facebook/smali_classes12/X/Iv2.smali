.class public final enum LX/Iv2;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Iv2;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Iv2;

.field public static final enum CW_0:LX/Iv2;

.field public static final enum CW_180:LX/Iv2;

.field public static final enum CW_270:LX/Iv2;

.field public static final enum CW_90:LX/Iv2;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2627562
    new-instance v0, LX/Iv2;

    const-string v1, "CW_0"

    invoke-direct {v0, v1, v2}, LX/Iv2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Iv2;->CW_0:LX/Iv2;

    .line 2627563
    new-instance v0, LX/Iv2;

    const-string v1, "CW_90"

    invoke-direct {v0, v1, v3}, LX/Iv2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Iv2;->CW_90:LX/Iv2;

    .line 2627564
    new-instance v0, LX/Iv2;

    const-string v1, "CW_180"

    invoke-direct {v0, v1, v4}, LX/Iv2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Iv2;->CW_180:LX/Iv2;

    .line 2627565
    new-instance v0, LX/Iv2;

    const-string v1, "CW_270"

    invoke-direct {v0, v1, v5}, LX/Iv2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Iv2;->CW_270:LX/Iv2;

    .line 2627566
    const/4 v0, 0x4

    new-array v0, v0, [LX/Iv2;

    sget-object v1, LX/Iv2;->CW_0:LX/Iv2;

    aput-object v1, v0, v2

    sget-object v1, LX/Iv2;->CW_90:LX/Iv2;

    aput-object v1, v0, v3

    sget-object v1, LX/Iv2;->CW_180:LX/Iv2;

    aput-object v1, v0, v4

    sget-object v1, LX/Iv2;->CW_270:LX/Iv2;

    aput-object v1, v0, v5

    sput-object v0, LX/Iv2;->$VALUES:[LX/Iv2;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2627567
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Iv2;
    .locals 1

    .prologue
    .line 2627568
    const-class v0, LX/Iv2;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Iv2;

    return-object v0
.end method

.method public static values()[LX/Iv2;
    .locals 1

    .prologue
    .line 2627569
    sget-object v0, LX/Iv2;->$VALUES:[LX/Iv2;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Iv2;

    return-object v0
.end method
