.class public LX/JNy;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JO0;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JNy",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JO0;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2686753
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2686754
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JNy;->b:LX/0Zi;

    .line 2686755
    iput-object p1, p0, LX/JNy;->a:LX/0Ot;

    .line 2686756
    return-void
.end method

.method public static a(LX/0QB;)LX/JNy;
    .locals 4

    .prologue
    .line 2686742
    const-class v1, LX/JNy;

    monitor-enter v1

    .line 2686743
    :try_start_0
    sget-object v0, LX/JNy;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2686744
    sput-object v2, LX/JNy;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2686745
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2686746
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2686747
    new-instance v3, LX/JNy;

    const/16 p0, 0x1f43

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JNy;-><init>(LX/0Ot;)V

    .line 2686748
    move-object v0, v3

    .line 2686749
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2686750
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JNy;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2686751
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2686752
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 12

    .prologue
    .line 2686712
    check-cast p2, LX/JNx;

    .line 2686713
    iget-object v0, p0, LX/JNy;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JO0;

    iget-object v1, p2, LX/JNx;->a:LX/1Pb;

    iget-object v2, p2, LX/JNx;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2686714
    new-instance v4, LX/JNz;

    invoke-direct {v4, v0, v2}, LX/JNz;-><init>(LX/JO0;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2686715
    invoke-static {}, LX/3mP;->newBuilder()LX/3mP;

    move-result-object v5

    .line 2686716
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2686717
    check-cast v3, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->g()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/25L;->a(Ljava/lang/String;)LX/25L;

    move-result-object v3

    .line 2686718
    iput-object v3, v5, LX/3mP;->d:LX/25L;

    .line 2686719
    move-object v5, v5

    .line 2686720
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2686721
    check-cast v3, LX/0jW;

    .line 2686722
    iput-object v3, v5, LX/3mP;->e:LX/0jW;

    .line 2686723
    move-object v3, v5

    .line 2686724
    const/16 v5, 0x8

    .line 2686725
    iput v5, v3, LX/3mP;->b:I

    .line 2686726
    move-object v3, v3

    .line 2686727
    iput-object v4, v3, LX/3mP;->g:LX/25K;

    .line 2686728
    move-object v3, v3

    .line 2686729
    invoke-virtual {v3}, LX/3mP;->a()LX/25M;

    move-result-object v3

    .line 2686730
    iget-object v4, v0, LX/JO0;->a:LX/JNv;

    .line 2686731
    iget-object v5, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 2686732
    check-cast v5, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;

    .line 2686733
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;->t()LX/0Px;

    move-result-object v6

    .line 2686734
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 2686735
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;

    .line 2686736
    new-instance v9, LX/JO8;

    invoke-direct {v9, v5, v6}, LX/JO8;-><init>(Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;)V

    invoke-virtual {v7, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 2686737
    :cond_0
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    move-object v5, v5

    .line 2686738
    new-instance v6, LX/JNu;

    move-object v9, v1

    check-cast v9, LX/1Pb;

    invoke-static {v4}, LX/JO7;->a(LX/0QB;)LX/JO7;

    move-result-object v11

    check-cast v11, LX/JO7;

    move-object v7, p1

    move-object v8, v5

    move-object v10, v3

    invoke-direct/range {v6 .. v11}, LX/JNu;-><init>(Landroid/content/Context;LX/0Px;LX/1Pb;LX/25M;LX/JO7;)V

    .line 2686739
    move-object v3, v6

    .line 2686740
    iget-object v4, v0, LX/JO0;->b:LX/3mL;

    invoke-virtual {v4, p1}, LX/3mL;->c(LX/1De;)LX/3ml;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/3ml;->a(LX/3mX;)LX/3ml;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/4 v4, 0x6

    const/4 v5, 0x4

    invoke-interface {v3, v4, v5}, LX/1Di;->h(II)LX/1Di;

    move-result-object v3

    const/4 v4, 0x3

    const v5, 0x7f0b2550

    invoke-interface {v3, v4, v5}, LX/1Di;->g(II)LX/1Di;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2686741
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2686710
    invoke-static {}, LX/1dS;->b()V

    .line 2686711
    const/4 v0, 0x0

    return-object v0
.end method
