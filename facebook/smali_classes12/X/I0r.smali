.class public LX/I0r;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "LX/I0X;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/11R;

.field public final c:Landroid/content/Context;

.field private d:LX/I0g;

.field private e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/util/TimeZone;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2528119
    new-instance v0, LX/I0q;

    invoke-direct {v0}, LX/I0q;-><init>()V

    sput-object v0, LX/I0r;->a:Ljava/util/Comparator;

    return-void
.end method

.method private constructor <init>(LX/I0g;LX/0Or;Landroid/content/Context;LX/11R;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/I0g;",
            "LX/0Or",
            "<",
            "Ljava/util/TimeZone;",
            ">;",
            "Landroid/content/Context;",
            "LX/11R;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2528113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2528114
    iput-object p1, p0, LX/I0r;->d:LX/I0g;

    .line 2528115
    iput-object p2, p0, LX/I0r;->e:LX/0Or;

    .line 2528116
    iput-object p3, p0, LX/I0r;->c:Landroid/content/Context;

    .line 2528117
    iput-object p4, p0, LX/I0r;->b:LX/11R;

    .line 2528118
    return-void
.end method

.method public static a(LX/7oa;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2528112
    invoke-interface {p0}, LX/7oa;->eR_()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/Calendar;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2528107
    const/16 v0, 0xb

    invoke-virtual {p0, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 2528108
    const/16 v0, 0xc

    invoke-virtual {p0, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 2528109
    const/16 v0, 0xd

    invoke-virtual {p0, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 2528110
    const/16 v0, 0xe

    invoke-virtual {p0, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 2528111
    return-void
.end method

.method public static a(Ljava/util/List;J)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/I0X;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .line 2528104
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/I0X;

    iget-object v0, v0, LX/I0X;->a:LX/I0Y;

    sget-object v1, LX/I0Y;->DATE_HEADER:LX/I0Y;

    if-eq v0, v1, :cond_1

    .line 2528105
    :cond_0
    :goto_0
    return-void

    .line 2528106
    :cond_1
    new-instance v0, LX/I0X;

    sget-object v1, LX/I0Y;->EVENT_NULLSTATE_ROW:LX/I0Y;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/I0X;-><init>(LX/I0Y;Ljava/lang/Object;)V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private static a(ZLjava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List",
            "<",
            "LX/I0X;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2528101
    if-nez p0, :cond_0

    .line 2528102
    :goto_0
    return-void

    .line 2528103
    :cond_0
    sget-object v0, LX/I0r;->a:Ljava/util/Comparator;

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/I0r;
    .locals 5

    .prologue
    .line 2528099
    new-instance v3, LX/I0r;

    invoke-static {p0}, LX/I0g;->b(LX/0QB;)LX/I0g;

    move-result-object v0

    check-cast v0, LX/I0g;

    const/16 v1, 0x161a

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v2

    check-cast v2, LX/11R;

    invoke-direct {v3, v0, v4, v1, v2}, LX/I0r;-><init>(LX/I0g;LX/0Or;Landroid/content/Context;LX/11R;)V

    .line 2528100
    return-object v3
.end method

.method public static b(LX/7oa;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 2528091
    const/4 v0, 0x0

    .line 2528092
    invoke-interface {p0}, LX/7oa;->d()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2528093
    invoke-interface {p0}, LX/7oa;->d()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v1

    .line 2528094
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/CharSequence;

    const/4 v3, 0x0

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->j()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v2}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2528095
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->j()Ljava/lang/String;

    move-result-object v0

    .line 2528096
    :cond_0
    :goto_0
    return-object v0

    .line 2528097
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->c()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2528098
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;->c()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel$AddressModel;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(LX/I0r;Ljava/util/HashMap;J)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/util/List",
            "<",
            "LX/I0X;",
            ">;>;J)",
            "Ljava/util/List",
            "<",
            "LX/I0X;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2527956
    invoke-virtual {p0, p1, p2, p3}, LX/I0r;->a(Ljava/util/HashMap;J)Ljava/util/List;

    move-result-object v1

    .line 2527957
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/I0X;

    iget-object v0, v0, LX/I0X;->a:LX/I0Y;

    sget-object v2, LX/I0Y;->EVENT_NULLSTATE_ROW:LX/I0Y;

    if-ne v0, v2, :cond_0

    .line 2527958
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 2527959
    :cond_0
    return-object v1
.end method

.method private b(Ljava/util/HashMap;Ljava/util/HashMap;Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;Z)V
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/util/List",
            "<",
            "LX/I0X;",
            ">;>;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/44w",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;>;",
            "Lcom/facebook/events/graphql/EventDashboardGraphQLInterfaces$EventCalendarableItem;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 2528037
    if-eqz p3, :cond_0

    invoke-virtual/range {p3 .. p3}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->d()LX/7oa;

    move-result-object v2

    if-nez v2, :cond_1

    .line 2528038
    :cond_0
    :goto_0
    return-void

    .line 2528039
    :cond_1
    invoke-virtual/range {p3 .. p3}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->d()LX/7oa;

    move-result-object v14

    .line 2528040
    move-object/from16 v0, p0

    iget-object v2, v0, LX/I0r;->e:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/TimeZone;

    invoke-static {v2}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v2

    .line 2528041
    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v14}, LX/7oa;->j()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 2528042
    invoke-static {v2}, LX/I0r;->a(Ljava/util/Calendar;)V

    .line 2528043
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    .line 2528044
    invoke-interface {v14}, LX/7oa;->eQ_()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-interface {v14}, LX/7oa;->b()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, LX/I0r;->d:LX/I0g;

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v14}, LX/7oa;->j()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v14}, LX/7oa;->b()J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    invoke-virtual {v2, v4, v5, v6, v7}, LX/I0g;->a(JJ)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2528045
    :cond_2
    invoke-static {}, Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;->a()Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;

    move-result-object v3

    .line 2528046
    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;->a(Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;)Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;

    .line 2528047
    sget-object v2, LX/I0b;->SINGLE:LX/I0b;

    invoke-virtual {v3, v2}, Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;->a(LX/I0b;)Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;

    .line 2528048
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v8, v9}, LX/I0r;->b(LX/I0r;Ljava/util/HashMap;J)Ljava/util/List;

    move-result-object v4

    .line 2528049
    move-object/from16 v0, p0

    iget-object v2, v0, LX/I0r;->b:LX/11R;

    invoke-virtual {v2, v8, v9}, LX/11R;->a(J)J

    move-result-wide v6

    const-wide/16 v10, 0x0

    cmp-long v2, v6, v10

    if-gez v2, :cond_4

    sget-object v2, LX/I0a;->PAST:LX/I0a;

    .line 2528050
    :goto_1
    invoke-virtual {v3, v2}, Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;->a(LX/I0a;)Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;

    .line 2528051
    sget-object v5, LX/I0a;->FUTURE:LX/I0a;

    invoke-virtual {v2, v5}, LX/I0a;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v14}, LX/7oa;->R()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-interface {v14}, LX/7oa;->q()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v2

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-virtual {v2, v5}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2528052
    :cond_3
    new-instance v2, LX/I0X;

    sget-object v5, LX/I0Y;->EVENT_CANT_ATTEND_ROW:LX/I0Y;

    invoke-direct {v2, v5, v3}, LX/I0X;-><init>(LX/I0Y;Ljava/lang/Object;)V

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2528053
    :goto_2
    move/from16 v0, p4

    invoke-static {v0, v4}, LX/I0r;->a(ZLjava/util/List;)V

    .line 2528054
    invoke-interface {v14}, LX/7oa;->e()Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/44w;

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-direct {v3, v4, v5}, LX/44w;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 2528055
    :cond_4
    sget-object v2, LX/I0a;->FUTURE:LX/I0a;

    goto :goto_1

    .line 2528056
    :cond_5
    new-instance v2, LX/I0X;

    sget-object v5, LX/I0Y;->EVENT_ROW:LX/I0Y;

    invoke-direct {v2, v5, v3}, LX/I0X;-><init>(LX/I0Y;Ljava/lang/Object;)V

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2528057
    :cond_6
    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v14}, LX/7oa;->b()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v16

    .line 2528058
    const/4 v4, 0x0

    .line 2528059
    move-object/from16 v0, p0

    iget-object v2, v0, LX/I0r;->e:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/TimeZone;

    invoke-static {v2}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v15

    .line 2528060
    invoke-virtual {v15, v8, v9}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 2528061
    move-object/from16 v0, p0

    iget-object v2, v0, LX/I0r;->e:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/TimeZone;

    invoke-static {v2}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v18

    .line 2528062
    move-object/from16 v0, v18

    invoke-virtual {v0, v8, v9}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 2528063
    invoke-virtual/range {v18 .. v18}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    move-wide v6, v8

    move-wide v12, v8

    .line 2528064
    :goto_3
    cmp-long v2, v16, v2

    if-lez v2, :cond_e

    .line 2528065
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v12, v13}, LX/I0r;->b(LX/I0r;Ljava/util/HashMap;J)Ljava/util/List;

    move-result-object v5

    .line 2528066
    invoke-static {}, Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;->a()Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;->a(Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;)Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;

    move-result-object v19

    .line 2528067
    invoke-virtual {v15}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v10

    .line 2528068
    move-object/from16 v0, p0

    iget-object v2, v0, LX/I0r;->b:LX/11R;

    invoke-virtual {v2, v12, v13}, LX/11R;->a(J)J

    move-result-wide v2

    const-wide/16 v6, 0x0

    cmp-long v2, v2, v6

    if-gez v2, :cond_8

    sget-object v2, LX/I0a;->PAST:LX/I0a;

    move-object v3, v2

    .line 2528069
    :goto_4
    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;->a(LX/I0a;)Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;

    .line 2528070
    const/4 v2, 0x6

    const/4 v6, 0x1

    move-object/from16 v0, v18

    invoke-virtual {v0, v2, v6}, Ljava/util/Calendar;->add(II)V

    .line 2528071
    const/16 v2, 0xb

    const/4 v6, 0x6

    move-object/from16 v0, v18

    invoke-virtual {v0, v2, v6}, Ljava/util/Calendar;->set(II)V

    .line 2528072
    invoke-virtual/range {v18 .. v18}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    .line 2528073
    if-nez v4, :cond_a

    .line 2528074
    cmp-long v2, v16, v6

    if-lez v2, :cond_9

    sget-object v2, LX/I0b;->MULTI_DATE_START:LX/I0b;

    :goto_5
    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;->a(LX/I0b;)Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;

    .line 2528075
    const/4 v2, 0x1

    .line 2528076
    :goto_6
    sget-object v4, LX/I0a;->FUTURE:LX/I0a;

    invoke-virtual {v3, v4}, LX/I0a;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    invoke-interface {v14}, LX/7oa;->R()Z

    move-result v3

    if-nez v3, :cond_7

    invoke-interface {v14}, LX/7oa;->q()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-virtual {v3, v4}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 2528077
    :cond_7
    new-instance v3, LX/I0X;

    sget-object v4, LX/I0Y;->EVENT_CANT_ATTEND_ROW:LX/I0Y;

    move-object/from16 v0, v19

    invoke-direct {v3, v4, v0}, LX/I0X;-><init>(LX/I0Y;Ljava/lang/Object;)V

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2528078
    :goto_7
    move/from16 v0, p4

    invoke-static {v0, v5}, LX/I0r;->a(ZLjava/util/List;)V

    .line 2528079
    const/4 v3, 0x6

    const/4 v4, 0x1

    invoke-virtual {v15, v3, v4}, Ljava/util/Calendar;->add(II)V

    .line 2528080
    invoke-virtual {v15}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    move-wide v12, v4

    move v4, v2

    move-wide v2, v6

    move-wide v6, v10

    .line 2528081
    goto/16 :goto_3

    .line 2528082
    :cond_8
    sget-object v2, LX/I0a;->FUTURE:LX/I0a;

    move-object v3, v2

    goto :goto_4

    .line 2528083
    :cond_9
    sget-object v2, LX/I0b;->SINGLE:LX/I0b;

    goto :goto_5

    .line 2528084
    :cond_a
    cmp-long v2, v16, v6

    if-lez v2, :cond_b

    .line 2528085
    sget-object v2, LX/I0b;->MULTI_DATE_DURING:LX/I0b;

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;->a(LX/I0b;)Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;

    move v2, v4

    goto :goto_6

    .line 2528086
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, LX/I0r;->e:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/TimeZone;

    invoke-static {v2}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v2

    .line 2528087
    move-wide/from16 v0, v16

    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 2528088
    const/16 v12, 0xb

    invoke-virtual {v2, v12}, Ljava/util/Calendar;->get(I)I

    move-result v2

    const/4 v12, 0x6

    if-gt v2, v12, :cond_c

    sget-object v2, LX/I0b;->MULTI_DATE_DURING:LX/I0b;

    :goto_8
    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;->a(LX/I0b;)Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;

    move v2, v4

    goto :goto_6

    :cond_c
    sget-object v2, LX/I0b;->MULTI_DATE_END:LX/I0b;

    goto :goto_8

    .line 2528089
    :cond_d
    new-instance v3, LX/I0X;

    sget-object v4, LX/I0Y;->EVENT_ROW:LX/I0Y;

    move-object/from16 v0, v19

    invoke-direct {v3, v4, v0}, LX/I0X;-><init>(LX/I0Y;Ljava/lang/Object;)V

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 2528090
    :cond_e
    invoke-interface {v14}, LX/7oa;->e()Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/44w;

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-direct {v3, v4, v5}, LX/44w;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0
.end method


# virtual methods
.method public final a()Ljava/util/Calendar;
    .locals 1

    .prologue
    .line 2528034
    iget-object v0, p0, LX/I0r;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/TimeZone;

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    .line 2528035
    invoke-static {v0}, LX/I0r;->a(Ljava/util/Calendar;)V

    .line 2528036
    return-object v0
.end method

.method public final a(Ljava/util/HashMap;J)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/util/List",
            "<",
            "LX/I0X;",
            ">;>;J)",
            "Ljava/util/List",
            "<",
            "LX/I0X;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2528026
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 2528027
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2528028
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2528029
    new-instance v1, LX/I0X;

    sget-object v2, LX/I0Y;->DATE_HEADER:LX/I0Y;

    iget-object v3, p0, LX/I0r;->d:LX/I0g;

    invoke-virtual {v3, p2, p3}, LX/I0g;->a(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, LX/I0X;-><init>(LX/I0Y;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2528030
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2528031
    :cond_1
    :goto_0
    return-object v0

    .line 2528032
    :cond_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/I0X;

    iget-object v1, v1, LX/I0X;->a:LX/I0Y;

    sget-object v2, LX/I0Y;->PRIVATE_INVITES_HSCROLL:LX/I0Y;

    if-ne v1, v2, :cond_1

    .line 2528033
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    new-instance v2, LX/I0X;

    sget-object v3, LX/I0Y;->DATE_HEADER:LX/I0Y;

    iget-object v4, p0, LX/I0r;->d:LX/I0g;

    invoke-virtual {v4, p2, p3}, LX/I0g;->a(J)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, LX/I0X;-><init>(LX/I0Y;Ljava/lang/Object;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;Landroid/text/SpannableStringBuilder;Z)V
    .locals 8

    .prologue
    .line 2527999
    iget-object v0, p1, Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;->a:Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;

    move-object v0, v0

    .line 2528000
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->d()LX/7oa;

    move-result-object v1

    .line 2528001
    new-instance v2, Landroid/text/style/TextAppearanceSpan;

    iget-object v3, p0, LX/I0r;->c:Landroid/content/Context;

    if-nez p3, :cond_0

    .line 2528002
    iget-object v0, p1, Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;->c:LX/I0a;

    move-object v0, v0

    .line 2528003
    sget-object v4, LX/I0a;->PAST:LX/I0a;

    if-ne v0, v4, :cond_3

    :cond_0
    const v0, 0x7f0e08de

    :goto_0
    invoke-direct {v2, v3, v0}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    .line 2528004
    invoke-interface {v1}, LX/7oa;->eQ_()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2528005
    iget-object v0, p1, Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;->b:LX/I0b;

    move-object v0, v0

    .line 2528006
    sget-object v3, LX/I0b;->MULTI_DATE_DURING:LX/I0b;

    if-ne v0, v3, :cond_4

    .line 2528007
    :cond_1
    iget-object v0, p0, LX/I0r;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0821f0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2528008
    :goto_1
    const/4 v0, 0x0

    invoke-virtual {p2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    const/16 v3, 0x11

    invoke-virtual {p2, v2, v0, v1, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2528009
    iget-object v0, p1, Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;->a:Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;

    move-object v0, v0

    .line 2528010
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->d()LX/7oa;

    move-result-object v0

    .line 2528011
    invoke-interface {v0}, LX/7oa;->eQ_()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2528012
    iget-object v1, p1, Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;->b:LX/I0b;

    move-object v1, v1

    .line 2528013
    sget-object v2, LX/I0b;->MULTI_DATE_DURING:LX/I0b;

    if-eq v1, v2, :cond_2

    .line 2528014
    iget-object v1, p1, Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;->b:LX/I0b;

    move-object v1, v1

    .line 2528015
    sget-object v2, LX/I0b;->MULTI_DATE_START:LX/I0b;

    if-eq v1, v2, :cond_2

    invoke-interface {v0}, LX/7oa;->b()J

    move-result-wide v2

    invoke-static {v2, v3}, LX/5O7;->a(J)Z

    move-result v1

    if-nez v1, :cond_6

    .line 2528016
    :cond_2
    :goto_2
    return-void

    .line 2528017
    :cond_3
    const v0, 0x7f0e08dc

    goto :goto_0

    .line 2528018
    :cond_4
    iget-object v0, p1, Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;->b:LX/I0b;

    move-object v0, v0

    .line 2528019
    sget-object v3, LX/I0b;->MULTI_DATE_END:LX/I0b;

    if-ne v0, v3, :cond_5

    .line 2528020
    iget-object v0, p0, LX/I0r;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0821f1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_1

    .line 2528021
    :cond_5
    iget-object v0, p0, LX/I0r;->b:LX/11R;

    sget-object v3, LX/1lB;->HOUR_MINUTE_STYLE:LX/1lB;

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1}, LX/7oa;->j()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    invoke-virtual {v0, v3, v4, v5}, LX/11R;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_1

    .line 2528022
    :cond_6
    const-string v1, "\n"

    invoke-virtual {p2, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2528023
    invoke-virtual {p2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    .line 2528024
    iget-object v2, p0, LX/I0r;->b:LX/11R;

    sget-object v3, LX/1lB;->HOUR_MINUTE_STYLE:LX/1lB;

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0}, LX/7oa;->b()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, LX/11R;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2528025
    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    iget-object v2, p0, LX/I0r;->c:Landroid/content/Context;

    const v3, 0x7f0e08dd

    invoke-direct {v0, v2, v3}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    const/16 v3, 0x11

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_2
.end method

.method public final a(Ljava/util/HashMap;JLX/I0u;LX/I0s;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/util/List",
            "<",
            "LX/I0X;",
            ">;>;J",
            "LX/I0u;",
            "LX/I0s;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2527974
    if-nez p5, :cond_0

    .line 2527975
    :goto_0
    return-void

    .line 2527976
    :cond_0
    const/4 v4, 0x0

    .line 2527977
    invoke-virtual {p0}, LX/I0r;->a()Ljava/util/Calendar;

    move-result-object v6

    .line 2527978
    invoke-virtual {v6}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    .line 2527979
    move-wide/from16 v0, p2

    invoke-virtual {v6, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 2527980
    invoke-virtual {v6}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    .line 2527981
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    .line 2527982
    sget-object v5, LX/I0u;->LOADING_PAST:LX/I0u;

    move-object/from16 v0, p4

    if-eq v0, v5, :cond_1

    sget-object v5, LX/I0u;->LOADING_PAST_AND_FUTURE:LX/I0u;

    move-object/from16 v0, p4

    if-ne v0, v5, :cond_7

    .line 2527983
    :cond_1
    new-instance v4, LX/I0X;

    sget-object v5, LX/I0Y;->LOADING:LX/I0Y;

    const/4 v10, 0x0

    invoke-direct {v4, v5, v10}, LX/I0X;-><init>(LX/I0Y;Ljava/lang/Object;)V

    invoke-virtual {v7, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2527984
    const/4 v4, 0x1

    move-wide v11, v2

    move v3, v4

    move-wide v4, v11

    .line 2527985
    :goto_1
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 2527986
    if-eqz v2, :cond_4

    .line 2527987
    cmp-long v4, v4, v8

    if-nez v4, :cond_2

    .line 2527988
    move-object/from16 v0, p5

    invoke-virtual {v0, v3}, LX/I0s;->a(I)V

    .line 2527989
    :cond_2
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v4, v3

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/I0X;

    .line 2527990
    invoke-virtual {v7, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2527991
    add-int/lit8 v4, v4, 0x1

    .line 2527992
    goto :goto_2

    .line 2527993
    :cond_3
    const/4 v2, 0x6

    const/4 v3, 0x1

    invoke-virtual {v6, v2, v3}, Ljava/util/Calendar;->add(II)V

    .line 2527994
    invoke-virtual {v6}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    move-wide v11, v2

    move v3, v4

    move-wide v4, v11

    .line 2527995
    goto :goto_1

    .line 2527996
    :cond_4
    sget-object v2, LX/I0u;->LOADING_FUTURE:LX/I0u;

    move-object/from16 v0, p4

    if-eq v0, v2, :cond_5

    sget-object v2, LX/I0u;->LOADING_PAST_AND_FUTURE:LX/I0u;

    move-object/from16 v0, p4

    if-ne v0, v2, :cond_6

    .line 2527997
    :cond_5
    new-instance v2, LX/I0X;

    sget-object v3, LX/I0Y;->LOADING:LX/I0Y;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, LX/I0X;-><init>(LX/I0Y;Ljava/lang/Object;)V

    invoke-virtual {v7, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2527998
    :cond_6
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    move-object/from16 v0, p5

    invoke-virtual {v0, v2}, LX/I0s;->a(LX/0Px;)V

    goto/16 :goto_0

    :cond_7
    move-wide v11, v2

    move v3, v4

    move-wide v4, v11

    goto :goto_1
.end method

.method public final a(Ljava/util/HashMap;Ljava/util/HashMap;Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/util/List",
            "<",
            "LX/I0X;",
            ">;>;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/44w",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;>;",
            "Lcom/facebook/events/graphql/EventDashboardGraphQLInterfaces$EventCalendarableItem;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 2527960
    invoke-virtual {p3}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2527961
    if-nez v0, :cond_1

    .line 2527962
    :cond_0
    :goto_0
    return-void

    .line 2527963
    :cond_1
    const-string v1, "EVENT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2527964
    invoke-direct {p0, p1, p2, p3, p4}, LX/I0r;->b(Ljava/util/HashMap;Ljava/util/HashMap;Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;Z)V

    goto :goto_0

    .line 2527965
    :cond_2
    const-string v1, "BIRTHDAY"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2527966
    if-eqz p3, :cond_3

    invoke-virtual {p3}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->c()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2527967
    :cond_3
    :goto_1
    goto :goto_0

    .line 2527968
    :cond_4
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 2527969
    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p3}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->b()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 2527970
    invoke-static {v2}, LX/I0r;->a(Ljava/util/Calendar;)V

    .line 2527971
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {p0, p1, v2, v3}, LX/I0r;->b(LX/I0r;Ljava/util/HashMap;J)Ljava/util/List;

    move-result-object v3

    .line 2527972
    new-instance v4, LX/I0X;

    sget-object v2, LX/I0Y;->BIRTHDAYS:LX/I0Y;

    invoke-direct {v4, v2, p3}, LX/I0X;-><init>(LX/I0Y;Ljava/lang/Object;)V

    .line 2527973
    const/4 v2, 0x0

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/I0X;

    iget-object v2, v2, LX/I0X;->a:LX/I0Y;

    sget-object v5, LX/I0Y;->PRIVATE_INVITES_HSCROLL:LX/I0Y;

    if-ne v2, v5, :cond_5

    const/4 v2, 0x2

    :goto_2
    invoke-interface {v3, v2, v4}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_1

    :cond_5
    const/4 v2, 0x1

    goto :goto_2
.end method
