.class public final LX/HIP;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/HIQ;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/2km;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;

.field public c:I

.field public d:LX/HIT;

.field public e:LX/HIS;

.field public f:LX/HIU;

.field public final synthetic g:LX/HIQ;


# direct methods
.method public constructor <init>(LX/HIQ;)V
    .locals 1

    .prologue
    .line 2451072
    iput-object p1, p0, LX/HIP;->g:LX/HIQ;

    .line 2451073
    move-object v0, p1

    .line 2451074
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2451075
    const/high16 v0, -0x80000000

    iput v0, p0, LX/HIP;->c:I

    .line 2451076
    sget-object v0, Lcom/facebook/pages/common/reaction/components/HScrollPageCardComponentSpec;->a:LX/HIT;

    iput-object v0, p0, LX/HIP;->d:LX/HIT;

    .line 2451077
    sget-object v0, Lcom/facebook/pages/common/reaction/components/HScrollPageCardComponentSpec;->b:LX/HIS;

    iput-object v0, p0, LX/HIP;->e:LX/HIS;

    .line 2451078
    sget-object v0, Lcom/facebook/pages/common/reaction/components/HScrollPageCardComponentSpec;->c:LX/HIU;

    iput-object v0, p0, LX/HIP;->f:LX/HIU;

    .line 2451079
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2451080
    const-string v0, "HScrollPageCardComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2451081
    if-ne p0, p1, :cond_1

    .line 2451082
    :cond_0
    :goto_0
    return v0

    .line 2451083
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2451084
    goto :goto_0

    .line 2451085
    :cond_3
    check-cast p1, LX/HIP;

    .line 2451086
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2451087
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2451088
    if-eq v2, v3, :cond_0

    .line 2451089
    iget-object v2, p0, LX/HIP;->a:LX/2km;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/HIP;->a:LX/2km;

    iget-object v3, p1, LX/HIP;->a:LX/2km;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2451090
    goto :goto_0

    .line 2451091
    :cond_5
    iget-object v2, p1, LX/HIP;->a:LX/2km;

    if-nez v2, :cond_4

    .line 2451092
    :cond_6
    iget-object v2, p0, LX/HIP;->b:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/HIP;->b:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;

    iget-object v3, p1, LX/HIP;->b:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2451093
    goto :goto_0

    .line 2451094
    :cond_8
    iget-object v2, p1, LX/HIP;->b:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;

    if-nez v2, :cond_7

    .line 2451095
    :cond_9
    iget v2, p0, LX/HIP;->c:I

    iget v3, p1, LX/HIP;->c:I

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 2451096
    goto :goto_0

    .line 2451097
    :cond_a
    iget-object v2, p0, LX/HIP;->d:LX/HIT;

    if-eqz v2, :cond_c

    iget-object v2, p0, LX/HIP;->d:LX/HIT;

    iget-object v3, p1, LX/HIP;->d:LX/HIT;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    :cond_b
    move v0, v1

    .line 2451098
    goto :goto_0

    .line 2451099
    :cond_c
    iget-object v2, p1, LX/HIP;->d:LX/HIT;

    if-nez v2, :cond_b

    .line 2451100
    :cond_d
    iget-object v2, p0, LX/HIP;->e:LX/HIS;

    if-eqz v2, :cond_f

    iget-object v2, p0, LX/HIP;->e:LX/HIS;

    iget-object v3, p1, LX/HIP;->e:LX/HIS;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    :cond_e
    move v0, v1

    .line 2451101
    goto :goto_0

    .line 2451102
    :cond_f
    iget-object v2, p1, LX/HIP;->e:LX/HIS;

    if-nez v2, :cond_e

    .line 2451103
    :cond_10
    iget-object v2, p0, LX/HIP;->f:LX/HIU;

    if-eqz v2, :cond_11

    iget-object v2, p0, LX/HIP;->f:LX/HIU;

    iget-object v3, p1, LX/HIP;->f:LX/HIU;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2451104
    goto :goto_0

    .line 2451105
    :cond_11
    iget-object v2, p1, LX/HIP;->f:LX/HIU;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
