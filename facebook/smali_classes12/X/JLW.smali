.class public LX/JLW;
.super LX/5pb;
.source ""

# interfaces
.implements LX/5on;


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "FBMarketplaceComposerBridgeModule"
.end annotation


# instance fields
.field private final a:Lcom/facebook/content/SecureContextHelper;

.field private final b:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

.field private final c:LX/17Y;

.field private final d:LX/8LV;

.field public final e:LX/1EZ;

.field private final f:Ljava/util/concurrent/ExecutorService;

.field private final g:LX/JLi;

.field private h:LX/5pG;

.field private i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private j:J

.field private k:J

.field public l:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

.field private m:Ljava/lang/String;

.field private n:Z

.field public o:Lcom/facebook/photos/upload/operation/UploadOperation;

.field public p:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation
.end field

.field private q:Z


# direct methods
.method public constructor <init>(LX/5pY;Ljava/util/concurrent/ExecutorService;LX/17Y;Lcom/facebook/content/SecureContextHelper;LX/1EZ;LX/8LV;Lcom/facebook/composer/publish/ComposerPublishServiceHelper;LX/JLi;)V
    .locals 0
    .param p1    # LX/5pY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2681239
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 2681240
    iput-object p4, p0, LX/JLW;->a:Lcom/facebook/content/SecureContextHelper;

    .line 2681241
    iput-object p7, p0, LX/JLW;->b:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    .line 2681242
    iput-object p3, p0, LX/JLW;->c:LX/17Y;

    .line 2681243
    iput-object p6, p0, LX/JLW;->d:LX/8LV;

    .line 2681244
    iput-object p5, p0, LX/JLW;->e:LX/1EZ;

    .line 2681245
    iput-object p2, p0, LX/JLW;->f:Ljava/util/concurrent/ExecutorService;

    .line 2681246
    iput-object p8, p0, LX/JLW;->g:LX/JLi;

    .line 2681247
    invoke-virtual {p1, p0}, LX/5pX;->a(LX/5on;)V

    .line 2681248
    return-void
.end method

.method private a(LX/5pG;)LX/5Rj;
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 2681202
    new-instance v0, LX/5Rj;

    invoke-direct {v0}, LX/5Rj;-><init>()V

    const-string v1, "title"

    invoke-interface {p1, v1}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2681203
    iput-object v1, v0, LX/5Rj;->a:Ljava/lang/String;

    .line 2681204
    move-object v0, v0

    .line 2681205
    const-string v1, "description"

    invoke-interface {p1, v1}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2681206
    iput-object v1, v0, LX/5Rj;->d:Ljava/lang/String;

    .line 2681207
    move-object v0, v0

    .line 2681208
    const-string v1, "currency"

    invoke-interface {p1, v1}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2681209
    iput-object v1, v0, LX/5Rj;->f:Ljava/lang/String;

    .line 2681210
    move-object v0, v0

    .line 2681211
    const-string v1, "categoryID"

    invoke-interface {p1, v1}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2681212
    iput-object v1, v0, LX/5Rj;->g:Ljava/lang/String;

    .line 2681213
    move-object v0, v0

    .line 2681214
    iput-boolean v2, v0, LX/5Rj;->k:Z

    .line 2681215
    move-object v0, v0

    .line 2681216
    iput-boolean v2, v0, LX/5Rj;->l:Z

    .line 2681217
    move-object v0, v0

    .line 2681218
    iput-boolean v2, v0, LX/5Rj;->m:Z

    .line 2681219
    move-object v0, v0

    .line 2681220
    const-string v1, "zipcode"

    invoke-interface {p1, v1}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2681221
    const-string v1, "zipcode"

    invoke-interface {p1, v1}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2681222
    iput-object v1, v0, LX/5Rj;->b:Ljava/lang/String;

    .line 2681223
    :cond_0
    :goto_0
    const-string v1, "price"

    invoke-interface {p1, v1}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "price"

    invoke-interface {p1, v1}, LX/5pG;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 2681224
    const-string v1, "price"

    invoke-interface {p1, v1}, LX/5pG;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 2681225
    iput-object v1, v0, LX/5Rj;->e:Ljava/lang/Long;

    .line 2681226
    :cond_1
    :goto_1
    const-string v1, "draftType"

    invoke-interface {p1, v1}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2681227
    const-string v1, "draftType"

    invoke-interface {p1, v1}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2681228
    iput-object v1, v0, LX/5Rj;->h:Ljava/lang/String;

    .line 2681229
    :cond_2
    return-object v0

    .line 2681230
    :cond_3
    const-string v1, "latitude"

    invoke-interface {p1, v1}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "longitude"

    invoke-interface {p1, v1}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2681231
    const-string v1, "latitude"

    invoke-interface {p1, v1}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    .line 2681232
    iput-object v1, v0, LX/5Rj;->i:Ljava/lang/Double;

    .line 2681233
    move-object v1, v0

    .line 2681234
    const-string v2, "longitude"

    invoke-interface {p1, v2}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    .line 2681235
    iput-object v2, v1, LX/5Rj;->j:Ljava/lang/Double;

    .line 2681236
    goto :goto_0

    .line 2681237
    :cond_4
    const-string v1, "draftType"

    invoke-interface {p1, v1}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2681238
    invoke-virtual {p0}, LX/JLW;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Must have draft type to have a null price"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static synthetic a(LX/JLW;Lcom/facebook/ipc/composer/model/ProductItemAttachment;)Lcom/facebook/photos/upload/operation/UploadOperation;
    .locals 1

    .prologue
    .line 2681201
    invoke-direct {p0, p1}, LX/JLW;->a(Lcom/facebook/ipc/composer/model/ProductItemAttachment;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/facebook/ipc/composer/model/ProductItemAttachment;)Lcom/facebook/photos/upload/operation/UploadOperation;
    .locals 26

    .prologue
    .line 2681194
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 2681195
    move-object/from16 v0, p0

    iget-object v2, v0, LX/JLW;->i:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, LX/JLW;->i:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/ipc/media/MediaItem;

    .line 2681196
    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2681197
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 2681198
    :cond_0
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 2681199
    move-object/from16 v0, p0

    iget-object v2, v0, LX/JLW;->d:LX/8LV;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-wide v5, v0, LX/JLW;->j:J

    sget-object v7, LX/2rw;->MARKETPLACE:LX/2rw;

    invoke-virtual {v7}, LX/2rw;->toString()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->title:Ljava/lang/String;

    sget-object v9, Lcom/facebook/ipc/composer/model/MinutiaeTag;->a:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    const/4 v10, 0x0

    new-instance v11, LX/0Pz;

    invoke-direct {v11}, LX/0Pz;-><init>()V

    invoke-virtual {v11}, LX/0Pz;->b()LX/0Px;

    move-result-object v11

    const-wide/16 v12, -0x1

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/JLW;->m:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, LX/JLW;->j:J

    move-wide/from16 v20, v0

    const-string v22, "marketplace"

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    move-object/from16 v19, p1

    invoke-virtual/range {v2 .. v25}, LX/8LV;->a(LX/0Px;LX/0Px;JLjava/lang/String;Ljava/lang/String;Lcom/facebook/ipc/composer/model/MinutiaeTag;Ljava/lang/String;LX/0Px;JZLjava/lang/String;ZZLjava/lang/String;Lcom/facebook/ipc/composer/model/ProductItemAttachment;JLjava/lang/String;ZLcom/facebook/composer/publish/common/model/ComposerSessionLoggingData;Z)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v2

    .line 2681200
    return-object v2
.end method

.method private a(ILandroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2681172
    if-nez p1, :cond_1

    .line 2681173
    :cond_0
    :goto_0
    return-void

    .line 2681174
    :cond_1
    const-string v0, "extra_media_items"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/JLW;->i:Ljava/util/ArrayList;

    .line 2681175
    iget-object v0, p0, LX/JLW;->i:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/JLW;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2681176
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2681177
    iget-object v0, p0, LX/JLW;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v2

    :goto_1
    if-ge v1, v4, :cond_2

    iget-object v0, p0, LX/JLW;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 2681178
    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2681179
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2681180
    :cond_2
    const-string v0, ","

    invoke-static {v0, v3}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    .line 2681181
    iget-boolean v1, p0, LX/JLW;->n:Z

    if-eqz v1, :cond_3

    .line 2681182
    sget-object v1, LX/0ax;->hP:Ljava/lang/String;

    iget-wide v4, p0, LX/JLW;->k:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v1, v0, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2681183
    iget-object v3, p0, LX/JLW;->c:LX/17Y;

    .line 2681184
    iget-object v4, p0, LX/5pb;->a:LX/5pY;

    move-object v4, v4

    .line 2681185
    invoke-interface {v3, v4, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2681186
    const/high16 v3, 0x14000000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2681187
    iget-object v3, p0, LX/JLW;->a:Lcom/facebook/content/SecureContextHelper;

    .line 2681188
    iget-object v4, p0, LX/5pb;->a:LX/5pY;

    move-object v4, v4

    .line 2681189
    invoke-interface {v3, v1, v4}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2681190
    iput-boolean v2, p0, LX/JLW;->n:Z

    .line 2681191
    :cond_3
    new-instance v1, Lcom/facebook/react/bridge/WritableNativeMap;

    invoke-direct {v1}, Lcom/facebook/react/bridge/WritableNativeMap;-><init>()V

    .line 2681192
    const-string v2, "assetIDs"

    invoke-virtual {v1, v2, v0}, Lcom/facebook/react/bridge/WritableNativeMap;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2681193
    const-string v0, "MarketplaceComposerDidSelectMedia"

    invoke-direct {p0, v0, v1}, LX/JLW;->a(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2681083
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2681084
    const-class v1, Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;

    invoke-virtual {v0, v1}, LX/5pX;->a(Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;

    invoke-interface {v0, p1, p2}, Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;->emit(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2681085
    return-void
.end method

.method private h()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 2681163
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/JLW;->j:J

    .line 2681164
    iput-object v2, p0, LX/JLW;->i:Ljava/util/ArrayList;

    .line 2681165
    iput-object v2, p0, LX/JLW;->l:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    .line 2681166
    iput-object v2, p0, LX/JLW;->m:Ljava/lang/String;

    .line 2681167
    iput-object v2, p0, LX/JLW;->h:LX/5pG;

    .line 2681168
    iput-boolean v3, p0, LX/JLW;->n:Z

    .line 2681169
    iput-object v2, p0, LX/JLW;->o:Lcom/facebook/photos/upload/operation/UploadOperation;

    .line 2681170
    iput-boolean v3, p0, LX/JLW;->q:Z

    .line 2681171
    return-void
.end method


# virtual methods
.method public final a(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2681159
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    if-eqz p2, :cond_0

    .line 2681160
    :goto_0
    return-void

    .line 2681161
    :cond_0
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 2681162
    :pswitch_0
    invoke-direct {p0, p2, p3}, LX/JLW;->a(ILandroid/content/Intent;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2714
        :pswitch_0
    .end packed-switch
.end method

.method public cancelComposer()V
    .locals 4
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2681151
    iget-object v0, p0, LX/JLW;->p:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 2681152
    iget-object v0, p0, LX/JLW;->p:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2681153
    iput-object v3, p0, LX/JLW;->p:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2681154
    :cond_0
    iget-object v0, p0, LX/JLW;->o:Lcom/facebook/photos/upload/operation/UploadOperation;

    if-eqz v0, :cond_1

    .line 2681155
    iget-object v0, p0, LX/JLW;->f:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/fbreact/marketplace/FBMarketplaceComposerBridgeModule$4;

    invoke-direct {v1, p0}, Lcom/facebook/fbreact/marketplace/FBMarketplaceComposerBridgeModule$4;-><init>(LX/JLW;)V

    const v2, 0x12f7457c

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2681156
    :cond_1
    const-string v0, "MarketplaceComposerCancel"

    invoke-direct {p0, v0, v3}, LX/JLW;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2681157
    invoke-virtual {p0}, LX/JLW;->dismissComposer()V

    .line 2681158
    return-void
.end method

.method public dismissComposer()V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2681249
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2681250
    invoke-virtual {v0}, LX/5pX;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2681251
    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2681252
    :cond_0
    invoke-direct {p0}, LX/JLW;->h()V

    .line 2681253
    return-void
.end method

.method public editMarketplacePost(LX/5pG;Lcom/facebook/react/bridge/Callback;Lcom/facebook/react/bridge/Callback;)V
    .locals 3
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2681141
    invoke-direct {p0, p1}, LX/JLW;->a(LX/5pG;)LX/5Rj;

    move-result-object v0

    invoke-virtual {v0}, LX/5Rj;->a()Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    move-result-object v0

    .line 2681142
    invoke-static {}, Lcom/facebook/composer/publish/common/EditPostParams;->newBuilder()Lcom/facebook/composer/publish/common/EditPostParams$Builder;

    move-result-object v1

    iget-object v2, p0, LX/JLW;->m:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->setComposerSessionId(Ljava/lang/String;)Lcom/facebook/composer/publish/common/EditPostParams$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->setProductItemAttachment(Lcom/facebook/ipc/composer/model/ProductItemAttachment;)Lcom/facebook/composer/publish/common/EditPostParams$Builder;

    move-result-object v0

    const-string v1, "storyID"

    invoke-interface {p1, v1}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->setStoryId(Ljava/lang/String;)Lcom/facebook/composer/publish/common/EditPostParams$Builder;

    move-result-object v0

    const-string v1, "storyID"

    invoke-interface {p1, v1}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->setLegacyStoryApiId(Ljava/lang/String;)Lcom/facebook/composer/publish/common/EditPostParams$Builder;

    move-result-object v0

    .line 2681143
    iget-boolean v1, p0, LX/JLW;->q:Z

    if-eqz v1, :cond_0

    .line 2681144
    sget-object v1, LX/21D;->SELLER_CENTRAL:LX/21D;

    invoke-virtual {v0, v1}, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->setSourceType(LX/21D;)Lcom/facebook/composer/publish/common/EditPostParams$Builder;

    .line 2681145
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/EditPostParams$Builder;->a()Lcom/facebook/composer/publish/common/EditPostParams;

    move-result-object v0

    .line 2681146
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2681147
    const-string v2, "publishEditPostParamsKey"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2681148
    iget-object v0, p0, LX/JLW;->b:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-virtual {v0, v1}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/JLW;->p:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2681149
    iget-object v0, p0, LX/JLW;->p:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, LX/JLV;

    invoke-direct {v1, p0, p2, p3}, LX/JLV;-><init>(LX/JLW;Lcom/facebook/react/bridge/Callback;Lcom/facebook/react/bridge/Callback;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2681150
    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2681140
    const-string v0, "FBMarketplaceComposerBridgeModule"

    return-object v0
.end method

.method public launchComposerWithMarketplace(Ljava/lang/String;Ljava/lang/String;IZ)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2681136
    invoke-virtual {p0, p1, p2, p3, p4}, LX/JLW;->startSessionWithMarketplace(Ljava/lang/String;Ljava/lang/String;IZ)V

    .line 2681137
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/JLW;->n:Z

    .line 2681138
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/JLW;->openPhotoPickerWithActionTitle(Ljava/lang/String;)V

    .line 2681139
    return-void
.end method

.method public launchEditComposerForPost(Ljava/lang/String;ILjava/lang/String;ZZ)V
    .locals 3
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2681124
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/JLW;->m:Ljava/lang/String;

    .line 2681125
    sget-object v1, LX/0ax;->hQ:Ljava/lang/String;

    if-eqz p4, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v1, p1, p3, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2681126
    iget-object v1, p0, LX/JLW;->c:LX/17Y;

    .line 2681127
    iget-object v2, p0, LX/5pb;->a:LX/5pY;

    move-object v2, v2

    .line 2681128
    invoke-interface {v1, v2, v0}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2681129
    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2681130
    iget-object v1, p0, LX/JLW;->a:Lcom/facebook/content/SecureContextHelper;

    .line 2681131
    iget-object v2, p0, LX/5pb;->a:LX/5pY;

    move-object v2, v2

    .line 2681132
    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2681133
    iput-boolean p5, p0, LX/JLW;->q:Z

    .line 2681134
    return-void

    .line 2681135
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public maybePrefillComposerData()V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2681119
    iget-object v0, p0, LX/JLW;->h:LX/5pG;

    if-eqz v0, :cond_0

    .line 2681120
    new-instance v0, Lcom/facebook/react/bridge/WritableNativeMap;

    invoke-direct {v0}, Lcom/facebook/react/bridge/WritableNativeMap;-><init>()V

    .line 2681121
    iget-object v1, p0, LX/JLW;->h:LX/5pG;

    invoke-virtual {v0, v1}, Lcom/facebook/react/bridge/WritableNativeMap;->a(LX/5pG;)V

    .line 2681122
    const-string v1, "PrefillMarketplaceComposerData"

    invoke-direct {p0, v1, v0}, LX/JLW;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2681123
    :cond_0
    return-void
.end method

.method public openPhotoPickerWithActionTitle(Ljava/lang/String;)V
    .locals 4
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2681112
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v1, v0

    .line 2681113
    new-instance v0, LX/8AA;

    sget-object v2, LX/8AB;->MARKETPLACE:LX/8AB;

    invoke-direct {v0, v2}, LX/8AA;-><init>(LX/8AB;)V

    invoke-virtual {v0}, LX/8AA;->l()LX/8AA;

    move-result-object v0

    invoke-virtual {v0}, LX/8AA;->o()LX/8AA;

    move-result-object v0

    invoke-virtual {v0}, LX/8AA;->d()LX/8AA;

    move-result-object v0

    invoke-virtual {v0}, LX/8AA;->j()LX/8AA;

    move-result-object v0

    invoke-virtual {v0}, LX/8AA;->a()LX/8AA;

    move-result-object v0

    sget-object v2, LX/8A9;->NONE:LX/8A9;

    invoke-virtual {v0, v2}, LX/8AA;->a(LX/8A9;)LX/8AA;

    move-result-object v0

    const/4 v2, 0x1

    const/16 v3, 0xa

    invoke-virtual {v0, v2, v3}, LX/8AA;->b(II)LX/8AA;

    move-result-object v2

    iget-object v0, p0, LX/JLW;->i:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/JLW;->i:Ljava/util/ArrayList;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, LX/8AA;->a(LX/0Px;)LX/8AA;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->a(Landroid/content/Context;LX/8AA;)Landroid/content/Intent;

    move-result-object v0

    .line 2681114
    iget-object v1, p0, LX/5pb;->a:LX/5pY;

    move-object v1, v1

    .line 2681115
    const/16 v2, 0x2714

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, LX/5pX;->a(Landroid/content/Intent;ILandroid/os/Bundle;)Z

    .line 2681116
    return-void

    .line 2681117
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2681118
    goto :goto_0
.end method

.method public returnToMediaPicker(LX/5pG;)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2681106
    iput-object p1, p0, LX/JLW;->h:LX/5pG;

    .line 2681107
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2681108
    invoke-virtual {v0}, LX/5pX;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2681109
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/JLW;->n:Z

    .line 2681110
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/JLW;->openPhotoPickerWithActionTitle(Ljava/lang/String;)V

    .line 2681111
    :cond_0
    return-void
.end method

.method public startSessionWithMarketplace(Ljava/lang/String;Ljava/lang/String;IZ)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2681099
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2681100
    invoke-virtual {v0}, LX/5pX;->i()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2681101
    :goto_0
    return-void

    .line 2681102
    :cond_0
    invoke-direct {p0}, LX/JLW;->h()V

    .line 2681103
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/JLW;->m:Ljava/lang/String;

    .line 2681104
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, LX/JLW;->j:J

    .line 2681105
    if-nez p2, :cond_1

    const-wide/16 v0, 0x0

    :goto_1
    iput-wide v0, p0, LX/JLW;->k:J

    goto :goto_0

    :cond_1
    invoke-static {p2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_1
.end method

.method public submitMarketplacePost(LX/5pG;Lcom/facebook/react/bridge/Callback;Lcom/facebook/react/bridge/Callback;)V
    .locals 8
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2681086
    invoke-direct {p0, p1}, LX/JLW;->a(LX/5pG;)LX/5Rj;

    move-result-object v0

    .line 2681087
    invoke-virtual {v0}, LX/5Rj;->a()Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    move-result-object v0

    iput-object v0, p0, LX/JLW;->l:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    .line 2681088
    iget-object v0, p0, LX/JLW;->g:LX/JLi;

    iget-object v1, p0, LX/JLW;->m:Ljava/lang/String;

    new-instance v2, LX/JLU;

    invoke-direct {v2, p0, p2, p3}, LX/JLU;-><init>(LX/JLW;Lcom/facebook/react/bridge/Callback;Lcom/facebook/react/bridge/Callback;)V

    .line 2681089
    iget-object v3, v0, LX/JLi;->d:LX/0b3;

    iget-object v4, v0, LX/JLi;->b:LX/JLh;

    invoke-virtual {v3, v4}, LX/0b4;->a(LX/0b2;)Z

    .line 2681090
    iget-object v3, v0, LX/JLi;->d:LX/0b3;

    iget-object v4, v0, LX/JLi;->c:LX/JLg;

    invoke-virtual {v3, v4}, LX/0b4;->a(LX/0b2;)Z

    .line 2681091
    iput-object v1, v0, LX/JLi;->f:Ljava/lang/String;

    .line 2681092
    iput-object v2, v0, LX/JLi;->a:LX/JLU;

    .line 2681093
    iget-object v3, v0, LX/JLi;->g:Ljava/lang/Runnable;

    if-eqz v3, :cond_0

    .line 2681094
    iget-object v3, v0, LX/JLi;->e:Landroid/os/Handler;

    iget-object v4, v0, LX/JLi;->g:Ljava/lang/Runnable;

    invoke-static {v3, v4}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 2681095
    :cond_0
    new-instance v3, Lcom/facebook/fbreact/marketplace/MarketplacePostUploadReceiver$1;

    invoke-direct {v3, v0}, Lcom/facebook/fbreact/marketplace/MarketplacePostUploadReceiver$1;-><init>(LX/JLi;)V

    iput-object v3, v0, LX/JLi;->g:Ljava/lang/Runnable;

    .line 2681096
    iget-object v3, v0, LX/JLi;->e:Landroid/os/Handler;

    iget-object v4, v0, LX/JLi;->g:Ljava/lang/Runnable;

    const-wide/32 v5, 0x36ee80

    const v7, -0xe9e7950

    invoke-static {v3, v4, v5, v6, v7}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2681097
    iget-object v0, p0, LX/JLW;->f:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/fbreact/marketplace/FBMarketplaceComposerBridgeModule$2;

    invoke-direct {v1, p0}, Lcom/facebook/fbreact/marketplace/FBMarketplaceComposerBridgeModule$2;-><init>(LX/JLW;)V

    const v2, -0x5f3b31fb

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2681098
    return-void
.end method
