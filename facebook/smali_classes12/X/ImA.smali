.class public LX/ImA;
.super LX/7GD;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/ImA;


# instance fields
.field public final a:LX/7GS;

.field private final b:LX/7GW;

.field public final c:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;LX/2Hu;LX/01T;LX/7GA;LX/0Ot;LX/0Or;LX/7GS;LX/7GW;Ljava/lang/Integer;)V
    .locals 9
    .param p7    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
        .end annotation
    .end param
    .param p10    # Ljava/lang/Integer;
        .annotation runtime Lcom/facebook/messaging/payment/sync/annotations/PaymentsSyncApiVersion;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Lcom/facebook/push/mqtt/service/MqttPushServiceClientManager;",
            "LX/01T;",
            "LX/7GA;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/7GS;",
            "LX/7GW;",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2608992
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p7

    move-object v7, p5

    move-object v8, p6

    invoke-direct/range {v1 .. v8}, LX/7GD;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;LX/2Hu;LX/01T;LX/0Or;LX/7GA;LX/0Ot;)V

    .line 2608993
    move-object/from16 v0, p8

    iput-object v0, p0, LX/ImA;->a:LX/7GS;

    .line 2608994
    move-object/from16 v0, p9

    iput-object v0, p0, LX/ImA;->b:LX/7GW;

    .line 2608995
    move-object/from16 v0, p10

    iput-object v0, p0, LX/ImA;->c:Ljava/lang/Integer;

    .line 2608996
    return-void
.end method

.method public static a(LX/0QB;)LX/ImA;
    .locals 14

    .prologue
    .line 2608979
    sget-object v0, LX/ImA;->d:LX/ImA;

    if-nez v0, :cond_1

    .line 2608980
    const-class v1, LX/ImA;

    monitor-enter v1

    .line 2608981
    :try_start_0
    sget-object v0, LX/ImA;->d:LX/ImA;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2608982
    if-eqz v2, :cond_0

    .line 2608983
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2608984
    new-instance v3, LX/ImA;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-static {v0}, LX/2Hu;->a(LX/0QB;)LX/2Hu;

    move-result-object v6

    check-cast v6, LX/2Hu;

    invoke-static {v0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v7

    check-cast v7, LX/01T;

    invoke-static {v0}, LX/7GA;->a(LX/0QB;)LX/7GA;

    move-result-object v8

    check-cast v8, LX/7GA;

    const/16 v9, 0x259

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x15e8

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    invoke-static {v0}, LX/7GS;->a(LX/0QB;)LX/7GS;

    move-result-object v11

    check-cast v11, LX/7GS;

    invoke-static {v0}, LX/7GW;->a(LX/0QB;)LX/7GW;

    move-result-object v12

    check-cast v12, LX/7GW;

    invoke-static {v0}, LX/Im4;->b(LX/0QB;)Ljava/lang/Integer;

    move-result-object v13

    check-cast v13, Ljava/lang/Integer;

    invoke-direct/range {v3 .. v13}, LX/ImA;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;LX/2Hu;LX/01T;LX/7GA;LX/0Ot;LX/0Or;LX/7GS;LX/7GW;Ljava/lang/Integer;)V

    .line 2608985
    move-object v0, v3

    .line 2608986
    sput-object v0, LX/ImA;->d:LX/ImA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2608987
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2608988
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2608989
    :cond_1
    sget-object v0, LX/ImA;->d:LX/ImA;

    return-object v0

    .line 2608990
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2608991
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/7GT;
    .locals 1

    .prologue
    .line 2608978
    sget-object v0, LX/7GT;->PAYMENTS_QUEUE_TYPE:LX/7GT;

    return-object v0
.end method
