.class public LX/JVl;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/productminilist/ProductMiniCardComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JVl",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/productminilist/ProductMiniCardComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2701558
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2701559
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JVl;->b:LX/0Zi;

    .line 2701560
    iput-object p1, p0, LX/JVl;->a:LX/0Ot;

    .line 2701561
    return-void
.end method

.method public static a(LX/0QB;)LX/JVl;
    .locals 4

    .prologue
    .line 2701445
    const-class v1, LX/JVl;

    monitor-enter v1

    .line 2701446
    :try_start_0
    sget-object v0, LX/JVl;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2701447
    sput-object v2, LX/JVl;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2701448
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2701449
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2701450
    new-instance v3, LX/JVl;

    const/16 p0, 0x20a9

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JVl;-><init>(LX/0Ot;)V

    .line 2701451
    move-object v0, v3

    .line 2701452
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2701453
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JVl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2701454
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2701455
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Landroid/view/View;LX/1X1;)V
    .locals 13

    .prologue
    .line 2701493
    check-cast p2, LX/JVk;

    .line 2701494
    iget-object v0, p0, LX/JVl;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/productminilist/ProductMiniCardComponentSpec;

    iget-object v1, p2, LX/JVk;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2701495
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2701496
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v5

    .line 2701497
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2701498
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v12

    .line 2701499
    if-nez v5, :cond_3

    .line 2701500
    const/4 v3, 0x0

    .line 2701501
    :goto_0
    move-object v8, v3

    .line 2701502
    iget-object v3, v0, Lcom/facebook/feedplugins/productminilist/ProductMiniCardComponentSpec;->d:LX/7iV;

    invoke-static {v1}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v6

    const v9, 0xa7c5482

    .line 2701503
    if-nez v5, :cond_5

    .line 2701504
    const/4 v4, 0x0

    .line 2701505
    :goto_1
    move-object v7, v4

    .line 2701506
    sget-object v9, LX/7iN;->COMMERCE_NEWS_FEED:LX/7iN;

    .line 2701507
    iget-object v4, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v4, v4

    .line 2701508
    invoke-static {v4}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v10

    .line 2701509
    iget-object v4, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v4, v4

    .line 2701510
    :goto_2
    if-eqz v4, :cond_a

    .line 2701511
    iget-object v11, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v11, v11

    .line 2701512
    instance-of v11, v11, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v11, :cond_9

    .line 2701513
    if-eqz v4, :cond_9

    .line 2701514
    iget-object v11, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v11, v11

    .line 2701515
    if-eqz v11, :cond_9

    .line 2701516
    iget-object v11, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v11, v11

    .line 2701517
    check-cast v11, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v11}, Lcom/facebook/graphql/model/GraphQLStory;->aJ()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v11

    if-eqz v11, :cond_9

    .line 2701518
    iget-object v11, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v11, v11

    .line 2701519
    check-cast v11, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v11}, Lcom/facebook/graphql/model/GraphQLStory;->aJ()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->r()Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_9

    .line 2701520
    iget-object v11, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v11, v11

    .line 2701521
    check-cast v11, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v11}, Lcom/facebook/graphql/model/GraphQLStory;->aJ()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/graphql/model/GraphQLSponsoredData;->r()Ljava/lang/String;

    move-result-object v11

    .line 2701522
    :goto_3
    move-object v11, v11

    .line 2701523
    move-object v4, p1

    invoke-virtual/range {v3 .. v11}, LX/7iV;->a(Landroid/view/View;Lcom/facebook/graphql/model/GraphQLNode;LX/162;LX/7iM;LX/7iP;LX/7iN;ZLjava/lang/String;)V

    .line 2701524
    if-nez v5, :cond_1

    .line 2701525
    :cond_0
    :goto_4
    return-void

    .line 2701526
    :cond_1
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v3

    .line 2701527
    const/4 v6, 0x0

    .line 2701528
    invoke-virtual {v12}, LX/0Px;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_b

    move-object v4, v6

    .line 2701529
    :goto_5
    move-object v4, v4

    .line 2701530
    const v6, 0xa7c5482

    if-ne v3, v6, :cond_2

    .line 2701531
    iget-object v3, v0, Lcom/facebook/feedplugins/productminilist/ProductMiniCardComponentSpec;->e:LX/7j7;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    .line 2701532
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLNode;->oj()Z

    move-result v7

    if-eqz v7, :cond_d

    .line 2701533
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLNode;->ga()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7, v6}, LX/7j7;->a(LX/7j7;Ljava/lang/String;Landroid/content/Context;)V

    .line 2701534
    :goto_6
    goto :goto_4

    .line 2701535
    :cond_2
    const v6, 0x25d6af

    if-ne v3, v6, :cond_0

    .line 2701536
    iget-object v3, v0, Lcom/facebook/feedplugins/productminilist/ProductMiniCardComponentSpec;->e:LX/7j7;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    .line 2701537
    if-eqz v4, :cond_10

    .line 2701538
    const-string v7, "https://www.facebook.com/"

    invoke-virtual {v4, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_f

    .line 2701539
    iget-object v7, v3, LX/7j7;->b:LX/17Y;

    sget-object v9, LX/0ax;->aE:Ljava/lang/String;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v6, v9}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v7

    .line 2701540
    iget-object v9, v3, LX/7j7;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v9, v7, v6}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2701541
    :goto_7
    goto :goto_4

    :cond_3
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v3

    const v4, 0xa7c5482

    if-ne v3, v4, :cond_4

    sget-object v3, LX/7iP;->MINI_PRODUCT_CARD:LX/7iP;

    goto/16 :goto_0

    :cond_4
    sget-object v3, LX/7iP;->MINI_PRODUCT_END_CARD:LX/7iP;

    goto/16 :goto_0

    .line 2701542
    :cond_5
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v7

    .line 2701543
    invoke-virtual {v12}, LX/0Px;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_7

    .line 2701544
    const/4 v4, 0x0

    invoke-virtual {v12, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 2701545
    if-eqz v4, :cond_7

    .line 2701546
    if-ne v7, v9, :cond_6

    sget-object v4, LX/7iM;->NON_PDP_PRODUCT_TAG_CLICK:LX/7iM;

    goto/16 :goto_1

    :cond_6
    sget-object v4, LX/7iM;->NON_STORE_PRODUCT_MINI_END_CARD_CLICK:LX/7iM;

    goto/16 :goto_1

    .line 2701547
    :cond_7
    if-ne v7, v9, :cond_8

    sget-object v4, LX/7iM;->PDP_PRODUCT_TAG_CLICK:LX/7iM;

    goto/16 :goto_1

    :cond_8
    sget-object v4, LX/7iM;->STORE_PRODUCT_MINI_END_CARD_CLICK:LX/7iM;

    goto/16 :goto_1

    .line 2701548
    :cond_9
    iget-object v11, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v4, v11

    .line 2701549
    goto/16 :goto_2

    .line 2701550
    :cond_a
    const/4 v11, 0x0

    goto/16 :goto_3

    .line 2701551
    :cond_b
    const/4 v4, 0x0

    invoke-virtual {v12, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 2701552
    if-eqz v4, :cond_c

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_5

    :cond_c
    move-object v4, v6

    goto/16 :goto_5

    .line 2701553
    :cond_d
    if-eqz v4, :cond_e

    .line 2701554
    invoke-static {v3, v4, v6}, LX/7j7;->b(LX/7j7;Ljava/lang/String;Landroid/content/Context;)V

    goto/16 :goto_6

    .line 2701555
    :cond_e
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7, v8}, LX/7j7;->a(LX/7j7;Ljava/lang/String;LX/7iP;)V

    goto/16 :goto_6

    .line 2701556
    :cond_f
    invoke-static {v3, v4, v6}, LX/7j7;->b(LX/7j7;Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_7

    .line 2701557
    :cond_10
    iget-object v7, v3, LX/7j7;->c:LX/7j6;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v7, v9, v10, v11, v8}, LX/7j6;->a(Ljava/lang/String;Ljava/lang/String;ZLX/7iP;)V

    goto :goto_7
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 2701462
    check-cast p2, LX/JVk;

    .line 2701463
    iget-object v0, p0, LX/JVl;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/productminilist/ProductMiniCardComponentSpec;

    iget-object v1, p2, LX/JVk;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v6, 0x4

    .line 2701464
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 2701465
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2701466
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x2

    invoke-interface {v3, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v6}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    const v4, 0x7f020a3c

    invoke-interface {v3, v4}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v3

    const v4, 0x7f0b0a3d

    invoke-interface {v3, v4}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v3

    const/16 v4, 0x8

    const v5, 0x7f0b00bf

    invoke-interface {v3, v4, v5}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v3

    const/4 v5, 0x0

    const/4 p2, 0x1

    .line 2701467
    iget-object v4, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 2701468
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v4

    .line 2701469
    if-nez v4, :cond_1

    .line 2701470
    :goto_0
    move-object v4, v5

    .line 2701471
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v6}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v4, v5}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x6

    const v6, 0x7f0b00d2

    invoke-interface {v4, v5, v6}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v4

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-interface {v4, v5}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v4

    const/4 v6, 0x1

    .line 2701472
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    sget-object v7, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v5, v7}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v7

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    if-nez v5, :cond_3

    const/4 v5, 0x2

    :goto_1
    invoke-virtual {v7, v5}, LX/1ne;->j(I)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v6}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v5

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    const v6, 0x7f0b004c

    invoke-virtual {v5, v6}, LX/1ne;->q(I)LX/1ne;

    move-result-object v5

    move-object v5, v5

    .line 2701473
    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    const/4 v7, 0x1

    .line 2701474
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v5, v6}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v7}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v7}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v5

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->y()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    const v6, 0x7f0a043b

    invoke-virtual {v5, v6}, LX/1ne;->n(I)LX/1ne;

    move-result-object v5

    const v6, 0x7f0b004c

    invoke-virtual {v5, v6}, LX/1ne;->q(I)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v7}, LX/1ne;->t(I)LX/1ne;

    move-result-object v5

    move-object v5, v5

    .line 2701475
    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    const/4 v7, 0x1

    .line 2701476
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    if-nez v5, :cond_4

    .line 2701477
    const/4 v5, 0x0

    .line 2701478
    :goto_2
    move-object v5, v5

    .line 2701479
    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x0

    .line 2701480
    iget-object v5, v0, Lcom/facebook/feedplugins/productminilist/ProductMiniCardComponentSpec;->f:LX/0ad;

    sget-short v6, LX/1xL;->c:S

    const/4 v7, 0x0

    invoke-interface {v5, v6, v7}, LX/0ad;->a(SZ)Z

    move-result v5

    if-nez v5, :cond_5

    .line 2701481
    :cond_0
    :goto_3
    move-object v2, v4

    .line 2701482
    invoke-interface {v3, v2}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v2

    .line 2701483
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    .line 2701484
    const v4, 0x57cd851d

    const/4 v5, 0x0

    invoke-static {p1, v4, v5}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v4, v4

    .line 2701485
    invoke-interface {v3, v4}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2701486
    return-object v0

    .line 2701487
    :cond_1
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->hn()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    if-eqz v7, :cond_2

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->hn()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 2701488
    :goto_4
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, p2}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v5

    iget-object v7, v0, Lcom/facebook/feedplugins/productminilist/ProductMiniCardComponentSpec;->c:LX/1nu;

    invoke-virtual {v7, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v7

    sget-object p0, Lcom/facebook/feedplugins/productminilist/ProductMiniCardComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v7, p0}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v7

    invoke-virtual {v7, v4}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v4

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-virtual {v4, v7}, LX/1nw;->c(F)LX/1nw;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const v7, 0x7f0b0a3c

    invoke-interface {v4, v7}, LX/1Di;->i(I)LX/1Di;

    move-result-object v4

    invoke-interface {v5, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x0

    const v7, 0x7f0b00f6

    invoke-interface {v4, v5, v7}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0b00f6

    invoke-interface {v4, p2, v5}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v5

    goto/16 :goto_0

    :cond_2
    move-object v4, v5

    .line 2701489
    goto :goto_4

    :cond_3
    move v5, v6

    goto/16 :goto_1

    :cond_4
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v5, v6}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v7}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v7}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v5

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    const v6, 0x7f0b004e

    invoke-virtual {v5, v6}, LX/1ne;->q(I)LX/1ne;

    move-result-object v5

    goto/16 :goto_2

    .line 2701490
    :cond_5
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v5

    .line 2701491
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLNode;->oN()Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    move-result-object v5

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;->CONTACT_MERCHANT:Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    if-ne v5, v6, :cond_0

    .line 2701492
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v4

    iget-object v5, v0, Lcom/facebook/feedplugins/productminilist/ProductMiniCardComponentSpec;->b:LX/1vg;

    invoke-virtual {v5, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v5

    const v6, 0x7f020ee4

    invoke-virtual {v5, v6}, LX/2xv;->h(I)LX/2xv;

    move-result-object v5

    const v6, -0x686869

    invoke-virtual {v5, v6}, LX/2xv;->i(I)LX/2xv;

    move-result-object v5

    invoke-virtual {v5}, LX/1n6;->b()LX/1dc;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v4

    goto/16 :goto_3
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2701456
    invoke-static {}, LX/1dS;->b()V

    .line 2701457
    iget v0, p1, LX/1dQ;->b:I

    .line 2701458
    packed-switch v0, :pswitch_data_0

    .line 2701459
    :goto_0
    return-object v2

    .line 2701460
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2701461
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0, v1}, LX/JVl;->a(Landroid/view/View;LX/1X1;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x57cd851d
        :pswitch_0
    .end packed-switch
.end method
