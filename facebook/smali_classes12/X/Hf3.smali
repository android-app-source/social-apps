.class public final LX/Hf3;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source ""


# instance fields
.field public final synthetic a:LX/Hf5;

.field public b:F

.field public c:F

.field public d:F

.field public e:F


# direct methods
.method public constructor <init>(LX/Hf5;)V
    .locals 0

    .prologue
    .line 2491113
    iput-object p1, p0, LX/Hf3;->a:LX/Hf5;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDown(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 2491108
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, LX/Hf3;->d:F

    .line 2491109
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, LX/Hf3;->e:F

    .line 2491110
    iget v0, p0, LX/Hf3;->d:F

    iget-object v1, p0, LX/Hf3;->a:LX/Hf5;

    iget v1, v1, LX/Hf5;->q:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iput v0, p0, LX/Hf3;->b:F

    .line 2491111
    iget v0, p0, LX/Hf3;->e:F

    iget-object v1, p0, LX/Hf3;->a:LX/Hf5;

    iget v1, v1, LX/Hf5;->r:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iput v0, p0, LX/Hf3;->c:F

    .line 2491112
    const/4 v0, 0x1

    return v0
.end method

.method public final onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 4

    .prologue
    .line 2491069
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, LX/Hf3;->d:F

    .line 2491070
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, LX/Hf3;->e:F

    .line 2491071
    const/4 p4, 0x0

    .line 2491072
    iget v0, p0, LX/Hf3;->d:F

    iget v1, p0, LX/Hf3;->b:F

    sub-float/2addr v0, v1

    float-to-int v0, v0

    iget-object v1, p0, LX/Hf3;->a:LX/Hf5;

    iget v1, v1, LX/Hf5;->o:I

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 2491073
    iget v1, p0, LX/Hf3;->e:F

    iget v2, p0, LX/Hf3;->c:F

    sub-float/2addr v1, v2

    float-to-int v1, v1

    iget-object v2, p0, LX/Hf3;->a:LX/Hf5;

    iget v2, v2, LX/Hf5;->p:I

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    .line 2491074
    iget-object v2, p0, LX/Hf3;->a:LX/Hf5;

    iget-object v2, v2, LX/Hf5;->H:Landroid/util/DisplayMetrics;

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    div-int/lit8 v2, v2, 0x2

    .line 2491075
    iget-object v3, p0, LX/Hf3;->a:LX/Hf5;

    iget-object v3, v3, LX/Hf5;->H:Landroid/util/DisplayMetrics;

    iget v3, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    iget-object p1, p0, LX/Hf3;->a:LX/Hf5;

    iget p1, p1, LX/Hf5;->t:I

    sub-int/2addr v3, p1

    .line 2491076
    sub-int p1, v0, v2

    .line 2491077
    sub-int p2, v1, v3

    .line 2491078
    mul-int/2addr p1, p1

    mul-int/2addr p2, p2

    add-int/2addr p1, p2

    .line 2491079
    iget-object p2, p0, LX/Hf3;->a:LX/Hf5;

    iget p2, p2, LX/Hf5;->x:I

    iget-object p3, p0, LX/Hf3;->a:LX/Hf5;

    iget p3, p3, LX/Hf5;->x:I

    mul-int/2addr p2, p3

    if-gt p1, p2, :cond_0

    .line 2491080
    iget-object v0, p0, LX/Hf3;->a:LX/Hf5;

    const/4 v1, 0x1

    .line 2491081
    iput-boolean v1, v0, LX/Hf5;->z:Z

    .line 2491082
    iget-object v0, p0, LX/Hf3;->a:LX/Hf5;

    iget-object v1, p0, LX/Hf3;->a:LX/Hf5;

    iget v1, v1, LX/Hf5;->o:I

    div-int/lit8 v1, v1, 0x2

    sub-int v1, v2, v1

    iget-object v2, p0, LX/Hf3;->a:LX/Hf5;

    iget v2, v2, LX/Hf5;->p:I

    div-int/lit8 v2, v2, 0x2

    sub-int v2, v3, v2

    invoke-static {v0, v1, v2, p4}, LX/Hf5;->a$redex0(LX/Hf5;IIZ)V

    .line 2491083
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 2491084
    :cond_0
    iget-object v2, p0, LX/Hf3;->a:LX/Hf5;

    .line 2491085
    iput-boolean p4, v2, LX/Hf5;->z:Z

    .line 2491086
    iget-object v2, p0, LX/Hf3;->a:LX/Hf5;

    iget-object v3, p0, LX/Hf3;->a:LX/Hf5;

    iget v3, v3, LX/Hf5;->o:I

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v0, v3

    iget-object v3, p0, LX/Hf3;->a:LX/Hf5;

    iget v3, v3, LX/Hf5;->p:I

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v1, v3

    invoke-static {v2, v0, v1, p4}, LX/Hf5;->a$redex0(LX/Hf5;IIZ)V

    goto :goto_0
.end method

.method public final onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    .line 2491087
    iget-object v0, p0, LX/Hf3;->a:LX/Hf5;

    iget-object v0, v0, LX/Hf5;->g:LX/378;

    .line 2491088
    sget-object v1, LX/7Rp;->PLAYER_TAPPED:LX/7Rp;

    invoke-static {v0, v1}, LX/378;->a(LX/378;LX/7Rp;)LX/0oG;

    move-result-object v1

    invoke-static {v1}, LX/378;->a(LX/0oG;)V

    .line 2491089
    iget-object v0, p0, LX/Hf3;->a:LX/Hf5;

    iget-object v0, v0, LX/Hf5;->y:LX/D7o;

    sget-object v1, LX/D7o;->CHANNEL_FEED:LX/D7o;

    if-ne v0, v1, :cond_1

    .line 2491090
    iget-object v0, p0, LX/Hf3;->a:LX/Hf5;

    .line 2491091
    iget-object v1, v0, LX/Hf5;->a:Landroid/content/Context;

    iget-object v2, v0, LX/Hf5;->E:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object p0, v0, LX/Hf5;->k:LX/Hex;

    .line 2491092
    iget-object p1, p0, LX/Hex;->b:Lcom/facebook/video/watchandgo/view/WatchAndGoVideoPlayer;

    move-object p0, p1

    .line 2491093
    invoke-virtual {p0}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result p0

    iget-object p1, v0, LX/Hf5;->C:LX/04D;

    invoke-static {v1, v2, p0, p1}, Lcom/facebook/video/channelfeed/activity/ChannelFeedActivity;->a(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;ILX/04D;)Landroid/content/Intent;

    move-result-object v1

    .line 2491094
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2491095
    iget-object v2, v0, LX/Hf5;->h:Lcom/facebook/content/SecureContextHelper;

    iget-object p0, v0, LX/Hf5;->a:Landroid/content/Context;

    invoke-interface {v2, v1, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2491096
    sget-object v1, LX/04G;->CHANNEL_PLAYER:LX/04G;

    sget-object v2, LX/04G;->WATCH_AND_GO:LX/04G;

    invoke-static {v0, v1, v2}, LX/Hf5;->a$redex0(LX/Hf5;LX/04G;LX/04G;)V

    .line 2491097
    iget-object v1, v0, LX/Hf5;->k:LX/Hex;

    invoke-virtual {v1}, LX/Hex;->d()V

    .line 2491098
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 2491099
    :cond_1
    iget-object v0, p0, LX/Hf3;->a:LX/Hf5;

    iget-object v0, v0, LX/Hf5;->y:LX/D7o;

    sget-object v1, LX/D7o;->FULLSCREEN:LX/D7o;

    if-ne v0, v1, :cond_0

    .line 2491100
    iget-object v0, p0, LX/Hf3;->a:LX/Hf5;

    iget-object v0, v0, LX/Hf5;->a:Landroid/content/Context;

    iget-object v1, p0, LX/Hf3;->a:LX/Hf5;

    iget-object v1, v1, LX/Hf5;->G:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p0, LX/Hf3;->a:LX/Hf5;

    iget-object v2, v2, LX/Hf5;->F:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p0, LX/Hf3;->a:LX/Hf5;

    iget-object v3, v3, LX/Hf5;->k:LX/Hex;

    .line 2491101
    iget-object p1, v3, LX/Hex;->b:Lcom/facebook/video/watchandgo/view/WatchAndGoVideoPlayer;

    move-object v3, p1

    .line 2491102
    invoke-virtual {v3}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/video/watchandgo/view/WatchAndGoFullscreenActivity;->a(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;I)Landroid/content/Intent;

    move-result-object v0

    .line 2491103
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2491104
    iget-object v1, p0, LX/Hf3;->a:LX/Hf5;

    iget-object v1, v1, LX/Hf5;->h:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/Hf3;->a:LX/Hf5;

    iget-object v2, v2, LX/Hf5;->a:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2491105
    iget-object v0, p0, LX/Hf3;->a:LX/Hf5;

    sget-object v1, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    sget-object v2, LX/04G;->WATCH_AND_GO:LX/04G;

    invoke-static {v0, v1, v2}, LX/Hf5;->a$redex0(LX/Hf5;LX/04G;LX/04G;)V

    .line 2491106
    iget-object v0, p0, LX/Hf3;->a:LX/Hf5;

    iget-object v0, v0, LX/Hf5;->k:LX/Hex;

    invoke-virtual {v0}, LX/Hex;->d()V

    .line 2491107
    goto :goto_0
.end method
