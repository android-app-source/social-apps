.class public final LX/IAL;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;


# direct methods
.method public constructor <init>(Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;)V
    .locals 0

    .prologue
    .line 2545349
    iput-object p1, p0, LX/IAL;->a:Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2545315
    iget-object v0, p0, LX/IAL;->a:Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;

    invoke-virtual {v0}, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2545316
    iget-object v0, p0, LX/IAL;->a:Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;

    iget-object v0, v0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->y:LX/IAN;

    sget-object v2, LX/IAN;->HOSTED_BY_PAGE_OR_PEOPLE:LX/IAN;

    if-ne v0, v2, :cond_2

    .line 2545317
    iget-object v0, p0, LX/IAL;->a:Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;

    iget-object v0, v0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->z:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v4, :cond_1

    .line 2545318
    iget-object v0, p0, LX/IAL;->a:Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;

    iget-object v0, v0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->z:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/model/EventUser;

    .line 2545319
    iget-object v2, p0, LX/IAL;->a:Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;

    iget-object v2, v2, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->m:LX/I76;

    invoke-virtual {v2, v1, v0}, LX/I76;->a(Landroid/content/Context;Lcom/facebook/events/model/EventUser;)V

    .line 2545320
    :cond_0
    :goto_0
    return-void

    .line 2545321
    :cond_1
    iget-object v0, p0, LX/IAL;->a:Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;

    iget-object v0, v0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->m:LX/I76;

    iget-object v2, p0, LX/IAL;->a:Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;

    iget-object v2, v2, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->u:Lcom/facebook/events/model/Event;

    .line 2545322
    iget-object v3, v2, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2545323
    iget-object v3, p0, LX/IAL;->a:Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;

    iget-object v3, v3, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->z:LX/0Px;

    .line 2545324
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    iget-object v4, v0, LX/I76;->c:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/ComponentName;

    invoke-virtual {v5, v4}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v4

    .line 2545325
    const-string v5, "target_fragment"

    sget-object p0, LX/0cQ;->EVENTS_HOSTS_FRAGMENT:LX/0cQ;

    invoke-virtual {p0}, LX/0cQ;->ordinal()I

    move-result p0

    invoke-virtual {v4, v5, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2545326
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 2545327
    const-string p0, "EVENT_ID"

    invoke-virtual {v5, p0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2545328
    const-string p0, "HOSTS"

    invoke-static {v3}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object p1

    invoke-virtual {v5, p0, p1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2545329
    move-object v5, v5

    .line 2545330
    invoke-virtual {v4, v5}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2545331
    iget-object v5, v0, LX/I76;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v5, v4, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2545332
    goto :goto_0

    .line 2545333
    :cond_2
    iget-object v0, p0, LX/IAL;->a:Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;

    iget-object v0, v0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->y:LX/IAN;

    sget-object v2, LX/IAN;->WITH_ARTISTS:LX/IAN;

    if-ne v0, v2, :cond_0

    .line 2545334
    iget-object v0, p0, LX/IAL;->a:Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;

    iget-object v0, v0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->A:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v4, :cond_3

    .line 2545335
    iget-object v0, p0, LX/IAL;->a:Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;

    iget-object v0, v0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->A:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/model/EventArtist;

    .line 2545336
    iget-object v2, p0, LX/IAL;->a:Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;

    iget-object v2, v2, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->m:LX/I76;

    invoke-virtual {v2, v1, v0}, LX/I76;->a(Landroid/content/Context;Lcom/facebook/events/model/EventArtist;)V

    goto :goto_0

    .line 2545337
    :cond_3
    iget-object v0, p0, LX/IAL;->a:Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;

    iget-object v0, v0, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->m:LX/I76;

    iget-object v2, p0, LX/IAL;->a:Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;

    iget-object v2, v2, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->u:Lcom/facebook/events/model/Event;

    .line 2545338
    iget-object v3, v2, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2545339
    iget-object v3, p0, LX/IAL;->a:Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;

    iget-object v3, v3, Lcom/facebook/events/permalink/header/CaspianEventPermalinkHeaderView;->A:LX/0Px;

    .line 2545340
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    iget-object v4, v0, LX/I76;->c:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/ComponentName;

    invoke-virtual {v5, v4}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v4

    .line 2545341
    const-string v5, "target_fragment"

    sget-object p0, LX/0cQ;->EVENTS_HOSTS_FRAGMENT:LX/0cQ;

    invoke-virtual {p0}, LX/0cQ;->ordinal()I

    move-result p0

    invoke-virtual {v4, v5, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2545342
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 2545343
    const-string p0, "EVENT_ID"

    invoke-virtual {v5, p0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2545344
    const-string p0, "ARTISTS"

    invoke-static {v3}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object p1

    invoke-virtual {v5, p0, p1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2545345
    move-object v5, v5

    .line 2545346
    invoke-virtual {v4, v5}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2545347
    iget-object v5, v0, LX/I76;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v5, v4, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2545348
    goto/16 :goto_0
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 1

    .prologue
    .line 2545313
    const/4 v0, 0x0

    iput v0, p1, Landroid/text/TextPaint;->bgColor:I

    .line 2545314
    return-void
.end method
