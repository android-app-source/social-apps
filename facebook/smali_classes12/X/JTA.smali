.class public final LX/JTA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/JTB;


# direct methods
.method public constructor <init>(LX/JTB;)V
    .locals 0

    .prologue
    .line 2696421
    iput-object p1, p0, LX/JTA;->a:LX/JTB;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x2

    const v0, -0x2bbbb3d8

    invoke-static {v2, v5, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2696422
    iget-object v1, p0, LX/JTA;->a:LX/JTB;

    iget-object v1, v1, LX/JTB;->c:LX/JSe;

    .line 2696423
    iget-object v3, v1, LX/JSe;->l:Landroid/net/Uri;

    move-object v1, v3

    .line 2696424
    if-nez v1, :cond_0

    .line 2696425
    const v1, -0x2d0ea58e

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2696426
    :goto_0
    return-void

    .line 2696427
    :cond_0
    new-array v2, v2, [Landroid/net/Uri;

    const/4 v3, 0x0

    iget-object v4, p0, LX/JTA;->a:LX/JTB;

    invoke-static {v4, v1}, LX/JTB;->a(LX/JTB;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v4

    aput-object v4, v2, v3

    .line 2696428
    const/4 v6, 0x0

    .line 2696429
    new-instance v7, Landroid/net/Uri$Builder;

    invoke-direct {v7}, Landroid/net/Uri$Builder;-><init>()V

    .line 2696430
    const-string v4, "https"

    invoke-virtual {v7, v4}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2696431
    const-string v4, "music.dmkt-sp.jp"

    invoke-virtual {v7, v4}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2696432
    if-nez v1, :cond_6

    .line 2696433
    invoke-virtual {v7}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    .line 2696434
    :goto_1
    move-object v4, v4

    .line 2696435
    move-object v3, v4

    .line 2696436
    aput-object v3, v2, v5

    .line 2696437
    iget-object v3, p0, LX/JTA;->a:LX/JTB;

    iget-object v3, v3, LX/JTB;->a:LX/JTC;

    iget-object v3, v3, LX/JTC;->c:LX/JSU;

    if-eqz v3, :cond_5

    .line 2696438
    iget-object v1, p0, LX/JTA;->a:LX/JTB;

    iget-object v1, v1, LX/JTB;->a:LX/JTC;

    iget-object v1, v1, LX/JTC;->c:LX/JSU;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const/4 p1, 0x1

    const/4 v8, 0x0

    .line 2696439
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2696440
    new-instance v5, LX/3Af;

    invoke-direct {v5, v3}, LX/3Af;-><init>(Landroid/content/Context;)V

    .line 2696441
    new-instance v6, LX/7TY;

    invoke-direct {v6, v3}, LX/7TY;-><init>(Landroid/content/Context;)V

    .line 2696442
    aget-object v7, v2, v8

    if-eqz v7, :cond_1

    .line 2696443
    const v7, 0x7f083a8b

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/34c;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v7

    invoke-static {v1, v3, v2, v8}, LX/JSU;->a(LX/JSU;Landroid/content/Context;[Landroid/net/Uri;I)Landroid/view/MenuItem$OnMenuItemClickListener;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2696444
    :cond_1
    aget-object v7, v2, p1

    if-eqz v7, :cond_2

    .line 2696445
    const v7, 0x7f083a8c    # 1.81079E38f

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, LX/34c;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v4

    invoke-static {v1, v3, v2, p1}, LX/JSU;->a(LX/JSU;Landroid/content/Context;[Landroid/net/Uri;I)Landroid/view/MenuItem$OnMenuItemClickListener;

    move-result-object v7

    invoke-virtual {v4, v7}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2696446
    :cond_2
    invoke-virtual {v6}, LX/1OM;->ij_()I

    move-result v4

    if-eqz v4, :cond_3

    .line 2696447
    invoke-virtual {v5, v6}, LX/3Af;->a(LX/1OM;)V

    .line 2696448
    invoke-virtual {v5}, LX/3Af;->show()V

    .line 2696449
    :cond_3
    :goto_2
    iget-object v1, p0, LX/JTA;->a:LX/JTB;

    iget-object v1, v1, LX/JTB;->d:LX/JTY;

    if-eqz v1, :cond_4

    .line 2696450
    iget-object v1, p0, LX/JTA;->a:LX/JTB;

    iget-object v1, v1, LX/JTB;->d:LX/JTY;

    sget-object v2, LX/JTV;->deep_link:LX/JTV;

    invoke-virtual {v1, v2}, LX/JTY;->a(LX/JTV;)V

    .line 2696451
    :cond_4
    const v1, 0x62f37950

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto/16 :goto_0

    .line 2696452
    :cond_5
    iget-object v2, p0, LX/JTA;->a:LX/JTB;

    iget-object v2, v2, LX/JTB;->a:LX/JTC;

    iget-object v2, v2, LX/JTC;->b:LX/JTI;

    invoke-virtual {v2, v1}, LX/JTI;->a(Landroid/net/Uri;)V

    goto :goto_2

    .line 2696453
    :cond_6
    const-string v4, "app_id"

    invoke-virtual {v1, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2696454
    if-eqz v4, :cond_7

    const-string v8, "799889663393876"

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_8

    const-string v8, "823778694338306"

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 2696455
    :cond_7
    invoke-virtual {v7}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    goto/16 :goto_1

    .line 2696456
    :cond_8
    const-string v4, "appsite_data"

    invoke-virtual {v1, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2696457
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_e

    .line 2696458
    :try_start_0
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v8

    const-class v3, LX/JT9;

    invoke-virtual {v8, v4, v3}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/JT9;
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 2696459
    :goto_3
    if-eqz v4, :cond_d

    iget-object v8, v4, LX/JT9;->appSites:Ljava/util/List;

    if-eqz v8, :cond_d

    iget-object v8, v4, LX/JT9;->appSites:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_d

    .line 2696460
    iget-object v4, v4, LX/JT9;->appSites:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_9
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_d

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/JT8;

    .line 2696461
    iget-object v3, v4, LX/JT8;->appSiteUrl:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_9

    .line 2696462
    iget-object v4, v4, LX/JT8;->appSiteUrl:Ljava/lang/String;

    .line 2696463
    :goto_4
    if-nez v4, :cond_a

    .line 2696464
    invoke-virtual {v7}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    goto/16 :goto_1

    .line 2696465
    :catch_0
    invoke-virtual {v7}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    goto/16 :goto_1

    .line 2696466
    :catch_1
    invoke-virtual {v7}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    goto/16 :goto_1

    .line 2696467
    :cond_a
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    const-string v6, "url"

    invoke-virtual {v4, v6}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2696468
    if-nez v4, :cond_b

    .line 2696469
    invoke-virtual {v7}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    goto/16 :goto_1

    .line 2696470
    :cond_b
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    const-string v6, "fb_music"

    invoke-virtual {v4, v6}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2696471
    if-eqz v4, :cond_c

    .line 2696472
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v8, "/song/S"

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "/"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2696473
    :cond_c
    invoke-virtual {v7}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    goto/16 :goto_1

    :cond_d
    move-object v4, v6

    goto :goto_4

    :cond_e
    move-object v4, v6

    goto :goto_3
.end method
