.class public final LX/Ibt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/util/List",
        "<",
        "Landroid/location/Address;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Ibu;


# direct methods
.method public constructor <init>(LX/Ibu;)V
    .locals 0

    .prologue
    .line 2594872
    iput-object p1, p0, LX/Ibt;->a:LX/Ibu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2594873
    iget-object v0, p0, LX/Ibt;->a:LX/Ibu;

    iget-object v0, v0, LX/Ibu;->f:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2594874
    :goto_0
    return-void

    .line 2594875
    :cond_0
    iget-object v0, p0, LX/Ibt;->a:LX/Ibu;

    iget-object v0, v0, LX/Ibu;->c:LX/03V;

    sget-object v1, LX/Ibu;->a:Ljava/lang/String;

    const-string v2, "Can\'t get address line from location"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2594876
    check-cast p1, Ljava/util/List;

    .line 2594877
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2594878
    :cond_0
    iget-object v0, p0, LX/Ibt;->a:LX/Ibu;

    iget-object v0, v0, LX/Ibu;->c:LX/03V;

    sget-object v1, LX/Ibu;->a:Ljava/lang/String;

    const-string v2, "Geocoder return wrong result"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2594879
    :goto_0
    return-void

    .line 2594880
    :cond_1
    iget-object v0, p0, LX/Ibt;->a:LX/Ibu;

    iget-object v1, v0, LX/Ibu;->g:LX/IdH;

    iget-object v2, p0, LX/Ibt;->a:LX/Ibu;

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Address;

    invoke-virtual {v2, v0}, LX/Ibu;->a(Landroid/location/Address;)Ljava/lang/String;

    move-result-object v0

    .line 2594881
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, v1, LX/IdH;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v2

    if-nez v2, :cond_3

    .line 2594882
    :cond_2
    :goto_1
    goto :goto_0

    .line 2594883
    :cond_3
    iget-object v2, v1, LX/IdH;->a:Lcom/facebook/messaging/business/ride/view/RideRequestFragment;

    sget-object p0, LX/Ib6;->ORIGIN:LX/Ib6;

    invoke-static {v2, v0, p0}, Lcom/facebook/messaging/business/ride/view/RideRequestFragment;->a$redex0(Lcom/facebook/messaging/business/ride/view/RideRequestFragment;Ljava/lang/String;LX/Ib6;)V

    goto :goto_1
.end method
