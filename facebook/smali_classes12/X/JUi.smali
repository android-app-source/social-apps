.class public final LX/JUi;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JUj;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:D

.field public b:D

.field public c:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

.field public d:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

.field public f:Ljava/lang/String;

.field public final synthetic g:LX/JUj;


# direct methods
.method public constructor <init>(LX/JUj;)V
    .locals 1

    .prologue
    .line 2699637
    iput-object p1, p0, LX/JUi;->g:LX/JUj;

    .line 2699638
    move-object v0, p1

    .line 2699639
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2699640
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2699612
    const-string v0, "PageContextualRecommendationsMapComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2699613
    if-ne p0, p1, :cond_1

    .line 2699614
    :cond_0
    :goto_0
    return v0

    .line 2699615
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2699616
    goto :goto_0

    .line 2699617
    :cond_3
    check-cast p1, LX/JUi;

    .line 2699618
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2699619
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2699620
    if-eq v2, v3, :cond_0

    .line 2699621
    iget-wide v2, p0, LX/JUi;->a:D

    iget-wide v4, p1, LX/JUi;->a:D

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Double;->compare(DD)I

    move-result v2

    if-eqz v2, :cond_4

    move v0, v1

    .line 2699622
    goto :goto_0

    .line 2699623
    :cond_4
    iget-wide v2, p0, LX/JUi;->b:D

    iget-wide v4, p1, LX/JUi;->b:D

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Double;->compare(DD)I

    move-result v2

    if-eqz v2, :cond_5

    move v0, v1

    .line 2699624
    goto :goto_0

    .line 2699625
    :cond_5
    iget-object v2, p0, LX/JUi;->c:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/JUi;->c:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    iget-object v3, p1, LX/JUi;->c:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    :cond_6
    move v0, v1

    .line 2699626
    goto :goto_0

    .line 2699627
    :cond_7
    iget-object v2, p1, LX/JUi;->c:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    if-nez v2, :cond_6

    .line 2699628
    :cond_8
    iget-object v2, p0, LX/JUi;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/JUi;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/JUi;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    :cond_9
    move v0, v1

    .line 2699629
    goto :goto_0

    .line 2699630
    :cond_a
    iget-object v2, p1, LX/JUi;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_9

    .line 2699631
    :cond_b
    iget-object v2, p0, LX/JUi;->e:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    if-eqz v2, :cond_d

    iget-object v2, p0, LX/JUi;->e:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    iget-object v3, p1, LX/JUi;->e:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    :cond_c
    move v0, v1

    .line 2699632
    goto :goto_0

    .line 2699633
    :cond_d
    iget-object v2, p1, LX/JUi;->e:Lcom/facebook/graphql/model/GraphQLReactionStoryAction;

    if-nez v2, :cond_c

    .line 2699634
    :cond_e
    iget-object v2, p0, LX/JUi;->f:Ljava/lang/String;

    if-eqz v2, :cond_f

    iget-object v2, p0, LX/JUi;->f:Ljava/lang/String;

    iget-object v3, p1, LX/JUi;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2699635
    goto :goto_0

    .line 2699636
    :cond_f
    iget-object v2, p1, LX/JUi;->f:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
