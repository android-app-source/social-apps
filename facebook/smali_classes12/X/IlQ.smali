.class public LX/IlQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/IlO;


# instance fields
.field private final a:Landroid/view/LayoutInflater;

.field private final b:LX/IlR;


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;LX/IlR;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2607743
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2607744
    iput-object p1, p0, LX/IlQ;->a:Landroid/view/LayoutInflater;

    .line 2607745
    iput-object p2, p0, LX/IlQ;->b:LX/IlR;

    .line 2607746
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Landroid/view/View;Landroid/view/ViewGroup;)LX/IlT;
    .locals 3

    .prologue
    .line 2607747
    if-eqz p2, :cond_0

    instance-of v0, p2, Lcom/facebook/messaging/payment/prefs/transactions/ProductMessengerPayHistoryItemView;

    if-nez v0, :cond_1

    .line 2607748
    :cond_0
    iget-object v0, p0, LX/IlQ;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f031041

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2607749
    :goto_0
    check-cast v0, LX/IlT;

    .line 2607750
    iget-object v1, p0, LX/IlQ;->b:LX/IlR;

    invoke-virtual {v1, p1}, LX/IlR;->a(Ljava/lang/Object;)LX/IlW;

    move-result-object v1

    .line 2607751
    invoke-interface {v0, v1}, LX/IlT;->setMessengerPayHistoryItemViewParams(LX/IlW;)V

    .line 2607752
    return-object v0

    :cond_1
    move-object v0, p2

    goto :goto_0
.end method
