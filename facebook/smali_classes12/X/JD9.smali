.class public final LX/JD9;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:LX/1Fb;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Ljava/lang/String;

.field public final g:LX/JBL;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "collection"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation
.end field

.field public final h:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionMediasetModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final i:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final j:Ljava/lang/String;

.field public final k:Ljava/lang/String;

.field public final l:Landroid/os/Bundle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final n:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final o:LX/9lP;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final p:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/1Fb;Ljava/lang/String;LX/JBL;Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionMediasetModel;Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldsModel;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;LX/9lP;Z)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/1Fb;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # LX/JBL;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionMediasetModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p10    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p12    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p13    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p14    # Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p15    # LX/9lP;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "<init>"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2663173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2663174
    iput-object p1, p0, LX/JD9;->a:Ljava/lang/String;

    .line 2663175
    iput-object p2, p0, LX/JD9;->b:Ljava/lang/String;

    .line 2663176
    iput-object p3, p0, LX/JD9;->c:Ljava/lang/String;

    .line 2663177
    iput-object p4, p0, LX/JD9;->d:Ljava/lang/String;

    .line 2663178
    iput-object p5, p0, LX/JD9;->e:LX/1Fb;

    .line 2663179
    iput-object p6, p0, LX/JD9;->f:Ljava/lang/String;

    .line 2663180
    iput-object p7, p0, LX/JD9;->g:LX/JBL;

    .line 2663181
    iput-object p8, p0, LX/JD9;->h:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionMediasetModel;

    .line 2663182
    iput-object p9, p0, LX/JD9;->i:Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldsModel;

    .line 2663183
    iput-object p10, p0, LX/JD9;->j:Ljava/lang/String;

    .line 2663184
    iput-object p11, p0, LX/JD9;->k:Ljava/lang/String;

    .line 2663185
    iput-object p12, p0, LX/JD9;->l:Landroid/os/Bundle;

    .line 2663186
    iput-object p13, p0, LX/JD9;->m:Ljava/lang/String;

    .line 2663187
    iput-object p14, p0, LX/JD9;->n:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    .line 2663188
    move-object/from16 v0, p15

    iput-object v0, p0, LX/JD9;->o:LX/9lP;

    .line 2663189
    move/from16 v0, p16

    iput-boolean v0, p0, LX/JD9;->p:Z

    .line 2663190
    return-void
.end method
