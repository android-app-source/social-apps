.class public LX/HwQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0io;",
        "Services::",
        "LX/0il",
        "<TModelData;>;>",
        "Ljava/lang/Object;",
        "Landroid/view/View$OnTouchListener;"
    }
.end annotation


# instance fields
.field private final a:LX/HrF;

.field public final b:Z

.field public final c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field private d:I


# direct methods
.method public constructor <init>(LX/HrF;ZLX/0il;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/HrF;",
            "ZTServices;)V"
        }
    .end annotation

    .prologue
    .line 2520861
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2520862
    const/4 v0, -0x1

    iput v0, p0, LX/HwQ;->d:I

    .line 2520863
    iput-object p1, p0, LX/HwQ;->a:LX/HrF;

    .line 2520864
    iput-boolean p2, p0, LX/HwQ;->b:Z

    .line 2520865
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    iput-object v0, p0, LX/HwQ;->c:Ljava/lang/ref/WeakReference;

    .line 2520866
    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 2520867
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2520868
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 2520869
    :pswitch_0
    iput v1, p0, LX/HwQ;->d:I

    goto :goto_0

    .line 2520870
    :pswitch_1
    iget v0, p0, LX/HwQ;->d:I

    if-ne v0, v1, :cond_1

    .line 2520871
    invoke-virtual {p1}, Landroid/view/View;->getScrollY()I

    move-result v0

    iput v0, p0, LX/HwQ;->d:I

    .line 2520872
    :cond_1
    iget v0, p0, LX/HwQ;->d:I

    if-eq v0, v1, :cond_0

    .line 2520873
    iget-object v0, p0, LX/HwQ;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 2520874
    iget-boolean v1, p0, LX/HwQ;->b:Z

    if-eqz v1, :cond_2

    const/4 v0, 0x0

    :goto_1
    move v0, v0

    .line 2520875
    if-lez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getScrollY()I

    move-result v0

    iget v1, p0, LX/HwQ;->d:I

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x41a00000    # 20.0f

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v1, v2

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 2520876
    iget-object v0, p0, LX/HwQ;->a:LX/HrF;

    invoke-interface {v0}, LX/HrF;->a()V

    goto :goto_0

    :cond_2
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
