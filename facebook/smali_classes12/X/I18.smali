.class public final LX/I18;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;Z)V
    .locals 0

    .prologue
    .line 2528523
    iput-object p1, p0, LX/I18;->b:Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;

    iput-boolean p2, p0, LX/I18;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;Ljava/lang/String;)V
    .locals 12
    .param p2    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel$EventsFeedModel$EdgesModel;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2528524
    iget-object v0, p0, LX/I18;->b:Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;

    .line 2528525
    iput-object p2, v0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->o:Ljava/lang/String;

    .line 2528526
    iget-boolean v0, p0, LX/I18;->a:Z

    if-nez v0, :cond_0

    .line 2528527
    iget-object v0, p0, LX/I18;->b:Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;

    iget-object v0, v0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->k:LX/I1f;

    invoke-virtual {v0}, LX/I1f;->d()V

    .line 2528528
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2528529
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel$EventsFeedModel$EdgesModel;

    .line 2528530
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel$EventsFeedModel$EdgesModel;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 2528531
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventUpdatesFeedGraphQLModels$EventUpdatesFeedQueryModel$ActorModel$EventsFeedModel$EdgesModel;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2528532
    :cond_2
    iget-object v0, p0, LX/I18;->b:Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;

    iget-object v0, v0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->k:LX/I1f;

    .line 2528533
    iget-object v4, v0, LX/I1f;->c:LX/I1B;

    .line 2528534
    const/4 v5, 0x0

    move v6, v5

    :goto_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    if-ge v6, v5, :cond_3

    .line 2528535
    iget-object v7, v4, LX/I1B;->b:Ljava/util/HashMap;

    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/FeedUnit;

    invoke-interface {v5}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v5

    iget-object v8, v4, LX/I1B;->a:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    add-int/2addr v8, v6

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v5, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2528536
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_1

    .line 2528537
    :cond_3
    iget-object v5, v4, LX/I1B;->a:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2528538
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/graphql/model/FeedUnit;

    .line 2528539
    iget-object v6, v0, LX/I1f;->l:LX/0jU;

    invoke-virtual {v6, v7}, LX/0jU;->e(Lcom/facebook/graphql/model/FeedUnit;)Ljava/util/Set;

    move-result-object v11

    .line 2528540
    new-instance v6, Lcom/facebook/graphql/executor/GraphQLResult;

    sget-object v8, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    const-wide/16 v9, 0x0

    invoke-direct/range {v6 .. v11}, Lcom/facebook/graphql/executor/GraphQLResult;-><init>(Ljava/lang/Object;LX/0ta;JLjava/util/Set;)V

    .line 2528541
    iget-object v8, v0, LX/I1f;->m:LX/1My;

    iget-object v9, v0, LX/I1f;->q:LX/0TF;

    invoke-interface {v7}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v9, v7, v6}, LX/1My;->a(LX/0TF;Ljava/lang/String;Lcom/facebook/graphql/executor/GraphQLResult;)V

    goto :goto_2

    .line 2528542
    :cond_4
    invoke-static {v0}, LX/I1f;->i(LX/I1f;)V

    .line 2528543
    iget-object v0, p0, LX/I18;->b:Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;

    iget-object v0, v0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->k:LX/I1f;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/I1f;->b(Z)V

    .line 2528544
    iget-object v0, p0, LX/I18;->b:Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;

    iget-object v0, v0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->o:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/I18;->b:Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;

    iget-boolean v0, v0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->r:Z

    if-nez v0, :cond_5

    .line 2528545
    iget-object v0, p0, LX/I18;->b:Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;

    iget-object v0, v0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->k:LX/I1f;

    iget-object v1, p0, LX/I18;->b:Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;

    iget-object v1, v1, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->q:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/I1f;->b(Ljava/util/List;)V

    .line 2528546
    iget-object v0, p0, LX/I18;->b:Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;

    const/4 v1, 0x1

    .line 2528547
    iput-boolean v1, v0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->r:Z

    .line 2528548
    :cond_5
    return-void
.end method
