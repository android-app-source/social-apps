.class public final LX/Iof;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/util/ArrayList",
        "<",
        "Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLInterfaces$Theme;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Iog;


# direct methods
.method public constructor <init>(LX/Iog;)V
    .locals 0

    .prologue
    .line 2612062
    iput-object p1, p0, LX/Iof;->a:LX/Iog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2612063
    iget-object v0, p0, LX/Iof;->a:LX/Iog;

    iget-object v0, v0, LX/Iog;->c:LX/03V;

    const-string v1, "OrionMessengerPayLoader"

    const-string v2, "Failed to fetch the theme list"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2612064
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2612065
    check-cast p1, Ljava/util/ArrayList;

    .line 2612066
    iget-object v0, p0, LX/Iof;->a:LX/Iog;

    iget-object v0, v0, LX/Iog;->i:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->b(Ljava/util/List;)V

    .line 2612067
    return-void
.end method
