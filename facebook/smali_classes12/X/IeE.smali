.class public LX/IeE;
.super LX/3Mk;
.source ""


# direct methods
.method public constructor <init>(LX/0Zr;LX/3Lw;LX/2Oq;LX/3LP;LX/3Lv;LX/3Mn;LX/3Mp;LX/0lB;LX/0Uh;LX/0W9;LX/3Mq;LX/2RQ;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zr;",
            "LX/3Lw;",
            "LX/2Oq;",
            "LX/3LP;",
            "LX/3Lv;",
            "LX/3Mn;",
            "LX/3Mp;",
            "LX/0lB;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0W9;",
            "LX/3Mq;",
            "LX/2RQ;",
            "LX/0Ot",
            "<",
            "LX/3A0;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2598492
    invoke-direct/range {p0 .. p13}, LX/3Mk;-><init>(LX/0Zr;LX/3Lw;LX/2Oq;LX/3LP;LX/3Lv;LX/3Mn;LX/3Mp;LX/0lB;LX/0Uh;LX/0W9;LX/3Mq;LX/2RQ;LX/0Ot;)V

    .line 2598493
    return-void
.end method

.method public static c(LX/0QB;)LX/IeE;
    .locals 14

    .prologue
    .line 2598494
    new-instance v0, LX/IeE;

    invoke-static {p0}, LX/0Zr;->a(LX/0QB;)LX/0Zr;

    move-result-object v1

    check-cast v1, LX/0Zr;

    invoke-static {p0}, LX/3Lw;->b(LX/0QB;)LX/3Lw;

    move-result-object v2

    check-cast v2, LX/3Lw;

    invoke-static {p0}, LX/2Oq;->a(LX/0QB;)LX/2Oq;

    move-result-object v3

    check-cast v3, LX/2Oq;

    invoke-static {p0}, LX/3LP;->a(LX/0QB;)LX/3LP;

    move-result-object v4

    check-cast v4, LX/3LP;

    invoke-static {p0}, LX/3Lv;->b(LX/0QB;)LX/3Lv;

    move-result-object v5

    check-cast v5, LX/3Lv;

    invoke-static {p0}, LX/3Mn;->b(LX/0QB;)LX/3Mn;

    move-result-object v6

    check-cast v6, LX/3Mn;

    invoke-static {p0}, LX/3Mp;->a(LX/0QB;)LX/3Mp;

    move-result-object v7

    check-cast v7, LX/3Mp;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v8

    check-cast v8, LX/0lB;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v9

    check-cast v9, LX/0Uh;

    invoke-static {p0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v10

    check-cast v10, LX/0W9;

    invoke-static {p0}, LX/3Mq;->a(LX/0QB;)LX/3Mq;

    move-result-object v11

    check-cast v11, LX/3Mq;

    invoke-static {p0}, LX/2RQ;->b(LX/0QB;)LX/2RQ;

    move-result-object v12

    check-cast v12, LX/2RQ;

    const/16 v13, 0x110e

    invoke-static {p0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-direct/range {v0 .. v13}, LX/IeE;-><init>(LX/0Zr;LX/3Lw;LX/2Oq;LX/3LP;LX/3Lv;LX/3Mn;LX/3Mp;LX/0lB;LX/0Uh;LX/0W9;LX/3Mq;LX/2RQ;LX/0Ot;)V

    .line 2598495
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/user/model/UserIdentifier;)Z
    .locals 1

    .prologue
    .line 2598496
    iget-object v0, p0, LX/3Ml;->a:Ljava/util/Set;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/3Ml;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2598497
    :cond_0
    const/4 v0, 0x0

    .line 2598498
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, LX/3Ml;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2598499
    const/4 v0, 0x0

    return v0
.end method
