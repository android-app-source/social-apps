.class public LX/IBE;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field public final b:LX/IBD;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/IBD",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(LX/IBD;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/IBD",
            "<TT;>;TT;)V"
        }
    .end annotation

    .prologue
    .line 2546701
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2546702
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2546703
    iput-object p1, p0, LX/IBE;->b:LX/IBD;

    .line 2546704
    iput-object p2, p0, LX/IBE;->a:Ljava/lang/Object;

    .line 2546705
    return-void
.end method

.method public static a(LX/IBD;Ljava/lang/Object;)LX/IBE;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/IBD",
            "<TT;>;TT;)",
            "LX/IBE",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 2546700
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/IBE;

    invoke-direct {v0, p0, p1}, LX/IBE;-><init>(LX/IBD;Ljava/lang/Object;)V

    goto :goto_0
.end method
