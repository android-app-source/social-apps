.class public LX/Ios;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:LX/IoT;

.field public final b:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/payments/p2p/model/PaymentCard;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Lcom/facebook/user/model/Name;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Lcom/facebook/user/model/UserKey;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:LX/03R;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

.field public final g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLInterfaces$Theme;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final i:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Iot;)V
    .locals 1

    .prologue
    .line 2612333
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2612334
    iget-object v0, p1, LX/Iot;->a:LX/IoT;

    move-object v0, v0

    .line 2612335
    iput-object v0, p0, LX/Ios;->a:LX/IoT;

    .line 2612336
    iget-object v0, p1, LX/Iot;->b:LX/0am;

    move-object v0, v0

    .line 2612337
    iput-object v0, p0, LX/Ios;->b:LX/0am;

    .line 2612338
    iget-object v0, p1, LX/Iot;->c:Lcom/facebook/user/model/Name;

    move-object v0, v0

    .line 2612339
    iput-object v0, p0, LX/Ios;->c:Lcom/facebook/user/model/Name;

    .line 2612340
    iget-object v0, p1, LX/Iot;->d:Lcom/facebook/user/model/UserKey;

    move-object v0, v0

    .line 2612341
    iput-object v0, p0, LX/Ios;->d:Lcom/facebook/user/model/UserKey;

    .line 2612342
    iget-object v0, p1, LX/Iot;->e:LX/03R;

    move-object v0, v0

    .line 2612343
    iput-object v0, p0, LX/Ios;->e:LX/03R;

    .line 2612344
    iget-object v0, p1, LX/Iot;->f:Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    move-object v0, v0

    .line 2612345
    iput-object v0, p0, LX/Ios;->f:Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    .line 2612346
    iget-object v0, p1, LX/Iot;->g:Ljava/lang/String;

    move-object v0, v0

    .line 2612347
    iput-object v0, p0, LX/Ios;->g:Ljava/lang/String;

    .line 2612348
    iget-object v0, p1, LX/Iot;->h:Ljava/util/List;

    move-object v0, v0

    .line 2612349
    iput-object v0, p0, LX/Ios;->h:Ljava/util/List;

    .line 2612350
    iget-object v0, p1, LX/Iot;->i:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;

    move-object v0, v0

    .line 2612351
    iput-object v0, p0, LX/Ios;->i:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;

    .line 2612352
    return-void
.end method
