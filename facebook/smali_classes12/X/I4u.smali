.class public final LX/I4u;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;)V
    .locals 0

    .prologue
    .line 2534641
    iput-object p1, p0, LX/I4u;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventsDiscoveryFiltersModel$EventDiscoverSuggestionFiltersModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2534642
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventsDiscoveryFiltersModel$EventDiscoverSuggestionFiltersModel;

    .line 2534643
    invoke-virtual {v0}, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventsDiscoveryFiltersModel$EventDiscoverSuggestionFiltersModel;->j()Ljava/lang/String;

    move-result-object v2

    .line 2534644
    const-string v3, "time"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2534645
    iget-object v2, p0, LX/I4u;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;

    iget-object v2, v2, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->F:Ljava/util/List;

    invoke-virtual {v0}, Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventsDiscoveryFiltersModel$EventDiscoverSuggestionFiltersModel;->a()LX/0Px;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2534646
    new-instance v0, LX/I5Q;

    invoke-direct {v0}, LX/I5Q;-><init>()V

    iget-object v2, p0, LX/I4u;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0837ef

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2534647
    iput-object v2, v0, LX/I5Q;->a:Ljava/lang/String;

    .line 2534648
    move-object v0, v0

    .line 2534649
    invoke-virtual {v0}, LX/I5Q;->a()Lcom/facebook/events/eventsdiscovery/protocol/EventsDiscoveryGraphQLModels$FetchEventsDiscoveryFiltersModel$EventDiscoverSuggestionFiltersModel$FilterItemsModel;

    move-result-object v0

    .line 2534650
    iget-object v2, p0, LX/I4u;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;

    iget-object v2, v2, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->F:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2534651
    iget-object v0, p0, LX/I4u;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->a$redex0(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;Z)V

    goto :goto_0

    .line 2534652
    :cond_1
    return-void
.end method
