.class public LX/JVN;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JVL;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouProductCardComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2700565
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/JVN;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouProductCardComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2700566
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2700567
    iput-object p1, p0, LX/JVN;->b:LX/0Ot;

    .line 2700568
    return-void
.end method

.method public static a(LX/0QB;)LX/JVN;
    .locals 4

    .prologue
    .line 2700569
    const-class v1, LX/JVN;

    monitor-enter v1

    .line 2700570
    :try_start_0
    sget-object v0, LX/JVN;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2700571
    sput-object v2, LX/JVN;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2700572
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2700573
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2700574
    new-instance v3, LX/JVN;

    const/16 p0, 0x2094

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JVN;-><init>(LX/0Ot;)V

    .line 2700575
    move-object v0, v3

    .line 2700576
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2700577
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JVN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2700578
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2700579
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2700580
    const v0, -0x7acbb328

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 2700581
    check-cast p2, LX/JVM;

    .line 2700582
    iget-object v0, p0, LX/JVN;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouProductCardComponentSpec;

    iget-object v1, p2, LX/JVM;->a:Lcom/facebook/graphql/model/GraphQLProductItem;

    iget-object v2, p2, LX/JVM;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, LX/JVM;->c:Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsEdge;

    .line 2700583
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    const v5, 0x7f020a3d

    invoke-interface {v4, v5}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v4

    iget-object v5, v0, Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouProductCardComponentSpec;->d:LX/99j;

    invoke-virtual {v5}, LX/99j;->b()F

    move-result v5

    float-to-int v5, v5

    invoke-interface {v4, v5}, LX/1Dh;->H(I)LX/1Dh;

    move-result-object v4

    const/16 v5, 0x8

    const v6, 0x7f0b00bf

    invoke-interface {v4, v5, v6}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v4

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-virtual {v4, v6}, LX/1up;->c(F)LX/1up;

    move-result-object v6

    iget-object v4, v0, Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouProductCardComponentSpec;->b:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1Ad;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProductItem;->P()Lcom/facebook/graphql/model/GraphQLProductImage;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLProductImage;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v4

    sget-object v7, Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouProductCardComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v4, v7}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v4

    invoke-virtual {v4}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v4

    invoke-virtual {v6, v4}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    .line 2700584
    const v6, -0x7acbb328

    const/4 v7, 0x0

    invoke-static {p1, v6, v7}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v6

    move-object v6, v6

    .line 2700585
    invoke-interface {v4, v6}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v4

    invoke-interface {v5, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouProductCardComponentSpec;->a(LX/1De;)LX/1Di;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    const/4 p2, 0x1

    const/4 p0, 0x0

    .line 2700586
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x6

    const v7, 0x7f0b00bd

    invoke-interface {v5, v6, v7}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x7

    const v7, 0x7f0b00d2

    invoke-interface {v5, v6, v7}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v5

    const v6, 0x7f0a0097

    invoke-interface {v5, v6}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v5

    const v6, 0x7f0e0120

    invoke-static {p1, p0, v6}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v6

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProductItem;->y()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, p2}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v6

    sget-object v7, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v6, v7}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v6

    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v5

    const v6, 0x7f0e012d

    invoke-static {p1, p0, v6}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v6

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProductItem;->T()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, p2}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v6

    sget-object v7, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v6, v7}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v6

    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v5

    move-object v5, v5

    .line 2700587
    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouProductCardComponentSpec;->a(LX/1De;)LX/1Di;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-static {v0, p1, v1, v2, v3}, Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouProductCardComponentSpec;->b(Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouProductCardComponentSpec;LX/1De;Lcom/facebook/graphql/model/GraphQLProductItem;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsEdge;)LX/1Di;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 2700588
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2700589
    invoke-static {}, LX/1dS;->b()V

    .line 2700590
    iget v0, p1, LX/1dQ;->b:I

    .line 2700591
    packed-switch v0, :pswitch_data_0

    .line 2700592
    :goto_0
    return-object v2

    .line 2700593
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2700594
    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2700595
    check-cast v1, LX/JVM;

    .line 2700596
    iget-object v3, p0, LX/JVN;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouProductCardComponentSpec;

    iget-object v4, v1, LX/JVM;->a:Lcom/facebook/graphql/model/GraphQLProductItem;

    iget-object v5, v1, LX/JVM;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object p1, v1, LX/JVM;->c:Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsEdge;

    .line 2700597
    iget-object p2, v3, Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouProductCardComponentSpec;->c:LX/7j6;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLProductItem;->t()Ljava/lang/String;

    move-result-object p0

    sget-object v1, LX/7iP;->PDFY:LX/7iP;

    invoke-virtual {p2, p0, v1}, LX/7j6;->a(Ljava/lang/String;LX/7iP;)V

    .line 2700598
    iget-object p2, v5, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p2, p2

    .line 2700599
    check-cast p2, Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;

    const-string p0, "pdfy_open"

    invoke-static {v3, p2, p1, p0}, Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouProductCardComponentSpec;->a$redex0(Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouProductCardComponentSpec;Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnit;Lcom/facebook/graphql/model/GraphQLProductsDealsForYouFeedUnitProductItemsEdge;Ljava/lang/String;)V

    .line 2700600
    iget-object p2, v3, Lcom/facebook/feedplugins/pdfy/components/ProductsDealsForYouProductCardComponentSpec;->g:LX/1g6;

    sget-object p0, LX/7iQ;->PDFY_PRODUCT_CLICK:LX/7iQ;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLProductItem;->t()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, p0, v1}, LX/1g6;->a(LX/7iQ;Ljava/lang/String;)V

    .line 2700601
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x7acbb328
        :pswitch_0
    .end packed-switch
.end method
