.class public LX/IpF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/IoC;


# instance fields
.field private final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2612598
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2612599
    iput-object p1, p0, LX/IpF;->a:LX/0Zb;

    .line 2612600
    return-void
.end method

.method public static a(LX/0QB;)LX/IpF;
    .locals 1

    .prologue
    .line 2612601
    invoke-static {p0}, LX/IpF;->b(LX/0QB;)LX/IpF;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/IpF;
    .locals 2

    .prologue
    .line 2612602
    new-instance v1, LX/IpF;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-direct {v1, v0}, LX/IpF;-><init>(LX/0Zb;)V

    .line 2612603
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/facebook/messaging/payment/value/input/MessengerPayData;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2612604
    const-string v0, "payment_request"

    invoke-static {p3, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;

    .line 2612605
    invoke-virtual {p0, p1, v0}, LX/IpF;->a(Ljava/lang/String;Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;)V

    .line 2612606
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;)V
    .locals 6

    .prologue
    .line 2612607
    if-eqz p2, :cond_0

    .line 2612608
    const-string v0, "p2p_request"

    invoke-static {p1, v0}, Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;->d(Ljava/lang/String;Ljava/lang/String;)LX/5fz;

    move-result-object v0

    .line 2612609
    new-instance v1, Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {p2}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;->b()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;->b()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentCurrencyQuantityModel;->a()I

    move-result v3

    int-to-long v4, v3

    invoke-direct {v1, v2, v4, v5}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;J)V

    .line 2612610
    invoke-virtual {p2}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/5fz;->b(Ljava/lang/String;)LX/5fz;

    move-result-object v2

    invoke-virtual {p2}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;->l()Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentUserModel;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/5fz;->i(Ljava/lang/String;)LX/5fz;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/5fz;->a(Lcom/facebook/payments/currency/CurrencyAmount;)LX/5fz;

    .line 2612611
    iget-object v1, p0, LX/IpF;->a:LX/0Zb;

    .line 2612612
    iget-object v2, v0, LX/5fz;->a:Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;

    move-object v0, v2

    .line 2612613
    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2612614
    :cond_0
    return-void
.end method
