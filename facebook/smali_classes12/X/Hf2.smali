.class public final LX/Hf2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7Ru;


# instance fields
.field public final synthetic a:LX/Hf5;


# direct methods
.method public constructor <init>(LX/Hf5;)V
    .locals 0

    .prologue
    .line 2491056
    iput-object p1, p0, LX/Hf2;->a:LX/Hf5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0oG;)V
    .locals 3

    .prologue
    .line 2491057
    iget-object v0, p0, LX/Hf2;->a:LX/Hf5;

    iget-object v0, v0, LX/Hf5;->k:LX/Hex;

    .line 2491058
    iget-object v1, v0, LX/Hex;->b:Lcom/facebook/video/watchandgo/view/WatchAndGoVideoPlayer;

    move-object v0, v1

    .line 2491059
    iget-object v1, p0, LX/Hf2;->a:LX/Hf5;

    iget-object v1, v1, LX/Hf5;->s:LX/Her;

    .line 2491060
    sget-object v2, LX/Hes;->a:[I

    invoke-virtual {v1}, LX/Her;->ordinal()I

    move-result p0

    aget v2, v2, p0

    packed-switch v2, :pswitch_data_0

    .line 2491061
    sget-object v2, LX/7Rr;->UNDOCKED:LX/7Rr;

    :goto_0
    move-object v1, v2

    .line 2491062
    sget-object v2, LX/7Rq;->PLAYER_DOCK_POSITION:LX/7Rq;

    iget-object v2, v2, LX/7Rq;->value:Ljava/lang/String;

    iget-object v1, v1, LX/7Rr;->value:Ljava/lang/String;

    invoke-virtual {p1, v2, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2491063
    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getVideoId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v0

    invoke-static {p1, v1, v0}, LX/Hf5;->b(LX/0oG;Ljava/lang/String;I)Z

    .line 2491064
    return-void

    .line 2491065
    :pswitch_0
    sget-object v2, LX/7Rr;->TOP_LEFT:LX/7Rr;

    goto :goto_0

    .line 2491066
    :pswitch_1
    sget-object v2, LX/7Rr;->TOP_RIGHT:LX/7Rr;

    goto :goto_0

    .line 2491067
    :pswitch_2
    sget-object v2, LX/7Rr;->BOTTOM_LEFT:LX/7Rr;

    goto :goto_0

    .line 2491068
    :pswitch_3
    sget-object v2, LX/7Rr;->BOTTOM_RIGHT:LX/7Rr;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
