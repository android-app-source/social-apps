.class public final LX/JMB;
.super LX/0vn;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2683016
    invoke-direct {p0}, LX/0vn;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/3sp;)V
    .locals 3

    .prologue
    .line 2683004
    invoke-super {p0, p1, p2}, LX/0vn;->a(Landroid/view/View;LX/3sp;)V

    .line 2683005
    check-cast p1, LX/JLv;

    .line 2683006
    const-class v0, Landroid/widget/ScrollView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/3sp;->b(Ljava/lang/CharSequence;)V

    .line 2683007
    invoke-virtual {p1}, LX/JLv;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2683008
    invoke-static {p1}, LX/JLv;->getScrollRange(LX/JLv;)I

    move-result v0

    .line 2683009
    if-lez v0, :cond_1

    .line 2683010
    const/4 v1, 0x1

    invoke-virtual {p2, v1}, LX/3sp;->i(Z)V

    .line 2683011
    invoke-virtual {p1}, LX/JLv;->getScrollY()I

    move-result v1

    iget v2, p1, LX/JLv;->A:I

    if-le v1, v2, :cond_0

    .line 2683012
    const/16 v1, 0x2000

    invoke-virtual {p2, v1}, LX/3sp;->a(I)V

    .line 2683013
    :cond_0
    invoke-virtual {p1}, LX/JLv;->getScrollY()I

    move-result v1

    if-ge v1, v0, :cond_1

    .line 2683014
    const/16 v0, 0x1000

    invoke-virtual {p2, v0}, LX/3sp;->a(I)V

    .line 2683015
    :cond_1
    return-void
.end method

.method public final a(Landroid/view/View;ILandroid/os/Bundle;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2682987
    invoke-super {p0, p1, p2, p3}, LX/0vn;->a(Landroid/view/View;ILandroid/os/Bundle;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2682988
    :goto_0
    return v0

    .line 2682989
    :cond_0
    check-cast p1, LX/JLv;

    .line 2682990
    invoke-virtual {p1}, LX/JLv;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_1

    move v0, v1

    .line 2682991
    goto :goto_0

    .line 2682992
    :cond_1
    sparse-switch p2, :sswitch_data_0

    move v0, v1

    .line 2682993
    goto :goto_0

    .line 2682994
    :sswitch_0
    invoke-virtual {p1}, LX/JLv;->getHeight()I

    move-result v2

    invoke-virtual {p1}, LX/JLv;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p1}, LX/JLv;->getPaddingTop()I

    move-result v3

    sub-int/2addr v2, v3

    .line 2682995
    invoke-virtual {p1}, LX/JLv;->getScrollY()I

    move-result v3

    add-int/2addr v2, v3

    invoke-static {p1}, LX/JLv;->getScrollRange(LX/JLv;)I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 2682996
    invoke-virtual {p1}, LX/JLv;->getScrollY()I

    move-result v3

    if-eq v2, v3, :cond_2

    .line 2682997
    invoke-virtual {p1, v1, v2}, LX/JLv;->a(II)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 2682998
    goto :goto_0

    .line 2682999
    :sswitch_1
    invoke-virtual {p1}, LX/JLv;->getHeight()I

    move-result v2

    invoke-virtual {p1}, LX/JLv;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p1}, LX/JLv;->getPaddingTop()I

    move-result v3

    sub-int/2addr v2, v3

    .line 2683000
    invoke-virtual {p1}, LX/JLv;->getScrollY()I

    move-result v3

    sub-int v2, v3, v2

    iget v3, p1, LX/JLv;->B:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 2683001
    invoke-virtual {p1}, LX/JLv;->getScrollY()I

    move-result v3

    if-eq v2, v3, :cond_3

    .line 2683002
    invoke-virtual {p1, v1, v2}, LX/JLv;->a(II)V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 2683003
    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1000 -> :sswitch_0
        0x2000 -> :sswitch_1
    .end sparse-switch
.end method

.method public final d(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2

    .prologue
    .line 2682975
    invoke-super {p0, p1, p2}, LX/0vn;->d(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 2682976
    check-cast p1, LX/JLv;

    .line 2682977
    const-class v0, Landroid/widget/ScrollView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 2682978
    invoke-static {p2}, LX/2fV;->a(Landroid/view/accessibility/AccessibilityEvent;)LX/2fO;

    move-result-object v1

    .line 2682979
    invoke-static {p1}, LX/JLv;->getScrollRange(LX/JLv;)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    .line 2682980
    :goto_0
    invoke-virtual {v1, v0}, LX/2fO;->a(Z)V

    .line 2682981
    invoke-virtual {p1}, LX/JLv;->getScrollX()I

    move-result v0

    invoke-virtual {v1, v0}, LX/2fO;->d(I)V

    .line 2682982
    invoke-virtual {p1}, LX/JLv;->getScrollY()I

    move-result v0

    invoke-virtual {v1, v0}, LX/2fO;->e(I)V

    .line 2682983
    invoke-virtual {p1}, LX/JLv;->getScrollX()I

    move-result v0

    invoke-virtual {v1, v0}, LX/2fO;->f(I)V

    .line 2682984
    invoke-static {p1}, LX/JLv;->getScrollRange(LX/JLv;)I

    move-result v0

    invoke-virtual {v1, v0}, LX/2fO;->g(I)V

    .line 2682985
    return-void

    .line 2682986
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
