.class public LX/J4p;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/J4p;


# instance fields
.field public final a:LX/J5g;

.field public final b:Lcom/facebook/privacy/PrivacyOperationsClient;

.field public final c:LX/0SG;

.field public final d:Ljava/util/concurrent/ExecutorService;

.field public final e:LX/3R3;

.field private final f:LX/1Ck;


# direct methods
.method public constructor <init>(LX/J5g;Lcom/facebook/privacy/PrivacyOperationsClient;LX/0SG;Ljava/util/concurrent/ExecutorService;LX/3R3;LX/1Ck;)V
    .locals 0
    .param p4    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2644500
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2644501
    iput-object p1, p0, LX/J4p;->a:LX/J5g;

    .line 2644502
    iput-object p2, p0, LX/J4p;->b:Lcom/facebook/privacy/PrivacyOperationsClient;

    .line 2644503
    iput-object p3, p0, LX/J4p;->c:LX/0SG;

    .line 2644504
    iput-object p4, p0, LX/J4p;->d:Ljava/util/concurrent/ExecutorService;

    .line 2644505
    iput-object p5, p0, LX/J4p;->e:LX/3R3;

    .line 2644506
    iput-object p6, p0, LX/J4p;->f:LX/1Ck;

    .line 2644507
    return-void
.end method

.method public static a(LX/0QB;)LX/J4p;
    .locals 10

    .prologue
    .line 2644508
    sget-object v0, LX/J4p;->g:LX/J4p;

    if-nez v0, :cond_1

    .line 2644509
    const-class v1, LX/J4p;

    monitor-enter v1

    .line 2644510
    :try_start_0
    sget-object v0, LX/J4p;->g:LX/J4p;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2644511
    if-eqz v2, :cond_0

    .line 2644512
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2644513
    new-instance v3, LX/J4p;

    invoke-static {v0}, LX/J5g;->a(LX/0QB;)LX/J5g;

    move-result-object v4

    check-cast v4, LX/J5g;

    invoke-static {v0}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(LX/0QB;)Lcom/facebook/privacy/PrivacyOperationsClient;

    move-result-object v5

    check-cast v5, Lcom/facebook/privacy/PrivacyOperationsClient;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SG;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/3R3;->b(LX/0QB;)LX/3R3;

    move-result-object v8

    check-cast v8, LX/3R3;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v9

    check-cast v9, LX/1Ck;

    invoke-direct/range {v3 .. v9}, LX/J4p;-><init>(LX/J5g;Lcom/facebook/privacy/PrivacyOperationsClient;LX/0SG;Ljava/util/concurrent/ExecutorService;LX/3R3;LX/1Ck;)V

    .line 2644514
    move-object v0, v3

    .line 2644515
    sput-object v0, LX/J4p;->g:LX/J4p;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2644516
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2644517
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2644518
    :cond_1
    sget-object v0, LX/J4p;->g:LX/J4p;

    return-object v0

    .line 2644519
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2644520
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$FetchProfilePhotoCheckupQueryModel;LX/J4j;)V
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2644521
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$FetchProfilePhotoCheckupQueryModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$FetchProfilePhotoCheckupQueryModel$PhotoCheckupInfoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$FetchProfilePhotoCheckupQueryModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$FetchProfilePhotoCheckupQueryModel$PhotoCheckupInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$FetchProfilePhotoCheckupQueryModel$PhotoCheckupInfoModel;->k()Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$PhotosModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2644522
    :cond_0
    :goto_0
    return-void

    .line 2644523
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$FetchProfilePhotoCheckupQueryModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$FetchProfilePhotoCheckupQueryModel$PhotoCheckupInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$FetchProfilePhotoCheckupQueryModel$PhotoCheckupInfoModel;->k()Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$PhotosModel;

    move-result-object v0

    invoke-static {v0, p1}, LX/J4p;->b(Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$PhotosModel;LX/J4j;)V

    .line 2644524
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$FetchProfilePhotoCheckupQueryModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$FetchProfilePhotoCheckupQueryModel$PhotoCheckupInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$FetchProfilePhotoCheckupQueryModel$PhotoCheckupInfoModel;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    .line 2644525
    iput-object v0, p1, LX/J4j;->e:LX/1Fb;

    .line 2644526
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$FetchProfilePhotoCheckupQueryModel;->a()Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$FetchProfilePhotoCheckupQueryModel$PhotoCheckupInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$FetchProfilePhotoCheckupQueryModel$PhotoCheckupInfoModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    .line 2644527
    iput-object v0, p1, LX/J4j;->f:LX/1Fb;

    .line 2644528
    goto :goto_0
.end method

.method public static b(Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$PhotosModel;LX/J4j;)V
    .locals 2

    .prologue
    .line 2644529
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    .line 2644530
    iget-object v1, p1, LX/J4j;->c:LX/0Px;

    move-object v1, v1

    .line 2644531
    if-eqz v1, :cond_0

    .line 2644532
    iget-object v1, p1, LX/J4j;->c:LX/0Px;

    move-object v1, v1

    .line 2644533
    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2644534
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$PhotosModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2644535
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2644536
    iput-object v0, p1, LX/J4j;->c:LX/0Px;

    .line 2644537
    invoke-virtual {p0}, Lcom/facebook/privacy/checkup/protocol/FetchPhotoCheckupModels$PhotoCheckupPOPMediaModel$PhotosModel;->j()Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;

    move-result-object v0

    .line 2644538
    if-eqz v0, :cond_1

    .line 2644539
    invoke-virtual {v0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 2644540
    iput-object v1, p1, LX/J4j;->g:Ljava/lang/String;

    .line 2644541
    invoke-virtual {v0}, Lcom/facebook/photos/mediafetcher/protocol/MediaFetchQueriesModels$MediaPageInfoModel;->b()Z

    move-result v0

    .line 2644542
    iput-boolean v0, p1, LX/J4j;->h:Z

    .line 2644543
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/5mv;LX/5mu;Ljava/lang/String;LX/0Vd;)V
    .locals 8
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/5mv;",
            "LX/5mu;",
            "Ljava/lang/String;",
            "LX/0Vd",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2644544
    iget-object v6, p0, LX/J4p;->f:LX/1Ck;

    sget-object v7, LX/J4o;->SEND_BULK_PRIVACY_EDIT:LX/J4o;

    new-instance v0, LX/J4l;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/J4l;-><init>(LX/J4p;Ljava/lang/String;LX/5mv;LX/5mu;Ljava/lang/String;)V

    invoke-virtual {v6, v7, v0, p5}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2644545
    return-void
.end method

.method public final a(Ljava/util/Map;LX/0Vd;)Z
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/1oT;",
            ">;",
            "LX/0Vd",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 2644546
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    .line 2644547
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Ljava/util/Map$Entry;

    .line 2644548
    new-instance v0, Lcom/facebook/privacy/protocol/EditObjectsPrivacyParams$ObjectPrivacyEdit;

    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, LX/J4p;->c:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;->ENT_CONTENT:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/1oT;

    invoke-interface {v5}, LX/1oT;->c()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/facebook/privacy/protocol/EditObjectsPrivacyParams$ObjectPrivacyEdit;-><init>(Ljava/lang/String;JLcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;Ljava/lang/String;)V

    .line 2644549
    invoke-virtual {v7, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 2644550
    :cond_0
    if-eqz p2, :cond_1

    .line 2644551
    iget-object v0, p0, LX/J4p;->f:LX/1Ck;

    sget-object v1, LX/J4o;->SEND_PRIVACY_EDITS:LX/J4o;

    new-instance v2, LX/J4k;

    invoke-direct {v2, p0, v7}, LX/J4k;-><init>(LX/J4p;LX/0Pz;)V

    invoke-virtual {v0, v1, v2, p2}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    move-result v0

    .line 2644552
    :goto_1
    return v0

    .line 2644553
    :cond_1
    iget-object v0, p0, LX/J4p;->b:Lcom/facebook/privacy/PrivacyOperationsClient;

    const-string v1, "none"

    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v6}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(Ljava/lang/String;LX/0Px;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move v0, v6

    .line 2644554
    goto :goto_1
.end method
