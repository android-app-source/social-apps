.class public LX/HvK;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Hue;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Hue;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2518950
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2518951
    iput-object p1, p0, LX/HvK;->a:LX/0Ot;

    .line 2518952
    iput-object p2, p0, LX/HvK;->b:LX/0Ot;

    .line 2518953
    return-void
.end method


# virtual methods
.method public final a(LX/B5j;Ljava/lang/String;)LX/AQ9;
    .locals 7
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData::",
            "Lcom/facebook/ipc/composer/plugin/ComposerPluginDataProvider;",
            "DerivedData::",
            "Lcom/facebook/composer/system/api/ComposerDerivedData;",
            "Mutation::",
            "Lcom/facebook/ipc/composer/plugin/ComposerPluginMutation",
            "<TMutation;>;>(",
            "LX/B5j",
            "<TModelData;TDerivedData;TMutation;>;",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/ipc/composer/plugin/ComposerPlugin",
            "<TModelData;TDerivedData;TMutation;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2518954
    invoke-virtual {p1}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v0

    invoke-interface {v0}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getPluginConfig()Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v3

    .line 2518955
    :try_start_0
    iget-object v0, p0, LX/HvK;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hue;

    if-eqz p2, :cond_0

    invoke-static {p2}, LX/B5f;->a(Ljava/lang/String;)LX/B5f;

    move-result-object v1

    :goto_0
    invoke-virtual {v0, v3, p1, v1}, LX/Hue;->a(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;LX/B5j;LX/B5f;)LX/AQ9;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2518956
    :goto_1
    return-object v0

    :cond_0
    move-object v1, v2

    .line 2518957
    goto :goto_0

    .line 2518958
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2518959
    iget-object v0, p0, LX/HvK;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v4, "composer_load_model_from_saved_session_failed"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Failed to restore the composer plugin: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2518960
    if-nez p2, :cond_1

    .line 2518961
    invoke-static {v1}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 2518962
    :cond_1
    iget-object v0, p0, LX/HvK;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hue;

    invoke-virtual {v0, v3, p1, v2}, LX/Hue;->a(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;LX/B5j;LX/B5f;)LX/AQ9;

    move-result-object v0

    goto :goto_1
.end method
