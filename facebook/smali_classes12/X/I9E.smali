.class public final LX/I9E;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;


# direct methods
.method public constructor <init>(Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;)V
    .locals 0

    .prologue
    .line 2542568
    iput-object p1, p0, LX/I9E;->a:Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x7586e7fa

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2542569
    new-instance v1, LX/6WS;

    iget-object v2, p0, LX/I9E;->a:Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;

    invoke-virtual {v2}, Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/6WS;-><init>(Landroid/content/Context;)V

    .line 2542570
    invoke-virtual {v1}, LX/5OM;->c()LX/5OG;

    move-result-object v2

    .line 2542571
    iget-object v3, p0, LX/I9E;->a:Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;

    iget-object v3, v3, Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;->g:Lcom/facebook/events/model/Event;

    sget-object v4, LX/7vK;->ADMIN:LX/7vK;

    invoke-virtual {v3, v4}, Lcom/facebook/events/model/Event;->a(LX/7vK;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2542572
    const v3, 0x7f082f36

    invoke-virtual {v2, v3}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v3

    .line 2542573
    new-instance v4, LX/I9A;

    invoke-direct {v4, p0}, LX/I9A;-><init>(LX/I9E;)V

    invoke-virtual {v3, v4}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2542574
    const v3, 0x7f081f5e

    invoke-virtual {v2, v3}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v2

    .line 2542575
    new-instance v3, LX/I9B;

    invoke-direct {v3, p0}, LX/I9B;-><init>(LX/I9E;)V

    invoke-virtual {v2, v3}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2542576
    :goto_0
    iget-object v2, p0, LX/I9E;->a:Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;

    iget-object v2, v2, Lcom/facebook/events/permalink/cancelevent/CancelEventBannerView;->f:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v1, v2}, LX/0ht;->f(Landroid/view/View;)V

    .line 2542577
    const v1, -0xb0a88f2

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2542578
    :cond_0
    const v3, 0x7f081f5c

    invoke-virtual {v2, v3}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v3

    .line 2542579
    new-instance v4, LX/I9C;

    invoke-direct {v4, p0}, LX/I9C;-><init>(LX/I9E;)V

    invoke-virtual {v3, v4}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2542580
    const v3, 0x7f081f5d

    invoke-virtual {v2, v3}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v2

    .line 2542581
    new-instance v3, LX/I9D;

    invoke-direct {v3, p0}, LX/I9D;-><init>(LX/I9E;)V

    invoke-virtual {v2, v3}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_0
.end method
