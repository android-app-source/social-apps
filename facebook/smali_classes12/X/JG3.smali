.class public final LX/JG3;
.super LX/1ci;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1ci",
        "<",
        "LX/1FJ",
        "<",
        "LX/1ln;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:I

.field public final synthetic b:I

.field public final synthetic c:I

.field public final synthetic d:I

.field public final synthetic e:Ljava/lang/String;

.field public final synthetic f:Lcom/facebook/adinterfaces/react/AdInterfacesCallbackModule;


# direct methods
.method public constructor <init>(Lcom/facebook/adinterfaces/react/AdInterfacesCallbackModule;IIIILjava/lang/String;)V
    .locals 0

    .prologue
    .line 2666713
    iput-object p1, p0, LX/JG3;->f:Lcom/facebook/adinterfaces/react/AdInterfacesCallbackModule;

    iput p2, p0, LX/JG3;->a:I

    iput p3, p0, LX/JG3;->b:I

    iput p4, p0, LX/JG3;->c:I

    iput p5, p0, LX/JG3;->d:I

    iput-object p6, p0, LX/JG3;->e:Ljava/lang/String;

    invoke-direct {p0}, LX/1ci;-><init>()V

    return-void
.end method


# virtual methods
.method public final e(LX/1ca;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2666698
    invoke-interface {p1}, LX/1ca;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2666699
    :goto_0
    return-void

    .line 2666700
    :cond_0
    invoke-interface {p1}, LX/1ca;->d()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, LX/1FJ;

    .line 2666701
    const/4 v2, 0x0

    .line 2666702
    invoke-virtual {v8}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ln;

    .line 2666703
    if-eqz v8, :cond_3

    instance-of v3, v0, LX/1lm;

    if-eqz v3, :cond_3

    .line 2666704
    instance-of v2, v0, LX/1ll;

    if-eqz v2, :cond_1

    move-object v1, v0

    check-cast v1, LX/1ll;

    .line 2666705
    iget v2, v1, LX/1ll;->d:I

    move v1, v2

    .line 2666706
    :cond_1
    check-cast v0, LX/1lm;

    invoke-virtual {v0}, LX/1lm;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    move v6, v1

    move-object v1, v0

    .line 2666707
    :goto_1
    if-nez v1, :cond_2

    .line 2666708
    :try_start_0
    iget-object v0, p0, LX/JG3;->f:Lcom/facebook/adinterfaces/react/AdInterfacesCallbackModule;

    iget-object v0, v0, Lcom/facebook/adinterfaces/react/AdInterfacesCallbackModule;->b:LX/98q;

    invoke-virtual {v0}, LX/98q;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2666709
    invoke-static {v8}, LX/1FJ;->c(LX/1FJ;)V

    goto :goto_0

    .line 2666710
    :cond_2
    :try_start_1
    iget-object v0, p0, LX/JG3;->f:Lcom/facebook/adinterfaces/react/AdInterfacesCallbackModule;

    iget v2, p0, LX/JG3;->a:I

    iget v3, p0, LX/JG3;->b:I

    iget v4, p0, LX/JG3;->c:I

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    iget v7, p0, LX/JG3;->a:I

    sub-int/2addr v5, v7

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    iget v5, p0, LX/JG3;->d:I

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    iget v9, p0, LX/JG3;->b:I

    sub-int/2addr v7, v9

    invoke-static {v5, v7}, Ljava/lang/Math;->min(II)I

    move-result v5

    iget-object v7, p0, LX/JG3;->e:Ljava/lang/String;

    .line 2666711
    invoke-static/range {v0 .. v7}, Lcom/facebook/adinterfaces/react/AdInterfacesCallbackModule;->a$redex0(Lcom/facebook/adinterfaces/react/AdInterfacesCallbackModule;Landroid/graphics/Bitmap;IIIIILjava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2666712
    invoke-static {v8}, LX/1FJ;->c(LX/1FJ;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v8}, LX/1FJ;->c(LX/1FJ;)V

    throw v0

    :cond_3
    move v6, v1

    move-object v1, v2

    goto :goto_1
.end method

.method public final f(LX/1ca;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 2666696
    iget-object v0, p0, LX/JG3;->f:Lcom/facebook/adinterfaces/react/AdInterfacesCallbackModule;

    iget-object v0, v0, Lcom/facebook/adinterfaces/react/AdInterfacesCallbackModule;->b:LX/98q;

    invoke-virtual {v0}, LX/98q;->a()V

    .line 2666697
    return-void
.end method
