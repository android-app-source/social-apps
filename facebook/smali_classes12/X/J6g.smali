.class public final LX/J6g;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;)V
    .locals 0

    .prologue
    .line 2649956
    iput-object p1, p0, LX/J6g;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v3, 0x0

    const/4 v0, 0x1

    const v1, -0x26050601

    invoke-static {v9, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v8

    .line 2649957
    iget-object v0, p0, LX/J6g;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "https://www.facebook.com/privacy/review/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/J6g;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;

    iget-object v2, v2, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->d:LX/J3t;

    .line 2649958
    iget-object v4, v2, LX/J3t;->i:Ljava/lang/String;

    move-object v2, v4

    .line 2649959
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v6, 0x1

    .line 2649960
    sget-object v2, LX/21D;->PRIVACY_CHECKUP:LX/21D;

    const-string v4, "privacyCheckupShare"

    invoke-static {v1}, LX/89G;->a(Ljava/lang/String;)LX/89G;

    move-result-object v5

    invoke-virtual {v5}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v5

    invoke-static {v2, v4, v5}, LX/1nC;->a(LX/21D;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    .line 2649961
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsEditTagEnabled(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setDisableFriendTagging(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setDisableMentions(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    .line 2649962
    iget-object v4, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->f:LX/1Kf;

    const/4 v5, 0x0

    const/16 v6, 0x6dc

    invoke-interface {v4, v5, v2, v6, v0}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/support/v4/app/Fragment;)V

    .line 2649963
    iget-object v0, p0, LX/J6g;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->h:LX/J5k;

    iget-object v1, p0, LX/J6g;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;

    iget-object v1, v1, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;->d:LX/J3t;

    .line 2649964
    iget-object v2, v1, LX/J3t;->i:Ljava/lang/String;

    move-object v1, v2

    .line 2649965
    const-string v2, "share_timeline"

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, LX/J5k;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Long;)V

    .line 2649966
    const v0, -0x78508b7e

    invoke-static {v9, v9, v0, v8}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
