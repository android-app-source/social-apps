.class public LX/HXZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/HBQ;


# instance fields
.field private a:LX/8A4;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/8A4;LX/0Ot;)V
    .locals 1
    .param p1    # LX/8A4;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/8A4;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2479121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2479122
    const/4 v0, 0x0

    iput-object v0, p0, LX/HXZ;->a:LX/8A4;

    .line 2479123
    iput-object p1, p0, LX/HXZ;->a:LX/8A4;

    .line 2479124
    iput-object p2, p0, LX/HXZ;->b:LX/0Ot;

    .line 2479125
    return-void
.end method


# virtual methods
.method public final a()LX/8A4;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2479126
    iget-object v0, p0, LX/HXZ;->a:LX/8A4;

    if-nez v0, :cond_0

    .line 2479127
    iget-object v0, p0, LX/HXZ;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ProfilePermissions null when timeline is using it"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2479128
    :cond_0
    iget-object v0, p0, LX/HXZ;->a:LX/8A4;

    return-object v0
.end method
