.class public final LX/HpX;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2509609
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_b

    .line 2509610
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2509611
    :goto_0
    return v1

    .line 2509612
    :cond_0
    const-string v12, "can_upload"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 2509613
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v8, v0

    move v0, v2

    .line 2509614
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_9

    .line 2509615
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 2509616
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2509617
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 2509618
    const-string v12, "album_cover_photo"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 2509619
    invoke-static {p0, p1}, LX/HpR;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 2509620
    :cond_2
    const-string v12, "album_type"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 2509621
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v9

    goto :goto_1

    .line 2509622
    :cond_3
    const-string v12, "id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 2509623
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 2509624
    :cond_4
    const-string v12, "media"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 2509625
    invoke-static {p0, p1}, LX/HpT;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 2509626
    :cond_5
    const-string v12, "media_owner_object"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 2509627
    invoke-static {p0, p1}, LX/HpS;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 2509628
    :cond_6
    const-string v12, "privacy_scope"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 2509629
    invoke-static {p0, p1}, LX/HpV;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 2509630
    :cond_7
    const-string v12, "title"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 2509631
    invoke-static {p0, p1}, LX/HpW;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 2509632
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 2509633
    :cond_9
    const/16 v11, 0x8

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 2509634
    invoke-virtual {p1, v1, v10}, LX/186;->b(II)V

    .line 2509635
    invoke-virtual {p1, v2, v9}, LX/186;->b(II)V

    .line 2509636
    if-eqz v0, :cond_a

    .line 2509637
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v8}, LX/186;->a(IZ)V

    .line 2509638
    :cond_a
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2509639
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2509640
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2509641
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2509642
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2509643
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_b
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    move v10, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2509644
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2509645
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2509646
    if-eqz v0, :cond_0

    .line 2509647
    const-string v1, "album_cover_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2509648
    invoke-static {p0, v0, p2, p3}, LX/HpR;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2509649
    :cond_0
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2509650
    if-eqz v0, :cond_1

    .line 2509651
    const-string v0, "album_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2509652
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2509653
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2509654
    if-eqz v0, :cond_2

    .line 2509655
    const-string v1, "can_upload"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2509656
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2509657
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2509658
    if-eqz v0, :cond_3

    .line 2509659
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2509660
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2509661
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2509662
    if-eqz v0, :cond_4

    .line 2509663
    const-string v1, "media"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2509664
    invoke-static {p0, v0, p2}, LX/HpT;->a(LX/15i;ILX/0nX;)V

    .line 2509665
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2509666
    if-eqz v0, :cond_5

    .line 2509667
    const-string v1, "media_owner_object"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2509668
    invoke-static {p0, v0, p2}, LX/HpS;->a(LX/15i;ILX/0nX;)V

    .line 2509669
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2509670
    if-eqz v0, :cond_6

    .line 2509671
    const-string v1, "privacy_scope"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2509672
    invoke-static {p0, v0, p2, p3}, LX/HpV;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2509673
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2509674
    if-eqz v0, :cond_7

    .line 2509675
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2509676
    invoke-static {p0, v0, p2}, LX/HpW;->a(LX/15i;ILX/0nX;)V

    .line 2509677
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2509678
    return-void
.end method
