.class public LX/Ij2;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements LX/Iik;


# instance fields
.field public a:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/content/Context;

.field public c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public d:Lcom/facebook/widget/text/BetterTextView;

.field public e:Lcom/facebook/widget/text/BetterTextView;

.field public f:Lcom/facebook/widget/text/BetterTextView;

.field public g:Lcom/facebook/widget/text/BetterTextView;

.field private h:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2605239
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/Ij2;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2605240
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2605237
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/Ij2;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2605238
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2605218
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2605219
    const-class v0, LX/Ij2;

    invoke-static {v0, p0}, LX/Ij2;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2605220
    const v0, 0x7f030efa

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2605221
    iput-object p1, p0, LX/Ij2;->b:Landroid/content/Context;

    .line 2605222
    const v0, 0x7f0d247c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/Ij2;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2605223
    const v0, 0x7f0d02c4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Ij2;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 2605224
    const v0, 0x7f0d02c3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Ij2;->e:Lcom/facebook/widget/text/BetterTextView;

    .line 2605225
    const v0, 0x7f0d247d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Ij2;->f:Lcom/facebook/widget/text/BetterTextView;

    .line 2605226
    const v0, 0x7f0d1286

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Ij2;->g:Lcom/facebook/widget/text/BetterTextView;

    .line 2605227
    const v0, 0x7f0d247e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Ij2;->h:Lcom/facebook/widget/text/BetterTextView;

    .line 2605228
    iget-object v0, p0, LX/Ij2;->h:Lcom/facebook/widget/text/BetterTextView;

    new-instance v1, LX/Ij0;

    invoke-direct {v1, p0}, LX/Ij0;-><init>(LX/Ij2;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2605229
    return-void
.end method

.method public static a(Lcom/facebook/widget/text/BetterTextView;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2605232
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2605233
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2605234
    :goto_0
    return-void

    .line 2605235
    :cond_0
    invoke-virtual {p0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2605236
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Ij2;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object p0

    check-cast p0, Lcom/facebook/content/SecureContextHelper;

    iput-object p0, p1, LX/Ij2;->a:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method


# virtual methods
.method public setListener(LX/Iit;)V
    .locals 2

    .prologue
    .line 2605230
    iget-object v0, p0, LX/Ij2;->f:Lcom/facebook/widget/text/BetterTextView;

    new-instance v1, LX/Ij1;

    invoke-direct {v1, p0, p1}, LX/Ij1;-><init>(LX/Ij2;LX/Iit;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2605231
    return-void
.end method
