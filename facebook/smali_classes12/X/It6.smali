.class public LX/It6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final d:Ljava/lang/Object;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKeyFactory;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/2Ow;

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "LX/It5;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2621925
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/It6;->d:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/2Ow;Lcom/facebook/user/model/User;)V
    .locals 6
    .param p3    # Lcom/facebook/user/model/User;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKeyFactory;",
            ">;",
            "LX/2Ow;",
            "Lcom/facebook/user/model/User;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2621926
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2621927
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/It6;->c:Ljava/util/Map;

    .line 2621928
    iput-object p1, p0, LX/It6;->a:LX/0Ot;

    .line 2621929
    iput-object p2, p0, LX/It6;->b:LX/2Ow;

    .line 2621930
    invoke-static {}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a()Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 2621931
    iget-object v1, p0, LX/It6;->c:Ljava/util/Map;

    new-instance v2, LX/It5;

    new-instance v3, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 2621932
    iget-object v4, p3, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v4, v4

    .line 2621933
    invoke-virtual {p3}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/facebook/messaging/model/messages/ParticipantInfo;-><init>(Lcom/facebook/user/model/UserKey;Ljava/lang/String;)V

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    invoke-direct {v2, v0, v3}, LX/It5;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/0Px;)V

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2621934
    return-void
.end method

.method public static a(LX/0QB;)LX/It6;
    .locals 9

    .prologue
    .line 2621935
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2621936
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2621937
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2621938
    if-nez v1, :cond_0

    .line 2621939
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2621940
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2621941
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2621942
    sget-object v1, LX/It6;->d:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2621943
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2621944
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2621945
    :cond_1
    if-nez v1, :cond_4

    .line 2621946
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2621947
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2621948
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2621949
    new-instance v8, LX/It6;

    const/16 v1, 0xd67

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/2Ow;->a(LX/0QB;)LX/2Ow;

    move-result-object v1

    check-cast v1, LX/2Ow;

    invoke-static {v0}, LX/0XE;->b(LX/0QB;)Lcom/facebook/user/model/User;

    move-result-object v7

    check-cast v7, Lcom/facebook/user/model/User;

    invoke-direct {v8, p0, v1, v7}, LX/It6;-><init>(LX/0Ot;LX/2Ow;Lcom/facebook/user/model/User;)V

    .line 2621950
    move-object v1, v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2621951
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2621952
    if-nez v1, :cond_2

    .line 2621953
    sget-object v0, LX/It6;->d:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/It6;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2621954
    :goto_1
    if-eqz v0, :cond_3

    .line 2621955
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2621956
    :goto_3
    check-cast v0, LX/It6;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2621957
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2621958
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2621959
    :catchall_1
    move-exception v0

    .line 2621960
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2621961
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2621962
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2621963
    :cond_2
    :try_start_8
    sget-object v0, LX/It6;->d:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/It6;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public static c(LX/It6;Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/It5;
    .locals 1

    .prologue
    .line 2621964
    invoke-static {p1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->h(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2621965
    iget-object v0, p0, LX/It6;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/It5;

    .line 2621966
    if-nez v0, :cond_0

    .line 2621967
    invoke-static {p1}, LX/It4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/It4;

    .line 2621968
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final clearUserData()V
    .locals 1

    .prologue
    .line 2621969
    iget-object v0, p0, LX/It6;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 2621970
    return-void
.end method
