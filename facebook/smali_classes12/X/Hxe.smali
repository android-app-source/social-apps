.class public final LX/Hxe;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Hxd;


# instance fields
.field public final synthetic a:Lcom/facebook/events/dashboard/EventsDashboardFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/EventsDashboardFragment;)V
    .locals 0

    .prologue
    .line 2522771
    iput-object p1, p0, LX/Hxe;->a:Lcom/facebook/events/dashboard/EventsDashboardFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;ILjava/lang/String;Z)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;",
            ">;I",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 2522772
    iget-object v0, p0, LX/Hxe;->a:Lcom/facebook/events/dashboard/EventsDashboardFragment;

    .line 2522773
    iget-object p0, v0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->W:LX/5O9;

    if-nez p0, :cond_0

    .line 2522774
    invoke-static {v0, p1, p2}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->b$redex0(Lcom/facebook/events/dashboard/EventsDashboardFragment;Ljava/util/List;I)V

    .line 2522775
    :goto_0
    return-void

    .line 2522776
    :cond_0
    iget-object p0, v0, Lcom/facebook/events/dashboard/EventsDashboardFragment;->X:Ljava/util/EnumMap;

    sget-object p3, LX/Hxr;->ON_SUBSCRIPTIONS:LX/Hxr;

    new-instance p4, Lcom/facebook/events/dashboard/EventsDashboardFragment$18;

    invoke-direct {p4, v0, p1, p2}, Lcom/facebook/events/dashboard/EventsDashboardFragment$18;-><init>(Lcom/facebook/events/dashboard/EventsDashboardFragment;Ljava/util/List;I)V

    invoke-virtual {p0, p3, p4}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
