.class public LX/J1i;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/J1c;

.field public final c:LX/J1k;

.field private final d:LX/J1d;

.field private final e:LX/J1o;

.field public final f:LX/Duk;

.field public final g:Landroid/os/Vibrator;

.field private final h:Z

.field public final i:Ljava/lang/String;

.field private final j:LX/J1e;

.field public k:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

.field public l:LX/Ioo;

.field public m:Landroid/animation/ValueAnimator;

.field private n:Landroid/animation/ValueAnimator;

.field public o:Z


# direct methods
.method public constructor <init>(LX/Ioo;ZLjava/lang/String;Landroid/content/Context;LX/J1c;LX/J1k;LX/J1d;LX/J1o;LX/Duk;Landroid/os/Vibrator;)V
    .locals 1
    .param p1    # LX/Ioo;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2639105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2639106
    new-instance v0, LX/J1e;

    invoke-direct {v0, p0}, LX/J1e;-><init>(LX/J1i;)V

    iput-object v0, p0, LX/J1i;->j:LX/J1e;

    .line 2639107
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/J1i;->o:Z

    .line 2639108
    iput-object p1, p0, LX/J1i;->l:LX/Ioo;

    .line 2639109
    iput-boolean p2, p0, LX/J1i;->h:Z

    .line 2639110
    iput-object p3, p0, LX/J1i;->i:Ljava/lang/String;

    .line 2639111
    iput-object p4, p0, LX/J1i;->a:Landroid/content/Context;

    .line 2639112
    iput-object p5, p0, LX/J1i;->b:LX/J1c;

    .line 2639113
    iput-object p6, p0, LX/J1i;->c:LX/J1k;

    .line 2639114
    iput-object p7, p0, LX/J1i;->d:LX/J1d;

    .line 2639115
    iput-object p8, p0, LX/J1i;->e:LX/J1o;

    .line 2639116
    iput-object p9, p0, LX/J1i;->f:LX/Duk;

    .line 2639117
    iput-object p10, p0, LX/J1i;->g:Landroid/os/Vibrator;

    .line 2639118
    return-void
.end method

.method public static a$redex0(LX/J1i;I)V
    .locals 4

    .prologue
    .line 2639119
    iget-object v0, p0, LX/J1i;->n:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 2639120
    iget-object v0, p0, LX/J1i;->n:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->end()V

    .line 2639121
    const/4 v0, 0x0

    iput-object v0, p0, LX/J1i;->n:Landroid/animation/ValueAnimator;

    .line 2639122
    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, LX/J1i;->n:Landroid/animation/ValueAnimator;

    .line 2639123
    iget-object v0, p0, LX/J1i;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0068

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 2639124
    iget-object v1, p0, LX/J1i;->n:Landroid/animation/ValueAnimator;

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 2639125
    iget-object v0, p0, LX/J1i;->n:Landroid/animation/ValueAnimator;

    new-instance v1, LX/J1g;

    invoke-direct {v1, p0, p1}, LX/J1g;-><init>(LX/J1i;I)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 2639126
    iget-object v0, p0, LX/J1i;->n:Landroid/animation/ValueAnimator;

    new-instance v1, LX/J1h;

    invoke-direct {v1, p0}, LX/J1h;-><init>(LX/J1i;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 2639127
    iget-object v0, p0, LX/J1i;->n:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 2639128
    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private c(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2639129
    iget-object v0, p0, LX/J1i;->d:LX/J1d;

    iget-boolean v1, p0, LX/J1i;->h:Z

    .line 2639130
    const-string v2, "."

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->getTrimmedLength(Ljava/lang/CharSequence;)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    .line 2639131
    :goto_0
    const/4 v3, 0x7

    if-lt v2, v3, :cond_1

    .line 2639132
    iget-object v2, v0, LX/J1d;->a:Landroid/content/res/Resources;

    const v3, 0x7f0b1e78

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2639133
    :goto_1
    move v0, v2

    .line 2639134
    int-to-float v0, v0

    .line 2639135
    iget-object v2, p0, LX/J1i;->k:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    invoke-virtual {v2}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->getTextSize()F

    move-result v2

    .line 2639136
    cmpl-float v3, v2, v0

    if-nez v3, :cond_6

    .line 2639137
    :goto_2
    return-void

    .line 2639138
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->getTrimmedLength(Ljava/lang/CharSequence;)I

    move-result v2

    goto :goto_0

    .line 2639139
    :cond_1
    const/4 v3, 0x6

    if-lt v2, v3, :cond_2

    .line 2639140
    iget-object v2, v0, LX/J1d;->a:Landroid/content/res/Resources;

    const v3, 0x7f0b1e77

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    goto :goto_1

    .line 2639141
    :cond_2
    const/4 v3, 0x5

    if-lt v2, v3, :cond_3

    .line 2639142
    iget-object v2, v0, LX/J1d;->a:Landroid/content/res/Resources;

    const v3, 0x7f0b1e76

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    goto :goto_1

    .line 2639143
    :cond_3
    const/4 v3, 0x4

    if-lt v2, v3, :cond_4

    .line 2639144
    iget-object v2, v0, LX/J1d;->a:Landroid/content/res/Resources;

    const v3, 0x7f0b1e75

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    goto :goto_1

    .line 2639145
    :cond_4
    if-eqz v1, :cond_5

    iget-object v2, v0, LX/J1d;->a:Landroid/content/res/Resources;

    const v3, 0x7f0b1e79

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    goto :goto_1

    :cond_5
    iget-object v2, v0, LX/J1d;->a:Landroid/content/res/Resources;

    const v3, 0x7f0b1e74

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    goto :goto_1

    .line 2639146
    :cond_6
    iget-object v3, p0, LX/J1i;->m:Landroid/animation/ValueAnimator;

    if-eqz v3, :cond_7

    .line 2639147
    iget-object v3, p0, LX/J1i;->m:Landroid/animation/ValueAnimator;

    invoke-virtual {v3}, Landroid/animation/ValueAnimator;->cancel()V

    .line 2639148
    const/4 v3, 0x0

    iput-object v3, p0, LX/J1i;->m:Landroid/animation/ValueAnimator;

    .line 2639149
    :cond_7
    const/4 v3, 0x2

    new-array v3, v3, [F

    const/4 v4, 0x0

    aput v2, v3, v4

    const/4 v2, 0x1

    aput v0, v3, v2

    invoke-static {v3}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v2

    iput-object v2, p0, LX/J1i;->m:Landroid/animation/ValueAnimator;

    .line 2639150
    iget-object v2, p0, LX/J1i;->m:Landroid/animation/ValueAnimator;

    iget-object v3, p0, LX/J1i;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c0067

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v2, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 2639151
    iget-object v2, p0, LX/J1i;->m:Landroid/animation/ValueAnimator;

    new-instance v3, LX/J1f;

    invoke-direct {v3, p0}, LX/J1f;-><init>(LX/J1i;)V

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 2639152
    iget-object v2, p0, LX/J1i;->m:Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_2
.end method

.method private d(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2639153
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/J1i;->o:Z

    .line 2639154
    iget-object v0, p0, LX/J1i;->k:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    invoke-virtual {v0}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/text/Editable;->append(Ljava/lang/CharSequence;)Landroid/text/Editable;

    .line 2639155
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/J1i;->o:Z

    .line 2639156
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2639157
    iget-object v0, p0, LX/J1i;->k:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    invoke-virtual {v0}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2639158
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2639159
    :cond_0
    :goto_0
    return-void

    .line 2639160
    :cond_1
    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 2639161
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 2639162
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_2

    .line 2639163
    const-string v0, "00"

    invoke-direct {p0, v0}, LX/J1i;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 2639164
    :cond_2
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    if-ne v1, v0, :cond_0

    .line 2639165
    const-string v0, "0"

    invoke-direct {p0, v0}, LX/J1i;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/payments/p2p/ui/DollarIconEditText;)V
    .locals 2

    .prologue
    .line 2639166
    iput-object p1, p0, LX/J1i;->k:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    .line 2639167
    iget-object v0, p0, LX/J1i;->k:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    iget-object v1, p0, LX/J1i;->e:LX/J1o;

    invoke-virtual {v0, v1}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2639168
    iget-object v0, p0, LX/J1i;->e:LX/J1o;

    iget-object v1, p0, LX/J1i;->j:LX/J1e;

    .line 2639169
    iput-object v1, v0, LX/J1o;->f:LX/J1e;

    .line 2639170
    iget-object v0, p0, LX/J1i;->k:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    invoke-virtual {v0}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/J1i;->c(Ljava/lang/String;)V

    .line 2639171
    iget-object v0, p0, LX/J1i;->k:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    iget-object v1, p0, LX/J1i;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->setCurrencyCode(Ljava/lang/String;)V

    .line 2639172
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2639173
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/J1i;->a(Ljava/lang/String;Z)V

    .line 2639174
    return-void
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 2639175
    iget-object v0, p0, LX/J1i;->k:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    invoke-virtual {v0}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2639176
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/J1i;->o:Z

    .line 2639177
    iget-object v0, p0, LX/J1i;->k:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    invoke-virtual {v0, p1}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->setAmount(Ljava/lang/String;)V

    .line 2639178
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/J1i;->o:Z

    .line 2639179
    :cond_0
    if-eqz p2, :cond_1

    .line 2639180
    iget-object v0, p0, LX/J1i;->c:LX/J1k;

    new-instance v1, Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    iget-object p2, p0, LX/J1i;->i:Ljava/lang/String;

    invoke-direct {v1, p1, p2}, Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/J1k;->a(Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;)Z

    move-result v0

    .line 2639181
    if-eqz v0, :cond_2

    .line 2639182
    iget-object v0, p0, LX/J1i;->k:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    invoke-virtual {v0}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->a()V

    .line 2639183
    :cond_1
    :goto_0
    invoke-direct {p0, p1}, LX/J1i;->c(Ljava/lang/String;)V

    .line 2639184
    return-void

    .line 2639185
    :cond_2
    iget-object v0, p0, LX/J1i;->k:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    invoke-virtual {v0}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->b()V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 2639186
    iget-object v0, p0, LX/J1i;->k:Lcom/facebook/payments/p2p/ui/DollarIconEditText;

    invoke-virtual {v0, p1}, Lcom/facebook/payments/p2p/ui/DollarIconEditText;->setEnabled(Z)V

    .line 2639187
    return-void
.end method
