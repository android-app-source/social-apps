.class public LX/JRZ;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JRX;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/multishare/MultiShareEndItemComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2693490
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/JRZ;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/multishare/MultiShareEndItemComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2693491
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2693492
    iput-object p1, p0, LX/JRZ;->b:LX/0Ot;

    .line 2693493
    return-void
.end method

.method public static a(LX/0QB;)LX/JRZ;
    .locals 4

    .prologue
    .line 2693494
    const-class v1, LX/JRZ;

    monitor-enter v1

    .line 2693495
    :try_start_0
    sget-object v0, LX/JRZ;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2693496
    sput-object v2, LX/JRZ;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2693497
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2693498
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2693499
    new-instance v3, LX/JRZ;

    const/16 p0, 0x202e

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JRZ;-><init>(LX/0Ot;)V

    .line 2693500
    move-object v0, v3

    .line 2693501
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2693502
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JRZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2693503
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2693504
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 10

    .prologue
    .line 2693505
    check-cast p2, LX/JRY;

    .line 2693506
    iget-object v0, p0, LX/JRZ;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/multishare/MultiShareEndItemComponentSpec;

    iget-object v1, p2, LX/JRY;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/JRY;->b:LX/1Pm;

    const/16 p2, 0x8

    const/4 p0, 0x4

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 2693507
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2693508
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2693509
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2693510
    invoke-static {v3}, LX/1VO;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    invoke-static {v3}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v3

    .line 2693511
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v7}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v5

    iget v6, v0, Lcom/facebook/feedplugins/multishare/MultiShareEndItemComponentSpec;->d:I

    invoke-interface {v5, v6}, LX/1Dh;->F(I)LX/1Dh;

    move-result-object v5

    iget-object v6, v0, Lcom/facebook/feedplugins/multishare/MultiShareEndItemComponentSpec;->e:LX/1nu;

    invoke-virtual {v6, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v6

    invoke-virtual {v6, v2}, LX/1nw;->a(LX/1Pp;)LX/1nw;

    move-result-object v6

    invoke-virtual {v6, v3}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v3

    sget-object v6, Lcom/facebook/feedplugins/multishare/MultiShareEndItemComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v6}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v3

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-virtual {v3, v6}, LX/1nw;->c(F)LX/1nw;

    move-result-object v3

    sget-object v6, LX/1Up;->f:LX/1Up;

    invoke-virtual {v3, v6}, LX/1nw;->a(LX/1Up;)LX/1nw;

    move-result-object v3

    invoke-interface {v5, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v7}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v8}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v5

    const v6, 0x7f0b1169

    invoke-interface {v5, p2, v6}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v5

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f081033

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    sget v7, Lcom/facebook/feedplugins/multishare/MultiShareEndItemComponentSpec;->c:I

    invoke-static {p1, v6, v7, v8}, Lcom/facebook/feedplugins/multishare/MultiShareEndItemComponentSpec;->a(LX/1De;Ljava/lang/CharSequence;II)LX/1X5;

    move-result-object v6

    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v5

    sget v6, Lcom/facebook/feedplugins/multishare/MultiShareEndItemComponentSpec;->b:I

    invoke-static {p1, v4, v6, v9}, Lcom/facebook/feedplugins/multishare/MultiShareEndItemComponentSpec;->a(LX/1De;Ljava/lang/CharSequence;II)LX/1X5;

    move-result-object v4

    invoke-interface {v5, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x6

    const/4 v5, 0x5

    invoke-interface {v3, v4, v5}, LX/1Dh;->r(II)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, p2, v9}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v3

    const v4, 0x7f020a3c

    invoke-interface {v3, v4}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2693512
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2693513
    invoke-static {}, LX/1dS;->b()V

    .line 2693514
    const/4 v0, 0x0

    return-object v0
.end method
