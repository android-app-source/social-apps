.class public LX/JPQ;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JPR;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JPQ",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JPR;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2689726
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2689727
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JPQ;->b:LX/0Zi;

    .line 2689728
    iput-object p1, p0, LX/JPQ;->a:LX/0Ot;

    .line 2689729
    return-void
.end method

.method public static a(LX/0QB;)LX/JPQ;
    .locals 4

    .prologue
    .line 2689709
    const-class v1, LX/JPQ;

    monitor-enter v1

    .line 2689710
    :try_start_0
    sget-object v0, LX/JPQ;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2689711
    sput-object v2, LX/JPQ;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2689712
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2689713
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2689714
    new-instance v3, LX/JPQ;

    const/16 p0, 0x1fde

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JPQ;-><init>(LX/0Ot;)V

    .line 2689715
    move-object v0, v3

    .line 2689716
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2689717
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JPQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2689718
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2689719
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 5

    .prologue
    .line 2689720
    check-cast p2, LX/JPP;

    .line 2689721
    iget-object v0, p0, LX/JPQ;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JPR;

    iget-object v1, p2, LX/JPP;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/JPP;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    iget-object v3, p2, LX/JPP;->c:LX/1Pn;

    .line 2689722
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const/4 p0, 0x0

    invoke-interface {v4, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    const/4 p0, 0x2

    invoke-interface {v4, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    const p0, 0x7f0b257f

    invoke-interface {v4, p0}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v4

    const p0, 0x7f0b2580

    invoke-interface {v4, p0}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v4

    const p0, 0x7f020a3d

    invoke-interface {v4, p0}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v4

    invoke-static {v0, p1, v2, v1, v3}, LX/JPR;->a(LX/JPR;LX/1De;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1Dh;

    move-result-object p0

    invoke-interface {v4, p0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    iget-object p0, v0, LX/JPR;->b:LX/JOt;

    invoke-virtual {p0, p1}, LX/JOt;->c(LX/1De;)LX/JOs;

    move-result-object p0

    invoke-virtual {p0, v3}, LX/JOs;->a(LX/1Pn;)LX/JOs;

    move-result-object p0

    invoke-virtual {p0, v1}, LX/JOs;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/JOs;

    move-result-object p0

    invoke-virtual {p0, v2}, LX/JOs;->a(Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;)LX/JOs;

    move-result-object p0

    invoke-interface {v4, p0}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    const/4 p0, 0x3

    const p2, 0x7f0b2581

    invoke-interface {v4, p0, p2}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 2689723
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2689724
    invoke-static {}, LX/1dS;->b()V

    .line 2689725
    const/4 v0, 0x0

    return-object v0
.end method
