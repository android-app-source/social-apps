.class public final LX/Iwi;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;)V
    .locals 0

    .prologue
    .line 2630794
    iput-object p1, p0, LX/Iwi;->a:Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 3

    .prologue
    .line 2630795
    iget-object v0, p0, LX/Iwi;->a:Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;

    iget-object v0, v0, Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;->d:LX/Iw6;

    .line 2630796
    iget-object v1, v0, LX/Iw6;->a:LX/0Zb;

    const-string v2, "pages_browser_landing_page_load_error"

    invoke-static {v2}, LX/Iw6;->d(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2630797
    iget-object v0, p0, LX/Iwi;->a:Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;

    iget-object v0, v0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->m:LX/03V;

    const-string v1, "page_identity_data_fetch_fail"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2630798
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 11

    .prologue
    .line 2630799
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    const/4 v10, 0x1

    const/4 v3, 0x0

    .line 2630800
    iget-object v0, p0, LX/Iwi;->a:Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;

    iget-object v0, v0, Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;->d:LX/Iw6;

    .line 2630801
    iget-object v1, v0, LX/Iw6;->a:LX/0Zb;

    const-string v2, "pages_browser_landing_page_load_successful"

    invoke-static {v2}, LX/Iw6;->d(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2630802
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel;

    .line 2630803
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel;->a()Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2630804
    :cond_0
    :goto_0
    return-void

    .line 2630805
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel;->a()Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v4, v3

    :goto_1
    if-ge v4, v6, :cond_6

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;

    .line 2630806
    invoke-static {v0}, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->b(Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 2630807
    invoke-virtual {v0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;->k()Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel$SuggestedPagesModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel$SuggestedPagesModel;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v2, v3

    :goto_2
    if-ge v2, v8, :cond_3

    invoke-virtual {v7, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;

    .line 2630808
    invoke-virtual {v1}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->b()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 2630809
    iget-object v9, p0, LX/Iwi;->a:Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;

    iget-object v9, v9, Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;->c:LX/Iwr;

    invoke-virtual {v1}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, LX/Iwr;->a(Ljava/lang/String;)V

    .line 2630810
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    .line 2630811
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "invites"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2630812
    iget-object v1, p0, LX/Iwi;->a:Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;

    .line 2630813
    iput-object v0, v1, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->f:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;

    .line 2630814
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "top"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2630815
    iget-object v1, p0, LX/Iwi;->a:Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;

    .line 2630816
    iput-object v0, v1, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->e:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;

    .line 2630817
    :cond_5
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    .line 2630818
    :cond_6
    iget-object v0, p0, LX/Iwi;->a:Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;

    iget-object v0, v0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->f:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;

    if-eqz v0, :cond_7

    .line 2630819
    iget-object v0, p0, LX/Iwi;->a:Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;

    iget-object v0, v0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->p:LX/IwC;

    iget-object v1, p0, LX/Iwi;->a:Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;

    iget-object v1, v1, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->f:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;

    invoke-virtual {v0, v1, v10, v10}, LX/IwC;->a(Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;ZZ)V

    .line 2630820
    :cond_7
    iget-object v0, p0, LX/Iwi;->a:Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;

    iget-object v0, v0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->e:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;

    if-eqz v0, :cond_8

    .line 2630821
    iget-object v0, p0, LX/Iwi;->a:Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;

    iget-object v0, v0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->p:LX/IwC;

    iget-object v1, p0, LX/Iwi;->a:Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;

    iget-object v1, v1, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->e:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;

    invoke-virtual {v0, v1, v3, v10}, LX/IwC;->a(Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;ZZ)V

    .line 2630822
    :cond_8
    iget-object v0, p0, LX/Iwi;->a:Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;

    iget-object v0, v0, Lcom/facebook/pages/browser/fragment/PagesBrowserFragment;->p:LX/IwC;

    const v1, -0xa71393

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto/16 :goto_0
.end method
