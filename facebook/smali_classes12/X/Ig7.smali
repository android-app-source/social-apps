.class public LX/Ig7;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0mY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0mY",
            "<",
            "LX/Ifu;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0mY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0mY",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lcom/facebook/user/model/UserKey;

.field public d:Landroid/location/Location;

.field public e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/user/model/UserKey;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2600602
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2600603
    new-instance v0, LX/0mY;

    invoke-direct {v0}, LX/0mY;-><init>()V

    iput-object v0, p0, LX/Ig7;->a:LX/0mY;

    .line 2600604
    new-instance v0, LX/0mY;

    invoke-direct {v0}, LX/0mY;-><init>()V

    iput-object v0, p0, LX/Ig7;->b:LX/0mY;

    .line 2600605
    iput-object v1, p0, LX/Ig7;->d:Landroid/location/Location;

    .line 2600606
    iput-object v1, p0, LX/Ig7;->e:Ljava/lang/String;

    .line 2600607
    iput-object p1, p0, LX/Ig7;->c:Lcom/facebook/user/model/UserKey;

    .line 2600608
    return-void
.end method


# virtual methods
.method public final a(Landroid/location/Location;)V
    .locals 2

    .prologue
    .line 2600618
    iget-object v0, p0, LX/Ig7;->d:Landroid/location/Location;

    invoke-static {p1, v0}, LX/15f;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2600619
    :goto_0
    return-void

    .line 2600620
    :cond_0
    iput-object p1, p0, LX/Ig7;->d:Landroid/location/Location;

    .line 2600621
    iget-object v0, p0, LX/Ig7;->a:LX/0mY;

    invoke-virtual {v0}, LX/0mY;->a()Ljava/util/List;

    move-result-object p1

    .line 2600622
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2600623
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ifu;

    invoke-interface {v0, p0}, LX/Ifu;->a(LX/Ig7;)V

    .line 2600624
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2600625
    :cond_1
    iget-object v0, p0, LX/Ig7;->a:LX/0mY;

    invoke-virtual {v0}, LX/0mY;->b()V

    .line 2600626
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2600609
    iget-object v0, p0, LX/Ig7;->e:Ljava/lang/String;

    invoke-static {p1, v0}, LX/15f;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2600610
    :goto_0
    return-void

    .line 2600611
    :cond_0
    iput-object p1, p0, LX/Ig7;->e:Ljava/lang/String;

    .line 2600612
    iget-object v0, p0, LX/Ig7;->b:LX/0mY;

    invoke-virtual {v0}, LX/0mY;->a()Ljava/util/List;

    move-result-object v1

    .line 2600613
    const/4 v0, 0x0

    :goto_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result p1

    if-ge v0, p1, :cond_1

    .line 2600614
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 2600615
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2600616
    :cond_1
    iget-object v0, p0, LX/Ig7;->a:LX/0mY;

    invoke-virtual {v0}, LX/0mY;->b()V

    .line 2600617
    goto :goto_0
.end method
