.class public LX/IL3;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;",
            ">;"
        }
    .end annotation
.end field

.field private b:LX/INy;

.field public c:Z


# direct methods
.method public constructor <init>(LX/INy;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2567591
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2567592
    iput-object p1, p0, LX/IL3;->b:LX/INy;

    .line 2567593
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2567594
    new-instance v0, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;-><init>(Landroid/content/Context;)V

    .line 2567595
    iget-object v1, p0, LX/IL3;->b:LX/INy;

    .line 2567596
    new-instance p1, LX/INx;

    invoke-static {v1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    const-class p0, Landroid/content/Context;

    invoke-interface {v1, p0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/content/Context;

    const/16 p2, 0xc

    invoke-static {v1, p2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p2

    invoke-direct {p1, v2, p0, p2, v0}, LX/INx;-><init>(Lcom/facebook/content/SecureContextHelper;Landroid/content/Context;LX/0Or;Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;)V

    .line 2567597
    move-object v0, p1

    .line 2567598
    return-object v0
.end method

.method public final a(LX/1a1;I)V
    .locals 7

    .prologue
    .line 2567599
    check-cast p1, LX/INx;

    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 2567600
    iget-object v0, p0, LX/IL3;->a:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;

    .line 2567601
    if-eqz v0, :cond_1

    move v1, v5

    :goto_0
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 2567602
    const/4 v2, 0x0

    .line 2567603
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;->hG_()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_3

    .line 2567604
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;->hG_()LX/1vs;

    move-result-object v1

    iget-object v4, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2567605
    invoke-virtual {v4, v1, v3}, LX/15i;->g(II)I

    move-result v1

    if-eqz v1, :cond_2

    move v1, v5

    :goto_1
    if-eqz v1, :cond_5

    .line 2567606
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;->hG_()LX/1vs;

    move-result-object v1

    iget-object v4, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2567607
    invoke-virtual {v4, v1, v3}, LX/15i;->g(II)I

    move-result v1

    invoke-virtual {v4, v1, v3}, LX/15i;->g(II)I

    move-result v1

    if-eqz v1, :cond_4

    move v1, v5

    :goto_2
    if-eqz v1, :cond_0

    .line 2567608
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;->hG_()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-virtual {v2, v1, v3}, LX/15i;->g(II)I

    move-result v1

    invoke-virtual {v2, v1, v3}, LX/15i;->g(II)I

    move-result v1

    invoke-virtual {v2, v1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    .line 2567609
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;->b()Ljava/lang/String;

    move-result-object v4

    iget-boolean v6, p0, LX/IL3;->c:Z

    if-eqz v6, :cond_6

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;->e()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v0, v6}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->equals(Ljava/lang/Object;)Z

    move-result v6

    :goto_3
    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, LX/INx;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IZ)V

    .line 2567610
    return-void

    :cond_1
    move v1, v3

    .line 2567611
    goto :goto_0

    :cond_2
    move v1, v3

    .line 2567612
    goto :goto_1

    :cond_3
    move v1, v3

    goto :goto_1

    :cond_4
    move v1, v3

    goto :goto_2

    :cond_5
    move v1, v3

    goto :goto_2

    :cond_6
    move v6, v3

    .line 2567613
    goto :goto_3
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2567614
    iget-object v0, p0, LX/IL3;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
