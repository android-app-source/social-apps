.class public LX/Htm;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/Htl;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2516738
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 2516739
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View$OnClickListener;Lcom/facebook/composer/inlinesprouts/InlineSproutsView;Landroid/widget/TextView;LX/0zw;LX/0zw;Ljava/lang/Object;LX/0Px;)LX/Htl;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData:",
            "Ljava/lang/Object;",
            ":",
            "LX/0io;",
            ":",
            "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesIsKeyboardUp;",
            ":",
            "LX/0j0;",
            ":",
            "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesMaxBottomOverlayHeight;",
            ":",
            "LX/0j8;",
            ":",
            "LX/0jD;",
            ":",
            "Lcom/facebook/composer/inlinesprouts/model/InlineSproutsStateSpec$ProvidesInlineSproutsState;",
            ":",
            "LX/0ip;",
            "DerivedData:",
            "Ljava/lang/Object;",
            "Mutation:",
            "Ljava/lang/Object;",
            ":",
            "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
            ":",
            "Lcom/facebook/composer/inlinesprouts/model/InlineSproutsStateSpec$SetsInlineSproutsState",
            "<TMutation;>;Services:",
            "Ljava/lang/Object;",
            ":",
            "LX/0il",
            "<TModelData;>;:",
            "LX/0ik",
            "<TDerivedData;>;:",
            "LX/0im",
            "<TMutation;>;>(",
            "Landroid/view/View$OnClickListener;",
            "Lcom/facebook/composer/inlinesprouts/InlineSproutsView;",
            "Landroid/widget/TextView;",
            "LX/0zw",
            "<",
            "Landroid/view/View;",
            ">;",
            "LX/0zw",
            "<",
            "Landroid/view/View;",
            ">;TServices;",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/inlinesproutsinterfaces/InlineSproutItem;",
            ">;)",
            "LX/Htl",
            "<TModelData;TDerivedData;TMutation;TServices;>;"
        }
    .end annotation

    .prologue
    .line 2516740
    new-instance v0, LX/Htl;

    move-object/from16 v6, p6

    check-cast v6, LX/0il;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Context;

    const-class v1, LX/Htt;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/Htt;

    invoke-static {p0}, LX/Hth;->a(LX/0QB;)LX/Hth;

    move-result-object v10

    check-cast v10, LX/Hth;

    invoke-static {p0}, LX/Hte;->b(LX/0QB;)LX/Hte;

    move-result-object v11

    check-cast v11, LX/Hte;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v11}, LX/Htl;-><init>(Landroid/view/View$OnClickListener;Lcom/facebook/composer/inlinesprouts/InlineSproutsView;Landroid/widget/TextView;LX/0zw;LX/0zw;LX/0il;LX/0Px;Landroid/content/Context;LX/Htt;LX/Hth;LX/Hte;)V

    .line 2516741
    return-object v0
.end method
