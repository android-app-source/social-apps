.class public final LX/J26;
.super LX/2h1;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2h1",
        "<",
        "Lcom/facebook/payments/invoice/protocol/graphql/InvoiceMutationsModels$PaymentInvoiceMutationModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/J29;


# direct methods
.method public constructor <init>(LX/J29;)V
    .locals 0

    .prologue
    .line 2639781
    iput-object p1, p0, LX/J26;->a:LX/J29;

    invoke-direct {p0}, LX/2h1;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 3

    .prologue
    .line 2639773
    iget-object v0, p0, LX/J26;->a:LX/J29;

    .line 2639774
    const-class v1, LX/2Oo;

    invoke-static {p1, v1}, LX/23D;->a(Ljava/lang/Throwable;Ljava/lang/Class;)Ljava/lang/Throwable;

    move-result-object v1

    check-cast v1, LX/2Oo;

    .line 2639775
    if-nez v1, :cond_0

    iget-object v1, v0, LX/J29;->a:Landroid/content/Context;

    const v2, 0x7f08003e

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2639776
    :goto_0
    new-instance v2, LX/31Y;

    iget-object p0, v0, LX/J29;->a:Landroid/content/Context;

    invoke-direct {v2, p0}, LX/31Y;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v1

    const v2, 0x7f080016

    new-instance p0, LX/J27;

    invoke-direct {p0, v0}, LX/J27;-><init>(LX/J29;)V

    invoke-virtual {v1, v2, p0}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    invoke-virtual {v1}, LX/0ju;->b()LX/2EJ;

    .line 2639777
    return-void

    .line 2639778
    :cond_0
    invoke-virtual {v1}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/http/protocol/ApiErrorResult;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/http/protocol/ApiErrorResult;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2639779
    iget-object v0, p0, LX/J26;->a:LX/J29;

    iget-object v0, v0, LX/J29;->e:LX/6qh;

    new-instance v1, LX/73T;

    sget-object v2, LX/73S;->RESET:LX/73S;

    invoke-direct {v1, v2}, LX/73T;-><init>(LX/73S;)V

    invoke-virtual {v0, v1}, LX/6qh;->a(LX/73T;)V

    .line 2639780
    return-void
.end method
