.class public LX/Hm4;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;


# instance fields
.field public final b:Ljava/io/DataOutputStream;

.field public final c:Ljava/io/DataInputStream;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2500073
    const-class v0, LX/Hm4;

    sput-object v0, LX/Hm4;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Ljava/net/Socket;)V
    .locals 2

    .prologue
    .line 2500069
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2500070
    new-instance v0, Ljava/io/DataOutputStream;

    invoke-virtual {p1}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v0, p0, LX/Hm4;->b:Ljava/io/DataOutputStream;

    .line 2500071
    new-instance v0, Ljava/io/DataInputStream;

    invoke-virtual {p1}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, LX/Hm4;->c:Ljava/io/DataInputStream;

    .line 2500072
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2500066
    iget-object v0, p0, LX/Hm4;->c:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->close()V

    .line 2500067
    iget-object v0, p0, LX/Hm4;->b:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V

    .line 2500068
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 2500063
    iget-object v0, p0, LX/Hm4;->b:Ljava/io/DataOutputStream;

    invoke-virtual {v0, p1}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 2500064
    iget-object v0, p0, LX/Hm4;->b:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->flush()V

    .line 2500065
    return-void
.end method

.method public final a(Lcom/facebook/beam/protocol/BeamPreflightInfo;)V
    .locals 2

    .prologue
    .line 2500058
    iget-object v0, p0, LX/Hm4;->b:Ljava/io/DataOutputStream;

    sget-object v1, LX/Hm3;->PREFLIGHT_INFO:LX/Hm3;

    invoke-virtual {v1}, LX/Hm3;->getInt()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 2500059
    invoke-virtual {p1}, LX/Hm1;->a()Ljava/lang/String;

    move-result-object v0

    .line 2500060
    iget-object v1, p0, LX/Hm4;->b:Ljava/io/DataOutputStream;

    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 2500061
    iget-object v0, p0, LX/Hm4;->b:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->flush()V

    .line 2500062
    return-void
.end method

.method public final a(Ljava/io/File;)V
    .locals 5

    .prologue
    .line 2500074
    invoke-virtual {p1}, Ljava/io/File;->canRead()Z

    move-result v0

    const-string v1, "Can\'t read apk file for writing: %s."

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Tp;->b(ZLjava/lang/String;Ljava/lang/Object;)V

    .line 2500075
    iget-object v0, p0, LX/Hm4;->b:Ljava/io/DataOutputStream;

    sget-object v1, LX/Hm3;->APK:LX/Hm3;

    invoke-virtual {v1}, LX/Hm3;->getInt()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 2500076
    iget-object v0, p0, LX/Hm4;->b:Ljava/io/DataOutputStream;

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 2500077
    iget-object v0, p0, LX/Hm4;->b:Ljava/io/DataOutputStream;

    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 2500078
    const/16 v0, 0x2000

    new-array v0, v0, [B

    .line 2500079
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 2500080
    :goto_0
    :try_start_0
    invoke-virtual {v1, v0}, Ljava/io/FileInputStream;->read([B)I

    move-result v2

    if-lez v2, :cond_0

    .line 2500081
    iget-object v3, p0, LX/Hm4;->b:Ljava/io/DataOutputStream;

    const/4 v4, 0x0

    invoke-virtual {v3, v0, v4, v2}, Ljava/io/DataOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2500082
    :catch_0
    move-exception v0

    .line 2500083
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    .line 2500084
    throw v0

    .line 2500085
    :cond_0
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    .line 2500086
    iget-object v0, p0, LX/Hm4;->b:Ljava/io/DataOutputStream;

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->flush()V

    .line 2500087
    return-void
.end method

.method public final a(Ljava/io/FileOutputStream;)V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 2500046
    iget-object v0, p0, LX/Hm4;->c:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v2

    .line 2500047
    const-wide/32 v0, 0x9600000

    cmp-long v0, v2, v0

    if-lez v0, :cond_0

    .line 2500048
    new-instance v0, Ljava/io/IOException;

    const-string v1, "The file is too large!"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2500049
    :cond_0
    const-wide/16 v0, 0x0

    .line 2500050
    const/16 v4, 0x2000

    new-array v4, v4, [B

    .line 2500051
    :goto_0
    cmp-long v5, v0, v2

    if-gez v5, :cond_1

    .line 2500052
    sub-long v6, v2, v0

    const-wide/16 v8, 0x2000

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v6

    .line 2500053
    iget-object v5, p0, LX/Hm4;->c:Ljava/io/DataInputStream;

    long-to-int v6, v6

    invoke-virtual {v5, v4, v10, v6}, Ljava/io/DataInputStream;->read([BII)I

    move-result v5

    int-to-long v6, v5

    .line 2500054
    long-to-int v5, v6

    invoke-virtual {p1, v4, v10, v5}, Ljava/io/FileOutputStream;->write([BII)V

    .line 2500055
    add-long/2addr v0, v6

    .line 2500056
    goto :goto_0

    .line 2500057
    :cond_1
    return-void
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 2500040
    :try_start_0
    invoke-virtual {p0}, LX/Hm4;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2500041
    const/4 v0, 0x1

    .line 2500042
    :goto_0
    return v0

    .line 2500043
    :catch_0
    move-exception v0

    .line 2500044
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "Failed to close sockets"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2500045
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 2500039
    iget-object v0, p0, LX/Hm4;->c:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    return v0
.end method

.method public final d()LX/Hm3;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 2500038
    iget-object v0, p0, LX/Hm4;->c:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    invoke-static {v0}, LX/Hm3;->fromInt(I)LX/Hm3;

    move-result-object v0

    return-object v0
.end method

.method public final e()Lcom/facebook/beam/protocol/BeamPreflightInfo;
    .locals 2

    .prologue
    .line 2500032
    iget-object v0, p0, LX/Hm4;->c:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v0

    .line 2500033
    const-class v1, Lcom/facebook/beam/protocol/BeamPreflightInfo;

    .line 2500034
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object p0

    invoke-virtual {p0, v0, v1}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Hm1;

    move-object v1, p0

    .line 2500035
    check-cast v1, Lcom/facebook/beam/protocol/BeamPreflightInfo;

    move-object v0, v1

    .line 2500036
    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2500037
    iget-object v0, p0, LX/Hm4;->c:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
