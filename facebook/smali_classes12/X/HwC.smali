.class public final LX/HwC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final synthetic a:Lcom/facebook/composer/ui/statusview/ComposerStatusView;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/ui/statusview/ComposerStatusView;)V
    .locals 0

    .prologue
    .line 2520174
    iput-object p1, p0, LX/HwC;->a:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 2520175
    iget-object v0, p0, LX/HwC;->a:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    iget-object v0, v0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->c:Lcom/facebook/composer/ui/text/ComposerEditText;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2520176
    iget-object v0, p0, LX/HwC;->a:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    iget-object v0, v0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->c:Lcom/facebook/composer/ui/text/ComposerEditText;

    invoke-virtual {v0}, Lcom/facebook/composer/ui/text/ComposerEditText;->getWidth()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iget-object v2, p0, LX/HwC;->a:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    iget-object v2, v2, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->c:Lcom/facebook/composer/ui/text/ComposerEditText;

    invoke-virtual {v2}, Lcom/facebook/composer/ui/text/ComposerEditText;->getHeight()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/view/MotionEvent;->setLocation(FF)V

    .line 2520177
    iget-object v0, p0, LX/HwC;->a:Lcom/facebook/composer/ui/statusview/ComposerStatusView;

    iget-object v0, v0, Lcom/facebook/composer/ui/statusview/ComposerStatusView;->c:Lcom/facebook/composer/ui/text/ComposerEditText;

    invoke-virtual {v0, p2}, Lcom/facebook/composer/ui/text/ComposerEditText;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 2520178
    const/4 v0, 0x0

    return v0
.end method
