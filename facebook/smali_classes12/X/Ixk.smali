.class public LX/Ixk;
.super LX/BcS;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Void:",
        "Ljava/lang/Object;",
        ">",
        "LX/BcS;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ixl;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Ixk",
            "<TVoid;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Ixl;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2632225
    invoke-direct {p0}, LX/BcS;-><init>()V

    .line 2632226
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/Ixk;->b:LX/0Zi;

    .line 2632227
    iput-object p1, p0, LX/Ixk;->a:LX/0Ot;

    .line 2632228
    return-void
.end method

.method public static a(LX/0QB;)LX/Ixk;
    .locals 4

    .prologue
    .line 2632229
    const-class v1, LX/Ixk;

    monitor-enter v1

    .line 2632230
    :try_start_0
    sget-object v0, LX/Ixk;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2632231
    sput-object v2, LX/Ixk;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2632232
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2632233
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2632234
    new-instance v3, LX/Ixk;

    const/16 p0, 0x2c6f

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Ixk;-><init>(LX/0Ot;)V

    .line 2632235
    move-object v0, v3

    .line 2632236
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2632237
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Ixk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2632238
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2632239
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/BcQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2632240
    invoke-static {}, LX/1dS;->b()V

    .line 2632241
    iget v0, p1, LX/BcQ;->b:I

    .line 2632242
    packed-switch v0, :pswitch_data_0

    .line 2632243
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2632244
    :pswitch_0
    check-cast p2, LX/BdG;

    .line 2632245
    iget v1, p2, LX/BdG;->a:I

    iget-object v2, p2, LX/BdG;->b:Ljava/lang/Object;

    iget-object v0, p1, LX/BcQ;->c:[Ljava/lang/Object;

    const/4 v3, 0x0

    aget-object v0, v0, v3

    check-cast v0, LX/BcP;

    iget-object v3, p1, LX/BcQ;->a:LX/BcO;

    .line 2632246
    check-cast v3, LX/Ixj;

    .line 2632247
    iget-object v4, p0, LX/Ixk;->a:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Ixl;

    iget v5, v3, LX/Ixj;->b:I

    .line 2632248
    rem-int p2, v1, v5

    .line 2632249
    iget-object p1, v4, LX/Ixl;->a:LX/IxS;

    const/4 p0, 0x0

    .line 2632250
    new-instance v3, LX/IxR;

    invoke-direct {v3, p1}, LX/IxR;-><init>(LX/IxS;)V

    .line 2632251
    sget-object v4, LX/IxS;->a:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/IxQ;

    .line 2632252
    if-nez v4, :cond_0

    .line 2632253
    new-instance v4, LX/IxQ;

    invoke-direct {v4}, LX/IxQ;-><init>()V

    .line 2632254
    :cond_0
    invoke-static {v4, v0, p0, p0, v3}, LX/IxQ;->a$redex0(LX/IxQ;LX/1De;IILX/IxR;)V

    .line 2632255
    move-object v3, v4

    .line 2632256
    move-object p0, v3

    .line 2632257
    move-object p0, p0

    .line 2632258
    if-ge v1, v5, :cond_1

    const/4 p1, 0x1

    .line 2632259
    :goto_1
    iget-object v3, p0, LX/IxQ;->a:LX/IxR;

    iput-boolean p1, v3, LX/IxR;->a:Z

    .line 2632260
    iget-object v3, p0, LX/IxQ;->d:Ljava/util/BitSet;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 2632261
    move-object p1, p0

    .line 2632262
    iget-object p0, p1, LX/IxQ;->a:LX/IxR;

    iput p2, p0, LX/IxR;->b:I

    .line 2632263
    iget-object p0, p1, LX/IxQ;->d:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {p0, v3}, Ljava/util/BitSet;->set(I)V

    .line 2632264
    move-object p1, p1

    .line 2632265
    iget-object p2, p1, LX/IxQ;->a:LX/IxR;

    iput v5, p2, LX/IxR;->c:I

    .line 2632266
    iget-object p2, p1, LX/IxQ;->d:Ljava/util/BitSet;

    const/4 p0, 0x2

    invoke-virtual {p2, p0}, Ljava/util/BitSet;->set(I)V

    .line 2632267
    move-object p1, p1

    .line 2632268
    check-cast v2, Lcom/facebook/pages/launchpoint/components/DiscoverPageItemGraphQLModels$DiscoverPageItemGraphQLModel;

    .line 2632269
    iget-object p2, p1, LX/IxQ;->a:LX/IxR;

    iput-object v2, p2, LX/IxR;->d:Lcom/facebook/pages/launchpoint/components/DiscoverPageItemGraphQLModels$DiscoverPageItemGraphQLModel;

    .line 2632270
    iget-object p2, p1, LX/IxQ;->d:Ljava/util/BitSet;

    const/4 p0, 0x3

    invoke-virtual {p2, p0}, Ljava/util/BitSet;->set(I)V

    .line 2632271
    move-object p1, p1

    .line 2632272
    invoke-virtual {p1}, LX/1X5;->d()LX/1X1;

    move-result-object p1

    move-object v4, p1

    .line 2632273
    move-object v0, v4

    .line 2632274
    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1cf7a18e
        :pswitch_0
    .end packed-switch
.end method

.method public final a(LX/BcP;Ljava/util/List;LX/BcO;)V
    .locals 2

    .prologue
    .line 2632275
    check-cast p3, LX/Ixj;

    .line 2632276
    iget-object v0, p0, LX/Ixk;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p3, LX/Ixj;->c:LX/2kW;

    .line 2632277
    invoke-static {p1}, LX/Bd3;->b(LX/BcP;)LX/Bd0;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/Bd0;->a(LX/2kW;)LX/Bd0;

    move-result-object v1

    const/16 p0, 0x9

    .line 2632278
    iget-object v0, v1, LX/Bd0;->a:LX/Bd1;

    iput p0, v0, LX/Bd1;->c:I

    .line 2632279
    move-object v1, v1

    .line 2632280
    const/16 p0, 0xa

    invoke-virtual {v1, p0}, LX/Bd0;->b(I)LX/Bd0;

    move-result-object v1

    .line 2632281
    const p0, 0x1cf7a18e

    const/4 p3, 0x1

    new-array p3, p3, [Ljava/lang/Object;

    const/4 v0, 0x0

    aput-object p1, p3, v0

    invoke-static {p1, p0, p3}, LX/BcS;->a(LX/BcP;I[Ljava/lang/Object;)LX/BcQ;

    move-result-object p0

    move-object p0, p0

    .line 2632282
    invoke-virtual {v1, p0}, LX/Bd0;->b(LX/BcQ;)LX/Bd0;

    move-result-object v1

    invoke-static {p1}, LX/BcS;->a(LX/BcP;)LX/BcQ;

    move-result-object p0

    invoke-virtual {v1, p0}, LX/Bd0;->c(LX/BcQ;)LX/Bd0;

    move-result-object v1

    invoke-virtual {v1}, LX/Bd0;->b()LX/BcO;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2632283
    return-void
.end method
