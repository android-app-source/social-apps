.class public final LX/IQx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$GroupPostTopicsNameIdModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;)V
    .locals 0

    .prologue
    .line 2575904
    iput-object p1, p0, LX/IQx;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2575905
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2575906
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 2575907
    if-eqz p1, :cond_0

    .line 2575908
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2575909
    if-nez v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    if-eqz v0, :cond_3

    move v0, v2

    :goto_1
    if-eqz v0, :cond_6

    move v0, v2

    :goto_2
    if-eqz v0, :cond_8

    .line 2575910
    :goto_3
    return-void

    .line 2575911
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2575912
    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$GroupPostTopicsNameIdModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$GroupPostTopicsNameIdModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2575913
    if-nez v0, :cond_2

    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 2575914
    :cond_3
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2575915
    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$GroupPostTopicsNameIdModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$GroupPostTopicsNameIdModel;->a()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v4, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$GroupPostTopicsNameIdModel$GroupPostTopicsModel$NodesModel;

    invoke-virtual {v3, v0, v1, v4}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    .line 2575916
    if-eqz v0, :cond_4

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_4
    if-nez v0, :cond_5

    move v0, v2

    goto :goto_1

    .line 2575917
    :cond_4
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2575918
    goto :goto_4

    :cond_5
    move v0, v1

    goto :goto_1

    .line 2575919
    :cond_6
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2575920
    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$GroupPostTopicsNameIdModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$GroupPostTopicsNameIdModel;->a()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2575921
    invoke-virtual {v3, v0, v2}, LX/15i;->g(II)I

    move-result v0

    if-nez v0, :cond_7

    move v0, v2

    goto :goto_2

    :cond_7
    move v0, v1

    goto :goto_2

    .line 2575922
    :cond_8
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2575923
    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$GroupPostTopicsNameIdModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$GroupPostTopicsNameIdModel;->a()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v3, v0, v2}, LX/15i;->g(II)I

    move-result v0

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2575924
    iget-object v4, p0, LX/IQx;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;

    invoke-virtual {v3, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    .line 2575925
    iput-object v5, v4, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->h:Ljava/lang/String;

    .line 2575926
    iget-object v4, p0, LX/IQx;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;

    invoke-virtual {v3, v0, v2}, LX/15i;->h(II)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, LX/IQx;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->h:Ljava/lang/String;

    if-nez v0, :cond_a

    :cond_9
    move v0, v2

    .line 2575927
    :goto_5
    iput-boolean v0, v4, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->j:Z

    .line 2575928
    iget-object v0, p0, LX/IQx;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->k:LX/0Px;

    if-nez v0, :cond_c

    .line 2575929
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2575930
    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$GroupPostTopicsNameIdModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$GroupPostTopicsNameIdModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v3, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$GroupPostTopicsNameIdModel$GroupPostTopicsModel$NodesModel;

    invoke-virtual {v2, v0, v1, v3}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    iget-object v2, p0, LX/IQx;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;

    if-eqz v0, :cond_b

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 2575931
    :goto_6
    iput-object v0, v2, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->k:LX/0Px;

    .line 2575932
    :goto_7
    iget-object v0, p0, LX/IQx;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->g:LX/IQQ;

    iget-object v2, p0, LX/IQx;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;

    iget-object v2, v2, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->k:LX/0Px;

    iget-object v3, p0, LX/IQx;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;

    iget-object v3, v3, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->o:Ljava/util/Set;

    invoke-virtual {v0, v2, v3}, LX/IQQ;->a(LX/0Px;Ljava/util/Set;)V

    .line 2575933
    iget-object v0, p0, LX/IQx;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;

    .line 2575934
    iput-boolean v1, v0, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->i:Z

    .line 2575935
    goto/16 :goto_3

    .line 2575936
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_a
    move v0, v1

    .line 2575937
    goto :goto_5

    .line 2575938
    :cond_b
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2575939
    goto :goto_6

    .line 2575940
    :cond_c
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2575941
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2575942
    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$GroupPostTopicsNameIdModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$GroupPostTopicsNameIdModel;->a()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v4, Lcom/facebook/groups/feed/protocol/FetchGroupDiscussionTopicsNameModels$GroupPostTopicsNameIdModel$GroupPostTopicsModel$NodesModel;

    invoke-virtual {v3, v0, v1, v4}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    .line 2575943
    iget-object v3, p0, LX/IQx;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;

    iget-object v4, p0, LX/IQx;->a:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;

    iget-object v4, v4, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->k:LX/0Px;

    invoke-virtual {v2, v4}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v2

    if-eqz v0, :cond_d

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_8
    invoke-virtual {v2, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2575944
    iput-object v0, v3, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsAssignFragment;->k:LX/0Px;

    .line 2575945
    goto :goto_7

    .line 2575946
    :cond_d
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2575947
    goto :goto_8
.end method
