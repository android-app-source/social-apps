.class public LX/JSH;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/MountSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final a:LX/0SG;

.field private final b:Ljava/util/concurrent/ExecutorService;

.field private final c:LX/24C;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/24C",
            "<",
            "LX/JSO;",
            "Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/JSG;

.field private final e:LX/JS9;

.field private final f:LX/1Ad;


# direct methods
.method public constructor <init>(LX/24C;LX/0SG;Ljava/util/concurrent/ExecutorService;LX/JSG;LX/JSA;LX/1Ad;)V
    .locals 1
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2695011
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2695012
    iput-object p1, p0, LX/JSH;->c:LX/24C;

    .line 2695013
    iput-object p2, p0, LX/JSH;->a:LX/0SG;

    .line 2695014
    iput-object p3, p0, LX/JSH;->b:Ljava/util/concurrent/ExecutorService;

    .line 2695015
    iput-object p4, p0, LX/JSH;->d:LX/JSG;

    .line 2695016
    iput-object p6, p0, LX/JSH;->f:LX/1Ad;

    .line 2695017
    const/16 v0, 0x14

    invoke-static {v0}, LX/JSA;->a(I)LX/JS9;

    move-result-object v0

    iput-object v0, p0, LX/JSH;->e:LX/JS9;

    .line 2695018
    return-void
.end method

.method public static a(LX/0QB;)LX/JSH;
    .locals 10

    .prologue
    .line 2695019
    const-class v1, LX/JSH;

    monitor-enter v1

    .line 2695020
    :try_start_0
    sget-object v0, LX/JSH;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2695021
    sput-object v2, LX/JSH;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2695022
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2695023
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2695024
    new-instance v3, LX/JSH;

    .line 2695025
    new-instance v6, LX/24C;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-direct {v6, v4, v5}, LX/24C;-><init>(LX/0SG;LX/03V;)V

    .line 2695026
    move-object v4, v6

    .line 2695027
    check-cast v4, LX/24C;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/JSG;->a(LX/0QB;)LX/JSG;

    move-result-object v7

    check-cast v7, LX/JSG;

    const-class v8, LX/JSA;

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/JSA;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v9

    check-cast v9, LX/1Ad;

    invoke-direct/range {v3 .. v9}, LX/JSH;-><init>(LX/24C;LX/0SG;Ljava/util/concurrent/ExecutorService;LX/JSG;LX/JSA;LX/1Ad;)V

    .line 2695028
    move-object v0, v3

    .line 2695029
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2695030
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JSH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2695031
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2695032
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/JSe;Lcom/facebook/feed/rows/core/props/FeedProps;LX/JTY;LX/1Pq;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;)V
    .locals 13
    .param p2    # LX/JSe;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # LX/JTY;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # LX/1Pq;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/JSe;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/JTY;",
            "TE;",
            "LX/1np",
            "<",
            "Lcom/facebook/feedplugins/musicstory/MusicPlayer$Callback;",
            ">;",
            "LX/1np",
            "<",
            "LX/82M",
            "<",
            "LX/JSO;",
            "Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;",
            ">;>;",
            "LX/1np",
            "<",
            "LX/82L",
            "<",
            "LX/JSO;",
            ">;>;",
            "LX/1np",
            "<",
            "Landroid/view/View$OnClickListener;",
            ">;",
            "LX/1np",
            "<",
            "LX/JSO;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2695033
    move-object/from16 v1, p5

    check-cast v1, LX/1Pr;

    move-object/from16 v0, p3

    invoke-static {v1, p2, v0}, LX/JSG;->a(LX/1Pr;LX/JSe;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/JSO;

    move-result-object v1

    move-object/from16 v0, p10

    invoke-virtual {v0, v1}, LX/1np;->a(Ljava/lang/Object;)V

    .line 2695034
    new-instance v1, LX/JSI;

    const/4 v2, 0x0

    new-instance v4, LX/JSN;

    invoke-direct {v4, p2}, LX/JSN;-><init>(LX/JSe;)V

    move-object/from16 v5, p5

    check-cast v5, LX/1Pr;

    const/4 v7, 0x0

    const/4 v8, 0x0

    iget-object v10, p0, LX/JSH;->a:LX/0SG;

    iget-object v11, p0, LX/JSH;->b:Ljava/util/concurrent/ExecutorService;

    move-object v3, p1

    move-object/from16 v6, p3

    move-object v9, p2

    move-object/from16 v12, p4

    invoke-direct/range {v1 .. v12}, LX/JSI;-><init>(LX/JSl;Landroid/content/Context;LX/1KL;LX/1Pr;Lcom/facebook/feed/rows/core/props/FeedProps;ILcom/facebook/feedplugins/musicstory/PlaybackVisibilityRunnable;LX/JSe;LX/0SG;Ljava/util/concurrent/ExecutorService;LX/JTY;)V

    move-object/from16 v0, p6

    invoke-virtual {v0, v1}, LX/1np;->a(Ljava/lang/Object;)V

    .line 2695035
    new-instance v1, LX/82M;

    invoke-virtual/range {p10 .. p10}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-class v4, LX/JSq;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, LX/JSe;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static/range {p3 .. p3}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    new-instance v5, LX/JSq;

    new-instance v6, LX/JSp;

    invoke-direct {v6}, LX/JSp;-><init>()V

    invoke-direct {v5, v6}, LX/JSq;-><init>(LX/JSp;)V

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, LX/82M;-><init>(Ljava/lang/Object;Ljava/lang/String;LX/0jW;LX/24J;Ljava/lang/Object;)V

    .line 2695036
    move-object/from16 v0, p7

    invoke-virtual {v0, v1}, LX/1np;->a(Ljava/lang/Object;)V

    .line 2695037
    iget-object v2, p0, LX/JSH;->c:LX/24C;

    iget-object v3, v1, LX/82M;->b:LX/0jW;

    check-cast p5, LX/1Pr;

    iget-object v1, v1, LX/82M;->a:Ljava/lang/String;

    move-object/from16 v0, p5

    invoke-virtual {v2, v3, v0, v1}, LX/24C;->a(LX/0jW;LX/1Pr;Ljava/lang/String;)LX/82L;

    move-result-object v1

    move-object/from16 v0, p8

    invoke-virtual {v0, v1}, LX/1np;->a(Ljava/lang/Object;)V

    .line 2695038
    iget-object v2, p0, LX/JSH;->d:LX/JSG;

    invoke-virtual/range {p10 .. p10}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/JSO;

    move-object/from16 v0, p4

    invoke-virtual {v2, p2, v0, v1}, LX/JSG;->a(LX/JSe;LX/JTY;LX/JSO;)Landroid/view/View$OnClickListener;

    move-result-object v1

    move-object/from16 v0, p9

    invoke-virtual {v0, v1}, LX/1np;->a(Ljava/lang/Object;)V

    .line 2695039
    return-void
.end method

.method public final a(Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;LX/JSe;LX/1Pq;LX/JTY;Lcom/facebook/feed/rows/core/props/FeedProps;LX/JSI;LX/82L;)V
    .locals 10
    .param p2    # LX/JSe;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/1Pq;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # LX/JTY;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;",
            "LX/JSe;",
            "TE;",
            "LX/JTY;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Lcom/facebook/feedplugins/musicstory/MusicPlayer$Callback;",
            "LX/82L",
            "<",
            "LX/JSO;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2695040
    iget-object v1, p0, LX/JSH;->d:LX/JSG;

    move-object v2, p3

    check-cast v2, LX/1Pr;

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v3, p2

    move-object/from16 v4, p6

    move-object v5, p4

    move-object v6, p5

    move-object v9, p1

    invoke-virtual/range {v1 .. v9}, LX/JSG;->a(LX/1Pr;LX/JSe;LX/JSI;LX/JTY;Lcom/facebook/feed/rows/core/props/FeedProps;LX/JSl;ILcom/facebook/feedplugins/musicstory/MusicPlaybackView;)V

    .line 2695041
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2695042
    iget-object v1, p0, LX/JSH;->c:LX/24C;

    move-object/from16 v0, p7

    invoke-virtual {v1, v0}, LX/24C;->a(LX/82L;)V

    .line 2695043
    return-void
.end method

.method public final a(Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;LX/JSe;Lcom/facebook/common/callercontext/CallerContext;LX/JSI;LX/82M;LX/82L;Landroid/view/View$OnClickListener;LX/JSO;)V
    .locals 6
    .param p2    # LX/JSe;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation build Lcom/facebook/components/annotations/OnBind;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;",
            "LX/JSe;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "Lcom/facebook/feedplugins/musicstory/MusicPlayer$Callback;",
            "LX/82M",
            "<",
            "LX/JSO;",
            "Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;",
            ">;",
            "LX/82L",
            "<",
            "LX/JSO;",
            ">;",
            "Landroid/view/View$OnClickListener;",
            "LX/JSO;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2695044
    iget-object v0, p0, LX/JSH;->d:LX/JSG;

    invoke-virtual {v0, p2, p4}, LX/JSG;->a(LX/JSe;LX/JSI;)V

    .line 2695045
    iget-object v0, p2, LX/JSe;->f:Landroid/net/Uri;

    move-object v0, v0

    .line 2695046
    invoke-virtual {p1, v0, p3}, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2695047
    invoke-virtual {p1, p7}, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2695048
    iget-object v0, p2, LX/JSe;->f:Landroid/net/Uri;

    move-object v0, v0

    .line 2695049
    iget-object v1, p0, LX/JSH;->f:LX/1Ad;

    iget-object v2, p0, LX/JSH;->e:LX/JS9;

    invoke-virtual {p1, v0, p3, v1, v2}, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;LX/1Ad;LX/33B;)V

    .line 2695050
    iget-object v0, p0, LX/JSH;->c:LX/24C;

    const/4 v1, 0x0

    iget-object v3, p5, LX/82M;->e:LX/24J;

    move-object v2, p8

    move-object v4, p6

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, LX/24C;->a(Ljava/lang/Object;Ljava/lang/Object;LX/24J;LX/82L;Landroid/view/View;)V

    .line 2695051
    return-void
.end method
