.class public LX/IAd;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static j:LX/0Xm;


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:LX/IAe;

.field public final d:LX/11H;

.field public final e:Ljava/util/concurrent/ExecutorService;

.field public final f:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/6eF;

.field public final h:Lcom/facebook/content/SecureContextHelper;

.field public i:Landroid/support/v4/app/DialogFragment;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2545957
    const-class v0, LX/IAd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/IAd;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/IAe;LX/11H;Ljava/util/concurrent/ExecutorService;LX/1Ck;LX/6eF;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .param p4    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForegroundExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2545948
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2545949
    iput-object p1, p0, LX/IAd;->b:Landroid/content/Context;

    .line 2545950
    iput-object p2, p0, LX/IAd;->c:LX/IAe;

    .line 2545951
    iput-object p3, p0, LX/IAd;->d:LX/11H;

    .line 2545952
    iput-object p4, p0, LX/IAd;->e:Ljava/util/concurrent/ExecutorService;

    .line 2545953
    iput-object p5, p0, LX/IAd;->f:LX/1Ck;

    .line 2545954
    iput-object p6, p0, LX/IAd;->g:LX/6eF;

    .line 2545955
    iput-object p7, p0, LX/IAd;->h:Lcom/facebook/content/SecureContextHelper;

    .line 2545956
    return-void
.end method

.method public static a(LX/0QB;)LX/IAd;
    .locals 11

    .prologue
    .line 2545937
    const-class v1, LX/IAd;

    monitor-enter v1

    .line 2545938
    :try_start_0
    sget-object v0, LX/IAd;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2545939
    sput-object v2, LX/IAd;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2545940
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2545941
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2545942
    new-instance v3, LX/IAd;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/IAe;->a(LX/0QB;)LX/IAe;

    move-result-object v5

    check-cast v5, LX/IAe;

    invoke-static {v0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v6

    check-cast v6, LX/11H;

    invoke-static {v0}, LX/0tb;->a(LX/0QB;)LX/0TD;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v8

    check-cast v8, LX/1Ck;

    invoke-static {v0}, LX/6eF;->a(LX/0QB;)LX/6eF;

    move-result-object v9

    check-cast v9, LX/6eF;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v10

    check-cast v10, Lcom/facebook/content/SecureContextHelper;

    invoke-direct/range {v3 .. v10}, LX/IAd;-><init>(Landroid/content/Context;LX/IAe;LX/11H;Ljava/util/concurrent/ExecutorService;LX/1Ck;LX/6eF;Lcom/facebook/content/SecureContextHelper;)V

    .line 2545943
    move-object v0, v3

    .line 2545944
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2545945
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IAd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2545946
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2545947
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
