.class public LX/JJn;
.super LX/5pb;
.source ""

# interfaces
.implements LX/5on;
.implements LX/5pQ;


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "DeviceRequestAndroid"
.end annotation


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Zm;",
            ">;"
        }
    .end annotation
.end field

.field private b:Z

.field public c:LX/5pW;

.field private final d:LX/1Zl;


# direct methods
.method public constructor <init>(LX/5pY;LX/0Ot;)V
    .locals 1
    .param p1    # LX/5pY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5pY;",
            "LX/0Ot",
            "<",
            "LX/1Zm;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2679844
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 2679845
    new-instance v0, LX/JJm;

    invoke-direct {v0, p0}, LX/JJm;-><init>(LX/JJn;)V

    iput-object v0, p0, LX/JJn;->d:LX/1Zl;

    .line 2679846
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/JJn;->b:Z

    .line 2679847
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2679848
    invoke-virtual {v0, p0}, LX/5pX;->a(LX/5on;)V

    .line 2679849
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2679850
    invoke-virtual {v0, p0}, LX/5pX;->a(LX/5pQ;)V

    .line 2679851
    iput-object p2, p0, LX/JJn;->a:LX/0Ot;

    .line 2679852
    return-void
.end method

.method public static synthetic a(LX/JJn;)LX/5pY;
    .locals 1

    .prologue
    .line 2679855
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2679856
    return-object v0
.end method

.method private h()V
    .locals 2

    .prologue
    .line 2679853
    iget-object v0, p0, LX/JJn;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Zm;

    iget-object v1, p0, LX/JJn;->d:LX/1Zl;

    invoke-virtual {v0, v1}, LX/1Zm;->a(LX/1Zl;)Z

    .line 2679854
    return-void
.end method

.method private i()V
    .locals 2

    .prologue
    .line 2679806
    iget-object v0, p0, LX/JJn;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Zm;

    iget-object v1, p0, LX/JJn;->d:LX/1Zl;

    invoke-virtual {v0, v1}, LX/1Zm;->b(LX/1Zl;)Z

    .line 2679807
    return-void
.end method


# virtual methods
.method public final a(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    .line 2679841
    const/16 v0, 0x2716

    if-ne p1, v0, :cond_0

    iget-object v0, p0, LX/JJn;->c:LX/5pW;

    if-eqz v0, :cond_0

    .line 2679842
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/facebook/fbreact/devicerequests/DeviceRequestsNativeModule$2;

    invoke-direct {v1, p0, p3, p2}, Lcom/facebook/fbreact/devicerequests/DeviceRequestsNativeModule$2;-><init>(LX/JJn;Landroid/content/Intent;I)V

    const-wide/16 v2, 0x64

    const v4, 0xd9f47f3

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2679843
    :cond_0
    return-void
.end method

.method public final bM_()V
    .locals 1

    .prologue
    .line 2679838
    iget-boolean v0, p0, LX/JJn;->b:Z

    if-eqz v0, :cond_0

    .line 2679839
    invoke-direct {p0}, LX/JJn;->h()V

    .line 2679840
    :cond_0
    return-void
.end method

.method public final bN_()V
    .locals 1

    .prologue
    .line 2679857
    iget-boolean v0, p0, LX/JJn;->b:Z

    if-eqz v0, :cond_0

    .line 2679858
    invoke-direct {p0}, LX/JJn;->i()V

    .line 2679859
    :cond_0
    return-void
.end method

.method public final bO_()V
    .locals 0

    .prologue
    .line 2679837
    return-void
.end method

.method public beginDeviceRequestDiscovery()V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2679834
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/JJn;->b:Z

    .line 2679835
    invoke-direct {p0}, LX/JJn;->h()V

    .line 2679836
    return-void
.end method

.method public endDeviceRequestDiscovery()V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2679831
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/JJn;->b:Z

    .line 2679832
    invoke-direct {p0}, LX/JJn;->i()V

    .line 2679833
    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2679830
    const-string v0, "DeviceRequestAndroid"

    return-object v0
.end method

.method public ignoreDeviceRequest(LX/5pG;)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2679826
    new-instance v0, LX/246;

    invoke-direct {v0, p1}, LX/246;-><init>(LX/5pG;)V

    .line 2679827
    iget-object p0, v0, LX/246;->i:Ljava/lang/String;

    move-object v0, p0

    .line 2679828
    sget-object p0, LX/246;->a:Ljava/util/Set;

    invoke-interface {p0, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2679829
    return-void
.end method

.method public startLoginActivity(LX/5pG;LX/5pW;)V
    .locals 8
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2679808
    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v0

    .line 2679809
    new-instance v4, LX/246;

    invoke-direct {v4, p1}, LX/246;-><init>(LX/5pG;)V

    .line 2679810
    iget-object v1, v4, LX/246;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2679811
    iget-object v2, v4, LX/246;->f:Ljava/lang/String;

    move-object v2, v2

    .line 2679812
    iget-object v3, v4, LX/246;->g:Ljava/lang/String;

    move-object v3, v3

    .line 2679813
    iget-object v5, v4, LX/246;->i:Ljava/lang/String;

    move-object v4, v5

    .line 2679814
    const/4 v5, 0x1

    .line 2679815
    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    sget-object v7, LX/DAx;->a:Ljava/lang/String;

    const-string p1, "com.facebook.katana.ProxyAuth"

    invoke-virtual {v6, v7, p1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    const-string v7, "app_id"

    invoke-virtual {v6, v7, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    const-string v7, "nonce"

    invoke-virtual {v6, v7, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    const-string v7, "scope"

    invoke-virtual {v6, v7, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    const-string v7, "user_code"

    invoke-virtual {v6, v7, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    const-string v7, "force_confirmation"

    invoke-static {v5}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, v7, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    .line 2679816
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    const/4 p1, 0x0

    invoke-virtual {v7, v6, p1}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v7

    .line 2679817
    if-eqz v7, :cond_0

    invoke-static {v6}, LX/DAx;->b(Landroid/content/Intent;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 2679818
    :cond_0
    const/4 v6, 0x0

    .line 2679819
    :cond_1
    move-object v6, v6

    .line 2679820
    move-object v1, v6

    .line 2679821
    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    .line 2679822
    iput-object p2, p0, LX/JJn;->c:LX/5pW;

    .line 2679823
    const/16 v2, 0x2716

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 2679824
    :goto_0
    return-void

    .line 2679825
    :cond_2
    const-string v0, "E_ACTIVITY_DOES_NOT_EXIST"

    const-string v1, "failed"

    invoke-interface {p2, v0, v1}, LX/5pW;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
