.class public final LX/HlV;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/instagram/common/json/annotation/JsonType;
.end annotation


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/HlT;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2498669
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2498670
    const/4 v0, 0x0

    iput-object v0, p0, LX/HlV;->a:Ljava/util/List;

    .line 2498671
    return-void
.end method

.method private constructor <init>(Ljava/util/List;)V
    .locals 0
    .param p1    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/HlT;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2498672
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2498673
    iput-object p1, p0, LX/HlV;->a:Ljava/util/List;

    .line 2498674
    return-void
.end method

.method public static a(Ljava/util/List;LX/0SG;)LX/HlV;
    .locals 13
    .param p0    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/blescan/BleScanResult;",
            ">;",
            "LX/0SG;",
            ")",
            "LX/HlV;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2498675
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2498676
    :cond_0
    const/4 v0, 0x0

    .line 2498677
    :goto_0
    return-object v0

    .line 2498678
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2498679
    invoke-interface {p1}, LX/0SG;->a()J

    move-result-wide v2

    .line 2498680
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/blescan/BleScanResult;

    .line 2498681
    new-instance v5, LX/HlT;

    iget-wide v6, v0, Lcom/facebook/blescan/BleScanResult;->a:J

    sub-long v6, v2, v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    iget v7, v0, Lcom/facebook/blescan/BleScanResult;->c:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    iget-object v8, v0, Lcom/facebook/blescan/BleScanResult;->b:Ljava/lang/String;

    const/4 v9, 0x1

    new-array v9, v9, [LX/HlU;

    const/4 v10, 0x0

    new-instance v11, LX/HlU;

    const/4 v12, 0x3

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    iget-object v0, v0, Lcom/facebook/blescan/BleScanResult;->d:Ljava/lang/String;

    invoke-direct {v11, v12, v0}, LX/HlU;-><init>(Ljava/lang/Integer;Ljava/lang/String;)V

    aput-object v11, v9, v10

    invoke-static {v9}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v5, v6, v7, v8, v0}, LX/HlT;-><init>(Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/String;Ljava/util/List;)V

    .line 2498682
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2498683
    :cond_2
    new-instance v0, LX/HlV;

    invoke-direct {v0, v1}, LX/HlV;-><init>(Ljava/util/List;)V

    goto :goto_0
.end method
