.class public LX/JPH;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pc;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field private final a:LX/JPB;

.field public final b:LX/1vg;

.field private final c:LX/3mL;

.field public final d:LX/1LV;

.field private final e:LX/JPd;

.field private final f:LX/JPg;

.field private final g:LX/1DR;

.field private final h:LX/JPN;


# direct methods
.method public constructor <init>(LX/JPB;LX/1vg;LX/3mL;LX/1LV;LX/JPd;LX/JPg;LX/1DR;LX/JPN;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2689370
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2689371
    iput-object p1, p0, LX/JPH;->a:LX/JPB;

    .line 2689372
    iput-object p2, p0, LX/JPH;->b:LX/1vg;

    .line 2689373
    iput-object p3, p0, LX/JPH;->c:LX/3mL;

    .line 2689374
    iput-object p4, p0, LX/JPH;->d:LX/1LV;

    .line 2689375
    iput-object p5, p0, LX/JPH;->e:LX/JPd;

    .line 2689376
    iput-object p6, p0, LX/JPH;->f:LX/JPg;

    .line 2689377
    iput-object p7, p0, LX/JPH;->g:LX/1DR;

    .line 2689378
    iput-object p8, p0, LX/JPH;->h:LX/JPN;

    .line 2689379
    return-void
.end method

.method private static a(LX/JPH;LX/JPJ;)LX/0Px;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/JPJ;",
            ")",
            "LX/0Px",
            "<",
            "LX/JP9;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2689322
    invoke-virtual {p1}, LX/JPJ;->g()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 2689323
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 2689324
    check-cast v0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;

    .line 2689325
    invoke-static {v0}, LX/25C;->a(Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;)LX/0Px;

    move-result-object v5

    .line 2689326
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 2689327
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v7

    move v4, v3

    :goto_0
    if-ge v4, v7, :cond_2

    .line 2689328
    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    .line 2689329
    const/4 v1, 0x0

    .line 2689330
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    if-eqz v8, :cond_0

    .line 2689331
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v1

    .line 2689332
    :cond_0
    if-eqz v1, :cond_1

    .line 2689333
    invoke-static {v1}, LX/JPN;->a(Ljava/lang/String;)LX/JPM;

    move-result-object v8

    .line 2689334
    invoke-virtual {p1}, LX/JPJ;->g()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 2689335
    sget-object v9, LX/JPG;->a:[I

    invoke-virtual {v8}, LX/JPM;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_0

    .line 2689336
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "A new/illegal hpp card type was added but not defined"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2689337
    :pswitch_0
    iget-object v9, p0, LX/JPH;->h:LX/JPN;

    invoke-virtual {v9, v1, v0}, LX/JPN;->d(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;)Z

    move-result v1

    if-eqz v1, :cond_3

    move v1, v2

    .line 2689338
    :goto_1
    if-eqz v1, :cond_1

    .line 2689339
    new-instance v1, LX/JP9;

    invoke-direct {v1, v8, v0}, LX/JP9;-><init>(LX/JPM;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;)V

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2689340
    :cond_1
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 2689341
    :pswitch_1
    iget-object v9, p0, LX/JPH;->h:LX/JPN;

    const/4 v10, 0x0

    .line 2689342
    invoke-static {v9, v1, v0}, LX/JPN;->g(LX/JPN;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;)Z

    move-result v11

    if-nez v11, :cond_4

    .line 2689343
    :goto_2
    move v9, v10

    .line 2689344
    if-eqz v9, :cond_3

    iget-object v9, p0, LX/JPH;->h:LX/JPN;

    invoke-virtual {v9, v1, v0}, LX/JPN;->d(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;)Z

    move-result v1

    if-eqz v1, :cond_3

    move v1, v2

    .line 2689345
    goto :goto_1

    .line 2689346
    :pswitch_2
    iget-object v9, p0, LX/JPH;->h:LX/JPN;

    .line 2689347
    invoke-virtual {v9, v1, v0}, LX/JPN;->d(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;)Z

    move-result v10

    if-eqz v10, :cond_7

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->x()Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;

    move-result-object v10

    invoke-static {v9, v1, v0, v10}, LX/JPN;->a(LX/JPN;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;)Z

    move-result v10

    if-eqz v10, :cond_7

    const/4 v10, 0x1

    :goto_3
    move v1, v10

    .line 2689348
    if-eqz v1, :cond_3

    move v1, v2

    .line 2689349
    goto :goto_1

    .line 2689350
    :pswitch_3
    iget-object v9, p0, LX/JPH;->h:LX/JPN;

    invoke-virtual {v9, v1, v0}, LX/JPN;->c(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;)Z

    move-result v1

    if-eqz v1, :cond_3

    move v1, v2

    .line 2689351
    goto :goto_1

    .line 2689352
    :pswitch_4
    iget-object v9, p0, LX/JPH;->h:LX/JPN;

    const/4 v10, 0x0

    .line 2689353
    invoke-static {v9, v1, v0}, LX/JPN;->g(LX/JPN;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;)Z

    move-result v11

    if-nez v11, :cond_8

    .line 2689354
    :goto_4
    move v1, v10

    .line 2689355
    if-eqz v1, :cond_3

    move v1, v2

    .line 2689356
    goto :goto_1

    .line 2689357
    :cond_2
    invoke-static {v6}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0

    :cond_3
    move v1, v3

    goto :goto_1

    .line 2689358
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v11

    if-nez v11, :cond_5

    .line 2689359
    const-string v11, "content text is null"

    invoke-virtual {v9, v1, v0, v11}, LX/JPN;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;Ljava/lang/String;)V

    goto :goto_2

    .line 2689360
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v11

    invoke-static {v11}, LX/2yc;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/3Ab;

    move-result-object v11

    if-nez v11, :cond_6

    .line 2689361
    const-string v11, "getLinkableTextWithEntities is null"

    invoke-virtual {v9, v1, v0, v11}, LX/JPN;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;Ljava/lang/String;)V

    goto :goto_2

    .line 2689362
    :cond_6
    const/4 v10, 0x1

    goto :goto_2

    :cond_7
    const/4 v10, 0x0

    goto :goto_3

    .line 2689363
    :cond_8
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->k()Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    move-result-object v11

    if-nez v11, :cond_9

    .line 2689364
    const-string v11, "aymt hpp channel is null"

    invoke-virtual {v9, v1, v0, v11}, LX/JPN;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;Ljava/lang/String;)V

    goto :goto_4

    .line 2689365
    :cond_9
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->k()Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/graphql/model/GraphQLAYMTChannel;->k()LX/0Px;

    move-result-object v11

    if-nez v11, :cond_a

    .line 2689366
    const-string v11, "aymt hpp channel getTips() is null"

    invoke-virtual {v9, v1, v0, v11}, LX/JPN;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;Ljava/lang/String;)V

    goto :goto_4

    .line 2689367
    :cond_a
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->k()Lcom/facebook/graphql/model/GraphQLAYMTChannel;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/graphql/model/GraphQLAYMTChannel;->k()LX/0Px;

    move-result-object v11

    invoke-virtual {v11}, LX/0Px;->isEmpty()Z

    move-result v11

    if-eqz v11, :cond_b

    .line 2689368
    const-string v11, "aymt hpp channel getTips() is empty"

    invoke-virtual {v9, v1, v0, v11}, LX/JPN;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;Ljava/lang/String;)V

    goto :goto_4

    .line 2689369
    :cond_b
    const/4 v10, 0x1

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private static a(LX/1De;)LX/1Di;
    .locals 2

    .prologue
    .line 2689321
    invoke-static {p0}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v0

    const v1, 0x7f0a0442

    invoke-virtual {v0, v1}, LX/25Q;->i(I)LX/25Q;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const v1, 0x7f0b2583

    invoke-interface {v0, v1}, LX/1Di;->q(I)LX/1Di;

    move-result-object v0

    const/4 v1, 0x4

    invoke-interface {v0, v1}, LX/1Di;->b(I)LX/1Di;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/JPH;LX/1Pc;LX/1De;)LX/1Di;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;",
            "LX/1De;",
            ")",
            "LX/1Di;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 2689316
    iget-object v0, p0, LX/JPH;->g:LX/1DR;

    invoke-virtual {v0}, LX/1DR;->a()I

    move-result v0

    .line 2689317
    check-cast p1, LX/1Pn;

    invoke-interface {p1}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    int-to-float v0, v0

    invoke-static {v1, v0}, LX/0tP;->c(Landroid/content/Context;F)I

    move-result v0

    .line 2689318
    invoke-static {p2}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x2

    invoke-interface {v1, v2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, v4}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, v0}, LX/1Dh;->H(I)LX/1Dh;

    move-result-object v0

    invoke-static {p2}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v1

    const v2, 0x7f020915

    .line 2689319
    iget-object v3, p0, LX/JPH;->b:LX/1vg;

    invoke-virtual {v3, p2}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/2xv;->h(I)LX/2xv;

    move-result-object v3

    const p1, 0x7f0a010e

    invoke-virtual {v3, p1}, LX/2xv;->j(I)LX/2xv;

    move-result-object v3

    invoke-virtual {v3}, LX/1n6;->b()LX/1dc;

    move-result-object v3

    move-object v2, v3

    .line 2689320
    invoke-virtual {v1, v2}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const v2, 0x7f0b2584

    invoke-interface {v1, v2}, LX/1Di;->i(I)LX/1Di;

    move-result-object v1

    const v2, 0x7f0b2584

    invoke-interface {v1, v2}, LX/1Di;->q(I)LX/1Di;

    move-result-object v1

    const v2, 0x7f0b2584

    invoke-interface {v1, v4, v2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v1

    const/4 v2, 0x3

    const v3, 0x7f0b2571

    invoke-interface {v1, v2, v3}, LX/1Di;->c(II)LX/1Di;

    move-result-object v1

    const/4 v2, 0x0

    const v3, 0x7f0b2581

    invoke-interface {v1, v2, v3}, LX/1Di;->c(II)LX/1Di;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-static {p2}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v1

    const v2, 0x7f083a7f

    invoke-virtual {v1, v2}, LX/1ne;->h(I)LX/1ne;

    move-result-object v1

    const v2, 0x7f0b2582

    invoke-virtual {v1, v2}, LX/1ne;->q(I)LX/1ne;

    move-result-object v1

    const v2, 0x7f0a010e

    invoke-virtual {v1, v2}, LX/1ne;->n(I)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, v4}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/16 v2, 0x8

    const v3, 0x7f0b2571

    invoke-interface {v1, v2, v3}, LX/1Di;->c(II)LX/1Di;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/JPH;
    .locals 12

    .prologue
    .line 2689305
    const-class v1, LX/JPH;

    monitor-enter v1

    .line 2689306
    :try_start_0
    sget-object v0, LX/JPH;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2689307
    sput-object v2, LX/JPH;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2689308
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2689309
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2689310
    new-instance v3, LX/JPH;

    const-class v4, LX/JPB;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/JPB;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v5

    check-cast v5, LX/1vg;

    invoke-static {v0}, LX/3mL;->a(LX/0QB;)LX/3mL;

    move-result-object v6

    check-cast v6, LX/3mL;

    invoke-static {v0}, LX/1LV;->a(LX/0QB;)LX/1LV;

    move-result-object v7

    check-cast v7, LX/1LV;

    invoke-static {v0}, LX/JPd;->a(LX/0QB;)LX/JPd;

    move-result-object v8

    check-cast v8, LX/JPd;

    invoke-static {v0}, LX/JPg;->a(LX/0QB;)LX/JPg;

    move-result-object v9

    check-cast v9, LX/JPg;

    invoke-static {v0}, LX/1DR;->a(LX/0QB;)LX/1DR;

    move-result-object v10

    check-cast v10, LX/1DR;

    invoke-static {v0}, LX/JPN;->b(LX/0QB;)LX/JPN;

    move-result-object v11

    check-cast v11, LX/JPN;

    invoke-direct/range {v3 .. v11}, LX/JPH;-><init>(LX/JPB;LX/1vg;LX/3mL;LX/1LV;LX/JPd;LX/JPg;LX/1DR;LX/JPN;)V

    .line 2689311
    move-object v0, v3

    .line 2689312
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2689313
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JPH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2689314
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2689315
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1Pc;LX/JPJ;)LX/1Dg;
    .locals 6
    .param p2    # LX/1Pc;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/JPJ;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "TE;",
            "LX/JPJ;",
            ")",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 2689254
    new-instance v1, LX/JPF;

    invoke-direct {v1, p0, p3}, LX/JPF;-><init>(LX/JPH;LX/JPJ;)V

    .line 2689255
    invoke-static {}, LX/3mP;->newBuilder()LX/3mP;

    move-result-object v2

    iget-object v0, p3, LX/JPJ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2689256
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 2689257
    check-cast v0, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/25L;->a(Ljava/lang/String;)LX/25L;

    move-result-object v0

    .line 2689258
    iput-object v0, v2, LX/3mP;->d:LX/25L;

    .line 2689259
    move-object v2, v2

    .line 2689260
    iget-object v0, p3, LX/JPJ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2689261
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 2689262
    check-cast v0, LX/0jW;

    .line 2689263
    iput-object v0, v2, LX/3mP;->e:LX/0jW;

    .line 2689264
    move-object v0, v2

    .line 2689265
    const/16 v2, 0x8

    .line 2689266
    iput v2, v0, LX/3mP;->b:I

    .line 2689267
    move-object v0, v0

    .line 2689268
    iput-object v1, v0, LX/3mP;->g:LX/25K;

    .line 2689269
    move-object v0, v0

    .line 2689270
    invoke-virtual {v0}, LX/3mP;->a()LX/25M;

    move-result-object v5

    .line 2689271
    iget-object v0, p0, LX/JPH;->a:LX/JPB;

    invoke-static {p0, p3}, LX/JPH;->a(LX/JPH;LX/JPJ;)LX/0Px;

    move-result-object v3

    move-object v1, p1

    move-object v2, p3

    move-object v4, p2

    invoke-virtual/range {v0 .. v5}, LX/JPB;->a(Landroid/content/Context;LX/JPJ;LX/0Px;Ljava/lang/Object;LX/25M;)LX/JPA;

    move-result-object v1

    .line 2689272
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v0, v2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    invoke-static {p0, p2, p1}, LX/JPH;->a(LX/JPH;LX/1Pc;LX/1De;)LX/1Di;

    move-result-object v2

    invoke-interface {v0, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/JPH;->a(LX/1De;)LX/1Di;

    move-result-object v2

    invoke-interface {v0, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    iget-object v0, p0, LX/JPH;->e:LX/JPd;

    const/4 v3, 0x0

    .line 2689273
    new-instance v4, LX/JPc;

    invoke-direct {v4, v0}, LX/JPc;-><init>(LX/JPd;)V

    .line 2689274
    iget-object v5, v0, LX/JPd;->b:LX/0Zi;

    invoke-virtual {v5}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/JPb;

    .line 2689275
    if-nez v5, :cond_0

    .line 2689276
    new-instance v5, LX/JPb;

    invoke-direct {v5, v0}, LX/JPb;-><init>(LX/JPd;)V

    .line 2689277
    :cond_0
    invoke-static {v5, p1, v3, v3, v4}, LX/JPb;->a$redex0(LX/JPb;LX/1De;IILX/JPc;)V

    .line 2689278
    move-object v4, v5

    .line 2689279
    move-object v3, v4

    .line 2689280
    move-object v3, v3

    .line 2689281
    move-object v0, p2

    check-cast v0, LX/1Pn;

    .line 2689282
    iget-object v4, v3, LX/JPb;->a:LX/JPc;

    iput-object v0, v4, LX/JPc;->a:LX/1Pn;

    .line 2689283
    iget-object v4, v3, LX/JPb;->e:Ljava/util/BitSet;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 2689284
    move-object v0, v3

    .line 2689285
    iget-object v3, v0, LX/JPb;->a:LX/JPc;

    iput-object p3, v3, LX/JPc;->b:LX/JPJ;

    .line 2689286
    iget-object v3, v0, LX/JPb;->e:Ljava/util/BitSet;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 2689287
    move-object v0, v0

    .line 2689288
    invoke-interface {v2, v0}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v0

    iget-object v2, p0, LX/JPH;->c:LX/3mL;

    invoke-virtual {v2, p1}, LX/3mL;->c(LX/1De;)LX/3ml;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/3ml;->a(LX/3mX;)LX/3ml;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/4 v2, 0x7

    const v3, 0x7f0b257b

    invoke-interface {v1, v2, v3}, LX/1Di;->g(II)LX/1Di;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/JPH;->a(LX/1De;)LX/1Di;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    iget-object v1, p0, LX/JPH;->f:LX/JPg;

    const/4 v2, 0x0

    .line 2689289
    new-instance v3, LX/JPf;

    invoke-direct {v3, v1}, LX/JPf;-><init>(LX/JPg;)V

    .line 2689290
    iget-object v4, v1, LX/JPg;->b:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/JPe;

    .line 2689291
    if-nez v4, :cond_1

    .line 2689292
    new-instance v4, LX/JPe;

    invoke-direct {v4, v1}, LX/JPe;-><init>(LX/JPg;)V

    .line 2689293
    :cond_1
    invoke-static {v4, p1, v2, v2, v3}, LX/JPe;->a$redex0(LX/JPe;LX/1De;IILX/JPf;)V

    .line 2689294
    move-object v3, v4

    .line 2689295
    move-object v2, v3

    .line 2689296
    move-object v1, v2

    .line 2689297
    check-cast p2, LX/1Po;

    .line 2689298
    iget-object v2, v1, LX/JPe;->a:LX/JPf;

    iput-object p2, v2, LX/JPf;->a:LX/1Po;

    .line 2689299
    iget-object v2, v1, LX/JPe;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2689300
    move-object v1, v1

    .line 2689301
    iget-object v2, v1, LX/JPe;->a:LX/JPf;

    iput-object p3, v2, LX/JPf;->b:LX/JPJ;

    .line 2689302
    iget-object v2, v1, LX/JPe;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2689303
    move-object v1, v1

    .line 2689304
    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0
.end method
