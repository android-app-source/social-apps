.class public final LX/HS0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:J

.field public final synthetic b:LX/15i;

.field public final synthetic c:I

.field public final synthetic d:LX/0am;

.field public final synthetic e:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniUpsellCardView;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniUpsellCardView;JLX/15i;ILX/0am;)V
    .locals 0

    .prologue
    .line 2466250
    iput-object p1, p0, LX/HS0;->e:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniUpsellCardView;

    iput-wide p2, p0, LX/HS0;->a:J

    iput-object p4, p0, LX/HS0;->b:LX/15i;

    iput p5, p0, LX/HS0;->c:I

    iput-object p6, p0, LX/HS0;->d:LX/0am;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v0, 0x1

    const v1, 0xf7bb263

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2466245
    iget-object v0, p0, LX/HS0;->e:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniUpsellCardView;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniUpsellCardView;->b:LX/9XE;

    iget-wide v2, p0, LX/HS0;->a:J

    iget-object v4, p0, LX/HS0;->b:LX/15i;

    iget v5, p0, LX/HS0;->c:I

    const/4 v6, 0x5

    invoke-virtual {v4, v5, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    const-string v5, "upsell_card"

    invoke-virtual {v0, v2, v3, v4, v5}, LX/9XE;->a(JLjava/lang/String;Ljava/lang/String;)V

    .line 2466246
    iget-object v0, p0, LX/HS0;->e:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniUpsellCardView;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniUpsellCardView;->c:LX/GMm;

    invoke-virtual {v0}, LX/GMm;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/HS0;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2466247
    iget-object v0, p0, LX/HS0;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HRS;

    invoke-virtual {v0}, LX/HRS;->a()V

    .line 2466248
    :cond_0
    iget-object v0, p0, LX/HS0;->e:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniUpsellCardView;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniUpsellCardView;->c:LX/GMm;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-wide v4, p0, LX/HS0;->a:J

    invoke-virtual {v0, v2, v4, v5}, LX/GMm;->a(Landroid/content/Context;J)V

    .line 2466249
    const v0, -0x5499a177

    invoke-static {v7, v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
