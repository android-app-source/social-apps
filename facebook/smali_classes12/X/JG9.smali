.class public LX/JG9;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/media/MediaPlayer;

.field public b:Z

.field public c:Z

.field public d:F


# direct methods
.method public constructor <init>(Landroid/media/MediaPlayer;Z)V
    .locals 1

    .prologue
    .line 2666868
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2666869
    iput-object p1, p0, LX/JG9;->a:Landroid/media/MediaPlayer;

    .line 2666870
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/JG9;->c:Z

    .line 2666871
    iput-boolean p2, p0, LX/JG9;->b:Z

    .line 2666872
    const/4 v0, 0x0

    iput v0, p0, LX/JG9;->d:F

    .line 2666873
    return-void
.end method

.method public static a(I)D
    .locals 4

    .prologue
    .line 2666874
    int-to-double v0, p0

    const-wide v2, 0x408f400000000000L    # 1000.0

    div-double/2addr v0, v2

    return-wide v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;LX/0o1;)LX/JG9;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2666875
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {p1}, LX/JG9;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "raw"

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 2666876
    if-nez v0, :cond_0

    .line 2666877
    new-instance v0, LX/JGA;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not find audio asset: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/JGA;-><init>(Ljava/lang/String;)V

    invoke-interface {p2, v0}, LX/0o1;->a(Ljava/lang/Exception;)V

    .line 2666878
    new-instance v0, LX/JG9;

    new-instance v1, Landroid/media/MediaPlayer;

    invoke-direct {v1}, Landroid/media/MediaPlayer;-><init>()V

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/JG9;-><init>(Landroid/media/MediaPlayer;Z)V

    .line 2666879
    :goto_0
    return-object v0

    .line 2666880
    :cond_0
    invoke-static {p0, v0}, LX/6LH;->a(Landroid/content/Context;I)Landroid/content/res/AssetFileDescriptor;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 2666881
    invoke-static {p0, v0}, Landroid/media/MediaPlayer;->create(Landroid/content/Context;I)Landroid/media/MediaPlayer;

    move-result-object v0

    move-object v1, v0

    .line 2666882
    :cond_1
    :goto_1
    if-nez v1, :cond_3

    .line 2666883
    new-instance v0, LX/5p9;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not create audio: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5p9;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2666884
    :cond_2
    invoke-static {p0, v0}, LX/6LH;->b(Landroid/content/Context;I)Ljava/io/FileDescriptor;

    move-result-object v2

    .line 2666885
    if-eqz v2, :cond_1

    .line 2666886
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    .line 2666887
    :try_start_0
    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setDataSource(Ljava/io/FileDescriptor;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v0

    .line 2666888
    goto :goto_1

    .line 2666889
    :cond_3
    new-instance v0, LX/JG9;

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, LX/JG9;-><init>(Landroid/media/MediaPlayer;Z)V

    goto :goto_0

    .line 2666890
    :catch_0
    goto :goto_1
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2666866
    const/16 v0, 0x2e

    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 2666867
    if-gez v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public final a(F)V
    .locals 1

    .prologue
    .line 2666862
    iput p1, p0, LX/JG9;->d:F

    .line 2666863
    iget-boolean v0, p0, LX/JG9;->b:Z

    if-eqz v0, :cond_0

    .line 2666864
    iget-object v0, p0, LX/JG9;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1, p1}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 2666865
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2666855
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/JG9;->c:Z

    .line 2666856
    iget-boolean v0, p0, LX/JG9;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/JG9;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2666857
    iget-object v0, p0, LX/JG9;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    .line 2666858
    :cond_0
    return-void
.end method

.method public final d()D
    .locals 2

    .prologue
    .line 2666859
    iget-boolean v0, p0, LX/JG9;->b:Z

    if-eqz v0, :cond_0

    .line 2666860
    iget-object v0, p0, LX/JG9;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    invoke-static {v0}, LX/JG9;->a(I)D

    move-result-wide v0

    .line 2666861
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method
