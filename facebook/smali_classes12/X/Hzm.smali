.class public final LX/Hzm;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;)V
    .locals 0

    .prologue
    .line 2525971
    iput-object p1, p0, LX/Hzm;->a:Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2525972
    iget-object v0, p0, LX/Hzm;->a:Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;

    invoke-static {v0}, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->p(Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;)V

    .line 2525973
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2525974
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2525975
    iget-object v0, p0, LX/Hzm;->a:Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;

    .line 2525976
    if-eqz p1, :cond_0

    .line 2525977
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2525978
    if-eqz v1, :cond_0

    .line 2525979
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2525980
    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel$AllEventsModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2525981
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2525982
    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel$AllEventsModel;

    move-result-object v1

    .line 2525983
    iget-object v2, v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->d:LX/Hyx;

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel$AllEventsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v2, v3, p1}, LX/Hyx;->a(LX/0Px;Lcom/facebook/graphql/executor/GraphQLResult;)LX/0Px;

    move-result-object v2

    .line 2525984
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel$AllEventsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 2525985
    iget-object v3, v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->d:LX/Hyx;

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel$AllEventsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;->a()Ljava/lang/String;

    move-result-object p0

    .line 2525986
    iput-object p0, v3, LX/Hyx;->a:Ljava/lang/String;

    .line 2525987
    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel$AllEventsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;->b()Z

    move-result v1

    iput-boolean v1, v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->y:Z

    .line 2525988
    :goto_0
    iget-object v1, v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->d:LX/Hyx;

    invoke-virtual {v1, v2}, LX/Hyx;->a(LX/0Px;)V

    .line 2525989
    :cond_0
    return-void

    .line 2525990
    :cond_1
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->y:Z

    goto :goto_0
.end method
