.class public final enum LX/Hed;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Hed;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Hed;

.field public static final enum LEFT:LX/Hed;

.field public static final enum RIGHT:LX/Hed;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2490575
    new-instance v0, LX/Hed;

    const-string v1, "LEFT"

    invoke-direct {v0, v1, v2}, LX/Hed;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hed;->LEFT:LX/Hed;

    new-instance v0, LX/Hed;

    const-string v1, "RIGHT"

    invoke-direct {v0, v1, v3}, LX/Hed;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hed;->RIGHT:LX/Hed;

    .line 2490576
    const/4 v0, 0x2

    new-array v0, v0, [LX/Hed;

    sget-object v1, LX/Hed;->LEFT:LX/Hed;

    aput-object v1, v0, v2

    sget-object v1, LX/Hed;->RIGHT:LX/Hed;

    aput-object v1, v0, v3

    sput-object v0, LX/Hed;->$VALUES:[LX/Hed;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2490577
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Hed;
    .locals 1

    .prologue
    .line 2490578
    const-class v0, LX/Hed;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Hed;

    return-object v0
.end method

.method public static values()[LX/Hed;
    .locals 1

    .prologue
    .line 2490579
    sget-object v0, LX/Hed;->$VALUES:[LX/Hed;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Hed;

    return-object v0
.end method
