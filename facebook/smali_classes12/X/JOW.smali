.class public final LX/JOW;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JOY;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

.field public c:LX/1Pn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public d:LX/2dx;

.field public final synthetic e:LX/JOY;


# direct methods
.method public constructor <init>(LX/JOY;)V
    .locals 1

    .prologue
    .line 2687718
    iput-object p1, p0, LX/JOW;->e:LX/JOY;

    .line 2687719
    move-object v0, p1

    .line 2687720
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2687721
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2687722
    const-string v0, "AYMTCardComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2687723
    if-ne p0, p1, :cond_1

    .line 2687724
    :cond_0
    :goto_0
    return v0

    .line 2687725
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2687726
    goto :goto_0

    .line 2687727
    :cond_3
    check-cast p1, LX/JOW;

    .line 2687728
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2687729
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2687730
    if-eq v2, v3, :cond_0

    .line 2687731
    iget-object v2, p0, LX/JOW;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JOW;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/JOW;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2687732
    goto :goto_0

    .line 2687733
    :cond_5
    iget-object v2, p1, LX/JOW;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 2687734
    :cond_6
    iget-object v2, p0, LX/JOW;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/JOW;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    iget-object v3, p1, LX/JOW;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2687735
    goto :goto_0

    .line 2687736
    :cond_8
    iget-object v2, p1, LX/JOW;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    if-nez v2, :cond_7

    .line 2687737
    :cond_9
    iget-object v2, p0, LX/JOW;->c:LX/1Pn;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/JOW;->c:LX/1Pn;

    iget-object v3, p1, LX/JOW;->c:LX/1Pn;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2687738
    goto :goto_0

    .line 2687739
    :cond_b
    iget-object v2, p1, LX/JOW;->c:LX/1Pn;

    if-nez v2, :cond_a

    .line 2687740
    :cond_c
    iget-object v2, p0, LX/JOW;->d:LX/2dx;

    if-eqz v2, :cond_d

    iget-object v2, p0, LX/JOW;->d:LX/2dx;

    iget-object v3, p1, LX/JOW;->d:LX/2dx;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2687741
    goto :goto_0

    .line 2687742
    :cond_d
    iget-object v2, p1, LX/JOW;->d:LX/2dx;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
