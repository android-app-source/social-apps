.class public LX/J4L;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/J4F;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/J4J;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/J4K;

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Z

.field public j:Ljava/lang/String;

.field public k:Z

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public q:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/J4K;)V
    .locals 1

    .prologue
    .line 2643933
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2643934
    iput-object p1, p0, LX/J4L;->c:LX/J4K;

    .line 2643935
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/J4L;->a:Ljava/util/ArrayList;

    .line 2643936
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/J4L;->b:Ljava/util/HashMap;

    .line 2643937
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2643938
    iput-object v0, p0, LX/J4L;->d:LX/0Px;

    .line 2643939
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/J4L;->i:Z

    .line 2643940
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/J4L;->k:Z

    .line 2643941
    return-void
.end method


# virtual methods
.method public final a(LX/0Px;)V
    .locals 1
    .param p1    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLPrivacyCheckupActionType;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2643948
    if-nez p1, :cond_0

    .line 2643949
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2643950
    iput-object v0, p0, LX/J4L;->d:LX/0Px;

    .line 2643951
    :goto_0
    return-void

    .line 2643952
    :cond_0
    iput-object p1, p0, LX/J4L;->d:LX/0Px;

    goto :goto_0
.end method

.method public final a(LX/J4L;)V
    .locals 2

    .prologue
    .line 2643953
    iget-boolean v0, p1, LX/J4L;->i:Z

    iput-boolean v0, p0, LX/J4L;->i:Z

    .line 2643954
    iget-object v0, p1, LX/J4L;->d:LX/0Px;

    iput-object v0, p0, LX/J4L;->d:LX/0Px;

    .line 2643955
    iget-object v0, p0, LX/J4L;->a:Ljava/util/ArrayList;

    iget-object v1, p1, LX/J4L;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 2643956
    iget-object v0, p0, LX/J4L;->b:Ljava/util/HashMap;

    iget-object v1, p1, LX/J4L;->b:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 2643957
    iget-boolean v0, p1, LX/J4L;->k:Z

    iput-boolean v0, p0, LX/J4L;->k:Z

    .line 2643958
    iget-object v0, p1, LX/J4L;->j:Ljava/lang/String;

    iput-object v0, p0, LX/J4L;->j:Ljava/lang/String;

    .line 2643959
    iget-object v0, p1, LX/J4L;->p:LX/0Px;

    iput-object v0, p0, LX/J4L;->p:LX/0Px;

    .line 2643960
    iget-object v0, p1, LX/J4L;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2643961
    iget-object v0, p1, LX/J4L;->e:Ljava/lang/String;

    iput-object v0, p0, LX/J4L;->e:Ljava/lang/String;

    .line 2643962
    :cond_0
    iget-object v0, p1, LX/J4L;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2643963
    iget-object v0, p1, LX/J4L;->f:Ljava/lang/String;

    iput-object v0, p0, LX/J4L;->f:Ljava/lang/String;

    .line 2643964
    :cond_1
    iget-object v0, p1, LX/J4L;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2643965
    iget-object v0, p1, LX/J4L;->g:Ljava/lang/String;

    iput-object v0, p0, LX/J4L;->g:Ljava/lang/String;

    .line 2643966
    :cond_2
    iget-object v0, p1, LX/J4L;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2643967
    iget-object v0, p1, LX/J4L;->h:Ljava/lang/String;

    iput-object v0, p0, LX/J4L;->h:Ljava/lang/String;

    .line 2643968
    :cond_3
    iget-object v0, p1, LX/J4L;->l:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2643969
    iget-object v0, p1, LX/J4L;->l:Ljava/lang/String;

    iput-object v0, p0, LX/J4L;->l:Ljava/lang/String;

    .line 2643970
    :cond_4
    iget-object v0, p1, LX/J4L;->m:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 2643971
    iget-object v0, p1, LX/J4L;->m:Ljava/lang/String;

    iput-object v0, p0, LX/J4L;->m:Ljava/lang/String;

    .line 2643972
    :cond_5
    iget-object v0, p1, LX/J4L;->n:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 2643973
    iget-object v0, p1, LX/J4L;->n:Ljava/lang/String;

    iput-object v0, p0, LX/J4L;->n:Ljava/lang/String;

    .line 2643974
    :cond_6
    iget-object v0, p1, LX/J4L;->o:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 2643975
    iget-object v0, p1, LX/J4L;->o:Ljava/lang/String;

    iput-object v0, p0, LX/J4L;->o:Ljava/lang/String;

    .line 2643976
    :cond_7
    iget-object v0, p1, LX/J4L;->q:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 2643977
    iget-object v0, p1, LX/J4L;->q:Ljava/lang/String;

    iput-object v0, p0, LX/J4L;->q:Ljava/lang/String;

    .line 2643978
    :cond_8
    return-void
.end method

.method public final a(Ljava/util/Collection;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "LX/J4F;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2643942
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/J4F;

    .line 2643943
    iget-object v2, p0, LX/J4L;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2643944
    instance-of v2, v0, LX/J4J;

    if-eqz v2, :cond_0

    .line 2643945
    check-cast v0, LX/J4J;

    .line 2643946
    iget-object v2, p0, LX/J4L;->b:Ljava/util/HashMap;

    iget-object v3, v0, LX/J4J;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2643947
    :cond_1
    return-void
.end method
