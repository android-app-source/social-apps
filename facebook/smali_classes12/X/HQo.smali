.class public LX/HQo;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/HQo;


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2463999
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2464000
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/HQo;->a:Ljava/util/Map;

    .line 2464001
    return-void
.end method

.method public static a(LX/0QB;)LX/HQo;
    .locals 3

    .prologue
    .line 2464002
    sget-object v0, LX/HQo;->b:LX/HQo;

    if-nez v0, :cond_1

    .line 2464003
    const-class v1, LX/HQo;

    monitor-enter v1

    .line 2464004
    :try_start_0
    sget-object v0, LX/HQo;->b:LX/HQo;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2464005
    if-eqz v2, :cond_0

    .line 2464006
    :try_start_1
    new-instance v0, LX/HQo;

    invoke-direct {v0}, LX/HQo;-><init>()V

    .line 2464007
    move-object v0, v0

    .line 2464008
    sput-object v0, LX/HQo;->b:LX/HQo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2464009
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2464010
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2464011
    :cond_1
    sget-object v0, LX/HQo;->b:LX/HQo;

    return-object v0

    .line 2464012
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2464013
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2464014
    iget-object v0, p0, LX/HQo;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2464015
    return-void
.end method

.method public final b(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;
    .locals 1

    .prologue
    .line 2464016
    iget-object v0, p0, LX/HQo;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;

    return-object v0
.end method
