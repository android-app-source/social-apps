.class public final LX/JPy;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponent;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLInstagramPhotosFromFriendsFeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/4ZX;

.field public c:LX/1Pm;

.field public d:LX/JQ1;

.field public e:Ljava/lang/Boolean;

.field public f:LX/3mj;

.field public final synthetic g:Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponent;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponent;)V
    .locals 1

    .prologue
    .line 2690762
    iput-object p1, p0, LX/JPy;->g:Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponent;

    .line 2690763
    move-object v0, p1

    .line 2690764
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2690765
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2690736
    const-string v0, "InstagramPhotoFromFriendsItemComponent"

    return-object v0
.end method

.method public final a(LX/1X1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<",
            "Lcom/facebook/feedplugins/instagram/InstagramPhotoFromFriendsItemComponent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2690766
    check-cast p1, LX/JPy;

    .line 2690767
    iget-object v0, p1, LX/JPy;->d:LX/JQ1;

    iput-object v0, p0, LX/JPy;->d:LX/JQ1;

    .line 2690768
    iget-object v0, p1, LX/JPy;->e:Ljava/lang/Boolean;

    iput-object v0, p0, LX/JPy;->e:Ljava/lang/Boolean;

    .line 2690769
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2690742
    if-ne p0, p1, :cond_1

    .line 2690743
    :cond_0
    :goto_0
    return v0

    .line 2690744
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2690745
    goto :goto_0

    .line 2690746
    :cond_3
    check-cast p1, LX/JPy;

    .line 2690747
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2690748
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2690749
    if-eq v2, v3, :cond_0

    .line 2690750
    iget-object v2, p0, LX/JPy;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JPy;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/JPy;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2690751
    goto :goto_0

    .line 2690752
    :cond_5
    iget-object v2, p1, LX/JPy;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 2690753
    :cond_6
    iget-object v2, p0, LX/JPy;->b:LX/4ZX;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/JPy;->b:LX/4ZX;

    iget-object v3, p1, LX/JPy;->b:LX/4ZX;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2690754
    goto :goto_0

    .line 2690755
    :cond_8
    iget-object v2, p1, LX/JPy;->b:LX/4ZX;

    if-nez v2, :cond_7

    .line 2690756
    :cond_9
    iget-object v2, p0, LX/JPy;->c:LX/1Pm;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/JPy;->c:LX/1Pm;

    iget-object v3, p1, LX/JPy;->c:LX/1Pm;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2690757
    goto :goto_0

    .line 2690758
    :cond_b
    iget-object v2, p1, LX/JPy;->c:LX/1Pm;

    if-nez v2, :cond_a

    .line 2690759
    :cond_c
    iget-object v2, p0, LX/JPy;->f:LX/3mj;

    if-eqz v2, :cond_d

    iget-object v2, p0, LX/JPy;->f:LX/3mj;

    iget-object v3, p1, LX/JPy;->f:LX/3mj;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2690760
    goto :goto_0

    .line 2690761
    :cond_d
    iget-object v2, p1, LX/JPy;->f:LX/3mj;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 2690737
    const/4 v1, 0x0

    .line 2690738
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/JPy;

    .line 2690739
    iput-object v1, v0, LX/JPy;->d:LX/JQ1;

    .line 2690740
    iput-object v1, v0, LX/JPy;->e:Ljava/lang/Boolean;

    .line 2690741
    return-object v0
.end method
