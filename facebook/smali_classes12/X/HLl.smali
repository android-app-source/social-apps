.class public LX/HLl;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/HLl;


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/HLk;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2456229
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2456230
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/HLl;->a:Ljava/util/Map;

    .line 2456231
    return-void
.end method

.method public static a(LX/0QB;)LX/HLl;
    .locals 3

    .prologue
    .line 2456232
    sget-object v0, LX/HLl;->b:LX/HLl;

    if-nez v0, :cond_1

    .line 2456233
    const-class v1, LX/HLl;

    monitor-enter v1

    .line 2456234
    :try_start_0
    sget-object v0, LX/HLl;->b:LX/HLl;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2456235
    if-eqz v2, :cond_0

    .line 2456236
    :try_start_1
    new-instance v0, LX/HLl;

    invoke-direct {v0}, LX/HLl;-><init>()V

    .line 2456237
    move-object v0, v0

    .line 2456238
    sput-object v0, LX/HLl;->b:LX/HLl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2456239
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2456240
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2456241
    :cond_1
    sget-object v0, LX/HLl;->b:LX/HLl;

    return-object v0

    .line 2456242
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2456243
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(LX/HLl;Ljava/lang/String;)Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/HLk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2456244
    iget-object v0, p0, LX/HLl;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2456245
    iget-object v0, p0, LX/HLl;->a:Ljava/util/Map;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2456246
    :cond_0
    iget-object v0, p0, LX/HLl;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    return-object v0
.end method
