.class public LX/Ij8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6xu;


# instance fields
.field private final a:LX/6yW;


# direct methods
.method public constructor <init>(LX/6yW;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2605364
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2605365
    iput-object p1, p0, LX/Ij8;->a:LX/6yW;

    .line 2605366
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2605363
    const-string v0, "p2p_cancel_add_card"

    return-object v0
.end method

.method public final b(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2605353
    const-string v0, "p2p_confirm_card_details"

    return-object v0
.end method

.method public final c(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2605362
    const-string v0, "p2p_add_card_success"

    return-object v0
.end method

.method public final d(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2605361
    const-string v0, "p2p_add_card_fail"

    return-object v0
.end method

.method public final e(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2605360
    const-string v0, "p2p_add_card_save_button_click"

    return-object v0
.end method

.method public final f(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2605359
    const-string v0, "p2p_add_card_done_button_click"

    return-object v0
.end method

.method public final g(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2605358
    iget-object v0, p0, LX/Ij8;->a:LX/6yW;

    invoke-virtual {v0, p1}, LX/6yW;->g(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final h(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2605357
    iget-object v0, p0, LX/Ij8;->a:LX/6yW;

    invoke-virtual {v0, p1}, LX/6yW;->h(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final i(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2605356
    iget-object v0, p0, LX/Ij8;->a:LX/6yW;

    invoke-virtual {v0, p1}, LX/6yW;->i(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final j(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2605355
    iget-object v0, p0, LX/Ij8;->a:LX/6yW;

    invoke-virtual {v0, p1}, LX/6yW;->j(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final k(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2605354
    iget-object v0, p0, LX/Ij8;->a:LX/6yW;

    invoke-virtual {v0, p1}, LX/6yW;->k(Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
