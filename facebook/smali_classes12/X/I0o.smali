.class public LX/I0o;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;",
            ">;>;"
        }
    .end annotation
.end field

.field public final c:Landroid/content/Context;

.field public final d:LX/0tX;

.field private final e:LX/1My;

.field public final f:LX/0hB;

.field private final g:LX/0Or;
    .annotation build Lcom/facebook/events/dateformatter/annotation/EventsTimeZone;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/util/TimeZone;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2527903
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "EVENT"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "BIRTHDAY"

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, LX/I0o;->a:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0tX;LX/1My;LX/0hB;LX/0Or;)V
    .locals 0
    .param p5    # LX/0Or;
        .annotation build Lcom/facebook/events/dateformatter/annotation/EventsTimeZone;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0tX;",
            "LX/1My;",
            "LX/0hB;",
            "LX/0Or",
            "<",
            "Ljava/util/TimeZone;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2527904
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2527905
    iput-object p1, p0, LX/I0o;->c:Landroid/content/Context;

    .line 2527906
    iput-object p2, p0, LX/I0o;->d:LX/0tX;

    .line 2527907
    iput-object p3, p0, LX/I0o;->e:LX/1My;

    .line 2527908
    iput-object p4, p0, LX/I0o;->f:LX/0hB;

    .line 2527909
    iput-object p5, p0, LX/I0o;->g:LX/0Or;

    .line 2527910
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;ZJII)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "ZJII)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventsCalendarQueryModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2527911
    new-instance v0, LX/7n0;

    invoke-direct {v0}, LX/7n0;-><init>()V

    const-string v1, "item_types"

    sget-object v2, LX/I0o;->a:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    move-result-object v0

    const-string v1, "profile_image_size"

    invoke-static {p6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "cover_image_portrait_size"

    iget-object v2, p0, LX/I0o;->f:LX/0hB;

    invoke-virtual {v2}, LX/0hB;->f()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "cover_image_landscape_size"

    iget-object v2, p0, LX/I0o;->f:LX/0hB;

    invoke-virtual {v2}, LX/0hB;->g()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "first"

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "timezone"

    iget-object v0, p0, LX/I0o;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/TimeZone;

    invoke-virtual {v0}, Ljava/util/TimeZone;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "order"

    const-string v2, "ASCENDING"

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    .line 2527912
    const-string v0, "ends_after"

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, p3, p4}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2527913
    if-nez p2, :cond_0

    .line 2527914
    iget-object v0, p0, LX/I0o;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/TimeZone;

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    .line 2527915
    invoke-virtual {v0, p3, p4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 2527916
    const/4 v2, 0x6

    sget v3, LX/I0h;->a:I

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->add(II)V

    .line 2527917
    const-string v2, "starts_before"

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2527918
    :cond_0
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2527919
    const-string v0, "after_cursor"

    invoke-virtual {v1, v0, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2527920
    :cond_1
    new-instance v0, LX/7n0;

    invoke-direct {v0}, LX/7n0;-><init>()V

    move-object v0, v0

    .line 2527921
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2527922
    iget-object v2, v1, LX/0gW;->e:LX/0w7;

    move-object v1, v2

    .line 2527923
    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0w7;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->d:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    const/4 v1, 0x1

    .line 2527924
    iput-boolean v1, v0, LX/0zO;->p:Z

    .line 2527925
    move-object v0, v0

    .line 2527926
    iget-object v1, p0, LX/I0o;->d:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0Px;Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/graphql/EventDashboardGraphQLInterfaces$EventCalendarableItem;",
            ">;",
            "Lcom/facebook/graphql/executor/GraphQLResult;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2527927
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v8

    const/4 v0, 0x0

    move v7, v0

    :goto_0
    if-ge v7, v8, :cond_0

    invoke-virtual {p1, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;

    .line 2527928
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 2527929
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v6, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2527930
    new-instance v1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->d()LX/7oa;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    .line 2527931
    iget-object v3, p2, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v3, v3

    .line 2527932
    iget-wide v9, p2, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v4, v9

    .line 2527933
    invoke-direct/range {v1 .. v6}, Lcom/facebook/graphql/executor/GraphQLResult;-><init>(Ljava/lang/Object;LX/0ta;JLjava/util/Set;)V

    .line 2527934
    iget-object v2, p0, LX/I0o;->e:LX/1My;

    iget-object v3, p0, LX/I0o;->b:LX/0TF;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0, v1}, LX/1My;->a(LX/0TF;Ljava/lang/String;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 2527935
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    .line 2527936
    :cond_0
    return-void
.end method
