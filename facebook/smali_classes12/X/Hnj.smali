.class public LX/Hnj;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/0Zb;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2503517
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2503518
    iput-object p1, p0, LX/Hnj;->a:LX/0Zb;

    .line 2503519
    return-void
.end method

.method public static a(LX/0QB;)LX/Hnj;
    .locals 4

    .prologue
    .line 2503506
    const-class v1, LX/Hnj;

    monitor-enter v1

    .line 2503507
    :try_start_0
    sget-object v0, LX/Hnj;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2503508
    sput-object v2, LX/Hnj;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2503509
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2503510
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2503511
    new-instance p0, LX/Hnj;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/Hnj;-><init>(LX/0Zb;)V

    .line 2503512
    move-object v0, p0

    .line 2503513
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2503514
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Hnj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2503515
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2503516
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(LX/Hnj;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 3

    .prologue
    .line 2503502
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2503503
    const-string v1, "ref"

    iget-object v2, p0, LX/Hnj;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2503504
    const-string v1, "tab"

    iget-object v2, p0, LX/Hnj;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2503505
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2503496
    iput-object p1, p0, LX/Hnj;->b:Ljava/lang/String;

    .line 2503497
    iput-object p2, p0, LX/Hnj;->c:Ljava/lang/String;

    .line 2503498
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2503499
    const-string v0, "election_hub_initial_feed_fetch"

    invoke-static {p0, v0}, LX/Hnj;->b(LX/Hnj;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2503500
    iget-object v1, p0, LX/Hnj;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2503501
    return-void
.end method
