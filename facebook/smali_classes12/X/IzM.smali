.class public LX/IzM;
.super LX/2Iu;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2Iu",
        "<",
        "LX/IzK;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/IzM;


# direct methods
.method public constructor <init>(LX/IzO;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2634542
    const-string v0, "properties"

    invoke-direct {p0, p1, v0}, LX/2Iu;-><init>(LX/0QR;Ljava/lang/String;)V

    .line 2634543
    return-void
.end method

.method public static a(LX/0QB;)LX/IzM;
    .locals 4

    .prologue
    .line 2634544
    sget-object v0, LX/IzM;->c:LX/IzM;

    if-nez v0, :cond_1

    .line 2634545
    const-class v1, LX/IzM;

    monitor-enter v1

    .line 2634546
    :try_start_0
    sget-object v0, LX/IzM;->c:LX/IzM;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2634547
    if-eqz v2, :cond_0

    .line 2634548
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2634549
    new-instance p0, LX/IzM;

    invoke-static {v0}, LX/IzO;->a(LX/0QB;)LX/IzO;

    move-result-object v3

    check-cast v3, LX/IzO;

    invoke-direct {p0, v3}, LX/IzM;-><init>(LX/IzO;)V

    .line 2634550
    move-object v0, p0

    .line 2634551
    sput-object v0, LX/IzM;->c:LX/IzM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2634552
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2634553
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2634554
    :cond_1
    sget-object v0, LX/IzM;->c:LX/IzM;

    return-object v0

    .line 2634555
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2634556
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
