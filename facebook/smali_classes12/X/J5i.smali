.class public final enum LX/J5i;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/J5i;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/J5i;

.field public static final enum PHOTO_CHECKUP_BULK_EDIT_ACCEPTED:LX/J5i;

.field public static final enum PHOTO_CHECKUP_BULK_EDIT_REJECTED:LX/J5i;


# instance fields
.field public final eventName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2648598
    new-instance v0, LX/J5i;

    const-string v1, "PHOTO_CHECKUP_BULK_EDIT_ACCEPTED"

    const-string v2, "user_bulk_accepted"

    invoke-direct {v0, v1, v3, v2}, LX/J5i;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J5i;->PHOTO_CHECKUP_BULK_EDIT_ACCEPTED:LX/J5i;

    .line 2648599
    new-instance v0, LX/J5i;

    const-string v1, "PHOTO_CHECKUP_BULK_EDIT_REJECTED"

    const-string v2, "user_bulk_rejected"

    invoke-direct {v0, v1, v4, v2}, LX/J5i;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J5i;->PHOTO_CHECKUP_BULK_EDIT_REJECTED:LX/J5i;

    .line 2648600
    const/4 v0, 0x2

    new-array v0, v0, [LX/J5i;

    sget-object v1, LX/J5i;->PHOTO_CHECKUP_BULK_EDIT_ACCEPTED:LX/J5i;

    aput-object v1, v0, v3

    sget-object v1, LX/J5i;->PHOTO_CHECKUP_BULK_EDIT_REJECTED:LX/J5i;

    aput-object v1, v0, v4

    sput-object v0, LX/J5i;->$VALUES:[LX/J5i;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2648601
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2648602
    iput-object p3, p0, LX/J5i;->eventName:Ljava/lang/String;

    .line 2648603
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/J5i;
    .locals 1

    .prologue
    .line 2648604
    const-class v0, LX/J5i;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/J5i;

    return-object v0
.end method

.method public static values()[LX/J5i;
    .locals 1

    .prologue
    .line 2648605
    sget-object v0, LX/J5i;->$VALUES:[LX/J5i;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/J5i;

    return-object v0
.end method
