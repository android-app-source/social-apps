.class public LX/JLy;
.super LX/FUq;
.source ""


# instance fields
.field public a:LX/JLt;

.field public b:Z

.field public c:Z

.field private d:I

.field private final e:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(LX/5pX;)V
    .locals 1

    .prologue
    .line 2682774
    invoke-direct {p0, p1}, LX/FUq;-><init>(LX/5pX;)V

    .line 2682775
    new-instance v0, Lcom/facebook/fbreact/perf/ObservableReactScrollView$1;

    invoke-direct {v0, p0}, Lcom/facebook/fbreact/perf/ObservableReactScrollView$1;-><init>(LX/JLy;)V

    iput-object v0, p0, LX/JLy;->e:Ljava/lang/Runnable;

    .line 2682776
    return-void
.end method

.method public static setScrollState(LX/JLy;I)V
    .locals 2

    .prologue
    .line 2682777
    iget v0, p0, LX/JLy;->d:I

    if-eq p1, v0, :cond_0

    iget-object v0, p0, LX/JLy;->a:LX/JLt;

    if-eqz v0, :cond_0

    .line 2682778
    iget-object v0, p0, LX/JLy;->a:LX/JLt;

    .line 2682779
    packed-switch p1, :pswitch_data_0

    .line 2682780
    :cond_0
    :goto_0
    iput p1, p0, LX/JLy;->d:I

    .line 2682781
    return-void

    .line 2682782
    :pswitch_0
    iget-object v1, v0, LX/JLt;->a:Lcom/facebook/fbreact/perf/FrameLoggingReactScrollViewManager;

    iget-object v1, v1, Lcom/facebook/fbreact/perf/FrameLoggingReactScrollViewManager;->b:Lcom/facebook/common/perftest/DrawFrameLogger;

    invoke-static {v1}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/common/perftest/DrawFrameLogger;

    invoke-virtual {v1}, Lcom/facebook/common/perftest/DrawFrameLogger;->b()V

    goto :goto_0

    .line 2682783
    :pswitch_1
    iget-object v1, v0, LX/JLt;->a:Lcom/facebook/fbreact/perf/FrameLoggingReactScrollViewManager;

    iget-object v1, v1, Lcom/facebook/fbreact/perf/FrameLoggingReactScrollViewManager;->b:Lcom/facebook/common/perftest/DrawFrameLogger;

    invoke-static {v1}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/common/perftest/DrawFrameLogger;

    invoke-virtual {v1}, Lcom/facebook/common/perftest/DrawFrameLogger;->a()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final onScrollChanged(IIII)V
    .locals 4

    .prologue
    .line 2682784
    invoke-super {p0, p1, p2, p3, p4}, LX/FUq;->onScrollChanged(IIII)V

    .line 2682785
    sub-int v0, p4, p2

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    if-lez v0, :cond_1

    .line 2682786
    iget-object v0, p0, LX/JLy;->e:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, LX/JLy;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 2682787
    iget-boolean v0, p0, LX/JLy;->c:Z

    if-eqz v0, :cond_2

    .line 2682788
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/JLy;->setScrollState(LX/JLy;I)V

    .line 2682789
    :cond_0
    :goto_0
    iget-object v0, p0, LX/JLy;->e:Ljava/lang/Runnable;

    const-wide/16 v2, 0x28

    invoke-virtual {p0, v0, v2, v3}, LX/JLy;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2682790
    :cond_1
    return-void

    .line 2682791
    :cond_2
    iget-object v0, p0, LX/JLy;->a:LX/JLt;

    if-eqz v0, :cond_0

    .line 2682792
    const/4 v0, 0x2

    invoke-static {p0, v0}, LX/JLy;->setScrollState(LX/JLy;I)V

    goto :goto_0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    const v0, -0x4ab5261e

    invoke-static {v4, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2682793
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    .line 2682794
    if-ne v1, v4, :cond_1

    .line 2682795
    iput-boolean v2, p0, LX/JLy;->c:Z

    .line 2682796
    iput-boolean v2, p0, LX/JLy;->b:Z

    .line 2682797
    :cond_0
    :goto_0
    invoke-super {p0, p1}, LX/FUq;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    const v2, 0x38f5a685

    invoke-static {v2, v0}, LX/02F;->a(II)V

    return v1

    .line 2682798
    :cond_1
    if-ne v1, v2, :cond_0

    .line 2682799
    iget-boolean v1, p0, LX/JLy;->c:Z

    if-eqz v1, :cond_2

    iget-boolean v1, p0, LX/JLy;->b:Z

    if-nez v1, :cond_2

    .line 2682800
    invoke-static {p0, v3}, LX/JLy;->setScrollState(LX/JLy;I)V

    .line 2682801
    :cond_2
    iput-boolean v3, p0, LX/JLy;->c:Z

    goto :goto_0
.end method

.method public setOnScrollListener(LX/JLt;)V
    .locals 0

    .prologue
    .line 2682802
    iput-object p1, p0, LX/JLy;->a:LX/JLt;

    .line 2682803
    return-void
.end method
