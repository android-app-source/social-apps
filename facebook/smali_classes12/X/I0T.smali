.class public LX/I0T;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/events/common/EventAnalyticsParams;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2527063
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2527064
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2527065
    iput-object v0, p0, LX/I0T;->a:LX/0Px;

    .line 2527066
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 4

    .prologue
    .line 2527067
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2527068
    new-instance v1, LX/I0S;

    const v2, 0x7f030547

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-direct {v1, v0}, LX/I0S;-><init>(Landroid/view/View;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 3

    .prologue
    .line 2527069
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;

    iget-object v1, p0, LX/I0T;->a:LX/0Px;

    invoke-virtual {v1, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7oa;

    iget-object v2, p0, LX/I0T;->b:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/events/dashboard/calendar/EventsCalendarDashboardHScrollUnitView;->a(LX/7oa;Lcom/facebook/events/common/EventAnalyticsParams;)V

    .line 2527070
    return-void
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2527071
    iget-object v0, p0, LX/I0T;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
