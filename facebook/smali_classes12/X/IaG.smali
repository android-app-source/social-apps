.class public final LX/IaG;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;I)V
    .locals 0

    .prologue
    .line 2590739
    iput-object p1, p0, LX/IaG;->b:Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;

    iput p2, p0, LX/IaG;->a:I

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2590740
    iget-object v0, p0, LX/IaG;->b:Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;

    iget-object v0, v0, Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;->e:LX/IZw;

    const-string v1, "click_resend_code_button"

    invoke-virtual {v0, v1}, LX/IZw;->a(Ljava/lang/String;)V

    .line 2590741
    iget-object v0, p0, LX/IaG;->b:Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;

    .line 2590742
    iget-object v1, v0, Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;->j:LX/4At;

    if-nez v1, :cond_0

    .line 2590743
    new-instance v1, LX/4At;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const p1, 0x7f083a43

    invoke-direct {v1, v2, p1}, LX/4At;-><init>(Landroid/content/Context;I)V

    iput-object v1, v0, Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;->j:LX/4At;

    .line 2590744
    :cond_0
    iget-object v1, v0, Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;->j:LX/4At;

    invoke-virtual {v1}, LX/4At;->beginShowingProgress()V

    .line 2590745
    iget-object v0, p0, LX/IaG;->b:Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;

    iget-object v0, v0, Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;->b:LX/Ia3;

    iget-object v1, p0, LX/IaG;->b:Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;

    iget-object v1, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessConfirmCodeFragment;->m:Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$PhoneNumberInfoModel;

    invoke-virtual {v1}, Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$PhoneNumberInfoModel;->b()Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/IaF;

    invoke-direct {v2, p0}, LX/IaF;-><init>(LX/IaG;)V

    invoke-virtual {v0, v1, v2}, LX/Ia3;->a(Ljava/lang/String;LX/Ia2;)V

    .line 2590746
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 1

    .prologue
    .line 2590747
    iget v0, p0, LX/IaG;->a:I

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 2590748
    return-void
.end method
