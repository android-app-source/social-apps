.class public LX/Hu2;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "Lcom/facebook/ipc/composer/model/ComposerDateInfo$ProvidesDateInfo;",
        "PluginSession::",
        "LX/0il",
        "<TModelData;>;>",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/uicontrib/datepicker/Date;


# instance fields
.field public final b:Landroid/content/Context;

.field private final c:LX/0il;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TPluginSession;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/composer/lifeevent/view/ComposerLifeEventWithDatePickerView;

.field public final e:LX/11S;

.field private f:Lcom/facebook/uicontrib/datepicker/Date;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2517060
    new-instance v0, LX/5zw;

    invoke-direct {v0}, LX/5zw;-><init>()V

    invoke-virtual {v0}, LX/5zw;->a()Lcom/facebook/uicontrib/datepicker/Date;

    move-result-object v0

    sput-object v0, LX/Hu2;->a:Lcom/facebook/uicontrib/datepicker/Date;

    return-void
.end method

.method public constructor <init>(LX/0il;LX/JDf;Lcom/facebook/composer/lifeevent/view/ComposerLifeEventWithDatePickerView;Landroid/content/Context;LX/11S;)V
    .locals 2
    .param p1    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/JDf;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/composer/lifeevent/view/ComposerLifeEventWithDatePickerView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TPluginSession;",
            "Lcom/facebook/composer/lifeevent/controller/ComposerLifeEventDatePickerController$DatePickerListener;",
            "Lcom/facebook/composer/lifeevent/view/ComposerLifeEventWithDatePickerView;",
            "Landroid/content/Context;",
            "LX/11S;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2517061
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2517062
    sget-object v0, LX/Hu2;->a:Lcom/facebook/uicontrib/datepicker/Date;

    iput-object v0, p0, LX/Hu2;->f:Lcom/facebook/uicontrib/datepicker/Date;

    .line 2517063
    iput-object p4, p0, LX/Hu2;->b:Landroid/content/Context;

    .line 2517064
    iput-object p5, p0, LX/Hu2;->e:LX/11S;

    .line 2517065
    iput-object p3, p0, LX/Hu2;->d:Lcom/facebook/composer/lifeevent/view/ComposerLifeEventWithDatePickerView;

    .line 2517066
    iput-object p1, p0, LX/Hu2;->c:LX/0il;

    .line 2517067
    iget-object v0, p0, LX/Hu2;->d:Lcom/facebook/composer/lifeevent/view/ComposerLifeEventWithDatePickerView;

    new-instance v1, LX/Hu1;

    invoke-direct {v1, p0, p2}, LX/Hu1;-><init>(LX/Hu2;LX/JDf;)V

    invoke-virtual {v0, v1}, Lcom/facebook/composer/lifeevent/view/ComposerLifeEventWithDatePickerView;->setDatePickerClickListener(Landroid/view/View$OnClickListener;)V

    .line 2517068
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    .line 2517041
    iget-object v1, p0, LX/Hu2;->f:Lcom/facebook/uicontrib/datepicker/Date;

    iget-object v0, p0, LX/Hu2;->c:LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getDateInfo()Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerDateInfo;->a()Lcom/facebook/uicontrib/datepicker/Date;

    move-result-object v0

    if-ne v1, v0, :cond_0

    .line 2517042
    :goto_0
    return-void

    .line 2517043
    :cond_0
    iget-object v0, p0, LX/Hu2;->c:LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getDateInfo()Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerDateInfo;->a()Lcom/facebook/uicontrib/datepicker/Date;

    move-result-object v0

    iput-object v0, p0, LX/Hu2;->f:Lcom/facebook/uicontrib/datepicker/Date;

    .line 2517044
    iget-object v0, p0, LX/Hu2;->f:Lcom/facebook/uicontrib/datepicker/Date;

    if-nez v0, :cond_1

    .line 2517045
    sget-object v0, Lcom/facebook/uicontrib/datepicker/Date;->a:Lcom/facebook/uicontrib/datepicker/Date;

    iput-object v0, p0, LX/Hu2;->f:Lcom/facebook/uicontrib/datepicker/Date;

    .line 2517046
    :cond_1
    iget-object v0, p0, LX/Hu2;->f:Lcom/facebook/uicontrib/datepicker/Date;

    invoke-virtual {v0}, Lcom/facebook/uicontrib/datepicker/Date;->b()Ljava/lang/Integer;

    move-result-object v0

    if-nez v0, :cond_2

    .line 2517047
    iget-object v0, p0, LX/Hu2;->f:Lcom/facebook/uicontrib/datepicker/Date;

    invoke-virtual {v0}, Lcom/facebook/uicontrib/datepicker/Date;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 2517048
    :goto_1
    iget-object v1, p0, LX/Hu2;->d:Lcom/facebook/composer/lifeevent/view/ComposerLifeEventWithDatePickerView;

    invoke-virtual {v1, v0}, Lcom/facebook/composer/lifeevent/view/ComposerLifeEventWithDatePickerView;->setDateLabel(Ljava/lang/String;)V

    goto :goto_0

    .line 2517049
    :cond_2
    iget-object v0, p0, LX/Hu2;->f:Lcom/facebook/uicontrib/datepicker/Date;

    invoke-virtual {v0}, Lcom/facebook/uicontrib/datepicker/Date;->c()Ljava/lang/Integer;

    move-result-object v0

    if-nez v0, :cond_3

    .line 2517050
    iget-object v0, p0, LX/Hu2;->f:Lcom/facebook/uicontrib/datepicker/Date;

    .line 2517051
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 2517052
    invoke-virtual {v0}, Lcom/facebook/uicontrib/datepicker/Date;->a()I

    move-result v3

    invoke-virtual {v0}, Lcom/facebook/uicontrib/datepicker/Date;->b()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    const/4 v5, 0x1

    invoke-virtual {v2, v3, v4, v5}, Ljava/util/Calendar;->set(III)V

    .line 2517053
    iget-object v3, p0, LX/Hu2;->b:Landroid/content/Context;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    const/16 v2, 0x20

    invoke-static {v3, v4, v5, v2}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v2

    move-object v0, v2

    .line 2517054
    goto :goto_1

    .line 2517055
    :cond_3
    iget-object v0, p0, LX/Hu2;->f:Lcom/facebook/uicontrib/datepicker/Date;

    .line 2517056
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 2517057
    invoke-virtual {v0}, Lcom/facebook/uicontrib/datepicker/Date;->a()I

    move-result v3

    invoke-virtual {v0}, Lcom/facebook/uicontrib/datepicker/Date;->b()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v0}, Lcom/facebook/uicontrib/datepicker/Date;->c()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v2, v3, v4, v5}, Ljava/util/Calendar;->set(III)V

    .line 2517058
    iget-object v3, p0, LX/Hu2;->e:LX/11S;

    sget-object v4, LX/1lB;->EVENTS_RELATIVE_DATE_STYLE:LX/1lB;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    invoke-interface {v3, v4, v6, v7}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v2

    move-object v0, v2

    .line 2517059
    goto :goto_1
.end method
