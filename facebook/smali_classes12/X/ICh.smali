.class public final LX/ICh;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/ICn;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;)V
    .locals 0

    .prologue
    .line 2549648
    iput-object p1, p0, LX/ICh;->a:Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2549632
    iget-object v0, p0, LX/ICh;->a:Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;

    iget-object v0, v0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->s:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2549633
    iget-object v0, p0, LX/ICh;->a:Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;

    iget-object v0, v0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->s:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    new-instance v1, LX/ICg;

    invoke-direct {v1, p0}, LX/ICg;-><init>(LX/ICh;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2549634
    :cond_0
    iget-object v0, p0, LX/ICh;->a:Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;

    iget-object v0, v0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->s:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2549635
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 2549636
    check-cast p1, LX/ICn;

    .line 2549637
    iget-object v0, p0, LX/ICh;->a:Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;

    iget-object v0, v0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->r:LX/ICw;

    .line 2549638
    iget-object v1, p1, LX/ICn;->a:LX/0Px;

    move-object v1, v1

    .line 2549639
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/ICk;

    .line 2549640
    invoke-virtual {v2}, LX/ICk;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v4, v5, :cond_0

    iget-object v4, v0, LX/ICw;->c:Ljava/util/Map;

    invoke-virtual {v2}, LX/ICk;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2549641
    iget-object v4, v0, LX/ICw;->b:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2549642
    iget-object v4, v0, LX/ICw;->c:Ljava/util/Map;

    invoke-virtual {v2}, LX/ICk;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v5, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2549643
    :cond_1
    const v2, 0x1c8dc63d

    invoke-static {v0, v2}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2549644
    iget-object v0, p0, LX/ICh;->a:Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;

    .line 2549645
    iget-object v1, p1, LX/ICn;->b:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-object v1, v1

    .line 2549646
    iput-object v1, v0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 2549647
    return-void
.end method
