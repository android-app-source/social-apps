.class public LX/ILF;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0tX;

.field public final b:LX/0TD;

.field public final c:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(LX/0tX;Landroid/content/res/Resources;LX/0TD;)V
    .locals 0
    .param p3    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2567766
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2567767
    iput-object p1, p0, LX/ILF;->a:LX/0tX;

    .line 2567768
    iput-object p2, p0, LX/ILF;->c:Landroid/content/res/Resources;

    .line 2567769
    iput-object p3, p0, LX/ILF;->b:LX/0TD;

    .line 2567770
    return-void
.end method

.method public static b(LX/0QB;)LX/ILF;
    .locals 4

    .prologue
    .line 2567771
    new-instance v3, LX/ILF;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v2

    check-cast v2, LX/0TD;

    invoke-direct {v3, v0, v1, v2}, LX/ILF;-><init>(LX/0tX;Landroid/content/res/Resources;LX/0TD;)V

    .line 2567772
    return-object v3
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;I)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groups/community/protocol/FetchCommunityForumGroupsGraphQLModels$FetchCommunityForumGroupsModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2567773
    iget-object v1, p0, LX/ILF;->a:LX/0tX;

    .line 2567774
    new-instance v0, LX/IMJ;

    invoke-direct {v0}, LX/IMJ;-><init>()V

    move-object v0, v0

    .line 2567775
    const-string v2, "group_id"

    invoke-virtual {v0, v2, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "after_cursor"

    invoke-virtual {v0, v2, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "first_count"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v2, "cover_photo_height"

    iget-object v3, p0, LX/ILF;->c:Landroid/content/res/Resources;

    const v4, 0x7f0b008b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v2, "cover_photo_width"

    iget-object v3, p0, LX/ILF;->c:Landroid/content/res/Resources;

    const v4, 0x7f0b008b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/IMJ;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0
.end method
