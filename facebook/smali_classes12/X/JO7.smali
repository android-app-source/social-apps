.class public LX/JO7;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JO9;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JO7",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JO9;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2686911
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2686912
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JO7;->b:LX/0Zi;

    .line 2686913
    iput-object p1, p0, LX/JO7;->a:LX/0Ot;

    .line 2686914
    return-void
.end method

.method public static a(LX/0QB;)LX/JO7;
    .locals 4

    .prologue
    .line 2686915
    const-class v1, LX/JO7;

    monitor-enter v1

    .line 2686916
    :try_start_0
    sget-object v0, LX/JO7;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2686917
    sput-object v2, LX/JO7;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2686918
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2686919
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2686920
    new-instance v3, LX/JO7;

    const/16 p0, 0x1f47

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JO7;-><init>(LX/0Ot;)V

    .line 2686921
    move-object v0, v3

    .line 2686922
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2686923
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JO7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2686924
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2686925
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 2686926
    check-cast p2, LX/JO6;

    .line 2686927
    iget-object v0, p0, LX/JO7;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JO9;

    iget-object v2, p2, LX/JO6;->a:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;

    iget-object v3, p2, LX/JO6;->b:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;

    iget-object v4, p2, LX/JO6;->c:LX/3mj;

    iget-object v5, p2, LX/JO6;->d:LX/1Pb;

    move-object v1, p1

    .line 2686928
    iget-object v6, v0, LX/JO9;->d:LX/1DR;

    invoke-virtual {v6}, LX/1DR;->a()I

    move-result v6

    add-int/lit8 v6, v6, -0xc

    .line 2686929
    const v7, 0x3f570a3d    # 0.84f

    int-to-float v6, v6

    mul-float/2addr v6, v7

    const/high16 v7, 0x41a00000    # 20.0f

    sub-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    .line 2686930
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v7

    const/4 v8, 0x0

    invoke-interface {v7, v8}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v7

    const/4 v8, 0x2

    invoke-interface {v7, v8}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v7

    const v8, 0x7f020329

    invoke-interface {v7, v8}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v7

    const/16 v8, 0x8

    const p0, 0x7f0b254e

    invoke-interface {v7, v8, p0}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v7

    iget-object v8, v0, LX/JO9;->a:LX/JNp;

    const/4 p0, 0x0

    .line 2686931
    new-instance p1, LX/JNo;

    invoke-direct {p1, v8}, LX/JNo;-><init>(LX/JNp;)V

    .line 2686932
    iget-object p2, v8, LX/JNp;->b:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/JNn;

    .line 2686933
    if-nez p2, :cond_0

    .line 2686934
    new-instance p2, LX/JNn;

    invoke-direct {p2, v8}, LX/JNn;-><init>(LX/JNp;)V

    .line 2686935
    :cond_0
    invoke-static {p2, v1, p0, p0, p1}, LX/JNn;->a$redex0(LX/JNn;LX/1De;IILX/JNo;)V

    .line 2686936
    move-object p1, p2

    .line 2686937
    move-object p0, p1

    .line 2686938
    move-object v8, p0

    .line 2686939
    iget-object p0, v8, LX/JNn;->a:LX/JNo;

    iput-object v2, p0, LX/JNo;->c:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;

    .line 2686940
    iget-object p0, v8, LX/JNn;->e:Ljava/util/BitSet;

    const/4 p1, 0x2

    invoke-virtual {p0, p1}, Ljava/util/BitSet;->set(I)V

    .line 2686941
    move-object v8, v8

    .line 2686942
    iget-object p0, v8, LX/JNn;->a:LX/JNo;

    iput-object v3, p0, LX/JNo;->a:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;

    .line 2686943
    iget-object p0, v8, LX/JNn;->e:Ljava/util/BitSet;

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Ljava/util/BitSet;->set(I)V

    .line 2686944
    move-object v8, v8

    .line 2686945
    iget-object p0, v8, LX/JNn;->a:LX/JNo;

    iput v6, p0, LX/JNo;->b:I

    .line 2686946
    iget-object p0, v8, LX/JNn;->e:Ljava/util/BitSet;

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Ljava/util/BitSet;->set(I)V

    .line 2686947
    move-object v8, v8

    .line 2686948
    invoke-interface {v7, v8}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v7

    iget-object v8, v0, LX/JO9;->c:LX/JNe;

    const/4 p0, 0x0

    .line 2686949
    new-instance p1, LX/JNd;

    invoke-direct {p1, v8}, LX/JNd;-><init>(LX/JNe;)V

    .line 2686950
    iget-object p2, v8, LX/JNe;->b:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/JNc;

    .line 2686951
    if-nez p2, :cond_1

    .line 2686952
    new-instance p2, LX/JNc;

    invoke-direct {p2, v8}, LX/JNc;-><init>(LX/JNe;)V

    .line 2686953
    :cond_1
    invoke-static {p2, v1, p0, p0, p1}, LX/JNc;->a$redex0(LX/JNc;LX/1De;IILX/JNd;)V

    .line 2686954
    move-object p1, p2

    .line 2686955
    move-object p0, p1

    .line 2686956
    move-object v8, p0

    .line 2686957
    iget-object p0, v8, LX/JNc;->a:LX/JNd;

    iput-object v2, p0, LX/JNd;->d:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;

    .line 2686958
    iget-object p0, v8, LX/JNc;->e:Ljava/util/BitSet;

    const/4 p1, 0x3

    invoke-virtual {p0, p1}, Ljava/util/BitSet;->set(I)V

    .line 2686959
    move-object v8, v8

    .line 2686960
    iget-object p0, v8, LX/JNc;->a:LX/JNd;

    iput-object v3, p0, LX/JNd;->a:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;

    .line 2686961
    iget-object p0, v8, LX/JNc;->e:Ljava/util/BitSet;

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Ljava/util/BitSet;->set(I)V

    .line 2686962
    move-object v8, v8

    .line 2686963
    iget-object p0, v8, LX/JNc;->a:LX/JNd;

    iput-object v5, p0, LX/JNd;->b:LX/1Pb;

    .line 2686964
    iget-object p0, v8, LX/JNc;->e:Ljava/util/BitSet;

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Ljava/util/BitSet;->set(I)V

    .line 2686965
    move-object v8, v8

    .line 2686966
    iget-object p0, v8, LX/JNc;->a:LX/JNd;

    iput v6, p0, LX/JNd;->c:I

    .line 2686967
    iget-object p0, v8, LX/JNc;->e:Ljava/util/BitSet;

    const/4 p1, 0x2

    invoke-virtual {p0, p1}, Ljava/util/BitSet;->set(I)V

    .line 2686968
    move-object v8, v8

    .line 2686969
    iget-object p0, v8, LX/JNc;->a:LX/JNd;

    iput-object v4, p0, LX/JNd;->e:LX/3mj;

    .line 2686970
    iget-object p0, v8, LX/JNc;->e:Ljava/util/BitSet;

    const/4 p1, 0x4

    invoke-virtual {p0, p1}, Ljava/util/BitSet;->set(I)V

    .line 2686971
    move-object v8, v8

    .line 2686972
    invoke-interface {v7, v8}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v7

    iget-object v8, v0, LX/JO9;->b:LX/JOC;

    const/4 p0, 0x0

    .line 2686973
    new-instance p1, LX/JOB;

    invoke-direct {p1, v8}, LX/JOB;-><init>(LX/JOC;)V

    .line 2686974
    iget-object p2, v8, LX/JOC;->b:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/JOA;

    .line 2686975
    if-nez p2, :cond_2

    .line 2686976
    new-instance p2, LX/JOA;

    invoke-direct {p2, v8}, LX/JOA;-><init>(LX/JOC;)V

    .line 2686977
    :cond_2
    invoke-static {p2, v1, p0, p0, p1}, LX/JOA;->a$redex0(LX/JOA;LX/1De;IILX/JOB;)V

    .line 2686978
    move-object p1, p2

    .line 2686979
    move-object p0, p1

    .line 2686980
    move-object v8, p0

    .line 2686981
    iget-object p0, v8, LX/JOA;->a:LX/JOB;

    iput-object v2, p0, LX/JOB;->c:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnit;

    .line 2686982
    iget-object p0, v8, LX/JOA;->e:Ljava/util/BitSet;

    const/4 p1, 0x2

    invoke-virtual {p0, p1}, Ljava/util/BitSet;->set(I)V

    .line 2686983
    move-object v8, v8

    .line 2686984
    iget-object p0, v8, LX/JOA;->a:LX/JOB;

    iput-object v3, p0, LX/JOB;->a:Lcom/facebook/graphql/model/GraphQLConnectWithFacebookFamilyFeedUnitItem;

    .line 2686985
    iget-object p0, v8, LX/JOA;->e:Ljava/util/BitSet;

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Ljava/util/BitSet;->set(I)V

    .line 2686986
    move-object v8, v8

    .line 2686987
    iget-object p0, v8, LX/JOA;->a:LX/JOB;

    iput v6, p0, LX/JOB;->b:I

    .line 2686988
    iget-object p0, v8, LX/JOA;->e:Ljava/util/BitSet;

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Ljava/util/BitSet;->set(I)V

    .line 2686989
    move-object v6, v8

    .line 2686990
    invoke-interface {v7, v6}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6}, LX/1Di;->k()LX/1Dg;

    move-result-object v6

    move-object v0, v6

    .line 2686991
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2686992
    invoke-static {}, LX/1dS;->b()V

    .line 2686993
    const/4 v0, 0x0

    return-object v0
.end method
