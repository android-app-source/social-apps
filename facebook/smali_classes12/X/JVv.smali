.class public LX/JVv;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JVw;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JVv",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JVw;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2701788
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2701789
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JVv;->b:LX/0Zi;

    .line 2701790
    iput-object p1, p0, LX/JVv;->a:LX/0Ot;

    .line 2701791
    return-void
.end method

.method public static a(LX/0QB;)LX/JVv;
    .locals 4

    .prologue
    .line 2701792
    const-class v1, LX/JVv;

    monitor-enter v1

    .line 2701793
    :try_start_0
    sget-object v0, LX/JVv;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2701794
    sput-object v2, LX/JVv;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2701795
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2701796
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2701797
    new-instance v3, LX/JVv;

    const/16 p0, 0x20b5

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JVv;-><init>(LX/0Ot;)V

    .line 2701798
    move-object v0, v3

    .line 2701799
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2701800
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JVv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2701801
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2701802
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2701803
    const v0, -0x199987fe

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 5

    .prologue
    .line 2701804
    check-cast p2, LX/JVu;

    .line 2701805
    iget-object v0, p0, LX/JVv;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p2, LX/JVu;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 p2, 0x2

    const/high16 p0, 0x3f800000    # 1.0f

    const/4 v4, 0x1

    .line 2701806
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 2701807
    check-cast v1, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;

    .line 2701808
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v4}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0a0048

    invoke-interface {v2, v3}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPageStoriesYouMissedFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, v4}, LX/1ne;->t(I)LX/1ne;

    move-result-object v1

    const v3, 0x7f0a015d

    invoke-virtual {v1, v3}, LX/1ne;->n(I)LX/1ne;

    move-result-object v1

    const v3, 0x7f0b0050

    invoke-virtual {v1, v3}, LX/1ne;->q(I)LX/1ne;

    move-result-object v1

    const v3, 0x7f0b1063

    invoke-virtual {v1, v3}, LX/1ne;->s(I)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, p0}, LX/1ne;->j(F)LX/1ne;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    invoke-interface {v1, p0}, LX/1Di;->b(F)LX/1Di;

    move-result-object v1

    const v3, 0x7f0b08fe

    invoke-interface {v1, v4, v3}, LX/1Di;->c(II)LX/1Di;

    move-result-object v1

    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v2

    const v3, 0x7f020aea

    invoke-virtual {v2, v3}, LX/1o5;->h(I)LX/1o5;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    .line 2701809
    const v3, -0x199987fe

    const/4 p0, 0x0

    invoke-static {p1, v3, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v3

    move-object v3, v3

    .line 2701810
    invoke-interface {v2, v3}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v2

    const v3, 0x7f0b00ed

    invoke-interface {v2, v4, v3}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    const v3, 0x7f0b00ea

    invoke-interface {v2, p2, v3}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    const/4 v3, 0x3

    const v4, 0x7f0b00ee

    invoke-interface {v2, v3, v4}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    const/4 v3, 0x0

    const v4, 0x7f0b00eb

    invoke-interface {v2, v3, v4}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    const v3, 0x7f0b00e3

    invoke-interface {v2, v3}, LX/1Di;->i(I)LX/1Di;

    move-result-object v2

    const v3, 0x7f0b00e9

    invoke-interface {v2, v3}, LX/1Di;->q(I)LX/1Di;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 2701811
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2701812
    invoke-static {}, LX/1dS;->b()V

    .line 2701813
    iget v0, p1, LX/1dQ;->b:I

    .line 2701814
    packed-switch v0, :pswitch_data_0

    .line 2701815
    :goto_0
    return-object v2

    .line 2701816
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2701817
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2701818
    check-cast v1, LX/JVu;

    .line 2701819
    iget-object v3, p0, LX/JVv;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/JVw;

    iget-object p1, v1, LX/JVu;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object p2, v1, LX/JVu;->b:LX/1Pn;

    .line 2701820
    check-cast p2, LX/1Pk;

    invoke-interface {p2}, LX/1Pk;->e()LX/1SX;

    move-result-object p0

    invoke-virtual {p0, p1, v0}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V

    .line 2701821
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x199987fe
        :pswitch_0
    .end packed-switch
.end method
