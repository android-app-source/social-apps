.class public LX/ITJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/ITA;


# static fields
.field public static final a:Ljava/lang/Class;


# instance fields
.field public final b:Ljava/lang/Boolean;

.field public c:LX/IRb;

.field public final d:LX/3mF;

.field public final e:LX/17W;

.field public final f:Lcom/facebook/content/SecureContextHelper;

.field private final g:LX/0Zb;

.field public final h:Lcom/facebook/common/shortcuts/InstallShortcutHelper;

.field public final i:LX/0kL;

.field private final j:LX/0Sh;

.field public final k:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field public final l:Lcom/facebook/bookmark/client/BookmarkClient;

.field private final m:LX/1Ck;

.field private final n:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field public final o:LX/03V;

.field public final p:Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;

.field public final q:LX/DVF;

.field public final r:LX/DQW;

.field public final s:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DPT;",
            ">;"
        }
    .end annotation
.end field

.field public final t:LX/DK3;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2579234
    const-class v0, LX/ITJ;

    sput-object v0, LX/ITJ;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/IRb;LX/17W;Lcom/facebook/content/SecureContextHelper;LX/0Zb;Lcom/facebook/common/shortcuts/InstallShortcutHelper;LX/0kL;LX/3mF;LX/0Or;LX/0Sh;Lcom/facebook/bookmark/client/BookmarkClient;LX/1Ck;LX/0Or;LX/03V;Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;LX/DVF;LX/DQW;LX/0Ot;LX/DK3;Ljava/lang/Boolean;)V
    .locals 1
    .param p1    # LX/IRb;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .param p19    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/IRb;",
            "LX/17W;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0Zb;",
            "Lcom/facebook/common/shortcuts/InstallShortcutHelper;",
            "LX/0kL;",
            "LX/3mF;",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "Lcom/facebook/bookmark/client/BookmarkClient;",
            "LX/1Ck;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;",
            "LX/DVF;",
            "LX/DQW;",
            "LX/0Ot",
            "<",
            "LX/DPT;",
            ">;",
            "LX/DK3;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2579213
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2579214
    iput-object p1, p0, LX/ITJ;->c:LX/IRb;

    .line 2579215
    iput-object p2, p0, LX/ITJ;->e:LX/17W;

    .line 2579216
    iput-object p3, p0, LX/ITJ;->f:Lcom/facebook/content/SecureContextHelper;

    .line 2579217
    iput-object p4, p0, LX/ITJ;->g:LX/0Zb;

    .line 2579218
    iput-object p5, p0, LX/ITJ;->h:Lcom/facebook/common/shortcuts/InstallShortcutHelper;

    .line 2579219
    iput-object p6, p0, LX/ITJ;->i:LX/0kL;

    .line 2579220
    iput-object p7, p0, LX/ITJ;->d:LX/3mF;

    .line 2579221
    iput-object p9, p0, LX/ITJ;->j:LX/0Sh;

    .line 2579222
    iput-object p8, p0, LX/ITJ;->k:LX/0Or;

    .line 2579223
    iput-object p10, p0, LX/ITJ;->l:Lcom/facebook/bookmark/client/BookmarkClient;

    .line 2579224
    iput-object p11, p0, LX/ITJ;->m:LX/1Ck;

    .line 2579225
    iput-object p12, p0, LX/ITJ;->n:LX/0Or;

    .line 2579226
    iput-object p13, p0, LX/ITJ;->o:LX/03V;

    .line 2579227
    iput-object p14, p0, LX/ITJ;->p:Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;

    .line 2579228
    move-object/from16 v0, p15

    iput-object v0, p0, LX/ITJ;->q:LX/DVF;

    .line 2579229
    move-object/from16 v0, p16

    iput-object v0, p0, LX/ITJ;->r:LX/DQW;

    .line 2579230
    move-object/from16 v0, p19

    iput-object v0, p0, LX/ITJ;->b:Ljava/lang/Boolean;

    .line 2579231
    move-object/from16 v0, p18

    iput-object v0, p0, LX/ITJ;->t:LX/DK3;

    .line 2579232
    move-object/from16 v0, p17

    iput-object v0, p0, LX/ITJ;->s:LX/0Ot;

    .line 2579233
    return-void
.end method

.method public static a(LX/ITJ;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2579111
    iget-object v0, p0, LX/ITJ;->g:LX/0Zb;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 2579112
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2579113
    const-string v1, "group_header"

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2579114
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 2579115
    :cond_0
    return-void
.end method

.method private f(LX/9N6;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 13

    .prologue
    const/4 v6, 0x0

    .line 2579190
    const/4 v0, 0x0

    .line 2579191
    iget-object v1, p0, LX/ITJ;->l:Lcom/facebook/bookmark/client/BookmarkClient;

    invoke-virtual {v1}, Lcom/facebook/bookmark/client/BookmarkClient;->g()Z

    move-result v1

    .line 2579192
    if-eqz v1, :cond_0

    .line 2579193
    invoke-interface {p1}, LX/9N1;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2579194
    invoke-interface {p1}, LX/9N6;->y()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->k()LX/1vs;

    move-result-object v0

    iget-object v7, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2579195
    iget-object v8, p0, LX/ITJ;->l:Lcom/facebook/bookmark/client/BookmarkClient;

    new-instance v1, LX/2lK;

    invoke-interface {p1}, LX/9N6;->y()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v4

    invoke-interface {v4}, LX/9Mh;->d()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1}, LX/9N6;->y()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->F()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v1 .. v6}, LX/2lK;-><init>(JLjava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v7, v0, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 2579196
    iput-object v0, v1, LX/2lK;->f:Ljava/lang/String;

    .line 2579197
    move-object v0, v1

    .line 2579198
    invoke-virtual {v0}, LX/2lK;->a()Lcom/facebook/bookmark/model/Bookmark;

    move-result-object v0

    const/4 v9, 0x0

    .line 2579199
    const/4 v10, 0x1

    invoke-static {v8, v10}, Lcom/facebook/bookmark/client/BookmarkClient;->a(Lcom/facebook/bookmark/client/BookmarkClient;Z)Lcom/facebook/bookmark/model/BookmarksGroup;

    move-result-object v10

    .line 2579200
    if-nez v10, :cond_2

    .line 2579201
    sget-object v10, Lcom/facebook/bookmark/client/BookmarkClient;->a:Ljava/lang/Class;

    const-string v11, "No favorite group in BookmarkClient"

    invoke-static {v10, v11}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2579202
    :goto_0
    move-object v0, v9

    .line 2579203
    :cond_0
    if-nez v0, :cond_1

    .line 2579204
    iget-object v0, p0, LX/ITJ;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DPT;

    invoke-interface {p1}, LX/9N1;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/DPT;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2579205
    :cond_1
    return-object v0

    .line 2579206
    :cond_2
    iget-wide v11, v0, Lcom/facebook/bookmark/model/Bookmark;->id:J

    invoke-virtual {v10, v11, v12}, Lcom/facebook/bookmark/model/BookmarksGroup;->b(J)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 2579207
    sget-object v10, Lcom/facebook/bookmark/client/BookmarkClient;->a:Ljava/lang/Class;

    const-string v11, "Shouldn\'t see add to favorites when group is already in favorites"

    invoke-static {v10, v11}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0

    .line 2579208
    :cond_3
    invoke-virtual {v10, v0}, Lcom/facebook/bookmark/model/BookmarksGroup;->a(Lcom/facebook/bookmark/model/Bookmark;)V

    .line 2579209
    iget-object v9, v8, Lcom/facebook/bookmark/client/BookmarkClient;->i:Ljava/util/Set;

    iget-wide v11, v0, Lcom/facebook/bookmark/model/Bookmark;->id:J

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-interface {v9, v11}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2579210
    const/4 v9, 0x0

    invoke-static {v8, v10, v9}, Lcom/facebook/bookmark/client/BookmarkClient;->a(Lcom/facebook/bookmark/client/BookmarkClient;Lcom/facebook/bookmark/model/BookmarksGroup;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v10

    .line 2579211
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v9

    .line 2579212
    new-instance v11, LX/EgO;

    invoke-direct {v11, v8, v9, v0}, LX/EgO;-><init>(Lcom/facebook/bookmark/client/BookmarkClient;Lcom/google/common/util/concurrent/SettableFuture;Lcom/facebook/bookmark/model/Bookmark;)V

    invoke-static {v10, v11}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2579188
    iget-object v0, p0, LX/ITJ;->m:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 2579189
    return-void
.end method

.method public final a(LX/9N6;)V
    .locals 6

    .prologue
    .line 2579176
    if-eqz p1, :cond_0

    invoke-interface {p1}, LX/9N6;->y()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, LX/9N1;->l()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2579177
    :cond_0
    iget-object v0, p0, LX/ITJ;->o:LX/03V;

    sget-object v1, LX/ITJ;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "groupInformation or groupId is null in favoriteGroup"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2579178
    :goto_0
    return-void

    .line 2579179
    :cond_1
    new-instance v0, LX/9OZ;

    invoke-direct {v0}, LX/9OZ;-><init>()V

    .line 2579180
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/9OZ;->z:Z

    .line 2579181
    invoke-static {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->a(LX/9N6;)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-result-object v1

    .line 2579182
    invoke-static {v1}, LX/9OQ;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)LX/9OQ;

    move-result-object v2

    .line 2579183
    invoke-virtual {v0}, LX/9OZ;->a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    .line 2579184
    iput-object v0, v2, LX/9OQ;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    .line 2579185
    invoke-virtual {v2}, LX/9OQ;->a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-result-object v0

    .line 2579186
    iget-object v2, p0, LX/ITJ;->c:LX/IRb;

    invoke-interface {v2, v1, v0}, LX/IRb;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V

    .line 2579187
    iget-object v2, p0, LX/ITJ;->m:LX/1Ck;

    sget-object v3, LX/ITI;->GROUP_ADD_TO_FAVORITES:LX/ITI;

    invoke-direct {p0, p1}, LX/ITJ;->f(LX/9N6;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    new-instance v5, LX/ITE;

    invoke-direct {v5, p0, v0, v1}, LX/ITE;-><init>(LX/ITJ;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V

    invoke-virtual {v2, v3, v4, v5}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;ZLX/9N6;)V
    .locals 9

    .prologue
    .line 2579235
    if-eqz p3, :cond_0

    invoke-interface {p3}, LX/9N1;->l()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2579236
    :cond_0
    iget-object v0, p0, LX/ITJ;->o:LX/03V;

    sget-object v1, LX/ITJ;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "groupInformation or groupId is null in changeGroupSubscriptionStatus"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2579237
    :goto_0
    return-void

    .line 2579238
    :cond_1
    invoke-static {p3}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->a(LX/9N6;)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-result-object v5

    .line 2579239
    invoke-static {v5}, LX/9OQ;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)LX/9OQ;

    move-result-object v0

    .line 2579240
    if-eqz p2, :cond_2

    .line 2579241
    const-string v1, "follow_group"

    invoke-static {p0, v1}, LX/ITJ;->a(LX/ITJ;Ljava/lang/String;)V

    .line 2579242
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 2579243
    iput-object v1, v0, LX/9OQ;->v:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 2579244
    const v1, 0x7f081bcb

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2579245
    :goto_1
    invoke-virtual {v0}, LX/9OQ;->a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-result-object v4

    .line 2579246
    iget-object v0, p0, LX/ITJ;->c:LX/IRb;

    invoke-interface {v0, v5, v4}, LX/IRb;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V

    .line 2579247
    iget-object v6, p0, LX/ITJ;->m:LX/1Ck;

    sget-object v7, LX/ITI;->GROUP_FOLLOW_UNFOLLOW:LX/ITI;

    new-instance v8, LX/ITC;

    invoke-direct {v8, p0, p2, p3}, LX/ITC;-><init>(LX/ITJ;ZLX/9N6;)V

    new-instance v0, LX/ITD;

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, LX/ITD;-><init>(LX/ITJ;Landroid/content/Context;Ljava/lang/String;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V

    invoke-virtual {v6, v7, v8, v0}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_0

    .line 2579248
    :cond_2
    const-string v1, "unfollow_group"

    invoke-static {p0, v1}, LX/ITJ;->a(LX/ITJ;Ljava/lang/String;)V

    .line 2579249
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->CAN_SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 2579250
    iput-object v1, v0, LX/9OQ;->v:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 2579251
    const v1, 0x7f081bcc

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method

.method public final b(LX/9N6;)V
    .locals 12

    .prologue
    .line 2579143
    if-eqz p1, :cond_0

    invoke-interface {p1}, LX/9N1;->l()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2579144
    :cond_0
    iget-object v0, p0, LX/ITJ;->o:LX/03V;

    sget-object v1, LX/ITJ;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "groupInformation or groupId is null in unfavoriteGroup"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2579145
    :goto_0
    return-void

    .line 2579146
    :cond_1
    const-string v0, "remove_group_from_favorite"

    invoke-static {p0, v0}, LX/ITJ;->a(LX/ITJ;Ljava/lang/String;)V

    .line 2579147
    new-instance v0, LX/9OZ;

    invoke-direct {v0}, LX/9OZ;-><init>()V

    .line 2579148
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/9OZ;->z:Z

    .line 2579149
    invoke-static {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->a(LX/9N6;)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-result-object v1

    .line 2579150
    invoke-static {v1}, LX/9OQ;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)LX/9OQ;

    move-result-object v2

    .line 2579151
    invoke-virtual {v0}, LX/9OZ;->a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    .line 2579152
    iput-object v0, v2, LX/9OQ;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    .line 2579153
    invoke-virtual {v2}, LX/9OQ;->a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-result-object v0

    .line 2579154
    iget-object v2, p0, LX/ITJ;->c:LX/IRb;

    invoke-interface {v2, v1, v0}, LX/IRb;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V

    .line 2579155
    iget-object v2, p0, LX/ITJ;->m:LX/1Ck;

    sget-object v3, LX/ITI;->GROUP_REMOVE_FROM_FAVORITES:LX/ITI;

    .line 2579156
    const/4 v6, 0x0

    .line 2579157
    iget-object v7, p0, LX/ITJ;->l:Lcom/facebook/bookmark/client/BookmarkClient;

    invoke-virtual {v7}, Lcom/facebook/bookmark/client/BookmarkClient;->g()Z

    move-result v7

    .line 2579158
    if-eqz v7, :cond_2

    .line 2579159
    invoke-interface {p1}, LX/9N1;->l()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 2579160
    iget-object v8, p0, LX/ITJ;->l:Lcom/facebook/bookmark/client/BookmarkClient;

    const/4 v9, 0x0

    .line 2579161
    const/4 v10, 0x1

    invoke-static {v8, v10}, Lcom/facebook/bookmark/client/BookmarkClient;->a(Lcom/facebook/bookmark/client/BookmarkClient;Z)Lcom/facebook/bookmark/model/BookmarksGroup;

    move-result-object v10

    .line 2579162
    if-nez v10, :cond_4

    .line 2579163
    sget-object v10, Lcom/facebook/bookmark/client/BookmarkClient;->a:Ljava/lang/Class;

    const-string v11, "No favorite group in BookmarkClient"

    invoke-static {v10, v11}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2579164
    :goto_1
    move-object v6, v9

    .line 2579165
    :cond_2
    if-nez v6, :cond_3

    .line 2579166
    iget-object v6, p0, LX/ITJ;->s:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/DPT;

    invoke-interface {p1}, LX/9N1;->l()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/DPT;->b(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    .line 2579167
    :cond_3
    move-object v4, v6

    .line 2579168
    new-instance v5, LX/ITF;

    invoke-direct {v5, p0, v0, v1}, LX/ITF;-><init>(LX/ITJ;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V

    invoke-virtual {v2, v3, v4, v5}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0

    .line 2579169
    :cond_4
    invoke-virtual {v10, v6, v7}, Lcom/facebook/bookmark/model/BookmarksGroup;->b(J)Z

    move-result v11

    if-nez v11, :cond_5

    .line 2579170
    sget-object v10, Lcom/facebook/bookmark/client/BookmarkClient;->a:Ljava/lang/Class;

    const-string v11, "Shouldn\'t see add to favorites when group is already in favorites"

    invoke-static {v10, v11}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_1

    .line 2579171
    :cond_5
    invoke-virtual {v10, v6, v7}, Lcom/facebook/bookmark/model/BookmarksGroup;->a(J)V

    .line 2579172
    iget-object v9, v8, Lcom/facebook/bookmark/client/BookmarkClient;->i:Ljava/util/Set;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-interface {v9, v11}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2579173
    const/4 v9, 0x0

    invoke-static {v8, v10, v9}, Lcom/facebook/bookmark/client/BookmarkClient;->a(Lcom/facebook/bookmark/client/BookmarkClient;Lcom/facebook/bookmark/model/BookmarksGroup;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v10

    .line 2579174
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v9

    .line 2579175
    new-instance v11, LX/EgJ;

    invoke-direct {v11, v8, v9, v6, v7}, LX/EgJ;-><init>(Lcom/facebook/bookmark/client/BookmarkClient;Lcom/google/common/util/concurrent/SettableFuture;J)V

    invoke-static {v10, v11}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto :goto_1
.end method

.method public final d(LX/9N6;)V
    .locals 5

    .prologue
    .line 2579130
    if-nez p1, :cond_1

    .line 2579131
    iget-object v0, p0, LX/ITJ;->o:LX/03V;

    sget-object v1, LX/ITJ;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "groupInformation is null in sendJoinGroupRequest"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2579132
    :cond_0
    :goto_0
    return-void

    .line 2579133
    :cond_1
    invoke-interface {p1}, LX/9N1;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CAN_JOIN:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-eq v0, v1, :cond_2

    invoke-interface {p1}, LX/9N1;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-ne v0, v1, :cond_0

    .line 2579134
    :cond_2
    const-string v0, "send_join_request_to_group"

    invoke-static {p0, v0}, LX/ITJ;->a(LX/ITJ;Ljava/lang/String;)V

    .line 2579135
    iget-object v0, p0, LX/ITJ;->d:LX/3mF;

    invoke-interface {p1}, LX/9N1;->l()Ljava/lang/String;

    move-result-object v1

    const-string v2, "mobile_group_join"

    invoke-virtual {v0, v1, v2}, LX/3mF;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2579136
    invoke-static {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->a(LX/9N6;)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-result-object v2

    .line 2579137
    invoke-static {v2}, LX/9OQ;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)LX/9OQ;

    move-result-object v3

    .line 2579138
    invoke-interface {p1}, LX/9N1;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CAN_JOIN:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-ne v0, v4, :cond_3

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    :goto_1
    iput-object v0, v3, LX/9OQ;->F:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 2579139
    invoke-virtual {v3}, LX/9OQ;->a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-result-object v0

    .line 2579140
    iget-object v3, p0, LX/ITJ;->c:LX/IRb;

    invoke-interface {v3, v2, v0}, LX/IRb;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V

    .line 2579141
    iget-object v3, p0, LX/ITJ;->j:LX/0Sh;

    new-instance v4, LX/ITG;

    invoke-direct {v4, p0, v0, v2}, LX/ITG;-><init>(LX/ITJ;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V

    invoke-virtual {v3, v1, v4}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto :goto_0

    .line 2579142
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    goto :goto_1
.end method

.method public final e(LX/9N6;)V
    .locals 5

    .prologue
    .line 2579116
    if-eqz p1, :cond_0

    invoke-interface {p1}, LX/9N6;->y()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    if-nez v0, :cond_2

    .line 2579117
    :cond_0
    iget-object v0, p0, LX/ITJ;->o:LX/03V;

    sget-object v1, LX/ITJ;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "groupInformation is null in sendCancelGroupJoinRequestRequest"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2579118
    :cond_1
    :goto_0
    return-void

    .line 2579119
    :cond_2
    invoke-interface {p1}, LX/9N1;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-eq v0, v1, :cond_3

    invoke-interface {p1}, LX/9N1;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-ne v0, v1, :cond_1

    .line 2579120
    :cond_3
    invoke-interface {p1}, LX/9N1;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-ne v0, v1, :cond_4

    .line 2579121
    const-string v0, "send_leave_request_to_group"

    invoke-static {p0, v0}, LX/ITJ;->a(LX/ITJ;Ljava/lang/String;)V

    .line 2579122
    :goto_1
    iget-object v0, p0, LX/ITJ;->d:LX/3mF;

    invoke-interface {p1}, LX/9N1;->l()Ljava/lang/String;

    move-result-object v1

    const-string v2, "mobile_group_join"

    const-string v3, "ALLOW_READD"

    invoke-virtual {v0, v1, v2, v3}, LX/3mF;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2579123
    invoke-static {p1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->a(LX/9N6;)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-result-object v1

    .line 2579124
    invoke-static {v1}, LX/9OQ;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)LX/9OQ;

    move-result-object v2

    .line 2579125
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    iput-object v3, v2, LX/9OQ;->F:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 2579126
    invoke-virtual {v2}, LX/9OQ;->a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-result-object v2

    .line 2579127
    iget-object v3, p0, LX/ITJ;->c:LX/IRb;

    invoke-interface {v3, v1, v2}, LX/IRb;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V

    .line 2579128
    iget-object v3, p0, LX/ITJ;->j:LX/0Sh;

    new-instance v4, LX/ITH;

    invoke-direct {v4, p0, v2, v1}, LX/ITH;-><init>(LX/ITJ;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V

    invoke-virtual {v3, v0, v4}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto :goto_0

    .line 2579129
    :cond_4
    const-string v0, "send_cancel_request_to_group"

    invoke-static {p0, v0}, LX/ITJ;->a(LX/ITJ;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final e(LX/9N6;Landroid/content/Context;)V
    .locals 8

    .prologue
    .line 2579104
    const-string v0, "create_new_group_event"

    invoke-static {p0, v0}, LX/ITJ;->a(LX/ITJ;Ljava/lang/String;)V

    .line 2579105
    const-string v0, "group_header"

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, LX/ITJ;->n:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2579106
    iget-object v2, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v2

    .line 2579107
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    sget-object v3, Lcom/facebook/events/common/ActionMechanism;->GROUP_PERMALINK_ACTIONS:Lcom/facebook/events/common/ActionMechanism;

    invoke-interface {p1}, LX/9N1;->l()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1}, LX/9N6;->y()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-interface {v0}, LX/9Mh;->d()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p1}, LX/9N6;->y()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->K()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v6

    invoke-interface {p1}, LX/9N6;->y()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->s()LX/9Mi;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v7, 0x0

    :goto_0
    move-object v0, p2

    invoke-static/range {v0 .. v7}, Lcom/facebook/events/create/EventCreationNikumanActivity;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Long;Lcom/facebook/events/common/ActionMechanism;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2579108
    iget-object v1, p0, LX/ITJ;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2579109
    return-void

    .line 2579110
    :cond_0
    invoke-interface {p1}, LX/9N6;->y()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->s()LX/9Mi;

    move-result-object v0

    invoke-interface {v0}, LX/9Mh;->d()Ljava/lang/String;

    move-result-object v7

    goto :goto_0
.end method

.method public final i(LX/9N6;Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2579099
    if-eqz p1, :cond_0

    invoke-interface {p1}, LX/9N1;->l()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2579100
    :cond_0
    iget-object v0, p0, LX/ITJ;->o:LX/03V;

    sget-object v1, LX/ITJ;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "groupInformation or groupId is null in editNotificationSetting"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2579101
    :goto_0
    return-void

    .line 2579102
    :cond_1
    const-string v0, "edit_group_notif_setting"

    invoke-static {p0, v0}, LX/ITJ;->a(LX/ITJ;Ljava/lang/String;)V

    .line 2579103
    iget-object v0, p0, LX/ITJ;->e:LX/17W;

    const-string v1, "https://m.facebook.com/group/settings/?group_id=%s"

    invoke-interface {p1}, LX/9N1;->l()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    goto :goto_0
.end method
