.class public final LX/Ijq;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:LX/Ijy;


# direct methods
.method public constructor <init>(LX/Ijy;)V
    .locals 0

    .prologue
    .line 2606093
    iput-object p1, p0, LX/Ijq;->a:LX/Ijy;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 3

    .prologue
    .line 2606094
    sget-object v0, LX/Ijy;->a:Ljava/lang/Class;

    const-string v1, "Card failed to set to be primary"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2606095
    iget-object v0, p0, LX/Ijq;->a:LX/Ijy;

    iget-object v0, v0, LX/Ijy;->b:LX/03V;

    const-string v1, "P2P_PAYMENT_CARD_SET_PRIMARY_FAILED"

    const-string v2, "Attempted to set a paymentcard as primary, but received a response with an error"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2606096
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v0, v0

    .line 2606097
    sget-object v1, LX/1nY;->API_ERROR:LX/1nY;

    if-eq v0, v1, :cond_0

    .line 2606098
    iget-object v0, p0, LX/Ijq;->a:LX/Ijy;

    iget-object v0, v0, LX/Ijy;->e:Landroid/content/Context;

    invoke-static {v0, p1}, LX/6up;->a(Landroid/content/Context;Lcom/facebook/fbservice/service/ServiceException;)V

    .line 2606099
    :cond_0
    return-void
.end method

.method public final bridge synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2606100
    return-void
.end method
