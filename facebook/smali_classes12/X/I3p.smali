.class public LX/I3p;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/B5Y;


# instance fields
.field public final a:Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;

.field private final b:Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionEdgeModel;

.field private final c:Ljava/lang/String;

.field private final d:LX/I3o;


# direct methods
.method public constructor <init>(Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;Ljava/lang/String;)V
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "<init>"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2531790
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2531791
    iput-object p1, p0, LX/I3p;->a:Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;

    .line 2531792
    invoke-virtual {p1}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionsMasterModel;->b()Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionEdgeModel;

    move-result-object v0

    iput-object v0, p0, LX/I3p;->b:Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionEdgeModel;

    .line 2531793
    iput-object p2, p0, LX/I3p;->c:Ljava/lang/String;

    .line 2531794
    new-instance v0, LX/I3o;

    iget-object v1, p0, LX/I3p;->b:Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionEdgeModel;

    invoke-virtual {v1}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionEdgeModel;->b()Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionEdgeModel$DocumentBodyElementsModel;

    move-result-object v1

    invoke-direct {v0, v1}, LX/I3o;-><init>(Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionEdgeModel$DocumentBodyElementsModel;)V

    iput-object v0, p0, LX/I3p;->d:LX/I3o;

    .line 2531795
    return-void
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2531789
    iget-object v0, p0, LX/I3p;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 2531788
    const/4 v0, 0x0

    return v0
.end method

.method public final d()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentBylineTextModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2531787
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlInterfaces$RichDocumentBylineProfile;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2531786
    const/4 v0, 0x0

    return-object v0
.end method

.method public final iP_()Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleEdgeModel$CoverMediaModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2531785
    const/4 v0, 0x0

    return-object v0
.end method

.method public final iQ_()LX/8Z4;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2531784
    const/4 v0, 0x0

    return-object v0
.end method

.method public final j()LX/8Z4;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2531783
    const/4 v0, 0x0

    return-object v0
.end method

.method public final k()LX/B5W;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2531774
    const/4 v0, 0x0

    return-object v0
.end method

.method public final l()LX/B5X;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2531796
    iget-object v0, p0, LX/I3p;->d:LX/I3o;

    return-object v0
.end method

.method public final m()LX/8Z4;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2531775
    const/4 v0, 0x0

    return-object v0
.end method

.method public final n()LX/8Yp;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2531776
    const/4 v0, 0x0

    return-object v0
.end method

.method public final o()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2531777
    const/4 v0, 0x0

    return-object v0
.end method

.method public final p()LX/8Z4;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2531778
    iget-object v0, p0, LX/I3p;->b:Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionEdgeModel;

    invoke-virtual {v0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionEdgeModel;->c()LX/8Z4;

    move-result-object v0

    return-object v0
.end method

.method public final q()LX/8Z4;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2531779
    iget-object v0, p0, LX/I3p;->b:Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionEdgeModel;

    invoke-virtual {v0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionEdgeModel;->d()LX/8Z4;

    move-result-object v0

    return-object v0
.end method

.method public final r()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2531780
    iget-object v0, p0, LX/I3p;->b:Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionEdgeModel;

    invoke-virtual {v0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionEdgeModel;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final s()J
    .locals 2

    .prologue
    .line 2531781
    iget-object v0, p0, LX/I3p;->b:Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionEdgeModel;

    invoke-virtual {v0}, Lcom/facebook/events/eventcollections/graphql/EventCollectionsGraphQLModels$EventCollectionEdgeModel;->mC_()J

    move-result-wide v0

    return-wide v0
.end method

.method public final t()Lcom/facebook/graphql/enums/GraphQLDocumentTextDirectionEnum;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2531782
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentTextDirectionEnum;->LEFT_TO_RIGHT:Lcom/facebook/graphql/enums/GraphQLDocumentTextDirectionEnum;

    return-object v0
.end method
