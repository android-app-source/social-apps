.class public LX/JFa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/JF1;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:I

.field private final c:I

.field private final d:Ljava/lang/CharSequence;

.field private e:I


# direct methods
.method public constructor <init>(Ljava/lang/String;IILjava/lang/CharSequence;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LX/JFa;->a:Ljava/lang/String;

    iput p2, p0, LX/JFa;->b:I

    iput p3, p0, LX/JFa;->c:I

    iput-object p4, p0, LX/JFa;->d:Ljava/lang/CharSequence;

    iput p5, p0, LX/JFa;->e:I

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 1

    return-object p0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, LX/JFa;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, LX/JFa;

    iget v2, p1, LX/JFa;->b:I

    iget v3, p0, LX/JFa;->b:I

    if-ne v2, v3, :cond_3

    iget v2, p1, LX/JFa;->c:I

    iget v3, p0, LX/JFa;->c:I

    if-ne v2, v3, :cond_3

    iget-object v2, p1, LX/JFa;->a:Ljava/lang/String;

    iget-object v3, p0, LX/JFa;->a:Ljava/lang/String;

    invoke-static {v2, v3}, LX/2wy;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p1, LX/JFa;->d:Ljava/lang/CharSequence;

    iget-object v3, p0, LX/JFa;->d:Ljava/lang/CharSequence;

    invoke-static {v2, v3}, LX/2wy;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, LX/JFa;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, LX/JFa;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, LX/JFa;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, LX/JFa;->d:Ljava/lang/CharSequence;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/2wy;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
