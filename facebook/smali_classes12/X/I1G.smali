.class public LX/I1G;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0SG;

.field public final b:LX/0tX;

.field private final c:LX/1My;

.field private final d:LX/0ad;

.field public final e:LX/1Ck;

.field public final f:LX/I6X;

.field public final g:LX/0tQ;

.field public final h:LX/0hB;

.field public final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/util/TimeZone;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0SG;LX/0tX;LX/1My;LX/0ad;LX/1Ck;LX/I6X;LX/0tQ;LX/0hB;LX/0Or;)V
    .locals 0
    .param p9    # LX/0Or;
        .annotation build Lcom/facebook/events/dateformatter/annotation/EventsTimeZone;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0SG;",
            "LX/0tX;",
            "LX/1My;",
            "LX/0ad;",
            "LX/1Ck;",
            "LX/I6X;",
            "LX/0tQ;",
            "LX/0hB;",
            "LX/0Or",
            "<",
            "Ljava/util/TimeZone;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2528796
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2528797
    iput-object p1, p0, LX/I1G;->a:LX/0SG;

    .line 2528798
    iput-object p2, p0, LX/I1G;->b:LX/0tX;

    .line 2528799
    iput-object p3, p0, LX/I1G;->c:LX/1My;

    .line 2528800
    iput-object p4, p0, LX/I1G;->d:LX/0ad;

    .line 2528801
    iput-object p5, p0, LX/I1G;->e:LX/1Ck;

    .line 2528802
    iput-object p6, p0, LX/I1G;->f:LX/I6X;

    .line 2528803
    iput-object p7, p0, LX/I1G;->g:LX/0tQ;

    .line 2528804
    iput-object p8, p0, LX/I1G;->h:LX/0hB;

    .line 2528805
    iput-object p9, p0, LX/I1G;->i:LX/0Or;

    .line 2528806
    return-void
.end method

.method public static b(LX/0QB;)LX/I1G;
    .locals 10

    .prologue
    .line 2528807
    new-instance v0, LX/I1G;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-static {p0}, LX/1My;->b(LX/0QB;)LX/1My;

    move-result-object v3

    check-cast v3, LX/1My;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v5

    check-cast v5, LX/1Ck;

    invoke-static {p0}, LX/I6X;->b(LX/0QB;)LX/I6X;

    move-result-object v6

    check-cast v6, LX/I6X;

    invoke-static {p0}, LX/0tQ;->a(LX/0QB;)LX/0tQ;

    move-result-object v7

    check-cast v7, LX/0tQ;

    invoke-static {p0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v8

    check-cast v8, LX/0hB;

    const/16 v9, 0x1619

    invoke-static {p0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-direct/range {v0 .. v9}, LX/I1G;-><init>(LX/0SG;LX/0tX;LX/1My;LX/0ad;LX/1Ck;LX/I6X;LX/0tQ;LX/0hB;LX/0Or;)V

    .line 2528808
    return-object v0
.end method
