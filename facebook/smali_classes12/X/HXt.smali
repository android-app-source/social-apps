.class public LX/HXt;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2479543
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2479544
    return-void
.end method

.method public static a(LX/0QB;)LX/HXt;
    .locals 1

    .prologue
    .line 2479545
    new-instance v0, LX/HXt;

    invoke-direct {v0}, LX/HXt;-><init>()V

    .line 2479546
    move-object v0, v0

    .line 2479547
    return-object v0
.end method


# virtual methods
.method public final b()V
    .locals 2

    .prologue
    .line 2479548
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Card scanner is not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2479549
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Card scanner is not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final d()LX/HXu;
    .locals 2

    .prologue
    .line 2479550
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Card scanner is not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
