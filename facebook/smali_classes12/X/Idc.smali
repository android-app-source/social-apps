.class public final enum LX/Idc;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Idc;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Idc;

.field public static final enum Grow:LX/Idc;

.field public static final enum Move:LX/Idc;

.field public static final enum None:LX/Idc;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2597559
    new-instance v0, LX/Idc;

    const-string v1, "None"

    invoke-direct {v0, v1, v2}, LX/Idc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Idc;->None:LX/Idc;

    new-instance v0, LX/Idc;

    const-string v1, "Move"

    invoke-direct {v0, v1, v3}, LX/Idc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Idc;->Move:LX/Idc;

    new-instance v0, LX/Idc;

    const-string v1, "Grow"

    invoke-direct {v0, v1, v4}, LX/Idc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Idc;->Grow:LX/Idc;

    const/4 v0, 0x3

    new-array v0, v0, [LX/Idc;

    sget-object v1, LX/Idc;->None:LX/Idc;

    aput-object v1, v0, v2

    sget-object v1, LX/Idc;->Move:LX/Idc;

    aput-object v1, v0, v3

    sget-object v1, LX/Idc;->Grow:LX/Idc;

    aput-object v1, v0, v4

    sput-object v0, LX/Idc;->$VALUES:[LX/Idc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2597560
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Idc;
    .locals 1

    .prologue
    .line 2597561
    const-class v0, LX/Idc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Idc;

    return-object v0
.end method

.method public static values()[LX/Idc;
    .locals 1

    .prologue
    .line 2597562
    sget-object v0, LX/Idc;->$VALUES:[LX/Idc;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Idc;

    return-object v0
.end method
