.class public final enum LX/J5r;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/J5r;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/J5r;

.field public static final enum CHECKUP_STEPS:LX/J5r;

.field public static final enum CONCLUSION:LX/J5r;

.field public static final enum INTRODUCTION:LX/J5r;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2648682
    new-instance v0, LX/J5r;

    const-string v1, "INTRODUCTION"

    invoke-direct {v0, v1, v2}, LX/J5r;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J5r;->INTRODUCTION:LX/J5r;

    .line 2648683
    new-instance v0, LX/J5r;

    const-string v1, "CHECKUP_STEPS"

    invoke-direct {v0, v1, v3}, LX/J5r;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J5r;->CHECKUP_STEPS:LX/J5r;

    .line 2648684
    new-instance v0, LX/J5r;

    const-string v1, "CONCLUSION"

    invoke-direct {v0, v1, v4}, LX/J5r;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J5r;->CONCLUSION:LX/J5r;

    .line 2648685
    const/4 v0, 0x3

    new-array v0, v0, [LX/J5r;

    sget-object v1, LX/J5r;->INTRODUCTION:LX/J5r;

    aput-object v1, v0, v2

    sget-object v1, LX/J5r;->CHECKUP_STEPS:LX/J5r;

    aput-object v1, v0, v3

    sget-object v1, LX/J5r;->CONCLUSION:LX/J5r;

    aput-object v1, v0, v4

    sput-object v0, LX/J5r;->$VALUES:[LX/J5r;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2648686
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/J5r;
    .locals 1

    .prologue
    .line 2648687
    const-class v0, LX/J5r;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/J5r;

    return-object v0
.end method

.method public static values()[LX/J5r;
    .locals 1

    .prologue
    .line 2648688
    sget-object v0, LX/J5r;->$VALUES:[LX/J5r;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/J5r;

    return-object v0
.end method
