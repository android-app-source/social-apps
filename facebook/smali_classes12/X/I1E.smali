.class public final LX/I1E;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardDiscoveryFilterQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/I17;

.field public final synthetic b:LX/I1G;


# direct methods
.method public constructor <init>(LX/I1G;LX/I17;)V
    .locals 0

    .prologue
    .line 2528768
    iput-object p1, p0, LX/I1E;->b:LX/I1G;

    iput-object p2, p0, LX/I1E;->a:LX/I17;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2528769
    iget-object v0, p0, LX/I1E;->a:LX/I17;

    sget-object v1, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/I17;->a(Ljava/util/List;)V

    .line 2528770
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2528771
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2528772
    if-eqz p1, :cond_0

    .line 2528773
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2528774
    if-eqz v0, :cond_0

    .line 2528775
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2528776
    if-eqz v0, :cond_0

    .line 2528777
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2528778
    check-cast v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardDiscoveryFilterQueryModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardDiscoveryFilterQueryModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2528779
    iget-object v1, p0, LX/I1E;->a:LX/I17;

    .line 2528780
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2528781
    check-cast v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardDiscoveryFilterQueryModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardDiscoveryFilterQueryModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/I17;->a(Ljava/util/List;)V

    .line 2528782
    :goto_0
    return-void

    .line 2528783
    :cond_0
    iget-object v0, p0, LX/I1E;->a:LX/I17;

    sget-object v1, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/I17;->a(Ljava/util/List;)V

    goto :goto_0
.end method
