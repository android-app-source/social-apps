.class public final enum LX/IhU;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IhU;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IhU;

.field public static final enum ALBUMS:LX/IhU;

.field public static final enum CAMERA:LX/IhU;


# instance fields
.field public final titleResId:I


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2602670
    new-instance v0, LX/IhU;

    const-string v1, "CAMERA"

    const v2, 0x7f0823d7

    invoke-direct {v0, v1, v3, v2}, LX/IhU;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/IhU;->CAMERA:LX/IhU;

    .line 2602671
    new-instance v0, LX/IhU;

    const-string v1, "ALBUMS"

    const v2, 0x7f0823d8

    invoke-direct {v0, v1, v4, v2}, LX/IhU;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/IhU;->ALBUMS:LX/IhU;

    .line 2602672
    const/4 v0, 0x2

    new-array v0, v0, [LX/IhU;

    sget-object v1, LX/IhU;->CAMERA:LX/IhU;

    aput-object v1, v0, v3

    sget-object v1, LX/IhU;->ALBUMS:LX/IhU;

    aput-object v1, v0, v4

    sput-object v0, LX/IhU;->$VALUES:[LX/IhU;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 2602673
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2602674
    iput p3, p0, LX/IhU;->titleResId:I

    .line 2602675
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IhU;
    .locals 1

    .prologue
    .line 2602676
    const-class v0, LX/IhU;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IhU;

    return-object v0
.end method

.method public static values()[LX/IhU;
    .locals 1

    .prologue
    .line 2602677
    sget-object v0, LX/IhU;->$VALUES:[LX/IhU;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IhU;

    return-object v0
.end method
