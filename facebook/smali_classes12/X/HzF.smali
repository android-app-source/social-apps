.class public final LX/HzF;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPrivateEventInvitesCountQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;)V
    .locals 0

    .prologue
    .line 2525460
    iput-object p1, p0, LX/HzF;->a:Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2525461
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2525462
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2525463
    if-eqz p1, :cond_0

    .line 2525464
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2525465
    if-eqz v0, :cond_0

    .line 2525466
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2525467
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPrivateEventInvitesCountQueryModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPrivateEventInvitesCountQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPrivateEventInvitesCountQueryModel$AllEventsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2525468
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2525469
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPrivateEventInvitesCountQueryModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPrivateEventInvitesCountQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPrivateEventInvitesCountQueryModel$AllEventsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchPrivateEventInvitesCountQueryModel$AllEventsModel;->a()I

    move-result v0

    .line 2525470
    iget-object v1, p0, LX/HzF;->a:Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;

    iget-object v1, v1, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->g:Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;

    invoke-virtual {v1, v0}, Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;->setCalendarBadgeCount(I)V

    .line 2525471
    :cond_0
    return-void
.end method
