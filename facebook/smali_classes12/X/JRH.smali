.class public final LX/JRH;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JRI;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic d:LX/JRI;


# direct methods
.method public constructor <init>(LX/JRI;)V
    .locals 1

    .prologue
    .line 2693081
    iput-object p1, p0, LX/JRH;->d:LX/JRI;

    .line 2693082
    move-object v0, p1

    .line 2693083
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2693084
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2693085
    const-string v0, "MessengerGenericPromotionFooterComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2693086
    if-ne p0, p1, :cond_1

    .line 2693087
    :cond_0
    :goto_0
    return v0

    .line 2693088
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2693089
    goto :goto_0

    .line 2693090
    :cond_3
    check-cast p1, LX/JRH;

    .line 2693091
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2693092
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2693093
    if-eq v2, v3, :cond_0

    .line 2693094
    iget-object v2, p0, LX/JRH;->a:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JRH;->a:Ljava/lang/String;

    iget-object v3, p1, LX/JRH;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2693095
    goto :goto_0

    .line 2693096
    :cond_5
    iget-object v2, p1, LX/JRH;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 2693097
    :cond_6
    iget-object v2, p0, LX/JRH;->b:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/JRH;->b:Ljava/lang/String;

    iget-object v3, p1, LX/JRH;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2693098
    goto :goto_0

    .line 2693099
    :cond_8
    iget-object v2, p1, LX/JRH;->b:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 2693100
    :cond_9
    iget-object v2, p0, LX/JRH;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/JRH;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/JRH;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2693101
    goto :goto_0

    .line 2693102
    :cond_a
    iget-object v2, p1, LX/JRH;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
