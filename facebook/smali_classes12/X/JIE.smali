.class public final LX/JIE;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JIF;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

.field public b:Landroid/graphics/drawable/Drawable;

.field public c:LX/Emj;

.field public final synthetic d:LX/JIF;


# direct methods
.method public constructor <init>(LX/JIF;)V
    .locals 1

    .prologue
    .line 2677601
    iput-object p1, p0, LX/JIE;->d:LX/JIF;

    .line 2677602
    move-object v0, p1

    .line 2677603
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2677604
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2677583
    const-string v0, "DiscoveryHeaderComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2677584
    if-ne p0, p1, :cond_1

    .line 2677585
    :cond_0
    :goto_0
    return v0

    .line 2677586
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2677587
    goto :goto_0

    .line 2677588
    :cond_3
    check-cast p1, LX/JIE;

    .line 2677589
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2677590
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2677591
    if-eq v2, v3, :cond_0

    .line 2677592
    iget-object v2, p0, LX/JIE;->a:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JIE;->a:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    iget-object v3, p1, LX/JIE;->a:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2677593
    goto :goto_0

    .line 2677594
    :cond_5
    iget-object v2, p1, LX/JIE;->a:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    if-nez v2, :cond_4

    .line 2677595
    :cond_6
    iget-object v2, p0, LX/JIE;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/JIE;->b:Landroid/graphics/drawable/Drawable;

    iget-object v3, p1, LX/JIE;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2677596
    goto :goto_0

    .line 2677597
    :cond_8
    iget-object v2, p1, LX/JIE;->b:Landroid/graphics/drawable/Drawable;

    if-nez v2, :cond_7

    .line 2677598
    :cond_9
    iget-object v2, p0, LX/JIE;->c:LX/Emj;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/JIE;->c:LX/Emj;

    iget-object v3, p1, LX/JIE;->c:LX/Emj;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2677599
    goto :goto_0

    .line 2677600
    :cond_a
    iget-object v2, p1, LX/JIE;->c:LX/Emj;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
