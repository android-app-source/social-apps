.class public final LX/Hb3;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2484922
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 2484923
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2484924
    :goto_0
    return v1

    .line 2484925
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2484926
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_4

    .line 2484927
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2484928
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2484929
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 2484930
    const-string v4, "login_alerts"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2484931
    invoke-static {p0, p1}, LX/Hb2;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 2484932
    :cond_2
    const-string v4, "unused_user_sessions"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2484933
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2484934
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_ARRAY:LX/15z;

    if-ne v3, v4, :cond_3

    .line 2484935
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_ARRAY:LX/15z;

    if-eq v3, v4, :cond_3

    .line 2484936
    invoke-static {p0, p1}, LX/Hb4;->b(LX/15w;LX/186;)I

    move-result v3

    .line 2484937
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2484938
    :cond_3
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    move v0, v0

    .line 2484939
    goto :goto_1

    .line 2484940
    :cond_4
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2484941
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2484942
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2484943
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2484944
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2484945
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2484946
    if-eqz v0, :cond_5

    .line 2484947
    const-string v1, "login_alerts"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2484948
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2484949
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2484950
    if-eqz v1, :cond_0

    .line 2484951
    const-string v2, "email_for_alerts"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2484952
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2484953
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->b(II)Z

    move-result v1

    .line 2484954
    if-eqz v1, :cond_1

    .line 2484955
    const-string v2, "is_email_enabled"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2484956
    invoke-virtual {p2, v1}, LX/0nX;->a(Z)V

    .line 2484957
    :cond_1
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, LX/15i;->b(II)Z

    move-result v1

    .line 2484958
    if-eqz v1, :cond_2

    .line 2484959
    const-string v2, "is_jewel_notif_enabled"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2484960
    invoke-virtual {p2, v1}, LX/0nX;->a(Z)V

    .line 2484961
    :cond_2
    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, LX/15i;->b(II)Z

    move-result v1

    .line 2484962
    if-eqz v1, :cond_3

    .line 2484963
    const-string v2, "is_sms_enabled"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2484964
    invoke-virtual {p2, v1}, LX/0nX;->a(Z)V

    .line 2484965
    :cond_3
    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2484966
    if-eqz v1, :cond_4

    .line 2484967
    const-string v2, "phone_for_alerts"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2484968
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2484969
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2484970
    :cond_5
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2484971
    if-eqz v0, :cond_7

    .line 2484972
    const-string v1, "unused_user_sessions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2484973
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2484974
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_6

    .line 2484975
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/Hb4;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2484976
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2484977
    :cond_6
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2484978
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2484979
    return-void
.end method
