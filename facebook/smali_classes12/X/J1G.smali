.class public final enum LX/J1G;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/J1G;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/J1G;

.field public static final enum INCOMING:LX/J1G;

.field public static final enum OUTGOING:LX/J1G;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2638499
    new-instance v0, LX/J1G;

    const-string v1, "INCOMING"

    invoke-direct {v0, v1, v2}, LX/J1G;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J1G;->INCOMING:LX/J1G;

    .line 2638500
    new-instance v0, LX/J1G;

    const-string v1, "OUTGOING"

    invoke-direct {v0, v1, v3}, LX/J1G;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/J1G;->OUTGOING:LX/J1G;

    .line 2638501
    const/4 v0, 0x2

    new-array v0, v0, [LX/J1G;

    sget-object v1, LX/J1G;->INCOMING:LX/J1G;

    aput-object v1, v0, v2

    sget-object v1, LX/J1G;->OUTGOING:LX/J1G;

    aput-object v1, v0, v3

    sput-object v0, LX/J1G;->$VALUES:[LX/J1G;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2638502
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/J1G;
    .locals 1

    .prologue
    .line 2638503
    const-class v0, LX/J1G;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/J1G;

    return-object v0
.end method

.method public static values()[LX/J1G;
    .locals 1

    .prologue
    .line 2638504
    sget-object v0, LX/J1G;->$VALUES:[LX/J1G;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/J1G;

    return-object v0
.end method
