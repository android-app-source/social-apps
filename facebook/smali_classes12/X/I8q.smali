.class public final enum LX/I8q;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/I8q;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/I8q;

.field public static final enum END_MARKER:LX/I8q;

.field public static final enum PROGRESS_BAR:LX/I8q;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2542045
    new-instance v0, LX/I8q;

    const-string v1, "PROGRESS_BAR"

    invoke-direct {v0, v1, v2}, LX/I8q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I8q;->PROGRESS_BAR:LX/I8q;

    .line 2542046
    new-instance v0, LX/I8q;

    const-string v1, "END_MARKER"

    invoke-direct {v0, v1, v3}, LX/I8q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I8q;->END_MARKER:LX/I8q;

    .line 2542047
    const/4 v0, 0x2

    new-array v0, v0, [LX/I8q;

    sget-object v1, LX/I8q;->PROGRESS_BAR:LX/I8q;

    aput-object v1, v0, v2

    sget-object v1, LX/I8q;->END_MARKER:LX/I8q;

    aput-object v1, v0, v3

    sput-object v0, LX/I8q;->$VALUES:[LX/I8q;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2542048
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/I8q;
    .locals 1

    .prologue
    .line 2542049
    const-class v0, LX/I8q;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/I8q;

    return-object v0
.end method

.method public static values()[LX/I8q;
    .locals 1

    .prologue
    .line 2542050
    sget-object v0, LX/I8q;->$VALUES:[LX/I8q;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/I8q;

    return-object v0
.end method
