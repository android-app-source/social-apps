.class public LX/Izm;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/Izm;


# instance fields
.field private final a:LX/IzO;

.field private final b:LX/Izi;

.field private final c:LX/03V;


# direct methods
.method public constructor <init>(LX/IzO;LX/Izi;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2635568
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2635569
    iput-object p1, p0, LX/Izm;->a:LX/IzO;

    .line 2635570
    iput-object p3, p0, LX/Izm;->c:LX/03V;

    .line 2635571
    iput-object p2, p0, LX/Izm;->b:LX/Izi;

    .line 2635572
    return-void
.end method

.method public static a(LX/0QB;)LX/Izm;
    .locals 6

    .prologue
    .line 2635573
    sget-object v0, LX/Izm;->d:LX/Izm;

    if-nez v0, :cond_1

    .line 2635574
    const-class v1, LX/Izm;

    monitor-enter v1

    .line 2635575
    :try_start_0
    sget-object v0, LX/Izm;->d:LX/Izm;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2635576
    if-eqz v2, :cond_0

    .line 2635577
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2635578
    new-instance p0, LX/Izm;

    invoke-static {v0}, LX/IzO;->a(LX/0QB;)LX/IzO;

    move-result-object v3

    check-cast v3, LX/IzO;

    invoke-static {v0}, LX/Izi;->a(LX/0QB;)LX/Izi;

    move-result-object v4

    check-cast v4, LX/Izi;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-direct {p0, v3, v4, v5}, LX/Izm;-><init>(LX/IzO;LX/Izi;LX/03V;)V

    .line 2635579
    move-object v0, p0

    .line 2635580
    sput-object v0, LX/Izm;->d:LX/Izm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2635581
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2635582
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2635583
    :cond_1
    sget-object v0, LX/Izm;->d:LX/Izm;

    return-object v0

    .line 2635584
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2635585
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(JJ)V
    .locals 5

    .prologue
    .line 2635586
    const-string v0, "setPaymentCardIdForTransaction"

    const v1, 0x70664ddd

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2635587
    :try_start_0
    iget-object v0, p0, LX/Izm;->b:LX/Izi;

    invoke-virtual {v0, p1, p2}, LX/Izi;->a(J)Ljava/lang/Long;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2635588
    const v0, 0x6e709ae3

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2635589
    :goto_0
    return-void

    .line 2635590
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/Izm;->a:LX/IzO;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2635591
    const v0, 0x72774be7

    invoke-static {v1, v0}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2635592
    :try_start_2
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2635593
    sget-object v2, LX/IzY;->a:LX/0U1;

    .line 2635594
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2635595
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2635596
    sget-object v2, LX/IzY;->b:LX/0U1;

    .line 2635597
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2635598
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2635599
    const-string v2, "transaction_payment_card_id"

    const/4 v3, 0x0

    const v4, -0x773d62f8

    invoke-static {v4}, LX/03h;->a(I)V

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v0, -0x405feee4

    invoke-static {v0}, LX/03h;->a(I)V

    .line 2635600
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2635601
    const v0, 0x3c6b7b28

    :try_start_3
    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2635602
    :goto_1
    const v0, 0x4f3fc9e5

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    .line 2635603
    :catch_0
    move-exception v0

    .line 2635604
    :try_start_4
    iget-object v2, p0, LX/Izm;->c:LX/03V;

    const-string v3, "DbInsertTransactionPaymentCardIdHandler"

    const-string v4, "A SQLException occurred when trying to insert into the database"

    invoke-virtual {v2, v3, v4, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2635605
    const v0, 0x684ea093

    :try_start_5
    invoke-static {v1, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    .line 2635606
    :catchall_0
    move-exception v0

    const v1, 0x5f777404

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 2635607
    :catchall_1
    move-exception v0

    const v2, 0x5f14937a

    invoke-static {v1, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method
