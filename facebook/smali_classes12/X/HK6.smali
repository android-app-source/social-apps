.class public final LX/HK6;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/HK7;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/HLO;

.field public b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

.field public c:LX/2km;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic d:LX/HK7;


# direct methods
.method public constructor <init>(LX/HK7;)V
    .locals 1

    .prologue
    .line 2453650
    iput-object p1, p0, LX/HK6;->d:LX/HK7;

    .line 2453651
    move-object v0, p1

    .line 2453652
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2453653
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2453654
    const-string v0, "PagePYMLCityRecommendationsComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2453655
    if-ne p0, p1, :cond_1

    .line 2453656
    :cond_0
    :goto_0
    return v0

    .line 2453657
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2453658
    goto :goto_0

    .line 2453659
    :cond_3
    check-cast p1, LX/HK6;

    .line 2453660
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2453661
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2453662
    if-eq v2, v3, :cond_0

    .line 2453663
    iget-object v2, p0, LX/HK6;->a:LX/HLO;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/HK6;->a:LX/HLO;

    iget-object v3, p1, LX/HK6;->a:LX/HLO;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2453664
    goto :goto_0

    .line 2453665
    :cond_5
    iget-object v2, p1, LX/HK6;->a:LX/HLO;

    if-nez v2, :cond_4

    .line 2453666
    :cond_6
    iget-object v2, p0, LX/HK6;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/HK6;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v3, p1, LX/HK6;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2453667
    goto :goto_0

    .line 2453668
    :cond_8
    iget-object v2, p1, LX/HK6;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    if-nez v2, :cond_7

    .line 2453669
    :cond_9
    iget-object v2, p0, LX/HK6;->c:LX/2km;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/HK6;->c:LX/2km;

    iget-object v3, p1, LX/HK6;->c:LX/2km;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2453670
    goto :goto_0

    .line 2453671
    :cond_a
    iget-object v2, p1, LX/HK6;->c:LX/2km;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
