.class public LX/IYi;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/IYi;


# instance fields
.field private final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2588029
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2588030
    iput-object p1, p0, LX/IYi;->a:LX/0Zb;

    .line 2588031
    return-void
.end method

.method public static a(LX/0QB;)LX/IYi;
    .locals 4

    .prologue
    .line 2588032
    sget-object v0, LX/IYi;->b:LX/IYi;

    if-nez v0, :cond_1

    .line 2588033
    const-class v1, LX/IYi;

    monitor-enter v1

    .line 2588034
    :try_start_0
    sget-object v0, LX/IYi;->b:LX/IYi;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2588035
    if-eqz v2, :cond_0

    .line 2588036
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2588037
    new-instance p0, LX/IYi;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/IYi;-><init>(LX/0Zb;)V

    .line 2588038
    move-object v0, p0

    .line 2588039
    sput-object v0, LX/IYi;->b:LX/IYi;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2588040
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2588041
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2588042
    :cond_1
    sget-object v0, LX/IYi;->b:LX/IYi;

    return-object v0

    .line 2588043
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2588044
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/IYh;)V
    .locals 3

    .prologue
    .line 2588045
    iget-object v0, p0, LX/IYi;->a:LX/0Zb;

    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    iget-object v2, p1, LX/IYh;->name:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "look_now"

    .line 2588046
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2588047
    move-object v1, v1

    .line 2588048
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2588049
    return-void
.end method

.method public final a(LX/IYh;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2588050
    iget-object v0, p0, LX/IYi;->a:LX/0Zb;

    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    iget-object v2, p1, LX/IYh;->name:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "look_now"

    .line 2588051
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2588052
    move-object v1, v1

    .line 2588053
    const-string v2, "error"

    invoke-virtual {v1, v2, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2588054
    return-void
.end method
