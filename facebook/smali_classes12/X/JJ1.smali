.class public LX/JJ1;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2678809
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2678810
    return-void
.end method

.method public static a(LX/0QB;)LX/JJ1;
    .locals 4

    .prologue
    .line 2678851
    const-class v1, LX/JJ1;

    monitor-enter v1

    .line 2678852
    :try_start_0
    sget-object v0, LX/JJ1;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2678853
    sput-object v2, LX/JJ1;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2678854
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2678855
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2678856
    new-instance v3, LX/JJ1;

    invoke-direct {v3}, LX/JJ1;-><init>()V

    .line 2678857
    const/16 p0, 0xafd

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    .line 2678858
    iput-object p0, v3, LX/JJ1;->a:LX/0Or;

    .line 2678859
    move-object v0, v3

    .line 2678860
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2678861
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JJ1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2678862
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2678863
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 11
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/SavedStateActions;
        .end annotation
    .end param

    .prologue
    .line 2678811
    const-string v0, "SAVE"

    invoke-static {p2, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "UNSAVE"

    invoke-static {p2, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2678812
    iget-object v0, p0, LX/JJ1;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tX;

    .line 2678813
    new-instance v1, LX/4HM;

    invoke-direct {v1}, LX/4HM;-><init>()V

    .line 2678814
    const-string v2, "node_id"

    invoke-virtual {v1, v2, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2678815
    move-object v1, v1

    .line 2678816
    const-string v2, "save_action"

    invoke-virtual {v1, v2, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2678817
    move-object v1, v1

    .line 2678818
    const-string v2, "profile_discovery_bucket"

    .line 2678819
    const-string v3, "surface"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2678820
    move-object v1, v1

    .line 2678821
    const-string v2, "actionbar_button"

    .line 2678822
    const-string v3, "mechanism"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2678823
    move-object v1, v1

    .line 2678824
    new-instance v2, LX/FTz;

    invoke-direct {v2}, LX/FTz;-><init>()V

    move-object v2, v2

    .line 2678825
    const-string v3, "input"

    invoke-virtual {v2, v3, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v1

    check-cast v1, LX/FTz;

    .line 2678826
    new-instance v2, LX/FSk;

    invoke-direct {v2}, LX/FSk;-><init>()V

    .line 2678827
    iput-object p1, v2, LX/FSk;->a:Ljava/lang/String;

    .line 2678828
    move-object v3, v2

    .line 2678829
    const-string v2, "SAVE"

    invoke-static {p2, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 2678830
    :goto_1
    iput-object v2, v3, LX/FSk;->b:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 2678831
    move-object v2, v3

    .line 2678832
    const/4 v8, 0x1

    const/4 v10, 0x0

    const/4 v6, 0x0

    .line 2678833
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 2678834
    iget-object v5, v2, LX/FSk;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 2678835
    iget-object v7, v2, LX/FSk;->b:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v4, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v7

    .line 2678836
    const/4 v9, 0x2

    invoke-virtual {v4, v9}, LX/186;->c(I)V

    .line 2678837
    invoke-virtual {v4, v10, v5}, LX/186;->b(II)V

    .line 2678838
    invoke-virtual {v4, v8, v7}, LX/186;->b(II)V

    .line 2678839
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 2678840
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 2678841
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 2678842
    invoke-virtual {v5, v10}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2678843
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2678844
    new-instance v5, Lcom/facebook/profile/discovery/protocol/DiscoveryCardSaveGraphQLModels$DiscoveryCardSaveFieldsModel;

    invoke-direct {v5, v4}, Lcom/facebook/profile/discovery/protocol/DiscoveryCardSaveGraphQLModels$DiscoveryCardSaveFieldsModel;-><init>(LX/15i;)V

    .line 2678845
    move-object v2, v5

    .line 2678846
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/399;->a(LX/0jT;)LX/399;

    move-result-object v1

    .line 2678847
    invoke-virtual {v0, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2678848
    return-void

    .line 2678849
    :cond_1
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 2678850
    :cond_2
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedState;->NOT_SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    goto :goto_1
.end method
