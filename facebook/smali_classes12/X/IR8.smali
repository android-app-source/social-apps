.class public final LX/IR8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Landroid/widget/EditText;

.field public final synthetic b:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 2576147
    iput-object p1, p0, LX/IR8;->b:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;

    iput-object p2, p0, LX/IR8;->a:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 2576148
    iget-object v0, p0, LX/IR8;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2576149
    iget-object v1, p0, LX/IR8;->b:Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;

    .line 2576150
    const-string v2, "%1$s %2$s %3$s"

    const/4 p0, 0x3

    new-array p0, p0, [Ljava/lang/Object;

    const/4 p1, 0x0

    iget-object p2, v1, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->t:Ljava/lang/String;

    aput-object p2, p0, p1

    const/4 p1, 0x1

    iget-object p2, v1, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->a:LX/0Or;

    invoke-interface {p2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p2

    aput-object p2, p0, p1

    const/4 p1, 0x2

    iget-object p2, v1, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->n:Ljava/lang/String;

    aput-object p2, p0, p1

    invoke-static {v2, p0}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    .line 2576151
    new-instance p1, LX/4Fv;

    invoke-direct {p1}, LX/4Fv;-><init>()V

    iget-object v2, v1, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2576152
    const-string p2, "actor_id"

    invoke-virtual {p1, p2, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2576153
    move-object v2, p1

    .line 2576154
    const-string p1, "client_mutation_id"

    invoke-virtual {v2, p1, p0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2576155
    move-object v2, v2

    .line 2576156
    iget-object p0, v1, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->s:Ljava/lang/String;

    .line 2576157
    const-string p1, "topic_id"

    invoke-virtual {v2, p1, p0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2576158
    move-object v2, v2

    .line 2576159
    const-string p0, "topic_name"

    invoke-virtual {v2, p0, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2576160
    move-object v2, v2

    .line 2576161
    iput-object v0, v1, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->t:Ljava/lang/String;

    .line 2576162
    iget-object p0, v1, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->x:LX/IRw;

    invoke-virtual {p0}, LX/DNC;->b()LX/1Cv;

    move-result-object p0

    .line 2576163
    instance-of p1, p0, LX/IRE;

    if-eqz p1, :cond_0

    .line 2576164
    check-cast p0, LX/IRE;

    iget-object p1, v1, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->t:Ljava/lang/String;

    invoke-virtual {p0, p1}, LX/IRE;->a(Ljava/lang/String;)V

    .line 2576165
    :cond_0
    iget-object p0, v1, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->w:LX/1ZF;

    iget-object p1, v1, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->t:Ljava/lang/String;

    invoke-interface {p0, p1}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2576166
    new-instance p0, LX/9TN;

    invoke-direct {p0}, LX/9TN;-><init>()V

    move-object p0, p0

    .line 2576167
    const-string p1, "input"

    invoke-virtual {p0, p1, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2576168
    invoke-static {p0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v2

    .line 2576169
    iget-object p0, v1, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->b:LX/0tX;

    invoke-virtual {p0, v2}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 2576170
    new-instance p0, LX/IRB;

    invoke-direct {p0, v1}, LX/IRB;-><init>(Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;)V

    iget-object p1, v1, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsFragment;->c:LX/0Tf;

    invoke-static {v2, p0, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2576171
    return-void
.end method
