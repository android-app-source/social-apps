.class public final LX/HyE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;)V
    .locals 0

    .prologue
    .line 2523736
    iput-object p1, p0, LX/HyE;->a:Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x2

    const v0, 0x20f6ff3f

    invoke-static {v2, v5, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2523737
    iget-object v0, p0, LX/HyE;->a:Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->j:Lcom/facebook/events/model/Event;

    if-nez v0, :cond_0

    .line 2523738
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Event model is not set, use bindModel() before"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    const v1, -0x4770f560

    invoke-static {v2, v2, v1, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    throw v0

    .line 2523739
    :cond_0
    iget-object v0, p0, LX/HyE;->a:Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->j:Lcom/facebook/events/model/Event;

    sget-object v1, LX/7vK;->ADMIN:LX/7vK;

    invoke-virtual {v0, v1}, Lcom/facebook/events/model/Event;->a(LX/7vK;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2523740
    iget-object v0, p0, LX/HyE;->a:Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;

    invoke-static {v0}, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->c(Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;)V

    .line 2523741
    :cond_1
    :goto_0
    const v0, 0x6e8150d4

    invoke-static {v0, v6}, LX/02F;->a(II)V

    return-void

    .line 2523742
    :cond_2
    iget-object v0, p0, LX/HyE;->a:Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->j:Lcom/facebook/events/model/Event;

    .line 2523743
    iget-boolean v1, v0, Lcom/facebook/events/model/Event;->y:Z

    move v0, v1

    .line 2523744
    if-nez v0, :cond_1

    iget-object v0, p0, LX/HyE;->a:Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->j:Lcom/facebook/events/model/Event;

    .line 2523745
    iget-boolean v1, v0, Lcom/facebook/events/model/Event;->B:Z

    move v0, v1

    .line 2523746
    if-eqz v0, :cond_1

    .line 2523747
    iget-object v0, p0, LX/HyE;->a:Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->a:LX/I7w;

    iget-object v1, p0, LX/HyE;->a:Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;

    iget-object v1, v1, Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;->j:Lcom/facebook/events/model/Event;

    invoke-virtual {v1}, Lcom/facebook/events/model/Event;->F()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v1

    iget-object v2, p0, LX/HyE;->a:Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;

    iget-object v3, p0, LX/HyE;->a:Lcom/facebook/events/dashboard/EventsDashboardRowRsvpStatusView;

    sget-object v4, Lcom/facebook/events/common/ActionMechanism;->DASHBOARD_ROW_GUEST_STATUS:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual/range {v0 .. v5}, LX/I7w;->a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Landroid/view/View;Landroid/view/View;Lcom/facebook/events/common/ActionMechanism;Z)V

    goto :goto_0
.end method
