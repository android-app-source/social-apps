.class public LX/I0D;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/HxA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/base/fragment/FbFragment;

.field public c:Lcom/facebook/widget/CustomLinearLayout;

.field public d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2526622
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2526623
    const-class v0, LX/I0D;

    invoke-static {v0, p0}, LX/I0D;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2526624
    const v0, 0x7f030540

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2526625
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/I0D;->setOrientation(I)V

    .line 2526626
    const v0, 0x7f0d0ecf

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v0, p0, LX/I0D;->c:Lcom/facebook/widget/CustomLinearLayout;

    .line 2526627
    const v0, 0x7f0d0ed1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    new-instance p1, LX/I0C;

    invoke-direct {p1, p0}, LX/I0C;-><init>(LX/I0D;)V

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2526628
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/I0D;->d:Ljava/util/Set;

    .line 2526629
    return-void
.end method

.method public static a(LX/I0D;Ljava/util/List;)V
    .locals 12
    .param p0    # LX/I0D;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 2526630
    iget-object v0, p0, LX/I0D;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 2526631
    iget-object v0, p0, LX/I0D;->c:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0}, Lcom/facebook/widget/CustomLinearLayout;->removeAllViews()V

    .line 2526632
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;

    .line 2526633
    iget-object v2, p0, LX/I0D;->d:Ljava/util/Set;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;->m()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2526634
    iget-object v2, p0, LX/I0D;->c:Lcom/facebook/widget/CustomLinearLayout;

    .line 2526635
    new-instance v6, LX/HxG;

    invoke-virtual {p0}, LX/I0D;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v6, v7}, LX/HxG;-><init>(Landroid/content/Context;)V

    .line 2526636
    iget-object v8, p0, LX/I0D;->b:Lcom/facebook/base/fragment/FbFragment;

    const/4 v9, 0x1

    const/4 v10, 0x0

    iget-object v11, p0, LX/I0D;->e:Ljava/lang/String;

    move-object v7, v0

    invoke-virtual/range {v6 .. v11}, LX/HxG;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;Lcom/facebook/base/fragment/FbFragment;ZZLjava/lang/String;)V

    .line 2526637
    move-object v0, v6

    .line 2526638
    invoke-virtual {v2, v0}, Lcom/facebook/widget/CustomLinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 2526639
    :cond_0
    iget-object v0, p0, LX/I0D;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2526640
    iget-object v0, p0, LX/I0D;->c:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0, v5}, Lcom/facebook/widget/CustomLinearLayout;->setVisibility(I)V

    .line 2526641
    const v0, 0x7f0d0ed0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2526642
    :goto_1
    return-void

    .line 2526643
    :cond_1
    iget-object v0, p0, LX/I0D;->c:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0, v4}, Lcom/facebook/widget/CustomLinearLayout;->setVisibility(I)V

    .line 2526644
    const v0, 0x7f0d0ed0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/I0D;

    invoke-static {p0}, LX/HxA;->b(LX/0QB;)LX/HxA;

    move-result-object p0

    check-cast p0, LX/HxA;

    iput-object p0, p1, LX/I0D;->a:LX/HxA;

    return-void
.end method
