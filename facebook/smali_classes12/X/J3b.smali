.class public LX/J3b;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Du;


# instance fields
.field private final a:LX/0Tf;

.field public b:LX/6Ex;


# direct methods
.method public constructor <init>(LX/0Tf;)V
    .locals 0
    .param p1    # LX/0Tf;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2643069
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2643070
    iput-object p1, p0, LX/J3b;->a:LX/0Tf;

    .line 2643071
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 13

    .prologue
    .line 2643072
    iget-object v0, p0, LX/J3b;->a:LX/0Tf;

    new-instance v1, Lcom/facebook/payments/sample/checkout/PaymentsFlowSampleCheckoutSender$1;

    invoke-direct {v1, p0}, Lcom/facebook/payments/sample/checkout/PaymentsFlowSampleCheckoutSender$1;-><init>(LX/J3b;)V

    const-wide/16 v2, 0x3

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, LX/0Tf;->a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)LX/0YG;

    move-result-object v0

    .line 2643073
    invoke-interface {p1}, Lcom/facebook/payments/checkout/model/CheckoutData;->a()Lcom/facebook/payments/checkout/CheckoutCommonParams;

    move-result-object v5

    iget-boolean v5, v5, Lcom/facebook/payments/checkout/CheckoutCommonParams;->n:Z

    if-nez v5, :cond_1

    .line 2643074
    :cond_0
    return-object v0

    .line 2643075
    :cond_1
    new-instance v6, Landroid/os/Handler;

    invoke-direct {v6}, Landroid/os/Handler;-><init>()V

    .line 2643076
    const/4 v5, 0x1

    :goto_0
    const/4 v7, 0x3

    if-gt v5, v7, :cond_0

    .line 2643077
    new-instance v7, Lcom/facebook/payments/sample/checkout/PaymentsFlowSampleCheckoutSender$2;

    invoke-direct {v7, p0, v5}, Lcom/facebook/payments/sample/checkout/PaymentsFlowSampleCheckoutSender$2;-><init>(LX/J3b;I)V

    int-to-long v9, v5

    const-wide/16 v11, 0x3e8

    mul-long/2addr v9, v11

    const v8, 0x274e46f0

    invoke-static {v6, v7, v9, v10, v8}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2643078
    add-int/lit8 v5, v5, 0x1

    goto :goto_0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 2643079
    const/4 v0, 0x0

    iput-object v0, p0, LX/J3b;->b:LX/6Ex;

    .line 2643080
    return-void
.end method

.method public final a(LX/6Ex;)V
    .locals 0

    .prologue
    .line 2643081
    iput-object p1, p0, LX/J3b;->b:LX/6Ex;

    .line 2643082
    return-void
.end method

.method public final a(LX/6qh;)V
    .locals 0

    .prologue
    .line 2643083
    return-void
.end method

.method public final b(Lcom/facebook/payments/checkout/model/CheckoutData;)Z
    .locals 1

    .prologue
    .line 2643084
    const/4 v0, 0x0

    return v0
.end method

.method public final c(Lcom/facebook/payments/checkout/model/CheckoutData;)Z
    .locals 1

    .prologue
    .line 2643085
    const/4 v0, 0x0

    return v0
.end method

.method public final d(Lcom/facebook/payments/checkout/model/CheckoutData;)V
    .locals 0

    .prologue
    .line 2643086
    return-void
.end method
