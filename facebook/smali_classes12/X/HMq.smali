.class public final LX/HMq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/services/PagesServicesFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/services/PagesServicesFragment;)V
    .locals 0

    .prologue
    .line 2457619
    iput-object p1, p0, LX/HMq;->a:Lcom/facebook/pages/common/services/PagesServicesFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2457620
    iget-object v0, p0, LX/HMq;->a:Lcom/facebook/pages/common/services/PagesServicesFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/services/PagesServicesFragment;->s:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/HMq;->a:Lcom/facebook/pages/common/services/PagesServicesFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/services/PagesServicesFragment;->s:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    move v1, v0

    .line 2457621
    :goto_0
    iget-object v0, p0, LX/HMq;->a:Lcom/facebook/pages/common/services/PagesServicesFragment;

    iget-boolean v0, v0, Lcom/facebook/pages/common/services/PagesServicesFragment;->J:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HMq;->a:Lcom/facebook/pages/common/services/PagesServicesFragment;

    iget v0, v0, Lcom/facebook/pages/common/services/PagesServicesFragment;->K:I

    if-eq v0, v1, :cond_1

    .line 2457622
    :cond_0
    iget-object v0, p0, LX/HMq;->a:Lcom/facebook/pages/common/services/PagesServicesFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/services/PagesServicesFragment;->B:Landroid/view/View;

    iget-object v3, p0, LX/HMq;->a:Lcom/facebook/pages/common/services/PagesServicesFragment;

    iget-object v3, v3, Lcom/facebook/pages/common/services/PagesServicesFragment;->s:Lcom/facebook/widget/listview/BetterListView;

    sget-object v4, Lcom/facebook/pages/common/services/PagesServicesFragment;->I:[I

    iget-object v5, p0, LX/HMq;->a:Lcom/facebook/pages/common/services/PagesServicesFragment;

    iget-object v5, v5, Lcom/facebook/pages/common/services/PagesServicesFragment;->g:LX/0hB;

    invoke-virtual {v5}, LX/0hB;->d()I

    move-result v5

    invoke-static {v0, v3, p2, v4, v5}, LX/8FX;->a(Landroid/view/View;Landroid/view/ViewGroup;I[II)LX/3rL;

    move-result-object v3

    .line 2457623
    iget-object v4, p0, LX/HMq;->a:Lcom/facebook/pages/common/services/PagesServicesFragment;

    iget-object v0, v3, LX/3rL;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 2457624
    iput-boolean v0, v4, Lcom/facebook/pages/common/services/PagesServicesFragment;->J:Z

    .line 2457625
    iget-object v0, p0, LX/HMq;->a:Lcom/facebook/pages/common/services/PagesServicesFragment;

    iget-boolean v0, v0, Lcom/facebook/pages/common/services/PagesServicesFragment;->J:Z

    if-eqz v0, :cond_1

    .line 2457626
    iget-object v4, p0, LX/HMq;->a:Lcom/facebook/pages/common/services/PagesServicesFragment;

    iget-object v0, v3, LX/3rL;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v4, v0}, Lcom/facebook/pages/common/services/PagesServicesFragment;->E_(I)V

    .line 2457627
    iget-object v0, p0, LX/HMq;->a:Lcom/facebook/pages/common/services/PagesServicesFragment;

    .line 2457628
    iput v1, v0, Lcom/facebook/pages/common/services/PagesServicesFragment;->K:I

    .line 2457629
    :cond_1
    iget-object v0, p0, LX/HMq;->a:Lcom/facebook/pages/common/services/PagesServicesFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/services/PagesServicesFragment;->s:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/BetterListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2457630
    iget-object v0, p0, LX/HMq;->a:Lcom/facebook/pages/common/services/PagesServicesFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/services/PagesServicesFragment;->s:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/BetterListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    .line 2457631
    iget-object v1, p0, LX/HMq;->a:Lcom/facebook/pages/common/services/PagesServicesFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/services/PagesServicesFragment;->C:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/HMq;->a:Lcom/facebook/pages/common/services/PagesServicesFragment;

    iget v1, v1, Lcom/facebook/pages/common/services/PagesServicesFragment;->F:I

    if-eq v1, v0, :cond_2

    iget-object v1, p0, LX/HMq;->a:Lcom/facebook/pages/common/services/PagesServicesFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2457632
    iget-object v1, p0, LX/HMq;->a:Lcom/facebook/pages/common/services/PagesServicesFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/services/PagesServicesFragment;->C:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v2, p0, LX/HMq;->a:Lcom/facebook/pages/common/services/PagesServicesFragment;

    iget-object v2, v2, Lcom/facebook/pages/common/services/PagesServicesFragment;->s:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v1, v2, p2}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->a(Landroid/view/ViewGroup;I)V

    .line 2457633
    iget-object v1, p0, LX/HMq;->a:Lcom/facebook/pages/common/services/PagesServicesFragment;

    .line 2457634
    iput v0, v1, Lcom/facebook/pages/common/services/PagesServicesFragment;->F:I

    .line 2457635
    :cond_2
    return-void

    :cond_3
    move v1, v2

    .line 2457636
    goto :goto_0
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 2457637
    return-void
.end method
