.class public final LX/JCk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/JA0;

.field public final synthetic b:Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;LX/JA0;)V
    .locals 0

    .prologue
    .line 2662767
    iput-object p1, p0, LX/JCk;->b:Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;

    iput-object p2, p0, LX/JCk;->a:LX/JA0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x29bfcde1

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2662768
    sget-object v1, LX/JCp;->a:[I

    iget-object v2, p0, LX/JCk;->a:LX/JA0;

    invoke-virtual {v2}, LX/JA0;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2662769
    iget-object v1, p0, LX/JCk;->b:Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;

    iget-object v1, v1, Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;->d:LX/03V;

    const-string v2, "ListCollectionItemView"

    const-string v3, "unknown friendship status"

    invoke-virtual {v1, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2662770
    :goto_0
    const v1, 0x418ba33d

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2662771
    :pswitch_0
    iget-object v1, p0, LX/JCk;->b:Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;

    iget-object v2, p0, LX/JCk;->a:LX/JA0;

    .line 2662772
    invoke-static {v1, v2}, Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;->b$redex0(Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;LX/JA0;)V

    .line 2662773
    goto :goto_0

    .line 2662774
    :pswitch_1
    iget-object v1, p0, LX/JCk;->b:Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;

    iget-object v2, p0, LX/JCk;->a:LX/JA0;

    .line 2662775
    invoke-static {v1, v2}, Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;->c$redex0(Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;LX/JA0;)V

    .line 2662776
    goto :goto_0

    .line 2662777
    :pswitch_2
    iget-object v1, p0, LX/JCk;->b:Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;

    iget-object v2, p0, LX/JCk;->a:LX/JA0;

    invoke-static {v1, v2}, Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;->d(Lcom/facebook/timeline/aboutpage/views/CollectionFriendingButton;LX/JA0;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
