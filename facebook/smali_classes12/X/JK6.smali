.class public LX/JK6;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/JK6;


# instance fields
.field public final a:LX/0So;

.field public final b:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field public final c:LX/0ad;


# direct methods
.method public constructor <init>(LX/0So;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2680223
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2680224
    iput-object p1, p0, LX/JK6;->a:LX/0So;

    .line 2680225
    iput-object p2, p0, LX/JK6;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 2680226
    iput-object p3, p0, LX/JK6;->c:LX/0ad;

    .line 2680227
    return-void
.end method

.method public static a(LX/0QB;)LX/JK6;
    .locals 6

    .prologue
    .line 2680210
    sget-object v0, LX/JK6;->d:LX/JK6;

    if-nez v0, :cond_1

    .line 2680211
    const-class v1, LX/JK6;

    monitor-enter v1

    .line 2680212
    :try_start_0
    sget-object v0, LX/JK6;->d:LX/JK6;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2680213
    if-eqz v2, :cond_0

    .line 2680214
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2680215
    new-instance p0, LX/JK6;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v3

    check-cast v3, LX/0So;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v4

    check-cast v4, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-direct {p0, v3, v4, v5}, LX/JK6;-><init>(LX/0So;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0ad;)V

    .line 2680216
    move-object v0, p0

    .line 2680217
    sput-object v0, LX/JK6;->d:LX/JK6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2680218
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2680219
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2680220
    :cond_1
    sget-object v0, LX/JK6;->d:LX/JK6;

    return-object v0

    .line 2680221
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2680222
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/JK6;IJJJLjava/lang/String;)V
    .locals 10

    .prologue
    const v8, 0x60018

    const/4 v5, 0x2

    const/4 v4, 0x0

    const-wide/16 v6, -0x1

    .line 2680200
    const/4 v2, -0x1

    if-eq p1, v2, :cond_0

    .line 2680201
    cmp-long v2, p2, v6

    if-eqz v2, :cond_1

    cmp-long v2, p4, v6

    if-eqz v2, :cond_1

    .line 2680202
    iget-object v2, p0, LX/JK6;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v2, p1, v4, p2, p3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->markerStart(IIJ)V

    .line 2680203
    iget-object v2, p0, LX/JK6;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    move v3, p1

    move-wide v6, p4

    invoke-interface/range {v2 .. v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->markerEnd(IISJ)V

    .line 2680204
    iget-object v2, p0, LX/JK6;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    sub-long v4, p4, p2

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p8

    invoke-interface {v2, v8, v0, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 2680205
    :cond_0
    :goto_0
    return-void

    .line 2680206
    :cond_1
    cmp-long v2, p6, v6

    if-eqz v2, :cond_2

    .line 2680207
    iget-object v2, p0, LX/JK6;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-wide/from16 v0, p6

    long-to-int v3, v0

    invoke-interface {v2, p1, v5, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ISI)V

    .line 2680208
    iget-object v2, p0, LX/JK6;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static/range {p6 .. p7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p8

    invoke-interface {v2, v8, v0, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2680209
    :cond_2
    const-string v2, "React"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Marker "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p8

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " has incomplete data "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, p6

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(LX/JK6;JILjava/lang/String;)V
    .locals 5

    .prologue
    .line 2680195
    iget-object v0, p0, LX/JK6;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v1, 0x0

    invoke-interface {v0, p3, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(II)J

    move-result-wide v0

    .line 2680196
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 2680197
    iget-object v2, p0, LX/JK6;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x60018

    sub-long v0, p1, v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v3, p4, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 2680198
    :cond_0
    iget-object v0, p0, LX/JK6;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v1, 0x2

    invoke-interface {v0, p3, v1, p1, p2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ISJ)V

    .line 2680199
    return-void
.end method

.method private static a(LX/JK6;LX/5pG;Ljava/lang/String;I)V
    .locals 9

    .prologue
    const-wide/16 v0, -0x1

    .line 2680186
    invoke-interface {p1, p2}, LX/5pG;->a(Ljava/lang/String;)LX/5pG;

    move-result-object v6

    .line 2680187
    const-string v2, "startTime"

    invoke-interface {v6, v2}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "startTime"

    invoke-interface {v6, v2}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    double-to-long v2, v2

    .line 2680188
    :goto_0
    const-string v4, "endTime"

    invoke-interface {v6, v4}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "endTime"

    invoke-interface {v6, v4}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    double-to-long v4, v4

    .line 2680189
    :goto_1
    const-string v7, "totalTime"

    invoke-interface {v6, v7}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    const-string v0, "totalTime"

    invoke-interface {v6, v0}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    double-to-long v6, v0

    :goto_2
    move-object v0, p0

    move v1, p3

    move-object v8, p2

    .line 2680190
    invoke-static/range {v0 .. v8}, LX/JK6;->a(LX/JK6;IJJJLjava/lang/String;)V

    .line 2680191
    return-void

    :cond_0
    move-wide v2, v0

    .line 2680192
    goto :goto_0

    :cond_1
    move-wide v4, v0

    .line 2680193
    goto :goto_1

    :cond_2
    move-wide v6, v0

    .line 2680194
    goto :goto_2
.end method

.method private static c(LX/JK6;LX/5pG;)V
    .locals 14

    .prologue
    const-wide/16 v10, -0x1

    .line 2680132
    const-string v0, "fetchRelayQuery"

    invoke-interface {p1, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2680133
    const-string v0, "fetchRelayQuery"

    const v1, 0x6001d

    invoke-static {p0, p1, v0, v1}, LX/JK6;->a(LX/JK6;LX/5pG;Ljava/lang/String;I)V

    .line 2680134
    :cond_0
    const-string v0, "fetchRelayCache"

    invoke-interface {p1, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2680135
    const-string v0, "fetchRelayCache"

    const v1, 0x6001e

    invoke-static {p0, p1, v0, v1}, LX/JK6;->a(LX/JK6;LX/5pG;Ljava/lang/String;I)V

    .line 2680136
    :cond_1
    invoke-interface {p1}, LX/5pG;->a()Lcom/facebook/react/bridge/ReadableMapKeySetIterator;

    move-result-object v9

    .line 2680137
    :cond_2
    :goto_0
    invoke-interface {v9}, Lcom/facebook/react/bridge/ReadableMapKeySetIterator;->hasNextKey()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2680138
    invoke-interface {v9}, Lcom/facebook/react/bridge/ReadableMapKeySetIterator;->nextKey()Ljava/lang/String;

    move-result-object v8

    .line 2680139
    invoke-interface {p1, v8}, LX/5pG;->a(Ljava/lang/String;)LX/5pG;

    move-result-object v0

    .line 2680140
    const-string v1, "startTime"

    invoke-interface {v0, v1}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "startTime"

    invoke-interface {v0, v1}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    double-to-long v2, v2

    .line 2680141
    :goto_1
    const-string v1, "endTime"

    invoke-interface {v0, v1}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "endTime"

    invoke-interface {v0, v1}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    double-to-long v4, v4

    .line 2680142
    :goto_2
    const-string v1, "totalTime"

    invoke-interface {v0, v1}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    const-string v1, "totalTime"

    invoke-interface {v0, v1}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    double-to-long v6, v0

    .line 2680143
    :goto_3
    const/4 v1, -0x1

    .line 2680144
    const-string v0, "JSAppRequireTime"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2680145
    const v1, 0x6001a

    .line 2680146
    cmp-long v0, v2, v10

    if-eqz v0, :cond_3

    .line 2680147
    const v0, 0x60019

    const-string v12, "nativeTimeBeforeJS"

    invoke-static {p0, v2, v3, v0, v12}, LX/JK6;->a(LX/JK6;JILjava/lang/String;)V

    :cond_3
    :goto_4
    move-object v0, p0

    .line 2680148
    invoke-static/range {v0 .. v8}, LX/JK6;->a(LX/JK6;IJJJLjava/lang/String;)V

    goto :goto_0

    :cond_4
    move-wide v2, v10

    .line 2680149
    goto :goto_1

    :cond_5
    move-wide v4, v10

    .line 2680150
    goto :goto_2

    :cond_6
    move-wide v6, v10

    .line 2680151
    goto :goto_3

    .line 2680152
    :cond_7
    const-string v0, "JSTime"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2680153
    const v1, 0x6001b

    .line 2680154
    cmp-long v0, v4, v10

    if-eqz v0, :cond_3

    .line 2680155
    iget-object v0, p0, LX/JK6;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v12, 0x6001c

    const/4 v13, 0x0

    invoke-interface {v0, v12, v13, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->markerStart(IIJ)V

    goto :goto_4

    .line 2680156
    :cond_8
    const-string v0, "fetchRelayQuery"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2680157
    const-string v0, "fetchRelayCache"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2680158
    const-string v0, "React"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Unknown event logged: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v0, v12}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 2680159
    :cond_9
    return-void
.end method

.method private static d(LX/JK6;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2680185
    iget-object v1, p0, LX/JK6;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x60018

    invoke-interface {v1, v2, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(II)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method


# virtual methods
.method public final a(JJ)V
    .locals 7

    .prologue
    .line 2680180
    invoke-static {p0}, LX/JK6;->d(LX/JK6;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2680181
    iget-object v0, p0, LX/JK6;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x6001f

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2, p3, p4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->markerStart(IIJ)V

    .line 2680182
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    .line 2680183
    iget-object v0, p0, LX/JK6;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x60018

    const-string v2, "bridgeStartupTime"

    sub-long v4, p3, p1

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 2680184
    :cond_0
    return-void
.end method

.method public final a(LX/5pG;)V
    .locals 14

    .prologue
    .line 2680160
    const-string v0, "tag"

    invoke-interface {p1, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "tag"

    invoke-interface {p1, v0}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2680161
    :goto_0
    iget-object v1, p0, LX/JK6;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x60018

    const-string v3, "tag"

    invoke-interface {v1, v2, v3, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 2680162
    const-string v0, "extras"

    invoke-interface {p1, v0}, LX/5pG;->a(Ljava/lang/String;)LX/5pG;

    move-result-object v0

    const v9, 0x60018

    .line 2680163
    const-string v4, "fetchedFromCache"

    invoke-interface {v0, v4}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v4, "fetchedFromCache"

    invoke-interface {v0, v4}, LX/5pG;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x1

    .line 2680164
    :goto_1
    iget-object v5, p0, LX/JK6;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v6, "cacheFetch"

    if-eqz v4, :cond_3

    const-string v4, "true"

    :goto_2
    invoke-interface {v5, v9, v6, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 2680165
    const-string v4, "bytecodeCacheStats"

    invoke-interface {v0, v4}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2680166
    iget-object v4, p0, LX/JK6;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v5, "bytecodeCacheStats"

    invoke-interface {v0, v5}, LX/5pG;->a(Ljava/lang/String;)LX/5pG;

    move-result-object v5

    .line 2680167
    const-string v10, "bytecode_cache_size_on_start"

    const-string v11, "size_on_start"

    invoke-interface {v5, v11}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v11

    invoke-interface {v4, v9, v10, v11}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 2680168
    const-string v10, "bytecode_cache_current_size"

    const-string v11, "current_size"

    invoke-interface {v5, v11}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v11

    invoke-interface {v4, v9, v10, v11}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 2680169
    const-string v10, "bytecode_cache_commits"

    const-string v11, "commits"

    invoke-interface {v5, v11}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v11

    invoke-interface {v4, v9, v10, v11}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 2680170
    const-string v10, "bytecode_cache_drops"

    const-string v11, "drops"

    invoke-interface {v5, v11}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v11

    invoke-interface {v4, v9, v10, v11}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 2680171
    const-string v10, "bytecode_cache_writes"

    const-string v11, "writes"

    invoke-interface {v5, v11}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v11

    invoke-interface {v4, v9, v10, v11}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 2680172
    const-string v10, "bytecode_cache_reads"

    const-string v11, "reads"

    invoke-interface {v5, v11}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v11

    invoke-interface {v4, v9, v10, v11}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 2680173
    const-string v10, "bytecode_cache_read_only"

    const-string v11, "read_only"

    invoke-interface {v5, v11}, LX/5pG;->getBoolean(Ljava/lang/String;)Z

    move-result v11

    invoke-static {v11}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v11

    invoke-interface {v4, v9, v10, v11}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 2680174
    :cond_0
    iget-object v4, p0, LX/JK6;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v5, "qe_group"

    iget-object v6, p0, LX/JK6;->c:LX/0ad;

    sget-char v7, LX/347;->E:C

    const/4 v8, 0x0

    invoke-interface {v6, v7, v8}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v9, v5, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 2680175
    const-string v0, "timespans"

    invoke-interface {p1, v0}, LX/5pG;->a(Ljava/lang/String;)LX/5pG;

    move-result-object v0

    invoke-static {p0, v0}, LX/JK6;->c(LX/JK6;LX/5pG;)V

    .line 2680176
    return-void

    .line 2680177
    :cond_1
    const-string v0, "None"

    goto/16 :goto_0

    .line 2680178
    :cond_2
    const/4 v4, 0x0

    goto/16 :goto_1

    .line 2680179
    :cond_3
    const-string v4, "false"

    goto/16 :goto_2
.end method
