.class public final enum LX/Ilc;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Ilc;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Ilc;

.field public static final enum CANCELED:LX/Ilc;

.field public static final enum COMPLETED:LX/Ilc;

.field public static final enum PENDING:LX/Ilc;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2607908
    new-instance v0, LX/Ilc;

    const-string v1, "PENDING"

    invoke-direct {v0, v1, v2}, LX/Ilc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ilc;->PENDING:LX/Ilc;

    .line 2607909
    new-instance v0, LX/Ilc;

    const-string v1, "CANCELED"

    invoke-direct {v0, v1, v3}, LX/Ilc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ilc;->CANCELED:LX/Ilc;

    .line 2607910
    new-instance v0, LX/Ilc;

    const-string v1, "COMPLETED"

    invoke-direct {v0, v1, v4}, LX/Ilc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ilc;->COMPLETED:LX/Ilc;

    .line 2607911
    const/4 v0, 0x3

    new-array v0, v0, [LX/Ilc;

    sget-object v1, LX/Ilc;->PENDING:LX/Ilc;

    aput-object v1, v0, v2

    sget-object v1, LX/Ilc;->CANCELED:LX/Ilc;

    aput-object v1, v0, v3

    sget-object v1, LX/Ilc;->COMPLETED:LX/Ilc;

    aput-object v1, v0, v4

    sput-object v0, LX/Ilc;->$VALUES:[LX/Ilc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2607907
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Ilc;
    .locals 1

    .prologue
    .line 2607905
    const-class v0, LX/Ilc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Ilc;

    return-object v0
.end method

.method public static values()[LX/Ilc;
    .locals 1

    .prologue
    .line 2607906
    sget-object v0, LX/Ilc;->$VALUES:[LX/Ilc;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Ilc;

    return-object v0
.end method
