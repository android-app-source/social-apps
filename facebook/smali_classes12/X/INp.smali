.class public final LX/INp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

.field public final synthetic b:LX/IRb;

.field public final synthetic c:LX/INs;


# direct methods
.method public constructor <init>(LX/INs;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;LX/IRb;)V
    .locals 0

    .prologue
    .line 2571134
    iput-object p1, p0, LX/INp;->c:LX/INs;

    iput-object p2, p0, LX/INp;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    iput-object p3, p0, LX/INp;->b:LX/IRb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0xd1740cc

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2571135
    iget-object v1, p0, LX/INp;->c:LX/INs;

    iget-object v2, p0, LX/INp;->a:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    iget-object v3, p0, LX/INp;->b:LX/IRb;

    .line 2571136
    if-eqz v2, :cond_0

    if-nez v3, :cond_2

    .line 2571137
    :cond_0
    iget-object v5, v1, LX/INs;->h:LX/03V;

    sget-object v6, LX/INs;->j:Ljava/lang/Class;

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "groupInformation is null in sendJoinGroupRequest"

    invoke-virtual {v5, v6, v7}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2571138
    :cond_1
    :goto_0
    const v1, -0x51be0ea5

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2571139
    :cond_2
    invoke-interface {v2}, LX/9N1;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v5

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CAN_JOIN:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-eq v5, v6, :cond_3

    invoke-interface {v2}, LX/9N1;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v5

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-ne v5, v6, :cond_1

    .line 2571140
    :cond_3
    const-string v5, "send_join_request_to_group"

    invoke-interface {v2}, LX/9N1;->l()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v5, v6}, LX/INs;->a$redex0(LX/INs;Ljava/lang/String;Ljava/lang/String;)V

    .line 2571141
    iget-object v5, v1, LX/INs;->g:LX/3mF;

    invoke-interface {v2}, LX/9N1;->l()Ljava/lang/String;

    move-result-object v6

    const-string v7, "mobile_group_join"

    invoke-virtual {v5, v6, v7}, LX/3mF;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    .line 2571142
    invoke-static {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->a(LX/9N6;)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-result-object v7

    .line 2571143
    invoke-static {v7}, LX/9OQ;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)LX/9OQ;

    move-result-object p0

    .line 2571144
    invoke-interface {v2}, LX/9N1;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v5

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CAN_JOIN:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-ne v5, p1, :cond_4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    :goto_1
    iput-object v5, p0, LX/9OQ;->F:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 2571145
    invoke-virtual {p0}, LX/9OQ;->a()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    move-result-object v5

    .line 2571146
    invoke-interface {v3, v7, v5}, LX/IRb;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V

    .line 2571147
    iget-object p0, v1, LX/INs;->f:LX/0Sh;

    new-instance p1, LX/INq;

    invoke-direct {p1, v1, v3, v5, v7}, LX/INq;-><init>(LX/INs;LX/IRb;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)V

    invoke-virtual {p0, v6, p1}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto :goto_0

    .line 2571148
    :cond_4
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    goto :goto_1
.end method
