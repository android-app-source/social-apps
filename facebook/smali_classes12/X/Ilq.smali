.class public final LX/Ilq;
.super LX/2h1;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2h1",
        "<",
        "Lcom/facebook/payments/p2p/service/model/verification/VerifyPaymentResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;)V
    .locals 0

    .prologue
    .line 2608309
    iput-object p1, p0, LX/Ilq;->a:Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;

    invoke-direct {p0}, LX/2h1;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 5

    .prologue
    .line 2608310
    iget-object v0, p0, LX/Ilq;->a:Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;

    iget-object v0, v0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->l:Lcom/facebook/ui/dialogs/ProgressDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2608311
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v0, v0

    .line 2608312
    sget-object v1, LX/1nY;->CONNECTION_FAILURE:LX/1nY;

    if-ne v0, v1, :cond_0

    .line 2608313
    iget-object v0, p0, LX/Ilq;->a:Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/6up;->a(Landroid/content/Context;)V

    .line 2608314
    :goto_0
    return-void

    .line 2608315
    :cond_0
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v0, v0

    .line 2608316
    sget-object v1, LX/1nY;->API_ERROR:LX/1nY;

    if-eq v0, v1, :cond_1

    .line 2608317
    iget-object v0, p0, LX/Ilq;->a:Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f082cca

    .line 2608318
    sget-object v2, LX/6up;->a:Landroid/content/DialogInterface$OnClickListener;

    invoke-static {v0, v1, v2}, LX/6up;->a(Landroid/content/Context;ILandroid/content/DialogInterface$OnClickListener;)V

    .line 2608319
    goto :goto_0

    .line 2608320
    :cond_1
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2608321
    invoke-virtual {v0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/http/protocol/ApiErrorResult;

    .line 2608322
    iget-object v1, p0, LX/Ilq;->a:Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/Ilq;->a:Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;

    const v3, 0x7f082cca

    invoke-virtual {v2, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/http/protocol/ApiErrorResult;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/http/protocol/ApiErrorResult;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, LX/Ilq;->a:Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;

    const v4, 0x7f080016

    invoke-virtual {v3, v4}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/Ilp;

    invoke-direct {v4, p0}, LX/Ilp;-><init>(LX/Ilq;)V

    invoke-static {v1, v2, v0, v3, v4}, LX/Gza;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2608323
    check-cast p1, Lcom/facebook/payments/p2p/service/model/verification/VerifyPaymentResult;

    .line 2608324
    iget-object v0, p0, LX/Ilq;->a:Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;

    iget-object v0, v0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->l:Lcom/facebook/ui/dialogs/ProgressDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2608325
    invoke-virtual {p1}, Lcom/facebook/payments/p2p/service/model/verification/VerifyPaymentResult;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2608326
    iget-object v0, p0, LX/Ilq;->a:Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;

    invoke-virtual {p1}, Lcom/facebook/payments/p2p/service/model/verification/VerifyPaymentResult;->b()Ljava/lang/String;

    move-result-object v1

    .line 2608327
    iput-object v1, v0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->p:Ljava/lang/String;

    .line 2608328
    iget-object v0, p0, LX/Ilq;->a:Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;

    invoke-static {v0}, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->k(Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;)V

    .line 2608329
    :goto_0
    return-void

    .line 2608330
    :cond_0
    iget-object v0, p0, LX/Ilq;->a:Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;

    invoke-virtual {p1}, Lcom/facebook/payments/p2p/service/model/verification/VerifyPaymentResult;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/DuZ;->fromString(Ljava/lang/String;)LX/DuZ;

    move-result-object v1

    .line 2608331
    iput-object v1, v0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->n:LX/DuZ;

    .line 2608332
    iget-object v0, p0, LX/Ilq;->a:Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;

    invoke-virtual {p1}, Lcom/facebook/payments/p2p/service/model/verification/VerifyPaymentResult;->d()Lcom/facebook/payments/p2p/model/verification/ScreenData;

    move-result-object v1

    .line 2608333
    iput-object v1, v0, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->o:Lcom/facebook/payments/p2p/model/verification/ScreenData;

    .line 2608334
    iget-object v0, p0, LX/Ilq;->a:Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;

    invoke-static {v0}, Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;->c(Lcom/facebook/messaging/payment/prefs/verification/PaymentRiskVerificationControllerFragment;)V

    goto :goto_0
.end method
