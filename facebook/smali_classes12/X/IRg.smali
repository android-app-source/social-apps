.class public final LX/IRg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0zS;

.field public final synthetic b:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;LX/0zS;)V
    .locals 0

    .prologue
    .line 2576619
    iput-object p1, p0, LX/IRg;->b:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    iput-object p2, p0, LX/IRg;->a:LX/0zS;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2576620
    iget-object v0, p0, LX/IRg;->b:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    iget-object v1, v0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->b:LX/DO3;

    iget-object v0, p0, LX/IRg;->b:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    iget-object v2, v0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ah:Ljava/lang/String;

    iget-object v0, p0, LX/IRg;->b:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ac:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v3, p0, LX/IRg;->a:LX/0zS;

    iget-object v4, p0, LX/IRg;->b:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    invoke-virtual {v4}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v1, v2, v0, v3, v4}, LX/DO3;->a(Ljava/lang/String;Ljava/lang/String;LX/0zS;I)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
