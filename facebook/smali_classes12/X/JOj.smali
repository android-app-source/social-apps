.class public LX/JOj;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final a:LX/11T;

.field private final b:LX/8ye;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/3AZ;

.field private final e:LX/0W9;

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JPN;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/JPX;


# direct methods
.method public constructor <init>(LX/11T;LX/8ye;LX/0Ot;LX/3AZ;LX/0W9;LX/0Ot;LX/JPX;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/11T;",
            "LX/8ye;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;",
            "LX/3AZ;",
            "LX/0W9;",
            "LX/0Ot",
            "<",
            "LX/JPN;",
            ">;",
            "LX/JPX;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2688077
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2688078
    iput-object p1, p0, LX/JOj;->a:LX/11T;

    .line 2688079
    iput-object p2, p0, LX/JOj;->b:LX/8ye;

    .line 2688080
    iput-object p3, p0, LX/JOj;->c:LX/0Ot;

    .line 2688081
    iput-object p4, p0, LX/JOj;->d:LX/3AZ;

    .line 2688082
    iput-object p5, p0, LX/JOj;->e:LX/0W9;

    .line 2688083
    iput-object p6, p0, LX/JOj;->f:LX/0Ot;

    .line 2688084
    iput-object p7, p0, LX/JOj;->g:LX/JPX;

    .line 2688085
    return-void
.end method

.method private static a(LX/JOj;LX/1De;ILjava/lang/String;)LX/1Dh;
    .locals 6

    .prologue
    .line 2688086
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    const v1, 0x7f0b259a

    invoke-interface {v0, v1}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v1

    iget-object v2, p0, LX/JOj;->e:LX/0W9;

    invoke-virtual {v2}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v2

    invoke-static {v2}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v2

    int-to-long v4, p2

    invoke-virtual {v2, v4, v5}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    const v2, 0x7f0a010c

    invoke-virtual {v1, v2}, LX/1ne;->n(I)LX/1ne;

    move-result-object v1

    const v2, 0x7f0b2594

    invoke-virtual {v1, v2}, LX/1ne;->q(I)LX/1ne;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v1

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v0

    .line 2688087
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, p3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    const v2, 0x7f0a010e

    invoke-virtual {v1, v2}, LX/1ne;->n(I)LX/1ne;

    move-result-object v1

    const v2, 0x7f0b2596

    invoke-virtual {v1, v2}, LX/1ne;->q(I)LX/1ne;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, LX/1ne;->j(I)LX/1ne;

    move-result-object v1

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v1

    move-object v1, v1

    .line 2688088
    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x3

    const v2, 0x7f0b25a3

    invoke-interface {v0, v1, v2}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/JOj;LX/1De;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;)LX/1Dh;
    .locals 6
    .param p1    # LX/1De;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param

    .prologue
    .line 2688089
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    const v1, 0x7f0b2599

    invoke-interface {v0, v1}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v0

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->p()I

    move-result v1

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->q()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, p1, v1, v2}, LX/JOj;->a(LX/JOj;LX/1De;ILjava/lang/String;)LX/1Dh;

    move-result-object v1

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->n()I

    move-result v2

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, p1, v2, v3}, LX/JOj;->a(LX/JOj;LX/1De;ILjava/lang/String;)LX/1Dh;

    move-result-object v2

    const/4 p3, 0x2

    .line 2688090
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, p3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x0

    const v5, 0x7f0b25a1

    invoke-interface {v1, v4, v5}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    const v4, 0x7f0b25a1

    invoke-interface {v2, p3, v4}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    move-object v1, v3

    .line 2688091
    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/JPK;->b(LX/1De;)LX/1Di;

    move-result-object v1

    const/4 v2, 0x6

    const v3, 0x7f0b2571

    invoke-interface {v1, v2, v3}, LX/1Di;->g(II)LX/1Di;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    .line 2688092
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2688093
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 2688094
    iget-object v2, p0, LX/JOj;->d:LX/3AZ;

    invoke-virtual {v2, p1}, LX/3AZ;->c(LX/1De;)LX/3Aa;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, LX/3Aa;->b(Z)LX/3Aa;

    move-result-object v2

    invoke-static {v1}, LX/2yc;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/3Ab;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/3Aa;->a(LX/3Ab;)LX/3Aa;

    move-result-object v2

    const v3, 0x7f0a010c

    invoke-virtual {v2, v3}, LX/3Aa;->j(I)LX/3Aa;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, LX/3Aa;->h(I)LX/3Aa;

    move-result-object v2

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, LX/3Aa;->a(Landroid/text/TextUtils$TruncateAt;)LX/3Aa;

    move-result-object v2

    const v3, 0x7f0b2595

    invoke-virtual {v2, v3}, LX/3Aa;->n(I)LX/3Aa;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const/4 v3, 0x6

    const v4, 0x7f0b25a1

    invoke-interface {v2, v3, v4}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    const/4 v3, 0x7

    const v4, 0x7f0b25a3

    invoke-interface {v2, v3, v4}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    move-object v1, v2

    .line 2688095
    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2688096
    :cond_0
    return-object v0
.end method

.method private static a(LX/JOj;LX/1De;II)LX/1Di;
    .locals 11

    .prologue
    .line 2688097
    iget-object v0, p0, LX/JOj;->b:LX/8ye;

    invoke-virtual {v0, p1}, LX/8ye;->c(LX/1De;)LX/8yc;

    move-result-object v0

    div-int v1, p2, p3

    mul-int/lit8 v1, v1, 0x64

    .line 2688098
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020cd5

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    check-cast v3, Landroid/graphics/drawable/LayerDrawable;

    .line 2688099
    const v4, 0x102000d

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 2688100
    if-eqz v5, :cond_0

    .line 2688101
    int-to-double v7, v1

    const-wide v9, 0x3eb0c6f7a0b5ed8dL    # 1.0E-6

    cmpg-double v4, v7, v9

    if-gez v4, :cond_1

    const/16 v4, 0x7d

    .line 2688102
    :goto_0
    invoke-virtual {v5, v4}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 2688103
    :cond_0
    invoke-static {}, LX/1n3;->b()LX/1n5;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/1n5;->a(Landroid/graphics/drawable/Drawable;)LX/1n5;

    move-result-object v3

    invoke-virtual {v3}, LX/1n6;->b()LX/1dc;

    move-result-object v3

    move-object v1, v3

    .line 2688104
    invoke-virtual {v0, v1}, LX/8yc;->a(LX/1dc;)LX/8yc;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/8yc;->i(I)LX/8yc;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/8yc;->h(I)LX/8yc;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const/4 v1, 0x7

    const v2, 0x7f0b259b

    invoke-interface {v0, v1, v2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v0

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b259d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-interface {v0, v1}, LX/1Di;->o(I)LX/1Di;

    move-result-object v0

    const/4 v1, 0x6

    const v2, 0x7f0b25a1

    invoke-interface {v0, v1, v2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v0

    return-object v0

    .line 2688105
    :cond_1
    const/16 v4, 0xff

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/JOj;
    .locals 11

    .prologue
    .line 2688106
    const-class v1, LX/JOj;

    monitor-enter v1

    .line 2688107
    :try_start_0
    sget-object v0, LX/JOj;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2688108
    sput-object v2, LX/JOj;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2688109
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2688110
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2688111
    new-instance v3, LX/JOj;

    invoke-static {v0}, LX/11T;->a(LX/0QB;)LX/11T;

    move-result-object v4

    check-cast v4, LX/11T;

    invoke-static {v0}, LX/8ye;->a(LX/0QB;)LX/8ye;

    move-result-object v5

    check-cast v5, LX/8ye;

    const/16 v6, 0xbc6

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, LX/3AZ;->a(LX/0QB;)LX/3AZ;

    move-result-object v7

    check-cast v7, LX/3AZ;

    invoke-static {v0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v8

    check-cast v8, LX/0W9;

    const/16 v9, 0x1fdc

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {v0}, LX/JPX;->a(LX/0QB;)LX/JPX;

    move-result-object v10

    check-cast v10, LX/JPX;

    invoke-direct/range {v3 .. v10}, LX/JOj;-><init>(LX/11T;LX/8ye;LX/0Ot;LX/3AZ;LX/0W9;LX/0Ot;LX/JPX;)V

    .line 2688112
    move-object v0, v3

    .line 2688113
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2688114
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JOj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2688115
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2688116
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(LX/JOj;JJ)Ljava/lang/String;
    .locals 7

    .prologue
    const-wide/16 v4, 0x3e8

    .line 2688117
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/JOj;->a:LX/11T;

    invoke-virtual {v1}, LX/11T;->h()Ljava/text/SimpleDateFormat;

    move-result-object v1

    mul-long v2, p1, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/JOj;->a:LX/11T;

    invoke-virtual {v1}, LX/11T;->h()Ljava/text/SimpleDateFormat;

    move-result-object v1

    mul-long v2, p3, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;)LX/1Dg;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;",
            ")",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 2688118
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->x()Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;

    move-result-object v0

    .line 2688119
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const v2, 0x7f0b258c

    invoke-interface {v1, v2}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v1

    iget-object v2, p0, LX/JOj;->g:LX/JPX;

    const/4 v3, 0x0

    .line 2688120
    new-instance v4, LX/JPW;

    invoke-direct {v4, v2}, LX/JPW;-><init>(LX/JPX;)V

    .line 2688121
    iget-object v5, v2, LX/JPX;->b:LX/0Zi;

    invoke-virtual {v5}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/JPV;

    .line 2688122
    if-nez v5, :cond_0

    .line 2688123
    new-instance v5, LX/JPV;

    invoke-direct {v5, v2}, LX/JPV;-><init>(LX/JPX;)V

    .line 2688124
    :cond_0
    invoke-static {v5, p1, v3, v3, v4}, LX/JPV;->a$redex0(LX/JPV;LX/1De;IILX/JPW;)V

    .line 2688125
    move-object v4, v5

    .line 2688126
    move-object v3, v4

    .line 2688127
    move-object v2, v3

    .line 2688128
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->k()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 2688129
    iget-object v4, v2, LX/JPV;->a:LX/JPW;

    iput-object v3, v4, LX/JPW;->a:Landroid/net/Uri;

    .line 2688130
    iget-object v4, v2, LX/JPV;->e:Ljava/util/BitSet;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 2688131
    move-object v2, v2

    .line 2688132
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->a()Ljava/lang/String;

    move-result-object v3

    .line 2688133
    iget-object v4, v2, LX/JPV;->a:LX/JPW;

    iput-object v3, v4, LX/JPW;->b:Ljava/lang/String;

    .line 2688134
    iget-object v4, v2, LX/JPV;->e:Ljava/util/BitSet;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 2688135
    move-object v2, v2

    .line 2688136
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->v()J

    move-result-wide v4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->w()J

    move-result-wide v6

    invoke-static {p0, v4, v5, v6, v7}, LX/JOj;->a(LX/JOj;JJ)Ljava/lang/String;

    move-result-object v3

    .line 2688137
    iget-object v4, v2, LX/JPV;->a:LX/JPW;

    iput-object v3, v4, LX/JPW;->c:Ljava/lang/String;

    .line 2688138
    iget-object v4, v2, LX/JPV;->e:Ljava/util/BitSet;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 2688139
    move-object v2, v2

    .line 2688140
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->l()Z

    move-result v3

    .line 2688141
    iget-object v4, v2, LX/JPV;->a:LX/JPW;

    iput-boolean v3, v4, LX/JPW;->d:Z

    .line 2688142
    iget-object v4, v2, LX/JPV;->e:Ljava/util/BitSet;

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 2688143
    move-object v2, v2

    .line 2688144
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->j()Ljava/lang/String;

    move-result-object v3

    .line 2688145
    iget-object v4, v2, LX/JPV;->a:LX/JPW;

    iput-object v3, v4, LX/JPW;->e:Ljava/lang/String;

    .line 2688146
    iget-object v4, v2, LX/JPV;->e:Ljava/util/BitSet;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 2688147
    move-object v2, v2

    .line 2688148
    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/JPK;->b(LX/1De;)LX/1Di;

    move-result-object v2

    const/4 v3, 0x6

    const v4, 0x7f0b2571

    invoke-interface {v2, v3, v4}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-static {p0, p1, p2, v0}, LX/JOj;->a(LX/JOj;LX/1De;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;)LX/1Dh;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->r()I

    move-result v2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->t()I

    move-result v3

    invoke-static {p0, p1, v2, v3}, LX/JOj;->a(LX/JOj;LX/1De;II)LX/1Di;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->s()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;->u()Ljava/lang/String;

    move-result-object v0

    const/high16 p2, 0x3f800000    # 1.0f

    const/4 p0, 0x1

    const/4 v7, 0x2

    .line 2688149
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v7}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x6

    const v5, 0x7f0b25a1

    invoke-interface {v3, v4, v5}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x3

    const v5, 0x7f0b25a2

    invoke-interface {v3, v4, v5}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v3

    const v4, 0x7f0b259e

    invoke-interface {v3, v4}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v3

    const v4, 0x7f0b257f

    invoke-interface {v3, v4}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, p2}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v7}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, v5}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0b259a

    invoke-interface {v4, v5}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    const v6, 0x7f0a010c

    invoke-virtual {v5, v6}, LX/1ne;->n(I)LX/1ne;

    move-result-object v5

    const v6, 0x7f0b2596

    invoke-virtual {v5, v6}, LX/1ne;->q(I)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, p0}, LX/1ne;->t(I)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, p0}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v5

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v5, v6}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, p2}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v7}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v7}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0b259a

    invoke-interface {v4, v5}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    const v6, 0x7f0a010e

    invoke-virtual {v5, v6}, LX/1ne;->n(I)LX/1ne;

    move-result-object v5

    const v6, 0x7f0b2596

    invoke-virtual {v5, v6}, LX/1ne;->q(I)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, p0}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v5

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v5, v6}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    move-object v0, v3

    .line 2688150
    invoke-interface {v1, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    .line 2688151
    const v1, 0x2fe09e32

    const/4 v2, 0x0

    invoke-static {p1, v1, v2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v1

    move-object v1, v1

    .line 2688152
    invoke-interface {v0, v1}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0
.end method
