.class public LX/Iza;
.super LX/0Tv;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0Tz;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/Iza;


# direct methods
.method public static constructor <clinit>()V
    .locals 11

    .prologue
    .line 2634691
    const-class v0, LX/Iza;

    sput-object v0, LX/Iza;->a:Ljava/lang/Class;

    .line 2634692
    new-instance v0, LX/IzZ;

    invoke-direct {v0}, LX/IzZ;-><init>()V

    new-instance v1, LX/IzX;

    invoke-direct {v1}, LX/IzX;-><init>()V

    new-instance v2, LX/IzT;

    invoke-direct {v2}, LX/IzT;-><init>()V

    new-instance v3, LX/IzU;

    invoke-direct {v3}, LX/IzU;-><init>()V

    new-instance v4, LX/IzV;

    invoke-direct {v4}, LX/IzV;-><init>()V

    new-instance v5, LX/IzP;

    invoke-direct {v5}, LX/IzP;-><init>()V

    new-instance v6, LX/IzY;

    invoke-direct {v6}, LX/IzY;-><init>()V

    new-instance v7, LX/IzQ;

    invoke-direct {v7}, LX/IzQ;-><init>()V

    new-instance v8, LX/IzR;

    invoke-direct {v8}, LX/IzR;-><init>()V

    new-instance v9, LX/IzW;

    invoke-direct {v9}, LX/IzW;-><init>()V

    new-instance v10, LX/IzS;

    invoke-direct {v10}, LX/IzS;-><init>()V

    invoke-static/range {v0 .. v10}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/Iza;->b:LX/0Px;

    return-void
.end method

.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2634693
    const-string v0, "payments"

    const/16 v1, 0x19

    sget-object v2, LX/Iza;->b:LX/0Px;

    invoke-direct {p0, v0, v1, v2}, LX/0Tv;-><init>(Ljava/lang/String;ILX/0Px;)V

    .line 2634694
    return-void
.end method

.method public static a(LX/0QB;)LX/Iza;
    .locals 3

    .prologue
    .line 2634695
    sget-object v0, LX/Iza;->c:LX/Iza;

    if-nez v0, :cond_1

    .line 2634696
    const-class v1, LX/Iza;

    monitor-enter v1

    .line 2634697
    :try_start_0
    sget-object v0, LX/Iza;->c:LX/Iza;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2634698
    if-eqz v2, :cond_0

    .line 2634699
    :try_start_1
    new-instance v0, LX/Iza;

    invoke-direct {v0}, LX/Iza;-><init>()V

    .line 2634700
    move-object v0, v0

    .line 2634701
    sput-object v0, LX/Iza;->c:LX/Iza;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2634702
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2634703
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2634704
    :cond_1
    sget-object v0, LX/Iza;->c:LX/Iza;

    return-object v0

    .line 2634705
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2634706
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 2

    .prologue
    .line 2634707
    sget-object v0, LX/Iza;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result p2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, p2, :cond_0

    sget-object v0, LX/Iza;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Tz;

    .line 2634708
    iget-object p3, v0, LX/0Tz;->a:Ljava/lang/String;

    move-object v0, p3

    .line 2634709
    invoke-static {v0}, LX/0Tz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const p3, 0x4e2639b7    # 6.9720006E8f

    invoke-static {p3}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x11befd68

    invoke-static {v0}, LX/03h;->a(I)V

    .line 2634710
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2634711
    :cond_0
    invoke-virtual {p0, p1}, LX/0Tv;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2634712
    return-void
.end method

.method public final b(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0

    .prologue
    .line 2634713
    return-void
.end method
