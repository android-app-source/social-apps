.class public final LX/Iz4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/Iyy;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/payments/p2p/P2pPaymentFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/p2p/P2pPaymentFragment;)V
    .locals 0

    .prologue
    .line 2634104
    iput-object p1, p0, LX/Iz4;->a:Lcom/facebook/payments/p2p/P2pPaymentFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(LX/Iyy;)V
    .locals 2
    .param p1    # LX/Iyy;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2634094
    if-eqz p1, :cond_0

    .line 2634095
    sget-object v0, LX/Iz7;->a:[I

    invoke-virtual {p1}, LX/Iyy;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2634096
    :cond_0
    :goto_0
    return-void

    .line 2634097
    :pswitch_0
    iget-object v0, p0, LX/Iz4;->a:Lcom/facebook/payments/p2p/P2pPaymentFragment;

    iget-object v0, v0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->n:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2634098
    iget-object v0, p0, LX/Iz4;->a:Lcom/facebook/payments/p2p/P2pPaymentFragment;

    iget-object v0, v0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->o:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 2634099
    :pswitch_1
    iget-object v0, p0, LX/Iz4;->a:Lcom/facebook/payments/p2p/P2pPaymentFragment;

    iget-object v0, v0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->l:LX/Iyv;

    invoke-interface {v0}, LX/Iyv;->l()V

    goto :goto_0

    .line 2634100
    :pswitch_2
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2634102
    iget-object v0, p0, LX/Iz4;->a:Lcom/facebook/payments/p2p/P2pPaymentFragment;

    iget-object v0, v0, Lcom/facebook/payments/p2p/P2pPaymentFragment;->l:LX/Iyv;

    invoke-interface {v0}, LX/Iyv;->l()V

    .line 2634103
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2634101
    check-cast p1, LX/Iyy;

    invoke-direct {p0, p1}, LX/Iz4;->a(LX/Iyy;)V

    return-void
.end method
