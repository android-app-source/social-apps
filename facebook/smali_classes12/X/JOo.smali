.class public final LX/JOo;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JOq;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

.field public c:Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;

.field public final synthetic d:LX/JOq;


# direct methods
.method public constructor <init>(LX/JOq;)V
    .locals 1

    .prologue
    .line 2688255
    iput-object p1, p0, LX/JOo;->d:LX/JOq;

    .line 2688256
    move-object v0, p1

    .line 2688257
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2688258
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2688276
    const-string v0, "AdsSingleCampaignRowComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2688259
    if-ne p0, p1, :cond_1

    .line 2688260
    :cond_0
    :goto_0
    return v0

    .line 2688261
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2688262
    goto :goto_0

    .line 2688263
    :cond_3
    check-cast p1, LX/JOo;

    .line 2688264
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2688265
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2688266
    if-eq v2, v3, :cond_0

    .line 2688267
    iget-object v2, p0, LX/JOo;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/JOo;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/JOo;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2688268
    goto :goto_0

    .line 2688269
    :cond_5
    iget-object v2, p1, LX/JOo;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 2688270
    :cond_6
    iget-object v2, p0, LX/JOo;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/JOo;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    iget-object v3, p1, LX/JOo;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2688271
    goto :goto_0

    .line 2688272
    :cond_8
    iget-object v2, p1, LX/JOo;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    if-nez v2, :cond_7

    .line 2688273
    :cond_9
    iget-object v2, p0, LX/JOo;->c:Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/JOo;->c:Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;

    iget-object v3, p1, LX/JOo;->c:Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2688274
    goto :goto_0

    .line 2688275
    :cond_a
    iget-object v2, p1, LX/JOo;->c:Lcom/facebook/graphql/model/GraphQLCampaignInsightSummary;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
