.class public final synthetic LX/HhK;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2495421
    invoke-static {}, LX/HhL;->values()[LX/HhL;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/HhK;->a:[I

    :try_start_0
    sget-object v0, LX/HhK;->a:[I

    sget-object v1, LX/HhL;->TCP_OPEN:LX/HhL;

    invoke-virtual {v1}, LX/HhL;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_e

    :goto_0
    :try_start_1
    sget-object v0, LX/HhK;->a:[I

    sget-object v1, LX/HhL;->TCP_OPEN_80:LX/HhL;

    invoke-virtual {v1}, LX/HhL;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_d

    :goto_1
    :try_start_2
    sget-object v0, LX/HhK;->a:[I

    sget-object v1, LX/HhL;->TCP_OPEN_443:LX/HhL;

    invoke-virtual {v1}, LX/HhL;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_c

    :goto_2
    :try_start_3
    sget-object v0, LX/HhK;->a:[I

    sget-object v1, LX/HhL;->SSL:LX/HhL;

    invoke-virtual {v1}, LX/HhL;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_b

    :goto_3
    :try_start_4
    sget-object v0, LX/HhK;->a:[I

    sget-object v1, LX/HhL;->MQTT_SSL:LX/HhL;

    invoke-virtual {v1}, LX/HhL;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_a

    :goto_4
    :try_start_5
    sget-object v0, LX/HhK;->a:[I

    sget-object v1, LX/HhL;->VOIP_TURN:LX/HhL;

    invoke-virtual {v1}, LX/HhL;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_9

    :goto_5
    :try_start_6
    sget-object v0, LX/HhK;->a:[I

    sget-object v1, LX/HhL;->UDP_3478:LX/HhL;

    invoke-virtual {v1}, LX/HhL;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_8

    :goto_6
    :try_start_7
    sget-object v0, LX/HhK;->a:[I

    sget-object v1, LX/HhL;->WA_443:LX/HhL;

    invoke-virtual {v1}, LX/HhL;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_7

    :goto_7
    :try_start_8
    sget-object v0, LX/HhK;->a:[I

    sget-object v1, LX/HhL;->HTTP:LX/HhL;

    invoke-virtual {v1}, LX/HhL;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_6

    :goto_8
    :try_start_9
    sget-object v0, LX/HhK;->a:[I

    sget-object v1, LX/HhL;->HTTP_80:LX/HhL;

    invoke-virtual {v1}, LX/HhL;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_5

    :goto_9
    :try_start_a
    sget-object v0, LX/HhK;->a:[I

    sget-object v1, LX/HhL;->HTTP_AKAMAI:LX/HhL;

    invoke-virtual {v1}, LX/HhL;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_4

    :goto_a
    :try_start_b
    sget-object v0, LX/HhK;->a:[I

    sget-object v1, LX/HhL;->PROXY_CONNECT:LX/HhL;

    invoke-virtual {v1}, LX/HhL;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_3

    :goto_b
    :try_start_c
    sget-object v0, LX/HhK;->a:[I

    sget-object v1, LX/HhL;->HTTP_OPERA:LX/HhL;

    invoke-virtual {v1}, LX/HhL;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_2

    :goto_c
    :try_start_d
    sget-object v0, LX/HhK;->a:[I

    sget-object v1, LX/HhL;->HTTPS:LX/HhL;

    invoke-virtual {v1}, LX/HhL;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_1

    :goto_d
    :try_start_e
    sget-object v0, LX/HhK;->a:[I

    sget-object v1, LX/HhL;->HTTPS_443:LX/HhL;

    invoke-virtual {v1}, LX/HhL;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_0

    :goto_e
    return-void

    :catch_0
    goto :goto_e

    :catch_1
    goto :goto_d

    :catch_2
    goto :goto_c

    :catch_3
    goto :goto_b

    :catch_4
    goto :goto_a

    :catch_5
    goto :goto_9

    :catch_6
    goto :goto_8

    :catch_7
    goto :goto_7

    :catch_8
    goto :goto_6

    :catch_9
    goto :goto_5

    :catch_a
    goto/16 :goto_4

    :catch_b
    goto/16 :goto_3

    :catch_c
    goto/16 :goto_2

    :catch_d
    goto/16 :goto_1

    :catch_e
    goto/16 :goto_0
.end method
