.class public LX/IeT;
.super LX/3Ml;
.source ""


# instance fields
.field private final c:LX/3LP;

.field private final d:LX/2RQ;


# direct methods
.method public constructor <init>(LX/0Zr;LX/3LP;LX/2RQ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2598656
    invoke-direct {p0, p1}, LX/3Ml;-><init>(LX/0Zr;)V

    .line 2598657
    iput-object p2, p0, LX/IeT;->c:LX/3LP;

    .line 2598658
    iput-object p3, p0, LX/IeT;->d:LX/2RQ;

    .line 2598659
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 2598609
    iget-object v0, p0, LX/IeT;->d:LX/2RQ;

    invoke-virtual {v0}, LX/2RQ;->a()LX/2RR;

    move-result-object v0

    .line 2598610
    iput-object p1, v0, LX/2RR;->e:Ljava/lang/String;

    .line 2598611
    move-object v0, v0

    .line 2598612
    sget-object v1, LX/2RU;->MESSAGABLE_TYPES:LX/0Px;

    .line 2598613
    iput-object v1, v0, LX/2RR;->b:Ljava/util/Collection;

    .line 2598614
    move-object v0, v0

    .line 2598615
    iput-boolean v2, v0, LX/2RR;->f:Z

    .line 2598616
    move-object v0, v0

    .line 2598617
    iput-boolean v2, v0, LX/2RR;->h:Z

    .line 2598618
    move-object v0, v0

    .line 2598619
    invoke-virtual {v0}, LX/2RR;->i()LX/2RR;

    move-result-object v0

    const/16 v1, 0x1e

    .line 2598620
    iput v1, v0, LX/2RR;->p:I

    .line 2598621
    move-object v0, v0

    .line 2598622
    iget-object v1, p0, LX/IeT;->c:LX/3LP;

    invoke-virtual {v1, v0}, LX/3LP;->a(LX/2RR;)LX/3On;

    move-result-object v1

    .line 2598623
    :goto_0
    :try_start_0
    invoke-interface {v1}, LX/3On;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2598624
    invoke-interface {v1}, LX/3On;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2598625
    iget-object v2, v0, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v2, v2

    .line 2598626
    invoke-interface {p2, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2598627
    :catchall_0
    move-exception v0

    invoke-interface {v1}, LX/3On;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, LX/3On;->close()V

    .line 2598628
    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/CharSequence;)LX/39y;
    .locals 6

    .prologue
    .line 2598629
    new-instance v1, LX/39y;

    invoke-direct {v1}, LX/39y;-><init>()V

    .line 2598630
    if-eqz p1, :cond_2

    :try_start_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 2598631
    :goto_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_3

    .line 2598632
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v2

    .line 2598633
    invoke-direct {p0, v0, v2}, LX/IeT;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 2598634
    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2598635
    new-instance v2, LX/IeS;

    invoke-direct {v2, p0}, LX/IeS;-><init>(LX/IeT;)V

    invoke-static {v0, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2598636
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2598637
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/user/model/User;

    .line 2598638
    invoke-virtual {v3}, Lcom/facebook/user/model/User;->m()Lcom/facebook/user/model/UserFbidIdentifier;

    move-result-object v5

    .line 2598639
    if-eqz v5, :cond_0

    .line 2598640
    invoke-virtual {p0, v5}, LX/3Ml;->a(Lcom/facebook/user/model/UserIdentifier;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2598641
    iget-object v5, p0, LX/3Ml;->b:LX/3Md;

    invoke-interface {v5, v3}, LX/3Md;->a(Ljava/lang/Object;)LX/3OQ;

    move-result-object v3

    .line 2598642
    if-eqz v3, :cond_0

    .line 2598643
    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 2598644
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2598645
    invoke-static {p1, v0}, LX/3Og;->a(Ljava/lang/CharSequence;LX/0Px;)LX/3Og;

    move-result-object v0

    .line 2598646
    iput-object v0, v1, LX/39y;->a:Ljava/lang/Object;

    .line 2598647
    iget v2, v0, LX/3Og;->d:I

    move v0, v2

    .line 2598648
    iput v0, v1, LX/39y;->b:I

    .line 2598649
    :goto_2
    return-object v1

    .line 2598650
    :cond_2
    const-string v0, ""

    goto :goto_0

    .line 2598651
    :cond_3
    invoke-static {p1}, LX/3Og;->a(Ljava/lang/CharSequence;)LX/3Og;

    move-result-object v0

    iput-object v0, v1, LX/39y;->a:Ljava/lang/Object;

    .line 2598652
    const/4 v0, -0x1

    iput v0, v1, LX/39y;->b:I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 2598653
    :catch_0
    move-exception v0

    .line 2598654
    const-string v1, "orca:ContactPickerNotOnMessengerFriendsFilter"

    const-string v2, "exception while filtering"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2598655
    throw v0
.end method
