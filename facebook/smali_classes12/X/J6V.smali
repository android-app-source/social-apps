.class public final LX/J6V;
.super LX/63W;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;)V
    .locals 0

    .prologue
    .line 2649705
    iput-object p1, p0, LX/J6V;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    invoke-direct {p0}, LX/63W;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 2649706
    iget-object v0, p0, LX/J6V;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->q:LX/J3t;

    invoke-virtual {v0}, LX/J3t;->b()I

    move-result v0

    if-eqz v0, :cond_1

    .line 2649707
    iget-object v0, p0, LX/J6V;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v1, p0, LX/J6V;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v1, v1, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->q:LX/J3t;

    iget-object v2, p0, LX/J6V;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v2, v2, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->w:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v2

    invoke-virtual {v1, v2}, LX/J3t;->c(I)LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->p:LX/0Px;

    .line 2649708
    iget-object v0, p0, LX/J6V;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->p:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/J6V;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->p:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2649709
    iget-object v0, p0, LX/J6V;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->t:LX/J5k;

    iget-object v1, p0, LX/J6V;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v1, v1, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->q:LX/J3t;

    .line 2649710
    iget-object v2, v1, LX/J3t;->i:Ljava/lang/String;

    move-object v1, v2

    .line 2649711
    const-string v2, "step_save"

    iget-object v3, p0, LX/J6V;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v3, v3, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->w:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v4, p0, LX/J6V;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v4, v4, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->w:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v4}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, LX/J5k;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Long;)V

    .line 2649712
    iget-object v0, p0, LX/J6V;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    .line 2649713
    invoke-static {v0}, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->l$redex0(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;)V

    .line 2649714
    :goto_0
    return-void

    .line 2649715
    :cond_0
    iget-object v0, p0, LX/J6V;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->q:LX/J3t;

    invoke-virtual {v0}, LX/J3t;->a()V

    .line 2649716
    :cond_1
    iget-object v0, p0, LX/J6V;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->t:LX/J5k;

    iget-object v1, p0, LX/J6V;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v1, v1, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->q:LX/J3t;

    .line 2649717
    iget-object v2, v1, LX/J3t;->i:Ljava/lang/String;

    move-object v1, v2

    .line 2649718
    const-string v2, "step_navigation"

    iget-object v3, p0, LX/J6V;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v3, v3, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->w:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v4, p0, LX/J6V;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v4, v4, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->w:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v4}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, LX/J5k;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Long;)V

    .line 2649719
    iget-object v0, p0, LX/J6V;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->w:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    iget-object v1, p0, LX/J6V;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v1, v1, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->q:LX/J3t;

    invoke-virtual {v1}, LX/J3t;->c()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_2

    .line 2649720
    iget-object v0, p0, LX/J6V;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    invoke-virtual {v0}, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->finish()V

    goto :goto_0

    .line 2649721
    :cond_2
    iget-object v0, p0, LX/J6V;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->w:Lcom/facebook/widget/CustomViewPager;

    iget-object v1, p0, LX/J6V;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v1, v1, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->w:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 2649722
    iget-object v0, p0, LX/J6V;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->v:LX/0h5;

    iget-object v1, p0, LX/J6V;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v1, v1, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->q:LX/J3t;

    iget-object v2, p0, LX/J6V;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v2, v2, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->w:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v2

    invoke-virtual {v1, v2}, LX/J3t;->a(I)LX/J4L;

    move-result-object v1

    iget-object v1, v1, LX/J4L;->m:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 2649723
    iget-object v0, p0, LX/J6V;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v1, p0, LX/J6V;->a:Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;

    iget-object v1, v1, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->v:LX/0h5;

    .line 2649724
    invoke-static {v0, v1}, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;->a$redex0(Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupActivity;LX/0h5;)V

    .line 2649725
    goto/16 :goto_0
.end method
