.class public LX/HVS;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/17W;


# direct methods
.method public constructor <init>(LX/17W;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2475062
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2475063
    iput-object p1, p0, LX/HVS;->a:LX/17W;

    .line 2475064
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;)V
    .locals 8

    .prologue
    .line 2475065
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2475066
    const-string v1, "com.facebook.orca.auth.OVERRIDDEN_VIEWER_CONTEXT"

    iget-object v2, p3, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->i:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2475067
    iget-object v1, p0, LX/HVS;->a:LX/17W;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, LX/0ax;->eA:Ljava/lang/String;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-wide v6, p3, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->a:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 2475068
    return-void
.end method
