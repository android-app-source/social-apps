.class public final LX/IYf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/47M;


# instance fields
.field public final synthetic a:LX/IYg;


# direct methods
.method public constructor <init>(LX/IYg;)V
    .locals 0

    .prologue
    .line 2587973
    iput-object p1, p0, LX/IYf;->a:LX/IYg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/content/Intent;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 2587974
    const-string v0, "surface"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2587975
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2587976
    const-string v1, "ANDROID_SEARCH_LOCAL_PLACE_TIPS"

    .line 2587977
    :goto_0
    move-object v6, v1

    .line 2587978
    const-string v0, "place_id"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2587979
    const-string v0, "place_name"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2587980
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2587981
    :cond_0
    iget-object v0, p0, LX/IYf;->a:LX/IYg;

    iget-object v0, v0, LX/IYg;->b:LX/03V;

    sget-object v3, LX/IYg;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Trying to launch place feed via uri with an empty/null page id or name. Page id: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "; page name: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; surface: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2587982
    :goto_1
    return-object v7

    .line 2587983
    :cond_1
    invoke-static {v6}, LX/2s8;->j(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2587984
    iget-object v0, p0, LX/IYf;->a:LX/IYg;

    iget-object v0, v0, LX/IYg;->b:LX/03V;

    sget-object v3, LX/IYg;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Trying to launch place feed via uri with non local serp surface. Surface: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2587985
    :cond_2
    const-string v0, "ranking_data"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2587986
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2587987
    iget-object v0, p0, LX/IYf;->a:LX/IYg;

    .line 2587988
    iget-object v5, v0, LX/IYg;->d:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/1vC;

    .line 2587989
    const p1, 0x1e0002

    invoke-virtual {v5, p1, v4, v6}, LX/1vC;->b(ILjava/lang/String;Ljava/lang/String;)V

    .line 2587990
    const p1, 0x1e000a

    invoke-virtual {v5, p1, v4, v6}, LX/1vC;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 2587991
    iget-object v0, p0, LX/IYf;->a:LX/IYg;

    invoke-static {v0, v1, v2, v4, v6}, LX/IYg;->a$redex0(LX/IYg;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2587992
    iget-object v0, p0, LX/IYf;->a:LX/IYg;

    iget-object v0, v0, LX/IYg;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1nD;

    sget-object v5, LX/8ci;->q:LX/8ci;

    invoke-virtual/range {v0 .. v7}, LX/1nD;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8ci;Ljava/lang/String;Lcom/facebook/search/logging/api/SearchTypeaheadSession;)Landroid/content/Intent;

    move-result-object v7

    goto :goto_1

    .line 2587993
    :cond_3
    :try_start_0
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto/16 :goto_0

    .line 2587994
    :catch_0
    const-string v1, "ANDROID_SEARCH_LOCAL_PLACE_TIPS"

    goto/16 :goto_0
.end method
