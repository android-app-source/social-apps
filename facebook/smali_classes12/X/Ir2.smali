.class public final LX/Ir2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/analytics/logger/HoneyClientEvent;

.field public final synthetic b:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 0

    .prologue
    .line 2617390
    iput-object p1, p0, LX/Ir2;->b:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    iput-object p2, p0, LX/Ir2;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 2617391
    iget-object v0, p0, LX/Ir2;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "discarded_changes"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2617392
    iget-object v0, p0, LX/Ir2;->b:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->n:LX/0Zb;

    iget-object v1, p0, LX/Ir2;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-interface {v0, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2617393
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 2617394
    return-void
.end method
