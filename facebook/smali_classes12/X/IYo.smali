.class public final LX/IYo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/graphql/model/GraphQLCustomizedStory;",
        ">;",
        "Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/IYq;


# direct methods
.method public constructor <init>(LX/IYq;)V
    .locals 0

    .prologue
    .line 2588148
    iput-object p1, p0, LX/IYo;->a:LX/IYq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2588149
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2588150
    if-eqz p1, :cond_0

    .line 2588151
    iget-object p0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object p0, p0

    .line 2588152
    if-nez p0, :cond_1

    .line 2588153
    :cond_0
    const/4 p0, 0x0

    .line 2588154
    :goto_0
    move-object p0, p0

    .line 2588155
    move-object v0, p0

    .line 2588156
    return-object v0

    :cond_1
    new-instance v0, LX/1u8;

    invoke-direct {v0}, LX/1u8;-><init>()V

    .line 2588157
    iget-object p0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object p0, p0

    .line 2588158
    check-cast p0, Lcom/facebook/graphql/model/FeedUnit;

    .line 2588159
    iput-object p0, v0, LX/1u8;->g:Lcom/facebook/graphql/model/FeedUnit;

    .line 2588160
    move-object v0, v0

    .line 2588161
    iget-object p0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object p0, p0

    .line 2588162
    check-cast p0, Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->a()Ljava/lang/String;

    move-result-object p0

    .line 2588163
    iput-object p0, v0, LX/1u8;->d:Ljava/lang/String;

    .line 2588164
    move-object p0, v0

    .line 2588165
    invoke-virtual {p0}, LX/1u8;->a()Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object p0

    goto :goto_0
.end method
