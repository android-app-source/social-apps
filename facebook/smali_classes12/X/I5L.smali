.class public final LX/I5L;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;)V
    .locals 0

    .prologue
    .line 2535288
    iput-object p1, p0, LX/I5L;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2535316
    iget-object v0, p0, LX/I5L;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;

    iget-object v0, v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->m:LX/E8m;

    iget-object v1, p0, LX/I5L;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;

    iget-object v1, v1, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->m:LX/E8m;

    invoke-virtual {v1}, LX/E8m;->g()I

    move-result v1

    invoke-virtual {v0, v1}, LX/1OM;->i_(I)V

    .line 2535317
    iget-object v0, p0, LX/I5L;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;

    invoke-virtual {v0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->kK_()V

    .line 2535318
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2535289
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x0

    .line 2535290
    iget-object v0, p0, LX/I5L;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;

    .line 2535291
    invoke-virtual {v0, v1}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->f_(Z)V

    .line 2535292
    iget-object v0, p0, LX/I5L;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;

    invoke-static {v0, v1}, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->a$redex0(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;I)V

    .line 2535293
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2535294
    if-eqz v0, :cond_0

    .line 2535295
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2535296
    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2535297
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2535298
    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryFragmentModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2535299
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2535300
    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryFragmentModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2535301
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2535302
    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryFragmentModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    .line 2535303
    iget-object v1, p0, LX/I5L;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->a()Ljava/lang/String;

    move-result-object v2

    .line 2535304
    iput-object v2, v1, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->o:Ljava/lang/String;

    .line 2535305
    iget-object v1, p0, LX/I5L;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;

    iget-object v1, v1, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->l:LX/2jY;

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->b()Z

    move-result v0

    .line 2535306
    iput-boolean v0, v1, LX/2jY;->o:Z

    .line 2535307
    iget-object v0, p0, LX/I5L;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;

    iget-object v1, v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->m:LX/E8m;

    .line 2535308
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2535309
    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryFragmentModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/E8m;->a(LX/9qT;)V

    .line 2535310
    :goto_0
    iget-object v0, p0, LX/I5L;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;

    .line 2535311
    invoke-virtual {v0}, Lcom/facebook/reaction/ui/fragment/BaseReactionFragment;->x()V

    .line 2535312
    return-void

    .line 2535313
    :cond_0
    iget-object v0, p0, LX/I5L;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;

    iget-object v0, v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->l:LX/2jY;

    .line 2535314
    iput-boolean v1, v0, LX/2jY;->o:Z

    .line 2535315
    iget-object v0, p0, LX/I5L;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;

    iget-object v0, v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->m:LX/E8m;

    iget-object v1, p0, LX/I5L;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;

    iget-object v1, v1, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryReactionFragment;->m:LX/E8m;

    invoke-virtual {v1}, LX/E8m;->g()I

    move-result v1

    invoke-virtual {v0, v1}, LX/1OM;->i_(I)V

    goto :goto_0
.end method
