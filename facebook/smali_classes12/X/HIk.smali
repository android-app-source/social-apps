.class public final LX/HIk;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/HIm;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/HIl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/HIm",
            "<TE;>.PageAddressNavigationUnitComponentComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/HIm;

.field private c:[Ljava/lang/String;

.field private d:I

.field public e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/HIm;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 2451685
    iput-object p1, p0, LX/HIk;->b:LX/HIm;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 2451686
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "props"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "environment"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/HIk;->c:[Ljava/lang/String;

    .line 2451687
    iput v3, p0, LX/HIk;->d:I

    .line 2451688
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/HIk;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/HIk;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/HIk;LX/1De;IILX/HIl;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/HIm",
            "<TE;>.PageAddressNavigationUnitComponentComponentImpl;)V"
        }
    .end annotation

    .prologue
    .line 2451689
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 2451690
    iput-object p4, p0, LX/HIk;->a:LX/HIl;

    .line 2451691
    iget-object v0, p0, LX/HIk;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 2451692
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2451693
    invoke-super {p0}, LX/1X5;->a()V

    .line 2451694
    const/4 v0, 0x0

    iput-object v0, p0, LX/HIk;->a:LX/HIl;

    .line 2451695
    iget-object v0, p0, LX/HIk;->b:LX/HIm;

    iget-object v0, v0, LX/HIm;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 2451696
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/HIm;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2451697
    iget-object v1, p0, LX/HIk;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/HIk;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/HIk;->d:I

    if-ge v1, v2, :cond_2

    .line 2451698
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2451699
    :goto_0
    iget v2, p0, LX/HIk;->d:I

    if-ge v0, v2, :cond_1

    .line 2451700
    iget-object v2, p0, LX/HIk;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2451701
    iget-object v2, p0, LX/HIk;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2451702
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2451703
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2451704
    :cond_2
    iget-object v0, p0, LX/HIk;->a:LX/HIl;

    .line 2451705
    invoke-virtual {p0}, LX/HIk;->a()V

    .line 2451706
    return-object v0
.end method
