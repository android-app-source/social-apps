.class public final LX/JJB;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$CurationTagsTypeaheadQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;)V
    .locals 0

    .prologue
    .line 2678986
    iput-object p1, p0, LX/JJB;->a:Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2678983
    iget-object v0, p0, LX/JJB;->a:Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;->h:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2678984
    iget-object v0, p0, LX/JJB;->a:Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, LX/JJB;->a:Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;

    const v2, 0x7f083a73

    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2678985
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2678975
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2678976
    iget-object v0, p0, LX/JJB;->a:Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;->h:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2678977
    iget-object v0, p0, LX/JJB;->a:Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;

    iget-object v1, v0, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;->f:LX/JJ4;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 2678978
    :goto_0
    iput-object v0, v1, LX/JJ4;->a:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$CurationTagsTypeaheadQueryModel;

    .line 2678979
    iget-object v0, p0, LX/JJB;->a:Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;->f:LX/JJ4;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2678980
    return-void

    .line 2678981
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2678982
    check-cast v0, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$CurationTagsTypeaheadQueryModel;

    goto :goto_0
.end method
