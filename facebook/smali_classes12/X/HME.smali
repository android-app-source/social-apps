.class public LX/HME;
.super LX/1OM;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Landroid/view/View;

.field public c:LX/1OM;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Landroid/view/View;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;LX/1OM;Landroid/view/View;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2456941
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2456942
    iput-object p1, p0, LX/HME;->a:Landroid/content/Context;

    .line 2456943
    iput-object p2, p0, LX/HME;->b:Landroid/view/View;

    .line 2456944
    iput-object p3, p0, LX/HME;->c:LX/1OM;

    .line 2456945
    iput-object p4, p0, LX/HME;->d:Landroid/view/View;

    .line 2456946
    iput-object p5, p0, LX/HME;->e:Landroid/view/View;

    .line 2456947
    return-void
.end method

.method private static e(I)I
    .locals 1

    .prologue
    .line 2456939
    const/4 v0, 0x1

    move v0, v0

    .line 2456940
    sub-int v0, p0, v0

    return v0
.end method

.method private f()I
    .locals 1

    .prologue
    .line 2456938
    iget-object v0, p0, LX/HME;->d:Landroid/view/View;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private g()I
    .locals 1

    .prologue
    .line 2456937
    invoke-direct {p0}, LX/HME;->f()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public static i(LX/HME;)I
    .locals 2

    .prologue
    .line 2456936
    invoke-virtual {p0}, LX/1OM;->ij_()I

    move-result v0

    invoke-direct {p0}, LX/HME;->g()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2456923
    if-gtz p2, :cond_4

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2456924
    if-eqz v0, :cond_0

    .line 2456925
    iget-object v0, p0, LX/HME;->c:LX/1OM;

    .line 2456926
    rsub-int/lit8 v1, p2, 0x0

    move v1, v1

    .line 2456927
    invoke-virtual {v0, p1, v1}, LX/1OM;->a(Landroid/view/ViewGroup;I)LX/1a1;

    move-result-object v0

    .line 2456928
    :goto_1
    return-object v0

    .line 2456929
    :cond_0
    sget v0, LX/HMD;->a:I

    if-ne p2, v0, :cond_1

    .line 2456930
    new-instance v0, LX/HMC;

    iget-object v1, p0, LX/HME;->b:Landroid/view/View;

    invoke-direct {v0, v1}, LX/HMC;-><init>(Landroid/view/View;)V

    goto :goto_1

    .line 2456931
    :cond_1
    sget v0, LX/HMD;->b:I

    if-ne p2, v0, :cond_2

    .line 2456932
    new-instance v0, LX/HMC;

    iget-object v1, p0, LX/HME;->d:Landroid/view/View;

    invoke-direct {v0, v1}, LX/HMC;-><init>(Landroid/view/View;)V

    goto :goto_1

    .line 2456933
    :cond_2
    sget v0, LX/HMD;->c:I

    if-ne p2, v0, :cond_3

    .line 2456934
    new-instance v0, LX/HMC;

    iget-object v1, p0, LX/HME;->e:Landroid/view/View;

    invoke-direct {v0, v1}, LX/HMC;-><init>(Landroid/view/View;)V

    goto :goto_1

    .line 2456935
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot create ViewHolder for itemViewType: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/1a1;I)V
    .locals 3

    .prologue
    .line 2456913
    const/4 v0, 0x1

    move v0, v0

    .line 2456914
    if-lt p2, v0, :cond_2

    invoke-static {p0}, LX/HME;->i(LX/HME;)I

    move-result v0

    if-ge p2, v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2456915
    if-eqz v0, :cond_1

    .line 2456916
    iget-object v0, p0, LX/HME;->c:LX/1OM;

    invoke-static {p2}, LX/HME;->e(I)I

    move-result v1

    invoke-virtual {v0, p1, v1}, LX/1OM;->a(LX/1a1;I)V

    .line 2456917
    :cond_0
    return-void

    .line 2456918
    :cond_1
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    .line 2456919
    sget v1, LX/HMD;->a:I

    if-eq v0, v1, :cond_0

    .line 2456920
    sget v1, LX/HMD;->b:I

    if-eq v0, v1, :cond_0

    .line 2456921
    sget v1, LX/HMD;->c:I

    if-eq v0, v1, :cond_0

    .line 2456922
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot bind ViewHolder for position: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 2456902
    const/4 v0, 0x1

    move v0, v0

    .line 2456903
    if-ge p1, v0, :cond_0

    .line 2456904
    sget v0, LX/HMD;->a:I

    .line 2456905
    :goto_0
    return v0

    .line 2456906
    :cond_0
    invoke-static {p0}, LX/HME;->i(LX/HME;)I

    move-result v0

    if-ge p1, v0, :cond_1

    .line 2456907
    iget-object v0, p0, LX/HME;->c:LX/1OM;

    invoke-static {p1}, LX/HME;->e(I)I

    move-result v1

    invoke-virtual {v0, v1}, LX/1OM;->getItemViewType(I)I

    move-result v0

    .line 2456908
    rsub-int/lit8 v1, v0, 0x0

    move v0, v1

    .line 2456909
    goto :goto_0

    .line 2456910
    :cond_1
    invoke-static {p0}, LX/HME;->i(LX/HME;)I

    move-result v0

    invoke-direct {p0}, LX/HME;->f()I

    move-result v1

    add-int/2addr v0, v1

    if-ge p1, v0, :cond_2

    .line 2456911
    sget v0, LX/HMD;->b:I

    goto :goto_0

    .line 2456912
    :cond_2
    sget v0, LX/HMD;->c:I

    goto :goto_0
.end method

.method public final ij_()I
    .locals 2

    .prologue
    .line 2456899
    const/4 v0, 0x1

    move v0, v0

    .line 2456900
    iget-object v1, p0, LX/HME;->c:LX/1OM;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/HME;->c:LX/1OM;

    invoke-virtual {v1}, LX/1OM;->ij_()I

    move-result v1

    :goto_0
    move v1, v1

    .line 2456901
    add-int/2addr v0, v1

    invoke-direct {p0}, LX/HME;->g()I

    move-result v1

    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
