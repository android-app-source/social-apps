.class public LX/HYF;
.super LX/HYA;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/HYF;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2480358
    invoke-direct {p0}, LX/HYA;-><init>()V

    .line 2480359
    return-void
.end method

.method public static a(LX/0QB;)LX/HYF;
    .locals 3

    .prologue
    .line 2480362
    sget-object v0, LX/HYF;->a:LX/HYF;

    if-nez v0, :cond_1

    .line 2480363
    const-class v1, LX/HYF;

    monitor-enter v1

    .line 2480364
    :try_start_0
    sget-object v0, LX/HYF;->a:LX/HYF;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2480365
    if-eqz v2, :cond_0

    .line 2480366
    :try_start_1
    new-instance v0, LX/HYF;

    invoke-direct {v0}, LX/HYF;-><init>()V

    .line 2480367
    move-object v0, v0

    .line 2480368
    sput-object v0, LX/HYF;->a:LX/HYF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2480369
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2480370
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2480371
    :cond_1
    sget-object v0, LX/HYF;->a:LX/HYF;

    return-object v0

    .line 2480372
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2480373
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lorg/json/JSONObject;)LX/HYB;
    .locals 2

    .prologue
    .line 2480361
    new-instance v0, LX/HYE;

    invoke-direct {v0, p1}, LX/HYE;-><init>(Lorg/json/JSONObject;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2480360
    const-string v0, "dialogIsInteractive"

    return-object v0
.end method
