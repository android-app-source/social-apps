.class public final LX/Hu8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/HuA;


# direct methods
.method public constructor <init>(LX/HuA;)V
    .locals 0

    .prologue
    .line 2517163
    iput-object p1, p0, LX/Hu8;->a:LX/HuA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x2

    const v0, 0x22079784

    invoke-static {v5, v4, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2517164
    iget-object v0, p0, LX/Hu8;->a:LX/HuA;

    iget-object v0, v0, LX/HuA;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 2517165
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0j0;

    check-cast v1, LX/0j8;

    invoke-interface {v1}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a(Lcom/facebook/ipc/composer/model/ComposerLocationInfo;)LX/5RO;

    move-result-object v1

    const/4 v3, 0x0

    .line 2517166
    iput-object v3, v1, LX/5RO;->f:Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    .line 2517167
    move-object v1, v1

    .line 2517168
    iput-boolean v4, v1, LX/5RO;->e:Z

    .line 2517169
    move-object v1, v1

    .line 2517170
    invoke-virtual {v1}, LX/5RO;->b()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v3

    move-object v1, v0

    .line 2517171
    check-cast v1, LX/0im;

    invoke-interface {v1}, LX/0im;->c()LX/0jJ;

    move-result-object v1

    sget-object v4, LX/HuA;->a:LX/0jK;

    invoke-virtual {v1, v4}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v1

    check-cast v1, LX/0jL;

    invoke-virtual {v1, v3}, LX/0jL;->a(Lcom/facebook/ipc/composer/model/ComposerLocationInfo;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jL;

    invoke-virtual {v1}, LX/0jL;->a()V

    .line 2517172
    iget-object v1, p0, LX/Hu8;->a:LX/HuA;

    iget-object v1, v1, LX/HuA;->f:LX/0gd;

    sget-object v3, LX/0ge;->COMPOSER_IMPLICIT_LOCATION_REMOVED:LX/0ge;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0j0;

    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 2517173
    const v0, -0x1a937fb2

    invoke-static {v5, v5, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
