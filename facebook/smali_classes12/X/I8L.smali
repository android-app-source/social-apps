.class public final LX/I8L;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/I8M;


# direct methods
.method public constructor <init>(LX/I8M;)V
    .locals 0

    .prologue
    .line 2540976
    iput-object p1, p0, LX/I8L;->a:LX/I8M;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v7, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x2

    const v0, -0x7b7be110

    invoke-static {v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 2540977
    iget-object v0, p0, LX/I8L;->a:LX/I8M;

    iget-object v0, v0, LX/I8M;->p:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel$EdgesModel$NodeModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/I8L;->a:LX/I8M;

    iget-object v0, v0, LX/I8M;->p:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel$EdgesModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2540978
    :cond_0
    const v0, -0x19d1aaa1

    invoke-static {v2, v2, v0, v3}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2540979
    :goto_0
    return-void

    .line 2540980
    :cond_1
    iget-object v0, p0, LX/I8L;->a:LX/I8M;

    iget-object v0, v0, LX/I8M;->p:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel$EdgesModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v4

    .line 2540981
    iget-object v0, p0, LX/I8L;->a:LX/I8M;

    iget-object v0, v0, LX/I8M;->m:LX/1nQ;

    iget-object v5, p0, LX/I8L;->a:LX/I8M;

    iget-object v5, v5, LX/I8M;->r:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v5, v5, Lcom/facebook/events/common/EventAnalyticsParams;->d:Ljava/lang/String;

    sget-object v6, Lcom/facebook/events/common/ActionMechanism;->PERMALINK_ACTIVITY_TAB:Lcom/facebook/events/common/ActionMechanism;

    .line 2540982
    iget-object v8, v0, LX/1nQ;->i:LX/0Zb;

    const-string v9, "event_permalink_activity_tapped"

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v8

    .line 2540983
    invoke-virtual {v8}, LX/0oG;->a()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 2540984
    const-string v9, "event_permalink"

    invoke-virtual {v8, v9}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v8

    iget-object v9, v0, LX/1nQ;->j:LX/0kv;

    iget-object v10, v0, LX/1nQ;->g:Landroid/content/Context;

    invoke-virtual {v9, v10}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    move-result-object v8

    const-string v9, "source_module"

    invoke-virtual {v8, v9, v5}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v8

    const-string v9, "mechanism"

    invoke-virtual {v8, v9, v6}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    move-result-object v8

    const-string v9, "target_story_type"

    invoke-virtual {v8, v9, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v8

    invoke-virtual {v8}, LX/0oG;->d()V

    .line 2540985
    :cond_2
    const/4 v0, -0x1

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :cond_3
    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 2540986
    :goto_2
    const v0, -0x4370149a

    invoke-static {v0, v3}, LX/02F;->a(II)V

    goto :goto_0

    .line 2540987
    :sswitch_0
    const-string v1, "plan_mall_activity"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_1
    const-string v2, "admin_plan_mall_activity"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    move v0, v1

    goto :goto_1

    :sswitch_2
    const-string v1, "feedback_reaction_generic"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    move v0, v2

    goto :goto_1

    :sswitch_3
    const-string v1, "event_mall_comment"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v0, 0x3

    goto :goto_1

    :sswitch_4
    const-string v1, "event_comment_mention"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v0, 0x4

    goto :goto_1

    :sswitch_5
    const-string v1, "event_mall_reply"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v0, 0x5

    goto :goto_1

    :sswitch_6
    const-string v1, "plan_user_joined"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v0, 0x6

    goto :goto_1

    :sswitch_7
    const-string v1, "plan_user_associated"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v0, 0x7

    goto :goto_1

    :sswitch_8
    const-string v1, "plan_user_declined"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/16 v0, 0x8

    goto :goto_1

    :sswitch_9
    const-string v1, "plan_edited"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/16 v0, 0x9

    goto :goto_1

    .line 2540988
    :pswitch_0
    iget-object v0, p0, LX/I8L;->a:LX/I8M;

    iget-object v0, v0, LX/I8M;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17W;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, LX/0ax;->bz:Ljava/lang/String;

    iget-object v4, p0, LX/I8L;->a:LX/I8M;

    iget-object v4, v4, LX/I8M;->p:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel$EdgesModel$NodeModel;

    invoke-virtual {v4}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel$EdgesModel$NodeModel;->l()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel$EdgesModel$NodeModel$TargetStoryModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel$EventActivityListModel$EdgesModel$NodeModel$TargetStoryModel;->j()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4, v7}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    goto/16 :goto_2

    .line 2540989
    :pswitch_1
    iget-object v0, p0, LX/I8L;->a:LX/I8M;

    sget-object v1, LX/Blc;->PRIVATE_GOING:LX/Blc;

    invoke-static {v0, v1}, LX/I8M;->a$redex0(LX/I8M;LX/Blc;)V

    goto/16 :goto_2

    .line 2540990
    :pswitch_2
    iget-object v0, p0, LX/I8L;->a:LX/I8M;

    sget-object v1, LX/Blc;->PRIVATE_MAYBE:LX/Blc;

    invoke-static {v0, v1}, LX/I8M;->a$redex0(LX/I8M;LX/Blc;)V

    goto/16 :goto_2

    .line 2540991
    :pswitch_3
    iget-object v0, p0, LX/I8L;->a:LX/I8M;

    sget-object v1, LX/Blc;->PRIVATE_NOT_GOING:LX/Blc;

    invoke-static {v0, v1}, LX/I8M;->a$redex0(LX/I8M;LX/Blc;)V

    goto/16 :goto_2

    .line 2540992
    :pswitch_4
    iget-object v0, p0, LX/I8L;->a:LX/I8M;

    iget-object v0, v0, LX/I8M;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17W;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, LX/0ax;->B:Ljava/lang/String;

    iget-object v4, p0, LX/I8L;->a:LX/I8M;

    iget-object v4, v4, LX/I8M;->q:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    invoke-virtual {v4}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;->e()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4, v7}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    goto/16 :goto_2

    :sswitch_data_0
    .sparse-switch
        -0x6eff5cc5 -> :sswitch_2
        -0x54fe9ecc -> :sswitch_1
        -0x48e50f1c -> :sswitch_7
        -0x4240443c -> :sswitch_5
        -0x389ac79b -> :sswitch_4
        -0x186729bc -> :sswitch_0
        0x1238778c -> :sswitch_8
        0x4428c8b9 -> :sswitch_3
        0x573561ff -> :sswitch_9
        0x6c0d0227 -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
