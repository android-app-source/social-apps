.class public LX/JQZ;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JQb;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JQZ",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JQb;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2691922
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2691923
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JQZ;->b:LX/0Zi;

    .line 2691924
    iput-object p1, p0, LX/JQZ;->a:LX/0Ot;

    .line 2691925
    return-void
.end method

.method public static a(LX/0QB;)LX/JQZ;
    .locals 4

    .prologue
    .line 2691926
    const-class v1, LX/JQZ;

    monitor-enter v1

    .line 2691927
    :try_start_0
    sget-object v0, LX/JQZ;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2691928
    sput-object v2, LX/JQZ;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2691929
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2691930
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2691931
    new-instance v3, LX/JQZ;

    const/16 p0, 0x2006

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JQZ;-><init>(LX/0Ot;)V

    .line 2691932
    move-object v0, v3

    .line 2691933
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2691934
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JQZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2691935
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2691936
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 10

    .prologue
    .line 2691937
    check-cast p2, LX/JQY;

    .line 2691938
    iget-object v0, p0, LX/JQZ;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JQb;

    iget-object v1, p2, LX/JQY;->a:LX/1Pn;

    iget-object v2, p2, LX/JQY;->b:Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;

    const/16 v9, 0x8

    .line 2691939
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;->k()LX/0Px;

    move-result-object v5

    .line 2691940
    new-instance v3, LX/JQa;

    invoke-direct {v3, v0, v2}, LX/JQa;-><init>(LX/JQb;Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;)V

    .line 2691941
    invoke-static {}, LX/3mP;->newBuilder()LX/3mP;

    move-result-object v4

    .line 2691942
    iput-object v3, v4, LX/3mP;->g:LX/25K;

    .line 2691943
    move-object v3, v4

    .line 2691944
    invoke-virtual {v3}, LX/3mP;->a()LX/25M;

    move-result-object v8

    .line 2691945
    iget-object v3, v0, LX/JQb;->a:LX/JQS;

    move-object v4, p1

    move-object v6, v2

    move-object v7, v1

    invoke-virtual/range {v3 .. v8}, LX/JQS;->a(Landroid/content/Context;LX/0Px;Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;Ljava/lang/Object;LX/25M;)LX/JQR;

    move-result-object v3

    .line 2691946
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    iget-object v5, v0, LX/JQb;->c:LX/JQe;

    const/4 v6, 0x0

    .line 2691947
    new-instance v7, LX/JQd;

    invoke-direct {v7, v5}, LX/JQd;-><init>(LX/JQe;)V

    .line 2691948
    sget-object v8, LX/JQe;->a:LX/0Zi;

    invoke-virtual {v8}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/JQc;

    .line 2691949
    if-nez v8, :cond_0

    .line 2691950
    new-instance v8, LX/JQc;

    invoke-direct {v8}, LX/JQc;-><init>()V

    .line 2691951
    :cond_0
    invoke-static {v8, p1, v6, v6, v7}, LX/JQc;->a$redex0(LX/JQc;LX/1De;IILX/JQd;)V

    .line 2691952
    move-object v7, v8

    .line 2691953
    move-object v6, v7

    .line 2691954
    move-object v5, v6

    .line 2691955
    iget-object v6, v5, LX/JQc;->a:LX/JQd;

    iput-object v2, v6, LX/JQd;->a:Lcom/facebook/graphql/model/GraphQLJobCollectionFeedUnit;

    .line 2691956
    iget-object v6, v5, LX/JQc;->d:Ljava/util/BitSet;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 2691957
    move-object v5, v5

    .line 2691958
    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    iget-object v5, v0, LX/JQb;->b:LX/3mL;

    invoke-virtual {v5, p1}, LX/3mL;->c(LX/1De;)LX/3ml;

    move-result-object v5

    invoke-virtual {v5, v3}, LX/3ml;->a(LX/3mX;)LX/3ml;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    invoke-interface {v3, v9, v9}, LX/1Di;->h(II)LX/1Di;

    move-result-object v3

    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    iget-object v4, v0, LX/JQb;->d:LX/JQV;

    invoke-virtual {v4, p1}, LX/JQV;->c(LX/1De;)LX/JQT;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2691959
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2691960
    invoke-static {}, LX/1dS;->b()V

    .line 2691961
    const/4 v0, 0x0

    return-object v0
.end method
