.class public LX/Iz9;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

.field public b:LX/Iyx;

.field public c:LX/Iz5;

.field public d:Z


# direct methods
.method public constructor <init>(Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2634207
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2634208
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Iz9;->d:Z

    .line 2634209
    iput-object p1, p0, LX/Iz9;->a:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    .line 2634210
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2634211
    iget-object v0, p0, LX/Iz9;->a:Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;

    sget-object v1, LX/0rS;->PREFER_CACHE_IF_UP_TO_DATE:LX/0rS;

    iget-object v2, p0, LX/Iz9;->b:LX/Iyx;

    .line 2634212
    iget-object v3, v2, LX/Iyx;->d:Lcom/facebook/user/model/UserKey;

    move-object v2, v3

    .line 2634213
    invoke-virtual {v2}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/payments/p2p/protocol/PaymentProtocolUtil;->a(LX/0rS;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2634214
    new-instance v1, LX/Iz8;

    invoke-direct {v1, p0}, LX/Iz8;-><init>(LX/Iz9;)V

    .line 2634215
    sget-object v2, LX/131;->INSTANCE:LX/131;

    move-object v2, v2

    .line 2634216
    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/Iyy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2634217
    sget-object v0, LX/Iyy;->SUCCESS:LX/Iyy;

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
