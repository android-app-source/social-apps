.class public final LX/I7R;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/events/permalink/EventPermalinkFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/permalink/EventPermalinkFragment;)V
    .locals 0

    .prologue
    .line 2538941
    iput-object p1, p0, LX/I7R;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2538935
    iget-object v0, p0, LX/I7R;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/events/permalink/EventPermalinkFragment;->aF:LX/I7Q;

    invoke-virtual {v0, p1}, LX/I7Q;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    .line 2538936
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2538937
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2538938
    iget-object v0, p0, LX/I7R;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    invoke-static {v0}, Lcom/facebook/events/permalink/EventPermalinkFragment;->I(Lcom/facebook/events/permalink/EventPermalinkFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2538939
    :goto_0
    return-void

    .line 2538940
    :cond_0
    iget-object v0, p0, LX/I7R;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Lcom/facebook/events/permalink/EventPermalinkFragment;->a$redex0(Lcom/facebook/events/permalink/EventPermalinkFragment;Lcom/facebook/graphql/executor/GraphQLResult;Z)V

    goto :goto_0
.end method
