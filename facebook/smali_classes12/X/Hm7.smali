.class public LX/Hm7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# instance fields
.field private final a:LX/Hma;

.field private final b:LX/0Uh;

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Hmf;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Uh;LX/Hma;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2500187
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2500188
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2500189
    iput-object v0, p0, LX/Hm7;->c:LX/0Ot;

    .line 2500190
    iput-object p1, p0, LX/Hm7;->b:LX/0Uh;

    .line 2500191
    iput-object p2, p0, LX/Hm7;->a:LX/Hma;

    .line 2500192
    return-void
.end method


# virtual methods
.method public final init()V
    .locals 7

    .prologue
    .line 2500176
    iget-object v0, p0, LX/Hm7;->b:LX/0Uh;

    const/16 v1, 0x2f0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2500177
    :cond_0
    :goto_0
    return-void

    .line 2500178
    :cond_1
    iget-object v0, p0, LX/Hm7;->a:LX/Hma;

    .line 2500179
    invoke-virtual {v0}, LX/Hma;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_1
    move v0, v1

    .line 2500180
    if-eqz v0, :cond_0

    .line 2500181
    iget-object v0, p0, LX/Hm7;->a:LX/Hma;

    invoke-virtual {v0}, LX/Hma;->b()Ljava/lang/String;

    move-result-object v1

    .line 2500182
    iget-object v0, p0, LX/Hm7;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hmf;

    .line 2500183
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v2

    const-string v3, "beamTransactionID"

    invoke-virtual {v2, v3, v1}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v2

    .line 2500184
    iget-object v3, v0, LX/Hmf;->a:LX/0if;

    sget-object v4, LX/0ig;->aX:LX/0ih;

    sget-object v5, LX/Hmd;->INSTALL_SUCCEEDED:LX/Hmd;

    iget-object v5, v5, LX/Hmd;->actionName:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6, v2}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2500185
    iget-object v0, p0, LX/Hm7;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hmf;

    invoke-virtual {v0}, LX/Hmf;->b()V

    .line 2500186
    iget-object v0, p0, LX/Hm7;->a:LX/Hma;

    invoke-virtual {v0}, LX/Hma;->a()V

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method
