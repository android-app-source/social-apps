.class public LX/IP2;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/IP2;


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 5
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/fbreact/annotations/IsFb4aReactNativeEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 2573645
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2573646
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2573647
    const-string v1, "extra_parent_activity"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2573648
    const-string v1, "groups/pendingposts/{%s}"

    invoke-static {v1}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "group_feed_id"

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v3, LX/0cQ;->GROUP_PENDING_POSTS_FRAGMENT:LX/0cQ;

    invoke-virtual {v3}, LX/0cQ;->ordinal()I

    move-result v3

    invoke-virtual {p0, v1, v2, v3, v0}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;ILandroid/os/Bundle;)V

    .line 2573649
    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2573650
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2573651
    const-string v1, "enableFlaggedPostGroups"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2573652
    const-string v1, "source"

    const-string v2, "notification"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2573653
    const-string v1, "groups/flaggedreportedposts/{%s}"

    invoke-static {v1}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "group_feed_id"

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v3, LX/0cQ;->GROUP_REACT_REPORTED_POSTS_FRAGMENT:LX/0cQ;

    invoke-virtual {v3}, LX/0cQ;->ordinal()I

    move-result v3

    invoke-virtual {p0, v1, v2, v3, v0}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;ILandroid/os/Bundle;)V

    .line 2573654
    const-string v0, "groups/reportedposts/{%s}"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "group_feed_id"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v2, LX/0cQ;->GROUP_REACT_REPORTED_POSTS_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v1, v2, v3}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;ILandroid/os/Bundle;)V

    .line 2573655
    :cond_0
    return-void
.end method

.method public static a(LX/0QB;)LX/IP2;
    .locals 4

    .prologue
    .line 2573656
    sget-object v0, LX/IP2;->a:LX/IP2;

    if-nez v0, :cond_1

    .line 2573657
    const-class v1, LX/IP2;

    monitor-enter v1

    .line 2573658
    :try_start_0
    sget-object v0, LX/IP2;->a:LX/IP2;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2573659
    if-eqz v2, :cond_0

    .line 2573660
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2573661
    new-instance v3, LX/IP2;

    const/16 p0, 0x148e

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, p0}, LX/IP2;-><init>(LX/0Or;)V

    .line 2573662
    move-object v0, v3

    .line 2573663
    sput-object v0, LX/IP2;->a:LX/IP2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2573664
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2573665
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2573666
    :cond_1
    sget-object v0, LX/IP2;->a:LX/IP2;

    return-object v0

    .line 2573667
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2573668
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
