.class public final enum LX/I9k;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/I9k;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/I9k;

.field public static final enum COMPLETE:LX/I9k;

.field public static final enum ERROR:LX/I9k;

.field public static final enum INITIAL:LX/I9k;

.field public static final enum PAGING:LX/I9k;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2543428
    new-instance v0, LX/I9k;

    const-string v1, "INITIAL"

    invoke-direct {v0, v1, v2}, LX/I9k;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I9k;->INITIAL:LX/I9k;

    .line 2543429
    new-instance v0, LX/I9k;

    const-string v1, "PAGING"

    invoke-direct {v0, v1, v3}, LX/I9k;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I9k;->PAGING:LX/I9k;

    .line 2543430
    new-instance v0, LX/I9k;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v4}, LX/I9k;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I9k;->ERROR:LX/I9k;

    .line 2543431
    new-instance v0, LX/I9k;

    const-string v1, "COMPLETE"

    invoke-direct {v0, v1, v5}, LX/I9k;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/I9k;->COMPLETE:LX/I9k;

    .line 2543432
    const/4 v0, 0x4

    new-array v0, v0, [LX/I9k;

    sget-object v1, LX/I9k;->INITIAL:LX/I9k;

    aput-object v1, v0, v2

    sget-object v1, LX/I9k;->PAGING:LX/I9k;

    aput-object v1, v0, v3

    sget-object v1, LX/I9k;->ERROR:LX/I9k;

    aput-object v1, v0, v4

    sget-object v1, LX/I9k;->COMPLETE:LX/I9k;

    aput-object v1, v0, v5

    sput-object v0, LX/I9k;->$VALUES:[LX/I9k;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2543433
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/I9k;
    .locals 1

    .prologue
    .line 2543434
    const-class v0, LX/I9k;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/I9k;

    return-object v0
.end method

.method public static values()[LX/I9k;
    .locals 1

    .prologue
    .line 2543435
    sget-object v0, LX/I9k;->$VALUES:[LX/I9k;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/I9k;

    return-object v0
.end method
