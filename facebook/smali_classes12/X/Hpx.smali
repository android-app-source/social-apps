.class public final LX/Hpx;
.super LX/BcS;
.source ""


# static fields
.field private static a:LX/Hpx;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Hpv;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/Hpy;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2510144
    const/4 v0, 0x0

    sput-object v0, LX/Hpx;->a:LX/Hpx;

    .line 2510145
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Hpx;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2510146
    invoke-direct {p0}, LX/BcS;-><init>()V

    .line 2510147
    new-instance v0, LX/Hpy;

    invoke-direct {v0}, LX/Hpy;-><init>()V

    iput-object v0, p0, LX/Hpx;->c:LX/Hpy;

    .line 2510148
    return-void
.end method

.method public static declared-synchronized d()LX/Hpx;
    .locals 2

    .prologue
    .line 2510149
    const-class v1, LX/Hpx;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/Hpx;->a:LX/Hpx;

    if-nez v0, :cond_0

    .line 2510150
    new-instance v0, LX/Hpx;

    invoke-direct {v0}, LX/Hpx;-><init>()V

    sput-object v0, LX/Hpx;->a:LX/Hpx;

    .line 2510151
    :cond_0
    sget-object v0, LX/Hpx;->a:LX/Hpx;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 2510152
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/BcQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2510153
    invoke-static {}, LX/1dS;->b()V

    .line 2510154
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/BcP;Ljava/util/List;LX/BcO;)V
    .locals 1

    .prologue
    .line 2510155
    check-cast p3, LX/Hpw;

    .line 2510156
    iget-object v0, p3, LX/Hpw;->b:Ljava/util/List;

    .line 2510157
    invoke-interface {p2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2510158
    return-void
.end method
