.class public final LX/HJ4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/2km;

.field public final synthetic b:LX/9uc;

.field public final synthetic c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

.field public final synthetic d:Lcom/facebook/pages/common/reaction/components/PageContextualRecommendationFooterComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/reaction/components/PageContextualRecommendationFooterComponentPartDefinition;LX/2km;LX/9uc;Lcom/facebook/reaction/common/ReactionUnitComponentNode;)V
    .locals 0

    .prologue
    .line 2452007
    iput-object p1, p0, LX/HJ4;->d:Lcom/facebook/pages/common/reaction/components/PageContextualRecommendationFooterComponentPartDefinition;

    iput-object p2, p0, LX/HJ4;->a:LX/2km;

    iput-object p3, p0, LX/HJ4;->b:LX/9uc;

    iput-object p4, p0, LX/HJ4;->c:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x19349d76

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2452008
    iget-object v0, p0, LX/HJ4;->a:LX/2km;

    check-cast v0, LX/1Pn;

    invoke-interface {v0}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v7

    move-object v0, p1

    .line 2452009
    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbButton;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    const v1, 0x7f08178a

    invoke-virtual {v7, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v8

    .line 2452010
    iget-object v0, p0, LX/HJ4;->d:Lcom/facebook/pages/common/reaction/components/PageContextualRecommendationFooterComponentPartDefinition;

    iget-object v0, v0, Lcom/facebook/pages/common/reaction/components/PageContextualRecommendationFooterComponentPartDefinition;->c:LX/5up;

    if-eqz v8, :cond_0

    sget-object v1, LX/5uo;->UNSAVE:LX/5uo;

    :goto_0
    iget-object v2, p0, LX/HJ4;->b:LX/9uc;

    invoke-interface {v2}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;->R()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionActionFatFieldsModel$ProfileModel;->j()Ljava/lang/String;

    move-result-object v2

    const-string v3, "native_page_profile"

    const-string v4, "toggle_button"

    new-instance v5, LX/HJ3;

    invoke-direct {v5, p0, v8, v7, p1}, LX/HJ3;-><init>(LX/HJ4;ZLandroid/content/Context;Landroid/view/View;)V

    invoke-virtual/range {v0 .. v5}, LX/5up;->a(LX/5uo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2h0;)V

    .line 2452011
    const v0, -0x7499c851

    invoke-static {v0, v6}, LX/02F;->a(II)V

    return-void

    .line 2452012
    :cond_0
    sget-object v1, LX/5uo;->SAVE:LX/5uo;

    goto :goto_0
.end method
