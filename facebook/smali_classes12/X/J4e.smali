.class public LX/J4e;
.super LX/1Qj;
.source ""

# interfaces
.implements LX/1Pf;
.implements LX/Boz;
.implements LX/Bp2;
.implements LX/J4O;


# instance fields
.field private final n:LX/1PT;

.field private final o:LX/1QW;

.field private final p:LX/J4h;

.field private final q:LX/IC2;

.field private final r:LX/J4O;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/Runnable;LX/1PT;LX/1QW;LX/J4h;LX/IC2;LX/J4O;)V
    .locals 1

    .prologue
    .line 2644414
    sget-object v0, LX/1PU;->b:LX/1PY;

    invoke-direct {p0, p1, p2, v0}, LX/1Qj;-><init>(Landroid/content/Context;Ljava/lang/Runnable;LX/1PY;)V

    .line 2644415
    iput-object p3, p0, LX/J4e;->n:LX/1PT;

    .line 2644416
    iput-object p4, p0, LX/J4e;->o:LX/1QW;

    .line 2644417
    iput-object p5, p0, LX/J4e;->p:LX/J4h;

    .line 2644418
    iput-object p6, p0, LX/J4e;->q:LX/IC2;

    .line 2644419
    iput-object p7, p0, LX/J4e;->r:LX/J4O;

    .line 2644420
    return-void
.end method


# virtual methods
.method public final a(LX/1Rb;)V
    .locals 1

    .prologue
    .line 2644412
    iget-object v0, p0, LX/J4e;->o:LX/1QW;

    invoke-virtual {v0, p1}, LX/1QW;->a(LX/1Rb;)V

    .line 2644413
    return-void
.end method

.method public final a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 2644410
    iget-object v0, p0, LX/J4e;->o:LX/1QW;

    invoke-virtual {v0, p1, p2}, LX/1QW;->a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2644411
    return-void
.end method

.method public final a(LX/1f9;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 2644408
    iget-object v0, p0, LX/J4e;->o:LX/1QW;

    invoke-virtual {v0, p1, p2, p3}, LX/1QW;->a(LX/1f9;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2644409
    return-void
.end method

.method public final a(LX/5kD;LX/1bf;ZI)V
    .locals 1

    .prologue
    .line 2644406
    iget-object v0, p0, LX/J4e;->p:LX/J4h;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/J4h;->a(LX/5kD;LX/1bf;ZI)V

    .line 2644407
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLFeedback;Landroid/view/View;LX/An0;)V
    .locals 2

    .prologue
    .line 2644389
    iget-object v0, p0, LX/J4e;->q:LX/IC2;

    .line 2644390
    if-nez p1, :cond_0

    .line 2644391
    :goto_0
    return-void

    .line 2644392
    :cond_0
    new-instance v1, LX/8qL;

    invoke-direct {v1}, LX/8qL;-><init>()V

    .line 2644393
    iput-object p1, v1, LX/8qL;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2644394
    move-object v1, v1

    .line 2644395
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object p0

    .line 2644396
    iput-object p0, v1, LX/8qL;->d:Ljava/lang/String;

    .line 2644397
    move-object v1, v1

    .line 2644398
    new-instance p0, LX/21A;

    invoke-direct {p0}, LX/21A;-><init>()V

    const-string p3, "privacy_photo_checkup"

    .line 2644399
    iput-object p3, p0, LX/21A;->c:Ljava/lang/String;

    .line 2644400
    move-object p0, p0

    .line 2644401
    invoke-virtual {p0}, LX/21A;->b()Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-result-object p0

    .line 2644402
    iput-object p0, v1, LX/8qL;->g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 2644403
    move-object v1, v1

    .line 2644404
    invoke-virtual {v1}, LX/8qL;->a()Lcom/facebook/ufiservices/flyout/FeedbackParams;

    move-result-object v1

    .line 2644405
    iget-object p0, v0, LX/IC2;->a:LX/1nI;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p3

    invoke-interface {p0, p3, v1}, LX/1nI;->a(Landroid/content/Context;Lcom/facebook/ufiservices/flyout/FeedbackParams;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;LX/1oT;)V
    .locals 1

    .prologue
    .line 2644387
    iget-object v0, p0, LX/J4e;->r:LX/J4O;

    invoke-interface {v0, p1, p2}, LX/J4O;->a(Ljava/lang/String;LX/1oT;)V

    .line 2644388
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 2644421
    iget-object v0, p0, LX/J4e;->r:LX/J4O;

    invoke-interface {v0, p1}, LX/J4O;->a(Z)V

    .line 2644422
    return-void
.end method

.method public final c()LX/1PT;
    .locals 1

    .prologue
    .line 2644386
    iget-object v0, p0, LX/J4e;->n:LX/1PT;

    return-object v0
.end method

.method public final e()LX/1SX;
    .locals 1

    .prologue
    .line 2644385
    const/4 v0, 0x0

    return-object v0
.end method

.method public final g_(Ljava/lang/String;)LX/1oT;
    .locals 1

    .prologue
    .line 2644378
    iget-object v0, p0, LX/J4e;->r:LX/J4O;

    invoke-interface {v0, p1}, LX/J4O;->g_(Ljava/lang/String;)LX/1oT;

    move-result-object v0

    return-object v0
.end method

.method public final m()LX/1f9;
    .locals 1

    .prologue
    .line 2644384
    iget-object v0, p0, LX/J4e;->o:LX/1QW;

    invoke-virtual {v0}, LX/1QW;->m()LX/1f9;

    move-result-object v0

    return-object v0
.end method

.method public final mN_()LX/1oT;
    .locals 1

    .prologue
    .line 2644383
    iget-object v0, p0, LX/J4e;->r:LX/J4O;

    invoke-interface {v0}, LX/J4O;->mN_()LX/1oT;

    move-result-object v0

    return-object v0
.end method

.method public final o()LX/1Rb;
    .locals 1

    .prologue
    .line 2644382
    iget-object v0, p0, LX/J4e;->o:LX/1QW;

    invoke-virtual {v0}, LX/1QW;->o()LX/1Rb;

    move-result-object v0

    return-object v0
.end method

.method public final p()V
    .locals 1

    .prologue
    .line 2644380
    iget-object v0, p0, LX/J4e;->o:LX/1QW;

    invoke-virtual {v0}, LX/1QW;->p()V

    .line 2644381
    return-void
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 2644379
    iget-object v0, p0, LX/J4e;->o:LX/1QW;

    invoke-virtual {v0}, LX/1QW;->q()Z

    move-result v0

    return v0
.end method
