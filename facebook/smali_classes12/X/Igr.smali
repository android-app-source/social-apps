.class public final LX/Igr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4oG;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/media/mediapicker/PhotoItemController;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/media/mediapicker/PhotoItemController;)V
    .locals 0

    .prologue
    .line 2601768
    iput-object p1, p0, LX/Igr;->a:Lcom/facebook/messaging/media/mediapicker/PhotoItemController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 6

    .prologue
    .line 2601769
    iget-object v0, p0, LX/Igr;->a:Lcom/facebook/messaging/media/mediapicker/PhotoItemController;

    iget-object v0, v0, Lcom/facebook/messaging/media/mediapicker/PhotoItemController;->w:LX/Igl;

    if-eqz v0, :cond_0

    .line 2601770
    iget-object v0, p0, LX/Igr;->a:Lcom/facebook/messaging/media/mediapicker/PhotoItemController;

    iget-object v0, v0, Lcom/facebook/messaging/media/mediapicker/PhotoItemController;->x:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2601771
    iget-object v0, p0, LX/Igr;->a:Lcom/facebook/messaging/media/mediapicker/PhotoItemController;

    iget-object v0, v0, Lcom/facebook/messaging/media/mediapicker/PhotoItemController;->w:LX/Igl;

    iget-object v1, p0, LX/Igr;->a:Lcom/facebook/messaging/media/mediapicker/PhotoItemController;

    iget-object v1, v1, Lcom/facebook/messaging/media/mediapicker/PhotoItemController;->x:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2601772
    if-eqz p1, :cond_1

    .line 2601773
    iget-object v2, v0, LX/Igl;->a:LX/Ign;

    iget-object v2, v2, LX/Ign;->j:LX/Igt;

    invoke-virtual {v2, v1}, LX/Igt;->a(Lcom/facebook/ui/media/attachments/MediaResource;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2601774
    :cond_0
    :goto_0
    return-void

    .line 2601775
    :cond_1
    iget-object v2, v0, LX/Igl;->a:LX/Ign;

    iget-object v2, v2, LX/Ign;->j:LX/Igt;

    invoke-virtual {v2, v1}, LX/Igt;->b(Lcom/facebook/ui/media/attachments/MediaResource;)V

    .line 2601776
    goto :goto_0

    .line 2601777
    :cond_2
    iget-object v2, v0, LX/Igl;->a:LX/Ign;

    iget-object v2, v2, LX/Ign;->j:LX/Igt;

    iget-object v3, v0, LX/Igl;->a:LX/Ign;

    iget-object v3, v3, LX/Ign;->f:Landroid/content/Context;

    .line 2601778
    const v4, 0x7f082e02

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 p0, 0x0

    iget p1, v2, LX/Igt;->a:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v5, p0

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, LX/0kL;->a(Landroid/content/Context;Ljava/lang/CharSequence;)V

    .line 2601779
    iget-object v2, v0, LX/Igl;->a:LX/Ign;

    invoke-static {v2}, LX/Ign;->d$redex0(LX/Ign;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 2601780
    if-ltz v2, :cond_0

    .line 2601781
    iget-object v3, v0, LX/Igl;->a:LX/Ign;

    invoke-virtual {v3, v2}, LX/1OM;->i_(I)V

    goto :goto_0
.end method
