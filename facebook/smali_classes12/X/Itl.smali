.class public final LX/Itl;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/Itm;

.field private b:Lcom/facebook/messaging/model/messages/Message;

.field private c:LX/6fP;

.field private d:LX/6f3;

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:I


# direct methods
.method public constructor <init>(LX/Itm;)V
    .locals 1

    .prologue
    .line 2624280
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2624281
    sget-object v0, LX/6fP;->NONE:LX/6fP;

    iput-object v0, p0, LX/Itl;->c:LX/6fP;

    .line 2624282
    sget-object v0, LX/6f3;->UNKNOWN:LX/6f3;

    iput-object v0, p0, LX/Itl;->d:LX/6f3;

    .line 2624283
    iput-object p1, p0, LX/Itl;->a:LX/Itm;

    .line 2624284
    return-void
.end method

.method private a(LX/6f7;)LX/6f7;
    .locals 8

    .prologue
    .line 2624285
    iget-object v0, p0, LX/Itl;->g:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2624286
    :goto_0
    return-object p1

    .line 2624287
    :cond_0
    new-instance v0, Lcom/facebook/messaging/model/payment/PaymentTransactionData;

    iget-object v1, p0, LX/Itl;->g:Ljava/lang/String;

    iget-object v2, p0, LX/Itl;->b:Lcom/facebook/messaging/model/messages/Message;

    iget-object v2, v2, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v2, v2, Lcom/facebook/messaging/model/threadkey/ThreadKey;->e:J

    iget-object v4, p0, LX/Itl;->b:Lcom/facebook/messaging/model/messages/Message;

    iget-object v4, v4, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v4, v4, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    iget-object v6, p0, LX/Itl;->b:Lcom/facebook/messaging/model/messages/Message;

    iget-object v6, v6, Lcom/facebook/messaging/model/messages/Message;->u:Lcom/facebook/messaging/model/share/SentShareAttachment;

    iget-object v6, v6, Lcom/facebook/messaging/model/share/SentShareAttachment;->c:Lcom/facebook/messaging/model/payment/SentPayment;

    iget-object v6, v6, Lcom/facebook/messaging/model/payment/SentPayment;->a:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v6}, Lcom/facebook/payments/currency/CurrencyAmount;->c()I

    move-result v6

    iget-object v7, p0, LX/Itl;->b:Lcom/facebook/messaging/model/messages/Message;

    iget-object v7, v7, Lcom/facebook/messaging/model/messages/Message;->u:Lcom/facebook/messaging/model/share/SentShareAttachment;

    iget-object v7, v7, Lcom/facebook/messaging/model/share/SentShareAttachment;->c:Lcom/facebook/messaging/model/payment/SentPayment;

    iget-object v7, v7, Lcom/facebook/messaging/model/payment/SentPayment;->a:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 2624288
    iget-object p0, v7, Lcom/facebook/payments/currency/CurrencyAmount;->b:Ljava/lang/String;

    move-object v7, p0

    .line 2624289
    invoke-direct/range {v0 .. v7}, Lcom/facebook/messaging/model/payment/PaymentTransactionData;-><init>(Ljava/lang/String;JJILjava/lang/String;)V

    .line 2624290
    iput-object v0, p1, LX/6f7;->B:Lcom/facebook/messaging/model/payment/PaymentTransactionData;

    .line 2624291
    goto :goto_0
.end method


# virtual methods
.method public final a(I)LX/Itl;
    .locals 0

    .prologue
    .line 2624292
    iput p1, p0, LX/Itl;->h:I

    .line 2624293
    return-object p0
.end method

.method public final a(LX/6f3;)LX/Itl;
    .locals 1

    .prologue
    .line 2624294
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6f3;

    iput-object v0, p0, LX/Itl;->d:LX/6f3;

    .line 2624295
    return-object p0
.end method

.method public final a(LX/6fP;)LX/Itl;
    .locals 1

    .prologue
    .line 2624296
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6fP;

    iput-object v0, p0, LX/Itl;->c:LX/6fP;

    .line 2624297
    return-object p0
.end method

.method public final a(Lcom/facebook/messaging/model/messages/Message;)LX/Itl;
    .locals 1

    .prologue
    .line 2624298
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    iput-object v0, p0, LX/Itl;->b:Lcom/facebook/messaging/model/messages/Message;

    .line 2624299
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/Itl;
    .locals 0

    .prologue
    .line 2624300
    iput-object p1, p0, LX/Itl;->e:Ljava/lang/String;

    .line 2624301
    return-object p0
.end method

.method public final a()Lcom/facebook/messaging/model/messages/Message;
    .locals 5

    .prologue
    .line 2624302
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v1

    iget-object v0, p0, LX/Itl;->b:Lcom/facebook/messaging/model/messages/Message;

    const-string v2, "original message is not set"

    invoke-static {v0, v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    invoke-virtual {v1, v0}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6f7;

    move-result-object v0

    sget-object v1, LX/2uW;->FAILED_SEND:LX/2uW;

    .line 2624303
    iput-object v1, v0, LX/6f7;->l:LX/2uW;

    .line 2624304
    move-object v0, v0

    .line 2624305
    iget-object v1, p0, LX/Itl;->d:LX/6f3;

    invoke-virtual {v0, v1}, LX/6f7;->a(LX/6f3;)LX/6f7;

    move-result-object v0

    invoke-static {}, Lcom/facebook/messaging/model/send/SendError;->newBuilder()LX/6fO;

    move-result-object v1

    iget-object v2, p0, LX/Itl;->c:LX/6fP;

    .line 2624306
    iput-object v2, v1, LX/6fO;->a:LX/6fP;

    .line 2624307
    move-object v1, v1

    .line 2624308
    iget-object v2, p0, LX/Itl;->e:Ljava/lang/String;

    .line 2624309
    iput-object v2, v1, LX/6fO;->b:Ljava/lang/String;

    .line 2624310
    move-object v1, v1

    .line 2624311
    iget v2, p0, LX/Itl;->h:I

    .line 2624312
    iput v2, v1, LX/6fO;->e:I

    .line 2624313
    move-object v1, v1

    .line 2624314
    iget-object v2, p0, LX/Itl;->a:LX/Itm;

    iget-object v2, v2, LX/Itm;->d:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    .line 2624315
    iput-wide v2, v1, LX/6fO;->c:J

    .line 2624316
    move-object v1, v1

    .line 2624317
    iget-object v2, p0, LX/Itl;->f:Ljava/lang/String;

    .line 2624318
    iput-object v2, v1, LX/6fO;->d:Ljava/lang/String;

    .line 2624319
    move-object v1, v1

    .line 2624320
    invoke-virtual {v1}, LX/6fO;->g()Lcom/facebook/messaging/model/send/SendError;

    move-result-object v1

    .line 2624321
    iput-object v1, v0, LX/6f7;->u:Lcom/facebook/messaging/model/send/SendError;

    .line 2624322
    move-object v0, v0

    .line 2624323
    invoke-direct {p0, v0}, LX/Itl;->a(LX/6f7;)LX/6f7;

    move-result-object v0

    .line 2624324
    invoke-virtual {v0}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    return-object v0
.end method
