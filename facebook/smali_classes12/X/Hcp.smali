.class public final LX/Hcp;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Hcq;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;"
        }
    .end annotation
.end field


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2487521
    const-string v0, "TopicSocialContextComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2487522
    if-ne p0, p1, :cond_1

    .line 2487523
    :cond_0
    :goto_0
    return v0

    .line 2487524
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2487525
    goto :goto_0

    .line 2487526
    :cond_3
    check-cast p1, LX/Hcp;

    .line 2487527
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2487528
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2487529
    if-eq v2, v3, :cond_0

    .line 2487530
    iget-object v2, p0, LX/Hcp;->a:Ljava/util/List;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/Hcp;->a:Ljava/util/List;

    iget-object v3, p1, LX/Hcp;->a:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2487531
    goto :goto_0

    .line 2487532
    :cond_4
    iget-object v2, p1, LX/Hcp;->a:Ljava/util/List;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
