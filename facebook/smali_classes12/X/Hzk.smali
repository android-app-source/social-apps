.class public final LX/Hzk;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;)V
    .locals 0

    .prologue
    .line 2525916
    iput-object p1, p0, LX/Hzk;->a:Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2525917
    iget-object v0, p0, LX/Hzk;->a:Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->a$redex0(Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;Z)V

    .line 2525918
    iget-object v0, p0, LX/Hzk;->a:Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;

    invoke-static {v0}, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->p(Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;)V

    .line 2525919
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2525920
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2525921
    iget-object v0, p0, LX/Hzk;->a:Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->a$redex0(Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;Z)V

    .line 2525922
    iget-object v0, p0, LX/Hzk;->a:Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;

    .line 2525923
    const/4 v2, 0x0

    .line 2525924
    if-eqz p1, :cond_6

    .line 2525925
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2525926
    if-eqz v1, :cond_6

    .line 2525927
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2525928
    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel$AllEventsModel;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 2525929
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2525930
    check-cast v1, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;

    invoke-virtual {v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel$AllEventsModel;

    move-result-object v2

    .line 2525931
    iget-object v1, v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->d:LX/Hyx;

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel$AllEventsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v1, v3, p1}, LX/Hyx;->a(LX/0Px;Lcom/facebook/graphql/executor/GraphQLResult;)LX/0Px;

    move-result-object v1

    .line 2525932
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel$AllEventsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 2525933
    iget-object v3, v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->d:LX/Hyx;

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel$AllEventsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;->a()Ljava/lang/String;

    move-result-object p0

    .line 2525934
    iput-object p0, v3, LX/Hyx;->a:Ljava/lang/String;

    .line 2525935
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchUpcomingEventsQueryModel$AllEventsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;->b()Z

    move-result v2

    iput-boolean v2, v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->y:Z

    .line 2525936
    :goto_0
    iget-object v2, v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->d:LX/Hyx;

    invoke-virtual {v2, v1}, LX/Hyx;->a(LX/0Px;)V

    .line 2525937
    :goto_1
    iget-object v2, v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->x:LX/I05;

    sget-object v3, LX/I05;->FIRST_LOAD:LX/I05;

    if-ne v2, v3, :cond_3

    .line 2525938
    iget-object v2, v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->r:LX/Hzt;

    .line 2525939
    iget-object v3, v2, LX/Hzt;->f:LX/I2i;

    .line 2525940
    iget-object p0, v3, LX/I2i;->c:Ljava/util/List;

    if-ne p0, v1, :cond_7

    .line 2525941
    const/4 p0, 0x0

    .line 2525942
    :goto_2
    move v3, p0

    .line 2525943
    if-eqz v3, :cond_0

    .line 2525944
    invoke-static {v2}, LX/Hzt;->k(LX/Hzt;)V

    .line 2525945
    :cond_0
    :goto_3
    sget-object v1, LX/I05;->LOADED:LX/I05;

    iput-object v1, v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->x:LX/I05;

    .line 2525946
    iget-object v1, v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->B:LX/Hx7;

    sget-object v2, LX/Hx7;->CALENDAR:LX/Hx7;

    if-ne v1, v2, :cond_1

    .line 2525947
    iget-object v1, v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->r:LX/Hzt;

    iget-object v2, v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->x:LX/I05;

    invoke-virtual {v1, v2}, LX/Hzt;->a(LX/I05;)V

    .line 2525948
    :cond_1
    return-void

    .line 2525949
    :cond_2
    const/4 v2, 0x0

    iput-boolean v2, v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->y:Z

    goto :goto_0

    .line 2525950
    :cond_3
    iget-object v2, v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->r:LX/Hzt;

    .line 2525951
    iget-object v3, v2, LX/Hzt;->f:LX/I2i;

    .line 2525952
    if-eqz v1, :cond_4

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result p0

    if-eqz p0, :cond_9

    .line 2525953
    :cond_4
    const/4 p0, 0x0

    .line 2525954
    :goto_4
    move v3, p0

    .line 2525955
    if-eqz v3, :cond_5

    .line 2525956
    invoke-static {v2}, LX/Hzt;->k(LX/Hzt;)V

    .line 2525957
    :cond_5
    goto :goto_3

    :cond_6
    move-object v1, v2

    goto :goto_1

    .line 2525958
    :cond_7
    if-nez v1, :cond_8

    .line 2525959
    iget-object p0, v3, LX/I2i;->c:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->clear()V

    .line 2525960
    :goto_5
    invoke-static {v3}, LX/I2i;->a(LX/I2i;)V

    .line 2525961
    const/4 p0, 0x1

    goto :goto_2

    .line 2525962
    :cond_8
    new-instance p0, Ljava/util/ArrayList;

    invoke-direct {p0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object p0, v3, LX/I2i;->c:Ljava/util/List;

    goto :goto_5

    .line 2525963
    :cond_9
    iget-object p0, v3, LX/I2i;->c:Ljava/util/List;

    invoke-interface {p0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2525964
    invoke-static {v3}, LX/I2i;->a(LX/I2i;)V

    .line 2525965
    const/4 p0, 0x1

    goto :goto_4
.end method
