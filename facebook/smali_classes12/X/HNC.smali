.class public final LX/HNC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;

.field public final synthetic b:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public final synthetic c:LX/34b;

.field public final synthetic d:Landroid/content/Context;

.field public final synthetic e:LX/HNE;


# direct methods
.method public constructor <init>(LX/HNE;Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;Lcom/facebook/graphql/enums/GraphQLPageActionType;LX/34b;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2458221
    iput-object p1, p0, LX/HNC;->e:LX/HNE;

    iput-object p2, p0, LX/HNC;->a:Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;

    iput-object p3, p0, LX/HNC;->b:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    iput-object p4, p0, LX/HNC;->c:LX/34b;

    iput-object p5, p0, LX/HNC;->d:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, -0x79dfccde

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2458213
    iget-object v1, p0, LX/HNC;->e:LX/HNE;

    iget-object v1, v1, LX/HNE;->f:LX/HN9;

    iget-object v2, p0, LX/HNC;->a:Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;

    invoke-virtual {v2}, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    const-string v4, "pages_admin_bar_click_gear"

    iget-object v5, p0, LX/HNC;->b:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v2, v3, v4, v5}, LX/HN9;->a(JLjava/lang/String;Ljava/lang/String;)V

    .line 2458214
    iget-object v1, p0, LX/HNC;->c:LX/34b;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/HNC;->c:LX/34b;

    invoke-virtual {v1}, LX/1OM;->ij_()I

    move-result v1

    if-nez v1, :cond_1

    .line 2458215
    :cond_0
    const v1, -0x1b6d27f7

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2458216
    :goto_0
    return-void

    .line 2458217
    :cond_1
    new-instance v1, LX/3Af;

    iget-object v2, p0, LX/HNC;->d:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/3Af;-><init>(Landroid/content/Context;)V

    .line 2458218
    iget-object v2, p0, LX/HNC;->c:LX/34b;

    invoke-virtual {v1, v2}, LX/3Af;->a(LX/1OM;)V

    .line 2458219
    invoke-virtual {v1}, LX/3Af;->show()V

    .line 2458220
    const v1, -0x6e4b0722

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0
.end method
