.class public final LX/Ino;
.super LX/2s5;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;

.field private final b:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/support/v4/app/Fragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;LX/0gc;)V
    .locals 1

    .prologue
    .line 2610843
    iput-object p1, p0, LX/Ino;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;

    .line 2610844
    invoke-direct {p0, p2}, LX/2s5;-><init>(LX/0gc;)V

    .line 2610845
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/Ino;->b:Landroid/util/SparseArray;

    .line 2610846
    return-void
.end method


# virtual methods
.method public final G_(I)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 2610847
    iget-object v0, p0, LX/Ino;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;

    invoke-virtual {v0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {}, LX/Inp;->values()[LX/Inp;

    move-result-object v1

    aget-object v1, v1, p1

    iget v1, v1, LX/Inp;->titleResId:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 3

    .prologue
    .line 2610848
    sget-object v0, LX/Inn;->b:[I

    invoke-static {}, LX/Inp;->values()[LX/Inp;

    move-result-object v1

    aget-object v1, v1, p1

    invoke-virtual {v1}, LX/Inp;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2610849
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported messenger pay tab: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, LX/Inp;->values()[LX/Inp;

    move-result-object v2

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2610850
    :pswitch_0
    iget-object v0, p0, LX/Ino;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;

    iget-object v1, v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->J:LX/5g0;

    iget-object v0, p0, LX/Ino;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;

    invoke-virtual {v0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "orion_messenger_pay_params"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/payment/value/input/OrionMessengerPayParams;

    invoke-static {v1, v0}, LX/Io5;->a(LX/5g0;Lcom/facebook/messaging/payment/value/input/OrionMessengerPayParams;)Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    move-result-object v0

    .line 2610851
    :goto_0
    return-object v0

    :pswitch_1
    iget-object v0, p0, LX/Ino;->a:Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;

    invoke-virtual {v0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "orion_messenger_pay_params"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/payment/value/input/OrionMessengerPayParams;

    .line 2610852
    new-instance v1, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    invoke-direct {v1}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;-><init>()V

    .line 2610853
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2610854
    const-string p0, "payment_flow_type"

    sget-object p1, LX/5g0;->REQUEST:LX/5g0;

    invoke-virtual {v2, p0, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2610855
    const-string p0, "orion_messenger_pay_params"

    invoke-virtual {v2, p0, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2610856
    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2610857
    move-object v0, v1

    .line 2610858
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2610859
    invoke-super {p0, p1, p2}, LX/2s5;->a(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    .line 2610860
    iget-object v1, p0, LX/Ino;->b:Landroid/util/SparseArray;

    invoke-virtual {v1, p2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2610861
    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2610862
    invoke-static {}, LX/Inp;->values()[LX/Inp;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public final e(I)Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 2610863
    iget-object v0, p0, LX/Ino;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    return-object v0
.end method
