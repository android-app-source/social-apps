.class public LX/J0t;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6sf;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6sf",
        "<",
        "Lcom/facebook/payments/p2p/service/model/cards/ManualTransferMethod;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2638034
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2638035
    const-string v0, "transfer_option_id"

    iput-object v0, p0, LX/J0t;->a:Ljava/lang/String;

    .line 2638036
    const-string v0, "receipt_image_id"

    iput-object v0, p0, LX/J0t;->b:Ljava/lang/String;

    .line 2638037
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/paymentmethods/model/PaymentMethod;)LX/0Px;
    .locals 4

    .prologue
    .line 2638022
    check-cast p1, Lcom/facebook/payments/p2p/service/model/cards/ManualTransferMethod;

    .line 2638023
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    const-string v1, "type"

    invoke-virtual {p1}, Lcom/facebook/payments/p2p/service/model/cards/ManualTransferMethod;->b()LX/6zU;

    move-result-object v2

    invoke-virtual {v2}, LX/6zU;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v0

    .line 2638024
    new-instance v1, LX/0m9;

    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v2}, LX/0m9;-><init>(LX/0mC;)V

    const-string v2, "transfer_option_id"

    .line 2638025
    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/cards/ManualTransferMethod;->a:Ljava/lang/String;

    move-object v3, v3

    .line 2638026
    invoke-virtual {v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    move-result-object v1

    .line 2638027
    iget-object v2, p1, Lcom/facebook/payments/p2p/service/model/cards/ManualTransferMethod;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2638028
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2638029
    const-string v2, "receipt_image_id"

    .line 2638030
    iget-object v3, p1, Lcom/facebook/payments/p2p/service/model/cards/ManualTransferMethod;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2638031
    invoke-virtual {v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2638032
    :cond_0
    const-string v2, "data"

    invoke-virtual {v0, v2, v1}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 2638033
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v2, LX/6xk;->NMOR_PAYMENT_METHOD:LX/6xk;

    invoke-virtual {v2}, LX/6xk;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a()LX/6zU;
    .locals 1

    .prologue
    .line 2638021
    sget-object v0, LX/6zU;->MANUAL_TRANSFER:LX/6zU;

    return-object v0
.end method
