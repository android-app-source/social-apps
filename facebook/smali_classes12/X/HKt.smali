.class public LX/HKt;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kn;",
        ":",
        "LX/2kp;",
        ">",
        "LX/1S3;"
    }
.end annotation


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/pages/common/reaction/components/PagesJobCardComponentComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/HKt",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/pages/common/reaction/components/PagesJobCardComponentComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2455213
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2455214
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/HKt;->b:LX/0Zi;

    .line 2455215
    iput-object p1, p0, LX/HKt;->a:LX/0Ot;

    .line 2455216
    return-void
.end method

.method private b(LX/1X1;)V
    .locals 10

    .prologue
    .line 2455217
    check-cast p1, LX/HKs;

    .line 2455218
    iget-object v0, p0, LX/HKt;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PagesJobCardComponentComponentSpec;

    iget-object v1, p1, LX/HKs;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v2, p1, LX/HKs;->b:LX/2km;

    .line 2455219
    iget-object v3, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v3

    .line 2455220
    invoke-interface {v3}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v3

    if-nez v3, :cond_0

    .line 2455221
    :goto_0
    return-void

    .line 2455222
    :cond_0
    iget-object v3, v0, Lcom/facebook/pages/common/reaction/components/PagesJobCardComponentComponentSpec;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/E1f;

    .line 2455223
    iget-object v4, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v4, v4

    .line 2455224
    invoke-interface {v4}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v4

    move-object v5, v2

    check-cast v5, LX/1Pn;

    invoke-interface {v5}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v5

    const/4 v6, 0x0

    move-object v7, v2

    check-cast v7, LX/2kp;

    invoke-interface {v7}, LX/2kp;->t()LX/2jY;

    move-result-object v7

    .line 2455225
    iget-object v8, v7, LX/2jY;->a:Ljava/lang/String;

    move-object v7, v8

    .line 2455226
    move-object v8, v2

    check-cast v8, LX/2kp;

    invoke-interface {v8}, LX/2kp;->t()LX/2jY;

    move-result-object v8

    .line 2455227
    iget-object v9, v8, LX/2jY;->b:Ljava/lang/String;

    move-object v8, v9

    .line 2455228
    iget-object v9, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v9, v9

    .line 2455229
    invoke-virtual/range {v3 .. v9}, LX/E1f;->a(LX/9rk;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v3

    .line 2455230
    iget-object v4, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v4, v4

    .line 2455231
    iget-object v5, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v5, v5

    .line 2455232
    invoke-interface {v2, v4, v5, v3}, LX/2km;->a(Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V

    goto :goto_0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2455233
    const v0, -0x18e8b97f

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 10

    .prologue
    .line 2455234
    check-cast p2, LX/HKs;

    .line 2455235
    iget-object v0, p0, LX/HKt;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/reaction/components/PagesJobCardComponentComponentSpec;

    iget-object v1, p2, LX/HKs;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v2, p2, LX/HKs;->b:LX/2km;

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 2455236
    iget-object v3, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v3

    .line 2455237
    invoke-interface {v3}, LX/9uc;->o()LX/174;

    move-result-object v3

    invoke-interface {v3}, LX/174;->a()Ljava/lang/String;

    move-result-object v8

    .line 2455238
    iget-object v3, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v3

    .line 2455239
    invoke-interface {v3}, LX/9uc;->bo()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionJobComponentFragmentModel$JobPhotoModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionJobComponentFragmentModel$JobPhotoModel;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionJobComponentFragmentModel$JobPhotoModel$ImageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionJobComponentFragmentModel$JobPhotoModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v9

    .line 2455240
    iget-object v3, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v3

    .line 2455241
    invoke-interface {v3}, LX/9uc;->bp()LX/174;

    move-result-object v3

    invoke-interface {v3}, LX/174;->a()Ljava/lang/String;

    move-result-object v4

    .line 2455242
    iget-object v3, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v3

    .line 2455243
    invoke-interface {v3}, LX/9uc;->dq()LX/174;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 2455244
    iget-object v3, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v3

    .line 2455245
    invoke-interface {v3}, LX/9uc;->dq()LX/174;

    move-result-object v3

    invoke-interface {v3}, LX/174;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    move v5, v6

    .line 2455246
    :goto_0
    if-eqz v5, :cond_1

    .line 2455247
    iget-object v3, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v3

    .line 2455248
    invoke-interface {v3}, LX/9uc;->dq()LX/174;

    move-result-object v3

    invoke-interface {v3}, LX/174;->a()Ljava/lang/String;

    move-result-object v3

    .line 2455249
    :goto_1
    iget-object p0, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object p0, p0

    .line 2455250
    invoke-interface {p0}, LX/9uc;->dc()LX/174;

    move-result-object p0

    invoke-interface {p0}, LX/174;->a()Ljava/lang/String;

    move-result-object p0

    .line 2455251
    if-eqz v5, :cond_2

    .line 2455252
    check-cast v2, LX/1Pn;

    invoke-interface {v2}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v5

    const p2, 0x7f0836a2

    invoke-virtual {v5, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 2455253
    const/4 p2, 0x2

    new-array p2, p2, [Ljava/lang/Object;

    aput-object v4, p2, v7

    aput-object v3, p2, v6

    invoke-static {v5, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2455254
    :goto_2
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0a0097

    invoke-interface {v4, v5}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v4

    .line 2455255
    const v5, -0x18e8b97f

    const/4 p2, 0x0

    invoke-static {p1, v5, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v5, v5

    .line 2455256
    invoke-interface {v4, v5}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v7}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0b231b

    invoke-interface {v4, v5}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x3

    const p2, 0x7f0b0069

    invoke-interface {v4, v5, p2}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v4

    .line 2455257
    iget-object v5, v0, Lcom/facebook/pages/common/reaction/components/PagesJobCardComponentComponentSpec;->c:LX/1nu;

    invoke-virtual {v5, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v5

    sget-object p2, Lcom/facebook/pages/common/reaction/components/PagesJobCardComponentComponentSpec;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v5, p2}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v5

    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p2

    invoke-virtual {v5, p2}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const p2, 0x7f0b231a

    invoke-interface {v5, p2}, LX/1Di;->q(I)LX/1Di;

    move-result-object v5

    move-object v5, v5

    .line 2455258
    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0a00ca

    const v9, 0x7f0b0050

    invoke-static {p1, p0, v5, v6, v9}, Lcom/facebook/pages/common/reaction/components/PagesJobCardComponentComponentSpec;->a(LX/1De;Ljava/lang/String;III)LX/1Di;

    move-result-object v5

    const v9, 0x7f0b0069

    invoke-interface {v5, v6, v9}, LX/1Di;->g(II)LX/1Di;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0a00bd

    const v6, 0x7f0b004e

    invoke-static {p1, v8, v5, v7, v6}, Lcom/facebook/pages/common/reaction/components/PagesJobCardComponentComponentSpec;->a(LX/1De;Ljava/lang/String;III)LX/1Di;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0a00a3

    const v6, 0x7f0b004e

    invoke-static {p1, v3, v5, v7, v6}, Lcom/facebook/pages/common/reaction/components/PagesJobCardComponentComponentSpec;->a(LX/1De;Ljava/lang/String;III)LX/1Di;

    move-result-object v3

    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2455259
    return-object v0

    :cond_0
    move v5, v7

    .line 2455260
    goto/16 :goto_0

    .line 2455261
    :cond_1
    const/4 v3, 0x0

    goto/16 :goto_1

    :cond_2
    move-object v3, v4

    .line 2455262
    goto/16 :goto_2
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2455263
    invoke-static {}, LX/1dS;->b()V

    .line 2455264
    iget v0, p1, LX/1dQ;->b:I

    .line 2455265
    packed-switch v0, :pswitch_data_0

    .line 2455266
    :goto_0
    return-object v1

    .line 2455267
    :pswitch_0
    iget-object v0, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0}, LX/HKt;->b(LX/1X1;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x18e8b97f
        :pswitch_0
    .end packed-switch
.end method
