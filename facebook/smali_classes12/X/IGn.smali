.class public final LX/IGn;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyHighlightQueryModel;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 2556574
    const-class v1, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyHighlightQueryModel;

    const v0, -0x33e780af    # -3.997626E7f

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x2

    const-string v5, "FriendsNearbyHighlightQuery"

    const-string v6, "96770b64a56957861977e7c411999b62"

    const-string v7, "nodes"

    const-string v8, "10155155970786729"

    const/4 v9, 0x0

    .line 2556575
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 2556576
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2556577
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2556578
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2556579
    sparse-switch v0, :sswitch_data_0

    .line 2556580
    :goto_0
    return-object p1

    .line 2556581
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 2556582
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x2cadb26a -> :sswitch_1
        0x196b8 -> :sswitch_0
    .end sparse-switch
.end method

.method public final l()Lcom/facebook/graphql/query/VarArgsGraphQLJsonDeserializer;
    .locals 2

    .prologue
    .line 2556583
    new-instance v0, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQuery$FriendsNearbyHighlightQueryString$1;

    const-class v1, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQueryModels$FriendsNearbyHighlightQueryModel;

    invoke-direct {v0, p0, v1}, Lcom/facebook/friendsnearby/server/FriendsNearbyNewQuery$FriendsNearbyHighlightQueryString$1;-><init>(LX/IGn;Ljava/lang/Class;)V

    return-object v0
.end method
