.class public final LX/IGS;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;

.field public final synthetic b:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;)V
    .locals 0

    .prologue
    .line 2556020
    iput-object p1, p0, LX/IGS;->b:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    iput-object p2, p0, LX/IGS;->a:Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2556021
    iget-object v0, p0, LX/IGS;->b:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->x:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x30000a

    const-string v2, "FriendsNearbyPingWrite"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->f(ILjava/lang/String;)V

    .line 2556022
    sget-object v0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->m:Ljava/lang/Class;

    const-string v1, "Could not send location ping"

    invoke-static {v0, v1, p1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2556023
    iget-object v0, p0, LX/IGS;->b:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->w:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f080039

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2556024
    iget-object v0, p0, LX/IGS;->b:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    invoke-static {v0}, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->m$redex0(Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;)V

    .line 2556025
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2556026
    iget-object v0, p0, LX/IGS;->b:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->x:Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0x30000a

    const-string v2, "FriendsNearbyPingWrite"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 2556027
    iget-object v0, p0, LX/IGS;->b:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2556028
    iget-object v0, p0, LX/IGS;->b:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->E:LX/IHy;

    if-nez v0, :cond_0

    .line 2556029
    :goto_0
    return-void

    .line 2556030
    :cond_0
    iget-object v0, p0, LX/IGS;->b:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->B:Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;

    if-nez v0, :cond_1

    .line 2556031
    iget-object v0, p0, LX/IGS;->b:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->E:LX/IHy;

    iget-object v1, p0, LX/IGS;->a:Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;

    .line 2556032
    iget-object v2, v0, LX/IHy;->a:LX/IFX;

    iget-object p0, v0, LX/IHy;->b:Ljava/lang/String;

    invoke-virtual {v2, p0, v1}, LX/IFX;->a(Ljava/lang/String;Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;)V

    .line 2556033
    goto :goto_0

    .line 2556034
    :cond_1
    iget-object v0, p0, LX/IGS;->b:Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/pingdialog/ui/LocationPingDialogFragment;->E:LX/IHy;

    iget-object v1, p0, LX/IGS;->a:Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;

    .line 2556035
    iget-object v2, v0, LX/IHy;->a:LX/IFX;

    iget-object p0, v0, LX/IHy;->b:Ljava/lang/String;

    invoke-virtual {v2, p0, v1}, LX/IFX;->a(Ljava/lang/String;Lcom/facebook/friendsnearby/pingdialog/LocationPingParams;)V

    .line 2556036
    goto :goto_0
.end method
