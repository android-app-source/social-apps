.class public final LX/JCr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/JA0;

.field public final synthetic b:Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;LX/JA0;)V
    .locals 0

    .prologue
    .line 2662859
    iput-object p1, p0, LX/JCr;->b:Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;

    iput-object p2, p0, LX/JCr;->a:LX/JA0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x7c88aeb6

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2662860
    sget-object v1, LX/JCw;->a:[I

    iget-object v2, p0, LX/JCr;->a:LX/JA0;

    invoke-virtual {v2}, LX/JA0;->i()LX/J9z;

    move-result-object v2

    invoke-virtual {v2}, LX/J9z;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2662861
    iget-object v1, p0, LX/JCr;->b:Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;

    iget-object v1, v1, Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;->e:LX/03V;

    const-string v2, "ListCollectionItemView"

    const-string v3, "unknown group membership status"

    invoke-virtual {v1, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2662862
    :goto_0
    const v1, 0x53086f72

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2662863
    :pswitch_0
    iget-object v1, p0, LX/JCr;->b:Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;

    iget-object v2, p0, LX/JCr;->a:LX/JA0;

    .line 2662864
    invoke-static {v1, v2}, Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;->a$redex0(Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;LX/JA0;)V

    .line 2662865
    goto :goto_0

    .line 2662866
    :pswitch_1
    iget-object v1, p0, LX/JCr;->b:Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;

    iget-object v2, p0, LX/JCr;->a:LX/JA0;

    .line 2662867
    invoke-static {v1, v2}, Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;->b$redex0(Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;LX/JA0;)V

    .line 2662868
    goto :goto_0

    .line 2662869
    :pswitch_2
    iget-object v1, p0, LX/JCr;->b:Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;

    iget-object v2, p0, LX/JCr;->a:LX/JA0;

    .line 2662870
    invoke-static {v1, v2}, Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;->c$redex0(Lcom/facebook/timeline/aboutpage/views/CollectionGroupJoinButton;LX/JA0;)V

    .line 2662871
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
