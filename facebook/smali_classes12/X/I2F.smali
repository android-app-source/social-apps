.class public LX/I2F;
.super LX/A8X;
.source ""


# static fields
.field private static final a:Ljava/lang/Object;

.field private static final b:Ljava/lang/Object;


# instance fields
.field public final c:Landroid/content/Context;

.field public final d:Lcom/facebook/events/common/EventAnalyticsParams;

.field private final e:LX/6RZ;

.field private final f:LX/Blh;

.field public final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Lcom/facebook/content/SecureContextHelper;

.field public final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/model/Event;",
            ">;"
        }
    .end annotation
.end field

.field public final j:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public k:Z

.field public l:LX/I2H;

.field private m:LX/4yY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4yY",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private n:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2530105
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/I2F;->a:Ljava/lang/Object;

    .line 2530106
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/I2F;->b:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/events/common/EventAnalyticsParams;LX/I2H;Landroid/content/Context;LX/6RZ;LX/Blh;LX/0Or;Lcom/facebook/content/SecureContextHelper;)V
    .locals 1
    .param p1    # Lcom/facebook/events/common/EventAnalyticsParams;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/I2H;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/events/common/EventAnalyticsParams;",
            "LX/I2H;",
            "Landroid/content/Context;",
            "LX/6RZ;",
            "LX/Blh;",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;",
            "Lcom/facebook/content/SecureContextHelper;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2530093
    invoke-direct {p0}, LX/A8X;-><init>()V

    .line 2530094
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/I2F;->i:Ljava/util/List;

    .line 2530095
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/I2F;->j:Ljava/util/HashMap;

    .line 2530096
    iput-object p3, p0, LX/I2F;->c:Landroid/content/Context;

    .line 2530097
    iput-object p1, p0, LX/I2F;->d:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2530098
    iput-object p4, p0, LX/I2F;->e:LX/6RZ;

    .line 2530099
    iput-object p2, p0, LX/I2F;->l:LX/I2H;

    .line 2530100
    iput-object p5, p0, LX/I2F;->f:LX/Blh;

    .line 2530101
    iput-object p6, p0, LX/I2F;->g:LX/0Or;

    .line 2530102
    iput-object p7, p0, LX/I2F;->h:Lcom/facebook/content/SecureContextHelper;

    .line 2530103
    invoke-static {p0}, LX/I2F;->e(LX/I2F;)V

    .line 2530104
    return-void
.end method

.method public static a(Landroid/content/Context;LX/I2H;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2530087
    sget-object v0, LX/I2E;->a:[I

    invoke-virtual {p1}, LX/I2H;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2530088
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown Section Type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2530089
    :pswitch_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0821e6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2530090
    :goto_0
    return-object v0

    .line 2530091
    :pswitch_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0821e7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2530092
    :pswitch_2
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0821e8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static e(LX/I2F;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2530072
    iput v2, p0, LX/I2F;->n:I

    .line 2530073
    new-instance v0, LX/4yW;

    invoke-direct {v0}, LX/4yW;-><init>()V

    .line 2530074
    iget-object v1, p0, LX/I2F;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2530075
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, LX/50M;->b(Ljava/lang/Comparable;Ljava/lang/Comparable;)LX/50M;

    move-result-object v1

    const v2, 0x7f0d01aa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4yW;->a(LX/50M;Ljava/lang/Object;)LX/4yW;

    .line 2530076
    iget v1, p0, LX/I2F;->n:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/I2F;->n:I

    .line 2530077
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, LX/50M;->b(Ljava/lang/Comparable;Ljava/lang/Comparable;)LX/50M;

    move-result-object v1

    const v2, 0x7f0d01ab

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4yW;->a(LX/50M;Ljava/lang/Object;)LX/4yW;

    .line 2530078
    iget v1, p0, LX/I2F;->n:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/I2F;->n:I

    .line 2530079
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, LX/I2F;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, LX/50M;->b(Ljava/lang/Comparable;Ljava/lang/Comparable;)LX/50M;

    move-result-object v1

    const v2, 0x7f0d01ac

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4yW;->a(LX/50M;Ljava/lang/Object;)LX/4yW;

    .line 2530080
    iget v1, p0, LX/I2F;->n:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/I2F;->n:I

    .line 2530081
    iget-object v1, p0, LX/I2F;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    .line 2530082
    iget-boolean v2, p0, LX/I2F;->k:Z

    if-eqz v2, :cond_0

    .line 2530083
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v2, v1}, LX/50M;->b(Ljava/lang/Comparable;Ljava/lang/Comparable;)LX/50M;

    move-result-object v1

    const v2, 0x7f0d01ae

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4yW;->a(LX/50M;Ljava/lang/Object;)LX/4yW;

    .line 2530084
    iget v1, p0, LX/I2F;->n:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/I2F;->n:I

    .line 2530085
    :cond_0
    invoke-virtual {v0}, LX/4yW;->a()LX/4yZ;

    move-result-object v0

    iput-object v0, p0, LX/I2F;->m:LX/4yY;

    .line 2530086
    return-void
.end method

.method public static g(LX/I2F;)V
    .locals 4

    .prologue
    .line 2530065
    iget-object v0, p0, LX/I2F;->j:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 2530066
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/I2F;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2530067
    iget-object v2, p0, LX/I2F;->j:Ljava/util/HashMap;

    iget-object v0, p0, LX/I2F;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/model/Event;

    .line 2530068
    iget-object v3, v0, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v0, v3

    .line 2530069
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2530070
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2530071
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2530051
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    const v1, 0x7f0d01ab

    if-ne v0, v1, :cond_1

    .line 2530052
    check-cast p1, Lcom/facebook/fig/header/FigHeader;

    .line 2530053
    iget-object v0, p0, LX/I2F;->c:Landroid/content/Context;

    iget-object v1, p0, LX/I2F;->l:LX/I2H;

    invoke-static {v0, v1}, LX/I2F;->a(Landroid/content/Context;LX/I2H;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/fig/header/FigHeader;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2530054
    :cond_0
    :goto_0
    return-void

    .line 2530055
    :cond_1
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    const v1, 0x7f0d01ac

    if-ne v0, v1, :cond_2

    .line 2530056
    check-cast p1, LX/I29;

    .line 2530057
    invoke-virtual {p0, p2}, LX/I2F;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/model/Event;

    iget-object v1, p0, LX/I2F;->d:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-virtual {p1, v0, v1}, LX/I29;->a(Lcom/facebook/events/model/Event;Lcom/facebook/events/common/EventAnalyticsParams;)V

    goto :goto_0

    .line 2530058
    :cond_2
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    const v1, 0x7f0d01ae

    if-ne v0, v1, :cond_0

    .line 2530059
    check-cast p1, Lcom/facebook/fig/footer/FigFooter;

    .line 2530060
    iget-object v0, p0, LX/I2F;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0821df

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/fig/footer/FigFooter;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2530061
    invoke-virtual {p1, v2}, Lcom/facebook/fig/footer/FigFooter;->setTopDivider(Z)V

    .line 2530062
    invoke-virtual {p1, v2}, Lcom/facebook/fig/footer/FigFooter;->setFooterType(I)V

    .line 2530063
    new-instance v0, LX/I2D;

    invoke-direct {v0, p0}, LX/I2D;-><init>(LX/I2F;)V

    move-object v0, v0

    .line 2530064
    invoke-virtual {p1, v0}, Lcom/facebook/fig/footer/FigFooter;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/events/model/Event;)V
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 2530033
    if-nez p1, :cond_0

    .line 2530034
    :goto_0
    return-void

    .line 2530035
    :cond_0
    iget-object v0, p0, LX/I2F;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x6

    if-lt v0, v2, :cond_1

    invoke-virtual {p1}, Lcom/facebook/events/model/Event;->M()J

    move-result-wide v4

    iget-object v0, p0, LX/I2F;->i:Ljava/util/List;

    iget-object v2, p0, LX/I2F;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/model/Event;

    invoke-virtual {v0}, Lcom/facebook/events/model/Event;->M()J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-lez v0, :cond_1

    .line 2530036
    iput-boolean v3, p0, LX/I2F;->k:Z

    goto :goto_0

    .line 2530037
    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    iget-object v0, p0, LX/I2F;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    move v2, v1

    .line 2530038
    :goto_1
    iget-object v0, p0, LX/I2F;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 2530039
    iget-object v0, p0, LX/I2F;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/model/Event;

    .line 2530040
    if-nez v2, :cond_2

    invoke-virtual {v0}, Lcom/facebook/events/model/Event;->M()J

    move-result-wide v6

    invoke-virtual {p1}, Lcom/facebook/events/model/Event;->M()J

    move-result-wide v8

    cmp-long v5, v6, v8

    if-lez v5, :cond_2

    .line 2530041
    invoke-interface {v4, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v2, v3

    .line 2530042
    :cond_2
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2530043
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2530044
    :cond_3
    if-nez v2, :cond_4

    .line 2530045
    invoke-interface {v4, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2530046
    :cond_4
    iget-object v0, p0, LX/I2F;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2530047
    iget-object v0, p0, LX/I2F;->i:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2530048
    invoke-static {p0}, LX/I2F;->g(LX/I2F;)V

    .line 2530049
    invoke-static {p0}, LX/I2F;->e(LX/I2F;)V

    .line 2530050
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public final d(Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 5

    .prologue
    .line 2530021
    const v0, 0x7f0d01aa

    if-ne p2, v0, :cond_0

    .line 2530022
    new-instance v0, Landroid/view/View;

    iget-object v1, p0, LX/I2F;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2530023
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    iget-object v2, p0, LX/I2F;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0168

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2530024
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x2

    iget-object v3, p0, LX/I2F;->c:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b15dd

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2530025
    :goto_0
    return-object v0

    .line 2530026
    :cond_0
    const v0, 0x7f0d01ab

    if-ne p2, v0, :cond_1

    .line 2530027
    new-instance v0, Lcom/facebook/fig/header/FigHeader;

    iget-object v1, p0, LX/I2F;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/facebook/fig/header/FigHeader;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 2530028
    :cond_1
    const v0, 0x7f0d01ac

    if-ne p2, v0, :cond_2

    .line 2530029
    new-instance v0, LX/I29;

    iget-object v1, p0, LX/I2F;->c:Landroid/content/Context;

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/I29;-><init>(Landroid/content/Context;I)V

    goto :goto_0

    .line 2530030
    :cond_2
    const v0, 0x7f0d01ae

    if-ne p2, v0, :cond_3

    .line 2530031
    new-instance v0, Lcom/facebook/fig/footer/FigFooter;

    iget-object v1, p0, LX/I2F;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/facebook/fig/footer/FigFooter;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 2530032
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown View Type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getCount()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2530007
    iget-object v0, p0, LX/I2F;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    iget-object v0, p0, LX/I2F;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x2

    :goto_0
    add-int/2addr v0, v2

    iget-boolean v2, p0, LX/I2F;->k:Z

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2530010
    invoke-virtual {p0, p1}, LX/1OM;->getItemViewType(I)I

    move-result v0

    .line 2530011
    const v1, 0x7f0d01aa

    if-ne v0, v1, :cond_0

    .line 2530012
    sget-object v0, LX/I2F;->b:Ljava/lang/Object;

    .line 2530013
    :goto_0
    return-object v0

    .line 2530014
    :cond_0
    const v1, 0x7f0d01ab

    if-ne v0, v1, :cond_1

    .line 2530015
    iget-object v0, p0, LX/I2F;->l:LX/I2H;

    goto :goto_0

    .line 2530016
    :cond_1
    const v1, 0x7f0d01ac

    if-ne v0, v1, :cond_2

    .line 2530017
    iget-object v0, p0, LX/I2F;->i:Ljava/util/List;

    add-int/lit8 v1, p1, -0x2

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 2530018
    :cond_2
    const v1, 0x7f0d01ae

    if-ne v0, v1, :cond_3

    .line 2530019
    sget-object v0, LX/I2F;->a:Ljava/lang/Object;

    goto :goto_0

    .line 2530020
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown View Type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 2530009
    iget-object v0, p0, LX/I2F;->m:LX/4yY;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, LX/4yY;->a(Ljava/lang/Comparable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2530008
    iget v0, p0, LX/I2F;->n:I

    return v0
.end method
