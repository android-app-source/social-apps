.class public LX/J3m;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/J3k;

.field public static final b:LX/J3l;

.field public static final c:LX/J3j;

.field private static volatile f:LX/J3m;


# instance fields
.field public d:LX/11i;

.field public e:Lcom/facebook/quicklog/QuickPerformanceLogger;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2643269
    new-instance v0, LX/J3k;

    invoke-direct {v0}, LX/J3k;-><init>()V

    sput-object v0, LX/J3m;->a:LX/J3k;

    .line 2643270
    new-instance v0, LX/J3l;

    invoke-direct {v0}, LX/J3l;-><init>()V

    sput-object v0, LX/J3m;->b:LX/J3l;

    .line 2643271
    new-instance v0, LX/J3j;

    invoke-direct {v0}, LX/J3j;-><init>()V

    sput-object v0, LX/J3m;->c:LX/J3j;

    return-void
.end method

.method public constructor <init>(LX/11i;Lcom/facebook/quicklog/QuickPerformanceLogger;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2643272
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2643273
    iput-object p1, p0, LX/J3m;->d:LX/11i;

    .line 2643274
    iput-object p2, p0, LX/J3m;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 2643275
    return-void
.end method

.method public static a(LX/0QB;)LX/J3m;
    .locals 5

    .prologue
    .line 2643256
    sget-object v0, LX/J3m;->f:LX/J3m;

    if-nez v0, :cond_1

    .line 2643257
    const-class v1, LX/J3m;

    monitor-enter v1

    .line 2643258
    :try_start_0
    sget-object v0, LX/J3m;->f:LX/J3m;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2643259
    if-eqz v2, :cond_0

    .line 2643260
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2643261
    new-instance p0, LX/J3m;

    invoke-static {v0}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v3

    check-cast v3, LX/11i;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v4

    check-cast v4, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-direct {p0, v3, v4}, LX/J3m;-><init>(LX/11i;Lcom/facebook/quicklog/QuickPerformanceLogger;)V

    .line 2643262
    move-object v0, p0

    .line 2643263
    sput-object v0, LX/J3m;->f:LX/J3m;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2643264
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2643265
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2643266
    :cond_1
    sget-object v0, LX/J3m;->f:LX/J3m;

    return-object v0

    .line 2643267
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2643268
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
