.class public final LX/HRP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;)V
    .locals 0

    .prologue
    .line 2465150
    iput-object p1, p0, LX/HRP;->a:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v6, 0x2

    const/4 v4, 0x1

    const v0, 0x76773aed

    invoke-static {v6, v4, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2465151
    iget-object v0, p0, LX/HRP;->a:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9XE;

    iget-object v2, p0, LX/HRP;->a:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    iget-wide v2, v2, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->z:J

    .line 2465152
    iget-object v5, v0, LX/9XE;->a:LX/0Zb;

    sget-object v7, LX/9X6;->EVENT_ADMIN_CONTACT_INBOX_TAPPED_ENTRY_POINT:LX/9X6;

    invoke-static {v7, v2, v3}, LX/9XE;->c(LX/9X2;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p1, "from_activity_tab"

    invoke-virtual {v7, p1, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    invoke-interface {v5, v7}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2465153
    sget-object v0, LX/0ax;->aN:Ljava/lang/String;

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/HRP;->a:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    iget-wide v4, v4, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->z:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2465154
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2465155
    const-string v0, "com.facebook.katana.profile.id"

    iget-object v4, p0, LX/HRP;->a:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    iget-wide v4, v4, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->z:J

    invoke-virtual {v3, v0, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2465156
    iget-object v0, p0, LX/HRP;->a:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17W;

    iget-object v4, p0, LX/HRP;->a:Lcom/facebook/pages/fb4a/admin_activity/fragment/PageActivityFragment;

    invoke-virtual {v4}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v0, v4, v2, v3}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 2465157
    const v0, 0x2590408f

    invoke-static {v6, v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
