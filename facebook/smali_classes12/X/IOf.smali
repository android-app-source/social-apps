.class public final LX/IOf;
.super LX/63W;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;)V
    .locals 0

    .prologue
    .line 2572783
    iput-object p1, p0, LX/IOf;->a:Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;

    invoke-direct {p0}, LX/63W;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 10

    .prologue
    .line 2572784
    iget-object v0, p0, LX/IOf;->a:Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;

    iget-boolean v0, v0, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->p:Z

    if-eqz v0, :cond_1

    .line 2572785
    :cond_0
    :goto_0
    return-void

    .line 2572786
    :cond_1
    iget-object v0, p0, LX/IOf;->a:Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;

    iget-object v0, v0, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->b:LX/IOb;

    invoke-virtual {v0}, LX/IOb;->a()Ljava/util/Set;

    move-result-object v6

    .line 2572787
    invoke-interface {v6}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2572788
    iget-object v0, p0, LX/IOf;->a:Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;

    iget-object v0, v0, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->a:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f082823

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    goto :goto_0

    .line 2572789
    :cond_2
    iget-object v0, p0, LX/IOf;->a:Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;

    iget-object v0, v0, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->a:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f082822

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2572790
    new-instance v7, Ljava/util/Vector;

    invoke-direct {v7}, Ljava/util/Vector;-><init>()V

    .line 2572791
    new-instance v8, Ljava/util/Vector;

    invoke-direct {v8}, Ljava/util/Vector;-><init>()V

    .line 2572792
    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2572793
    invoke-virtual {v8, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 2572794
    iget-object v0, p0, LX/IOf;->a:Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;

    iget-object v0, v0, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->f:LX/DWD;

    iget-object v2, p0, LX/IOf;->a:Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;

    iget-object v2, v2, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->l:Ljava/lang/String;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 2572795
    sget-object v3, LX/0Q7;->a:LX/0Px;

    move-object v3, v3

    .line 2572796
    const-string v4, "add_from_profile"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, LX/DWD;->a(Ljava/lang/String;LX/0Px;LX/0Px;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2572797
    new-instance v1, LX/IOe;

    invoke-direct {v1, p0, v7, v8, v6}, LX/IOe;-><init>(LX/IOf;Ljava/util/Vector;Ljava/util/Vector;Ljava/util/Set;)V

    iget-object v2, p0, LX/IOf;->a:Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;

    iget-object v2, v2, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsFragment;->i:LX/0TD;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_1
.end method
