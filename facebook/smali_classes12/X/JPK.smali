.class public LX/JPK;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2689433
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/1De;LX/1vg;Ljava/lang/String;Z)LX/1Dh;
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 2689429
    if-eqz p3, :cond_0

    const v0, 0x7f02071f

    const v1, 0x7f0a00d4

    invoke-static {p0, p1, v0, v1}, LX/JPK;->a(LX/1De;LX/1vg;II)LX/1dc;

    move-result-object v0

    .line 2689430
    :goto_0
    invoke-static {p0}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, v2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, v2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v1

    invoke-static {p0}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const v2, 0x7f0b2598

    invoke-interface {v0, v2}, LX/1Di;->i(I)LX/1Di;

    move-result-object v0

    const v2, 0x7f0b2598

    invoke-interface {v0, v2}, LX/1Di;->q(I)LX/1Di;

    move-result-object v0

    invoke-interface {v1, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-static {p0}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    const v2, 0x7f0a010e

    invoke-virtual {v1, v2}, LX/1ne;->n(I)LX/1ne;

    move-result-object v1

    const v2, 0x7f0b2596

    invoke-virtual {v1, v2}, LX/1ne;->q(I)LX/1ne;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v1

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v0

    return-object v0

    .line 2689431
    :cond_0
    const v0, 0x7f02071f

    const v1, 0x7f0a00fa

    invoke-static {p0, p1, v0, v1}, LX/JPK;->a(LX/1De;LX/1vg;II)LX/1dc;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/1De;)LX/1Di;
    .locals 3

    .prologue
    .line 2689432
    invoke-static {p0}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v0

    const v1, 0x7f0a0442

    invoke-virtual {v0, v1}, LX/25Q;->i(I)LX/25Q;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const v1, 0x7f0b2583

    invoke-interface {v0, v1}, LX/1Di;->q(I)LX/1Di;

    move-result-object v0

    const/4 v1, 0x4

    invoke-interface {v0, v1}, LX/1Di;->b(I)LX/1Di;

    move-result-object v0

    const/4 v1, 0x6

    const v2, 0x7f0b2581

    invoke-interface {v0, v1, v2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/1De;LX/1nu;Lcom/facebook/common/callercontext/CallerContext;Landroid/net/Uri;)LX/1Di;
    .locals 2

    .prologue
    .line 2689428
    invoke-virtual {p1, p0}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v0

    const v1, 0x7f0213f3

    invoke-virtual {v0, v1}, LX/1nw;->h(I)LX/1nw;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const v1, 0x7f0b258d

    invoke-interface {v0, v1}, LX/1Di;->i(I)LX/1Di;

    move-result-object v0

    const v1, 0x7f0b258d

    invoke-interface {v0, v1}, LX/1Di;->q(I)LX/1Di;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/1De;LX/1vg;II)LX/1dc;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/1vg;",
            "II)",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2689427
    invoke-virtual {p1, p0}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/2xv;->h(I)LX/2xv;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/2xv;->j(I)LX/2xv;

    move-result-object v0

    invoke-virtual {v0}, LX/1n6;->b()LX/1dc;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/1De;)LX/1Di;
    .locals 2

    .prologue
    .line 2689426
    invoke-static {p0}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v0

    const v1, 0x7f0a0442

    invoke-virtual {v0, v1}, LX/25Q;->i(I)LX/25Q;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const v1, 0x7f0b2583

    invoke-interface {v0, v1}, LX/1Di;->q(I)LX/1Di;

    move-result-object v0

    const/4 v1, 0x4

    invoke-interface {v0, v1}, LX/1Di;->b(I)LX/1Di;

    move-result-object v0

    return-object v0
.end method
