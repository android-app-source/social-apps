.class public abstract LX/HmN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LX/HmU;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic b:LX/HmV;


# direct methods
.method public constructor <init>(LX/HmV;)V
    .locals 0

    .prologue
    .line 2500613
    iput-object p1, p0, LX/HmN;->b:LX/HmV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()LX/HmU;
.end method

.method public final call()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2500614
    :try_start_0
    invoke-virtual {p0}, LX/HmN;->a()LX/HmU;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 2500615
    :goto_0
    return-object v0

    .line 2500616
    :catch_0
    iget-object v0, p0, LX/HmN;->b:LX/HmV;

    invoke-static {v0}, LX/HmV;->i$redex0(LX/HmV;)V

    .line 2500617
    iget-object v0, p0, LX/HmN;->b:LX/HmV;

    iget-object v0, v0, LX/HmV;->k:LX/HmU;

    goto :goto_0

    .line 2500618
    :catch_1
    move-exception v0

    .line 2500619
    iget-object v1, p0, LX/HmN;->b:LX/HmV;

    iget-object v1, v1, LX/HmV;->h:LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "An unexpected error occurred in a BeamCallable"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2500620
    const-string v1, "BeamReceiver"

    const-string v2, "An unexpected error occurred"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2500621
    iget-object v1, p0, LX/HmN;->b:LX/HmV;

    iget-object v1, v1, LX/HmV;->f:LX/Hmc;

    .line 2500622
    sget-object v2, LX/Hmb;->FLOW_UNHANDLED_EXCEPTION:LX/Hmb;

    invoke-static {v1, v2}, LX/Hmc;->a(LX/Hmc;LX/Hmb;)V

    .line 2500623
    throw v0
.end method
