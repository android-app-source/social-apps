.class public LX/HhS;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/HhS;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2495548
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2495549
    const-string v0, "zero_interstitial"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/HhR;

    const-class v2, Lcom/facebook/zero/activity/ZeroOptinInterstitialActivity;

    invoke-direct {v1, v2}, LX/HhR;-><init>(Ljava/lang/Class;)V

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;LX/47M;)V

    .line 2495550
    const-string v0, "dialtone_optin_interstitial"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/HhR;

    const-class v2, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivity;

    invoke-direct {v1, v2}, LX/HhR;-><init>(Ljava/lang/Class;)V

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;LX/47M;)V

    .line 2495551
    const-string v0, "dialtone_optin_interstitial_new"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/HhR;

    const-class v2, Lcom/facebook/zero/activity/DialtoneOptinInterstitialActivityNew;

    invoke-direct {v1, v2}, LX/HhR;-><init>(Ljava/lang/Class;)V

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;LX/47M;)V

    .line 2495552
    const-string v0, "lightswitch_optin_interstitial"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/HhR;

    const-class v2, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivity;

    invoke-direct {v1, v2}, LX/HhR;-><init>(Ljava/lang/Class;)V

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;LX/47M;)V

    .line 2495553
    const-string v0, "lightswitch_optin_interstitial_new"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/HhR;

    const-class v2, Lcom/facebook/zero/activity/LightswitchOptinInterstitialActivityNew;

    invoke-direct {v1, v2}, LX/HhR;-><init>(Ljava/lang/Class;)V

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;LX/47M;)V

    .line 2495554
    const-string v0, "time_based_optin_interstitial"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/HhR;

    const-class v2, Lcom/facebook/zero/activity/TimeBasedOptinInterstitialActivity;

    invoke-direct {v1, v2}, LX/HhR;-><init>(Ljava/lang/Class;)V

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;LX/47M;)V

    .line 2495555
    const-string v0, "time_based_optin_interstitial_new"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/HhR;

    const-class v2, Lcom/facebook/zero/activity/TimeBasedOptinInterstitialActivityNew;

    invoke-direct {v1, v2}, LX/HhR;-><init>(Ljava/lang/Class;)V

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;LX/47M;)V

    .line 2495556
    return-void
.end method

.method public static a(LX/0QB;)LX/HhS;
    .locals 3

    .prologue
    .line 2495557
    sget-object v0, LX/HhS;->a:LX/HhS;

    if-nez v0, :cond_1

    .line 2495558
    const-class v1, LX/HhS;

    monitor-enter v1

    .line 2495559
    :try_start_0
    sget-object v0, LX/HhS;->a:LX/HhS;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2495560
    if-eqz v2, :cond_0

    .line 2495561
    :try_start_1
    new-instance v0, LX/HhS;

    invoke-direct {v0}, LX/HhS;-><init>()V

    .line 2495562
    move-object v0, v0

    .line 2495563
    sput-object v0, LX/HhS;->a:LX/HhS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2495564
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2495565
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2495566
    :cond_1
    sget-object v0, LX/HhS;->a:LX/HhS;

    return-object v0

    .line 2495567
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2495568
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
