.class public final enum LX/Ifh;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Ifh;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Ifh;

.field public static final enum INBOX_2_UNIT:LX/Ifh;

.field public static final enum INVITES_INTERSTITIAL:LX/Ifh;

.field public static final enum NUX_INVITE_PAGE:LX/Ifh;

.field public static final enum PEOPLE_TAB_INVITE_SMS_ROW:LX/Ifh;

.field public static final enum PEOPLE_TAB_INVITE_UPSELL:LX/Ifh;

.field public static final enum PEOPLE_TAB_PERMANENT_INVITE_ROW:LX/Ifh;

.field public static final enum THREAD_SETTINGS_INVITE_BUTTON:LX/Ifh;

.field public static final enum THREAD_VIEW_INVITE_BANNER:LX/Ifh;

.field public static final enum THREAD_VIEW_INVITE_BUTTON:LX/Ifh;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2600084
    new-instance v0, LX/Ifh;

    const-string v1, "THREAD_VIEW_INVITE_BUTTON"

    invoke-direct {v0, v1, v3}, LX/Ifh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ifh;->THREAD_VIEW_INVITE_BUTTON:LX/Ifh;

    .line 2600085
    new-instance v0, LX/Ifh;

    const-string v1, "THREAD_VIEW_INVITE_BANNER"

    invoke-direct {v0, v1, v4}, LX/Ifh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ifh;->THREAD_VIEW_INVITE_BANNER:LX/Ifh;

    .line 2600086
    new-instance v0, LX/Ifh;

    const-string v1, "PEOPLE_TAB_INVITE_UPSELL"

    invoke-direct {v0, v1, v5}, LX/Ifh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ifh;->PEOPLE_TAB_INVITE_UPSELL:LX/Ifh;

    .line 2600087
    new-instance v0, LX/Ifh;

    const-string v1, "PEOPLE_TAB_PERMANENT_INVITE_ROW"

    invoke-direct {v0, v1, v6}, LX/Ifh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ifh;->PEOPLE_TAB_PERMANENT_INVITE_ROW:LX/Ifh;

    .line 2600088
    new-instance v0, LX/Ifh;

    const-string v1, "THREAD_SETTINGS_INVITE_BUTTON"

    invoke-direct {v0, v1, v7}, LX/Ifh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ifh;->THREAD_SETTINGS_INVITE_BUTTON:LX/Ifh;

    .line 2600089
    new-instance v0, LX/Ifh;

    const-string v1, "PEOPLE_TAB_INVITE_SMS_ROW"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/Ifh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ifh;->PEOPLE_TAB_INVITE_SMS_ROW:LX/Ifh;

    .line 2600090
    new-instance v0, LX/Ifh;

    const-string v1, "NUX_INVITE_PAGE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/Ifh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ifh;->NUX_INVITE_PAGE:LX/Ifh;

    .line 2600091
    new-instance v0, LX/Ifh;

    const-string v1, "INBOX_2_UNIT"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/Ifh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ifh;->INBOX_2_UNIT:LX/Ifh;

    .line 2600092
    new-instance v0, LX/Ifh;

    const-string v1, "INVITES_INTERSTITIAL"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/Ifh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Ifh;->INVITES_INTERSTITIAL:LX/Ifh;

    .line 2600093
    const/16 v0, 0x9

    new-array v0, v0, [LX/Ifh;

    sget-object v1, LX/Ifh;->THREAD_VIEW_INVITE_BUTTON:LX/Ifh;

    aput-object v1, v0, v3

    sget-object v1, LX/Ifh;->THREAD_VIEW_INVITE_BANNER:LX/Ifh;

    aput-object v1, v0, v4

    sget-object v1, LX/Ifh;->PEOPLE_TAB_INVITE_UPSELL:LX/Ifh;

    aput-object v1, v0, v5

    sget-object v1, LX/Ifh;->PEOPLE_TAB_PERMANENT_INVITE_ROW:LX/Ifh;

    aput-object v1, v0, v6

    sget-object v1, LX/Ifh;->THREAD_SETTINGS_INVITE_BUTTON:LX/Ifh;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/Ifh;->PEOPLE_TAB_INVITE_SMS_ROW:LX/Ifh;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/Ifh;->NUX_INVITE_PAGE:LX/Ifh;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/Ifh;->INBOX_2_UNIT:LX/Ifh;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/Ifh;->INVITES_INTERSTITIAL:LX/Ifh;

    aput-object v2, v0, v1

    sput-object v0, LX/Ifh;->$VALUES:[LX/Ifh;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2600094
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Ifh;
    .locals 1

    .prologue
    .line 2600095
    const-class v0, LX/Ifh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Ifh;

    return-object v0
.end method

.method public static values()[LX/Ifh;
    .locals 1

    .prologue
    .line 2600096
    sget-object v0, LX/Ifh;->$VALUES:[LX/Ifh;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Ifh;

    return-object v0
.end method
