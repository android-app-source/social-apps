.class public final enum LX/If6;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/If6;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/If6;

.field public static final enum ADDING_CONTACT:LX/If6;

.field public static final enum NEW_CONTACT:LX/If6;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2599315
    new-instance v0, LX/If6;

    const-string v1, "NEW_CONTACT"

    invoke-direct {v0, v1, v2}, LX/If6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/If6;->NEW_CONTACT:LX/If6;

    .line 2599316
    new-instance v0, LX/If6;

    const-string v1, "ADDING_CONTACT"

    invoke-direct {v0, v1, v3}, LX/If6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/If6;->ADDING_CONTACT:LX/If6;

    .line 2599317
    const/4 v0, 0x2

    new-array v0, v0, [LX/If6;

    sget-object v1, LX/If6;->NEW_CONTACT:LX/If6;

    aput-object v1, v0, v2

    sget-object v1, LX/If6;->ADDING_CONTACT:LX/If6;

    aput-object v1, v0, v3

    sput-object v0, LX/If6;->$VALUES:[LX/If6;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2599314
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/If6;
    .locals 1

    .prologue
    .line 2599312
    const-class v0, LX/If6;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/If6;

    return-object v0
.end method

.method public static values()[LX/If6;
    .locals 1

    .prologue
    .line 2599313
    sget-object v0, LX/If6;->$VALUES:[LX/If6;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/If6;

    return-object v0
.end method
