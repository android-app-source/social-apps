.class public final LX/ItJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/send/client/SendMessageManager;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/send/client/SendMessageManager;)V
    .locals 0

    .prologue
    .line 2622264
    iput-object p1, p0, LX/ItJ;->a:Lcom/facebook/messaging/send/client/SendMessageManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x26

    const v1, -0x5facc25f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2622265
    const-string v0, "thread_key"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2622266
    const-string v2, "offline_threading_ids"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 2622267
    if-eqz v2, :cond_0

    .line 2622268
    iget-object v3, p0, LX/ItJ;->a:Lcom/facebook/messaging/send/client/SendMessageManager;

    invoke-static {v3, v0, v2}, Lcom/facebook/messaging/send/client/SendMessageManager;->b(Lcom/facebook/messaging/send/client/SendMessageManager;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/util/Collection;)V

    .line 2622269
    iget-object v3, p0, LX/ItJ;->a:Lcom/facebook/messaging/send/client/SendMessageManager;

    invoke-virtual {v3, v0, v2}, Lcom/facebook/messaging/send/client/SendMessageManager;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/util/Collection;)V

    .line 2622270
    :cond_0
    const/16 v0, 0x27

    const v2, 0x3cf188d0

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
