.class public LX/HkJ;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final g:LX/Hkw;

.field public static final h:Ljava/util/concurrent/ThreadPoolExecutor;


# instance fields
.field public a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/HkK;

.field public c:LX/HkB;

.field public d:LX/Hk0;

.field public e:LX/HkR;

.field public final f:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, LX/Hkw;

    invoke-direct {v0}, LX/Hkw;-><init>()V

    sput-object v0, LX/HkJ;->g:LX/Hkw;

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newCachedThreadPool(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ThreadPoolExecutor;

    sput-object v0, LX/HkJ;->h:Ljava/util/concurrent/ThreadPoolExecutor;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, LX/HkK;->a()LX/HkK;

    move-result-object v0

    iput-object v0, p0, LX/HkJ;->b:LX/HkK;

    sget-object v0, LX/Hj2;->e:Ljava/lang/String;

    move-object v0, v0

    goto :goto_1

    :goto_0
    return-void

    :goto_1
    invoke-static {v0}, LX/Hky;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "https://graph.facebook.com/network_ads_common/"

    :goto_2
    iput-object v0, p0, LX/HkJ;->f:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const-string v1, "https://graph.%s.facebook.com/network_ads_common/"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method public static a$redex0(LX/HkJ;LX/Hjr;)V
    .locals 1

    iget-object v0, p0, LX/HkJ;->c:LX/HkB;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HkJ;->c:LX/HkB;

    invoke-interface {v0, p1}, LX/HkB;->a(LX/Hjr;)V

    :cond_0
    invoke-static {p0}, LX/HkJ;->b(LX/HkJ;)V

    return-void
.end method

.method public static a$redex0(LX/HkJ;Ljava/lang/String;)V
    .locals 9

    :try_start_0
    iget-object v0, p0, LX/HkJ;->b:LX/HkK;

    invoke-virtual {v0, p1}, LX/HkK;->a(Ljava/lang/String;)LX/HkM;

    move-result-object v0

    iget-object v1, v0, LX/HkM;->a:LX/Hjx;

    move-object v1, v1

    if-eqz v1, :cond_0

    iget-object v2, v1, LX/Hjx;->c:LX/Hjy;

    move-object v2, v2

    iget v5, v2, LX/Hjy;->e:I

    mul-int/lit16 v5, v5, 0x3e8

    int-to-long v5, v5

    move-wide v2, v5

    iget-object v4, p0, LX/HkJ;->d:LX/Hk0;

    sget-object v5, LX/Hkj;->a:Ljava/util/Map;

    invoke-static {v4}, LX/Hkj;->d(LX/Hk0;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v5, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    sget-object v2, LX/HkI;->a:[I

    iget-object v3, v0, LX/HkM;->b:LX/HkL;

    move-object v3, v3

    invoke-virtual {v3}, LX/HkL;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    sget-object v0, LX/HjN;->UNKNOWN_RESPONSE:LX/HjN;

    invoke-virtual {v0, p1}, LX/HjN;->getAdErrorWrapper(Ljava/lang/String;)LX/Hjr;

    move-result-object v0

    invoke-static {p0, v0}, LX/HkJ;->a$redex0(LX/HkJ;LX/Hjr;)V

    :goto_0
    return-void

    :pswitch_0
    check-cast v0, LX/HkN;

    if-eqz v1, :cond_1

    iget-object v2, v1, LX/Hjx;->c:LX/Hjy;

    move-object v1, v2

    iget-boolean v2, v1, LX/Hjy;->h:Z

    move v1, v2

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/HkJ;->d:LX/Hk0;

    sget-object v2, LX/Hkj;->c:Ljava/util/Map;

    invoke-static {v1}, LX/Hkj;->d(LX/Hk0;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    iget-object v1, p0, LX/HkJ;->c:LX/HkB;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/HkJ;->c:LX/HkB;

    invoke-interface {v1, v0}, LX/HkB;->a(LX/HkN;)V

    :cond_2
    invoke-static {p0}, LX/HkJ;->b(LX/HkJ;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v1, LX/HjN;->PARSER_FAILURE:LX/HjN;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/HjN;->getAdErrorWrapper(Ljava/lang/String;)LX/Hjr;

    move-result-object v0

    invoke-static {p0, v0}, LX/HkJ;->a$redex0(LX/HkJ;LX/Hjr;)V

    goto :goto_0

    :pswitch_1
    :try_start_1
    check-cast v0, LX/HkO;

    iget-object v1, v0, LX/HkO;->a:Ljava/lang/String;

    move-object v1, v1

    iget v2, v0, LX/HkO;->b:I

    move v0, v2

    sget-object v2, LX/HjN;->ERROR_MESSAGE:LX/HjN;

    invoke-static {v0, v2}, LX/HjN;->adErrorTypeFromCode(ILX/HjN;)LX/HjN;

    move-result-object v0

    if-eqz v1, :cond_3

    move-object p1, v1

    :cond_3
    invoke-virtual {v0, p1}, LX/HjN;->getAdErrorWrapper(Ljava/lang/String;)LX/Hjr;

    move-result-object v0

    invoke-static {p0, v0}, LX/HkJ;->a$redex0(LX/HkJ;LX/Hjr;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static b(LX/HkJ;)V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, LX/HkJ;->e:LX/HkR;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HkJ;->e:LX/HkR;

    iput v1, v0, LX/HkR;->e:I

    iget-object v0, p0, LX/HkJ;->e:LX/HkR;

    invoke-virtual {v0, v1}, LX/HkR;->a(I)V

    const/4 v0, 0x0

    iput-object v0, p0, LX/HkJ;->e:LX/HkR;

    :cond_0
    return-void
.end method
