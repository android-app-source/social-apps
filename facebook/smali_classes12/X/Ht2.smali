.class public LX/Ht2;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Ht0;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ht3;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2515357
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Ht2;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Ht3;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2515354
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2515355
    iput-object p1, p0, LX/Ht2;->b:LX/0Ot;

    .line 2515356
    return-void
.end method

.method public static a(LX/0QB;)LX/Ht2;
    .locals 4

    .prologue
    .line 2515343
    const-class v1, LX/Ht2;

    monitor-enter v1

    .line 2515344
    :try_start_0
    sget-object v0, LX/Ht2;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2515345
    sput-object v2, LX/Ht2;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2515346
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2515347
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2515348
    new-instance v3, LX/Ht2;

    const/16 p0, 0x1982

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Ht2;-><init>(LX/0Ot;)V

    .line 2515349
    move-object v0, v3

    .line 2515350
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2515351
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Ht2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2515352
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2515353
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2515341
    invoke-static {}, LX/1dS;->b()V

    .line 2515342
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;LX/1Dg;IILX/1no;LX/1X1;)V
    .locals 9

    .prologue
    const/16 v0, 0x8

    const/16 v1, 0x1e

    const v2, 0x1de09b65

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v8

    .line 2515333
    check-cast p6, LX/Ht1;

    .line 2515334
    iget-object v0, p0, LX/Ht2;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ht3;

    iget-object v4, p6, LX/Ht1;->a:LX/ADz;

    iget-object v5, p6, LX/Ht1;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iget-object v6, p6, LX/Ht1;->c:Lcom/facebook/ipc/composer/model/ComposerReshareContext;

    iget-object v7, p6, LX/Ht1;->d:LX/HtA;

    move v1, p3

    move v2, p4

    move-object v3, p5

    .line 2515335
    iget-object p0, v0, LX/Ht3;->a:Lcom/facebook/composer/feedattachment/ComposerFeedAttachmentViewBinder;

    invoke-virtual {p0, v5, v4}, Lcom/facebook/composer/feedattachment/ComposerFeedAttachmentViewBinder;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/attachments/angora/AngoraAttachmentView;)V

    .line 2515336
    invoke-static {v4, v6, v7}, Lcom/facebook/composer/feedattachment/ComposerFeedAttachmentViewBinder;->a(LX/ADz;Lcom/facebook/ipc/composer/model/ComposerReshareContext;LX/HtA;)V

    .line 2515337
    invoke-virtual {v4, v1, v2}, LX/ADz;->measure(II)V

    .line 2515338
    invoke-virtual {v4}, LX/ADz;->getMeasuredWidth()I

    move-result p0

    iput p0, v3, LX/1no;->a:I

    .line 2515339
    invoke-virtual {v4}, LX/ADz;->getMeasuredHeight()I

    move-result p0

    iput p0, v3, LX/1no;->b:I

    .line 2515340
    const/16 v0, 0x8

    const/16 v1, 0x1f

    const v2, -0x164fedf1

    invoke-static {v0, v1, v2, v8}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(LX/1X1;LX/1X1;)V
    .locals 1

    .prologue
    .line 2515308
    check-cast p1, LX/Ht1;

    .line 2515309
    check-cast p2, LX/Ht1;

    .line 2515310
    iget-object v0, p1, LX/Ht1;->a:LX/ADz;

    iput-object v0, p2, LX/Ht1;->a:LX/ADz;

    .line 2515311
    return-void
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2515330
    iget-object v0, p0, LX/Ht2;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 2515331
    new-instance v0, LX/ADz;

    invoke-direct {v0, p1}, LX/ADz;-><init>(Landroid/content/Context;)V

    move-object v0, v0

    .line 2515332
    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2515329
    const/4 v0, 0x1

    return v0
.end method

.method public final d(LX/1De;LX/1X1;)V
    .locals 2

    .prologue
    .line 2515320
    check-cast p2, LX/Ht1;

    .line 2515321
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v1

    .line 2515322
    iget-object v0, p0, LX/Ht2;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 2515323
    new-instance v0, LX/ADz;

    invoke-direct {v0, p1}, LX/ADz;-><init>(Landroid/content/Context;)V

    .line 2515324
    iput-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    .line 2515325
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2515326
    check-cast v0, LX/ADz;

    iput-object v0, p2, LX/Ht1;->a:LX/ADz;

    .line 2515327
    invoke-static {v1}, LX/1cy;->a(LX/1np;)V

    .line 2515328
    return-void
.end method

.method public final e(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 4

    .prologue
    .line 2515315
    check-cast p3, LX/Ht1;

    .line 2515316
    iget-object v0, p0, LX/Ht2;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ht3;

    check-cast p2, LX/ADz;

    iget-object v1, p3, LX/Ht1;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iget-object v2, p3, LX/Ht1;->c:Lcom/facebook/ipc/composer/model/ComposerReshareContext;

    iget-object v3, p3, LX/Ht1;->d:LX/HtA;

    .line 2515317
    iget-object p0, v0, LX/Ht3;->a:Lcom/facebook/composer/feedattachment/ComposerFeedAttachmentViewBinder;

    invoke-virtual {p0, v1, p2}, Lcom/facebook/composer/feedattachment/ComposerFeedAttachmentViewBinder;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/attachments/angora/AngoraAttachmentView;)V

    .line 2515318
    invoke-static {p2, v2, v3}, Lcom/facebook/composer/feedattachment/ComposerFeedAttachmentViewBinder;->a(LX/ADz;Lcom/facebook/ipc/composer/model/ComposerReshareContext;LX/HtA;)V

    .line 2515319
    return-void
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 2515314
    sget-object v0, LX/1mv;->VIEW:LX/1mv;

    return-object v0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 2515313
    const/16 v0, 0xf

    return v0
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 2515312
    const/4 v0, 0x1

    return v0
.end method
