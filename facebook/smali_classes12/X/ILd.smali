.class public final LX/ILd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/ILc;


# instance fields
.field public final synthetic a:LX/4At;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/ILe;


# direct methods
.method public constructor <init>(LX/ILe;LX/4At;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2568201
    iput-object p1, p0, LX/ILd;->c:LX/ILe;

    iput-object p2, p0, LX/ILd;->a:LX/4At;

    iput-object p3, p0, LX/ILd;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 2568202
    iget-object v0, p0, LX/ILd;->a:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 2568203
    iget-object v0, p0, LX/ILd;->c:LX/ILe;

    iget-object v0, v0, LX/ILe;->a:Lcom/facebook/groups/community/fragments/SchoolCodeConfirmationFragment;

    const-string v1, "community_code_confirmation_success"

    .line 2568204
    iget-object v3, v0, Lcom/facebook/groups/community/fragments/SchoolCodeConfirmationFragment;->b:LX/0Zb;

    const/4 v4, 0x0

    invoke-interface {v3, v1, v4}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v3

    .line 2568205
    invoke-virtual {v3}, LX/0oG;->a()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2568206
    const-string v4, "group_email_code_confirmation"

    invoke-virtual {v3, v4}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v3

    const-string v4, "group_id"

    iget-object v2, v0, Lcom/facebook/groups/community/fragments/SchoolCodeConfirmationFragment;->g:Ljava/lang/String;

    invoke-virtual {v3, v4, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v3

    invoke-virtual {v3}, LX/0oG;->d()V

    .line 2568207
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2568208
    iget-object v1, p0, LX/ILd;->c:LX/ILe;

    iget-object v1, v1, LX/ILe;->a:Lcom/facebook/groups/community/fragments/SchoolCodeConfirmationFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 2568209
    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2568210
    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    .line 2568211
    return-void
.end method

.method public final a(LX/IMp;)V
    .locals 5

    .prologue
    .line 2568194
    iget-object v0, p0, LX/ILd;->a:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 2568195
    iget-object v0, p0, LX/ILd;->c:LX/ILe;

    iget-object v0, v0, LX/ILe;->a:Lcom/facebook/groups/community/fragments/SchoolCodeConfirmationFragment;

    const-string v1, "community_code_confirmation_wrong_code"

    .line 2568196
    iget-object v3, v0, Lcom/facebook/groups/community/fragments/SchoolCodeConfirmationFragment;->b:LX/0Zb;

    const/4 v4, 0x0

    invoke-interface {v3, v1, v4}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v3

    .line 2568197
    invoke-virtual {v3}, LX/0oG;->a()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2568198
    const-string v4, "group_email_code_confirmation"

    invoke-virtual {v3, v4}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v3

    const-string v4, "group_id"

    iget-object v2, v0, Lcom/facebook/groups/community/fragments/SchoolCodeConfirmationFragment;->g:Ljava/lang/String;

    invoke-virtual {v3, v4, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v3

    invoke-virtual {v3}, LX/0oG;->d()V

    .line 2568199
    :cond_0
    new-instance v0, LX/0ju;

    iget-object v1, p0, LX/ILd;->c:LX/ILe;

    iget-object v1, v1, LX/ILe;->a:Lcom/facebook/groups/community/fragments/SchoolCodeConfirmationFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0ju;-><init>(Landroid/content/Context;)V

    iget-object v1, p1, LX/IMp;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    iget-object v1, p1, LX/IMp;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0ju;->c(Z)LX/0ju;

    move-result-object v0

    const v1, 0x7f080016

    new-instance v2, LX/ILb;

    invoke-direct {v2, p0}, LX/ILb;-><init>(LX/ILd;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 2568200
    return-void
.end method
