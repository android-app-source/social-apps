.class public final LX/Iqb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5Ne;


# instance fields
.field public final synthetic a:LX/Iqd;


# direct methods
.method public constructor <init>(LX/Iqd;)V
    .locals 0

    .prologue
    .line 2617048
    iput-object p1, p0, LX/Iqb;->a:LX/Iqd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2617049
    iget-object v0, p0, LX/Iqb;->a:LX/Iqd;

    iget-object v0, v0, LX/Iqd;->c:LX/IrN;

    if-eqz v0, :cond_1

    .line 2617050
    iget-object v0, p0, LX/Iqb;->a:LX/Iqd;

    iget-object v0, v0, LX/Iqd;->b:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v0}, Lcom/facebook/drawingview/DrawingView;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2617051
    iget-object v0, p0, LX/Iqb;->a:LX/Iqd;

    iget-object v0, v0, LX/Iqd;->c:LX/IrN;

    .line 2617052
    iget-object v1, v0, LX/IrN;->a:LX/IrP;

    sget-object v2, LX/Iqe;->DOODLING:LX/Iqe;

    invoke-static {v1, v2}, LX/IrP;->b(LX/IrP;LX/Iqe;)V

    .line 2617053
    :cond_0
    iget-object v0, p0, LX/Iqb;->a:LX/Iqd;

    iget-object v0, v0, LX/Iqd;->c:LX/IrN;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/IrN;->a(Z)V

    .line 2617054
    :cond_1
    iget-object v0, p0, LX/Iqb;->a:LX/Iqd;

    iget-object v0, v0, LX/Iqd;->a:LX/IqZ;

    if-eqz v0, :cond_2

    .line 2617055
    iget-object v0, p0, LX/Iqb;->a:LX/Iqd;

    iget-object v0, v0, LX/Iqd;->a:LX/IqZ;

    invoke-virtual {v0}, LX/IqZ;->b()V

    .line 2617056
    :cond_2
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 2617057
    iget-object v0, p0, LX/Iqb;->a:LX/Iqd;

    iget-object v0, v0, LX/Iqd;->c:LX/IrN;

    if-eqz v0, :cond_1

    .line 2617058
    iget-object v0, p0, LX/Iqb;->a:LX/Iqd;

    iget-object v0, v0, LX/Iqd;->b:Lcom/facebook/drawingview/DrawingView;

    invoke-virtual {v0}, Lcom/facebook/drawingview/DrawingView;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2617059
    iget-object v0, p0, LX/Iqb;->a:LX/Iqd;

    iget-object v0, v0, LX/Iqd;->c:LX/IrN;

    .line 2617060
    iget-object v1, v0, LX/IrN;->a:LX/IrP;

    sget-object v2, LX/Iqe;->DOODLE:LX/Iqe;

    invoke-static {v1, v2}, LX/IrP;->b(LX/IrP;LX/Iqe;)V

    .line 2617061
    :cond_0
    iget-object v0, p0, LX/Iqb;->a:LX/Iqd;

    iget-object v0, v0, LX/Iqd;->c:LX/IrN;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/IrN;->a(Z)V

    .line 2617062
    :cond_1
    iget-object v0, p0, LX/Iqb;->a:LX/Iqd;

    iget-object v0, v0, LX/Iqd;->a:LX/IqZ;

    if-eqz v0, :cond_2

    .line 2617063
    iget-object v0, p0, LX/Iqb;->a:LX/Iqd;

    iget-object v0, v0, LX/Iqd;->a:LX/IqZ;

    invoke-virtual {v0}, LX/IqZ;->a()V

    .line 2617064
    :cond_2
    return-void
.end method
