.class public final LX/Hut;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Huw;


# direct methods
.method public constructor <init>(LX/Huw;)V
    .locals 0

    .prologue
    .line 2518044
    iput-object p1, p0, LX/Hut;->a:LX/Huw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x78f60ed6

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2518045
    iget-object v1, p0, LX/Hut;->a:LX/Huw;

    iget-object v1, v1, LX/Huw;->h:LX/3kp;

    invoke-virtual {v1}, LX/3kp;->b()V

    .line 2518046
    iget-object v1, p0, LX/Hut;->a:LX/Huw;

    .line 2518047
    iget-object v3, v1, LX/Huw;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0il;

    .line 2518048
    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0io;

    check-cast v4, LX/0iq;

    invoke-interface {v4}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v4

    iget-object v4, v4, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 2518049
    iget-object p0, v4, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v4, p0

    .line 2518050
    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2518051
    invoke-static {v4}, LX/2cA;->h(LX/1oS;)Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    move-result-object p0

    invoke-static {p0}, LX/4YK;->a(Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;)LX/4YK;

    move-result-object p0

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;->TAGGEES:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    .line 2518052
    iput-object p1, p0, LX/4YK;->e:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    .line 2518053
    move-object p0, p0

    .line 2518054
    invoke-virtual {p0}, LX/4YK;->a()Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    move-result-object p0

    .line 2518055
    invoke-static {v4}, LX/8QP;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/8QP;

    move-result-object v4

    invoke-virtual {v4, p0}, LX/8QP;->a(Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;)LX/8QP;

    move-result-object v4

    invoke-virtual {v4}, LX/8QP;->b()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object p0

    .line 2518056
    new-instance p1, LX/8QV;

    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0io;

    check-cast v4, LX/0iq;

    invoke-interface {v4}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v4

    iget-object v4, v4, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-direct {p1, v4}, LX/8QV;-><init>(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    const/4 v4, 0x1

    .line 2518057
    iput-boolean v4, p1, LX/8QV;->c:Z

    .line 2518058
    move-object v4, p1

    .line 2518059
    invoke-virtual {v4, p0}, LX/8QV;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/8QV;

    move-result-object v4

    invoke-virtual {v4}, LX/8QV;->b()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v4

    .line 2518060
    iget-object p0, v1, LX/Huw;->f:LX/HqJ;

    .line 2518061
    iget-object p1, p0, LX/HqJ;->a:Lcom/facebook/composer/activity/ComposerFragment;

    invoke-static {p1, v4}, Lcom/facebook/composer/activity/ComposerFragment;->a$redex0(Lcom/facebook/composer/activity/ComposerFragment;Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    .line 2518062
    iget-object v4, v1, LX/Huw;->g:LX/0gd;

    sget-object p0, LX/0ge;->COMPOSER_TAG_EXPANSION_PILL_CLICKED:LX/0ge;

    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0io;

    check-cast v3, LX/0j0;

    invoke-interface {v3}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, p0, v3}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 2518063
    const v1, -0x5f1a905b

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
