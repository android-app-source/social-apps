.class public abstract LX/IJZ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2563935
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/facebook/location/ImmutableLocation;Ljava/lang/String;)LX/0am;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/location/ImmutableLocation;",
            "Ljava/lang/String;",
            ")",
            "LX/0am",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2563928
    invoke-virtual {p0, p2, p3}, LX/IJZ;->a(Lcom/facebook/location/ImmutableLocation;Ljava/lang/String;)LX/0am;

    move-result-object v1

    .line 2563929
    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 2563930
    invoke-static {p1, v0}, LX/2A3;->b(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 2563931
    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object p0

    .line 2563932
    :goto_0
    move-object v0, p0

    .line 2563933
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 2563934
    :goto_1
    return-object v0

    :cond_0
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    goto :goto_1

    :cond_1
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object p0

    goto :goto_0
.end method

.method public abstract a(Lcom/facebook/location/ImmutableLocation;Ljava/lang/String;)LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/location/ImmutableLocation;",
            "Ljava/lang/String;",
            ")",
            "LX/0am",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a()Ljava/lang/String;
.end method

.method public abstract b()Ljava/lang/String;
.end method
