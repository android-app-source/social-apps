.class public final LX/IRD;
.super LX/DMC;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/DMC",
        "<",
        "Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsHeaderBar;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/IRE;


# direct methods
.method public constructor <init>(LX/IRE;LX/DML;)V
    .locals 0

    .prologue
    .line 2576276
    iput-object p1, p0, LX/IRD;->a:LX/IRE;

    invoke-direct {p0, p2}, LX/DMC;-><init>(LX/DML;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 9

    .prologue
    .line 2576277
    check-cast p1, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsHeaderBar;

    .line 2576278
    iget-object v0, p0, LX/IRD;->a:LX/IRE;

    iget-object v0, v0, LX/IRE;->a:Ljava/lang/String;

    iget-object v1, p0, LX/IRD;->a:LX/IRE;

    iget-object v1, v1, LX/IRE;->b:Ljava/lang/String;

    iget-object v2, p0, LX/IRD;->a:LX/IRE;

    iget-object v2, v2, LX/IRE;->c:Ljava/lang/String;

    const/4 v7, 0x1

    const/4 p0, 0x0

    .line 2576279
    iget-object v3, p1, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsHeaderBar;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2576280
    iget-object v3, p1, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsHeaderBar;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p1}, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsHeaderBar;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f081bb3    # 1.8091883E38f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v0, v5, p0

    invoke-static {v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2576281
    if-eqz v1, :cond_0

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 2576282
    iget-object v3, p1, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsHeaderBar;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    sget-object v5, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsHeaderBar;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v4, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2576283
    :goto_0
    iget-object v3, p1, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsHeaderBar;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p1}, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsHeaderBar;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f00cd

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, p0

    invoke-virtual {v4, v5, v6, v7}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2576284
    return-void

    .line 2576285
    :cond_0
    iget-object v3, p1, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsHeaderBar;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v4, 0x0

    sget-object v5, Lcom/facebook/groups/feed/ui/GroupsDiscussionTopicsHeaderBar;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v4, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_0
.end method
