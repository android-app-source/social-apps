.class public final LX/HzW;
.super LX/BlS;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;)V
    .locals 0

    .prologue
    .line 2525543
    iput-object p1, p0, LX/HzW;->a:Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;

    invoke-direct {p0}, LX/BlS;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 3

    .prologue
    .line 2525544
    check-cast p1, LX/BlR;

    .line 2525545
    iget-object v0, p1, LX/BlR;->b:Lcom/facebook/events/model/Event;

    .line 2525546
    iget-object v1, v0, Lcom/facebook/events/model/Event;->g:Lcom/facebook/events/model/PrivacyKind;

    move-object v0, v1

    .line 2525547
    sget-object v1, Lcom/facebook/events/model/PrivacyKind;->PRIVATE:Lcom/facebook/events/model/PrivacyKind;

    if-eq v0, v1, :cond_1

    .line 2525548
    :cond_0
    :goto_0
    return-void

    .line 2525549
    :cond_1
    iget-object v0, p0, LX/HzW;->a:Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->g:Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;

    .line 2525550
    iget v1, v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;->b:I

    move v0, v1

    .line 2525551
    iget-object v1, p1, LX/BlR;->a:LX/BlI;

    sget-object v2, LX/BlI;->SENDING:LX/BlI;

    if-ne v1, v2, :cond_2

    .line 2525552
    iget-object v1, p0, LX/HzW;->a:Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;

    iget-object v1, v1, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->g:Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;->setCalendarBadgeCount(I)V

    goto :goto_0

    .line 2525553
    :cond_2
    iget-object v1, p1, LX/BlR;->a:LX/BlI;

    sget-object v2, LX/BlI;->FAILURE:LX/BlI;

    if-ne v1, v2, :cond_0

    .line 2525554
    iget-object v1, p0, LX/HzW;->a:Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;

    iget-object v1, v1, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->g:Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v0}, Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;->setCalendarBadgeCount(I)V

    goto :goto_0
.end method
