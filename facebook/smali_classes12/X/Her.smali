.class public final enum LX/Her;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Her;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Her;

.field public static final enum BOTTOM_LEFT:LX/Her;

.field public static final enum BOTTOM_RIGHT:LX/Her;

.field public static final enum TOP_LEFT:LX/Her;

.field public static final enum TOP_RIGHT:LX/Her;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2490853
    new-instance v0, LX/Her;

    const-string v1, "TOP_LEFT"

    invoke-direct {v0, v1, v2}, LX/Her;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Her;->TOP_LEFT:LX/Her;

    .line 2490854
    new-instance v0, LX/Her;

    const-string v1, "TOP_RIGHT"

    invoke-direct {v0, v1, v3}, LX/Her;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Her;->TOP_RIGHT:LX/Her;

    .line 2490855
    new-instance v0, LX/Her;

    const-string v1, "BOTTOM_LEFT"

    invoke-direct {v0, v1, v4}, LX/Her;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Her;->BOTTOM_LEFT:LX/Her;

    .line 2490856
    new-instance v0, LX/Her;

    const-string v1, "BOTTOM_RIGHT"

    invoke-direct {v0, v1, v5}, LX/Her;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Her;->BOTTOM_RIGHT:LX/Her;

    .line 2490857
    const/4 v0, 0x4

    new-array v0, v0, [LX/Her;

    sget-object v1, LX/Her;->TOP_LEFT:LX/Her;

    aput-object v1, v0, v2

    sget-object v1, LX/Her;->TOP_RIGHT:LX/Her;

    aput-object v1, v0, v3

    sget-object v1, LX/Her;->BOTTOM_LEFT:LX/Her;

    aput-object v1, v0, v4

    sget-object v1, LX/Her;->BOTTOM_RIGHT:LX/Her;

    aput-object v1, v0, v5

    sput-object v0, LX/Her;->$VALUES:[LX/Her;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2490858
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Her;
    .locals 1

    .prologue
    .line 2490859
    const-class v0, LX/Her;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Her;

    return-object v0
.end method

.method public static values()[LX/Her;
    .locals 1

    .prologue
    .line 2490860
    sget-object v0, LX/Her;->$VALUES:[LX/Her;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Her;

    return-object v0
.end method
