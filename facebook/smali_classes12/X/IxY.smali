.class public final LX/IxY;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2631859
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_b

    .line 2631860
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2631861
    :goto_0
    return v1

    .line 2631862
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_8

    .line 2631863
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 2631864
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2631865
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_0

    if-eqz v11, :cond_0

    .line 2631866
    const-string v12, "can_viewer_like"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 2631867
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    move v10, v3

    move v3, v2

    goto :goto_1

    .line 2631868
    :cond_1
    const-string v12, "category_names"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 2631869
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 2631870
    :cond_2
    const-string v12, "does_viewer_like"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 2631871
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v8, v0

    move v0, v2

    goto :goto_1

    .line 2631872
    :cond_3
    const-string v12, "id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 2631873
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 2631874
    :cond_4
    const-string v12, "name"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 2631875
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 2631876
    :cond_5
    const-string v12, "page_likers"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 2631877
    invoke-static {p0, p1}, LX/IxO;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 2631878
    :cond_6
    const-string v12, "profile_picture"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 2631879
    invoke-static {p0, p1}, LX/Ixg;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 2631880
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 2631881
    :cond_8
    const/4 v11, 0x7

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 2631882
    if-eqz v3, :cond_9

    .line 2631883
    invoke-virtual {p1, v1, v10}, LX/186;->a(IZ)V

    .line 2631884
    :cond_9
    invoke-virtual {p1, v2, v9}, LX/186;->b(II)V

    .line 2631885
    if-eqz v0, :cond_a

    .line 2631886
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v8}, LX/186;->a(IZ)V

    .line 2631887
    :cond_a
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2631888
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2631889
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2631890
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2631891
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_b
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    move v10, v1

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2631892
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2631893
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2631894
    if-eqz v0, :cond_0

    .line 2631895
    const-string v1, "can_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2631896
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2631897
    :cond_0
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2631898
    if-eqz v0, :cond_1

    .line 2631899
    const-string v0, "category_names"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2631900
    invoke-virtual {p0, p1, v2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 2631901
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2631902
    if-eqz v0, :cond_2

    .line 2631903
    const-string v1, "does_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2631904
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2631905
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2631906
    if-eqz v0, :cond_3

    .line 2631907
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2631908
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2631909
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2631910
    if-eqz v0, :cond_4

    .line 2631911
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2631912
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2631913
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2631914
    if-eqz v0, :cond_5

    .line 2631915
    const-string v1, "page_likers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2631916
    invoke-static {p0, v0, p2}, LX/IxO;->a(LX/15i;ILX/0nX;)V

    .line 2631917
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2631918
    if-eqz v0, :cond_6

    .line 2631919
    const-string v1, "profile_picture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2631920
    invoke-static {p0, v0, p2}, LX/Ixg;->a(LX/15i;ILX/0nX;)V

    .line 2631921
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2631922
    return-void
.end method
