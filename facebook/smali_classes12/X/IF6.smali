.class public LX/IF6;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/IF6;


# instance fields
.field public final a:Lcom/facebook/quicklog/QuickPerformanceLogger;


# direct methods
.method public constructor <init>(Lcom/facebook/quicklog/QuickPerformanceLogger;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2553572
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2553573
    iput-object p1, p0, LX/IF6;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 2553574
    return-void
.end method

.method public static a(LX/0QB;)LX/IF6;
    .locals 4

    .prologue
    .line 2553575
    sget-object v0, LX/IF6;->b:LX/IF6;

    if-nez v0, :cond_1

    .line 2553576
    const-class v1, LX/IF6;

    monitor-enter v1

    .line 2553577
    :try_start_0
    sget-object v0, LX/IF6;->b:LX/IF6;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2553578
    if-eqz v2, :cond_0

    .line 2553579
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2553580
    new-instance p0, LX/IF6;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v3

    check-cast v3, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-direct {p0, v3}, LX/IF6;-><init>(Lcom/facebook/quicklog/QuickPerformanceLogger;)V

    .line 2553581
    move-object v0, p0

    .line 2553582
    sput-object v0, LX/IF6;->b:LX/IF6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2553583
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2553584
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2553585
    :cond_1
    sget-object v0, LX/IF6;->b:LX/IF6;

    return-object v0

    .line 2553586
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2553587
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
