.class public final LX/ICL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;

.field public final synthetic b:Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;)V
    .locals 0

    .prologue
    .line 2549280
    iput-object p1, p0, LX/ICL;->b:Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;

    iput-object p2, p0, LX/ICL;->a:Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x1c4b7c4d

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2549281
    new-instance v1, LX/ICO;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/ICO;-><init>(Landroid/content/Context;)V

    .line 2549282
    new-instance v2, LX/ICK;

    invoke-direct {v2, p0, v1, p1}, LX/ICK;-><init>(LX/ICL;LX/ICO;Landroid/view/View;)V

    .line 2549283
    iput-object v2, v1, LX/ICO;->l:LX/ICK;

    .line 2549284
    invoke-virtual {v1, p1}, LX/0ht;->a(Landroid/view/View;)V

    .line 2549285
    iget-object v1, p0, LX/ICL;->b:Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;->d:Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;

    sget-object v2, LX/ICQ;->TAKEN:LX/ICQ;

    iget-object v3, p0, LX/ICL;->a:Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;->a(LX/ICQ;Lcom/facebook/graphql/model/GraphQLStoryGallerySurveyFeedUnit;)V

    .line 2549286
    iget-object v1, p0, LX/ICL;->b:Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/storygallerysurvey/StoryGallerySurveyFeedUnitTwoButtonFooterPartDefinition;->d:Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;

    sget-object v2, LX/ICQ;->TAKEN:LX/ICQ;

    invoke-virtual {v1, v2}, Lcom/facebook/feedplugins/storygallerysurvey/logger/StoryGallerySurveyLogger;->a(LX/ICQ;)V

    .line 2549287
    const v1, 0x54ab6513

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
