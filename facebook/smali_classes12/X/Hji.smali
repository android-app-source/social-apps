.class public LX/Hji;
.super LX/HjV;
.source ""


# instance fields
.field private final a:Ljava/lang/String;

.field public b:Landroid/content/Context;

.field private c:Z

.field private d:I

.field private e:I

.field private f:Z

.field private g:LX/HjY;

.field public h:LX/Hk9;

.field private i:Z

.field private j:LX/Hjl;

.field public k:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, LX/HjV;-><init>()V

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Hji;->a:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Hji;->i:Z

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;LX/Hk9;Ljava/util/Map;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/ads/internal/adapters/InterstitialAdapterListener;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, LX/Hji;->b:Landroid/content/Context;

    iput-object p2, p0, LX/Hji;->h:LX/Hk9;

    const-string v0, "data"

    invoke-interface {p3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    invoke-static {v0}, LX/Hjl;->a(Lorg/json/JSONObject;)LX/Hjl;

    move-result-object v0

    iput-object v0, p0, LX/Hji;->j:LX/Hjl;

    iget-object v0, p0, LX/Hji;->j:LX/Hjl;

    invoke-static {p1, v0}, LX/Hkl;->a(Landroid/content/Context;LX/HjW;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p2, p0}, LX/Hk9;->b(LX/HjV;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, LX/HjY;

    iget-object v1, p0, LX/Hji;->a:Ljava/lang/String;

    iget-object v2, p0, LX/Hji;->h:LX/Hk9;

    invoke-direct {v0, p1, v1, p0, v2}, LX/HjY;-><init>(Landroid/content/Context;Ljava/lang/String;LX/HjV;LX/Hk9;)V

    iput-object v0, p0, LX/Hji;->g:LX/HjY;

    iget-object v0, p0, LX/Hji;->g:LX/HjY;

    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    new-instance v2, Ljava/lang/StringBuilder;

    const-string p1, "com.facebook.ads.interstitial.impression.logged:"

    invoke-direct {v2, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object p1, v0, LX/HjY;->a:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    const-string p1, "com.facebook.ads.interstitial.displayed:"

    invoke-direct {v2, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object p1, v0, LX/HjY;->a:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    const-string p1, "com.facebook.ads.interstitial.dismissed:"

    invoke-direct {v2, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object p1, v0, LX/HjY;->a:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    const-string p1, "com.facebook.ads.interstitial.clicked:"

    invoke-direct {v2, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object p1, v0, LX/HjY;->a:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    const-string p1, "com.facebook.ads.interstitial.error:"

    invoke-direct {v2, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object p1, v0, LX/HjY;->a:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v2, v0, LX/HjY;->b:Landroid/content/Context;

    invoke-static {v2}, LX/0Xw;->a(Landroid/content/Context;)LX/0Xw;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, LX/0Xw;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Hji;->i:Z

    iget-object v0, p0, LX/Hji;->j:LX/Hjl;

    iget-object v1, v0, LX/Hjl;->g:Ljava/util/Map;

    move-object v1, v1

    const-string v0, "is_tablet"

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "is_tablet"

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, LX/Hji;->c:Z

    :cond_2
    const-string v0, "ad_height"

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "ad_height"

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Hji;->d:I

    :cond_3
    const-string v0, "ad_width"

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "ad_width"

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Hji;->e:I

    :cond_4
    const-string v0, "native_close"

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "native_close"

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/Hji;->f:Z

    :cond_5
    const-string v0, "preloadMarkup"

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "preloadMarkup"

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, LX/Hji;->j:LX/Hjl;

    if-nez v0, :cond_7

    :goto_1
    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, LX/Hji;->h:LX/Hk9;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Hji;->h:LX/Hk9;

    invoke-virtual {v0, p0}, LX/Hk9;->a(LX/HjV;)V

    goto/16 :goto_0

    :cond_7
    new-instance v3, Landroid/webkit/WebView;

    iget-object v4, p0, LX/Hji;->b:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, LX/Hji;->k:Landroid/webkit/WebView;

    iget-object v3, p0, LX/Hji;->k:Landroid/webkit/WebView;

    new-instance v4, LX/Hjh;

    invoke-direct {v4, p0}, LX/Hjh;-><init>(LX/Hji;)V

    invoke-virtual {v3, v4}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    iget-object v3, p0, LX/Hji;->k:Landroid/webkit/WebView;

    invoke-static {}, LX/Hkp;->a()Ljava/lang/String;

    move-result-object v4

    iget-object v5, v0, LX/Hjl;->a:Ljava/lang/String;

    move-object v5, v5

    const-string v6, "text/html"

    const-string v7, "utf-8"

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, LX/Hji;->g:LX/HjY;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Hji;->g:LX/HjY;

    :try_start_0
    iget-object v1, v0, LX/HjY;->b:Landroid/content/Context;

    invoke-static {v1}, LX/0Xw;->a(Landroid/content/Context;)LX/0Xw;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0Xw;->a(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v0, p0, LX/Hji;->k:Landroid/webkit/WebView;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Hji;->k:Landroid/webkit/WebView;

    invoke-static {v0}, LX/Hkp;->a(Landroid/webkit/WebView;)V

    iget-object v0, p0, LX/Hji;->k:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->destroy()V

    const/4 v0, 0x0

    iput-object v0, p0, LX/Hji;->k:Landroid/webkit/WebView;

    :cond_1
    return-void

    :catch_0
    goto :goto_0
.end method
