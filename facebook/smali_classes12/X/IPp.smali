.class public final LX/IPp;
.super LX/BcO;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/BcO",
        "<",
        "LX/IPr;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public b:Z

.field public c:LX/1rs;

.field public d:I

.field public e:Z

.field public final synthetic f:LX/IPr;


# direct methods
.method public constructor <init>(LX/IPr;)V
    .locals 1

    .prologue
    .line 2574690
    iput-object p1, p0, LX/IPp;->f:LX/IPr;

    .line 2574691
    move-object v0, p1

    .line 2574692
    invoke-direct {p0, v0}, LX/BcO;-><init>(LX/BcS;)V

    .line 2574693
    return-void
.end method


# virtual methods
.method public final a(Z)LX/BcO;
    .locals 2

    .prologue
    .line 2574708
    invoke-super {p0, p1}, LX/BcO;->a(Z)LX/BcO;

    move-result-object v0

    check-cast v0, LX/IPp;

    .line 2574709
    if-nez p1, :cond_0

    .line 2574710
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/IPp;->b:Z

    .line 2574711
    :cond_0
    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2574694
    if-ne p0, p1, :cond_1

    .line 2574695
    :cond_0
    :goto_0
    return v0

    .line 2574696
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2574697
    goto :goto_0

    .line 2574698
    :cond_3
    check-cast p1, LX/IPp;

    .line 2574699
    iget-boolean v2, p0, LX/IPp;->b:Z

    iget-boolean v3, p1, LX/IPp;->b:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 2574700
    goto :goto_0

    .line 2574701
    :cond_4
    iget-object v2, p0, LX/IPp;->c:LX/1rs;

    if-eqz v2, :cond_6

    iget-object v2, p0, LX/IPp;->c:LX/1rs;

    iget-object v3, p1, LX/IPp;->c:LX/1rs;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    :cond_5
    move v0, v1

    .line 2574702
    goto :goto_0

    .line 2574703
    :cond_6
    iget-object v2, p1, LX/IPp;->c:LX/1rs;

    if-nez v2, :cond_5

    .line 2574704
    :cond_7
    iget v2, p0, LX/IPp;->d:I

    iget v3, p1, LX/IPp;->d:I

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 2574705
    goto :goto_0

    .line 2574706
    :cond_8
    iget-boolean v2, p0, LX/IPp;->e:Z

    iget-boolean v3, p1, LX/IPp;->e:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 2574707
    goto :goto_0
.end method
