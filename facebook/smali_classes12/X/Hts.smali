.class public LX/Hts;
.super Landroid/widget/BaseAdapter;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:I

.field private final c:LX/Htv;

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/inlinesproutsinterfaces/InlineSproutItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/Htv;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2516892
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 2516893
    iput-object p1, p0, LX/Hts;->a:Landroid/content/Context;

    .line 2516894
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2516895
    iput-object v0, p0, LX/Hts;->d:LX/0Px;

    .line 2516896
    iput-object p2, p0, LX/Hts;->c:LX/Htv;

    .line 2516897
    const/4 v0, 0x2

    invoke-static {v0}, LX/3CW;->c(I)[Ljava/lang/Integer;

    move-result-object v0

    move-object v0, v0

    .line 2516898
    array-length v0, v0

    iput v0, p0, LX/Hts;->b:I

    .line 2516899
    return-void
.end method


# virtual methods
.method public final getCount()I
    .locals 1

    .prologue
    .line 2516891
    iget-object v0, p0, LX/Hts;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2516890
    iget-object v0, p0, LX/Hts;->d:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2516900
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 2516884
    invoke-virtual {p0, p1}, LX/Hts;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hs6;

    .line 2516885
    invoke-virtual {v0}, LX/Hs6;->d()LX/Hu0;

    move-result-object v0

    .line 2516886
    iget-object v1, v0, LX/Hu0;->h:LX/HuQ;

    move-object v1, v1

    .line 2516887
    if-eqz v1, :cond_0

    .line 2516888
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object v0, v1

    .line 2516889
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, LX/3CW;->a(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, LX/3CW;->a(I)I

    move-result v0

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    .line 2516852
    invoke-virtual {p0, p1}, LX/Hts;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hs6;

    .line 2516853
    invoke-virtual {v0}, LX/Hs6;->d()LX/Hu0;

    move-result-object v1

    .line 2516854
    iget-object v2, v1, LX/Hu0;->h:LX/HuQ;

    move-object v2, v2

    .line 2516855
    if-eqz v2, :cond_2

    .line 2516856
    iget-object v2, v1, LX/Hu0;->h:LX/HuQ;

    move-object v1, v2

    .line 2516857
    const/4 v4, 0x0

    .line 2516858
    if-nez p2, :cond_1

    .line 2516859
    iget-object v2, v1, LX/HuQ;->a:LX/HuR;

    const-string v3, "lightweight_place_picker_started"

    invoke-static {v2, v3}, LX/HuR;->a$redex0(LX/HuR;Ljava/lang/String;)V

    .line 2516860
    new-instance p2, LX/HuT;

    iget-object v2, v1, LX/HuQ;->a:LX/HuR;

    iget-object v2, v2, LX/HuR;->d:Landroid/content/Context;

    invoke-direct {p2, v2}, LX/HuT;-><init>(Landroid/content/Context;)V

    .line 2516861
    iget-object v2, p2, LX/HuT;->a:Lcom/facebook/composer/inlinesprouts/SproutListItem;

    move-object v2, v2

    .line 2516862
    invoke-static {v2}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    .line 2516863
    new-instance v3, LX/HuO;

    invoke-direct {v3, v1}, LX/HuO;-><init>(LX/HuQ;)V

    invoke-virtual {v2, v3}, Lcom/facebook/composer/inlinesprouts/SproutListItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2516864
    iget-object v3, v1, LX/HuQ;->a:LX/HuR;

    iget-object v3, v3, LX/HuR;->h:LX/Htv;

    iget-object p0, v1, LX/HuQ;->a:LX/HuR;

    iget-object p0, p0, LX/HuR;->i:LX/HuU;

    iget-object p1, v1, LX/HuQ;->a:LX/HuR;

    iget-object p1, p1, LX/HuR;->b:LX/Hr2;

    invoke-virtual {p0, p1}, LX/HuU;->a(LX/Hr2;)LX/Hu0;

    move-result-object p0

    invoke-virtual {v3, p0, v2}, LX/Htv;->a(LX/Hu0;Lcom/facebook/composer/inlinesprouts/SproutListItem;)V

    .line 2516865
    iget-object v2, p2, LX/HuT;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v2, v2

    .line 2516866
    new-instance v3, LX/1P1;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    invoke-direct {v3, v4, v4}, LX/1P1;-><init>(IZ)V

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2516867
    iget-object v3, v1, LX/HuQ;->a:LX/HuR;

    iget-object v3, v3, LX/HuR;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0il;

    .line 2516868
    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0j0;

    check-cast v3, LX/0j8;

    invoke-interface {v3}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->g()LX/0Px;

    move-result-object v3

    .line 2516869
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    iget-object p0, v1, LX/HuQ;->a:LX/HuR;

    iget-object p0, p0, LX/HuR;->g:LX/0ad;

    sget p1, LX/1EB;->ag:I

    const/16 p3, 0xf

    invoke-interface {p0, p1, p3}, LX/0ad;->a(II)I

    move-result p0

    invoke-static {v4, p0}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 2516870
    const/4 p0, 0x0

    invoke-virtual {v3, p0, v4}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v3

    .line 2516871
    iget-object v4, v1, LX/HuQ;->b:LX/J3e;

    .line 2516872
    if-nez v3, :cond_0

    .line 2516873
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    .line 2516874
    :cond_0
    iput-object v3, v4, LX/J3e;->b:Ljava/util/List;

    .line 2516875
    invoke-virtual {v4}, LX/1OM;->notifyDataSetChanged()V

    .line 2516876
    iget-object v3, v1, LX/HuQ;->b:LX/J3e;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2516877
    new-instance v3, LX/HuP;

    invoke-direct {v3, v1}, LX/HuP;-><init>(LX/HuQ;)V

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setOnScrollListener(LX/1OX;)V

    .line 2516878
    :cond_1
    move-object p2, p2

    .line 2516879
    :goto_0
    invoke-virtual {v0}, LX/Hs6;->g()LX/Hty;

    move-result-object v0

    invoke-virtual {v0}, LX/Hty;->getAnalyticsName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 2516880
    return-object p2

    .line 2516881
    :cond_2
    if-eqz p2, :cond_3

    check-cast p2, Lcom/facebook/composer/inlinesprouts/SproutListItem;

    .line 2516882
    :goto_1
    iget-object v2, p0, LX/Hts;->c:LX/Htv;

    invoke-virtual {v2, v1, p2}, LX/Htv;->a(LX/Hu0;Lcom/facebook/composer/inlinesprouts/SproutListItem;)V

    goto :goto_0

    .line 2516883
    :cond_3
    iget-object v2, p0, LX/Hts;->a:Landroid/content/Context;

    invoke-static {v2, p3}, Lcom/facebook/composer/inlinesprouts/SproutListItem;->a(Landroid/content/Context;Landroid/view/ViewGroup;)Lcom/facebook/composer/inlinesprouts/SproutListItem;

    move-result-object p2

    goto :goto_1
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2516851
    iget v0, p0, LX/Hts;->b:I

    return v0
.end method
