.class public final LX/IIH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V
    .locals 0

    .prologue
    .line 2561751
    iput-object p1, p0, LX/IIH;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 2

    .prologue
    .line 2561752
    if-eqz p2, :cond_0

    .line 2561753
    iget-object v0, p0, LX/IIH;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    .line 2561754
    iget-object v1, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->n:LX/IID;

    .line 2561755
    const-string p1, "friends_nearby_dashboard_search_open"

    invoke-static {v1, p1}, LX/IID;->i(LX/IID;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    .line 2561756
    iget-object p2, v1, LX/IID;->a:LX/0Zb;

    invoke-interface {p2, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2561757
    iget-object v1, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->O:Lcom/facebook/widget/text/BetterButton;

    const/16 p1, 0x8

    invoke-virtual {v1, p1}, Lcom/facebook/widget/text/BetterButton;->setVisibility(I)V

    .line 2561758
    invoke-static {v0}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->x(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;)V

    .line 2561759
    iget-object v1, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->Q:Lcom/facebook/widget/listview/SplitHideableListView;

    const/4 p1, 0x0

    invoke-virtual {v1, p1}, Lcom/facebook/widget/listview/SplitHideableListView;->setSelection(I)V

    .line 2561760
    iget-object v0, p0, LX/IIH;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, p0, LX/IIH;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;

    iget-object v1, v1, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->L:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;->a$redex0(Lcom/facebook/friendsnearby/ui/FriendsNearbyFragment;Ljava/lang/CharSequence;)V

    .line 2561761
    :cond_0
    return-void
.end method
