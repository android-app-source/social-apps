.class public final LX/HF8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/pagecreation/graphql/PageCategorySuggestionQueryModels$PageCategorySuggestionQueryModel$CategoriesModel;

.field public final synthetic b:LX/HF9;


# direct methods
.method public constructor <init>(LX/HF9;Lcom/facebook/pages/common/pagecreation/graphql/PageCategorySuggestionQueryModels$PageCategorySuggestionQueryModel$CategoriesModel;)V
    .locals 0

    .prologue
    .line 2443095
    iput-object p1, p0, LX/HF8;->b:LX/HF9;

    iput-object p2, p0, LX/HF8;->a:Lcom/facebook/pages/common/pagecreation/graphql/PageCategorySuggestionQueryModels$PageCategorySuggestionQueryModel$CategoriesModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x62b34f33

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2443096
    iget-object v1, p0, LX/HF8;->b:LX/HF9;

    iget-object v1, v1, LX/HF9;->a:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    iget-object v2, p0, LX/HF8;->a:Lcom/facebook/pages/common/pagecreation/graphql/PageCategorySuggestionQueryModels$PageCategorySuggestionQueryModel$CategoriesModel;

    invoke-virtual {v2}, Lcom/facebook/pages/common/pagecreation/graphql/PageCategorySuggestionQueryModels$PageCategorySuggestionQueryModel$CategoriesModel;->k()Ljava/lang/String;

    move-result-object v2

    .line 2443097
    iput-object v2, v1, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->i:Ljava/lang/String;

    .line 2443098
    new-instance v1, LX/HEu;

    iget-object v2, p0, LX/HF8;->a:Lcom/facebook/pages/common/pagecreation/graphql/PageCategorySuggestionQueryModels$PageCategorySuggestionQueryModel$CategoriesModel;

    invoke-virtual {v2}, Lcom/facebook/pages/common/pagecreation/graphql/PageCategorySuggestionQueryModels$PageCategorySuggestionQueryModel$CategoriesModel;->k()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/HF8;->a:Lcom/facebook/pages/common/pagecreation/graphql/PageCategorySuggestionQueryModels$PageCategorySuggestionQueryModel$CategoriesModel;

    invoke-virtual {v3}, Lcom/facebook/pages/common/pagecreation/graphql/PageCategorySuggestionQueryModels$PageCategorySuggestionQueryModel$CategoriesModel;->l()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, LX/HEu;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2443099
    iget-object v2, p0, LX/HF8;->b:LX/HF9;

    iget-object v2, v2, LX/HF9;->a:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    iget-object v2, v2, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->l:LX/HFJ;

    .line 2443100
    iput-object v1, v2, LX/HFJ;->c:LX/HEu;

    .line 2443101
    iget-object v1, p0, LX/HF8;->b:LX/HF9;

    iget-object v1, v1, LX/HF9;->a:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->j:Lcom/facebook/resources/ui/FbEditText;

    iget-object v2, p0, LX/HF8;->a:Lcom/facebook/pages/common/pagecreation/graphql/PageCategorySuggestionQueryModels$PageCategorySuggestionQueryModel$CategoriesModel;

    invoke-virtual {v2}, Lcom/facebook/pages/common/pagecreation/graphql/PageCategorySuggestionQueryModels$PageCategorySuggestionQueryModel$CategoriesModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2443102
    iget-object v1, p0, LX/HF8;->b:LX/HF9;

    iget-object v1, v1, LX/HF9;->a:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->m:Lcom/facebook/resources/ui/FbEditText;

    iget-object v2, p0, LX/HF8;->a:Lcom/facebook/pages/common/pagecreation/graphql/PageCategorySuggestionQueryModels$PageCategorySuggestionQueryModel$CategoriesModel;

    invoke-virtual {v2}, Lcom/facebook/pages/common/pagecreation/graphql/PageCategorySuggestionQueryModels$PageCategorySuggestionQueryModel$CategoriesModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2443103
    iget-object v1, p0, LX/HF8;->b:LX/HF9;

    iget-object v1, v1, LX/HF9;->a:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->m:Lcom/facebook/resources/ui/FbEditText;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbEditText;->setVisibility(I)V

    .line 2443104
    iget-object v1, p0, LX/HF8;->b:LX/HF9;

    iget-object v1, v1, LX/HF9;->a:Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    new-instance v2, LX/HEu;

    iget-object v3, p0, LX/HF8;->a:Lcom/facebook/pages/common/pagecreation/graphql/PageCategorySuggestionQueryModels$PageCategorySuggestionQueryModel$CategoriesModel;

    invoke-virtual {v3}, Lcom/facebook/pages/common/pagecreation/graphql/PageCategorySuggestionQueryModels$PageCategorySuggestionQueryModel$CategoriesModel;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/HF8;->a:Lcom/facebook/pages/common/pagecreation/graphql/PageCategorySuggestionQueryModels$PageCategorySuggestionQueryModel$CategoriesModel;

    invoke-virtual {v4}, Lcom/facebook/pages/common/pagecreation/graphql/PageCategorySuggestionQueryModels$PageCategorySuggestionQueryModel$CategoriesModel;->j()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, LX/HEu;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2443105
    iput-object v2, v1, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->n:LX/HEu;

    .line 2443106
    const v1, -0x673dc2f8

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
