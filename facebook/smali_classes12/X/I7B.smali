.class public final LX/I7B;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$EventPermalinkActivityQueryModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/events/permalink/EventPermalinkFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/permalink/EventPermalinkFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2538651
    iput-object p1, p0, LX/I7B;->b:Lcom/facebook/events/permalink/EventPermalinkFragment;

    iput-object p2, p0, LX/I7B;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2538652
    new-instance v0, LX/7o2;

    invoke-direct {v0}, LX/7o2;-><init>()V

    move-object v0, v0

    .line 2538653
    const-string v1, "event_id"

    iget-object v2, p0, LX/I7B;->b:Lcom/facebook/events/permalink/EventPermalinkFragment;

    iget-object v2, v2, Lcom/facebook/events/permalink/EventPermalinkFragment;->aG:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2538654
    const-string v1, "count"

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2538655
    const-string v1, "end_cursor"

    iget-object v2, p0, LX/I7B;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2538656
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2538657
    iget-object v1, p0, LX/I7B;->b:Lcom/facebook/events/permalink/EventPermalinkFragment;

    iget-object v1, v1, Lcom/facebook/events/permalink/EventPermalinkFragment;->M:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0
.end method
