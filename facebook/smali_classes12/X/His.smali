.class public final LX/His;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/view/LayoutInflater;

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Landroid/location/Address;",
            ">;"
        }
    .end annotation
.end field

.field public c:Z

.field public d:LX/Hiu;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2497989
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2497990
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/His;->c:Z

    .line 2497991
    iput-object p1, p0, LX/His;->a:Landroid/view/LayoutInflater;

    .line 2497992
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2497993
    iput-object v0, p0, LX/His;->b:LX/0Px;

    .line 2497994
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2497995
    packed-switch p2, :pswitch_data_0

    .line 2497996
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2497997
    :pswitch_0
    iget-object v0, p0, LX/His;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f0300ad

    invoke-virtual {v0, v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2497998
    new-instance v1, LX/Hiq;

    invoke-direct {v1, p0, v0}, LX/Hiq;-><init>(LX/His;Lcom/facebook/fbui/widget/contentview/ContentView;)V

    move-object v0, v1

    goto :goto_0

    .line 2497999
    :pswitch_1
    new-instance v0, LX/Hir;

    iget-object v1, p0, LX/His;->a:Landroid/view/LayoutInflater;

    const v2, 0x7f0300aa

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Hir;-><init>(Landroid/view/View;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(LX/1a1;I)V
    .locals 2

    .prologue
    .line 2498000
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2498001
    :cond_0
    :goto_0
    return-void

    .line 2498002
    :pswitch_0
    check-cast p1, LX/Hiq;

    iget-object v0, p0, LX/His;->b:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Address;

    .line 2498003
    invoke-virtual {v0}, Landroid/location/Address;->getMaxAddressLineIndex()I

    move-result v1

    if-ltz v1, :cond_1

    .line 2498004
    iget-object v1, p1, LX/Hiq;->m:Lcom/facebook/fbui/widget/contentview/ContentView;

    const/4 p0, 0x0

    invoke-virtual {v0, p0}, Landroid/location/Address;->getAddressLine(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2498005
    iget-object v1, p1, LX/Hiq;->m:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v0}, Landroid/location/Address;->getLocality()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2498006
    :goto_1
    iget-object v1, p1, LX/Hiq;->m:Lcom/facebook/fbui/widget/contentview/ContentView;

    new-instance p0, LX/Hip;

    invoke-direct {p0, p1, v0}, LX/Hip;-><init>(LX/Hiq;Landroid/location/Address;)V

    invoke-virtual {v1, p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2498007
    goto :goto_0

    .line 2498008
    :pswitch_1
    iget-boolean v0, p0, LX/His;->c:Z

    if-nez v0, :cond_0

    .line 2498009
    check-cast p1, LX/Hir;

    iget-object v0, p1, LX/Hir;->l:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 2498010
    :cond_1
    iget-object v1, p1, LX/Hiq;->m:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v0}, Landroid/location/Address;->getLocality()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2498011
    invoke-virtual {p0}, LX/1OM;->ij_()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2498012
    iget-object v0, p0, LX/His;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method
