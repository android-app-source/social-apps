.class public final LX/IMh;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/groups/community/protocol/GroupJoinWithEmailMutationModels$GroupJoinWithEmailModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/ILc;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Ljava/util/ArrayList;

.field public final synthetic e:LX/IMk;


# direct methods
.method public constructor <init>(LX/IMk;LX/ILc;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 2570092
    iput-object p1, p0, LX/IMh;->e:LX/IMk;

    iput-object p2, p0, LX/IMh;->a:LX/ILc;

    iput-object p3, p0, LX/IMh;->b:Ljava/lang/String;

    iput-object p4, p0, LX/IMh;->c:Ljava/lang/String;

    iput-object p5, p0, LX/IMh;->d:Ljava/util/ArrayList;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 10

    .prologue
    .line 2570093
    iget-object v0, p0, LX/IMh;->e:LX/IMk;

    iget-object v1, p0, LX/IMh;->b:Ljava/lang/String;

    iget-object v2, p0, LX/IMh;->c:Ljava/lang/String;

    iget-object v3, p0, LX/IMh;->d:Ljava/util/ArrayList;

    invoke-static {v0, v1, v2, v3, p1}, LX/IMk;->a$redex0(LX/IMk;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Throwable;)V

    .line 2570094
    iget-object v0, p0, LX/IMh;->a:LX/ILc;

    iget-object v1, p0, LX/IMh;->e:LX/IMk;

    iget-object v2, p0, LX/IMh;->d:Ljava/util/ArrayList;

    const/4 v4, 0x0

    const/4 v9, 0x1

    const/4 p0, 0x0

    .line 2570095
    const/4 v3, -0x1

    .line 2570096
    instance-of v5, p1, LX/4Ua;

    if-eqz v5, :cond_0

    .line 2570097
    check-cast p1, LX/4Ua;

    .line 2570098
    iget-object v5, p1, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    if-eqz v5, :cond_2

    .line 2570099
    iget-object v3, p1, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    iget v3, v3, Lcom/facebook/graphql/error/GraphQLError;->code:I

    .line 2570100
    packed-switch v3, :pswitch_data_0

    .line 2570101
    iget-object v4, p1, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    iget-object v5, v4, Lcom/facebook/graphql/error/GraphQLError;->summary:Ljava/lang/String;

    .line 2570102
    iget-object v4, p1, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    iget-object v4, v4, Lcom/facebook/graphql/error/GraphQLError;->description:Ljava/lang/String;

    .line 2570103
    :goto_0
    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2570104
    :cond_0
    iget-object v4, v1, LX/IMk;->c:Landroid/content/res/Resources;

    const v5, 0x7f08003c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 2570105
    iget-object v4, v1, LX/IMk;->c:Landroid/content/res/Resources;

    const v6, 0x7f08003e

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2570106
    :cond_1
    new-instance v6, LX/IMp;

    invoke-direct {v6, v5, v4, v3}, LX/IMp;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    move-object v1, v6

    .line 2570107
    invoke-interface {v0, v1}, LX/ILc;->a(LX/IMp;)V

    .line 2570108
    return-void

    .line 2570109
    :pswitch_0
    iget-object v4, v1, LX/IMk;->c:Landroid/content/res/Resources;

    const v5, 0x7f083029

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 2570110
    iget-object v4, v1, LX/IMk;->c:Landroid/content/res/Resources;

    const v6, 0x7f083029

    new-array v7, v9, [Ljava/lang/Object;

    const-string v8, " or "

    new-array v9, v9, [Ljava/lang/Object;

    aput-object v2, v9, p0

    invoke-static {v8, v9}, LX/0YN;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, p0

    invoke-virtual {v4, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :cond_2
    move-object v5, v4

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x14ff1a
        :pswitch_0
    .end packed-switch
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2570111
    iget-object v0, p0, LX/IMh;->a:LX/ILc;

    invoke-interface {v0}, LX/ILc;->a()V

    .line 2570112
    return-void
.end method
