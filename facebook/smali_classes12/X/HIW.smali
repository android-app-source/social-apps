.class public final LX/HIW;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/HIX;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/HIR;

.field public b:LX/HIU;

.field public c:LX/HIT;

.field public d:LX/HIS;

.field public final synthetic e:LX/HIX;


# direct methods
.method public constructor <init>(LX/HIX;)V
    .locals 1

    .prologue
    .line 2451229
    iput-object p1, p0, LX/HIW;->e:LX/HIX;

    .line 2451230
    move-object v0, p1

    .line 2451231
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2451232
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2451233
    const-string v0, "HScrollPageCardFooterComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2451234
    if-ne p0, p1, :cond_1

    .line 2451235
    :cond_0
    :goto_0
    return v0

    .line 2451236
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2451237
    goto :goto_0

    .line 2451238
    :cond_3
    check-cast p1, LX/HIW;

    .line 2451239
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2451240
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2451241
    if-eq v2, v3, :cond_0

    .line 2451242
    iget-object v2, p0, LX/HIW;->a:LX/HIR;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/HIW;->a:LX/HIR;

    iget-object v3, p1, LX/HIW;->a:LX/HIR;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2451243
    goto :goto_0

    .line 2451244
    :cond_5
    iget-object v2, p1, LX/HIW;->a:LX/HIR;

    if-nez v2, :cond_4

    .line 2451245
    :cond_6
    iget-object v2, p0, LX/HIW;->b:LX/HIU;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/HIW;->b:LX/HIU;

    iget-object v3, p1, LX/HIW;->b:LX/HIU;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2451246
    goto :goto_0

    .line 2451247
    :cond_8
    iget-object v2, p1, LX/HIW;->b:LX/HIU;

    if-nez v2, :cond_7

    .line 2451248
    :cond_9
    iget-object v2, p0, LX/HIW;->c:LX/HIT;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/HIW;->c:LX/HIT;

    iget-object v3, p1, LX/HIW;->c:LX/HIT;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2451249
    goto :goto_0

    .line 2451250
    :cond_b
    iget-object v2, p1, LX/HIW;->c:LX/HIT;

    if-nez v2, :cond_a

    .line 2451251
    :cond_c
    iget-object v2, p0, LX/HIW;->d:LX/HIS;

    if-eqz v2, :cond_d

    iget-object v2, p0, LX/HIW;->d:LX/HIS;

    iget-object v3, p1, LX/HIW;->d:LX/HIS;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2451252
    goto :goto_0

    .line 2451253
    :cond_d
    iget-object v2, p1, LX/HIW;->d:LX/HIS;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
