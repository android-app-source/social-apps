.class public LX/IB5;
.super LX/2s5;
.source ""


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Blc;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/content/Context;

.field public final d:[Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;

.field private e:Landroid/os/Bundle;

.field public f:LX/IB0;


# direct methods
.method public constructor <init>(LX/0gc;Landroid/content/Context;Ljava/util/List;LX/0Px;Landroid/os/Bundle;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0gc;",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "LX/Blc;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x3

    .line 2546599
    invoke-direct {p0, p1}, LX/2s5;-><init>(LX/0gc;)V

    .line 2546600
    iput-object p2, p0, LX/IB5;->c:Landroid/content/Context;

    .line 2546601
    if-eqz p3, :cond_0

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    if-eq v0, v3, :cond_2

    .line 2546602
    :cond_0
    new-array v0, v3, [LX/Blc;

    const/4 v1, 0x0

    sget-object v2, LX/Blc;->PRIVATE_GOING:LX/Blc;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LX/Blc;->PRIVATE_MAYBE:LX/Blc;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, LX/Blc;->PRIVATE_INVITED:LX/Blc;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/IB5;->a:Ljava/util/List;

    .line 2546603
    :goto_0
    if-eqz p4, :cond_1

    invoke-virtual {p4}, LX/0Px;->size()I

    move-result v0

    if-eq v0, v3, :cond_3

    .line 2546604
    :cond_1
    const v0, 0x7f081f1c

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const v1, 0x7f081f1d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f081f1e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/IB5;->b:LX/0Px;

    .line 2546605
    :goto_1
    iget-object v0, p0, LX/IB5;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;

    iput-object v0, p0, LX/IB5;->d:[Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;

    .line 2546606
    iput-object p5, p0, LX/IB5;->e:Landroid/os/Bundle;

    .line 2546607
    return-void

    .line 2546608
    :cond_2
    iput-object p3, p0, LX/IB5;->a:Ljava/util/List;

    goto :goto_0

    .line 2546609
    :cond_3
    iput-object p4, p0, LX/IB5;->b:LX/0Px;

    goto :goto_1
.end method


# virtual methods
.method public final G_(I)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 2546597
    iget-object v0, p0, LX/IB5;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v0, p0, LX/IB5;->b:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 2546598
    iget-object v0, p0, LX/IB5;->d:[Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2546588
    invoke-virtual {p0, p2}, LX/2s5;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2546589
    iget-object v1, p0, LX/IB5;->d:[Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;

    iget-object v2, p0, LX/IB5;->e:Landroid/os/Bundle;

    iget-object v0, p0, LX/IB5;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Blc;

    .line 2546590
    new-instance v3, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;

    invoke-direct {v3}, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;-><init>()V

    .line 2546591
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4, v2}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    .line 2546592
    const-string v5, "EVENT_GUEST_LIST_RSVP_TYPE"

    invoke-virtual {v0}, LX/Blc;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2546593
    invoke-virtual {v3, v4}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2546594
    move-object v0, v3

    .line 2546595
    aput-object v0, v1, p2

    .line 2546596
    :cond_0
    invoke-super {p0, p1, p2}, LX/2s5;->a(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2546587
    iget-object v0, p0, LX/IB5;->d:[Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;

    array-length v0, v0

    return v0
.end method

.method public final b(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2546582
    invoke-super {p0, p1, p2, p3}, LX/2s5;->b(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 2546583
    iget-object v0, p0, LX/IB5;->f:LX/IB0;

    if-eqz v0, :cond_0

    .line 2546584
    iget-object v1, p0, LX/IB5;->f:LX/IB0;

    iget-object v0, p0, LX/IB5;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Blc;

    .line 2546585
    iget-object p0, v1, LX/IB0;->a:Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;

    iget-object p0, p0, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFrameFragment;->a:LX/IAH;

    invoke-virtual {p0, v0}, LX/IAH;->a(LX/Blc;)V

    .line 2546586
    :cond_0
    return-void
.end method

.method public final d()Ljava/util/Set;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2546576
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v1

    .line 2546577
    iget-object v2, p0, LX/IB5;->d:[Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 2546578
    if-eqz v4, :cond_0

    .line 2546579
    invoke-virtual {v4}, Lcom/facebook/events/permalink/messageguests/EventMessageGuestsFragment;->d()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 2546580
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2546581
    :cond_1
    invoke-static {v1}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
