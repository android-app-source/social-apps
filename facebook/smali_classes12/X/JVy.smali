.class public LX/JVy;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/JVx;


# direct methods
.method public constructor <init>(LX/JVx;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2701923
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2701924
    iput-object p1, p0, LX/JVy;->a:LX/JVx;

    .line 2701925
    return-void
.end method

.method public static a(LX/0QB;)LX/JVy;
    .locals 4

    .prologue
    .line 2701926
    const-class v1, LX/JVy;

    monitor-enter v1

    .line 2701927
    :try_start_0
    sget-object v0, LX/JVy;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2701928
    sput-object v2, LX/JVy;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2701929
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2701930
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2701931
    new-instance p0, LX/JVy;

    invoke-static {v0}, LX/JVx;->a(LX/0QB;)LX/JVx;

    move-result-object v3

    check-cast v3, LX/JVx;

    invoke-direct {p0, v3}, LX/JVy;-><init>(LX/JVx;)V

    .line 2701932
    move-object v0, p0

    .line 2701933
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2701934
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JVy;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2701935
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2701936
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;",
            ">;)",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2701937
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2701938
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;

    .line 2701939
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;->r()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2701940
    const/4 v0, 0x0

    .line 2701941
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;->r()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnitItem;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;",
            ">;)",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2701942
    const/4 v0, 0x1

    const/4 v3, 0x0

    .line 2701943
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 2701944
    check-cast v1, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;

    .line 2701945
    const/4 v5, 0x0

    .line 2701946
    iget-object v2, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 2701947
    check-cast v2, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;

    .line 2701948
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;->r()LX/0Px;

    move-result-object v6

    .line 2701949
    invoke-virtual {v6}, LX/0Px;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_d

    .line 2701950
    if-eqz v0, :cond_0

    .line 2701951
    iget-object v4, p0, LX/JVy;->a:LX/JVx;

    const-string v6, "Invalid PageLike unit: feedUnit.getItems() is empty."

    invoke-virtual {v4, v2, v6}, LX/JVx;->a(Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;Ljava/lang/String;)V

    :cond_0
    move-object v2, v5

    .line 2701952
    :goto_0
    move-object v2, v2

    .line 2701953
    if-nez v2, :cond_1

    move-object v1, v3

    .line 2701954
    :goto_1
    move-object v0, v1

    .line 2701955
    return-object v0

    .line 2701956
    :cond_1
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->A()Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitsConnection;

    move-result-object v2

    .line 2701957
    if-nez v2, :cond_3

    .line 2701958
    if-eqz v0, :cond_2

    .line 2701959
    iget-object v2, p0, LX/JVy;->a:LX/JVx;

    const-string v4, "Invalid PageLike unit: pymlItems is null"

    invoke-virtual {v2, v1, v4}, LX/JVx;->a(Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;Ljava/lang/String;)V

    :cond_2
    move-object v1, v3

    .line 2701960
    goto :goto_1

    .line 2701961
    :cond_3
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitsConnection;->a()LX/0Px;

    move-result-object v2

    .line 2701962
    if-nez v2, :cond_5

    .line 2701963
    if-eqz v0, :cond_4

    .line 2701964
    iget-object v2, p0, LX/JVy;->a:LX/JVx;

    const-string v4, "Invalid PageLike unit: pymlItems.getEdges() is null"

    invoke-virtual {v2, v1, v4}, LX/JVx;->a(Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;Ljava/lang/String;)V

    :cond_4
    move-object v1, v3

    .line 2701965
    goto :goto_1

    .line 2701966
    :cond_5
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 2701967
    if-eqz v0, :cond_6

    .line 2701968
    iget-object v2, p0, LX/JVy;->a:LX/JVx;

    const-string v4, "Invalid PageLike unit: pymlItems.getEdges() is empty"

    invoke-virtual {v2, v1, v4}, LX/JVx;->a(Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;Ljava/lang/String;)V

    :cond_6
    move-object v1, v3

    .line 2701969
    goto :goto_1

    .line 2701970
    :cond_7
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitsEdge;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitsEdge;->a()Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;

    move-result-object v2

    .line 2701971
    if-nez v2, :cond_9

    .line 2701972
    if-eqz v0, :cond_8

    .line 2701973
    iget-object v2, p0, LX/JVy;->a:LX/JVx;

    const-string v4, "Invalid PageLike unit: node is null"

    invoke-virtual {v2, v1, v4}, LX/JVx;->a(Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;Ljava/lang/String;)V

    :cond_8
    move-object v1, v3

    .line 2701974
    goto :goto_1

    .line 2701975
    :cond_9
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;->B()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v4

    if-eqz v4, :cond_a

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;->B()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_a

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;->B()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_a

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;->u()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    if-eqz v4, :cond_a

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;->u()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v4

    if-eqz v4, :cond_a

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;->u()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v4

    if-nez v4, :cond_14

    .line 2701976
    :cond_a
    const/4 v4, 0x0

    .line 2701977
    :goto_2
    move v4, v4

    .line 2701978
    if-nez v4, :cond_c

    .line 2701979
    if-eqz v0, :cond_b

    .line 2701980
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;->B()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v4

    if-nez v4, :cond_15

    .line 2701981
    iget-object v4, p0, LX/JVy;->a:LX/JVx;

    const-string v5, "Invalid PageLike unit: feedUnitItem.getProfile() == null"

    invoke-virtual {v4, v1, v5}, LX/JVx;->a(Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;Ljava/lang/String;)V

    .line 2701982
    :cond_b
    :goto_3
    move-object v1, v3

    .line 2701983
    goto/16 :goto_1

    .line 2701984
    :cond_c
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 2701985
    invoke-static {v2, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;LX/0Px;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    goto/16 :goto_1

    .line 2701986
    :cond_d
    const/4 v4, 0x0

    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnitItem;

    .line 2701987
    if-nez v4, :cond_f

    .line 2701988
    if-eqz v0, :cond_e

    .line 2701989
    iget-object v4, p0, LX/JVy;->a:LX/JVx;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Invalid PageLike unit: 0th GraphQLPagesYouMayAdvertiseFeedUnitItem is null, list size: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v6

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v2, v6}, LX/JVx;->a(Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;Ljava/lang/String;)V

    :cond_e
    move-object v2, v5

    .line 2701990
    goto/16 :goto_0

    .line 2701991
    :cond_f
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnitItem;->j()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v4

    .line 2701992
    if-nez v4, :cond_11

    .line 2701993
    if-eqz v0, :cond_10

    .line 2701994
    iget-object v4, p0, LX/JVy;->a:LX/JVx;

    const-string v6, "Invalid PageLike unit: FeedUnitPreview is null"

    invoke-virtual {v4, v2, v6}, LX/JVx;->a(Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;Ljava/lang/String;)V

    :cond_10
    move-object v2, v5

    .line 2701995
    goto/16 :goto_0

    .line 2701996
    :cond_11
    instance-of v6, v4, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    if-nez v6, :cond_13

    .line 2701997
    if-eqz v0, :cond_12

    .line 2701998
    iget-object v6, p0, LX/JVy;->a:LX/JVx;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Invalid PageLike unit: FeedUnitPreview is not an instanceof GraphQLPYMLWithLargeImageFeedUnit, class: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", GraphQLType: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {v4}, Lcom/facebook/graphql/model/FeedUnit;->D_()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v2, v4}, LX/JVx;->a(Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;Ljava/lang/String;)V

    :cond_12
    move-object v2, v5

    .line 2701999
    goto/16 :goto_0

    :cond_13
    move-object v2, v4

    .line 2702000
    check-cast v2, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    goto/16 :goto_0

    :cond_14
    const/4 v4, 0x1

    goto/16 :goto_2

    .line 2702001
    :cond_15
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;->B()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_16

    .line 2702002
    iget-object v4, p0, LX/JVy;->a:LX/JVx;

    const-string v5, "Invalid PageLike unit: feedUnitItem.getProfile().getId() == null"

    invoke-virtual {v4, v1, v5}, LX/JVx;->a(Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 2702003
    :cond_16
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;->B()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_17

    .line 2702004
    iget-object v4, p0, LX/JVy;->a:LX/JVx;

    const-string v5, "Invalid PageLike unit: feedUnitItem.getProfile().getName() == null"

    invoke-virtual {v4, v1, v5}, LX/JVx;->a(Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 2702005
    :cond_17
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;->u()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    if-nez v4, :cond_18

    .line 2702006
    iget-object v4, p0, LX/JVy;->a:LX/JVx;

    const-string v5, "Invalid PageLike unit: feedUnitItem.getCreativeImage() == null"

    invoke-virtual {v4, v1, v5}, LX/JVx;->a(Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 2702007
    :cond_18
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;->u()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v4

    if-nez v4, :cond_19

    .line 2702008
    iget-object v4, p0, LX/JVy;->a:LX/JVx;

    const-string v5, "Invalid PageLike unit: feedUnitItem.getCreativeImage().getWidth() == 0"

    invoke-virtual {v4, v1, v5}, LX/JVx;->a(Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 2702009
    :cond_19
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;->u()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v4

    if-nez v4, :cond_b

    .line 2702010
    iget-object v4, p0, LX/JVy;->a:LX/JVx;

    const-string v5, "Invalid PageLike unit: feedUnitItem.getCreativeImage().getHeight() == 0"

    invoke-virtual {v4, v1, v5}, LX/JVx;->a(Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;Ljava/lang/String;)V

    goto/16 :goto_3
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Z)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;",
            ">;Z)",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2702011
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2702012
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;

    .line 2702013
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;->r()LX/0Px;

    move-result-object v3

    .line 2702014
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2702015
    if-eqz p2, :cond_0

    .line 2702016
    iget-object v1, p0, LX/JVy;->a:LX/JVx;

    const-string v3, "Invalid GraphQLStory unit: feedUnit.getItems() is empty."

    invoke-virtual {v1, v0, v3}, LX/JVx;->a(Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;Ljava/lang/String;)V

    :cond_0
    move-object v0, v2

    .line 2702017
    :goto_0
    return-object v0

    .line 2702018
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnitItem;

    .line 2702019
    if-nez v1, :cond_3

    .line 2702020
    if-eqz p2, :cond_2

    .line 2702021
    iget-object v1, p0, LX/JVy;->a:LX/JVx;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Invalid GraphQLStory unit: feedItems.get(0) == null, size: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, LX/JVx;->a(Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;Ljava/lang/String;)V

    :cond_2
    move-object v0, v2

    .line 2702022
    goto :goto_0

    .line 2702023
    :cond_3
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnitItem;->j()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    .line 2702024
    if-nez v1, :cond_5

    .line 2702025
    if-eqz p2, :cond_4

    .line 2702026
    iget-object v1, p0, LX/JVy;->a:LX/JVx;

    const-string v3, "Invalid GraphQLStory unit: feed unit preview is null"

    invoke-virtual {v1, v0, v3}, LX/JVx;->a(Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;Ljava/lang/String;)V

    :cond_4
    move-object v0, v2

    .line 2702027
    goto :goto_0

    .line 2702028
    :cond_5
    instance-of v3, v1, Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v3, :cond_7

    .line 2702029
    if-eqz p2, :cond_6

    .line 2702030
    iget-object v3, p0, LX/JVy;->a:LX/JVx;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Invalid GraphQLStory unit: FeedUnitPreview is not an instanceof GraphQLStory, class: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", GraphQLType: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v1}, Lcom/facebook/graphql/model/FeedUnit;->D_()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, LX/JVx;->a(Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;Ljava/lang/String;)V

    :cond_6
    move-object v0, v2

    .line 2702031
    goto :goto_0

    .line 2702032
    :cond_7
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    move-object v0, v1

    .line 2702033
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0, v2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;LX/0Px;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPagesYouMayAdvertiseFeedUnit;",
            ">;)",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2702034
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/JVy;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Z)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    return-object v0
.end method
