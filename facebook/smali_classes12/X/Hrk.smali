.class public LX/Hrk;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/composer/activity/InlinePrivacySurveyController$DataProvider;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/composer/activity/InlinePrivacySurveyController$PostSurveyActions;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lcom/facebook/privacy/audience/InlinePrivacySurveyConfig;

.field public e:Z

.field public final f:LX/339;

.field public final g:Landroid/content/Context;

.field private final h:LX/03V;

.field public final i:LX/8Ps;

.field public final j:LX/33A;

.field public final k:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public final l:LX/2c9;

.field public final m:LX/Hrg;


# direct methods
.method public constructor <init>(LX/339;Landroid/content/Context;LX/03V;LX/8Ps;LX/33A;LX/0Or;LX/2c9;)V
    .locals 1
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/privacy/gating/IsInlinePrivacySurveyEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/339;",
            "Landroid/content/Context;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/8Ps;",
            "LX/33A;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/2c9;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2513435
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2513436
    new-instance v0, LX/Hrg;

    invoke-direct {v0, p0}, LX/Hrg;-><init>(LX/Hrk;)V

    iput-object v0, p0, LX/Hrk;->m:LX/Hrg;

    .line 2513437
    iput-object p1, p0, LX/Hrk;->f:LX/339;

    .line 2513438
    iput-object p2, p0, LX/Hrk;->g:Landroid/content/Context;

    .line 2513439
    iput-object p3, p0, LX/Hrk;->h:LX/03V;

    .line 2513440
    iput-object p4, p0, LX/Hrk;->i:LX/8Ps;

    .line 2513441
    iput-object p5, p0, LX/Hrk;->j:LX/33A;

    .line 2513442
    iput-object p6, p0, LX/Hrk;->k:LX/0Or;

    .line 2513443
    iput-object p7, p0, LX/Hrk;->l:LX/2c9;

    .line 2513444
    return-void
.end method

.method public static a(ZZLcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;)LX/AP5;
    .locals 1

    .prologue
    .line 2513445
    if-nez p0, :cond_0

    .line 2513446
    sget-object v0, LX/AP5;->NONE:LX/AP5;

    .line 2513447
    :goto_0
    return-object v0

    .line 2513448
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;->NONE:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    if-eq p2, v0, :cond_1

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    if-ne p2, v0, :cond_2

    .line 2513449
    :cond_1
    sget-object v0, LX/AP5;->NONE:LX/AP5;

    goto :goto_0

    .line 2513450
    :cond_2
    if-eqz p1, :cond_3

    .line 2513451
    sget-object v0, LX/AP5;->TAGGEES:LX/AP5;

    goto :goto_0

    .line 2513452
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;->FRIENDS_OF_TAGGEES:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    if-ne p2, v0, :cond_4

    sget-object v0, LX/AP5;->FRIENDS_OF_TAGGEES:LX/AP5;

    goto :goto_0

    :cond_4
    sget-object v0, LX/AP5;->TAGGEES:LX/AP5;

    goto :goto_0
.end method

.method public static a$redex0(LX/Hrk;Lcom/facebook/graphql/model/GraphQLPrivacyOption;LX/HqL;LX/HqM;)V
    .locals 6

    .prologue
    .line 2513453
    invoke-virtual {p2}, LX/HqL;->a()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v0

    .line 2513454
    iget-object p2, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, p2

    .line 2513455
    invoke-static {p1, v0}, LX/2cA;->a(LX/1oU;LX/1oU;)Z

    move-result v0

    .line 2513456
    if-eqz v0, :cond_0

    .line 2513457
    new-instance v0, LX/Hri;

    invoke-direct {v0, p0, p3}, LX/Hri;-><init>(LX/Hrk;LX/HqM;)V

    .line 2513458
    const/4 p3, 0x1

    const/4 p2, 0x0

    .line 2513459
    invoke-static {p0}, LX/Hrk;->c(LX/Hrk;)LX/HqL;

    move-result-object v1

    .line 2513460
    iget-object v2, p0, LX/Hrk;->g:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081487

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2513461
    invoke-virtual {v1}, LX/HqL;->b()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v1}, LX/HqL;->a()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v1

    .line 2513462
    iget-object v3, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v3, v3

    .line 2513463
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->e()Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    move-result-object v3

    .line 2513464
    sget-object v4, LX/Hrj;->a:[I

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;->ordinal()I

    move-result v3

    aget v3, v4, v3

    packed-switch v3, :pswitch_data_0

    .line 2513465
    const v3, 0x7f081488

    :goto_0
    move v1, v3

    .line 2513466
    :goto_1
    iget-object v3, p0, LX/Hrk;->g:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    new-array v4, p3, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->d()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, p2

    invoke-virtual {v3, v1, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2513467
    new-instance v3, LX/47x;

    iget-object v4, p0, LX/Hrk;->g:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v3, v4}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {v3, v2}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    move-result-object v2

    const-string v3, "\n\n"

    invoke-virtual {v2, v3}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    move-result-object v2

    new-instance v3, Landroid/text/style/StyleSpan;

    invoke-direct {v3, p3}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/16 v4, 0x21

    invoke-virtual {v2, v3, v4}, LX/47x;->a(Ljava/lang/Object;I)LX/47x;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    move-result-object v1

    invoke-virtual {v1}, LX/47x;->a()LX/47x;

    move-result-object v1

    invoke-virtual {v1}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v1

    .line 2513468
    new-instance v2, LX/31Y;

    iget-object v3, p0, LX/Hrk;->g:Landroid/content/Context;

    invoke-direct {v2, v3}, LX/31Y;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v1

    const v2, 0x7f080036

    invoke-virtual {v1, v2, v0}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    invoke-virtual {v1}, LX/0ju;->b()LX/2EJ;

    .line 2513469
    :goto_2
    return-void

    .line 2513470
    :cond_0
    iget-object v0, p0, LX/Hrk;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HqL;

    .line 2513471
    if-eqz v0, :cond_1

    invoke-virtual {v0}, LX/HqL;->a()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, LX/HqL;->a()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    if-nez v1, :cond_4

    .line 2513472
    :cond_1
    sget-object v0, LX/AP5;->NONE:LX/AP5;

    .line 2513473
    :goto_3
    move-object v0, v0

    .line 2513474
    iget-object v1, p3, LX/HqM;->a:Lcom/facebook/composer/activity/ComposerFragment;

    invoke-static {v1, p1}, Lcom/facebook/composer/activity/ComposerFragment;->a$redex0(Lcom/facebook/composer/activity/ComposerFragment;Lcom/facebook/graphql/model/GraphQLPrivacyOption;)V

    .line 2513475
    iget-object v1, p3, LX/HqM;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v1, v1, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->u()Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;->a()LX/AP3;

    move-result-object v1

    sget-object v2, LX/2by;->INLINE_PRIVACY_SURVEY:LX/2by;

    .line 2513476
    iput-object v2, v1, LX/AP3;->e:LX/2by;

    .line 2513477
    move-object v1, v1

    .line 2513478
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->d()Ljava/lang/String;

    move-result-object v2

    .line 2513479
    iput-object v2, v1, LX/AP3;->c:Ljava/lang/String;

    .line 2513480
    move-object v1, v1

    .line 2513481
    iput-object v0, v1, LX/AP3;->d:LX/AP5;

    .line 2513482
    move-object v1, v1

    .line 2513483
    invoke-virtual {v1}, LX/AP3;->a()Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    move-result-object v2

    .line 2513484
    iget-object v1, p3, LX/HqM;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v1, v1, Lcom/facebook/composer/activity/ComposerFragment;->Q:LX/0jJ;

    sget-object v3, Lcom/facebook/composer/activity/ComposerFragment;->be:LX/0jK;

    invoke-virtual {v1, v3}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v1

    check-cast v1, LX/0jL;

    invoke-virtual {v1, v2}, LX/0jL;->a(Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jL;

    invoke-virtual {v1}, LX/0jL;->a()V

    .line 2513485
    iget-object v1, p3, LX/HqM;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v1, v1, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    sget-object v2, LX/5L2;->ON_PRIVACY_CHANGE_FROM_INLINE_PRIVACY_SURVEY:LX/5L2;

    invoke-virtual {v1, v2}, LX/HvN;->a(LX/5L2;)V

    .line 2513486
    goto :goto_2

    .line 2513487
    :cond_2
    const v1, 0x7f081488

    goto/16 :goto_1

    .line 2513488
    :pswitch_0
    const v3, 0x7f08148a

    goto/16 :goto_0

    .line 2513489
    :pswitch_1
    iget-boolean v3, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->b:Z

    move v3, v3

    .line 2513490
    if-eqz v3, :cond_3

    .line 2513491
    const v3, 0x7f08148a

    goto/16 :goto_0

    .line 2513492
    :cond_3
    const v3, 0x7f081489

    goto/16 :goto_0

    .line 2513493
    :cond_4
    invoke-virtual {v0}, LX/HqL;->a()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    invoke-virtual {v1, p1}, Lcom/facebook/privacy/model/PrivacyOptionsResult;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v1

    .line 2513494
    if-nez v1, :cond_5

    .line 2513495
    sget-object v0, LX/AP5;->NONE:LX/AP5;

    goto :goto_3

    .line 2513496
    :cond_5
    invoke-virtual {v0}, LX/HqL;->b()Z

    move-result v2

    invoke-virtual {v0}, LX/HqL;->a()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v0

    .line 2513497
    iget-boolean p0, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->b:Z

    move v0, p0

    .line 2513498
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->e()Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    move-result-object v1

    invoke-static {v2, v0, v1}, LX/Hrk;->a(ZZLcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;)LX/AP5;

    move-result-object v0

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static c(LX/Hrk;)LX/HqL;
    .locals 3

    .prologue
    .line 2513499
    iget-object v0, p0, LX/Hrk;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HqL;

    .line 2513500
    if-nez v0, :cond_0

    .line 2513501
    iget-object v0, p0, LX/Hrk;->h:LX/03V;

    const-string v1, "inline_privacy_survey_data_provider"

    const-string v2, "DataProvider is garbage collected. Double-check the caller to strongly reference the passed-in provider."

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2513502
    const/4 v0, 0x0

    .line 2513503
    :cond_0
    return-object v0
.end method

.method public static d(LX/Hrk;)LX/HqM;
    .locals 3

    .prologue
    .line 2513504
    iget-object v0, p0, LX/Hrk;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HqM;

    .line 2513505
    if-nez v0, :cond_0

    .line 2513506
    iget-object v0, p0, LX/Hrk;->h:LX/03V;

    const-string v1, "inline_privacy_survey_post_survey_actions"

    const-string v2, "PostSurveyActions is garbage collected. Double-check the caller to strongly reference the passed in callback."

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2513507
    const/4 v0, 0x0

    .line 2513508
    :cond_0
    return-object v0
.end method
