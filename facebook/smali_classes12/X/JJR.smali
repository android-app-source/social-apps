.class public LX/JJR;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/76V;


# instance fields
.field public a:LX/8tu;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/17W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/JJY;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/13A;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/3Rb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final f:Landroid/view/View;

.field private final g:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

.field private final h:Landroid/widget/TextView;

.field private final i:Landroid/widget/TextView;

.field public final j:Landroid/widget/TextView;

.field private final k:Landroid/widget/TextView;

.field public final l:Landroid/view/View;

.field private final m:Landroid/view/View;

.field private final n:Lcom/facebook/resources/ui/FbTextView;

.field private final o:Lcom/facebook/fbui/facepile/FacepileView;

.field public final p:Landroid/view/View;

.field public final q:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

.field public final r:Landroid/widget/TextView;

.field public final s:Landroid/widget/TextView;

.field public final t:Landroid/widget/TextView;

.field public u:LX/JJX;

.field private v:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

.field public w:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private x:Ljava/lang/Runnable;

.field private y:Z

.field private z:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 2679497
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2679498
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, LX/JJR;

    invoke-static {v0}, LX/8tu;->a(LX/0QB;)LX/8tu;

    move-result-object v3

    check-cast v3, LX/8tu;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v4

    check-cast v4, LX/17W;

    const-class v5, LX/JJY;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/JJY;

    const-class p1, LX/13A;

    invoke-interface {v0, p1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/13A;

    invoke-static {v0}, LX/3Rb;->a(LX/0QB;)LX/3Rb;

    move-result-object v0

    check-cast v0, LX/3Rb;

    iput-object v3, v2, LX/JJR;->a:LX/8tu;

    iput-object v4, v2, LX/JJR;->b:LX/17W;

    iput-object v5, v2, LX/JJR;->c:LX/JJY;

    iput-object p1, v2, LX/JJR;->d:LX/13A;

    iput-object v0, v2, LX/JJR;->e:LX/3Rb;

    .line 2679499
    const v0, 0x7f03094f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2679500
    const v0, 0x7f0d17ce

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/JJR;->f:Landroid/view/View;

    .line 2679501
    const v0, 0x7f0d17cf

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    iput-object v0, p0, LX/JJR;->g:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 2679502
    const v0, 0x7f0d17d0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/JJR;->h:Landroid/widget/TextView;

    .line 2679503
    const v0, 0x7f0d17d1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/JJR;->i:Landroid/widget/TextView;

    .line 2679504
    const v0, 0x7f0d17d3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/JJR;->j:Landroid/widget/TextView;

    .line 2679505
    const v0, 0x7f0d17d2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/JJR;->k:Landroid/widget/TextView;

    .line 2679506
    const v0, 0x7f0d17d4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/JJR;->m:Landroid/view/View;

    .line 2679507
    const v0, 0x7f0d17d5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/facepile/FacepileView;

    iput-object v0, p0, LX/JJR;->o:Lcom/facebook/fbui/facepile/FacepileView;

    .line 2679508
    const v0, 0x7f0d17d6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/JJR;->n:Lcom/facebook/resources/ui/FbTextView;

    .line 2679509
    invoke-virtual {p0}, LX/JJR;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00b6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/JJR;->z:I

    .line 2679510
    const v0, 0x7f0d17d7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/JJR;->p:Landroid/view/View;

    .line 2679511
    const v0, 0x7f0d17d8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    iput-object v0, p0, LX/JJR;->q:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 2679512
    const v0, 0x7f0d17d9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/JJR;->r:Landroid/widget/TextView;

    .line 2679513
    const v0, 0x7f0d17da

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/JJR;->s:Landroid/widget/TextView;

    .line 2679514
    const v0, 0x7f0d17db

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/JJR;->t:Landroid/widget/TextView;

    .line 2679515
    const v0, 0x7f0d17dc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/JJR;->l:Landroid/view/View;

    .line 2679516
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/JJR;->y:Z

    .line 2679517
    return-void
.end method

.method public static a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;Landroid/widget/TextView;)V
    .locals 1
    .param p0    # Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2679400
    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->title:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2679401
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2679402
    :goto_0
    return-void

    .line 2679403
    :cond_1
    iget-object v0, p0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->title:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2679404
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private setFacepileUrls(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2679493
    iget-object v1, p0, LX/JJR;->o:Lcom/facebook/fbui/facepile/FacepileView;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/fbui/facepile/FacepileView;->setVisibility(I)V

    .line 2679494
    iget-object v0, p0, LX/JJR;->o:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/facepile/FacepileView;->setFaceUrls(Ljava/util/List;)V

    .line 2679495
    return-void

    .line 2679496
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private setSecondaryActionButton(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;)V
    .locals 2

    .prologue
    .line 2679488
    iget-object v0, p0, LX/JJR;->k:Landroid/widget/TextView;

    invoke-static {p1, v0}, LX/JJR;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;Landroid/widget/TextView;)V

    .line 2679489
    iget-object v0, p0, LX/JJR;->k:Landroid/widget/TextView;

    iget-object v1, p0, LX/JJR;->u:LX/JJX;

    .line 2679490
    new-instance p0, LX/JJV;

    invoke-direct {p0, v1}, LX/JJV;-><init>(LX/JJX;)V

    move-object v1, p0

    .line 2679491
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2679492
    return-void
.end method

.method private setSocialContext(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 2679484
    iget-object v0, p0, LX/JJR;->n:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2679485
    iget-object v1, p0, LX/JJR;->n:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2679486
    return-void

    .line 2679487
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2679479
    iget-object v0, p0, LX/JJR;->x:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 2679480
    iget-object v0, p0, LX/JJR;->x:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 2679481
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/JJR;->y:Z

    .line 2679482
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, LX/JJR;->setVisibility(I)V

    .line 2679483
    return-void
.end method

.method public final a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Ljava/lang/String;Lcom/facebook/interstitial/manager/InterstitialTrigger;)V
    .locals 9

    .prologue
    const/4 v7, 0x0

    const/16 v6, 0x8

    const/4 v8, 0x3

    const/4 v2, 0x0

    .line 2679411
    iget-object v0, p0, LX/JJR;->v:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    if-ne v0, p1, :cond_1

    .line 2679412
    iget-boolean v0, p0, LX/JJR;->y:Z

    if-eqz v0, :cond_0

    .line 2679413
    invoke-virtual {p0, v6}, LX/JJR;->setVisibility(I)V

    .line 2679414
    :cond_0
    :goto_0
    return-void

    .line 2679415
    :cond_1
    iput-object p1, p0, LX/JJR;->v:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    .line 2679416
    iget-object v0, p0, LX/JJR;->v:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    invoke-virtual {v0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->c()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    move-result-object v1

    .line 2679417
    iget-object v0, p0, LX/JJR;->v:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->customRenderParams:LX/0P1;

    iput-object v0, p0, LX/JJR;->w:LX/0P1;

    .line 2679418
    if-eqz v1, :cond_2

    iget-object v0, p0, LX/JJR;->w:LX/0P1;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/JJR;->w:LX/0P1;

    const-string v3, "instagram_user_name"

    invoke-virtual {v0, v3}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/JJR;->w:LX/0P1;

    const-string v3, "confirmation_page_title"

    invoke-virtual {v0, v3}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2679419
    :cond_2
    invoke-virtual {p0}, LX/JJR;->a()V

    goto :goto_0

    .line 2679420
    :cond_3
    iget-object v0, p0, LX/JJR;->d:LX/13A;

    iget-object v3, p0, LX/JJR;->v:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    invoke-virtual {v0, v3, p2, v1, p3}, LX/13A;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Ljava/lang/String;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/78A;

    move-result-object v0

    .line 2679421
    iget-object v3, p0, LX/JJR;->c:LX/JJY;

    iget-object v4, p0, LX/JJR;->x:Ljava/lang/Runnable;

    iget-object v5, p0, LX/JJR;->v:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    invoke-virtual {v3, p0, v4, v0, v5}, LX/JJY;->a(LX/JJR;Ljava/lang/Runnable;LX/78A;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;)LX/JJX;

    move-result-object v0

    iput-object v0, p0, LX/JJR;->u:LX/JJX;

    .line 2679422
    sget-object v0, LX/76S;->ANY:LX/76S;

    invoke-static {v1, v0}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->b(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;LX/76S;)Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    move-result-object v0

    .line 2679423
    if-eqz v0, :cond_8

    .line 2679424
    iget-object v3, p0, LX/JJR;->g:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;->uri:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 2679425
    iget-object v0, p0, LX/JJR;->g:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    .line 2679426
    :goto_1
    iget-object v0, p0, LX/JJR;->h:Landroid/widget/TextView;

    iget-object v3, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->title:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2679427
    iget-object v0, p0, LX/JJR;->i:Landroid/widget/TextView;

    iget-object v3, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->content:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2679428
    iget-object v0, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->primaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    .line 2679429
    iget-object v3, p0, LX/JJR;->j:Landroid/widget/TextView;

    invoke-static {v0, v3}, LX/JJR;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;Landroid/widget/TextView;)V

    .line 2679430
    iget-object v4, p0, LX/JJR;->j:Landroid/widget/TextView;

    iget-object v5, p0, LX/JJR;->u:LX/JJX;

    iget-object v3, p0, LX/JJR;->w:LX/0P1;

    const-string p1, "instagram_user_name"

    invoke-virtual {v3, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2679431
    new-instance p1, LX/JJU;

    invoke-direct {p1, v5, v3}, LX/JJU;-><init>(LX/JJX;Ljava/lang/String;)V

    move-object v3, p1

    .line 2679432
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2679433
    iget-object v0, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->secondaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    invoke-direct {p0, v0}, LX/JJR;->setSecondaryActionButton(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;)V

    .line 2679434
    invoke-direct {p0, v7}, LX/JJR;->setFacepileUrls(Ljava/util/List;)V

    .line 2679435
    iget-object v0, p0, LX/JJR;->m:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2679436
    iget-object v0, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->socialContext:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;

    if-nez v0, :cond_9

    .line 2679437
    invoke-direct {p0, v7}, LX/JJR;->setSocialContext(Ljava/lang/CharSequence;)V

    .line 2679438
    iget-object v0, p0, LX/JJR;->m:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 2679439
    :cond_4
    :goto_2
    iget-object v0, p0, LX/JJR;->w:LX/0P1;

    const-string v1, "confirmation_page_icon"

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, LX/JJR;->w:LX/0P1;

    const-string v1, "confirmation_page_icon"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 2679440
    iget-object v1, p0, LX/JJR;->q:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    iget-object v0, p0, LX/JJR;->w:LX/0P1;

    const-string v3, "confirmation_page_icon"

    invoke-virtual {v0, v3}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 2679441
    iget-object v0, p0, LX/JJR;->q:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    .line 2679442
    :goto_3
    iget-object v0, p0, LX/JJR;->w:LX/0P1;

    const-string v1, "confirmation_page_title"

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/JJR;->w:LX/0P1;

    const-string v1, "confirmation_page_title"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 2679443
    iget-object v1, p0, LX/JJR;->r:Landroid/widget/TextView;

    iget-object v0, p0, LX/JJR;->w:LX/0P1;

    const-string v3, "confirmation_page_title"

    invoke-virtual {v0, v3}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2679444
    :cond_5
    iget-object v0, p0, LX/JJR;->w:LX/0P1;

    const-string v1, "confirmation_page_content"

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, LX/JJR;->w:LX/0P1;

    const-string v1, "confirmation_page_content"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 2679445
    iget-object v1, p0, LX/JJR;->s:Landroid/widget/TextView;

    iget-object v0, p0, LX/JJR;->w:LX/0P1;

    const-string v3, "confirmation_page_content"

    invoke-virtual {v0, v3}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v6, 0x0

    .line 2679446
    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    .line 2679447
    invoke-interface {v4}, Landroid/text/Spanned;->length()I

    move-result v3

    const-class v5, Landroid/text/style/URLSpan;

    invoke-interface {v4, v6, v3, v5}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Landroid/text/style/URLSpan;

    .line 2679448
    array-length v5, v3

    if-eqz v5, :cond_c

    .line 2679449
    aget-object v3, v3, v6

    .line 2679450
    new-instance v5, Landroid/text/SpannableString;

    invoke-direct {v5, v4}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2679451
    invoke-interface {v4, v3}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v6

    .line 2679452
    invoke-interface {v4, v3}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v7

    .line 2679453
    invoke-interface {v4, v3}, Landroid/text/Spanned;->getSpanFlags(Ljava/lang/Object;)I

    move-result v8

    .line 2679454
    new-instance v0, LX/JJQ;

    invoke-direct {v0, p0, v3}, LX/JJQ;-><init>(LX/JJR;Landroid/text/style/URLSpan;)V

    .line 2679455
    invoke-virtual {v5, v0, v6, v7, v8}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 2679456
    invoke-virtual {v5, v3}, Landroid/text/SpannableString;->removeSpan(Ljava/lang/Object;)V

    .line 2679457
    move-object v3, v5

    .line 2679458
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2679459
    iget-object v3, p0, LX/JJR;->a:LX/8tu;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2679460
    :cond_6
    :goto_4
    iget-object v0, p0, LX/JJR;->w:LX/0P1;

    const-string v1, "confirmation_page_button"

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2679461
    iget-object v1, p0, LX/JJR;->t:Landroid/widget/TextView;

    iget-object v0, p0, LX/JJR;->w:LX/0P1;

    const-string v3, "confirmation_page_button"

    invoke-virtual {v0, v3}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2679462
    iget-object v0, p0, LX/JJR;->t:Landroid/widget/TextView;

    iget-object v1, p0, LX/JJR;->u:LX/JJX;

    .line 2679463
    new-instance v3, LX/JJW;

    invoke-direct {v3, v1}, LX/JJW;-><init>(LX/JJX;)V

    move-object v1, v3

    .line 2679464
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2679465
    :cond_7
    iget-object v0, p0, LX/JJR;->u:LX/JJX;

    invoke-virtual {v0}, LX/76U;->a()V

    .line 2679466
    iput-boolean v2, p0, LX/JJR;->y:Z

    .line 2679467
    invoke-virtual {p0, v2}, LX/JJR;->setVisibility(I)V

    goto/16 :goto_0

    .line 2679468
    :cond_8
    iget-object v0, p0, LX/JJR;->g:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    goto/16 :goto_1

    .line 2679469
    :cond_9
    iget-object v0, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->socialContext:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;->text:Ljava/lang/String;

    invoke-direct {p0, v0}, LX/JJR;->setSocialContext(Ljava/lang/CharSequence;)V

    .line 2679470
    iget-object v0, v1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->socialContext:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;

    iget-object v3, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;->friendIds:LX/0Px;

    .line 2679471
    invoke-static {v3}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2679472
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v8}, Ljava/util/ArrayList;-><init>(I)V

    move v1, v2

    .line 2679473
    :goto_5
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_a

    if-ge v1, v8, :cond_a

    .line 2679474
    iget-object v5, p0, LX/JJR;->e:LX/3Rb;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget v6, p0, LX/JJR;->z:I

    iget v7, p0, LX/JJR;->z:I

    invoke-virtual {v5, v0, v6, v7}, LX/3Rb;->a(Ljava/lang/String;II)Landroid/net/Uri;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2679475
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 2679476
    :cond_a
    invoke-direct {p0, v4}, LX/JJR;->setFacepileUrls(Ljava/util/List;)V

    goto/16 :goto_2

    .line 2679477
    :cond_b
    iget-object v0, p0, LX/JJR;->q:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    goto/16 :goto_3

    .line 2679478
    :cond_c
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4
.end method

.method public final onMeasure(II)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2679407
    iget-boolean v0, p0, LX/JJR;->y:Z

    if-eqz v0, :cond_0

    .line 2679408
    invoke-virtual {p0, v1, v1}, LX/JJR;->setMeasuredDimension(II)V

    .line 2679409
    :goto_0
    return-void

    .line 2679410
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;->onMeasure(II)V

    goto :goto_0
.end method

.method public setOnDismiss(Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 2679405
    iput-object p1, p0, LX/JJR;->x:Ljava/lang/Runnable;

    .line 2679406
    return-void
.end method
