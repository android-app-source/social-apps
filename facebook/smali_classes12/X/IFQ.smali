.class public final enum LX/IFQ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IFQ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IFQ;

.field public static final enum INVITED:LX/IFQ;

.field public static final enum INVITING:LX/IFQ;

.field public static final enum NOT_INVITED:LX/IFQ;

.field public static final enum UNINVITING:LX/IFQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2554113
    new-instance v0, LX/IFQ;

    const-string v1, "NOT_INVITED"

    invoke-direct {v0, v1, v2}, LX/IFQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IFQ;->NOT_INVITED:LX/IFQ;

    .line 2554114
    new-instance v0, LX/IFQ;

    const-string v1, "UNINVITING"

    invoke-direct {v0, v1, v3}, LX/IFQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IFQ;->UNINVITING:LX/IFQ;

    .line 2554115
    new-instance v0, LX/IFQ;

    const-string v1, "INVITING"

    invoke-direct {v0, v1, v4}, LX/IFQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IFQ;->INVITING:LX/IFQ;

    .line 2554116
    new-instance v0, LX/IFQ;

    const-string v1, "INVITED"

    invoke-direct {v0, v1, v5}, LX/IFQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IFQ;->INVITED:LX/IFQ;

    .line 2554117
    const/4 v0, 0x4

    new-array v0, v0, [LX/IFQ;

    sget-object v1, LX/IFQ;->NOT_INVITED:LX/IFQ;

    aput-object v1, v0, v2

    sget-object v1, LX/IFQ;->UNINVITING:LX/IFQ;

    aput-object v1, v0, v3

    sget-object v1, LX/IFQ;->INVITING:LX/IFQ;

    aput-object v1, v0, v4

    sget-object v1, LX/IFQ;->INVITED:LX/IFQ;

    aput-object v1, v0, v5

    sput-object v0, LX/IFQ;->$VALUES:[LX/IFQ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2554120
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IFQ;
    .locals 1

    .prologue
    .line 2554119
    const-class v0, LX/IFQ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IFQ;

    return-object v0
.end method

.method public static values()[LX/IFQ;
    .locals 1

    .prologue
    .line 2554118
    sget-object v0, LX/IFQ;->$VALUES:[LX/IFQ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IFQ;

    return-object v0
.end method
