.class public LX/JS5;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/3N2;

.field public final b:LX/JRz;

.field public final c:Lcom/facebook/content/SecureContextHelper;

.field public final d:LX/17T;

.field public final e:LX/17d;


# direct methods
.method public constructor <init>(LX/3N2;LX/JRz;Lcom/facebook/content/SecureContextHelper;LX/17T;LX/17d;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2694630
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2694631
    iput-object p1, p0, LX/JS5;->a:LX/3N2;

    .line 2694632
    iput-object p2, p0, LX/JS5;->b:LX/JRz;

    .line 2694633
    iput-object p3, p0, LX/JS5;->c:Lcom/facebook/content/SecureContextHelper;

    .line 2694634
    iput-object p4, p0, LX/JS5;->d:LX/17T;

    .line 2694635
    iput-object p5, p0, LX/JS5;->e:LX/17d;

    .line 2694636
    return-void
.end method

.method private static a(LX/JS5;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 2694637
    iget-object v0, p0, LX/JS5;->a:LX/3N2;

    .line 2694638
    :try_start_0
    iget-object v1, v0, LX/3N2;->b:Landroid/content/pm/PackageManager;

    const/4 p0, 0x0

    invoke-virtual {v1, p1, p0}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 2694639
    iget-object p0, v0, LX/3N2;->b:Landroid/content/pm/PackageManager;

    invoke-virtual {p0, v1}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2694640
    :goto_0
    move-object v0, v1

    .line 2694641
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :catch_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static b(Ljava/lang/String;)Landroid/net/Uri;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2694642
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2694643
    if-nez v0, :cond_0

    move-object v0, v1

    .line 2694644
    :goto_0
    return-object v0

    .line 2694645
    :cond_0
    const-string v2, "appsite_data"

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2694646
    const-string v3, "app_id"

    invoke-virtual {v0, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2694647
    if-eqz v0, :cond_1

    const-string v3, "799889663393876"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "823778694338306"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    move-object v0, v1

    .line 2694648
    goto :goto_0

    .line 2694649
    :cond_2
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 2694650
    :try_start_0
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v0

    const-class v3, LX/JS4;

    invoke-virtual {v0, v2, v3}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JS4;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2694651
    :goto_1
    if-eqz v0, :cond_7

    iget-object v2, v0, LX/JS4;->appSites:Ljava/util/List;

    if-eqz v2, :cond_7

    iget-object v2, v0, LX/JS4;->appSites:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_7

    .line 2694652
    iget-object v0, v0, LX/JS4;->appSites:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JS3;

    .line 2694653
    iget-object v3, v0, LX/JS3;->appSiteUrl:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 2694654
    iget-object v0, v0, LX/JS3;->appSiteUrl:Ljava/lang/String;

    .line 2694655
    :goto_2
    if-nez v0, :cond_4

    move-object v0, v1

    .line 2694656
    goto :goto_0

    .line 2694657
    :catch_0
    move-object v0, v1

    goto :goto_0

    .line 2694658
    :cond_4
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v2, "url"

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2694659
    if-nez v0, :cond_5

    move-object v0, v1

    .line 2694660
    goto :goto_0

    .line 2694661
    :cond_5
    new-instance v1, Landroid/net/Uri$Builder;

    invoke-direct {v1}, Landroid/net/Uri$Builder;-><init>()V

    .line 2694662
    const-string v2, "https"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2694663
    const-string v2, "music.dmkt-sp.jp"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2694664
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v2, "fb_music"

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2694665
    if-eqz v0, :cond_6

    .line 2694666
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "/song/S"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2694667
    :cond_6
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    goto/16 :goto_0

    :cond_7
    move-object v0, v1

    goto :goto_2

    :cond_8
    move-object v0, v1

    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/162;)V
    .locals 13

    .prologue
    .line 2694668
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2694669
    new-instance v11, LX/0ju;

    invoke-direct {v11, p1}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 2694670
    invoke-static {p0, p2}, LX/JS5;->a(LX/JS5;Ljava/lang/String;)Z

    move-result v2

    .line 2694671
    invoke-static/range {p3 .. p3}, LX/JS5;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    .line 2694672
    if-eqz v2, :cond_0

    .line 2694673
    const v1, 0x7f081852

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2694674
    :goto_0
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p4, v1, v3

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2694675
    const/4 v1, 0x1

    new-array v12, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object v0, v12, v1

    new-instance v0, LX/JS2;

    move-object v1, p0

    move-object v3, p1

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object v10, p2

    invoke-direct/range {v0 .. v10}, LX/JS2;-><init>(LX/JS5;ZLandroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/162;Landroid/net/Uri;Ljava/lang/String;)V

    invoke-virtual {v11, v12, v0}, LX/0ju;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2694676
    invoke-virtual {v11}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2694677
    return-void

    .line 2694678
    :cond_0
    if-eqz v9, :cond_1

    .line 2694679
    const v1, 0x7f081854

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2694680
    :cond_1
    const v1, 0x7f081853

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
