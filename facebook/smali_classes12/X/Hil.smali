.class public final enum LX/Hil;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Hil;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Hil;

.field public static final enum CONVERSATION:LX/Hil;

.field public static final enum RECENT:LX/Hil;


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2497914
    new-instance v0, LX/Hil;

    const-string v1, "RECENT"

    const-string v2, "recent"

    invoke-direct {v0, v1, v3, v2}, LX/Hil;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hil;->RECENT:LX/Hil;

    .line 2497915
    new-instance v0, LX/Hil;

    const-string v1, "CONVERSATION"

    const-string v2, "conversation"

    invoke-direct {v0, v1, v4, v2}, LX/Hil;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Hil;->CONVERSATION:LX/Hil;

    .line 2497916
    const/4 v0, 0x2

    new-array v0, v0, [LX/Hil;

    sget-object v1, LX/Hil;->RECENT:LX/Hil;

    aput-object v1, v0, v3

    sget-object v1, LX/Hil;->CONVERSATION:LX/Hil;

    aput-object v1, v0, v4

    sput-object v0, LX/Hil;->$VALUES:[LX/Hil;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2497917
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2497918
    iput-object p3, p0, LX/Hil;->name:Ljava/lang/String;

    .line 2497919
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Hil;
    .locals 1

    .prologue
    .line 2497913
    const-class v0, LX/Hil;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Hil;

    return-object v0
.end method

.method public static values()[LX/Hil;
    .locals 1

    .prologue
    .line 2497912
    sget-object v0, LX/Hil;->$VALUES:[LX/Hil;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Hil;

    return-object v0
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2497911
    iget-object v0, p0, LX/Hil;->name:Ljava/lang/String;

    return-object v0
.end method
