.class public LX/J0k;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/J0k;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2637771
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2637772
    return-void
.end method

.method public static a(LX/0QB;)LX/J0k;
    .locals 3

    .prologue
    .line 2637773
    sget-object v0, LX/J0k;->a:LX/J0k;

    if-nez v0, :cond_1

    .line 2637774
    const-class v1, LX/J0k;

    monitor-enter v1

    .line 2637775
    :try_start_0
    sget-object v0, LX/J0k;->a:LX/J0k;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2637776
    if-eqz v2, :cond_0

    .line 2637777
    :try_start_1
    new-instance v0, LX/J0k;

    invoke-direct {v0}, LX/J0k;-><init>()V

    .line 2637778
    move-object v0, v0

    .line 2637779
    sput-object v0, LX/J0k;->a:LX/J0k;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2637780
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2637781
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2637782
    :cond_1
    sget-object v0, LX/J0k;->a:LX/J0k;

    return-object v0

    .line 2637783
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2637784
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 2637754
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 2637755
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "q"

    const-string v3, "viewer() {primary_email}"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2637756
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "fetchPrimaryEmailAddress"

    .line 2637757
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 2637758
    move-object v1, v1

    .line 2637759
    const-string v2, "GET"

    .line 2637760
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 2637761
    move-object v1, v1

    .line 2637762
    const-string v2, "graphql"

    .line 2637763
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 2637764
    move-object v1, v1

    .line 2637765
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 2637766
    move-object v0, v1

    .line 2637767
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2637768
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2637769
    move-object v0, v0

    .line 2637770
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2637749
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2637750
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const-string v1, "viewer"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 2637751
    if-nez v0, :cond_0

    .line 2637752
    const-string v0, ""

    .line 2637753
    :goto_0
    return-object v0

    :cond_0
    const-string v1, "primary_email"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    const-string v1, ""

    invoke-static {v0, v1}, LX/16N;->a(LX/0lF;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
