.class public LX/HJu;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/HJw;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/HJu",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/HJw;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2453484
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2453485
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/HJu;->b:LX/0Zi;

    .line 2453486
    iput-object p1, p0, LX/HJu;->a:LX/0Ot;

    .line 2453487
    return-void
.end method

.method public static a(LX/0QB;)LX/HJu;
    .locals 4

    .prologue
    .line 2453473
    const-class v1, LX/HJu;

    monitor-enter v1

    .line 2453474
    :try_start_0
    sget-object v0, LX/HJu;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2453475
    sput-object v2, LX/HJu;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2453476
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2453477
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2453478
    new-instance v3, LX/HJu;

    const/16 p0, 0x2bba

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/HJu;-><init>(LX/0Ot;)V

    .line 2453479
    move-object v0, v3

    .line 2453480
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2453481
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/HJu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2453482
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2453483
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private b(LX/1X1;)V
    .locals 10

    .prologue
    .line 2453457
    check-cast p1, LX/HJt;

    .line 2453458
    iget-object v0, p0, LX/HJu;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HJw;

    iget-object v1, p1, LX/HJt;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v2, p1, LX/HJt;->c:LX/2km;

    .line 2453459
    iget-object v3, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v3, v3

    .line 2453460
    invoke-interface {v3}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v3

    if-nez v3, :cond_0

    .line 2453461
    :goto_0
    return-void

    .line 2453462
    :cond_0
    iget-object v3, v0, LX/HJw;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/E1f;

    .line 2453463
    iget-object v4, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v4, v4

    .line 2453464
    invoke-interface {v4}, LX/9uc;->k()Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLModels$ReactionStoryAttachmentActionFragmentModel;

    move-result-object v4

    move-object v5, v2

    check-cast v5, LX/1Pn;

    invoke-interface {v5}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v5

    const/4 v6, 0x0

    move-object v7, v2

    check-cast v7, LX/2kp;

    invoke-interface {v7}, LX/2kp;->t()LX/2jY;

    move-result-object v7

    .line 2453465
    iget-object v8, v7, LX/2jY;->a:Ljava/lang/String;

    move-object v7, v8

    .line 2453466
    move-object v8, v2

    check-cast v8, LX/2kp;

    invoke-interface {v8}, LX/2kp;->t()LX/2jY;

    move-result-object v8

    .line 2453467
    iget-object v9, v8, LX/2jY;->b:Ljava/lang/String;

    move-object v8, v9

    .line 2453468
    iget-object v9, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v9, v9

    .line 2453469
    invoke-virtual/range {v3 .. v9}, LX/E1f;->a(LX/9rk;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v3

    .line 2453470
    iget-object v4, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v4, v4

    .line 2453471
    iget-object v5, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v5, v5

    .line 2453472
    invoke-interface {v2, v4, v5, v3}, LX/2km;->a(Ljava/lang/String;Ljava/lang/String;LX/Cfl;)V

    goto :goto_0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2453456
    const v0, 0x5af2901f

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 10

    .prologue
    .line 2453436
    check-cast p2, LX/HJt;

    .line 2453437
    iget-object v0, p0, LX/HJu;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-boolean v0, p2, LX/HJt;->a:Z

    iget-object v1, p2, LX/HJt;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    const/4 v6, 0x0

    const/4 v3, 0x1

    const/4 p2, 0x7

    const/4 v4, 0x0

    .line 2453438
    iget-object v2, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v2, v2

    .line 2453439
    invoke-interface {v2}, LX/9uc;->cT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;

    move-result-object v2

    if-eqz v2, :cond_1

    move v2, v3

    .line 2453440
    :goto_0
    iget-object v5, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v5, v5

    .line 2453441
    invoke-interface {v5}, LX/9uc;->ap()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    move-result-object v5

    invoke-static {v5}, LX/HJw;->a(Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;)I

    move-result v7

    .line 2453442
    iget-object v5, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v5, v5

    .line 2453443
    invoke-interface {v5}, LX/9uc;->bA()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$MessageModel;->a()Ljava/lang/String;

    move-result-object v8

    .line 2453444
    if-eqz v2, :cond_2

    .line 2453445
    iget-object v5, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v5, v5

    .line 2453446
    invoke-interface {v5}, LX/9uc;->cT()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentFieldsModel$SubMessageModel;->a()Ljava/lang/String;

    move-result-object v5

    .line 2453447
    :goto_1
    if-eqz v2, :cond_0

    .line 2453448
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const p0, 0x7f08184a

    invoke-virtual {v9, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 2453449
    :cond_0
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v9

    invoke-interface {v9, v3}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    const v9, 0x7f0213ab

    invoke-interface {v3, v9}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v3

    .line 2453450
    const v9, 0x5af2901f

    const/4 p0, 0x0

    invoke-static {p1, v9, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v9

    move-object v9, v9

    .line 2453451
    invoke-interface {v3, v9}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v9

    if-eqz v0, :cond_3

    move v3, v4

    :goto_2
    invoke-interface {v9, v3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    const v9, 0x7f0b0d5f

    invoke-interface {v3, p2, v9}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v3

    const/4 v9, 0x6

    const p0, 0x7f0b0d5e

    invoke-interface {v3, v9, p0}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v3

    if-nez v2, :cond_4

    move-object v2, v6

    :goto_3
    invoke-interface {v3, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-static {p1, v8, v7}, LX/HJw;->a(LX/1De;Ljava/lang/String;I)LX/1Di;

    move-result-object v5

    if-eqz v0, :cond_6

    const v2, 0x7f0b0d60

    :goto_4
    invoke-interface {v5, p2, v2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v5

    if-eqz v0, :cond_7

    const v2, 0x7f0b0d60

    :goto_5
    invoke-interface {v5, v4, v2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    invoke-interface {v3, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2453452
    return-object v0

    :cond_1
    move v2, v4

    .line 2453453
    goto/16 :goto_0

    :cond_2
    move-object v5, v6

    .line 2453454
    goto :goto_1

    .line 2453455
    :cond_3
    const/4 v3, 0x2

    goto :goto_2

    :cond_4
    const v2, 0x7f0a00ba

    invoke-static {p1, v5, v2}, LX/HJw;->a(LX/1De;Ljava/lang/String;I)LX/1Di;

    move-result-object v5

    if-eqz v0, :cond_5

    const v2, 0x7f0b0d60

    :goto_6
    invoke-interface {v5, p2, v2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    goto :goto_3

    :cond_5
    const v2, 0x7f0b0d5f

    goto :goto_6

    :cond_6
    const v2, 0x7f0b0d5f

    goto :goto_4

    :cond_7
    const v2, 0x7f0b0d6a

    goto :goto_5
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2453423
    invoke-static {}, LX/1dS;->b()V

    .line 2453424
    iget v0, p1, LX/1dQ;->b:I

    .line 2453425
    packed-switch v0, :pswitch_data_0

    .line 2453426
    :goto_0
    return-object v1

    .line 2453427
    :pswitch_0
    iget-object v0, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0}, LX/HJu;->b(LX/1X1;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x5af2901f
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/HJs;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/HJu",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2453428
    new-instance v1, LX/HJt;

    invoke-direct {v1, p0}, LX/HJt;-><init>(LX/HJu;)V

    .line 2453429
    iget-object v2, p0, LX/HJu;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/HJs;

    .line 2453430
    if-nez v2, :cond_0

    .line 2453431
    new-instance v2, LX/HJs;

    invoke-direct {v2, p0}, LX/HJs;-><init>(LX/HJu;)V

    .line 2453432
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/HJs;->a$redex0(LX/HJs;LX/1De;IILX/HJt;)V

    .line 2453433
    move-object v1, v2

    .line 2453434
    move-object v0, v1

    .line 2453435
    return-object v0
.end method
