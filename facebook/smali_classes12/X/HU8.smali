.class public final LX/HU8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Landroid/net/Uri;

.field public final synthetic b:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 2471603
    iput-object p1, p0, LX/HU8;->b:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;

    iput-object p2, p0, LX/HU8;->a:Landroid/net/Uri;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x3a7eb803

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2471604
    iget-object v0, p0, LX/HU8;->b:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->h:LX/HTY;

    .line 2471605
    const-string v2, "pages_endorsements_social_context_click"

    invoke-static {v0, v2}, LX/HTY;->b(LX/HTY;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 2471606
    iget-object v3, v0, LX/HTY;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2471607
    new-instance v2, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2471608
    iget-object v0, p0, LX/HU8;->a:Landroid/net/Uri;

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2471609
    iget-object v0, p0, LX/HU8;->b:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/HU8;->b:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalEndorsementsFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2471610
    const v0, -0x1f245dad

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
