.class public LX/JMP;
.super LX/4cO;
.source ""


# instance fields
.field private final b:Landroid/net/Uri;

.field private final c:Landroid/content/ContentResolver;

.field private final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2683905
    invoke-direct {p0, p3}, LX/4cO;-><init>(Ljava/lang/String;)V

    .line 2683906
    iput-object p2, p0, LX/JMP;->b:Landroid/net/Uri;

    .line 2683907
    iput-object p1, p0, LX/JMP;->c:Landroid/content/ContentResolver;

    .line 2683908
    iput-object p4, p0, LX/JMP;->d:Ljava/lang/String;

    .line 2683909
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2683910
    iget-object v0, p0, LX/JMP;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/io/OutputStream;)V
    .locals 2

    .prologue
    .line 2683911
    iget-object v0, p0, LX/JMP;->c:Landroid/content/ContentResolver;

    iget-object v1, p0, LX/JMP;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    .line 2683912
    :try_start_0
    invoke-static {v0, p1}, LX/0hW;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2683913
    invoke-static {v0}, LX/1md;->a(Ljava/io/InputStream;)V

    .line 2683914
    return-void

    .line 2683915
    :catchall_0
    move-exception v1

    invoke-static {v0}, LX/1md;->a(Ljava/io/InputStream;)V

    throw v1
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2683916
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2683917
    const-string v0, "binary"

    return-object v0
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 2683918
    const-wide/16 v0, -0x1

    return-wide v0
.end method
