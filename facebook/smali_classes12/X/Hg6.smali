.class public final LX/Hg6;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Hg7;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/Hfw;

.field public b:LX/Hfg;

.field public final synthetic c:LX/Hg7;


# direct methods
.method public constructor <init>(LX/Hg7;)V
    .locals 1

    .prologue
    .line 2492689
    iput-object p1, p0, LX/Hg6;->c:LX/Hg7;

    .line 2492690
    move-object v0, p1

    .line 2492691
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2492692
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2492693
    const-string v0, "WorkGroupsTabsListHeaderComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2492694
    if-ne p0, p1, :cond_1

    .line 2492695
    :cond_0
    :goto_0
    return v0

    .line 2492696
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2492697
    goto :goto_0

    .line 2492698
    :cond_3
    check-cast p1, LX/Hg6;

    .line 2492699
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2492700
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2492701
    if-eq v2, v3, :cond_0

    .line 2492702
    iget-object v2, p0, LX/Hg6;->a:LX/Hfw;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Hg6;->a:LX/Hfw;

    iget-object v3, p1, LX/Hg6;->a:LX/Hfw;

    invoke-virtual {v2, v3}, LX/Hfw;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2492703
    goto :goto_0

    .line 2492704
    :cond_5
    iget-object v2, p1, LX/Hg6;->a:LX/Hfw;

    if-nez v2, :cond_4

    .line 2492705
    :cond_6
    iget-object v2, p0, LX/Hg6;->b:LX/Hfg;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/Hg6;->b:LX/Hfg;

    iget-object v3, p1, LX/Hg6;->b:LX/Hfg;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2492706
    goto :goto_0

    .line 2492707
    :cond_7
    iget-object v2, p1, LX/Hg6;->b:LX/Hfg;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
