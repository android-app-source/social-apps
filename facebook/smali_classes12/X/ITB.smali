.class public LX/ITB;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/ITA;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/ITK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/8ht;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/DPA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/3my;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/88k;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

.field public j:LX/ITJ;

.field public k:LX/IT7;

.field public l:Z

.field public m:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

.field private n:Lcom/facebook/search/api/GraphSearchQuery;

.field public o:LX/DPp;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 10

    .prologue
    .line 2579036
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2579037
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, LX/ITB;

    const/16 v3, 0x259

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const-class v4, LX/ITK;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/ITK;

    invoke-static {v0}, LX/8ht;->a(LX/0QB;)LX/8ht;

    move-result-object v5

    check-cast v5, LX/8ht;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v7

    check-cast v7, LX/0Uh;

    invoke-static {v0}, LX/DPB;->b(LX/0QB;)LX/DPB;

    move-result-object v8

    check-cast v8, LX/DPA;

    invoke-static {v0}, LX/3my;->b(LX/0QB;)LX/3my;

    move-result-object v9

    check-cast v9, LX/3my;

    invoke-static {v0}, LX/88k;->b(LX/0QB;)LX/88k;

    move-result-object v0

    check-cast v0, LX/88k;

    iput-object v3, v2, LX/ITB;->a:LX/0Ot;

    iput-object v4, v2, LX/ITB;->b:LX/ITK;

    iput-object v5, v2, LX/ITB;->c:LX/8ht;

    iput-object v6, v2, LX/ITB;->d:LX/0ad;

    iput-object v7, v2, LX/ITB;->e:LX/0Uh;

    iput-object v8, v2, LX/ITB;->f:LX/DPA;

    iput-object v9, v2, LX/ITB;->g:LX/3my;

    iput-object v0, v2, LX/ITB;->h:LX/88k;

    .line 2579038
    const/4 v2, 0x0

    .line 2579039
    iget-object v0, p0, LX/ITB;->d:LX/0ad;

    sget-short v1, LX/88j;->k:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 2579040
    if-eqz v0, :cond_0

    new-instance v0, LX/IT9;

    invoke-direct {v0}, LX/IT9;-><init>()V

    .line 2579041
    :goto_0
    invoke-virtual {p0}, LX/ITB;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0}, LX/IT7;->a()I

    move-result v2

    invoke-static {v1, v2, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2579042
    const v1, 0x7f0d1589

    invoke-virtual {p0, v1}, LX/ITB;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 2579043
    invoke-interface {v0, v1}, LX/IT7;->a(Landroid/view/View;)V

    .line 2579044
    move-object v0, v0

    .line 2579045
    iput-object v0, p0, LX/ITB;->k:LX/IT7;

    .line 2579046
    iget-object v0, p0, LX/ITB;->k:LX/IT7;

    new-instance v1, LX/IT5;

    invoke-direct {v1, p0, p1}, LX/IT5;-><init>(LX/ITB;Landroid/content/Context;)V

    invoke-interface {v0, v1}, LX/IT7;->a(LX/IT4;)V

    .line 2579047
    return-void

    .line 2579048
    :cond_0
    new-instance v0, LX/IT8;

    invoke-direct {v0}, LX/IT8;-><init>()V

    goto :goto_0
.end method

.method public static b(LX/ITB;)Z
    .locals 2

    .prologue
    .line 2579049
    iget-object v0, p0, LX/ITB;->i:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ITB;->i:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ITB;->i:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/ITB;->h:LX/88k;

    iget-object v1, p0, LX/ITB;->i:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->K()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/88k;->a(Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getSearchQuery(LX/ITB;)Lcom/facebook/search/api/GraphSearchQuery;
    .locals 6

    .prologue
    .line 2579050
    iget-object v0, p0, LX/ITB;->n:Lcom/facebook/search/api/GraphSearchQuery;

    if-nez v0, :cond_0

    .line 2579051
    iget-object v0, p0, LX/ITB;->i:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    iget-object v1, p0, LX/ITB;->i:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->l()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    iget-object v3, p0, LX/ITB;->c:LX/8ht;

    iget-object v4, p0, LX/ITB;->e:LX/0Uh;

    iget-object v5, p0, LX/ITB;->d:LX/0ad;

    invoke-static/range {v0 .. v5}, LX/DOI;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;Ljava/lang/String;ZLX/8ht;LX/0Uh;LX/0ad;)Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v0

    iput-object v0, p0, LX/ITB;->n:Lcom/facebook/search/api/GraphSearchQuery;

    .line 2579052
    :cond_0
    iget-object v0, p0, LX/ITB;->n:Lcom/facebook/search/api/GraphSearchQuery;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2579053
    iget-object v0, p0, LX/ITB;->j:LX/ITJ;

    if-eqz v0, :cond_0

    .line 2579054
    iget-object v0, p0, LX/ITB;->j:LX/ITJ;

    invoke-virtual {v0}, LX/ITJ;->a()V

    .line 2579055
    :cond_0
    return-void
.end method
