.class public final LX/HRy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:J

.field public final synthetic b:LX/0am;

.field public final synthetic c:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;JLX/0am;)V
    .locals 0

    .prologue
    .line 2466137
    iput-object p1, p0, LX/HRy;->c:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;

    iput-wide p2, p0, LX/HRy;->a:J

    iput-object p4, p0, LX/HRy;->b:LX/0am;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, -0x521ad08f

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2466138
    iget-object v0, p0, LX/HRy;->c:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->n:LX/9XE;

    iget-wide v2, p0, LX/HRy;->a:J

    .line 2466139
    iget-object v4, v0, LX/9XE;->a:LX/0Zb;

    sget-object v5, LX/9X5;->EVENT_ADMIN_ACTIVITY_CLICK_EDIT_UNI_STATUS:LX/9X5;

    invoke-static {v5, v2, v3}, LX/9XE;->d(LX/9X2;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-interface {v4, v5}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2466140
    iget-object v0, p0, LX/HRy;->c:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->o:LX/GMm;

    invoke-virtual {v0}, LX/GMm;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/HRy;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2466141
    iget-object v0, p0, LX/HRy;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HRS;

    invoke-virtual {v0}, LX/HRS;->a()V

    .line 2466142
    :cond_0
    iget-object v0, p0, LX/HRy;->c:Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/admin_activity/views/PageActivityUniRunningStatusCardView;->o:LX/GMm;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-wide v4, p0, LX/HRy;->a:J

    invoke-virtual {v0, v2, v4, v5}, LX/GMm;->a(Landroid/content/Context;J)V

    .line 2466143
    const v0, -0x485acd50

    invoke-static {v6, v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
