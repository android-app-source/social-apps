.class public final enum LX/HLk;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HLk;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/HLk;

.field public static final enum NUX_CAN_SHOW:LX/HLk;

.field public static final enum NUX_DISMISSED:LX/HLk;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2456225
    new-instance v0, LX/HLk;

    const-string v1, "NUX_CAN_SHOW"

    invoke-direct {v0, v1, v2}, LX/HLk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HLk;->NUX_CAN_SHOW:LX/HLk;

    .line 2456226
    new-instance v0, LX/HLk;

    const-string v1, "NUX_DISMISSED"

    invoke-direct {v0, v1, v3}, LX/HLk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HLk;->NUX_DISMISSED:LX/HLk;

    .line 2456227
    const/4 v0, 0x2

    new-array v0, v0, [LX/HLk;

    sget-object v1, LX/HLk;->NUX_CAN_SHOW:LX/HLk;

    aput-object v1, v0, v2

    sget-object v1, LX/HLk;->NUX_DISMISSED:LX/HLk;

    aput-object v1, v0, v3

    sput-object v0, LX/HLk;->$VALUES:[LX/HLk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2456228
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HLk;
    .locals 1

    .prologue
    .line 2456224
    const-class v0, LX/HLk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HLk;

    return-object v0
.end method

.method public static values()[LX/HLk;
    .locals 1

    .prologue
    .line 2456223
    sget-object v0, LX/HLk;->$VALUES:[LX/HLk;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HLk;

    return-object v0
.end method
