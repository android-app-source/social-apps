.class public final LX/Hyj;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;)V
    .locals 0

    .prologue
    .line 2524492
    iput-object p1, p0, LX/Hyj;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2524470
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 2524471
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2524472
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2524473
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    .line 2524474
    if-eqz v0, :cond_0

    .line 2524475
    invoke-static {v0}, LX/Bm1;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;)Lcom/facebook/events/model/Event;

    move-result-object v0

    .line 2524476
    iget-object v1, p0, LX/Hyj;->a:Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;

    iget-object v1, v1, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->F:LX/Hz2;

    .line 2524477
    iget-object v2, v1, LX/Hz2;->g:LX/I2o;

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2524478
    iget-object v2, v1, LX/Hz2;->j:LX/I2Z;

    .line 2524479
    if-nez v0, :cond_1

    .line 2524480
    :goto_0
    invoke-static {v1}, LX/Hz2;->m(LX/Hz2;)V

    .line 2524481
    :cond_0
    return-void

    .line 2524482
    :cond_1
    iget-object v3, v2, LX/I2Z;->d:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/events/model/Event;

    .line 2524483
    invoke-virtual {v3}, Lcom/facebook/events/model/Event;->M()J

    move-result-wide v5

    invoke-virtual {v0}, Lcom/facebook/events/model/Event;->M()J

    move-result-wide v7

    cmp-long v5, v5, v7

    if-lez v5, :cond_2

    iget-object v5, v2, LX/I2Z;->i:Ljava/util/HashMap;

    .line 2524484
    iget-object v6, v3, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v6, v6

    .line 2524485
    invoke-virtual {v5, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2524486
    iget-object v4, v2, LX/I2Z;->d:Ljava/util/List;

    iget-object v5, v2, LX/I2Z;->i:Ljava/util/HashMap;

    .line 2524487
    iget-object v6, v3, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v3, v6

    .line 2524488
    invoke-virtual {v5, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-interface {v4, v3, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 2524489
    invoke-static {v2}, LX/I2Z;->b(LX/I2Z;)V

    goto :goto_0

    .line 2524490
    :cond_3
    iget-object v3, v2, LX/I2Z;->d:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2524491
    invoke-static {v2}, LX/I2Z;->b(LX/I2Z;)V

    goto :goto_0
.end method
