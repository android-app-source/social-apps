.class public LX/I3M;
.super LX/Gcw;
.source ""


# instance fields
.field public c:LX/HxA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;

.field public e:Lcom/facebook/events/common/EventAnalyticsParams;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2531300
    invoke-direct {p0, p1}, LX/Gcw;-><init>(Landroid/content/Context;)V

    .line 2531301
    const-class p1, LX/I3M;

    invoke-static {p1, p0}, LX/I3M;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2531302
    new-instance p1, LX/I3L;

    invoke-direct {p1, p0}, LX/I3L;-><init>(LX/I3M;)V

    iput-object p1, p0, LX/I3M;->b:Landroid/view/View$OnClickListener;

    .line 2531303
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/I3M;

    invoke-static {p0}, LX/HxA;->b(LX/0QB;)LX/HxA;

    move-result-object p0

    check-cast p0, LX/HxA;

    iput-object p0, p1, LX/I3M;->c:LX/HxA;

    return-void
.end method


# virtual methods
.method public final c(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2531297
    iget-object v0, p0, LX/I3M;->d:Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/I3M;->d:Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;->c()Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel$EventsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/I3M;->d:Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;->c()Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel$EventsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel$EventsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-gt v0, p1, :cond_1

    .line 2531298
    :cond_0
    const/4 v0, 0x0

    .line 2531299
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/I3M;->d:Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;->c()Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel$EventsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel$EventsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7oa;

    invoke-interface {v0}, LX/7oa;->e()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getEventAnalyticsParams()Lcom/facebook/events/common/EventAnalyticsParams;
    .locals 1

    .prologue
    .line 2531304
    iget-object v0, p0, LX/I3M;->e:Lcom/facebook/events/common/EventAnalyticsParams;

    return-object v0
.end method

.method public getModule()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2531296
    const-string v0, "event_suggestions"

    return-object v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 2531295
    iget-object v0, p0, LX/I3M;->d:Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/I3M;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0821b5

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/I3M;->d:Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getViewAllText()Ljava/lang/CharSequence;
    .locals 5

    .prologue
    .line 2531294
    iget-object v0, p0, LX/I3M;->d:Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, LX/I3M;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0821a9

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, LX/I3M;->getTitle()Ljava/lang/CharSequence;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
