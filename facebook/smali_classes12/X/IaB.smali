.class public LX/IaB;
.super LX/398;
.source ""


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/IZq;


# direct methods
.method public constructor <init>(LX/0Or;LX/IZq;)V
    .locals 3
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/IZq;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2590697
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2590698
    iput-object p1, p0, LX/IaB;->a:LX/0Or;

    .line 2590699
    iput-object p2, p0, LX/IaB;->b:LX/IZq;

    .line 2590700
    sget-object v0, LX/3RH;->W:Ljava/lang/String;

    const-string v1, "{provider_name}"

    const-string v2, "{provider_page_fbid}"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/IaA;

    invoke-direct {v1, p0}, LX/IaA;-><init>(LX/IaB;)V

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;LX/47M;)V

    .line 2590701
    return-void
.end method
