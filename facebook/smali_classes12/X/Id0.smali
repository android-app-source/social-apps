.class public LX/Id0;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public a:LX/Ibv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/IZM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CMq;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Landroid/net/Uri;

.field public e:Lcom/facebook/maps/FbMapViewDelegate;

.field public f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public g:Lcom/facebook/messaging/business/ride/view/RideRouteInfoView;

.field public h:Lcom/facebook/messaging/business/common/view/BusinessPairTextView;

.field public i:Lcom/facebook/messaging/business/common/view/BusinessPairTextView;

.field public j:Lcom/facebook/messaging/business/common/view/BusinessPairTextView;

.field public k:Lcom/facebook/widget/text/BetterTextView;

.field public l:LX/6Zu;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2596001
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2596002
    const-class v0, LX/Id0;

    invoke-static {v0, p0}, LX/Id0;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2596003
    const v0, 0x7f031223

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2596004
    const v0, 0x7f0d2a72

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/maps/FbMapViewDelegate;

    iput-object v0, p0, LX/Id0;->e:Lcom/facebook/maps/FbMapViewDelegate;

    .line 2596005
    iget-object v0, p0, LX/Id0;->e:Lcom/facebook/maps/FbMapViewDelegate;

    const/4 p1, 0x0

    invoke-virtual {v0, p1}, LX/6Zn;->a(Landroid/os/Bundle;)V

    .line 2596006
    const v0, 0x7f0d2a6e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/ride/view/RideRouteInfoView;

    iput-object v0, p0, LX/Id0;->g:Lcom/facebook/messaging/business/ride/view/RideRouteInfoView;

    .line 2596007
    const v0, 0x7f0d2a73

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/Id0;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2596008
    const v0, 0x7f0d2a6f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/common/view/BusinessPairTextView;

    iput-object v0, p0, LX/Id0;->h:Lcom/facebook/messaging/business/common/view/BusinessPairTextView;

    .line 2596009
    const v0, 0x7f0d2a70

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/common/view/BusinessPairTextView;

    iput-object v0, p0, LX/Id0;->i:Lcom/facebook/messaging/business/common/view/BusinessPairTextView;

    .line 2596010
    const v0, 0x7f0d2a71

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/business/common/view/BusinessPairTextView;

    iput-object v0, p0, LX/Id0;->j:Lcom/facebook/messaging/business/common/view/BusinessPairTextView;

    .line 2596011
    const v0, 0x7f0d0810

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Id0;->k:Lcom/facebook/widget/text/BetterTextView;

    .line 2596012
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/Id0;->setOrientation(I)V

    .line 2596013
    new-instance v0, LX/Icy;

    invoke-direct {v0, p0}, LX/Icy;-><init>(LX/Id0;)V

    invoke-virtual {p0, v0}, LX/Id0;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2596014
    new-instance v0, LX/Icz;

    invoke-direct {v0, p0}, LX/Icz;-><init>(LX/Id0;)V

    iput-object v0, p0, LX/Id0;->l:LX/6Zu;

    .line 2596015
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p1, LX/Id0;

    invoke-static {v3}, LX/Ibv;->a(LX/0QB;)LX/Ibv;

    move-result-object v1

    check-cast v1, LX/Ibv;

    new-instance p0, LX/IZM;

    invoke-static {v3}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v2

    check-cast v2, Landroid/content/res/Resources;

    invoke-direct {p0, v2}, LX/IZM;-><init>(Landroid/content/res/Resources;)V

    move-object v2, p0

    check-cast v2, LX/IZM;

    const/16 p0, 0x27c5

    invoke-static {v3, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    iput-object v1, p1, LX/Id0;->a:LX/Ibv;

    iput-object v2, p1, LX/Id0;->b:LX/IZM;

    iput-object v3, p1, LX/Id0;->c:LX/0Ot;

    return-void
.end method


# virtual methods
.method public final onMeasure(II)V
    .locals 6

    .prologue
    .line 2596016
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 2596017
    iget-object v1, p0, LX/Id0;->e:Lcom/facebook/maps/FbMapViewDelegate;

    invoke-virtual {v1}, Lcom/facebook/maps/FbMapViewDelegate;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 2596018
    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2596019
    int-to-float v0, v0

    float-to-double v2, v0

    const-wide v4, 0x3ffe666666666666L    # 1.9

    div-double/2addr v2, v4

    double-to-int v0, v2

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2596020
    iget-object v0, p0, LX/Id0;->e:Lcom/facebook/maps/FbMapViewDelegate;

    invoke-virtual {v0, v1}, Lcom/facebook/maps/FbMapViewDelegate;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2596021
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;->onMeasure(II)V

    .line 2596022
    return-void
.end method
