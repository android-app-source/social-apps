.class public final LX/ISY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/feed/ui/GroupsInlineComposer;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/ui/GroupsInlineComposer;)V
    .locals 0

    .prologue
    .line 2578055
    iput-object p1, p0, LX/ISY;->a:Lcom/facebook/groups/feed/ui/GroupsInlineComposer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    .line 2578056
    const-string v0, "groups_seeds_composer_create_poll"

    invoke-static {v0}, LX/ISn;->a(Ljava/lang/String;)V

    .line 2578057
    iget-object v0, p0, LX/ISY;->a:Lcom/facebook/groups/feed/ui/GroupsInlineComposer;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->s:LX/ISp;

    .line 2578058
    invoke-static {v0}, LX/ISp;->d(LX/ISp;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    .line 2578059
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setDisableMentions(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 2578060
    new-instance v2, LX/89K;

    invoke-direct {v2}, LX/89K;-><init>()V

    const-string v2, "GroupsPollComposerPluginConfig"

    invoke-static {v2}, LX/89L;->a(Ljava/lang/String;)LX/89L;

    move-result-object v2

    invoke-static {v2}, LX/89K;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPluginConfig(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 2578061
    iget-object v2, v0, LX/ISp;->a:LX/1Kf;

    const/4 v3, 0x0

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v4

    const/16 p0, 0x6dc

    iget-object v1, v0, LX/ISp;->g:Landroid/content/Context;

    const-class p1, Landroid/app/Activity;

    invoke-static {v1, p1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    invoke-interface {v2, v3, v4, p0, v1}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 2578062
    const/4 v0, 0x1

    return v0
.end method
