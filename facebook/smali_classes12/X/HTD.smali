.class public final LX/HTD;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 2469142
    const/4 v5, 0x0

    .line 2469143
    const-wide/16 v6, 0x0

    .line 2469144
    const/4 v4, 0x0

    .line 2469145
    const/4 v3, 0x0

    .line 2469146
    const/4 v2, 0x0

    .line 2469147
    const/4 v1, 0x0

    .line 2469148
    const/4 v0, 0x0

    .line 2469149
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v8, v9, :cond_a

    .line 2469150
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2469151
    const/4 v0, 0x0

    .line 2469152
    :goto_0
    return v0

    .line 2469153
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v9, :cond_7

    .line 2469154
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 2469155
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2469156
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_0

    if-eqz v4, :cond_0

    .line 2469157
    const-string v9, "aggregated_claim_count"

    invoke-virtual {v4, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 2469158
    const/4 v1, 0x1

    .line 2469159
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    move v5, v4

    goto :goto_1

    .line 2469160
    :cond_1
    const-string v9, "expiration"

    invoke-virtual {v4, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 2469161
    const/4 v0, 0x1

    .line 2469162
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    goto :goto_1

    .line 2469163
    :cond_2
    const-string v9, "id"

    invoke-virtual {v4, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 2469164
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    move v8, v4

    goto :goto_1

    .line 2469165
    :cond_3
    const-string v9, "offer_title"

    invoke-virtual {v4, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 2469166
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    move v7, v4

    goto :goto_1

    .line 2469167
    :cond_4
    const-string v9, "published_offer_views"

    invoke-virtual {v4, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 2469168
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2469169
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v9, LX/15z;->START_ARRAY:LX/15z;

    if-ne v6, v9, :cond_5

    .line 2469170
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v9, LX/15z;->END_ARRAY:LX/15z;

    if-eq v6, v9, :cond_5

    .line 2469171
    invoke-static {p0, p1}, LX/HTC;->b(LX/15w;LX/186;)I

    move-result v6

    .line 2469172
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2469173
    :cond_5
    invoke-static {v4, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v4

    move v4, v4

    .line 2469174
    move v6, v4

    goto/16 :goto_1

    .line 2469175
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 2469176
    :cond_7
    const/4 v4, 0x5

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2469177
    if-eqz v1, :cond_8

    .line 2469178
    const/4 v1, 0x0

    const/4 v4, 0x0

    invoke-virtual {p1, v1, v5, v4}, LX/186;->a(III)V

    .line 2469179
    :cond_8
    if-eqz v0, :cond_9

    .line 2469180
    const/4 v1, 0x1

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2469181
    :cond_9
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 2469182
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2469183
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2469184
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_a
    move v8, v4

    move v11, v3

    move v12, v2

    move-wide v2, v6

    move v7, v11

    move v6, v12

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 2469115
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2469116
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v0

    .line 2469117
    if-eqz v0, :cond_0

    .line 2469118
    const-string v1, "aggregated_claim_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2469119
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2469120
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 2469121
    cmp-long v2, v0, v2

    if-eqz v2, :cond_1

    .line 2469122
    const-string v2, "expiration"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2469123
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 2469124
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2469125
    if-eqz v0, :cond_2

    .line 2469126
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2469127
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2469128
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2469129
    if-eqz v0, :cond_3

    .line 2469130
    const-string v1, "offer_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2469131
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2469132
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2469133
    if-eqz v0, :cond_5

    .line 2469134
    const-string v1, "published_offer_views"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2469135
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2469136
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_4

    .line 2469137
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/HTC;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2469138
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2469139
    :cond_4
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2469140
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2469141
    return-void
.end method
