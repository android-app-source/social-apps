.class public final LX/I5Z;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/events/feed/data/EventFeedStories;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/I5f;


# direct methods
.method public constructor <init>(LX/I5f;)V
    .locals 0

    .prologue
    .line 2535864
    iput-object p1, p0, LX/I5Z;->a:LX/I5f;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2535865
    iget-object v0, p0, LX/I5Z;->a:LX/I5f;

    const/4 v1, 0x0

    .line 2535866
    iput-boolean v1, v0, LX/I5f;->s:Z

    .line 2535867
    iget-object v0, p0, LX/I5Z;->a:LX/I5f;

    invoke-static {v0}, LX/I5f;->l(LX/I5f;)V

    .line 2535868
    iget-object v0, p0, LX/I5Z;->a:LX/I5f;

    iget-object v0, v0, LX/I5f;->p:LX/I5b;

    invoke-virtual {v0}, LX/I5a;->b()V

    .line 2535869
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2535870
    check-cast p1, Lcom/facebook/events/feed/data/EventFeedStories;

    const/4 v5, 0x0

    .line 2535871
    iget-object v0, p1, Lcom/facebook/events/feed/data/EventFeedStories;->a:LX/0Px;

    move-object v2, v0

    .line 2535872
    iget-object v0, p1, Lcom/facebook/events/feed/data/EventFeedStories;->b:Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-object v0, v0

    .line 2535873
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 2535874
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 2535875
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 2535876
    iget-object v3, p1, Lcom/facebook/events/feed/data/EventFeedStories;->c:LX/0ta;

    move-object v3, v3

    .line 2535877
    sget-object v4, LX/0ta;->FROM_SERVER:LX/0ta;

    if-ne v3, v4, :cond_0

    .line 2535878
    iget-object v3, p0, LX/I5Z;->a:LX/I5f;

    iget-object v3, v3, LX/I5f;->c:LX/0fz;

    invoke-virtual {v3}, LX/0fz;->d()V

    .line 2535879
    :cond_0
    iget-object v3, p0, LX/I5Z;->a:LX/I5f;

    iget-object v3, v3, LX/I5f;->c:LX/0fz;

    sget-object v4, LX/0qw;->FULL:LX/0qw;

    invoke-virtual {v3, v1, v0, v4}, LX/0fz;->a(Ljava/util/List;Lcom/facebook/graphql/model/GraphQLPageInfo;LX/0qw;)V

    .line 2535880
    iget-object v0, p0, LX/I5Z;->a:LX/I5f;

    iget-object v0, v0, LX/I5f;->c:LX/0fz;

    invoke-virtual {v0}, LX/0fz;->size()I

    move-result v0

    if-nez v0, :cond_3

    .line 2535881
    iget-object v0, p0, LX/I5Z;->a:LX/I5f;

    const/4 v1, 0x1

    .line 2535882
    iput-boolean v1, v0, LX/I5f;->q:Z

    .line 2535883
    :cond_1
    :goto_0
    iget-object v0, p0, LX/I5Z;->a:LX/I5f;

    iget-boolean v0, v0, LX/I5f;->s:Z

    if-eqz v0, :cond_2

    .line 2535884
    iget-object v0, p0, LX/I5Z;->a:LX/I5f;

    .line 2535885
    iput-boolean v5, v0, LX/I5f;->s:Z

    .line 2535886
    :cond_2
    iget-object v0, p0, LX/I5Z;->a:LX/I5f;

    iget-object v0, v0, LX/I5f;->n:LX/I7I;

    invoke-virtual {v0}, LX/I7I;->a()V

    .line 2535887
    iget-object v0, p0, LX/I5Z;->a:LX/I5f;

    iget-object v0, v0, LX/I5f;->p:LX/I5b;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v1

    invoke-virtual {v0, v1}, LX/I5b;->a(I)V

    .line 2535888
    return-void

    .line 2535889
    :cond_3
    iget-object v0, p0, LX/I5Z;->a:LX/I5f;

    iget-object v0, v0, LX/I5f;->c:LX/0fz;

    invoke-virtual {v0}, LX/0fz;->u()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2535890
    iget-object v0, p0, LX/I5Z;->a:LX/I5f;

    iget-object v0, v0, LX/I5f;->c:LX/0fz;

    invoke-virtual {v0}, LX/0fz;->r()I

    .line 2535891
    iget-object v0, p0, LX/I5Z;->a:LX/I5f;

    .line 2535892
    iput-boolean v5, v0, LX/I5f;->q:Z

    .line 2535893
    goto :goto_0
.end method
