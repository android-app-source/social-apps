.class public final LX/IpN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/contacts/graphql/Contact;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/IpP;


# direct methods
.method public constructor <init>(LX/IpP;)V
    .locals 0

    .prologue
    .line 2612842
    iput-object p1, p0, LX/IpN;->a:LX/IpP;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    .line 2612843
    iget-object v0, p0, LX/IpN;->a:LX/IpP;

    iget-object v0, v0, LX/IpP;->c:LX/03V;

    const-string v1, "OrionRequestMessengerPayLoader"

    const-string v2, "Failed to fetch the Contact for recipient %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, LX/IpN;->a:LX/IpP;

    iget-object v5, v5, LX/IpP;->g:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2612844
    iget-object p0, v5, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->p:Lcom/facebook/user/model/UserKey;

    move-object v5, p0

    .line 2612845
    invoke-virtual {v5}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2612846
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2612847
    check-cast p1, Lcom/facebook/contacts/graphql/Contact;

    .line 2612848
    iget-object v0, p0, LX/IpN;->a:LX/IpP;

    iget-object v0, v0, LX/IpP;->g:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    invoke-virtual {p1}, Lcom/facebook/contacts/graphql/Contact;->e()Lcom/facebook/user/model/Name;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->a(Lcom/facebook/user/model/Name;)V

    .line 2612849
    return-void
.end method
