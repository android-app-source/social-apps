.class public LX/JMT;
.super LX/5pb;
.source ""

# interfaces
.implements LX/5pQ;


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "RCTNetworking"
.end annotation


# instance fields
.field private final a:Lcom/facebook/http/common/FbHttpRequestProcessor;

.field private final b:Ljava/lang/Object;

.field private final c:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "LX/15D",
            "<",
            "LX/JMS;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/common/callercontext/CallerContext;

.field private final e:Ljava/lang/String;

.field private final f:Lorg/apache/http/client/ResponseHandler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/http/client/ResponseHandler",
            "<",
            "LX/JMS;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/5pY;Lcom/facebook/http/common/FbHttpRequestProcessor;Lcom/facebook/common/callercontext/CallerContext;LX/0s2;)V
    .locals 1

    .prologue
    .line 2684047
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 2684048
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/JMT;->b:Ljava/lang/Object;

    .line 2684049
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/JMT;->c:Landroid/util/SparseArray;

    .line 2684050
    new-instance v0, LX/JMQ;

    invoke-direct {v0, p0}, LX/JMQ;-><init>(LX/JMT;)V

    iput-object v0, p0, LX/JMT;->f:Lorg/apache/http/client/ResponseHandler;

    .line 2684051
    iput-object p2, p0, LX/JMT;->a:Lcom/facebook/http/common/FbHttpRequestProcessor;

    .line 2684052
    iput-object p3, p0, LX/JMT;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 2684053
    invoke-virtual {p4}, LX/0s2;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/JMT;->e:Ljava/lang/String;

    .line 2684054
    return-void
.end method

.method private a(I)LX/15D;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/15D",
            "<",
            "LX/JMS;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2684055
    iget-object v1, p0, LX/JMT;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 2684056
    :try_start_0
    iget-object v0, p0, LX/JMT;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/15D;

    .line 2684057
    iget-object v2, p0, LX/JMT;->c:Landroid/util/SparseArray;

    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 2684058
    monitor-exit v1

    return-object v0

    .line 2684059
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static synthetic a(LX/JMT;I)LX/15D;
    .locals 1

    .prologue
    .line 2684060
    invoke-direct {p0, p1}, LX/JMT;->a(I)LX/15D;

    move-result-object v0

    return-object v0
.end method

.method private static a([Lorg/apache/http/Header;)LX/5pH;
    .locals 7

    .prologue
    .line 2684061
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v1

    .line 2684062
    array-length v2, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, p0, v0

    .line 2684063
    invoke-interface {v3}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v4

    .line 2684064
    invoke-interface {v1, v4}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2684065
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v1, v4}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v3}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v4, v3}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2684066
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2684067
    :cond_0
    invoke-interface {v3}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v4, v3}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 2684068
    :cond_1
    return-object v1
.end method

.method private a([Lorg/apache/http/Header;LX/5pC;)Lorg/apache/http/client/methods/HttpPost;
    .locals 9
    .param p1    # [Lorg/apache/http/Header;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2684082
    new-instance v1, LX/4cL;

    invoke-direct {v1}, LX/4cL;-><init>()V

    .line 2684083
    const/4 v0, 0x0

    invoke-interface {p2}, LX/5pC;->size()I

    move-result v2

    :goto_0
    if-ge v0, v2, :cond_5

    .line 2684084
    invoke-interface {p2, v0}, LX/5pC;->a(I)LX/5pG;

    move-result-object v3

    .line 2684085
    const-string v4, "fieldName"

    invoke-interface {v3, v4}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2684086
    if-nez v4, :cond_0

    .line 2684087
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Attribute \'name\' missing for formData part at index "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2684088
    :cond_0
    const-string v5, "string"

    invoke-interface {v3, v5}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2684089
    const-string v5, "string"

    invoke-interface {v3, v5}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2684090
    new-instance v5, LX/4cY;

    invoke-direct {v5, v3}, LX/4cY;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4, v5}, LX/4cL;->a(Ljava/lang/String;LX/4cO;)V

    .line 2684091
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2684092
    :cond_1
    const-string v5, "uri"

    invoke-interface {v3, v5}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 2684093
    const-string v5, "uri"

    invoke-interface {v3, v5}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2684094
    const-string v6, "name"

    invoke-interface {v3, v6}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2684095
    const-string v7, "type"

    invoke-interface {v3, v7}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2684096
    if-eqz v6, :cond_2

    if-nez v3, :cond_3

    .line 2684097
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Incomplete payload for URI formData part"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2684098
    :cond_3
    new-instance v7, LX/JMP;

    .line 2684099
    iget-object v8, p0, LX/5pb;->a:LX/5pY;

    move-object v8, v8

    .line 2684100
    invoke-virtual {v8}, LX/5pY;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-direct {v7, v8, v5, v3, v6}, LX/JMP;-><init>(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v4, v7}, LX/4cL;->a(Ljava/lang/String;LX/4cO;)V

    goto :goto_1

    .line 2684101
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unrecognized FormData part."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2684102
    :cond_5
    new-instance v0, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v0}, Lorg/apache/http/client/methods/HttpPost;-><init>()V

    .line 2684103
    invoke-static {v0, p1}, LX/JMT;->a(Lorg/apache/http/client/methods/HttpUriRequest;[Lorg/apache/http/Header;)V

    .line 2684104
    invoke-virtual {v0, v1}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 2684105
    return-object v0
.end method

.method private static a([Lorg/apache/http/Header;Ljava/lang/String;)Lorg/apache/http/client/methods/HttpPost;
    .locals 6
    .param p0    # [Lorg/apache/http/Header;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 2684069
    new-instance v2, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v2}, Lorg/apache/http/client/methods/HttpPost;-><init>()V

    .line 2684070
    new-instance v3, Lorg/apache/http/entity/StringEntity;

    invoke-direct {v3, p1}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;)V

    .line 2684071
    invoke-virtual {v2, v3}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 2684072
    if-eqz p0, :cond_1

    move v1, v0

    .line 2684073
    :goto_0
    array-length v4, p0

    if-ge v0, v4, :cond_2

    .line 2684074
    aget-object v4, p0, v0

    invoke-interface {v4}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "content-type"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2684075
    aget-object v1, p0, v0

    invoke-interface {v1}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lorg/apache/http/entity/StringEntity;->setContentType(Ljava/lang/String;)V

    .line 2684076
    const/4 v1, 0x1

    .line 2684077
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2684078
    :cond_0
    aget-object v4, p0, v0

    invoke-virtual {v2, v4}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Lorg/apache/http/Header;)V

    goto :goto_1

    :cond_1
    move v1, v0

    .line 2684079
    :cond_2
    if-nez v1, :cond_3

    .line 2684080
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Payload is set but no content-type header specified"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2684081
    :cond_3
    return-object v2
.end method

.method private a(II[Lorg/apache/http/Header;)V
    .locals 3

    .prologue
    .line 2684111
    invoke-static {p3}, LX/JMT;->a([Lorg/apache/http/Header;)LX/5pH;

    move-result-object v0

    .line 2684112
    invoke-static {}, LX/5op;->a()LX/5pD;

    move-result-object v1

    .line 2684113
    invoke-interface {v1, p1}, LX/5pD;->pushInt(I)V

    .line 2684114
    invoke-interface {v1, p2}, LX/5pD;->pushInt(I)V

    .line 2684115
    invoke-interface {v1, v0}, LX/5pD;->a(LX/5pH;)V

    .line 2684116
    invoke-direct {p0}, LX/JMT;->h()Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;

    move-result-object v0

    const-string v2, "didReceiveNetworkResponse"

    invoke-interface {v0, v2, v1}, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;->emit(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2684117
    return-void
.end method

.method private a(ILX/15D;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LX/15D",
            "<",
            "LX/JMS;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2684118
    iget-object v1, p0, LX/JMT;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 2684119
    :try_start_0
    iget-object v0, p0, LX/JMT;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2684120
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 2684106
    invoke-static {}, LX/5op;->a()LX/5pD;

    move-result-object v0

    .line 2684107
    invoke-interface {v0, p1}, LX/5pD;->pushInt(I)V

    .line 2684108
    invoke-interface {v0, p2}, LX/5pD;->pushString(Ljava/lang/String;)V

    .line 2684109
    invoke-direct {p0}, LX/JMT;->h()Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;

    move-result-object v1

    const-string v2, "didReceiveNetworkData"

    invoke-interface {v1, v2, v0}, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;->emit(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2684110
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;ILX/5pC;LX/5pG;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2683969
    invoke-static {p2}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v1

    .line 2683970
    invoke-direct {p0, p4}, LX/JMT;->a(LX/5pC;)[Lorg/apache/http/Header;

    move-result-object v2

    .line 2683971
    const-string v0, "GET"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2683972
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/net/URI;)V

    .line 2683973
    invoke-static {v0, v2}, LX/JMT;->a(Lorg/apache/http/client/methods/HttpUriRequest;[Lorg/apache/http/Header;)V

    .line 2683974
    :goto_0
    invoke-static {}, LX/15D;->newBuilder()LX/15E;

    move-result-object v1

    const-string v2, "react_native"

    .line 2683975
    iput-object v2, v1, LX/15E;->c:Ljava/lang/String;

    .line 2683976
    move-object v1, v1

    .line 2683977
    iget-object v2, p0, LX/JMT;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 2683978
    iput-object v2, v1, LX/15E;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 2683979
    move-object v1, v1

    .line 2683980
    const-string v2, "ReactNativeHTTP"

    .line 2683981
    iput-object v2, v1, LX/15E;->e:Ljava/lang/String;

    .line 2683982
    move-object v1, v1

    .line 2683983
    iput-object v0, v1, LX/15E;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 2683984
    move-object v0, v1

    .line 2683985
    iget-object v1, p0, LX/JMT;->f:Lorg/apache/http/client/ResponseHandler;

    .line 2683986
    iput-object v1, v0, LX/15E;->g:Lorg/apache/http/client/ResponseHandler;

    .line 2683987
    move-object v0, v0

    .line 2683988
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    .line 2683989
    iput-object v1, v0, LX/15E;->k:Lcom/facebook/http/interfaces/RequestPriority;

    .line 2683990
    move-object v0, v0

    .line 2683991
    invoke-virtual {v0}, LX/15E;->a()LX/15D;

    move-result-object v0

    .line 2683992
    invoke-direct {p0, p3, v0}, LX/JMT;->a(ILX/15D;)V

    .line 2683993
    iget-object v1, p0, LX/JMT;->a:Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-virtual {v1, v0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->b(LX/15D;)LX/1j2;

    move-result-object v0

    .line 2683994
    iget-object v1, v0, LX/1j2;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    move-object v0, v1

    .line 2683995
    new-instance v1, LX/JMR;

    invoke-direct {v1, p0, p3, p6}, LX/JMR;-><init>(LX/JMT;ILjava/lang/String;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2683996
    return-void

    .line 2683997
    :cond_0
    const-string v0, "POST"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2683998
    const-string v0, "string"

    invoke-interface {p5, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2683999
    const-string v0, "string"

    invoke-interface {p5, v0}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, LX/JMT;->a([Lorg/apache/http/Header;Ljava/lang/String;)Lorg/apache/http/client/methods/HttpPost;

    move-result-object v0

    .line 2684000
    :goto_1
    invoke-virtual {v0, v1}, Lorg/apache/http/client/methods/HttpPost;->setURI(Ljava/net/URI;)V

    .line 2684001
    invoke-static {v0}, LX/JMT;->a(Lorg/apache/http/client/methods/HttpPost;)V

    goto :goto_0

    .line 2684002
    :cond_1
    const-string v0, "formData"

    invoke-interface {p5, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2684003
    const-string v0, "formData"

    invoke-interface {p5, v0}, LX/5pG;->b(Ljava/lang/String;)LX/5pC;

    move-result-object v0

    invoke-direct {p0, v2, v0}, LX/JMT;->a([Lorg/apache/http/Header;LX/5pC;)Lorg/apache/http/client/methods/HttpPost;

    move-result-object v0

    goto :goto_1

    .line 2684004
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unsupported POST data type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2684005
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported HTTP request method "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(Lorg/apache/http/client/methods/HttpPost;)V
    .locals 4

    .prologue
    .line 2684034
    const-string v0, "content-encoding"

    invoke-virtual {p0, v0}, Lorg/apache/http/client/methods/HttpPost;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v1

    .line 2684035
    if-eqz v1, :cond_0

    .line 2684036
    invoke-virtual {p0, v1}, Lorg/apache/http/client/methods/HttpPost;->removeHeader(Lorg/apache/http/Header;)V

    .line 2684037
    :cond_0
    invoke-virtual {p0}, Lorg/apache/http/client/methods/HttpPost;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v2

    .line 2684038
    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->getContentType()Lorg/apache/http/Header;

    move-result-object v0

    const-string v3, "Unexpected entitiy with no COntent-Type defined"

    invoke-static {v0, v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/Header;

    .line 2684039
    if-nez v1, :cond_1

    const-string v1, "application/x-www-form-urlencoded"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2684040
    :cond_1
    new-instance v0, LX/14g;

    invoke-direct {v0, v2}, LX/14g;-><init>(Lorg/apache/http/HttpEntity;)V

    invoke-virtual {p0, v0}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 2684041
    :cond_2
    return-void
.end method

.method private static a(Lorg/apache/http/client/methods/HttpUriRequest;[Lorg/apache/http/Header;)V
    .locals 2
    .param p1    # [Lorg/apache/http/Header;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2684042
    if-eqz p1, :cond_0

    .line 2684043
    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    .line 2684044
    aget-object v1, p1, v0

    invoke-interface {p0, v1}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Lorg/apache/http/Header;)V

    .line 2684045
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2684046
    :cond_0
    return-void
.end method

.method private a(LX/5pC;)[Lorg/apache/http/Header;
    .locals 9
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 2684018
    if-nez p1, :cond_0

    .line 2684019
    const/4 v0, 0x0

    .line 2684020
    :goto_0
    return-object v0

    .line 2684021
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-interface {p1}, LX/5pC;->size()I

    move-result v0

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 2684022
    invoke-interface {p1}, LX/5pC;->size()I

    move-result v5

    move v2, v3

    move v0, v3

    :goto_1
    if-ge v2, v5, :cond_4

    .line 2684023
    invoke-interface {p1, v2}, LX/5pC;->b(I)LX/5pC;

    move-result-object v6

    .line 2684024
    if-eqz v6, :cond_1

    invoke-interface {v6}, LX/5pC;->size()I

    move-result v7

    const/4 v8, 0x2

    if-eq v7, v8, :cond_2

    .line 2684025
    :cond_1
    new-instance v0, LX/5p9;

    const-string v1, "Unexpected structure of headers array"

    invoke-direct {v0, v1}, LX/5p9;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2684026
    :cond_2
    invoke-interface {v6, v3}, LX/5pC;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 2684027
    invoke-interface {v6, v1}, LX/5pC;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 2684028
    const-string v8, "user-agent"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    move v0, v1

    .line 2684029
    :cond_3
    new-instance v8, Lorg/apache/http/message/BasicHeader;

    invoke-direct {v8, v7, v6}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2684030
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2684031
    :cond_4
    if-nez v0, :cond_5

    .line 2684032
    new-instance v0, Lorg/apache/http/message/BasicHeader;

    const-string v1, "user-agent"

    iget-object v2, p0, LX/JMT;->e:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2684033
    :cond_5
    new-array v0, v3, [Lorg/apache/http/Header;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/http/Header;

    goto :goto_0
.end method

.method public static a$redex0(LX/JMT;ILX/JMS;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2684006
    iget v0, p2, LX/JMS;->a:I

    iget-object v1, p2, LX/JMS;->b:[Lorg/apache/http/Header;

    invoke-direct {p0, p1, v0, v1}, LX/JMT;->a(II[Lorg/apache/http/Header;)V

    .line 2684007
    const-string v0, ""

    .line 2684008
    const-string v1, "text"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2684009
    new-instance v0, Ljava/lang/String;

    iget-object v1, p2, LX/JMS;->c:[B

    const-string v2, "UTF-8"

    invoke-static {v2}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 2684010
    :cond_0
    :goto_0
    invoke-direct {p0, p1, v0}, LX/JMT;->a(ILjava/lang/String;)V

    .line 2684011
    invoke-static {}, LX/5op;->a()LX/5pD;

    move-result-object v0

    .line 2684012
    invoke-interface {v0, p1}, LX/5pD;->pushInt(I)V

    .line 2684013
    invoke-interface {v0}, LX/5pD;->pushNull()V

    .line 2684014
    invoke-direct {p0}, LX/JMT;->h()Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;

    move-result-object v1

    const-string v2, "didCompleteNetworkResponse"

    invoke-interface {v1, v2, v0}, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;->emit(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2684015
    return-void

    .line 2684016
    :cond_1
    const-string v1, "base64"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2684017
    iget-object v0, p2, LX/JMS;->c:[B

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(LX/JMT;ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 2683964
    invoke-static {}, LX/5op;->a()LX/5pD;

    move-result-object v0

    .line 2683965
    invoke-interface {v0, p1}, LX/5pD;->pushInt(I)V

    .line 2683966
    invoke-interface {v0, p2}, LX/5pD;->pushString(Ljava/lang/String;)V

    .line 2683967
    invoke-direct {p0}, LX/JMT;->h()Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;

    move-result-object v1

    const-string v2, "didCompleteNetworkResponse"

    invoke-interface {v1, v2, v0}, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;->emit(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2683968
    return-void
.end method

.method private h()Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;
    .locals 2

    .prologue
    .line 2683962
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2683963
    const-class v1, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;

    invoke-virtual {v0, v1}, LX/5pX;->a(Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;

    return-object v0
.end method


# virtual methods
.method public abortRequest(I)V
    .locals 2
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2683958
    invoke-direct {p0, p1}, LX/JMT;->a(I)LX/15D;

    move-result-object v0

    .line 2683959
    if-eqz v0, :cond_0

    .line 2683960
    iget-object v1, p0, LX/JMT;->a:Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-virtual {v1, v0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->c(LX/15D;)Z

    .line 2683961
    :cond_0
    return-void
.end method

.method public final bM_()V
    .locals 0

    .prologue
    .line 2683957
    return-void
.end method

.method public final bN_()V
    .locals 0

    .prologue
    .line 2683956
    return-void
.end method

.method public final bO_()V
    .locals 5

    .prologue
    .line 2683948
    iget-object v2, p0, LX/JMT;->b:Ljava/lang/Object;

    monitor-enter v2

    .line 2683949
    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, LX/JMT;->c:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 2683950
    iget-object v0, p0, LX/JMT;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/15D;

    .line 2683951
    if-eqz v0, :cond_0

    .line 2683952
    iget-object v4, p0, LX/JMT;->a:Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-virtual {v4, v0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->c(LX/15D;)Z

    .line 2683953
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2683954
    :cond_1
    iget-object v0, p0, LX/JMT;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 2683955
    monitor-exit v2

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 2683945
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2683946
    invoke-virtual {v0, p0}, LX/5pX;->a(LX/5pQ;)V

    .line 2683947
    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2683944
    const-string v0, "RCTNetworking"

    return-object v0
.end method

.method public sendRequest(Ljava/lang/String;Ljava/lang/String;ILX/5pC;LX/5pG;Ljava/lang/String;ZI)V
    .locals 3
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2683939
    :try_start_0
    invoke-direct/range {p0 .. p6}, LX/JMT;->a(Ljava/lang/String;Ljava/lang/String;ILX/5pC;LX/5pG;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2683940
    :goto_0
    return-void

    .line 2683941
    :catch_0
    move-exception v0

    .line 2683942
    const-class v1, LX/JMT;

    const-string v2, "Error while preparing request"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2683943
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p3, v0}, LX/JMT;->b(LX/JMT;ILjava/lang/String;)V

    goto :goto_0
.end method
