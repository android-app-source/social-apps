.class public LX/I1N;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/facebook/events/common/EventAnalyticsParams;

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/graphql/EventDashboardGraphQLInterfaces$EventCalendarableItem;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public f:Z

.field private g:LX/4yY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4yY",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/HzL;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2528937
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Integer;

    const/4 v2, 0x0

    const v3, 0x7f0d01a7

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const v3, 0x7f0d01a3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const v3, 0x7f0d01a6

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const v3, 0x7f0d01a8

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, LX/I1N;->a:Ljava/util/HashSet;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/events/common/EventAnalyticsParams;Landroid/content/Context;)V
    .locals 1
    .param p1    # Lcom/facebook/events/common/EventAnalyticsParams;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2528930
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2528931
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/I1N;->d:Ljava/util/List;

    .line 2528932
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/I1N;->e:Ljava/util/HashMap;

    .line 2528933
    iput-object p2, p0, LX/I1N;->b:Landroid/content/Context;

    .line 2528934
    iput-object p1, p0, LX/I1N;->c:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2528935
    invoke-static {p0}, LX/I1N;->e(LX/I1N;)V

    .line 2528936
    return-void
.end method

.method public static e(LX/I1N;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 2528920
    new-instance v0, LX/4yW;

    invoke-direct {v0}, LX/4yW;-><init>()V

    .line 2528921
    iget-object v1, p0, LX/I1N;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2528922
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, LX/50M;->b(Ljava/lang/Comparable;Ljava/lang/Comparable;)LX/50M;

    move-result-object v1

    const v2, 0x7f0d01a7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4yW;->a(LX/50M;Ljava/lang/Object;)LX/4yW;

    .line 2528923
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, LX/50M;->b(Ljava/lang/Comparable;Ljava/lang/Comparable;)LX/50M;

    move-result-object v1

    const v2, 0x7f0d01a3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4yW;->a(LX/50M;Ljava/lang/Object;)LX/4yW;

    .line 2528924
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, LX/I1N;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, LX/50M;->b(Ljava/lang/Comparable;Ljava/lang/Comparable;)LX/50M;

    move-result-object v1

    const v2, 0x7f0d01a6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4yW;->a(LX/50M;Ljava/lang/Object;)LX/4yW;

    .line 2528925
    iget-object v1, p0, LX/I1N;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    .line 2528926
    iget-boolean v2, p0, LX/I1N;->f:Z

    if-eqz v2, :cond_0

    .line 2528927
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v2, v1}, LX/50M;->b(Ljava/lang/Comparable;Ljava/lang/Comparable;)LX/50M;

    move-result-object v1

    const v2, 0x7f0d01a8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4yW;->a(LX/50M;Ljava/lang/Object;)LX/4yW;

    .line 2528928
    :cond_0
    invoke-virtual {v0}, LX/4yW;->a()LX/4yZ;

    move-result-object v0

    iput-object v0, p0, LX/I1N;->g:LX/4yY;

    .line 2528929
    return-void
.end method

.method public static f(LX/I1N;)V
    .locals 4

    .prologue
    .line 2528915
    iget-object v0, p0, LX/I1N;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 2528916
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/I1N;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2528917
    iget-object v2, p0, LX/I1N;->e:Ljava/util/HashMap;

    iget-object v0, p0, LX/I1N;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2528918
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2528919
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2528904
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 2528905
    const v0, 0x7f0d01a3

    if-ne p2, v0, :cond_0

    .line 2528906
    new-instance v0, LX/I1P;

    new-instance v1, Lcom/facebook/fig/header/FigHeader;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/fig/header/FigHeader;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, LX/I1P;-><init>(Lcom/facebook/fig/header/FigHeader;)V

    .line 2528907
    :goto_0
    return-object v0

    .line 2528908
    :cond_0
    const v0, 0x7f0d01a6

    if-ne p2, v0, :cond_1

    .line 2528909
    new-instance v0, LX/I1M;

    const v2, 0x7f03054a

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, LX/I1M;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 2528910
    :cond_1
    const v0, 0x7f0d01a8

    if-ne p2, v0, :cond_2

    .line 2528911
    new-instance v0, LX/I1W;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/facebook/fig/footer/FigFooter;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/facebook/fig/footer/FigFooter;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1, v2}, LX/I1W;-><init>(Landroid/content/Context;Lcom/facebook/fig/footer/FigFooter;)V

    goto :goto_0

    .line 2528912
    :cond_2
    const v0, 0x7f0d01a7

    if-ne p2, v0, :cond_3

    .line 2528913
    new-instance v0, LX/I1M;

    const v2, 0x7f030580

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, LX/I1M;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 2528914
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown View Type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(LX/1a1;I)V
    .locals 4

    .prologue
    .line 2528882
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    .line 2528883
    const v1, 0x7f0d01a3

    if-ne v0, v1, :cond_1

    .line 2528884
    check-cast p1, LX/I1P;

    .line 2528885
    iget-object v0, p0, LX/I1N;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0821e5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/I1P;->a(Ljava/lang/String;)V

    .line 2528886
    :cond_0
    :goto_0
    return-void

    .line 2528887
    :cond_1
    const v1, 0x7f0d01a6

    if-ne v0, v1, :cond_2

    .line 2528888
    invoke-static {}, Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;->a()Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;

    move-result-object v1

    .line 2528889
    iget-object v0, p0, LX/I1N;->d:Ljava/util/List;

    add-int/lit8 v2, p2, -0x2

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;

    .line 2528890
    iput-object v0, v1, Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;->a:Lcom/facebook/events/graphql/EventDashboardGraphQLModels$EventCalendarableItemModel;

    .line 2528891
    sget-object v0, LX/I0b;->SINGLE:LX/I0b;

    .line 2528892
    iput-object v0, v1, Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;->b:LX/I0b;

    .line 2528893
    sget-object v0, LX/I0a;->FUTURE:LX/I0a;

    .line 2528894
    iput-object v0, v1, Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;->c:LX/I0a;

    .line 2528895
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;

    iget-object v2, p0, LX/I1N;->c:Lcom/facebook/events/common/EventAnalyticsParams;

    sget-object v3, Lcom/facebook/events/common/ActionMechanism;->HOME_TAB_UPCOMING_EVENT:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardRowView;->a(Lcom/facebook/events/dashboard/calendar/EventCalendarableItemSlice;Lcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/events/common/ActionMechanism;)V

    goto :goto_0

    .line 2528896
    :cond_2
    const v1, 0x7f0d01a8

    if-ne v0, v1, :cond_0

    .line 2528897
    iget-object v0, p0, LX/I1N;->h:LX/HzL;

    if-eqz v0, :cond_0

    .line 2528898
    check-cast p1, LX/I1W;

    .line 2528899
    new-instance v0, LX/I1L;

    invoke-direct {v0, p0}, LX/I1L;-><init>(LX/I1N;)V

    .line 2528900
    iget-object v1, p1, LX/I1W;->l:Lcom/facebook/fig/footer/FigFooter;

    invoke-virtual {v1, v0}, Lcom/facebook/fig/footer/FigFooter;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2528901
    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 2528903
    iget-object v0, p0, LX/I1N;->g:LX/4yY;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, LX/4yY;->a(Ljava/lang/Comparable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final ij_()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2528902
    iget-object v0, p0, LX/I1N;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    iget-object v0, p0, LX/I1N;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x2

    :goto_0
    add-int/2addr v0, v2

    iget-boolean v2, p0, LX/I1N;->f:Z

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method
