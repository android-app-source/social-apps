.class public LX/HvG;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/HvB;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/87V;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0in;LX/2zF;LX/0Or;LX/0Ot;)V
    .locals 1
    .param p1    # LX/0in;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0in",
            "<",
            "Lcom/facebook/ipc/composer/plugin/ComposerPlugin",
            "<",
            "Lcom/facebook/composer/system/api/ComposerModel;",
            "Lcom/facebook/composer/system/api/ComposerDerivedData;",
            "Lcom/facebook/composer/system/api/ComposerMutation;",
            ">;>;",
            "LX/2zF;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/87V;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2518787
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2518788
    invoke-virtual {p2, p1}, LX/2zF;->a(LX/0in;)LX/HvB;

    move-result-object v0

    iput-object v0, p0, LX/HvG;->a:LX/HvB;

    .line 2518789
    iput-object p3, p0, LX/HvG;->b:LX/0Or;

    .line 2518790
    iput-object p4, p0, LX/HvG;->c:LX/0Ot;

    .line 2518791
    return-void
.end method


# virtual methods
.method public final a(LX/HvD;Lcom/facebook/composer/system/model/ComposerModelImpl;Lcom/facebook/composer/system/model/ComposerModelImpl;)Z
    .locals 10

    .prologue
    const/4 v1, 0x1

    .line 2518792
    const/4 v0, 0x0

    .line 2518793
    invoke-interface {p2}, LX/0j1;->getTargetAlbum()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/HvG;->a:LX/HvB;

    invoke-virtual {v2, p3}, LX/HvB;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;)LX/2zG;

    move-result-object v2

    invoke-virtual {v2}, LX/2zG;->f()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2518794
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, LX/0jL;->b(Lcom/facebook/graphql/model/GraphQLAlbum;)LX/0jL;

    move v0, v1

    .line 2518795
    :cond_0
    invoke-interface {p3}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v2

    .line 2518796
    iget-object v6, p0, LX/HvG;->b:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 2518797
    iget-wide v8, v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    cmp-long v6, v8, v6

    if-nez v6, :cond_1

    iget-object v6, v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object v7, LX/2rw;->UNDIRECTED:LX/2rw;

    if-ne v6, v7, :cond_5

    :cond_1
    const/4 v6, 0x1

    :goto_0
    invoke-static {v6}, LX/0PB;->checkArgument(Z)V

    .line 2518798
    invoke-interface {p3}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v2

    iget-wide v2, v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-gtz v2, :cond_4

    .line 2518799
    new-instance v2, LX/89I;

    iget-object v0, p0, LX/HvG;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    sget-object v0, LX/2rw;->UNDIRECTED:LX/2rw;

    invoke-direct {v2, v4, v5, v0}, LX/89I;-><init>(JLX/2rw;)V

    invoke-virtual {v2}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/0jL;->b(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)LX/0jL;

    .line 2518800
    :goto_1
    invoke-interface {p3}, LX/0is;->o()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getInspirationModels()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2518801
    iget-object v0, p0, LX/HvG;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/87V;

    invoke-virtual {v0, p1, p2, p3}, LX/87V;->a(LX/0jL;LX/0is;LX/0is;)Z

    move-result v0

    or-int/2addr v0, v1

    .line 2518802
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2518803
    invoke-virtual {p2}, Lcom/facebook/composer/system/model/ComposerModelImpl;->j()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->getCaptureState()LX/86q;

    move-result-object v3

    .line 2518804
    invoke-virtual {p3}, Lcom/facebook/composer/system/model/ComposerModelImpl;->j()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->getCaptureState()LX/86q;

    move-result-object v4

    .line 2518805
    if-eq v4, v3, :cond_2

    .line 2518806
    sget-object v5, LX/Ary;->a:[I

    invoke-virtual {v4}, LX/86q;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 2518807
    :cond_2
    :goto_2
    move v1, v2

    .line 2518808
    or-int/2addr v1, v0

    .line 2518809
    :cond_3
    return v1

    :cond_4
    move v1, v0

    goto :goto_1

    .line 2518810
    :cond_5
    const/4 v6, 0x0

    goto :goto_0

    .line 2518811
    :pswitch_0
    sget-object v5, LX/86q;->READY:LX/86q;

    if-ne v3, v5, :cond_6

    :goto_3
    invoke-static {v3, v4}, LX/Arz;->a(LX/86q;LX/86q;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    goto :goto_2

    :cond_6
    move v1, v2

    goto :goto_3

    .line 2518812
    :pswitch_1
    sget-object v5, LX/86q;->READY:LX/86q;

    if-ne v3, v5, :cond_7

    :goto_4
    invoke-static {v3, v4}, LX/Arz;->a(LX/86q;LX/86q;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    goto :goto_2

    :cond_7
    move v1, v2

    goto :goto_4

    .line 2518813
    :pswitch_2
    sget-object v5, LX/86q;->START_RECORD_VIDEO_REQUESTED:LX/86q;

    if-eq v3, v5, :cond_8

    sget-object v5, LX/86q;->RECORDING_VIDEO:LX/86q;

    if-ne v3, v5, :cond_9

    :cond_8
    :goto_5
    invoke-static {v3, v4}, LX/Arz;->a(LX/86q;LX/86q;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    goto :goto_2

    :cond_9
    move v1, v2

    goto :goto_5

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
