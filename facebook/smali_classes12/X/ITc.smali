.class public final LX/ITc;
.super LX/DMC;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/DMC",
        "<",
        "LX/ITB;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/ITj;


# direct methods
.method public constructor <init>(LX/ITj;LX/DML;)V
    .locals 0

    .prologue
    .line 2579436
    iput-object p1, p0, LX/ITc;->a:LX/ITj;

    invoke-direct {p0, p2}, LX/DMC;-><init>(LX/DML;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 12

    .prologue
    .line 2579373
    check-cast p1, LX/ITB;

    .line 2579374
    iget-object v0, p0, LX/ITc;->a:LX/ITj;

    const/4 v2, 0x0

    const/4 v5, 0x2

    const/4 v3, 0x0

    .line 2579375
    if-eqz p1, :cond_4

    invoke-virtual {p1, v3}, LX/ITB;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    instance-of v1, v1, Lcom/facebook/fig/actionbar/FigActionBar;

    if-eqz v1, :cond_4

    .line 2579376
    invoke-virtual {p1, v3}, LX/ITB;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fig/actionbar/FigActionBar;

    .line 2579377
    :goto_0
    if-eqz v1, :cond_0

    .line 2579378
    invoke-virtual {v1, v5}, Lcom/facebook/fig/actionbar/FigActionBar;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    move-object v2, v1

    .line 2579379
    :cond_0
    if-eqz v2, :cond_1

    iget-boolean v1, v0, LX/ITj;->t:Z

    if-nez v1, :cond_1

    iget-object v1, v0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->p()Z

    move-result v1

    if-nez v1, :cond_3

    .line 2579380
    :cond_1
    :goto_1
    iget-object v0, p0, LX/ITc;->a:LX/ITj;

    .line 2579381
    iput-object p1, v0, LX/ITj;->o:LX/ITA;

    .line 2579382
    iget-object v0, p0, LX/ITc;->a:LX/ITj;

    iget-object v0, v0, LX/ITj;->m:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    iget-object v1, p0, LX/ITc;->a:LX/ITj;

    iget-object v1, v1, LX/ITj;->b:LX/IRb;

    iget-object v2, p0, LX/ITc;->a:LX/ITj;

    iget-object v2, v2, LX/ITj;->z:LX/DPp;

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2579383
    iput-object v0, p1, LX/ITB;->i:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 2579384
    iput-object v2, p1, LX/ITB;->o:LX/DPp;

    .line 2579385
    iget-object v5, p1, LX/ITB;->j:LX/ITJ;

    if-eqz v5, :cond_5

    .line 2579386
    iget-object v5, p1, LX/ITB;->j:LX/ITJ;

    .line 2579387
    iput-object v1, v5, LX/ITJ;->c:LX/IRb;

    .line 2579388
    :goto_2
    iget-object v5, p1, LX/ITB;->i:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v5, :cond_2

    iget-object v5, p1, LX/ITB;->i:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v5}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v5

    if-eqz v5, :cond_2

    iget-object v5, p1, LX/ITB;->i:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v5}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v5

    if-eqz v5, :cond_2

    iget-object v5, p1, LX/ITB;->i:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v5}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v5

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-eq v5, v6, :cond_2

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->c()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_6

    .line 2579389
    :cond_2
    new-array v3, v3, [LX/IX1;

    sget-object v5, LX/IX1;->CREATE_GROUP:LX/IX1;

    aput-object v5, v3, v4

    invoke-static {v3}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v3

    .line 2579390
    iget-object v4, p1, LX/ITB;->k:LX/IT7;

    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    invoke-interface {v4, v3}, LX/IT7;->a(LX/0Px;)V

    .line 2579391
    const/4 v3, 0x4

    invoke-virtual {p1, v3}, LX/ITB;->setVisibility(I)V

    .line 2579392
    :goto_3
    return-void

    .line 2579393
    :cond_3
    iget-object v1, v0, LX/ITj;->u:LX/0iA;

    new-instance v3, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v4, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->GROUP_MALL_VIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v3, v4}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    const-class v4, LX/IWx;

    invoke-virtual {v1, v3, v4}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v1

    check-cast v1, LX/IWx;

    .line 2579394
    if-eqz v1, :cond_1

    .line 2579395
    new-instance v1, LX/0hs;

    invoke-virtual {p1}, LX/ITB;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v3, v5}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 2579396
    const v3, 0x7f0838a3

    invoke-virtual {v1, v3}, LX/0hs;->a(I)V

    .line 2579397
    const v3, 0x7f0838a4

    invoke-virtual {v1, v3}, LX/0hs;->b(I)V

    .line 2579398
    sget-object v3, LX/3AV;->BELOW:LX/3AV;

    invoke-virtual {v1, v3}, LX/0ht;->a(LX/3AV;)V

    .line 2579399
    invoke-virtual {v1, v2}, LX/0ht;->f(Landroid/view/View;)V

    .line 2579400
    iget-object v1, v0, LX/ITj;->u:LX/0iA;

    invoke-virtual {v1}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v1

    const-string v2, "4558"

    invoke-virtual {v1, v2}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 2579401
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/ITj;->t:Z

    goto/16 :goto_1

    :cond_4
    move-object v1, v2

    goto/16 :goto_0

    .line 2579402
    :cond_5
    iget-object v5, p1, LX/ITB;->b:LX/ITK;

    invoke-virtual {v5, v1}, LX/ITK;->a(LX/IRb;)LX/ITJ;

    move-result-object v5

    iput-object v5, p1, LX/ITB;->j:LX/ITJ;

    goto/16 :goto_2

    .line 2579403
    :cond_6
    invoke-virtual {p1, v4}, LX/ITB;->setVisibility(I)V

    .line 2579404
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v5

    .line 2579405
    iget-object v6, p1, LX/ITB;->i:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v6}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->D()Z

    move-result v6

    iput-boolean v6, p1, LX/ITB;->l:Z

    .line 2579406
    iget-object v6, p1, LX/ITB;->i:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v6}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->q()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v6

    iput-object v6, p1, LX/ITB;->m:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 2579407
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v6

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-ne v6, v7, :cond_b

    .line 2579408
    sget-object v6, LX/IX1;->JOINED:LX/IX1;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2579409
    iget-object v8, p1, LX/ITB;->i:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v8, :cond_12

    iget-object v8, p1, LX/ITB;->i:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v8}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v8

    if-eqz v8, :cond_12

    iget-object v8, p1, LX/ITB;->i:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v8}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->v()J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-eqz v8, :cond_12

    const/4 v8, 0x1

    :goto_4
    move v6, v8

    .line 2579410
    if-nez v6, :cond_7

    .line 2579411
    sget-object v6, LX/IX1;->ADD_MEMBERS:LX/IX1;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2579412
    :cond_7
    :goto_5
    iget-object v6, p1, LX/ITB;->g:LX/3my;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/3my;->d(Lcom/facebook/graphql/enums/GraphQLGroupCategory;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 2579413
    sget-object v3, LX/IX1;->CREATE_SUBGROUP:LX/IX1;

    .line 2579414
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2579415
    :cond_8
    :goto_6
    sget-object v3, LX/IX1;->INFO_ICON:LX/IX1;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2579416
    const/4 v6, 0x0

    .line 2579417
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v4, v6

    :goto_7
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/IX1;

    .line 2579418
    iget v3, v3, LX/IX1;->showAsAction:I

    const/4 v8, 0x2

    if-ne v3, v8, :cond_13

    .line 2579419
    add-int/lit8 v3, v4, 0x1

    :goto_8
    move v4, v3

    .line 2579420
    goto :goto_7

    .line 2579421
    :cond_9
    const/4 v3, 0x4

    if-gt v4, v3, :cond_a

    const/4 v6, 0x1

    :cond_a
    move v3, v6

    .line 2579422
    invoke-static {v3}, LX/0PB;->checkState(Z)V

    .line 2579423
    iget-object v3, p1, LX/ITB;->k:LX/IT7;

    invoke-static {v5}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v4

    invoke-interface {v3, v4}, LX/IT7;->a(LX/0Px;)V

    goto/16 :goto_3

    .line 2579424
    :cond_b
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v6

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-ne v6, v7, :cond_c

    .line 2579425
    sget-object v6, LX/IX1;->REQUESTED:LX/IX1;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 2579426
    :cond_c
    sget-object v6, LX/IX1;->JOIN:LX/IX1;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 2579427
    :cond_d
    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->K()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v6

    .line 2579428
    if-eqz v6, :cond_f

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->CLOSED:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    if-eq v6, v7, :cond_e

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->OPEN:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    if-ne v6, v7, :cond_f

    .line 2579429
    :cond_e
    :goto_9
    iget-object v4, p1, LX/ITB;->c:LX/8ht;

    invoke-static {p1}, LX/ITB;->getSearchQuery(LX/ITB;)Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v6

    invoke-virtual {v4, v6}, LX/8ht;->f(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v4

    if-eqz v4, :cond_10

    invoke-static {p1}, LX/ITB;->b(LX/ITB;)Z

    move-result v4

    if-nez v4, :cond_10

    .line 2579430
    sget-object v3, LX/IX1;->SEARCH:LX/IX1;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    :cond_f
    move v3, v4

    .line 2579431
    goto :goto_9

    .line 2579432
    :cond_10
    invoke-static {p1}, LX/ITB;->b(LX/ITB;)Z

    move-result v4

    if-eqz v4, :cond_11

    .line 2579433
    sget-object v3, LX/IX1;->CHANNELS:LX/IX1;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 2579434
    :cond_11
    if-eqz v3, :cond_8

    .line 2579435
    sget-object v3, LX/IX1;->SHARE:LX/IX1;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_6

    :cond_12
    const/4 v8, 0x0

    goto/16 :goto_4

    :cond_13
    move v3, v4

    goto :goto_8
.end method
