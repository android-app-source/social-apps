.class public final LX/IDm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5OO;


# instance fields
.field public final synthetic a:J

.field public final synthetic b:J

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

.field public final synthetic e:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

.field public final synthetic f:LX/IDs;


# direct methods
.method public constructor <init>(LX/IDs;JJLjava/lang/String;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V
    .locals 0

    .prologue
    .line 2551155
    iput-object p1, p0, LX/IDm;->f:LX/IDs;

    iput-wide p2, p0, LX/IDm;->a:J

    iput-wide p4, p0, LX/IDm;->b:J

    iput-object p6, p0, LX/IDm;->c:Ljava/lang/String;

    iput-object p7, p0, LX/IDm;->d:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object p8, p0, LX/IDm;->e:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MenuItem;)Z
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2551156
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    const v3, 0x7f0d0218

    if-ne v2, v3, :cond_0

    .line 2551157
    iget-object v1, p0, LX/IDm;->f:LX/IDs;

    iget-wide v2, p0, LX/IDm;->a:J

    iget-wide v4, p0, LX/IDm;->b:J

    iget-object v6, p0, LX/IDm;->c:Ljava/lang/String;

    .line 2551158
    invoke-static/range {v1 .. v6}, LX/IDs;->a$redex0(LX/IDs;JJLjava/lang/String;)V

    .line 2551159
    :goto_0
    return v0

    .line 2551160
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    const v3, 0x7f0d0219

    if-ne v2, v3, :cond_1

    .line 2551161
    iget-object v1, p0, LX/IDm;->f:LX/IDs;

    iget-wide v2, p0, LX/IDm;->b:J

    invoke-static {v1, v2, v3, v0}, LX/IDs;->a$redex0(LX/IDs;JZ)V

    goto :goto_0

    .line 2551162
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    const v3, 0x7f0d021a

    if-ne v2, v3, :cond_2

    .line 2551163
    iget-object v1, p0, LX/IDm;->f:LX/IDs;

    iget-wide v2, p0, LX/IDm;->b:J

    .line 2551164
    sget-object v4, LX/0ax;->ai:Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 2551165
    iget-object v5, v1, LX/IDs;->h:LX/17W;

    iget-object v6, v1, LX/IDs;->c:Landroid/content/Context;

    invoke-virtual {v5, v6, v4}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2551166
    goto :goto_0

    .line 2551167
    :cond_2
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    const v3, 0x7f0d021b

    if-ne v2, v3, :cond_3

    .line 2551168
    iget-object v1, p0, LX/IDm;->f:LX/IDs;

    iget-wide v2, p0, LX/IDm;->b:J

    .line 2551169
    iget-object v4, v1, LX/2hY;->a:LX/2dj;

    sget-object v5, LX/2hC;->PROFILE_FRIEND_LIST:LX/2hC;

    invoke-virtual {v4, v2, v3, v5}, LX/2dj;->a(JLX/2hC;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2551170
    iget-object v4, v1, LX/IDs;->f:LX/2do;

    new-instance v5, LX/2iB;

    invoke-direct {v5, v2, v3}, LX/2iB;-><init>(J)V

    invoke-virtual {v4, v5}, LX/0b4;->a(LX/0b7;)V

    .line 2551171
    goto :goto_0

    .line 2551172
    :cond_3
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    const v3, 0x7f0d021c

    if-ne v2, v3, :cond_4

    .line 2551173
    iget-object v1, p0, LX/IDm;->f:LX/IDs;

    iget-wide v2, p0, LX/IDm;->b:J

    iget-object v4, p0, LX/IDm;->c:Ljava/lang/String;

    iget-object v5, p0, LX/IDm;->d:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iget-object v6, p0, LX/IDm;->e:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 2551174
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 2551175
    const-string v8, "profile_name"

    invoke-virtual {v7, v8, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2551176
    const-string v8, "friendship_status"

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2551177
    const-string v8, "subscribe_status"

    invoke-virtual {v6}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2551178
    sget-object v8, LX/0ax;->bO:Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    sget-object p0, LX/DHs;->SUGGESTIONS:LX/DHs;

    invoke-virtual {p0}, LX/DHs;->name()Ljava/lang/String;

    move-result-object p0

    iget-object p1, v1, LX/IDs;->e:LX/DHr;

    invoke-virtual {p1}, LX/DHr;->name()Ljava/lang/String;

    move-result-object p1

    invoke-static {v8, v9, p0, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 2551179
    iget-object v9, v1, LX/IDs;->h:LX/17W;

    iget-object p0, v1, LX/IDs;->c:Landroid/content/Context;

    invoke-virtual {v9, p0, v8, v7}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 2551180
    goto/16 :goto_0

    .line 2551181
    :cond_4
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    const v3, 0x7f0d021e

    if-ne v2, v3, :cond_5

    .line 2551182
    iget-object v2, p0, LX/IDm;->f:LX/IDs;

    iget-wide v4, p0, LX/IDm;->b:J

    invoke-static {v2, v4, v5, v1}, LX/IDs;->a$redex0(LX/IDs;JZ)V

    goto/16 :goto_0

    :cond_5
    move v0, v1

    .line 2551183
    goto/16 :goto_0
.end method
