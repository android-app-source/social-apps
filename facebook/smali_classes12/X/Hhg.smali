.class public final LX/Hhg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Hhh;


# direct methods
.method public constructor <init>(LX/Hhh;)V
    .locals 0

    .prologue
    .line 2496085
    iput-object p1, p0, LX/Hhg;->a:LX/Hhh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 11

    .prologue
    .line 2496086
    iget-object v0, p0, LX/Hhg;->a:LX/Hhh;

    const/4 v9, 0x0

    const/4 v10, 0x1

    .line 2496087
    iget-object v1, v0, LX/Hhh;->a:LX/0W3;

    sget-wide v3, LX/0X5;->ij:J

    const/4 v2, 0x0

    invoke-interface {v1, v3, v4, v2}, LX/0W4;->a(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2496088
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v9

    .line 2496089
    :goto_0
    move v0, v1

    .line 2496090
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 2496091
    :cond_0
    :try_start_0
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 2496092
    :try_start_1
    const-string v1, "_iptest_timeout"

    invoke-virtual {v5, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v7

    .line 2496093
    :goto_1
    :try_start_2
    const-string v1, "_iptest_expected_rc"

    invoke-virtual {v5, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_2

    move-result v8

    .line 2496094
    :goto_2
    :try_start_3
    invoke-virtual {v5}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/HhL;->valueOfName(Ljava/lang/String;)LX/HhL;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    move-result-object v2

    .line 2496095
    new-instance v1, LX/HhM;

    invoke-virtual {v5}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5}, Landroid/net/Uri;->getPort()I

    move-result v4

    invoke-virtual {v5}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-direct/range {v1 .. v8}, LX/HhM;-><init>(LX/HhL;Ljava/lang/String;ILjava/lang/String;Ljava/util/Collection;II)V

    .line 2496096
    invoke-virtual {v1}, LX/HhM;->d()V

    .line 2496097
    iget-object v2, v1, LX/HhM;->k:Ljava/lang/Throwable;

    move-object v2, v2

    .line 2496098
    if-eqz v2, :cond_1

    .line 2496099
    move v1, v9

    .line 2496100
    goto :goto_0

    .line 2496101
    :catch_0
    move v1, v10

    goto :goto_0

    .line 2496102
    :catch_1
    const/16 v7, 0x3a98

    goto :goto_1

    .line 2496103
    :catch_2
    const/16 v8, 0xc8

    goto :goto_2

    .line 2496104
    :catch_3
    move-exception v1

    .line 2496105
    const-class v2, LX/HhW;

    const-string v3, "Invalid scheme for paid test detected"

    invoke-static {v2, v3, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v1, v10

    .line 2496106
    goto :goto_0

    :cond_1
    move v1, v10

    .line 2496107
    goto :goto_0
.end method
