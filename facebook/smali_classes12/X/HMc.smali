.class public LX/HMc;
.super Lcom/facebook/widget/CustomViewPager;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public a:LX/HMc;

.field public b:LX/HMe;

.field public c:LX/HMb;

.field public d:Landroid/os/Handler;

.field public e:Ljava/lang/Runnable;

.field public f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 2457410
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomViewPager;-><init>(Landroid/content/Context;)V

    .line 2457411
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/HMc;->f:Z

    .line 2457412
    :try_start_0
    const-class v0, Landroid/support/v4/view/ViewPager;

    const-string v1, "o"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 2457413
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 2457414
    const-class v0, Landroid/support/v4/view/ViewPager;

    const-string v2, "f"

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 2457415
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 2457416
    new-instance v2, LX/HMb;

    invoke-virtual {p0}, LX/HMc;->getContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/animation/Interpolator;

    invoke-direct {v2, v3, v0}, LX/HMb;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v2, p0, LX/HMc;->c:LX/HMb;

    .line 2457417
    iget-object v0, p0, LX/HMc;->c:LX/HMb;

    invoke-virtual {v1, p0, v0}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2457418
    :goto_0
    iput-object p0, p0, LX/HMc;->a:LX/HMc;

    .line 2457419
    new-instance v0, LX/HMe;

    invoke-direct {v0, p1}, LX/HMe;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/HMc;->b:LX/HMe;

    .line 2457420
    iget-object v0, p0, LX/HMc;->b:LX/HMe;

    invoke-virtual {p0, v0}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2457421
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LX/HMc;->d:Landroid/os/Handler;

    .line 2457422
    new-instance v0, Lcom/facebook/pages/common/services/PagesServiceCarousel$1;

    invoke-direct {v0, p0}, Lcom/facebook/pages/common/services/PagesServiceCarousel$1;-><init>(LX/HMc;)V

    iput-object v0, p0, LX/HMc;->e:Ljava/lang/Runnable;

    .line 2457423
    iget-object v0, p0, LX/HMc;->d:Landroid/os/Handler;

    iget-object v1, p0, LX/HMc;->e:Ljava/lang/Runnable;

    const-wide/16 v2, 0xbb8

    const v4, -0x7947d743

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2457424
    return-void

    :catch_0
    goto :goto_0
.end method

.method private h()V
    .locals 2

    .prologue
    .line 2457425
    iget-object v0, p0, LX/HMc;->d:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HMc;->e:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 2457426
    iget-object v0, p0, LX/HMc;->d:Landroid/os/Handler;

    iget-object v1, p0, LX/HMc;->e:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 2457427
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/HMc;->f:Z

    .line 2457428
    :cond_0
    return-void
.end method


# virtual methods
.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x1bbd1505

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2457429
    invoke-super {p0}, Lcom/facebook/widget/CustomViewPager;->onDetachedFromWindow()V

    .line 2457430
    invoke-direct {p0}, LX/HMc;->h()V

    .line 2457431
    const/16 v1, 0x2d

    const v2, -0x24c6e24a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onMeasure(II)V
    .locals 2

    .prologue
    .line 2457432
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    int-to-float v0, v0

    const v1, 0x3fe38e39

    div-float/2addr v0, v1

    float-to-int v0, v0

    .line 2457433
    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 2457434
    invoke-super {p0, p1, v0}, Lcom/facebook/widget/CustomViewPager;->onMeasure(II)V

    .line 2457435
    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x661b6a42

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2457436
    invoke-direct {p0}, LX/HMc;->h()V

    .line 2457437
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomViewPager;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    const v2, -0x62185923

    invoke-static {v3, v3, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v1
.end method
