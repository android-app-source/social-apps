.class public final LX/HmM;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/widget/LinearLayout;

.field public final b:Landroid/widget/LinearLayout;

.field public final c:Landroid/widget/LinearLayout;

.field public final synthetic d:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;)V
    .locals 2

    .prologue
    .line 2500487
    iput-object p1, p0, LX/HmM;->d:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2500488
    const v0, 0x7f0d06d1

    invoke-virtual {p1, v0}, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/HmM;->a:Landroid/widget/LinearLayout;

    .line 2500489
    const v0, 0x7f0d06d3

    invoke-virtual {p1, v0}, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/HmM;->b:Landroid/widget/LinearLayout;

    .line 2500490
    const v0, 0x7f0d06d7

    invoke-virtual {p1, v0}, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/HmM;->c:Landroid/widget/LinearLayout;

    .line 2500491
    iget-object v0, p0, LX/HmM;->b:Landroid/widget/LinearLayout;

    const v1, 0x7f0d06d6

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    .line 2500492
    new-instance v1, LX/HmL;

    invoke-direct {v1, p0, p1}, LX/HmL;-><init>(LX/HmM;Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2500493
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 2500483
    iget-object v0, p0, LX/HmM;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2500484
    iget-object v0, p0, LX/HmM;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2500485
    iget-object v0, p0, LX/HmM;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2500486
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2500478
    invoke-direct {p0}, LX/HmM;->a()V

    .line 2500479
    iget-object v0, p0, LX/HmM;->a:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2500480
    iget-object v0, p0, LX/HmM;->a:Landroid/widget/LinearLayout;

    const v1, 0x7f0d06d2

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2500481
    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2500482
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Long;)V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 2500456
    invoke-direct {p0}, LX/HmM;->a()V

    .line 2500457
    iget-object v0, p0, LX/HmM;->c:Landroid/widget/LinearLayout;

    const v1, 0x7f0d06d8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2500458
    iget-object v1, p0, LX/HmM;->d:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    const v2, 0x7f08394e

    invoke-virtual {v1, v2}, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2500459
    iget-object v0, p0, LX/HmM;->c:Landroid/widget/LinearLayout;

    const v1, 0x7f0d06d9

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2500460
    if-eqz p2, :cond_1

    .line 2500461
    iget-object v1, p0, LX/HmM;->d:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    iget-object v1, v1, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->t:LX/0W3;

    sget-wide v2, LX/0X5;->iy:J

    invoke-interface {v1, v2, v3}, LX/0W4;->a(J)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2500462
    iget-object v1, p0, LX/HmM;->d:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    const v2, 0x7f08394f

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v10

    const/4 v4, 0x1

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sget-object v5, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->u:Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    div-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2500463
    :goto_0
    invoke-virtual {v0, v10}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2500464
    :goto_1
    iget-object v0, p0, LX/HmM;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2500465
    return-void

    .line 2500466
    :cond_0
    iget-object v1, p0, LX/HmM;->d:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    const v2, 0x7f083950

    invoke-virtual {v1, v2}, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2500467
    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2500468
    invoke-direct {p0}, LX/HmM;->a()V

    .line 2500469
    iget-object v0, p0, LX/HmM;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2500470
    iget-object v0, p0, LX/HmM;->b:Landroid/widget/LinearLayout;

    const v2, 0x7f0d06d4

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2500471
    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2500472
    iget-object v0, p0, LX/HmM;->b:Landroid/widget/LinearLayout;

    const v2, 0x7f0d06d5

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2500473
    invoke-virtual {v0, p2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2500474
    iget-object v0, p0, LX/HmM;->b:Landroid/widget/LinearLayout;

    const v2, 0x7f0d06d6

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    .line 2500475
    if-eqz p3, :cond_0

    :goto_0
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2500476
    return-void

    .line 2500477
    :cond_0
    const/16 v1, 0x8

    goto :goto_0
.end method
