.class public final LX/IjG;
.super LX/2h1;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2h1",
        "<",
        "Lcom/facebook/payments/p2p/service/model/cards/ValidatePaymentCardBinResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/IjH;


# direct methods
.method public constructor <init>(LX/IjH;)V
    .locals 0

    .prologue
    .line 2605508
    iput-object p1, p0, LX/IjG;->a:LX/IjH;

    invoke-direct {p0}, LX/2h1;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 4

    .prologue
    .line 2605509
    iget-object v0, p0, LX/IjG;->a:LX/IjH;

    const/4 v1, 0x0

    .line 2605510
    iput-object v1, v0, LX/IjH;->d:Ljava/lang/String;

    .line 2605511
    iget-object v0, p0, LX/IjG;->a:LX/IjH;

    .line 2605512
    iget-object v1, p1, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v1, v1

    .line 2605513
    sget-object v2, LX/1nY;->API_ERROR:LX/1nY;

    if-eq v1, v2, :cond_0

    .line 2605514
    :goto_0
    return-void

    .line 2605515
    :cond_0
    iget-object v1, p1, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v1, v1

    .line 2605516
    invoke-virtual {v1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/http/protocol/ApiErrorResult;

    .line 2605517
    invoke-virtual {v1}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 2605518
    :pswitch_0
    invoke-virtual {v1}, Lcom/facebook/http/protocol/ApiErrorResult;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/http/protocol/ApiErrorResult;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2605519
    iget-object v2, v0, LX/IjH;->f:LX/6qh;

    if-nez v2, :cond_1

    .line 2605520
    :goto_1
    goto :goto_0

    .line 2605521
    :cond_1
    new-instance v2, LX/6dy;

    iget-object v3, v0, LX/IjH;->a:Landroid/content/Context;

    const p0, 0x7f082c25

    invoke-virtual {v3, p0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object p0, v0, LX/IjH;->a:Landroid/content/Context;

    const p1, 0x7f082d03

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-direct {v2, v3, p0}, LX/6dy;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2605522
    iput-object v1, v2, LX/6dy;->d:Ljava/lang/String;

    .line 2605523
    move-object v2, v2

    .line 2605524
    iget-object v3, v0, LX/IjH;->a:Landroid/content/Context;

    const p0, 0x7f082c27

    invoke-virtual {v3, p0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2605525
    iput-object v3, v2, LX/6dy;->c:Ljava/lang/String;

    .line 2605526
    move-object v2, v2

    .line 2605527
    const/4 v3, 0x1

    .line 2605528
    iput-boolean v3, v2, LX/6dy;->f:Z

    .line 2605529
    move-object v2, v2

    .line 2605530
    invoke-virtual {v2}, LX/6dy;->a()Lcom/facebook/messaging/dialog/ConfirmActionParams;

    move-result-object v2

    .line 2605531
    invoke-static {v2}, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;->b(Lcom/facebook/messaging/dialog/ConfirmActionParams;)Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;

    move-result-object v2

    .line 2605532
    iget-object v3, v0, LX/IjH;->g:LX/6wv;

    .line 2605533
    iput-object v3, v2, Lcom/facebook/payments/dialog/PaymentsConfirmDialogFragment;->m:LX/6wv;

    .line 2605534
    iget-object v3, v0, LX/IjH;->f:LX/6qh;

    invoke-virtual {v3, v2}, LX/6qh;->a(Lcom/facebook/ui/dialogs/FbDialogFragment;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x2750
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final bridge synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2605535
    return-void
.end method
