.class public final LX/JOJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CEO;


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:LX/JOL;

.field public final synthetic c:Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;Landroid/content/Context;LX/JOL;)V
    .locals 0

    .prologue
    .line 2687256
    iput-object p1, p0, LX/JOJ;->c:Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;

    iput-object p2, p0, LX/JOJ;->a:Landroid/content/Context;

    iput-object p3, p0, LX/JOJ;->b:LX/JOL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/greetingcards/verve/model/VMAction;Landroid/view/View;)V
    .locals 4

    .prologue
    .line 2687257
    const-string v0, "button1"

    iget-object v1, p1, Lcom/facebook/greetingcards/verve/model/VMAction;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2687258
    iget-object v0, p0, LX/JOJ;->c:Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;

    iget-object v1, p0, LX/JOJ;->a:Landroid/content/Context;

    const/4 v2, 0x0

    const-string v3, "card"

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;->a(Lcom/facebook/feedplugins/greetingcard/GreetingCardPartDefinition;Landroid/content/Context;Lcom/facebook/greetingcards/model/GreetingCard;Ljava/lang/String;)Lcom/facebook/greetingcards/render/RenderCardFragment;

    move-result-object v0

    .line 2687259
    iget-object v1, p0, LX/JOJ;->b:LX/JOL;

    iget-object v1, v1, LX/JOL;->d:Lcom/facebook/greetingcards/render/GreetingCardPopoverFragment;

    if-eqz v1, :cond_1

    .line 2687260
    iget-object v1, p0, LX/JOJ;->b:LX/JOL;

    iget-object v1, v1, LX/JOL;->d:Lcom/facebook/greetingcards/render/GreetingCardPopoverFragment;

    invoke-virtual {v1, v0}, Lcom/facebook/greetingcards/render/GreetingCardPopoverFragment;->a(Lcom/facebook/greetingcards/render/FoldingPopoverFragment$BackPressAwareFragment;)V

    .line 2687261
    :cond_0
    :goto_0
    return-void

    .line 2687262
    :cond_1
    iget-object v1, p0, LX/JOJ;->b:LX/JOL;

    iget-object v1, v1, LX/JOL;->e:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    if-eqz v1, :cond_0

    .line 2687263
    iget-object v1, p0, LX/JOJ;->b:LX/JOL;

    iget-object v1, v1, LX/JOL;->e:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    invoke-virtual {v1, v0}, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->a(Lcom/facebook/greetingcards/render/FoldingPopoverFragment$BackPressAwareFragment;)V

    goto :goto_0
.end method
