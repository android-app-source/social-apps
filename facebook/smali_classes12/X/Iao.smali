.class public final LX/Iao;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;)V
    .locals 0

    .prologue
    .line 2591341
    iput-object p1, p0, LX/Iao;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x5a4743ab

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2591342
    iget-object v1, p0, LX/Iao;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;

    iget-object v1, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->f:LX/IZw;

    const-string v2, "click_request_code_button"

    invoke-virtual {v1, v2}, LX/IZw;->a(Ljava/lang/String;)V

    .line 2591343
    iget-object v1, p0, LX/Iao;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;

    .line 2591344
    iget-object v2, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->l:LX/4At;

    if-nez v2, :cond_0

    .line 2591345
    new-instance v2, LX/4At;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    const p1, 0x7f083a41

    invoke-direct {v2, v3, p1}, LX/4At;-><init>(Landroid/content/Context;I)V

    iput-object v2, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->l:LX/4At;

    .line 2591346
    :cond_0
    iget-object v2, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->l:LX/4At;

    invoke-virtual {v2}, LX/4At;->beginShowingProgress()V

    .line 2591347
    iget-object v1, p0, LX/Iao;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;

    iget-object v1, v1, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->b:LX/Ia3;

    iget-object v2, p0, LX/Iao;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;

    .line 2591348
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v2}, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->k(Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v2}, Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;->l(Lcom/facebook/messaging/business/nativesignup/view/BusinessRequestCodeFragment;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v2, v3

    .line 2591349
    new-instance v3, LX/Ian;

    invoke-direct {v3, p0}, LX/Ian;-><init>(LX/Iao;)V

    invoke-virtual {v1, v2, v3}, LX/Ia3;->a(Ljava/lang/String;LX/Ia2;)V

    .line 2591350
    const v1, -0x539f75eb

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
