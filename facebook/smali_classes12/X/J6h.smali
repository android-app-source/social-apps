.class public LX/J6h;
.super LX/2s5;
.source ""


# instance fields
.field private a:Landroid/content/res/Resources;

.field private b:LX/J3t;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/0gc;LX/J3t;)V
    .locals 0

    .prologue
    .line 2650049
    invoke-direct {p0, p2}, LX/2s5;-><init>(LX/0gc;)V

    .line 2650050
    iput-object p1, p0, LX/J6h;->a:Landroid/content/res/Resources;

    .line 2650051
    iput-object p3, p0, LX/J6h;->b:LX/J3t;

    .line 2650052
    return-void
.end method


# virtual methods
.method public final G_(I)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 2650053
    iget-object v0, p0, LX/J6h;->a:Landroid/content/res/Resources;

    const v1, 0x7f083901

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 2

    .prologue
    .line 2650043
    new-instance v0, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;

    invoke-direct {v0}, Lcom/facebook/privacy/checkup/ui/PrivacyGenericCheckupFragment;-><init>()V

    .line 2650044
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2650045
    const-string p0, "stepIndex"

    invoke-virtual {v1, p0, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2650046
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2650047
    move-object v0, v0

    .line 2650048
    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2650042
    iget-object v0, p0, LX/J6h;->b:LX/J3t;

    invoke-virtual {v0}, LX/J3t;->c()I

    move-result v0

    return v0
.end method
