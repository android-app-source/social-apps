.class public LX/JSE;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JSH;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JSE",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JSH;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2694895
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2694896
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JSE;->b:LX/0Zi;

    .line 2694897
    iput-object p1, p0, LX/JSE;->a:LX/0Ot;

    .line 2694898
    return-void
.end method

.method public static a(LX/0QB;)LX/JSE;
    .locals 4

    .prologue
    .line 2694899
    const-class v1, LX/JSE;

    monitor-enter v1

    .line 2694900
    :try_start_0
    sget-object v0, LX/JSE;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2694901
    sput-object v2, LX/JSE;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2694902
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2694903
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2694904
    new-instance v3, LX/JSE;

    const/16 p0, 0x2037

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JSE;-><init>(LX/0Ot;)V

    .line 2694905
    move-object v0, v3

    .line 2694906
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2694907
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JSE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2694908
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2694909
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2694887
    invoke-static {}, LX/1dS;->b()V

    .line 2694888
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;LX/1Dg;IILX/1no;LX/1X1;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x28ca081

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2694892
    iget-object v1, p0, LX/JSE;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    .line 2694893
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {p3, p4, v1, p5}, LX/1oC;->a(IIFLX/1no;)V

    .line 2694894
    const/16 v1, 0x1f

    const v2, -0x14bc66c1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2694889
    iget-object v0, p0, LX/JSE;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 2694890
    new-instance v0, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;

    invoke-direct {v0, p1}, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;-><init>(Landroid/content/Context;)V

    move-object v0, v0

    .line 2694891
    return-object v0
.end method

.method public final b(LX/1De;LX/1X1;)V
    .locals 11

    .prologue
    .line 2694910
    check-cast p2, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;

    .line 2694911
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v6

    .line 2694912
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v7

    .line 2694913
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v8

    .line 2694914
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v9

    .line 2694915
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v10

    .line 2694916
    iget-object v0, p0, LX/JSE;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JSH;

    iget-object v2, p2, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->a:LX/JSe;

    iget-object v3, p2, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v4, p2, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->c:LX/JTY;

    iget-object v5, p2, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->d:LX/1Pq;

    move-object v1, p1

    invoke-virtual/range {v0 .. v10}, LX/JSH;->a(LX/1De;LX/JSe;Lcom/facebook/feed/rows/core/props/FeedProps;LX/JTY;LX/1Pq;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;)V

    .line 2694917
    iget-object v0, v6, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2694918
    check-cast v0, LX/JSI;

    iput-object v0, p2, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->e:LX/JSI;

    .line 2694919
    invoke-static {v6}, LX/1cy;->a(LX/1np;)V

    .line 2694920
    iget-object v0, v7, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2694921
    check-cast v0, LX/82M;

    iput-object v0, p2, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->f:LX/82M;

    .line 2694922
    invoke-static {v7}, LX/1cy;->a(LX/1np;)V

    .line 2694923
    iget-object v0, v8, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2694924
    check-cast v0, LX/82L;

    iput-object v0, p2, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->g:LX/82L;

    .line 2694925
    invoke-static {v8}, LX/1cy;->a(LX/1np;)V

    .line 2694926
    iget-object v0, v9, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2694927
    check-cast v0, Landroid/view/View$OnClickListener;

    iput-object v0, p2, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->h:Landroid/view/View$OnClickListener;

    .line 2694928
    invoke-static {v9}, LX/1cy;->a(LX/1np;)V

    .line 2694929
    iget-object v0, v10, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2694930
    check-cast v0, LX/JSO;

    iput-object v0, p2, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->i:LX/JSO;

    .line 2694931
    invoke-static {v10}, LX/1cy;->a(LX/1np;)V

    .line 2694932
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2694878
    const/4 v0, 0x1

    return v0
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 2694879
    sget-object v0, LX/1mv;->VIEW:LX/1mv;

    return-object v0
.end method

.method public final g(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 9

    .prologue
    .line 2694880
    check-cast p3, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;

    .line 2694881
    iget-object v0, p0, LX/JSE;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JSH;

    move-object v1, p2

    check-cast v1, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;

    iget-object v2, p3, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->a:LX/JSe;

    iget-object v3, p3, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->j:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v4, p3, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->e:LX/JSI;

    iget-object v5, p3, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->f:LX/82M;

    iget-object v6, p3, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->g:LX/82L;

    iget-object v7, p3, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->h:Landroid/view/View$OnClickListener;

    iget-object v8, p3, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->i:LX/JSO;

    invoke-virtual/range {v0 .. v8}, LX/JSH;->a(Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;LX/JSe;Lcom/facebook/common/callercontext/CallerContext;LX/JSI;LX/82M;LX/82L;Landroid/view/View$OnClickListener;LX/JSO;)V

    .line 2694882
    return-void
.end method

.method public final h(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 8

    .prologue
    .line 2694883
    check-cast p3, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;

    .line 2694884
    iget-object v0, p0, LX/JSE;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JSH;

    move-object v1, p2

    check-cast v1, Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;

    iget-object v2, p3, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->a:LX/JSe;

    iget-object v3, p3, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->d:LX/1Pq;

    iget-object v4, p3, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->c:LX/JTY;

    iget-object v5, p3, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v6, p3, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->e:LX/JSI;

    iget-object v7, p3, Lcom/facebook/feedplugins/musicstory/MusicPlaybackComponent$MusicPlaybackComponentImpl;->g:LX/82L;

    invoke-virtual/range {v0 .. v7}, LX/JSH;->a(Lcom/facebook/feedplugins/musicstory/MusicPlaybackView;LX/JSe;LX/1Pq;LX/JTY;Lcom/facebook/feed/rows/core/props/FeedProps;LX/JSI;LX/82L;)V

    .line 2694885
    return-void
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 2694886
    const/16 v0, 0xf

    return v0
.end method
