.class public LX/I4Z;
.super LX/Cod;
.source ""

# interfaces
.implements LX/CnG;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cod",
        "<",
        "LX/I4U;",
        ">;",
        "Lcom/facebook/events/eventcollections/view/impl/block/DateHeaderBlockView;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:Lcom/facebook/richdocument/view/widget/RichTextView;

.field public final c:Lcom/facebook/richdocument/view/widget/RichTextView;

.field public d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2534220
    invoke-direct {p0, p1}, LX/Cod;-><init>(Landroid/view/View;)V

    .line 2534221
    const-class v0, LX/I4Z;

    invoke-static {v0, p0}, LX/I4Z;->a(Ljava/lang/Class;LX/02k;)V

    .line 2534222
    const v0, 0x7f0d0db4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    iput-object v0, p0, LX/I4Z;->b:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 2534223
    const v0, 0x7f0d0db5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    iput-object v0, p0, LX/I4Z;->c:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 2534224
    invoke-virtual {p0}, LX/Cod;->c()Landroid/view/View;

    move-result-object v0

    new-instance v1, LX/I4Y;

    invoke-direct {v1, p0}, LX/I4Y;-><init>(LX/I4Z;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2534225
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/I4Z;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object p0

    check-cast p0, Lcom/facebook/content/SecureContextHelper;

    iput-object p0, p1, LX/I4Z;->a:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2534226
    iget-object v0, p0, LX/I4Z;->b:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 2534227
    iget-object p0, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, p0

    .line 2534228
    invoke-virtual {v0, p1}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 2534229
    return-void
.end method
