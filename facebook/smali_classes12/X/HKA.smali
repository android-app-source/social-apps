.class public final LX/HKA;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/HKB;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/HKJ;

.field public b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

.field public c:LX/2km;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic d:LX/HKB;


# direct methods
.method public constructor <init>(LX/HKB;)V
    .locals 1

    .prologue
    .line 2453836
    iput-object p1, p0, LX/HKA;->d:LX/HKB;

    .line 2453837
    move-object v0, p1

    .line 2453838
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2453839
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2453840
    const-string v0, "PagePhotoAlbumComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2453841
    if-ne p0, p1, :cond_1

    .line 2453842
    :cond_0
    :goto_0
    return v0

    .line 2453843
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2453844
    goto :goto_0

    .line 2453845
    :cond_3
    check-cast p1, LX/HKA;

    .line 2453846
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2453847
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2453848
    if-eq v2, v3, :cond_0

    .line 2453849
    iget-object v2, p0, LX/HKA;->a:LX/HKJ;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/HKA;->a:LX/HKJ;

    iget-object v3, p1, LX/HKA;->a:LX/HKJ;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2453850
    goto :goto_0

    .line 2453851
    :cond_5
    iget-object v2, p1, LX/HKA;->a:LX/HKJ;

    if-nez v2, :cond_4

    .line 2453852
    :cond_6
    iget-object v2, p0, LX/HKA;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/HKA;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v3, p1, LX/HKA;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2453853
    goto :goto_0

    .line 2453854
    :cond_8
    iget-object v2, p1, LX/HKA;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    if-nez v2, :cond_7

    .line 2453855
    :cond_9
    iget-object v2, p0, LX/HKA;->c:LX/2km;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/HKA;->c:LX/2km;

    iget-object v3, p1, LX/HKA;->c:LX/2km;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2453856
    goto :goto_0

    .line 2453857
    :cond_a
    iget-object v2, p1, LX/HKA;->c:LX/2km;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
