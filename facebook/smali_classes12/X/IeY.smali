.class public LX/IeY;
.super LX/3Ml;
.source ""


# instance fields
.field private final c:LX/3Mn;

.field private final d:LX/2Or;

.field private final e:LX/2Oq;


# direct methods
.method public constructor <init>(LX/0Zr;LX/3Mn;LX/2Or;LX/2Oq;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2598788
    invoke-direct {p0, p1}, LX/3Ml;-><init>(LX/0Zr;)V

    .line 2598789
    iput-object p2, p0, LX/IeY;->c:LX/3Mn;

    .line 2598790
    iput-object p3, p0, LX/IeY;->d:LX/2Or;

    .line 2598791
    iput-object p4, p0, LX/IeY;->e:LX/2Oq;

    .line 2598792
    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/CharSequence;)LX/39y;
    .locals 6
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2598793
    const-string v0, "ContactPickerSmsGroupFilter.Filtering"

    const v1, 0x2c3be458

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2598794
    new-instance v1, LX/39y;

    invoke-direct {v1}, LX/39y;-><init>()V

    .line 2598795
    if-eqz p1, :cond_1

    :try_start_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 2598796
    :goto_0
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, LX/IeY;->d:LX/2Or;

    invoke-virtual {v2}, LX/2Or;->n()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, LX/IeY;->e:LX/2Oq;

    invoke-virtual {v2}, LX/2Oq;->a()Z

    move-result v2

    if-nez v2, :cond_2

    .line 2598797
    :cond_0
    invoke-static {p1}, LX/3Og;->a(Ljava/lang/CharSequence;)LX/3Og;

    move-result-object v0

    iput-object v0, v1, LX/39y;->a:Ljava/lang/Object;

    .line 2598798
    const/4 v0, -0x1

    iput v0, v1, LX/39y;->b:I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2598799
    const v0, 0x79d8c82e

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2598800
    const-string v0, "ContactPickerSmsGroupFilter"

    invoke-static {v0}, LX/0PR;->c(Ljava/lang/String;)V

    move-object v0, v1

    .line 2598801
    :goto_1
    return-object v0

    .line 2598802
    :cond_1
    :try_start_1
    const-string v0, ""

    goto :goto_0

    .line 2598803
    :cond_2
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2598804
    iget-object v2, p0, LX/IeY;->c:LX/3Mn;

    const/16 v4, 0x24

    invoke-virtual {v2, v0, v4}, LX/3Mn;->a(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v2

    .line 2598805
    const/4 v0, 0x0

    .line 2598806
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v0

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2598807
    const/16 v5, 0x1e

    if-ge v2, v5, :cond_3

    .line 2598808
    iget-object v5, p0, LX/3Ml;->b:LX/3Md;

    invoke-interface {v5, v0}, LX/3Md;->a(Ljava/lang/Object;)LX/3OQ;

    move-result-object v0

    .line 2598809
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2598810
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    .line 2598811
    goto :goto_2

    .line 2598812
    :cond_3
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2598813
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    iput v2, v1, LX/39y;->b:I

    .line 2598814
    invoke-static {p1, v0}, LX/3Og;->a(Ljava/lang/CharSequence;LX/0Px;)LX/3Og;

    move-result-object v0

    iput-object v0, v1, LX/39y;->a:Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2598815
    const v0, -0x10d51b25

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2598816
    const-string v0, "ContactPickerSmsGroupFilter"

    invoke-static {v0}, LX/0PR;->c(Ljava/lang/String;)V

    :goto_3
    move-object v0, v1

    .line 2598817
    goto :goto_1

    .line 2598818
    :catch_0
    move-exception v0

    .line 2598819
    :try_start_2
    const-string v2, "ContactPickerSmsGroupFilter"

    const-string v3, "Exception while filtering"

    invoke-static {v2, v3, v0}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2598820
    invoke-static {p1}, LX/3Og;->b(Ljava/lang/CharSequence;)LX/3Og;

    move-result-object v0

    iput-object v0, v1, LX/39y;->a:Ljava/lang/Object;

    .line 2598821
    const/4 v0, -0x1

    iput v0, v1, LX/39y;->b:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2598822
    const v0, -0x64836714

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2598823
    const-string v0, "ContactPickerSmsGroupFilter"

    invoke-static {v0}, LX/0PR;->c(Ljava/lang/String;)V

    goto :goto_3

    .line 2598824
    :catchall_0
    move-exception v0

    const v1, 0x50088e5c    # 9.1641242E9f

    invoke-static {v1}, LX/02m;->a(I)V

    .line 2598825
    const-string v1, "ContactPickerSmsGroupFilter"

    invoke-static {v1}, LX/0PR;->c(Ljava/lang/String;)V

    throw v0
.end method
