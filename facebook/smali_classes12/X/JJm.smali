.class public final LX/JJm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Zl;


# instance fields
.field public final synthetic a:LX/JJn;


# direct methods
.method public constructor <init>(LX/JJn;)V
    .locals 0

    .prologue
    .line 2679774
    iput-object p1, p0, LX/JJm;->a:LX/JJn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Map;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/246;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2679775
    iget-object v0, p0, LX/JJm;->a:LX/JJn;

    invoke-static {v0}, LX/JJn;->a(LX/JJn;)LX/5pY;

    move-result-object v0

    const-class v1, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;

    invoke-virtual {v0, v1}, LX/5pX;->a(Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;

    const-string v1, "deviceRequestsDiscoveryEvent"

    .line 2679776
    new-instance v3, Lcom/facebook/react/bridge/WritableNativeArray;

    invoke-direct {v3}, Lcom/facebook/react/bridge/WritableNativeArray;-><init>()V

    .line 2679777
    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/246;

    .line 2679778
    new-instance v5, Lcom/facebook/react/bridge/WritableNativeMap;

    invoke-direct {v5}, Lcom/facebook/react/bridge/WritableNativeMap;-><init>()V

    .line 2679779
    const-string p0, "appID"

    iget-object p1, v2, LX/246;->b:Ljava/lang/String;

    invoke-interface {v5, p0, p1}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2679780
    const-string p0, "appName"

    iget-object p1, v2, LX/246;->c:Ljava/lang/String;

    invoke-interface {v5, p0, p1}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2679781
    const-string p0, "deviceName"

    iget-object p1, v2, LX/246;->d:Ljava/lang/String;

    invoke-interface {v5, p0, p1}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2679782
    const-string p0, "imageUri"

    iget-object p1, v2, LX/246;->e:Ljava/lang/String;

    invoke-interface {v5, p0, p1}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2679783
    const-string p0, "nonce"

    iget-object p1, v2, LX/246;->f:Ljava/lang/String;

    invoke-interface {v5, p0, p1}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2679784
    const-string p0, "scope"

    iget-object p1, v2, LX/246;->g:Ljava/lang/String;

    invoke-interface {v5, p0, p1}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2679785
    const-string p0, "timestampExpire"

    iget p1, v2, LX/246;->h:I

    invoke-interface {v5, p0, p1}, LX/5pH;->putInt(Ljava/lang/String;I)V

    .line 2679786
    const-string p0, "userCode"

    iget-object p1, v2, LX/246;->i:Ljava/lang/String;

    invoke-interface {v5, p0, p1}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2679787
    const-string p0, "codeType"

    iget-object p1, v2, LX/246;->j:Ljava/lang/String;

    invoke-interface {v5, p0, p1}, LX/5pH;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2679788
    move-object v2, v5

    .line 2679789
    invoke-interface {v3, v2}, LX/5pD;->a(LX/5pH;)V

    goto :goto_0

    .line 2679790
    :cond_0
    move-object v2, v3

    .line 2679791
    invoke-interface {v0, v1, v2}, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;->emit(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2679792
    return-void
.end method
