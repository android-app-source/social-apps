.class public LX/JRW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/25K;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/17Q;

.field private b:LX/0Zb;


# direct methods
.method public constructor <init>(LX/17Q;LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2693425
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2693426
    iput-object p1, p0, LX/JRW;->a:LX/17Q;

    .line 2693427
    iput-object p2, p0, LX/JRW;->b:LX/0Zb;

    .line 2693428
    return-void
.end method

.method public static a(LX/0QB;)LX/JRW;
    .locals 5

    .prologue
    .line 2693429
    const-class v1, LX/JRW;

    monitor-enter v1

    .line 2693430
    :try_start_0
    sget-object v0, LX/JRW;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2693431
    sput-object v2, LX/JRW;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2693432
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2693433
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2693434
    new-instance p0, LX/JRW;

    invoke-static {v0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v3

    check-cast v3, LX/17Q;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-direct {p0, v3, v4}, LX/JRW;-><init>(LX/17Q;LX/0Zb;)V

    .line 2693435
    move-object v0, p0

    .line 2693436
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2693437
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JRW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2693438
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2693439
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(ILX/0Px;)V
    .locals 3

    .prologue
    .line 2693440
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 2693441
    :goto_0
    return-void

    .line 2693442
    :cond_0
    invoke-virtual {p2, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3hq;

    .line 2693443
    iget-object v1, v0, LX/3hq;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v1, v1

    .line 2693444
    invoke-static {v1}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    .line 2693445
    iget v2, v0, LX/3hq;->d:I

    move v2, v2

    .line 2693446
    iget-object p1, v0, LX/3hq;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, p1

    .line 2693447
    invoke-static {v0}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    invoke-static {v1, v2, v0}, LX/17Q;->a(ZILX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2693448
    iget-object v1, p0, LX/JRW;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0
.end method
