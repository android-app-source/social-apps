.class public final LX/I4z;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/location/ImmutableLocation;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;)V
    .locals 0

    .prologue
    .line 2534674
    iput-object p1, p0, LX/I4z;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2534675
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2534676
    check-cast p1, Lcom/facebook/location/ImmutableLocation;

    .line 2534677
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->l()Landroid/location/Location;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2534678
    :cond_0
    :goto_0
    return-void

    .line 2534679
    :cond_1
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 2534680
    new-instance v1, LX/0m9;

    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v2}, LX/0m9;-><init>(LX/0mC;)V

    .line 2534681
    const-string v2, "latitude"

    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->l()Landroid/location/Location;

    move-result-object v3

    invoke-virtual {v3}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, LX/0m9;->a(Ljava/lang/String;D)LX/0m9;

    .line 2534682
    const-string v2, "longitude"

    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->l()Landroid/location/Location;

    move-result-object v3

    invoke-virtual {v3}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, LX/0m9;->a(Ljava/lang/String;D)LX/0m9;

    .line 2534683
    const-string v2, "lat_lon"

    invoke-virtual {v0, v2, v1}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 2534684
    iget-object v1, p0, LX/I4z;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, LX/I4z;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;

    iget-object v3, v3, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->z:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2534685
    iput-object v0, v1, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryFragment;->z:Ljava/lang/String;

    .line 2534686
    goto :goto_0
.end method
