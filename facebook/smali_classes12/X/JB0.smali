.class public final LX/JB0;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 24

    .prologue
    .line 2657768
    const/16 v20, 0x0

    .line 2657769
    const/16 v19, 0x0

    .line 2657770
    const/16 v18, 0x0

    .line 2657771
    const/16 v17, 0x0

    .line 2657772
    const/16 v16, 0x0

    .line 2657773
    const/4 v15, 0x0

    .line 2657774
    const/4 v14, 0x0

    .line 2657775
    const/4 v13, 0x0

    .line 2657776
    const/4 v12, 0x0

    .line 2657777
    const/4 v11, 0x0

    .line 2657778
    const/4 v10, 0x0

    .line 2657779
    const/4 v9, 0x0

    .line 2657780
    const/4 v8, 0x0

    .line 2657781
    const/4 v7, 0x0

    .line 2657782
    const/4 v6, 0x0

    .line 2657783
    const/4 v5, 0x0

    .line 2657784
    const/4 v4, 0x0

    .line 2657785
    const/4 v3, 0x0

    .line 2657786
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v21

    sget-object v22, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-eq v0, v1, :cond_1

    .line 2657787
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2657788
    const/4 v3, 0x0

    .line 2657789
    :goto_0
    return v3

    .line 2657790
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2657791
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v21

    sget-object v22, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-eq v0, v1, :cond_11

    .line 2657792
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v21

    .line 2657793
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2657794
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v22

    sget-object v23, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    if-eq v0, v1, :cond_1

    if-eqz v21, :cond_1

    .line 2657795
    const-string v22, "__type__"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-nez v22, :cond_2

    const-string v22, "__typename"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_3

    .line 2657796
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v20

    goto :goto_1

    .line 2657797
    :cond_3
    const-string v22, "android_urls"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_4

    .line 2657798
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v19

    goto :goto_1

    .line 2657799
    :cond_4
    const-string v22, "app_section"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_5

    .line 2657800
    invoke-static/range {p0 .. p1}, LX/8sg;->a(LX/15w;LX/186;)I

    move-result v18

    goto :goto_1

    .line 2657801
    :cond_5
    const-string v22, "backing_application"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_6

    .line 2657802
    invoke-static/range {p0 .. p1}, LX/6RK;->a(LX/15w;LX/186;)I

    move-result v17

    goto :goto_1

    .line 2657803
    :cond_6
    const-string v22, "formattype"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_7

    .line 2657804
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v16

    goto :goto_1

    .line 2657805
    :cond_7
    const-string v22, "id"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_8

    .line 2657806
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    goto/16 :goto_1

    .line 2657807
    :cond_8
    const-string v22, "is_multi_company_group"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_9

    .line 2657808
    const/4 v5, 0x1

    .line 2657809
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v14

    goto/16 :goto_1

    .line 2657810
    :cond_9
    const-string v22, "is_viewer_coworker"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_a

    .line 2657811
    const/4 v4, 0x1

    .line 2657812
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v13

    goto/16 :goto_1

    .line 2657813
    :cond_a
    const-string v22, "is_work_user"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_b

    .line 2657814
    const/4 v3, 0x1

    .line 2657815
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v12

    goto/16 :goto_1

    .line 2657816
    :cond_b
    const-string v22, "name"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_c

    .line 2657817
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto/16 :goto_1

    .line 2657818
    :cond_c
    const-string v22, "page"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_d

    .line 2657819
    invoke-static/range {p0 .. p1}, LX/8sW;->a(LX/15w;LX/186;)I

    move-result v10

    goto/16 :goto_1

    .line 2657820
    :cond_d
    const-string v22, "profile_picture"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_e

    .line 2657821
    invoke-static/range {p0 .. p1}, LX/8sX;->a(LX/15w;LX/186;)I

    move-result v9

    goto/16 :goto_1

    .line 2657822
    :cond_e
    const-string v22, "redirection_info"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_f

    .line 2657823
    invoke-static/range {p0 .. p1}, LX/5Q3;->a(LX/15w;LX/186;)I

    move-result v8

    goto/16 :goto_1

    .line 2657824
    :cond_f
    const-string v22, "tag"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_10

    .line 2657825
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto/16 :goto_1

    .line 2657826
    :cond_10
    const-string v22, "url"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_0

    .line 2657827
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_1

    .line 2657828
    :cond_11
    const/16 v21, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 2657829
    const/16 v21, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2657830
    const/16 v20, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2657831
    const/16 v19, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2657832
    const/16 v18, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2657833
    const/16 v17, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2657834
    const/16 v16, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1, v15}, LX/186;->b(II)V

    .line 2657835
    if-eqz v5, :cond_12

    .line 2657836
    const/4 v5, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v14}, LX/186;->a(IZ)V

    .line 2657837
    :cond_12
    if-eqz v4, :cond_13

    .line 2657838
    const/4 v4, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v13}, LX/186;->a(IZ)V

    .line 2657839
    :cond_13
    if-eqz v3, :cond_14

    .line 2657840
    const/16 v3, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->a(IZ)V

    .line 2657841
    :cond_14
    const/16 v3, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 2657842
    const/16 v3, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 2657843
    const/16 v3, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 2657844
    const/16 v3, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 2657845
    const/16 v3, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 2657846
    const/16 v3, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 2657847
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2657848
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2657849
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 2657850
    if-eqz v0, :cond_0

    .line 2657851
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2657852
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2657853
    :cond_0
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2657854
    if-eqz v0, :cond_1

    .line 2657855
    const-string v0, "android_urls"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2657856
    invoke-virtual {p0, p1, v2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 2657857
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2657858
    if-eqz v0, :cond_2

    .line 2657859
    const-string v1, "app_section"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2657860
    invoke-static {p0, v0, p2}, LX/8sg;->a(LX/15i;ILX/0nX;)V

    .line 2657861
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2657862
    if-eqz v0, :cond_3

    .line 2657863
    const-string v1, "backing_application"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2657864
    invoke-static {p0, v0, p2, p3}, LX/6RK;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2657865
    :cond_3
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 2657866
    if-eqz v0, :cond_4

    .line 2657867
    const-string v0, "formattype"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2657868
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2657869
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2657870
    if-eqz v0, :cond_5

    .line 2657871
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2657872
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2657873
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2657874
    if-eqz v0, :cond_6

    .line 2657875
    const-string v1, "is_multi_company_group"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2657876
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2657877
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2657878
    if-eqz v0, :cond_7

    .line 2657879
    const-string v1, "is_viewer_coworker"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2657880
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2657881
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2657882
    if-eqz v0, :cond_8

    .line 2657883
    const-string v1, "is_work_user"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2657884
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2657885
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2657886
    if-eqz v0, :cond_9

    .line 2657887
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2657888
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2657889
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2657890
    if-eqz v0, :cond_a

    .line 2657891
    const-string v1, "page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2657892
    invoke-static {p0, v0, p2, p3}, LX/8sW;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2657893
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2657894
    if-eqz v0, :cond_b

    .line 2657895
    const-string v1, "profile_picture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2657896
    invoke-static {p0, v0, p2}, LX/8sX;->a(LX/15i;ILX/0nX;)V

    .line 2657897
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2657898
    if-eqz v0, :cond_c

    .line 2657899
    const-string v1, "redirection_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2657900
    invoke-static {p0, v0, p2, p3}, LX/5Q3;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2657901
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2657902
    if-eqz v0, :cond_d

    .line 2657903
    const-string v1, "tag"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2657904
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2657905
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2657906
    if-eqz v0, :cond_e

    .line 2657907
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2657908
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2657909
    :cond_e
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2657910
    return-void
.end method
