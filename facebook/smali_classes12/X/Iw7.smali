.class public final LX/Iw7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;

.field public final synthetic b:LX/IwC;


# direct methods
.method public constructor <init>(LX/IwC;Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;)V
    .locals 0

    .prologue
    .line 2629420
    iput-object p1, p0, LX/Iw7;->b:LX/IwC;

    iput-object p2, p0, LX/Iw7;->a:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x4cc02dc0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2629421
    iget-object v1, p0, LX/Iw7;->b:LX/IwC;

    iget-object v2, p0, LX/Iw7;->a:Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;

    .line 2629422
    invoke-virtual {v2}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->c()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 2629423
    iget-object v6, v1, LX/IwC;->j:LX/Iw6;

    .line 2629424
    iget-object v7, v6, LX/Iw6;->a:LX/0Zb;

    const-string v8, "tapped_page_on_landing"

    invoke-static {v8}, LX/Iw6;->d(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    const-string v9, "page_id"

    invoke-virtual {v8, v9, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    invoke-interface {v7, v8}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2629425
    sget-object v6, LX/0ax;->aE:Ljava/lang/String;

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 2629426
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 2629427
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->l()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v2}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v4

    :goto_0
    invoke-static {v7, v5, v8, v4}, LX/5ve;->b(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2629428
    iget-object v4, v1, LX/IwC;->i:LX/17W;

    iget-object v5, v1, LX/IwC;->f:Landroid/content/Context;

    invoke-virtual {v4, v5, v6, v7}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 2629429
    const v1, -0xaeb5b30

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2629430
    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method
