.class public LX/Hff;
.super LX/BcS;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TGroupInfo:",
        "Ljava/lang/Object;",
        ">",
        "LX/BcS;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/work/groupstab/WorkGroupsTabSectionSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Hff",
            "<TTGroupInfo;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/work/groupstab/WorkGroupsTabSectionSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2492220
    invoke-direct {p0}, LX/BcS;-><init>()V

    .line 2492221
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/Hff;->b:LX/0Zi;

    .line 2492222
    iput-object p1, p0, LX/Hff;->a:LX/0Ot;

    .line 2492223
    return-void
.end method

.method private a(ILjava/lang/Object;LX/BcP;LX/BcO;)LX/1X1;
    .locals 9

    .prologue
    .line 2492224
    check-cast p4, LX/Hfe;

    .line 2492225
    iget-object v0, p0, LX/Hff;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/work/groupstab/WorkGroupsTabSectionSpec;

    iget-object v3, p4, LX/Hfe;->c:LX/Hfw;

    iget-object v4, p4, LX/Hfe;->e:LX/95R;

    iget-object v5, p4, LX/Hfe;->f:LX/95R;

    iget-object v6, p4, LX/Hfe;->g:LX/95R;

    iget-object v7, p4, LX/Hfe;->h:LX/95R;

    move v1, p1

    move-object v2, p2

    move-object v8, p3

    invoke-virtual/range {v0 .. v8}, Lcom/facebook/work/groupstab/WorkGroupsTabSectionSpec;->a(ILjava/lang/Object;LX/Hfw;LX/95R;LX/95R;LX/95R;LX/95R;LX/BcP;)LX/1X1;

    move-result-object v0

    .line 2492226
    return-object v0
.end method

.method public static a(LX/0QB;)LX/Hff;
    .locals 4

    .prologue
    .line 2492209
    const-class v1, LX/Hff;

    monitor-enter v1

    .line 2492210
    :try_start_0
    sget-object v0, LX/Hff;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2492211
    sput-object v2, LX/Hff;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2492212
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2492213
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2492214
    new-instance v3, LX/Hff;

    const/16 p0, 0x38ce

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Hff;-><init>(LX/0Ot;)V

    .line 2492215
    move-object v0, v3

    .line 2492216
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2492217
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Hff;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2492218
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2492219
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/BcP;Z)V
    .locals 3

    .prologue
    .line 2492227
    invoke-virtual {p0}, LX/BcP;->i()LX/BcO;

    move-result-object v0

    .line 2492228
    if-nez v0, :cond_0

    .line 2492229
    :goto_0
    return-void

    .line 2492230
    :cond_0
    check-cast v0, LX/Hfe;

    .line 2492231
    new-instance v1, LX/Hfd;

    iget-object v2, v0, LX/Hfe;->j:LX/Hff;

    invoke-direct {v1, v2, p1}, LX/Hfd;-><init>(LX/Hff;Z)V

    move-object v0, v1

    .line 2492232
    invoke-virtual {p0, v0}, LX/BcP;->a(LX/BcR;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/BcQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x0

    .line 2492193
    invoke-static {}, LX/1dS;->b()V

    .line 2492194
    iget v0, p1, LX/BcQ;->b:I

    .line 2492195
    sparse-switch v0, :sswitch_data_0

    move-object v0, v6

    .line 2492196
    :goto_0
    return-object v0

    .line 2492197
    :sswitch_0
    check-cast p2, LX/BdG;

    .line 2492198
    iget v1, p2, LX/BdG;->a:I

    iget-object v2, p2, LX/BdG;->b:Ljava/lang/Object;

    iget-object v0, p1, LX/BcQ;->c:[Ljava/lang/Object;

    aget-object v0, v0, v4

    check-cast v0, LX/BcP;

    iget-object v3, p1, LX/BcQ;->a:LX/BcO;

    invoke-direct {p0, v1, v2, v0, v3}, LX/Hff;->a(ILjava/lang/Object;LX/BcP;LX/BcO;)LX/1X1;

    move-result-object v0

    goto :goto_0

    .line 2492199
    :sswitch_1
    check-cast p2, LX/BcM;

    .line 2492200
    iget-boolean v1, p2, LX/BcM;->a:Z

    iget-object v2, p2, LX/BcM;->b:LX/BcL;

    iget-object v3, p2, LX/BcM;->c:Ljava/lang/Throwable;

    iget-object v0, p1, LX/BcQ;->c:[Ljava/lang/Object;

    aget-object v4, v0, v4

    check-cast v4, LX/BcP;

    move-object v0, p0

    .line 2492201
    iget-object p0, v0, LX/Hff;->a:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    const/4 v5, 0x0

    .line 2492202
    sget-object p0, LX/Hfk;->b:[I

    invoke-virtual {v2}, LX/BcL;->ordinal()I

    move-result v0

    aget p0, p0, v0

    packed-switch p0, :pswitch_data_0

    .line 2492203
    :goto_1
    invoke-static {v4, v1, v2, v3}, LX/BcS;->a(LX/BcP;ZLX/BcL;Ljava/lang/Throwable;)V

    .line 2492204
    move-object v0, v6

    .line 2492205
    goto :goto_0

    .line 2492206
    :pswitch_0
    invoke-static {v4, v5}, LX/Hff;->a(LX/BcP;Z)V

    goto :goto_1

    .line 2492207
    :pswitch_1
    const/4 p0, 0x1

    invoke-static {v4, p0}, LX/Hff;->a(LX/BcP;Z)V

    goto :goto_1

    .line 2492208
    :pswitch_2
    invoke-static {v4, v5}, LX/Hff;->a(LX/BcP;Z)V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x6d7ae600 -> :sswitch_1
        -0x6b1be0aa -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(LX/BcO;LX/BcO;)V
    .locals 1

    .prologue
    .line 2492188
    check-cast p1, LX/Hfe;

    .line 2492189
    check-cast p2, LX/Hfe;

    .line 2492190
    iget-boolean v0, p1, LX/Hfe;->b:Z

    iput-boolean v0, p2, LX/Hfe;->b:Z

    .line 2492191
    iget-object v0, p1, LX/Hfe;->c:LX/Hfw;

    iput-object v0, p2, LX/Hfe;->c:LX/Hfw;

    .line 2492192
    return-void
.end method

.method public final a(LX/BcP;Ljava/util/List;LX/BcO;)V
    .locals 11

    .prologue
    .line 2492106
    check-cast p3, LX/Hfe;

    .line 2492107
    iget-object v0, p0, LX/Hff;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/work/groupstab/WorkGroupsTabSectionSpec;

    iget-boolean v3, p3, LX/Hfe;->b:Z

    iget-object v4, p3, LX/Hfe;->c:LX/Hfw;

    iget-object v5, p3, LX/Hfe;->d:LX/5K7;

    iget-object v6, p3, LX/Hfe;->e:LX/95R;

    iget-object v7, p3, LX/Hfe;->f:LX/95R;

    iget-object v8, p3, LX/Hfe;->g:LX/95R;

    iget-object v9, p3, LX/Hfe;->h:LX/95R;

    iget-object v10, p3, LX/Hfe;->i:LX/1X1;

    move-object v1, p1

    move-object v2, p2

    .line 2492108
    invoke-static {v1}, LX/Bce;->b(LX/BcP;)LX/Bcc;

    move-result-object p0

    invoke-virtual {p0, v10}, LX/Bcc;->a(LX/1X1;)LX/Bcc;

    move-result-object p0

    const-string p1, "gysj_section"

    invoke-virtual {p0, p1}, LX/Bcc;->b(Ljava/lang/String;)LX/Bcc;

    move-result-object p0

    invoke-virtual {p0}, LX/Bcc;->b()LX/BcO;

    move-result-object p0

    invoke-interface {v2, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2492109
    invoke-static {v1}, LX/Bce;->b(LX/BcP;)LX/Bcc;

    move-result-object p0

    iget-object p1, v0, Lcom/facebook/work/groupstab/WorkGroupsTabSectionSpec;->c:LX/Hg3;

    const/4 p2, 0x0

    .line 2492110
    new-instance p3, LX/Hg2;

    invoke-direct {p3, p1}, LX/Hg2;-><init>(LX/Hg3;)V

    .line 2492111
    sget-object v10, LX/Hg3;->a:LX/0Zi;

    invoke-virtual {v10}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, LX/Hg1;

    .line 2492112
    if-nez v10, :cond_0

    .line 2492113
    new-instance v10, LX/Hg1;

    invoke-direct {v10}, LX/Hg1;-><init>()V

    .line 2492114
    :cond_0
    invoke-static {v10, v1, p2, p2, p3}, LX/Hg1;->a$redex0(LX/Hg1;LX/1De;IILX/Hg2;)V

    .line 2492115
    move-object p3, v10

    .line 2492116
    move-object p2, p3

    .line 2492117
    move-object p1, p2

    .line 2492118
    invoke-virtual {p1}, LX/1X5;->d()LX/1X1;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/Bcc;->a(LX/1X1;)LX/Bcc;

    move-result-object p0

    const-string p1, "section_separator"

    invoke-virtual {p0, p1}, LX/Bcc;->b(Ljava/lang/String;)LX/Bcc;

    move-result-object p0

    invoke-virtual {p0}, LX/Bcc;->b()LX/BcO;

    move-result-object p0

    invoke-interface {v2, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2492119
    invoke-static {v1}, LX/Bce;->b(LX/BcP;)LX/Bcc;

    move-result-object p1

    iget-object p0, v0, Lcom/facebook/work/groupstab/WorkGroupsTabSectionSpec;->b:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Hg7;

    const/4 p2, 0x0

    .line 2492120
    new-instance p3, LX/Hg6;

    invoke-direct {p3, p0}, LX/Hg6;-><init>(LX/Hg7;)V

    .line 2492121
    sget-object v10, LX/Hg7;->a:LX/0Zi;

    invoke-virtual {v10}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, LX/Hg5;

    .line 2492122
    if-nez v10, :cond_1

    .line 2492123
    new-instance v10, LX/Hg5;

    invoke-direct {v10}, LX/Hg5;-><init>()V

    .line 2492124
    :cond_1
    invoke-static {v10, v1, p2, p2, p3}, LX/Hg5;->a$redex0(LX/Hg5;LX/1De;IILX/Hg6;)V

    .line 2492125
    move-object p3, v10

    .line 2492126
    move-object p2, p3

    .line 2492127
    move-object p0, p2

    .line 2492128
    iget-object p2, p0, LX/Hg5;->a:LX/Hg6;

    iput-object v4, p2, LX/Hg6;->a:LX/Hfw;

    .line 2492129
    iget-object p2, p0, LX/Hg5;->d:Ljava/util/BitSet;

    const/4 p3, 0x0

    invoke-virtual {p2, p3}, Ljava/util/BitSet;->set(I)V

    .line 2492130
    move-object p0, p0

    .line 2492131
    new-instance p2, LX/Hfg;

    invoke-direct {p2, v0, v1, v5}, LX/Hfg;-><init>(Lcom/facebook/work/groupstab/WorkGroupsTabSectionSpec;LX/BcP;LX/5K7;)V

    .line 2492132
    iget-object p3, p0, LX/Hg5;->a:LX/Hg6;

    iput-object p2, p3, LX/Hg6;->b:LX/Hfg;

    .line 2492133
    iget-object p3, p0, LX/Hg5;->d:Ljava/util/BitSet;

    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Ljava/util/BitSet;->set(I)V

    .line 2492134
    move-object p0, p0

    .line 2492135
    invoke-virtual {p0}, LX/1X5;->d()LX/1X1;

    move-result-object p0

    invoke-virtual {p1, p0}, LX/Bcc;->a(LX/1X1;)LX/Bcc;

    move-result-object p0

    const-string p1, "your_groups_header"

    invoke-virtual {p0, p1}, LX/Bcc;->b(Ljava/lang/String;)LX/Bcc;

    move-result-object p0

    const/4 p1, 0x1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    .line 2492136
    iget-object p2, p0, LX/Bcc;->a:LX/Bcd;

    iput-object p1, p2, LX/Bcd;->c:Ljava/lang/Boolean;

    .line 2492137
    move-object p0, p0

    .line 2492138
    invoke-virtual {p0}, LX/Bcc;->b()LX/BcO;

    move-result-object p0

    invoke-interface {v2, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2492139
    invoke-static {}, LX/Bck;->d()LX/Bck;

    move-result-object p0

    invoke-virtual {p0, v1}, LX/Bck;->c(LX/BcP;)LX/Bcg;

    move-result-object p0

    .line 2492140
    sget-object p1, LX/Hfw;->RECENT:LX/Hfw;

    if-ne v4, p1, :cond_4

    .line 2492141
    invoke-virtual {v7}, LX/95R;->a()LX/2kW;

    move-result-object p1

    .line 2492142
    :goto_0
    move-object p1, p1

    .line 2492143
    invoke-virtual {p0, p1}, LX/Bcg;->a(LX/2kW;)LX/Bcg;

    move-result-object p0

    .line 2492144
    const p1, -0x6b1be0aa

    const/4 p2, 0x1

    new-array p2, p2, [Ljava/lang/Object;

    const/4 p3, 0x0

    aput-object v1, p2, p3

    invoke-static {v1, p1, p2}, LX/BcS;->a(LX/BcP;I[Ljava/lang/Object;)LX/BcQ;

    move-result-object p1

    move-object p1, p1

    .line 2492145
    invoke-virtual {p0, p1}, LX/Bcg;->b(LX/BcQ;)LX/Bcg;

    move-result-object p0

    .line 2492146
    const p1, -0x6d7ae600

    const/4 p2, 0x1

    new-array p2, p2, [Ljava/lang/Object;

    const/4 p3, 0x0

    aput-object v1, p2, p3

    invoke-static {v1, p1, p2}, LX/BcS;->a(LX/BcP;I[Ljava/lang/Object;)LX/BcQ;

    move-result-object p1

    move-object p1, p1

    .line 2492147
    invoke-virtual {p0, p1}, LX/Bcg;->c(LX/BcQ;)LX/Bcg;

    move-result-object p0

    invoke-virtual {v4}, LX/Hfw;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/Bcg;->b(Ljava/lang/String;)LX/Bcg;

    move-result-object p0

    invoke-virtual {p0}, LX/Bcg;->b()LX/BcO;

    move-result-object p0

    invoke-interface {v2, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2492148
    if-eqz v3, :cond_3

    .line 2492149
    invoke-static {v1}, LX/Bce;->b(LX/BcP;)LX/Bcc;

    move-result-object p0

    const/4 p1, 0x0

    .line 2492150
    new-instance p2, LX/Hfy;

    invoke-direct {p2}, LX/Hfy;-><init>()V

    .line 2492151
    sget-object p3, LX/Hfz;->b:LX/0Zi;

    invoke-virtual {p3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, LX/Hfx;

    .line 2492152
    if-nez p3, :cond_2

    .line 2492153
    new-instance p3, LX/Hfx;

    invoke-direct {p3}, LX/Hfx;-><init>()V

    .line 2492154
    :cond_2
    invoke-static {p3, v1, p1, p1, p2}, LX/Hfx;->a$redex0(LX/Hfx;LX/1De;IILX/Hfy;)V

    .line 2492155
    move-object p2, p3

    .line 2492156
    move-object p1, p2

    .line 2492157
    move-object p1, p1

    .line 2492158
    invoke-virtual {p1}, LX/1X5;->d()LX/1X1;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/Bcc;->a(LX/1X1;)LX/Bcc;

    move-result-object p0

    invoke-virtual {p0}, LX/Bcc;->b()LX/BcO;

    move-result-object p0

    invoke-interface {v2, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2492159
    :cond_3
    return-void

    .line 2492160
    :cond_4
    sget-object p1, LX/Hfw;->ALL:LX/Hfw;

    if-ne v4, p1, :cond_5

    .line 2492161
    invoke-virtual {v6}, LX/95R;->a()LX/2kW;

    move-result-object p1

    goto :goto_0

    .line 2492162
    :cond_5
    sget-object p1, LX/Hfw;->FAVORITED:LX/Hfw;

    if-ne v4, p1, :cond_6

    .line 2492163
    invoke-virtual {v8}, LX/95R;->a()LX/2kW;

    move-result-object p1

    goto :goto_0

    .line 2492164
    :cond_6
    sget-object p1, LX/Hfw;->TOP:LX/Hfw;

    if-ne v4, p1, :cond_7

    .line 2492165
    invoke-virtual {v9}, LX/95R;->a()LX/2kW;

    move-result-object p1

    goto/16 :goto_0

    .line 2492166
    :cond_7
    const/4 p1, 0x0

    goto/16 :goto_0
.end method

.method public final c(LX/BcP;LX/BcO;)V
    .locals 4

    .prologue
    .line 2492167
    check-cast p2, LX/Hfe;

    .line 2492168
    invoke-static {}, LX/BcS;->a()LX/1np;

    move-result-object v1

    .line 2492169
    invoke-static {}, LX/BcS;->a()LX/1np;

    move-result-object v2

    .line 2492170
    iget-object v0, p0, LX/Hff;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/work/groupstab/WorkGroupsTabSectionSpec;

    const/4 p1, 0x0

    .line 2492171
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 2492172
    iput-object v3, v1, LX/1np;->a:Ljava/lang/Object;

    .line 2492173
    iget-object v3, v0, Lcom/facebook/work/groupstab/WorkGroupsTabSectionSpec;->e:LX/0Uh;

    const/16 p0, 0x679

    invoke-virtual {v3, p0, p1}, LX/0Uh;->a(IZ)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2492174
    sget-object v3, LX/Hfw;->TOP:LX/Hfw;

    .line 2492175
    iput-object v3, v2, LX/1np;->a:Ljava/lang/Object;

    .line 2492176
    :goto_0
    iget-object p0, v0, Lcom/facebook/work/groupstab/WorkGroupsTabSectionSpec;->f:LX/HgN;

    .line 2492177
    iget-object v3, v2, LX/1np;->a:Ljava/lang/Object;

    move-object v3, v3

    .line 2492178
    check-cast v3, LX/Hfw;

    .line 2492179
    iput-object v3, p0, LX/HgN;->b:LX/Hfw;

    .line 2492180
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2492181
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p2, LX/Hfe;->b:Z

    .line 2492182
    iget-object v0, v2, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2492183
    check-cast v0, LX/Hfw;

    iput-object v0, p2, LX/Hfe;->c:LX/Hfw;

    .line 2492184
    return-void

    .line 2492185
    :cond_0
    sget-object v3, LX/Hfw;->RECENT:LX/Hfw;

    .line 2492186
    iput-object v3, v2, LX/1np;->a:Ljava/lang/Object;

    .line 2492187
    goto :goto_0
.end method
