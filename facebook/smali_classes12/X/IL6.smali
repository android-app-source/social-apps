.class public LX/IL6;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/DKB;

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/DKA;


# direct methods
.method public constructor <init>(LX/DKB;LX/DKA;)V
    .locals 0
    .param p1    # LX/DKB;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2567672
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2567673
    iput-object p1, p0, LX/IL6;->a:LX/DKB;

    .line 2567674
    iput-object p2, p0, LX/IL6;->c:LX/DKA;

    .line 2567675
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 2

    .prologue
    .line 2567667
    new-instance v0, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;-><init>(Landroid/content/Context;)V

    .line 2567668
    const/4 v1, 0x1

    .line 2567669
    iput-boolean v1, v0, LX/DaX;->t:Z

    .line 2567670
    new-instance v1, LX/IL5;

    invoke-direct {v1, v0}, LX/IL5;-><init>(Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;)V

    .line 2567671
    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 2567657
    iget-object v0, p0, LX/IL6;->b:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;

    move-object v8, v0

    .line 2567658
    if-eqz v8, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Tp;->b(Z)V

    .line 2567659
    check-cast p1, LX/IL5;

    .line 2567660
    if-nez p1, :cond_1

    .line 2567661
    :goto_1
    return-void

    :cond_0
    move v0, v4

    .line 2567662
    goto :goto_0

    .line 2567663
    :cond_1
    iget-object v1, p1, LX/IL5;->l:Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;

    invoke-virtual {v8}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;->a()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v8}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;->b()Ljava/lang/String;

    move-result-object v5

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    invoke-virtual/range {v1 .. v7}, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;D)V

    .line 2567664
    iget-object v0, p1, LX/IL5;->l:Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;

    iget-object v1, p0, LX/IL6;->c:LX/DKA;

    invoke-virtual {v8}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v4}, LX/DKA;->a(Ljava/lang/String;Z)I

    move-result v1

    invoke-virtual {v0, v1}, LX/DaX;->setPlaceholderImage(I)V

    .line 2567665
    iget-object v0, p1, LX/IL5;->l:Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;

    iget-object v1, p0, LX/IL6;->a:LX/DKB;

    invoke-virtual {v8}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/DKB;->c(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;->a(Z)V

    .line 2567666
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    new-instance v1, LX/IL4;

    invoke-direct {v1, p0, v8, p1}, LX/IL4;-><init>(LX/IL6;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;LX/IL5;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2567654
    iget-object v0, p0, LX/IL6;->b:LX/0Px;

    if-nez v0, :cond_0

    .line 2567655
    const/4 v0, 0x0

    .line 2567656
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/IL6;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    goto :goto_0
.end method
