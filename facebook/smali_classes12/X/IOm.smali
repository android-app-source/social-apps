.class public final LX/IOm;
.super LX/DMC;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/DMC",
        "<",
        "Landroid/widget/LinearLayout;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel$GroupsModel$EdgesModel;

.field public final synthetic b:LX/IOb;

.field public final synthetic c:Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsListAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsListAdapter;LX/DML;Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel$GroupsModel$EdgesModel;LX/IOb;)V
    .locals 0

    .prologue
    .line 2572934
    iput-object p1, p0, LX/IOm;->c:Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsListAdapter;

    iput-object p3, p0, LX/IOm;->a:Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel$GroupsModel$EdgesModel;

    iput-object p4, p0, LX/IOm;->b:LX/IOb;

    invoke-direct {p0, p2}, LX/DMC;-><init>(LX/DML;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 8

    .prologue
    .line 2572935
    check-cast p1, Landroid/widget/LinearLayout;

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2572936
    const v0, 0x7f0d0a22

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 2572937
    iget-object v1, p0, LX/IOm;->a:Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel$GroupsModel$EdgesModel;

    invoke-virtual {v1}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel$GroupsModel$EdgesModel;->a()Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2572938
    const v0, 0x7f0d156b

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2572939
    iget-object v1, p0, LX/IOm;->a:Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel$GroupsModel$EdgesModel;

    invoke-virtual {v1}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel$GroupsModel$EdgesModel;->a()Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel;->j()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_1

    .line 2572940
    iget-object v1, p0, LX/IOm;->a:Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel$GroupsModel$EdgesModel;

    invoke-virtual {v1}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel$GroupsModel$EdgesModel;->a()Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel;->j()LX/1vs;

    move-result-object v1

    iget-object v4, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2572941
    const-class v5, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v4, v1, v3, v5}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    if-eqz v1, :cond_3

    .line 2572942
    iget-object v1, p0, LX/IOm;->a:Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel$GroupsModel$EdgesModel;

    invoke-virtual {v1}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel$GroupsModel$EdgesModel;->a()Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel;->j()LX/1vs;

    move-result-object v1

    iget-object v4, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const-class v5, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v4, v1, v3, v5}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v1}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel$CoverPhotoModel$PhotoModel;->j()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    .line 2572943
    if-eqz v1, :cond_2

    move v1, v2

    :goto_1
    if-eqz v1, :cond_5

    .line 2572944
    iget-object v1, p0, LX/IOm;->a:Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel$GroupsModel$EdgesModel;

    invoke-virtual {v1}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel$GroupsModel$EdgesModel;->a()Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel;->j()LX/1vs;

    move-result-object v1

    iget-object v4, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const-class v5, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v4, v1, v3, v5}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v1}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel$CoverPhotoModel$PhotoModel;->j()LX/1vs;

    move-result-object v1

    iget-object v4, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2572945
    invoke-virtual {v4, v1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    move v1, v2

    :goto_2
    if-eqz v1, :cond_6

    .line 2572946
    iget-object v1, p0, LX/IOm;->a:Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel$GroupsModel$EdgesModel;

    invoke-virtual {v1}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel$GroupsModel$EdgesModel;->a()Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel;->j()LX/1vs;

    move-result-object v1

    iget-object v4, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const-class v5, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v4, v1, v3, v5}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v1}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel$CoverPhotoModel$PhotoModel;->j()LX/1vs;

    move-result-object v1

    iget-object v4, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-virtual {v4, v1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    .line 2572947
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v4, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsListAdapter;->g:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2572948
    :goto_3
    iget-object v0, p0, LX/IOm;->a:Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel$GroupsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel$GroupsModel$EdgesModel;->a()Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel;->k()Ljava/lang/String;

    move-result-object v0

    .line 2572949
    iget-object v1, p0, LX/IOm;->b:LX/IOb;

    invoke-virtual {v1, v0}, LX/IOb;->c(Ljava/lang/String;)Z

    move-result v1

    .line 2572950
    const v4, 0x7f0d156c

    invoke-virtual {p1, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 2572951
    invoke-virtual {v4}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 2572952
    iget-object v6, p0, LX/IOm;->c:Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsListAdapter;

    iget-object v6, v6, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsListAdapter;->c:Landroid/content/res/Resources;

    const v7, 0x7f0a05f0

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    sget-object v7, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v5, v6, v7}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 2572953
    if-eqz v1, :cond_7

    .line 2572954
    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2572955
    iget-object v1, p0, LX/IOm;->c:Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsListAdapter;

    iget-object v1, v1, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsListAdapter;->c:Landroid/content/res/Resources;

    const v4, 0x7f082824

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v5, p0, LX/IOm;->a:Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel$GroupsModel$EdgesModel;

    invoke-virtual {v5}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel$GroupsModel$EdgesModel;->a()Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel;->l()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v3

    invoke-virtual {v1, v4, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2572956
    :goto_4
    new-instance v1, LX/IOl;

    invoke-direct {v1, p0, v0}, LX/IOl;-><init>(LX/IOm;Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2572957
    return-void

    :cond_0
    move v1, v3

    .line 2572958
    goto/16 :goto_0

    :cond_1
    move v1, v3

    goto/16 :goto_0

    :cond_2
    move v1, v3

    goto/16 :goto_1

    :cond_3
    move v1, v3

    goto/16 :goto_1

    :cond_4
    move v1, v3

    goto/16 :goto_2

    :cond_5
    move v1, v3

    goto/16 :goto_2

    .line 2572959
    :cond_6
    const/4 v1, 0x0

    sget-object v4, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsListAdapter;->g:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_3

    .line 2572960
    :cond_7
    const/4 v1, 0x4

    invoke-virtual {v4, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2572961
    iget-object v1, p0, LX/IOm;->c:Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsListAdapter;

    iget-object v1, v1, Lcom/facebook/groups/fb4a/addtogroups/FB4AAddToGroupsListAdapter;->c:Landroid/content/res/Resources;

    const v4, 0x7f082825

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v5, p0, LX/IOm;->a:Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel$GroupsModel$EdgesModel;

    invoke-virtual {v5}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$FetchUserGroupsForProfileAddToGroupsModel$GroupsModel$EdgesModel;->a()Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/groups/fb4a/addtogroups/protocol/FB4AAddToGroupsModels$GroupsForProfileAddToGroupFragmentModel;->l()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v3

    invoke-virtual {v1, v4, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_4
.end method
