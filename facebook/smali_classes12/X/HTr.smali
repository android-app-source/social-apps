.class public final LX/HTr;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 2471070
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 2471071
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2471072
    :goto_0
    return v1

    .line 2471073
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2471074
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_4

    .line 2471075
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2471076
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2471077
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 2471078
    const-string v4, "edges"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2471079
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2471080
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_ARRAY:LX/15z;

    if-ne v3, v4, :cond_2

    .line 2471081
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_ARRAY:LX/15z;

    if-eq v3, v4, :cond_2

    .line 2471082
    invoke-static {p0, p1}, LX/HTq;->b(LX/15w;LX/186;)I

    move-result v3

    .line 2471083
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2471084
    :cond_2
    invoke-static {v2, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v2

    move v2, v2

    .line 2471085
    goto :goto_1

    .line 2471086
    :cond_3
    const-string v4, "page_info"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2471087
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2471088
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v5, :cond_b

    .line 2471089
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2471090
    :goto_3
    move v0, v3

    .line 2471091
    goto :goto_1

    .line 2471092
    :cond_4
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2471093
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2471094
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2471095
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 2471096
    :cond_6
    const-string v8, "has_next_page"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 2471097
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v5, v0

    move v0, v4

    .line 2471098
    :cond_7
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_9

    .line 2471099
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 2471100
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2471101
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_7

    if-eqz v7, :cond_7

    .line 2471102
    const-string v8, "end_cursor"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 2471103
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_4

    .line 2471104
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_4

    .line 2471105
    :cond_9
    const/4 v7, 0x2

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 2471106
    invoke-virtual {p1, v3, v6}, LX/186;->b(II)V

    .line 2471107
    if-eqz v0, :cond_a

    .line 2471108
    invoke-virtual {p1, v4, v5}, LX/186;->a(IZ)V

    .line 2471109
    :cond_a
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_3

    :cond_b
    move v0, v3

    move v5, v3

    move v6, v3

    goto :goto_4
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2471110
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2471111
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2471112
    if-eqz v0, :cond_1

    .line 2471113
    const-string v1, "edges"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2471114
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2471115
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 2471116
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/HTq;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2471117
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2471118
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2471119
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2471120
    if-eqz v0, :cond_4

    .line 2471121
    const-string v1, "page_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2471122
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2471123
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2471124
    if-eqz v1, :cond_2

    .line 2471125
    const-string v2, "end_cursor"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2471126
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2471127
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->b(II)Z

    move-result v1

    .line 2471128
    if-eqz v1, :cond_3

    .line 2471129
    const-string v2, "has_next_page"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2471130
    invoke-virtual {p2, v1}, LX/0nX;->a(Z)V

    .line 2471131
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2471132
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2471133
    return-void
.end method
