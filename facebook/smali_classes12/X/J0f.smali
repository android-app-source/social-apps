.class public LX/J0f;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionPaymentCardParams;",
        "Lcom/facebook/payments/p2p/model/PaymentCard;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/J0f;


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2637496
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2637497
    return-void
.end method

.method public static a(LX/0QB;)LX/J0f;
    .locals 3

    .prologue
    .line 2637527
    sget-object v0, LX/J0f;->b:LX/J0f;

    if-nez v0, :cond_1

    .line 2637528
    const-class v1, LX/J0f;

    monitor-enter v1

    .line 2637529
    :try_start_0
    sget-object v0, LX/J0f;->b:LX/J0f;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2637530
    if-eqz v2, :cond_0

    .line 2637531
    :try_start_1
    new-instance v0, LX/J0f;

    invoke-direct {v0}, LX/J0f;-><init>()V

    .line 2637532
    move-object v0, v0

    .line 2637533
    sput-object v0, LX/J0f;->b:LX/J0f;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2637534
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2637535
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2637536
    :cond_1
    sget-object v0, LX/J0f;->b:LX/J0f;

    return-object v0

    .line 2637537
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2637538
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 2637539
    check-cast p1, Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionPaymentCardParams;

    .line 2637540
    iget-object v0, p1, Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionPaymentCardParams;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2637541
    const-string v1, "node(%s) { payment_method { credential_id, number, first_name, last_name, expire_month, expire_year, association, address { postal_code } } }"

    invoke-static {v1, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/J0f;->a:Ljava/lang/String;

    .line 2637542
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 2637543
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "q"

    iget-object v3, p0, LX/J0f;->a:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2637544
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "fetchTransactionPaymentCard"

    .line 2637545
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 2637546
    move-object v1, v1

    .line 2637547
    const-string v2, "GET"

    .line 2637548
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 2637549
    move-object v1, v1

    .line 2637550
    const-string v2, "graphql"

    .line 2637551
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 2637552
    move-object v1, v1

    .line 2637553
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 2637554
    move-object v0, v1

    .line 2637555
    sget-object v1, LX/14S;->JSONPARSER:LX/14S;

    .line 2637556
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2637557
    move-object v0, v0

    .line 2637558
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2637498
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2637499
    invoke-virtual {p2}, LX/1pN;->e()LX/15w;

    move-result-object v0

    .line 2637500
    invoke-virtual {v0}, LX/15w;->c()LX/15z;

    .line 2637501
    :goto_0
    invoke-virtual {v0}, LX/15w;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v1

    const-string v2, "payment_method"

    if-eq v1, v2, :cond_0

    .line 2637502
    invoke-virtual {v0}, LX/15w;->c()LX/15z;

    goto :goto_0

    .line 2637503
    :cond_0
    invoke-virtual {v0}, LX/15w;->h()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2637504
    invoke-virtual {v0}, LX/15w;->c()LX/15z;

    .line 2637505
    const-class v1, Lcom/facebook/payments/p2p/model/P2pCreditCard;

    invoke-virtual {v0, v1}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/P2pCreditCard;

    .line 2637506
    if-eqz v0, :cond_1

    .line 2637507
    invoke-static {}, Lcom/facebook/payments/p2p/model/PaymentCard;->newBuilder()LX/DtG;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/P2pCreditCard;->f()Lcom/facebook/payments/p2p/model/Address;

    move-result-object v2

    .line 2637508
    iput-object v2, v1, LX/DtG;->e:Lcom/facebook/payments/p2p/model/Address;

    .line 2637509
    move-object v1, v1

    .line 2637510
    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/P2pCreditCard;->g()Ljava/lang/String;

    move-result-object v2

    .line 2637511
    iput-object v2, v1, LX/DtG;->f:Ljava/lang/String;

    .line 2637512
    move-object v1, v1

    .line 2637513
    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/P2pCreditCard;->b()J

    move-result-wide v2

    .line 2637514
    iput-wide v2, v1, LX/DtG;->a:J

    .line 2637515
    move-object v1, v1

    .line 2637516
    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/P2pCreditCard;->d()I

    move-result v2

    .line 2637517
    iput v2, v1, LX/DtG;->c:I

    .line 2637518
    move-object v1, v1

    .line 2637519
    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/P2pCreditCard;->d()I

    move-result v2

    .line 2637520
    iput v2, v1, LX/DtG;->d:I

    .line 2637521
    move-object v1, v1

    .line 2637522
    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/P2pCreditCard;->c()Ljava/lang/String;

    move-result-object v0

    .line 2637523
    iput-object v0, v1, LX/DtG;->b:Ljava/lang/String;

    .line 2637524
    move-object v0, v1

    .line 2637525
    new-instance v1, Lcom/facebook/payments/p2p/model/PaymentCard;

    invoke-direct {v1, v0}, Lcom/facebook/payments/p2p/model/PaymentCard;-><init>(LX/DtG;)V

    move-object v0, v1

    .line 2637526
    :goto_1
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
