.class public final LX/I2M;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/events/dashboard/multirow/EventsBirthdayRowPartDefinition;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/StringBuilder;

.field public g:Z

.field public h:Z

.field public i:Z


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/multirow/EventsBirthdayRowPartDefinition;Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;Lcom/facebook/base/fragment/FbFragment;Z)V
    .locals 9

    .prologue
    .line 2530176
    iput-object p1, p0, LX/I2M;->a:Lcom/facebook/events/dashboard/multirow/EventsBirthdayRowPartDefinition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2530177
    const-string v0, ""

    iput-object v0, p0, LX/I2M;->c:Ljava/lang/String;

    .line 2530178
    const-string v0, ""

    iput-object v0, p0, LX/I2M;->d:Ljava/lang/String;

    .line 2530179
    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;->n()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/I2M;->b:Ljava/lang/String;

    .line 2530180
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, LX/I2M;->f:Ljava/lang/StringBuilder;

    .line 2530181
    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;->p()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    .line 2530182
    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, LX/I2M;->e:Ljava/lang/String;

    .line 2530183
    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;->l()Z

    move-result v0

    iput-boolean v0, p0, LX/I2M;->i:Z

    .line 2530184
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    .line 2530185
    invoke-virtual {p2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;->j()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v4, v0, LX/1vs;->b:I

    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2530186
    if-eqz v4, :cond_0

    .line 2530187
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v3, v4, v1}, LX/15i;->j(II)I

    move-result v1

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, LX/15i;->j(II)I

    move-result v5

    invoke-static {v2, v0, v1, v5}, LX/6RS;->a(Ljava/util/Date;Ljava/util/TimeZone;II)Ljava/util/Calendar;

    move-result-object v1

    .line 2530188
    invoke-virtual {v1}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    .line 2530189
    const/4 v5, 0x1

    const/4 v6, -0x1

    invoke-virtual {v0, v5, v6}, Ljava/util/Calendar;->roll(II)V

    .line 2530190
    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Landroid/text/format/DateUtils;->isToday(J)Z

    move-result v5

    iput-boolean v5, p0, LX/I2M;->g:Z

    .line 2530191
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v5

    .line 2530192
    const/4 v6, 0x2

    invoke-virtual {v3, v4, v6}, LX/15i;->j(II)I

    move-result v6

    const/4 v7, 0x1

    invoke-virtual {v3, v4, v7}, LX/15i;->j(II)I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    const/4 v8, 0x0

    invoke-virtual {v3, v4, v8}, LX/15i;->j(II)I

    move-result v8

    invoke-virtual {v5, v6, v7, v8}, Ljava/util/Calendar;->set(III)V

    .line 2530193
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    invoke-static {v6, v5}, LX/6RS;->a(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v5

    iput-boolean v5, p0, LX/I2M;->h:Z

    .line 2530194
    iget-boolean v5, p0, LX/I2M;->h:Z

    if-eqz v5, :cond_2

    .line 2530195
    :goto_1
    if-eqz p4, :cond_3

    .line 2530196
    iget-object v1, p1, Lcom/facebook/events/dashboard/multirow/EventsBirthdayRowPartDefinition;->a:LX/6RZ;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v5

    invoke-virtual {v1, v5, v2}, LX/6RZ;->b(Ljava/util/Date;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/I2M;->c:Ljava/lang/String;

    .line 2530197
    :goto_2
    const/4 v1, 0x2

    invoke-virtual {v3, v4, v1}, LX/15i;->j(II)I

    move-result v1

    if-lez v1, :cond_0

    .line 2530198
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    const/4 v1, 0x2

    invoke-virtual {v3, v4, v1}, LX/15i;->j(II)I

    move-result v1

    sub-int v1, v0, v1

    .line 2530199
    iget-boolean v0, p0, LX/I2M;->h:Z

    if-eqz v0, :cond_4

    const v0, 0x7f0f0105

    .line 2530200
    :goto_3
    invoke-virtual {p3}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v2, v0, v1, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/I2M;->d:Ljava/lang/String;

    .line 2530201
    :cond_0
    return-void

    .line 2530202
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 2530203
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move-object v0, v1

    .line 2530204
    goto :goto_1

    .line 2530205
    :cond_3
    iget-object v1, p1, Lcom/facebook/events/dashboard/multirow/EventsBirthdayRowPartDefinition;->a:LX/6RZ;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/6RZ;->b(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/I2M;->c:Ljava/lang/String;

    goto :goto_2

    .line 2530206
    :cond_4
    iget-boolean v0, p0, LX/I2M;->g:Z

    if-eqz v0, :cond_5

    const v0, 0x7f0f0103

    goto :goto_3

    :cond_5
    const v0, 0x7f0f0104

    goto :goto_3
.end method
