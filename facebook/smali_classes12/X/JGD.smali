.class public final LX/JGD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/JGB;


# instance fields
.field public final synthetic a:D

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/JGL;


# direct methods
.method public constructor <init>(LX/JGL;DLjava/lang/String;)V
    .locals 0

    .prologue
    .line 2666900
    iput-object p1, p0, LX/JGD;->c:LX/JGL;

    iput-wide p2, p0, LX/JGD;->a:D

    iput-object p4, p0, LX/JGD;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/JG9;)V
    .locals 4

    .prologue
    .line 2666901
    iget-wide v0, p0, LX/JGD;->a:D

    double-to-float v0, v0

    invoke-virtual {p1, v0}, LX/JG9;->a(F)V

    .line 2666902
    :try_start_0
    iget-boolean v0, p1, LX/JG9;->b:Z

    if-eqz v0, :cond_0

    .line 2666903
    iget-object v0, p1, LX/JG9;->a:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2666904
    :cond_0
    return-void

    .line 2666905
    :catch_0
    move-exception v0

    .line 2666906
    new-instance v1, LX/5p9;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not play audio: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/JGD;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LX/5p9;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
