.class public LX/Im6;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2608845
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 2608846
    return-void
.end method

.method public static a(Ljava/lang/Boolean;)Ljava/lang/Integer;
    .locals 1
    .param p0    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/messaging/payment/sync/annotations/IsPaymentsSyncV3Enabled;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/payment/sync/annotations/PaymentsSyncApiVersion;
    .end annotation

    .prologue
    .line 2608842
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2608843
    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 2608844
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 2608841
    return-void
.end method
