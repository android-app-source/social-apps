.class public LX/HV7;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private final a:Lcom/facebook/resources/ui/FbTextView;

.field public b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 2474742
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, LX/HV7;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2474743
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 2474733
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2474734
    const/4 v0, 0x0

    iput-object v0, p0, LX/HV7;->b:Ljava/lang/String;

    .line 2474735
    const v0, 0x7f0315ad

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2474736
    const v0, 0x7f0d30e8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/HV7;->a:Lcom/facebook/resources/ui/FbTextView;

    .line 2474737
    invoke-virtual {p0}, LX/HV7;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0817e0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/HV7;->b:Ljava/lang/String;

    .line 2474738
    invoke-static {p0}, LX/HV7;->a(LX/HV7;)V

    .line 2474739
    return-void
.end method

.method public static a(LX/HV7;)V
    .locals 6

    .prologue
    .line 2474740
    iget-object v0, p0, LX/HV7;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, LX/HV7;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0817df

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, LX/HV7;->b:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2474741
    return-void
.end method
