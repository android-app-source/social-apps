.class public LX/HOa;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/view/LayoutInflater;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CYR;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/lang/String;

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CY3;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Ot;LX/0Ot;Ljava/lang/String;)V
    .locals 1
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/CYR;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/CY3;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2460224
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2460225
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, LX/HOa;->a:Landroid/view/LayoutInflater;

    .line 2460226
    iput-object p2, p0, LX/HOa;->b:LX/0Ot;

    .line 2460227
    iput-object p3, p0, LX/HOa;->d:LX/0Ot;

    .line 2460228
    iput-object p4, p0, LX/HOa;->c:Ljava/lang/String;

    .line 2460229
    return-void
.end method

.method private b(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PhoneNumberCommonFieldsModel;LX/8ET;Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;)LX/HOQ;
    .locals 6
    .param p1    # Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PhoneNumberCommonFieldsModel;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 2460230
    iget-object v0, p0, LX/HOa;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f030e2e

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;

    .line 2460231
    invoke-interface {p2}, LX/8ET;->e()Ljava/lang/String;

    move-result-object v1

    if-eqz p1, :cond_0

    const/4 v2, 0x1

    :cond_0
    iget-object v3, p0, LX/HOa;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    .line 2460232
    if-eqz p1, :cond_1

    .line 2460233
    invoke-virtual {p1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PhoneNumberCommonFieldsModel;->c()Ljava/lang/String;

    move-result-object v3

    .line 2460234
    :goto_0
    move-object v3, v3

    .line 2460235
    iget-object v4, p0, LX/HOa;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/CYR;

    .line 2460236
    if-eqz p1, :cond_2

    .line 2460237
    invoke-virtual {p1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PhoneNumberCommonFieldsModel;->a()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 2460238
    :try_start_0
    invoke-virtual {p1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PhoneNumberCommonFieldsModel;->a()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    .line 2460239
    :goto_1
    move v4, v5

    .line 2460240
    invoke-interface {p2}, LX/8ET;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionPhoneNumberEditView;->a(Ljava/lang/String;ZLjava/lang/String;ILjava/lang/String;)V

    .line 2460241
    return-object v0

    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    .line 2460242
    :catch_0
    move-exception v5

    move-object p0, v5

    .line 2460243
    iget-object v5, v4, LX/CYR;->c:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/03V;

    const-class p3, LX/CYR;

    invoke-virtual {p3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v5, p3, p0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2460244
    :cond_2
    const/4 v5, -0x1

    goto :goto_1
.end method

.method public static varargs b([Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 2460245
    array-length v0, p0

    if-lez v0, :cond_0

    const/4 v0, 0x0

    aget-object v0, p0, v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PhoneNumberCommonFieldsModel;LX/8ET;Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;)LX/HOQ;
    .locals 7
    .param p1    # Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PhoneNumberCommonFieldsModel;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 2460246
    const/4 v0, 0x0

    .line 2460247
    sget-object v1, LX/HOZ;->a:[I

    invoke-interface {p2}, LX/8ET;->c()Lcom/facebook/graphql/enums/GraphQLPageCtaConfigFieldType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLPageCtaConfigFieldType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2460248
    :goto_0
    return-object v0

    .line 2460249
    :pswitch_0
    invoke-direct {p0, p1, p2, p3}, LX/HOa;->b(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PhoneNumberCommonFieldsModel;LX/8ET;Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;)LX/HOQ;

    move-result-object v0

    goto :goto_0

    .line 2460250
    :pswitch_1
    iget-object v0, p0, LX/HOa;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f030e23

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionEmailEditView;

    .line 2460251
    invoke-interface {p2}, LX/8ET;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2}, LX/8ET;->d()Ljava/lang/String;

    move-result-object v2

    new-instance p1, LX/HOX;

    invoke-direct {p1, p0, p2}, LX/HOX;-><init>(LX/HOa;LX/8ET;)V

    invoke-virtual {v0, v1, v2, p1}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionEmailEditView;->a(Ljava/lang/String;Ljava/lang/String;LX/HOU;)V

    .line 2460252
    move-object v0, v0

    .line 2460253
    goto :goto_0

    .line 2460254
    :pswitch_2
    iget-object v0, p0, LX/HOa;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f030e31

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionSelectView;

    .line 2460255
    invoke-interface {p2}, LX/8ET;->dS_()LX/0Px;

    move-result-object v1

    invoke-interface {p2}, LX/8ET;->d()Ljava/lang/String;

    move-result-object v2

    new-instance p1, LX/HOV;

    invoke-direct {p1, p0, p2}, LX/HOV;-><init>(LX/HOa;LX/8ET;)V

    invoke-virtual {v0, v1, v2, p1}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionSelectView;->a(LX/0Px;Ljava/lang/String;LX/HOU;)V

    .line 2460256
    move-object v0, v0

    .line 2460257
    goto :goto_0

    .line 2460258
    :pswitch_3
    iget-object v0, p0, LX/HOa;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f030e37

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionUrlEditView;

    .line 2460259
    invoke-interface {p2}, LX/8ET;->d()Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/HOW;

    invoke-direct {v2, p0, p2}, LX/HOW;-><init>(LX/HOa;LX/8ET;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionUrlEditView;->a(Ljava/lang/String;LX/HOU;)V

    .line 2460260
    move-object v0, v0

    .line 2460261
    goto :goto_0

    .line 2460262
    :pswitch_4
    iget-object v0, p0, LX/HOa;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f030e35

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithEntitiesView;

    .line 2460263
    invoke-interface {p2}, LX/8ET;->a()LX/175;

    move-result-object v1

    new-instance v2, LX/HOY;

    invoke-direct {v2, p0, p2}, LX/HOY;-><init>(LX/HOa;LX/8ET;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithEntitiesView;->a(LX/175;LX/HOU;)V

    .line 2460264
    move-object v0, v0

    .line 2460265
    goto :goto_0

    .line 2460266
    :pswitch_5
    iget-object v0, p0, LX/HOa;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f030e2c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionMultiLineTextView;

    .line 2460267
    const/4 v2, 0x0

    .line 2460268
    invoke-interface {p2}, LX/8ET;->j()LX/8ES;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 2460269
    invoke-interface {p2}, LX/8ET;->j()LX/8ES;

    move-result-object v1

    invoke-interface {v1}, LX/8ES;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result p1

    move v3, v2

    .line 2460270
    :goto_1
    if-ge v3, p1, :cond_4

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8ER;

    .line 2460271
    sget-object p0, Lcom/facebook/graphql/enums/GraphQLPageCtaConfigFieldType;->TEXT_MULTILINE_CHAR_LIMIT:Lcom/facebook/graphql/enums/GraphQLPageCtaConfigFieldType;

    invoke-interface {v1}, LX/8ER;->c()Lcom/facebook/graphql/enums/GraphQLPageCtaConfigFieldType;

    move-result-object p3

    if-ne p0, p3, :cond_3

    .line 2460272
    invoke-interface {v1}, LX/8ER;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 2460273
    :goto_2
    move v1, v1

    .line 2460274
    if-nez v1, :cond_2

    .line 2460275
    invoke-interface {p2}, LX/8ET;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2}, LX/8ET;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionMultiLineTextView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2460276
    :goto_3
    move-object v0, v0

    .line 2460277
    goto/16 :goto_0

    .line 2460278
    :pswitch_6
    const/4 v3, 0x0

    const/4 v5, 0x0

    .line 2460279
    check-cast p2, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionConfigFieldsModel;

    .line 2460280
    invoke-virtual {p2}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionConfigFieldsModel;->n()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionConfigFieldsModel$SubfieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionConfigFieldsModel;->n()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionConfigFieldsModel$SubfieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionConfigFieldsModel$SubfieldsModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionConfigFieldsModel;->n()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionConfigFieldsModel$SubfieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionConfigFieldsModel$SubfieldsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-nez v0, :cond_5

    :cond_0
    move-object v0, v3

    .line 2460281
    :cond_1
    move-object v0, v0

    .line 2460282
    goto/16 :goto_0

    .line 2460283
    :pswitch_7
    iget-object v0, p0, LX/HOa;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f030e33

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithClearButtonEditView;

    .line 2460284
    invoke-interface {p2}, LX/8ET;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2}, LX/8ET;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionTextWithClearButtonEditView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2460285
    move-object v0, v0

    .line 2460286
    goto/16 :goto_0

    .line 2460287
    :pswitch_8
    new-instance v0, LX/HOR;

    new-instance v1, Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {p3}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/widget/SwitchCompat;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, LX/HOR;-><init>(Lcom/facebook/widget/SwitchCompat;)V

    .line 2460288
    iget-object v1, v0, LX/HOR;->b:Lcom/facebook/widget/SwitchCompat;

    invoke-interface {p2}, LX/8ET;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/widget/SwitchCompat;->setText(Ljava/lang/CharSequence;)V

    .line 2460289
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2}, LX/8ET;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    .line 2460290
    iget-object v2, v0, LX/HOR;->b:Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/SwitchCompat;->setChecked(Z)V

    .line 2460291
    move-object v0, v0

    .line 2460292
    goto/16 :goto_0

    .line 2460293
    :cond_2
    invoke-interface {p2}, LX/8ET;->e()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2}, LX/8ET;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3, v1}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionMultiLineTextView;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_3

    .line 2460294
    :cond_3
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto/16 :goto_1

    :cond_4
    move v1, v2

    .line 2460295
    goto/16 :goto_2

    .line 2460296
    :cond_5
    iget-object v0, p0, LX/HOa;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f030e27

    invoke-virtual {v0, v1, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionGroupView;

    move v4, v5

    .line 2460297
    :goto_4
    invoke-virtual {p2}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionConfigFieldsModel;->n()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionConfigFieldsModel$SubfieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionConfigFieldsModel$SubfieldsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v4, v1, :cond_1

    .line 2460298
    invoke-virtual {p2}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionConfigFieldsModel;->n()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionConfigFieldsModel$SubfieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionConfigFieldsModel$SubfieldsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8ET;

    .line 2460299
    invoke-virtual {p0, p1, v1, p3}, LX/HOa;->a(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PhoneNumberCommonFieldsModel;LX/8ET;Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionInputFieldsContainer;)LX/HOQ;

    move-result-object v2

    .line 2460300
    invoke-interface {v1}, LX/8ET;->b()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v1}, LX/8ET;->dR_()Z

    move-result v1

    invoke-virtual {v0, v6, v2, v1}, Lcom/facebook/pages/common/surface/calltoaction/ui/PageCallToActionGroupView;->a(Ljava/lang/String;LX/HOQ;Z)V

    .line 2460301
    if-eqz v3, :cond_6

    .line 2460302
    instance-of v1, v2, Ljava/util/Observer;

    if-eqz v1, :cond_8

    move-object v1, v2

    .line 2460303
    check-cast v1, Ljava/util/Observer;

    .line 2460304
    iget-object v2, v3, LX/HOR;->a:LX/HOP;

    invoke-virtual {v2, v1}, LX/HOP;->addObserver(Ljava/util/Observer;)V

    .line 2460305
    iget-object v2, v3, LX/HOR;->a:LX/HOP;

    iget-object v6, v3, LX/HOR;->b:Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {v6}, Lcom/facebook/widget/SwitchCompat;->isChecked()Z

    move-result v6

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-interface {v1, v2, v6}, Ljava/util/Observer;->update(Ljava/util/Observable;Ljava/lang/Object;)V

    .line 2460306
    move-object v2, v3

    .line 2460307
    :goto_5
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move-object v3, v2

    goto :goto_4

    .line 2460308
    :cond_6
    instance-of v1, v2, LX/HOR;

    if-eqz v1, :cond_8

    .line 2460309
    if-nez v3, :cond_7

    const/4 v1, 0x1

    :goto_6
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 2460310
    check-cast v2, LX/HOR;

    goto :goto_5

    :cond_7
    move v1, v5

    .line 2460311
    goto :goto_6

    :cond_8
    move-object v2, v3

    goto :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method
