.class public final LX/Hiu;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;


# direct methods
.method public constructor <init>(Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;)V
    .locals 0

    .prologue
    .line 2498019
    iput-object p1, p0, LX/Hiu;->a:Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(ILandroid/location/Address;)V
    .locals 8

    .prologue
    .line 2498020
    iget-object v0, p0, LX/Hiu;->a:Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;

    invoke-virtual {v0}, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->a()V

    .line 2498021
    iget-object v0, p0, LX/Hiu;->a:Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;

    iget-boolean v0, v0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->r:Z

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Landroid/location/Address;->hasLatitude()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Landroid/location/Address;->hasLongitude()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2498022
    :cond_0
    iget-object v0, p0, LX/Hiu;->a:Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;

    iget-object v0, v0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->w:LX/HiW;

    if-eqz v0, :cond_1

    .line 2498023
    iget-object v0, p0, LX/Hiu;->a:Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;

    iget-object v0, v0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->w:LX/HiW;

    invoke-virtual {v0, p2}, LX/HiW;->a(Landroid/location/Address;)V

    .line 2498024
    :cond_1
    iget-object v0, p0, LX/Hiu;->a:Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;

    iget-object v0, v0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->f:LX/HiN;

    sget-object v1, LX/HiM;->RECENT:LX/HiM;

    invoke-virtual {v0, p2, v1}, LX/HiN;->a(Landroid/location/Address;LX/HiM;)V

    .line 2498025
    :goto_0
    iget-object v0, p0, LX/Hiu;->a:Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;

    iget-object v0, v0, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->e:LX/HiK;

    iget-object v1, p0, LX/Hiu;->a:Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;

    invoke-static {v1}, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->getInputString(Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Landroid/location/Address;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v2, p0, LX/Hiu;->a:Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;

    iget-boolean v2, v2, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->r:Z

    if-eqz v2, :cond_3

    const-string v4, "google"

    :goto_1
    iget-object v2, p0, LX/Hiu;->a:Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;

    iget-object v5, v2, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->u:Ljava/lang/String;

    iget-object v2, p0, LX/Hiu;->a:Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;

    iget-object v6, v2, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->t:Landroid/location/Location;

    move v2, p1

    move-object v7, p2

    invoke-virtual/range {v0 .. v7}, LX/HiK;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/location/Location;Landroid/location/Address;)V

    .line 2498026
    return-void

    .line 2498027
    :cond_2
    iget-object v0, p0, LX/Hiu;->a:Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;

    .line 2498028
    invoke-static {v0, p2}, Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;->a$redex0(Lcom/facebook/addresstypeahead/view/AddressTypeAheadSearchView;Landroid/location/Address;)V

    .line 2498029
    goto :goto_0

    .line 2498030
    :cond_3
    const-string v4, "here_thrift"

    goto :goto_1
.end method
