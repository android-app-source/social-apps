.class public final LX/J42;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/privacy/model/PrivacyOptionsResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/J45;

.field private b:LX/J6P;

.field private c:Z


# direct methods
.method public constructor <init>(LX/J45;LX/J6P;Z)V
    .locals 0

    .prologue
    .line 2643512
    iput-object p1, p0, LX/J42;->a:LX/J45;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    .line 2643513
    iput-object p2, p0, LX/J42;->b:LX/J6P;

    .line 2643514
    iput-boolean p3, p0, LX/J42;->c:Z

    .line 2643515
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2643527
    iget-boolean v0, p0, LX/J42;->c:Z

    if-eqz v0, :cond_0

    .line 2643528
    iget-object v0, p0, LX/J42;->a:LX/J45;

    iget-object v0, v0, LX/J45;->h:LX/03V;

    const-string v1, "privacy_checkup_manager_composer_options_server_fetch_failed"

    const-string v2, "Failed to get composer options from server"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2643529
    iget-object v0, p0, LX/J42;->a:LX/J45;

    iget-object v1, p0, LX/J42;->b:LX/J6P;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/J45;->a(LX/J6P;Z)V

    .line 2643530
    :goto_0
    return-void

    .line 2643531
    :cond_0
    iget-object v0, p0, LX/J42;->a:LX/J45;

    iget-object v0, v0, LX/J45;->h:LX/03V;

    const-string v1, "privacy_checkup_manager_composer_options_fetch_failed"

    const-string v2, "Failed to get composer options from server or cache: "

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2643516
    check-cast p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;

    .line 2643517
    if-nez p1, :cond_0

    .line 2643518
    iget-object v0, p0, LX/J42;->a:LX/J45;

    iget-object v0, v0, LX/J45;->h:LX/03V;

    const-string v1, "privacy_checkup_manager_null_composer_options"

    const-string v2, "Composer options are null! Did the parse fail?"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2643519
    :goto_0
    iget-object v0, p0, LX/J42;->b:LX/J6P;

    invoke-virtual {v0}, LX/J6P;->a()V

    .line 2643520
    return-void

    .line 2643521
    :cond_0
    iget-object v0, p0, LX/J42;->a:LX/J45;

    new-instance v1, LX/8QV;

    invoke-direct {v1}, LX/8QV;-><init>()V

    .line 2643522
    iput-object p1, v1, LX/8QV;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    .line 2643523
    move-object v1, v1

    .line 2643524
    iget-object v2, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {v1, v2}, LX/8QV;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/8QV;

    move-result-object v1

    invoke-virtual {v1}, LX/8QV;->b()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v1

    .line 2643525
    iput-object v1, v0, LX/J45;->k:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 2643526
    goto :goto_0
.end method
