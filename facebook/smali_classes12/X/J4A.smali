.class public final LX/J4A;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel;",
        ">;",
        "LX/J4L;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/J4B;


# direct methods
.method public constructor <init>(LX/J4B;)V
    .locals 0

    .prologue
    .line 2643845
    iput-object p1, p0, LX/J4A;->a:LX/J4B;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2643833
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2643834
    iget-object v0, p0, LX/J4A;->a:LX/J4B;

    iget-object v0, v0, LX/J4B;->d:LX/J4E;

    iget-object v0, v0, LX/J4E;->e:LX/J4N;

    iget-object v1, p0, LX/J4A;->a:LX/J4B;

    iget-object v1, v1, LX/J4B;->a:Ljava/lang/String;

    .line 2643835
    if-eqz p1, :cond_0

    .line 2643836
    iget-object p0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object p0, p0

    .line 2643837
    if-nez p0, :cond_1

    .line 2643838
    :cond_0
    const/4 p0, 0x0

    .line 2643839
    :goto_0
    move-object v0, p0

    .line 2643840
    return-object v0

    .line 2643841
    :cond_1
    iget-object p0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object p0, p0

    .line 2643842
    check-cast p0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel;

    .line 2643843
    const/4 p1, 0x0

    invoke-static {v0, p0, v1, p1}, LX/J4N;->a(LX/J4N;Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel;Ljava/lang/String;I)LX/J4L;

    move-result-object p1

    move-object p0, p1

    .line 2643844
    goto :goto_0
.end method
