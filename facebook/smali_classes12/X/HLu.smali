.class public LX/HLu;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 2456478
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2456479
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/HLu;->setOrientation(I)V

    .line 2456480
    invoke-virtual {p0}, LX/HLu;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2456481
    const v1, 0x7f0213ab

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {p0, v1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2456482
    const v1, 0x7f0b0d5e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    const v2, 0x7f0b0d5f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    const v3, 0x7f0b0d5e

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    const v4, 0x7f0b0d5f

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v1, v2, v3, v0}, LX/HLu;->setPadding(IIII)V

    .line 2456483
    invoke-virtual {p0}, LX/HLu;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030e7c

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2456484
    const v0, 0x7f0d2374

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;

    iput-object v0, p0, LX/HLu;->a:Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;

    .line 2456485
    iget-object v0, p0, LX/HLu;->a:Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;

    sget-object v1, LX/0xq;->ROBOTO:LX/0xq;

    sget-object v2, LX/0xr;->REGULAR:LX/0xr;

    iget-object v3, p0, LX/HLu;->a:Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;

    invoke-virtual {v3}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, LX/0xs;->a(Landroid/widget/TextView;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)V

    .line 2456486
    iget-object v0, p0, LX/HLu;->a:Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;

    invoke-virtual {v0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->a()V

    .line 2456487
    return-void
.end method
