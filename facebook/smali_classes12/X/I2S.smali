.class public LX/I2S;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/6RZ;

.field public final b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/6RZ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2530310
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2530311
    iput-object p1, p0, LX/I2S;->b:Landroid/content/Context;

    .line 2530312
    iput-object p2, p0, LX/I2S;->a:LX/6RZ;

    .line 2530313
    return-void
.end method

.method public static b(LX/0QB;)LX/I2S;
    .locals 3

    .prologue
    .line 2530336
    new-instance v2, LX/I2S;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/6RZ;->a(LX/0QB;)LX/6RZ;

    move-result-object v1

    check-cast v1, LX/6RZ;

    invoke-direct {v2, v0, v1}, LX/I2S;-><init>(Landroid/content/Context;LX/6RZ;)V

    .line 2530337
    return-object v2
.end method


# virtual methods
.method public final a(LX/6RY;Ljava/util/Date;ZZI)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2530314
    const/4 v0, 0x3

    if-gt p5, v0, :cond_0

    .line 2530315
    sget-object v0, LX/I2R;->a:[I

    invoke-virtual {p1}, LX/6RY;->ordinal()I

    move-result p2

    aget v0, v0, p2

    packed-switch v0, :pswitch_data_0

    .line 2530316
    iget-object v0, p0, LX/I2S;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const p2, 0x7f0821ac

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    move-object v0, v0

    .line 2530317
    :goto_1
    return-object v0

    .line 2530318
    :cond_0
    sget-object v0, LX/I2R;->a:[I

    invoke-virtual {p1}, LX/6RY;->ordinal()I

    move-result p5

    aget v0, v0, p5

    packed-switch v0, :pswitch_data_1

    .line 2530319
    :cond_1
    if-eqz p4, :cond_2

    .line 2530320
    iget-object v0, p0, LX/I2S;->a:LX/6RZ;

    invoke-virtual {v0, p2}, LX/6RZ;->f(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 2530321
    :goto_2
    move-object v0, v0

    .line 2530322
    goto :goto_1

    .line 2530323
    :pswitch_0
    iget-object v0, p0, LX/I2S;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const p2, 0x7f0821aa

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2530324
    :pswitch_1
    iget-object v0, p0, LX/I2S;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const p2, 0x7f0821ab

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2530325
    :pswitch_2
    iget-object v0, p0, LX/I2S;->a:LX/6RZ;

    .line 2530326
    iget-object p5, v0, LX/6RZ;->p:Ljava/lang/String;

    move-object v0, p5

    .line 2530327
    goto :goto_2

    .line 2530328
    :pswitch_3
    iget-object v0, p0, LX/I2S;->a:LX/6RZ;

    .line 2530329
    iget-object p5, v0, LX/6RZ;->q:Ljava/lang/String;

    move-object v0, p5

    .line 2530330
    goto :goto_2

    .line 2530331
    :pswitch_4
    iget-object v0, p0, LX/I2S;->a:LX/6RZ;

    invoke-virtual {v0, p2}, LX/6RZ;->e(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 2530332
    :pswitch_5
    iget-object v0, p0, LX/I2S;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const p5, 0x7f0821ab

    invoke-virtual {v0, p5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 2530333
    :pswitch_6
    if-eqz p3, :cond_1

    .line 2530334
    iget-object v0, p0, LX/I2S;->a:LX/6RZ;

    invoke-virtual {v0, p2}, LX/6RZ;->h(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 2530335
    :cond_2
    iget-object v0, p0, LX/I2S;->a:LX/6RZ;

    invoke-virtual {v0, p2}, LX/6RZ;->g(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
