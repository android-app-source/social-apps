.class public final enum LX/HSF;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HSF;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/HSF;

.field public static final enum ACTIVITY:LX/HSF;

.field public static final enum INSIGHTS:LX/HSF;

.field public static final enum PAGE:LX/HSF;


# instance fields
.field public final tabButtonId:I

.field public final tabName:Ljava/lang/String;

.field public final tabTextString:I

.field public final tabTextViewId:I


# direct methods
.method public static constructor <clinit>()V
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v2, 0x0

    .line 2466679
    new-instance v0, LX/HSF;

    const-string v1, "PAGE"

    const-string v3, "page"

    const v4, 0x7f0d22f8

    const v5, 0x7f0d22f8

    const v6, 0x7f081802

    invoke-direct/range {v0 .. v6}, LX/HSF;-><init>(Ljava/lang/String;ILjava/lang/String;III)V

    sput-object v0, LX/HSF;->PAGE:LX/HSF;

    .line 2466680
    new-instance v3, LX/HSF;

    const-string v4, "ACTIVITY"

    const-string v6, "activity"

    const v7, 0x7f0d22f9

    const v8, 0x7f0d22f9

    const v9, 0x7f081803

    move v5, v10

    invoke-direct/range {v3 .. v9}, LX/HSF;-><init>(Ljava/lang/String;ILjava/lang/String;III)V

    sput-object v3, LX/HSF;->ACTIVITY:LX/HSF;

    .line 2466681
    new-instance v3, LX/HSF;

    const-string v4, "INSIGHTS"

    const-string v6, "insights"

    const v7, 0x7f0d22fa

    const v8, 0x7f0d22fa

    const v9, 0x7f081804

    move v5, v11

    invoke-direct/range {v3 .. v9}, LX/HSF;-><init>(Ljava/lang/String;ILjava/lang/String;III)V

    sput-object v3, LX/HSF;->INSIGHTS:LX/HSF;

    .line 2466682
    const/4 v0, 0x3

    new-array v0, v0, [LX/HSF;

    sget-object v1, LX/HSF;->PAGE:LX/HSF;

    aput-object v1, v0, v2

    sget-object v1, LX/HSF;->ACTIVITY:LX/HSF;

    aput-object v1, v0, v10

    sget-object v1, LX/HSF;->INSIGHTS:LX/HSF;

    aput-object v1, v0, v11

    sput-object v0, LX/HSF;->$VALUES:[LX/HSF;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "III)V"
        }
    .end annotation

    .prologue
    .line 2466683
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2466684
    iput-object p3, p0, LX/HSF;->tabName:Ljava/lang/String;

    .line 2466685
    iput p4, p0, LX/HSF;->tabButtonId:I

    .line 2466686
    iput p5, p0, LX/HSF;->tabTextViewId:I

    .line 2466687
    iput p6, p0, LX/HSF;->tabTextString:I

    .line 2466688
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HSF;
    .locals 1

    .prologue
    .line 2466689
    const-class v0, LX/HSF;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HSF;

    return-object v0
.end method

.method public static values()[LX/HSF;
    .locals 1

    .prologue
    .line 2466690
    sget-object v0, LX/HSF;->$VALUES:[LX/HSF;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HSF;

    return-object v0
.end method
