.class public LX/IOF;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/IOE;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2571627
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2571628
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/IOF;->a:Ljava/util/HashMap;

    .line 2571629
    return-void
.end method

.method public static a(LX/0QB;)LX/IOF;
    .locals 3

    .prologue
    .line 2571632
    const-class v1, LX/IOF;

    monitor-enter v1

    .line 2571633
    :try_start_0
    sget-object v0, LX/IOF;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2571634
    sput-object v2, LX/IOF;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2571635
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2571636
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2571637
    new-instance v0, LX/IOF;

    invoke-direct {v0}, LX/IOF;-><init>()V

    .line 2571638
    move-object v0, v0

    .line 2571639
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2571640
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IOF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2571641
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2571642
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/IOE;)V
    .locals 1

    .prologue
    .line 2571630
    iget-object v0, p0, LX/IOF;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2571631
    return-void
.end method
