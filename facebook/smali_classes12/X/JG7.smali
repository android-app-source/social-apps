.class public LX/JG7;
.super LX/5pb;
.source ""

# interfaces
.implements LX/5pQ;


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "HostStateAndroid"
.end annotation


# instance fields
.field private a:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/5pY;)V
    .locals 1

    .prologue
    .line 2666848
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 2666849
    const-string v0, "uninitialized"

    iput-object v0, p0, LX/JG7;->a:Ljava/lang/String;

    .line 2666850
    return-void
.end method

.method private h()V
    .locals 3

    .prologue
    .line 2666830
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2666831
    const-class v1, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;

    invoke-virtual {v0, v1}, LX/5pX;->a(Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;

    const-string v1, "hostLifecycleEvent"

    iget-object v2, p0, LX/JG7;->a:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/facebook/react/modules/core/DeviceEventManagerModule$RCTDeviceEventEmitter;->emit(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2666832
    return-void
.end method


# virtual methods
.method public final bM_()V
    .locals 1

    .prologue
    .line 2666845
    const-string v0, "resumed"

    iput-object v0, p0, LX/JG7;->a:Ljava/lang/String;

    .line 2666846
    invoke-direct {p0}, LX/JG7;->h()V

    .line 2666847
    return-void
.end method

.method public final bN_()V
    .locals 1

    .prologue
    .line 2666842
    const-string v0, "paused"

    iput-object v0, p0, LX/JG7;->a:Ljava/lang/String;

    .line 2666843
    invoke-direct {p0}, LX/JG7;->h()V

    .line 2666844
    return-void
.end method

.method public final bO_()V
    .locals 0

    .prologue
    .line 2666841
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 2666837
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2666838
    invoke-virtual {v0, p0}, LX/5pX;->a(LX/5pQ;)V

    .line 2666839
    const-string v0, "initialized"

    iput-object v0, p0, LX/JG7;->a:Ljava/lang/String;

    .line 2666840
    return-void
.end method

.method public final f()V
    .locals 0

    .prologue
    .line 2666836
    return-void
.end method

.method public getCurrentHostState(Lcom/facebook/react/bridge/Callback;Lcom/facebook/react/bridge/Callback;)V
    .locals 3
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2666834
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/JG7;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-interface {p1, v0}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    .line 2666835
    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2666833
    const-string v0, "HostStateAndroid"

    return-object v0
.end method
