.class public LX/Itm;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile f:LX/Itm;


# instance fields
.field private final c:LX/03V;

.field public final d:LX/0SG;

.field public final e:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    .line 2624365
    const-class v0, LX/Itm;

    sput-object v0, LX/Itm;->a:Ljava/lang/Class;

    .line 2624366
    const/16 v0, 0x16

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/16 v1, 0x17

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0xe6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v3, 0x170

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/16 v4, 0x1f4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/16 v5, 0x1f5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/16 v6, 0x8

    new-array v6, v6, [Ljava/lang/Integer;

    const/4 v7, 0x0

    const/16 v8, 0x1f6

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const/16 v8, 0x1f7

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    const/16 v8, 0x1f8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x3

    const/16 v8, 0x1f9

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x4

    const/16 v8, 0x1fa

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x5

    const/16 v8, 0x1fb

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x6

    const/16 v8, 0x1fc

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x7

    const/16 v8, 0x1fd

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static/range {v0 .. v6}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/Itm;->b:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/0SG;Landroid/content/res/Resources;)V
    .locals 0
    .param p2    # LX/0SG;
        .annotation runtime Lcom/facebook/messaging/database/threads/NeedsDbClock;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2624360
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2624361
    iput-object p1, p0, LX/Itm;->c:LX/03V;

    .line 2624362
    iput-object p2, p0, LX/Itm;->d:LX/0SG;

    .line 2624363
    iput-object p3, p0, LX/Itm;->e:Landroid/content/res/Resources;

    .line 2624364
    return-void
.end method

.method public static a(LX/0QB;)LX/Itm;
    .locals 6

    .prologue
    .line 2624367
    sget-object v0, LX/Itm;->f:LX/Itm;

    if-nez v0, :cond_1

    .line 2624368
    const-class v1, LX/Itm;

    monitor-enter v1

    .line 2624369
    :try_start_0
    sget-object v0, LX/Itm;->f:LX/Itm;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2624370
    if-eqz v2, :cond_0

    .line 2624371
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2624372
    new-instance p0, LX/Itm;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {v0}, LX/2N6;->a(LX/0QB;)LX/2N6;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-direct {p0, v3, v4, v5}, LX/Itm;-><init>(LX/03V;LX/0SG;Landroid/content/res/Resources;)V

    .line 2624373
    move-object v0, p0

    .line 2624374
    sput-object v0, LX/Itm;->f:LX/Itm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2624375
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2624376
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2624377
    :cond_1
    sget-object v0, LX/Itm;->f:LX/Itm;

    return-object v0

    .line 2624378
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2624379
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/Itm;Ljava/lang/String;I)V
    .locals 4

    .prologue
    .line 2624357
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2624358
    iget-object v0, p0, LX/Itm;->c:LX/03V;

    const-string v1, "SendApiHandler_NULL_RETRYABLE_ERROR_MESSAGE"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Empty errStr for graph NO_RETRY error, errorNo="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2624359
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Throwable;Lcom/facebook/messaging/model/messages/Message;LX/6f3;)LX/FKG;
    .locals 4

    .prologue
    .line 2624325
    instance-of v0, p1, LX/FKG;

    if-eqz v0, :cond_0

    .line 2624326
    check-cast p1, LX/FKG;

    .line 2624327
    :goto_0
    return-object p1

    .line 2624328
    :cond_0
    invoke-virtual {p0}, LX/Itm;->a()LX/Itl;

    move-result-object v0

    .line 2624329
    invoke-virtual {v0, p2}, LX/Itl;->a(Lcom/facebook/messaging/model/messages/Message;)LX/Itl;

    move-result-object v1

    invoke-virtual {v1, p3}, LX/Itl;->a(LX/6f3;)LX/Itl;

    .line 2624330
    const/16 p3, 0x1f4

    .line 2624331
    invoke-static {p1}, LX/1Bz;->getCausalChain(Ljava/lang/Throwable;)Ljava/util/List;

    move-result-object v1

    .line 2624332
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Throwable;

    .line 2624333
    instance-of v3, v1, LX/2Oo;

    if-eqz v3, :cond_2

    .line 2624334
    check-cast v1, LX/2Oo;

    .line 2624335
    invoke-virtual {v1}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v1

    .line 2624336
    if-eqz v1, :cond_1

    sget-object v3, LX/Itm;->b:Ljava/util/Set;

    invoke-virtual {v1}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result p2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {v3, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2624337
    invoke-virtual {v1}, Lcom/facebook/http/protocol/ApiErrorResult;->c()Ljava/lang/String;

    move-result-object v2

    .line 2624338
    invoke-virtual {v1}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v3

    invoke-static {p0, v2, v3}, LX/Itm;->a(LX/Itm;Ljava/lang/String;I)V

    .line 2624339
    sget-object v3, LX/6fP;->PERMANENT_FAILURE:LX/6fP;

    invoke-virtual {v0, v3}, LX/Itl;->a(LX/6fP;)LX/Itl;

    move-result-object v3

    invoke-virtual {v1}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v1

    .line 2624340
    iput v1, v3, LX/Itl;->h:I

    .line 2624341
    move-object v1, v3

    .line 2624342
    iput-object v2, v1, LX/Itl;->e:Ljava/lang/String;

    .line 2624343
    move-object v1, v1

    .line 2624344
    :goto_1
    move-object v1, v1

    .line 2624345
    new-instance v0, LX/FKG;

    invoke-virtual {v1}, LX/Itl;->a()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v1

    invoke-direct {v0, p1, v1}, LX/FKG;-><init>(Ljava/lang/Throwable;Lcom/facebook/messaging/model/messages/Message;)V

    move-object p1, v0

    goto :goto_0

    .line 2624346
    :cond_2
    instance-of v3, v1, Lorg/apache/http/client/HttpResponseException;

    if-eqz v3, :cond_4

    .line 2624347
    check-cast v1, Lorg/apache/http/client/HttpResponseException;

    .line 2624348
    invoke-virtual {v1}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v1

    .line 2624349
    const/16 v3, 0x190

    if-lt v1, v3, :cond_3

    if-ge v1, p3, :cond_3

    .line 2624350
    sget-object v1, LX/6fP;->HTTP_4XX_ERROR:LX/6fP;

    invoke-virtual {v0, v1}, LX/Itl;->a(LX/6fP;)LX/Itl;

    move-result-object v1

    goto :goto_1

    .line 2624351
    :cond_3
    if-lt v1, p3, :cond_1

    .line 2624352
    sget-object v1, LX/6fP;->HTTP_5XX_ERROR:LX/6fP;

    invoke-virtual {v0, v1}, LX/Itl;->a(LX/6fP;)LX/Itl;

    move-result-object v1

    goto :goto_1

    .line 2624353
    :cond_4
    instance-of v1, v1, Ljava/io/IOException;

    if-eqz v1, :cond_1

    .line 2624354
    sget-object v1, LX/6fP;->IO_EXCEPTION:LX/6fP;

    invoke-virtual {v0, v1}, LX/Itl;->a(LX/6fP;)LX/Itl;

    move-result-object v1

    goto :goto_1

    .line 2624355
    :cond_5
    sget-object v1, LX/6fP;->OTHER:LX/6fP;

    invoke-virtual {v0, v1}, LX/Itl;->a(LX/6fP;)LX/Itl;

    move-result-object v1

    goto :goto_1
.end method

.method public final a()LX/Itl;
    .locals 2

    .prologue
    .line 2624356
    new-instance v0, LX/Itl;

    invoke-direct {v0, p0}, LX/Itl;-><init>(LX/Itm;)V

    return-object v0
.end method
