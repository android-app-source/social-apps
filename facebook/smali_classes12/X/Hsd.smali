.class public final LX/Hsd;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Hsf;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/attachments/angora/AngoraAttachmentView;

.field public b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

.field public final synthetic c:LX/Hsf;


# direct methods
.method public constructor <init>(LX/Hsf;)V
    .locals 1

    .prologue
    .line 2514714
    iput-object p1, p0, LX/Hsd;->c:LX/Hsf;

    .line 2514715
    move-object v0, p1

    .line 2514716
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2514717
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2514696
    const-string v0, "AngoraAttachmentComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2514700
    if-ne p0, p1, :cond_1

    .line 2514701
    :cond_0
    :goto_0
    return v0

    .line 2514702
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2514703
    goto :goto_0

    .line 2514704
    :cond_3
    check-cast p1, LX/Hsd;

    .line 2514705
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2514706
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2514707
    if-eq v2, v3, :cond_0

    .line 2514708
    iget-object v2, p0, LX/Hsd;->a:Lcom/facebook/attachments/angora/AngoraAttachmentView;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Hsd;->a:Lcom/facebook/attachments/angora/AngoraAttachmentView;

    iget-object v3, p1, LX/Hsd;->a:Lcom/facebook/attachments/angora/AngoraAttachmentView;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2514709
    goto :goto_0

    .line 2514710
    :cond_5
    iget-object v2, p1, LX/Hsd;->a:Lcom/facebook/attachments/angora/AngoraAttachmentView;

    if-nez v2, :cond_4

    .line 2514711
    :cond_6
    iget-object v2, p0, LX/Hsd;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/Hsd;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iget-object v3, p1, LX/Hsd;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2514712
    goto :goto_0

    .line 2514713
    :cond_7
    iget-object v2, p1, LX/Hsd;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 2514697
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/Hsd;

    .line 2514698
    const/4 v1, 0x0

    iput-object v1, v0, LX/Hsd;->a:Lcom/facebook/attachments/angora/AngoraAttachmentView;

    .line 2514699
    return-object v0
.end method
