.class public final LX/IWe;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2584543
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 2584544
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2584545
    :goto_0
    return v1

    .line 2584546
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2584547
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_4

    .line 2584548
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2584549
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2584550
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 2584551
    const-string v4, "__type__"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "__typename"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2584552
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v2

    goto :goto_1

    .line 2584553
    :cond_3
    const-string v4, "image"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2584554
    const/4 v3, 0x0

    .line 2584555
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v4, :cond_9

    .line 2584556
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2584557
    :goto_2
    move v0, v3

    .line 2584558
    goto :goto_1

    .line 2584559
    :cond_4
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2584560
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2584561
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2584562
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 2584563
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2584564
    :cond_7
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_8

    .line 2584565
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 2584566
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2584567
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_7

    if-eqz v4, :cond_7

    .line 2584568
    const-string v5, "uri"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 2584569
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_3

    .line 2584570
    :cond_8
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2584571
    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2584572
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_9
    move v0, v3

    goto :goto_3
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2584573
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2584574
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 2584575
    if-eqz v0, :cond_0

    .line 2584576
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2584577
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2584578
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2584579
    if-eqz v0, :cond_2

    .line 2584580
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2584581
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2584582
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2584583
    if-eqz v1, :cond_1

    .line 2584584
    const-string p1, "uri"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2584585
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2584586
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2584587
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2584588
    return-void
.end method
