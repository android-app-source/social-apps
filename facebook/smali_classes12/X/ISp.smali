.class public LX/ISp;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/1Kf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0W9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3iH;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Landroid/content/Context;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

.field public i:LX/8E8;

.field public j:Lcom/facebook/ipc/composer/intent/ComposerPageData;

.field private k:LX/ISe;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2578378
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2578379
    return-void
.end method

.method public static a(LX/0QB;)LX/ISp;
    .locals 1

    .prologue
    .line 2578361
    invoke-static {p0}, LX/ISp;->b(LX/0QB;)LX/ISp;

    move-result-object v0

    return-object v0
.end method

.method public static a$redex0(LX/ISp;Lcom/facebook/auth/viewercontext/ViewerContext;)V
    .locals 3

    .prologue
    .line 2578384
    invoke-static {p0}, LX/ISp;->h(LX/ISp;)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel$EdgesModel$NodeModel;

    move-result-object v0

    .line 2578385
    if-eqz v0, :cond_0

    .line 2578386
    invoke-static {}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->newBuilder()Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel$EdgesModel$NodeModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPageName(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel$EdgesModel$NodeModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPageProfilePicUrl(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPostAsPageViewerContext(Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v0

    iput-object v0, p0, LX/ISp;->j:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    .line 2578387
    :cond_0
    iget-object v0, p0, LX/ISp;->k:LX/ISe;

    if-eqz v0, :cond_1

    .line 2578388
    iget-object v0, p0, LX/ISp;->k:LX/ISe;

    iget-object v1, p0, LX/ISp;->j:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    .line 2578389
    iget-object v2, v0, LX/ISe;->a:Lcom/facebook/groups/feed/ui/GroupsInlineComposer;

    .line 2578390
    iput-object v1, v2, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->x:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    .line 2578391
    iget-object v2, v0, LX/ISe;->a:Lcom/facebook/groups/feed/ui/GroupsInlineComposer;

    invoke-static {v2}, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->h$redex0(Lcom/facebook/groups/feed/ui/GroupsInlineComposer;)V

    .line 2578392
    iget-object v2, v0, LX/ISe;->a:Lcom/facebook/groups/feed/ui/GroupsInlineComposer;

    .line 2578393
    const/4 v0, 0x0

    iput-object v0, v2, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->F:LX/34b;

    .line 2578394
    invoke-static {v2}, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->getBottomSheetAdapterForMorebutton(Lcom/facebook/groups/feed/ui/GroupsInlineComposer;)LX/34b;

    move-result-object v0

    iput-object v0, v2, Lcom/facebook/groups/feed/ui/GroupsInlineComposer;->F:LX/34b;

    .line 2578395
    :cond_1
    return-void
.end method

.method public static b(LX/0QB;)LX/ISp;
    .locals 8

    .prologue
    .line 2578380
    new-instance v0, LX/ISp;

    invoke-direct {v0}, LX/ISp;-><init>()V

    .line 2578381
    invoke-static {p0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v1

    check-cast v1, LX/1Kf;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v2

    check-cast v2, LX/0Uh;

    invoke-static {p0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v3

    check-cast v3, LX/0W9;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    const/16 v5, 0xf07

    invoke-static {p0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x1430

    invoke-static {p0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const-class v7, Landroid/content/Context;

    invoke-interface {p0, v7}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Context;

    .line 2578382
    iput-object v1, v0, LX/ISp;->a:LX/1Kf;

    iput-object v2, v0, LX/ISp;->b:LX/0Uh;

    iput-object v3, v0, LX/ISp;->c:LX/0W9;

    iput-object v4, v0, LX/ISp;->d:LX/0ad;

    iput-object v5, v0, LX/ISp;->e:LX/0Ot;

    iput-object v6, v0, LX/ISp;->f:LX/0Ot;

    iput-object v7, v0, LX/ISp;->g:Landroid/content/Context;

    .line 2578383
    return-object v0
.end method

.method public static d(LX/ISp;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration;
    .locals 4

    .prologue
    .line 2578376
    iget-object v0, p0, LX/ISp;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-static {v0}, LX/DZD;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)LX/DZC;

    move-result-object v0

    iget-object v1, p0, LX/ISp;->b:LX/0Uh;

    sget v2, LX/7l1;->j:I

    invoke-virtual {v1, v2}, LX/0Uh;->a(I)LX/03R;

    move-result-object v1

    iget-object v2, p0, LX/ISp;->c:LX/0W9;

    invoke-static {v0, v1, v2}, LX/DJw;->a(LX/DZC;LX/03R;LX/0W9;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    iget-object v1, p0, LX/ISp;->d:LX/0ad;

    sget-short v2, LX/1EB;->an:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setUsePublishExperiment(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {p0}, LX/ISp;->c()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setShowPageVoiceSwitcher(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    iget-object v1, p0, LX/ISp;->j:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialPageData(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    return-object v0
.end method

.method public static g(LX/ISp;)Z
    .locals 1

    .prologue
    .line 2578377
    iget-object v0, p0, LX/ISp;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->J()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ISp;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->J()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static h(LX/ISp;)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel$EdgesModel$NodeModel;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2578375
    iget-object v0, p0, LX/ISp;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->J()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ISp;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->J()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ISp;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->J()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;->a()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel$EdgesModel;->j()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel$EdgesModel$NodeModel;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;LX/ISe;)V
    .locals 2
    .param p2    # LX/ISe;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2578364
    iput-object p1, p0, LX/ISp;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 2578365
    iput-object p2, p0, LX/ISp;->k:LX/ISe;

    .line 2578366
    invoke-static {p0}, LX/ISp;->g(LX/ISp;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/ISp;->j:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    if-nez v0, :cond_1

    .line 2578367
    invoke-static {p0}, LX/ISp;->h(LX/ISp;)Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel$EdgesModel$NodeModel;

    move-result-object v1

    .line 2578368
    if-eqz v1, :cond_1

    .line 2578369
    iget-object v0, p0, LX/ISp;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3iH;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel$EdgesModel$NodeModel;->b()Ljava/lang/String;

    move-result-object p1

    .line 2578370
    iget-object v1, p0, LX/ISp;->i:LX/8E8;

    if-nez v1, :cond_0

    .line 2578371
    new-instance v1, LX/ISo;

    invoke-direct {v1, p0}, LX/ISo;-><init>(LX/ISp;)V

    iput-object v1, p0, LX/ISp;->i:LX/8E8;

    .line 2578372
    :cond_0
    iget-object v1, p0, LX/ISp;->i:LX/8E8;

    move-object p2, v1

    .line 2578373
    iget-object v1, p0, LX/ISp;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    invoke-virtual {v0, p1, p2, v1}, LX/3iH;->a(Ljava/lang/String;LX/8E8;Ljava/util/concurrent/Executor;)V

    .line 2578374
    :cond_1
    return-void
.end method

.method public final b()V
    .locals 6

    .prologue
    .line 2578362
    iget-object v1, p0, LX/ISp;->a:LX/1Kf;

    const/4 v2, 0x0

    invoke-static {p0}, LX/ISp;->d(LX/ISp;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v3

    const/16 v4, 0x6dc

    iget-object v0, p0, LX/ISp;->g:Landroid/content/Context;

    const-class v5, Landroid/app/Activity;

    invoke-static {v0, v5}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-interface {v1, v2, v3, v4, v0}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 2578363
    return-void
.end method

.method public final c()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2578360
    invoke-static {p0}, LX/ISp;->g(LX/ISp;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/ISp;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->J()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupPageAdminActorsInformationModel$GroupAdminedPageMembersModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ne v1, v0, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    iget-object v2, p0, LX/ISp;->h:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
