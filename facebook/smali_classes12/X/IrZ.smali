.class public LX/IrZ;
.super LX/3wu;
.source ""


# instance fields
.field private t:F


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0

    .prologue
    .line 2618254
    invoke-direct {p0, p1, p2}, LX/3wu;-><init>(Landroid/content/Context;I)V

    .line 2618255
    return-void
.end method

.method public static m(LX/IrZ;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2618256
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    .line 2618257
    iget v0, p0, LX/IrZ;->t:F

    invoke-virtual {p1, v0}, Landroid/view/View;->setRotation(F)V

    .line 2618258
    :goto_0
    return-void

    .line 2618259
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setRotation(F)V

    goto :goto_0
.end method


# virtual methods
.method public final b(Landroid/view/View;I)V
    .locals 0

    .prologue
    .line 2618260
    invoke-super {p0, p1, p2}, LX/3wu;->b(Landroid/view/View;I)V

    .line 2618261
    invoke-static {p0, p1}, LX/IrZ;->m(LX/IrZ;Landroid/view/View;)V

    .line 2618262
    return-void
.end method

.method public final c(LX/1Od;LX/1Ok;)V
    .locals 1

    .prologue
    .line 2618263
    invoke-super {p0, p1, p2}, LX/3wu;->c(LX/1Od;LX/1Ok;)V

    .line 2618264
    const/4 v0, 0x0

    invoke-virtual {p0}, LX/1OR;->v()I

    move-result p1

    :goto_0
    if-ge v0, p1, :cond_0

    .line 2618265
    invoke-virtual {p0, v0}, LX/1OR;->f(I)Landroid/view/View;

    move-result-object p2

    .line 2618266
    invoke-static {p0, p2}, LX/IrZ;->m(LX/IrZ;Landroid/view/View;)V

    .line 2618267
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2618268
    :cond_0
    return-void
.end method
