.class public final LX/Ir8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;)V
    .locals 0

    .prologue
    .line 2617435
    iput-object p1, p0, LX/Ir8;->a:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2617436
    iget-object v0, p0, LX/Ir8;->a:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->z:Lcom/facebook/messaging/doodle/CaptionEditorView;

    invoke-virtual {v0}, Lcom/facebook/messaging/doodle/CaptionEditorView;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2617437
    iget-object v0, p0, LX/Ir8;->a:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    const/4 v1, 0x1

    iget-object v2, p0, LX/Ir8;->a:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    iget-object v2, v2, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->F:Landroid/view/View;

    invoke-static {v0, v1, v2}, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->a$redex0(Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;ILandroid/view/View;)V

    .line 2617438
    iget-object v0, p0, LX/Ir8;->a:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->F:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2617439
    :goto_0
    iget-object v0, p0, LX/Ir8;->a:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->F:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 2617440
    return-void

    .line 2617441
    :cond_0
    iget-object v0, p0, LX/Ir8;->a:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    iget-object v1, p0, LX/Ir8;->a:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    iget-object v1, v1, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->F:Landroid/view/View;

    invoke-static {v0, v2, v1}, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->a$redex0(Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;ILandroid/view/View;)V

    .line 2617442
    iget-object v0, p0, LX/Ir8;->a:Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;

    iget-object v0, v0, Lcom/facebook/messaging/photos/editing/MessengerPhotoEditDialogFragment;->F:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
