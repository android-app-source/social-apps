.class public LX/Ipn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;


# instance fields
.field public final fetchPaymentMethods:Ljava/lang/Boolean;

.field public final fetchTransferFbId:Ljava/lang/Long;

.field public final irisSeqId:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v4, 0xa

    const/4 v2, 0x2

    const/4 v3, 0x1

    .line 2614382
    new-instance v0, LX/1sv;

    const-string v1, "DeltaPaymentForcedFetch"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/Ipn;->b:LX/1sv;

    .line 2614383
    new-instance v0, LX/1sw;

    const-string v1, "fetchTransferFbId"

    invoke-direct {v0, v1, v4, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipn;->c:LX/1sw;

    .line 2614384
    new-instance v0, LX/1sw;

    const-string v1, "fetchPaymentMethods"

    invoke-direct {v0, v1, v2, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipn;->d:LX/1sw;

    .line 2614385
    new-instance v0, LX/1sw;

    const-string v1, "irisSeqId"

    const/16 v2, 0x3e8

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ipn;->e:LX/1sw;

    .line 2614386
    sput-boolean v3, LX/Ipn;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 2614387
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2614388
    iput-object p1, p0, LX/Ipn;->fetchTransferFbId:Ljava/lang/Long;

    .line 2614389
    iput-object p2, p0, LX/Ipn;->fetchPaymentMethods:Ljava/lang/Boolean;

    .line 2614390
    iput-object p3, p0, LX/Ipn;->irisSeqId:Ljava/lang/Long;

    .line 2614391
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2614392
    if-eqz p2, :cond_4

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    .line 2614393
    :goto_0
    if-eqz p2, :cond_5

    const-string v0, "\n"

    move-object v3, v0

    .line 2614394
    :goto_1
    if-eqz p2, :cond_6

    const-string v0, " "

    .line 2614395
    :goto_2
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v1, "DeltaPaymentForcedFetch"

    invoke-direct {v5, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2614396
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614397
    const-string v1, "("

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614398
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614399
    const/4 v1, 0x1

    .line 2614400
    iget-object v6, p0, LX/Ipn;->fetchTransferFbId:Ljava/lang/Long;

    if-eqz v6, :cond_0

    .line 2614401
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614402
    const-string v1, "fetchTransferFbId"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614403
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614404
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614405
    iget-object v1, p0, LX/Ipn;->fetchTransferFbId:Ljava/lang/Long;

    if-nez v1, :cond_7

    .line 2614406
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    move v1, v2

    .line 2614407
    :cond_0
    iget-object v6, p0, LX/Ipn;->fetchPaymentMethods:Ljava/lang/Boolean;

    if-eqz v6, :cond_a

    .line 2614408
    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, ","

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614409
    :cond_1
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614410
    const-string v1, "fetchPaymentMethods"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614411
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614412
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614413
    iget-object v1, p0, LX/Ipn;->fetchPaymentMethods:Ljava/lang/Boolean;

    if-nez v1, :cond_8

    .line 2614414
    const-string v1, "null"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614415
    :goto_4
    iget-object v1, p0, LX/Ipn;->irisSeqId:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 2614416
    if-nez v2, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614417
    :cond_2
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614418
    const-string v1, "irisSeqId"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614419
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614420
    const-string v1, ":"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614421
    iget-object v0, p0, LX/Ipn;->irisSeqId:Ljava/lang/Long;

    if-nez v0, :cond_9

    .line 2614422
    const-string v0, "null"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614423
    :cond_3
    :goto_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v4}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614424
    const-string v0, ")"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614425
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2614426
    :cond_4
    const-string v0, ""

    move-object v4, v0

    goto/16 :goto_0

    .line 2614427
    :cond_5
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_1

    .line 2614428
    :cond_6
    const-string v0, ""

    goto/16 :goto_2

    .line 2614429
    :cond_7
    iget-object v1, p0, LX/Ipn;->fetchTransferFbId:Ljava/lang/Long;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 2614430
    :cond_8
    iget-object v1, p0, LX/Ipn;->fetchPaymentMethods:Ljava/lang/Boolean;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v1, v6, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 2614431
    :cond_9
    iget-object v0, p0, LX/Ipn;->irisSeqId:Ljava/lang/Long;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5

    :cond_a
    move v2, v1

    goto/16 :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 2

    .prologue
    .line 2614432
    invoke-virtual {p1}, LX/1su;->a()V

    .line 2614433
    iget-object v0, p0, LX/Ipn;->fetchTransferFbId:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 2614434
    iget-object v0, p0, LX/Ipn;->fetchTransferFbId:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 2614435
    sget-object v0, LX/Ipn;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2614436
    iget-object v0, p0, LX/Ipn;->fetchTransferFbId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2614437
    :cond_0
    iget-object v0, p0, LX/Ipn;->fetchPaymentMethods:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 2614438
    iget-object v0, p0, LX/Ipn;->fetchPaymentMethods:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 2614439
    sget-object v0, LX/Ipn;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2614440
    iget-object v0, p0, LX/Ipn;->fetchPaymentMethods:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(Z)V

    .line 2614441
    :cond_1
    iget-object v0, p0, LX/Ipn;->irisSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 2614442
    iget-object v0, p0, LX/Ipn;->irisSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 2614443
    sget-object v0, LX/Ipn;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2614444
    iget-object v0, p0, LX/Ipn;->irisSeqId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2614445
    :cond_2
    invoke-virtual {p1}, LX/1su;->c()V

    .line 2614446
    invoke-virtual {p1}, LX/1su;->b()V

    .line 2614447
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2614448
    if-nez p1, :cond_1

    .line 2614449
    :cond_0
    :goto_0
    return v0

    .line 2614450
    :cond_1
    instance-of v1, p1, LX/Ipn;

    if-eqz v1, :cond_0

    .line 2614451
    check-cast p1, LX/Ipn;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2614452
    if-nez p1, :cond_3

    .line 2614453
    :cond_2
    :goto_1
    move v0, v2

    .line 2614454
    goto :goto_0

    .line 2614455
    :cond_3
    iget-object v0, p0, LX/Ipn;->fetchTransferFbId:Ljava/lang/Long;

    if-eqz v0, :cond_a

    move v0, v1

    .line 2614456
    :goto_2
    iget-object v3, p1, LX/Ipn;->fetchTransferFbId:Ljava/lang/Long;

    if-eqz v3, :cond_b

    move v3, v1

    .line 2614457
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 2614458
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2614459
    iget-object v0, p0, LX/Ipn;->fetchTransferFbId:Ljava/lang/Long;

    iget-object v3, p1, LX/Ipn;->fetchTransferFbId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2614460
    :cond_5
    iget-object v0, p0, LX/Ipn;->fetchPaymentMethods:Ljava/lang/Boolean;

    if-eqz v0, :cond_c

    move v0, v1

    .line 2614461
    :goto_4
    iget-object v3, p1, LX/Ipn;->fetchPaymentMethods:Ljava/lang/Boolean;

    if-eqz v3, :cond_d

    move v3, v1

    .line 2614462
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 2614463
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2614464
    iget-object v0, p0, LX/Ipn;->fetchPaymentMethods:Ljava/lang/Boolean;

    iget-object v3, p1, LX/Ipn;->fetchPaymentMethods:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2614465
    :cond_7
    iget-object v0, p0, LX/Ipn;->irisSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_e

    move v0, v1

    .line 2614466
    :goto_6
    iget-object v3, p1, LX/Ipn;->irisSeqId:Ljava/lang/Long;

    if-eqz v3, :cond_f

    move v3, v1

    .line 2614467
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 2614468
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2614469
    iget-object v0, p0, LX/Ipn;->irisSeqId:Ljava/lang/Long;

    iget-object v3, p1, LX/Ipn;->irisSeqId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_9
    move v2, v1

    .line 2614470
    goto :goto_1

    :cond_a
    move v0, v2

    .line 2614471
    goto :goto_2

    :cond_b
    move v3, v2

    .line 2614472
    goto :goto_3

    :cond_c
    move v0, v2

    .line 2614473
    goto :goto_4

    :cond_d
    move v3, v2

    .line 2614474
    goto :goto_5

    :cond_e
    move v0, v2

    .line 2614475
    goto :goto_6

    :cond_f
    move v3, v2

    .line 2614476
    goto :goto_7
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2614477
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2614478
    sget-boolean v0, LX/Ipn;->a:Z

    .line 2614479
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/Ipn;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2614480
    return-object v0
.end method
