.class public LX/HYq;
.super LX/HYp;
.source ""


# instance fields
.field public final b:Lcom/facebook/registration/model/SimpleRegFormData;

.field public final c:LX/HZt;

.field public final d:LX/0Uh;


# direct methods
.method public constructor <init>(Lcom/facebook/registration/model/SimpleRegFormData;LX/HZt;LX/0Uh;)V
    .locals 0
    .param p3    # LX/0Uh;
        .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2481049
    invoke-direct {p0}, LX/HYp;-><init>()V

    .line 2481050
    iput-object p1, p0, LX/HYq;->b:Lcom/facebook/registration/model/SimpleRegFormData;

    .line 2481051
    iput-object p2, p0, LX/HYq;->c:LX/HZt;

    .line 2481052
    iput-object p3, p0, LX/HYq;->d:LX/0Uh;

    .line 2481053
    iget-object p1, p0, LX/HYp;->a:Ljava/util/Map;

    sget-object p2, LX/HYj;->EMAIL_SWITCH_TO_PHONE:LX/HYj;

    invoke-static {}, LX/HYq;->f()LX/HZ1;

    move-result-object p3

    invoke-interface {p1, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2481054
    iget-object p1, p0, LX/HYp;->a:Ljava/util/Map;

    sget-object p2, LX/HYj;->PHONE_SWITCH_TO_EMAIL:LX/HYj;

    new-instance p3, LX/HYo;

    invoke-direct {p3, p0}, LX/HYo;-><init>(LX/HYq;)V

    invoke-interface {p1, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2481055
    return-void
.end method

.method public static f()LX/HZ1;
    .locals 2

    .prologue
    .line 2481048
    new-instance v0, LX/HZ1;

    const-class v1, Lcom/facebook/registration/fragment/RegistrationPhoneFragment;

    invoke-direct {v0, v1}, LX/HZ1;-><init>(Ljava/lang/Class;)V

    return-object v0
.end method

.method public static g(LX/HYq;)LX/HZ1;
    .locals 3

    .prologue
    .line 2481042
    iget-object v0, p0, LX/HYq;->d:LX/0Uh;

    const/16 v1, 0x39

    invoke-virtual {v0, v1}, LX/0Uh;->a(I)LX/03R;

    move-result-object v0

    .line 2481043
    iget-object v1, p0, LX/HYq;->c:LX/HZt;

    const-string v2, "reg_email_prefill"

    invoke-virtual {v1, v2, v0}, LX/HZt;->a(Ljava/lang/String;LX/03R;)V

    .line 2481044
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    move v0, v0

    .line 2481045
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HYq;->b:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v0}, Lcom/facebook/registration/model/RegistrationFormData;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2481046
    new-instance v0, LX/HZ1;

    const-class v1, Lcom/facebook/registration/fragment/RegistrationOptionalPrefillEmailFragment;

    invoke-direct {v0, v1}, LX/HZ1;-><init>(Ljava/lang/Class;)V

    .line 2481047
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/HZ1;

    const-class v1, Lcom/facebook/registration/fragment/RegistrationEmailFragment;

    invoke-direct {v0, v1}, LX/HZ1;-><init>(Ljava/lang/Class;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(ZZ)LX/HYm;
    .locals 1

    .prologue
    .line 2481041
    new-instance v0, LX/HYn;

    invoke-direct {v0, p0, p1, p2}, LX/HYn;-><init>(LX/HYq;ZZ)V

    return-object v0
.end method
