.class public LX/IcC;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:Lcom/facebook/content/SecureContextHelper;

.field private final c:LX/IcF;

.field private final d:LX/0Uh;

.field public final e:LX/IZK;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/IcF;LX/0Uh;LX/IZK;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2595263
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2595264
    iput-object p1, p0, LX/IcC;->a:Landroid/content/Context;

    .line 2595265
    iput-object p2, p0, LX/IcC;->b:Lcom/facebook/content/SecureContextHelper;

    .line 2595266
    iput-object p3, p0, LX/IcC;->c:LX/IcF;

    .line 2595267
    iput-object p4, p0, LX/IcC;->d:LX/0Uh;

    .line 2595268
    iput-object p5, p0, LX/IcC;->e:LX/IZK;

    .line 2595269
    return-void
.end method

.method public static a(LX/0QB;)LX/IcC;
    .locals 1

    .prologue
    .line 2595270
    invoke-static {p0}, LX/IcC;->b(LX/0QB;)LX/IcC;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/IcC;Ljava/lang/String;Ljava/lang/String;LX/15i;I)V
    .locals 5

    .prologue
    .line 2595271
    new-instance v0, Landroid/content/Intent;

    sget-object v1, LX/3GK;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2595272
    sget-object v1, LX/3RH;->r:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2595273
    const-string v1, "ShareType"

    const-string v2, "ShareType.ridePromoShare"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2595274
    const-string v1, "parcelable_share_extras"

    const/4 v2, 0x3

    invoke-virtual {p3, p4, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {p3, p4, v3}, LX/15i;->g(II)I

    move-result v3

    .line 2595275
    if-nez v3, :cond_2

    .line 2595276
    const/4 v4, 0x0

    .line 2595277
    :goto_0
    move-object v3, v4

    .line 2595278
    new-instance v4, Lcom/facebook/messaging/business/ride/utils/RidePromoShareExtras;

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p3

    if-eqz p3, :cond_0

    iget-object p3, p0, LX/IcC;->a:Landroid/content/Context;

    const p4, 0x7f082d8e

    invoke-virtual {p3, p4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    :cond_0
    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p3

    if-eqz p3, :cond_1

    const-string p2, ""

    :cond_1
    invoke-direct {v4, p1, v2, v3, p2}, Lcom/facebook/messaging/business/ride/utils/RidePromoShareExtras;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;)V

    move-object v2, v4

    .line 2595279
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2595280
    iget-object v1, p0, LX/IcC;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/IcC;->a:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2595281
    return-void

    :cond_2
    invoke-static {}, Lcom/facebook/ui/media/attachments/MediaResource;->a()LX/5zn;

    move-result-object v4

    const/4 p4, 0x0

    invoke-virtual {p3, v3, p4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object p4

    invoke-static {p4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p4

    .line 2595282
    iput-object p4, v4, LX/5zn;->b:Landroid/net/Uri;

    .line 2595283
    move-object v4, v4

    .line 2595284
    sget-object p4, LX/5zj;->SHARE:LX/5zj;

    .line 2595285
    iput-object p4, v4, LX/5zn;->d:LX/5zj;

    .line 2595286
    move-object v4, v4

    .line 2595287
    sget-object p4, LX/2MK;->PHOTO:LX/2MK;

    .line 2595288
    iput-object p4, v4, LX/5zn;->c:LX/2MK;

    .line 2595289
    move-object v4, v4

    .line 2595290
    invoke-virtual {v4}, LX/5zn;->G()Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v4

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/IcC;
    .locals 13

    .prologue
    .line 2595291
    new-instance v0, LX/IcC;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    .line 2595292
    new-instance v6, LX/IcF;

    invoke-static {p0}, LX/2Og;->a(LX/0QB;)LX/2Og;

    move-result-object v7

    check-cast v7, LX/2Og;

    invoke-static {p0}, LX/FDq;->a(LX/0QB;)LX/FDq;

    move-result-object v8

    check-cast v8, LX/FDq;

    const/16 v9, 0x15e7

    invoke-static {p0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v10

    check-cast v10, LX/03V;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v11

    check-cast v11, LX/0tX;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v12

    check-cast v12, LX/1Ck;

    invoke-direct/range {v6 .. v12}, LX/IcF;-><init>(LX/2Og;LX/FDq;LX/0Or;LX/03V;LX/0tX;LX/1Ck;)V

    .line 2595293
    move-object v3, v6

    .line 2595294
    check-cast v3, LX/IcF;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-static {p0}, LX/IZK;->b(LX/0QB;)LX/IZK;

    move-result-object v5

    check-cast v5, LX/IZK;

    invoke-direct/range {v0 .. v5}, LX/IcC;-><init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/IcF;LX/0Uh;LX/IZK;)V

    .line 2595295
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 2595296
    iget-object v0, p0, LX/IcC;->c:LX/IcF;

    new-instance v1, LX/IcA;

    invoke-direct {v1, p0, p1, p2}, LX/IcA;-><init>(LX/IcC;Ljava/lang/String;Ljava/lang/String;)V

    .line 2595297
    new-instance v2, LX/IbH;

    invoke-direct {v2}, LX/IbH;-><init>()V

    move-object v2, v2

    .line 2595298
    const-string v3, "provider"

    invoke-virtual {v2, v3, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2595299
    const-string v3, "promo_data"

    invoke-virtual {v2, v3, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2595300
    iget-object v3, v0, LX/IcF;->f:LX/1Ck;

    sget-object v4, LX/IcE;->GET_RIDE_PROMO_SHARE:LX/IcE;

    iget-object p0, v0, LX/IcF;->e:LX/0tX;

    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    .line 2595301
    new-instance p0, LX/IcD;

    invoke-direct {p0, v0, v1}, LX/IcD;-><init>(LX/IcF;LX/IcA;)V

    move-object p0, p0

    .line 2595302
    invoke-virtual {v3, v4, v2, p0}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2595303
    return-void
.end method
