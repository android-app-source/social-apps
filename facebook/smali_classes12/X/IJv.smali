.class public final LX/IJv;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groupcommerce/protocol/FetchGroupCommercePreferredMarketplaceInfoModels$PreferredMarketplaceQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Landroid/support/v4/app/DialogFragment;

.field public final synthetic c:Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;

.field public final synthetic d:Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;Ljava/lang/String;Landroid/support/v4/app/DialogFragment;Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;)V
    .locals 0

    .prologue
    .line 2564693
    iput-object p1, p0, LX/IJv;->d:Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;

    iput-object p2, p0, LX/IJv;->a:Ljava/lang/String;

    iput-object p3, p0, LX/IJv;->b:Landroid/support/v4/app/DialogFragment;

    iput-object p4, p0, LX/IJv;->c:Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 6
    .param p1    # Lcom/facebook/graphql/executor/GraphQLResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groupcommerce/protocol/FetchGroupCommercePreferredMarketplaceInfoModels$PreferredMarketplaceQueryModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 2564695
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2564696
    check-cast v0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommercePreferredMarketplaceInfoModels$PreferredMarketplaceQueryModel;

    .line 2564697
    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommercePreferredMarketplaceInfoModels$PreferredMarketplaceQueryModel;->a()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-nez v1, :cond_1

    .line 2564698
    :cond_0
    :goto_0
    return-void

    .line 2564699
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommercePreferredMarketplaceInfoModels$PreferredMarketplaceQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, 0x4f0e8639

    invoke-static {v1, v0, v3, v2}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 2564700
    if-eqz v0, :cond_2

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    .line 2564701
    :goto_1
    invoke-virtual {v0}, LX/39O;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2564702
    invoke-virtual {v0, v3}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2564703
    const-class v2, Lcom/facebook/groupcommerce/protocol/FetchGroupCommercePreferredMarketplaceInfoModels$PreferredMarketplaceQueryModel$PreferredMarketplacesModel$EdgesModel$NodeModel;

    invoke-virtual {v1, v0, v3, v2}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/groupcommerce/protocol/FetchGroupCommercePreferredMarketplaceInfoModels$PreferredMarketplaceQueryModel$PreferredMarketplacesModel$EdgesModel$NodeModel;

    .line 2564704
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommercePreferredMarketplaceInfoModels$PreferredMarketplaceQueryModel$PreferredMarketplacesModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2564705
    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommercePreferredMarketplaceInfoModels$PreferredMarketplaceQueryModel$PreferredMarketplacesModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2564706
    new-instance v1, LX/89I;

    iget-object v4, p0, LX/IJv;->a:Ljava/lang/String;

    invoke-static {v4}, LX/2rw;->fromString(Ljava/lang/String;)LX/2rw;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, LX/89I;-><init>(JLX/2rw;)V

    .line 2564707
    invoke-virtual {v0}, Lcom/facebook/groupcommerce/protocol/FetchGroupCommercePreferredMarketplaceInfoModels$PreferredMarketplaceQueryModel$PreferredMarketplacesModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v0

    .line 2564708
    iput-object v0, v1, LX/89I;->c:Ljava/lang/String;

    .line 2564709
    iget-object v0, p0, LX/IJv;->d:Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;

    iget-object v0, v0, Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;->v:LX/0W9;

    invoke-virtual {v0}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v0

    .line 2564710
    invoke-static {v0}, Ljava/util/Currency;->getInstance(Ljava/util/Locale;)Ljava/util/Currency;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Currency;->getCurrencyCode()Ljava/lang/String;

    move-result-object v0

    .line 2564711
    new-instance v2, LX/2rZ;

    invoke-direct {v2}, LX/2rZ;-><init>()V

    iget-object v3, p0, LX/IJv;->d:Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;

    iget-object v3, v3, Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;->w:Landroid/content/res/Resources;

    const v4, 0x7f082465

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2564712
    iput-object v3, v2, LX/2rZ;->d:Ljava/lang/String;

    .line 2564713
    move-object v2, v2

    .line 2564714
    invoke-virtual {v2}, LX/2rZ;->a()Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    move-result-object v2

    .line 2564715
    sget-object v3, LX/21D;->GROUP_FEED:LX/21D;

    const-string v4, "forSalePostSellComposerActivity"

    invoke-virtual {v1, v2}, LX/89I;->a(LX/2rX;)LX/89I;

    move-result-object v1

    invoke-virtual {v1}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    invoke-static {v3, v4, v0, v1, v5}, LX/1nC;->a(LX/21D;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerTargetData;LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    new-instance v1, LX/89K;

    invoke-direct {v1}, LX/89K;-><init>()V

    invoke-static {}, LX/88g;->c()LX/88g;

    move-result-object v1

    invoke-static {v1}, LX/89K;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPluginConfig(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    .line 2564716
    iget-object v1, p0, LX/IJv;->b:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2564717
    iget-object v1, p0, LX/IJv;->d:Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;

    iget-object v1, v1, Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;->s:LX/1Kf;

    const/16 v2, 0x6dc

    iget-object v3, p0, LX/IJv;->c:Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;

    invoke-interface {v1, v5, v0, v2, v3}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    goto/16 :goto_0

    .line 2564718
    :cond_2
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto/16 :goto_1
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2564719
    iget-object v0, p0, LX/IJv;->b:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2564720
    iget-object v0, p0, LX/IJv;->d:Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;

    invoke-virtual {v0}, Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;->finish()V

    .line 2564721
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2564694
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-direct {p0, p1}, LX/IJv;->a(Lcom/facebook/graphql/executor/GraphQLResult;)V

    return-void
.end method
