.class public final LX/HLG;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Z

.field public b:Z

.field public c:Z

.field public d:LX/25L;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:LX/25L;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:I

.field public g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 2455716
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2455717
    iput v1, p0, LX/HLG;->f:I

    .line 2455718
    iput-boolean v1, p0, LX/HLG;->a:Z

    .line 2455719
    iput-boolean v1, p0, LX/HLG;->b:Z

    .line 2455720
    iput-boolean v1, p0, LX/HLG;->c:Z

    .line 2455721
    iput-object v0, p0, LX/HLG;->d:LX/25L;

    .line 2455722
    iput-object v0, p0, LX/HLG;->e:LX/25L;

    .line 2455723
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/HLG;->g:Ljava/util/Map;

    .line 2455724
    iput v1, p0, LX/HLG;->f:I

    .line 2455725
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2455726
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2455727
    iput v1, p0, LX/HLG;->f:I

    .line 2455728
    iput-boolean v1, p0, LX/HLG;->a:Z

    .line 2455729
    iput-boolean v1, p0, LX/HLG;->b:Z

    .line 2455730
    iput-boolean v1, p0, LX/HLG;->c:Z

    .line 2455731
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/HLG;->g:Ljava/util/Map;

    .line 2455732
    iput v1, p0, LX/HLG;->f:I

    .line 2455733
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_facepile"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/25L;->a(Ljava/lang/String;)LX/25L;

    move-result-object v0

    move-object v0, v0

    .line 2455734
    iput-object v0, p0, LX/HLG;->d:LX/25L;

    .line 2455735
    invoke-static {p0, p1}, LX/HLG;->b(LX/HLG;Ljava/lang/String;)LX/25L;

    move-result-object v0

    iput-object v0, p0, LX/HLG;->e:LX/25L;

    .line 2455736
    return-void
.end method

.method public static b(LX/HLG;Ljava/lang/String;)LX/25L;
    .locals 2

    .prologue
    .line 2455737
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/HLG;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_pages"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/25L;->a(Ljava/lang/String;)LX/25L;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 2455738
    iget-object v0, p0, LX/HLG;->g:Ljava/util/Map;

    iget v1, p0, LX/HLG;->f:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HLG;->g:Ljava/util/Map;

    iget v1, p0, LX/HLG;->f:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
