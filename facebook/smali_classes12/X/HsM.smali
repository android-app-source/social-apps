.class public LX/HsM;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/HsL;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2514338
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 2514339
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/HsL;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData:",
            "Ljava/lang/Object;",
            ":",
            "LX/0io;",
            ":",
            "LX/0jD;",
            ":",
            "LX/0j6;",
            "DerivedData::",
            "LX/5RE;",
            "Mutation:",
            "Ljava/lang/Object;",
            ":",
            "Lcom/facebook/composer/attachments/ComposerAttachment$SetsAttachments",
            "<TMutation;>;:",
            "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
            "Services:",
            "Ljava/lang/Object;",
            ":",
            "LX/0il",
            "<TModelData;>;:",
            "LX/0ik",
            "<TDerivedData;>;:",
            "LX/0im",
            "<TMutation;>;>(TServices;)",
            "LX/HsL",
            "<TModelData;TDerivedData;TMutation;TServices;>;"
        }
    .end annotation

    .prologue
    .line 2514340
    new-instance v0, LX/HsL;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/9A6;->b(LX/0QB;)LX/9A6;

    move-result-object v2

    check-cast v2, LX/9A6;

    .line 2514341
    new-instance v7, LX/HsE;

    invoke-static {p0}, LX/0tK;->a(LX/0QB;)LX/0tK;

    move-result-object v3

    check-cast v3, LX/0tK;

    const-class v4, LX/26F;

    invoke-interface {p0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/26F;

    const-class v5, LX/26G;

    invoke-interface {p0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/26G;

    const-class v6, LX/26H;

    invoke-interface {p0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/26H;

    invoke-direct {v7, v3, v4, v5, v6}, LX/HsE;-><init>(LX/0tK;LX/26F;LX/26G;LX/26H;)V

    .line 2514342
    move-object v3, v7

    .line 2514343
    check-cast v3, LX/HsE;

    const-class v4, LX/HsD;

    invoke-interface {p0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/HsD;

    invoke-static {p0}, LX/7l0;->b(LX/0QB;)LX/7l0;

    move-result-object v5

    check-cast v5, LX/7l0;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v6

    check-cast v6, LX/1Ck;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v7

    check-cast v7, LX/0Uh;

    move-object v8, p1

    check-cast v8, LX/0il;

    invoke-direct/range {v0 .. v8}, LX/HsL;-><init>(Landroid/content/Context;LX/9A6;LX/HsE;LX/HsD;LX/7l0;LX/1Ck;LX/0Uh;LX/0il;)V

    .line 2514344
    return-object v0
.end method
