.class public final LX/IAA;
.super LX/BlU;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/events/permalink/guestlist/EventGuestListView;


# direct methods
.method public constructor <init>(Lcom/facebook/events/permalink/guestlist/EventGuestListView;)V
    .locals 0

    .prologue
    .line 2544743
    iput-object p1, p0, LX/IAA;->a:Lcom/facebook/events/permalink/guestlist/EventGuestListView;

    invoke-direct {p0}, LX/BlU;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/events/permalink/guestlist/EventGuestListView;B)V
    .locals 0

    .prologue
    .line 2544744
    invoke-direct {p0, p1}, LX/IAA;-><init>(Lcom/facebook/events/permalink/guestlist/EventGuestListView;)V

    return-void
.end method

.method private a(LX/BlT;)V
    .locals 3

    .prologue
    .line 2544745
    if-eqz p1, :cond_0

    iget-object v0, p1, LX/BlT;->c:Lcom/facebook/events/model/Event;

    .line 2544746
    iget-object v1, v0, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v0, v1

    .line 2544747
    iget-object v1, p0, LX/IAA;->a:Lcom/facebook/events/permalink/guestlist/EventGuestListView;

    iget-object v1, v1, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->i:Lcom/facebook/events/model/Event;

    .line 2544748
    iget-object v2, v1, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2544749
    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2544750
    :cond_0
    :goto_0
    return-void

    .line 2544751
    :cond_1
    sget-object v0, LX/IA9;->b:[I

    iget-object v1, p1, LX/BlT;->a:LX/BlI;

    invoke-virtual {v1}, LX/BlI;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 2544752
    :pswitch_0
    sget-object v0, LX/IA9;->a:[I

    iget-object v1, p1, LX/BlT;->c:Lcom/facebook/events/model/Event;

    invoke-virtual {v1}, Lcom/facebook/events/model/Event;->F()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 2544753
    :goto_1
    iget-object v0, p0, LX/IAA;->a:Lcom/facebook/events/permalink/guestlist/EventGuestListView;

    iget-object v0, v0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->m:Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;

    iget-object v1, p1, LX/BlT;->c:Lcom/facebook/events/model/Event;

    invoke-virtual {v0, v1}, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->a(Lcom/facebook/events/model/Event;)V

    .line 2544754
    sget-object v0, LX/IA9;->a:[I

    iget-object v1, p1, LX/BlT;->b:Lcom/facebook/events/model/Event;

    invoke-virtual {v1}, Lcom/facebook/events/model/Event;->F()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    .line 2544755
    :pswitch_1
    iget-object v0, p0, LX/IAA;->a:Lcom/facebook/events/permalink/guestlist/EventGuestListView;

    iget-object v0, v0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->o:Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;

    .line 2544756
    iget-object v1, v0, Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;->a:Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;

    move-object v0, v1

    .line 2544757
    invoke-virtual {v0}, Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;->b()V

    goto :goto_0

    .line 2544758
    :pswitch_2
    iget-object v0, p0, LX/IAA;->a:Lcom/facebook/events/permalink/guestlist/EventGuestListView;

    iget-object v0, v0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->o:Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;

    .line 2544759
    iget-object v1, v0, Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;->a:Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;

    move-object v0, v1

    .line 2544760
    invoke-virtual {v0}, Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;->a()V

    goto :goto_1

    .line 2544761
    :pswitch_3
    iget-object v0, p0, LX/IAA;->a:Lcom/facebook/events/permalink/guestlist/EventGuestListView;

    iget-object v0, v0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->o:Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;

    .line 2544762
    iget-object v1, v0, Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;->b:Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;

    move-object v0, v1

    .line 2544763
    invoke-virtual {v0}, Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;->a()V

    goto :goto_1

    .line 2544764
    :pswitch_4
    iget-object v0, p0, LX/IAA;->a:Lcom/facebook/events/permalink/guestlist/EventGuestListView;

    iget-object v0, v0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->o:Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;

    .line 2544765
    iget-object v1, v0, Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;->b:Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;

    move-object v0, v1

    .line 2544766
    invoke-virtual {v0}, Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;->b()V

    goto :goto_0

    .line 2544767
    :pswitch_5
    iget-object v0, p0, LX/IAA;->a:Lcom/facebook/events/permalink/guestlist/EventGuestListView;

    iget-object v0, v0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->o:Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;

    .line 2544768
    iget-object v1, v0, Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;->c:Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;

    move-object v0, v1

    .line 2544769
    invoke-virtual {v0}, Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;->b()V

    goto :goto_0

    .line 2544770
    :pswitch_6
    sget-object v0, LX/IA9;->a:[I

    iget-object v1, p1, LX/BlT;->c:Lcom/facebook/events/model/Event;

    invoke-virtual {v1}, Lcom/facebook/events/model/Event;->F()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_3

    .line 2544771
    :goto_2
    iget-object v0, p1, LX/BlT;->b:Lcom/facebook/events/model/Event;

    invoke-virtual {v0}, Lcom/facebook/events/model/Event;->F()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2544772
    sget-object v0, LX/IA9;->a:[I

    iget-object v1, p1, LX/BlT;->b:Lcom/facebook/events/model/Event;

    invoke-virtual {v1}, Lcom/facebook/events/model/Event;->F()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_4

    goto/16 :goto_0

    .line 2544773
    :pswitch_7
    iget-object v0, p0, LX/IAA;->a:Lcom/facebook/events/permalink/guestlist/EventGuestListView;

    iget-object v0, v0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->m:Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;

    iget-object v1, p1, LX/BlT;->c:Lcom/facebook/events/model/Event;

    invoke-virtual {v0, v1}, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->a(Lcom/facebook/events/model/Event;)V

    .line 2544774
    iget-object v0, p0, LX/IAA;->a:Lcom/facebook/events/permalink/guestlist/EventGuestListView;

    iget-object v0, v0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->o:Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;

    .line 2544775
    iget-object v1, v0, Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;->a:Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;

    move-object v0, v1

    .line 2544776
    invoke-virtual {v0}, Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;->a()V

    goto/16 :goto_0

    .line 2544777
    :pswitch_8
    iget-object v0, p0, LX/IAA;->a:Lcom/facebook/events/permalink/guestlist/EventGuestListView;

    iget-object v0, v0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->o:Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;

    .line 2544778
    iget-object v1, v0, Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;->a:Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;

    move-object v0, v1

    .line 2544779
    invoke-virtual {v0}, Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;->b()V

    goto :goto_2

    .line 2544780
    :pswitch_9
    iget-object v0, p0, LX/IAA;->a:Lcom/facebook/events/permalink/guestlist/EventGuestListView;

    iget-object v0, v0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->o:Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;

    .line 2544781
    iget-object v1, v0, Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;->b:Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;

    move-object v0, v1

    .line 2544782
    invoke-virtual {v0}, Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;->b()V

    goto :goto_2

    .line 2544783
    :pswitch_a
    iget-object v0, p0, LX/IAA;->a:Lcom/facebook/events/permalink/guestlist/EventGuestListView;

    iget-object v0, v0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->m:Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;

    iget-object v1, p1, LX/BlT;->c:Lcom/facebook/events/model/Event;

    invoke-virtual {v0, v1}, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->a(Lcom/facebook/events/model/Event;)V

    .line 2544784
    iget-object v0, p0, LX/IAA;->a:Lcom/facebook/events/permalink/guestlist/EventGuestListView;

    iget-object v0, v0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->o:Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;

    .line 2544785
    iget-object v1, v0, Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;->b:Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;

    move-object v0, v1

    .line 2544786
    invoke-virtual {v0}, Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;->a()V

    goto/16 :goto_0

    .line 2544787
    :pswitch_b
    iget-object v0, p0, LX/IAA;->a:Lcom/facebook/events/permalink/guestlist/EventGuestListView;

    iget-object v0, v0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->m:Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;

    iget-object v1, p1, LX/BlT;->c:Lcom/facebook/events/model/Event;

    invoke-virtual {v0, v1}, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->a(Lcom/facebook/events/model/Event;)V

    .line 2544788
    iget-object v0, p0, LX/IAA;->a:Lcom/facebook/events/permalink/guestlist/EventGuestListView;

    iget-object v0, v0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->o:Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;

    .line 2544789
    iget-object v1, v0, Lcom/facebook/events/permalink/guestlist/EventGuestListCountsView;->c:Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;

    move-object v0, v1

    .line 2544790
    invoke-virtual {v0}, Lcom/facebook/events/permalink/guestlist/EventGuestListCountView;->a()V

    goto/16 :goto_0

    .line 2544791
    :cond_2
    iget-object v0, p0, LX/IAA;->a:Lcom/facebook/events/permalink/guestlist/EventGuestListView;

    iget-object v0, v0, Lcom/facebook/events/permalink/guestlist/EventGuestListView;->m:Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;

    iget-object v1, p1, LX/BlT;->c:Lcom/facebook/events/model/Event;

    invoke-virtual {v0, v1}, Lcom/facebook/events/permalink/guestlist/EventGuestTileRowView;->a(Lcom/facebook/events/model/Event;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_6
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_1
        :pswitch_4
        :pswitch_5
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_8
        :pswitch_9
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x1
        :pswitch_7
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method


# virtual methods
.method public final synthetic b(LX/0b7;)V
    .locals 0

    .prologue
    .line 2544792
    check-cast p1, LX/BlT;

    invoke-direct {p0, p1}, LX/IAA;->a(LX/BlT;)V

    return-void
.end method
