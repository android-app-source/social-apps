.class public final LX/I6w;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2538278
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_3

    .line 2538279
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2538280
    :goto_0
    return v1

    .line 2538281
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2538282
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_2

    .line 2538283
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 2538284
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2538285
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 2538286
    const-string v3, "node"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2538287
    const/4 v2, 0x0

    .line 2538288
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_9

    .line 2538289
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2538290
    :goto_2
    move v0, v2

    .line 2538291
    goto :goto_1

    .line 2538292
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2538293
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2538294
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    .line 2538295
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2538296
    :cond_5
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_8

    .line 2538297
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2538298
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2538299
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_5

    if-eqz v5, :cond_5

    .line 2538300
    const-string v6, "description"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 2538301
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_3

    .line 2538302
    :cond_6
    const-string v6, "display_name"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 2538303
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_3

    .line 2538304
    :cond_7
    const-string v6, "level"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 2538305
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventNotificationSubscriptionLevel;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    goto :goto_3

    .line 2538306
    :cond_8
    const/4 v5, 0x3

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2538307
    invoke-virtual {p1, v2, v4}, LX/186;->b(II)V

    .line 2538308
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v3}, LX/186;->b(II)V

    .line 2538309
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2538310
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto :goto_2

    :cond_9
    move v0, v2

    move v3, v2

    move v4, v2

    goto :goto_3
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2538311
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2538312
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2538313
    if-eqz v0, :cond_3

    .line 2538314
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2538315
    const/4 p3, 0x2

    .line 2538316
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2538317
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2538318
    if-eqz v1, :cond_0

    .line 2538319
    const-string p1, "description"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2538320
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2538321
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2538322
    if-eqz v1, :cond_1

    .line 2538323
    const-string p1, "display_name"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2538324
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2538325
    :cond_1
    invoke-virtual {p0, v0, p3}, LX/15i;->g(II)I

    move-result v1

    .line 2538326
    if-eqz v1, :cond_2

    .line 2538327
    const-string v1, "level"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2538328
    invoke-virtual {p0, v0, p3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2538329
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2538330
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2538331
    return-void
.end method
