.class public LX/ICG;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ICI;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/ICG",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/ICI;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2549113
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2549114
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/ICG;->b:LX/0Zi;

    .line 2549115
    iput-object p1, p0, LX/ICG;->a:LX/0Ot;

    .line 2549116
    return-void
.end method

.method public static a(LX/0QB;)LX/ICG;
    .locals 4

    .prologue
    .line 2549117
    const-class v1, LX/ICG;

    monitor-enter v1

    .line 2549118
    :try_start_0
    sget-object v0, LX/ICG;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2549119
    sput-object v2, LX/ICG;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2549120
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2549121
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2549122
    new-instance v3, LX/ICG;

    const/16 p0, 0x216c

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/ICG;-><init>(LX/0Ot;)V

    .line 2549123
    move-object v0, v3

    .line 2549124
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2549125
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/ICG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2549126
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2549127
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onSaveClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2549128
    const v0, 0x53d13bd3

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 2549129
    check-cast p2, LX/ICF;

    .line 2549130
    iget-object v0, p0, LX/ICG;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ICI;

    iget-object v2, p2, LX/ICF;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, LX/ICF;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v4, p2, LX/ICF;->c:Ljava/lang/CharSequence;

    iget-object v5, p2, LX/ICF;->d:Ljava/lang/CharSequence;

    move-object v1, p1

    .line 2549131
    iget-object p0, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p0, p0

    .line 2549132
    check-cast p0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {p0}, LX/ICI;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object p0

    .line 2549133
    if-nez p0, :cond_0

    .line 2549134
    const/4 p0, 0x0

    .line 2549135
    :goto_0
    move-object v0, p0

    .line 2549136
    return-object v0

    .line 2549137
    :cond_0
    iget-object p0, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p0, p0

    .line 2549138
    check-cast p0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->m()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object p0

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne p0, p1, :cond_1

    const/4 p0, 0x1

    .line 2549139
    :goto_1
    iget-object p1, v0, LX/ICI;->a:LX/2g9;

    invoke-virtual {p1, v1}, LX/2g9;->c(LX/1De;)LX/2gA;

    move-result-object p2

    if-eqz p0, :cond_2

    const/16 p1, 0x81

    :goto_2
    invoke-virtual {p2, p1}, LX/2gA;->h(I)LX/2gA;

    move-result-object p1

    if-eqz p0, :cond_3

    :goto_3
    invoke-virtual {p1, v4}, LX/2gA;->a(Ljava/lang/CharSequence;)LX/2gA;

    move-result-object p0

    const p1, 0x7f020784

    invoke-virtual {p0, p1}, LX/2gA;->k(I)LX/2gA;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    .line 2549140
    const p1, 0x53d13bd3

    const/4 p2, 0x0

    invoke-static {v1, p1, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p1

    move-object p1, p1

    .line 2549141
    invoke-interface {p0, p1}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object p0

    invoke-interface {p0}, LX/1Di;->k()LX/1Dg;

    move-result-object p0

    goto :goto_0

    .line 2549142
    :cond_1
    const/4 p0, 0x0

    goto :goto_1

    .line 2549143
    :cond_2
    const/16 p1, 0x101

    goto :goto_2

    :cond_3
    move-object v4, v5

    goto :goto_3
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2549144
    invoke-static {}, LX/1dS;->b()V

    .line 2549145
    iget v0, p1, LX/1dQ;->b:I

    .line 2549146
    packed-switch v0, :pswitch_data_0

    .line 2549147
    :goto_0
    return-object v2

    .line 2549148
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2549149
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2549150
    check-cast v1, LX/ICF;

    .line 2549151
    iget-object v3, p0, LX/ICG;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/ICI;

    iget-object v4, v1, LX/ICF;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object p1, v1, LX/ICF;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object p2, v1, LX/ICF;->e:LX/1Po;

    invoke-virtual {v3, v0, v4, p1, p2}, LX/ICI;->onSaveClick(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;)V

    .line 2549152
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x53d13bd3
        :pswitch_0
    .end packed-switch
.end method
