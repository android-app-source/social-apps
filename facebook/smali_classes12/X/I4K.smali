.class public LX/I4K;
.super LX/Cm5;
.source ""

# interfaces
.implements LX/Clr;


# instance fields
.field public final a:Lcom/facebook/events/common/EventAnalyticsParams;

.field public b:Lcom/facebook/events/model/Event;


# direct methods
.method public constructor <init>(LX/I4J;)V
    .locals 1

    .prologue
    .line 2533988
    invoke-direct {p0, p1}, LX/Cm5;-><init>(LX/Cm7;)V

    .line 2533989
    iget-object v0, p1, LX/I4J;->a:LX/7oa;

    invoke-static {v0}, LX/Bm1;->b(LX/7oa;)Lcom/facebook/events/model/Event;

    move-result-object v0

    iput-object v0, p0, LX/I4K;->b:Lcom/facebook/events/model/Event;

    .line 2533990
    iget-object v0, p1, LX/I4J;->b:Lcom/facebook/events/common/EventAnalyticsParams;

    iput-object v0, p0, LX/I4K;->a:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2533991
    return-void
.end method


# virtual methods
.method public final g()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;
    .locals 1

    .prologue
    .line 2533985
    iget-object v0, p0, LX/I4K;->b:Lcom/facebook/events/model/Event;

    .line 2533986
    iget-object p0, v0, Lcom/facebook/events/model/Event;->m:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-object v0, p0

    .line 2533987
    return-object v0
.end method

.method public final h()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;
    .locals 1

    .prologue
    .line 2533992
    iget-object v0, p0, LX/I4K;->b:Lcom/facebook/events/model/Event;

    invoke-virtual {v0}, Lcom/facebook/events/model/Event;->F()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v0

    return-object v0
.end method

.method public final i()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
    .locals 1

    .prologue
    .line 2533982
    iget-object v0, p0, LX/I4K;->b:Lcom/facebook/events/model/Event;

    .line 2533983
    iget-object p0, v0, Lcom/facebook/events/model/Event;->D:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-object v0, p0

    .line 2533984
    return-object v0
.end method

.method public final lx_()I
    .locals 1

    .prologue
    .line 2533981
    const/16 v0, 0x192

    return v0
.end method

.method public final n()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2533978
    iget-object v0, p0, LX/I4K;->b:Lcom/facebook/events/model/Event;

    .line 2533979
    iget-object p0, v0, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v0, p0

    .line 2533980
    return-object v0
.end method
