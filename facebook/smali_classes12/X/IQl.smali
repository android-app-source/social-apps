.class public final LX/IQl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# instance fields
.field public final synthetic a:LX/IQm;


# direct methods
.method public constructor <init>(LX/IQm;)V
    .locals 0

    .prologue
    .line 2575657
    iput-object p1, p0, LX/IQl;->a:LX/IQm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2575658
    iget-object v0, p0, LX/IQl;->a:LX/IQm;

    iget-object v0, v0, LX/IQm;->b:LX/IQn;

    iget-object v0, v0, LX/IQn;->p:LX/IQj;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/IQj;->a$redex0(LX/IQj;Z)V

    .line 2575659
    iget-object v0, p0, LX/IQl;->a:LX/IQm;

    iget-object v0, v0, LX/IQm;->b:LX/IQn;

    invoke-virtual {v0}, LX/1a1;->e()I

    move-result v0

    .line 2575660
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2575661
    iget-object v1, p0, LX/IQl;->a:LX/IQm;

    iget-object v1, v1, LX/IQm;->a:Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesAdapter;

    invoke-virtual {v1, v0}, LX/1OM;->i_(I)V

    .line 2575662
    :cond_0
    iget-object v0, p0, LX/IQl;->a:LX/IQm;

    iget-object v0, v0, LX/IQm;->b:LX/IQn;

    iget-object v0, v0, LX/IQn;->o:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0}, Lcom/facebook/fig/button/FigButton;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f080039

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2575663
    iget-object v0, p0, LX/IQl;->a:LX/IQm;

    iget-object v0, v0, LX/IQm;->a:Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesAdapter;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsAuthoredSaleStoriesAdapter;->e:LX/03V;

    const-string v1, "authored_sale_stories_cross_post_failure"

    const-string v2, "GraphQL mutation for cross-posting failed"

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    .line 2575664
    iput-object p1, v1, LX/0VK;->c:Ljava/lang/Throwable;

    .line 2575665
    move-object v1, v1

    .line 2575666
    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 2575667
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2575668
    return-void
.end method
