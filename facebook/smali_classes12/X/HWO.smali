.class public final LX/HWO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4oV;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;)V
    .locals 0

    .prologue
    .line 2476007
    iput-object p1, p0, LX/HWO;->a:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(III)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2476008
    iget-object v0, p0, LX/HWO;->a:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->F:LX/E8s;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/HWO;->a:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;

    iget-boolean v0, v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->O:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HWO;->a:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;

    iget-boolean v0, v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->P:Z

    if-eqz v0, :cond_1

    .line 2476009
    :cond_0
    iget-object v0, p0, LX/HWO;->a:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;

    iget-object v3, v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->H:Landroid/view/View;

    iget-object v0, p0, LX/HWO;->a:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;

    iget-object v4, v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->t:Lcom/facebook/widget/ScrollingAwareScrollView;

    iget-object v0, p0, LX/HWO;->a:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->F:LX/E8s;

    invoke-virtual {v0}, LX/E8s;->getMeasuredHeight()I

    move-result v0

    if-ge p2, v0, :cond_3

    move v0, v1

    :goto_0
    sget-object v5, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->N:[I

    iget-object v6, p0, LX/HWO;->a:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;

    iget-object v6, v6, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->g:LX/0hB;

    invoke-virtual {v6}, LX/0hB;->d()I

    move-result v6

    invoke-static {v3, v4, v0, v5, v6}, LX/8FX;->a(Landroid/view/View;Landroid/view/ViewGroup;I[II)LX/3rL;

    move-result-object v3

    .line 2476010
    iget-object v4, p0, LX/HWO;->a:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;

    iget-object v0, v3, LX/3rL;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 2476011
    iput-boolean v0, v4, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->O:Z

    .line 2476012
    iget-object v0, p0, LX/HWO;->a:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;

    iget-boolean v0, v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->O:Z

    if-eqz v0, :cond_1

    .line 2476013
    iget-object v4, p0, LX/HWO;->a:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;

    iget-object v0, v3, LX/3rL;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v4, v0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->E_(I)V

    .line 2476014
    iget-object v0, p0, LX/HWO;->a:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;

    .line 2476015
    iput-boolean v1, v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->P:Z

    .line 2476016
    :cond_1
    iget-object v0, p0, LX/HWO;->a:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;

    iget v0, v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->K:I

    if-eq v0, p2, :cond_2

    iget-object v0, p0, LX/HWO;->a:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->I:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/HWO;->a:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->F:LX/E8s;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/HWO;->a:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2476017
    iget-object v0, p0, LX/HWO;->a:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->F:LX/E8s;

    invoke-virtual {v0}, LX/E8s;->getMeasuredHeight()I

    move-result v0

    if-ge p2, v0, :cond_4

    .line 2476018
    iget-object v0, p0, LX/HWO;->a:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->I:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v2, p0, LX/HWO;->a:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;

    iget-object v2, v2, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->t:Lcom/facebook/widget/ScrollingAwareScrollView;

    invoke-virtual {v0, v2, v1}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->a(Landroid/view/ViewGroup;I)V

    .line 2476019
    :goto_1
    iget-object v0, p0, LX/HWO;->a:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;

    .line 2476020
    iput p2, v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->K:I

    .line 2476021
    :cond_2
    return-void

    :cond_3
    move v0, v2

    .line 2476022
    goto :goto_0

    .line 2476023
    :cond_4
    iget-object v0, p0, LX/HWO;->a:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->I:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v1, p0, LX/HWO;->a:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;

    iget-object v1, v1, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->t:Lcom/facebook/widget/ScrollingAwareScrollView;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->a(Landroid/view/ViewGroup;I)V

    goto :goto_1
.end method
