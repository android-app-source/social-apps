.class public final LX/HFI;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/auth/viewercontext/ViewerContext;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

.field public final synthetic b:Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;

.field public final synthetic c:Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;)V
    .locals 0

    .prologue
    .line 2443325
    iput-object p1, p0, LX/HFI;->c:Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;

    iput-object p2, p0, LX/HFI;->a:Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

    iput-object p3, p0, LX/HFI;->b:Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2443326
    iget-object v0, p0, LX/HFI;->c:Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->g:LX/03V;

    sget-object v1, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->k:Ljava/lang/String;

    const-string v2, "fetch vc failed"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2443327
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2443316
    check-cast p1, Lcom/facebook/auth/viewercontext/ViewerContext;

    const/4 v3, 0x0

    .line 2443317
    if-eqz p1, :cond_0

    .line 2443318
    iget-object v0, p0, LX/HFI;->c:Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BQX;

    iget-object v1, p0, LX/HFI;->a:Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

    .line 2443319
    iget-object v2, v1, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->b:Landroid/net/Uri;

    move-object v1, v2

    .line 2443320
    iget-object v2, p0, LX/HFI;->a:Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

    .line 2443321
    iget-object v4, v2, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->d:Ljava/lang/String;

    move-object v2, v4

    .line 2443322
    invoke-virtual {v0, v1, v2, v3}, LX/BQX;->a(Landroid/net/Uri;Ljava/lang/String;Z)V

    .line 2443323
    iget-object v0, p0, LX/HFI;->c:Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;

    iget-object v1, p0, LX/HFI;->b:Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;

    invoke-virtual {v0, v1, p1, v3}, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;->a(Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;Lcom/facebook/auth/viewercontext/ViewerContext;Z)V

    .line 2443324
    :cond_0
    return-void
.end method
