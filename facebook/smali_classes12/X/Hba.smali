.class public final enum LX/Hba;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Hba;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Hba;

.field public static final enum CONCLUSION:LX/Hba;

.field public static final enum INTRO:LX/Hba;

.field public static final enum LOGIN_ALERTS:LX/Hba;

.field public static final enum PASSWORD:LX/Hba;

.field public static final enum UNUSED_SESSIONS:LX/Hba;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2485915
    new-instance v0, LX/Hba;

    const-string v1, "INTRO"

    invoke-direct {v0, v1, v2}, LX/Hba;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hba;->INTRO:LX/Hba;

    .line 2485916
    new-instance v0, LX/Hba;

    const-string v1, "UNUSED_SESSIONS"

    invoke-direct {v0, v1, v3}, LX/Hba;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hba;->UNUSED_SESSIONS:LX/Hba;

    .line 2485917
    new-instance v0, LX/Hba;

    const-string v1, "LOGIN_ALERTS"

    invoke-direct {v0, v1, v4}, LX/Hba;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hba;->LOGIN_ALERTS:LX/Hba;

    .line 2485918
    new-instance v0, LX/Hba;

    const-string v1, "PASSWORD"

    invoke-direct {v0, v1, v5}, LX/Hba;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hba;->PASSWORD:LX/Hba;

    .line 2485919
    new-instance v0, LX/Hba;

    const-string v1, "CONCLUSION"

    invoke-direct {v0, v1, v6}, LX/Hba;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hba;->CONCLUSION:LX/Hba;

    .line 2485920
    const/4 v0, 0x5

    new-array v0, v0, [LX/Hba;

    sget-object v1, LX/Hba;->INTRO:LX/Hba;

    aput-object v1, v0, v2

    sget-object v1, LX/Hba;->UNUSED_SESSIONS:LX/Hba;

    aput-object v1, v0, v3

    sget-object v1, LX/Hba;->LOGIN_ALERTS:LX/Hba;

    aput-object v1, v0, v4

    sget-object v1, LX/Hba;->PASSWORD:LX/Hba;

    aput-object v1, v0, v5

    sget-object v1, LX/Hba;->CONCLUSION:LX/Hba;

    aput-object v1, v0, v6

    sput-object v0, LX/Hba;->$VALUES:[LX/Hba;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2485914
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Hba;
    .locals 1

    .prologue
    .line 2485913
    const-class v0, LX/Hba;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Hba;

    return-object v0
.end method

.method public static values()[LX/Hba;
    .locals 1

    .prologue
    .line 2485912
    sget-object v0, LX/Hba;->$VALUES:[LX/Hba;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Hba;

    return-object v0
.end method
