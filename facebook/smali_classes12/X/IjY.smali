.class public final LX/IjY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/IjZ;


# direct methods
.method public constructor <init>(LX/IjZ;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2605892
    iput-object p1, p0, LX/IjY;->c:LX/IjZ;

    iput-object p2, p0, LX/IjY;->a:Ljava/lang/String;

    iput-object p3, p0, LX/IjY;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x184dfbf

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2605893
    iget-object v1, p0, LX/IjY;->a:Ljava/lang/String;

    iget-object v2, p0, LX/IjY;->b:Ljava/lang/String;

    .line 2605894
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2605895
    const-string p1, "sender_name"

    invoke-virtual {v4, p1, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2605896
    const-string p1, "transaction_id"

    invoke-virtual {v4, p1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2605897
    new-instance p1, Lcom/facebook/messaging/payment/prefs/receipts/nux/DeclinePaymentDialogFragment;

    invoke-direct {p1}, Lcom/facebook/messaging/payment/prefs/receipts/nux/DeclinePaymentDialogFragment;-><init>()V

    .line 2605898
    invoke-virtual {p1, v4}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2605899
    move-object v1, p1

    .line 2605900
    new-instance v2, LX/IjX;

    invoke-direct {v2, p0}, LX/IjX;-><init>(LX/IjY;)V

    .line 2605901
    iput-object v2, v1, Lcom/facebook/messaging/payment/prefs/receipts/nux/DeclinePaymentDialogFragment;->r:LX/IjX;

    .line 2605902
    iget-object v2, p0, LX/IjY;->c:LX/IjZ;

    .line 2605903
    iget-object v4, v2, LX/73V;->a:LX/6qh;

    invoke-virtual {v4, v1}, LX/6qh;->a(Lcom/facebook/ui/dialogs/FbDialogFragment;)V

    .line 2605904
    const v1, -0x692af2e0

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
