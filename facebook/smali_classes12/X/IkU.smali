.class public final LX/IkU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "LX/FGc;",
        "Lcom/facebook/payments/invoice/protocol/graphql/InvoiceMutationsModels$PaymentInvoiceMutationModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/IkW;


# direct methods
.method public constructor <init>(LX/IkW;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2606769
    iput-object p1, p0, LX/IkU;->c:LX/IkW;

    iput-object p2, p0, LX/IkU;->a:Ljava/lang/String;

    iput-object p3, p0, LX/IkU;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2606765
    check-cast p1, LX/FGc;

    .line 2606766
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/FGc;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2606767
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "mediaUploadFailure: empty result or result.fbid"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2606768
    :cond_1
    iget-object v0, p0, LX/IkU;->c:LX/IkW;

    iget-object v0, v0, LX/IkW;->b:LX/Iyi;

    iget-object v1, p0, LX/IkU;->a:Ljava/lang/String;

    sget-object v2, LX/6xh;->PAGES_COMMERCE:LX/6xh;

    const-string v3, "ATTACH_NEW_RECEIPT"

    new-instance v4, LX/4I7;

    invoke-direct {v4}, LX/4I7;-><init>()V

    const-string v5, "receipt_image_id"

    invoke-virtual {v4, v5}, LX/4I7;->a(Ljava/lang/String;)LX/4I7;

    move-result-object v4

    invoke-virtual {p1}, LX/FGc;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/4I7;->b(Ljava/lang/String;)LX/4I7;

    move-result-object v4

    new-instance v5, LX/4I7;

    invoke-direct {v5}, LX/4I7;-><init>()V

    const-string v6, "payment_option_id"

    invoke-virtual {v5, v6}, LX/4I7;->a(Ljava/lang/String;)LX/4I7;

    move-result-object v5

    iget-object v6, p0, LX/IkU;->b:Ljava/lang/String;

    invoke-virtual {v5, v6}, LX/4I7;->b(Ljava/lang/String;)LX/4I7;

    move-result-object v5

    invoke-static {v4, v5}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/Iyi;->a(Ljava/lang/String;LX/6xh;Ljava/lang/String;LX/0Px;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
