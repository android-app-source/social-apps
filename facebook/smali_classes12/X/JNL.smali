.class public LX/JNL;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionItemComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JNL",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionItemComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2685737
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2685738
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JNL;->b:LX/0Zi;

    .line 2685739
    iput-object p1, p0, LX/JNL;->a:LX/0Ot;

    .line 2685740
    return-void
.end method

.method public static a(LX/0QB;)LX/JNL;
    .locals 4

    .prologue
    .line 2685726
    const-class v1, LX/JNL;

    monitor-enter v1

    .line 2685727
    :try_start_0
    sget-object v0, LX/JNL;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2685728
    sput-object v2, LX/JNL;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2685729
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2685730
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2685731
    new-instance v3, LX/JNL;

    const/16 p0, 0x1ee5

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JNL;-><init>(LX/0Ot;)V

    .line 2685732
    move-object v0, v3

    .line 2685733
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2685734
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JNL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2685735
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2685736
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2685741
    const v0, -0x73906b2d

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 7

    .prologue
    .line 2685666
    check-cast p2, LX/JNK;

    .line 2685667
    iget-object v0, p0, LX/JNL;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionItemComponentSpec;

    iget-object v1, p2, LX/JNK;->a:Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;

    iget-object v2, p2, LX/JNK;->b:Ljava/lang/String;

    iget-object v3, p2, LX/JNK;->c:Ljava/lang/String;

    .line 2685668
    iget-object v4, v0, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionItemComponentSpec;->c:LX/1DR;

    iget-object v5, v0, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionItemComponentSpec;->b:Landroid/content/Context;

    invoke-virtual {v4, v5}, LX/1DR;->a(Landroid/content/Context;)I

    move-result v4

    .line 2685669
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x0

    invoke-interface {v5, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v4}, LX/1Dh;->H(I)LX/1Dh;

    move-result-object v5

    const v6, 0x7f020a3c

    invoke-interface {v5, v6}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v5

    .line 2685670
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    const/4 p0, 0x2

    invoke-interface {v6, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v6

    const/4 p0, 0x1

    invoke-interface {v6, p0}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v6

    move-object v6, v6

    .line 2685671
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object p0

    if-eqz p0, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->Q()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object p0

    if-eqz p0, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->Q()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object p0

    if-eqz p0, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->Q()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p0

    if-nez p0, :cond_5

    .line 2685672
    :cond_0
    const/4 p0, 0x0

    .line 2685673
    :goto_0
    move-object v4, p0

    .line 2685674
    invoke-interface {v6, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-interface {v5, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    iget-object v5, v0, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionItemComponentSpec;->e:LX/JNR;

    const/4 v6, 0x0

    .line 2685675
    new-instance p0, LX/JNQ;

    invoke-direct {p0, v5}, LX/JNQ;-><init>(LX/JNR;)V

    .line 2685676
    iget-object p2, v5, LX/JNR;->b:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/JNP;

    .line 2685677
    if-nez p2, :cond_1

    .line 2685678
    new-instance p2, LX/JNP;

    invoke-direct {p2, v5}, LX/JNP;-><init>(LX/JNR;)V

    .line 2685679
    :cond_1
    invoke-static {p2, p1, v6, v6, p0}, LX/JNP;->a$redex0(LX/JNP;LX/1De;IILX/JNQ;)V

    .line 2685680
    move-object p0, p2

    .line 2685681
    move-object v6, p0

    .line 2685682
    move-object v5, v6

    .line 2685683
    iget-object v6, v5, LX/JNP;->a:LX/JNQ;

    iput-object v1, v6, LX/JNQ;->a:Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;

    .line 2685684
    iget-object v6, v5, LX/JNP;->e:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v6, p0}, Ljava/util/BitSet;->set(I)V

    .line 2685685
    move-object v5, v5

    .line 2685686
    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    .line 2685687
    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v5

    const v6, 0x7f0a011a

    invoke-virtual {v5, v6}, LX/25Q;->i(I)LX/25Q;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const v6, 0x7f0b0033

    invoke-interface {v5, v6}, LX/1Di;->q(I)LX/1Di;

    move-result-object v5

    const/4 v6, 0x6

    const/4 p0, 0x2

    invoke-interface {v5, v6, p0}, LX/1Di;->e(II)LX/1Di;

    move-result-object v5

    const/4 v6, 0x4

    invoke-interface {v5, v6}, LX/1Di;->b(I)LX/1Di;

    move-result-object v5

    move-object v5, v5

    .line 2685688
    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v5

    .line 2685689
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string p0, "UPCOMING_EVENTS"

    aput-object p0, v4, v6

    const/4 v6, 0x1

    const-string p0, "FRIENDS_EVENTS"

    aput-object p0, v4, v6

    invoke-static {v3, v4}, LX/0YN;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v4

    move v4, v4

    .line 2685690
    if-eqz v4, :cond_4

    iget-object v4, v0, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionItemComponentSpec;->f:LX/JNO;

    const/4 v6, 0x0

    .line 2685691
    new-instance p0, LX/JNN;

    invoke-direct {p0, v4}, LX/JNN;-><init>(LX/JNO;)V

    .line 2685692
    sget-object p2, LX/JNO;->a:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/JNM;

    .line 2685693
    if-nez p2, :cond_2

    .line 2685694
    new-instance p2, LX/JNM;

    invoke-direct {p2}, LX/JNM;-><init>()V

    .line 2685695
    :cond_2
    invoke-static {p2, p1, v6, v6, p0}, LX/JNM;->a$redex0(LX/JNM;LX/1De;IILX/JNN;)V

    .line 2685696
    move-object p0, p2

    .line 2685697
    move-object v6, p0

    .line 2685698
    move-object v4, v6

    .line 2685699
    iget-object v6, v4, LX/JNM;->a:LX/JNN;

    iput-object v1, v6, LX/JNN;->a:Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;

    .line 2685700
    iget-object v6, v4, LX/JNM;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v6, p0}, Ljava/util/BitSet;->set(I)V

    .line 2685701
    move-object v4, v4

    .line 2685702
    :goto_1
    invoke-interface {v5, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    iget-object v5, v0, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionItemComponentSpec;->g:LX/JNG;

    const/4 v6, 0x0

    .line 2685703
    new-instance p0, LX/JNF;

    invoke-direct {p0, v5}, LX/JNF;-><init>(LX/JNG;)V

    .line 2685704
    iget-object p2, v5, LX/JNG;->b:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/JNE;

    .line 2685705
    if-nez p2, :cond_3

    .line 2685706
    new-instance p2, LX/JNE;

    invoke-direct {p2, v5}, LX/JNE;-><init>(LX/JNG;)V

    .line 2685707
    :cond_3
    invoke-static {p2, p1, v6, v6, p0}, LX/JNE;->a$redex0(LX/JNE;LX/1De;IILX/JNF;)V

    .line 2685708
    move-object p0, p2

    .line 2685709
    move-object v6, p0

    .line 2685710
    move-object v5, v6

    .line 2685711
    iget-object v6, v5, LX/JNE;->a:LX/JNF;

    iput-object v1, v6, LX/JNF;->a:Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;

    .line 2685712
    iget-object v6, v5, LX/JNE;->e:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v6, p0}, Ljava/util/BitSet;->set(I)V

    .line 2685713
    move-object v5, v5

    .line 2685714
    iget-object v6, v5, LX/JNE;->a:LX/JNF;

    iput-object v2, v6, LX/JNF;->b:Ljava/lang/String;

    .line 2685715
    iget-object v6, v5, LX/JNE;->e:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v6, p0}, Ljava/util/BitSet;->set(I)V

    .line 2685716
    move-object v5, v5

    .line 2685717
    iget-object v6, v5, LX/JNE;->a:LX/JNF;

    iput-object v3, v6, LX/JNF;->c:Ljava/lang/String;

    .line 2685718
    iget-object v6, v5, LX/JNE;->e:Ljava/util/BitSet;

    const/4 p0, 0x2

    invoke-virtual {v6, p0}, Ljava/util/BitSet;->set(I)V

    .line 2685719
    move-object v5, v5

    .line 2685720
    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    .line 2685721
    const v5, -0x73906b2d

    const/4 v6, 0x0

    invoke-static {p1, v5, v6}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v5, v5

    .line 2685722
    invoke-interface {v4, v5}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 2685723
    return-object v0

    :cond_4
    const/4 v4, 0x0

    goto :goto_1

    .line 2685724
    :cond_5
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEvent;->Q()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p0

    .line 2685725
    iget-object p2, v0, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionItemComponentSpec;->d:LX/1nu;

    invoke-virtual {p2, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object p2

    invoke-static {p0}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object p0

    invoke-virtual {p2, p0}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object p0

    sget-object p2, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionItemComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p0, p2}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object p0

    const p2, 0x7f0a045d

    invoke-virtual {p0, p2}, LX/1nw;->h(I)LX/1nw;

    move-result-object p0

    const p2, 0x3ff745d1

    invoke-virtual {p0, p2}, LX/1nw;->c(F)LX/1nw;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    add-int/lit8 p2, v4, -0x4

    invoke-interface {p0, p2}, LX/1Di;->j(I)LX/1Di;

    move-result-object p0

    goto/16 :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2685640
    invoke-static {}, LX/1dS;->b()V

    .line 2685641
    iget v0, p1, LX/1dQ;->b:I

    .line 2685642
    packed-switch v0, :pswitch_data_0

    .line 2685643
    :goto_0
    return-object v2

    .line 2685644
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2685645
    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2685646
    check-cast v1, LX/JNK;

    .line 2685647
    iget-object v3, p0, LX/JNL;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionItemComponentSpec;

    iget-object v4, v1, LX/JNK;->a:Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;

    iget-object v5, v1, LX/JNK;->c:Ljava/lang/String;

    .line 2685648
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v6

    .line 2685649
    iget-object p1, v3, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionItemComponentSpec;->h:LX/Blh;

    iget-object p2, v3, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionItemComponentSpec;->b:Landroid/content/Context;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLEvent;->ax()Ljava/lang/String;

    move-result-object v6

    iget-object p0, v3, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionItemComponentSpec;->i:LX/0kx;

    const-string v1, "native_newsfeed"

    invoke-virtual {p0, v1}, LX/0kx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 2685650
    iget-object v1, v3, Lcom/facebook/feedplugins/eventssuggestion/rows/components/EventsSuggestionItemComponentSpec;->i:LX/0kx;

    invoke-virtual {v1}, LX/0kx;->b()Ljava/lang/String;

    move-result-object v1

    const-string v0, "event_dashboard"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2685651
    const-string v1, "home_tab_"

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v5, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2685652
    :goto_1
    move-object v1, v1

    .line 2685653
    invoke-static {v6}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2685654
    iget-object v3, p1, LX/Blh;->d:LX/0id;

    sget-object v0, LX/Blh;->a:Ljava/lang/String;

    invoke-virtual {v3, p2, v0}, LX/0id;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 2685655
    invoke-static {v6}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2685656
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2685657
    const-string v3, "event_id"

    invoke-virtual {v0, v3, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2685658
    const-string v3, "extra_ref_module"

    invoke-virtual {v0, v3, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2685659
    const-string v3, "event_ref_mechanism"

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2685660
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    iget-object v3, p1, LX/Blh;->b:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/ComponentName;

    invoke-virtual {v4, v3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v3

    .line 2685661
    invoke-virtual {v3, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2685662
    const-string v0, "target_fragment"

    sget-object v4, LX/0cQ;->EVENTS_PERMALINK_FRAGMENT:LX/0cQ;

    invoke-virtual {v4}, LX/0cQ;->ordinal()I

    move-result v4

    invoke-virtual {v3, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2685663
    move-object v3, v3

    .line 2685664
    iget-object v0, p1, LX/Blh;->c:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, v3, p2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2685665
    goto/16 :goto_0

    :cond_0
    sget-object v1, Lcom/facebook/events/common/ActionMechanism;->EVENTS_SUGGESTION_UNIT:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v1}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :pswitch_data_0
    .packed-switch -0x73906b2d
        :pswitch_0
    .end packed-switch
.end method
