.class public LX/Hze;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0SG;

.field public final b:Landroid/content/ContentResolver;

.field public final c:LX/Bky;

.field public final d:LX/HyO;

.field public final e:LX/0tX;

.field public final f:LX/0TD;

.field public final g:LX/0hB;

.field public final h:LX/1Ck;

.field public final i:I

.field public j:Ljava/lang/String;

.field public k:Z

.field public l:Z

.field public m:Ljava/lang/String;

.field public n:Z

.field public o:Z

.field public p:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0SG;Landroid/content/ContentResolver;LX/Bky;LX/HyO;LX/0tX;LX/0TD;LX/0hB;LX/1Ck;)V
    .locals 2
    .param p6    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2525837
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2525838
    iput-boolean v0, p0, LX/Hze;->k:Z

    .line 2525839
    iput-boolean v1, p0, LX/Hze;->l:Z

    .line 2525840
    iput-boolean v0, p0, LX/Hze;->n:Z

    .line 2525841
    iput-boolean v1, p0, LX/Hze;->o:Z

    .line 2525842
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2525843
    iput-object v0, p0, LX/Hze;->p:LX/0Px;

    .line 2525844
    iput-object p1, p0, LX/Hze;->a:LX/0SG;

    .line 2525845
    iput-object p2, p0, LX/Hze;->b:Landroid/content/ContentResolver;

    .line 2525846
    iput-object p3, p0, LX/Hze;->c:LX/Bky;

    .line 2525847
    iput-object p4, p0, LX/Hze;->d:LX/HyO;

    .line 2525848
    iput-object p5, p0, LX/Hze;->e:LX/0tX;

    .line 2525849
    iput-object p6, p0, LX/Hze;->f:LX/0TD;

    .line 2525850
    iput-object p7, p0, LX/Hze;->g:LX/0hB;

    .line 2525851
    iput-object p8, p0, LX/Hze;->h:LX/1Ck;

    .line 2525852
    const/16 v0, 0xc

    iput v0, p0, LX/Hze;->i:I

    .line 2525853
    return-void
.end method

.method public static a(LX/Hze;ILX/Hxg;J)LX/0Vd;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LX/Hxg;",
            "J)",
            "LX/0Vd",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchAllUpcomingEventsQueryModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2525836
    new-instance v1, LX/HzZ;

    move-object v2, p0

    move-object v3, p2

    move-wide v4, p3

    move v6, p1

    invoke-direct/range {v1 .. v6}, LX/HzZ;-><init>(LX/Hze;LX/Hxg;JI)V

    return-object v1
.end method

.method public static a$redex0(LX/Hze;I)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchAllUpcomingEventsQueryModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2525878
    new-instance v0, LX/7oH;

    invoke-direct {v0}, LX/7oH;-><init>()V

    move-object v0, v0

    .line 2525879
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    new-instance v1, LX/7oH;

    invoke-direct {v1}, LX/7oH;-><init>()V

    const-string v2, "profile_image_size"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "cover_image_portrait_size"

    iget-object v3, p0, LX/Hze;->g:LX/0hB;

    invoke-virtual {v3}, LX/0hB;->f()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "cover_image_landscape_size"

    iget-object v3, p0, LX/Hze;->g:LX/0hB;

    invoke-virtual {v3}, LX/0hB;->g()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "after_cursor"

    iget-object v3, p0, LX/Hze;->j:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "first_count"

    iget v3, p0, LX/Hze;->i:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    .line 2525880
    iget-object v2, v1, LX/0gW;->e:LX/0w7;

    move-object v1, v2

    .line 2525881
    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0w7;)LX/0zO;

    move-result-object v0

    .line 2525882
    iget-object v1, p0, LX/Hze;->e:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0
.end method

.method public static a$redex0(LX/Hze;LX/0Px;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2525883
    iget-object v0, p0, LX/Hze;->f:LX/0TD;

    new-instance v1, Lcom/facebook/events/dashboard/EventsPager$7;

    invoke-direct {v1, p0, p1}, Lcom/facebook/events/dashboard/EventsPager$7;-><init>(LX/Hze;LX/0Px;)V

    const v2, 0x1e2e1af6

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2525884
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2525867
    iget-object v0, p0, LX/Hze;->h:LX/1Ck;

    sget-object v1, LX/Hzc;->FETCH_UPCOMING_EVENTS:LX/Hzc;

    invoke-virtual {v0, v1}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2525868
    iget-object v0, p0, LX/Hze;->h:LX/1Ck;

    sget-object v1, LX/Hzc;->FETCH_PAST_EVENTS:LX/Hzc;

    invoke-virtual {v0, v1}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2525869
    iput-object v4, p0, LX/Hze;->j:Ljava/lang/String;

    .line 2525870
    iput-boolean v2, p0, LX/Hze;->k:Z

    .line 2525871
    iput-boolean v3, p0, LX/Hze;->l:Z

    .line 2525872
    iput-object v4, p0, LX/Hze;->m:Ljava/lang/String;

    .line 2525873
    iput-boolean v2, p0, LX/Hze;->n:Z

    .line 2525874
    iput-boolean v3, p0, LX/Hze;->o:Z

    .line 2525875
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2525876
    iput-object v0, p0, LX/Hze;->p:LX/0Px;

    .line 2525877
    return-void
.end method

.method public final b(ILX/Hxg;)V
    .locals 5
    .param p2    # LX/Hxg;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 2525854
    iget-object v0, p0, LX/Hze;->h:LX/1Ck;

    sget-object v1, LX/Hzc;->FETCH_UPCOMING_EVENTS:LX/Hzc;

    invoke-virtual {v0, v1}, LX/1Ck;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2525855
    :cond_0
    :goto_0
    return-void

    .line 2525856
    :cond_1
    iget-object v0, p0, LX/Hze;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 2525857
    iget-boolean v2, p0, LX/Hze;->k:Z

    if-nez v2, :cond_3

    .line 2525858
    iput-boolean v3, p0, LX/Hze;->k:Z

    .line 2525859
    iput-object v4, p0, LX/Hze;->j:Ljava/lang/String;

    .line 2525860
    iput-boolean v3, p0, LX/Hze;->l:Z

    .line 2525861
    :cond_2
    new-instance v2, LX/HzY;

    invoke-direct {v2, p0, p1}, LX/HzY;-><init>(LX/Hze;I)V

    .line 2525862
    invoke-static {p0, p1, p2, v0, v1}, LX/Hze;->a(LX/Hze;ILX/Hxg;J)LX/0Vd;

    move-result-object v0

    .line 2525863
    iget-object v1, p0, LX/Hze;->h:LX/1Ck;

    sget-object v3, LX/Hzc;->FETCH_UPCOMING_EVENTS:LX/Hzc;

    invoke-virtual {v1, v3, v2, v0}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_0

    .line 2525864
    :cond_3
    iget-boolean v2, p0, LX/Hze;->l:Z

    if-nez v2, :cond_2

    .line 2525865
    if-eqz p2, :cond_0

    .line 2525866
    iget-boolean v0, p0, LX/Hze;->l:Z

    const/4 v1, 0x0

    invoke-interface {p2, v0, v1, v4}, LX/Hxg;->a(ZILjava/lang/Long;)V

    goto :goto_0
.end method
