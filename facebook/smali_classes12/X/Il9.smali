.class public LX/Il9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Ikp;


# instance fields
.field private final a:Landroid/view/ViewGroup;

.field private final b:Lcom/facebook/widget/text/BetterButton;

.field private final c:Lcom/facebook/widget/text/BetterTextView;

.field private final d:Lcom/facebook/widget/text/BetterTextView;

.field public e:LX/Iku;


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 2
    .param p2    # Landroid/view/ViewGroup;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2607511
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2607512
    const v0, 0x7f03119f

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LX/Il9;->a:Landroid/view/ViewGroup;

    .line 2607513
    iget-object v0, p0, LX/Il9;->a:Landroid/view/ViewGroup;

    const v1, 0x7f0d0525

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterButton;

    iput-object v0, p0, LX/Il9;->b:Lcom/facebook/widget/text/BetterButton;

    .line 2607514
    iget-object v0, p0, LX/Il9;->a:Landroid/view/ViewGroup;

    const v1, 0x7f0d0526

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Il9;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 2607515
    iget-object v0, p0, LX/Il9;->a:Landroid/view/ViewGroup;

    const v1, 0x7f0d295f

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Il9;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 2607516
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2607517
    new-instance v0, LX/Il7;

    invoke-direct {v0, p0, p1}, LX/Il7;-><init>(LX/Il9;Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;)V

    return-object v0
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 1

    .prologue
    .line 2607518
    iget-object v0, p0, LX/Il9;->a:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public final a(LX/Il0;)V
    .locals 5

    .prologue
    const/16 v2, 0x8

    const/4 v4, 0x0

    .line 2607519
    iget-object v0, p1, LX/Il0;->n:LX/0am;

    move-object v0, v0

    .line 2607520
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2607521
    sget-object v1, LX/Il8;->a:[I

    .line 2607522
    iget-object v0, p1, LX/Il0;->n:LX/0am;

    move-object v0, v0

    .line 2607523
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2607524
    iget-object v0, p0, LX/Il9;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2607525
    const-string v0, "ActionButtonBindable"

    const-string v1, "We should not see this transfer status %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    .line 2607526
    iget-object v3, p1, LX/Il0;->n:LX/0am;

    move-object v3, v3

    .line 2607527
    invoke-virtual {v3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2607528
    :goto_0
    return-void

    .line 2607529
    :pswitch_0
    iget-object v0, p0, LX/Il9;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2607530
    iget-object v0, p0, LX/Il9;->b:Lcom/facebook/widget/text/BetterButton;

    invoke-virtual {v0, v4}, Lcom/facebook/widget/text/BetterButton;->setVisibility(I)V

    .line 2607531
    iget-object v0, p0, LX/Il9;->b:Lcom/facebook/widget/text/BetterButton;

    const v1, 0x7f082d48

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterButton;->setText(I)V

    .line 2607532
    iget-object v0, p0, LX/Il9;->b:Lcom/facebook/widget/text/BetterButton;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;->VOIDED:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    invoke-direct {p0, v1}, LX/Il9;->a(Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2607533
    iget-object v0, p0, LX/Il9;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2607534
    iget-object v0, p0, LX/Il9;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_0

    .line 2607535
    :pswitch_1
    iget-object v0, p0, LX/Il9;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2607536
    iget-object v0, p0, LX/Il9;->b:Lcom/facebook/widget/text/BetterButton;

    invoke-virtual {v0, v4}, Lcom/facebook/widget/text/BetterButton;->setVisibility(I)V

    .line 2607537
    iget-object v0, p0, LX/Il9;->b:Lcom/facebook/widget/text/BetterButton;

    const v1, 0x7f082d49

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterButton;->setText(I)V

    .line 2607538
    iget-object v0, p0, LX/Il9;->b:Lcom/facebook/widget/text/BetterButton;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;->PAYMENT_SUBMITTED:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    invoke-direct {p0, v1}, LX/Il9;->a(Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2607539
    iget-object v0, p0, LX/Il9;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2607540
    iget-object v0, p0, LX/Il9;->c:Lcom/facebook/widget/text/BetterTextView;

    const v1, 0x7f082d4a

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 2607541
    iget-object v0, p0, LX/Il9;->c:Lcom/facebook/widget/text/BetterTextView;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;->AWAITING_PAYMENT_METHOD:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    invoke-direct {p0, v1}, LX/Il9;->a(Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2607542
    iget-object v0, p0, LX/Il9;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2607543
    iget-object v0, p0, LX/Il9;->d:Lcom/facebook/widget/text/BetterTextView;

    const v1, 0x7f082d48

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 2607544
    iget-object v0, p0, LX/Il9;->d:Lcom/facebook/widget/text/BetterTextView;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;->VOIDED:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    invoke-direct {p0, v1}, LX/Il9;->a(Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
