.class public final LX/HV3;
.super LX/1Cv;
.source ""


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLInterfaces$VideoDetailFragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLInterfaces$VideoDetailFragment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2474660
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 2474661
    iput-object p1, p0, LX/HV3;->a:Ljava/util/List;

    .line 2474662
    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2474663
    new-instance v0, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 6

    .prologue
    .line 2474664
    check-cast p3, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;

    check-cast p2, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

    .line 2474665
    iput-object p2, p3, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;->l:Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

    .line 2474666
    iget-object v0, p3, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;->i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const v1, 0x3fe38e39

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 2474667
    new-instance v0, LX/1Uo;

    invoke-virtual {p3}, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    sget-object v1, LX/1Up;->g:LX/1Up;

    invoke-virtual {v0, v1}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v0

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 2474668
    iget-object v1, p3, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;->i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2474669
    invoke-virtual {p2}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->L()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->L()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 2474670
    :goto_0
    iget-object v0, p3, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ad;

    sget-object v2, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 2474671
    iget-object v1, p3, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;->i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2474672
    new-instance v0, LX/HV5;

    invoke-direct {v0, p3, p2}, LX/HV5;-><init>(Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;)V

    invoke-virtual {p3, v0}, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2474673
    invoke-virtual {p2}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->K()LX/174;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;->a(LX/174;)Ljava/lang/String;

    move-result-object v0

    .line 2474674
    invoke-virtual {p2}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->v()LX/174;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;->a(LX/174;)Ljava/lang/String;

    move-result-object v1

    .line 2474675
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 2474676
    iget-object v2, p3, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;->j:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2474677
    :goto_1
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2474678
    iget-object v0, p3, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;->k:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2474679
    :cond_0
    if-eqz p2, :cond_1

    .line 2474680
    invoke-virtual {p2}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->x()I

    move-result v0

    .line 2474681
    iget-object v1, p3, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;->g:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p3}, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f00b1

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 2474682
    const/16 p0, 0x3e8

    if-ge v0, p0, :cond_4

    .line 2474683
    const-string p0, "%,d"

    const/4 p1, 0x1

    new-array p1, p1, [Ljava/lang/Object;

    const/4 p4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p5

    aput-object p5, p1, p4

    invoke-static {p0, p1}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    .line 2474684
    :goto_2
    move-object p0, p0

    .line 2474685
    aput-object p0, v4, v5

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2474686
    iget-object v0, p3, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;->h:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p2}, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;->y()I

    move-result v1

    .line 2474687
    div-int/lit16 v2, v1, 0x3e8

    .line 2474688
    div-int/lit8 v3, v2, 0x3c

    .line 2474689
    rem-int/lit8 v2, v2, 0x3c

    .line 2474690
    const-string v4, "%d:%02d"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v4, v3, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 2474691
    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2474692
    :cond_1
    return-void

    .line 2474693
    :cond_2
    const/4 v0, 0x0

    move-object v1, v0

    goto/16 :goto_0

    .line 2474694
    :cond_3
    iget-object v0, p3, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;->j:Lcom/facebook/widget/text/BetterTextView;

    iget-object v2, p3, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;->f:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_4
    iget-object p0, p3, Lcom/facebook/pages/fb4a/videohub/ui/PageVideoListItem;->b:LX/154;

    invoke-virtual {p0, v0}, LX/154;->a(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_2
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2474695
    iget-object v0, p0, LX/HV3;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2474696
    iget-object v0, p0, LX/HV3;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2474697
    int-to-long v0, p1

    return-wide v0
.end method
