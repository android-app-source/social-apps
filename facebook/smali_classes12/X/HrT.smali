.class public final LX/HrT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Ar6;


# instance fields
.field public final synthetic a:Lcom/facebook/composer/activity/ComposerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/activity/ComposerFragment;)V
    .locals 0

    .prologue
    .line 2511066
    iput-object p1, p0, LX/HrT;->a:Lcom/facebook/composer/activity/ComposerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 9

    .prologue
    .line 2511039
    iget-object v0, p0, LX/HrT;->a:Lcom/facebook/composer/activity/ComposerFragment;

    const/4 v5, 0x0

    .line 2511040
    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->i()Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    move-result-object v7

    .line 2511041
    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->J:LX/HvN;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->getDateInfo()Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    .line 2511042
    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/facebook/user/model/User;

    .line 2511043
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2511044
    iget-object v2, v7, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->b:Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;

    move-object v2, v2

    .line 2511045
    new-instance v4, LX/5zw;

    invoke-direct {v4}, LX/5zw;-><init>()V

    .line 2511046
    iget v8, v3, Lcom/facebook/user/model/User;->C:I

    move v8, v8

    .line 2511047
    iput v8, v4, LX/5zw;->a:I

    .line 2511048
    move-object v8, v4

    .line 2511049
    iget v4, v3, Lcom/facebook/user/model/User;->D:I

    move v4, v4

    .line 2511050
    if-nez v4, :cond_0

    move-object v4, v5

    .line 2511051
    :goto_0
    iput-object v4, v8, LX/5zw;->b:Ljava/lang/Integer;

    .line 2511052
    move-object v4, v8

    .line 2511053
    iget v8, v3, Lcom/facebook/user/model/User;->E:I

    move v8, v8

    .line 2511054
    if-nez v8, :cond_1

    .line 2511055
    :goto_1
    iput-object v5, v4, LX/5zw;->c:Ljava/lang/Integer;

    .line 2511056
    move-object v3, v4

    .line 2511057
    invoke-virtual {v3}, LX/5zw;->a()Lcom/facebook/uicontrib/datepicker/Date;

    move-result-object v3

    invoke-virtual {v6}, Lcom/facebook/ipc/composer/model/ComposerDateInfo;->a()Lcom/facebook/uicontrib/datepicker/Date;

    move-result-object v4

    invoke-virtual {v6}, Lcom/facebook/ipc/composer/model/ComposerDateInfo;->b()Lcom/facebook/uicontrib/datepicker/Date;

    move-result-object v5

    invoke-virtual {v6}, Lcom/facebook/ipc/composer/model/ComposerDateInfo;->c()Z

    move-result v6

    .line 2511058
    iget-boolean v8, v7, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;->h:Z

    move v7, v8

    .line 2511059
    invoke-static/range {v1 .. v7}, Lcom/facebook/composer/datepicker/DatePickerActivity;->a(Landroid/content/Context;Lcom/facebook/graphql/enums/GraphQLLifeEventAPIIdentifier;Lcom/facebook/uicontrib/datepicker/Date;Lcom/facebook/uicontrib/datepicker/Date;Lcom/facebook/uicontrib/datepicker/Date;ZZ)Landroid/content/Intent;

    move-result-object v2

    .line 2511060
    iget-object v1, v0, Lcom/facebook/composer/activity/ComposerFragment;->aH:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    const/16 v3, 0x8

    invoke-interface {v1, v2, v3, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2511061
    return-void

    .line 2511062
    :cond_0
    iget v4, v3, Lcom/facebook/user/model/User;->D:I

    move v4, v4

    .line 2511063
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto :goto_0

    .line 2511064
    :cond_1
    iget v5, v3, Lcom/facebook/user/model/User;->E:I

    move v3, v5

    .line 2511065
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    goto :goto_1
.end method

.method public final a(Landroid/content/Intent;I)V
    .locals 2

    .prologue
    .line 2511037
    iget-object v0, p0, LX/HrT;->a:Lcom/facebook/composer/activity/ComposerFragment;

    iget-object v0, v0, Lcom/facebook/composer/activity/ComposerFragment;->aH:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, LX/HrT;->a:Lcom/facebook/composer/activity/ComposerFragment;

    invoke-interface {v0, p1, p2, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2511038
    return-void
.end method

.method public final a(Lcom/facebook/ui/dialogs/FbDialogFragment;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2511033
    iget-object v0, p0, LX/HrT;->a:Lcom/facebook/composer/activity/ComposerFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2511034
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2511035
    iget-object v0, p0, LX/HrT;->a:Lcom/facebook/composer/activity/ComposerFragment;

    invoke-static {v0}, Lcom/facebook/composer/activity/ComposerFragment;->ak(Lcom/facebook/composer/activity/ComposerFragment;)V

    .line 2511036
    return-void
.end method
