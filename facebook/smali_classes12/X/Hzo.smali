.class public final LX/Hzo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/HzM;


# instance fields
.field public final synthetic a:Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;)V
    .locals 0

    .prologue
    .line 2526002
    iput-object p1, p0, LX/Hzo;->a:Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/Hx7;Z)V
    .locals 1

    .prologue
    .line 2526003
    if-eqz p2, :cond_0

    .line 2526004
    iget-object v0, p0, LX/Hzo;->a:Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;

    .line 2526005
    iget-object p0, v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->B:LX/Hx7;

    if-ne p0, p1, :cond_1

    .line 2526006
    :cond_0
    :goto_0
    return-void

    .line 2526007
    :cond_1
    sget-object p0, LX/Hx7;->CALENDAR:LX/Hx7;

    if-ne p1, p0, :cond_4

    .line 2526008
    iget-object p0, v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->E:LX/1P0;

    invoke-virtual {p0}, LX/1OR;->f()Landroid/os/Parcelable;

    move-result-object p0

    iput-object p0, v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->G:Landroid/os/Parcelable;

    .line 2526009
    :goto_1
    iput-object p1, v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->B:LX/Hx7;

    .line 2526010
    iget-object p0, v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->s:Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;

    invoke-virtual {p0, p1}, Lcom/facebook/events/dashboard/EventsTabbedDashboardTabBar;->setSelectedTabType(LX/Hx7;)V

    .line 2526011
    iget-object p0, v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->r:LX/Hzt;

    invoke-virtual {p0, p1}, LX/Hzt;->a(LX/Hx7;)V

    .line 2526012
    iget-object p0, v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->x:LX/I05;

    sget-object p2, LX/I05;->INITIAL:LX/I05;

    if-ne p0, p2, :cond_2

    .line 2526013
    invoke-static {v0}, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->c(Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;)V

    .line 2526014
    :cond_2
    iget-object p2, v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->r:LX/Hzt;

    sget-object p0, LX/Hx7;->DISCOVER:LX/Hx7;

    if-ne p1, p0, :cond_3

    iget-object p0, v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->w:LX/I05;

    :goto_2
    invoke-virtual {p2, p0}, LX/Hzt;->a(LX/I05;)V

    .line 2526015
    sget-object p0, LX/Hx7;->CALENDAR:LX/Hx7;

    if-ne p1, p0, :cond_5

    .line 2526016
    iget-object p0, v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->E:LX/1P0;

    iget-object p2, v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->F:Landroid/os/Parcelable;

    invoke-virtual {p0, p2}, LX/1OR;->a(Landroid/os/Parcelable;)V

    .line 2526017
    :goto_3
    goto :goto_0

    .line 2526018
    :cond_3
    iget-object p0, v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->x:LX/I05;

    goto :goto_2

    .line 2526019
    :cond_4
    iget-object p0, v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->E:LX/1P0;

    invoke-virtual {p0}, LX/1OR;->f()Landroid/os/Parcelable;

    move-result-object p0

    iput-object p0, v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->F:Landroid/os/Parcelable;

    goto :goto_1

    .line 2526020
    :cond_5
    iget-object p0, v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->E:LX/1P0;

    iget-object p2, v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->G:Landroid/os/Parcelable;

    invoke-virtual {p0, p2}, LX/1OR;->a(Landroid/os/Parcelable;)V

    goto :goto_3
.end method
