.class public LX/J4q;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Lcom/facebook/privacy/checkup/protocol/FetchGenericPrivacyReviewParams;",
        "Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/0sO;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2644580
    invoke-direct {p0, p1}, LX/0ro;-><init>(LX/0sO;)V

    .line 2644581
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2644577
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2644578
    const-class v0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel;

    invoke-virtual {p3, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/checkup/protocol/FetchPrivacyCheckupModels$FetchGenericPrivacyReviewQueryModel;

    .line 2644579
    return-object v0
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 2644555
    const/4 v0, 0x1

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 3

    .prologue
    .line 2644556
    check-cast p1, Lcom/facebook/privacy/checkup/protocol/FetchGenericPrivacyReviewParams;

    .line 2644557
    invoke-static {}, LX/J5A;->d()LX/J56;

    move-result-object v0

    .line 2644558
    const-string v1, "max_num_steps"

    .line 2644559
    iget v2, p1, Lcom/facebook/privacy/checkup/protocol/FetchGenericPrivacyReviewParams;->a:I

    move v2, v2

    .line 2644560
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2644561
    const-string v1, "privacy_review_type"

    .line 2644562
    iget-object v2, p1, Lcom/facebook/privacy/checkup/protocol/FetchGenericPrivacyReviewParams;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2644563
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2644564
    const-string v1, "item_count"

    .line 2644565
    iget v2, p1, Lcom/facebook/privacy/checkup/protocol/FetchGenericPrivacyReviewParams;->c:I

    move v2, v2

    .line 2644566
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2644567
    const-string v1, "after"

    .line 2644568
    iget-object v2, p1, Lcom/facebook/privacy/checkup/protocol/FetchGenericPrivacyReviewParams;->d:Ljava/lang/String;

    move-object v2, v2

    .line 2644569
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2644570
    const-string v1, "image_scale"

    .line 2644571
    iget-object v2, p1, Lcom/facebook/privacy/checkup/protocol/FetchGenericPrivacyReviewParams;->e:Ljava/lang/String;

    move-object v2, v2

    .line 2644572
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2644573
    const-string v1, "review_id"

    .line 2644574
    iget-object v2, p1, Lcom/facebook/privacy/checkup/protocol/FetchGenericPrivacyReviewParams;->f:Ljava/lang/String;

    move-object v2, v2

    .line 2644575
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2644576
    return-object v0
.end method
