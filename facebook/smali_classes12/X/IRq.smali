.class public final LX/IRq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/DNQ;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)V
    .locals 0

    .prologue
    .line 2576724
    iput-object p1, p0, LX/IRq;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 2576725
    iget-object v0, p0, LX/IRq;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    invoke-static {v0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->P(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)V

    .line 2576726
    iget-object v0, p0, LX/IRq;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    invoke-static {v0}, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->X(Lcom/facebook/groups/feed/ui/GroupsFeedFragment;)V

    .line 2576727
    iget-object v0, p0, LX/IRq;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ax:LX/IPj;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/IRq;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v0, :cond_0

    .line 2576728
    iget-object v0, p0, LX/IRq;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ax:LX/IPj;

    iget-object v1, p0, LX/IRq;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    iget-object v1, v1, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aO:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v1}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->r()I

    move-result v1

    iget-object v2, p0, LX/IRq;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    iget-object v2, v2, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->l:LX/DNR;

    .line 2576729
    iget-object v3, v2, LX/DNR;->c:LX/0fz;

    move-object v2, v3

    .line 2576730
    iget-object v3, p0, LX/IRq;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    iget-object v3, v3, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->aP:LX/1Qq;

    invoke-static {v1, v2, v3}, LX/IPu;->a(ILX/0fz;LX/1Qq;)I

    move-result v1

    .line 2576731
    iput v1, v0, LX/IPj;->v:I

    .line 2576732
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 2576733
    iget-object v0, p0, LX/IRq;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->k:LX/DNJ;

    invoke-virtual {v0, p1}, LX/DNJ;->b(Z)V

    .line 2576734
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2576735
    iget-object v0, p0, LX/IRq;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->a:LX/2lS;

    invoke-virtual {v0}, LX/2lS;->a()V

    .line 2576736
    iget-object v0, p0, LX/IRq;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->k:LX/DNJ;

    invoke-virtual {v0}, LX/DNJ;->g()V

    .line 2576737
    return-void
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 2576738
    iget-object v0, p0, LX/IRq;->a:Lcom/facebook/groups/feed/ui/GroupsFeedFragment;

    iget-object v0, v0, Lcom/facebook/groups/feed/ui/GroupsFeedFragment;->ao:LX/DNp;

    invoke-virtual {v0, p1}, LX/DNp;->a(Z)V

    .line 2576739
    return-void
.end method
