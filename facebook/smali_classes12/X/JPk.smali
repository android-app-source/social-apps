.class public LX/JPk;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/hpp/VisitYourPageCardComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JPk",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/hpp/VisitYourPageCardComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2690367
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2690368
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JPk;->b:LX/0Zi;

    .line 2690369
    iput-object p1, p0, LX/JPk;->a:LX/0Ot;

    .line 2690370
    return-void
.end method

.method public static a(LX/0QB;)LX/JPk;
    .locals 4

    .prologue
    .line 2690356
    const-class v1, LX/JPk;

    monitor-enter v1

    .line 2690357
    :try_start_0
    sget-object v0, LX/JPk;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2690358
    sput-object v2, LX/JPk;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2690359
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2690360
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2690361
    new-instance v3, LX/JPk;

    const/16 p0, 0x1fea

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JPk;-><init>(LX/0Ot;)V

    .line 2690362
    move-object v0, v3

    .line 2690363
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2690364
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JPk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2690365
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2690366
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 2690321
    check-cast p2, LX/JPj;

    .line 2690322
    iget-object v0, p0, LX/JPk;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/hpp/VisitYourPageCardComponentSpec;

    iget-object v1, p2, LX/JPj;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/JPj;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    const/4 v4, 0x0

    .line 2690323
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2690324
    check-cast v3, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    .line 2690325
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    if-nez v3, :cond_0

    move-object v3, v4

    .line 2690326
    :goto_0
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 2690327
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v5

    .line 2690328
    :goto_1
    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 2690329
    :goto_2
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    const/4 p0, 0x0

    invoke-interface {v5, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    const/4 p0, 0x2

    invoke-interface {v5, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v5

    const p0, 0x7f0b257f

    invoke-interface {v5, p0}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v5

    const p0, 0x7f0b2580

    invoke-interface {v5, p0}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v5

    const p0, 0x7f020a3d

    invoke-interface {v5, p0}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v5

    .line 2690330
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object p0

    iget-object p2, v0, Lcom/facebook/feedplugins/hpp/VisitYourPageCardComponentSpec;->b:LX/1nu;

    invoke-virtual {p2, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object p2

    sget-object v1, Lcom/facebook/feedplugins/hpp/VisitYourPageCardComponentSpec;->d:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p2, v1}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object p2

    invoke-virtual {p2, v4}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object p2

    const v1, 0x7f0213f3

    invoke-virtual {p2, v1}, LX/1nw;->h(I)LX/1nw;

    move-result-object p2

    invoke-virtual {p2}, LX/1X5;->c()LX/1Di;

    move-result-object p2

    const v1, 0x7f0b257f

    invoke-interface {p2, v1}, LX/1Di;->i(I)LX/1Di;

    move-result-object p2

    const v1, 0x7f0b257e

    invoke-interface {p2, v1}, LX/1Di;->q(I)LX/1Di;

    move-result-object p2

    const/4 v1, 0x7

    const v2, 0x7f0b257d

    invoke-interface {p2, v1, v2}, LX/1Di;->g(II)LX/1Di;

    move-result-object p2

    const/4 v1, 0x6

    const v2, 0x7f0b257c

    invoke-interface {p2, v1, v2}, LX/1Di;->g(II)LX/1Di;

    move-result-object p2

    invoke-interface {p0, p2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object p0

    move-object v4, p0

    .line 2690331
    invoke-interface {v5, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/JPK;->a(LX/1De;)LX/1Di;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    const v5, 0x7f0a010e

    invoke-virtual {v3, v5}, LX/1ne;->n(I)LX/1ne;

    move-result-object v3

    const v5, 0x7f0b2582

    invoke-virtual {v3, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    const/4 v5, 0x1

    invoke-virtual {v3, v5}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/4 v5, 0x7

    const p0, 0x7f0b2571

    invoke-interface {v3, v5, p0}, LX/1Di;->g(II)LX/1Di;

    move-result-object v3

    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x3

    const v5, 0x7f0b2581

    invoke-interface {v3, v4, v5}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v3

    .line 2690332
    const v4, 0x22af8cba

    const/4 v5, 0x0

    invoke-static {p1, v4, v5}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v4, v4

    .line 2690333
    invoke-interface {v3, v4}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2690334
    return-object v0

    .line 2690335
    :cond_0
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    .line 2690336
    :cond_1
    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    goto/16 :goto_2

    :cond_2
    move-object v5, v4

    goto/16 :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2690337
    invoke-static {}, LX/1dS;->b()V

    .line 2690338
    iget v0, p1, LX/1dQ;->b:I

    .line 2690339
    packed-switch v0, :pswitch_data_0

    .line 2690340
    :goto_0
    return-object v2

    .line 2690341
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2690342
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2690343
    check-cast v1, LX/JPj;

    .line 2690344
    iget-object v3, p0, LX/JPk;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/hpp/VisitYourPageCardComponentSpec;

    iget-object v4, v1, LX/JPj;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object p1, v1, LX/JPj;->b:Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;

    .line 2690345
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object p2

    .line 2690346
    if-nez p2, :cond_2

    if-eqz v4, :cond_2

    .line 2690347
    iget-object p2, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p2, p2

    .line 2690348
    check-cast p2, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelFeedUnit;->s()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object p2

    move-object p0, p2

    .line 2690349
    :goto_1
    if-nez p0, :cond_0

    .line 2690350
    iget-object p2, v3, Lcom/facebook/feedplugins/hpp/VisitYourPageCardComponentSpec;->c:LX/0Ot;

    invoke-interface {p2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/JPN;

    const-string p0, "Cannot open profile without GraphQLPage"

    invoke-virtual {p2, v4, p1, p0}, LX/JPN;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;Ljava/lang/String;)V

    .line 2690351
    :goto_2
    goto :goto_0

    .line 2690352
    :cond_0
    invoke-static {p0}, LX/3Bd;->a(Lcom/facebook/graphql/model/GraphQLPage;)LX/1y5;

    move-result-object p2

    if-nez p2, :cond_1

    .line 2690353
    iget-object p2, v3, Lcom/facebook/feedplugins/hpp/VisitYourPageCardComponentSpec;->c:LX/0Ot;

    invoke-interface {p2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/JPN;

    const-string p0, "Cannot open profile LinkifyTargetBuilder.getLinkifyTarget(page) is null"

    invoke-virtual {p2, v4, p1, p0}, LX/JPN;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;Ljava/lang/String;)V

    goto :goto_2

    .line 2690354
    :cond_1
    iget-object p2, v3, Lcom/facebook/feedplugins/hpp/VisitYourPageCardComponentSpec;->c:LX/0Ot;

    invoke-interface {p2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/JPN;

    invoke-virtual {p2, v4, p1}, LX/JPN;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLMobilePageAdminPanelItem;)V

    .line 2690355
    iget-object p2, v3, Lcom/facebook/feedplugins/hpp/VisitYourPageCardComponentSpec;->a:LX/1nA;

    invoke-static {p0}, LX/3Bd;->a(Lcom/facebook/graphql/model/GraphQLPage;)LX/1y5;

    move-result-object p0

    const/4 v1, 0x0

    invoke-virtual {p2, v0, p0, v1}, LX/1nA;->a(Landroid/view/View;LX/1y5;Landroid/os/Bundle;)V

    goto :goto_2

    :cond_2
    move-object p0, p2

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x22af8cba
        :pswitch_0
    .end packed-switch
.end method
