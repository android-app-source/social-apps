.class public final LX/IXU;
.super LX/Chz;
.source ""


# instance fields
.field public final synthetic a:LX/IXX;


# direct methods
.method public constructor <init>(LX/IXX;)V
    .locals 0

    .prologue
    .line 2586168
    iput-object p1, p0, LX/IXU;->a:LX/IXX;

    invoke-direct {p0}, LX/Chz;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 8

    .prologue
    .line 2586169
    check-cast p1, LX/CiP;

    .line 2586170
    iget-object v0, p0, LX/IXU;->a:LX/IXX;

    .line 2586171
    iget-object v1, p1, LX/CiP;->b:LX/0ta;

    move-object v1, v1

    .line 2586172
    iput-object v1, v0, LX/IXX;->s:LX/0ta;

    .line 2586173
    iget-object v0, p0, LX/IXU;->a:LX/IXX;

    .line 2586174
    iget-object v1, p1, LX/CiP;->a:LX/Clo;

    move-object v1, v1

    .line 2586175
    const/4 v3, 0x0

    .line 2586176
    iget-object v2, v0, LX/IXX;->g:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    .line 2586177
    iput v3, v0, LX/IXX;->u:I

    .line 2586178
    invoke-virtual {v1}, LX/Clo;->d()I

    move-result v5

    move v4, v3

    :goto_0
    if-ge v4, v5, :cond_2

    .line 2586179
    invoke-virtual {v1, v4}, LX/Clo;->a(I)LX/Clr;

    move-result-object v2

    .line 2586180
    invoke-interface {v2}, LX/Clr;->lx_()I

    move-result v6

    const/16 v7, 0x8

    if-eq v6, v7, :cond_0

    invoke-interface {v2}, LX/Clr;->lx_()I

    move-result v6

    const/16 v7, 0x19

    if-ne v6, v7, :cond_1

    :cond_0
    instance-of v6, v2, LX/Cmi;

    if-eqz v6, :cond_1

    .line 2586181
    check-cast v2, LX/Cmi;

    .line 2586182
    iget-object v6, v2, LX/Cmi;->g:Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    move-object v6, v6

    .line 2586183
    if-eqz v6, :cond_1

    .line 2586184
    iget-object v2, v0, LX/IXX;->g:Ljava/util/HashMap;

    invoke-virtual {v2, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2586185
    iget-object v2, v0, LX/IXX;->g:Ljava/util/HashMap;

    invoke-virtual {v2, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 2586186
    :goto_1
    iget-object v7, v0, LX/IXX;->g:Ljava/util/HashMap;

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v7, v6, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2586187
    iget v2, v0, LX/IXX;->u:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, LX/IXX;->u:I

    .line 2586188
    :cond_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_0

    .line 2586189
    :cond_2
    iget-object v1, p0, LX/IXU;->a:LX/IXX;

    iget-object v0, p0, LX/IXU;->a:LX/IXX;

    iget-object v0, v0, LX/IXX;->s:LX/0ta;

    sget-object v2, LX/0ta;->FROM_SERVER:LX/0ta;

    if-eq v0, v2, :cond_3

    const/4 v0, 0x1

    .line 2586190
    :goto_2
    iput-boolean v0, v1, LX/IXX;->t:Z

    .line 2586191
    iget-object v0, p0, LX/IXU;->a:LX/IXX;

    .line 2586192
    iget-boolean v1, p1, LX/CiP;->c:Z

    move v1, v1

    .line 2586193
    iput-boolean v1, v0, LX/IXX;->x:Z

    .line 2586194
    iget-object v0, p0, LX/IXU;->a:LX/IXX;

    iget-object v0, v0, LX/IXX;->d:LX/Chv;

    invoke-virtual {v0, p0}, LX/0b4;->b(LX/0b2;)Z

    .line 2586195
    return-void

    .line 2586196
    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    :cond_4
    move v2, v3

    goto :goto_1
.end method
