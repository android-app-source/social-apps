.class public LX/HfA;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/list/annotations/GroupSectionSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public a:LX/HfJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2491401
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2491402
    return-void
.end method

.method public static a(LX/0QB;)LX/HfA;
    .locals 4

    .prologue
    .line 2491403
    const-class v1, LX/HfA;

    monitor-enter v1

    .line 2491404
    :try_start_0
    sget-object v0, LX/HfA;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2491405
    sput-object v2, LX/HfA;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2491406
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2491407
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2491408
    new-instance p0, LX/HfA;

    invoke-direct {p0}, LX/HfA;-><init>()V

    .line 2491409
    invoke-static {v0}, LX/HfJ;->a(LX/0QB;)LX/HfJ;

    move-result-object v3

    check-cast v3, LX/HfJ;

    .line 2491410
    iput-object v3, p0, LX/HfA;->a:LX/HfJ;

    .line 2491411
    move-object v0, p0

    .line 2491412
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2491413
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/HfA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2491414
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2491415
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
