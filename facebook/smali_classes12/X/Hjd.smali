.class public final enum LX/Hjd;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Hjd;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LX/Hjd;

.field public static final enum b:LX/Hjd;

.field public static final enum c:LX/Hjd;

.field private static final synthetic h:[LX/Hjd;


# instance fields
.field public d:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public e:Ljava/lang/String;

.field public f:LX/Hjc;

.field public g:LX/HkF;


# direct methods
.method public static constructor <clinit>()V
    .locals 11

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    new-instance v0, LX/Hjd;

    const-string v1, "ANBANNER"

    const-class v3, LX/Hjg;

    sget-object v4, LX/Hjc;->b:LX/Hjc;

    sget-object v5, LX/HkF;->BANNER:LX/HkF;

    invoke-direct/range {v0 .. v5}, LX/Hjd;-><init>(Ljava/lang/String;ILjava/lang/Class;LX/Hjc;LX/HkF;)V

    sput-object v0, LX/Hjd;->a:LX/Hjd;

    new-instance v3, LX/Hjd;

    const-string v4, "ANINTERSTITIAL"

    const-class v6, LX/Hji;

    sget-object v7, LX/Hjc;->b:LX/Hjc;

    sget-object v8, LX/HkF;->INTERSTITIAL:LX/HkF;

    move v5, v9

    invoke-direct/range {v3 .. v8}, LX/Hjd;-><init>(Ljava/lang/String;ILjava/lang/Class;LX/Hjc;LX/HkF;)V

    sput-object v3, LX/Hjd;->b:LX/Hjd;

    new-instance v3, LX/Hjd;

    const-string v4, "ANNATIVE"

    const-class v6, LX/Hjk;

    sget-object v7, LX/Hjc;->b:LX/Hjc;

    sget-object v8, LX/HkF;->NATIVE:LX/HkF;

    move v5, v10

    invoke-direct/range {v3 .. v8}, LX/Hjd;-><init>(Ljava/lang/String;ILjava/lang/Class;LX/Hjc;LX/HkF;)V

    sput-object v3, LX/Hjd;->c:LX/Hjd;

    const/4 v0, 0x3

    new-array v0, v0, [LX/Hjd;

    sget-object v1, LX/Hjd;->a:LX/Hjd;

    aput-object v1, v0, v2

    sget-object v1, LX/Hjd;->b:LX/Hjd;

    aput-object v1, v0, v9

    sget-object v1, LX/Hjd;->c:LX/Hjd;

    aput-object v1, v0, v10

    sput-object v0, LX/Hjd;->h:[LX/Hjd;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/Class;LX/Hjc;LX/HkF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "LX/Hjc;",
            "LX/HkF;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, LX/Hjd;->d:Ljava/lang/Class;

    iput-object p4, p0, LX/Hjd;->f:LX/Hjc;

    iput-object p5, p0, LX/Hjd;->g:LX/HkF;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Hjd;
    .locals 1

    const-class v0, LX/Hjd;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Hjd;

    return-object v0
.end method

.method public static values()[LX/Hjd;
    .locals 1

    sget-object v0, LX/Hjd;->h:[LX/Hjd;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Hjd;

    return-object v0
.end method
