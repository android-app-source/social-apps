.class public LX/IEE;
.super LX/2s5;
.source ""


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/DHs;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Landroid/widget/AbsListView$OnScrollListener;


# direct methods
.method public constructor <init>(LX/0gc;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;Landroid/widget/AbsListView$OnScrollListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0gc;",
            "Landroid/content/res/Resources;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "LX/DHs;",
            ">;",
            "Landroid/widget/AbsListView$OnScrollListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2551673
    invoke-direct {p0, p1}, LX/2s5;-><init>(LX/0gc;)V

    .line 2551674
    iput-object p2, p0, LX/IEE;->a:Landroid/content/res/Resources;

    .line 2551675
    iput-object p3, p0, LX/IEE;->b:Ljava/lang/String;

    .line 2551676
    iput-object p4, p0, LX/IEE;->c:Ljava/lang/String;

    .line 2551677
    iput-object p5, p0, LX/IEE;->d:Ljava/lang/String;

    .line 2551678
    iput-object p6, p0, LX/IEE;->e:LX/0Px;

    .line 2551679
    iput-object p7, p0, LX/IEE;->f:Landroid/widget/AbsListView$OnScrollListener;

    .line 2551680
    return-void
.end method

.method private e(I)LX/DHs;
    .locals 1

    .prologue
    .line 2551681
    iget-object v0, p0, LX/IEE;->e:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DHs;

    return-object v0
.end method


# virtual methods
.method public final G_(I)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 2551682
    sget-object v0, LX/IED;->a:[I

    invoke-direct {p0, p1}, LX/IEE;->e(I)LX/DHs;

    move-result-object v1

    invoke-virtual {v1}, LX/DHs;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2551683
    iget-object v0, p0, LX/IEE;->a:Landroid/content/res/Resources;

    const v1, 0x7f08381d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2551684
    :goto_0
    return-object v0

    .line 2551685
    :pswitch_0
    iget-object v0, p0, LX/IEE;->a:Landroid/content/res/Resources;

    const v1, 0x7f08381e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2551686
    :pswitch_1
    iget-object v0, p0, LX/IEE;->a:Landroid/content/res/Resources;

    const v1, 0x7f083822

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2551687
    :pswitch_2
    iget-object v0, p0, LX/IEE;->a:Landroid/content/res/Resources;

    const v1, 0x7f083820

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2551688
    :pswitch_3
    iget-object v0, p0, LX/IEE;->a:Landroid/content/res/Resources;

    const v1, 0x7f083822

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2551689
    :pswitch_4
    iget-object v0, p0, LX/IEE;->a:Landroid/content/res/Resources;

    const v1, 0x7f083824

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 4

    .prologue
    .line 2551690
    sget-object v0, LX/IED;->a:[I

    invoke-direct {p0, p1}, LX/IEE;->e(I)LX/DHs;

    move-result-object v1

    invoke-virtual {v1}, LX/DHs;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2551691
    iget-object v0, p0, LX/IEE;->b:Ljava/lang/String;

    iget-object v1, p0, LX/IEE;->c:Ljava/lang/String;

    iget-object v2, p0, LX/IEE;->d:Ljava/lang/String;

    .line 2551692
    new-instance v3, Lcom/facebook/friendlist/fragment/AllFriendListFragment;

    invoke-direct {v3}, Lcom/facebook/friendlist/fragment/AllFriendListFragment;-><init>()V

    .line 2551693
    invoke-static {v0, v1, v2}, Lcom/facebook/friendlist/fragment/FriendListFragment;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object p1

    invoke-virtual {v3, p1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2551694
    move-object v0, v3

    .line 2551695
    :goto_0
    iget-object v1, p0, LX/IEE;->f:Landroid/widget/AbsListView$OnScrollListener;

    .line 2551696
    iput-object v1, v0, Lcom/facebook/friendlist/fragment/FriendListFragment;->Q:Landroid/widget/AbsListView$OnScrollListener;

    .line 2551697
    return-object v0

    .line 2551698
    :pswitch_0
    iget-object v0, p0, LX/IEE;->b:Ljava/lang/String;

    iget-object v1, p0, LX/IEE;->c:Ljava/lang/String;

    iget-object v2, p0, LX/IEE;->d:Ljava/lang/String;

    .line 2551699
    new-instance v3, Lcom/facebook/friendlist/fragment/MutualFriendListFragment;

    invoke-direct {v3}, Lcom/facebook/friendlist/fragment/MutualFriendListFragment;-><init>()V

    .line 2551700
    invoke-static {v0, v1, v2}, Lcom/facebook/friendlist/fragment/FriendListFragment;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object p1

    invoke-virtual {v3, p1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2551701
    move-object v0, v3

    .line 2551702
    goto :goto_0

    .line 2551703
    :pswitch_1
    iget-object v0, p0, LX/IEE;->b:Ljava/lang/String;

    iget-object v1, p0, LX/IEE;->c:Ljava/lang/String;

    iget-object v2, p0, LX/IEE;->d:Ljava/lang/String;

    .line 2551704
    new-instance v3, Lcom/facebook/friendlist/fragment/PYMKFriendListFragment;

    invoke-direct {v3}, Lcom/facebook/friendlist/fragment/PYMKFriendListFragment;-><init>()V

    .line 2551705
    invoke-static {v0, v1, v2}, Lcom/facebook/friendlist/fragment/FriendListFragment;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object p1

    invoke-virtual {v3, p1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2551706
    move-object v0, v3

    .line 2551707
    goto :goto_0

    .line 2551708
    :pswitch_2
    iget-object v0, p0, LX/IEE;->b:Ljava/lang/String;

    iget-object v1, p0, LX/IEE;->c:Ljava/lang/String;

    iget-object v2, p0, LX/IEE;->d:Ljava/lang/String;

    .line 2551709
    new-instance v3, Lcom/facebook/friendlist/fragment/RecentlyAddedFriendListFragment;

    invoke-direct {v3}, Lcom/facebook/friendlist/fragment/RecentlyAddedFriendListFragment;-><init>()V

    .line 2551710
    invoke-static {v0, v1, v2}, Lcom/facebook/friendlist/fragment/FriendListFragment;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object p1

    invoke-virtual {v3, p1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2551711
    move-object v0, v3

    .line 2551712
    goto :goto_0

    .line 2551713
    :pswitch_3
    iget-object v0, p0, LX/IEE;->b:Ljava/lang/String;

    iget-object v1, p0, LX/IEE;->c:Ljava/lang/String;

    iget-object v2, p0, LX/IEE;->d:Ljava/lang/String;

    .line 2551714
    new-instance v3, Lcom/facebook/friendlist/fragment/SuggestionsFriendListFragment;

    invoke-direct {v3}, Lcom/facebook/friendlist/fragment/SuggestionsFriendListFragment;-><init>()V

    .line 2551715
    invoke-static {v0, v1, v2}, Lcom/facebook/friendlist/fragment/FriendListFragment;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object p1

    invoke-virtual {v3, p1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2551716
    move-object v0, v3

    .line 2551717
    goto :goto_0

    .line 2551718
    :pswitch_4
    iget-object v0, p0, LX/IEE;->b:Ljava/lang/String;

    iget-object v1, p0, LX/IEE;->c:Ljava/lang/String;

    iget-object v2, p0, LX/IEE;->d:Ljava/lang/String;

    .line 2551719
    new-instance v3, Lcom/facebook/friendlist/fragment/WithNewPostsFriendListFragment;

    invoke-direct {v3}, Lcom/facebook/friendlist/fragment/WithNewPostsFriendListFragment;-><init>()V

    .line 2551720
    invoke-static {v0, v1, v2}, Lcom/facebook/friendlist/fragment/FriendListFragment;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object p1

    invoke-virtual {v3, p1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2551721
    move-object v0, v3

    .line 2551722
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2551723
    iget-object v0, p0, LX/IEE;->e:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
