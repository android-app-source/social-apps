.class public final LX/HcL;
.super LX/HcB;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/HcB",
        "<",
        "Lcom/facebook/share/model/ShareVideo;",
        "LX/HcL;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Landroid/net/Uri;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2486968
    invoke-direct {p0}, LX/HcB;-><init>()V

    return-void
.end method

.method public static a(LX/HcL;Lcom/facebook/share/model/ShareVideo;)LX/HcL;
    .locals 2

    .prologue
    .line 2486970
    if-nez p1, :cond_0

    .line 2486971
    :goto_0
    return-object p0

    :cond_0
    invoke-super {p0, p1}, LX/HcB;->a(Lcom/facebook/share/model/ShareMedia;)LX/HcB;

    move-result-object v0

    check-cast v0, LX/HcL;

    .line 2486972
    iget-object v1, p1, Lcom/facebook/share/model/ShareVideo;->a:Landroid/net/Uri;

    move-object v1, v1

    .line 2486973
    iput-object v1, v0, LX/HcL;->a:Landroid/net/Uri;

    .line 2486974
    move-object p0, v0

    .line 2486975
    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(Lcom/facebook/share/model/ShareMedia;)LX/HcB;
    .locals 1

    .prologue
    .line 2486969
    check-cast p1, Lcom/facebook/share/model/ShareVideo;

    invoke-static {p0, p1}, LX/HcL;->a(LX/HcL;Lcom/facebook/share/model/ShareVideo;)LX/HcL;

    move-result-object v0

    return-object v0
.end method
