.class public final LX/HbX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/HbY;


# direct methods
.method public constructor <init>(LX/HbY;)V
    .locals 0

    .prologue
    .line 2485667
    iput-object p1, p0, LX/HbX;->a:LX/HbY;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(II)Landroid/animation/ValueAnimator;
    .locals 4

    .prologue
    .line 2485668
    const/4 v0, 0x2

    new-array v0, v0, [I

    const/4 v1, 0x0

    aput p1, v0, v1

    const/4 v1, 0x1

    aput p2, v0, v1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 2485669
    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 2485670
    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 2485671
    new-instance v1, LX/HbV;

    invoke-direct {v1, p0}, LX/HbV;-><init>(LX/HbX;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 2485672
    new-instance v1, LX/HbW;

    invoke-direct {v1, p0}, LX/HbW;-><init>(LX/HbX;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 2485673
    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 13

    .prologue
    const-wide/16 v8, 0x64

    const/4 v6, 0x0

    const/4 v2, 0x2

    const/4 v5, 0x1

    const v0, -0x3e67ff53    # -19.00033f

    invoke-static {v2, v5, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2485549
    iget-object v0, p0, LX/HbX;->a:LX/HbY;

    iget-object v0, v0, LX/HbN;->l:LX/Hbb;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HbX;->a:LX/HbY;

    iget v0, v0, LX/HbY;->v:I

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HbX;->a:LX/HbY;

    iget-object v0, v0, LX/HbN;->l:LX/Hbb;

    .line 2485550
    iget-boolean v3, v0, LX/Hbb;->e:Z

    move v0, v3

    .line 2485551
    if-nez v0, :cond_1

    .line 2485552
    :cond_0
    const v0, 0x3ff5bedc

    invoke-static {v2, v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2485553
    :goto_0
    return-void

    .line 2485554
    :cond_1
    iget-object v0, p0, LX/HbX;->a:LX/HbY;

    iget-boolean v0, v0, LX/HbY;->G:Z

    if-eqz v0, :cond_4

    .line 2485555
    iget-object v0, p0, LX/HbX;->a:LX/HbY;

    iget v0, v0, LX/HbY;->w:I

    iget-object v2, p0, LX/HbX;->a:LX/HbY;

    iget v2, v2, LX/HbY;->v:I

    invoke-direct {p0, v0, v2}, LX/HbX;->a(II)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 2485556
    iget-object v2, p0, LX/HbX;->a:LX/HbY;

    .line 2485557
    iput-boolean v6, v2, LX/HbY;->G:Z

    .line 2485558
    iget-object v2, p0, LX/HbX;->a:LX/HbY;

    iget-object v2, v2, LX/HbY;->x:LX/Had;

    iget-object v3, p0, LX/HbX;->a:LX/HbY;

    iget v3, v3, LX/HbN;->n:I

    iget-object v4, p0, LX/HbX;->a:LX/HbY;

    iget-boolean v4, v4, LX/HbY;->y:Z

    const/4 p1, 0x0

    const/4 v12, -0x1

    .line 2485559
    iget-object v7, v2, LX/Had;->a:LX/Hag;

    iget-object v10, v2, LX/Had;->a:LX/Hag;

    iget v10, v10, LX/Hag;->h:I

    .line 2485560
    iput v10, v7, LX/Hag;->i:I

    .line 2485561
    iget-object v7, v2, LX/Had;->a:LX/Hag;

    .line 2485562
    iput v12, v7, LX/Hag;->h:I

    .line 2485563
    iget-object v7, v2, LX/Had;->a:LX/Hag;

    sget-object v10, LX/Hac;->COLLAPSE_STARTED:LX/Hac;

    .line 2485564
    iput-object v10, v7, LX/Hag;->e:LX/Hac;

    .line 2485565
    iget-object v7, v2, LX/Had;->a:LX/Hag;

    iget-object v7, v7, LX/Hag;->c:LX/Hbd;

    .line 2485566
    iput-boolean p1, v7, LX/Hbd;->c:Z

    .line 2485567
    iget-object v7, v2, LX/Had;->a:LX/Hag;

    iget-object v7, v7, LX/Hag;->c:LX/Hbd;

    invoke-virtual {v7}, LX/Hbd;->c()V

    .line 2485568
    if-eqz v4, :cond_2

    .line 2485569
    iget-object v7, v2, LX/Had;->a:LX/Hag;

    const/16 v10, 0x64

    add-int/lit8 v11, v3, -0x1

    .line 2485570
    iget-object v3, v7, LX/Hag;->c:LX/Hbd;

    .line 2485571
    iput v12, v3, LX/Hbd;->t:I

    .line 2485572
    iget-object v3, v7, LX/Hag;->c:LX/Hbd;

    .line 2485573
    iput v10, v3, LX/Hbd;->u:I

    .line 2485574
    iget-object v3, v7, LX/Hag;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v3, v11}, Landroid/support/v7/widget/RecyclerView;->b(I)V

    .line 2485575
    :cond_2
    iget-object v7, v2, LX/Had;->a:LX/Hag;

    iget-object v7, v7, LX/Hag;->d:LX/HbJ;

    const/16 v3, 0x8

    const/4 v12, -0x1

    .line 2485576
    iget v10, v7, LX/HbJ;->d:I

    if-ne v10, v12, :cond_5

    .line 2485577
    :goto_1
    iget-object v7, v2, LX/Had;->a:LX/Hag;

    const v10, 0x7f02020b

    const/4 v11, 0x1

    .line 2485578
    packed-switch v11, :pswitch_data_0

    .line 2485579
    :goto_2
    iget-object v7, v2, LX/Had;->a:LX/Hag;

    iget-object v7, v7, LX/Hag;->f:Lcom/facebook/fbui/glyph/GlyphButton;

    iget-object v10, v2, LX/Had;->a:LX/Hag;

    iget-object v10, v10, LX/Hag;->b:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f083651

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Lcom/facebook/fbui/glyph/GlyphButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2485580
    iget-object v2, p0, LX/HbX;->a:LX/HbY;

    .line 2485581
    iput-boolean v6, v2, LX/HbY;->y:Z

    .line 2485582
    iget-object v2, p0, LX/HbX;->a:LX/HbY;

    .line 2485583
    sget-object v3, LX/HbU;->a:[I

    iget-object v4, v2, LX/HbN;->l:LX/Hbb;

    .line 2485584
    iget-object v6, v4, LX/Hbb;->d:LX/Hba;

    move-object v4, v6

    .line 2485585
    invoke-virtual {v4}, LX/Hba;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_1

    .line 2485586
    :goto_3
    iget-object v3, v2, LX/HbY;->A:LX/HbE;

    if-eqz v3, :cond_3

    iget-object v3, v2, LX/HbN;->l:LX/Hbb;

    .line 2485587
    iget-object v4, v3, LX/Hbb;->d:LX/Hba;

    move-object v3, v4

    .line 2485588
    sget-object v4, LX/Hba;->LOGIN_ALERTS:LX/Hba;

    if-ne v3, v4, :cond_3

    .line 2485589
    iget-object v3, v2, LX/HbY;->H:LX/Hai;

    invoke-virtual {v3}, LX/Hai;->i()Z

    move-result v6

    .line 2485590
    if-eqz v6, :cond_8

    iget-object v3, v2, LX/HbN;->m:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08362e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object v4, v3

    .line 2485591
    :goto_4
    if-eqz v6, :cond_9

    iget-object v3, v2, LX/HbN;->m:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x7f08362c

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2485592
    :goto_5
    iget-object v6, v2, LX/HbY;->q:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v6, v4}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 2485593
    iget-object v6, v2, LX/HbY;->p:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v6, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2485594
    iget-object v6, v2, LX/HbN;->l:LX/Hbb;

    .line 2485595
    iput-object v4, v6, LX/Hbb;->c:Ljava/lang/String;

    .line 2485596
    iget-object v4, v2, LX/HbN;->l:LX/Hbb;

    .line 2485597
    iput-object v3, v4, LX/Hbb;->b:Ljava/lang/String;

    .line 2485598
    iget-object v3, v2, LX/HbY;->A:LX/HbE;

    .line 2485599
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 2485600
    iget-object v4, v3, LX/HbE;->j:Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;

    invoke-virtual {v4}, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;->a()LX/1vs;

    move-result-object v4

    iget v4, v4, LX/1vs;->b:I

    if-nez v4, :cond_a

    .line 2485601
    :cond_3
    :goto_6
    const/4 v3, 0x0

    iput-object v3, v2, LX/HbY;->A:LX/HbE;

    .line 2485602
    iget-object v3, v2, LX/HbY;->t:Lcom/facebook/securitycheckup/items/DisableScrollRelativeLayout;

    invoke-virtual {v3}, Lcom/facebook/securitycheckup/items/DisableScrollRelativeLayout;->removeAllViews()V

    .line 2485603
    iget-object v2, p0, LX/HbX;->a:LX/HbY;

    iget-object v2, v2, LX/HbY;->q:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v2}, Lcom/facebook/resources/ui/FbButton;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v8, v9}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 2485604
    iget-object v2, p0, LX/HbX;->a:LX/HbY;

    iget-object v2, v2, LX/HbY;->q:Lcom/facebook/resources/ui/FbButton;

    iget-object v3, p0, LX/HbX;->a:LX/HbY;

    iget-object v3, v3, LX/HbN;->m:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a00e6

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbButton;->setTextColor(I)V

    .line 2485605
    iget-object v2, p0, LX/HbX;->a:LX/HbY;

    iget-object v2, v2, LX/HbN;->l:LX/Hbb;

    .line 2485606
    iput-boolean v5, v2, LX/Hbb;->f:Z

    .line 2485607
    :goto_7
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 2485608
    const v0, 0x137cc052

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto/16 :goto_0

    .line 2485609
    :cond_4
    iget-object v0, p0, LX/HbX;->a:LX/HbY;

    iget v0, v0, LX/HbY;->v:I

    iget-object v2, p0, LX/HbX;->a:LX/HbY;

    iget v2, v2, LX/HbY;->w:I

    invoke-direct {p0, v0, v2}, LX/HbX;->a(II)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 2485610
    iget-object v2, p0, LX/HbX;->a:LX/HbY;

    .line 2485611
    iput-boolean v5, v2, LX/HbY;->G:Z

    .line 2485612
    iget-object v2, p0, LX/HbX;->a:LX/HbY;

    iget-object v2, v2, LX/HbY;->x:LX/Had;

    iget-object v3, p0, LX/HbX;->a:LX/HbY;

    iget v3, v3, LX/HbN;->n:I

    .line 2485613
    iget-object v4, v2, LX/Had;->a:LX/Hag;

    sget-object v5, LX/Hac;->EXPAND_STARTED:LX/Hac;

    .line 2485614
    iput-object v5, v4, LX/Hag;->e:LX/Hac;

    .line 2485615
    iget-object v4, v2, LX/Had;->a:LX/Hag;

    const/4 v5, -0x1

    const/16 v6, 0x64

    .line 2485616
    iget-object v7, v4, LX/Hag;->c:LX/Hbd;

    .line 2485617
    iput v5, v7, LX/Hbd;->t:I

    .line 2485618
    iget-object v7, v4, LX/Hag;->c:LX/Hbd;

    .line 2485619
    iput v6, v7, LX/Hbd;->u:I

    .line 2485620
    iget-object v7, v4, LX/Hag;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v7, v3}, Landroid/support/v7/widget/RecyclerView;->b(I)V

    .line 2485621
    iget-object v4, v2, LX/Had;->a:LX/Hag;

    iget-object v4, v4, LX/Hag;->c:LX/Hbd;

    invoke-virtual {v4}, LX/Hbd;->c()V

    .line 2485622
    iget-object v2, p0, LX/HbX;->a:LX/HbY;

    iget-object v2, v2, LX/HbY;->q:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v2}, Lcom/facebook/resources/ui/FbButton;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v8, v9}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    goto :goto_7

    .line 2485623
    :cond_5
    iget-object v10, v7, LX/HbJ;->c:Landroid/util/SparseArray;

    iget v11, v7, LX/HbJ;->d:I

    add-int/lit8 v11, v11, -0x1

    invoke-virtual {v10, v11}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v10

    if-ltz v10, :cond_6

    .line 2485624
    iget-object v10, v7, LX/HbJ;->c:Landroid/util/SparseArray;

    iget v11, v7, LX/HbJ;->d:I

    add-int/lit8 v11, v11, -0x1

    invoke-virtual {v10, v11}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, LX/HbN;

    .line 2485625
    instance-of v11, v10, LX/HbY;

    if-eqz v11, :cond_6

    .line 2485626
    check-cast v10, LX/HbY;

    invoke-virtual {v10, v3}, LX/HbY;->c(I)V

    .line 2485627
    :cond_6
    iget-object v10, v7, LX/HbJ;->c:Landroid/util/SparseArray;

    iget v11, v7, LX/HbJ;->d:I

    add-int/lit8 v11, v11, 0x1

    invoke-virtual {v10, v11}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v10

    if-ltz v10, :cond_7

    .line 2485628
    iget-object v10, v7, LX/HbJ;->c:Landroid/util/SparseArray;

    iget v11, v7, LX/HbJ;->d:I

    add-int/lit8 v11, v11, 0x1

    invoke-virtual {v10, v11}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, LX/HbN;

    .line 2485629
    instance-of v11, v10, LX/HbY;

    if-eqz v11, :cond_7

    .line 2485630
    check-cast v10, LX/HbY;

    invoke-virtual {v10, v3}, LX/HbY;->c(I)V

    .line 2485631
    :cond_7
    iput v12, v7, LX/HbJ;->d:I

    goto/16 :goto_1

    .line 2485632
    :pswitch_0
    iget-object v12, v7, LX/Hag;->f:Lcom/facebook/fbui/glyph/GlyphButton;

    iget-object v3, v7, LX/Hag;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v12, v3}, Lcom/facebook/fbui/glyph/GlyphButton;->setGlyphColor(I)V

    goto/16 :goto_2

    .line 2485633
    :pswitch_1
    iget-object v12, v7, LX/Hag;->f:Lcom/facebook/fbui/glyph/GlyphButton;

    iget-object v3, v7, LX/Hag;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v10}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v12, v3}, Lcom/facebook/fbui/glyph/GlyphButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2485634
    iget-object v12, v7, LX/Hag;->b:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v12

    .line 2485635
    invoke-static {v12, p1}, LX/Hbr;->a(Landroid/util/DisplayMetrics;I)I

    move-result v12

    .line 2485636
    iget-object v3, v7, LX/Hag;->f:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v3, v12, v12, v12, v12}, Lcom/facebook/fbui/glyph/GlyphButton;->setPadding(IIII)V

    goto/16 :goto_2

    .line 2485637
    :pswitch_2
    iget-object v3, v2, LX/HbY;->F:LX/HaZ;

    const-string v4, "END_SESSION_COLLAPSE"

    invoke-virtual {v3, v4}, LX/HaZ;->a(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 2485638
    :pswitch_3
    iget-object v3, v2, LX/HbY;->F:LX/HaZ;

    const-string v4, "LA_COLLAPSE"

    invoke-virtual {v3, v4}, LX/HaZ;->a(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 2485639
    :pswitch_4
    iget-object v3, v2, LX/HbY;->F:LX/HaZ;

    const-string v4, "PASSWORD_COLLAPSE"

    invoke-virtual {v3, v4}, LX/HaZ;->a(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 2485640
    :cond_8
    iget-object v3, v2, LX/HbN;->m:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08362d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object v4, v3

    goto/16 :goto_4

    .line 2485641
    :cond_9
    iget-object v3, v2, LX/HbN;->m:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x7f08362b

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_5

    .line 2485642
    :cond_a
    iget-object v4, v3, LX/HbE;->a:LX/HbG;

    if-eqz v4, :cond_b

    iget-object v4, v3, LX/HbE;->a:LX/HbG;

    .line 2485643
    iget-boolean v10, v4, LX/HbG;->e:Z

    move v4, v10

    .line 2485644
    if-eqz v4, :cond_b

    move v4, v6

    :goto_8
    iget-object v10, v3, LX/HbE;->b:LX/HbG;

    if-eqz v10, :cond_c

    iget-object v10, v3, LX/HbE;->b:LX/HbG;

    .line 2485645
    iget-boolean v11, v10, LX/HbG;->e:Z

    move v10, v11

    .line 2485646
    if-eqz v10, :cond_c

    move v10, v6

    :goto_9
    iget-object v11, v3, LX/HbE;->c:LX/HbG;

    if-eqz v11, :cond_d

    iget-object v11, v3, LX/HbE;->c:LX/HbG;

    .line 2485647
    iget-boolean v12, v11, LX/HbG;->e:Z

    move v11, v12

    .line 2485648
    if-eqz v11, :cond_d

    .line 2485649
    :goto_a
    new-instance v7, LX/4JK;

    invoke-direct {v7}, LX/4JK;-><init>()V

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    .line 2485650
    const-string v12, "set_notif"

    invoke-virtual {v7, v12, v11}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2485651
    move-object v7, v7

    .line 2485652
    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    .line 2485653
    const-string v12, "set_email"

    invoke-virtual {v7, v12, v11}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2485654
    move-object v7, v7

    .line 2485655
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    .line 2485656
    const-string v12, "set_phone"

    invoke-virtual {v7, v12, v11}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2485657
    move-object v7, v7

    .line 2485658
    iget-object v11, v3, LX/HbE;->x:LX/HaZ;

    .line 2485659
    iget-object v12, v11, LX/HaZ;->b:Ljava/lang/String;

    move-object v11, v12

    .line 2485660
    const-string v12, "source"

    invoke-virtual {v7, v12, v11}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2485661
    move-object v7, v7

    .line 2485662
    new-instance v11, LX/Hav;

    invoke-direct {v11}, LX/Hav;-><init>()V

    move-object v11, v11

    .line 2485663
    const-string v12, "input"

    invoke-virtual {v11, v12, v7}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2485664
    invoke-static {v11}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v11

    .line 2485665
    iget-object v7, v3, LX/HbE;->t:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0tX;

    invoke-virtual {v7, v11}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2485666
    goto/16 :goto_6

    :cond_b
    move v4, v7

    goto :goto_8

    :cond_c
    move v10, v7

    goto :goto_9

    :cond_d
    move v6, v7

    goto :goto_a

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
