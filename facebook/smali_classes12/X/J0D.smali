.class public LX/J0D;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/payments/p2p/service/model/cards/DeletePaymentCardParams;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/J0D;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2636666
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2636667
    return-void
.end method

.method public static a(LX/0QB;)LX/J0D;
    .locals 3

    .prologue
    .line 2636668
    sget-object v0, LX/J0D;->a:LX/J0D;

    if-nez v0, :cond_1

    .line 2636669
    const-class v1, LX/J0D;

    monitor-enter v1

    .line 2636670
    :try_start_0
    sget-object v0, LX/J0D;->a:LX/J0D;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2636671
    if-eqz v2, :cond_0

    .line 2636672
    :try_start_1
    new-instance v0, LX/J0D;

    invoke-direct {v0}, LX/J0D;-><init>()V

    .line 2636673
    move-object v0, v0

    .line 2636674
    sput-object v0, LX/J0D;->a:LX/J0D;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2636675
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2636676
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2636677
    :cond_1
    sget-object v0, LX/J0D;->a:LX/J0D;

    return-object v0

    .line 2636678
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2636679
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 8

    .prologue
    .line 2636647
    check-cast p1, Lcom/facebook/payments/p2p/service/model/cards/DeletePaymentCardParams;

    .line 2636648
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 2636649
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "delete_payment_cards"

    .line 2636650
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 2636651
    move-object v1, v1

    .line 2636652
    const-string v2, "DELETE"

    .line 2636653
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 2636654
    move-object v1, v1

    .line 2636655
    const-string v2, "p2p_%d"

    .line 2636656
    iget-wide v6, p1, Lcom/facebook/payments/p2p/service/model/cards/DeletePaymentCardParams;->b:J

    move-wide v4, v6

    .line 2636657
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2636658
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 2636659
    move-object v1, v1

    .line 2636660
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 2636661
    move-object v0, v1

    .line 2636662
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2636663
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2636664
    move-object v0, v0

    .line 2636665
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2636645
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2636646
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->F()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
