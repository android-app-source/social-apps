.class public LX/JQ5;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/17S;

.field private final b:LX/17Q;

.field public final c:LX/1We;


# direct methods
.method public constructor <init>(LX/17S;LX/17Q;LX/1We;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2691004
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2691005
    iput-object p1, p0, LX/JQ5;->a:LX/17S;

    .line 2691006
    iput-object p2, p0, LX/JQ5;->b:LX/17Q;

    .line 2691007
    iput-object p3, p0, LX/JQ5;->c:LX/1We;

    .line 2691008
    return-void
.end method

.method public static a(LX/0QB;)LX/JQ5;
    .locals 6

    .prologue
    .line 2691009
    const-class v1, LX/JQ5;

    monitor-enter v1

    .line 2691010
    :try_start_0
    sget-object v0, LX/JQ5;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2691011
    sput-object v2, LX/JQ5;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2691012
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2691013
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2691014
    new-instance p0, LX/JQ5;

    invoke-static {v0}, LX/17S;->a(LX/0QB;)LX/17S;

    move-result-object v3

    check-cast v3, LX/17S;

    invoke-static {v0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v4

    check-cast v4, LX/17Q;

    invoke-static {v0}, LX/1We;->a(LX/0QB;)LX/1We;

    move-result-object v5

    check-cast v5, LX/1We;

    invoke-direct {p0, v3, v4, v5}, LX/JQ5;-><init>(LX/17S;LX/17Q;LX/1We;)V

    .line 2691015
    move-object v0, p0

    .line 2691016
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2691017
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JQ5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2691018
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2691019
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
