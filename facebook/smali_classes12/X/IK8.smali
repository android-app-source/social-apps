.class public LX/IK8;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/14x;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/content/SecureContextHelper;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2565073
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2565074
    return-void
.end method

.method public static b(LX/0QB;)LX/IK8;
    .locals 4

    .prologue
    .line 2565075
    new-instance v3, LX/IK8;

    invoke-direct {v3}, LX/IK8;-><init>()V

    .line 2565076
    invoke-static {p0}, LX/14x;->a(LX/0QB;)LX/14x;

    move-result-object v0

    check-cast v0, LX/14x;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    const-class v2, Landroid/content/Context;

    invoke-interface {p0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    .line 2565077
    iput-object v0, v3, LX/IK8;->a:LX/14x;

    iput-object v1, v3, LX/IK8;->b:Lcom/facebook/content/SecureContextHelper;

    iput-object v2, v3, LX/IK8;->c:Landroid/content/Context;

    .line 2565078
    return-object v3
.end method


# virtual methods
.method public final a(Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$JoinableModeModel;)V
    .locals 3

    .prologue
    .line 2565079
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$JoinableModeModel;->a()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2565080
    :cond_0
    :goto_0
    return-void

    .line 2565081
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$JoinableModeModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2565082
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2565083
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2565084
    iget-object v0, p0, LX/IK8;->a:LX/14x;

    invoke-virtual {v0}, LX/14x;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/IK8;->a:LX/14x;

    invoke-virtual {v0}, LX/14x;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/IK8;->a:LX/14x;

    const-string v2, "70.0"

    invoke-virtual {v0, v2}, LX/14x;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2565085
    sget-object v0, LX/007;->e:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 2565086
    iget-object v0, p0, LX/IK8;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/IK8;->c:Landroid/content/Context;

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0

    .line 2565087
    :cond_2
    iget-object v0, p0, LX/IK8;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/IK8;->c:Landroid/content/Context;

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method
