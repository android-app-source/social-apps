.class public LX/Hxz;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/feed/banner/GenericNotificationBanner;

.field public b:Lcom/facebook/events/common/EventAnalyticsParams;

.field public c:LX/HxV;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:LX/0Tn;

.field public final e:LX/0zG;

.field public final f:Lcom/facebook/events/dashboard/EventsDashboardFragment;

.field private final g:LX/Hx6;

.field public final h:Z

.field public final i:LX/Bie;

.field private final j:LX/HyA;

.field public final k:LX/3kp;

.field public final l:LX/Bl6;

.field public final m:LX/Hxx;

.field public final n:LX/0ad;

.field public final o:LX/Hxy;

.field public final p:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/events/model/Event;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/EventsDashboardFragment;LX/Hx6;Ljava/lang/Boolean;LX/0ad;LX/0zG;LX/Bie;LX/HyA;LX/3kp;LX/Bl6;)V
    .locals 3
    .param p1    # Lcom/facebook/events/dashboard/EventsDashboardFragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/Hx6;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2523410
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2523411
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "events/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    const-string v1, "subscriptions_megaphone"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iput-object v0, p0, LX/Hxz;->d:LX/0Tn;

    .line 2523412
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/Hxz;->p:Ljava/util/Set;

    .line 2523413
    iput-object p1, p0, LX/Hxz;->f:Lcom/facebook/events/dashboard/EventsDashboardFragment;

    .line 2523414
    iput-object p2, p0, LX/Hxz;->g:LX/Hx6;

    .line 2523415
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/Hxz;->h:Z

    .line 2523416
    iput-object p4, p0, LX/Hxz;->n:LX/0ad;

    .line 2523417
    iput-object p5, p0, LX/Hxz;->e:LX/0zG;

    .line 2523418
    iput-object p6, p0, LX/Hxz;->i:LX/Bie;

    .line 2523419
    iput-object p7, p0, LX/Hxz;->j:LX/HyA;

    .line 2523420
    iput-object p8, p0, LX/Hxz;->k:LX/3kp;

    .line 2523421
    iput-object p9, p0, LX/Hxz;->l:LX/Bl6;

    .line 2523422
    new-instance v0, LX/Hxx;

    invoke-direct {v0, p0}, LX/Hxx;-><init>(LX/Hxz;)V

    iput-object v0, p0, LX/Hxz;->m:LX/Hxx;

    .line 2523423
    iget-object v0, p0, LX/Hxz;->l:LX/Bl6;

    iget-object v1, p0, LX/Hxz;->m:LX/Hxx;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2523424
    new-instance v0, LX/Hxy;

    invoke-direct {v0, p0}, LX/Hxy;-><init>(LX/Hxz;)V

    iput-object v0, p0, LX/Hxz;->o:LX/Hxy;

    .line 2523425
    iget-object v0, p0, LX/Hxz;->l:LX/Bl6;

    iget-object v1, p0, LX/Hxz;->o:LX/Hxy;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2523426
    return-void
.end method

.method public static j(LX/Hxz;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 2523427
    iget-object v0, p0, LX/Hxz;->f:Lcom/facebook/events/dashboard/EventsDashboardFragment;

    .line 2523428
    iget-object p0, v0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, p0

    .line 2523429
    return-object v0
.end method

.method public static k(LX/Hxz;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 2523430
    iget-object v0, p0, LX/Hxz;->f:Lcom/facebook/events/dashboard/EventsDashboardFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public static o(LX/Hxz;)V
    .locals 2

    .prologue
    .line 2523431
    iget-object v0, p0, LX/Hxz;->f:Lcom/facebook/events/dashboard/EventsDashboardFragment;

    const v1, 0x7f0d08df

    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    .line 2523432
    new-instance v1, LX/Hxw;

    invoke-direct {v1, p0}, LX/Hxw;-><init>(LX/Hxz;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2523433
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2523434
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 2523435
    iget-object v0, p0, LX/Hxz;->g:LX/Hx6;

    sget-object v1, LX/Hx6;->BIRTHDAYS:LX/Hx6;

    if-eq v0, v1, :cond_0

    .line 2523436
    iget-object v0, p0, LX/Hxz;->j:LX/HyA;

    invoke-virtual {v0}, LX/HyA;->a()V

    .line 2523437
    :cond_0
    new-instance v0, Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-static {p0}, LX/Hxz;->j(LX/Hxz;)Landroid/os/Bundle;

    move-result-object v1

    .line 2523438
    if-eqz v1, :cond_1

    const-string v2, "action_ref"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/common/ActionSource;

    if-nez v2, :cond_2

    .line 2523439
    :cond_1
    sget-object v2, Lcom/facebook/events/common/EventActionContext;->b:Lcom/facebook/events/common/EventActionContext;

    .line 2523440
    :goto_0
    move-object v1, v2

    .line 2523441
    invoke-static {p0}, LX/Hxz;->j(LX/Hxz;)Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "event_source_module"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2523442
    if-eqz v2, :cond_4

    .line 2523443
    :goto_1
    move-object v2, v2

    .line 2523444
    iget-object v3, p0, LX/Hxz;->f:Lcom/facebook/events/dashboard/EventsDashboardFragment;

    invoke-virtual {v3}, Lcom/facebook/events/dashboard/EventsDashboardFragment;->a()Ljava/lang/String;

    move-result-object v3

    move-object v3, v3

    .line 2523445
    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/facebook/events/common/EventAnalyticsParams;-><init>(Lcom/facebook/events/common/EventActionContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, LX/Hxz;->b:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2523446
    return-void

    .line 2523447
    :cond_2
    sget-object v3, Lcom/facebook/events/common/ActionSource;->LAUNCHER:Lcom/facebook/events/common/ActionSource;

    invoke-virtual {v3, v2}, Lcom/facebook/events/common/ActionSource;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2523448
    new-instance v3, Lcom/facebook/events/common/EventActionContext;

    sget-object v4, Lcom/facebook/events/common/ActionSource;->DASHBOARD:Lcom/facebook/events/common/ActionSource;

    const/4 v5, 0x1

    invoke-direct {v3, v4, v2, v5}, Lcom/facebook/events/common/EventActionContext;-><init>(Lcom/facebook/events/common/ActionSource;Lcom/facebook/events/common/ActionSource;Z)V

    move-object v2, v3

    goto :goto_0

    .line 2523449
    :cond_3
    new-instance v3, Lcom/facebook/events/common/EventActionContext;

    sget-object v4, Lcom/facebook/events/common/ActionSource;->DASHBOARD:Lcom/facebook/events/common/ActionSource;

    const/4 v5, 0x0

    invoke-direct {v3, v4, v2, v5}, Lcom/facebook/events/common/EventActionContext;-><init>(Lcom/facebook/events/common/ActionSource;Lcom/facebook/events/common/ActionSource;Z)V

    move-object v2, v3

    goto :goto_0

    :cond_4
    const-string v2, "unknown"

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method public final a(LX/DBa;)V
    .locals 1

    .prologue
    .line 2523450
    iget-object v0, p0, LX/Hxz;->a:Lcom/facebook/feed/banner/GenericNotificationBanner;

    invoke-virtual {v0, p1}, Lcom/facebook/feed/banner/GenericNotificationBanner;->a(LX/DBa;)V

    .line 2523451
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2523452
    iget-object v0, p0, LX/Hxz;->f:Lcom/facebook/events/dashboard/EventsDashboardFragment;

    const v1, 0x7f0d08bd

    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/banner/GenericNotificationBanner;

    iput-object v0, p0, LX/Hxz;->a:Lcom/facebook/feed/banner/GenericNotificationBanner;

    .line 2523453
    return-void
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 2523454
    iget-object v0, p0, LX/Hxz;->a:Lcom/facebook/feed/banner/GenericNotificationBanner;

    invoke-virtual {v0}, LX/4nk;->a()V

    .line 2523455
    return-void
.end method
