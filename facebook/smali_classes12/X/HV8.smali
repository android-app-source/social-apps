.class public final LX/HV8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:J

.field public final synthetic b:LX/HV9;


# direct methods
.method public constructor <init>(LX/HV9;J)V
    .locals 0

    .prologue
    .line 2474744
    iput-object p1, p0, LX/HV8;->b:LX/HV9;

    iput-wide p2, p0, LX/HV8;->a:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x71cd4d6b

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2474745
    iget-object v1, p0, LX/HV8;->b:LX/HV9;

    iget-wide v2, p0, LX/HV8;->a:J

    .line 2474746
    iget-object v5, v1, LX/HV9;->a:LX/9XE;

    sget-object v6, LX/9XI;->EVENT_TAPPED_VIDEO_HUB_ALL_VIDEOS:LX/9XI;

    invoke-virtual {v5, v6, v2, v3}, LX/9XE;->a(LX/9X2;J)V

    .line 2474747
    iget-object v5, v1, LX/HV9;->b:LX/17W;

    invoke-virtual {v1}, LX/HV9;->getContext()Landroid/content/Context;

    move-result-object v6

    sget-object v7, LX/0ax;->aZ:Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    const-string p1, "VideoHubFragment"

    invoke-static {v7, p0, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2474748
    const v1, -0x57761c11

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
