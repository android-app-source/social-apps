.class public LX/JHB;
.super Lcom/facebook/android/maps/MapView;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;
.implements LX/5pQ;


# instance fields
.field public t:Z

.field private u:I

.field private final v:Landroid/graphics/Point;

.field private final w:Landroid/graphics/Point;

.field public x:LX/68M;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:LX/3BT;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2672482
    invoke-direct {p0, p1}, Lcom/facebook/android/maps/MapView;-><init>(Landroid/content/Context;)V

    .line 2672483
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/JHB;->t:Z

    .line 2672484
    const/4 v0, 0x2

    iput v0, p0, LX/JHB;->u:I

    .line 2672485
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, LX/JHB;->v:Landroid/graphics/Point;

    .line 2672486
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, LX/JHB;->w:Landroid/graphics/Point;

    .line 2672487
    invoke-virtual {p0}, LX/JHB;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 2672488
    invoke-virtual {p0}, LX/JHB;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2672489
    new-instance v1, LX/JHA;

    invoke-direct {v1, p0, p1, v0}, LX/JHA;-><init>(LX/JHB;Landroid/content/Context;Landroid/content/res/Resources;)V

    invoke-virtual {p0, v1}, Lcom/facebook/android/maps/MapView;->a(LX/68J;)V

    .line 2672490
    return-void
.end method

.method private a(LX/5pX;LX/680;LX/692;)V
    .locals 16

    .prologue
    .line 2672472
    invoke-virtual/range {p2 .. p2}, LX/680;->h()LX/31h;

    move-result-object v2

    .line 2672473
    move-object/from16 v0, p0

    iget-object v3, v0, LX/JHB;->v:Landroid/graphics/Point;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Point;->set(II)V

    .line 2672474
    move-object/from16 v0, p0

    iget-object v3, v0, LX/JHB;->v:Landroid/graphics/Point;

    invoke-virtual {v2, v3}, LX/31h;->a(Landroid/graphics/Point;)Lcom/facebook/android/maps/model/LatLng;

    move-result-object v8

    .line 2672475
    move-object/from16 v0, p0

    iget-object v3, v0, LX/JHB;->w:Landroid/graphics/Point;

    invoke-virtual/range {p0 .. p0}, LX/JHB;->getWidth()I

    move-result v4

    invoke-virtual/range {p0 .. p0}, LX/JHB;->getHeight()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Point;->set(II)V

    .line 2672476
    move-object/from16 v0, p0

    iget-object v3, v0, LX/JHB;->w:Landroid/graphics/Point;

    invoke-virtual {v2, v3}, LX/31h;->a(Landroid/graphics/Point;)Lcom/facebook/android/maps/model/LatLng;

    move-result-object v9

    .line 2672477
    iget-wide v2, v9, Lcom/facebook/android/maps/model/LatLng;->b:D

    iget-wide v4, v8, Lcom/facebook/android/maps/model/LatLng;->b:D

    sub-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v10

    .line 2672478
    iget-wide v2, v8, Lcom/facebook/android/maps/model/LatLng;->b:D

    invoke-static {v2, v3}, Ljava/lang/Math;->signum(D)D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-lez v2, :cond_0

    iget-wide v2, v9, Lcom/facebook/android/maps/model/LatLng;->b:D

    invoke-static {v2, v3}, Ljava/lang/Math;->signum(D)D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpg-double v2, v2, v4

    if-gez v2, :cond_0

    .line 2672479
    const-wide v2, 0x4076800000000000L    # 360.0

    add-double/2addr v10, v2

    .line 2672480
    :cond_0
    const-class v2, LX/5rQ;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/5pX;->b(Ljava/lang/Class;)LX/5p4;

    move-result-object v2

    check-cast v2, LX/5rQ;

    invoke-virtual {v2}, LX/5rQ;->i()LX/5s9;

    move-result-object v12

    new-instance v2, LX/JHT;

    invoke-virtual/range {p0 .. p0}, LX/JHB;->getId()I

    move-result v3

    move-object/from16 v0, p3

    iget-object v4, v0, LX/692;->a:Lcom/facebook/android/maps/model/LatLng;

    iget-wide v4, v4, Lcom/facebook/android/maps/model/LatLng;->a:D

    move-object/from16 v0, p3

    iget-object v6, v0, LX/692;->a:Lcom/facebook/android/maps/model/LatLng;

    iget-wide v6, v6, Lcom/facebook/android/maps/model/LatLng;->b:D

    iget-wide v14, v9, Lcom/facebook/android/maps/model/LatLng;->a:D

    iget-wide v8, v8, Lcom/facebook/android/maps/model/LatLng;->a:D

    sub-double v8, v14, v8

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(D)D

    move-result-wide v8

    invoke-direct/range {v2 .. v11}, LX/JHT;-><init>(IDDDD)V

    invoke-virtual {v12, v2}, LX/5s9;->a(LX/5r0;)V

    .line 2672481
    return-void
.end method


# virtual methods
.method public final a(LX/680;LX/692;)V
    .locals 1

    .prologue
    .line 2672468
    const/4 v0, 0x1

    .line 2672469
    iput-boolean v0, p0, LX/JHB;->t:Z

    .line 2672470
    invoke-virtual {p0}, LX/JHB;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, LX/5pX;

    invoke-direct {p0, v0, p1, p2}, LX/JHB;->a(LX/5pX;LX/680;LX/692;)V

    .line 2672471
    return-void
.end method

.method public final bM_()V
    .locals 0

    .prologue
    .line 2672467
    return-void
.end method

.method public final bN_()V
    .locals 0

    .prologue
    .line 2672466
    return-void
.end method

.method public final bO_()V
    .locals 0

    .prologue
    .line 2672465
    return-void
.end method

.method public final dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 2672461
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2672462
    :goto_0
    invoke-super {p0, p1}, Lcom/facebook/android/maps/MapView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    .line 2672463
    :pswitch_0
    invoke-virtual {p0}, LX/JHB;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_0

    .line 2672464
    :pswitch_1
    invoke-virtual {p0}, LX/JHB;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onPreDraw()Z
    .locals 1

    .prologue
    .line 2672458
    iget-boolean v0, p0, LX/JHB;->t:Z

    if-eqz v0, :cond_0

    iget v0, p0, LX/JHB;->u:I

    if-lez v0, :cond_0

    .line 2672459
    iget v0, p0, LX/JHB;->u:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/JHB;->u:I

    .line 2672460
    :cond_0
    iget-boolean v0, p0, LX/JHB;->t:Z

    if-eqz v0, :cond_1

    iget v0, p0, LX/JHB;->u:I

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
