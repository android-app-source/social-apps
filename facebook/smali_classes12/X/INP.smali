.class public abstract LX/INP;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public a:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

.field public b:Lcom/facebook/fig/header/FigHeader;

.field public c:Lcom/facebook/fig/footer/FigFooter;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2570781
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2570782
    invoke-virtual {p0}, LX/INP;->a()V

    .line 2570783
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 2570786
    const v0, 0x7f0302ff

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2570787
    const v0, 0x7f0d0a4e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/header/FigHeader;

    iput-object v0, p0, LX/INP;->b:Lcom/facebook/fig/header/FigHeader;

    .line 2570788
    iget-object v0, p0, LX/INP;->b:Lcom/facebook/fig/header/FigHeader;

    invoke-virtual {p0}, LX/INP;->getHeaderTextResId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fig/header/FigHeader;->setTitleText(I)V

    .line 2570789
    const v0, 0x7f0d0a4f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    iput-object v0, p0, LX/INP;->a:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    .line 2570790
    const v0, 0x7f0d0a51

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/footer/FigFooter;

    iput-object v0, p0, LX/INP;->c:Lcom/facebook/fig/footer/FigFooter;

    .line 2570791
    const/4 v2, 0x0

    .line 2570792
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, LX/INP;->setGravity(I)V

    .line 2570793
    invoke-virtual {p0}, LX/INP;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00d7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2570794
    const v1, 0x7f020c6f

    invoke-virtual {p0, v1}, LX/INP;->setBackgroundResource(I)V

    .line 2570795
    invoke-virtual {p0, v2, v0, v2, v2}, LX/INP;->setPadding(IIII)V

    .line 2570796
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/INP;->setOrientation(I)V

    .line 2570797
    return-void
.end method

.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 2570784
    iget-object v0, p0, LX/INP;->c:Lcom/facebook/fig/footer/FigFooter;

    invoke-virtual {v0, p1}, Lcom/facebook/fig/footer/FigFooter;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2570785
    return-void
.end method

.method public abstract getHeaderTextResId()I
.end method
