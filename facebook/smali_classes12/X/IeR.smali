.class public LX/IeR;
.super LX/3Ml;
.source ""


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field public d:LX/IiJ;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2598554
    const-class v0, LX/IeR;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/IeR;->c:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(LX/0Zr;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2598555
    invoke-direct {p0, p1}, LX/3Ml;-><init>(LX/0Zr;)V

    .line 2598556
    return-void
.end method

.method private a(LX/15i;I)Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 2598557
    invoke-virtual {p1, p2, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2598558
    invoke-virtual {p1, p2, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 2598559
    invoke-static {v0, v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 2598560
    :goto_0
    return-object v0

    .line 2598561
    :cond_0
    iget-object v0, p0, LX/IeR;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 2598562
    const/4 v2, 0x0

    invoke-virtual {p1, p2, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2598563
    invoke-static {v2, v3, v0, v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(JJ)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/IeR;
    .locals 3

    .prologue
    .line 2598564
    new-instance v1, LX/IeR;

    invoke-static {p0}, LX/0Zr;->a(LX/0QB;)LX/0Zr;

    move-result-object v0

    check-cast v0, LX/0Zr;

    invoke-direct {v1, v0}, LX/IeR;-><init>(LX/0Zr;)V

    .line 2598565
    new-instance v2, LX/IiJ;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-direct {v2, v0}, LX/IiJ;-><init>(LX/0tX;)V

    .line 2598566
    move-object v0, v2

    .line 2598567
    check-cast v0, LX/IiJ;

    const/16 v2, 0x15e7

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    .line 2598568
    iput-object v0, v1, LX/IeR;->d:LX/IiJ;

    iput-object v2, v1, LX/IeR;->e:LX/0Or;

    .line 2598569
    return-object v1
.end method


# virtual methods
.method public final b(Ljava/lang/CharSequence;)LX/39y;
    .locals 14
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2598570
    new-instance v6, LX/39y;

    invoke-direct {v6}, LX/39y;-><init>()V

    .line 2598571
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 2598572
    :goto_0
    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2598573
    invoke-static {v4}, LX/3Og;->a(Ljava/lang/CharSequence;)LX/3Og;

    move-result-object v0

    iput-object v0, v6, LX/39y;->a:Ljava/lang/Object;

    .line 2598574
    const/4 v0, -0x1

    iput v0, v6, LX/39y;->b:I

    move-object v0, v6

    .line 2598575
    :goto_1
    return-object v0

    .line 2598576
    :cond_0
    const-string v4, ""

    goto :goto_0

    .line 2598577
    :cond_1
    :try_start_0
    iget-object v0, p0, LX/IeR;->d:LX/IiJ;

    invoke-virtual {v0, v4}, LX/IiJ;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    const v1, 0x7484835b    # 8.399016E31f

    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/messagesearch/graphql/MessageSearchQueryModels$MessageSearchQueryModel$MessageThreadsModel;

    .line 2598578
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    .line 2598579
    invoke-virtual {v0}, Lcom/facebook/messaging/messagesearch/graphql/MessageSearchQueryModels$MessageSearchQueryModel$MessageThreadsModel;->a()LX/2uF;

    move-result-object v0

    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v8

    :cond_2
    invoke-interface {v8}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v8}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2598580
    const/4 v2, 0x0

    const v3, -0x489bc5fb

    invoke-static {v1, v0, v2, v3}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 2598581
    if-eqz v0, :cond_3

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    .line 2598582
    :goto_2
    new-instance v9, LX/0Pz;

    invoke-direct {v9}, LX/0Pz;-><init>()V

    .line 2598583
    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v10

    :goto_3
    invoke-interface {v10}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v10}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v5, v0, LX/1vs;->b:I

    .line 2598584
    new-instance v0, Landroid/util/Pair;

    const/4 v1, 0x4

    invoke-virtual {v3, v5, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v3, v5, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v9, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2598585
    const/4 v0, 0x2

    const-class v1, Lcom/facebook/messaging/messagesearch/graphql/MessageSearchQueryModels$MessageSearchQueryModel$MessageThreadsModel$EdgesModel$MatchedMessagesModel$MessageSenderModel;

    invoke-virtual {v3, v5, v0, v1}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/messagesearch/graphql/MessageSearchQueryModels$MessageSearchQueryModel$MessageThreadsModel$EdgesModel$MatchedMessagesModel$MessageSenderModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/messagesearch/graphql/MessageSearchQueryModels$MessageSearchQueryModel$MessageThreadsModel$EdgesModel$MatchedMessagesModel$MessageSenderModel;->k()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v2, v0, LX/1vs;->b:I

    .line 2598586
    new-instance v0, LX/DAV;

    const/4 v11, 0x1

    invoke-virtual {v1, v2, v11}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    const-class v11, Lcom/facebook/messaging/messagesearch/graphql/MessageSearchQueryModels$MessageSearchQueryModel$MessageThreadsModel$EdgesModel$MatchedMessagesModel$MessageSenderModel;

    invoke-virtual {v3, v5, v2, v11}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/messagesearch/graphql/MessageSearchQueryModels$MessageSearchQueryModel$MessageThreadsModel$EdgesModel$MatchedMessagesModel$MessageSenderModel;

    invoke-virtual {v2}, Lcom/facebook/messaging/messagesearch/graphql/MessageSearchQueryModels$MessageSearchQueryModel$MessageThreadsModel$EdgesModel$MatchedMessagesModel$MessageSenderModel;->j()Ljava/lang/String;

    move-result-object v2

    const/4 v11, 0x3

    invoke-virtual {v3, v5, v11}, LX/15i;->g(II)I

    move-result v5

    invoke-direct {p0, v3, v5}, LX/IeR;->a(LX/15i;I)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v3

    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, LX/DAV;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;LX/0Px;)V

    .line 2598587
    invoke-virtual {v7, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_3

    .line 2598588
    :catch_0
    move-exception v0

    .line 2598589
    :goto_4
    sget-object v1, LX/IeR;->c:Ljava/lang/String;

    const-string v2, "Exception while searching messages with query: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-static {v1, v0, v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2598590
    const/4 v0, 0x0

    iput v0, v6, LX/39y;->b:I

    .line 2598591
    invoke-static {p1}, LX/3Og;->b(Ljava/lang/CharSequence;)LX/3Og;

    move-result-object v0

    iput-object v0, v6, LX/39y;->a:Ljava/lang/Object;

    move-object v0, v6

    .line 2598592
    goto/16 :goto_1

    .line 2598593
    :cond_3
    :try_start_1
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto/16 :goto_2

    .line 2598594
    :cond_4
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2598595
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    iput v1, v6, LX/39y;->b:I

    .line 2598596
    invoke-static {p1, v0}, LX/3Og;->a(Ljava/lang/CharSequence;LX/0Px;)LX/3Og;

    move-result-object v0

    iput-object v0, v6, LX/39y;->a:Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v0, v6

    .line 2598597
    goto/16 :goto_1

    .line 2598598
    :catch_1
    move-exception v0

    goto :goto_4
.end method
