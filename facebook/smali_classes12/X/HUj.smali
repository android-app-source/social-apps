.class public final LX/HUj;
.super LX/1Cv;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;)V
    .locals 0

    .prologue
    .line 2473591
    iput-object p1, p0, LX/HUj;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    invoke-direct {p0}, LX/1Cv;-><init>()V

    return-void
.end method

.method private a(I)Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel;
    .locals 2

    .prologue
    .line 2473588
    iget-object v0, p0, LX/HUj;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->t:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    if-ge p1, v0, :cond_0

    if-lez p1, :cond_0

    .line 2473589
    iget-object v0, p0, LX/HUj;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->t:Ljava/util/List;

    add-int/lit8 v1, p1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel;

    .line 2473590
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2473584
    iget-object v0, p0, LX/HUj;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iget-boolean v0, v0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->F:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 2473585
    :goto_0
    sget-object v1, LX/HUm;->ALL_VIDEOS:LX/HUm;

    invoke-virtual {v1}, LX/HUm;->ordinal()I

    move-result v1

    if-ne p1, v1, :cond_1

    new-instance v1, LX/HV9;

    iget-object v2, p0, LX/HUj;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LX/HV9;-><init>(Landroid/content/Context;I)V

    move-object v0, v1

    :goto_1
    return-object v0

    .line 2473586
    :cond_0
    const v0, 0x7f010633

    goto :goto_0

    .line 2473587
    :cond_1
    new-instance v1, LX/HVD;

    iget-object v2, p0, LX/HUj;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LX/HVD;-><init>(Landroid/content/Context;I)V

    move-object v0, v1

    goto :goto_1
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 5

    .prologue
    .line 2473546
    iget-object v0, p0, LX/HUj;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iget-boolean v0, v0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->F:Z

    if-eqz v0, :cond_0

    .line 2473547
    const v0, 0x7f0a0048

    invoke-virtual {p3, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 2473548
    :cond_0
    check-cast p2, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel;

    .line 2473549
    sget-object v0, LX/HUm;->ALL_VIDEOS:LX/HUm;

    invoke-virtual {v0}, LX/HUm;->ordinal()I

    move-result v0

    if-ne p4, v0, :cond_2

    .line 2473550
    check-cast p3, LX/HV9;

    .line 2473551
    iget-object v0, p0, LX/HUj;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iget-wide v0, v0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->q:J

    iget-object v2, p0, LX/HUj;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iget v2, v2, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->v:I

    .line 2473552
    iget-object p0, p3, LX/HV9;->c:Lcom/facebook/fbui/widget/text/BadgeTextView;

    invoke-virtual {p3}, LX/HV9;->getContext()Landroid/content/Context;

    move-result-object p1

    const p2, 0x7f08180a

    invoke-virtual {p1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2473553
    iget-object p0, p3, LX/HV9;->c:Lcom/facebook/fbui/widget/text/BadgeTextView;

    invoke-static {v2}, LX/HVD;->a(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setBadgeText(Ljava/lang/CharSequence;)V

    .line 2473554
    iget-object p0, p3, LX/HV9;->c:Lcom/facebook/fbui/widget/text/BadgeTextView;

    new-instance p1, LX/HV8;

    invoke-direct {p1, p3, v0, v1}, LX/HV8;-><init>(LX/HV9;J)V

    invoke-virtual {p0, p1}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2473555
    :cond_1
    :goto_0
    return-void

    .line 2473556
    :cond_2
    sget-object v0, LX/HUm;->VIDEO_LIST:LX/HUm;

    invoke-virtual {v0}, LX/HUm;->ordinal()I

    move-result v0

    if-ne p4, v0, :cond_1

    .line 2473557
    check-cast p3, LX/HVD;

    .line 2473558
    invoke-virtual {p0}, LX/HUj;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, LX/HUj;->a(I)Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel;

    move-result-object v0

    if-ne p2, v0, :cond_3

    .line 2473559
    iget-object v0, p0, LX/HUj;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->d(Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;Z)V

    .line 2473560
    :cond_3
    iget-object v0, p0, LX/HUj;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iget-wide v0, v0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->q:J

    const/16 p4, 0x8

    const/4 p0, 0x0

    .line 2473561
    invoke-virtual {p2}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel;->m()Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel$VideoListVideosModel;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {p2}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel;->m()Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel$VideoListVideosModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel$VideoListVideosModel;->j()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2473562
    :cond_4
    invoke-virtual {p3, p4}, LX/HVD;->setVisibility(I)V

    .line 2473563
    :cond_5
    goto :goto_0

    .line 2473564
    :cond_6
    invoke-virtual {p3, p0}, LX/HVD;->setVisibility(I)V

    .line 2473565
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2473566
    invoke-virtual {p2}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel;->j()Ljava/lang/String;

    move-result-object v2

    .line 2473567
    invoke-virtual {p2}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel;->l()Ljava/lang/String;

    move-result-object v3

    .line 2473568
    if-nez v3, :cond_7

    .line 2473569
    iget-object v3, p3, LX/HVD;->d:Ljava/lang/String;

    .line 2473570
    :cond_7
    move-object v3, v3

    .line 2473571
    invoke-virtual {p2}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel;->m()Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel$VideoListVideosModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel$VideoListVideosModel;->a()I

    move-result v4

    new-instance p1, LX/HVC;

    invoke-direct {p1, p3, v0, v1, v2}, LX/HVC;-><init>(LX/HVD;JLjava/lang/String;)V

    .line 2473572
    iget-object v2, p3, LX/HVD;->c:Lcom/facebook/fbui/widget/text/BadgeTextView;

    invoke-virtual {v2, v3}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2473573
    iget-object v2, p3, LX/HVD;->c:Lcom/facebook/fbui/widget/text/BadgeTextView;

    invoke-static {v4}, LX/HVD;->a(I)Ljava/lang/String;

    move-result-object p5

    invoke-virtual {v2, p5}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setBadgeText(Ljava/lang/CharSequence;)V

    .line 2473574
    iget-object v2, p3, LX/HVD;->c:Lcom/facebook/fbui/widget/text/BadgeTextView;

    invoke-virtual {v2, p1}, Lcom/facebook/fbui/widget/text/BadgeTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2473575
    invoke-virtual {p2}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel;->m()Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel$VideoListVideosModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel$VideoListVideosModel;->j()LX/0Px;

    move-result-object p1

    move v4, p0

    .line 2473576
    :goto_1
    const/4 v2, 0x3

    if-ge v4, v2, :cond_5

    .line 2473577
    iget-object v2, p3, LX/HVD;->b:[Landroid/view/View;

    aget-object v2, v2, v4

    check-cast v2, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;

    .line 2473578
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    if-ge v4, v3, :cond_8

    .line 2473579
    invoke-virtual {p1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

    .line 2473580
    invoke-static {v2, v3}, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->b(Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;)V

    .line 2473581
    invoke-virtual {v2, p0}, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->setVisibility(I)V

    .line 2473582
    :goto_2
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_1

    .line 2473583
    :cond_8
    invoke-virtual {v2, p4}, Lcom/facebook/pages/fb4a/videohub/ui/PagesVideoHubVideoListItem;->setVisibility(I)V

    goto :goto_2
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2473537
    iget-object v0, p0, LX/HUj;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->t:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2473545
    invoke-direct {p0, p1}, LX/HUj;->a(I)Lcom/facebook/pages/fb4a/videohub/graphql/VideoHubModels$PageVideoListModel;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2473542
    iget-object v0, p0, LX/HUj;->a:Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;

    iget-object v0, v0, Lcom/facebook/pages/fb4a/videohub/fragments/PagesVideosFragment;->t:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    if-ge p1, v0, :cond_0

    .line 2473543
    int-to-long v0, p1

    .line 2473544
    :goto_0
    return-wide v0

    :cond_0
    const-wide/high16 v0, -0x8000000000000000L

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2473539
    if-nez p1, :cond_0

    .line 2473540
    sget-object v0, LX/HUm;->ALL_VIDEOS:LX/HUm;

    invoke-virtual {v0}, LX/HUm;->ordinal()I

    move-result v0

    .line 2473541
    :goto_0
    return v0

    :cond_0
    sget-object v0, LX/HUm;->VIDEO_LIST:LX/HUm;

    invoke-virtual {v0}, LX/HUm;->ordinal()I

    move-result v0

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2473538
    invoke-static {}, LX/HUm;->values()[LX/HUm;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
