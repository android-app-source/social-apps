.class public final LX/IbY;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 2594091
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 2594092
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2594093
    :goto_0
    return v1

    .line 2594094
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2594095
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_3

    .line 2594096
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 2594097
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2594098
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 2594099
    const-string v3, "ride_providers"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2594100
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2594101
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->START_ARRAY:LX/15z;

    if-ne v2, v3, :cond_2

    .line 2594102
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_ARRAY:LX/15z;

    if-eq v2, v3, :cond_2

    .line 2594103
    const/4 v3, 0x0

    .line 2594104
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v4, :cond_9

    .line 2594105
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2594106
    :goto_3
    move v2, v3

    .line 2594107
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2594108
    :cond_2
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    move v0, v0

    .line 2594109
    goto :goto_1

    .line 2594110
    :cond_3
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2594111
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2594112
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    .line 2594113
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2594114
    :cond_6
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_8

    .line 2594115
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2594116
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2594117
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_6

    if-eqz v5, :cond_6

    .line 2594118
    const-string v6, "name"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 2594119
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_4

    .line 2594120
    :cond_7
    const-string v6, "ride_invite"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 2594121
    const/4 v5, 0x0

    .line 2594122
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v6, :cond_10

    .line 2594123
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2594124
    :goto_5
    move v2, v5

    .line 2594125
    goto :goto_4

    .line 2594126
    :cond_8
    const/4 v5, 0x2

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2594127
    invoke-virtual {p1, v3, v4}, LX/186;->b(II)V

    .line 2594128
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, LX/186;->b(II)V

    .line 2594129
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_3

    :cond_9
    move v2, v3

    move v4, v3

    goto :goto_4

    .line 2594130
    :cond_a
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2594131
    :cond_b
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_f

    .line 2594132
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 2594133
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2594134
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_b

    if-eqz v9, :cond_b

    .line 2594135
    const-string v10, "share_error"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_c

    .line 2594136
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_6

    .line 2594137
    :cond_c
    const-string v10, "share_error_title"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_d

    .line 2594138
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_6

    .line 2594139
    :cond_d
    const-string v10, "share_image"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_e

    .line 2594140
    const/4 v9, 0x0

    .line 2594141
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v6, v10, :cond_14

    .line 2594142
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2594143
    :goto_7
    move v6, v9

    .line 2594144
    goto :goto_6

    .line 2594145
    :cond_e
    const-string v10, "share_message"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_a

    .line 2594146
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_6

    .line 2594147
    :cond_f
    const/4 v9, 0x4

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 2594148
    invoke-virtual {p1, v5, v8}, LX/186;->b(II)V

    .line 2594149
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v7}, LX/186;->b(II)V

    .line 2594150
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v6}, LX/186;->b(II)V

    .line 2594151
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v2}, LX/186;->b(II)V

    .line 2594152
    invoke-virtual {p1}, LX/186;->d()I

    move-result v5

    goto/16 :goto_5

    :cond_10
    move v2, v5

    move v6, v5

    move v7, v5

    move v8, v5

    goto :goto_6

    .line 2594153
    :cond_11
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2594154
    :cond_12
    :goto_8
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_13

    .line 2594155
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 2594156
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2594157
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_12

    if-eqz v10, :cond_12

    .line 2594158
    const-string v11, "uri"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_11

    .line 2594159
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_8

    .line 2594160
    :cond_13
    const/4 v10, 0x1

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 2594161
    invoke-virtual {p1, v9, v6}, LX/186;->b(II)V

    .line 2594162
    invoke-virtual {p1}, LX/186;->d()I

    move-result v9

    goto :goto_7

    :cond_14
    move v6, v9

    goto :goto_8
.end method
