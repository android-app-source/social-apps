.class public LX/IlP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/IlO;


# instance fields
.field public final a:LX/Ild;

.field public final b:LX/Ilg;

.field public final c:LX/IlQ;

.field public d:LX/IlO;


# direct methods
.method public constructor <init>(LX/Ild;LX/Ilg;LX/IlQ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2607726
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2607727
    iput-object p1, p0, LX/IlP;->a:LX/Ild;

    .line 2607728
    iput-object p2, p0, LX/IlP;->b:LX/Ilg;

    .line 2607729
    iput-object p3, p0, LX/IlP;->c:LX/IlQ;

    .line 2607730
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Landroid/view/View;Landroid/view/ViewGroup;)LX/IlT;
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2607731
    instance-of v0, p1, Lcom/facebook/payments/p2p/model/PaymentTransaction;

    if-nez v0, :cond_0

    instance-of v0, p1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;

    if-eqz v0, :cond_3

    :cond_0
    move v0, v2

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2607732
    instance-of v0, p1, Lcom/facebook/payments/p2p/model/PaymentTransaction;

    if-eqz v0, :cond_4

    move-object v0, p1

    check-cast v0, Lcom/facebook/payments/p2p/model/PaymentTransaction;

    .line 2607733
    iget-object v3, v0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->n:Lcom/facebook/payments/p2p/model/CommerceOrder;

    move-object v0, v3

    .line 2607734
    if-eqz v0, :cond_4

    .line 2607735
    iget-object v0, p0, LX/IlP;->c:LX/IlQ;

    iput-object v0, p0, LX/IlP;->d:LX/IlO;

    .line 2607736
    :goto_1
    iget-object v0, p0, LX/IlP;->d:LX/IlO;

    invoke-interface {v0, p1, p2, p3}, LX/IlO;->a(Ljava/lang/Object;Landroid/view/View;Landroid/view/ViewGroup;)LX/IlT;

    move-result-object v0

    .line 2607737
    instance-of v3, v0, Lcom/facebook/messaging/payment/prefs/transactions/UserMessengerPayHistoryItemView;

    if-nez v3, :cond_1

    instance-of v3, v0, Lcom/facebook/messaging/payment/prefs/transactions/ProductMessengerPayHistoryItemView;

    if-eqz v3, :cond_2

    :cond_1
    move v1, v2

    :cond_2
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 2607738
    return-object v0

    :cond_3
    move v0, v1

    .line 2607739
    goto :goto_0

    .line 2607740
    :cond_4
    instance-of v0, p1, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;

    if-eqz v0, :cond_5

    .line 2607741
    iget-object v0, p0, LX/IlP;->b:LX/Ilg;

    iput-object v0, p0, LX/IlP;->d:LX/IlO;

    goto :goto_1

    .line 2607742
    :cond_5
    iget-object v0, p0, LX/IlP;->a:LX/Ild;

    iput-object v0, p0, LX/IlP;->d:LX/IlO;

    goto :goto_1
.end method
