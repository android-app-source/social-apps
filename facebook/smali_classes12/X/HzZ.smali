.class public final LX/HzZ;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$FetchAllUpcomingEventsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Hxg;

.field public final synthetic b:J

.field public final synthetic c:I

.field public final synthetic d:LX/Hze;


# direct methods
.method public constructor <init>(LX/Hze;LX/Hxg;JI)V
    .locals 1

    .prologue
    .line 2525733
    iput-object p1, p0, LX/HzZ;->d:LX/Hze;

    iput-object p2, p0, LX/HzZ;->a:LX/Hxg;

    iput-wide p3, p0, LX/HzZ;->b:J

    iput p5, p0, LX/HzZ;->c:I

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2525734
    iget-object v0, p0, LX/HzZ;->a:LX/Hxg;

    invoke-interface {v0}, LX/Hxg;->a()V

    .line 2525735
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 2525736
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x0

    .line 2525737
    iget-object v0, p0, LX/HzZ;->d:LX/Hze;

    iget-object v0, v0, LX/Hze;->d:LX/HyO;

    sget-object v2, LX/HyN;->NETWORK_FETCH:LX/HyN;

    invoke-virtual {v0, v2}, LX/HyO;->b(LX/HyN;)V

    .line 2525738
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2525739
    if-nez v0, :cond_1

    .line 2525740
    iget-object v0, p0, LX/HzZ;->a:LX/Hxg;

    invoke-interface {v0}, LX/Hxg;->a()V

    .line 2525741
    :cond_0
    :goto_0
    return-void

    .line 2525742
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2525743
    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchAllUpcomingEventsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchAllUpcomingEventsQueryModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$FetchAllUpcomingEventsQueryModel$EventsModel;

    move-result-object v2

    .line 2525744
    if-eqz v2, :cond_0

    .line 2525745
    iget-object v0, p0, LX/HzZ;->d:LX/Hze;

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchAllUpcomingEventsQueryModel$EventsModel;->a()LX/0Px;

    move-result-object v3

    invoke-static {v0, v3}, LX/Hze;->a$redex0(LX/Hze;LX/0Px;)V

    .line 2525746
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchAllUpcomingEventsQueryModel$EventsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2525747
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchAllUpcomingEventsQueryModel$EventsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchAllUpcomingEventsQueryModel$EventsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7oa;

    invoke-static {v0}, LX/Bm1;->b(LX/7oa;)Lcom/facebook/events/model/Event;

    move-result-object v0

    .line 2525748
    invoke-virtual {v0}, Lcom/facebook/events/model/Event;->M()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 2525749
    :goto_1
    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchAllUpcomingEventsQueryModel$EventsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 2525750
    iget-object v1, p0, LX/HzZ;->d:LX/Hze;

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchAllUpcomingEventsQueryModel$EventsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;->a()Ljava/lang/String;

    move-result-object v3

    .line 2525751
    iput-object v3, v1, LX/Hze;->j:Ljava/lang/String;

    .line 2525752
    iget-object v1, p0, LX/HzZ;->d:LX/Hze;

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchAllUpcomingEventsQueryModel$EventsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;->b()Z

    move-result v3

    .line 2525753
    iput-boolean v3, v1, LX/Hze;->l:Z

    .line 2525754
    :goto_2
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iget-wide v6, p0, LX/HzZ;->b:J

    cmp-long v1, v4, v6

    if-gez v1, :cond_3

    .line 2525755
    iget-object v0, p0, LX/HzZ;->d:LX/Hze;

    iget v1, p0, LX/HzZ;->c:I

    iget-object v2, p0, LX/HzZ;->a:LX/Hxg;

    invoke-virtual {v0, v1, v2}, LX/Hze;->b(ILX/Hxg;)V

    goto :goto_0

    .line 2525756
    :cond_2
    iget-object v3, p0, LX/HzZ;->d:LX/Hze;

    .line 2525757
    iput-object v1, v3, LX/Hze;->j:Ljava/lang/String;

    .line 2525758
    iget-object v1, p0, LX/HzZ;->d:LX/Hze;

    const/4 v3, 0x0

    .line 2525759
    iput-boolean v3, v1, LX/Hze;->l:Z

    .line 2525760
    goto :goto_2

    .line 2525761
    :cond_3
    iget-object v1, p0, LX/HzZ;->a:LX/Hxg;

    if-eqz v1, :cond_0

    .line 2525762
    iget-object v1, p0, LX/HzZ;->a:LX/Hxg;

    iget-object v3, p0, LX/HzZ;->d:LX/Hze;

    iget-boolean v3, v3, LX/Hze;->l:Z

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$FetchAllUpcomingEventsQueryModel$EventsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    invoke-interface {v1, v3, v2, v0}, LX/Hxg;->a(ZILjava/lang/Long;)V

    goto/16 :goto_0

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method
