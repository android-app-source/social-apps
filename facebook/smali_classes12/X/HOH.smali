.class public final LX/HOH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/HOK;

.field public final synthetic b:LX/HOI;


# direct methods
.method public constructor <init>(LX/HOI;LX/HOK;)V
    .locals 0

    .prologue
    .line 2459934
    iput-object p1, p0, LX/HOH;->b:LX/HOI;

    iput-object p2, p0, LX/HOH;->a:LX/HOK;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 13

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x175da994

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2459935
    iget-object v1, p0, LX/HOH;->b:LX/HOI;

    iget-object v1, v1, LX/HOI;->l:LX/HON;

    iget-object v1, v1, LX/HON;->f:LX/HOG;

    iget-object v2, p0, LX/HOH;->a:LX/HOK;

    .line 2459936
    iget-object p0, v2, LX/HOK;->c:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;

    move-object v2, p0

    .line 2459937
    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v4

    const v5, -0x7ceb8589

    if-ne v4, v5, :cond_1

    .line 2459938
    iget-object v4, v1, LX/HOG;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;

    .line 2459939
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 2459940
    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->c()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->c()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->k()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$FormFieldsModel;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 2459941
    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->c()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->k()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$FormFieldsModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel$FormFieldsModel;->a()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    .line 2459942
    const/4 v6, 0x0

    move v7, v6

    :goto_0
    if-ge v7, v10, :cond_0

    invoke-virtual {v9, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionConfigFieldsModel;

    .line 2459943
    check-cast v6, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionConfigFieldsModel;

    invoke-virtual {v8, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2459944
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_0

    .line 2459945
    :cond_0
    iget-object v6, v4, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;->h:LX/CYE;

    if-eqz v6, :cond_2

    iget-object v6, v4, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;->h:LX/CYE;

    .line 2459946
    iget-boolean v7, v6, LX/CYE;->mUseActionFlow:Z

    move v6, v7

    .line 2459947
    if-eqz v6, :cond_2

    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->gB_()Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    move-result-object v6

    check-cast v6, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    move-object v8, v6

    .line 2459948
    :goto_1
    invoke-virtual {v4}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v12

    iget-object v6, v4, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;->j:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;

    iget-object v7, v4, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;->i:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminInfoDataModel;

    iget-wide v10, v4, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;->g:J

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    iget-object v11, v4, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;->h:LX/CYE;

    invoke-static/range {v6 .. v11}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->a(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminInfoDataModel;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;Ljava/lang/String;Ljava/lang/String;LX/CYE;)Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    move-result-object v6

    const-string v7, "select_to_configure_cta_tag"

    invoke-static {v12, v4, v6, v7}, LX/CYR;->a(LX/0gc;Lcom/facebook/base/fragment/FbFragment;Lcom/facebook/base/fragment/FbFragment;Ljava/lang/String;)V

    .line 2459949
    :goto_2
    const v1, -0x58ac5402

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2459950
    :cond_1
    iget-object v4, v1, LX/HOG;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;

    .line 2459951
    invoke-virtual {v4}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v6

    iget-wide v8, v4, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;->g:J

    iget-object v7, v4, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;->h:LX/CYE;

    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    move-result-object v10

    sget-object v11, LX/CYF;->CREATE_ACTION:LX/CYF;

    invoke-static {v8, v9, v7, v10, v11}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;->a(JLX/CYE;Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;LX/CYF;)Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesEditActionFragment;

    move-result-object v7

    invoke-static {v6, v4, v7}, LX/CYR;->a(LX/0gc;Lcom/facebook/base/fragment/FbFragment;Lcom/facebook/base/fragment/FbFragment;)V

    .line 2459952
    goto :goto_2

    .line 2459953
    :cond_2
    iget-object v6, v4, Lcom/facebook/pages/common/surface/calltoaction/fragment/PagesSelectActionFragment;->j:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;

    invoke-static {v6}, LX/CYR;->d(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$PageCallToActionAdminDataModel;)Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    move-result-object v8

    goto :goto_1
.end method
