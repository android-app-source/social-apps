.class public LX/JRU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:LX/JRT;

.field private final b:LX/JRW;

.field private final c:LX/3mL;

.field private final d:LX/3ho;


# direct methods
.method public constructor <init>(LX/JRT;LX/JRW;LX/3mL;LX/3ho;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2693414
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2693415
    iput-object p1, p0, LX/JRU;->a:LX/JRT;

    .line 2693416
    iput-object p2, p0, LX/JRU;->b:LX/JRW;

    .line 2693417
    iput-object p3, p0, LX/JRU;->c:LX/3mL;

    .line 2693418
    iput-object p4, p0, LX/JRU;->d:LX/3ho;

    .line 2693419
    return-void
.end method

.method public static a(LX/0QB;)LX/JRU;
    .locals 7

    .prologue
    .line 2693391
    const-class v1, LX/JRU;

    monitor-enter v1

    .line 2693392
    :try_start_0
    sget-object v0, LX/JRU;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2693393
    sput-object v2, LX/JRU;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2693394
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2693395
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2693396
    new-instance p0, LX/JRU;

    const-class v3, LX/JRT;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/JRT;

    invoke-static {v0}, LX/JRW;->a(LX/0QB;)LX/JRW;

    move-result-object v4

    check-cast v4, LX/JRW;

    invoke-static {v0}, LX/3mL;->a(LX/0QB;)LX/3mL;

    move-result-object v5

    check-cast v5, LX/3mL;

    invoke-static {v0}, LX/3ho;->a(LX/0QB;)LX/3ho;

    move-result-object v6

    check-cast v6, LX/3ho;

    invoke-direct {p0, v3, v4, v5, v6}, LX/JRU;-><init>(LX/JRT;LX/JRW;LX/3mL;LX/3ho;)V

    .line 2693397
    move-object v0, p0

    .line 2693398
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2693399
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JRU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2693400
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2693401
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1Pm;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1Dg;
    .locals 15
    .param p2    # LX/1Pm;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/1Pm;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 2693402
    invoke-static/range {p3 .. p3}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    .line 2693403
    new-instance v11, LX/JRV;

    iget-object v1, p0, LX/JRU;->d:LX/3ho;

    invoke-direct {v11, v1}, LX/JRV;-><init>(LX/3ho;)V

    .line 2693404
    invoke-virtual/range {p3 .. p3}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MULTI_SHARE_NO_END_CARD:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v1, v3}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;)Z

    move-result v1

    .line 2693405
    invoke-static {}, LX/3mP;->newBuilder()LX/3mP;

    move-result-object v3

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v3, v1}, LX/3mP;->a(Z)LX/3mP;

    move-result-object v1

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/25L;->a(Ljava/lang/String;)LX/25L;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/3mP;->a(LX/25L;)LX/3mP;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/3mP;->a(LX/0jW;)LX/3mP;

    move-result-object v1

    iget-object v2, p0, LX/JRU;->b:LX/JRW;

    invoke-virtual {v1, v2}, LX/3mP;->a(LX/25K;)LX/3mP;

    move-result-object v1

    invoke-virtual {v1}, LX/3mP;->a()LX/25M;

    move-result-object v12

    .line 2693406
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 2693407
    invoke-virtual/range {p3 .. p3}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v14

    .line 2693408
    const/4 v3, 0x0

    :goto_1
    invoke-virtual {v14}, LX/0Px;->size()I

    move-result v1

    if-ge v3, v1, :cond_1

    .line 2693409
    new-instance v1, LX/3hq;

    invoke-virtual {v14, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/flatbuffers/Flattenable;

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    add-int/lit8 v4, v3, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-direct/range {v1 .. v10}, LX/3hq;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;IIZZZFFF)V

    invoke-interface {v13, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2693410
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 2693411
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 2693412
    :cond_1
    iget-object v1, p0, LX/JRU;->a:LX/JRT;

    invoke-static {v13}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    move-object/from16 v2, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object v6, v12

    move-object v7, v11

    invoke-virtual/range {v1 .. v7}, LX/JRT;->a(Landroid/content/Context;LX/0Px;LX/1Pm;Lcom/facebook/feed/rows/core/props/FeedProps;LX/25M;LX/JRV;)LX/JRS;

    move-result-object v1

    .line 2693413
    iget-object v2, p0, LX/JRU;->c:LX/3mL;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, LX/3mL;->c(LX/1De;)LX/3ml;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/3ml;->a(LX/3mX;)LX/3ml;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/16 v2, 0x8

    const/4 v3, 0x5

    invoke-interface {v1, v2, v3}, LX/1Di;->h(II)LX/1Di;

    move-result-object v1

    const v2, 0x7f0a00d5

    invoke-interface {v1, v2}, LX/1Di;->x(I)LX/1Di;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    return-object v1
.end method
