.class public final LX/HmB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2500330
    iput-object p1, p0, LX/HmB;->c:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    iput-object p2, p0, LX/HmB;->a:Ljava/lang/String;

    iput-object p3, p0, LX/HmB;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()Ljava/lang/Integer;
    .locals 6

    .prologue
    .line 2500331
    iget-object v0, p0, LX/HmB;->c:Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    invoke-virtual {v0}, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, LX/HmB;->a:Ljava/lang/String;

    iget-object v2, p0, LX/HmB;->b:Ljava/lang/String;

    const/4 p0, 0x1

    .line 2500332
    const-string v3, "wifi"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/wifi/WifiManager;

    .line 2500333
    invoke-virtual {v3}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v4

    if-nez v4, :cond_0

    .line 2500334
    invoke-virtual {v3, p0}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    move-result v4

    .line 2500335
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 2500336
    :cond_0
    new-instance v4, Landroid/net/wifi/WifiConfiguration;

    invoke-direct {v4}, Landroid/net/wifi/WifiConfiguration;-><init>()V

    .line 2500337
    invoke-static {v1}, LX/HnN;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    .line 2500338
    iget-object v5, v4, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v5, p0}, Ljava/util/BitSet;->set(I)V

    .line 2500339
    invoke-static {v2}, LX/HnN;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    .line 2500340
    const/4 v5, 0x2

    iput v5, v4, Landroid/net/wifi/WifiConfiguration;->status:I

    .line 2500341
    invoke-static {v0, v3, v4}, LX/HnN;->a(Landroid/content/Context;Landroid/net/wifi/WifiManager;Landroid/net/wifi/WifiConfiguration;)I

    move-result v3

    move v0, v3

    .line 2500342
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2500343
    invoke-direct {p0}, LX/HmB;->a()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
