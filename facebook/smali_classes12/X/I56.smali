.class public final LX/I56;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;)V
    .locals 0

    .prologue
    .line 2535054
    iput-object p1, p0, LX/I56;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 2535055
    iget-object v0, p0, LX/I56;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;

    iget-object v0, v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->h:LX/H4G;

    invoke-virtual {v0, p3}, LX/H4G;->getItemViewType(I)I

    move-result v0

    .line 2535056
    invoke-static {}, LX/H4E;->values()[LX/H4E;

    move-result-object v1

    aget-object v0, v1, v0

    .line 2535057
    iget-object v1, p0, LX/I56;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;

    iget-object v1, v1, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->k:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    iget-object v1, v1, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->a:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    .line 2535058
    sget-object v2, LX/H4E;->CURRENT_LOCATION_CELL:LX/H4E;

    if-ne v0, v2, :cond_2

    .line 2535059
    iput-boolean v4, v1, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->b:Z

    .line 2535060
    iget-object v0, p0, LX/I56;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;

    iget-object v0, v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->j:Landroid/location/Location;

    if-eqz v0, :cond_0

    .line 2535061
    iget-object v0, p0, LX/I56;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;

    iget-object v0, v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->j:Landroid/location/Location;

    invoke-virtual {v1, v0}, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->a(Landroid/location/Location;)V

    .line 2535062
    iput-boolean v4, v1, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->b:Z

    .line 2535063
    :cond_0
    iput-object v3, v1, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->d:Ljava/lang/String;

    .line 2535064
    iput-object v3, v1, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->e:Ljava/lang/String;

    .line 2535065
    :cond_1
    :goto_0
    iget-object v0, p0, LX/I56;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;

    iget-object v1, p0, LX/I56;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;

    iget-object v1, v1, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->k:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    iget-object v1, v1, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->a:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    sget-object v2, LX/H4F;->SEARCH_RADIUS_5:LX/H4F;

    invoke-static {v0, v1, v2}, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->a$redex0(Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;LX/H4F;)V

    .line 2535066
    return-void

    .line 2535067
    :cond_2
    sget-object v2, LX/H4E;->LOCATION_CELL:LX/H4E;

    if-ne v0, v2, :cond_1

    .line 2535068
    iget-object v0, p0, LX/I56;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;

    iget-object v0, v0, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->h:LX/H4G;

    invoke-virtual {v0, p3}, LX/H4G;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesTypeaheadLocationResultsConnectionFragmentModel$EdgesModel$NodeModel;

    .line 2535069
    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesTypeaheadLocationResultsConnectionFragmentModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v2

    .line 2535070
    iput-object v2, v1, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->d:Ljava/lang/String;

    .line 2535071
    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesTypeaheadLocationResultsConnectionFragmentModel$EdgesModel$NodeModel;->l()Ljava/lang/String;

    move-result-object v2

    .line 2535072
    iput-object v2, v1, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->e:Ljava/lang/String;

    .line 2535073
    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesTypeaheadLocationResultsConnectionFragmentModel$EdgesModel$NodeModel;->k()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    invoke-virtual {v3, v2, v5}, LX/15i;->l(II)D

    move-result-wide v2

    .line 2535074
    iput-wide v2, v1, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->f:D

    .line 2535075
    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesTypeaheadLocationResultsConnectionFragmentModel$EdgesModel$NodeModel;->k()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    invoke-virtual {v3, v2, v4}, LX/15i;->l(II)D

    move-result-wide v2

    .line 2535076
    iput-wide v2, v1, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->g:D

    .line 2535077
    iput-boolean v5, v1, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->b:Z

    .line 2535078
    iget-object v1, p0, LX/I56;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;

    iget-object v1, v1, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->b:LX/1nQ;

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesTypeaheadLocationResultsConnectionFragmentModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, LX/I56;->a:Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;

    iget-object v2, v2, Lcom/facebook/events/eventsdiscovery/EventsDiscoveryLocationPickerFragment;->l:Ljava/lang/String;

    .line 2535079
    iget-object v3, v1, LX/1nQ;->i:LX/0Zb;

    const-string v4, "selected_location_filter_location"

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v3

    .line 2535080
    invoke-virtual {v3}, LX/0oG;->a()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2535081
    const-string v4, "event_discovery"

    invoke-virtual {v3, v4}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v3

    const-string v4, "page_id"

    invoke-virtual {v3, v4, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v3

    const-string v4, "event_suggestion_token"

    invoke-virtual {v3, v4, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v3

    invoke-virtual {v3}, LX/0oG;->d()V

    .line 2535082
    :cond_3
    goto :goto_0
.end method
