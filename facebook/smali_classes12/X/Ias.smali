.class public final LX/Ias;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/IZu;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;)V
    .locals 0

    .prologue
    .line 2591434
    iput-object p1, p0, LX/Ias;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2591430
    iget-object v0, p0, LX/Ias;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->a$redex0(Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;Z)V

    .line 2591431
    iget-object v0, p0, LX/Ias;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;

    iget-object v0, v0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->c:LX/IZK;

    invoke-virtual {v0}, LX/IZK;->a()V

    .line 2591432
    iget-object v0, p0, LX/Ias;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;

    iget-object v0, v0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->e:LX/IZw;

    const-string v1, "error_load_new_user_signup"

    const-string v2, "BusinessSignUpFragment"

    invoke-virtual {v0, v1, v2}, LX/IZw;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2591433
    return-void
.end method

.method public final a(Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;)V
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "onSuccess"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2591424
    iget-object v0, p0, LX/Ias;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;

    .line 2591425
    iput-object p1, v0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->v:Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;

    .line 2591426
    iget-object v0, p0, LX/Ias;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;

    invoke-static {v0, p1}, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->b$redex0(Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;Lcom/facebook/messaging/business/nativesignup/graphql/NativeSignUpQueryModels$NativeSignUpNewUserSignUpQueryModel$MessengerCommerceModel$NewUserSignupModel;)V

    .line 2591427
    iget-object v0, p0, LX/Ias;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->a$redex0(Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;Z)V

    .line 2591428
    iget-object v0, p0, LX/Ias;->a:Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;

    iget-object v0, v0, Lcom/facebook/messaging/business/nativesignup/view/BusinessSignUpFragment;->e:LX/IZw;

    const-string v1, "success_load_new_user_signup"

    const-string v2, "BusinessSignUpFragment"

    invoke-virtual {v0, v1, v2}, LX/IZw;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2591429
    return-void
.end method
