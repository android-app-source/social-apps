.class public final LX/I6R;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/feed/protocol/EventsPinnedPostMutationsModels$EventChangeStoryPinStatusMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:Landroid/content/Context;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/I6T;


# direct methods
.method public constructor <init>(LX/I6T;Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2537535
    iput-object p1, p0, LX/I6R;->d:LX/I6T;

    iput-object p2, p0, LX/I6R;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object p3, p0, LX/I6R;->b:Landroid/content/Context;

    iput-object p4, p0, LX/I6R;->c:Ljava/lang/String;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2537536
    iget-object v0, p0, LX/I6R;->d:LX/I6T;

    iget-object v0, v0, LX/I6T;->e:LX/03V;

    const-class v1, LX/I6T;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Failed to change story pin status"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2537537
    new-instance v1, LX/I6Z;

    iget-object v0, p0, LX/I6R;->b:Landroid/content/Context;

    invoke-direct {v1, v0}, LX/I6Z;-><init>(Landroid/content/Context;)V

    .line 2537538
    iget-object v0, p0, LX/I6R;->c:Ljava/lang/String;

    const-string v2, "PINNED"

    if-ne v0, v2, :cond_0

    const v0, 0x7f083800

    .line 2537539
    :goto_0
    iget-object v2, v1, LX/I6Z;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2537540
    iget-object v0, p0, LX/I6R;->d:LX/I6T;

    iget-object v0, v0, LX/I6T;->f:LX/2hU;

    const/16 v2, 0xfa0

    invoke-virtual {v0, v1, v2}, LX/2hU;->a(Landroid/view/View;I)LX/4nS;

    move-result-object v0

    .line 2537541
    new-instance v2, LX/I6Q;

    invoke-direct {v2, p0, v0}, LX/I6Q;-><init>(LX/I6R;LX/4nS;)V

    .line 2537542
    iget-object p0, v1, LX/I6Z;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0, v2}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2537543
    invoke-virtual {v0}, LX/4nS;->a()V

    .line 2537544
    return-void

    .line 2537545
    :cond_0
    const v0, 0x7f083801

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 2537546
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2537547
    iget-object v0, p0, LX/I6R;->d:LX/I6T;

    iget-object v0, v0, LX/I6T;->h:Lcom/facebook/events/permalink/EventPermalinkFragment;

    if-nez v0, :cond_1

    .line 2537548
    :cond_0
    :goto_0
    return-void

    .line 2537549
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2537550
    check-cast v0, Lcom/facebook/events/feed/protocol/EventsPinnedPostMutationsModels$EventChangeStoryPinStatusMutationModel;

    .line 2537551
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/events/feed/protocol/EventsPinnedPostMutationsModels$EventChangeStoryPinStatusMutationModel;->a()Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2537552
    invoke-virtual {v0}, Lcom/facebook/events/feed/protocol/EventsPinnedPostMutationsModels$EventChangeStoryPinStatusMutationModel;->a()Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel;

    move-result-object v1

    .line 2537553
    iget-object v2, p0, LX/I6R;->d:LX/I6T;

    iget-object v2, v2, LX/I6T;->h:Lcom/facebook/events/permalink/EventPermalinkFragment;

    iget-object v3, p0, LX/I6R;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/events/feed/protocol/EventsPinnedPostMutationsModels$EventChangeStoryPinStatusMutationModel;->j()Ljava/lang/String;

    move-result-object v0

    .line 2537554
    iget-object v4, v2, Lcom/facebook/events/permalink/EventPermalinkFragment;->G:LX/0fz;

    invoke-virtual {v4, v3}, LX/0fz;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    .line 2537555
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2537556
    :cond_2
    iget-object v0, p0, LX/I6R;->d:LX/I6T;

    iget-object v0, v0, LX/I6T;->h:Lcom/facebook/events/permalink/EventPermalinkFragment;

    iget-object v2, p0, LX/I6R;->d:LX/I6T;

    iget-object v2, v2, LX/I6T;->i:LX/I6W;

    invoke-virtual {v2, v1}, LX/I6W;->a(Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel;)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v2

    iget-object v3, p0, LX/I6R;->d:LX/I6T;

    iget-object v3, v3, LX/I6T;->i:LX/I6W;

    invoke-virtual {v3, v1}, LX/I6W;->b(Lcom/facebook/events/feed/protocol/EventPinnedPostAndRecentStoryGraphQLModels$EventPinnedPostAndRecentStoryFragmentModel;)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/facebook/events/permalink/EventPermalinkFragment;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)V

    goto :goto_0

    .line 2537557
    :cond_3
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 2537558
    check-cast v4, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 2537559
    invoke-static {v4}, LX/1u8;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)LX/1u8;

    move-result-object v6

    .line 2537560
    iput-object v0, v6, LX/1u8;->i:Ljava/lang/String;

    .line 2537561
    move-object v6, v6

    .line 2537562
    invoke-virtual {v6}, LX/1u8;->a()Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v6

    .line 2537563
    iget-object p1, v2, Lcom/facebook/events/permalink/EventPermalinkFragment;->G:LX/0fz;

    invoke-virtual {p1, v4}, LX/0fz;->b(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)V

    .line 2537564
    iget-object v4, v2, Lcom/facebook/events/permalink/EventPermalinkFragment;->G:LX/0fz;

    invoke-virtual {v4, v6}, LX/0fz;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)V

    goto :goto_1
.end method
