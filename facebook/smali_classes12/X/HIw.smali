.class public abstract LX/HIw;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:F

.field public final b:Landroid/widget/ImageView$ScaleType;


# direct methods
.method public constructor <init>(FLandroid/widget/ImageView$ScaleType;)V
    .locals 0

    .prologue
    .line 2451933
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2451934
    iput p1, p0, LX/HIw;->a:F

    .line 2451935
    iput-object p2, p0, LX/HIw;->b:Landroid/widget/ImageView$ScaleType;

    .line 2451936
    return-void
.end method


# virtual methods
.method public abstract a(Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V
.end method

.method public final b(Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V
    .locals 2

    .prologue
    .line 2451937
    invoke-virtual {p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 2451938
    if-eqz v0, :cond_0

    .line 2451939
    const/4 v1, -0x2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2451940
    invoke-virtual {p1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2451941
    :cond_0
    iget v0, p0, LX/HIw;->a:F

    invoke-virtual {p1, v0}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 2451942
    return-void
.end method

.method public final c(Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V
    .locals 1

    .prologue
    .line 2451943
    iget-object v0, p0, LX/HIw;->b:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 2451944
    return-void
.end method
