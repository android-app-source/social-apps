.class public LX/IVJ;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/IVH;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/IVL;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2581740
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/IVJ;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/IVL;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2581716
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2581717
    iput-object p1, p0, LX/IVJ;->b:LX/0Ot;

    .line 2581718
    return-void
.end method

.method public static a(LX/0QB;)LX/IVJ;
    .locals 4

    .prologue
    .line 2581729
    const-class v1, LX/IVJ;

    monitor-enter v1

    .line 2581730
    :try_start_0
    sget-object v0, LX/IVJ;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2581731
    sput-object v2, LX/IVJ;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2581732
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2581733
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2581734
    new-instance v3, LX/IVJ;

    const/16 p0, 0x2427

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/IVJ;-><init>(LX/0Ot;)V

    .line 2581735
    move-object v0, v3

    .line 2581736
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2581737
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IVJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2581738
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2581739
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2581727
    invoke-static {}, LX/1dS;->b()V

    .line 2581728
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2581741
    iget-object v0, p0, LX/IVJ;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 2581742
    new-instance v0, Lcom/facebook/fig/footer/FigFooter;

    invoke-direct {v0, p1}, Lcom/facebook/fig/footer/FigFooter;-><init>(Landroid/content/Context;)V

    move-object v0, v0

    .line 2581743
    return-object v0
.end method

.method public final e(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 2

    .prologue
    .line 2581721
    check-cast p3, LX/IVI;

    .line 2581722
    iget-object v0, p0, LX/IVJ;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IVL;

    check-cast p2, Lcom/facebook/fig/footer/FigFooter;

    iget-object v1, p3, LX/IVI;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2581723
    const p0, 0x7f082970

    invoke-virtual {p2, p0}, Lcom/facebook/fig/footer/FigFooter;->setTitleText(I)V

    .line 2581724
    const/16 p0, 0x11

    invoke-virtual {p2, p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setGravity(I)V

    .line 2581725
    new-instance p0, LX/IVK;

    invoke-direct {p0, v0, v1, p1}, LX/IVK;-><init>(LX/IVL;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1De;)V

    invoke-virtual {p2, p0}, Lcom/facebook/fig/footer/FigFooter;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2581726
    return-void
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 2581720
    sget-object v0, LX/1mv;->VIEW:LX/1mv;

    return-object v0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 2581719
    const/16 v0, 0xf

    return v0
.end method
