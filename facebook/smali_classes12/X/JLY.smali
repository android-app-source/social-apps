.class public LX/JLY;
.super LX/5pb;
.source ""

# interfaces
.implements LX/5on;


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "FBMarketplaceImagePickerModule"
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lcom/facebook/react/bridge/Callback;

.field private c:Lcom/facebook/react/bridge/Callback;


# direct methods
.method public constructor <init>(LX/5pY;)V
    .locals 0
    .param p1    # LX/5pY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2681256
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 2681257
    invoke-virtual {p1, p0}, LX/5pX;->a(LX/5on;)V

    .line 2681258
    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2681259
    const-string v0, "extra_media_items"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2681260
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2681261
    :cond_0
    iget-object v0, p0, LX/JLY;->c:Lcom/facebook/react/bridge/Callback;

    if-eqz v0, :cond_1

    .line 2681262
    iget-object v0, p0, LX/JLY;->c:Lcom/facebook/react/bridge/Callback;

    new-array v1, v4, [Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    .line 2681263
    :cond_1
    :goto_0
    return-void

    .line 2681264
    :cond_2
    iget-object v1, p0, LX/JLY;->b:Lcom/facebook/react/bridge/Callback;

    if-eqz v1, :cond_1

    .line 2681265
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 2681266
    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v1

    .line 2681267
    iget-object v2, p0, LX/JLY;->b:Lcom/facebook/react/bridge/Callback;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    .line 2681268
    iget v4, v1, Lcom/facebook/ipc/media/data/MediaData;->mHeight:I

    move v4, v4

    .line 2681269
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x2

    .line 2681270
    iget v4, v1, Lcom/facebook/ipc/media/data/MediaData;->mWidth:I

    move v1, v4

    .line 2681271
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v0

    invoke-interface {v2, v3}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private h()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2681272
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2681273
    new-instance v1, LX/8AA;

    sget-object v2, LX/8AB;->MARKETPLACE:LX/8AB;

    invoke-direct {v1, v2}, LX/8AA;-><init>(LX/8AB;)V

    invoke-virtual {v1}, LX/8AA;->l()LX/8AA;

    move-result-object v1

    invoke-virtual {v1}, LX/8AA;->o()LX/8AA;

    move-result-object v1

    invoke-virtual {v1}, LX/8AA;->d()LX/8AA;

    move-result-object v1

    invoke-virtual {v1}, LX/8AA;->j()LX/8AA;

    move-result-object v1

    invoke-virtual {v1}, LX/8AA;->a()LX/8AA;

    move-result-object v1

    sget-object v2, LX/8A9;->NONE:LX/8A9;

    invoke-virtual {v1, v2}, LX/8AA;->a(LX/8A9;)LX/8AA;

    move-result-object v1

    invoke-virtual {v1, v3, v3}, LX/8AA;->b(II)LX/8AA;

    move-result-object v1

    .line 2681274
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2681275
    invoke-virtual {v1, v2}, LX/8AA;->a(LX/0Px;)LX/8AA;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->a(Landroid/content/Context;LX/8AA;)Landroid/content/Intent;

    move-result-object v0

    .line 2681276
    iget-object v1, p0, LX/5pb;->a:LX/5pY;

    move-object v1, v1

    .line 2681277
    const/16 v2, 0x2715

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, LX/5pX;->a(Landroid/content/Intent;ILandroid/os/Bundle;)Z

    .line 2681278
    return-void
.end method


# virtual methods
.method public final a(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 2681279
    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    .line 2681280
    iget-object v0, p0, LX/JLY;->c:Lcom/facebook/react/bridge/Callback;

    if-eqz v0, :cond_0

    .line 2681281
    iget-object v0, p0, LX/JLY;->c:Lcom/facebook/react/bridge/Callback;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    .line 2681282
    :cond_0
    :goto_0
    return-void

    .line 2681283
    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 2681284
    :pswitch_0
    invoke-direct {p0, p3}, LX/JLY;->a(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2715
        :pswitch_0
    .end packed-switch
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2681285
    const-string v0, "FBMarketplaceImagePickerModule"

    return-object v0
.end method

.method public openSelectDialog(LX/5pG;Ljava/lang/String;Lcom/facebook/react/bridge/Callback;Lcom/facebook/react/bridge/Callback;)V
    .locals 1
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2681286
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2681287
    invoke-virtual {v0}, LX/5pX;->i()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2681288
    :goto_0
    return-void

    .line 2681289
    :cond_0
    iput-object p2, p0, LX/JLY;->a:Ljava/lang/String;

    .line 2681290
    iput-object p3, p0, LX/JLY;->b:Lcom/facebook/react/bridge/Callback;

    .line 2681291
    iput-object p4, p0, LX/JLY;->c:Lcom/facebook/react/bridge/Callback;

    .line 2681292
    invoke-direct {p0}, LX/JLY;->h()V

    goto :goto_0
.end method
