.class public final LX/Hzw;
.super LX/0Vd;
.source ""


# instance fields
.field public final synthetic a:LX/HyL;

.field public final synthetic b:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

.field public final synthetic c:Lcom/facebook/events/model/Event;

.field public final synthetic d:LX/I03;


# direct methods
.method public constructor <init>(LX/I03;LX/HyL;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Lcom/facebook/events/model/Event;)V
    .locals 0

    .prologue
    .line 2526362
    iput-object p1, p0, LX/Hzw;->d:LX/I03;

    iput-object p2, p0, LX/Hzw;->a:LX/HyL;

    iput-object p3, p0, LX/Hzw;->b:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    iput-object p4, p0, LX/Hzw;->c:Lcom/facebook/events/model/Event;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2526363
    iget-object v0, p0, LX/Hzw;->d:LX/I03;

    iget-object v1, p0, LX/Hzw;->a:LX/HyL;

    invoke-static {v0, v1}, LX/I03;->a$redex0(LX/I03;LX/HyL;)V

    .line 2526364
    iget-object v0, p0, LX/Hzw;->b:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->UNWATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne v0, v1, :cond_0

    .line 2526365
    iget-object v0, p0, LX/Hzw;->d:LX/I03;

    iget-object v0, v0, LX/I03;->d:LX/Bl6;

    new-instance v1, LX/BlE;

    iget-object v2, p0, LX/Hzw;->c:Lcom/facebook/events/model/Event;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, LX/BlE;-><init>(Lcom/facebook/events/model/Event;Z)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2526366
    :cond_0
    iget-object v0, p0, LX/Hzw;->d:LX/I03;

    iget-object v0, v0, LX/I03;->f:Landroid/content/ContentResolver;

    iget-object v1, p0, LX/Hzw;->d:LX/I03;

    iget-object v1, v1, LX/I03;->g:LX/Bky;

    iget-object v2, p0, LX/Hzw;->c:Lcom/facebook/events/model/Event;

    iget-object v3, p0, LX/Hzw;->d:LX/I03;

    iget-object v3, v3, LX/I03;->h:LX/0TD;

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/events/data/EventsProvider;->a(Landroid/content/ContentResolver;LX/Bky;Lcom/facebook/events/model/Event;LX/0TD;)V

    .line 2526367
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2526368
    return-void
.end method
