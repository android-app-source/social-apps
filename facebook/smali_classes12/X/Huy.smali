.class public LX/Huy;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/concurrent/ScheduledExecutorService;

.field private final b:LX/03V;

.field public c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/composer/savedsession/ComposerSavedSessionController$SessionProvider;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/concurrent/ScheduledFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ScheduledFuture",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ScheduledExecutorService;LX/03V;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2518160
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2518161
    iput-object v0, p0, LX/Huy;->c:Ljava/lang/ref/WeakReference;

    .line 2518162
    iput-object v0, p0, LX/Huy;->d:Ljava/util/concurrent/ScheduledFuture;

    .line 2518163
    iput-object p1, p0, LX/Huy;->a:Ljava/util/concurrent/ScheduledExecutorService;

    .line 2518164
    iput-object p2, p0, LX/Huy;->b:LX/03V;

    .line 2518165
    return-void
.end method

.method public static c(LX/Huy;)Z
    .locals 1

    .prologue
    .line 2518166
    iget-object v0, p0, LX/Huy;->d:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static f(LX/Huy;)LX/Hrc;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2518167
    iget-object v0, p0, LX/Huy;->c:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2518168
    iget-object v0, p0, LX/Huy;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hrc;

    .line 2518169
    if-nez v0, :cond_0

    .line 2518170
    iget-object v0, p0, LX/Huy;->b:LX/03V;

    const-string v1, "composer_session_provider_collected"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2518171
    const/4 v0, 0x0

    .line 2518172
    :cond_0
    return-object v0

    .line 2518173
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
