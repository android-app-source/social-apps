.class public LX/Hbe;
.super Landroid/text/style/BulletSpan;
.source ""


# static fields
.field private static e:Landroid/graphics/Path;


# instance fields
.field private final a:I

.field private final b:Z

.field private final c:I

.field private final d:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2485999
    const/4 v0, 0x0

    sput-object v0, LX/Hbe;->e:Landroid/graphics/Path;

    return-void
.end method

.method public constructor <init>(II)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2486000
    invoke-direct {p0, p1}, Landroid/text/style/BulletSpan;-><init>(I)V

    .line 2486001
    iput p1, p0, LX/Hbe;->a:I

    .line 2486002
    iput-boolean v0, p0, LX/Hbe;->b:Z

    .line 2486003
    iput v0, p0, LX/Hbe;->c:I

    .line 2486004
    iput p2, p0, LX/Hbe;->d:I

    .line 2486005
    return-void
.end method


# virtual methods
.method public final drawLeadingMargin(Landroid/graphics/Canvas;Landroid/graphics/Paint;IIIIILjava/lang/CharSequence;IIZLandroid/text/Layout;)V
    .locals 8

    .prologue
    .line 2486006
    check-cast p8, Landroid/text/Spanned;

    move-object/from16 v0, p8

    invoke-interface {v0, p0}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v1

    move/from16 v0, p9

    if-ne v1, v0, :cond_3

    .line 2486007
    invoke-virtual {p2}, Landroid/graphics/Paint;->getStyle()Landroid/graphics/Paint$Style;

    move-result-object v2

    .line 2486008
    const/4 v1, 0x0

    .line 2486009
    iget-boolean v3, p0, LX/Hbe;->b:Z

    if-eqz v3, :cond_0

    .line 2486010
    invoke-virtual {p2}, Landroid/graphics/Paint;->getColor()I

    move-result v1

    .line 2486011
    iget v3, p0, LX/Hbe;->c:I

    invoke-virtual {p2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 2486012
    :cond_0
    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2486013
    invoke-virtual {p1}, Landroid/graphics/Canvas;->isHardwareAccelerated()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2486014
    sget-object v3, LX/Hbe;->e:Landroid/graphics/Path;

    if-nez v3, :cond_1

    .line 2486015
    new-instance v3, Landroid/graphics/Path;

    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    .line 2486016
    sput-object v3, LX/Hbe;->e:Landroid/graphics/Path;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const v6, 0x3f99999a    # 1.2f

    iget v7, p0, LX/Hbe;->d:I

    int-to-float v7, v7

    mul-float/2addr v6, v7

    sget-object v7, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 2486017
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 2486018
    iget v3, p0, LX/Hbe;->d:I

    mul-int/2addr v3, p4

    mul-int/lit8 v3, v3, 0x2

    add-int/2addr v3, p3

    int-to-float v3, v3

    add-int v4, p5, p7

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    iget v5, p0, LX/Hbe;->d:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2486019
    sget-object v3, LX/Hbe;->e:Landroid/graphics/Path;

    invoke-virtual {p1, v3, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 2486020
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 2486021
    :goto_0
    iget-boolean v3, p0, LX/Hbe;->b:Z

    if-eqz v3, :cond_2

    .line 2486022
    invoke-virtual {p2, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2486023
    :cond_2
    invoke-virtual {p2, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2486024
    :cond_3
    return-void

    .line 2486025
    :cond_4
    iget v3, p0, LX/Hbe;->d:I

    mul-int/2addr v3, p4

    mul-int/lit8 v3, v3, 0x2

    add-int/2addr v3, p3

    int-to-float v3, v3

    add-int v4, p5, p7

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    iget v5, p0, LX/Hbe;->d:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    iget v5, p0, LX/Hbe;->d:I

    int-to-float v5, v5

    invoke-virtual {p1, v3, v4, v5, p2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public final getLeadingMargin(Z)I
    .locals 2

    .prologue
    .line 2486026
    iget v0, p0, LX/Hbe;->d:I

    mul-int/lit8 v0, v0, 0x2

    iget v1, p0, LX/Hbe;->a:I

    add-int/2addr v0, v1

    return v0
.end method
