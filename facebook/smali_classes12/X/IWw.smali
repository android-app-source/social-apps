.class public LX/IWw;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/widget/ImageView;

.field public final c:Landroid/graphics/Paint;

.field public d:F

.field public e:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 2585322
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2585323
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/IWw;->c:Landroid/graphics/Paint;

    .line 2585324
    const/4 v3, 0x0

    .line 2585325
    const-class v0, LX/IWw;

    invoke-static {v0, p0}, LX/IWw;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2585326
    invoke-virtual {p0, v3}, LX/IWw;->setOrientation(I)V

    .line 2585327
    const v0, 0x7f030800

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2585328
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, LX/IWw;->setVisibility(I)V

    .line 2585329
    iget-object v0, p0, LX/IWw;->a:Landroid/content/res/Resources;

    const v1, 0x7f020c7e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/IWw;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2585330
    const v0, 0x7f0d1514

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/IWw;->b:Landroid/widget/ImageView;

    .line 2585331
    iget-object v0, p0, LX/IWw;->b:Landroid/widget/ImageView;

    .line 2585332
    iget-object v1, p0, LX/IWw;->a:Landroid/content/res/Resources;

    const v2, 0x7f020970

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2585333
    new-instance v2, Landroid/graphics/PorterDuffColorFilter;

    iget-object v4, p0, LX/IWw;->a:Landroid/content/res/Resources;

    const p1, 0x7f0a05f0

    invoke-virtual {v4, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    sget-object p1, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v2, v4, p1}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 2585334
    move-object v1, v1

    .line 2585335
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2585336
    iget-object v0, p0, LX/IWw;->c:Landroid/graphics/Paint;

    iget-object v1, p0, LX/IWw;->a:Landroid/content/res/Resources;

    const v2, 0x7f0a05ed

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2585337
    iget-object v0, p0, LX/IWw;->c:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2585338
    iget-object v0, p0, LX/IWw;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b11e1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, LX/IWw;->d:F

    .line 2585339
    iget-object v0, p0, LX/IWw;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b11e3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, LX/IWw;->e:F

    .line 2585340
    invoke-virtual {p0, v3}, LX/IWw;->setWillNotDraw(Z)V

    .line 2585341
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/IWw;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object p0

    check-cast p0, Landroid/content/res/Resources;

    iput-object p0, p1, LX/IWw;->a:Landroid/content/res/Resources;

    return-void
.end method


# virtual methods
.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 2585315
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 2585316
    iget v1, p0, LX/IWw;->d:F

    .line 2585317
    invoke-virtual {p0}, LX/IWw;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget v2, p0, LX/IWw;->d:F

    sub-float v3, v0, v2

    .line 2585318
    invoke-virtual {p0}, LX/IWw;->getHeight()I

    move-result v0

    int-to-float v0, v0

    iget v2, p0, LX/IWw;->e:F

    sub-float v2, v0, v2

    .line 2585319
    invoke-virtual {p0}, LX/IWw;->getHeight()I

    move-result v0

    int-to-float v4, v0

    .line 2585320
    iget-object v5, p0, LX/IWw;->c:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 2585321
    return-void
.end method
