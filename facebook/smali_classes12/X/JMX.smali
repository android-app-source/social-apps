.class public final LX/JMX;
.super LX/5pb;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "FbRelayNativeAdapter"
.end annotation


# instance fields
.field private a:LX/0sg;

.field private b:LX/0ti;


# direct methods
.method public constructor <init>(LX/5pY;LX/0sg;LX/0ti;)V
    .locals 0
    .param p1    # LX/5pY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2684409
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 2684410
    iput-object p2, p0, LX/JMX;->a:LX/0sg;

    .line 2684411
    iput-object p3, p0, LX/JMX;->b:LX/0ti;

    .line 2684412
    return-void
.end method

.method private static a(LX/5pC;Lcom/facebook/react/bridge/ReadableType;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 2684376
    sget-object v0, LX/JMW;->a:[I

    invoke-virtual {p1}, Lcom/facebook/react/bridge/ReadableType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2684377
    new-instance v0, LX/5p9;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown type in update "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5p9;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2684378
    :pswitch_0
    const/4 v0, 0x0

    .line 2684379
    :goto_0
    return-object v0

    .line 2684380
    :pswitch_1
    invoke-interface {p0, v2}, LX/5pC;->getBoolean(I)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 2684381
    :pswitch_2
    invoke-interface {p0, v2}, LX/5pC;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 2684382
    :pswitch_3
    invoke-interface {p0, v2}, LX/5pC;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2684408
    const-string v0, "FbRelayNativeAdapter"

    return-object v0
.end method

.method public final updateCLC(LX/5pC;)V
    .locals 11
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2684383
    iget-object v0, p0, LX/JMX;->a:LX/0sg;

    invoke-virtual {v0}, LX/0sg;->a()LX/2lk;

    move-result-object v4

    .line 2684384
    sget-object v5, LX/0t7;->a:LX/0t8;

    .line 2684385
    new-instance v6, Landroid/util/SparseArray;

    invoke-direct {v6}, Landroid/util/SparseArray;-><init>()V

    move v1, v2

    .line 2684386
    :goto_0
    invoke-interface {p1}, LX/5pC;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 2684387
    invoke-interface {p1, v1}, LX/5pC;->b(I)LX/5pC;

    move-result-object v7

    .line 2684388
    invoke-interface {v7, v2}, LX/5pC;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2684389
    const/4 v3, 0x1

    invoke-interface {v7, v3}, LX/5pC;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 2684390
    const/4 v3, 0x2

    invoke-interface {v7, v3}, LX/5pC;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 2684391
    invoke-static {v0}, LX/38I;->a(Ljava/lang/String;)I

    move-result v10

    .line 2684392
    invoke-virtual {v6, v10}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    .line 2684393
    if-nez v0, :cond_0

    .line 2684394
    invoke-virtual {v5, v10}, LX/0t8;->a(I)[Ljava/lang/String;

    move-result-object v0

    .line 2684395
    new-instance v3, Ljava/util/HashSet;

    if-nez v0, :cond_2

    new-array v0, v2, [Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    :goto_1
    invoke-direct {v3, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 2684396
    invoke-virtual {v6, v10, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    move-object v0, v3

    .line 2684397
    :cond_0
    invoke-virtual {v0, v9}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2684398
    const/4 v0, 0x3

    invoke-interface {v7, v0}, LX/5pC;->getType(I)Lcom/facebook/react/bridge/ReadableType;

    move-result-object v0

    .line 2684399
    invoke-static {v7, v0}, LX/JMX;->a(LX/5pC;Lcom/facebook/react/bridge/ReadableType;)Ljava/lang/Object;

    move-result-object v0

    .line 2684400
    invoke-interface {v4, v8, v9, v0}, LX/2lk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)Z

    .line 2684401
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2684402
    :cond_2
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_1

    .line 2684403
    :cond_3
    :try_start_0
    iget-object v0, p0, LX/JMX;->b:LX/0ti;

    invoke-virtual {v0, v4}, LX/0ti;->a(LX/2lk;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2684404
    :goto_2
    return-void

    .line 2684405
    :catch_0
    move-exception v0

    .line 2684406
    iget-object v1, p0, LX/5pb;->a:LX/5pY;

    move-object v1, v1

    .line 2684407
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    invoke-virtual {v1, v2}, LX/5pX;->a(Ljava/lang/RuntimeException;)V

    goto :goto_2
.end method
