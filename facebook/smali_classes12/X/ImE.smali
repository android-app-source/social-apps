.class public final LX/ImE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/messaging/payment/sync/delta/PaymentsPrefetchedSyncData;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2609169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2609170
    new-instance v0, Lcom/facebook/messaging/payment/sync/delta/PaymentsPrefetchedSyncData;

    invoke-direct {v0}, Lcom/facebook/messaging/payment/sync/delta/PaymentsPrefetchedSyncData;-><init>()V

    return-object v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2609171
    new-array v0, p1, [Lcom/facebook/messaging/payment/sync/delta/PaymentsPrefetchedSyncData;

    return-object v0
.end method
