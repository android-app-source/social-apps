.class public final LX/I0R;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;)V
    .locals 0

    .prologue
    .line 2527010
    iput-object p1, p0, LX/I0R;->a:Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x37c4ab2d

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2527011
    iget-object v1, p0, LX/I0R;->a:Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;

    iget-object v1, v1, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;->j:LX/7oa;

    if-nez v1, :cond_0

    .line 2527012
    const v1, -0x59840f08

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2527013
    :goto_0
    return-void

    .line 2527014
    :cond_0
    iget-object v1, p0, LX/I0R;->a:Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;

    iget-object v1, v1, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;->a:LX/Blh;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/I0R;->a:Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;

    iget-object v3, v3, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;->j:LX/7oa;

    invoke-interface {v3}, LX/7oa;->e()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/I0R;->a:Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;

    iget-object v4, v4, Lcom/facebook/events/dashboard/calendar/EventCalendarDashboardCantAttendRowView;->k:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v4, v4, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    invoke-virtual {v1, v2, v3, v4}, LX/Blh;->a(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/events/common/EventActionContext;)V

    .line 2527015
    const v1, 0x746bef37

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0
.end method
