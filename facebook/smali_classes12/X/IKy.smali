.class public final LX/IKy;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/community/protocol/FetchCommunityForumGroupsGraphQLModels$FetchCommunityForumGroupsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;)V
    .locals 0

    .prologue
    .line 2567470
    iput-object p1, p0, LX/IKy;->a:Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groups/community/protocol/FetchCommunityForumGroupsGraphQLModels$FetchCommunityForumGroupsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2567471
    if-eqz p1, :cond_0

    .line 2567472
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2567473
    if-eqz v0, :cond_0

    .line 2567474
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2567475
    check-cast v0, Lcom/facebook/groups/community/protocol/FetchCommunityForumGroupsGraphQLModels$FetchCommunityForumGroupsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/community/protocol/FetchCommunityForumGroupsGraphQLModels$FetchCommunityForumGroupsModel;->a()Lcom/facebook/groups/community/protocol/FetchCommunityForumGroupsGraphQLModels$FetchCommunityForumGroupsModel$CommunityForumChildGroupsModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2567476
    :cond_0
    :goto_0
    return-void

    .line 2567477
    :cond_1
    iget-object v1, p0, LX/IKy;->a:Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;

    .line 2567478
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2567479
    check-cast v0, Lcom/facebook/groups/community/protocol/FetchCommunityForumGroupsGraphQLModels$FetchCommunityForumGroupsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/community/protocol/FetchCommunityForumGroupsGraphQLModels$FetchCommunityForumGroupsModel;->a()Lcom/facebook/groups/community/protocol/FetchCommunityForumGroupsGraphQLModels$FetchCommunityForumGroupsModel$CommunityForumChildGroupsModel;

    move-result-object v0

    .line 2567480
    iput-object v0, v1, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->w:Lcom/facebook/groups/community/protocol/FetchCommunityForumGroupsGraphQLModels$FetchCommunityForumGroupsModel$CommunityForumChildGroupsModel;

    .line 2567481
    goto :goto_0
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    const/4 v1, -0x1

    .line 2567482
    instance-of v0, p1, LX/4Ua;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 2567483
    check-cast v0, LX/4Ua;

    iget-object v0, v0, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    if-eqz v0, :cond_0

    check-cast p1, LX/4Ua;

    iget-object v0, p1, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    iget v0, v0, Lcom/facebook/graphql/error/GraphQLError;->code:I

    .line 2567484
    :goto_0
    iget-object v1, p0, LX/IKy;->a:Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;

    iget-object v1, v1, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->r:LX/IL9;

    iget-object v2, p0, LX/IKy;->a:Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;

    iget-object v2, v2, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;->u:Ljava/lang/String;

    const-class v3, Lcom/facebook/groups/community/activities/JoinCollegeCommunityActivity;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3}, LX/IL9;->a(Ljava/lang/String;ILjava/lang/String;)V

    .line 2567485
    return-void

    :cond_0
    move v0, v1

    .line 2567486
    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2567487
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-direct {p0, p1}, LX/IKy;->a(Lcom/facebook/graphql/executor/GraphQLResult;)V

    return-void
.end method
