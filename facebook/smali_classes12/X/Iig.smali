.class public LX/Iig;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Iif;


# instance fields
.field private a:LX/Iif;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Iiz;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ij4;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Uh;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Iiz;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Ij4;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2605000
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2605001
    iput-object p1, p0, LX/Iig;->b:LX/0Ot;

    .line 2605002
    iput-object p2, p0, LX/Iig;->c:LX/0Ot;

    .line 2605003
    iput-object p3, p0, LX/Iig;->d:LX/0Uh;

    .line 2605004
    return-void
.end method


# virtual methods
.method public final a(LX/Iiv;)LX/Iik;
    .locals 3

    .prologue
    .line 2605005
    iget-object v0, p0, LX/Iig;->d:LX/0Uh;

    const/16 v1, 0x22d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2605006
    iget-object v0, p0, LX/Iig;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Iif;

    iput-object v0, p0, LX/Iig;->a:LX/Iif;

    .line 2605007
    :goto_0
    iget-object v0, p0, LX/Iig;->a:LX/Iif;

    invoke-interface {v0, p1}, LX/Iif;->a(LX/Iiv;)LX/Iik;

    move-result-object v0

    return-object v0

    .line 2605008
    :cond_0
    iget-object v0, p0, LX/Iig;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Iif;

    iput-object v0, p0, LX/Iig;->a:LX/Iif;

    goto :goto_0
.end method
