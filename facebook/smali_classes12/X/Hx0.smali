.class public final LX/Hx0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;",
        "Lcom/facebook/events/carousel/EventCardViewBinder;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Hx5;


# direct methods
.method public constructor <init>(LX/Hx5;)V
    .locals 0

    .prologue
    .line 2521509
    iput-object p1, p0, LX/Hx0;->a:LX/Hx5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2521510
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;

    .line 2521511
    if-nez p1, :cond_0

    .line 2521512
    const/4 v0, 0x0

    .line 2521513
    :goto_0
    return-object v0

    .line 2521514
    :cond_0
    invoke-static {p1}, LX/Bm1;->c(LX/7oa;)LX/7vC;

    move-result-object v1

    .line 2521515
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ab()Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel$EventViewerSubscribedSourceProfilesModel;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ab()Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel$EventViewerSubscribedSourceProfilesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel$EventViewerSubscribedSourceProfilesModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2521516
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ab()Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel$EventViewerSubscribedSourceProfilesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel$EventViewerSubscribedSourceProfilesModel;->a()LX/0Px;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel$EventViewerSubscribedSourceProfilesModel$NodesModel;

    .line 2521517
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel$EventViewerSubscribedSourceProfilesModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v2

    .line 2521518
    iput-object v2, v1, LX/7vC;->al:Ljava/lang/String;

    .line 2521519
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel$EventViewerSubscribedSourceProfilesModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v0

    .line 2521520
    iput-object v0, v1, LX/7vC;->am:Ljava/lang/String;

    .line 2521521
    :cond_1
    invoke-virtual {v1}, LX/7vC;->b()Lcom/facebook/events/model/Event;

    move-result-object v0

    .line 2521522
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 2521523
    iget-object v2, p0, LX/Hx0;->a:LX/Hx5;

    iget-object v2, v2, LX/Hx5;->J:LX/I33;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2521524
    iget-object v2, v0, Lcom/facebook/events/model/Event;->an:Ljava/lang/String;

    move-object v2, v2

    .line 2521525
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 2521526
    iget-object v2, v0, Lcom/facebook/events/model/Event;->ao:Ljava/lang/String;

    move-object v2, v2

    .line 2521527
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 2521528
    iget-object v2, p0, LX/Hx0;->a:LX/Hx5;

    iget-object v2, v2, LX/Hx5;->L:LX/I34;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2521529
    :cond_2
    iget-object v2, p0, LX/Hx0;->a:LX/Hx5;

    iget-object v2, v2, LX/Hx5;->M:LX/Gct;

    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->aa()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;

    move-result-object v3

    iget-object v4, p0, LX/Hx0;->a:LX/Hx5;

    iget-object v4, v4, LX/Hx5;->F:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-virtual {v2, v0, v3, v4, v1}, LX/Gct;->a(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;Lcom/facebook/events/common/EventAnalyticsParams;Ljava/util/List;)Lcom/facebook/events/carousel/EventCardViewBinder;

    move-result-object v0

    goto :goto_0
.end method
