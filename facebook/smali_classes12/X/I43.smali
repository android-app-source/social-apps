.class public final LX/I43;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 22

    .prologue
    .line 2532928
    const-wide/16 v16, 0x0

    .line 2532929
    const/4 v15, 0x0

    .line 2532930
    const/4 v14, 0x0

    .line 2532931
    const/4 v13, 0x0

    .line 2532932
    const/4 v12, 0x0

    .line 2532933
    const/4 v11, 0x0

    .line 2532934
    const/4 v10, 0x0

    .line 2532935
    const-wide/16 v8, 0x0

    .line 2532936
    const-wide/16 v6, 0x0

    .line 2532937
    const/4 v5, 0x0

    .line 2532938
    const/4 v4, 0x0

    .line 2532939
    const/4 v3, 0x0

    .line 2532940
    const/4 v2, 0x0

    .line 2532941
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v18

    sget-object v19, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_f

    .line 2532942
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2532943
    const/4 v2, 0x0

    .line 2532944
    :goto_0
    return v2

    .line 2532945
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_b

    .line 2532946
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 2532947
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2532948
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v2, :cond_0

    .line 2532949
    const-string v6, "creation_time"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2532950
    const/4 v2, 0x1

    .line 2532951
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 2532952
    :cond_1
    const-string v6, "document_body_elements"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2532953
    invoke-static/range {p0 .. p1}, LX/I42;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v20, v2

    goto :goto_1

    .line 2532954
    :cond_2
    const-string v6, "document_description"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2532955
    invoke-static/range {p0 .. p1}, LX/8al;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v19, v2

    goto :goto_1

    .line 2532956
    :cond_3
    const-string v6, "document_owner"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2532957
    invoke-static/range {p0 .. p1}, LX/8ZM;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v18, v2

    goto :goto_1

    .line 2532958
    :cond_4
    const-string v6, "document_subtitle"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 2532959
    invoke-static/range {p0 .. p1}, LX/8al;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v17, v2

    goto :goto_1

    .line 2532960
    :cond_5
    const-string v6, "document_title"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 2532961
    invoke-static/range {p0 .. p1}, LX/8al;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v16, v2

    goto :goto_1

    .line 2532962
    :cond_6
    const-string v6, "id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 2532963
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v11, v2

    goto/16 :goto_1

    .line 2532964
    :cond_7
    const-string v6, "modified_timestamp"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 2532965
    const/4 v2, 0x1

    .line 2532966
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v6

    move v9, v2

    move-wide v14, v6

    goto/16 :goto_1

    .line 2532967
    :cond_8
    const-string v6, "publish_timestamp"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 2532968
    const/4 v2, 0x1

    .line 2532969
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v6

    move v8, v2

    move-wide v12, v6

    goto/16 :goto_1

    .line 2532970
    :cond_9
    const-string v6, "url"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 2532971
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v10, v2

    goto/16 :goto_1

    .line 2532972
    :cond_a
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 2532973
    :cond_b
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 2532974
    if-eqz v3, :cond_c

    .line 2532975
    const/4 v3, 0x0

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 2532976
    :cond_c
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2532977
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2532978
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2532979
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2532980
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2532981
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 2532982
    if-eqz v9, :cond_d

    .line 2532983
    const/4 v3, 0x7

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide v4, v14

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 2532984
    :cond_d
    if-eqz v8, :cond_e

    .line 2532985
    const/16 v3, 0x8

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide v4, v12

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 2532986
    :cond_e
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 2532987
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_f
    move/from16 v18, v13

    move/from16 v19, v14

    move/from16 v20, v15

    move-wide v14, v8

    move v8, v2

    move v9, v3

    move v3, v4

    move/from16 v21, v11

    move v11, v10

    move v10, v5

    move-wide/from16 v4, v16

    move/from16 v17, v12

    move/from16 v16, v21

    move-wide v12, v6

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 2532988
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2532989
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 2532990
    cmp-long v2, v0, v4

    if-eqz v2, :cond_0

    .line 2532991
    const-string v2, "creation_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2532992
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 2532993
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2532994
    if-eqz v0, :cond_1

    .line 2532995
    const-string v1, "document_body_elements"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2532996
    invoke-static {p0, v0, p2, p3}, LX/I42;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2532997
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2532998
    if-eqz v0, :cond_2

    .line 2532999
    const-string v1, "document_description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2533000
    invoke-static {p0, v0, p2, p3}, LX/8al;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2533001
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2533002
    if-eqz v0, :cond_3

    .line 2533003
    const-string v1, "document_owner"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2533004
    invoke-static {p0, v0, p2, p3}, LX/8ZM;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2533005
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2533006
    if-eqz v0, :cond_4

    .line 2533007
    const-string v1, "document_subtitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2533008
    invoke-static {p0, v0, p2, p3}, LX/8al;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2533009
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2533010
    if-eqz v0, :cond_5

    .line 2533011
    const-string v1, "document_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2533012
    invoke-static {p0, v0, p2, p3}, LX/8al;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2533013
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2533014
    if-eqz v0, :cond_6

    .line 2533015
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2533016
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2533017
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 2533018
    cmp-long v2, v0, v4

    if-eqz v2, :cond_7

    .line 2533019
    const-string v2, "modified_timestamp"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2533020
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 2533021
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 2533022
    cmp-long v2, v0, v4

    if-eqz v2, :cond_8

    .line 2533023
    const-string v2, "publish_timestamp"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2533024
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 2533025
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2533026
    if-eqz v0, :cond_9

    .line 2533027
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2533028
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2533029
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2533030
    return-void
.end method
