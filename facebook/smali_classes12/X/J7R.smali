.class public LX/J7R;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/1dy;


# direct methods
.method public constructor <init>(LX/1dy;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2650858
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2650859
    iput-object p1, p0, LX/J7R;->a:LX/1dy;

    .line 2650860
    return-void
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2650861
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 2650862
    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;->name()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2650863
    return-object v0
.end method

.method public static a(LX/J7R;Lcom/google/common/util/concurrent/ListenableFuture;Lcom/facebook/profile/inforequest/protocol/InfoRequestMutationsModels$InfoRequestMutationFieldsModel;LX/J73;Ljava/util/concurrent/Executor;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;",
            "Lcom/facebook/profile/inforequest/protocol/InfoRequestMutationsModels$InfoRequestMutationFieldsModel;",
            "LX/J73;",
            "Ljava/util/concurrent/Executor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2650864
    iget-object v0, p0, LX/J7R;->a:LX/1dy;

    invoke-virtual {v0, p1, p2}, LX/1dy;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0jT;)V

    .line 2650865
    new-instance v0, LX/J7Q;

    invoke-direct {v0, p0, p3}, LX/J7Q;-><init>(LX/J7R;LX/J73;)V

    invoke-static {p1, v0, p4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2650866
    return-void
.end method

.method public static b(LX/0QB;)LX/J7R;
    .locals 2

    .prologue
    .line 2650867
    new-instance v1, LX/J7R;

    invoke-static {p0}, LX/1dy;->b(LX/0QB;)LX/1dy;

    move-result-object v0

    check-cast v0, LX/1dy;

    invoke-direct {v1, v0}, LX/J7R;-><init>(LX/1dy;)V

    .line 2650868
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;Ljava/lang/String;LX/0aG;LX/J73;Ljava/util/concurrent/Executor;)V
    .locals 4

    .prologue
    .line 2650869
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2650870
    const-string v1, "operationParams"

    new-instance v2, Lcom/facebook/profile/inforequest/protocol/DeleteInfoRequestParams;

    invoke-static {p2}, LX/J7R;->a(Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldType;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v2, p3, v3}, Lcom/facebook/profile/inforequest/protocol/DeleteInfoRequestParams;-><init>(Ljava/lang/String;Ljava/util/List;)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2650871
    const-string v1, "timeline_delete_info_request"

    const v2, -0x28a065bf

    invoke-static {p4, v1, v0, v2}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 2650872
    new-instance v1, LX/J7K;

    invoke-direct {v1}, LX/J7K;-><init>()V

    .line 2650873
    iput-object p1, v1, LX/J7K;->a:Ljava/lang/String;

    .line 2650874
    move-object v1, v1

    .line 2650875
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;->REQUESTABLE:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    .line 2650876
    iput-object v2, v1, LX/J7K;->b:Lcom/facebook/graphql/enums/GraphQLInfoRequestFieldStatus;

    .line 2650877
    move-object v1, v1

    .line 2650878
    invoke-virtual {v1}, LX/J7K;->a()Lcom/facebook/profile/inforequest/protocol/InfoRequestMutationsModels$InfoRequestMutationFieldsModel;

    move-result-object v1

    .line 2650879
    invoke-static {p0, v0, v1, p5, p6}, LX/J7R;->a(LX/J7R;Lcom/google/common/util/concurrent/ListenableFuture;Lcom/facebook/profile/inforequest/protocol/InfoRequestMutationsModels$InfoRequestMutationFieldsModel;LX/J73;Ljava/util/concurrent/Executor;)V

    .line 2650880
    return-void
.end method
