.class public final LX/ItL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "LX/ItR;",
        "LX/FKH;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/send/client/SendMessageManager;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/send/client/SendMessageManager;)V
    .locals 0

    .prologue
    .line 2622292
    iput-object p1, p0, LX/ItL;->a:Lcom/facebook/messaging/send/client/SendMessageManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4

    .prologue
    .line 2622293
    check-cast p1, LX/ItR;

    .line 2622294
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v0

    iget-object v1, p1, LX/ItR;->a:Lcom/facebook/messaging/model/messages/Message;

    invoke-virtual {v0, v1}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6f7;

    move-result-object v0

    iget-object v1, p1, LX/ItR;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2622295
    iput-object v1, v0, LX/6f7;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2622296
    move-object v1, v0

    .line 2622297
    iget-object v0, p1, LX/ItR;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->A:Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 2622298
    :goto_0
    iput-object v0, v1, LX/6f7;->A:Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    .line 2622299
    move-object v0, v1

    .line 2622300
    invoke-virtual {v0}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 2622301
    iget-object v1, p0, LX/ItL;->a:Lcom/facebook/messaging/send/client/SendMessageManager;

    invoke-virtual {v1, v0}, Lcom/facebook/messaging/send/client/SendMessageManager;->a(Lcom/facebook/messaging/model/messages/Message;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    .line 2622302
    :cond_0
    new-instance v0, Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    iget-object v2, p1, LX/ItR;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v3, p1, LX/ItR;->a:Lcom/facebook/messaging/model/messages/Message;

    iget-object v3, v3, Lcom/facebook/messaging/model/messages/Message;->A:Lcom/facebook/messaging/model/send/PendingSendQueueKey;

    iget-object v3, v3, Lcom/facebook/messaging/model/send/PendingSendQueueKey;->b:LX/6fM;

    invoke-direct {v0, v2, v3}, Lcom/facebook/messaging/model/send/PendingSendQueueKey;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/6fM;)V

    goto :goto_0
.end method
