.class public final enum LX/IW2;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IW2;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IW2;

.field public static final enum ENDED:LX/IW2;

.field public static final enum STARTED:LX/IW2;

.field public static final enum SUCCESS:LX/IW2;

.field public static final enum SUCCESS_STALE:LX/IW2;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2583340
    new-instance v0, LX/IW2;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v2}, LX/IW2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IW2;->SUCCESS:LX/IW2;

    new-instance v0, LX/IW2;

    const-string v1, "SUCCESS_STALE"

    invoke-direct {v0, v1, v3}, LX/IW2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IW2;->SUCCESS_STALE:LX/IW2;

    new-instance v0, LX/IW2;

    const-string v1, "STARTED"

    invoke-direct {v0, v1, v4}, LX/IW2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IW2;->STARTED:LX/IW2;

    new-instance v0, LX/IW2;

    const-string v1, "ENDED"

    invoke-direct {v0, v1, v5}, LX/IW2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IW2;->ENDED:LX/IW2;

    .line 2583341
    const/4 v0, 0x4

    new-array v0, v0, [LX/IW2;

    sget-object v1, LX/IW2;->SUCCESS:LX/IW2;

    aput-object v1, v0, v2

    sget-object v1, LX/IW2;->SUCCESS_STALE:LX/IW2;

    aput-object v1, v0, v3

    sget-object v1, LX/IW2;->STARTED:LX/IW2;

    aput-object v1, v0, v4

    sget-object v1, LX/IW2;->ENDED:LX/IW2;

    aput-object v1, v0, v5

    sput-object v0, LX/IW2;->$VALUES:[LX/IW2;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2583342
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IW2;
    .locals 1

    .prologue
    .line 2583343
    const-class v0, LX/IW2;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IW2;

    return-object v0
.end method

.method public static values()[LX/IW2;
    .locals 1

    .prologue
    .line 2583344
    sget-object v0, LX/IW2;->$VALUES:[LX/IW2;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IW2;

    return-object v0
.end method
