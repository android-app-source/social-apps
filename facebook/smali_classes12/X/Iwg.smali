.class public final LX/Iwg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1L9;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1L9",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 2630751
    iput-object p1, p0, LX/Iwg;->c:Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;

    iput-boolean p2, p0, LX/Iwg;->a:Z

    iput-object p3, p0, LX/Iwg;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2630752
    return-void
.end method

.method public final a(Ljava/lang/Object;Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 4

    .prologue
    .line 2630753
    const-string v0, "pages_browser_like_failure"

    .line 2630754
    iget-boolean v1, p0, LX/Iwg;->a:Z

    if-nez v1, :cond_0

    .line 2630755
    const-string v0, "pages_browser_unlike_failure"

    .line 2630756
    :cond_0
    iget-object v1, p0, LX/Iwg;->c:Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;

    iget-object v1, v1, Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;->d:LX/Iw6;

    iget-object v2, p0, LX/Iwg;->b:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, LX/Iw6;->a(Ljava/lang/String;J)V

    .line 2630757
    iget-object v1, p0, LX/Iwg;->c:Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;

    iget-object v2, p0, LX/Iwg;->b:Ljava/lang/String;

    iget-boolean v0, p0, LX/Iwg;->a:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;->a(Ljava/lang/String;Z)V

    .line 2630758
    iget-object v0, p0, LX/Iwg;->c:Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;

    invoke-virtual {v0}, Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;->b()V

    .line 2630759
    return-void

    .line 2630760
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2630761
    check-cast p1, Ljava/lang/String;

    .line 2630762
    const-string v0, "pages_browser_like_succesful"

    .line 2630763
    iget-boolean v1, p0, LX/Iwg;->a:Z

    if-nez v1, :cond_0

    .line 2630764
    const-string v0, "pages_browser_unlike_succesful"

    .line 2630765
    :cond_0
    iget-object v1, p0, LX/Iwg;->c:Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;

    iget-object v1, v1, Lcom/facebook/pages/browser/fragment/BasePagesBrowserFragment;->d:LX/Iw6;

    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, LX/Iw6;->a(Ljava/lang/String;J)V

    .line 2630766
    return-void
.end method

.method public final bridge synthetic c(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2630767
    return-void
.end method
