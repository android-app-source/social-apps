.class public final LX/IJU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Zu;


# instance fields
.field public final synthetic a:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;)V
    .locals 0

    .prologue
    .line 2563553
    iput-object p1, p0, LX/IJU;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 2563554
    iget-object v0, p0, LX/IJU;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->u:LX/IJV;

    sget-object v1, LX/IJV;->EXPANDED:LX/IJV;

    if-ne v0, v1, :cond_0

    .line 2563555
    iget-object v0, p0, LX/IJU;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;

    const-wide/16 v2, 0x2ee

    invoke-static {v0, v2, v3}, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->a$redex0(Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;J)V

    .line 2563556
    :goto_0
    return-void

    .line 2563557
    :cond_0
    iget-object v0, p0, LX/IJU;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;

    iget-object v0, v0, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->h:LX/IID;

    .line 2563558
    const-string v1, "friends_nearby_dashboard_map_expand"

    invoke-static {v0, v1}, LX/IID;->i(LX/IID;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 2563559
    iget-object v2, v0, LX/IID;->a:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2563560
    iget-object v0, p0, LX/IJU;->a:Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;

    sget-object v1, LX/IJV;->EXPANDED:LX/IJV;

    invoke-virtual {v0, v1}, Lcom/facebook/friendsnearby/ui/FriendsNearbyMapFragment;->a(LX/IJV;)V

    goto :goto_0
.end method
