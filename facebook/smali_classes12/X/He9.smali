.class public final LX/He9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/uberbar/ui/UberbarFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/uberbar/ui/UberbarFragment;)V
    .locals 0

    .prologue
    .line 2489721
    iput-object p1, p0, LX/He9;->a:Lcom/facebook/uberbar/ui/UberbarFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x2

    const v0, -0x22c9087e

    invoke-static {v2, v4, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2489708
    iget-object v1, p0, LX/He9;->a:Lcom/facebook/uberbar/ui/UberbarFragment;

    iget-object v1, v1, Lcom/facebook/uberbar/ui/UberbarFragment;->c:Landroid/widget/EditText;

    if-nez v1, :cond_0

    .line 2489709
    const v1, -0x59e80f96

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2489710
    :goto_0
    return-void

    .line 2489711
    :cond_0
    iget-object v1, p0, LX/He9;->a:Lcom/facebook/uberbar/ui/UberbarFragment;

    iget-object v1, v1, Lcom/facebook/uberbar/ui/UberbarFragment;->c:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 2489712
    iget-object v1, p0, LX/He9;->a:Lcom/facebook/uberbar/ui/UberbarFragment;

    iget-object v1, v1, Lcom/facebook/uberbar/ui/UberbarFragment;->c:Landroid/widget/EditText;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2489713
    :goto_1
    const v1, -0x2a8545b

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0

    .line 2489714
    :cond_1
    iget-object v1, p0, LX/He9;->a:Lcom/facebook/uberbar/ui/UberbarFragment;

    iget-object v1, v1, Lcom/facebook/uberbar/ui/UberbarFragment;->b:Landroid/view/inputmethod/InputMethodManager;

    iget-object v2, p0, LX/He9;->a:Lcom/facebook/uberbar/ui/UberbarFragment;

    iget-object v2, v2, Lcom/facebook/uberbar/ui/UberbarFragment;->c:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 2489715
    iget-object v1, p0, LX/He9;->a:Lcom/facebook/uberbar/ui/UberbarFragment;

    iget-object v1, v1, Lcom/facebook/uberbar/ui/UberbarFragment;->a:LX/He3;

    .line 2489716
    iget-object v2, v1, LX/He2;->h:LX/G5R;

    .line 2489717
    const/4 v1, 0x1

    iput-boolean v1, v2, LX/G5R;->e:Z

    .line 2489718
    iget-object v1, p0, LX/He9;->a:Lcom/facebook/uberbar/ui/UberbarFragment;

    .line 2489719
    iput-boolean v4, v1, Lcom/facebook/uberbar/ui/UberbarFragment;->h:Z

    .line 2489720
    iget-object v1, p0, LX/He9;->a:Lcom/facebook/uberbar/ui/UberbarFragment;

    invoke-virtual {v1}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_1
.end method
