.class public final LX/ILR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;)V
    .locals 0

    .prologue
    .line 2567983
    iput-object p1, p0, LX/ILR;->a:Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2567984
    iget-object v0, p0, LX/ILR;->a:Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;

    iget-object v1, v0, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->e:LX/3mF;

    iget-object v0, p0, LX/ILR;->a:Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;

    iget-object v0, v0, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->k:LX/DKB;

    invoke-virtual {v0}, LX/DKB;->b()LX/0Px;

    move-result-object v2

    iget-object v0, p0, LX/ILR;->a:Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;

    iget-object v0, v0, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2567985
    iget-object v3, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v3

    .line 2567986
    const-string v3, "community_onboarding_nux"

    .line 2567987
    new-instance v4, LX/4Fb;

    invoke-direct {v4}, LX/4Fb;-><init>()V

    .line 2567988
    const-string v5, "group_ids"

    invoke-virtual {v4, v5, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 2567989
    move-object v4, v4

    .line 2567990
    const-string v5, "user_id"

    invoke-virtual {v4, v5, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2567991
    move-object v4, v4

    .line 2567992
    const-string v5, "source"

    invoke-virtual {v4, v5, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2567993
    move-object v4, v4

    .line 2567994
    new-instance v5, LX/B25;

    invoke-direct {v5}, LX/B25;-><init>()V

    move-object v5, v5

    .line 2567995
    const-string p0, "input"

    invoke-virtual {v5, p0, v4}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2567996
    invoke-static {v5}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v4

    .line 2567997
    iget-object v5, v1, LX/3mF;->a:LX/0tX;

    invoke-virtual {v5, v4}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v0, v4

    .line 2567998
    return-object v0
.end method
