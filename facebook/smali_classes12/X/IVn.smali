.class public LX/IVn;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public b:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final c:LX/DOK;

.field public d:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/IVl;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/IVk;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2582597
    const-class v0, LX/IVn;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/IVn;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/DOK;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/IVl;",
            ">;",
            "LX/DOK;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2582591
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2582592
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/IVn;->g:Ljava/util/List;

    .line 2582593
    iput-object p1, p0, LX/IVn;->e:LX/0Or;

    .line 2582594
    iput-object p2, p0, LX/IVn;->c:LX/DOK;

    .line 2582595
    iput-object p3, p0, LX/IVn;->f:LX/0Ot;

    .line 2582596
    return-void
.end method

.method public static a(LX/0QB;)LX/IVn;
    .locals 4

    .prologue
    .line 2582586
    new-instance v1, LX/IVn;

    const/16 v0, 0x242e

    invoke-static {p0, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {p0}, LX/DOK;->a(LX/0QB;)LX/DOK;

    move-result-object v0

    check-cast v0, LX/DOK;

    const/16 v3, 0x259

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-direct {v1, v2, v0, v3}, LX/IVn;-><init>(LX/0Or;LX/DOK;LX/0Ot;)V

    .line 2582587
    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    .line 2582588
    iput-object v0, v1, LX/IVn;->b:LX/0ad;

    .line 2582589
    move-object v0, v1

    .line 2582590
    return-object v0
.end method
