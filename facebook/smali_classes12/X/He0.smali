.class public LX/He0;
.super LX/Hcr;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Hcr",
        "<",
        "Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/topics/sections/title/TopicTitlePartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/topics/sections/title/TopicTitlePartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2489548
    invoke-direct {p0}, LX/Hcr;-><init>()V

    .line 2489549
    const/4 v0, 0x0

    iput-object v0, p0, LX/He0;->b:Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;

    .line 2489550
    iput-object p1, p0, LX/He0;->a:LX/0Ot;

    .line 2489551
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;)Landroid/util/Pair;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;",
            ")",
            "Landroid/util/Pair",
            "<",
            "LX/0zO",
            "<",
            "Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;",
            ">;",
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 2489552
    invoke-virtual {p0, p1}, LX/Hcr;->b(Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;)V

    .line 2489553
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2489554
    iget-object v0, p0, LX/He0;->b:Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;
    .locals 1

    .prologue
    .line 2489555
    iget-object v0, p0, LX/He0;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    return-object v0
.end method

.method public final b(Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;)V
    .locals 2

    .prologue
    .line 2489556
    iget-object v0, p0, LX/He0;->b:Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/He0;->b:Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;

    invoke-virtual {v0}, Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;->k()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/He0;->b:Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;

    invoke-virtual {v0}, Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2489557
    :goto_0
    return-void

    .line 2489558
    :cond_0
    iput-object p1, p0, LX/He0;->b:Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;

    .line 2489559
    invoke-virtual {p0}, LX/He0;->setChanged()V

    .line 2489560
    invoke-virtual {p0}, LX/He0;->notifyObservers()V

    goto :goto_0
.end method

.method public final c()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2489561
    iget-object v0, p0, LX/He0;->b:Lcom/facebook/topics/sections/SimpleTopicGraphQLModels$SimpleTopicQueryModel;

    return-object v0
.end method
