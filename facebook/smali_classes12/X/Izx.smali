.class public LX/Izx;
.super LX/Izu;
.source ""


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Zb;

.field private final c:LX/J0B;

.field private final d:LX/Izb;

.field private final e:LX/Izd;

.field private final f:LX/Izc;

.field private final g:LX/Ize;

.field private final h:LX/Izq;

.field private final i:LX/Izg;

.field private final j:LX/Izh;

.field private final k:LX/Izl;

.field private final l:LX/IzM;

.field private final m:LX/Izk;


# direct methods
.method public constructor <init>(LX/0Or;LX/0Zb;LX/J0B;LX/Izb;LX/Izd;LX/Izc;LX/Ize;LX/Izq;LX/Izg;LX/Izk;LX/Izh;LX/IzM;LX/Izl;)V
    .locals 1
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/payments/p2p/config/IsP2pPaymentsSyncProtocolEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Zb;",
            "LX/J0B;",
            "LX/Izb;",
            "LX/Izd;",
            "LX/Izc;",
            "LX/Ize;",
            "LX/Izq;",
            "LX/Izg;",
            "LX/Izk;",
            "LX/Izh;",
            "LX/IzM;",
            "LX/Izl;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2636292
    const-string v0, "PaymentDbServiceHandler"

    invoke-direct {p0, v0}, LX/Izu;-><init>(Ljava/lang/String;)V

    .line 2636293
    iput-object p1, p0, LX/Izx;->a:LX/0Or;

    .line 2636294
    iput-object p2, p0, LX/Izx;->b:LX/0Zb;

    .line 2636295
    iput-object p3, p0, LX/Izx;->c:LX/J0B;

    .line 2636296
    iput-object p4, p0, LX/Izx;->d:LX/Izb;

    .line 2636297
    iput-object p5, p0, LX/Izx;->e:LX/Izd;

    .line 2636298
    iput-object p7, p0, LX/Izx;->g:LX/Ize;

    .line 2636299
    iput-object p6, p0, LX/Izx;->f:LX/Izc;

    .line 2636300
    iput-object p8, p0, LX/Izx;->h:LX/Izq;

    .line 2636301
    iput-object p9, p0, LX/Izx;->i:LX/Izg;

    .line 2636302
    iput-object p10, p0, LX/Izx;->m:LX/Izk;

    .line 2636303
    iput-object p11, p0, LX/Izx;->j:LX/Izh;

    .line 2636304
    iput-object p12, p0, LX/Izx;->l:LX/IzM;

    .line 2636305
    iput-object p13, p0, LX/Izx;->k:LX/Izl;

    .line 2636306
    return-void
.end method

.method private C(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 2636279
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    .line 2636280
    invoke-virtual {v1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/cards/FetchPaymentCardsResult;

    .line 2636281
    iget-object v2, v0, Lcom/facebook/payments/p2p/service/model/cards/FetchPaymentCardsResult;->b:Lcom/facebook/payments/p2p/model/PaymentCard;

    move-object v2, v2

    .line 2636282
    if-eqz v2, :cond_0

    .line 2636283
    iget-object v2, p0, LX/Izx;->e:LX/Izd;

    .line 2636284
    iget-object v3, v0, Lcom/facebook/payments/p2p/service/model/cards/FetchPaymentCardsResult;->b:Lcom/facebook/payments/p2p/model/PaymentCard;

    move-object v3, v3

    .line 2636285
    invoke-virtual {v2, v3}, LX/Izd;->a(Lcom/facebook/payments/p2p/model/PaymentCard;)V

    .line 2636286
    :goto_0
    iget-object v2, p0, LX/Izx;->e:LX/Izd;

    .line 2636287
    iget-object v3, v0, Lcom/facebook/payments/p2p/service/model/cards/FetchPaymentCardsResult;->c:LX/0Px;

    move-object v0, v3

    .line 2636288
    invoke-virtual {v2, v0}, LX/Izd;->a(LX/0Px;)V

    .line 2636289
    iget-object v0, p0, LX/Izx;->c:LX/J0B;

    invoke-virtual {v0}, LX/J0B;->c()V

    .line 2636290
    return-object v1

    .line 2636291
    :cond_0
    iget-object v2, p0, LX/Izx;->e:LX/Izd;

    invoke-virtual {v2}, LX/Izd;->a()V

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/Izx;
    .locals 14

    .prologue
    .line 2636268
    new-instance v0, LX/Izx;

    const/16 v1, 0x1547

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v2

    check-cast v2, LX/0Zb;

    invoke-static {p0}, LX/J0B;->a(LX/0QB;)LX/J0B;

    move-result-object v3

    check-cast v3, LX/J0B;

    .line 2636269
    new-instance v6, LX/Izb;

    invoke-static {p0}, LX/IzB;->a(LX/0QB;)LX/IzB;

    move-result-object v4

    check-cast v4, LX/IzB;

    invoke-static {p0}, LX/Izf;->a(LX/0QB;)LX/Izf;

    move-result-object v5

    check-cast v5, LX/Izf;

    invoke-direct {v6, v4, v5}, LX/Izb;-><init>(LX/IzB;LX/Izf;)V

    .line 2636270
    move-object v4, v6

    .line 2636271
    check-cast v4, LX/Izb;

    invoke-static {p0}, LX/Izd;->b(LX/0QB;)LX/Izd;

    move-result-object v5

    check-cast v5, LX/Izd;

    .line 2636272
    new-instance v8, LX/Izc;

    invoke-static {p0}, LX/IzB;->a(LX/0QB;)LX/IzB;

    move-result-object v6

    check-cast v6, LX/IzB;

    invoke-static {p0}, LX/Izi;->a(LX/0QB;)LX/Izi;

    move-result-object v7

    check-cast v7, LX/Izi;

    invoke-direct {v8, v6, v7}, LX/Izc;-><init>(LX/IzB;LX/Izi;)V

    .line 2636273
    move-object v6, v8

    .line 2636274
    check-cast v6, LX/Izc;

    .line 2636275
    new-instance v9, LX/Ize;

    invoke-static {p0}, LX/IzB;->a(LX/0QB;)LX/IzB;

    move-result-object v7

    check-cast v7, LX/IzB;

    invoke-static {p0}, LX/Izm;->a(LX/0QB;)LX/Izm;

    move-result-object v8

    check-cast v8, LX/Izm;

    invoke-direct {v9, v7, v8}, LX/Ize;-><init>(LX/IzB;LX/Izm;)V

    .line 2636276
    move-object v7, v9

    .line 2636277
    check-cast v7, LX/Ize;

    invoke-static {p0}, LX/Izq;->a(LX/0QB;)LX/Izq;

    move-result-object v8

    check-cast v8, LX/Izq;

    invoke-static {p0}, LX/Izg;->a(LX/0QB;)LX/Izg;

    move-result-object v9

    check-cast v9, LX/Izg;

    invoke-static {p0}, LX/Izk;->a(LX/0QB;)LX/Izk;

    move-result-object v10

    check-cast v10, LX/Izk;

    invoke-static {p0}, LX/Izh;->a(LX/0QB;)LX/Izh;

    move-result-object v11

    check-cast v11, LX/Izh;

    invoke-static {p0}, LX/IzM;->a(LX/0QB;)LX/IzM;

    move-result-object v12

    check-cast v12, LX/IzM;

    invoke-static {p0}, LX/Izl;->a(LX/0QB;)LX/Izl;

    move-result-object v13

    check-cast v13, LX/Izl;

    invoke-direct/range {v0 .. v13}, LX/Izx;-><init>(LX/0Or;LX/0Zb;LX/J0B;LX/Izb;LX/Izd;LX/Izc;LX/Ize;LX/Izq;LX/Izg;LX/Izk;LX/Izh;LX/IzM;LX/Izl;)V

    .line 2636278
    return-object v0
.end method


# virtual methods
.method public final B(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 10

    .prologue
    .line 2636055
    iget-object v0, p0, LX/Izx;->l:LX/IzM;

    sget-object v1, LX/IzL;->j:LX/IzK;

    .line 2636056
    invoke-virtual {v0, v1}, LX/2Iu;->a(LX/0To;)Ljava/lang/String;

    move-result-object v2

    .line 2636057
    if-nez v2, :cond_1

    .line 2636058
    sget-object v2, LX/03R;->UNSET:LX/03R;

    .line 2636059
    :goto_0
    move-object v0, v2

    .line 2636060
    sget-object v1, LX/03R;->UNSET:LX/03R;

    if-eq v0, v1, :cond_0

    .line 2636061
    new-instance v1, LX/Dtk;

    invoke-direct {v1}, LX/Dtk;-><init>()V

    invoke-virtual {v0}, LX/03R;->asBoolean()Z

    move-result v0

    .line 2636062
    iput-boolean v0, v1, LX/Dtk;->a:Z

    .line 2636063
    move-object v0, v1

    .line 2636064
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 2636065
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 2636066
    invoke-virtual {v4, v8}, LX/186;->c(I)V

    .line 2636067
    iget-boolean v5, v0, LX/Dtk;->a:Z

    invoke-virtual {v4, v7, v5}, LX/186;->a(IZ)V

    .line 2636068
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 2636069
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 2636070
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 2636071
    invoke-virtual {v5, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2636072
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2636073
    new-instance v5, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentAccountEnabledStatusModel;

    invoke-direct {v5, v4}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentAccountEnabledStatusModel;-><init>(LX/15i;)V

    .line 2636074
    move-object v0, v5

    .line 2636075
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2636076
    :goto_1
    return-object v0

    .line 2636077
    :cond_0
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    .line 2636078
    invoke-virtual {v1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentAccountEnabledStatusModel;

    .line 2636079
    invoke-virtual {v0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentAccountEnabledStatusModel;->a()Z

    move-result v0

    invoke-static {v0}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    .line 2636080
    iget-object v2, p0, LX/Izx;->l:LX/IzM;

    sget-object v3, LX/IzL;->j:LX/IzK;

    invoke-virtual {v2, v3, v0}, LX/2Iu;->a(LX/0To;LX/03R;)V

    move-object v0, v1

    .line 2636081
    goto :goto_1

    .line 2636082
    :cond_1
    :try_start_0
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 2636083
    invoke-static {v2}, LX/03R;->fromDbValue(I)LX/03R;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 2636084
    :catch_0
    sget-object v2, LX/03R;->UNSET:LX/03R;

    goto :goto_0
.end method

.method public final a(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 5

    .prologue
    .line 2636224
    iget-object v0, p0, LX/Izx;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2636225
    invoke-direct {p0, p1, p2}, LX/Izx;->C(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2636226
    :goto_0
    return-object v0

    .line 2636227
    :cond_0
    iget-object v0, p0, LX/Izx;->d:LX/Izb;

    .line 2636228
    iget-object v1, v0, LX/Izb;->a:LX/IzB;

    invoke-virtual {v1}, LX/IzB;->a()LX/0am;

    move-result-object v1

    .line 2636229
    if-eqz v1, :cond_3

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2636230
    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/p2p/model/PaymentCard;

    .line 2636231
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2636232
    new-instance v2, LX/IzD;

    sget-object v0, LX/IzC;->CARD_EXISTS_AND_IN_CACHE:LX/IzC;

    invoke-direct {v2, v1, v0}, LX/IzD;-><init>(Lcom/facebook/payments/p2p/model/PaymentCard;LX/IzC;)V

    move-object v1, v2

    .line 2636233
    :goto_1
    move-object v0, v1

    .line 2636234
    iget-object v1, v0, LX/IzD;->b:LX/IzC;

    sget-object v2, LX/IzC;->CARD_EXISTS_BUT_NOT_IN_CACHE:LX/IzC;

    if-eq v1, v2, :cond_5

    const/4 v1, 0x1

    :goto_2
    move v1, v1

    .line 2636235
    if-nez v1, :cond_1

    .line 2636236
    invoke-direct {p0, p1, p2}, LX/Izx;->C(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 2636237
    :cond_1
    iget-object v1, v0, LX/IzD;->a:Lcom/facebook/payments/p2p/model/PaymentCard;

    move-object v0, v1

    .line 2636238
    iget-object v1, p0, LX/Izx;->d:LX/Izb;

    .line 2636239
    iget-object v2, v1, LX/Izb;->a:LX/IzB;

    invoke-virtual {v2}, LX/IzB;->b()LX/0am;

    move-result-object v3

    .line 2636240
    iget-object v2, v1, LX/Izb;->b:LX/Izf;

    invoke-virtual {v2}, LX/Izf;->a()LX/0Px;

    move-result-object v4

    .line 2636241
    invoke-virtual {v3}, LX/0am;->isPresent()Z

    move-result v2

    if-nez v2, :cond_6

    .line 2636242
    sget-object v2, LX/IzF;->c:LX/IzF;

    move-object v2, v2

    .line 2636243
    :goto_3
    move-object v1, v2

    .line 2636244
    iget-object v2, v1, LX/IzF;->b:LX/IzE;

    sget-object v3, LX/IzE;->CARDS_EXIST_BUT_NOT_ALL_IN_CACHE:LX/IzE;

    if-eq v2, v3, :cond_a

    const/4 v2, 0x1

    :goto_4
    move v2, v2

    .line 2636245
    if-nez v2, :cond_2

    .line 2636246
    invoke-direct {p0, p1, p2}, LX/Izx;->C(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 2636247
    :cond_2
    iget-object v2, v1, LX/IzF;->a:LX/0Px;

    move-object v1, v2

    .line 2636248
    iget-object v2, p0, LX/Izx;->c:LX/J0B;

    invoke-virtual {v2}, LX/J0B;->c()V

    .line 2636249
    new-instance v2, Lcom/facebook/payments/p2p/service/model/cards/FetchPaymentCardsResult;

    invoke-direct {v2, v0, v1}, Lcom/facebook/payments/p2p/service/model/cards/FetchPaymentCardsResult;-><init>(Lcom/facebook/payments/p2p/model/PaymentCard;Ljava/util/List;)V

    invoke-static {v2}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 2636250
    :cond_3
    iget-object v1, v0, LX/Izb;->b:LX/Izf;

    invoke-virtual {v1}, LX/Izf;->b()Ljava/lang/Long;

    move-result-object v1

    .line 2636251
    if-nez v1, :cond_4

    .line 2636252
    sget-object v1, LX/IzD;->d:LX/IzD;

    move-object v1, v1

    .line 2636253
    goto :goto_1

    .line 2636254
    :cond_4
    sget-object v1, LX/IzD;->c:LX/IzD;

    move-object v1, v1

    .line 2636255
    goto :goto_1

    :cond_5
    const/4 v1, 0x0

    goto :goto_2

    .line 2636256
    :cond_6
    invoke-virtual {v3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    if-eq v2, v4, :cond_7

    .line 2636257
    sget-object v2, LX/IzF;->c:LX/IzF;

    move-object v2, v2

    .line 2636258
    goto :goto_3

    .line 2636259
    :cond_7
    invoke-virtual {v3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Px;

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 2636260
    sget-object v2, LX/IzF;->d:LX/IzF;

    move-object v2, v2

    .line 2636261
    goto :goto_3

    .line 2636262
    :cond_8
    invoke-virtual {v3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Px;

    .line 2636263
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2636264
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_9

    const/4 v3, 0x1

    :goto_5
    invoke-static {v3}, LX/0PB;->checkArgument(Z)V

    .line 2636265
    new-instance v3, LX/IzF;

    sget-object v4, LX/IzE;->CARDS_EXIST_AND_ALL_IN_CACHE:LX/IzE;

    invoke-direct {v3, v2, v4}, LX/IzF;-><init>(LX/0Px;LX/IzE;)V

    move-object v2, v3

    .line 2636266
    goto :goto_3

    .line 2636267
    :cond_9
    const/4 v3, 0x0

    goto :goto_5

    :cond_a
    const/4 v2, 0x0

    goto :goto_4
.end method

.method public final b(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 12

    .prologue
    .line 2636198
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2636199
    const-string v1, "fetchTransactionPaymentCardParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionPaymentCardParams;

    .line 2636200
    iget-object v1, v0, Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionPaymentCardParams;->a:Ljava/lang/String;

    move-object v0, v1

    .line 2636201
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2636202
    iget-object v0, p0, LX/Izx;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2636203
    iget-object v0, p0, LX/Izx;->f:LX/Izc;

    .line 2636204
    iget-object v5, v0, LX/Izc;->b:LX/Izi;

    invoke-virtual {v5, v2, v3}, LX/Izi;->a(J)Ljava/lang/Long;

    move-result-object v5

    .line 2636205
    if-eqz v5, :cond_2

    .line 2636206
    iget-object v6, v0, LX/Izc;->a:LX/IzB;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    .line 2636207
    invoke-virtual {v6, v7, v8}, LX/IzB;->a(J)LX/0am;

    move-result-object v5

    .line 2636208
    invoke-virtual {v5}, LX/0am;->isPresent()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 2636209
    :goto_0
    move-object v5, v5

    .line 2636210
    :goto_1
    move-object v0, v5

    .line 2636211
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2636212
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2636213
    :goto_2
    return-object v0

    .line 2636214
    :cond_0
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    .line 2636215
    invoke-virtual {v1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/PaymentCard;

    .line 2636216
    if-eqz v0, :cond_1

    .line 2636217
    iget-object v4, p0, LX/Izx;->g:LX/Ize;

    .line 2636218
    iget-object v5, v4, LX/Ize;->b:LX/Izm;

    .line 2636219
    iget-wide v10, v0, Lcom/facebook/payments/p2p/model/PaymentCard;->a:J

    move-wide v7, v10

    .line 2636220
    invoke-virtual {v5, v2, v3, v7, v8}, LX/Izm;->a(JJ)V

    .line 2636221
    iget-object v5, v4, LX/Ize;->a:LX/IzB;

    invoke-virtual {v5, v0}, LX/IzB;->b(Lcom/facebook/payments/p2p/model/PaymentCard;)V

    .line 2636222
    :cond_1
    move-object v0, v1

    .line 2636223
    goto :goto_2

    :cond_2
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v5

    goto :goto_1

    :cond_3
    invoke-static {v6, v7, v8}, LX/IzB;->d(LX/IzB;J)LX/0am;

    move-result-object v5

    goto :goto_0
.end method

.method public final c(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 6

    .prologue
    .line 2636163
    const/4 v1, 0x0

    .line 2636164
    iget-object v0, p0, LX/Izx;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2636165
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2636166
    const-string v2, "fetchPaymentTransactionParams"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/transactions/FetchPaymentTransactionParams;

    .line 2636167
    iget-object v2, v0, Lcom/facebook/payments/p2p/service/model/transactions/FetchPaymentTransactionParams;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2636168
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2636169
    iget-object v4, p0, LX/Izx;->j:LX/Izh;

    invoke-virtual {v4, v2, v3}, LX/Izh;->a(J)Lcom/facebook/payments/p2p/model/PaymentTransaction;

    move-result-object v2

    .line 2636170
    if-eqz v2, :cond_1

    .line 2636171
    iget-object v1, v2, Lcom/facebook/payments/p2p/model/PaymentTransaction;->g:LX/DtQ;

    move-object v1, v1

    .line 2636172
    iget-object v3, v0, Lcom/facebook/payments/p2p/service/model/transactions/FetchPaymentTransactionParams;->b:LX/0rS;

    move-object v0, v3

    .line 2636173
    sget-object v3, LX/0rS;->STALE_DATA_OKAY:LX/0rS;

    if-eq v0, v3, :cond_0

    .line 2636174
    iget-object v0, v2, Lcom/facebook/payments/p2p/model/PaymentTransaction;->g:LX/DtQ;

    move-object v0, v0

    .line 2636175
    iget-boolean v0, v0, LX/DtQ;->isTerminalStatus:Z

    if-eqz v0, :cond_1

    .line 2636176
    :cond_0
    invoke-static {v2}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2636177
    :goto_0
    return-object v0

    .line 2636178
    :cond_1
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    .line 2636179
    invoke-virtual {v2}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/PaymentTransaction;

    .line 2636180
    if-eqz v1, :cond_2

    .line 2636181
    iget-object v3, v0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->g:LX/DtQ;

    move-object v3, v3

    .line 2636182
    if-eq v1, v3, :cond_2

    .line 2636183
    iget-object v3, p0, LX/Izx;->b:LX/0Zb;

    const-string v4, "p2p_inconsistent_status"

    const-string v5, "p2p_settings"

    invoke-static {v4, v5}, Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;->d(Ljava/lang/String;Ljava/lang/String;)LX/5fz;

    move-result-object v4

    .line 2636184
    iget-object v5, v0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->b:Ljava/lang/String;

    move-object v5, v5

    .line 2636185
    iget-object p1, v4, LX/5fz;->a:Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;

    const-string p2, "transaction_id"

    invoke-virtual {p1, p2, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2636186
    move-object v4, v4

    .line 2636187
    invoke-virtual {v1}, LX/DtQ;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2636188
    iget-object v5, v4, LX/5fz;->a:Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;

    const-string p1, "local_status_on_client"

    invoke-virtual {v5, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2636189
    move-object v1, v4

    .line 2636190
    iget-object v4, v0, Lcom/facebook/payments/p2p/model/PaymentTransaction;->g:LX/DtQ;

    move-object v4, v4

    .line 2636191
    invoke-virtual {v4}, LX/DtQ;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2636192
    iget-object v5, v1, LX/5fz;->a:Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;

    const-string p1, "status_from_server"

    invoke-virtual {v5, p1, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2636193
    move-object v1, v1

    .line 2636194
    iget-object v4, v1, LX/5fz;->a:Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;

    move-object v1, v4

    .line 2636195
    invoke-interface {v3, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2636196
    :cond_2
    iget-object v1, p0, LX/Izx;->k:LX/Izl;

    invoke-virtual {v1, v0}, LX/Izl;->b(Lcom/facebook/payments/p2p/model/PaymentTransaction;)V

    move-object v0, v2

    .line 2636197
    goto :goto_0
.end method

.method public final g(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 8

    .prologue
    .line 2636155
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    .line 2636156
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2636157
    sget-object v2, Lcom/facebook/payments/p2p/service/model/cards/DeletePaymentCardParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/cards/DeletePaymentCardParams;

    .line 2636158
    iget-object v2, p0, LX/Izx;->e:LX/Izd;

    .line 2636159
    iget-wide v6, v0, Lcom/facebook/payments/p2p/service/model/cards/DeletePaymentCardParams;->b:J

    move-wide v4, v6

    .line 2636160
    iget-object v0, v2, LX/Izd;->b:LX/Izj;

    invoke-virtual {v0, v4, v5}, LX/Izj;->b(J)V

    .line 2636161
    iget-object v0, v2, LX/Izd;->a:LX/IzB;

    invoke-virtual {v0, v4, v5}, LX/IzB;->b(J)V

    .line 2636162
    return-object v1
.end method

.method public final h(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 8

    .prologue
    .line 2636145
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    .line 2636146
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2636147
    sget-object v2, Lcom/facebook/payments/p2p/service/model/cards/SetPrimaryCardParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/cards/SetPrimaryCardParams;

    .line 2636148
    iget-object v2, p0, LX/Izx;->e:LX/Izd;

    .line 2636149
    iget-wide v6, v0, Lcom/facebook/payments/p2p/service/model/cards/SetPrimaryCardParams;->b:J

    move-wide v4, v6

    .line 2636150
    iget-object v0, v2, LX/Izd;->b:LX/Izj;

    invoke-virtual {v0, v4, v5}, LX/Izj;->a(J)V

    .line 2636151
    iget-object v0, v2, LX/Izd;->a:LX/IzB;

    invoke-virtual {v0, v4, v5}, LX/IzB;->a(J)LX/0am;

    move-result-object v0

    .line 2636152
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2636153
    iget-object v3, v2, LX/Izd;->a:LX/IzB;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/PaymentCard;

    invoke-virtual {v3, v0}, LX/IzB;->a(Lcom/facebook/payments/p2p/model/PaymentCard;)V

    .line 2636154
    :cond_0
    return-object v1
.end method

.method public final i(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 2636129
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2636130
    sget-object v1, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityParams;

    .line 2636131
    iget-object v1, v0, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityParams;->b:LX/0rS;

    move-object v1, v1

    .line 2636132
    sget-object v2, LX/0rS;->PREFER_CACHE_IF_UP_TO_DATE:LX/0rS;

    if-ne v1, v2, :cond_0

    .line 2636133
    iget-object v1, p0, LX/Izx;->h:LX/Izq;

    .line 2636134
    iget-object v2, v0, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityParams;->c:Ljava/lang/String;

    move-object v2, v2

    .line 2636135
    invoke-virtual {v1, v2}, LX/Izq;->a(Ljava/lang/String;)LX/03R;

    move-result-object v1

    .line 2636136
    invoke-virtual {v1}, LX/03R;->isSet()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2636137
    new-instance v0, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityResult;

    invoke-virtual {v1}, LX/03R;->asBoolean()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityResult;-><init>(Ljava/lang/Boolean;)V

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2636138
    :goto_0
    return-object v0

    .line 2636139
    :cond_0
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    .line 2636140
    invoke-virtual {v2}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityResult;

    .line 2636141
    iget-object v3, p0, LX/Izx;->h:LX/Izq;

    .line 2636142
    iget-object p0, v0, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityParams;->c:Ljava/lang/String;

    move-object v0, p0

    .line 2636143
    invoke-virtual {v1}, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityResult;->a()Z

    move-result v1

    invoke-virtual {v3, v0, v1}, LX/Izq;->a(Ljava/lang/String;Z)V

    move-object v0, v2

    .line 2636144
    goto :goto_0
.end method

.method public final k(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 2636123
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2636124
    const-string v1, "fetchTransactionListParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListParams;

    .line 2636125
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    .line 2636126
    invoke-virtual {v2}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListResult;

    .line 2636127
    iget-object v3, p0, LX/Izx;->k:LX/Izl;

    invoke-virtual {v3, v0, v1}, LX/Izl;->a(Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListParams;Lcom/facebook/payments/p2p/service/model/transactions/FetchTransactionListResult;)V

    .line 2636128
    return-object v2
.end method

.method public final l(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 2636117
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2636118
    const-string v1, "fetchMoreTransactionsParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/transactions/FetchMoreTransactionsParams;

    .line 2636119
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    .line 2636120
    invoke-virtual {v2}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/p2p/service/model/transactions/FetchMoreTransactionsResult;

    .line 2636121
    iget-object v3, p0, LX/Izx;->k:LX/Izl;

    invoke-virtual {v3, v0, v1}, LX/Izl;->a(Lcom/facebook/payments/p2p/service/model/transactions/FetchMoreTransactionsParams;Lcom/facebook/payments/p2p/service/model/transactions/FetchMoreTransactionsResult;)V

    .line 2636122
    return-object v2
.end method

.method public final u(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2636104
    iget-object v0, p0, LX/Izx;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2636105
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2636106
    sget-object v1, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestParams;

    .line 2636107
    iget-object v1, v0, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestParams;->b:Ljava/lang/String;

    move-object v0, v1

    .line 2636108
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 2636109
    iget-object v2, p0, LX/Izx;->i:LX/Izg;

    invoke-virtual {v2, v0, v1}, LX/Izg;->a(J)Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;

    move-result-object v0

    .line 2636110
    if-eqz v0, :cond_0

    .line 2636111
    invoke-static {v0}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;->a(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;)Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2636112
    :goto_0
    return-object v0

    .line 2636113
    :cond_0
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    .line 2636114
    invoke-virtual {v1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;

    .line 2636115
    iget-object v2, p0, LX/Izx;->m:LX/Izk;

    invoke-virtual {v2, v0}, LX/Izk;->b(Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentRequestModel;)V

    move-object v0, v1

    .line 2636116
    goto :goto_0
.end method

.method public final v(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2636085
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2636086
    if-nez v0, :cond_0

    .line 2636087
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null params provided"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forException(Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2636088
    :goto_0
    return-object v0

    .line 2636089
    :cond_0
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2636090
    sget-object v1, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsParams;

    .line 2636091
    iget-object v1, p0, LX/Izx;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2636092
    iget-object v1, v0, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsParams;->b:LX/J1G;

    move-object v1, v1

    .line 2636093
    sget-object v2, LX/J1G;->INCOMING:LX/J1G;

    if-ne v1, v2, :cond_1

    .line 2636094
    iget-object v1, p0, LX/Izx;->i:LX/Izg;

    invoke-virtual {v1}, LX/Izg;->a()LX/0Px;

    move-result-object v1

    .line 2636095
    if-eqz v1, :cond_1

    .line 2636096
    new-instance v0, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsResult;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-direct {v0, v2}, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsResult;-><init>(Ljava/util/ArrayList;)V

    .line 2636097
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 2636098
    :cond_1
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    .line 2636099
    iget-object v2, v0, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsParams;->b:LX/J1G;

    move-object v0, v2

    .line 2636100
    sget-object v2, LX/J1G;->INCOMING:LX/J1G;

    if-ne v0, v2, :cond_2

    .line 2636101
    invoke-virtual {v1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsResult;

    .line 2636102
    iget-object v2, p0, LX/Izx;->m:LX/Izk;

    invoke-virtual {v0}, Lcom/facebook/payments/p2p/service/model/request/FetchPaymentRequestsResult;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/Izk;->a(LX/0Px;)V

    :cond_2
    move-object v0, v1

    .line 2636103
    goto :goto_0
.end method
