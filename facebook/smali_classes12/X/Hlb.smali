.class public final LX/Hlb;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2498780
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0nX;LX/HlT;Z)V
    .locals 4

    .prologue
    .line 2498781
    if-eqz p2, :cond_0

    .line 2498782
    invoke-virtual {p0}, LX/0nX;->f()V

    .line 2498783
    :cond_0
    iget-object v0, p1, LX/HlT;->a:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 2498784
    const-string v0, "age_ms"

    iget-object v1, p1, LX/HlT;->a:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p0, v0, v2, v3}, LX/0nX;->a(Ljava/lang/String;J)V

    .line 2498785
    :cond_1
    iget-object v0, p1, LX/HlT;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 2498786
    const-string v0, "rssi_dbm"

    iget-object v1, p1, LX/HlT;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/0nX;->a(Ljava/lang/String;I)V

    .line 2498787
    :cond_2
    iget-object v0, p1, LX/HlT;->c:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 2498788
    const-string v0, "hardware_address"

    iget-object v1, p1, LX/HlT;->c:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2498789
    :cond_3
    iget-object v0, p1, LX/HlT;->d:Ljava/util/List;

    if-eqz v0, :cond_a

    .line 2498790
    const-string v0, "payloads"

    invoke-virtual {p0, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2498791
    invoke-virtual {p0}, LX/0nX;->d()V

    .line 2498792
    iget-object v0, p1, LX/HlT;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HlU;

    .line 2498793
    if-eqz v0, :cond_4

    .line 2498794
    const/4 v2, 0x1

    .line 2498795
    if-eqz v2, :cond_5

    .line 2498796
    invoke-virtual {p0}, LX/0nX;->f()V

    .line 2498797
    :cond_5
    iget-object v3, v0, LX/HlU;->a:Ljava/lang/Integer;

    if-eqz v3, :cond_6

    .line 2498798
    const-string v3, "type"

    iget-object p1, v0, LX/HlU;->a:Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {p0, v3, p1}, LX/0nX;->a(Ljava/lang/String;I)V

    .line 2498799
    :cond_6
    iget-object v3, v0, LX/HlU;->b:Ljava/lang/String;

    if-eqz v3, :cond_7

    .line 2498800
    const-string v3, "data_base64"

    iget-object p1, v0, LX/HlU;->b:Ljava/lang/String;

    invoke-virtual {p0, v3, p1}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2498801
    :cond_7
    if-eqz v2, :cond_8

    .line 2498802
    invoke-virtual {p0}, LX/0nX;->g()V

    .line 2498803
    :cond_8
    goto :goto_0

    .line 2498804
    :cond_9
    invoke-virtual {p0}, LX/0nX;->e()V

    .line 2498805
    :cond_a
    if-eqz p2, :cond_b

    .line 2498806
    invoke-virtual {p0}, LX/0nX;->g()V

    .line 2498807
    :cond_b
    return-void
.end method
