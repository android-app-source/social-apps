.class public final LX/IXA;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/CsZ;

.field private final b:LX/Chv;

.field public final c:LX/1P1;

.field private final d:LX/Ci8;

.field private final e:LX/Chm;

.field public f:Z

.field public g:I

.field public h:I


# direct methods
.method public constructor <init>(LX/CsZ;LX/Chv;LX/1P1;)V
    .locals 2

    .prologue
    .line 2585578
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2585579
    new-instance v0, LX/IX6;

    invoke-direct {v0, p0}, LX/IX6;-><init>(LX/IXA;)V

    iput-object v0, p0, LX/IXA;->d:LX/Ci8;

    .line 2585580
    new-instance v0, LX/IX7;

    invoke-direct {v0, p0}, LX/IX7;-><init>(LX/IXA;)V

    iput-object v0, p0, LX/IXA;->e:LX/Chm;

    .line 2585581
    iput-object p1, p0, LX/IXA;->a:LX/CsZ;

    .line 2585582
    iput-object p2, p0, LX/IXA;->b:LX/Chv;

    .line 2585583
    iput-object p3, p0, LX/IXA;->c:LX/1P1;

    .line 2585584
    iget-object v0, p0, LX/IXA;->a:LX/CsZ;

    invoke-virtual {v0}, LX/CsZ;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/IXA;->g:I

    .line 2585585
    iget-object v0, p0, LX/IXA;->b:LX/Chv;

    iget-object v1, p0, LX/IXA;->d:LX/Ci8;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2585586
    iget-object v0, p0, LX/IXA;->b:LX/Chv;

    iget-object v1, p0, LX/IXA;->e:LX/Chm;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2585587
    return-void
.end method

.method public static a$redex0(LX/IXA;)V
    .locals 2

    .prologue
    .line 2585574
    iget-object v0, p0, LX/IXA;->b:LX/Chv;

    iget-object v1, p0, LX/IXA;->d:LX/Ci8;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 2585575
    iget-object v0, p0, LX/IXA;->b:LX/Chv;

    iget-object v1, p0, LX/IXA;->e:LX/Chm;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 2585576
    const/4 v0, 0x1

    new-instance v1, LX/IX8;

    invoke-direct {v1, p0}, LX/IX8;-><init>(LX/IXA;)V

    invoke-static {p0, v0, v1}, LX/IXA;->a$redex0(LX/IXA;ILandroid/animation/Animator$AnimatorListener;)V

    .line 2585577
    return-void
.end method

.method public static a$redex0(LX/IXA;ILandroid/animation/Animator$AnimatorListener;)V
    .locals 4

    .prologue
    .line 2585567
    iget-object v0, p0, LX/IXA;->a:LX/CsZ;

    invoke-virtual {v0}, LX/CsZ;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    if-nez p1, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 2585568
    if-eqz p2, :cond_1

    .line 2585569
    invoke-virtual {v0, p2}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 2585570
    :goto_1
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 2585571
    return-void

    .line 2585572
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2585573
    :cond_1
    new-instance v1, LX/IX9;

    invoke-direct {v1, p0, v0}, LX/IX9;-><init>(LX/IXA;Landroid/view/ViewPropertyAnimator;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    goto :goto_1
.end method
