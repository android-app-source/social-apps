.class public final synthetic LX/ItG;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I

.field public static final synthetic b:[I

.field public static final synthetic c:[I

.field public static final synthetic d:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2622254
    invoke-static {}, LX/6eh;->values()[LX/6eh;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/ItG;->d:[I

    :try_start_0
    sget-object v0, LX/ItG;->d:[I

    sget-object v1, LX/6eh;->VIDEO_CLIP:LX/6eh;

    invoke-virtual {v1}, LX/6eh;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_a

    :goto_0
    :try_start_1
    sget-object v0, LX/ItG;->d:[I

    sget-object v1, LX/6eh;->PHOTOS:LX/6eh;

    invoke-virtual {v1}, LX/6eh;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_9

    :goto_1
    :try_start_2
    sget-object v0, LX/ItG;->d:[I

    sget-object v1, LX/6eh;->AUDIO_CLIP:LX/6eh;

    invoke-virtual {v1}, LX/6eh;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_8

    .line 2622255
    :goto_2
    invoke-static {}, LX/2EU;->values()[LX/2EU;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/ItG;->c:[I

    :try_start_3
    sget-object v0, LX/ItG;->c:[I

    sget-object v1, LX/2EU;->CHANNEL_CONNECTED:LX/2EU;

    invoke-virtual {v1}, LX/2EU;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_7

    .line 2622256
    :goto_3
    invoke-static {}, LX/FHY;->values()[LX/FHY;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/ItG;->b:[I

    :try_start_4
    sget-object v0, LX/ItG;->b:[I

    sget-object v1, LX/FHY;->NOT_ALL_STARTED:LX/FHY;

    invoke-virtual {v1}, LX/FHY;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_6

    :goto_4
    :try_start_5
    sget-object v0, LX/ItG;->b:[I

    sget-object v1, LX/FHY;->FAILED:LX/FHY;

    invoke-virtual {v1}, LX/FHY;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    :goto_5
    :try_start_6
    sget-object v0, LX/ItG;->b:[I

    sget-object v1, LX/FHY;->SUCCEEDED:LX/FHY;

    invoke-virtual {v1}, LX/FHY;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_4

    :goto_6
    :try_start_7
    sget-object v0, LX/ItG;->b:[I

    sget-object v1, LX/FHY;->NO_MEDIA_ITEMS:LX/FHY;

    invoke-virtual {v1}, LX/FHY;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_3

    :goto_7
    :try_start_8
    sget-object v0, LX/ItG;->b:[I

    sget-object v1, LX/FHY;->IN_PROGRESS:LX/FHY;

    invoke-virtual {v1}, LX/FHY;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_2

    .line 2622257
    :goto_8
    invoke-static {}, LX/5e9;->values()[LX/5e9;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/ItG;->a:[I

    :try_start_9
    sget-object v0, LX/ItG;->a:[I

    sget-object v1, LX/5e9;->ONE_TO_ONE:LX/5e9;

    invoke-virtual {v1}, LX/5e9;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_1

    :goto_9
    :try_start_a
    sget-object v0, LX/ItG;->a:[I

    sget-object v1, LX/5e9;->GROUP:LX/5e9;

    invoke-virtual {v1}, LX/5e9;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_0

    :goto_a
    return-void

    :catch_0
    goto :goto_a

    :catch_1
    goto :goto_9

    :catch_2
    goto :goto_8

    :catch_3
    goto :goto_7

    :catch_4
    goto :goto_6

    :catch_5
    goto :goto_5

    :catch_6
    goto :goto_4

    :catch_7
    goto :goto_3

    :catch_8
    goto :goto_2

    :catch_9
    goto/16 :goto_1

    :catch_a
    goto/16 :goto_0
.end method
