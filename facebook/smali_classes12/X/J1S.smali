.class public final enum LX/J1S;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/J1S;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/J1S;

.field public static final enum BANNER_DISMISS:LX/J1S;

.field public static final enum MARK_AS_SOLD:LX/J1S;


# instance fields
.field public final mutation:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2638706
    new-instance v0, LX/J1S;

    const-string v1, "MARK_AS_SOLD"

    const-string v2, "p2p_platform_banner_mark_as_sold"

    invoke-direct {v0, v1, v3, v2}, LX/J1S;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J1S;->MARK_AS_SOLD:LX/J1S;

    .line 2638707
    new-instance v0, LX/J1S;

    const-string v1, "BANNER_DISMISS"

    const-string v2, "p2p_platform_banner_dismissal"

    invoke-direct {v0, v1, v4, v2}, LX/J1S;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J1S;->BANNER_DISMISS:LX/J1S;

    .line 2638708
    const/4 v0, 0x2

    new-array v0, v0, [LX/J1S;

    sget-object v1, LX/J1S;->MARK_AS_SOLD:LX/J1S;

    aput-object v1, v0, v3

    sget-object v1, LX/J1S;->BANNER_DISMISS:LX/J1S;

    aput-object v1, v0, v4

    sput-object v0, LX/J1S;->$VALUES:[LX/J1S;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2638709
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2638710
    iput-object p3, p0, LX/J1S;->mutation:Ljava/lang/String;

    .line 2638711
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/J1S;
    .locals 1

    .prologue
    .line 2638712
    const-class v0, LX/J1S;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/J1S;

    return-object v0
.end method

.method public static values()[LX/J1S;
    .locals 1

    .prologue
    .line 2638713
    sget-object v0, LX/J1S;->$VALUES:[LX/J1S;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/J1S;

    return-object v0
.end method
