.class public LX/I4X;
.super LX/3x6;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public a:LX/Cju;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/CkJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final c:Landroid/content/Context;

.field private final d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2534185
    invoke-direct {p0}, LX/3x6;-><init>()V

    .line 2534186
    const-class v0, LX/I4X;

    invoke-static {v0, p0, p1}, LX/I4X;->a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V

    .line 2534187
    iput-object p1, p0, LX/I4X;->c:Landroid/content/Context;

    .line 2534188
    iget-object v0, p0, LX/I4X;->a:LX/Cju;

    const v1, 0x7f0d011e

    invoke-interface {v0, v1}, LX/Cju;->c(I)I

    move-result v0

    iput v0, p0, LX/I4X;->d:I

    .line 2534189
    return-void
.end method

.method private a(Landroid/view/View;LX/Clr;LX/Clr;)I
    .locals 7

    .prologue
    .line 2534209
    invoke-static {p2}, LX/Cjt;->from(LX/Clr;)LX/Cjt;

    move-result-object v1

    .line 2534210
    invoke-static {p3}, LX/Cjt;->from(LX/Clr;)LX/Cjt;

    move-result-object v4

    .line 2534211
    iget-object v0, p0, LX/I4X;->b:LX/CkJ;

    const/4 v6, 0x0

    move-object v2, p2

    move-object v3, p1

    move-object v5, p3

    invoke-virtual/range {v0 .. v6}, LX/CkJ;->a(LX/Cjt;LX/Clr;Landroid/view/View;LX/Cjt;LX/Clr;Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method private static a(LX/CoJ;I)LX/Clr;
    .locals 1

    .prologue
    .line 2534206
    if-ltz p1, :cond_0

    invoke-virtual {p0}, LX/1OM;->ij_()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 2534207
    :cond_0
    const/4 v0, 0x0

    .line 2534208
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0, p1}, LX/CoJ;->e(I)LX/Clr;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-static {p2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/I4X;

    invoke-static {p0}, LX/Cjv;->a(LX/0QB;)LX/Cjv;

    move-result-object v0

    check-cast v0, LX/Cju;

    invoke-static {p0}, LX/CkJ;->a(LX/0QB;)LX/CkJ;

    move-result-object p0

    check-cast p0, LX/CkJ;

    iput-object v0, p1, LX/I4X;->a:LX/Cju;

    iput-object p0, p1, LX/I4X;->b:LX/CkJ;

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Rect;Landroid/view/View;Landroid/support/v7/widget/RecyclerView;LX/1Ok;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2534190
    invoke-static {p2}, Landroid/support/v7/widget/RecyclerView;->d(Landroid/view/View;)I

    move-result v1

    .line 2534191
    iget-object v0, p3, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v0

    .line 2534192
    check-cast v0, LX/CoJ;

    .line 2534193
    invoke-static {v0, v1}, LX/I4X;->a(LX/CoJ;I)LX/Clr;

    move-result-object v2

    .line 2534194
    const/4 v3, 0x0

    .line 2534195
    invoke-interface {v2}, LX/Clr;->lx_()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 2534196
    iget-object v4, p0, LX/I4X;->c:Landroid/content/Context;

    if-eqz v4, :cond_0

    iget-object v3, p0, LX/I4X;->c:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b23ba

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    :cond_0
    :pswitch_0
    move v3, v3

    .line 2534197
    const/4 v4, 0x0

    .line 2534198
    invoke-interface {v2}, LX/Clr;->lx_()I

    move-result v5

    packed-switch v5, :pswitch_data_1

    .line 2534199
    iget-object v5, p0, LX/I4X;->c:Landroid/content/Context;

    if-eqz v5, :cond_1

    iget-object v4, p0, LX/I4X;->c:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b23bb

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    :cond_1
    :pswitch_1
    move v4, v4

    .line 2534200
    iget-object v5, p3, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v5, v5

    .line 2534201
    invoke-virtual {v5}, LX/1OM;->ij_()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-ne v1, v5, :cond_2

    .line 2534202
    iget v0, p0, LX/I4X;->d:I

    invoke-virtual {p1, v3, v6, v4, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 2534203
    :goto_0
    return-void

    .line 2534204
    :cond_2
    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, LX/I4X;->a(LX/CoJ;I)LX/Clr;

    move-result-object v0

    .line 2534205
    invoke-direct {p0, p2, v2, v0}, LX/I4X;->a(Landroid/view/View;LX/Clr;LX/Clr;)I

    move-result v0

    invoke-virtual {p1, v3, v6, v4, v0}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x194
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x194
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
