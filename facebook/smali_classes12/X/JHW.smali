.class public LX/JHW;
.super Landroid/view/SurfaceView;
.source ""

# interfaces
.implements LX/5pQ;
.implements LX/0KW;


# instance fields
.field public final a:Landroid/os/Handler;

.field public final b:Ljava/lang/Runnable;

.field private c:Z

.field public d:I

.field public e:Landroid/net/Uri;

.field public f:Z

.field public g:LX/0Kx;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public h:LX/JHU;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public i:Z

.field public j:LX/0GT;

.field public k:Z

.field public l:F

.field public m:Ljava/lang/String;

.field public n:Z

.field private final o:LX/0Ju;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2673157
    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    .line 2673158
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LX/JHW;->a:Landroid/os/Handler;

    .line 2673159
    new-instance v0, Lcom/facebook/catalyst/views/video/ReactVideoPlayer$1;

    invoke-direct {v0, p0}, Lcom/facebook/catalyst/views/video/ReactVideoPlayer$1;-><init>(LX/JHW;)V

    iput-object v0, p0, LX/JHW;->b:Ljava/lang/Runnable;

    .line 2673160
    new-instance v0, LX/JHV;

    invoke-direct {v0, p0}, LX/JHV;-><init>(LX/JHW;)V

    iput-object v0, p0, LX/JHW;->o:LX/0Ju;

    .line 2673161
    const/4 v0, 0x2

    .line 2673162
    new-instance v1, LX/0Kz;

    const/16 v2, 0x9c4

    const/16 p1, 0x1388

    invoke-direct {v1, v0, v2, p1}, LX/0Kz;-><init>(III)V

    move-object v0, v1

    .line 2673163
    iput-object v0, p0, LX/JHW;->g:LX/0Kx;

    .line 2673164
    iget-object v0, p0, LX/JHW;->g:LX/0Kx;

    invoke-interface {v0, p0}, LX/0Kx;->a(LX/0KW;)V

    .line 2673165
    return-void
.end method

.method public static h(LX/JHW;)V
    .locals 14

    .prologue
    const/4 v11, 0x2

    const/4 v13, 0x0

    const/4 v12, 0x1

    .line 2673145
    iget-object v0, p0, LX/JHW;->g:LX/0Kx;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2673146
    new-instance v0, LX/0MK;

    iget-object v1, p0, LX/JHW;->e:Landroid/net/Uri;

    new-instance v2, LX/0OD;

    const-string v3, "ExoHttpSource"

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, LX/0OD;-><init>(Ljava/lang/String;LX/0OH;)V

    new-instance v3, LX/0OB;

    const/high16 v4, 0x10000

    invoke-direct {v3, v4}, LX/0OB;-><init>(I)V

    const/high16 v4, 0x200000

    new-array v5, v12, [LX/0ME;

    new-instance v6, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;

    invoke-direct {v6}, Lcom/google/android/exoplayer/extractor/mp4/Mp4Extractor;-><init>()V

    aput-object v6, v5, v13

    invoke-direct/range {v0 .. v5}, LX/0MK;-><init>(Landroid/net/Uri;LX/0G6;LX/0O1;I[LX/0ME;)V

    .line 2673147
    iget-object v1, p0, LX/JHW;->m:Ljava/lang/String;

    const-string v2, "cover"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    move v5, v11

    .line 2673148
    :goto_0
    new-instance v1, LX/0Jw;

    invoke-virtual {p0}, LX/JHW;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v4, LX/0L1;->a:LX/0L1;

    const-wide/16 v6, 0x0

    iget-object v8, p0, LX/JHW;->a:Landroid/os/Handler;

    iget-object v9, p0, LX/JHW;->o:LX/0Ju;

    const/4 v10, -0x1

    move-object v3, v0

    invoke-direct/range {v1 .. v10}, LX/0Jw;-><init>(Landroid/content/Context;LX/0L9;LX/0L1;IJLandroid/os/Handler;LX/0Ju;I)V

    .line 2673149
    new-instance v2, LX/0GX;

    sget-object v3, LX/0L1;->a:LX/0L1;

    invoke-direct {v2, v0, v3}, LX/0GX;-><init>(LX/0L9;LX/0L1;)V

    iput-object v2, p0, LX/JHW;->j:LX/0GT;

    .line 2673150
    iget-object v0, p0, LX/JHW;->g:LX/0Kx;

    new-array v2, v11, [LX/0GT;

    aput-object v1, v2, v13

    iget-object v3, p0, LX/JHW;->j:LX/0GT;

    aput-object v3, v2, v12

    invoke-interface {v0, v2}, LX/0Kx;->a([LX/0GT;)V

    .line 2673151
    iget-object v0, p0, LX/JHW;->g:LX/0Kx;

    invoke-virtual {p0}, LX/JHW;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v2

    invoke-interface {v0, v1, v12, v2}, LX/0Kx;->a(LX/0GS;ILjava/lang/Object;)V

    .line 2673152
    iget v0, p0, LX/JHW;->d:I

    if-lez v0, :cond_0

    .line 2673153
    iget v0, p0, LX/JHW;->d:I

    invoke-virtual {p0, v0}, LX/JHW;->a(I)V

    .line 2673154
    iput v13, p0, LX/JHW;->d:I

    .line 2673155
    :cond_0
    iput-boolean v12, p0, LX/JHW;->n:Z

    .line 2673156
    return-void

    :cond_1
    move v5, v12

    goto :goto_0
.end method


# virtual methods
.method public final a(I)V
    .locals 4

    .prologue
    .line 2673142
    iget-object v0, p0, LX/JHW;->g:LX/0Kx;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2673143
    iget-object v0, p0, LX/JHW;->g:LX/0Kx;

    mul-int/lit16 v1, p1, 0x3e8

    int-to-long v2, v1

    invoke-interface {v0, v2, v3}, LX/0Kx;->a(J)V

    .line 2673144
    return-void
.end method

.method public final a(LX/0Kv;)V
    .locals 1

    .prologue
    .line 2673140
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/JHW;->f:Z

    .line 2673141
    return-void
.end method

.method public final a(ZI)V
    .locals 6

    .prologue
    .line 2673119
    iget-object v0, p0, LX/JHW;->h:LX/JHU;

    if-nez v0, :cond_0

    .line 2673120
    :goto_0
    return-void

    .line 2673121
    :cond_0
    const/4 v0, 0x1

    if-ne p2, v0, :cond_3

    .line 2673122
    iget-boolean v0, p0, LX/JHW;->f:Z

    if-eqz v0, :cond_2

    sget-object v0, LX/JHX;->ERROR:LX/JHX;

    .line 2673123
    :goto_1
    move-object v0, v0

    .line 2673124
    iget-object v1, p0, LX/JHW;->h:LX/JHU;

    .line 2673125
    invoke-static {}, LX/5op;->b()LX/5pH;

    move-result-object v3

    .line 2673126
    const-string v2, "target"

    iget-object v4, v1, LX/JHU;->a:LX/JHW;

    invoke-virtual {v4}, LX/JHW;->getId()I

    move-result v4

    invoke-interface {v3, v2, v4}, LX/5pH;->putInt(Ljava/lang/String;I)V

    .line 2673127
    const-string v2, "state"

    invoke-virtual {v0}, LX/JHX;->ordinal()I

    move-result v4

    invoke-interface {v3, v2, v4}, LX/5pH;->putInt(Ljava/lang/String;I)V

    .line 2673128
    iget-object v2, v1, LX/JHU;->b:LX/5rJ;

    const-class v4, Lcom/facebook/react/uimanager/events/RCTEventEmitter;

    invoke-virtual {v2, v4}, LX/5pX;->a(Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;

    move-result-object v2

    check-cast v2, Lcom/facebook/react/uimanager/events/RCTEventEmitter;

    iget-object v4, v1, LX/JHU;->a:LX/JHW;

    invoke-virtual {v4}, LX/JHW;->getId()I

    move-result v4

    const-string v5, "topVideoStateChange"

    invoke-interface {v2, v4, v5, v3}, Lcom/facebook/react/uimanager/events/RCTEventEmitter;->receiveEvent(ILjava/lang/String;LX/5pH;)V

    .line 2673129
    const/4 v0, 0x4

    if-ne p2, v0, :cond_1

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {p0, v0}, LX/JHW;->setPeriodicUpdatesEnabled(Z)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    .line 2673130
    :cond_2
    sget-object v0, LX/JHX;->IDLE:LX/JHX;

    goto :goto_1

    .line 2673131
    :cond_3
    const/4 v0, 0x2

    if-ne p2, v0, :cond_4

    .line 2673132
    sget-object v0, LX/JHX;->PREPARING:LX/JHX;

    goto :goto_1

    .line 2673133
    :cond_4
    const/4 v0, 0x3

    if-ne p2, v0, :cond_5

    .line 2673134
    sget-object v0, LX/JHX;->BUFFERING:LX/JHX;

    goto :goto_1

    .line 2673135
    :cond_5
    const/4 v0, 0x4

    if-ne p2, v0, :cond_7

    .line 2673136
    if-eqz p1, :cond_6

    sget-object v0, LX/JHX;->PLAYING:LX/JHX;

    goto :goto_1

    :cond_6
    sget-object v0, LX/JHX;->READY:LX/JHX;

    goto :goto_1

    .line 2673137
    :cond_7
    const/4 v0, 0x5

    if-ne p2, v0, :cond_8

    .line 2673138
    sget-object v0, LX/JHX;->ENDED:LX/JHX;

    goto :goto_1

    .line 2673139
    :cond_8
    sget-object v0, LX/JHX;->UNDEFINED:LX/JHX;

    goto :goto_1
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2673114
    iget-object v0, p0, LX/JHW;->g:LX/0Kx;

    if-eqz v0, :cond_0

    .line 2673115
    iget-object v0, p0, LX/JHW;->g:LX/0Kx;

    invoke-interface {v0}, LX/0Kx;->d()V

    .line 2673116
    const/4 v0, 0x0

    iput-object v0, p0, LX/JHW;->g:LX/0Kx;

    .line 2673117
    :cond_0
    iget-object v0, p0, LX/JHW;->a:Landroid/os/Handler;

    iget-object v1, p0, LX/JHW;->b:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 2673118
    return-void
.end method

.method public final bM_()V
    .locals 1

    .prologue
    .line 2673109
    iget-object v0, p0, LX/JHW;->g:LX/0Kx;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2673110
    iget-boolean v0, p0, LX/JHW;->c:Z

    if-eqz v0, :cond_0

    .line 2673111
    invoke-virtual {p0}, LX/JHW;->f()V

    .line 2673112
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/JHW;->c:Z

    .line 2673113
    :cond_0
    return-void
.end method

.method public final bN_()V
    .locals 1

    .prologue
    .line 2673088
    iget-object v0, p0, LX/JHW;->g:LX/0Kx;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2673089
    iget-object v0, p0, LX/JHW;->g:LX/0Kx;

    invoke-interface {v0}, LX/0Kx;->b()Z

    move-result v0

    iput-boolean v0, p0, LX/JHW;->c:Z

    .line 2673090
    invoke-virtual {p0}, LX/JHW;->g()V

    .line 2673091
    return-void
.end method

.method public final bO_()V
    .locals 1

    .prologue
    .line 2673106
    iget-object v0, p0, LX/JHW;->g:LX/0Kx;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2673107
    invoke-virtual {p0}, LX/JHW;->b()V

    .line 2673108
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2673102
    iget-object v0, p0, LX/JHW;->g:LX/0Kx;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2673103
    iget-object v0, p0, LX/JHW;->g:LX/0Kx;

    invoke-interface {v0, v1}, LX/0Kx;->a(Z)V

    .line 2673104
    invoke-virtual {p0, v1}, LX/JHW;->setPeriodicUpdatesEnabled(Z)V

    .line 2673105
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2673098
    iget-object v0, p0, LX/JHW;->g:LX/0Kx;

    invoke-static {v0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2673099
    iget-object v0, p0, LX/JHW;->g:LX/0Kx;

    invoke-interface {v0, v1}, LX/0Kx;->a(Z)V

    .line 2673100
    invoke-virtual {p0, v1}, LX/JHW;->setPeriodicUpdatesEnabled(Z)V

    .line 2673101
    return-void
.end method

.method public setPeriodicUpdatesEnabled(Z)V
    .locals 3

    .prologue
    .line 2673092
    iget-boolean v0, p0, LX/JHW;->i:Z

    if-ne v0, p1, :cond_1

    .line 2673093
    :cond_0
    :goto_0
    return-void

    .line 2673094
    :cond_1
    iput-boolean p1, p0, LX/JHW;->i:Z

    .line 2673095
    iget-boolean v0, p0, LX/JHW;->i:Z

    if-eqz v0, :cond_0

    .line 2673096
    iget-object v0, p0, LX/JHW;->a:Landroid/os/Handler;

    iget-object v1, p0, LX/JHW;->b:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 2673097
    iget-object v0, p0, LX/JHW;->a:Landroid/os/Handler;

    iget-object v1, p0, LX/JHW;->b:Ljava/lang/Runnable;

    const v2, -0x3302a096

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method
