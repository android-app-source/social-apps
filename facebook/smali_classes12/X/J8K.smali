.class public LX/J8K;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:Ljava/lang/Runnable;

.field private static b:Landroid/os/Handler;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2651969
    sput-object v0, LX/J8K;->a:Ljava/lang/Runnable;

    .line 2651970
    sput-object v0, LX/J8K;->b:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2651968
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/J8M;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2651954
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/J8M;

    .line 2651955
    invoke-interface {v0}, LX/J8M;->set()V

    goto :goto_0

    .line 2651956
    :cond_0
    new-instance v0, LX/J8X;

    invoke-direct {v0}, LX/J8X;-><init>()V

    .line 2651957
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v1, v2, :cond_3

    .line 2651958
    const-class v1, LX/J8K;

    monitor-enter v1

    .line 2651959
    :try_start_0
    sget-object v2, LX/J8K;->b:Landroid/os/Handler;

    if-nez v2, :cond_1

    .line 2651960
    new-instance v2, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v2, LX/J8K;->b:Landroid/os/Handler;

    .line 2651961
    :cond_1
    sget-object v2, LX/J8K;->a:Ljava/lang/Runnable;

    if-eqz v2, :cond_2

    .line 2651962
    sget-object v2, LX/J8K;->b:Landroid/os/Handler;

    sget-object v3, LX/J8K;->a:Ljava/lang/Runnable;

    invoke-static {v2, v3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 2651963
    :cond_2
    new-instance v2, Lcom/facebook/strictmode/StrictModeHelper$1;

    invoke-direct {v2, v0}, Lcom/facebook/strictmode/StrictModeHelper$1;-><init>(LX/J8X;)V

    sput-object v2, LX/J8K;->a:Ljava/lang/Runnable;

    .line 2651964
    sget-object v0, LX/J8K;->b:Landroid/os/Handler;

    sget-object v2, LX/J8K;->a:Ljava/lang/Runnable;

    const v3, -0xd6a7a49

    invoke-static {v0, v2, v3}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 2651965
    monitor-exit v1

    .line 2651966
    :cond_3
    return-void

    .line 2651967
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
