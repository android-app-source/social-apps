.class public LX/Imm;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/2Oi;

.field private b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0ad;

.field private final d:LX/Imi;

.field private final e:LX/3QW;


# direct methods
.method public constructor <init>(LX/2Oi;LX/0Or;LX/0ad;LX/Imi;LX/3QW;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/payments/p2p/config/AreP2pPaymentsSendEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2Oi;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0ad;",
            "LX/Imi;",
            "LX/3QW;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2610165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2610166
    iput-object p1, p0, LX/Imm;->a:LX/2Oi;

    .line 2610167
    iput-object p2, p0, LX/Imm;->b:LX/0Or;

    .line 2610168
    iput-object p3, p0, LX/Imm;->c:LX/0ad;

    .line 2610169
    iput-object p4, p0, LX/Imm;->d:LX/Imi;

    .line 2610170
    iput-object p5, p0, LX/Imm;->e:LX/3QW;

    .line 2610171
    return-void
.end method
