.class public final LX/I7F;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field public final synthetic a:Lcom/facebook/events/permalink/EventPermalinkFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/permalink/EventPermalinkFragment;)V
    .locals 0

    .prologue
    .line 2538704
    iput-object p1, p0, LX/I7F;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, -0x3e93da91

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2538705
    iget-object v1, p0, LX/I7F;->a:Lcom/facebook/events/permalink/EventPermalinkFragment;

    const/16 p1, 0x8

    const/4 p0, 0x0

    .line 2538706
    invoke-static {v1}, Lcom/facebook/events/permalink/EventPermalinkFragment;->I(Lcom/facebook/events/permalink/EventPermalinkFragment;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2538707
    :cond_0
    :goto_0
    const/16 v1, 0x27

    const v2, 0x4fa4be90

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2538708
    :cond_1
    iget-object v2, v1, Lcom/facebook/events/permalink/EventPermalinkFragment;->y:LX/0kb;

    invoke-virtual {v2}, LX/0kb;->d()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2538709
    invoke-static {v1, p1}, Lcom/facebook/events/permalink/EventPermalinkFragment;->d(Lcom/facebook/events/permalink/EventPermalinkFragment;I)V

    .line 2538710
    iget-object v2, v1, Lcom/facebook/events/permalink/EventPermalinkFragment;->aX:Lcom/facebook/events/model/Event;

    if-nez v2, :cond_2

    .line 2538711
    invoke-static {v1, p0}, Lcom/facebook/events/permalink/EventPermalinkFragment;->a(Lcom/facebook/events/permalink/EventPermalinkFragment;I)V

    .line 2538712
    :cond_2
    iget-object v2, v1, Lcom/facebook/events/permalink/EventPermalinkFragment;->aj:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventPermalinkFragmentModel;

    if-nez v2, :cond_0

    .line 2538713
    invoke-static {v1}, Lcom/facebook/events/permalink/EventPermalinkFragment;->E(Lcom/facebook/events/permalink/EventPermalinkFragment;)V

    goto :goto_0

    .line 2538714
    :cond_3
    invoke-static {v1, p1}, Lcom/facebook/events/permalink/EventPermalinkFragment;->a(Lcom/facebook/events/permalink/EventPermalinkFragment;I)V

    .line 2538715
    iget-object v2, v1, Lcom/facebook/events/permalink/EventPermalinkFragment;->aX:Lcom/facebook/events/model/Event;

    if-nez v2, :cond_0

    .line 2538716
    invoke-static {v1, p0}, Lcom/facebook/events/permalink/EventPermalinkFragment;->d(Lcom/facebook/events/permalink/EventPermalinkFragment;I)V

    goto :goto_0
.end method
