.class public LX/HOT;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""


# instance fields
.field public a:Landroid/view/View$OnClickListener;

.field public b:Landroid/widget/TextView;

.field public c:LX/0ht;

.field public d:LX/HO8;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2460070
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2460071
    new-instance v0, LX/HOS;

    invoke-direct {v0, p0}, LX/HOS;-><init>(LX/HOT;)V

    iput-object v0, p0, LX/HOT;->a:Landroid/view/View$OnClickListener;

    .line 2460072
    const v0, 0x7f030e21

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2460073
    const v0, 0x7f0d2291

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/HOT;->b:Landroid/widget/TextView;

    .line 2460074
    iget-object v0, p0, LX/HOT;->b:Landroid/widget/TextView;

    iget-object p1, p0, LX/HOT;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2460075
    return-void
.end method


# virtual methods
.method public final onMeasure(II)V
    .locals 6

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    const/high16 v4, 0x40000000    # 2.0f

    .line 2460076
    invoke-virtual {p0}, LX/HOT;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2460077
    const v1, 0x7f0b0e34

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    .line 2460078
    const v2, 0x7f0b0e35

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    .line 2460079
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v3, v3

    mul-float/2addr v1, v4

    sub-float v1, v3, v1

    .line 2460080
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v0, v0

    mul-float/2addr v2, v4

    sub-float/2addr v0, v2

    .line 2460081
    float-to-int v1, v1

    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 2460082
    float-to-int v0, v0

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 2460083
    invoke-super {p0, v1, v0}, Lcom/facebook/widget/CustomRelativeLayout;->onMeasure(II)V

    .line 2460084
    return-void
.end method
