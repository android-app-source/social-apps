.class public LX/JUM;
.super Landroid/graphics/drawable/Drawable;
.source ""


# instance fields
.field private a:Landroid/graphics/Paint;

.field private b:Landroid/graphics/drawable/Drawable;

.field private c:I

.field private d:I

.field private e:I

.field private f:I


# direct methods
.method public constructor <init>(Landroid/content/Context;IIII)V
    .locals 2

    .prologue
    .line 2698110
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 2698111
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/JUM;->a:Landroid/graphics/Paint;

    .line 2698112
    iget-object v0, p0, LX/JUM;->a:Landroid/graphics/Paint;

    invoke-static {p1, p3}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2698113
    iget-object v0, p0, LX/JUM;->a:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 2698114
    new-instance v0, LX/0wM;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0wM;-><init>(Landroid/content/res/Resources;)V

    .line 2698115
    const v1, 0x7f0a00d5

    invoke-static {p1, v1}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, p2, v1}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/JUM;->b:Landroid/graphics/drawable/Drawable;

    .line 2698116
    int-to-float v0, p4

    invoke-static {p1, v0}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, LX/JUM;->c:I

    .line 2698117
    int-to-float v0, p5

    invoke-static {p1, v0}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, LX/JUM;->d:I

    .line 2698118
    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 2698105
    iget v0, p0, LX/JUM;->c:I

    div-int/lit8 v0, v0, 0x2

    .line 2698106
    iget v1, p0, LX/JUM;->e:I

    add-int/2addr v1, v0

    int-to-float v1, v1

    iget v2, p0, LX/JUM;->f:I

    add-int/2addr v2, v0

    int-to-float v2, v2

    int-to-float v0, v0

    iget-object v3, p0, LX/JUM;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v0, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 2698107
    iget-object v0, p0, LX/JUM;->b:Landroid/graphics/drawable/Drawable;

    iget v1, p0, LX/JUM;->e:I

    iget v2, p0, LX/JUM;->d:I

    add-int/2addr v1, v2

    iget v2, p0, LX/JUM;->f:I

    iget v3, p0, LX/JUM;->d:I

    add-int/2addr v2, v3

    iget v3, p0, LX/JUM;->e:I

    iget v4, p0, LX/JUM;->c:I

    add-int/2addr v3, v4

    iget v4, p0, LX/JUM;->d:I

    sub-int/2addr v3, v4

    iget v4, p0, LX/JUM;->f:I

    iget v5, p0, LX/JUM;->c:I

    add-int/2addr v4, v5

    iget v5, p0, LX/JUM;->d:I

    sub-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2698108
    iget-object v0, p0, LX/JUM;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 2698109
    return-void
.end method

.method public final getIntrinsicHeight()I
    .locals 1

    .prologue
    .line 2698104
    iget v0, p0, LX/JUM;->c:I

    return v0
.end method

.method public final getIntrinsicWidth()I
    .locals 1

    .prologue
    .line 2698119
    iget v0, p0, LX/JUM;->c:I

    return v0
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 2698103
    const/4 v0, -0x3

    return v0
.end method

.method public final onBoundsChange(Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 2698100
    iget v0, p1, Landroid/graphics/Rect;->left:I

    iput v0, p0, LX/JUM;->e:I

    .line 2698101
    iget v0, p1, Landroid/graphics/Rect;->top:I

    iput v0, p0, LX/JUM;->f:I

    .line 2698102
    return-void
.end method

.method public final setAlpha(I)V
    .locals 1

    .prologue
    .line 2698098
    iget-object v0, p0, LX/JUM;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 2698099
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1
    .param p1    # Landroid/graphics/ColorFilter;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2698096
    iget-object v0, p0, LX/JUM;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 2698097
    return-void
.end method
