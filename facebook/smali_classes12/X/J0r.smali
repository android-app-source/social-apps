.class public final LX/J0r;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/payments/p2p/service/model/cards/FetchPaymentCardsResult;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2637977
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2637978
    new-instance v0, Lcom/facebook/payments/p2p/service/model/cards/FetchPaymentCardsResult;

    invoke-direct {v0, p1}, Lcom/facebook/payments/p2p/service/model/cards/FetchPaymentCardsResult;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2637979
    new-array v0, p1, [Lcom/facebook/payments/p2p/service/model/cards/FetchPaymentCardsResult;

    return-object v0
.end method
