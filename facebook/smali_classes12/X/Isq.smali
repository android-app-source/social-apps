.class public LX/Isq;
.super LX/6Lb;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6Lb",
        "<",
        "Ljava/lang/Void;",
        "LX/Isp;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/3LP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/3MU;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/3NF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/3Km;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/2Oq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/3Lv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/2RQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/Iss;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final k:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "LX/Ish;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2621401
    invoke-direct {p0, p1}, LX/6Lb;-><init>(Ljava/util/concurrent/Executor;)V

    .line 2621402
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/Isq;->k:Ljava/util/LinkedList;

    .line 2621403
    return-void
.end method

.method public static a$redex0(LX/Isq;LX/0Px;LX/0Px;)LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/Ish;",
            ">;",
            "LX/0Px",
            "<",
            "LX/Ish;",
            ">;)",
            "LX/0Px",
            "<",
            "LX/Ish;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2621390
    const-string v0, "getRecentSearches"

    const v2, -0x5727779c

    invoke-static {v0, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2621391
    :try_start_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_1

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ish;

    .line 2621392
    iget-object v4, p0, LX/Isq;->k:Ljava/util/LinkedList;

    invoke-virtual {v4, v0}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2621393
    iget-object v4, p0, LX/Isq;->k:Ljava/util/LinkedList;

    invoke-virtual {v4, v0}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 2621394
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2621395
    :cond_1
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v2

    :goto_1
    if-ge v1, v2, :cond_3

    invoke-virtual {p2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ish;

    .line 2621396
    iget-object v3, p0, LX/Isq;->k:Ljava/util/LinkedList;

    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2621397
    iget-object v3, p0, LX/Isq;->k:Ljava/util/LinkedList;

    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 2621398
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2621399
    :cond_3
    iget-object v0, p0, LX/Isq;->k:Ljava/util/LinkedList;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2621400
    const v1, 0x2029c08b

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    :catchall_0
    move-exception v0

    const v1, 0x8a159e8

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public static g(LX/Isq;)Landroid/util/Pair;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Pair",
            "<",
            "LX/0Px",
            "<",
            "LX/Ish;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2621366
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2621367
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2621368
    invoke-static {p0}, LX/Isq;->i(LX/Isq;)Ljava/util/List;

    move-result-object v0

    .line 2621369
    iget-object v1, p0, LX/Isq;->g:LX/2Oq;

    invoke-virtual {v1}, LX/2Oq;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2621370
    iget-object v1, p0, LX/Isq;->h:LX/3Lv;

    const/16 v4, 0xa

    sget-object v5, LX/3Oo;->NULL_STATE:LX/3Oo;

    invoke-virtual {v1, v4, v5}, LX/3Lv;->a(ILX/3Oo;)LX/0Px;

    move-result-object v1

    .line 2621371
    invoke-static {v1}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2621372
    :goto_0
    move-object v0, v0

    .line 2621373
    :cond_0
    move-object v1, v0

    .line 2621374
    const/4 v0, 0x0

    .line 2621375
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2621376
    const/16 v5, 0x28

    if-ge v1, v5, :cond_2

    .line 2621377
    const/16 v5, 0x14

    if-ge v1, v5, :cond_1

    .line 2621378
    new-instance v5, LX/Ish;

    sget-object v6, LX/Isg;->TOP_FRIENDS:LX/Isg;

    invoke-direct {v5, v0, v6}, LX/Ish;-><init>(Lcom/facebook/user/model/User;LX/Isg;)V

    invoke-virtual {v2, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2621379
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 2621380
    goto :goto_1

    .line 2621381
    :cond_1
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2

    .line 2621382
    :cond_2
    new-instance v0, Landroid/util/Pair;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0

    .line 2621383
    :cond_3
    invoke-static {v0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2621384
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto :goto_0

    .line 2621385
    :cond_4
    new-instance v4, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result p0

    add-int/2addr v5, p0

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 2621386
    invoke-interface {v4, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2621387
    invoke-interface {v4, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2621388
    new-instance v5, LX/DAp;

    invoke-direct {v5, v4}, LX/DAp;-><init>(Ljava/util/Collection;)V

    invoke-static {v4, v5}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    move-object v0, v4

    .line 2621389
    goto :goto_0
.end method

.method public static i(LX/Isq;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2621350
    const-string v0, "getTopFriends"

    const v1, -0x1a9376fa

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2621351
    :try_start_0
    iget-object v0, p0, LX/Isq;->i:LX/2RQ;

    sget-object v1, LX/2RU;->MESSAGABLE_TYPES:LX/0Px;

    const/16 v2, 0x28

    .line 2621352
    invoke-virtual {v0}, LX/2RQ;->a()LX/2RR;

    move-result-object v3

    .line 2621353
    iput-object v1, v3, LX/2RR;->b:Ljava/util/Collection;

    .line 2621354
    move-object v3, v3

    .line 2621355
    sget-object v4, LX/2RS;->PHAT_RANK:LX/2RS;

    .line 2621356
    iput-object v4, v3, LX/2RR;->n:LX/2RS;

    .line 2621357
    move-object v3, v3

    .line 2621358
    const/4 v4, 0x1

    .line 2621359
    iput-boolean v4, v3, LX/2RR;->o:Z

    .line 2621360
    move-object v3, v3

    .line 2621361
    iput v2, v3, LX/2RR;->p:I

    .line 2621362
    move-object v3, v3

    .line 2621363
    move-object v0, v3

    .line 2621364
    iget-object v1, p0, LX/Isq;->a:LX/3LP;

    invoke-virtual {v1, v0}, LX/3LP;->b(LX/2RR;)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2621365
    const v1, -0x271cf248

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    :catchall_0
    move-exception v0

    const v1, 0x691d7c93

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public static j(LX/Isq;)LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/Ish;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2621327
    const-string v0, "getSuggestedBots"

    const v1, 0x7471385f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2621328
    :try_start_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2621329
    iget-object v0, p0, LX/Isq;->e:LX/3Km;

    invoke-virtual {v0}, LX/3Km;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/Isq;->e:LX/3Km;

    invoke-virtual {v0}, LX/3Km;->d()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 2621330
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/Isq;->e:LX/3Km;

    invoke-virtual {v0}, LX/3Km;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2621331
    iget-object v0, p0, LX/Isq;->d:LX/3NF;

    invoke-virtual {v0}, LX/3NF;->b()Lcom/facebook/user/model/User;

    move-result-object v0

    .line 2621332
    :goto_0
    if-eqz v0, :cond_1

    .line 2621333
    iget-object v1, p0, LX/Isq;->e:LX/3Km;

    invoke-virtual {v1}, LX/3Km;->d()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2621334
    new-instance v1, LX/Ish;

    const-string v3, "M"

    sget-object v4, LX/Isg;->DIRECT_M:LX/Isg;

    invoke-direct {v1, v0, v3, v4}, LX/Ish;-><init>(Lcom/facebook/user/model/User;Ljava/lang/String;LX/Isg;)V

    invoke-virtual {v2, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2621335
    :cond_1
    :goto_1
    :try_start_2
    iget-object v0, p0, LX/Isq;->c:LX/3MU;

    const/16 v1, 0x14

    invoke-virtual {v0, v1}, LX/3MU;->a(I)LX/0Px;

    move-result-object v3

    .line 2621336
    if-eqz v3, :cond_4

    .line 2621337
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_2
    if-ge v1, v4, :cond_4

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2621338
    new-instance v5, LX/Ish;

    sget-object v6, LX/Isg;->BOTS:LX/Isg;

    invoke-direct {v5, v0, v6}, LX/Ish;-><init>(Lcom/facebook/user/model/User;LX/Isg;)V

    invoke-virtual {v2, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2621339
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2621340
    :cond_2
    :try_start_3
    iget-object v0, p0, LX/Isq;->d:LX/3NF;

    invoke-virtual {v0}, LX/3NF;->a()Lcom/facebook/user/model/User;

    move-result-object v0

    goto :goto_0

    .line 2621341
    :cond_3
    new-instance v1, LX/Ish;

    sget-object v3, LX/Isg;->BOTS:LX/Isg;

    invoke-direct {v1, v0, v3}, LX/Ish;-><init>(Lcom/facebook/user/model/User;LX/Isg;)V

    invoke-virtual {v2, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 2621342
    :catch_0
    move-exception v0

    .line 2621343
    :try_start_4
    iget-object v1, p0, LX/Isq;->f:LX/03V;

    const-string v3, "Failed to add M to suggestions."

    invoke-virtual {v1, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 2621344
    :catch_1
    move-exception v0

    .line 2621345
    :try_start_5
    iget-object v1, p0, LX/Isq;->f:LX/03V;

    const-string v2, "SearchNullStateSuggestionLoader"

    const-string v3, "getSuggestedBots failed"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2621346
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 2621347
    const v1, -0x2b3517b8

    invoke-static {v1}, LX/02m;->a(I)V

    :goto_3
    return-object v0

    .line 2621348
    :cond_4
    :try_start_6
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v0

    .line 2621349
    const v1, -0x726b3253

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_3

    :catchall_0
    move-exception v0

    const v1, 0x37290ef4

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method


# virtual methods
.method public final b(Ljava/lang/Object;)LX/6LZ;
    .locals 1

    .prologue
    .line 2621326
    sget-object v0, LX/6Lb;->a:LX/6LZ;

    return-object v0
.end method

.method public final b(Ljava/lang/Object;LX/6LZ;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2

    .prologue
    .line 2621320
    iget-object v0, p2, LX/6LZ;->b:LX/6La;

    sget-object v1, LX/6La;->INTERMEDIATE:LX/6La;

    if-ne v0, v1, :cond_0

    .line 2621321
    iget-object v0, p2, LX/6LZ;->a:Ljava/lang/Object;

    check-cast v0, LX/Isp;

    .line 2621322
    iget-object v1, p0, LX/Isq;->b:LX/0TD;

    new-instance p1, LX/Iso;

    invoke-direct {p1, p0, v0}, LX/Iso;-><init>(LX/Isq;LX/Isp;)V

    invoke-interface {v1, p1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object v0, v1

    .line 2621323
    :goto_0
    return-object v0

    .line 2621324
    :cond_0
    iget-object v0, p0, LX/Isq;->b:LX/0TD;

    new-instance v1, LX/Isn;

    invoke-direct {v1, p0}, LX/Isn;-><init>(LX/Isq;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    move-object v0, v0

    .line 2621325
    goto :goto_0
.end method
