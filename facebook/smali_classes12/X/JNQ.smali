.class public final LX/JNQ;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/JNR;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;

.field public final synthetic b:LX/JNR;


# direct methods
.method public constructor <init>(LX/JNR;)V
    .locals 1

    .prologue
    .line 2685890
    iput-object p1, p0, LX/JNQ;->b:LX/JNR;

    .line 2685891
    move-object v0, p1

    .line 2685892
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2685893
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2685878
    const-string v0, "EventsSuggestionItemFooterViewComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2685879
    if-ne p0, p1, :cond_1

    .line 2685880
    :cond_0
    :goto_0
    return v0

    .line 2685881
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2685882
    goto :goto_0

    .line 2685883
    :cond_3
    check-cast p1, LX/JNQ;

    .line 2685884
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2685885
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2685886
    if-eq v2, v3, :cond_0

    .line 2685887
    iget-object v2, p0, LX/JNQ;->a:Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/JNQ;->a:Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;

    iget-object v3, p1, LX/JNQ;->a:Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2685888
    goto :goto_0

    .line 2685889
    :cond_4
    iget-object v2, p1, LX/JNQ;->a:Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
