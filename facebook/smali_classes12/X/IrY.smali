.class public LX/IrY;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private A:I

.field private B:I

.field private C:F

.field private D:F

.field private E:F

.field private F:I

.field private G:J

.field private H:I

.field public I:Landroid/view/MotionEvent;

.field public J:I

.field public final K:Landroid/os/Handler;

.field public L:Landroid/view/GestureDetector;

.field public final a:Landroid/content/Context;

.field private final b:LX/IrX;

.field public c:LX/Irj;

.field public d:F

.field public e:F

.field public f:Z

.field public g:Z

.field private h:F

.field private i:F

.field private j:F

.field private k:F

.field private l:F

.field public m:F

.field public n:F

.field private o:F

.field private p:F

.field public q:F

.field public r:F

.field private s:F

.field private t:F

.field private u:J

.field private v:J

.field public w:Z

.field private x:I

.field private y:I

.field private z:I


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/IrX;)V
    .locals 2

    .prologue
    .line 2618085
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2618086
    const/4 v0, 0x0

    iput v0, p0, LX/IrY;->J:I

    .line 2618087
    iput-object p1, p0, LX/IrY;->a:Landroid/content/Context;

    .line 2618088
    iput-object p2, p0, LX/IrY;->b:LX/IrX;

    .line 2618089
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/IrY;->x:I

    .line 2618090
    const/4 v0, 0x3

    iput v0, p0, LX/IrY;->y:I

    .line 2618091
    const/4 v0, 0x0

    iput-object v0, p0, LX/IrY;->K:Landroid/os/Handler;

    .line 2618092
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2618093
    const v1, 0x7f0b0365

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, LX/IrY;->H:I

    .line 2618094
    const v1, 0x7f0b0366

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, LX/IrY;->z:I

    .line 2618095
    const v1, 0x7f0b0370

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, LX/IrY;->A:I

    .line 2618096
    const v1, 0x7f0b0371

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, LX/IrY;->B:I

    .line 2618097
    return-void
.end method

.method public static a(FFFF)F
    .locals 6

    .prologue
    .line 2618252
    float-to-double v0, p0

    float-to-double v2, p1

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    float-to-double v2, p2

    float-to-double v4, p3

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v2

    sub-double/2addr v0, v2

    .line 2618253
    const-wide v2, 0x4066800000000000L    # 180.0

    mul-double/2addr v0, v2

    const-wide v2, 0x400921fb54442d18L    # Math.PI

    div-double/2addr v0, v2

    double-to-float v0, v0

    return v0
.end method

.method private static b(LX/IrY;Landroid/view/MotionEvent;)V
    .locals 14

    .prologue
    const/4 v1, 0x1

    const/high16 v13, 0x40000000    # 2.0f

    const/4 v2, 0x0

    .line 2618213
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 2618214
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v10

    .line 2618215
    iget-wide v6, p0, LX/IrY;->G:J

    sub-long/2addr v4, v6

    const-wide/16 v6, 0x80

    cmp-long v0, v4, v6

    if-ltz v0, :cond_6

    move v0, v1

    .line 2618216
    :goto_0
    const/4 v3, 0x0

    move v8, v2

    move v9, v2

    move v4, v0

    .line 2618217
    :goto_1
    if-ge v8, v10, :cond_b

    .line 2618218
    iget v0, p0, LX/IrY;->E:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    .line 2618219
    :goto_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v11

    .line 2618220
    add-int/lit8 v12, v11, 0x1

    move v6, v2

    move v5, v3

    .line 2618221
    :goto_3
    if-ge v6, v12, :cond_a

    .line 2618222
    if-ge v6, v11, :cond_8

    .line 2618223
    invoke-virtual {p1, v8, v6}, Landroid/view/MotionEvent;->getHistoricalTouchMajor(II)F

    move-result v3

    .line 2618224
    :goto_4
    iget v7, p0, LX/IrY;->H:I

    int-to-float v7, v7

    cmpg-float v7, v3, v7

    if-gez v7, :cond_0

    iget v3, p0, LX/IrY;->H:I

    int-to-float v3, v3

    .line 2618225
    :cond_0
    add-float v7, v5, v3

    .line 2618226
    iget v5, p0, LX/IrY;->C:F

    invoke-static {v5}, Ljava/lang/Float;->isNaN(F)Z

    move-result v5

    if-nez v5, :cond_1

    iget v5, p0, LX/IrY;->C:F

    cmpl-float v5, v3, v5

    if-lez v5, :cond_2

    .line 2618227
    :cond_1
    iput v3, p0, LX/IrY;->C:F

    .line 2618228
    :cond_2
    iget v5, p0, LX/IrY;->D:F

    invoke-static {v5}, Ljava/lang/Float;->isNaN(F)Z

    move-result v5

    if-nez v5, :cond_3

    iget v5, p0, LX/IrY;->D:F

    cmpg-float v5, v3, v5

    if-gez v5, :cond_4

    .line 2618229
    :cond_3
    iput v3, p0, LX/IrY;->D:F

    .line 2618230
    :cond_4
    if-eqz v0, :cond_d

    .line 2618231
    iget v5, p0, LX/IrY;->E:F

    sub-float/2addr v3, v5

    invoke-static {v3}, Ljava/lang/Math;->signum(F)F

    move-result v3

    float-to-int v3, v3

    .line 2618232
    iget v5, p0, LX/IrY;->F:I

    if-ne v3, v5, :cond_5

    if-nez v3, :cond_d

    iget v5, p0, LX/IrY;->F:I

    if-nez v5, :cond_d

    .line 2618233
    :cond_5
    iput v3, p0, LX/IrY;->F:I

    .line 2618234
    if-ge v6, v11, :cond_9

    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->getHistoricalEventTime(I)J

    move-result-wide v4

    .line 2618235
    :goto_5
    iput-wide v4, p0, LX/IrY;->G:J

    move v3, v2

    .line 2618236
    :goto_6
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    move v5, v7

    move v4, v3

    goto :goto_3

    :cond_6
    move v0, v2

    .line 2618237
    goto :goto_0

    :cond_7
    move v0, v2

    .line 2618238
    goto :goto_2

    .line 2618239
    :cond_8
    invoke-virtual {p1, v8}, Landroid/view/MotionEvent;->getTouchMajor(I)F

    move-result v3

    goto :goto_4

    .line 2618240
    :cond_9
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    goto :goto_5

    .line 2618241
    :cond_a
    add-int v3, v9, v12

    .line 2618242
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    move v9, v3

    move v3, v5

    goto/16 :goto_1

    .line 2618243
    :cond_b
    int-to-float v0, v9

    div-float v0, v3, v0

    .line 2618244
    if-eqz v4, :cond_c

    .line 2618245
    iget v1, p0, LX/IrY;->C:F

    iget v3, p0, LX/IrY;->D:F

    add-float/2addr v1, v3

    add-float/2addr v0, v1

    const/high16 v1, 0x40400000    # 3.0f

    div-float/2addr v0, v1

    .line 2618246
    iget v1, p0, LX/IrY;->C:F

    add-float/2addr v1, v0

    div-float/2addr v1, v13

    iput v1, p0, LX/IrY;->C:F

    .line 2618247
    iget v1, p0, LX/IrY;->D:F

    add-float/2addr v1, v0

    div-float/2addr v1, v13

    iput v1, p0, LX/IrY;->D:F

    .line 2618248
    iput v0, p0, LX/IrY;->E:F

    .line 2618249
    iput v2, p0, LX/IrY;->F:I

    .line 2618250
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v0

    iput-wide v0, p0, LX/IrY;->G:J

    .line 2618251
    :cond_c
    return-void

    :cond_d
    move v3, v4

    goto :goto_6
.end method

.method private static e(LX/IrY;)V
    .locals 2

    .prologue
    const/high16 v0, 0x7fc00000    # NaNf

    .line 2618207
    iput v0, p0, LX/IrY;->C:F

    .line 2618208
    iput v0, p0, LX/IrY;->D:F

    .line 2618209
    iput v0, p0, LX/IrY;->E:F

    .line 2618210
    const/4 v0, 0x0

    iput v0, p0, LX/IrY;->F:I

    .line 2618211
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/IrY;->G:J

    .line 2618212
    return-void
.end method

.method private static f(LX/IrY;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2618206
    iget v1, p0, LX/IrY;->J:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/MotionEvent;)Z
    .locals 13

    .prologue
    .line 2618098
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v0

    iput-wide v0, p0, LX/IrY;->u:J

    .line 2618099
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v9

    .line 2618100
    iget-boolean v0, p0, LX/IrY;->f:Z

    if-eqz v0, :cond_0

    .line 2618101
    iget-object v0, p0, LX/IrY;->L:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 2618102
    :cond_0
    const/4 v0, 0x1

    if-eq v9, v0, :cond_1

    const/4 v0, 0x3

    if-ne v9, v0, :cond_4

    :cond_1
    const/4 v0, 0x1

    .line 2618103
    :goto_0
    if-eqz v9, :cond_2

    if-eqz v0, :cond_6

    .line 2618104
    :cond_2
    iget-boolean v1, p0, LX/IrY;->w:Z

    if-eqz v1, :cond_5

    .line 2618105
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/IrY;->w:Z

    .line 2618106
    const/4 v1, 0x0

    iput v1, p0, LX/IrY;->j:F

    .line 2618107
    const/4 v1, 0x0

    iput v1, p0, LX/IrY;->J:I

    .line 2618108
    :cond_3
    :goto_1
    if-eqz v0, :cond_6

    .line 2618109
    invoke-static {p0}, LX/IrY;->e(LX/IrY;)V

    .line 2618110
    const/4 v0, 0x1

    .line 2618111
    :goto_2
    return v0

    .line 2618112
    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    .line 2618113
    :cond_5
    iget v1, p0, LX/IrY;->J:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    if-eqz v0, :cond_3

    .line 2618114
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/IrY;->w:Z

    .line 2618115
    const/4 v1, 0x0

    iput v1, p0, LX/IrY;->j:F

    .line 2618116
    const/4 v1, 0x0

    iput v1, p0, LX/IrY;->J:I

    goto :goto_1

    .line 2618117
    :cond_6
    if-eqz v9, :cond_7

    const/4 v0, 0x6

    if-eq v9, v0, :cond_7

    const/4 v0, 0x5

    if-ne v9, v0, :cond_9

    :cond_7
    const/4 v0, 0x1

    move v8, v0

    .line 2618118
    :goto_3
    const/4 v0, 0x6

    if-ne v9, v0, :cond_a

    const/4 v0, 0x1

    move v1, v0

    .line 2618119
    :goto_4
    if-eqz v1, :cond_b

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    .line 2618120
    :goto_5
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 2618121
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    .line 2618122
    if-eqz v1, :cond_c

    add-int/lit8 v1, v2, -0x1

    .line 2618123
    :goto_6
    iget v3, p0, LX/IrY;->J:I

    const/4 v6, 0x1

    if-ne v3, v6, :cond_e

    .line 2618124
    iget-object v3, p0, LX/IrY;->c:LX/Irj;

    if-eqz v3, :cond_d

    .line 2618125
    iget-object v3, p0, LX/IrY;->c:LX/Irj;

    invoke-virtual {v3}, LX/Irj;->a()F

    move-result v4

    .line 2618126
    iget-object v3, p0, LX/IrY;->c:LX/Irj;

    invoke-virtual {v3}, LX/Irj;->b()F

    move-result v3

    .line 2618127
    :goto_7
    invoke-static {p0, p1}, LX/IrY;->b(LX/IrY;Landroid/view/MotionEvent;)V

    .line 2618128
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 2618129
    const/4 v5, 0x0

    move v12, v5

    move v5, v6

    move v6, v7

    move v7, v12

    :goto_8
    if-ge v7, v2, :cond_11

    .line 2618130
    if-eq v0, v7, :cond_8

    .line 2618131
    iget v10, p0, LX/IrY;->E:F

    const/high16 v11, 0x40000000    # 2.0f

    div-float/2addr v10, v11

    .line 2618132
    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getX(I)F

    move-result v11

    sub-float/2addr v11, v4

    invoke-static {v11}, Ljava/lang/Math;->abs(F)F

    move-result v11

    add-float/2addr v11, v10

    add-float/2addr v6, v11

    .line 2618133
    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getY(I)F

    move-result v11

    sub-float/2addr v11, v3

    invoke-static {v11}, Ljava/lang/Math;->abs(F)F

    move-result v11

    add-float/2addr v10, v11

    add-float/2addr v5, v10

    .line 2618134
    :cond_8
    add-int/lit8 v7, v7, 0x1

    goto :goto_8

    .line 2618135
    :cond_9
    const/4 v0, 0x0

    move v8, v0

    goto :goto_3

    .line 2618136
    :cond_a
    const/4 v0, 0x0

    move v1, v0

    goto :goto_4

    .line 2618137
    :cond_b
    const/4 v0, -0x1

    goto :goto_5

    :cond_c
    move v1, v2

    .line 2618138
    goto :goto_6

    .line 2618139
    :cond_d
    iget-object v3, p0, LX/IrY;->I:Landroid/view/MotionEvent;

    invoke-virtual {v3}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    .line 2618140
    iget-object v3, p0, LX/IrY;->I:Landroid/view/MotionEvent;

    invoke-virtual {v3}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    goto :goto_7

    .line 2618141
    :cond_e
    const/4 v3, 0x0

    move v12, v3

    move v3, v4

    move v4, v5

    move v5, v12

    :goto_9
    if-ge v5, v2, :cond_10

    .line 2618142
    if-eq v0, v5, :cond_f

    .line 2618143
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v6

    add-float/2addr v4, v6

    .line 2618144
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v6

    add-float/2addr v3, v6

    .line 2618145
    :cond_f
    add-int/lit8 v5, v5, 0x1

    goto :goto_9

    .line 2618146
    :cond_10
    int-to-float v5, v1

    div-float/2addr v4, v5

    .line 2618147
    int-to-float v5, v1

    div-float/2addr v3, v5

    goto :goto_7

    .line 2618148
    :cond_11
    int-to-float v0, v1

    div-float/2addr v6, v0

    .line 2618149
    int-to-float v0, v1

    div-float/2addr v5, v0

    .line 2618150
    invoke-static {p0}, LX/IrY;->f(LX/IrY;)Z

    move-result v0

    if-eqz v0, :cond_19

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    iget-object v1, p0, LX/IrY;->c:LX/Irj;

    invoke-virtual {v1}, LX/Irj;->a()F

    move-result v1

    sub-float/2addr v0, v1

    .line 2618151
    :goto_a
    invoke-static {p0}, LX/IrY;->f(LX/IrY;)Z

    move-result v1

    if-eqz v1, :cond_1b

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    iget-object v2, p0, LX/IrY;->c:LX/Irj;

    invoke-virtual {v2}, LX/Irj;->b()F

    move-result v2

    sub-float/2addr v1, v2

    .line 2618152
    :goto_b
    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v6, v2

    .line 2618153
    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v5, v2

    .line 2618154
    invoke-static {p0}, LX/IrY;->f(LX/IrY;)Z

    move-result v2

    if-eqz v2, :cond_1d

    move v2, v5

    .line 2618155
    :goto_c
    iget-boolean v7, p0, LX/IrY;->w:Z

    .line 2618156
    iput v4, p0, LX/IrY;->d:F

    .line 2618157
    iput v3, p0, LX/IrY;->e:F

    .line 2618158
    invoke-static {p0}, LX/IrY;->f(LX/IrY;)Z

    move-result v3

    if-nez v3, :cond_13

    iget-boolean v3, p0, LX/IrY;->w:Z

    if-eqz v3, :cond_13

    iget v3, p0, LX/IrY;->z:I

    int-to-float v3, v3

    cmpg-float v3, v2, v3

    if-ltz v3, :cond_12

    if-eqz v8, :cond_13

    .line 2618159
    :cond_12
    const/4 v3, 0x0

    iput-boolean v3, p0, LX/IrY;->w:Z

    .line 2618160
    iput v2, p0, LX/IrY;->j:F

    .line 2618161
    const/4 v3, 0x0

    iput v3, p0, LX/IrY;->J:I

    .line 2618162
    :cond_13
    if-eqz v8, :cond_14

    .line 2618163
    iput v6, p0, LX/IrY;->k:F

    iput v6, p0, LX/IrY;->o:F

    .line 2618164
    iput v5, p0, LX/IrY;->l:F

    iput v5, p0, LX/IrY;->p:F

    .line 2618165
    iput v0, p0, LX/IrY;->m:F

    iput v0, p0, LX/IrY;->q:F

    iput v0, p0, LX/IrY;->s:F

    .line 2618166
    iput v1, p0, LX/IrY;->n:F

    iput v1, p0, LX/IrY;->r:F

    iput v1, p0, LX/IrY;->t:F

    .line 2618167
    iput v2, p0, LX/IrY;->h:F

    iput v2, p0, LX/IrY;->i:F

    iput v2, p0, LX/IrY;->j:F

    .line 2618168
    :cond_14
    invoke-static {p0}, LX/IrY;->f(LX/IrY;)Z

    move-result v3

    if-eqz v3, :cond_1e

    iget v3, p0, LX/IrY;->x:I

    .line 2618169
    :goto_d
    iget-boolean v4, p0, LX/IrY;->w:Z

    if-nez v4, :cond_16

    int-to-float v3, v3

    cmpl-float v3, v2, v3

    if-ltz v3, :cond_16

    if-nez v7, :cond_15

    iget v3, p0, LX/IrY;->t:F

    iget v4, p0, LX/IrY;->s:F

    invoke-static {v3, v4, v1, v0}, LX/IrY;->a(FFFF)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    iget v4, p0, LX/IrY;->y:I

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_16

    .line 2618170
    :cond_15
    iput v6, p0, LX/IrY;->k:F

    iput v6, p0, LX/IrY;->o:F

    .line 2618171
    iput v5, p0, LX/IrY;->l:F

    iput v5, p0, LX/IrY;->p:F

    .line 2618172
    iput v0, p0, LX/IrY;->m:F

    iput v0, p0, LX/IrY;->q:F

    .line 2618173
    iput v1, p0, LX/IrY;->n:F

    iput v1, p0, LX/IrY;->r:F

    .line 2618174
    iput v2, p0, LX/IrY;->h:F

    iput v2, p0, LX/IrY;->i:F

    .line 2618175
    iget-wide v10, p0, LX/IrY;->u:J

    iput-wide v10, p0, LX/IrY;->v:J

    .line 2618176
    iget-object v3, p0, LX/IrY;->b:LX/IrX;

    invoke-virtual {v3, p0}, LX/IrX;->b(LX/IrY;)Z

    move-result v3

    iput-boolean v3, p0, LX/IrY;->w:Z

    .line 2618177
    :cond_16
    const/4 v3, 0x2

    if-ne v9, v3, :cond_18

    .line 2618178
    iput v6, p0, LX/IrY;->k:F

    .line 2618179
    iput v5, p0, LX/IrY;->l:F

    .line 2618180
    iput v2, p0, LX/IrY;->h:F

    .line 2618181
    iput v0, p0, LX/IrY;->m:F

    .line 2618182
    iput v1, p0, LX/IrY;->n:F

    .line 2618183
    const/4 v2, 0x1

    .line 2618184
    invoke-static {p0}, LX/IrY;->f(LX/IrY;)Z

    move-result v3

    if-eqz v3, :cond_17

    iget-boolean v3, p0, LX/IrY;->g:Z

    if-eqz v3, :cond_17

    .line 2618185
    mul-float/2addr v0, v0

    mul-float/2addr v1, v1

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    .line 2618186
    iget v1, p0, LX/IrY;->B:I

    int-to-float v1, v1

    iget v3, p0, LX/IrY;->A:I

    int-to-float v3, v3

    invoke-static {v1, v3, v0}, LX/Iqu;->b(FFF)F

    move-result v0

    .line 2618187
    iget v1, p0, LX/IrY;->k:F

    iget v3, p0, LX/IrY;->o:F

    invoke-static {v1, v3, v0}, LX/Iqu;->a(FFF)F

    move-result v1

    iput v1, p0, LX/IrY;->o:F

    .line 2618188
    iget v1, p0, LX/IrY;->l:F

    iget v3, p0, LX/IrY;->p:F

    invoke-static {v1, v3, v0}, LX/Iqu;->a(FFF)F

    move-result v1

    iput v1, p0, LX/IrY;->p:F

    .line 2618189
    iget v1, p0, LX/IrY;->m:F

    iget v3, p0, LX/IrY;->q:F

    invoke-static {v1, v3, v0}, LX/Iqu;->a(FFF)F

    move-result v1

    iput v1, p0, LX/IrY;->q:F

    .line 2618190
    iget v1, p0, LX/IrY;->n:F

    iget v3, p0, LX/IrY;->r:F

    invoke-static {v1, v3, v0}, LX/Iqu;->a(FFF)F

    move-result v1

    iput v1, p0, LX/IrY;->r:F

    .line 2618191
    iget v1, p0, LX/IrY;->h:F

    iget v3, p0, LX/IrY;->i:F

    invoke-static {v1, v3, v0}, LX/Iqu;->a(FFF)F

    move-result v0

    iput v0, p0, LX/IrY;->i:F

    .line 2618192
    :cond_17
    iget-boolean v0, p0, LX/IrY;->w:Z

    if-eqz v0, :cond_1f

    .line 2618193
    iget-object v0, p0, LX/IrY;->b:LX/IrX;

    invoke-virtual {v0, p0}, LX/IrX;->a(LX/IrY;)Z

    move-result v0

    .line 2618194
    :goto_e
    if-eqz v0, :cond_18

    .line 2618195
    iget v0, p0, LX/IrY;->k:F

    iput v0, p0, LX/IrY;->o:F

    .line 2618196
    iget v0, p0, LX/IrY;->l:F

    iput v0, p0, LX/IrY;->p:F

    .line 2618197
    iget v0, p0, LX/IrY;->m:F

    iput v0, p0, LX/IrY;->q:F

    .line 2618198
    iget v0, p0, LX/IrY;->n:F

    iput v0, p0, LX/IrY;->r:F

    .line 2618199
    iget v0, p0, LX/IrY;->h:F

    iput v0, p0, LX/IrY;->i:F

    .line 2618200
    iget-wide v0, p0, LX/IrY;->u:J

    iput-wide v0, p0, LX/IrY;->v:J

    .line 2618201
    :cond_18
    const/4 v0, 0x1

    goto/16 :goto_2

    .line 2618202
    :cond_19
    const/4 v0, 0x1

    if-le v2, v0, :cond_1a

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    sub-float/2addr v0, v1

    goto/16 :goto_a

    :cond_1a
    const/4 v0, 0x0

    goto/16 :goto_a

    .line 2618203
    :cond_1b
    const/4 v1, 0x1

    if-le v2, v1, :cond_1c

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    sub-float/2addr v1, v2

    goto/16 :goto_b

    :cond_1c
    const/4 v1, 0x0

    goto/16 :goto_b

    .line 2618204
    :cond_1d
    mul-float v2, v6, v6

    mul-float v7, v5, v5

    add-float/2addr v2, v7

    float-to-double v10, v2

    invoke-static {v10, v11}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v10

    double-to-float v2, v10

    goto/16 :goto_c

    .line 2618205
    :cond_1e
    iget v3, p0, LX/IrY;->z:I

    goto/16 :goto_d

    :cond_1f
    move v0, v2

    goto :goto_e
.end method
