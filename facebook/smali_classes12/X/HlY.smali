.class public final LX/HlY;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/instagram/common/json/annotation/JsonType;
.end annotation


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/HlZ;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/HlZ;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2498734
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2498735
    iput-object v0, p0, LX/HlY;->a:Ljava/util/List;

    .line 2498736
    iput-object v0, p0, LX/HlY;->b:LX/HlZ;

    .line 2498737
    return-void
.end method

.method private constructor <init>(Ljava/util/List;LX/HlZ;)V
    .locals 0
    .param p1    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/HlZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/HlZ;",
            ">;",
            "LX/HlZ;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2498713
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2498714
    iput-object p1, p0, LX/HlY;->a:Ljava/util/List;

    .line 2498715
    iput-object p2, p0, LX/HlY;->b:LX/HlZ;

    .line 2498716
    return-void
.end method

.method public static a(Ljava/util/List;Lcom/facebook/wifiscan/WifiScanResult;LX/0SG;)LX/HlY;
    .locals 11
    .param p0    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Lcom/facebook/wifiscan/WifiScanResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/wifiscan/WifiScanResult;",
            ">;",
            "Lcom/facebook/wifiscan/WifiScanResult;",
            "LX/0SG;",
            ")",
            "LX/HlY;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2498717
    if-nez p0, :cond_0

    if-nez p1, :cond_0

    .line 2498718
    const/4 v0, 0x0

    .line 2498719
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/HlY;

    .line 2498720
    if-nez p0, :cond_1

    .line 2498721
    const/4 v3, 0x0

    .line 2498722
    :goto_1
    move-object v1, v3

    .line 2498723
    if-nez p1, :cond_4

    .line 2498724
    const/4 v3, 0x0

    .line 2498725
    :goto_2
    move-object v2, v3

    .line 2498726
    invoke-direct {v0, v1, v2}, LX/HlY;-><init>(Ljava/util/List;LX/HlZ;)V

    goto :goto_0

    .line 2498727
    :cond_1
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 2498728
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_3
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v8, v3

    check-cast v8, Lcom/facebook/wifiscan/WifiScanResult;

    .line 2498729
    const-wide/16 v3, 0x0

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 2498730
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x11

    if-lt v3, v5, :cond_2

    .line 2498731
    invoke-interface {p2}, LX/0SG;->a()J

    move-result-wide v3

    invoke-static {v8, v3, v4}, LX/2zX;->a(Lcom/facebook/wifiscan/WifiScanResult;J)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 2498732
    :cond_2
    new-instance v3, LX/HlZ;

    iget-object v5, v8, Lcom/facebook/wifiscan/WifiScanResult;->b:Ljava/lang/String;

    iget v6, v8, Lcom/facebook/wifiscan/WifiScanResult;->c:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    iget-object v7, v8, Lcom/facebook/wifiscan/WifiScanResult;->d:Ljava/lang/String;

    iget-object v8, v8, Lcom/facebook/wifiscan/WifiScanResult;->e:Ljava/lang/Integer;

    invoke-direct/range {v3 .. v8}, LX/HlZ;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-interface {v9, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_3
    move-object v3, v9

    .line 2498733
    goto :goto_1

    :cond_4
    new-instance v3, LX/HlZ;

    invoke-interface {p2}, LX/0SG;->a()J

    move-result-wide v5

    iget-wide v7, p1, Lcom/facebook/wifiscan/WifiScanResult;->a:J

    sub-long/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iget-object v5, p1, Lcom/facebook/wifiscan/WifiScanResult;->b:Ljava/lang/String;

    iget v6, p1, Lcom/facebook/wifiscan/WifiScanResult;->c:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    iget-object v7, p1, Lcom/facebook/wifiscan/WifiScanResult;->d:Ljava/lang/String;

    iget-object v8, p1, Lcom/facebook/wifiscan/WifiScanResult;->e:Ljava/lang/Integer;

    invoke-direct/range {v3 .. v8}, LX/HlZ;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_2
.end method
