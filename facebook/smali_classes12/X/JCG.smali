.class public final enum LX/JCG;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/JCG;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/JCG;

.field public static final enum ABOUT_LIST_ITEM_FIRST:LX/JCG;

.field public static final enum ABOUT_LIST_ITEM_MIDDLE:LX/JCG;

.field public static final enum ABOUT_MORE_ABOUT:LX/JCG;

.field public static final enum INVISIBLE_PROFILE_DATA:LX/JCG;

.field public static final enum LIST_ITEM_END:LX/JCG;

.field public static final enum LIST_ITEM_MIDDLE:LX/JCG;

.field public static final enum LOADING:LX/JCG;

.field public static final enum PROFILE_FIELD_EXPERIENCE_ITEM:LX/JCG;

.field public static final enum PROFILE_FIELD_EXPERIENCE_ITEM_END:LX/JCG;

.field public static final enum PROFILE_FIELD_SECTION_HEADER:LX/JCG;

.field public static final enum PROFILE_FIELD_TEXT:LX/JCG;

.field public static final enum PROFILE_FIELD_TEXT_END:LX/JCG;

.field public static final enum SECTION_COLLECTION_HEADER:LX/JCG;

.field public static final enum SECTION_FOOTER:LX/JCG;

.field public static final enum SECTION_FULL_FRAME:LX/JCG;

.field public static final enum SECTION_HEADER:LX/JCG;

.field public static final enum UNKNOWN_TYPE:LX/JCG;


# instance fields
.field private final mItemLayoutResId:I


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2661693
    new-instance v0, LX/JCG;

    const-string v1, "LOADING"

    const v2, 0x7f0314f0

    invoke-direct {v0, v1, v4, v2}, LX/JCG;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/JCG;->LOADING:LX/JCG;

    .line 2661694
    new-instance v0, LX/JCG;

    const-string v1, "PROFILE_FIELD_SECTION_HEADER"

    invoke-direct {v0, v1, v5}, LX/JCG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JCG;->PROFILE_FIELD_SECTION_HEADER:LX/JCG;

    .line 2661695
    new-instance v0, LX/JCG;

    const-string v1, "ABOUT_MORE_ABOUT"

    invoke-direct {v0, v1, v6}, LX/JCG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JCG;->ABOUT_MORE_ABOUT:LX/JCG;

    .line 2661696
    new-instance v0, LX/JCG;

    const-string v1, "SECTION_COLLECTION_HEADER"

    invoke-direct {v0, v1, v7}, LX/JCG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JCG;->SECTION_COLLECTION_HEADER:LX/JCG;

    .line 2661697
    new-instance v0, LX/JCG;

    const-string v1, "SECTION_HEADER"

    invoke-direct {v0, v1, v8}, LX/JCG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JCG;->SECTION_HEADER:LX/JCG;

    .line 2661698
    new-instance v0, LX/JCG;

    const-string v1, "SECTION_FOOTER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/JCG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JCG;->SECTION_FOOTER:LX/JCG;

    .line 2661699
    new-instance v0, LX/JCG;

    const-string v1, "SECTION_FULL_FRAME"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/JCG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JCG;->SECTION_FULL_FRAME:LX/JCG;

    .line 2661700
    new-instance v0, LX/JCG;

    const-string v1, "ABOUT_LIST_ITEM_FIRST"

    const/4 v2, 0x7

    const v3, 0x7f0302a2

    invoke-direct {v0, v1, v2, v3}, LX/JCG;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/JCG;->ABOUT_LIST_ITEM_FIRST:LX/JCG;

    .line 2661701
    new-instance v0, LX/JCG;

    const-string v1, "ABOUT_LIST_ITEM_MIDDLE"

    const/16 v2, 0x8

    const v3, 0x7f0302a2

    invoke-direct {v0, v1, v2, v3}, LX/JCG;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/JCG;->ABOUT_LIST_ITEM_MIDDLE:LX/JCG;

    .line 2661702
    new-instance v0, LX/JCG;

    const-string v1, "LIST_ITEM_MIDDLE"

    const/16 v2, 0x9

    const v3, 0x7f0302a4

    invoke-direct {v0, v1, v2, v3}, LX/JCG;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/JCG;->LIST_ITEM_MIDDLE:LX/JCG;

    .line 2661703
    new-instance v0, LX/JCG;

    const-string v1, "LIST_ITEM_END"

    const/16 v2, 0xa

    const v3, 0x7f0302a4

    invoke-direct {v0, v1, v2, v3}, LX/JCG;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/JCG;->LIST_ITEM_END:LX/JCG;

    .line 2661704
    new-instance v0, LX/JCG;

    const-string v1, "PROFILE_FIELD_TEXT"

    const/16 v2, 0xb

    const v3, 0x7f03104b

    invoke-direct {v0, v1, v2, v3}, LX/JCG;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/JCG;->PROFILE_FIELD_TEXT:LX/JCG;

    .line 2661705
    new-instance v0, LX/JCG;

    const-string v1, "PROFILE_FIELD_TEXT_END"

    const/16 v2, 0xc

    const v3, 0x7f03104b

    invoke-direct {v0, v1, v2, v3}, LX/JCG;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/JCG;->PROFILE_FIELD_TEXT_END:LX/JCG;

    .line 2661706
    new-instance v0, LX/JCG;

    const-string v1, "PROFILE_FIELD_EXPERIENCE_ITEM"

    const/16 v2, 0xd

    const v3, 0x7f031054

    invoke-direct {v0, v1, v2, v3}, LX/JCG;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/JCG;->PROFILE_FIELD_EXPERIENCE_ITEM:LX/JCG;

    .line 2661707
    new-instance v0, LX/JCG;

    const-string v1, "PROFILE_FIELD_EXPERIENCE_ITEM_END"

    const/16 v2, 0xe

    const v3, 0x7f031054

    invoke-direct {v0, v1, v2, v3}, LX/JCG;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/JCG;->PROFILE_FIELD_EXPERIENCE_ITEM_END:LX/JCG;

    .line 2661708
    new-instance v0, LX/JCG;

    const-string v1, "INVISIBLE_PROFILE_DATA"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/JCG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JCG;->INVISIBLE_PROFILE_DATA:LX/JCG;

    .line 2661709
    new-instance v0, LX/JCG;

    const-string v1, "UNKNOWN_TYPE"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, LX/JCG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JCG;->UNKNOWN_TYPE:LX/JCG;

    .line 2661710
    const/16 v0, 0x11

    new-array v0, v0, [LX/JCG;

    sget-object v1, LX/JCG;->LOADING:LX/JCG;

    aput-object v1, v0, v4

    sget-object v1, LX/JCG;->PROFILE_FIELD_SECTION_HEADER:LX/JCG;

    aput-object v1, v0, v5

    sget-object v1, LX/JCG;->ABOUT_MORE_ABOUT:LX/JCG;

    aput-object v1, v0, v6

    sget-object v1, LX/JCG;->SECTION_COLLECTION_HEADER:LX/JCG;

    aput-object v1, v0, v7

    sget-object v1, LX/JCG;->SECTION_HEADER:LX/JCG;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/JCG;->SECTION_FOOTER:LX/JCG;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/JCG;->SECTION_FULL_FRAME:LX/JCG;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/JCG;->ABOUT_LIST_ITEM_FIRST:LX/JCG;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/JCG;->ABOUT_LIST_ITEM_MIDDLE:LX/JCG;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/JCG;->LIST_ITEM_MIDDLE:LX/JCG;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/JCG;->LIST_ITEM_END:LX/JCG;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/JCG;->PROFILE_FIELD_TEXT:LX/JCG;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/JCG;->PROFILE_FIELD_TEXT_END:LX/JCG;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/JCG;->PROFILE_FIELD_EXPERIENCE_ITEM:LX/JCG;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/JCG;->PROFILE_FIELD_EXPERIENCE_ITEM_END:LX/JCG;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/JCG;->INVISIBLE_PROFILE_DATA:LX/JCG;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/JCG;->UNKNOWN_TYPE:LX/JCG;

    aput-object v2, v0, v1

    sput-object v0, LX/JCG;->$VALUES:[LX/JCG;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2661711
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2661712
    const/4 v0, 0x0

    iput v0, p0, LX/JCG;->mItemLayoutResId:I

    .line 2661713
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 2661714
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2661715
    iput p3, p0, LX/JCG;->mItemLayoutResId:I

    .line 2661716
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/JCG;
    .locals 1

    .prologue
    .line 2661717
    const-class v0, LX/JCG;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/JCG;

    return-object v0
.end method

.method public static values()[LX/JCG;
    .locals 1

    .prologue
    .line 2661718
    sget-object v0, LX/JCG;->$VALUES:[LX/JCG;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/JCG;

    return-object v0
.end method


# virtual methods
.method public final getItemLayoutResId()I
    .locals 1

    .prologue
    .line 2661719
    iget v0, p0, LX/JCG;->mItemLayoutResId:I

    return v0
.end method
