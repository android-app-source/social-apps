.class public LX/I1o;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Z

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/model/Event;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/facebook/events/common/EventAnalyticsParams;

.field private final e:Landroid/content/Context;

.field private final f:LX/6RZ;

.field private final g:LX/Blh;


# direct methods
.method public constructor <init>(Lcom/facebook/events/common/EventAnalyticsParams;Landroid/content/Context;LX/6RZ;LX/Blh;)V
    .locals 1
    .param p1    # Lcom/facebook/events/common/EventAnalyticsParams;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2529388
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2529389
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/I1o;->b:Ljava/util/List;

    .line 2529390
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/I1o;->c:Ljava/util/HashMap;

    .line 2529391
    iput-object p2, p0, LX/I1o;->e:Landroid/content/Context;

    .line 2529392
    iput-object p1, p0, LX/I1o;->d:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2529393
    iput-object p3, p0, LX/I1o;->f:LX/6RZ;

    .line 2529394
    iput-object p4, p0, LX/I1o;->g:LX/Blh;

    .line 2529395
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 4

    .prologue
    .line 2529380
    packed-switch p2, :pswitch_data_0

    .line 2529381
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unsupported View Type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2529382
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03055c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2529383
    new-instance v0, LX/I14;

    invoke-direct {v0, v1}, LX/I14;-><init>(Landroid/view/View;)V

    .line 2529384
    :goto_0
    return-object v0

    .line 2529385
    :pswitch_1
    new-instance v1, LX/I29;

    iget-object v0, p0, LX/I1o;->e:Landroid/content/Context;

    const/16 v2, 0x9

    invoke-direct {v1, v0, v2}, LX/I29;-><init>(Landroid/content/Context;I)V

    .line 2529386
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v0, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v0}, LX/I29;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2529387
    new-instance v0, LX/I1n;

    invoke-direct {v0, v1}, LX/I1n;-><init>(Landroid/view/View;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(LX/1a1;I)V
    .locals 3

    .prologue
    .line 2529371
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    .line 2529372
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 2529373
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    check-cast v0, LX/I29;

    .line 2529374
    iget-object v1, p0, LX/I1o;->b:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/model/Event;

    .line 2529375
    iget-object v2, p0, LX/I1o;->d:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-virtual {v0, v1, v2}, LX/I29;->a(Lcom/facebook/events/model/Event;Lcom/facebook/events/common/EventAnalyticsParams;)V

    .line 2529376
    :cond_0
    :goto_0
    return-void

    .line 2529377
    :cond_1
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 2529378
    check-cast p1, LX/I14;

    .line 2529379
    iget-boolean v0, p0, LX/I1o;->a:Z

    invoke-virtual {p1, v0}, LX/I14;->b(Z)V

    goto :goto_0
.end method

.method public final a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/model/Event;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2529363
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2529364
    iget-object v2, p0, LX/I1o;->c:Ljava/util/HashMap;

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/model/Event;

    .line 2529365
    iget-object v3, v0, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v0, v3

    .line 2529366
    iget-object v3, p0, LX/I1o;->b:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/2addr v3, v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2529367
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2529368
    :cond_0
    iget-object v0, p0, LX/I1o;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2529369
    iget-object v0, p0, LX/I1o;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/1OM;->c(II)V

    .line 2529370
    return-void
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 2529356
    iget-boolean v0, p0, LX/I1o;->a:Z

    if-eq v0, p1, :cond_0

    .line 2529357
    iput-boolean p1, p0, LX/I1o;->a:Z

    .line 2529358
    iget-boolean v0, p0, LX/I1o;->a:Z

    .line 2529359
    if-eqz v0, :cond_1

    .line 2529360
    iget-object p1, p0, LX/I1o;->b:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    invoke-virtual {p0, p1}, LX/1OM;->j_(I)V

    .line 2529361
    :cond_0
    :goto_0
    return-void

    .line 2529362
    :cond_1
    iget-object p1, p0, LX/I1o;->b:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    invoke-virtual {p0, p1}, LX/1OM;->d(I)V

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2529355
    iget-object v0, p0, LX/I1o;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne p1, v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final ij_()I
    .locals 2

    .prologue
    .line 2529354
    iget-object v0, p0, LX/I1o;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    iget-boolean v0, p0, LX/I1o;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
