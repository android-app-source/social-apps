.class public LX/JDa;
.super Landroid/widget/TableLayout;
.source ""


# instance fields
.field public a:LX/J9L;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:I

.field private final c:I


# direct methods
.method private constructor <init>(Landroid/content/Context;Landroid/view/LayoutInflater;I)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 2664249
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/TableLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2664250
    const-class v0, LX/JDa;

    invoke-static {v0, p0}, LX/JDa;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2664251
    const/4 v0, 0x2

    move v0, v0

    .line 2664252
    iput v0, p0, LX/JDa;->b:I

    .line 2664253
    iget-object v0, p0, LX/JDa;->a:LX/J9L;

    .line 2664254
    iget v2, v0, LX/J9L;->a:I

    move v0, v2

    .line 2664255
    iput v0, p0, LX/JDa;->c:I

    move v0, v1

    .line 2664256
    :goto_0
    iget v2, p0, LX/JDa;->b:I

    if-ge v0, v2, :cond_1

    .line 2664257
    new-instance v3, Landroid/widget/TableRow;

    invoke-virtual {p0}, LX/JDa;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v3, v2}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    .line 2664258
    new-instance v2, Landroid/widget/TableLayout$LayoutParams;

    const/4 v4, -0x1

    const/4 v5, -0x2

    invoke-direct {v2, v4, v5}, Landroid/widget/TableLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v2}, Landroid/widget/TableRow;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move v2, v1

    .line 2664259
    :goto_1
    iget v4, p0, LX/JDa;->c:I

    if-ge v2, v4, :cond_0

    .line 2664260
    invoke-virtual {p2, p3, v3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 2664261
    invoke-virtual {v3, v4}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 2664262
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2664263
    :cond_0
    invoke-virtual {p0, v3}, LX/JDa;->addView(Landroid/view/View;)V

    .line 2664264
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2664265
    :cond_1
    invoke-virtual {p0, v6}, LX/JDa;->setShrinkAllColumns(Z)V

    .line 2664266
    invoke-virtual {p0, v6}, LX/JDa;->setStretchAllColumns(Z)V

    .line 2664267
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/view/LayoutInflater;I)LX/JDa;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2664268
    new-instance v0, LX/JDa;

    invoke-direct {v0, p0, p1, p2}, LX/JDa;-><init>(Landroid/content/Context;Landroid/view/LayoutInflater;I)V

    .line 2664269
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b2493

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2664270
    invoke-virtual {v0, v3, v1, v3, v1}, LX/JDa;->setPadding(IIII)V

    .line 2664271
    return-object v0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/JDa;

    invoke-static {p0}, LX/J9L;->a(LX/0QB;)LX/J9L;

    move-result-object p0

    check-cast p0, LX/J9L;

    iput-object p0, p1, LX/JDa;->a:LX/J9L;

    return-void
.end method

.method public static b(Landroid/content/Context;Landroid/view/LayoutInflater;I)LX/JDa;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2664272
    new-instance v0, LX/JDa;

    invoke-direct {v0, p0, p1, p2}, LX/JDa;-><init>(Landroid/content/Context;Landroid/view/LayoutInflater;I)V

    .line 2664273
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b2493

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2664274
    invoke-virtual {v0, v1, v3, v1, v3}, LX/JDa;->setPadding(IIII)V

    .line 2664275
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/util/List;LX/9lP;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;)V
    .locals 9
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "bindModel"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLInterfaces$AppCollectionItem$;",
            ">;",
            "LX/9lP;",
            "Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 2664276
    move v4, v5

    :goto_0
    iget v0, p0, LX/JDa;->b:I

    if-ge v4, v0, :cond_4

    .line 2664277
    invoke-virtual {p0, v4}, LX/JDa;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    .line 2664278
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    iget v2, p0, LX/JDa;->c:I

    mul-int/2addr v2, v4

    if-gt v1, v2, :cond_1

    .line 2664279
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TableRow;->setVisibility(I)V

    .line 2664280
    :cond_0
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 2664281
    :cond_1
    invoke-virtual {v0, v5}, Landroid/widget/TableRow;->setVisibility(I)V

    move v6, v5

    .line 2664282
    :goto_1
    iget v1, p0, LX/JDa;->c:I

    if-ge v6, v1, :cond_0

    .line 2664283
    iget v1, p0, LX/JDa;->c:I

    mul-int/2addr v1, v4

    add-int v7, v1, v6

    .line 2664284
    invoke-virtual {v0, v6}, Landroid/widget/TableRow;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 2664285
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v7, v1, :cond_3

    .line 2664286
    const/4 v1, 0x4

    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2664287
    :cond_2
    :goto_2
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto :goto_1

    .line 2664288
    :cond_3
    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;

    .line 2664289
    iget v2, p0, LX/JDa;->b:I

    iget v8, p0, LX/JDa;->c:I

    mul-int/2addr v8, v2

    move-object v2, v3

    .line 2664290
    check-cast v2, LX/JCi;

    .line 2664291
    invoke-interface {v2, v1}, LX/JCi;->a(Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;)V

    .line 2664292
    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 2664293
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    invoke-virtual {p3, v3}, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2664294
    check-cast v2, Lcom/facebook/timeline/aboutpage/views/PhotoCollectionItemView;

    invoke-virtual {v2, v1, v7, v8}, Lcom/facebook/timeline/aboutpage/views/PhotoCollectionItemView;->a(Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;II)V

    goto :goto_2

    .line 2664295
    :cond_4
    return-void
.end method
