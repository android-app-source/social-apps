.class public final LX/Hzi;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/location/ImmutableLocation;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;)V
    .locals 0

    .prologue
    .line 2525906
    iput-object p1, p0, LX/Hzi;->a:Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 2525912
    iget-object v0, p0, LX/Hzi;->a:Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->f:LX/I5M;

    const-string v1, "ANDROID_EVENT_DISCOVER_DASHBOARD"

    iget-object v2, p0, LX/Hzi;->a:Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;

    iget-object v2, v2, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->H:LX/0Vd;

    iget-object v3, p0, LX/Hzi;->a:Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;

    iget-object v3, v3, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->o:LX/0m9;

    const-string v4, "events_tabbed_dashboard"

    invoke-virtual {v0, v1, v2, v3, v4}, LX/I5M;->a(Ljava/lang/String;LX/0Vd;LX/0m9;Ljava/lang/String;)V

    .line 2525913
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2525907
    check-cast p1, Lcom/facebook/location/ImmutableLocation;

    .line 2525908
    if-eqz p1, :cond_0

    .line 2525909
    iget-object v0, p0, LX/Hzi;->a:Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->o:LX/0m9;

    const-string v1, "lat_lon"

    invoke-static {p1}, Lcom/facebook/events/dashboard/EventsDiscoveryDashboardFragment;->a(Lcom/facebook/location/ImmutableLocation;)LX/0m9;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 2525910
    :cond_0
    iget-object v0, p0, LX/Hzi;->a:Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;

    iget-object v0, v0, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->f:LX/I5M;

    const-string v1, "ANDROID_EVENT_DISCOVER_DASHBOARD"

    iget-object v2, p0, LX/Hzi;->a:Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;

    iget-object v2, v2, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->H:LX/0Vd;

    iget-object v3, p0, LX/Hzi;->a:Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;

    iget-object v3, v3, Lcom/facebook/events/dashboard/EventsTabbedDashboardFragment;->o:LX/0m9;

    const-string v4, "events_tabbed_dashboard"

    invoke-virtual {v0, v1, v2, v3, v4}, LX/I5M;->a(Ljava/lang/String;LX/0Vd;LX/0m9;Ljava/lang/String;)V

    .line 2525911
    return-void
.end method
