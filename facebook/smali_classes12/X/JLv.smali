.class public LX/JLv;
.super Landroid/widget/FrameLayout;
.source ""

# interfaces
.implements LX/1O9;
.implements LX/0vZ;


# static fields
.field private static final v:LX/JMB;

.field private static final w:[I


# instance fields
.field public A:I

.field public B:I

.field private a:J

.field public final b:Landroid/graphics/Rect;

.field public c:LX/1Og;

.field private d:LX/0vj;

.field private e:LX/0vj;

.field private f:I

.field private g:Z

.field private h:Z

.field private i:Landroid/view/View;

.field private j:Z

.field public k:Landroid/view/VelocityTracker;

.field public l:Z

.field private m:Z

.field public n:I

.field public o:I

.field public p:I

.field private q:I

.field private final r:[I

.field private final s:[I

.field private t:I

.field private u:Lcom/facebook/fbreact/views/fbscroll/BugFixScrollView$SavedState;

.field private final x:LX/1uH;

.field private final y:LX/1Oy;

.field private z:F


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2682523
    new-instance v0, LX/JMB;

    invoke-direct {v0}, LX/JMB;-><init>()V

    sput-object v0, LX/JLv;->v:LX/JMB;

    .line 2682524
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x101017a

    aput v2, v0, v1

    sput-object v0, LX/JLv;->w:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2682521
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/JLv;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2682522
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2682519
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/JLv;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2682520
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2682488
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2682489
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/JLv;->b:Landroid/graphics/Rect;

    .line 2682490
    iput-boolean v2, p0, LX/JLv;->g:Z

    .line 2682491
    iput-boolean v1, p0, LX/JLv;->h:Z

    .line 2682492
    const/4 v0, 0x0

    iput-object v0, p0, LX/JLv;->i:Landroid/view/View;

    .line 2682493
    iput-boolean v1, p0, LX/JLv;->j:Z

    .line 2682494
    iput-boolean v2, p0, LX/JLv;->m:Z

    .line 2682495
    const/4 v0, -0x1

    iput v0, p0, LX/JLv;->q:I

    .line 2682496
    new-array v0, v3, [I

    iput-object v0, p0, LX/JLv;->r:[I

    .line 2682497
    new-array v0, v3, [I

    iput-object v0, p0, LX/JLv;->s:[I

    .line 2682498
    iput v1, p0, LX/JLv;->A:I

    .line 2682499
    iput v1, p0, LX/JLv;->B:I

    .line 2682500
    invoke-virtual {p0}, LX/JLv;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v3, 0x0

    invoke-static {v0, v3}, LX/1Og;->a(Landroid/content/Context;Landroid/view/animation/Interpolator;)LX/1Og;

    move-result-object v0

    iput-object v0, p0, LX/JLv;->c:LX/1Og;

    .line 2682501
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/JLv;->setFocusable(Z)V

    .line 2682502
    const/high16 v0, 0x40000

    invoke-virtual {p0, v0}, LX/JLv;->setDescendantFocusability(I)V

    .line 2682503
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/JLv;->setWillNotDraw(Z)V

    .line 2682504
    invoke-virtual {p0}, LX/JLv;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 2682505
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v3

    iput v3, p0, LX/JLv;->n:I

    .line 2682506
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v3

    iput v3, p0, LX/JLv;->o:I

    .line 2682507
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v0

    iput v0, p0, LX/JLv;->p:I

    .line 2682508
    sget-object v0, LX/JLv;->w:[I

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2682509
    invoke-virtual {v0, v1, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    .line 2682510
    iget-boolean v3, p0, LX/JLv;->l:Z

    if-eq v1, v3, :cond_0

    .line 2682511
    iput-boolean v1, p0, LX/JLv;->l:Z

    .line 2682512
    invoke-virtual {p0}, LX/JLv;->requestLayout()V

    .line 2682513
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 2682514
    new-instance v0, LX/1uH;

    invoke-direct {v0, p0}, LX/1uH;-><init>(Landroid/view/ViewGroup;)V

    iput-object v0, p0, LX/JLv;->x:LX/1uH;

    .line 2682515
    new-instance v0, LX/1Oy;

    invoke-direct {v0, p0}, LX/1Oy;-><init>(Landroid/view/View;)V

    iput-object v0, p0, LX/JLv;->y:LX/1Oy;

    .line 2682516
    invoke-virtual {p0, v2}, LX/JLv;->setNestedScrollingEnabled(Z)V

    .line 2682517
    sget-object v0, LX/JLv;->v:LX/JMB;

    invoke-static {p0, v0}, LX/0vv;->a(Landroid/view/View;LX/0vn;)V

    .line 2682518
    return-void
.end method

.method public static a(LX/JLv;Landroid/graphics/Rect;)I
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 2682465
    invoke-virtual {p0}, LX/JLv;->getChildCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 2682466
    :goto_0
    return v2

    .line 2682467
    :cond_0
    invoke-virtual {p0}, LX/JLv;->getHeight()I

    move-result v3

    .line 2682468
    invoke-virtual {p0}, LX/JLv;->getScrollY()I

    move-result v0

    .line 2682469
    add-int v1, v0, v3

    .line 2682470
    invoke-virtual {p0}, LX/JLv;->getVerticalFadingEdgeLength()I

    move-result v4

    .line 2682471
    iget v5, p1, Landroid/graphics/Rect;->top:I

    iget v6, p0, LX/JLv;->A:I

    if-le v5, v6, :cond_1

    .line 2682472
    add-int/2addr v0, v4

    .line 2682473
    :cond_1
    iget v5, p1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p0, v2}, LX/JLv;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v6

    iget v7, p0, LX/JLv;->B:I

    sub-int/2addr v6, v7

    if-ge v5, v6, :cond_2

    .line 2682474
    sub-int/2addr v1, v4

    .line 2682475
    :cond_2
    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    if-le v4, v1, :cond_4

    iget v4, p1, Landroid/graphics/Rect;->top:I

    if-le v4, v0, :cond_4

    .line 2682476
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v4

    if-le v4, v3, :cond_3

    .line 2682477
    iget v3, p1, Landroid/graphics/Rect;->top:I

    sub-int v0, v3, v0

    add-int/lit8 v0, v0, 0x0

    .line 2682478
    :goto_1
    invoke-virtual {p0, v2}, LX/JLv;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v2

    iget v3, p0, LX/JLv;->B:I

    sub-int/2addr v2, v3

    .line 2682479
    sub-int v1, v2, v1

    .line 2682480
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    :goto_2
    move v2, v0

    .line 2682481
    goto :goto_0

    .line 2682482
    :cond_3
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    goto :goto_1

    .line 2682483
    :cond_4
    iget v4, p1, Landroid/graphics/Rect;->top:I

    if-ge v4, v0, :cond_6

    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    if-ge v4, v1, :cond_6

    .line 2682484
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v2

    if-le v2, v3, :cond_5

    .line 2682485
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    sub-int v0, v1, v0

    rsub-int/lit8 v0, v0, 0x0

    .line 2682486
    :goto_3
    invoke-virtual {p0}, LX/JLv;->getScrollY()I

    move-result v1

    neg-int v1, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_2

    .line 2682487
    :cond_5
    iget v1, p1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v1

    rsub-int/lit8 v0, v0, 0x0

    goto :goto_3

    :cond_6
    move v0, v2

    goto :goto_2
.end method

.method private a(ZII)Landroid/view/View;
    .locals 11

    .prologue
    .line 2682442
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, LX/JLv;->getFocusables(I)Ljava/util/ArrayList;

    move-result-object v6

    .line 2682443
    const/4 v3, 0x0

    .line 2682444
    const/4 v2, 0x0

    .line 2682445
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    .line 2682446
    const/4 v0, 0x0

    move v5, v0

    :goto_0
    if-ge v5, v7, :cond_7

    .line 2682447
    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2682448
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v4

    .line 2682449
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v8

    .line 2682450
    if-ge p2, v8, :cond_8

    if-ge v4, p3, :cond_8

    .line 2682451
    if-ge p2, v4, :cond_0

    if-ge v8, p3, :cond_0

    const/4 v1, 0x1

    .line 2682452
    :goto_1
    if-nez v3, :cond_1

    move v10, v1

    move-object v1, v0

    move v0, v10

    .line 2682453
    :goto_2
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    move-object v3, v1

    move v2, v0

    goto :goto_0

    .line 2682454
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 2682455
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v9

    if-lt v4, v9, :cond_3

    :cond_2
    if-nez p1, :cond_4

    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    move-result v4

    if-le v8, v4, :cond_4

    :cond_3
    const/4 v4, 0x1

    .line 2682456
    :goto_3
    if-eqz v2, :cond_5

    .line 2682457
    if-eqz v1, :cond_8

    if-eqz v4, :cond_8

    move-object v1, v0

    move v0, v2

    .line 2682458
    goto :goto_2

    .line 2682459
    :cond_4
    const/4 v4, 0x0

    goto :goto_3

    .line 2682460
    :cond_5
    if-eqz v1, :cond_6

    .line 2682461
    const/4 v1, 0x1

    move v10, v1

    move-object v1, v0

    move v0, v10

    goto :goto_2

    .line 2682462
    :cond_6
    if-eqz v4, :cond_8

    move-object v1, v0

    move v0, v2

    .line 2682463
    goto :goto_2

    .line 2682464
    :cond_7
    return-object v3

    :cond_8
    move v0, v2

    move-object v1, v3

    goto :goto_2
.end method

.method private a(Landroid/view/MotionEvent;)V
    .locals 3

    .prologue
    .line 2682432
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const v1, 0xff00

    and-int/2addr v0, v1

    shr-int/lit8 v0, v0, 0x8

    .line 2682433
    invoke-static {p1, v0}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v1

    .line 2682434
    iget v2, p0, LX/JLv;->q:I

    if-ne v1, v2, :cond_0

    .line 2682435
    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 2682436
    :goto_0
    invoke-static {p1, v0}, LX/2xd;->d(Landroid/view/MotionEvent;I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, LX/JLv;->f:I

    .line 2682437
    invoke-static {p1, v0}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, LX/JLv;->q:I

    .line 2682438
    iget-object v0, p0, LX/JLv;->k:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    .line 2682439
    iget-object v0, p0, LX/JLv;->k:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    .line 2682440
    :cond_0
    return-void

    .line 2682441
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(IIIIIIII)Z
    .locals 7

    .prologue
    .line 2682400
    invoke-static {p0}, LX/0vv;->a(Landroid/view/View;)I

    move-result v3

    .line 2682401
    invoke-virtual {p0}, LX/JLv;->computeHorizontalScrollRange()I

    move-result v0

    invoke-virtual {p0}, LX/JLv;->computeHorizontalScrollExtent()I

    move-result v1

    if-le v0, v1, :cond_5

    const/4 v0, 0x1

    .line 2682402
    :goto_0
    invoke-virtual {p0}, LX/JLv;->computeVerticalScrollRange()I

    move-result v1

    invoke-virtual {p0}, LX/JLv;->computeVerticalScrollExtent()I

    move-result v2

    if-le v1, v2, :cond_6

    const/4 v1, 0x1

    move v2, v1

    .line 2682403
    :goto_1
    if-eqz v3, :cond_0

    const/4 v1, 0x1

    if-ne v3, v1, :cond_7

    if-eqz v0, :cond_7

    :cond_0
    const/4 v0, 0x1

    move v1, v0

    .line 2682404
    :goto_2
    if-eqz v3, :cond_1

    const/4 v0, 0x1

    if-ne v3, v0, :cond_8

    if-eqz v2, :cond_8

    :cond_1
    const/4 v0, 0x1

    .line 2682405
    :goto_3
    add-int v6, p3, p1

    .line 2682406
    if-nez v1, :cond_2

    .line 2682407
    const/4 p7, 0x0

    .line 2682408
    :cond_2
    add-int v3, p4, p2

    .line 2682409
    if-nez v0, :cond_3

    .line 2682410
    const/4 p8, 0x0

    .line 2682411
    :cond_3
    neg-int v5, p7

    .line 2682412
    add-int v4, p7, p5

    .line 2682413
    neg-int v0, p8

    iget v1, p0, LX/JLv;->A:I

    add-int v2, v0, v1

    .line 2682414
    add-int v1, p8, p6

    .line 2682415
    const/4 v0, 0x0

    .line 2682416
    if-le v6, v4, :cond_9

    .line 2682417
    const/4 v0, 0x1

    move v5, v4

    move v4, v0

    .line 2682418
    :goto_4
    const/4 v0, 0x0

    .line 2682419
    if-le v3, v1, :cond_a

    .line 2682420
    const/4 v0, 0x1

    .line 2682421
    :goto_5
    invoke-virtual {p0, v5, v1, v4, v0}, LX/JLv;->onOverScrolled(IIZZ)V

    .line 2682422
    if-nez v4, :cond_4

    if-eqz v0, :cond_b

    :cond_4
    const/4 v0, 0x1

    :goto_6
    return v0

    .line 2682423
    :cond_5
    const/4 v0, 0x0

    goto :goto_0

    .line 2682424
    :cond_6
    const/4 v1, 0x0

    move v2, v1

    goto :goto_1

    .line 2682425
    :cond_7
    const/4 v0, 0x0

    move v1, v0

    goto :goto_2

    .line 2682426
    :cond_8
    const/4 v0, 0x0

    goto :goto_3

    .line 2682427
    :cond_9
    if-ge v6, v5, :cond_d

    .line 2682428
    const/4 v0, 0x1

    move v4, v0

    goto :goto_4

    .line 2682429
    :cond_a
    if-ge v3, v2, :cond_c

    .line 2682430
    const/4 v0, 0x1

    move v1, v2

    goto :goto_5

    .line 2682431
    :cond_b
    const/4 v0, 0x0

    goto :goto_6

    :cond_c
    move v1, v3

    goto :goto_5

    :cond_d
    move v4, v0

    move v5, v6

    goto :goto_4
.end method

.method public static a(LX/JLv;III)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2682310
    invoke-virtual {p0}, LX/JLv;->getHeight()I

    move-result v0

    .line 2682311
    invoke-virtual {p0}, LX/JLv;->getScrollY()I

    move-result v4

    .line 2682312
    add-int v5, v4, v0

    .line 2682313
    const/16 v0, 0x21

    if-ne p1, v0, :cond_2

    move v0, v1

    .line 2682314
    :goto_0
    invoke-direct {p0, v0, p2, p3}, LX/JLv;->a(ZII)Landroid/view/View;

    move-result-object v3

    .line 2682315
    if-nez v3, :cond_0

    move-object v3, p0

    .line 2682316
    :cond_0
    if-lt p2, v4, :cond_3

    if-gt p3, v5, :cond_3

    .line 2682317
    :goto_1
    invoke-virtual {p0}, LX/JLv;->findFocus()Landroid/view/View;

    move-result-object v0

    if-eq v3, v0, :cond_1

    invoke-virtual {v3, p1}, Landroid/view/View;->requestFocus(I)Z

    .line 2682318
    :cond_1
    return v2

    :cond_2
    move v0, v2

    .line 2682319
    goto :goto_0

    .line 2682320
    :cond_3
    if-eqz v0, :cond_4

    sub-int v0, p2, v4

    .line 2682321
    :goto_2
    invoke-direct {p0, v0}, LX/JLv;->e(I)V

    move v2, v1

    goto :goto_1

    .line 2682322
    :cond_4
    sub-int v0, p3, v5

    goto :goto_2
.end method

.method private a(Landroid/view/View;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2682398
    invoke-virtual {p0}, LX/JLv;->getHeight()I

    move-result v1

    invoke-direct {p0, p1, v0, v1}, LX/JLv;->a(Landroid/view/View;II)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private a(Landroid/view/View;II)Z
    .locals 2

    .prologue
    .line 2682395
    iget-object v0, p0, LX/JLv;->b:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 2682396
    iget-object v0, p0, LX/JLv;->b:Landroid/graphics/Rect;

    invoke-virtual {p0, p1, v0}, LX/JLv;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 2682397
    iget-object v0, p0, LX/JLv;->b:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, p2

    invoke-virtual {p0}, LX/JLv;->getScrollY()I

    move-result v1

    if-lt v0, v1, :cond_0

    iget-object v0, p0, LX/JLv;->b:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, p2

    invoke-virtual {p0}, LX/JLv;->getScrollY()I

    move-result v1

    add-int/2addr v1, p3

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Landroid/view/View;Landroid/view/View;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 2682391
    if-ne p0, p1, :cond_0

    move v0, v1

    .line 2682392
    :goto_0
    return v0

    .line 2682393
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 2682394
    instance-of v2, v0, Landroid/view/ViewGroup;

    if-eqz v2, :cond_1

    check-cast v0, Landroid/view/View;

    invoke-static {v0, p1}, LX/JLv;->a(Landroid/view/View;Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(III)I
    .locals 1

    .prologue
    .line 2682386
    if-ge p1, p2, :cond_0

    if-gez p0, :cond_2

    .line 2682387
    :cond_0
    const/4 p0, 0x0

    .line 2682388
    :cond_1
    :goto_0
    return p0

    .line 2682389
    :cond_2
    add-int v0, p1, p0

    if-le v0, p2, :cond_1

    .line 2682390
    sub-int p0, p2, p1

    goto :goto_0
.end method

.method private b(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2682380
    iget-object v0, p0, LX/JLv;->b:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 2682381
    iget-object v0, p0, LX/JLv;->b:Landroid/graphics/Rect;

    invoke-virtual {p0, p1, v0}, LX/JLv;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 2682382
    iget-object v0, p0, LX/JLv;->b:Landroid/graphics/Rect;

    invoke-static {p0, v0}, LX/JLv;->a(LX/JLv;Landroid/graphics/Rect;)I

    move-result v0

    .line 2682383
    if-eqz v0, :cond_0

    .line 2682384
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, LX/JLv;->scrollBy(II)V

    .line 2682385
    :cond_0
    return-void
.end method

.method public static c(LX/JLv;II)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2682365
    invoke-virtual {p0}, LX/JLv;->getChildCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 2682366
    :goto_0
    return-void

    .line 2682367
    :cond_0
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, LX/JLv;->a:J

    sub-long/2addr v0, v2

    .line 2682368
    const-wide/16 v2, 0xfa

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 2682369
    invoke-virtual {p0}, LX/JLv;->getHeight()I

    move-result v0

    invoke-virtual {p0}, LX/JLv;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, LX/JLv;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    .line 2682370
    invoke-virtual {p0, v4}, LX/JLv;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    iget v2, p0, LX/JLv;->B:I

    sub-int/2addr v1, v2

    .line 2682371
    sub-int v0, v1, v0

    invoke-static {v4, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 2682372
    invoke-virtual {p0}, LX/JLv;->getScrollY()I

    move-result v1

    .line 2682373
    iget v2, p0, LX/JLv;->A:I

    add-int v3, v1, p2

    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    sub-int/2addr v0, v1

    .line 2682374
    iget-object v2, p0, LX/JLv;->c:LX/1Og;

    invoke-virtual {p0}, LX/JLv;->getScrollX()I

    move-result v3

    invoke-virtual {v2, v3, v1, v4, v0}, LX/1Og;->a(IIII)V

    .line 2682375
    invoke-static {p0}, LX/0vv;->d(Landroid/view/View;)V

    .line 2682376
    :goto_1
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, LX/JLv;->a:J

    goto :goto_0

    .line 2682377
    :cond_1
    iget-object v0, p0, LX/JLv;->c:LX/1Og;

    invoke-virtual {v0}, LX/1Og;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2682378
    iget-object v0, p0, LX/JLv;->c:LX/1Og;

    invoke-virtual {v0}, LX/1Og;->h()V

    .line 2682379
    :cond_2
    invoke-virtual {p0, p1, p2}, LX/JLv;->scrollBy(II)V

    goto :goto_1
.end method

.method public static c(LX/JLv;I)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2682353
    const/16 v0, 0x82

    if-ne p1, v0, :cond_1

    const/4 v0, 0x1

    .line 2682354
    :goto_0
    invoke-virtual {p0}, LX/JLv;->getHeight()I

    move-result v2

    .line 2682355
    iget-object v3, p0, LX/JLv;->b:Landroid/graphics/Rect;

    iput v1, v3, Landroid/graphics/Rect;->top:I

    .line 2682356
    iget-object v1, p0, LX/JLv;->b:Landroid/graphics/Rect;

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    .line 2682357
    if-eqz v0, :cond_0

    .line 2682358
    invoke-virtual {p0}, LX/JLv;->getChildCount()I

    move-result v0

    .line 2682359
    if-lez v0, :cond_0

    .line 2682360
    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, LX/JLv;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2682361
    iget-object v1, p0, LX/JLv;->b:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    invoke-virtual {p0}, LX/JLv;->getPaddingBottom()I

    move-result v3

    add-int/2addr v0, v3

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 2682362
    iget-object v0, p0, LX/JLv;->b:Landroid/graphics/Rect;

    iget-object v1, p0, LX/JLv;->b:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 2682363
    :cond_0
    iget-object v0, p0, LX/JLv;->b:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, LX/JLv;->b:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    invoke-static {p0, p1, v0, v1}, LX/JLv;->a(LX/JLv;III)Z

    move-result v0

    return v0

    :cond_1
    move v0, v1

    .line 2682364
    goto :goto_0
.end method

.method private d()V
    .locals 1

    .prologue
    .line 2682350
    iget-object v0, p0, LX/JLv;->k:Landroid/view/VelocityTracker;

    if-nez v0, :cond_0

    .line 2682351
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, LX/JLv;->k:Landroid/view/VelocityTracker;

    .line 2682352
    :cond_0
    return-void
.end method

.method public static d(LX/JLv;I)Z
    .locals 7

    .prologue
    const/16 v6, 0x82

    const/4 v2, 0x0

    .line 2682323
    invoke-virtual {p0}, LX/JLv;->findFocus()Landroid/view/View;

    move-result-object v0

    .line 2682324
    if-ne v0, p0, :cond_0

    const/4 v0, 0x0

    .line 2682325
    :cond_0
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v1

    invoke-virtual {v1, p0, v0, p1}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    .line 2682326
    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {p0}, LX/JLv;->getHeight()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v1, v4

    float-to-int v1, v1

    move v1, v1

    .line 2682327
    if-eqz v3, :cond_2

    invoke-virtual {p0}, LX/JLv;->getHeight()I

    move-result v4

    invoke-direct {p0, v3, v1, v4}, LX/JLv;->a(Landroid/view/View;II)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2682328
    iget-object v1, p0, LX/JLv;->b:Landroid/graphics/Rect;

    invoke-virtual {v3, v1}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 2682329
    iget-object v1, p0, LX/JLv;->b:Landroid/graphics/Rect;

    invoke-virtual {p0, v3, v1}, LX/JLv;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 2682330
    iget-object v1, p0, LX/JLv;->b:Landroid/graphics/Rect;

    invoke-static {p0, v1}, LX/JLv;->a(LX/JLv;Landroid/graphics/Rect;)I

    move-result v1

    .line 2682331
    invoke-direct {p0, v1}, LX/JLv;->e(I)V

    .line 2682332
    invoke-virtual {v3, p1}, Landroid/view/View;->requestFocus(I)Z

    .line 2682333
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->isFocused()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0, v0}, LX/JLv;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2682334
    invoke-virtual {p0}, LX/JLv;->getDescendantFocusability()I

    move-result v0

    .line 2682335
    const/high16 v1, 0x20000

    invoke-virtual {p0, v1}, LX/JLv;->setDescendantFocusability(I)V

    .line 2682336
    invoke-virtual {p0}, LX/JLv;->requestFocus()Z

    .line 2682337
    invoke-virtual {p0, v0}, LX/JLv;->setDescendantFocusability(I)V

    .line 2682338
    :cond_1
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 2682339
    :cond_2
    const/16 v3, 0x21

    if-ne p1, v3, :cond_4

    invoke-virtual {p0}, LX/JLv;->getScrollY()I

    move-result v3

    if-ge v3, v1, :cond_4

    .line 2682340
    invoke-virtual {p0}, LX/JLv;->getScrollY()I

    move-result v1

    .line 2682341
    :cond_3
    :goto_2
    if-nez v1, :cond_5

    move v0, v2

    .line 2682342
    goto :goto_1

    .line 2682343
    :cond_4
    if-ne p1, v6, :cond_3

    .line 2682344
    invoke-virtual {p0}, LX/JLv;->getChildCount()I

    move-result v3

    if-lez v3, :cond_3

    .line 2682345
    invoke-virtual {p0, v2}, LX/JLv;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    move-result v3

    .line 2682346
    invoke-virtual {p0}, LX/JLv;->getScrollY()I

    move-result v4

    invoke-virtual {p0}, LX/JLv;->getHeight()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {p0}, LX/JLv;->getPaddingBottom()I

    move-result v5

    sub-int/2addr v4, v5

    .line 2682347
    sub-int v5, v3, v4

    if-ge v5, v1, :cond_3

    .line 2682348
    sub-int v1, v3, v4

    goto :goto_2

    .line 2682349
    :cond_5
    if-ne p1, v6, :cond_6

    :goto_3
    invoke-direct {p0, v1}, LX/JLv;->e(I)V

    goto :goto_0

    :cond_6
    neg-int v1, v1

    goto :goto_3
.end method

.method private e()V
    .locals 1

    .prologue
    .line 2682527
    iget-object v0, p0, LX/JLv;->k:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    .line 2682528
    iget-object v0, p0, LX/JLv;->k:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 2682529
    const/4 v0, 0x0

    iput-object v0, p0, LX/JLv;->k:Landroid/view/VelocityTracker;

    .line 2682530
    :cond_0
    return-void
.end method

.method private e(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2682620
    if-eqz p1, :cond_0

    .line 2682621
    iget-boolean v0, p0, LX/JLv;->m:Z

    if-eqz v0, :cond_1

    .line 2682622
    invoke-static {p0, v1, p1}, LX/JLv;->c(LX/JLv;II)V

    .line 2682623
    :cond_0
    :goto_0
    return-void

    .line 2682624
    :cond_1
    invoke-virtual {p0, v1, p1}, LX/JLv;->scrollBy(II)V

    goto :goto_0
.end method

.method private f()V
    .locals 1

    .prologue
    .line 2682625
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/JLv;->j:Z

    .line 2682626
    invoke-direct {p0}, LX/JLv;->e()V

    .line 2682627
    iget-object v0, p0, LX/JLv;->d:LX/0vj;

    if-eqz v0, :cond_0

    .line 2682628
    iget-object v0, p0, LX/JLv;->d:LX/0vj;

    invoke-virtual {v0}, LX/0vj;->c()Z

    .line 2682629
    iget-object v0, p0, LX/JLv;->e:LX/0vj;

    invoke-virtual {v0}, LX/0vj;->c()Z

    .line 2682630
    :cond_0
    return-void
.end method

.method private f(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2682631
    invoke-virtual {p0}, LX/JLv;->getScrollY()I

    move-result v0

    .line 2682632
    if-gtz v0, :cond_0

    if-lez p1, :cond_3

    :cond_0
    invoke-static {p0}, LX/JLv;->getScrollRange(LX/JLv;)I

    move-result v1

    if-lt v0, v1, :cond_1

    if-gez p1, :cond_3

    :cond_1
    const/4 v0, 0x1

    .line 2682633
    :goto_0
    int-to-float v1, p1

    invoke-virtual {p0, v2, v1}, LX/JLv;->dispatchNestedPreFling(FF)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2682634
    int-to-float v1, p1

    invoke-virtual {p0, v2, v1, v0}, LX/JLv;->dispatchNestedFling(FFZ)Z

    .line 2682635
    if-eqz v0, :cond_2

    .line 2682636
    invoke-virtual {p0, p1}, LX/JLv;->a(I)V

    .line 2682637
    :cond_2
    return-void

    .line 2682638
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2682639
    invoke-static {p0}, LX/0vv;->a(Landroid/view/View;)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 2682640
    iget-object v0, p0, LX/JLv;->d:LX/0vj;

    if-nez v0, :cond_0

    .line 2682641
    invoke-virtual {p0}, LX/JLv;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2682642
    new-instance v1, LX/0vj;

    invoke-direct {v1, v0}, LX/0vj;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, LX/JLv;->d:LX/0vj;

    .line 2682643
    new-instance v1, LX/0vj;

    invoke-direct {v1, v0}, LX/0vj;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, LX/JLv;->e:LX/0vj;

    .line 2682644
    :cond_0
    :goto_0
    return-void

    .line 2682645
    :cond_1
    iput-object v2, p0, LX/JLv;->d:LX/0vj;

    .line 2682646
    iput-object v2, p0, LX/JLv;->e:LX/0vj;

    goto :goto_0
.end method

.method public static getScrollRange(LX/JLv;)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2682658
    invoke-virtual {p0}, LX/JLv;->getChildCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 2682659
    invoke-virtual {p0, v0}, LX/JLv;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2682660
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    invoke-virtual {p0}, LX/JLv;->getHeight()I

    move-result v2

    invoke-virtual {p0}, LX/JLv;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, LX/JLv;->getPaddingTop()I

    move-result v3

    sub-int/2addr v2, v3

    sub-int/2addr v1, v2

    iget v2, p0, LX/JLv;->B:I

    sub-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 2682661
    :cond_0
    return v0
.end method

.method private getVerticalScrollFactorCompat()F
    .locals 5

    .prologue
    .line 2682647
    iget v0, p0, LX/JLv;->z:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    .line 2682648
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 2682649
    invoke-virtual {p0}, LX/JLv;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2682650
    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    const v3, 0x101004d

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2682651
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Expected theme to define listPreferredItemHeight."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2682652
    :cond_0
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/util/TypedValue;->getDimension(Landroid/util/DisplayMetrics;)F

    move-result v0

    iput v0, p0, LX/JLv;->z:F

    .line 2682653
    :cond_1
    iget v0, p0, LX/JLv;->z:F

    return v0
.end method


# virtual methods
.method public a(I)V
    .locals 11

    .prologue
    const/4 v3, 0x0

    .line 2682662
    invoke-virtual {p0}, LX/JLv;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 2682663
    invoke-virtual {p0}, LX/JLv;->getHeight()I

    move-result v0

    invoke-virtual {p0}, LX/JLv;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, LX/JLv;->getPaddingTop()I

    move-result v1

    sub-int v4, v0, v1

    .line 2682664
    iget-object v0, p0, LX/JLv;->c:LX/1Og;

    invoke-virtual {p0}, LX/JLv;->getScrollX()I

    move-result v1

    invoke-virtual {p0}, LX/JLv;->getScrollY()I

    move-result v2

    const v8, 0x7fffffff

    div-int/lit8 v10, v4, 0x2

    move v4, p1

    move v5, v3

    move v6, v3

    move v7, v3

    move v9, v3

    invoke-virtual/range {v0 .. v10}, LX/1Og;->a(IIIIIIIIII)V

    .line 2682665
    invoke-static {p0}, LX/0vv;->d(Landroid/view/View;)V

    .line 2682666
    :cond_0
    return-void
.end method

.method public final a(II)V
    .locals 2

    .prologue
    .line 2682618
    invoke-virtual {p0}, LX/JLv;->getScrollX()I

    move-result v0

    sub-int v0, p1, v0

    invoke-virtual {p0}, LX/JLv;->getScrollY()I

    move-result v1

    sub-int v1, p2, v1

    invoke-static {p0, v0, v1}, LX/JLv;->c(LX/JLv;II)V

    .line 2682619
    return-void
.end method

.method public final addView(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2682654
    invoke-virtual {p0}, LX/JLv;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 2682655
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ScrollView can host only one direct child"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2682656
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 2682657
    return-void
.end method

.method public final addView(Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 2682614
    invoke-virtual {p0}, LX/JLv;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 2682615
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ScrollView can host only one direct child"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2682616
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;I)V

    .line 2682617
    return-void
.end method

.method public final addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    .prologue
    .line 2682610
    invoke-virtual {p0}, LX/JLv;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 2682611
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ScrollView can host only one direct child"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2682612
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 2682613
    return-void
.end method

.method public final addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    .prologue
    .line 2682606
    invoke-virtual {p0}, LX/JLv;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 2682607
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ScrollView can host only one direct child"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2682608
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2682609
    return-void
.end method

.method public final computeScroll()V
    .locals 11

    .prologue
    const/4 v0, 0x1

    const/4 v5, 0x0

    .line 2682587
    iget-object v1, p0, LX/JLv;->c:LX/1Og;

    invoke-virtual {v1}, LX/1Og;->g()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2682588
    invoke-virtual {p0}, LX/JLv;->getScrollX()I

    move-result v3

    .line 2682589
    invoke-virtual {p0}, LX/JLv;->getScrollY()I

    move-result v4

    .line 2682590
    iget-object v1, p0, LX/JLv;->c:LX/1Og;

    invoke-virtual {v1}, LX/1Og;->b()I

    move-result v1

    .line 2682591
    iget-object v2, p0, LX/JLv;->c:LX/1Og;

    invoke-virtual {v2}, LX/1Og;->c()I

    move-result v10

    .line 2682592
    if-ne v3, v1, :cond_0

    if-eq v4, v10, :cond_5

    .line 2682593
    :cond_0
    invoke-static {p0}, LX/JLv;->getScrollRange(LX/JLv;)I

    move-result v6

    .line 2682594
    invoke-static {p0}, LX/0vv;->a(Landroid/view/View;)I

    move-result v2

    .line 2682595
    if-eqz v2, :cond_1

    if-ne v2, v0, :cond_3

    if-lez v6, :cond_3

    :cond_1
    move v9, v0

    .line 2682596
    :goto_0
    sub-int/2addr v1, v3

    sub-int v2, v10, v4

    move-object v0, p0

    move v7, v5

    move v8, v5

    invoke-direct/range {v0 .. v8}, LX/JLv;->a(IIIIIIII)Z

    .line 2682597
    if-eqz v9, :cond_2

    .line 2682598
    invoke-direct {p0}, LX/JLv;->g()V

    .line 2682599
    iget v0, p0, LX/JLv;->A:I

    if-gt v10, v0, :cond_4

    iget v0, p0, LX/JLv;->A:I

    if-le v4, v0, :cond_4

    .line 2682600
    iget-object v0, p0, LX/JLv;->d:LX/0vj;

    iget-object v1, p0, LX/JLv;->c:LX/1Og;

    invoke-virtual {v1}, LX/1Og;->f()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, LX/0vj;->a(I)Z

    .line 2682601
    :cond_2
    :goto_1
    return-void

    :cond_3
    move v9, v5

    .line 2682602
    goto :goto_0

    .line 2682603
    :cond_4
    if-lt v10, v6, :cond_2

    if-ge v4, v6, :cond_2

    .line 2682604
    iget-object v0, p0, LX/JLv;->e:LX/0vj;

    iget-object v1, p0, LX/JLv;->c:LX/1Og;

    invoke-virtual {v1}, LX/1Og;->f()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, LX/0vj;->a(I)Z

    goto :goto_1

    .line 2682605
    :cond_5
    iget-object v0, p0, LX/JLv;->c:LX/1Og;

    invoke-virtual {v0}, LX/1Og;->h()V

    goto :goto_1
.end method

.method public final computeVerticalScrollOffset()I
    .locals 2

    .prologue
    .line 2682586
    const/4 v0, 0x0

    invoke-super {p0}, Landroid/widget/FrameLayout;->computeVerticalScrollOffset()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public final computeVerticalScrollRange()I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2682575
    invoke-virtual {p0}, LX/JLv;->getChildCount()I

    move-result v0

    .line 2682576
    invoke-virtual {p0}, LX/JLv;->getHeight()I

    move-result v1

    invoke-virtual {p0}, LX/JLv;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, LX/JLv;->getPaddingTop()I

    move-result v2

    sub-int/2addr v1, v2

    .line 2682577
    if-nez v0, :cond_1

    move v0, v1

    .line 2682578
    :cond_0
    :goto_0
    return v0

    .line 2682579
    :cond_1
    invoke-virtual {p0, v3}, LX/JLv;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    .line 2682580
    invoke-virtual {p0}, LX/JLv;->getScrollY()I

    move-result v2

    .line 2682581
    sub-int v1, v0, v1

    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 2682582
    if-gez v2, :cond_2

    .line 2682583
    sub-int/2addr v0, v2

    goto :goto_0

    .line 2682584
    :cond_2
    if-le v2, v1, :cond_0

    .line 2682585
    sub-int v1, v2, v1

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public final dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 6

    .prologue
    .line 2682532
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_3

    const/16 v0, 0x21

    const/4 v2, 0x0

    const/16 v1, 0x82

    .line 2682533
    iget-object v3, p0, LX/JLv;->b:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->setEmpty()V

    .line 2682534
    const/4 v3, 0x0

    .line 2682535
    invoke-virtual {p0, v3}, LX/JLv;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 2682536
    if-eqz v4, :cond_0

    .line 2682537
    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    .line 2682538
    invoke-virtual {p0}, LX/JLv;->getPaddingTop()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {p0}, LX/JLv;->getPaddingBottom()I

    move-result v5

    add-int/2addr v4, v5

    iget v5, p0, LX/JLv;->A:I

    sub-int/2addr v4, v5

    iget v5, p0, LX/JLv;->B:I

    sub-int/2addr v4, v5

    .line 2682539
    invoke-virtual {p0}, LX/JLv;->getHeight()I

    move-result v5

    if-ge v5, v4, :cond_0

    const/4 v3, 0x1

    .line 2682540
    :cond_0
    move v3, v3

    .line 2682541
    if-nez v3, :cond_5

    .line 2682542
    invoke-virtual {p0}, LX/JLv;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v3, 0x4

    if-eq v0, v3, :cond_2

    .line 2682543
    invoke-virtual {p0}, LX/JLv;->findFocus()Landroid/view/View;

    move-result-object v0

    .line 2682544
    if-ne v0, p0, :cond_1

    const/4 v0, 0x0

    .line 2682545
    :cond_1
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v3

    invoke-virtual {v3, p0, v0, v1}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 2682546
    if-eqz v0, :cond_2

    if-eq v0, p0, :cond_2

    invoke-virtual {v0, v1}, Landroid/view/View;->requestFocus(I)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v2, 0x1

    .line 2682547
    :cond_2
    :goto_0
    move v0, v2

    .line 2682548
    if-eqz v0, :cond_4

    :cond_3
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 2682549
    :cond_5
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    if-nez v3, :cond_6

    .line 2682550
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_6
    :goto_2
    move v0, v2

    :goto_3
    move v2, v0

    .line 2682551
    goto :goto_0

    .line 2682552
    :sswitch_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isAltPressed()Z

    move-result v1

    if-nez v1, :cond_7

    .line 2682553
    invoke-static {p0, v0}, LX/JLv;->d(LX/JLv;I)Z

    move-result v0

    goto :goto_3

    .line 2682554
    :cond_7
    invoke-static {p0, v0}, LX/JLv;->c(LX/JLv;I)Z

    move-result v0

    goto :goto_3

    .line 2682555
    :sswitch_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isAltPressed()Z

    move-result v0

    if-nez v0, :cond_8

    .line 2682556
    invoke-static {p0, v1}, LX/JLv;->d(LX/JLv;I)Z

    move-result v0

    goto :goto_3

    .line 2682557
    :cond_8
    invoke-static {p0, v1}, LX/JLv;->c(LX/JLv;I)Z

    move-result v0

    goto :goto_3

    .line 2682558
    :sswitch_2
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 2682559
    :goto_4
    const/16 v1, 0x82

    if-ne v0, v1, :cond_b

    const/4 v1, 0x1

    .line 2682560
    :goto_5
    invoke-virtual {p0}, LX/JLv;->getHeight()I

    move-result v3

    .line 2682561
    if-eqz v1, :cond_c

    .line 2682562
    iget-object v1, p0, LX/JLv;->b:Landroid/graphics/Rect;

    invoke-virtual {p0}, LX/JLv;->getScrollY()I

    move-result v4

    add-int/2addr v4, v3

    iput v4, v1, Landroid/graphics/Rect;->top:I

    .line 2682563
    invoke-virtual {p0}, LX/JLv;->getChildCount()I

    move-result v1

    .line 2682564
    if-lez v1, :cond_9

    .line 2682565
    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, LX/JLv;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2682566
    iget-object v4, p0, LX/JLv;->b:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    add-int/2addr v4, v3

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v5

    if-le v4, v5, :cond_9

    .line 2682567
    iget-object v4, p0, LX/JLv;->b:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v1

    sub-int/2addr v1, v3

    iput v1, v4, Landroid/graphics/Rect;->top:I

    .line 2682568
    :cond_9
    :goto_6
    iget-object v1, p0, LX/JLv;->b:Landroid/graphics/Rect;

    iget-object v4, p0, LX/JLv;->b:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    add-int/2addr v3, v4

    iput v3, v1, Landroid/graphics/Rect;->bottom:I

    .line 2682569
    iget-object v1, p0, LX/JLv;->b:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, LX/JLv;->b:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    invoke-static {p0, v0, v1, v3}, LX/JLv;->a(LX/JLv;III)Z

    .line 2682570
    goto :goto_2

    :cond_a
    move v0, v1

    goto :goto_4

    .line 2682571
    :cond_b
    const/4 v1, 0x0

    goto :goto_5

    .line 2682572
    :cond_c
    iget-object v1, p0, LX/JLv;->b:Landroid/graphics/Rect;

    invoke-virtual {p0}, LX/JLv;->getScrollY()I

    move-result v4

    sub-int/2addr v4, v3

    iput v4, v1, Landroid/graphics/Rect;->top:I

    .line 2682573
    iget-object v1, p0, LX/JLv;->b:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget v4, p0, LX/JLv;->A:I

    if-ge v1, v4, :cond_9

    .line 2682574
    iget-object v1, p0, LX/JLv;->b:Landroid/graphics/Rect;

    iget v4, p0, LX/JLv;->A:I

    iput v4, v1, Landroid/graphics/Rect;->top:I

    goto :goto_6

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x14 -> :sswitch_1
        0x3e -> :sswitch_2
    .end sparse-switch
.end method

.method public final dispatchNestedFling(FFZ)Z
    .locals 1

    .prologue
    .line 2682531
    iget-object v0, p0, LX/JLv;->y:LX/1Oy;

    invoke-virtual {v0, p1, p2, p3}, LX/1Oy;->a(FFZ)Z

    move-result v0

    return v0
.end method

.method public final dispatchNestedPreFling(FF)Z
    .locals 1

    .prologue
    .line 2682526
    iget-object v0, p0, LX/JLv;->y:LX/1Oy;

    invoke-virtual {v0, p1, p2}, LX/1Oy;->a(FF)Z

    move-result v0

    return v0
.end method

.method public final dispatchNestedPreScroll(II[I[I)Z
    .locals 1

    .prologue
    .line 2682399
    iget-object v0, p0, LX/JLv;->y:LX/1Oy;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/1Oy;->a(II[I[I)Z

    move-result v0

    return v0
.end method

.method public final dispatchNestedScroll(IIII[I)Z
    .locals 6

    .prologue
    .line 2682525
    iget-object v0, p0, LX/JLv;->y:LX/1Oy;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, LX/1Oy;->a(IIII[I)Z

    move-result v0

    return v0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 2681974
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->draw(Landroid/graphics/Canvas;)V

    .line 2681975
    iget-object v0, p0, LX/JLv;->d:LX/0vj;

    if-eqz v0, :cond_3

    .line 2681976
    invoke-virtual {p0}, LX/JLv;->getScrollY()I

    move-result v0

    .line 2681977
    iget-object v1, p0, LX/JLv;->d:LX/0vj;

    invoke-virtual {v1}, LX/0vj;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2681978
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    .line 2681979
    invoke-virtual {p0}, LX/JLv;->getWidth()I

    move-result v2

    invoke-virtual {p0}, LX/JLv;->getPaddingLeft()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, LX/JLv;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    .line 2681980
    invoke-virtual {p0}, LX/JLv;->getPaddingLeft()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, LX/JLv;->A:I

    invoke-static {v4, v0}, Ljava/lang/Math;->min(II)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2681981
    iget-object v3, p0, LX/JLv;->d:LX/0vj;

    invoke-virtual {p0}, LX/JLv;->getHeight()I

    move-result v4

    invoke-virtual {v3, v2, v4}, LX/0vj;->a(II)V

    .line 2681982
    iget-object v2, p0, LX/JLv;->d:LX/0vj;

    invoke-virtual {v2, p1}, LX/0vj;->a(Landroid/graphics/Canvas;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2681983
    invoke-static {p0}, LX/0vv;->d(Landroid/view/View;)V

    .line 2681984
    :cond_0
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 2681985
    :cond_1
    iget-object v1, p0, LX/JLv;->e:LX/0vj;

    invoke-virtual {v1}, LX/0vj;->a()Z

    move-result v1

    if-nez v1, :cond_3

    .line 2681986
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    .line 2681987
    invoke-virtual {p0}, LX/JLv;->getWidth()I

    move-result v2

    invoke-virtual {p0}, LX/JLv;->getPaddingLeft()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, LX/JLv;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    .line 2681988
    invoke-virtual {p0}, LX/JLv;->getHeight()I

    move-result v3

    .line 2681989
    neg-int v4, v2

    invoke-virtual {p0}, LX/JLv;->getPaddingLeft()I

    move-result v5

    add-int/2addr v4, v5

    int-to-float v4, v4

    invoke-static {p0}, LX/JLv;->getScrollRange(LX/JLv;)I

    move-result v5

    invoke-static {v5, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/2addr v0, v3

    int-to-float v0, v0

    invoke-virtual {p1, v4, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2681990
    const/high16 v0, 0x43340000    # 180.0f

    int-to-float v4, v2

    const/4 v5, 0x0

    invoke-virtual {p1, v0, v4, v5}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 2681991
    iget-object v0, p0, LX/JLv;->e:LX/0vj;

    invoke-virtual {v0, v2, v3}, LX/0vj;->a(II)V

    .line 2681992
    iget-object v0, p0, LX/JLv;->e:LX/0vj;

    invoke-virtual {v0, p1}, LX/0vj;->a(Landroid/graphics/Canvas;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2681993
    invoke-static {p0}, LX/0vv;->d(Landroid/view/View;)V

    .line 2681994
    :cond_2
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 2681995
    :cond_3
    return-void
.end method

.method public getBottomFadingEdgeStrength()F
    .locals 4

    .prologue
    .line 2682136
    invoke-virtual {p0}, LX/JLv;->getChildCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 2682137
    const/4 v0, 0x0

    .line 2682138
    :goto_0
    return v0

    .line 2682139
    :cond_0
    invoke-virtual {p0}, LX/JLv;->getVerticalFadingEdgeLength()I

    move-result v0

    .line 2682140
    invoke-virtual {p0}, LX/JLv;->getHeight()I

    move-result v1

    invoke-virtual {p0}, LX/JLv;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    .line 2682141
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, LX/JLv;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v2

    invoke-virtual {p0}, LX/JLv;->getScrollY()I

    move-result v3

    sub-int/2addr v2, v3

    sub-int v1, v2, v1

    iget v2, p0, LX/JLv;->B:I

    sub-int/2addr v1, v2

    .line 2682142
    if-ge v1, v0, :cond_1

    .line 2682143
    int-to-float v1, v1

    int-to-float v0, v0

    div-float v0, v1, v0

    goto :goto_0

    .line 2682144
    :cond_1
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public getNestedScrollAxes()I
    .locals 1

    .prologue
    .line 2682133
    iget-object v0, p0, LX/JLv;->x:LX/1uH;

    .line 2682134
    iget p0, v0, LX/1uH;->b:I

    move v0, p0

    .line 2682135
    return v0
.end method

.method public getTopFadingEdgeStrength()F
    .locals 3

    .prologue
    .line 2682125
    invoke-virtual {p0}, LX/JLv;->getChildCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 2682126
    const/4 v0, 0x0

    .line 2682127
    :goto_0
    return v0

    .line 2682128
    :cond_0
    invoke-virtual {p0}, LX/JLv;->getVerticalFadingEdgeLength()I

    move-result v0

    .line 2682129
    invoke-virtual {p0}, LX/JLv;->getScrollY()I

    move-result v1

    iget v2, p0, LX/JLv;->A:I

    sub-int/2addr v1, v2

    .line 2682130
    if-ge v1, v0, :cond_1

    .line 2682131
    int-to-float v1, v1

    int-to-float v0, v0

    div-float v0, v1, v0

    goto :goto_0

    .line 2682132
    :cond_1
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public final hasNestedScrollingParent()Z
    .locals 1

    .prologue
    .line 2682124
    iget-object v0, p0, LX/JLv;->y:LX/1Oy;

    invoke-virtual {v0}, LX/1Oy;->b()Z

    move-result v0

    return v0
.end method

.method public final isNestedScrollingEnabled()Z
    .locals 1

    .prologue
    .line 2682121
    iget-object v0, p0, LX/JLv;->y:LX/1Oy;

    .line 2682122
    iget-boolean p0, v0, LX/1Oy;->c:Z

    move v0, p0

    .line 2682123
    return v0
.end method

.method public final measureChild(Landroid/view/View;II)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2682116
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 2682117
    invoke-virtual {p0}, LX/JLv;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, LX/JLv;->getPaddingRight()I

    move-result v2

    add-int/2addr v1, v2

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {p2, v1, v0}, LX/JLv;->getChildMeasureSpec(III)I

    move-result v0

    .line 2682118
    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 2682119
    invoke-virtual {p1, v0, v1}, Landroid/view/View;->measure(II)V

    .line 2682120
    return-void
.end method

.method public final measureChildWithMargins(Landroid/view/View;IIII)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2c

    const v1, 0x624ef2d3

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2682111
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 2682112
    invoke-virtual {p0}, LX/JLv;->getPaddingLeft()I

    move-result v2

    invoke-virtual {p0}, LX/JLv;->getPaddingRight()I

    move-result v3

    add-int/2addr v2, v3

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v2, v3

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v2, v3

    add-int/2addr v2, p3

    iget v3, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {p2, v2, v3}, LX/JLv;->getChildMeasureSpec(III)I

    move-result v2

    .line 2682113
    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v0, v3

    const/4 v3, 0x0

    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 2682114
    invoke-virtual {p1, v2, v0}, Landroid/view/View;->measure(II)V

    .line 2682115
    const/16 v0, 0x2d

    const v2, -0x21579f2a

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x81ca341

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2682109
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/JLv;->h:Z

    .line 2682110
    const/16 v1, 0x2d

    const v2, -0x7c7738d7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    .line 2682093
    invoke-static {p1}, LX/2xd;->d(Landroid/view/MotionEvent;)I

    move-result v0

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    .line 2682094
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2682095
    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 2682096
    :pswitch_0
    iget-boolean v0, p0, LX/JLv;->j:Z

    if-nez v0, :cond_0

    .line 2682097
    const/16 v0, 0x9

    invoke-static {p1, v0}, LX/2xd;->e(Landroid/view/MotionEvent;I)F

    move-result v0

    .line 2682098
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_0

    .line 2682099
    invoke-direct {p0}, LX/JLv;->getVerticalScrollFactorCompat()F

    move-result v1

    mul-float/2addr v0, v1

    float-to-int v1, v0

    .line 2682100
    invoke-static {p0}, LX/JLv;->getScrollRange(LX/JLv;)I

    move-result v0

    .line 2682101
    invoke-virtual {p0}, LX/JLv;->getScrollY()I

    move-result v2

    .line 2682102
    sub-int v1, v2, v1

    .line 2682103
    iget v3, p0, LX/JLv;->A:I

    if-ge v1, v3, :cond_2

    .line 2682104
    iget v0, p0, LX/JLv;->A:I

    .line 2682105
    :cond_1
    :goto_1
    if-eq v0, v2, :cond_0

    .line 2682106
    invoke-virtual {p0}, LX/JLv;->getScrollX()I

    move-result v1

    invoke-super {p0, v1, v0}, Landroid/widget/FrameLayout;->scrollTo(II)V

    .line 2682107
    const/4 v0, 0x1

    goto :goto_0

    .line 2682108
    :cond_2
    if-gt v1, v0, :cond_1

    move v0, v1

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_0
    .end packed-switch
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9

    .prologue
    const/4 v5, 0x2

    const/4 v4, -0x1

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2682046
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    .line 2682047
    if-ne v2, v5, :cond_0

    iget-boolean v3, p0, LX/JLv;->j:Z

    if-eqz v3, :cond_0

    .line 2682048
    :goto_0
    return v0

    .line 2682049
    :cond_0
    invoke-virtual {p0}, LX/JLv;->getScrollY()I

    move-result v3

    if-nez v3, :cond_1

    invoke-static {p0, v0}, LX/0vv;->b(Landroid/view/View;I)Z

    move-result v3

    if-nez v3, :cond_1

    move v0, v1

    .line 2682050
    goto :goto_0

    .line 2682051
    :cond_1
    and-int/lit16 v2, v2, 0xff

    packed-switch v2, :pswitch_data_0

    .line 2682052
    :cond_2
    :goto_1
    :pswitch_0
    iget-boolean v0, p0, LX/JLv;->j:Z

    goto :goto_0

    .line 2682053
    :pswitch_1
    iget v2, p0, LX/JLv;->q:I

    .line 2682054
    if-eq v2, v4, :cond_2

    .line 2682055
    invoke-static {p1, v2}, LX/2xd;->a(Landroid/view/MotionEvent;I)I

    move-result v3

    .line 2682056
    if-ne v3, v4, :cond_3

    .line 2682057
    const-string v0, "BugFixScrollView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Invalid pointerId="

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " in onInterceptTouchEvent"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 2682058
    :cond_3
    invoke-static {p1, v3}, LX/2xd;->d(Landroid/view/MotionEvent;I)F

    move-result v2

    float-to-int v2, v2

    .line 2682059
    iget v3, p0, LX/JLv;->f:I

    sub-int v3, v2, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    .line 2682060
    iget v4, p0, LX/JLv;->n:I

    if-le v3, v4, :cond_2

    invoke-virtual {p0}, LX/JLv;->getNestedScrollAxes()I

    move-result v3

    and-int/lit8 v3, v3, 0x2

    if-nez v3, :cond_2

    .line 2682061
    iput-boolean v0, p0, LX/JLv;->j:Z

    .line 2682062
    iput v2, p0, LX/JLv;->f:I

    .line 2682063
    invoke-direct {p0}, LX/JLv;->d()V

    .line 2682064
    iget-object v2, p0, LX/JLv;->k:Landroid/view/VelocityTracker;

    invoke-virtual {v2, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 2682065
    iput v1, p0, LX/JLv;->t:I

    .line 2682066
    invoke-virtual {p0}, LX/JLv;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 2682067
    if-eqz v1, :cond_2

    .line 2682068
    invoke-interface {v1, v0}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_1

    .line 2682069
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    .line 2682070
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    const/4 v4, 0x0

    .line 2682071
    invoke-virtual {p0}, LX/JLv;->getChildCount()I

    move-result v6

    if-lez v6, :cond_4

    .line 2682072
    invoke-virtual {p0}, LX/JLv;->getScrollY()I

    move-result v6

    .line 2682073
    invoke-virtual {p0, v4}, LX/JLv;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 2682074
    invoke-virtual {v7}, Landroid/view/View;->getTop()I

    move-result v8

    sub-int/2addr v8, v6

    if-lt v2, v8, :cond_4

    invoke-virtual {v7}, Landroid/view/View;->getBottom()I

    move-result v8

    sub-int v6, v8, v6

    if-ge v2, v6, :cond_4

    invoke-virtual {v7}, Landroid/view/View;->getLeft()I

    move-result v6

    if-lt v3, v6, :cond_4

    invoke-virtual {v7}, Landroid/view/View;->getRight()I

    move-result v6

    if-ge v3, v6, :cond_4

    const/4 v4, 0x1

    .line 2682075
    :cond_4
    move v3, v4

    .line 2682076
    if-nez v3, :cond_5

    .line 2682077
    iput-boolean v1, p0, LX/JLv;->j:Z

    .line 2682078
    invoke-direct {p0}, LX/JLv;->e()V

    goto/16 :goto_1

    .line 2682079
    :cond_5
    iput v2, p0, LX/JLv;->f:I

    .line 2682080
    invoke-static {p1, v1}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v2

    iput v2, p0, LX/JLv;->q:I

    .line 2682081
    iget-object v2, p0, LX/JLv;->k:Landroid/view/VelocityTracker;

    if-nez v2, :cond_7

    .line 2682082
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v2

    iput-object v2, p0, LX/JLv;->k:Landroid/view/VelocityTracker;

    .line 2682083
    :goto_2
    iget-object v2, p0, LX/JLv;->k:Landroid/view/VelocityTracker;

    invoke-virtual {v2, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 2682084
    iget-object v2, p0, LX/JLv;->c:LX/1Og;

    invoke-virtual {v2}, LX/1Og;->a()Z

    move-result v2

    if-nez v2, :cond_6

    :goto_3
    iput-boolean v0, p0, LX/JLv;->j:Z

    .line 2682085
    invoke-virtual {p0, v5}, LX/JLv;->startNestedScroll(I)Z

    goto/16 :goto_1

    :cond_6
    move v0, v1

    .line 2682086
    goto :goto_3

    .line 2682087
    :pswitch_3
    iput-boolean v1, p0, LX/JLv;->j:Z

    .line 2682088
    iput v4, p0, LX/JLv;->q:I

    .line 2682089
    invoke-direct {p0}, LX/JLv;->e()V

    .line 2682090
    invoke-virtual {p0}, LX/JLv;->stopNestedScroll()V

    goto/16 :goto_1

    .line 2682091
    :pswitch_4
    invoke-direct {p0, p1}, LX/JLv;->a(Landroid/view/MotionEvent;)V

    goto/16 :goto_1

    .line 2682092
    :cond_7
    iget-object v2, p0, LX/JLv;->k:Landroid/view/VelocityTracker;

    invoke-virtual {v2}, Landroid/view/VelocityTracker;->clear()V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public onLayout(ZIIII)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 2682027
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 2682028
    iput-boolean v1, p0, LX/JLv;->g:Z

    .line 2682029
    iget-object v0, p0, LX/JLv;->i:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/JLv;->i:Landroid/view/View;

    invoke-static {v0, p0}, LX/JLv;->a(Landroid/view/View;Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2682030
    iget-object v0, p0, LX/JLv;->i:Landroid/view/View;

    invoke-direct {p0, v0}, LX/JLv;->b(Landroid/view/View;)V

    .line 2682031
    :cond_0
    iput-object v3, p0, LX/JLv;->i:Landroid/view/View;

    .line 2682032
    iget-boolean v0, p0, LX/JLv;->h:Z

    if-nez v0, :cond_2

    .line 2682033
    iget-object v0, p0, LX/JLv;->u:Lcom/facebook/fbreact/views/fbscroll/BugFixScrollView$SavedState;

    if-eqz v0, :cond_1

    .line 2682034
    invoke-virtual {p0}, LX/JLv;->getScrollX()I

    move-result v0

    iget-object v2, p0, LX/JLv;->u:Lcom/facebook/fbreact/views/fbscroll/BugFixScrollView$SavedState;

    iget v2, v2, Lcom/facebook/fbreact/views/fbscroll/BugFixScrollView$SavedState;->a:I

    invoke-virtual {p0, v0, v2}, LX/JLv;->scrollTo(II)V

    .line 2682035
    iput-object v3, p0, LX/JLv;->u:Lcom/facebook/fbreact/views/fbscroll/BugFixScrollView$SavedState;

    .line 2682036
    :cond_1
    invoke-virtual {p0}, LX/JLv;->getChildCount()I

    move-result v0

    if-lez v0, :cond_3

    invoke-virtual {p0, v1}, LX/JLv;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    .line 2682037
    :goto_0
    sub-int v2, p5, p3

    invoke-virtual {p0}, LX/JLv;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, LX/JLv;->getPaddingTop()I

    move-result v3

    sub-int/2addr v2, v3

    sub-int/2addr v0, v2

    iget v2, p0, LX/JLv;->B:I

    sub-int/2addr v0, v2

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 2682038
    invoke-virtual {p0}, LX/JLv;->getScrollY()I

    move-result v1

    if-le v1, v0, :cond_4

    .line 2682039
    invoke-virtual {p0}, LX/JLv;->getScrollX()I

    move-result v1

    invoke-virtual {p0, v1, v0}, LX/JLv;->scrollTo(II)V

    .line 2682040
    :cond_2
    :goto_1
    invoke-virtual {p0}, LX/JLv;->getScrollX()I

    move-result v0

    invoke-virtual {p0}, LX/JLv;->getScrollY()I

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/JLv;->scrollTo(II)V

    .line 2682041
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/JLv;->h:Z

    .line 2682042
    return-void

    :cond_3
    move v0, v1

    .line 2682043
    goto :goto_0

    .line 2682044
    :cond_4
    invoke-virtual {p0}, LX/JLv;->getScrollY()I

    move-result v0

    iget v1, p0, LX/JLv;->A:I

    if-ge v0, v1, :cond_2

    .line 2682045
    invoke-virtual {p0}, LX/JLv;->getScrollX()I

    move-result v0

    iget v1, p0, LX/JLv;->A:I

    invoke-virtual {p0, v0, v1}, LX/JLv;->scrollTo(II)V

    goto :goto_1
.end method

.method public onMeasure(II)V
    .locals 5

    .prologue
    .line 2682012
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 2682013
    iget-boolean v0, p0, LX/JLv;->l:Z

    if-nez v0, :cond_1

    .line 2682014
    :cond_0
    :goto_0
    return-void

    .line 2682015
    :cond_1
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 2682016
    if-eqz v0, :cond_0

    .line 2682017
    invoke-virtual {p0}, LX/JLv;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 2682018
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/JLv;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2682019
    invoke-virtual {p0}, LX/JLv;->getMeasuredHeight()I

    move-result v2

    .line 2682020
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    if-ge v0, v2, :cond_0

    .line 2682021
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 2682022
    invoke-virtual {p0}, LX/JLv;->getPaddingLeft()I

    move-result v3

    invoke-virtual {p0}, LX/JLv;->getPaddingRight()I

    move-result v4

    add-int/2addr v3, v4

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {p1, v3, v0}, LX/JLv;->getChildMeasureSpec(III)I

    move-result v0

    .line 2682023
    invoke-virtual {p0}, LX/JLv;->getPaddingTop()I

    move-result v3

    sub-int/2addr v2, v3

    .line 2682024
    invoke-virtual {p0}, LX/JLv;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    .line 2682025
    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 2682026
    invoke-virtual {v1, v0, v2}, Landroid/view/View;->measure(II)V

    goto :goto_0
.end method

.method public final onNestedFling(Landroid/view/View;FFZ)Z
    .locals 1

    .prologue
    .line 2682008
    if-nez p4, :cond_0

    .line 2682009
    float-to-int v0, p3

    invoke-direct {p0, v0}, LX/JLv;->f(I)V

    .line 2682010
    const/4 v0, 0x1

    .line 2682011
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onNestedPreFling(Landroid/view/View;FF)Z
    .locals 1

    .prologue
    .line 2682007
    const/4 v0, 0x0

    return v0
.end method

.method public final onNestedPreScroll(Landroid/view/View;II[I)V
    .locals 0

    .prologue
    .line 2682006
    return-void
.end method

.method public final onNestedScroll(Landroid/view/View;IIII)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2682000
    invoke-virtual {p0}, LX/JLv;->getScrollY()I

    move-result v0

    .line 2682001
    invoke-virtual {p0, v1, p5}, LX/JLv;->scrollBy(II)V

    .line 2682002
    invoke-virtual {p0}, LX/JLv;->getScrollY()I

    move-result v2

    sub-int/2addr v2, v0

    .line 2682003
    sub-int v4, p5, v2

    .line 2682004
    const/4 v5, 0x0

    move-object v0, p0

    move v3, v1

    invoke-virtual/range {v0 .. v5}, LX/JLv;->dispatchNestedScroll(IIII[I)Z

    .line 2682005
    return-void
.end method

.method public final onNestedScrollAccepted(Landroid/view/View;Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 2681996
    iget-object v0, p0, LX/JLv;->x:LX/1uH;

    .line 2681997
    iput p3, v0, LX/1uH;->b:I

    .line 2681998
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, LX/JLv;->startNestedScroll(I)Z

    .line 2681999
    return-void
.end method

.method public final onOverScrolled(IIZZ)V
    .locals 2

    .prologue
    .line 2681970
    iget-object v0, p0, LX/JLv;->c:LX/1Og;

    invoke-virtual {v0}, LX/1Og;->a()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p0}, LX/JLv;->getScrollRange(LX/JLv;)I

    move-result v0

    if-ge p2, v0, :cond_0

    iget v0, p0, LX/JLv;->A:I

    if-gt p2, v0, :cond_1

    :cond_0
    iget-object v0, p0, LX/JLv;->c:LX/1Og;

    invoke-virtual {v0}, LX/1Og;->c()I

    move-result v0

    iget-object v1, p0, LX/JLv;->c:LX/1Og;

    invoke-virtual {v1}, LX/1Og;->e()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 2681971
    iget-object v0, p0, LX/JLv;->c:LX/1Og;

    invoke-virtual {v0}, LX/1Og;->h()V

    .line 2681972
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->scrollTo(II)V

    .line 2681973
    return-void
.end method

.method public final onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2682177
    const/4 v1, 0x2

    if-ne p1, v1, :cond_2

    .line 2682178
    const/16 p1, 0x82

    .line 2682179
    :cond_0
    :goto_0
    if-nez p2, :cond_3

    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p0, v2, p1}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    .line 2682180
    :goto_1
    if-nez v1, :cond_4

    .line 2682181
    :cond_1
    :goto_2
    return v0

    .line 2682182
    :cond_2
    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    .line 2682183
    const/16 p1, 0x21

    goto :goto_0

    .line 2682184
    :cond_3
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v1

    invoke-virtual {v1, p0, p2, p1}, Landroid/view/FocusFinder;->findNextFocusFromRect(Landroid/view/ViewGroup;Landroid/graphics/Rect;I)Landroid/view/View;

    move-result-object v1

    goto :goto_1

    .line 2682185
    :cond_4
    invoke-direct {p0, v1}, LX/JLv;->a(Landroid/view/View;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2682186
    invoke-virtual {v1, p1, p2}, Landroid/view/View;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result v0

    goto :goto_2
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 2682305
    check-cast p1, Lcom/facebook/fbreact/views/fbscroll/BugFixScrollView$SavedState;

    .line 2682306
    invoke-virtual {p1}, Lcom/facebook/fbreact/views/fbscroll/BugFixScrollView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 2682307
    iput-object p1, p0, LX/JLv;->u:Lcom/facebook/fbreact/views/fbscroll/BugFixScrollView$SavedState;

    .line 2682308
    invoke-virtual {p0}, LX/JLv;->requestLayout()V

    .line 2682309
    return-void
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 2682301
    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 2682302
    new-instance v1, Lcom/facebook/fbreact/views/fbscroll/BugFixScrollView$SavedState;

    invoke-direct {v1, v0}, Lcom/facebook/fbreact/views/fbscroll/BugFixScrollView$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 2682303
    invoke-virtual {p0}, LX/JLv;->getScrollY()I

    move-result v0

    iput v0, v1, Lcom/facebook/fbreact/views/fbscroll/BugFixScrollView$SavedState;->a:I

    .line 2682304
    return-object v1
.end method

.method public onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x3d8b9ac    # -3.47416E36f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2682290
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    .line 2682291
    invoke-virtual {p0}, LX/JLv;->findFocus()Landroid/view/View;

    move-result-object v1

    .line 2682292
    if-eqz v1, :cond_0

    if-ne p0, v1, :cond_1

    .line 2682293
    :cond_0
    const/16 v1, 0x2d

    const v2, 0x7ebc2149

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2682294
    :goto_0
    return-void

    .line 2682295
    :cond_1
    const/4 v2, 0x0

    invoke-direct {p0, v1, v2, p4}, LX/JLv;->a(Landroid/view/View;II)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2682296
    iget-object v2, p0, LX/JLv;->b:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 2682297
    iget-object v2, p0, LX/JLv;->b:Landroid/graphics/Rect;

    invoke-virtual {p0, v1, v2}, LX/JLv;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 2682298
    iget-object v1, p0, LX/JLv;->b:Landroid/graphics/Rect;

    invoke-static {p0, v1}, LX/JLv;->a(LX/JLv;Landroid/graphics/Rect;)I

    move-result v1

    .line 2682299
    invoke-direct {p0, v1}, LX/JLv;->e(I)V

    .line 2682300
    :cond_2
    const v1, 0x679f27eb

    invoke-static {v1, v0}, LX/02F;->g(II)V

    goto :goto_0
.end method

.method public final onStartNestedScroll(Landroid/view/View;Landroid/view/View;I)Z
    .locals 1

    .prologue
    .line 2682289
    and-int/lit8 v0, p3, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onStopNestedScroll(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2682287
    invoke-virtual {p0}, LX/JLv;->stopNestedScroll()V

    .line 2682288
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 20

    .prologue
    const/4 v2, 0x2

    const/4 v3, 0x1

    const v4, -0x20f9cdf5

    invoke-static {v2, v3, v4}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v16

    .line 2682203
    invoke-direct/range {p0 .. p0}, LX/JLv;->d()V

    .line 2682204
    invoke-static/range {p1 .. p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v17

    .line 2682205
    invoke-static/range {p1 .. p1}, LX/2xd;->a(Landroid/view/MotionEvent;)I

    move-result v2

    .line 2682206
    if-nez v2, :cond_0

    .line 2682207
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, LX/JLv;->t:I

    .line 2682208
    :cond_0
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, LX/JLv;->t:I

    int-to-float v4, v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v3, v4}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 2682209
    packed-switch v2, :pswitch_data_0

    .line 2682210
    :cond_1
    :goto_0
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/JLv;->k:Landroid/view/VelocityTracker;

    if-eqz v2, :cond_2

    .line 2682211
    move-object/from16 v0, p0

    iget-object v2, v0, LX/JLv;->k:Landroid/view/VelocityTracker;

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 2682212
    :cond_2
    invoke-virtual/range {v17 .. v17}, Landroid/view/MotionEvent;->recycle()V

    .line 2682213
    const/4 v2, 0x1

    const v3, 0x741b6460

    move/from16 v0, v16

    invoke-static {v3, v0}, LX/02F;->a(II)V

    :goto_1
    return v2

    .line 2682214
    :pswitch_1
    invoke-virtual/range {p0 .. p0}, LX/JLv;->getChildCount()I

    move-result v2

    if-nez v2, :cond_3

    .line 2682215
    const/4 v2, 0x0

    const v3, 0x4841837b

    move/from16 v0, v16

    invoke-static {v3, v0}, LX/02F;->a(II)V

    goto :goto_1

    .line 2682216
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, LX/JLv;->c:LX/1Og;

    invoke-virtual {v2}, LX/1Og;->a()Z

    move-result v2

    if-nez v2, :cond_6

    const/4 v2, 0x1

    :goto_2
    move-object/from16 v0, p0

    iput-boolean v2, v0, LX/JLv;->j:Z

    if-eqz v2, :cond_4

    .line 2682217
    invoke-virtual/range {p0 .. p0}, LX/JLv;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    .line 2682218
    if-eqz v2, :cond_4

    .line 2682219
    const/4 v3, 0x1

    invoke-interface {v2, v3}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 2682220
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, LX/JLv;->c:LX/1Og;

    invoke-virtual {v2}, LX/1Og;->a()Z

    move-result v2

    if-nez v2, :cond_5

    .line 2682221
    move-object/from16 v0, p0

    iget-object v2, v0, LX/JLv;->c:LX/1Og;

    invoke-virtual {v2}, LX/1Og;->h()V

    .line 2682222
    :cond_5
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    move-object/from16 v0, p0

    iput v2, v0, LX/JLv;->f:I

    .line 2682223
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, LX/JLv;->q:I

    .line 2682224
    const/4 v2, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/JLv;->startNestedScroll(I)Z

    goto :goto_0

    .line 2682225
    :cond_6
    const/4 v2, 0x0

    goto :goto_2

    .line 2682226
    :pswitch_2
    move-object/from16 v0, p0

    iget v2, v0, LX/JLv;->q:I

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/2xd;->a(Landroid/view/MotionEvent;I)I

    move-result v18

    .line 2682227
    const/4 v2, -0x1

    move/from16 v0, v18

    if-ne v0, v2, :cond_7

    .line 2682228
    const-string v2, "BugFixScrollView"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid pointerId="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v4, v0, LX/JLv;->q:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " in onTouchEvent"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 2682229
    :cond_7
    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/2xd;->d(Landroid/view/MotionEvent;I)F

    move-result v2

    float-to-int v3, v2

    .line 2682230
    move-object/from16 v0, p0

    iget v2, v0, LX/JLv;->f:I

    sub-int/2addr v2, v3

    .line 2682231
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, LX/JLv;->s:[I

    move-object/from16 v0, p0

    iget-object v6, v0, LX/JLv;->r:[I

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v2, v5, v6}, LX/JLv;->dispatchNestedPreScroll(II[I[I)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 2682232
    move-object/from16 v0, p0

    iget-object v4, v0, LX/JLv;->s:[I

    const/4 v5, 0x1

    aget v4, v4, v5

    sub-int/2addr v2, v4

    .line 2682233
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, LX/JLv;->r:[I

    const/4 v6, 0x1

    aget v5, v5, v6

    int-to-float v5, v5

    move-object/from16 v0, v17

    invoke-virtual {v0, v4, v5}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 2682234
    move-object/from16 v0, p0

    iget v4, v0, LX/JLv;->t:I

    move-object/from16 v0, p0

    iget-object v5, v0, LX/JLv;->r:[I

    const/4 v6, 0x1

    aget v5, v5, v6

    add-int/2addr v4, v5

    move-object/from16 v0, p0

    iput v4, v0, LX/JLv;->t:I

    .line 2682235
    :cond_8
    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/JLv;->j:Z

    if-nez v4, :cond_13

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v4

    move-object/from16 v0, p0

    iget v5, v0, LX/JLv;->n:I

    if-le v4, v5, :cond_13

    .line 2682236
    invoke-virtual/range {p0 .. p0}, LX/JLv;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    .line 2682237
    if-eqz v4, :cond_9

    .line 2682238
    const/4 v5, 0x1

    invoke-interface {v4, v5}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 2682239
    :cond_9
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, LX/JLv;->j:Z

    .line 2682240
    if-lez v2, :cond_c

    .line 2682241
    move-object/from16 v0, p0

    iget v4, v0, LX/JLv;->n:I

    sub-int/2addr v2, v4

    move v4, v2

    .line 2682242
    :goto_3
    move-object/from16 v0, p0

    iget-boolean v2, v0, LX/JLv;->j:Z

    if-eqz v2, :cond_1

    .line 2682243
    move-object/from16 v0, p0

    iget-object v2, v0, LX/JLv;->r:[I

    const/4 v5, 0x1

    aget v2, v2, v5

    sub-int v2, v3, v2

    move-object/from16 v0, p0

    iput v2, v0, LX/JLv;->f:I

    .line 2682244
    invoke-virtual/range {p0 .. p0}, LX/JLv;->getScrollY()I

    move-result v19

    .line 2682245
    invoke-static/range {p0 .. p0}, LX/JLv;->getScrollRange(LX/JLv;)I

    move-result v8

    .line 2682246
    invoke-static/range {p0 .. p0}, LX/0vv;->a(Landroid/view/View;)I

    move-result v2

    .line 2682247
    if-eqz v2, :cond_a

    const/4 v3, 0x1

    if-ne v2, v3, :cond_d

    if-lez v8, :cond_d

    :cond_a
    const/4 v2, 0x1

    move v15, v2

    .line 2682248
    :goto_4
    const/4 v3, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {p0 .. p0}, LX/JLv;->getScrollY()I

    move-result v6

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v10}, LX/JLv;->a(IIIIIIII)Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-virtual/range {p0 .. p0}, LX/JLv;->hasNestedScrollingParent()Z

    move-result v2

    if-nez v2, :cond_b

    .line 2682249
    move-object/from16 v0, p0

    iget-object v2, v0, LX/JLv;->k:Landroid/view/VelocityTracker;

    invoke-virtual {v2}, Landroid/view/VelocityTracker;->clear()V

    .line 2682250
    :cond_b
    invoke-virtual/range {p0 .. p0}, LX/JLv;->getScrollY()I

    move-result v2

    sub-int v11, v2, v19

    .line 2682251
    sub-int v13, v4, v11

    .line 2682252
    const/4 v10, 0x0

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, LX/JLv;->r:[I

    move-object/from16 v9, p0

    invoke-virtual/range {v9 .. v14}, LX/JLv;->dispatchNestedScroll(IIII[I)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 2682253
    move-object/from16 v0, p0

    iget v2, v0, LX/JLv;->f:I

    move-object/from16 v0, p0

    iget-object v3, v0, LX/JLv;->r:[I

    const/4 v4, 0x1

    aget v3, v3, v4

    sub-int/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, LX/JLv;->f:I

    .line 2682254
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, LX/JLv;->r:[I

    const/4 v4, 0x1

    aget v3, v3, v4

    int-to-float v3, v3

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 2682255
    move-object/from16 v0, p0

    iget v2, v0, LX/JLv;->t:I

    move-object/from16 v0, p0

    iget-object v3, v0, LX/JLv;->r:[I

    const/4 v4, 0x1

    aget v3, v3, v4

    add-int/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, LX/JLv;->t:I

    goto/16 :goto_0

    .line 2682256
    :cond_c
    move-object/from16 v0, p0

    iget v4, v0, LX/JLv;->n:I

    add-int/2addr v2, v4

    move v4, v2

    goto/16 :goto_3

    .line 2682257
    :cond_d
    const/4 v2, 0x0

    move v15, v2

    goto :goto_4

    .line 2682258
    :cond_e
    if-eqz v15, :cond_1

    .line 2682259
    invoke-direct/range {p0 .. p0}, LX/JLv;->g()V

    .line 2682260
    add-int v2, v19, v4

    .line 2682261
    move-object/from16 v0, p0

    iget v3, v0, LX/JLv;->A:I

    if-ge v2, v3, :cond_11

    .line 2682262
    move-object/from16 v0, p0

    iget-object v2, v0, LX/JLv;->d:LX/0vj;

    int-to-float v3, v4

    invoke-virtual/range {p0 .. p0}, LX/JLv;->getHeight()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/2xd;->c(Landroid/view/MotionEvent;I)F

    move-result v4

    invoke-virtual/range {p0 .. p0}, LX/JLv;->getWidth()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v4, v5

    invoke-virtual {v2, v3, v4}, LX/0vj;->a(FF)Z

    .line 2682263
    move-object/from16 v0, p0

    iget-object v2, v0, LX/JLv;->e:LX/0vj;

    invoke-virtual {v2}, LX/0vj;->a()Z

    move-result v2

    if-nez v2, :cond_f

    .line 2682264
    move-object/from16 v0, p0

    iget-object v2, v0, LX/JLv;->e:LX/0vj;

    invoke-virtual {v2}, LX/0vj;->c()Z

    .line 2682265
    :cond_f
    :goto_5
    move-object/from16 v0, p0

    iget-object v2, v0, LX/JLv;->d:LX/0vj;

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, LX/JLv;->d:LX/0vj;

    invoke-virtual {v2}, LX/0vj;->a()Z

    move-result v2

    if-eqz v2, :cond_10

    move-object/from16 v0, p0

    iget-object v2, v0, LX/JLv;->e:LX/0vj;

    invoke-virtual {v2}, LX/0vj;->a()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2682266
    :cond_10
    invoke-static/range {p0 .. p0}, LX/0vv;->d(Landroid/view/View;)V

    goto/16 :goto_0

    .line 2682267
    :cond_11
    if-le v2, v8, :cond_f

    .line 2682268
    move-object/from16 v0, p0

    iget-object v2, v0, LX/JLv;->e:LX/0vj;

    int-to-float v3, v4

    invoke-virtual/range {p0 .. p0}, LX/JLv;->getHeight()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    const/high16 v4, 0x3f800000    # 1.0f

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/2xd;->c(Landroid/view/MotionEvent;I)F

    move-result v5

    invoke-virtual/range {p0 .. p0}, LX/JLv;->getWidth()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v5, v6

    sub-float/2addr v4, v5

    invoke-virtual {v2, v3, v4}, LX/0vj;->a(FF)Z

    .line 2682269
    move-object/from16 v0, p0

    iget-object v2, v0, LX/JLv;->d:LX/0vj;

    invoke-virtual {v2}, LX/0vj;->a()Z

    move-result v2

    if-nez v2, :cond_f

    .line 2682270
    move-object/from16 v0, p0

    iget-object v2, v0, LX/JLv;->d:LX/0vj;

    invoke-virtual {v2}, LX/0vj;->c()Z

    goto :goto_5

    .line 2682271
    :pswitch_3
    move-object/from16 v0, p0

    iget-boolean v2, v0, LX/JLv;->j:Z

    if-eqz v2, :cond_1

    .line 2682272
    move-object/from16 v0, p0

    iget-object v2, v0, LX/JLv;->k:Landroid/view/VelocityTracker;

    .line 2682273
    const/16 v3, 0x3e8

    move-object/from16 v0, p0

    iget v4, v0, LX/JLv;->p:I

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 2682274
    move-object/from16 v0, p0

    iget v3, v0, LX/JLv;->q:I

    invoke-static {v2, v3}, LX/2uG;->b(Landroid/view/VelocityTracker;I)F

    move-result v2

    float-to-int v2, v2

    .line 2682275
    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v3

    move-object/from16 v0, p0

    iget v4, v0, LX/JLv;->o:I

    if-le v3, v4, :cond_12

    .line 2682276
    neg-int v2, v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, LX/JLv;->f(I)V

    .line 2682277
    :cond_12
    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, LX/JLv;->q:I

    .line 2682278
    invoke-direct/range {p0 .. p0}, LX/JLv;->f()V

    goto/16 :goto_0

    .line 2682279
    :pswitch_4
    move-object/from16 v0, p0

    iget-boolean v2, v0, LX/JLv;->j:Z

    if-eqz v2, :cond_1

    invoke-virtual/range {p0 .. p0}, LX/JLv;->getChildCount()I

    move-result v2

    if-lez v2, :cond_1

    .line 2682280
    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, LX/JLv;->q:I

    .line 2682281
    invoke-direct/range {p0 .. p0}, LX/JLv;->f()V

    goto/16 :goto_0

    .line 2682282
    :pswitch_5
    invoke-static/range {p1 .. p1}, LX/2xd;->b(Landroid/view/MotionEvent;)I

    move-result v2

    .line 2682283
    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/2xd;->d(Landroid/view/MotionEvent;I)F

    move-result v3

    float-to-int v3, v3

    move-object/from16 v0, p0

    iput v3, v0, LX/JLv;->f:I

    .line 2682284
    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, LX/JLv;->q:I

    goto/16 :goto_0

    .line 2682285
    :pswitch_6
    invoke-direct/range {p0 .. p1}, LX/JLv;->a(Landroid/view/MotionEvent;)V

    .line 2682286
    move-object/from16 v0, p0

    iget v2, v0, LX/JLv;->q:I

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/2xd;->a(Landroid/view/MotionEvent;I)I

    move-result v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/2xd;->d(Landroid/view/MotionEvent;I)F

    move-result v2

    float-to-int v2, v2

    move-object/from16 v0, p0

    iput v2, v0, LX/JLv;->f:I

    goto/16 :goto_0

    :cond_13
    move v4, v2

    goto/16 :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public final requestChildFocus(Landroid/view/View;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2682198
    iget-boolean v0, p0, LX/JLv;->g:Z

    if-nez v0, :cond_0

    .line 2682199
    invoke-direct {p0, p2}, LX/JLv;->b(Landroid/view/View;)V

    .line 2682200
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->requestChildFocus(Landroid/view/View;Landroid/view/View;)V

    .line 2682201
    return-void

    .line 2682202
    :cond_0
    iput-object p2, p0, LX/JLv;->i:Landroid/view/View;

    goto :goto_0
.end method

.method public final requestChildRectangleOnScreen(Landroid/view/View;Landroid/graphics/Rect;Z)Z
    .locals 3

    .prologue
    .line 2682187
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getScrollX()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getScrollY()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Rect;->offset(II)V

    .line 2682188
    const/4 v1, 0x0

    .line 2682189
    invoke-static {p0, p2}, LX/JLv;->a(LX/JLv;Landroid/graphics/Rect;)I

    move-result v2

    .line 2682190
    if-eqz v2, :cond_1

    const/4 v0, 0x1

    .line 2682191
    :goto_0
    if-eqz v0, :cond_0

    .line 2682192
    if-eqz p3, :cond_2

    .line 2682193
    invoke-virtual {p0, v1, v2}, LX/JLv;->scrollBy(II)V

    .line 2682194
    :cond_0
    :goto_1
    move v0, v0

    .line 2682195
    return v0

    :cond_1
    move v0, v1

    .line 2682196
    goto :goto_0

    .line 2682197
    :cond_2
    invoke-static {p0, v1, v2}, LX/JLv;->c(LX/JLv;II)V

    goto :goto_1
.end method

.method public final requestDisallowInterceptTouchEvent(Z)V
    .locals 0

    .prologue
    .line 2682145
    if-eqz p1, :cond_0

    .line 2682146
    invoke-direct {p0}, LX/JLv;->e()V

    .line 2682147
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->requestDisallowInterceptTouchEvent(Z)V

    .line 2682148
    return-void
.end method

.method public final requestLayout()V
    .locals 1

    .prologue
    .line 2682174
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/JLv;->g:Z

    .line 2682175
    invoke-super {p0}, Landroid/widget/FrameLayout;->requestLayout()V

    .line 2682176
    return-void
.end method

.method public final scrollTo(II)V
    .locals 4

    .prologue
    .line 2682166
    invoke-virtual {p0}, LX/JLv;->getChildCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 2682167
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/JLv;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2682168
    invoke-virtual {p0}, LX/JLv;->getWidth()I

    move-result v1

    invoke-virtual {p0}, LX/JLv;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, LX/JLv;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v2

    invoke-static {p1, v1, v2}, LX/JLv;->b(III)I

    move-result v1

    .line 2682169
    invoke-virtual {p0}, LX/JLv;->getHeight()I

    move-result v2

    invoke-virtual {p0}, LX/JLv;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, LX/JLv;->getPaddingTop()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    iget v3, p0, LX/JLv;->B:I

    sub-int/2addr v0, v3

    invoke-static {p2, v2, v0}, LX/JLv;->b(III)I

    move-result v0

    .line 2682170
    iget v2, p0, LX/JLv;->A:I

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 2682171
    invoke-virtual {p0}, LX/JLv;->getScrollX()I

    move-result v2

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, LX/JLv;->getScrollY()I

    move-result v2

    if-eq v0, v2, :cond_1

    .line 2682172
    :cond_0
    invoke-super {p0, v1, v0}, Landroid/widget/FrameLayout;->scrollTo(II)V

    .line 2682173
    :cond_1
    return-void
.end method

.method public setMaxScrollDeltaY(I)V
    .locals 2

    .prologue
    .line 2682161
    iput p1, p0, LX/JLv;->B:I

    .line 2682162
    invoke-static {p0}, LX/JLv;->getScrollRange(LX/JLv;)I

    move-result v0

    .line 2682163
    invoke-virtual {p0}, LX/JLv;->getScrollY()I

    move-result v1

    if-le v1, v0, :cond_0

    .line 2682164
    invoke-virtual {p0}, LX/JLv;->getScrollX()I

    move-result v1

    invoke-virtual {p0, v1, v0}, LX/JLv;->scrollTo(II)V

    .line 2682165
    :cond_0
    return-void
.end method

.method public setMinScrollDeltaY(I)V
    .locals 2

    .prologue
    .line 2682157
    iput p1, p0, LX/JLv;->A:I

    .line 2682158
    invoke-virtual {p0}, LX/JLv;->getScrollY()I

    move-result v0

    iget v1, p0, LX/JLv;->A:I

    if-ge v0, v1, :cond_0

    .line 2682159
    invoke-virtual {p0}, LX/JLv;->getScrollX()I

    move-result v0

    iget v1, p0, LX/JLv;->A:I

    invoke-virtual {p0, v0, v1}, LX/JLv;->scrollTo(II)V

    .line 2682160
    :cond_0
    return-void
.end method

.method public setNestedScrollingEnabled(Z)V
    .locals 1

    .prologue
    .line 2682155
    iget-object v0, p0, LX/JLv;->y:LX/1Oy;

    invoke-virtual {v0, p1}, LX/1Oy;->a(Z)V

    .line 2682156
    return-void
.end method

.method public setSmoothScrollingEnabled(Z)V
    .locals 0

    .prologue
    .line 2682153
    iput-boolean p1, p0, LX/JLv;->m:Z

    .line 2682154
    return-void
.end method

.method public final shouldDelayChildPressedState()Z
    .locals 1

    .prologue
    .line 2682152
    const/4 v0, 0x1

    return v0
.end method

.method public final startNestedScroll(I)Z
    .locals 1

    .prologue
    .line 2682151
    iget-object v0, p0, LX/JLv;->y:LX/1Oy;

    invoke-virtual {v0, p1}, LX/1Oy;->a(I)Z

    move-result v0

    return v0
.end method

.method public final stopNestedScroll()V
    .locals 1

    .prologue
    .line 2682149
    iget-object v0, p0, LX/JLv;->y:LX/1Oy;

    invoke-virtual {v0}, LX/1Oy;->c()V

    .line 2682150
    return-void
.end method
