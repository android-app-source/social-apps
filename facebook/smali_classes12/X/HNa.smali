.class public final LX/HNa;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionMutationsModels$PageCallToActionCoreUpdateMutationFieldsModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/HNc;


# direct methods
.method public constructor <init>(LX/HNc;)V
    .locals 0

    .prologue
    .line 2458632
    iput-object p1, p0, LX/HNa;->a:LX/HNc;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2458638
    iget-object v0, p0, LX/HNa;->a:LX/HNc;

    iget-object v0, v0, LX/HNc;->e:LX/HO0;

    if-eqz v0, :cond_0

    .line 2458639
    iget-object v0, p0, LX/HNa;->a:LX/HNc;

    iget-object v0, v0, LX/HNc;->e:LX/HO0;

    .line 2458640
    iget-object v1, v0, LX/HO0;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->g:LX/0if;

    sget-object v2, LX/0ig;->ak:LX/0ih;

    const-string v3, "error_message_shown"

    const-string p0, "server_error"

    invoke-virtual {v1, v2, v3, p0}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 2458641
    iget-object v1, v0, LX/HO0;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    invoke-static {v1, p1}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->a$redex0(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;Ljava/lang/Throwable;)V

    .line 2458642
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2458633
    iget-object v0, p0, LX/HNa;->a:LX/HNc;

    iget-object v0, v0, LX/HNc;->e:LX/HO0;

    if-eqz v0, :cond_0

    .line 2458634
    iget-object v0, p0, LX/HNa;->a:LX/HNc;

    iget-object v0, v0, LX/HNc;->e:LX/HO0;

    .line 2458635
    iget-object v1, v0, LX/HO0;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->g:LX/0if;

    sget-object v2, LX/0ig;->ak:LX/0ih;

    const-string v3, "save_success"

    iget-object v4, v0, LX/HO0;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v4, v4, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->D:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    invoke-virtual {v4}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->b()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 2458636
    iget-object v1, v0, LX/HO0;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object v2, v0, LX/HO0;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081689

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 p0, 0x0

    iget-object p1, v0, LX/HO0;->a:Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;

    iget-object p1, p1, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->D:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    invoke-virtual {p1}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->e()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v4, p0

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;->b$redex0(Lcom/facebook/pages/common/surface/calltoaction/fragment/PageConfigureCallToActionFragment;Ljava/lang/String;)V

    .line 2458637
    :cond_0
    return-void
.end method
