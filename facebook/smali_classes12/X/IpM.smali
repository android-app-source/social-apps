.class public final LX/IpM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/IpP;


# direct methods
.method public constructor <init>(LX/IpP;)V
    .locals 0

    .prologue
    .line 2612841
    iput-object p1, p0, LX/IpM;->a:LX/IpP;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2612836
    iget-object v0, p0, LX/IpM;->a:LX/IpP;

    iget-object v0, v0, LX/IpP;->c:LX/03V;

    const-string v1, "OrionRequestMessengerPayLoader"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to fetch eligibility of the sender to send money to recipient "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/IpM;->a:LX/IpP;

    iget-object v3, v3, LX/IpP;->g:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2612837
    iget-object p1, v3, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->p:Lcom/facebook/user/model/UserKey;

    move-object v3, p1

    .line 2612838
    invoke-virtual {v3}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2612839
    iget-object v0, p0, LX/IpM;->a:LX/IpP;

    iget-object v0, v0, LX/IpP;->f:LX/Io3;

    invoke-virtual {v0}, LX/Io3;->a()V

    .line 2612840
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2612833
    check-cast p1, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityResult;

    .line 2612834
    iget-object v0, p0, LX/IpM;->a:LX/IpP;

    iget-object v0, v0, LX/IpP;->g:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    invoke-virtual {p1}, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityResult;->a()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->a(Z)V

    .line 2612835
    return-void
.end method
