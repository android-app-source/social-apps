.class public LX/Hex;
.super Lcom/facebook/widget/OverlayLayout;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7Ro;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/video/watchandgo/view/WatchAndGoVideoPlayer;

.field public c:Landroid/widget/ProgressBar;

.field private d:Landroid/widget/ProgressBar;

.field private e:Landroid/view/View;

.field public f:LX/Hew;

.field private g:LX/Hev;

.field public h:I

.field public i:I

.field private j:I

.field private k:I

.field private l:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 8

    .prologue
    .line 2490979
    invoke-direct {p0, p1}, Lcom/facebook/widget/OverlayLayout;-><init>(Landroid/content/Context;)V

    .line 2490980
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2490981
    iput-object v0, p0, LX/Hex;->a:LX/0Ot;

    .line 2490982
    const-class v0, LX/Hex;

    invoke-static {v0, p0}, LX/Hex;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2490983
    const v0, 0x7f03160a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 2490984
    const v0, 0x7f0214b7

    invoke-virtual {p0, v0}, LX/Hex;->setBackgroundResource(I)V

    .line 2490985
    invoke-virtual {p0}, LX/Hex;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2490986
    const v1, 0x7f0b1be8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, LX/Hex;->l:I

    .line 2490987
    const v1, 0x7f0b1be9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, LX/Hex;->j:I

    .line 2490988
    const v1, 0x7f0b1bea

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, LX/Hex;->k:I

    .line 2490989
    iget v0, p0, LX/Hex;->l:I

    iget v1, p0, LX/Hex;->j:I

    iget v2, p0, LX/Hex;->l:I

    iget v3, p0, LX/Hex;->k:I

    invoke-virtual {p0, v0, v1, v2, v3}, LX/Hex;->setPadding(IIII)V

    .line 2490990
    const v0, 0x7f0d1338

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/watchandgo/view/WatchAndGoVideoPlayer;

    iput-object v0, p0, LX/Hex;->b:Lcom/facebook/video/watchandgo/view/WatchAndGoVideoPlayer;

    .line 2490991
    const v0, 0x7f0d04de

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, LX/Hex;->c:Landroid/widget/ProgressBar;

    .line 2490992
    const v0, 0x7f0d318c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/Hex;->e:Landroid/view/View;

    .line 2490993
    const v0, 0x7f0d318d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, LX/Hex;->d:Landroid/widget/ProgressBar;

    .line 2490994
    new-instance v0, LX/Hew;

    invoke-direct {v0, p0}, LX/Hew;-><init>(LX/Hex;)V

    iput-object v0, p0, LX/Hex;->f:LX/Hew;

    .line 2490995
    new-instance v0, Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-direct {v0, p1}, Lcom/facebook/video/player/plugins/VideoPlugin;-><init>(Landroid/content/Context;)V

    .line 2490996
    const/4 v1, 0x0

    .line 2490997
    iput-boolean v1, v0, Lcom/facebook/video/player/plugins/VideoPlugin;->d:Z

    .line 2490998
    iget-object v1, p0, LX/Hex;->b:Lcom/facebook/video/watchandgo/view/WatchAndGoVideoPlayer;

    .line 2490999
    invoke-static {v1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 2491000
    new-instance v0, LX/Hev;

    invoke-direct {v0, p1}, LX/Hev;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/Hex;->g:LX/Hev;

    .line 2491001
    iget-object v0, p0, LX/Hex;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Ro;

    .line 2491002
    iget-object v4, v0, LX/7Ro;->a:LX/0W3;

    sget-wide v6, LX/0X5;->hL:J

    const/16 v5, 0x2a

    invoke-interface {v4, v6, v7, v5}, LX/0W4;->a(JI)I

    move-result v4

    move v0, v4

    .line 2491003
    iput v0, p0, LX/Hex;->h:I

    .line 2491004
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, LX/Hex;

    const/16 p0, 0x3850

    invoke-static {v1, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    iput-object v1, p1, LX/Hex;->a:LX/0Ot;

    return-void
.end method


# virtual methods
.method public final a(LX/2pa;I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2491005
    iput v4, p0, LX/Hex;->i:I

    .line 2491006
    iget-object v0, p0, LX/Hex;->b:Lcom/facebook/video/watchandgo/view/WatchAndGoVideoPlayer;

    sget-object v1, LX/04G;->WATCH_AND_GO:LX/04G;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerType(LX/04G;)V

    .line 2491007
    iget-object v0, p0, LX/Hex;->g:LX/Hev;

    .line 2491008
    iget-object v1, p0, LX/Hex;->b:Lcom/facebook/video/watchandgo/view/WatchAndGoVideoPlayer;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, LX/3Ge;->a(Lcom/facebook/video/player/RichVideoPlayer;LX/2pa;LX/7Lf;)Lcom/facebook/video/player/RichVideoPlayer;

    .line 2491009
    iget-object v1, p0, LX/Hex;->b:Lcom/facebook/video/watchandgo/view/WatchAndGoVideoPlayer;

    invoke-virtual {v1, p1}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/2pa;)V

    .line 2491010
    iget-object v1, p0, LX/Hex;->b:Lcom/facebook/video/watchandgo/view/WatchAndGoVideoPlayer;

    sget-object v2, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    .line 2491011
    iget-object v0, p0, LX/Hex;->b:Lcom/facebook/video/watchandgo/view/WatchAndGoVideoPlayer;

    sget-object v1, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v0, v4, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    .line 2491012
    iget-object v0, p0, LX/Hex;->b:Lcom/facebook/video/watchandgo/view/WatchAndGoVideoPlayer;

    sget-object v1, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v0, p2, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(ILX/04g;)V

    .line 2491013
    iget-object v0, p0, LX/Hex;->f:LX/Hew;

    iget v1, p0, LX/Hex;->h:I

    int-to-long v2, v1

    invoke-virtual {v0, v4, v2, v3}, LX/Hew;->sendEmptyMessageDelayed(IJ)Z

    .line 2491014
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2491015
    iget-object v0, p0, LX/Hex;->d:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2491016
    return-void
.end method

.method public setCallbackListener(LX/3It;)V
    .locals 1

    .prologue
    .line 2491017
    iget-object v0, p0, LX/Hex;->b:Lcom/facebook/video/watchandgo/view/WatchAndGoVideoPlayer;

    .line 2491018
    iput-object p1, v0, Lcom/facebook/video/player/RichVideoPlayer;->F:LX/3It;

    .line 2491019
    return-void
.end method

.method public setTouchListener(Landroid/view/View$OnTouchListener;)V
    .locals 1

    .prologue
    .line 2491020
    iget-object v0, p0, LX/Hex;->e:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2491021
    return-void
.end method
