.class public LX/HWC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2475722
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2475723
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 4

    .prologue
    .line 2475724
    const-string v0, "com.facebook.katana.profile.id"

    const-wide/16 v2, -0x1

    invoke-virtual {p1, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 2475725
    new-instance v2, Lcom/facebook/pages/identity/fragments/residence/PageResidenceFragment;

    invoke-direct {v2}, Lcom/facebook/pages/identity/fragments/residence/PageResidenceFragment;-><init>()V

    .line 2475726
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2475727
    const-string p0, "com.facebook.katana.profile.id"

    invoke-virtual {v3, p0, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2475728
    invoke-virtual {v2, v3}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2475729
    move-object v0, v2

    .line 2475730
    return-object v0
.end method
