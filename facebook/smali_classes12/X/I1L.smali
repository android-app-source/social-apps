.class public final LX/I1L;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/I1N;


# direct methods
.method public constructor <init>(LX/I1N;)V
    .locals 0

    .prologue
    .line 2528871
    iput-object p1, p0, LX/I1L;->a:LX/I1N;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x3c9ee932

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2528872
    iget-object v1, p0, LX/I1L;->a:LX/I1N;

    iget-object v1, v1, LX/I1N;->h:LX/HzL;

    .line 2528873
    iget-object v3, v1, LX/HzL;->a:Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;

    iget-object v3, v3, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->y:LX/1nQ;

    iget-object v4, v1, LX/HzL;->a:Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;

    iget-object v4, v4, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->l:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v4, v4, Lcom/facebook/events/common/EventAnalyticsParams;->d:Ljava/lang/String;

    sget-object v5, Lcom/facebook/events/common/ActionMechanism;->EVENT_DASHBOARD_HOME_TAB_SEE_ALL_UPCOMING_EVENTS:Lcom/facebook/events/common/ActionMechanism;

    .line 2528874
    iget-object v6, v3, LX/1nQ;->i:LX/0Zb;

    const-string p0, "event_home_tab_upcoming_events_see_all"

    const/4 p1, 0x0

    invoke-interface {v6, p0, p1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v6

    .line 2528875
    invoke-virtual {v6}, LX/0oG;->a()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 2528876
    const-string p0, "event_dashboard"

    invoke-virtual {v6, p0}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v6

    iget-object p0, v3, LX/1nQ;->j:LX/0kv;

    iget-object p1, v3, LX/1nQ;->g:Landroid/content/Context;

    invoke-virtual {p0, p1}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v6, p0}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    move-result-object v6

    const-string p0, "ref_module"

    invoke-virtual {v6, p0, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v6

    const-string p0, "ref_mechanism"

    invoke-virtual {v5}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, p0, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v6

    invoke-virtual {v6}, LX/0oG;->d()V

    .line 2528877
    :cond_0
    iget-object v3, v1, LX/HzL;->a:Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;

    sget-object v4, LX/Hx7;->CALENDAR:LX/Hx7;

    .line 2528878
    invoke-static {v3, v4}, Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;->b$redex0(Lcom/facebook/events/dashboard/EventsHeroDashboardFragment;LX/Hx7;)V

    .line 2528879
    const v1, -0x70193a11

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
