.class public LX/ILp;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/ILp;


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(LX/0Or;Landroid/content/res/Resources;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;",
            "Landroid/content/res/Resources;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2568410
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2568411
    iput-object p1, p0, LX/ILp;->a:LX/0Or;

    .line 2568412
    iput-object p2, p0, LX/ILp;->b:Landroid/content/res/Resources;

    .line 2568413
    return-void
.end method

.method public static a(LX/0QB;)LX/ILp;
    .locals 5

    .prologue
    .line 2568424
    sget-object v0, LX/ILp;->c:LX/ILp;

    if-nez v0, :cond_1

    .line 2568425
    const-class v1, LX/ILp;

    monitor-enter v1

    .line 2568426
    :try_start_0
    sget-object v0, LX/ILp;->c:LX/ILp;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2568427
    if-eqz v2, :cond_0

    .line 2568428
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2568429
    new-instance v4, LX/ILp;

    const/16 v3, 0xc

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-direct {v4, p0, v3}, LX/ILp;-><init>(LX/0Or;Landroid/content/res/Resources;)V

    .line 2568430
    move-object v0, v4

    .line 2568431
    sput-object v0, LX/ILp;->c:LX/ILp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2568432
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2568433
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2568434
    :cond_1
    sget-object v0, LX/ILp;->c:LX/ILp;

    return-object v0

    .line 2568435
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2568436
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupCategory;)Landroid/content/Intent;
    .locals 6

    .prologue
    .line 2568414
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v0, p0, LX/ILp;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 2568415
    move-object v0, v0

    .line 2568416
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->COMMUNITY_SEARCH_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2568417
    const-string v1, "group_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2568418
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->COLLEGE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    invoke-virtual {p3, v1}, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2568419
    const-string v1, "community_default_search_query"

    iget-object v2, p0, LX/ILp;->b:Landroid/content/res/Resources;

    const v3, 0x7f08301e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/ILp;->b:Landroid/content/res/Resources;

    const v4, 0x7f08301f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/ILp;->b:Landroid/content/res/Resources;

    const v5, 0x7f083020

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2568420
    :cond_0
    :goto_0
    const-string v1, "group_name"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2568421
    return-object v0

    .line 2568422
    :cond_1
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->CITY:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    invoke-virtual {p3, v1}, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2568423
    const-string v1, "community_default_search_query"

    iget-object v2, p0, LX/ILp;->b:Landroid/content/res/Resources;

    const v3, 0x7f083021

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/ILp;->b:Landroid/content/res/Resources;

    const v4, 0x7f083022

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/ILp;->b:Landroid/content/res/Resources;

    const v5, 0x7f083023

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto :goto_0
.end method
