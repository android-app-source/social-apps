.class public LX/HJw;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/2kp;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/E1f;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/E1f;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2453489
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2453490
    iput-object p1, p0, LX/HJw;->a:LX/0Ot;

    .line 2453491
    return-void
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;)I
    .locals 2

    .prologue
    .line 2453492
    sget-object v0, LX/HJv;->a:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2453493
    const/4 v0, 0x0

    const-string v1, "This shouldn\'t be hit as the calling part definition ensures it"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2453494
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "This should not be happening,check calling part definition"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2453495
    :pswitch_0
    const v0, 0x7f0a053d

    .line 2453496
    :goto_0
    return v0

    .line 2453497
    :pswitch_1
    const v0, 0x7f0a053e

    goto :goto_0

    .line 2453498
    :pswitch_2
    const v0, 0x7f0a053f

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(LX/1De;Ljava/lang/String;I)LX/1Di;
    .locals 2

    .prologue
    .line 2453499
    invoke-static {p0}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/1ne;->n(I)LX/1ne;

    move-result-object v0

    const v1, 0x7f0b004e

    invoke-virtual {v0, v1}, LX/1ne;->q(I)LX/1ne;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/HJw;
    .locals 4

    .prologue
    .line 2453500
    const-class v1, LX/HJw;

    monitor-enter v1

    .line 2453501
    :try_start_0
    sget-object v0, LX/HJw;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2453502
    sput-object v2, LX/HJw;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2453503
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2453504
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2453505
    new-instance v3, LX/HJw;

    const/16 p0, 0x3098

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/HJw;-><init>(LX/0Ot;)V

    .line 2453506
    move-object v0, v3

    .line 2453507
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2453508
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/HJw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2453509
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2453510
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
