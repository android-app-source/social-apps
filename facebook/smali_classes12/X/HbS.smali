.class public final LX/HbS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/HbY;


# direct methods
.method public constructor <init>(LX/HbY;)V
    .locals 0

    .prologue
    .line 2485505
    iput-object p1, p0, LX/HbS;->a:LX/HbY;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x1ee5aec

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2485506
    iget-object v1, p0, LX/HbS;->a:LX/HbY;

    iget-object v1, v1, LX/HbY;->F:LX/HaZ;

    const-string v2, "PASSWORD_CHANGE_CTA"

    invoke-virtual {v1, v2}, LX/HaZ;->a(Ljava/lang/String;)V

    .line 2485507
    iget-object v1, p0, LX/HbS;->a:LX/HbY;

    iget-object v1, v1, LX/HbY;->D:LX/Hbf;

    .line 2485508
    new-instance p0, Landroid/content/Intent;

    invoke-direct {p0}, Landroid/content/Intent;-><init>()V

    iget-object v2, v1, LX/Hbf;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ComponentName;

    invoke-virtual {p0, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v2

    .line 2485509
    const-string p0, "target_fragment"

    sget-object p1, LX/0cQ;->SECURITY_CHECKUP_PASSWORD_CHANGE_FRAGMENT:LX/0cQ;

    invoke-virtual {p1}, LX/0cQ;->ordinal()I

    move-result p1

    invoke-virtual {v2, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2485510
    iget-object p0, v1, LX/Hbf;->a:Lcom/facebook/content/SecureContextHelper;

    iget-object p1, v1, LX/Hbf;->c:Landroid/content/Context;

    invoke-interface {p0, v2, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2485511
    const v1, -0x34068802    # -3.2698364E7f

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
