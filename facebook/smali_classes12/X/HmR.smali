.class public final LX/HmR;
.super LX/HmN;
.source ""


# instance fields
.field public final synthetic a:LX/HmV;


# direct methods
.method public constructor <init>(LX/HmV;)V
    .locals 1

    .prologue
    .line 2500678
    iput-object p1, p0, LX/HmR;->a:LX/HmV;

    invoke-direct {p0, p1}, LX/HmN;-><init>(LX/HmV;)V

    return-void
.end method


# virtual methods
.method public final a()LX/HmU;
    .locals 7

    .prologue
    .line 2500679
    iget-object v0, p0, LX/HmR;->a:LX/HmV;

    iget-object v0, v0, LX/HmV;->p:Ljava/lang/String;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Tp;->a(Z)V

    .line 2500680
    iget-object v0, p0, LX/HmR;->a:LX/HmV;

    iget-object v0, v0, LX/HmV;->j:LX/Hm4;

    invoke-virtual {v0}, LX/Hm4;->d()LX/Hm3;

    move-result-object v0

    .line 2500681
    if-eqz v0, :cond_0

    .line 2500682
    sget-object v1, LX/HmT;->a:[I

    invoke-virtual {v0}, LX/Hm3;->ordinal()I

    move-result v0

    aget v0, v1, v0

    sparse-switch v0, :sswitch_data_0

    .line 2500683
    :cond_0
    iget-object v0, p0, LX/HmR;->a:LX/HmV;

    invoke-static {v0}, LX/HmV;->j(LX/HmV;)V

    .line 2500684
    iget-object v0, p0, LX/HmR;->a:LX/HmV;

    iget-object v0, v0, LX/HmV;->k:LX/HmU;

    :goto_1
    return-object v0

    .line 2500685
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2500686
    :sswitch_0
    iget-object v0, p0, LX/HmR;->a:LX/HmV;

    invoke-static {v0}, LX/HmV;->k(LX/HmV;)V

    .line 2500687
    iget-object v0, p0, LX/HmR;->a:LX/HmV;

    iget-object v0, v0, LX/HmV;->k:LX/HmU;

    goto :goto_1

    .line 2500688
    :sswitch_1
    iget-object v0, p0, LX/HmR;->a:LX/HmV;

    iget-object v0, v0, LX/HmV;->j:LX/Hm4;

    .line 2500689
    iget-object v1, v0, LX/Hm4;->c:Ljava/io/DataInputStream;

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    .line 2500690
    iget-object v0, p0, LX/HmR;->a:LX/HmV;

    iget-object v1, p0, LX/HmR;->a:LX/HmV;

    iget-object v1, v1, LX/HmV;->e:LX/Hm6;

    iget-object v2, p0, LX/HmR;->a:LX/HmV;

    iget-object v2, v2, LX/HmV;->p:Ljava/lang/String;

    .line 2500691
    invoke-static {v1}, LX/Hm6;->b(LX/Hm6;)Ljava/io/File;

    move-result-object v3

    .line 2500692
    new-instance v4, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "temp_"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".apk"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v3, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 2500693
    move-object v1, v4

    .line 2500694
    iput-object v1, v0, LX/HmV;->r:Ljava/io/File;

    .line 2500695
    new-instance v0, Ljava/io/FileOutputStream;

    iget-object v1, p0, LX/HmR;->a:LX/HmV;

    iget-object v1, v1, LX/HmV;->r:Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 2500696
    iget-object v1, p0, LX/HmR;->a:LX/HmV;

    iget-object v1, v1, LX/HmV;->j:LX/Hm4;

    invoke-virtual {v1, v0}, LX/Hm4;->a(Ljava/io/FileOutputStream;)V

    .line 2500697
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V

    .line 2500698
    :try_start_0
    iget-object v0, p0, LX/HmR;->a:LX/HmV;

    iget-object v0, v0, LX/HmV;->j:LX/Hm4;

    .line 2500699
    iget-object v1, v0, LX/Hm4;->b:Ljava/io/DataOutputStream;

    sget-object v2, LX/Hm3;->TRANSFER_SUCCESS:LX/Hm3;

    invoke-virtual {v2}, LX/Hm3;->getInt()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 2500700
    iget-object v1, v0, LX/Hm4;->b:Ljava/io/DataOutputStream;

    invoke-virtual {v1}, Ljava/io/DataOutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2500701
    :goto_2
    iget-object v0, p0, LX/HmR;->a:LX/HmV;

    sget-object v1, LX/HmU;->WAIT_VERIFY_APK:LX/HmU;

    .line 2500702
    iput-object v1, v0, LX/HmV;->k:LX/HmU;

    move-object v0, v1

    .line 2500703
    goto :goto_1

    .line 2500704
    :catch_0
    move-exception v0

    .line 2500705
    iget-object v1, p0, LX/HmR;->a:LX/HmV;

    iget-object v1, v1, LX/HmV;->f:LX/Hmc;

    .line 2500706
    sget-object v2, LX/Hmb;->WRITE_TRANSFER_SUCCESS_EXCEPTION:LX/Hmb;

    invoke-static {v1, v2}, LX/Hmc;->a(LX/Hmc;LX/Hmb;)V

    .line 2500707
    const-string v1, "BeamReceiver"

    const-string v2, "Failed to send success message to sender"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x6 -> :sswitch_1
    .end sparse-switch
.end method
