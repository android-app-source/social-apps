.class public LX/INF;
.super LX/7HQ;
.source ""

# interfaces
.implements LX/BWd;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/7HQ",
        "<",
        "LX/INB;",
        ">;",
        "LX/BWd",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field public final b:LX/7Hg;

.field private final c:LX/INI;

.field private final d:LX/IND;

.field private e:Z


# direct methods
.method public constructor <init>(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;LX/7Hg;LX/INE;LX/7Hl;LX/7HW;LX/7Hk;LX/INI;LX/7Ho;LX/2Sd;)V
    .locals 6
    .param p1    # Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/7HW;
        .annotation runtime Lcom/facebook/groups/community/search/CommunityGroupsTypeahead;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2570683
    move-object v0, p0

    move-object v1, p2

    move-object v2, p4

    move-object v3, p5

    move-object v4, p6

    move-object v5, p9

    invoke-direct/range {v0 .. v5}, LX/7HQ;-><init>(LX/7Hg;LX/7Hl;LX/7HW;LX/7Hk;LX/2Sd;)V

    .line 2570684
    iput-object p2, p0, LX/INF;->b:LX/7Hg;

    .line 2570685
    iput-object p7, p0, LX/INF;->c:LX/INI;

    .line 2570686
    invoke-virtual {p3, p1}, LX/INE;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityNuxQuestionsFragmentModel$CommunityNuxQuestionsModel;)LX/IND;

    move-result-object v0

    iput-object v0, p0, LX/INF;->d:LX/IND;

    .line 2570687
    iget-object v0, p0, LX/INF;->b:LX/7Hg;

    iget-object v1, p0, LX/INF;->d:LX/IND;

    invoke-virtual {v0, v1}, LX/7Hg;->a(LX/7HS;)V

    .line 2570688
    sget-object v0, LX/7HY;->REMOTE:LX/7HY;

    iget-object v1, p0, LX/INF;->d:LX/IND;

    .line 2570689
    iget-object v2, v1, LX/7HT;->c:LX/7Hn;

    move-object v1, v2

    .line 2570690
    invoke-virtual {p8, v0, v1}, LX/7Ho;->a(LX/7HY;LX/7Hn;)V

    .line 2570691
    return-void
.end method


# virtual methods
.method public final a(LX/7Hi;Ljava/lang/String;)LX/7HN;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7Hi",
            "<",
            "LX/INB;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "LX/7HN",
            "<",
            "LX/INB;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2570708
    sget-object v0, LX/7Hb;->a:LX/7Hb;

    move-object v0, v0

    .line 2570709
    return-object v0
.end method

.method public final a(LX/7Hi;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7Hi",
            "<",
            "LX/INB;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2570700
    invoke-virtual {p0, p1}, LX/7HQ;->b(LX/7Hi;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2570701
    :goto_0
    return-void

    .line 2570702
    :cond_0
    iget-boolean v0, p0, LX/INF;->e:Z

    if-eqz v0, :cond_1

    .line 2570703
    iget-object v0, p1, LX/7Hi;->c:LX/7HY;

    move-object v0, v0

    .line 2570704
    sget-object v1, LX/7HY;->REMOTE:LX/7HY;

    if-ne v0, v1, :cond_1

    .line 2570705
    invoke-virtual {p0, p1}, LX/7HQ;->c(LX/7Hi;)LX/7Hc;

    .line 2570706
    goto :goto_0

    .line 2570707
    :cond_1
    invoke-super {p0, p1}, LX/7HQ;->a(LX/7Hi;)V

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2570699
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2570698
    const/4 v0, 0x0

    return v0
.end method

.method public final a(I)Z
    .locals 2

    .prologue
    .line 2570696
    invoke-static {p1}, LX/IN8;->fromOrdinal(I)LX/IN8;

    move-result-object v0

    .line 2570697
    sget-object v1, LX/IN8;->GroupRow:LX/IN8;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 2570692
    iput-boolean p1, p0, LX/INF;->e:Z

    .line 2570693
    iget-object v0, p0, LX/INF;->c:LX/INI;

    .line 2570694
    iput-boolean p1, v0, LX/INI;->a:Z

    .line 2570695
    return-void
.end method
