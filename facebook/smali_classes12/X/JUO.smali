.class public LX/JUO;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2698166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2698167
    iput-object p1, p0, LX/JUO;->a:LX/0Zb;

    .line 2698168
    return-void
.end method

.method public static a(LX/JUO;LX/6VK;Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;)V
    .locals 3

    .prologue
    .line 2698169
    iget-object v0, p0, LX/JUO;->a:LX/0Zb;

    invoke-virtual {p1}, LX/6VK;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 2698170
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2698171
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->l()Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;->PULSE:Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;

    if-eq v1, v2, :cond_0

    .line 2698172
    const-string v1, "target_id"

    invoke-static {p2}, LX/25D;->a(LX/16q;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2698173
    :cond_0
    const-string v1, "native_newsfeed"

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v1, "feed_type"

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;->o()Lcom/facebook/graphql/enums/GraphQLFriendsLocationsFeedType;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    move-result-object v0

    const-string v1, "location_category"

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;->l()Lcom/facebook/graphql/enums/GraphQLFriendLocationCategory;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    move-result-object v0

    invoke-virtual {v0}, LX/0oG;->d()V

    .line 2698174
    :cond_1
    return-void
.end method

.method public static b(LX/0QB;)LX/JUO;
    .locals 2

    .prologue
    .line 2698175
    new-instance v1, LX/JUO;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-direct {v1, v0}, LX/JUO;-><init>(LX/0Zb;)V

    .line 2698176
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;)V
    .locals 1

    .prologue
    .line 2698177
    sget-object v0, LX/6VK;->FRIENDS_LOCATIONS_FEEDSTORY_TAP_PROFILE:LX/6VK;

    invoke-static {p0, v0, p1, p2}, LX/JUO;->a(LX/JUO;LX/6VK;Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;)V

    .line 2698178
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2698179
    iget-object v0, p0, LX/JUO;->a:LX/0Zb;

    const-string v1, "friends_nearby_feedunit_wave"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 2698180
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2698181
    const-string v1, "native_newsfeed"

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v1, "target_id"

    invoke-virtual {v0, v1, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v1, "action"

    const-string v2, "wave_sent"

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    invoke-virtual {v0}, LX/0oG;->d()V

    .line 2698182
    :cond_0
    return-void
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;)V
    .locals 1

    .prologue
    .line 2698183
    sget-object v0, LX/6VK;->FRIENDS_LOCATIONS_FEEDSTORY_V2_TAP_PROFILE:LX/6VK;

    invoke-static {p0, v0, p1, p2}, LX/JUO;->a(LX/JUO;LX/6VK;Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;)V

    .line 2698184
    return-void
.end method

.method public final c(Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;)V
    .locals 1

    .prologue
    .line 2698185
    sget-object v0, LX/6VK;->FRIENDS_LOCATIONS_FEEDSTORY_V2_TAP_MAP:LX/6VK;

    invoke-static {p0, v0, p1, p2}, LX/JUO;->a(LX/JUO;LX/6VK;Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;)V

    .line 2698186
    return-void
.end method

.method public final d(Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;)V
    .locals 1

    .prologue
    .line 2698187
    sget-object v0, LX/6VK;->FRIENDS_LOCATIONS_FEEDSTORY_TAP_PROFILE_PIC:LX/6VK;

    invoke-static {p0, v0, p1, p2}, LX/JUO;->a(LX/JUO;LX/6VK;Lcom/facebook/graphql/model/GraphQLFriendLocationFeedUnitItem;Lcom/facebook/graphql/model/GraphQLFriendsLocationsFeedUnit;)V

    .line 2698188
    return-void
.end method
