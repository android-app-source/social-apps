.class public LX/I3G;
.super LX/Gco;
.source ""


# instance fields
.field private final a:LX/7vU;


# direct methods
.method public constructor <init>(LX/7vU;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2531212
    invoke-direct {p0}, LX/Gco;-><init>()V

    .line 2531213
    iput-object p1, p0, LX/I3G;->a:LX/7vU;

    .line 2531214
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/facebook/events/model/Event;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2531215
    const v0, 0x7f0821b6

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/events/model/Event;)V
    .locals 4

    .prologue
    .line 2531216
    iget-object v0, p0, LX/I3G;->a:LX/7vU;

    .line 2531217
    iget-object v1, p1, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2531218
    const-string v2, "MOBILE_SUGGESTIONS_DASHBOARD"

    .line 2531219
    new-instance v3, LX/4EL;

    invoke-direct {v3}, LX/4EL;-><init>()V

    new-instance p0, LX/4EG;

    invoke-direct {p0}, LX/4EG;-><init>()V

    invoke-virtual {p0, v2}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    move-result-object p0

    invoke-static {p0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object p0

    invoke-virtual {v3, p0}, LX/4EL;->a(Ljava/util/List;)LX/4EL;

    move-result-object v3

    .line 2531220
    new-instance p0, LX/4EW;

    invoke-direct {p0}, LX/4EW;-><init>()V

    .line 2531221
    const-string p1, "event_id"

    invoke-virtual {p0, p1, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2531222
    move-object p0, p0

    .line 2531223
    const-string p1, "HIDE"

    .line 2531224
    const-string v1, "negative_action_type"

    invoke-virtual {p0, v1, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2531225
    move-object p0, p0

    .line 2531226
    const-string p1, "context"

    invoke-virtual {p0, p1, v3}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 2531227
    move-object v3, p0

    .line 2531228
    new-instance p0, LX/7uP;

    invoke-direct {p0}, LX/7uP;-><init>()V

    move-object p0, p0

    .line 2531229
    const-string p1, "input"

    invoke-virtual {p0, p1, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v3

    check-cast v3, LX/7uP;

    invoke-static {v3}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v3

    .line 2531230
    iget-object p0, v0, LX/7vU;->a:LX/0tX;

    invoke-virtual {p0, v3}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2531231
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2531232
    const/4 v0, 0x0

    return v0
.end method
