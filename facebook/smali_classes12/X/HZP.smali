.class public final LX/HZP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/registration/fragment/RegistrationInlineTermsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/registration/fragment/RegistrationInlineTermsFragment;)V
    .locals 0

    .prologue
    .line 2482207
    iput-object p1, p0, LX/HZP;->a:Lcom/facebook/registration/fragment/RegistrationInlineTermsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x2

    const v0, -0x2d817cbf

    invoke-static {v4, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2482208
    iget-object v0, p0, LX/HZP;->a:Lcom/facebook/registration/fragment/RegistrationInlineTermsFragment;

    iget-object v0, v0, Lcom/facebook/registration/fragment/RegistrationInlineTermsFragment;->b:Lcom/facebook/registration/model/SimpleRegFormData;

    invoke-virtual {v0, v2}, Lcom/facebook/registration/model/RegistrationFormData;->b(Z)V

    .line 2482209
    iget-object v0, p0, LX/HZP;->a:Lcom/facebook/registration/fragment/RegistrationInlineTermsFragment;

    iget-object v0, v0, Lcom/facebook/registration/fragment/RegistrationInlineTermsFragment;->c:LX/HaQ;

    iget-object v2, p0, LX/HZP;->a:Lcom/facebook/registration/fragment/RegistrationInlineTermsFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/HaQ;->a(Landroid/app/Activity;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, LX/HZO;

    invoke-direct {v3, p0}, LX/HZO;-><init>(LX/HZP;)V

    iget-object v0, p0, LX/HZP;->a:Lcom/facebook/registration/fragment/RegistrationInlineTermsFragment;

    iget-object v0, v0, Lcom/facebook/registration/fragment/RegistrationInlineTermsFragment;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    invoke-static {v2, v3, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2482210
    const v0, -0x78c316e2

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
