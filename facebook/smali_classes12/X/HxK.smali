.class public LX/HxK;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/facebook/events/model/Event;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/facebook/events/model/Event;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final c:LX/Bl1;

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/model/Event;",
            ">;"
        }
    .end annotation
.end field

.field public e:Z

.field public f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/events/model/Event;",
            ">;"
        }
    .end annotation
.end field

.field public g:I

.field public h:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2522357
    new-instance v0, LX/HxI;

    invoke-direct {v0}, LX/HxI;-><init>()V

    .line 2522358
    sput-object v0, LX/HxK;->a:Ljava/util/Comparator;

    invoke-static {v0}, Ljava/util/Collections;->reverseOrder(Ljava/util/Comparator;)Ljava/util/Comparator;

    move-result-object v0

    sput-object v0, LX/HxK;->b:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(LX/Bl1;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2522353
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2522354
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/HxK;->d:Ljava/util/List;

    .line 2522355
    iput-object p1, p0, LX/HxK;->c:LX/Bl1;

    .line 2522356
    return-void
.end method

.method public static a(LX/HxK;Landroid/database/Cursor;LX/0Py;Z)V
    .locals 3
    .param p0    # LX/HxK;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "LX/0Py",
            "<",
            "Lcom/facebook/events/model/Event;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2522343
    iput v0, p0, LX/HxK;->g:I

    .line 2522344
    iput v0, p0, LX/HxK;->h:I

    .line 2522345
    iput-boolean p3, p0, LX/HxK;->e:Z

    .line 2522346
    iget-object v0, p0, LX/HxK;->c:LX/Bl1;

    invoke-virtual {v0, p1}, LX/Bl1;->a(Landroid/database/Cursor;)V

    .line 2522347
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, LX/HxK;->d:Ljava/util/List;

    .line 2522348
    if-eqz p3, :cond_0

    .line 2522349
    iget-object v0, p0, LX/HxK;->d:Ljava/util/List;

    sget-object v1, LX/HxK;->a:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2522350
    :goto_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/HxK;->c:LX/Bl1;

    invoke-virtual {v1}, LX/Bl1;->b()I

    move-result v1

    iget-object v2, p0, LX/HxK;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/2addr v1, v2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/HxK;->f:Ljava/util/ArrayList;

    .line 2522351
    return-void

    .line 2522352
    :cond_0
    iget-object v0, p0, LX/HxK;->d:Ljava/util/List;

    sget-object v1, LX/HxK;->b:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2522342
    invoke-virtual {p0}, LX/HxK;->b()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 2522341
    iget-object v0, p0, LX/HxK;->c:LX/Bl1;

    invoke-virtual {v0}, LX/Bl1;->b()I

    move-result v0

    iget-object v1, p0, LX/HxK;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final c()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2522340
    new-instance v0, LX/HxJ;

    invoke-direct {v0, p0}, LX/HxJ;-><init>(LX/HxK;)V

    return-object v0
.end method
