.class public final LX/J1b;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;)V
    .locals 0

    .prologue
    .line 2639038
    iput-object p1, p0, LX/J1b;->a:Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 2639039
    if-eqz p2, :cond_0

    .line 2639040
    iget-object v0, p0, LX/J1b;->a:Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;

    iget-object v0, v0, Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0wM;

    const v1, 0x7f021426

    iget-object v2, p0, LX/J1b;->a:Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;

    invoke-virtual {v2}, Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/73a;->a(Landroid/content/Context;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2639041
    iget-object v1, p0, LX/J1b;->a:Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;

    iget-object v1, v1, Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;->e:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v1, v0, v3, v3, v3}, Lcom/facebook/widget/text/BetterEditTextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2639042
    :goto_0
    return-void

    .line 2639043
    :cond_0
    iget-object v0, p0, LX/J1b;->a:Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;

    iget-object v0, v0, Lcom/facebook/payments/p2p/ui/P2pPaymentMemoView;->e:Lcom/facebook/widget/text/BetterEditTextView;

    const v1, 0x7f021426

    invoke-virtual {v0, v1, v2, v2, v2}, Lcom/facebook/widget/text/BetterEditTextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(IIII)V

    goto :goto_0
.end method
