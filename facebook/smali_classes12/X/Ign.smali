.class public LX/Ign;
.super LX/1OM;
.source ""

# interfaces
.implements LX/Ige;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;",
        "LX/Ige;"
    }
.end annotation


# instance fields
.field public final a:LX/0Rl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rl",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/view/LayoutInflater;

.field private final c:LX/Igs;

.field private final d:LX/Igw;

.field private final e:Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;

.field public final f:Landroid/content/Context;

.field private final g:LX/Igl;

.field private final h:LX/Igm;

.field public i:LX/Igc;

.field public j:LX/Igt;

.field public k:LX/Igi;

.field public l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ">;"
        }
    .end annotation
.end field

.field public m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/LayoutInflater;LX/Igs;LX/Igw;Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;)V
    .locals 2
    .param p5    # Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2601740
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2601741
    new-instance v0, LX/Igj;

    invoke-direct {v0, p0}, LX/Igj;-><init>(LX/Ign;)V

    iput-object v0, p0, LX/Ign;->a:LX/0Rl;

    .line 2601742
    new-instance v0, LX/Igl;

    invoke-direct {v0, p0}, LX/Igl;-><init>(LX/Ign;)V

    iput-object v0, p0, LX/Ign;->g:LX/Igl;

    .line 2601743
    new-instance v0, LX/Igm;

    invoke-direct {v0, p0}, LX/Igm;-><init>(LX/Ign;)V

    iput-object v0, p0, LX/Ign;->h:LX/Igm;

    .line 2601744
    iput-object p1, p0, LX/Ign;->f:Landroid/content/Context;

    .line 2601745
    iput-object p2, p0, LX/Ign;->b:Landroid/view/LayoutInflater;

    .line 2601746
    iput-object p3, p0, LX/Ign;->c:LX/Igs;

    .line 2601747
    iput-object p4, p0, LX/Ign;->d:LX/Igw;

    .line 2601748
    iput-object p5, p0, LX/Ign;->e:Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;

    .line 2601749
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Ign;->l:Ljava/util/List;

    .line 2601750
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Ign;->m:Ljava/util/List;

    .line 2601751
    return-void
.end method

.method public static d$redex0(LX/Ign;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2601739
    iget-object v0, p0, LX/Ign;->k:LX/Igi;

    sget-object v1, LX/Igi;->ALL:LX/Igi;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/Ign;->l:Ljava/util/List;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/Ign;->m:Ljava/util/List;

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2601643
    packed-switch p2, :pswitch_data_0

    .line 2601644
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected view type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2601645
    :pswitch_0
    iget-object v0, p0, LX/Ign;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f030f43

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2601646
    iget-object v1, p0, LX/Ign;->c:LX/Igs;

    .line 2601647
    new-instance p2, Lcom/facebook/messaging/media/mediapicker/PhotoItemController;

    invoke-static {v1}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v2

    check-cast v2, LX/1Ad;

    invoke-static {v1}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v1}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object p1

    check-cast p1, LX/11S;

    invoke-direct {p2, v2, v3, p1, v0}, Lcom/facebook/messaging/media/mediapicker/PhotoItemController;-><init>(LX/1Ad;Landroid/content/res/Resources;LX/11S;Landroid/view/View;)V

    .line 2601648
    move-object v0, p2

    .line 2601649
    iget-object v1, p0, LX/Ign;->g:LX/Igl;

    .line 2601650
    iput-object v1, v0, Lcom/facebook/messaging/media/mediapicker/PhotoItemController;->w:LX/Igl;

    .line 2601651
    :goto_0
    return-object v0

    .line 2601652
    :pswitch_1
    iget-object v0, p0, LX/Ign;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f0315a8

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2601653
    iget-object v1, p0, LX/Ign;->d:LX/Igw;

    .line 2601654
    new-instance p2, Lcom/facebook/messaging/media/mediapicker/VideoItemController;

    invoke-static {v1}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v2

    check-cast v2, LX/1Ad;

    invoke-static {v1}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v1}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object p1

    check-cast p1, LX/11S;

    invoke-direct {p2, v2, v3, p1, v0}, Lcom/facebook/messaging/media/mediapicker/VideoItemController;-><init>(LX/1Ad;Landroid/content/res/Resources;LX/11S;Landroid/view/View;)V

    .line 2601655
    move-object v0, p2

    .line 2601656
    iget-object v1, p0, LX/Ign;->h:LX/Igm;

    .line 2601657
    iput-object v1, v0, Lcom/facebook/messaging/media/mediapicker/VideoItemController;->r:LX/Igm;

    .line 2601658
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(JZ)V
    .locals 8

    .prologue
    .line 2601723
    if-eqz p3, :cond_1

    .line 2601724
    iget-object v0, p0, LX/Ign;->m:Ljava/util/List;

    .line 2601725
    iget-object v3, p0, LX/Ign;->l:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2601726
    iget-wide v5, v3, Lcom/facebook/ui/media/attachments/MediaResource;->h:J

    cmp-long v5, v5, p1

    if-nez v5, :cond_0

    iget-object v5, v3, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v6, LX/2MK;->PHOTO:LX/2MK;

    if-ne v5, v6, :cond_0

    .line 2601727
    :goto_0
    move-object v1, v3

    .line 2601728
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2601729
    :goto_1
    return-void

    .line 2601730
    :cond_1
    iget-object v3, p0, LX/Ign;->m:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 2601731
    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2601732
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2601733
    iget-object v5, v3, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v6, LX/2MK;->PHOTO:LX/2MK;

    if-ne v5, v6, :cond_2

    .line 2601734
    iget-wide v5, v3, Lcom/facebook/ui/media/attachments/MediaResource;->h:J

    cmp-long v3, v5, p1

    if-nez v3, :cond_2

    .line 2601735
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    .line 2601736
    iget-object v3, p0, LX/Ign;->k:LX/Igi;

    sget-object v4, LX/Igi;->SELECTED:LX/Igi;

    if-ne v3, v4, :cond_3

    .line 2601737
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 2601738
    :cond_3
    goto :goto_1

    :cond_4
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public final a(LX/1a1;I)V
    .locals 10

    .prologue
    .line 2601669
    invoke-static {p0}, LX/Ign;->d$redex0(LX/Ign;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2601670
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v1

    .line 2601671
    packed-switch v1, :pswitch_data_0

    .line 2601672
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected view type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2601673
    :pswitch_0
    check-cast p1, Lcom/facebook/messaging/media/mediapicker/PhotoItemController;

    .line 2601674
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 2601675
    iget-object v4, v0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v7, LX/2MK;->PHOTO:LX/2MK;

    if-ne v4, v7, :cond_5

    move v4, v5

    :goto_0
    invoke-static {v4}, LX/0PB;->checkArgument(Z)V

    .line 2601676
    iput-object v0, p1, Lcom/facebook/messaging/media/mediapicker/PhotoItemController;->x:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2601677
    iget-object v4, p1, Lcom/facebook/messaging/media/mediapicker/PhotoItemController;->o:LX/11S;

    sget-object v7, LX/1lB;->MONTH_DAY_YEAR_STYLE:LX/1lB;

    iget-wide v8, v0, Lcom/facebook/ui/media/attachments/MediaResource;->D:J

    invoke-interface {v4, v7, v8, v9}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v4

    .line 2601678
    iget-object v7, p1, Lcom/facebook/messaging/media/mediapicker/PhotoItemController;->n:Landroid/content/res/Resources;

    const v8, 0x7f082e05

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v4, v5, v6

    invoke-virtual {v7, v8, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 2601679
    iget-object v5, p1, Lcom/facebook/messaging/media/mediapicker/PhotoItemController;->t:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v5, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2601680
    iget-object v4, v0, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-static {v4}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v4

    iget-object v5, p1, Lcom/facebook/messaging/media/mediapicker/PhotoItemController;->s:LX/1o9;

    .line 2601681
    iput-object v5, v4, LX/1bX;->c:LX/1o9;

    .line 2601682
    move-object v4, v4

    .line 2601683
    invoke-virtual {v4}, LX/1bX;->n()LX/1bf;

    move-result-object v5

    .line 2601684
    iget-object v7, p1, Lcom/facebook/messaging/media/mediapicker/PhotoItemController;->t:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v4, p1, Lcom/facebook/messaging/media/mediapicker/PhotoItemController;->m:LX/1Ad;

    invoke-virtual {v4}, LX/1Ad;->o()LX/1Ad;

    move-result-object v4

    sget-object v8, Lcom/facebook/messaging/media/mediapicker/PhotoItemController;->l:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v4, v8}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v4

    iget-object v8, p1, Lcom/facebook/messaging/media/mediapicker/PhotoItemController;->p:LX/Igp;

    invoke-virtual {v4, v8}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v4

    check-cast v4, LX/1Ad;

    invoke-virtual {v4, v5}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v4

    check-cast v4, LX/1Ad;

    iget-object v5, p1, Lcom/facebook/messaging/media/mediapicker/PhotoItemController;->t:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v5}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v4

    check-cast v4, LX/1Ad;

    invoke-virtual {v4}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v4

    invoke-virtual {v7, v4}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2601685
    iget-object v4, p1, Lcom/facebook/messaging/media/mediapicker/PhotoItemController;->t:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v4, v6}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2601686
    iget-object v4, p1, Lcom/facebook/messaging/media/mediapicker/PhotoItemController;->v:Landroid/view/View;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 2601687
    iget-object v1, p0, LX/Ign;->j:LX/Igt;

    invoke-virtual {v1, v0}, LX/Igt;->c(Lcom/facebook/ui/media/attachments/MediaResource;)Z

    move-result v0

    .line 2601688
    iget-object v1, p1, Lcom/facebook/messaging/media/mediapicker/PhotoItemController;->u:Lcom/facebook/widget/PhotoToggleButton;

    invoke-virtual {v1}, Lcom/facebook/widget/PhotoToggleButton;->isChecked()Z

    move-result v1

    if-eq v1, v0, :cond_0

    .line 2601689
    iget-object v1, p1, Lcom/facebook/messaging/media/mediapicker/PhotoItemController;->u:Lcom/facebook/widget/PhotoToggleButton;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/PhotoToggleButton;->setChecked(Z)V

    .line 2601690
    :cond_0
    iget-object v0, p0, LX/Ign;->e:Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;

    .line 2601691
    iget-boolean v1, v0, Lcom/facebook/messaging/media/mediapicker/MediaPickerEnvironment;->b:Z

    move v0, v1

    .line 2601692
    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 2601693
    :goto_1
    iget-object v2, p1, Lcom/facebook/messaging/media/mediapicker/PhotoItemController;->u:Lcom/facebook/widget/PhotoToggleButton;

    if-eqz v0, :cond_6

    const/4 v1, 0x0

    :goto_2
    invoke-virtual {v2, v1}, Lcom/facebook/widget/PhotoToggleButton;->setVisibility(I)V

    .line 2601694
    :goto_3
    return-void

    .line 2601695
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 2601696
    :pswitch_1
    check-cast p1, Lcom/facebook/messaging/media/mediapicker/VideoItemController;

    .line 2601697
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 2601698
    iget-object v4, v0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v7, LX/2MK;->VIDEO:LX/2MK;

    if-ne v4, v7, :cond_7

    move v4, v5

    :goto_4
    invoke-static {v4}, LX/0PB;->checkArgument(Z)V

    .line 2601699
    iput-object v0, p1, Lcom/facebook/messaging/media/mediapicker/VideoItemController;->s:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2601700
    iget-object v4, p1, Lcom/facebook/messaging/media/mediapicker/VideoItemController;->p:Landroid/content/res/Resources;

    const v7, 0x7f0b1f00

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 2601701
    iget-object v7, p1, Lcom/facebook/messaging/media/mediapicker/VideoItemController;->u:LX/1o9;

    if-nez v7, :cond_2

    .line 2601702
    new-instance v7, LX/1o9;

    invoke-direct {v7, v4, v4}, LX/1o9;-><init>(II)V

    iput-object v7, p1, Lcom/facebook/messaging/media/mediapicker/VideoItemController;->u:LX/1o9;

    .line 2601703
    :cond_2
    iget-object v4, p1, Lcom/facebook/messaging/media/mediapicker/VideoItemController;->t:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-nez v4, :cond_3

    .line 2601704
    iget-object v4, p1, Lcom/facebook/messaging/media/mediapicker/VideoItemController;->w:Landroid/view/View;

    const v7, 0x7f0d1b29

    invoke-virtual {v4, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v4, p1, Lcom/facebook/messaging/media/mediapicker/VideoItemController;->t:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2601705
    iget-object v4, p1, Lcom/facebook/messaging/media/mediapicker/VideoItemController;->t:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v7, p1, Lcom/facebook/messaging/media/mediapicker/VideoItemController;->n:LX/Igv;

    invoke-virtual {v4, v7}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2601706
    iget-object v4, p1, Lcom/facebook/messaging/media/mediapicker/VideoItemController;->t:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-virtual {v4, v7}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 2601707
    :cond_3
    iget-object v4, p1, Lcom/facebook/messaging/media/mediapicker/VideoItemController;->v:Landroid/view/View;

    if-nez v4, :cond_4

    .line 2601708
    iget-object v4, p1, Lcom/facebook/messaging/media/mediapicker/VideoItemController;->w:Landroid/view/View;

    const v7, 0x7f0d1adc

    invoke-virtual {v4, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p1, Lcom/facebook/messaging/media/mediapicker/VideoItemController;->v:Landroid/view/View;

    .line 2601709
    :cond_4
    iget-object v4, p1, Lcom/facebook/messaging/media/mediapicker/VideoItemController;->v:Landroid/view/View;

    const/16 v7, 0x8

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 2601710
    iget-object v4, p1, Lcom/facebook/messaging/media/mediapicker/VideoItemController;->q:LX/11S;

    sget-object v7, LX/1lB;->MONTH_DAY_YEAR_STYLE:LX/1lB;

    iget-wide v8, v0, Lcom/facebook/ui/media/attachments/MediaResource;->D:J

    invoke-interface {v4, v7, v8, v9}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v4

    .line 2601711
    iget-object v7, p1, Lcom/facebook/messaging/media/mediapicker/VideoItemController;->p:Landroid/content/res/Resources;

    const v8, 0x7f082e06

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v4, v5, v6

    invoke-virtual {v7, v8, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 2601712
    iget-object v5, p1, Lcom/facebook/messaging/media/mediapicker/VideoItemController;->t:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v5, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2601713
    iget-object v4, v0, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-static {v4}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v4

    iget-object v5, p1, Lcom/facebook/messaging/media/mediapicker/VideoItemController;->u:LX/1o9;

    .line 2601714
    iput-object v5, v4, LX/1bX;->c:LX/1o9;

    .line 2601715
    move-object v4, v4

    .line 2601716
    invoke-virtual {v4}, LX/1bX;->n()LX/1bf;

    move-result-object v5

    .line 2601717
    iget-object v7, p1, Lcom/facebook/messaging/media/mediapicker/VideoItemController;->t:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v4, p1, Lcom/facebook/messaging/media/mediapicker/VideoItemController;->o:LX/1Ad;

    invoke-virtual {v4}, LX/1Ad;->o()LX/1Ad;

    move-result-object v4

    sget-object v8, Lcom/facebook/messaging/media/mediapicker/VideoItemController;->l:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v4, v8}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v4

    iget-object v8, p1, Lcom/facebook/messaging/media/mediapicker/VideoItemController;->m:LX/Igu;

    invoke-virtual {v4, v8}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v4

    check-cast v4, LX/1Ad;

    invoke-virtual {v4, v5}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v4

    check-cast v4, LX/1Ad;

    iget-object v5, p1, Lcom/facebook/messaging/media/mediapicker/VideoItemController;->t:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v5}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v4

    check-cast v4, LX/1Ad;

    invoke-virtual {v4}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v4

    invoke-virtual {v7, v4}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2601718
    iget-object v4, p1, Lcom/facebook/messaging/media/mediapicker/VideoItemController;->t:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v4, v6}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2601719
    goto/16 :goto_3

    :cond_5
    move v4, v6

    .line 2601720
    goto/16 :goto_0

    .line 2601721
    :cond_6
    const/16 v1, 0x8

    goto/16 :goto_2

    :cond_7
    move v4, v6

    .line 2601722
    goto/16 :goto_4

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(LX/Igi;)V
    .locals 1

    .prologue
    .line 2601665
    iget-object v0, p0, LX/Ign;->k:LX/Igi;

    if-eq v0, p1, :cond_0

    .line 2601666
    iput-object p1, p0, LX/Ign;->k:LX/Igi;

    .line 2601667
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 2601668
    :cond_0
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 4

    .prologue
    .line 2601660
    invoke-static {p0}, LX/Ign;->d$redex0(LX/Ign;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v0, v0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    .line 2601661
    sget-object v1, LX/Igk;->a:[I

    invoke-virtual {v0}, LX/2MK;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2601662
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected media resource type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2601663
    :pswitch_0
    const/4 v0, 0x0

    .line 2601664
    :goto_0
    return v0

    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2601659
    invoke-static {p0}, LX/Ign;->d$redex0(LX/Ign;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
