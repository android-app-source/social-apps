.class public final LX/Iki;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/payments/invoice/protocol/graphql/InvoiceMutationsModels$PaymentInvoiceMutationModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;)V
    .locals 0

    .prologue
    .line 2606965
    iput-object p1, p0, LX/Iki;->a:Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2606966
    iget-object v0, p0, LX/Iki;->a:Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;

    const/4 v1, 0x0

    .line 2606967
    invoke-static {v0}, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->k(Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;)V

    .line 2606968
    iget-object v2, v0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->c:LX/03V;

    const-string v3, "InvoicesSummaryPresenter_invoiceMutationFailure"

    new-instance p0, Ljava/lang/StringBuilder;

    const-string p1, "invoiceID: "

    invoke-direct {p0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object p1, v0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->l:Ljava/lang/String;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, v3, p0, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2606969
    iget-object v2, v0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->g:LX/IkZ;

    .line 2606970
    iget-object v3, v2, LX/IkZ;->a:Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;

    const-string p0, "payflows_fail"

    invoke-static {v3, p0}, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->a$redex0(Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;Ljava/lang/String;)V

    .line 2606971
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2606972
    iget-object v0, p0, LX/Iki;->a:Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;

    .line 2606973
    invoke-static {v0}, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->k(Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;)V

    .line 2606974
    iget-object p0, v0, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentPresenter;->g:LX/IkZ;

    .line 2606975
    iget-object p1, p0, LX/IkZ;->a:Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;

    const-string v0, "payflows_success"

    invoke-static {p1, v0}, Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;->a$redex0(Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;Ljava/lang/String;)V

    .line 2606976
    iget-object p1, p0, LX/IkZ;->a:Lcom/facebook/messaging/payment/prefs/receipts/manual/InvoicesProofOfPaymentFragment;

    .line 2606977
    invoke-virtual {p1}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    const/4 p0, -0x1

    invoke-virtual {v0, p0}, Landroid/app/Activity;->setResult(I)V

    .line 2606978
    invoke-virtual {p1}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2606979
    return-void
.end method
