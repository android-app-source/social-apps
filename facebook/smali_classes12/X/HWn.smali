.class public final LX/HWn;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public final synthetic b:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;Lcom/facebook/graphql/enums/GraphQLPageActionType;)V
    .locals 0

    .prologue
    .line 2477058
    iput-object p1, p0, LX/HWn;->b:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iput-object p2, p0, LX/HWn;->a:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2477059
    iget-object v0, p0, LX/HWn;->b:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->k:LX/CXj;

    new-instance v1, LX/CXw;

    iget-object v2, p0, LX/HWn;->b:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v2, v2, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2477060
    iget-object v3, v2, LX/HX9;->c:Landroid/os/ParcelUuid;

    move-object v2, v3

    .line 2477061
    iget-object v3, p0, LX/HWn;->a:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-direct {v1, v2, v3, v4, v4}, LX/CXw;-><init>(Landroid/os/ParcelUuid;Lcom/facebook/graphql/enums/GraphQLPageActionType;Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;LX/0ta;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2477062
    iget-object v0, p0, LX/HWn;->b:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->E:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    iget-object v1, p0, LX/HWn;->b:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "fail to get additional tab data for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/HWn;->a:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2477063
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2477064
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2477065
    iget-object v0, p0, LX/HWn;->b:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v1, v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->k:LX/CXj;

    new-instance v2, LX/CXw;

    iget-object v0, p0, LX/HWn;->b:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2477066
    iget-object v3, v0, LX/HX9;->c:Landroid/os/ParcelUuid;

    move-object v3, v3

    .line 2477067
    iget-object v4, p0, LX/HWn;->a:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 2477068
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2477069
    check-cast v0, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;

    .line 2477070
    iget-object v5, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v5, v5

    .line 2477071
    invoke-direct {v2, v3, v4, v0, v5}, LX/CXw;-><init>(Landroid/os/ParcelUuid;Lcom/facebook/graphql/enums/GraphQLPageActionType;Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;LX/0ta;)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 2477072
    return-void
.end method
