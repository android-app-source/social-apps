.class public LX/I2B;
.super LX/A8X;
.source ""


# static fields
.field private static final a:Ljava/lang/Object;

.field private static final b:Ljava/lang/Object;

.field private static final c:Ljava/lang/Object;


# instance fields
.field public final d:Landroid/content/Context;

.field public final e:Lcom/facebook/events/common/EventAnalyticsParams;

.field private final f:LX/I21;

.field public final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Lcom/facebook/content/SecureContextHelper;

.field public final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;",
            ">;"
        }
    .end annotation
.end field

.field public j:Z

.field private k:LX/4yY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/4yY",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private l:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2529928
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/I2B;->a:Ljava/lang/Object;

    .line 2529929
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/I2B;->b:Ljava/lang/Object;

    .line 2529930
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/I2B;->c:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/events/common/EventAnalyticsParams;Landroid/content/Context;LX/I21;LX/0Or;Lcom/facebook/content/SecureContextHelper;)V
    .locals 1
    .param p1    # Lcom/facebook/events/common/EventAnalyticsParams;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/events/common/EventAnalyticsParams;",
            "Landroid/content/Context;",
            "LX/I21;",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;",
            "Lcom/facebook/content/SecureContextHelper;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2529988
    invoke-direct {p0}, LX/A8X;-><init>()V

    .line 2529989
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/I2B;->i:Ljava/util/List;

    .line 2529990
    iput-object p2, p0, LX/I2B;->d:Landroid/content/Context;

    .line 2529991
    iput-object p1, p0, LX/I2B;->e:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2529992
    iput-object p3, p0, LX/I2B;->f:LX/I21;

    .line 2529993
    iput-object p4, p0, LX/I2B;->g:LX/0Or;

    .line 2529994
    iput-object p5, p0, LX/I2B;->h:Lcom/facebook/content/SecureContextHelper;

    .line 2529995
    invoke-static {p0}, LX/I2B;->d(LX/I2B;)V

    .line 2529996
    return-void
.end method

.method public static d(LX/I2B;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2529973
    iput v2, p0, LX/I2B;->l:I

    .line 2529974
    new-instance v0, LX/4yW;

    invoke-direct {v0}, LX/4yW;-><init>()V

    .line 2529975
    iget-object v1, p0, LX/I2B;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2529976
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, LX/50M;->b(Ljava/lang/Comparable;Ljava/lang/Comparable;)LX/50M;

    move-result-object v1

    const v2, 0x7f0d01aa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4yW;->a(LX/50M;Ljava/lang/Object;)LX/4yW;

    .line 2529977
    iget v1, p0, LX/I2B;->l:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/I2B;->l:I

    .line 2529978
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, LX/50M;->b(Ljava/lang/Comparable;Ljava/lang/Comparable;)LX/50M;

    move-result-object v1

    const v2, 0x7f0d01ab

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4yW;->a(LX/50M;Ljava/lang/Object;)LX/4yW;

    .line 2529979
    iget v1, p0, LX/I2B;->l:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/I2B;->l:I

    .line 2529980
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, LX/I2B;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, LX/50M;->b(Ljava/lang/Comparable;Ljava/lang/Comparable;)LX/50M;

    move-result-object v1

    const v2, 0x7f0d01ad

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4yW;->a(LX/50M;Ljava/lang/Object;)LX/4yW;

    .line 2529981
    iget v1, p0, LX/I2B;->l:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/I2B;->l:I

    .line 2529982
    iget-object v1, p0, LX/I2B;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    .line 2529983
    iget-boolean v2, p0, LX/I2B;->j:Z

    if-eqz v2, :cond_0

    .line 2529984
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v2, v1}, LX/50M;->b(Ljava/lang/Comparable;Ljava/lang/Comparable;)LX/50M;

    move-result-object v1

    const v2, 0x7f0d01ae

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/4yW;->a(LX/50M;Ljava/lang/Object;)LX/4yW;

    .line 2529985
    iget v1, p0, LX/I2B;->l:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/I2B;->l:I

    .line 2529986
    :cond_0
    invoke-virtual {v0}, LX/4yW;->a()LX/4yZ;

    move-result-object v0

    iput-object v0, p0, LX/I2B;->k:LX/4yY;

    .line 2529987
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2529957
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    .line 2529958
    const v1, 0x7f0d01ab

    if-ne v0, v1, :cond_1

    .line 2529959
    check-cast p1, Lcom/facebook/fig/header/FigHeader;

    .line 2529960
    iget-object v0, p0, LX/I2B;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08219c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/fig/header/FigHeader;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2529961
    :cond_0
    :goto_0
    return-void

    .line 2529962
    :cond_1
    const v1, 0x7f0d01ad

    if-ne v0, v1, :cond_2

    .line 2529963
    invoke-virtual {p0, p2}, LX/I2B;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;

    .line 2529964
    check-cast p1, Lcom/facebook/fig/listitem/FigListItem;

    .line 2529965
    iget-object v1, p0, LX/I2B;->f:LX/I21;

    iget-object v2, p0, LX/I2B;->e:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-virtual {v1, p1, v0, v2}, LX/I21;->a(Lcom/facebook/fig/listitem/FigListItem;Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;Lcom/facebook/events/common/EventAnalyticsParams;)V

    goto :goto_0

    .line 2529966
    :cond_2
    const v1, 0x7f0d01ae

    if-ne v0, v1, :cond_0

    .line 2529967
    check-cast p1, Lcom/facebook/fig/footer/FigFooter;

    .line 2529968
    iget-object v0, p0, LX/I2B;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0821df

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/fig/footer/FigFooter;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2529969
    invoke-virtual {p1, v2}, Lcom/facebook/fig/footer/FigFooter;->setTopDivider(Z)V

    .line 2529970
    invoke-virtual {p1, v2}, Lcom/facebook/fig/footer/FigFooter;->setFooterType(I)V

    .line 2529971
    new-instance v0, LX/I2A;

    invoke-direct {v0, p0}, LX/I2A;-><init>(LX/I2B;)V

    move-object v0, v0

    .line 2529972
    invoke-virtual {p1, v0}, Lcom/facebook/fig/footer/FigFooter;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public final d(Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 5

    .prologue
    .line 2529945
    const v0, 0x7f0d01aa

    if-ne p2, v0, :cond_0

    .line 2529946
    new-instance v0, Landroid/view/View;

    iget-object v1, p0, LX/I2B;->d:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2529947
    iget-object v1, p0, LX/I2B;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0168

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 2529948
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x2

    iget-object v3, p0, LX/I2B;->d:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b15dd

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2529949
    :goto_0
    return-object v0

    .line 2529950
    :cond_0
    const v0, 0x7f0d01ab

    if-ne p2, v0, :cond_1

    .line 2529951
    new-instance v0, Lcom/facebook/fig/header/FigHeader;

    iget-object v1, p0, LX/I2B;->d:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/facebook/fig/header/FigHeader;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 2529952
    :cond_1
    const v0, 0x7f0d01ad

    if-ne p2, v0, :cond_2

    .line 2529953
    new-instance v0, Lcom/facebook/fig/listitem/FigListItem;

    iget-object v1, p0, LX/I2B;->d:Landroid/content/Context;

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/facebook/fig/listitem/FigListItem;-><init>(Landroid/content/Context;I)V

    goto :goto_0

    .line 2529954
    :cond_2
    const v0, 0x7f0d01ae

    if-ne p2, v0, :cond_3

    .line 2529955
    new-instance v0, Lcom/facebook/fig/footer/FigFooter;

    iget-object v1, p0, LX/I2B;->d:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/facebook/fig/footer/FigFooter;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 2529956
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown View Type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getCount()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2529944
    iget-object v0, p0, LX/I2B;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    iget-object v0, p0, LX/I2B;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x2

    :goto_0
    add-int/2addr v0, v2

    iget-boolean v2, p0, LX/I2B;->j:Z

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2529933
    invoke-virtual {p0, p1}, LX/1OM;->getItemViewType(I)I

    move-result v0

    .line 2529934
    const v1, 0x7f0d01aa

    if-ne v0, v1, :cond_0

    .line 2529935
    sget-object v0, LX/I2B;->c:Ljava/lang/Object;

    .line 2529936
    :goto_0
    return-object v0

    .line 2529937
    :cond_0
    const v1, 0x7f0d01ab

    if-ne v0, v1, :cond_1

    .line 2529938
    sget-object v0, LX/I2B;->b:Ljava/lang/Object;

    goto :goto_0

    .line 2529939
    :cond_1
    const v1, 0x7f0d01ad

    if-ne v0, v1, :cond_2

    .line 2529940
    iget-object v0, p0, LX/I2B;->i:Ljava/util/List;

    add-int/lit8 v1, p1, -0x2

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 2529941
    :cond_2
    const v1, 0x7f0d01ae

    if-ne v0, v1, :cond_3

    .line 2529942
    sget-object v0, LX/I2B;->a:Ljava/lang/Object;

    goto :goto_0

    .line 2529943
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown View Type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 2529932
    iget-object v0, p0, LX/I2B;->k:LX/4yY;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, LX/4yY;->a(Ljava/lang/Comparable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2529931
    iget v0, p0, LX/I2B;->l:I

    return v0
.end method
