.class public LX/Ig3;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static a:I

.field private static final e:Ljava/lang/Object;


# instance fields
.field public b:LX/0W3;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/Ig4;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/Ifw;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2600518
    const v0, 0xea60

    sput v0, LX/Ig3;->a:I

    .line 2600519
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Ig3;->e:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2600517
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/Ig3;
    .locals 9

    .prologue
    .line 2600486
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2600487
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2600488
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2600489
    if-nez v1, :cond_0

    .line 2600490
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2600491
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2600492
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2600493
    sget-object v1, LX/Ig3;->e:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2600494
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2600495
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2600496
    :cond_1
    if-nez v1, :cond_4

    .line 2600497
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2600498
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2600499
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2600500
    new-instance p0, LX/Ig3;

    invoke-direct {p0}, LX/Ig3;-><init>()V

    .line 2600501
    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v1

    check-cast v1, LX/0W3;

    invoke-static {v0}, LX/Ig4;->b(LX/0QB;)LX/Ig4;

    move-result-object v7

    check-cast v7, LX/Ig4;

    invoke-static {v0}, LX/Ifw;->a(LX/0QB;)LX/Ifw;

    move-result-object v8

    check-cast v8, LX/Ifw;

    .line 2600502
    iput-object v1, p0, LX/Ig3;->b:LX/0W3;

    iput-object v7, p0, LX/Ig3;->c:LX/Ig4;

    iput-object v8, p0, LX/Ig3;->d:LX/Ifw;

    .line 2600503
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2600504
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2600505
    if-nez v1, :cond_2

    .line 2600506
    sget-object v0, LX/Ig3;->e:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ig3;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2600507
    :goto_1
    if-eqz v0, :cond_3

    .line 2600508
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2600509
    :goto_3
    check-cast v0, LX/Ig3;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2600510
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2600511
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2600512
    :catchall_1
    move-exception v0

    .line 2600513
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2600514
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2600515
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2600516
    :cond_2
    :try_start_8
    sget-object v0, LX/Ig3;->e:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ig3;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method
