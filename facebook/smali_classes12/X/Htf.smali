.class public LX/Htf;
.super LX/Hs6;
.source ""

# interfaces
.implements LX/0iK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0io;",
        "DerivedData::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesIsPhotoSupported;",
        "Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;>",
        "LX/Hs6;",
        "LX/0iK",
        "<TModelData;TDerivedData;>;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field private final c:LX/Hr2;

.field private final d:LX/0ad;

.field private final e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:LX/Hu0;


# direct methods
.method public constructor <init>(LX/0il;LX/Hr2;Landroid/content/res/Resources;LX/0ad;)V
    .locals 2
    .param p1    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/Hr2;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;",
            "LX/Hr2;",
            "Landroid/content/res/Resources;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2516535
    invoke-direct {p0}, LX/Hs6;-><init>()V

    .line 2516536
    const-string v0, ""

    iput-object v0, p0, LX/Htf;->f:Ljava/lang/String;

    .line 2516537
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/Htf;->b:Ljava/lang/ref/WeakReference;

    .line 2516538
    iput-object p2, p0, LX/Htf;->c:LX/Hr2;

    .line 2516539
    iput-object p3, p0, LX/Htf;->a:Landroid/content/res/Resources;

    .line 2516540
    iput-object p4, p0, LX/Htf;->d:LX/0ad;

    .line 2516541
    const v0, 0x7f0812b9

    invoke-virtual {p3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Htf;->e:Ljava/lang/String;

    .line 2516542
    iget-object v0, p0, LX/Htf;->e:Ljava/lang/String;

    invoke-direct {p0, v0}, LX/Htf;->a(Ljava/lang/String;)V

    .line 2516543
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2516544
    iget-object v0, p0, LX/Htf;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2516545
    :goto_0
    return-void

    .line 2516546
    :cond_0
    invoke-static {}, LX/Hu0;->newBuilder()LX/Htz;

    move-result-object v0

    const v1, 0x7f0207b3

    .line 2516547
    iput v1, v0, LX/Htz;->a:I

    .line 2516548
    move-object v0, v0

    .line 2516549
    const v1, 0x7f0a04c1

    .line 2516550
    iput v1, v0, LX/Htz;->f:I

    .line 2516551
    move-object v0, v0

    .line 2516552
    iput-object p1, v0, LX/Htz;->b:Ljava/lang/String;

    .line 2516553
    move-object v0, v0

    .line 2516554
    invoke-virtual {p0}, LX/Htf;->g()LX/Hty;

    move-result-object v1

    invoke-virtual {v1}, LX/Hty;->getAnalyticsName()Ljava/lang/String;

    move-result-object v1

    .line 2516555
    iput-object v1, v0, LX/Htz;->d:Ljava/lang/String;

    .line 2516556
    move-object v0, v0

    .line 2516557
    iget-object v1, p0, LX/Htf;->c:LX/Hr2;

    .line 2516558
    iput-object v1, v0, LX/Htz;->e:LX/Hr2;

    .line 2516559
    move-object v0, v0

    .line 2516560
    invoke-virtual {v0}, LX/Htz;->a()LX/Hu0;

    move-result-object v0

    iput-object v0, p0, LX/Htf;->g:LX/Hu0;

    .line 2516561
    iput-object p1, p0, LX/Htf;->f:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/5L2;)V
    .locals 0

    .prologue
    .line 2516568
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2516562
    iget-object v0, p0, LX/Htf;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    .line 2516563
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2516564
    iget-object v0, p0, LX/Htf;->d:LX/0ad;

    sget-char v1, LX/1EB;->aq:C

    iget-object v2, p0, LX/Htf;->e:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2516565
    invoke-direct {p0, v0}, LX/Htf;->a(Ljava/lang/String;)V

    .line 2516566
    :goto_0
    return-void

    .line 2516567
    :cond_0
    iget-object v0, p0, LX/Htf;->e:Ljava/lang/String;

    invoke-direct {p0, v0}, LX/Htf;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2516527
    const/4 v0, 0x1

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2516534
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2516533
    iget-object v0, p0, LX/Htf;->a:Landroid/content/res/Resources;

    const v1, 0x7f0812b2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/Hu0;
    .locals 1

    .prologue
    .line 2516532
    iget-object v0, p0, LX/Htf;->g:LX/Hu0;

    return-object v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 2516531
    iget-object v0, p0, LX/Htf;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    check-cast v0, LX/0ik;

    invoke-interface {v0}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2zG;

    invoke-virtual {v0}, LX/2zG;->t()Z

    move-result v0

    return v0
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 2516529
    iget-object v0, p0, LX/Htf;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    .line 2516530
    invoke-static {v0}, LX/7kq;->l(LX/0Px;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, LX/7kq;->j(LX/0Px;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()LX/Hty;
    .locals 1

    .prologue
    .line 2516528
    sget-object v0, LX/Hty;->PHOTO_GALLERY:LX/Hty;

    return-object v0
.end method
