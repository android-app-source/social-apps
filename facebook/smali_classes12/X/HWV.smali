.class public final LX/HWV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:LX/HWW;


# direct methods
.method public constructor <init>(LX/HWW;)V
    .locals 0

    .prologue
    .line 2476418
    iput-object p1, p0, LX/HWV;->a:LX/HWW;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 5

    .prologue
    .line 2476399
    iget-object v0, p0, LX/HWV;->a:LX/HWW;

    iget-object v0, v0, LX/HWW;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-nez v0, :cond_0

    .line 2476400
    :goto_0
    return-void

    .line 2476401
    :cond_0
    iget-object v0, p0, LX/HWV;->a:LX/HWW;

    iget-object v0, v0, LX/HWW;->i:Landroid/util/SparseArray;

    iget-object v1, p0, LX/HWV;->a:LX/HWW;

    iget v1, v1, LX/HWW;->j:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2476402
    if-nez v0, :cond_1

    .line 2476403
    iget-object v0, p0, LX/HWV;->a:LX/HWW;

    iget-object v0, v0, LX/HWW;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    iget-object v1, p0, LX/HWV;->a:LX/HWW;

    iget-object v1, v1, LX/HWW;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    iget-object v1, p0, LX/HWV;->a:LX/HWW;

    iget-object v1, v1, LX/HWW;->b:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getCompoundPaddingLeft()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    iget-object v1, p0, LX/HWV;->a:LX/HWW;

    iget-object v1, v1, LX/HWW;->b:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getCompoundPaddingRight()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    iget-object v1, p0, LX/HWV;->a:LX/HWW;

    iget-object v1, v1, LX/HWW;->c:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    iget-object v2, p0, LX/HWV;->a:LX/HWW;

    iget-object v2, v2, LX/HWW;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v1

    add-float/2addr v0, v1

    iget-object v1, p0, LX/HWV;->a:LX/HWW;

    iget-object v1, v1, LX/HWW;->c:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->getCompoundPaddingRight()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    iget-object v1, p0, LX/HWV;->a:LX/HWW;

    iget-object v1, v1, LX/HWW;->c:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->getCompoundPaddingLeft()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    float-to-int v0, v0

    .line 2476404
    iget-object v1, p0, LX/HWV;->a:LX/HWW;

    iget-object v1, v1, LX/HWW;->h:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    if-le v0, v1, :cond_3

    .line 2476405
    iget-object v0, p0, LX/HWV;->a:LX/HWW;

    iget-object v0, v0, LX/HWW;->i:Landroid/util/SparseArray;

    iget-object v1, p0, LX/HWV;->a:LX/HWW;

    iget v1, v1, LX/HWW;->j:I

    iget-object v2, p0, LX/HWV;->a:LX/HWW;

    iget-object v2, v2, LX/HWW;->d:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2476406
    iget-object v0, p0, LX/HWV;->a:LX/HWW;

    iget-object v0, v0, LX/HWW;->d:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 2476407
    :cond_1
    :goto_1
    iget-object v1, p0, LX/HWV;->a:LX/HWW;

    .line 2476408
    iget-object v2, v1, LX/HWW;->e:Landroid/widget/TextView;

    if-eq v2, v0, :cond_2

    .line 2476409
    iget-object v2, v1, LX/HWW;->e:Landroid/widget/TextView;

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2476410
    iget-object v2, v1, LX/HWW;->e:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2476411
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2476412
    iget-object v2, v1, LX/HWW;->a:LX/1Uf;

    iget-object v3, v1, LX/HWW;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-static {v3}, LX/1eD;->c(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/1eE;

    move-result-object v3

    const/4 v4, 0x1

    const/4 p0, 0x0

    invoke-virtual {v2, v3, v4, p0}, LX/1Uf;->a(LX/1eE;ZLX/0lF;)Landroid/text/Spannable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2476413
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2476414
    iput-object v0, v1, LX/HWW;->e:Landroid/widget/TextView;

    .line 2476415
    :cond_2
    goto/16 :goto_0

    .line 2476416
    :cond_3
    iget-object v0, p0, LX/HWV;->a:LX/HWW;

    iget-object v0, v0, LX/HWW;->i:Landroid/util/SparseArray;

    iget-object v1, p0, LX/HWV;->a:LX/HWW;

    iget v1, v1, LX/HWW;->j:I

    iget-object v2, p0, LX/HWV;->a:LX/HWW;

    iget-object v2, v2, LX/HWW;->c:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2476417
    iget-object v0, p0, LX/HWV;->a:LX/HWW;

    iget-object v0, v0, LX/HWW;->c:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    goto :goto_1
.end method
