.class public final enum LX/Iqf;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Iqf;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Iqf;

.field public static final enum IMAGE_CHANGE:LX/Iqf;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2617116
    new-instance v0, LX/Iqf;

    const-string v1, "IMAGE_CHANGE"

    invoke-direct {v0, v1, v2}, LX/Iqf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Iqf;->IMAGE_CHANGE:LX/Iqf;

    .line 2617117
    const/4 v0, 0x1

    new-array v0, v0, [LX/Iqf;

    sget-object v1, LX/Iqf;->IMAGE_CHANGE:LX/Iqf;

    aput-object v1, v0, v2

    sput-object v0, LX/Iqf;->$VALUES:[LX/Iqf;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2617118
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Iqf;
    .locals 1

    .prologue
    .line 2617119
    const-class v0, LX/Iqf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Iqf;

    return-object v0
.end method

.method public static values()[LX/Iqf;
    .locals 1

    .prologue
    .line 2617120
    sget-object v0, LX/Iqf;->$VALUES:[LX/Iqf;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Iqf;

    return-object v0
.end method
