.class public LX/IE7;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/res/Resources;

.field public final b:Ljava/lang/String;

.field public final c:LX/IDs;

.field private final d:LX/23P;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;LX/DHs;LX/DHr;LX/IDt;LX/23P;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p2    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/DHs;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/DHr;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2551538
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2551539
    iput-object p1, p0, LX/IE7;->b:Ljava/lang/String;

    .line 2551540
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, LX/IE7;->a:Landroid/content/res/Resources;

    .line 2551541
    invoke-virtual {p5, p2, p3, p4}, LX/IDt;->a(Landroid/content/Context;LX/DHs;LX/DHr;)LX/IDs;

    move-result-object v0

    iput-object v0, p0, LX/IE7;->c:LX/IDs;

    .line 2551542
    iput-object p6, p0, LX/IE7;->d:LX/23P;

    .line 2551543
    return-void
.end method

.method public static a(LX/IE7;I)Ljava/lang/CharSequence;
    .locals 3
    .param p0    # LX/IE7;
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 2551544
    iget-object v0, p0, LX/IE7;->d:LX/23P;

    iget-object v1, p0, LX/IE7;->a:Landroid/content/res/Resources;

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/IE9;LX/IDH;)V
    .locals 11

    .prologue
    .line 2551545
    invoke-interface {p2}, LX/2lq;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 2551546
    invoke-interface {p2}, LX/2lr;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2551547
    sget-object v0, LX/IE6;->a:[I

    invoke-interface {p2}, LX/2lq;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2551548
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, LX/IE9;->setShowButtonContainer(Z)V

    .line 2551549
    :goto_0
    invoke-interface {p2}, LX/2lq;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v2

    .line 2551550
    invoke-interface {p2}, LX/83X;->c()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v3

    .line 2551551
    const-string v4, ""

    invoke-virtual {p1, v4}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2551552
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v2, v4, :cond_0

    .line 2551553
    const v2, 0x7f080f85

    invoke-virtual {p1, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(I)V

    .line 2551554
    :goto_1
    iget v0, p2, LX/IDH;->j:I

    move v0, v0

    .line 2551555
    if-nez v0, :cond_9

    .line 2551556
    const-string v0, ""

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setMetaText(Ljava/lang/CharSequence;)V

    .line 2551557
    :goto_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p2}, LX/2lr;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->getSubtitleText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/IE9;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2551558
    return-void

    .line 2551559
    :pswitch_0
    const v0, 0x7f0101e9

    invoke-virtual {p1, v0}, LX/IE9;->setActionButtonStyle(I)V

    .line 2551560
    const v0, 0x7f080f7b

    invoke-static {p0, v0}, LX/IE7;->a(LX/IE7;I)Ljava/lang/CharSequence;

    move-result-object v0

    const v1, 0x7f080f7c

    invoke-static {p0, v1}, LX/IE7;->a(LX/IE7;I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/IE9;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 2551561
    iget-object v0, p0, LX/IE7;->a:Landroid/content/res/Resources;

    const v1, 0x7f080f9c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/IE9;->setActionButtonContentDescription(Ljava/lang/CharSequence;)V

    .line 2551562
    :goto_3
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/IE9;->setShowButtonContainer(Z)V

    .line 2551563
    new-instance v0, LX/IE4;

    invoke-direct {v0, p0, p2, p1}, LX/IE4;-><init>(LX/IE7;LX/IDH;LX/IE9;)V

    .line 2551564
    iget-object v1, p1, LX/IE9;->k:Lcom/facebook/friends/ui/SmartButtonLite;

    invoke-virtual {v1, v0}, Lcom/facebook/friends/ui/SmartButtonLite;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2551565
    new-instance v0, LX/IE5;

    invoke-direct {v0, p0, p2, p1}, LX/IE5;-><init>(LX/IE7;LX/IDH;LX/IE9;)V

    .line 2551566
    iget-object v1, p1, LX/IE9;->l:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2551567
    goto :goto_0

    .line 2551568
    :pswitch_1
    const v0, 0x7f0101e9

    invoke-virtual {p1, v0}, LX/IE9;->setActionButtonStyle(I)V

    .line 2551569
    const v0, 0x7f080f7b

    invoke-static {p0, v0}, LX/IE7;->a(LX/IE7;I)Ljava/lang/CharSequence;

    move-result-object v0

    const v1, 0x7f080f7c

    invoke-static {p0, v1}, LX/IE7;->a(LX/IE7;I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/IE9;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 2551570
    iget-object v0, p0, LX/IE7;->a:Landroid/content/res/Resources;

    const v1, 0x7f080f9f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/IE9;->setActionButtonContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 2551571
    :pswitch_2
    const v0, 0x7f0101e3

    invoke-virtual {p1, v0}, LX/IE9;->setActionButtonStyle(I)V

    .line 2551572
    const v0, 0x7f080017

    invoke-static {p0, v0}, LX/IE7;->a(LX/IE7;I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/IE9;->setActionButtonText(Ljava/lang/CharSequence;)V

    .line 2551573
    iget-object v0, p0, LX/IE7;->a:Landroid/content/res/Resources;

    const v1, 0x7f080f9e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/IE9;->setActionButtonContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 2551574
    :pswitch_3
    const v0, 0x7f0101e3

    invoke-virtual {p1, v0}, LX/IE9;->setActionButtonStyle(I)V

    .line 2551575
    const v0, 0x7f080f76

    invoke-static {p0, v0}, LX/IE7;->a(LX/IE7;I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/IE9;->setActionButtonText(Ljava/lang/CharSequence;)V

    .line 2551576
    iget-object v0, p0, LX/IE7;->a:Landroid/content/res/Resources;

    const v1, 0x7f080f9d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/IE9;->setActionButtonContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 2551577
    :cond_0
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v2, v4, :cond_1

    if-eq v2, v3, :cond_1

    .line 2551578
    const v2, 0x7f080f8a

    invoke-virtual {p1, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(I)V

    goto/16 :goto_1

    .line 2551579
    :cond_1
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v2, v4, :cond_5

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v3, v4, :cond_5

    const/4 v4, 0x1

    :goto_4
    move v4, v4

    .line 2551580
    if-eqz v4, :cond_2

    .line 2551581
    const v2, 0x7f080f86

    invoke-virtual {p1, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(I)V

    goto/16 :goto_1

    .line 2551582
    :cond_2
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v2, v4, :cond_6

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v3, v4, :cond_6

    const/4 v4, 0x1

    :goto_5
    move v2, v4

    .line 2551583
    if-eqz v2, :cond_3

    .line 2551584
    const v2, 0x7f080fa2

    invoke-virtual {p1, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(I)V

    goto/16 :goto_1

    .line 2551585
    :cond_3
    invoke-interface {p2}, LX/2ls;->h()Ljava/lang/String;

    move-result-object v2

    .line 2551586
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 2551587
    invoke-virtual {p1, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 2551588
    :cond_4
    iget-object v5, p0, LX/IE7;->b:Ljava/lang/String;

    invoke-interface {p2}, LX/2lr;->a()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 2551589
    const-string v5, ""

    .line 2551590
    :goto_6
    move-object v2, v5

    .line 2551591
    invoke-virtual {p1, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_5
    const/4 v4, 0x0

    goto :goto_4

    :cond_6
    const/4 v4, 0x0

    goto :goto_5

    .line 2551592
    :cond_7
    invoke-interface {p2}, LX/2lq;->e()I

    move-result v5

    .line 2551593
    if-lez v5, :cond_8

    iget-object v6, p0, LX/IE7;->a:Landroid/content/res/Resources;

    const v7, 0x7f0f005c

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v6, v7, v5, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto :goto_6

    :cond_8
    const-string v5, ""

    goto :goto_6

    .line 2551594
    :cond_9
    invoke-virtual {p1}, LX/IE9;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f00f4

    .line 2551595
    iget v2, p2, LX/IDH;->j:I

    move v2, v2

    .line 2551596
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 2551597
    iget v5, p2, LX/IDH;->j:I

    move v5, v5

    .line 2551598
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2551599
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setMetaText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
