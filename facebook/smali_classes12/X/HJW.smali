.class public final LX/HJW;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/HJX;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

.field public b:LX/2km;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/HJX;


# direct methods
.method public constructor <init>(LX/HJX;)V
    .locals 1

    .prologue
    .line 2452827
    iput-object p1, p0, LX/HJW;->c:LX/HJX;

    .line 2452828
    move-object v0, p1

    .line 2452829
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2452830
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2452831
    const-string v0, "PageInfoRowUnitComponentComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2452832
    if-ne p0, p1, :cond_1

    .line 2452833
    :cond_0
    :goto_0
    return v0

    .line 2452834
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2452835
    goto :goto_0

    .line 2452836
    :cond_3
    check-cast p1, LX/HJW;

    .line 2452837
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2452838
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2452839
    if-eq v2, v3, :cond_0

    .line 2452840
    iget-object v2, p0, LX/HJW;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/HJW;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v3, p1, LX/HJW;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2452841
    goto :goto_0

    .line 2452842
    :cond_5
    iget-object v2, p1, LX/HJW;->a:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    if-nez v2, :cond_4

    .line 2452843
    :cond_6
    iget-object v2, p0, LX/HJW;->b:LX/2km;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/HJW;->b:LX/2km;

    iget-object v3, p1, LX/HJW;->b:LX/2km;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2452844
    goto :goto_0

    .line 2452845
    :cond_7
    iget-object v2, p1, LX/HJW;->b:LX/2km;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
