.class public LX/JOV;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pk;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/1vb;


# direct methods
.method public constructor <init>(LX/1vb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2687674
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2687675
    iput-object p1, p0, LX/JOV;->a:LX/1vb;

    .line 2687676
    return-void
.end method

.method public static a(LX/0QB;)LX/JOV;
    .locals 4

    .prologue
    .line 2687677
    const-class v1, LX/JOV;

    monitor-enter v1

    .line 2687678
    :try_start_0
    sget-object v0, LX/JOV;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2687679
    sput-object v2, LX/JOV;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2687680
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2687681
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2687682
    new-instance p0, LX/JOV;

    invoke-static {v0}, LX/1vb;->a(LX/0QB;)LX/1vb;

    move-result-object v3

    check-cast v3, LX/1vb;

    invoke-direct {p0, v3}, LX/JOV;-><init>(LX/1vb;)V

    .line 2687683
    move-object v0, p0

    .line 2687684
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2687685
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JOV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2687686
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2687687
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
