.class public LX/Ips;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;

.field private static final e:LX/1sw;

.field private static final f:LX/1sw;

.field private static final g:LX/1sw;


# instance fields
.field public final irisSeqId:Ljava/lang/Long;

.field public final newStatus:Ljava/lang/Integer;

.field public final requestFbId:Ljava/lang/Long;

.field public final timestampMs:Ljava/lang/Long;

.field public final transferFbId:Ljava/lang/Long;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/16 v4, 0xa

    .line 2614938
    new-instance v0, LX/1sv;

    const-string v1, "DeltaPaymentRequestStatus"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/Ips;->b:LX/1sv;

    .line 2614939
    new-instance v0, LX/1sw;

    const-string v1, "requestFbId"

    invoke-direct {v0, v1, v4, v5}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ips;->c:LX/1sw;

    .line 2614940
    new-instance v0, LX/1sw;

    const-string v1, "timestampMs"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ips;->d:LX/1sw;

    .line 2614941
    new-instance v0, LX/1sw;

    const-string v1, "newStatus"

    const/16 v2, 0x8

    const/4 v3, 0x3

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ips;->e:LX/1sw;

    .line 2614942
    new-instance v0, LX/1sw;

    const-string v1, "transferFbId"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ips;->f:LX/1sw;

    .line 2614943
    new-instance v0, LX/1sw;

    const-string v1, "irisSeqId"

    const/16 v2, 0x3e8

    invoke-direct {v0, v1, v4, v2}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/Ips;->g:LX/1sw;

    .line 2614944
    sput-boolean v5, LX/Ips;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 2614931
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2614932
    iput-object p1, p0, LX/Ips;->requestFbId:Ljava/lang/Long;

    .line 2614933
    iput-object p2, p0, LX/Ips;->timestampMs:Ljava/lang/Long;

    .line 2614934
    iput-object p3, p0, LX/Ips;->newStatus:Ljava/lang/Integer;

    .line 2614935
    iput-object p4, p0, LX/Ips;->transferFbId:Ljava/lang/Long;

    .line 2614936
    iput-object p5, p0, LX/Ips;->irisSeqId:Ljava/lang/Long;

    .line 2614937
    return-void
.end method

.method public static a(LX/Ips;)V
    .locals 4

    .prologue
    const/4 v3, 0x6

    .line 2614922
    iget-object v0, p0, LX/Ips;->requestFbId:Ljava/lang/Long;

    if-nez v0, :cond_0

    .line 2614923
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'requestFbId\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/Ips;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2614924
    :cond_0
    iget-object v0, p0, LX/Ips;->timestampMs:Ljava/lang/Long;

    if-nez v0, :cond_1

    .line 2614925
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'timestampMs\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/Ips;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2614926
    :cond_1
    iget-object v0, p0, LX/Ips;->newStatus:Ljava/lang/Integer;

    if-nez v0, :cond_2

    .line 2614927
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Required field \'newStatus\' was not present! Struct: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/Ips;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1}, LX/7H4;-><init>(ILjava/lang/String;)V

    throw v0

    .line 2614928
    :cond_2
    iget-object v0, p0, LX/Ips;->newStatus:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    sget-object v0, LX/Ipz;->a:LX/1sn;

    iget-object v1, p0, LX/Ips;->newStatus:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, LX/1sn;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2614929
    new-instance v0, LX/7H4;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The field \'newStatus\' has been assigned the invalid value "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/Ips;->newStatus:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7H4;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2614930
    :cond_3
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 2614862
    if-eqz p2, :cond_3

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 2614863
    :goto_0
    if-eqz p2, :cond_4

    const-string v0, "\n"

    move-object v2, v0

    .line 2614864
    :goto_1
    if-eqz p2, :cond_5

    const-string v0, " "

    move-object v1, v0

    .line 2614865
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v0, "DeltaPaymentRequestStatus"

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2614866
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614867
    const-string v0, "("

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614868
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614869
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614870
    const-string v0, "requestFbId"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614871
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614872
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614873
    iget-object v0, p0, LX/Ips;->requestFbId:Ljava/lang/Long;

    if-nez v0, :cond_6

    .line 2614874
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614875
    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614876
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614877
    const-string v0, "timestampMs"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614878
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614879
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614880
    iget-object v0, p0, LX/Ips;->timestampMs:Ljava/lang/Long;

    if-nez v0, :cond_7

    .line 2614881
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614882
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614883
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614884
    const-string v0, "newStatus"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614885
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614886
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614887
    iget-object v0, p0, LX/Ips;->newStatus:Ljava/lang/Integer;

    if-nez v0, :cond_8

    .line 2614888
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614889
    :cond_0
    :goto_5
    iget-object v0, p0, LX/Ips;->transferFbId:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 2614890
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614891
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614892
    const-string v0, "transferFbId"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614893
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614894
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614895
    iget-object v0, p0, LX/Ips;->transferFbId:Ljava/lang/Long;

    if-nez v0, :cond_a

    .line 2614896
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614897
    :cond_1
    :goto_6
    iget-object v0, p0, LX/Ips;->irisSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 2614898
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614899
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614900
    const-string v0, "irisSeqId"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614901
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614902
    const-string v0, ":"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614903
    iget-object v0, p0, LX/Ips;->irisSeqId:Ljava/lang/Long;

    if-nez v0, :cond_b

    .line 2614904
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614905
    :cond_2
    :goto_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614906
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614907
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2614908
    :cond_3
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_0

    .line 2614909
    :cond_4
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_1

    .line 2614910
    :cond_5
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_2

    .line 2614911
    :cond_6
    iget-object v0, p0, LX/Ips;->requestFbId:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 2614912
    :cond_7
    iget-object v0, p0, LX/Ips;->timestampMs:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_4

    .line 2614913
    :cond_8
    sget-object v0, LX/Ipz;->b:Ljava/util/Map;

    iget-object v5, p0, LX/Ips;->newStatus:Ljava/lang/Integer;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2614914
    if-eqz v0, :cond_9

    .line 2614915
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614916
    const-string v5, " ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2614917
    :cond_9
    iget-object v5, p0, LX/Ips;->newStatus:Ljava/lang/Integer;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2614918
    if-eqz v0, :cond_0

    .line 2614919
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_5

    .line 2614920
    :cond_a
    iget-object v0, p0, LX/Ips;->transferFbId:Ljava/lang/Long;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v0, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_6

    .line 2614921
    :cond_b
    iget-object v0, p0, LX/Ips;->irisSeqId:Ljava/lang/Long;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_7
.end method

.method public final a(LX/1su;)V
    .locals 2

    .prologue
    .line 2614793
    invoke-static {p0}, LX/Ips;->a(LX/Ips;)V

    .line 2614794
    invoke-virtual {p1}, LX/1su;->a()V

    .line 2614795
    iget-object v0, p0, LX/Ips;->requestFbId:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 2614796
    sget-object v0, LX/Ips;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2614797
    iget-object v0, p0, LX/Ips;->requestFbId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2614798
    :cond_0
    iget-object v0, p0, LX/Ips;->timestampMs:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 2614799
    sget-object v0, LX/Ips;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2614800
    iget-object v0, p0, LX/Ips;->timestampMs:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2614801
    :cond_1
    iget-object v0, p0, LX/Ips;->newStatus:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 2614802
    sget-object v0, LX/Ips;->e:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2614803
    iget-object v0, p0, LX/Ips;->newStatus:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, LX/1su;->a(I)V

    .line 2614804
    :cond_2
    iget-object v0, p0, LX/Ips;->transferFbId:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 2614805
    iget-object v0, p0, LX/Ips;->transferFbId:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 2614806
    sget-object v0, LX/Ips;->f:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2614807
    iget-object v0, p0, LX/Ips;->transferFbId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2614808
    :cond_3
    iget-object v0, p0, LX/Ips;->irisSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 2614809
    iget-object v0, p0, LX/Ips;->irisSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 2614810
    sget-object v0, LX/Ips;->g:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 2614811
    iget-object v0, p0, LX/Ips;->irisSeqId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 2614812
    :cond_4
    invoke-virtual {p1}, LX/1su;->c()V

    .line 2614813
    invoke-virtual {p1}, LX/1su;->b()V

    .line 2614814
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2614819
    if-nez p1, :cond_1

    .line 2614820
    :cond_0
    :goto_0
    return v0

    .line 2614821
    :cond_1
    instance-of v1, p1, LX/Ips;

    if-eqz v1, :cond_0

    .line 2614822
    check-cast p1, LX/Ips;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2614823
    if-nez p1, :cond_3

    .line 2614824
    :cond_2
    :goto_1
    move v0, v2

    .line 2614825
    goto :goto_0

    .line 2614826
    :cond_3
    iget-object v0, p0, LX/Ips;->requestFbId:Ljava/lang/Long;

    if-eqz v0, :cond_e

    move v0, v1

    .line 2614827
    :goto_2
    iget-object v3, p1, LX/Ips;->requestFbId:Ljava/lang/Long;

    if-eqz v3, :cond_f

    move v3, v1

    .line 2614828
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 2614829
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2614830
    iget-object v0, p0, LX/Ips;->requestFbId:Ljava/lang/Long;

    iget-object v3, p1, LX/Ips;->requestFbId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2614831
    :cond_5
    iget-object v0, p0, LX/Ips;->timestampMs:Ljava/lang/Long;

    if-eqz v0, :cond_10

    move v0, v1

    .line 2614832
    :goto_4
    iget-object v3, p1, LX/Ips;->timestampMs:Ljava/lang/Long;

    if-eqz v3, :cond_11

    move v3, v1

    .line 2614833
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 2614834
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2614835
    iget-object v0, p0, LX/Ips;->timestampMs:Ljava/lang/Long;

    iget-object v3, p1, LX/Ips;->timestampMs:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2614836
    :cond_7
    iget-object v0, p0, LX/Ips;->newStatus:Ljava/lang/Integer;

    if-eqz v0, :cond_12

    move v0, v1

    .line 2614837
    :goto_6
    iget-object v3, p1, LX/Ips;->newStatus:Ljava/lang/Integer;

    if-eqz v3, :cond_13

    move v3, v1

    .line 2614838
    :goto_7
    if-nez v0, :cond_8

    if-eqz v3, :cond_9

    .line 2614839
    :cond_8
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2614840
    iget-object v0, p0, LX/Ips;->newStatus:Ljava/lang/Integer;

    iget-object v3, p1, LX/Ips;->newStatus:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2614841
    :cond_9
    iget-object v0, p0, LX/Ips;->transferFbId:Ljava/lang/Long;

    if-eqz v0, :cond_14

    move v0, v1

    .line 2614842
    :goto_8
    iget-object v3, p1, LX/Ips;->transferFbId:Ljava/lang/Long;

    if-eqz v3, :cond_15

    move v3, v1

    .line 2614843
    :goto_9
    if-nez v0, :cond_a

    if-eqz v3, :cond_b

    .line 2614844
    :cond_a
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2614845
    iget-object v0, p0, LX/Ips;->transferFbId:Ljava/lang/Long;

    iget-object v3, p1, LX/Ips;->transferFbId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2614846
    :cond_b
    iget-object v0, p0, LX/Ips;->irisSeqId:Ljava/lang/Long;

    if-eqz v0, :cond_16

    move v0, v1

    .line 2614847
    :goto_a
    iget-object v3, p1, LX/Ips;->irisSeqId:Ljava/lang/Long;

    if-eqz v3, :cond_17

    move v3, v1

    .line 2614848
    :goto_b
    if-nez v0, :cond_c

    if-eqz v3, :cond_d

    .line 2614849
    :cond_c
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 2614850
    iget-object v0, p0, LX/Ips;->irisSeqId:Ljava/lang/Long;

    iget-object v3, p1, LX/Ips;->irisSeqId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_d
    move v2, v1

    .line 2614851
    goto/16 :goto_1

    :cond_e
    move v0, v2

    .line 2614852
    goto/16 :goto_2

    :cond_f
    move v3, v2

    .line 2614853
    goto/16 :goto_3

    :cond_10
    move v0, v2

    .line 2614854
    goto :goto_4

    :cond_11
    move v3, v2

    .line 2614855
    goto :goto_5

    :cond_12
    move v0, v2

    .line 2614856
    goto :goto_6

    :cond_13
    move v3, v2

    .line 2614857
    goto :goto_7

    :cond_14
    move v0, v2

    .line 2614858
    goto :goto_8

    :cond_15
    move v3, v2

    .line 2614859
    goto :goto_9

    :cond_16
    move v0, v2

    .line 2614860
    goto :goto_a

    :cond_17
    move v3, v2

    .line 2614861
    goto :goto_b
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2614818
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2614815
    sget-boolean v0, LX/Ips;->a:Z

    .line 2614816
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/Ips;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 2614817
    return-object v0
.end method
