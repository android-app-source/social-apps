.class public final enum LX/Hvo;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Hvo;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Hvo;

.field public static final enum NONE:LX/Hvo;

.field public static final enum RUNNING:LX/Hvo;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2519895
    new-instance v0, LX/Hvo;

    const-string v1, "RUNNING"

    invoke-direct {v0, v1, v2}, LX/Hvo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hvo;->RUNNING:LX/Hvo;

    new-instance v0, LX/Hvo;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v3}, LX/Hvo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Hvo;->NONE:LX/Hvo;

    .line 2519896
    const/4 v0, 0x2

    new-array v0, v0, [LX/Hvo;

    sget-object v1, LX/Hvo;->RUNNING:LX/Hvo;

    aput-object v1, v0, v2

    sget-object v1, LX/Hvo;->NONE:LX/Hvo;

    aput-object v1, v0, v3

    sput-object v0, LX/Hvo;->$VALUES:[LX/Hvo;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2519897
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Hvo;
    .locals 1

    .prologue
    .line 2519898
    const-class v0, LX/Hvo;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Hvo;

    return-object v0
.end method

.method public static values()[LX/Hvo;
    .locals 1

    .prologue
    .line 2519899
    sget-object v0, LX/Hvo;->$VALUES:[LX/Hvo;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Hvo;

    return-object v0
.end method
