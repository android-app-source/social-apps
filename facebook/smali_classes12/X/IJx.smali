.class public final LX/IJx;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/support/v4/app/DialogFragment;

.field public final synthetic b:Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;

.field public final synthetic c:Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;Landroid/support/v4/app/DialogFragment;Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;)V
    .locals 0

    .prologue
    .line 2564727
    iput-object p1, p0, LX/IJx;->c:Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;

    iput-object p2, p0, LX/IJx;->a:Landroid/support/v4/app/DialogFragment;

    iput-object p3, p0, LX/IJx;->b:Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 5
    .param p1    # Lcom/facebook/graphql/executor/GraphQLResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2564728
    iget-object v0, p0, LX/IJx;->a:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2564729
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2564730
    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 2564731
    invoke-static {v0}, LX/DZD;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)LX/DZC;

    move-result-object v0

    iget-object v1, p0, LX/IJx;->c:Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;

    iget-object v1, v1, Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;->v:LX/0W9;

    invoke-static {v0, v1}, LX/DJw;->b(LX/DZC;LX/0W9;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    .line 2564732
    iget-object v1, p0, LX/IJx;->c:Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;

    iget-object v1, v1, Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;->s:LX/1Kf;

    const/4 v2, 0x0

    const/16 v3, 0x6dc

    iget-object v4, p0, LX/IJx;->b:Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;

    invoke-interface {v1, v2, v0, v3, v4}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 2564733
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2564734
    iget-object v0, p0, LX/IJx;->a:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2564735
    iget-object v0, p0, LX/IJx;->c:Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;

    invoke-virtual {v0}, Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;->finish()V

    .line 2564736
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2564737
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-direct {p0, p1}, LX/IJx;->a(Lcom/facebook/graphql/executor/GraphQLResult;)V

    return-void
.end method
