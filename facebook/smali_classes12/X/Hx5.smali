.class public LX/Hx5;
.super LX/3Tf;
.source ""


# static fields
.field private static final c:Ljava/lang/Object;

.field public static final d:Ljava/lang/Object;

.field public static final e:Ljava/lang/Object;

.field public static final f:Ljava/lang/Object;

.field private static final g:Ljava/lang/Object;

.field private static final h:[LX/Hx4;


# instance fields
.field public A:Ljava/lang/Object;

.field public B:Lcom/facebook/events/graphql/EventsGraphQLModels$FetchEventCountsQueryModel$EventCountsModel;

.field public C:Z

.field private D:I

.field private E:I

.field public F:Lcom/facebook/events/common/EventAnalyticsParams;

.field public final G:Landroid/content/Context;

.field public final H:LX/6RZ;

.field public final I:LX/Gd0;

.field public final J:LX/I33;

.field public final K:LX/I3G;

.field public final L:LX/I34;

.field public final M:LX/Gct;

.field public final N:LX/Bm1;

.field private final O:Z

.field private final P:Z

.field public final Q:LX/I76;

.field private final R:LX/0ad;

.field private final S:Landroid/view/View$OnClickListener;

.field public i:Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;

.field public j:LX/HxQ;

.field public k:Lcom/facebook/events/dashboard/EventsDashboardFragment;

.field private final l:LX/HxK;

.field private m:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Hx2;",
            ">;"
        }
    .end annotation
.end field

.field private o:Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;

.field public p:LX/Gcz;

.field public q:LX/Gcz;

.field private r:Z

.field public s:LX/Hx6;

.field public t:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;",
            ">;"
        }
    .end annotation
.end field

.field public u:I

.field private v:Ljava/lang/String;

.field private w:Z

.field public x:Z

.field private y:I

.field private z:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2521684
    sget-object v0, LX/Hx4;->EMPTY_HEADER:LX/Hx4;

    sput-object v0, LX/Hx5;->c:Ljava/lang/Object;

    .line 2521685
    sget-object v0, LX/Hx4;->LOADING:LX/Hx4;

    sput-object v0, LX/Hx5;->d:Ljava/lang/Object;

    .line 2521686
    sget-object v0, LX/Hx4;->VIEW_ALL:LX/Hx4;

    sput-object v0, LX/Hx5;->e:Ljava/lang/Object;

    .line 2521687
    sget-object v0, LX/Hx4;->NO_EVENTS:LX/Hx4;

    sput-object v0, LX/Hx5;->f:Ljava/lang/Object;

    .line 2521688
    sget-object v0, LX/Hx4;->BIRTHDAYS_CARD:LX/Hx4;

    sput-object v0, LX/Hx5;->g:Ljava/lang/Object;

    .line 2521689
    invoke-static {}, LX/Hx4;->values()[LX/Hx4;

    move-result-object v0

    sput-object v0, LX/Hx5;->h:[LX/Hx4;

    return-void
.end method

.method public constructor <init>(LX/HxP;LX/HxQ;Landroid/content/Context;LX/6RZ;LX/Bm1;LX/Gd0;LX/I33;LX/I3G;LX/I34;LX/Gct;LX/I76;Ljava/lang/Boolean;LX/0ad;)V
    .locals 2
    .param p12    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2521690
    invoke-direct {p0}, LX/3Tf;-><init>()V

    .line 2521691
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Hx5;->w:Z

    .line 2521692
    const/4 v0, -0x1

    iput v0, p0, LX/Hx5;->y:I

    .line 2521693
    const/4 v0, -0x1

    iput v0, p0, LX/Hx5;->z:I

    .line 2521694
    const/4 v0, -0x1

    iput v0, p0, LX/Hx5;->D:I

    .line 2521695
    const/4 v0, -0x1

    iput v0, p0, LX/Hx5;->E:I

    .line 2521696
    new-instance v0, LX/Hwt;

    invoke-direct {v0, p0}, LX/Hwt;-><init>(LX/Hx5;)V

    iput-object v0, p0, LX/Hx5;->S:Landroid/view/View$OnClickListener;

    .line 2521697
    new-instance v0, Landroid/view/ContextThemeWrapper;

    const v1, 0x7f0e08eb

    invoke-direct {v0, p3, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, LX/Hx5;->G:Landroid/content/Context;

    .line 2521698
    iput-object p2, p0, LX/Hx5;->j:LX/HxQ;

    .line 2521699
    iput-object p4, p0, LX/Hx5;->H:LX/6RZ;

    .line 2521700
    iput-object p6, p0, LX/Hx5;->I:LX/Gd0;

    .line 2521701
    iput-object p7, p0, LX/Hx5;->J:LX/I33;

    .line 2521702
    iput-object p8, p0, LX/Hx5;->K:LX/I3G;

    .line 2521703
    iput-object p10, p0, LX/Hx5;->M:LX/Gct;

    .line 2521704
    iput-object p9, p0, LX/Hx5;->L:LX/I34;

    .line 2521705
    iput-object p5, p0, LX/Hx5;->N:LX/Bm1;

    .line 2521706
    invoke-virtual {p1}, LX/HxP;->a()Z

    move-result v0

    iput-boolean v0, p0, LX/Hx5;->O:Z

    .line 2521707
    invoke-virtual {p12}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/Hx5;->P:Z

    .line 2521708
    iput-object p13, p0, LX/Hx5;->R:LX/0ad;

    .line 2521709
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Hx5;->r:Z

    .line 2521710
    new-instance v0, LX/HxK;

    new-instance v1, LX/Bl1;

    invoke-direct {v1}, LX/Bl1;-><init>()V

    invoke-direct {v0, v1}, LX/HxK;-><init>(LX/Bl1;)V

    iput-object v0, p0, LX/Hx5;->l:LX/HxK;

    .line 2521711
    iput-object p11, p0, LX/Hx5;->Q:LX/I76;

    .line 2521712
    return-void
.end method

.method public static a(LX/0QB;)LX/Hx5;
    .locals 15

    .prologue
    .line 2521713
    new-instance v1, LX/Hx5;

    invoke-static {p0}, LX/HxP;->a(LX/0QB;)LX/HxP;

    move-result-object v2

    check-cast v2, LX/HxP;

    invoke-static {p0}, LX/HxQ;->a(LX/0QB;)LX/HxQ;

    move-result-object v3

    check-cast v3, LX/HxQ;

    const-class v4, Landroid/content/Context;

    invoke-interface {p0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {p0}, LX/6RZ;->a(LX/0QB;)LX/6RZ;

    move-result-object v5

    check-cast v5, LX/6RZ;

    invoke-static {p0}, LX/Bm1;->a(LX/0QB;)LX/Bm1;

    move-result-object v6

    check-cast v6, LX/Bm1;

    const-class v7, LX/Gd0;

    invoke-interface {p0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/Gd0;

    .line 2521714
    new-instance v9, LX/I33;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v8

    check-cast v8, LX/0tX;

    invoke-direct {v9, v8}, LX/I33;-><init>(LX/0tX;)V

    .line 2521715
    move-object v8, v9

    .line 2521716
    check-cast v8, LX/I33;

    .line 2521717
    new-instance v10, LX/I3G;

    .line 2521718
    new-instance v11, LX/7vU;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v9

    check-cast v9, LX/0tX;

    invoke-direct {v11, v9}, LX/7vU;-><init>(LX/0tX;)V

    .line 2521719
    move-object v9, v11

    .line 2521720
    check-cast v9, LX/7vU;

    invoke-direct {v10, v9}, LX/I3G;-><init>(LX/7vU;)V

    .line 2521721
    move-object v9, v10

    .line 2521722
    check-cast v9, LX/I3G;

    .line 2521723
    new-instance v11, LX/I34;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v10

    check-cast v10, LX/0tX;

    invoke-direct {v11, v10}, LX/I34;-><init>(LX/0tX;)V

    .line 2521724
    move-object v10, v11

    .line 2521725
    check-cast v10, LX/I34;

    const-class v11, LX/Gct;

    invoke-interface {p0, v11}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/Gct;

    invoke-static {p0}, LX/I76;->b(LX/0QB;)LX/I76;

    move-result-object v12

    check-cast v12, LX/I76;

    invoke-static {p0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v13

    check-cast v13, Ljava/lang/Boolean;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v14

    check-cast v14, LX/0ad;

    invoke-direct/range {v1 .. v14}, LX/Hx5;-><init>(LX/HxP;LX/HxQ;Landroid/content/Context;LX/6RZ;LX/Bm1;LX/Gd0;LX/I33;LX/I3G;LX/I34;LX/Gct;LX/I76;Ljava/lang/Boolean;LX/0ad;)V

    .line 2521726
    move-object v0, v1

    .line 2521727
    return-object v0
.end method

.method private a(IILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 2521728
    invoke-static {p0, p1}, LX/Hx5;->g(LX/Hx5;I)LX/Hx3;

    move-result-object v0

    .line 2521729
    sget-object v1, LX/Hx1;->a:[I

    invoke-virtual {v0}, LX/Hx3;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2521730
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No child views for section type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2521731
    :pswitch_0
    iget-boolean v0, p0, LX/Hx5;->O:Z

    if-eqz v0, :cond_1

    .line 2521732
    invoke-direct {p0, p3}, LX/Hx5;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object p3

    .line 2521733
    :cond_0
    :goto_0
    return-object p3

    .line 2521734
    :cond_1
    iget-object v0, p0, LX/Hx5;->R:LX/0ad;

    sget-short v1, LX/347;->r:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2521735
    if-nez p3, :cond_2

    .line 2521736
    const v0, 0x7f030559

    invoke-static {p0, v0, p4}, LX/Hx5;->a(LX/Hx5;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/dashboard/EventsDashboardHeaderView;

    move-object p3, v0

    .line 2521737
    :goto_1
    new-instance v0, LX/Hwu;

    invoke-direct {v0, p0}, LX/Hwu;-><init>(LX/Hx5;)V

    iget-object v1, p0, LX/Hx5;->F:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-virtual {p3, v0, v1}, Lcom/facebook/events/dashboard/EventsDashboardHeaderView;->a(LX/0QR;Lcom/facebook/events/common/EventAnalyticsParams;)V

    goto :goto_0

    .line 2521738
    :cond_2
    check-cast p3, Lcom/facebook/events/dashboard/EventsDashboardHeaderView;

    goto :goto_1

    .line 2521739
    :cond_3
    if-nez p3, :cond_4

    .line 2521740
    const v0, 0x7f030556

    invoke-static {p0, v0, p4}, LX/Hx5;->a(LX/Hx5;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;

    move-object p3, v0

    .line 2521741
    :goto_2
    iget-object v0, p0, LX/Hx5;->s:LX/Hx6;

    invoke-virtual {p3, v0}, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->setDashboardFilterType(LX/Hx6;)V

    .line 2521742
    new-instance v0, LX/Hwv;

    invoke-direct {v0, p0}, LX/Hwv;-><init>(LX/Hx5;)V

    invoke-virtual {p3, v0}, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->setCountsSummarySupplier(LX/0QR;)V

    .line 2521743
    new-instance v0, LX/Hwx;

    invoke-direct {v0, p0}, LX/Hwx;-><init>(LX/Hx5;)V

    .line 2521744
    iput-object v0, p3, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;->k:LX/Hww;

    .line 2521745
    iput-object p3, p0, LX/Hx5;->i:Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;

    goto :goto_0

    .line 2521746
    :cond_4
    check-cast p3, Lcom/facebook/events/dashboard/EventsDashboardCaspianHeaderView;

    goto :goto_2

    .line 2521747
    :pswitch_1
    add-int/lit8 v0, p1, -0x1

    move v0, v0

    .line 2521748
    invoke-static {p0, v0, p2}, LX/Hx5;->f(LX/Hx5;II)Ljava/lang/Object;

    move-result-object v1

    .line 2521749
    sget-object v2, LX/Hx5;->d:Ljava/lang/Object;

    if-ne v1, v2, :cond_c

    .line 2521750
    if-nez p3, :cond_5

    .line 2521751
    new-instance p3, LX/I9b;

    iget-object v1, p0, LX/Hx5;->G:Landroid/content/Context;

    invoke-direct {p3, v1}, LX/I9b;-><init>(Landroid/content/Context;)V

    .line 2521752
    sget-object v1, LX/Hx5;->d:Ljava/lang/Object;

    invoke-virtual {p3, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 2521753
    const v1, 0x7f0a00d5

    invoke-virtual {p3, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 2521754
    :cond_5
    move-object v1, p3

    .line 2521755
    :goto_3
    move-object p3, v1

    .line 2521756
    goto :goto_0

    .line 2521757
    :pswitch_2
    if-nez p3, :cond_7

    new-instance p3, LX/I0D;

    iget-object v0, p0, LX/Hx5;->G:Landroid/content/Context;

    invoke-direct {p3, v0}, LX/I0D;-><init>(Landroid/content/Context;)V

    .line 2521758
    :goto_4
    iget-object v0, p0, LX/Hx5;->m:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Hx5;->m:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2521759
    iget-object v0, p0, LX/Hx5;->m:LX/0Px;

    iget-object v1, p0, LX/Hx5;->k:Lcom/facebook/events/dashboard/EventsDashboardFragment;

    iget-object v2, p0, LX/Hx5;->v:Ljava/lang/String;

    .line 2521760
    iput-object v1, p3, LX/I0D;->b:Lcom/facebook/base/fragment/FbFragment;

    .line 2521761
    iput-object v2, p3, LX/I0D;->e:Ljava/lang/String;

    .line 2521762
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_12

    iget-object v3, p3, LX/I0D;->d:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_12

    .line 2521763
    invoke-static {p3, v0}, LX/I0D;->a(LX/I0D;Ljava/util/List;)V

    .line 2521764
    :cond_6
    :goto_5
    goto/16 :goto_0

    .line 2521765
    :cond_7
    check-cast p3, LX/I0D;

    goto :goto_4

    .line 2521766
    :pswitch_3
    if-nez p3, :cond_8

    new-instance p3, LX/I3M;

    iget-object v0, p0, LX/Hx5;->G:Landroid/content/Context;

    invoke-direct {p3, v0}, LX/I3M;-><init>(Landroid/content/Context;)V

    .line 2521767
    :goto_6
    iget-object v0, p0, LX/Hx5;->p:LX/Gcz;

    if-eqz v0, :cond_0

    .line 2521768
    iget-object v0, p3, LX/I3M;->d:Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;

    if-eqz v0, :cond_14

    const/4 v0, 0x1

    :goto_7
    move v0, v0

    .line 2521769
    if-nez v0, :cond_0

    .line 2521770
    iget-object v0, p0, LX/Hx5;->o:Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;

    iget-object v1, p0, LX/Hx5;->p:LX/Gcz;

    iget-object v2, p0, LX/Hx5;->F:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2521771
    iget-object v3, v2, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 2521772
    iget-object p0, v3, Lcom/facebook/events/common/EventActionContext;->e:Lcom/facebook/events/common/ActionSource;

    move-object v3, p0

    .line 2521773
    sget-object p0, Lcom/facebook/events/common/ActionSource;->DASHBOARD:Lcom/facebook/events/common/ActionSource;

    if-ne v3, p0, :cond_15

    .line 2521774
    sget-object v3, Lcom/facebook/events/common/ActionSource;->MOBILE_SUGGESTIONS_DASHBOARD:Lcom/facebook/events/common/ActionSource;

    .line 2521775
    :goto_8
    iput-object v0, p3, LX/I3M;->d:Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;

    .line 2521776
    invoke-virtual {v2, v3}, Lcom/facebook/events/common/EventAnalyticsParams;->a(Lcom/facebook/events/common/ActionSource;)Lcom/facebook/events/common/EventAnalyticsParams;

    move-result-object v3

    iput-object v3, p3, LX/I3M;->e:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2521777
    invoke-virtual {p3, v1}, LX/Gcw;->setPagerAdapter(LX/Gcz;)V

    .line 2521778
    goto/16 :goto_0

    .line 2521779
    :cond_8
    check-cast p3, LX/I3M;

    goto :goto_6

    .line 2521780
    :pswitch_4
    if-nez p3, :cond_a

    new-instance p3, LX/I36;

    iget-object v0, p0, LX/Hx5;->G:Landroid/content/Context;

    invoke-direct {p3, v0}, LX/I36;-><init>(Landroid/content/Context;)V

    .line 2521781
    :goto_9
    iget-object v0, p0, LX/Hx5;->q:LX/Gcz;

    if-eqz v0, :cond_0

    .line 2521782
    iget v0, p3, LX/I36;->e:I

    if-lez v0, :cond_16

    const/4 v0, 0x1

    :goto_a
    move v0, v0

    .line 2521783
    if-nez v0, :cond_0

    .line 2521784
    iget-object v0, p0, LX/Hx5;->t:Ljava/util/List;

    iget v1, p0, LX/Hx5;->u:I

    iget-object v2, p0, LX/Hx5;->q:LX/Gcz;

    iget-object v3, p0, LX/Hx5;->F:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2521785
    iget-object p0, v3, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 2521786
    iget-object p1, p0, Lcom/facebook/events/common/EventActionContext;->e:Lcom/facebook/events/common/ActionSource;

    move-object p0, p1

    .line 2521787
    sget-object p1, Lcom/facebook/events/common/ActionSource;->DASHBOARD:Lcom/facebook/events/common/ActionSource;

    if-ne p0, p1, :cond_17

    .line 2521788
    sget-object p0, Lcom/facebook/events/common/ActionSource;->MOBILE_SUBSCRIPTIONS_DASHBOARD:Lcom/facebook/events/common/ActionSource;

    .line 2521789
    :goto_b
    iput-object v0, p3, LX/I36;->d:Ljava/util/List;

    .line 2521790
    iput v1, p3, LX/I36;->e:I

    .line 2521791
    iget-object p1, p3, LX/I36;->d:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_9

    if-lez v1, :cond_9

    .line 2521792
    const p1, 0x7f0821cb

    const p2, 0x7f020872

    const p4, 0x7f0a010e

    invoke-virtual {p3, p1, p2, p4}, LX/Gcw;->a(III)V

    .line 2521793
    :cond_9
    invoke-virtual {v3, p0}, Lcom/facebook/events/common/EventAnalyticsParams;->a(Lcom/facebook/events/common/ActionSource;)Lcom/facebook/events/common/EventAnalyticsParams;

    move-result-object p0

    iput-object p0, p3, LX/I36;->f:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2521794
    invoke-virtual {p3, v2}, LX/Gcw;->setPagerAdapter(LX/Gcz;)V

    .line 2521795
    goto/16 :goto_0

    .line 2521796
    :cond_a
    check-cast p3, LX/I36;

    goto :goto_9

    .line 2521797
    :pswitch_5
    if-nez p3, :cond_0

    .line 2521798
    iget-boolean v0, p0, LX/Hx5;->O:Z

    if-eqz v0, :cond_b

    .line 2521799
    const v0, 0x7f030557

    invoke-static {p0, v0, p4}, LX/Hx5;->a(LX/Hx5;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p3

    goto/16 :goto_0

    .line 2521800
    :cond_b
    const v0, 0x7f030560

    invoke-static {p0, v0, p4}, LX/Hx5;->a(LX/Hx5;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p3

    goto/16 :goto_0

    .line 2521801
    :cond_c
    sget-object v2, LX/Hx5;->e:Ljava/lang/Object;

    if-ne v1, v2, :cond_d

    .line 2521802
    if-eqz p3, :cond_f

    .line 2521803
    :goto_c
    move-object v1, p3

    .line 2521804
    goto/16 :goto_3

    .line 2521805
    :cond_d
    sget-object v2, LX/Hx5;->f:Ljava/lang/Object;

    if-ne v1, v2, :cond_e

    .line 2521806
    if-nez p3, :cond_10

    new-instance p3, LX/Hy6;

    iget-object v1, p0, LX/Hx5;->G:Landroid/content/Context;

    invoke-direct {p3, v1}, LX/Hy6;-><init>(Landroid/content/Context;)V

    .line 2521807
    :goto_d
    iget-object v1, p0, LX/Hx5;->s:LX/Hx6;

    if-nez v1, :cond_11

    .line 2521808
    const/16 v1, 0x8

    invoke-virtual {p3, v1}, LX/Hy6;->setVisibility(I)V

    .line 2521809
    :goto_e
    move-object v1, p3

    .line 2521810
    goto/16 :goto_3

    .line 2521811
    :cond_e
    check-cast v1, Lcom/facebook/events/model/Event;

    .line 2521812
    invoke-static {p0, v1, p3}, LX/Hx5;->a(LX/Hx5;Lcom/facebook/events/model/Event;Landroid/view/View;)Landroid/view/View;

    move-result-object v1

    goto/16 :goto_3

    .line 2521813
    :cond_f
    const v1, 0x7f030565

    invoke-static {p0, v1, p4}, LX/Hx5;->a(LX/Hx5;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/dashboard/EventsDashboardViewAllRowView;

    .line 2521814
    iget-object v2, p0, LX/Hx5;->F:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2521815
    iput-object v2, v1, Lcom/facebook/events/dashboard/EventsDashboardViewAllRowView;->c:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2521816
    iput-object p0, v1, Lcom/facebook/events/dashboard/EventsDashboardViewAllRowView;->b:LX/Hx5;

    .line 2521817
    move-object p3, v1

    .line 2521818
    goto :goto_c

    .line 2521819
    :cond_10
    check-cast p3, LX/Hy6;

    goto :goto_d

    .line 2521820
    :cond_11
    const/4 v1, 0x0

    invoke-virtual {p3, v1}, LX/Hy6;->setVisibility(I)V

    .line 2521821
    iget-object v1, p0, LX/Hx5;->s:LX/Hx6;

    .line 2521822
    invoke-static {p3, v1}, LX/Hy6;->b(LX/Hy6;LX/Hx6;)V

    .line 2521823
    goto :goto_e

    .line 2521824
    :cond_12
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_13
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;

    .line 2521825
    iget-object p1, p3, LX/I0D;->d:Ljava/util/Set;

    invoke-virtual {v3}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;->m()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_13

    .line 2521826
    invoke-static {p3, v0}, LX/I0D;->a(LX/I0D;Ljava/util/List;)V

    goto/16 :goto_5

    :cond_14
    const/4 v0, 0x0

    goto/16 :goto_7

    .line 2521827
    :cond_15
    iget-object v3, v2, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 2521828
    iget-object p0, v3, Lcom/facebook/events/common/EventActionContext;->e:Lcom/facebook/events/common/ActionSource;

    move-object v3, p0

    .line 2521829
    goto/16 :goto_8

    :cond_16
    const/4 v0, 0x0

    goto/16 :goto_a

    .line 2521830
    :cond_17
    iget-object p0, v3, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 2521831
    iget-object p1, p0, Lcom/facebook/events/common/EventActionContext;->e:Lcom/facebook/events/common/ActionSource;

    move-object p0, p1

    .line 2521832
    goto/16 :goto_b

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_5
        :pswitch_4
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public static a(LX/Hx5;ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(I",
            "Landroid/view/ViewGroup;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 2521833
    iget-object v0, p0, LX/Hx5;->G:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2521834
    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/Hx5;Lcom/facebook/events/model/Event;Landroid/view/View;)Landroid/view/View;
    .locals 6

    .prologue
    .line 2521673
    if-eqz p2, :cond_0

    .line 2521674
    check-cast p2, Lcom/facebook/events/dashboard/EventsDashboardRowView;

    move-object v0, p2

    .line 2521675
    :goto_0
    iget-object v1, p0, LX/Hx5;->l:LX/HxK;

    .line 2521676
    iget-object v2, v1, LX/HxK;->d:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    move v2, v2

    .line 2521677
    iget-object v3, p0, LX/Hx5;->F:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v1, p0, LX/Hx5;->j:LX/HxQ;

    .line 2521678
    iget-object v4, v1, LX/HxQ;->c:LX/Hx6;

    move-object v4, v4

    .line 2521679
    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/events/dashboard/EventsDashboardRowView;->a(Lcom/facebook/events/model/Event;ZLcom/facebook/events/common/EventAnalyticsParams;LX/Hx6;Z)V

    .line 2521680
    return-object v0

    .line 2521681
    :cond_0
    new-instance v0, Lcom/facebook/events/dashboard/EventsDashboardRowView;

    iget-object v1, p0, LX/Hx5;->G:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/facebook/events/dashboard/EventsDashboardRowView;-><init>(Landroid/content/Context;)V

    .line 2521682
    iget-object v1, p0, LX/Hx5;->S:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/events/dashboard/EventsDashboardRowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private a(Landroid/view/View;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2521835
    if-eqz p1, :cond_0

    .line 2521836
    :goto_0
    return-object p1

    .line 2521837
    :cond_0
    new-instance p1, Landroid/view/View;

    iget-object v0, p0, LX/Hx5;->G:Landroid/content/Context;

    invoke-direct {p1, v0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2521838
    sget-object v0, LX/Hx5;->c:Ljava/lang/Object;

    invoke-virtual {p1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private a(LX/6RY;Ljava/util/Date;ZZ)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2521839
    iget-object v0, p0, LX/Hx5;->l:LX/HxK;

    invoke-virtual {v0}, LX/HxK;->b()I

    move-result v0

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    .line 2521840
    sget-object v0, LX/Hx1;->b:[I

    invoke-virtual {p1}, LX/6RY;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2521841
    iget-object v0, p0, LX/Hx5;->G:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0821ac

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    move-object v0, v0

    .line 2521842
    :goto_1
    return-object v0

    .line 2521843
    :cond_0
    sget-object v0, LX/Hx1;->b:[I

    invoke-virtual {p1}, LX/6RY;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 2521844
    :cond_1
    if-eqz p4, :cond_2

    .line 2521845
    iget-object v0, p0, LX/Hx5;->H:LX/6RZ;

    invoke-virtual {v0, p2}, LX/6RZ;->f(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 2521846
    :goto_2
    move-object v0, v0

    .line 2521847
    goto :goto_1

    .line 2521848
    :pswitch_0
    iget-object v0, p0, LX/Hx5;->G:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0821aa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2521849
    :pswitch_1
    iget-object v0, p0, LX/Hx5;->G:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0821ab

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2521850
    :pswitch_2
    iget-object v0, p0, LX/Hx5;->H:LX/6RZ;

    .line 2521851
    iget-object v1, v0, LX/6RZ;->p:Ljava/lang/String;

    move-object v0, v1

    .line 2521852
    goto :goto_2

    .line 2521853
    :pswitch_3
    iget-object v0, p0, LX/Hx5;->H:LX/6RZ;

    .line 2521854
    iget-object v1, v0, LX/6RZ;->q:Ljava/lang/String;

    move-object v0, v1

    .line 2521855
    goto :goto_2

    .line 2521856
    :pswitch_4
    iget-object v0, p0, LX/Hx5;->H:LX/6RZ;

    invoke-virtual {v0, p2}, LX/6RZ;->e(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 2521857
    :pswitch_5
    iget-object v0, p0, LX/Hx5;->G:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0821ab

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 2521858
    :pswitch_6
    if-eqz p3, :cond_1

    .line 2521859
    iget-object v0, p0, LX/Hx5;->H:LX/6RZ;

    invoke-virtual {v0, p2}, LX/6RZ;->h(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 2521860
    :cond_2
    iget-object v0, p0, LX/Hx5;->H:LX/6RZ;

    invoke-virtual {v0, p2}, LX/6RZ;->g(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private a(Ljava/util/List;II)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/Hx2;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 2521861
    iput-object p1, p0, LX/Hx5;->n:Ljava/util/List;

    .line 2521862
    iput p2, p0, LX/Hx5;->D:I

    .line 2521863
    iput p3, p0, LX/Hx5;->E:I

    .line 2521864
    const v0, 0x199d2a36

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2521865
    return-void
.end method

.method private b(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 2521866
    invoke-static {p0, p1}, LX/Hx5;->g(LX/Hx5;I)LX/Hx3;

    move-result-object v0

    .line 2521867
    sget-object v1, LX/Hx1;->a:[I

    invoke-virtual {v0}, LX/Hx3;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2521868
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No section header for section type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2521869
    :pswitch_0
    invoke-direct {p0, p2}, LX/Hx5;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    .line 2521870
    :goto_0
    return-object v0

    .line 2521871
    :pswitch_1
    invoke-direct {p0}, LX/Hx5;->i()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2521872
    invoke-direct {p0, p2}, LX/Hx5;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 2521873
    :cond_0
    add-int/lit8 v0, p1, -0x1

    move v0, v0

    .line 2521874
    iget-object v1, p0, LX/Hx5;->n:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hx2;

    iget-object v0, v0, LX/Hx2;->b:Ljava/lang/String;

    .line 2521875
    if-eqz p2, :cond_1

    .line 2521876
    check-cast p2, Lcom/facebook/widget/text/BetterTextView;

    .line 2521877
    :goto_1
    invoke-virtual {p2, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2521878
    move-object v0, p2

    .line 2521879
    goto :goto_0

    .line 2521880
    :cond_1
    const v1, 0x7f03055f

    invoke-static {p0, v1, p3}, LX/Hx5;->a(LX/Hx5;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/text/BetterTextView;

    move-object p2, v1

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static f(LX/Hx5;II)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2521881
    invoke-direct {p0, p1, p2}, LX/Hx5;->g(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2521882
    sget-object v0, LX/Hx5;->e:Ljava/lang/Object;

    .line 2521883
    :goto_0
    return-object v0

    .line 2521884
    :cond_0
    invoke-direct {p0, p1, p2}, LX/Hx5;->h(II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2521885
    sget-object v0, LX/Hx5;->d:Ljava/lang/Object;

    goto :goto_0

    .line 2521886
    :cond_1
    iget-object v0, p0, LX/Hx5;->n:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hx2;

    .line 2521887
    iget v1, v0, LX/Hx2;->d:I

    if-nez v1, :cond_3

    .line 2521888
    iget-boolean v0, p0, LX/Hx5;->x:Z

    if-eqz v0, :cond_2

    .line 2521889
    sget-object v0, LX/Hx5;->f:Ljava/lang/Object;

    goto :goto_0

    .line 2521890
    :cond_2
    sget-object v0, LX/Hx5;->d:Ljava/lang/Object;

    goto :goto_0

    .line 2521891
    :cond_3
    iget-object v1, p0, LX/Hx5;->l:LX/HxK;

    iget v0, v0, LX/Hx2;->c:I

    add-int/2addr v0, p2

    .line 2521892
    iget-object v2, v1, LX/HxK;->f:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_4

    .line 2521893
    iget-object v2, v1, LX/HxK;->f:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/model/Event;

    .line 2521894
    :goto_1
    move-object v0, v2

    .line 2521895
    goto :goto_0

    .line 2521896
    :cond_4
    iget v2, v1, LX/HxK;->g:I

    iget-object v3, v1, LX/HxK;->c:LX/Bl1;

    invoke-virtual {v3}, LX/Bl1;->b()I

    move-result v3

    if-ge v2, v3, :cond_8

    iget v2, v1, LX/HxK;->h:I

    iget-object v3, v1, LX/HxK;->d:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_8

    .line 2521897
    iget-object v2, v1, LX/HxK;->c:LX/Bl1;

    iget v3, v1, LX/HxK;->g:I

    invoke-virtual {v2, v3}, LX/Bl1;->a(I)Z

    .line 2521898
    iget-object v2, v1, LX/HxK;->c:LX/Bl1;

    invoke-virtual {v2}, LX/Bl1;->c()J

    move-result-wide v4

    .line 2521899
    iget-object v2, v1, LX/HxK;->d:Ljava/util/List;

    iget v3, v1, LX/HxK;->h:I

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/model/Event;

    .line 2521900
    invoke-virtual {v2}, Lcom/facebook/events/model/Event;->M()J

    move-result-wide v6

    .line 2521901
    iget-boolean v3, v1, LX/HxK;->e:Z

    if-eqz v3, :cond_6

    cmp-long v3, v4, v6

    if-gez v3, :cond_7

    .line 2521902
    :cond_5
    iget-object v2, v1, LX/HxK;->f:Ljava/util/ArrayList;

    iget-object v3, v1, LX/HxK;->c:LX/Bl1;

    invoke-virtual {v3}, LX/Bl1;->d()Lcom/facebook/events/model/Event;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2521903
    iget v2, v1, LX/HxK;->g:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, LX/HxK;->g:I

    .line 2521904
    :goto_2
    iget-object v2, v1, LX/HxK;->f:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_4

    .line 2521905
    iget-object v2, v1, LX/HxK;->f:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/model/Event;

    goto :goto_1

    .line 2521906
    :cond_6
    cmp-long v3, v4, v6

    if-gtz v3, :cond_5

    .line 2521907
    :cond_7
    iget-object v3, v1, LX/HxK;->f:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2521908
    iget v2, v1, LX/HxK;->h:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, LX/HxK;->h:I

    goto :goto_2

    .line 2521909
    :cond_8
    iget v2, v1, LX/HxK;->g:I

    iget-object v3, v1, LX/HxK;->c:LX/Bl1;

    invoke-virtual {v3}, LX/Bl1;->b()I

    move-result v3

    if-ge v2, v3, :cond_9

    .line 2521910
    iget-object v2, v1, LX/HxK;->c:LX/Bl1;

    iget v3, v1, LX/HxK;->g:I

    invoke-virtual {v2, v3}, LX/Bl1;->a(I)Z

    .line 2521911
    iget-object v2, v1, LX/HxK;->f:Ljava/util/ArrayList;

    iget-object v3, v1, LX/HxK;->c:LX/Bl1;

    invoke-virtual {v3}, LX/Bl1;->d()Lcom/facebook/events/model/Event;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2521912
    iget v2, v1, LX/HxK;->g:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, LX/HxK;->g:I

    .line 2521913
    iget-object v2, v1, LX/HxK;->f:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_8

    .line 2521914
    iget-object v2, v1, LX/HxK;->f:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/model/Event;

    goto/16 :goto_1

    .line 2521915
    :cond_9
    :goto_3
    iget v2, v1, LX/HxK;->h:I

    iget-object v3, v1, LX/HxK;->d:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_a

    .line 2521916
    iget-object v2, v1, LX/HxK;->f:Ljava/util/ArrayList;

    iget-object v3, v1, LX/HxK;->d:Ljava/util/List;

    iget v4, v1, LX/HxK;->h:I

    add-int/lit8 v5, v4, 0x1

    iput v5, v1, LX/HxK;->h:I

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 2521917
    :cond_a
    iget-object v2, v1, LX/HxK;->f:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/model/Event;

    goto/16 :goto_1
.end method

.method public static g(LX/Hx5;I)LX/Hx3;
    .locals 3

    .prologue
    .line 2521918
    if-nez p1, :cond_0

    .line 2521919
    sget-object v0, LX/Hx3;->DASHBOARD_FILTER:LX/Hx3;

    .line 2521920
    :goto_0
    return-object v0

    .line 2521921
    :cond_0
    add-int/lit8 v0, p1, -0x1

    move v0, v0

    .line 2521922
    invoke-direct {p0}, LX/Hx5;->i()I

    move-result v1

    .line 2521923
    if-lez v1, :cond_3

    .line 2521924
    iget-boolean v2, p0, LX/Hx5;->C:Z

    if-nez v2, :cond_1

    iget v2, p0, LX/Hx5;->D:I

    if-gez v2, :cond_2

    .line 2521925
    :cond_1
    if-ge v0, v1, :cond_3

    .line 2521926
    sget-object v0, LX/Hx3;->EVENT:LX/Hx3;

    goto :goto_0

    .line 2521927
    :cond_2
    iget v2, p0, LX/Hx5;->D:I

    if-gt v0, v2, :cond_3

    .line 2521928
    sget-object v0, LX/Hx3;->EVENT:LX/Hx3;

    goto :goto_0

    .line 2521929
    :cond_3
    add-int/lit8 v0, v1, 0x1

    sub-int v0, p1, v0

    .line 2521930
    invoke-direct {p0}, LX/Hx5;->m()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2521931
    if-nez v0, :cond_4

    .line 2521932
    sget-object v0, LX/Hx3;->SUBSCRIPTIONS:LX/Hx3;

    goto :goto_0

    .line 2521933
    :cond_4
    add-int/lit8 v0, v0, -0x1

    .line 2521934
    :cond_5
    invoke-direct {p0}, LX/Hx5;->l()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2521935
    if-nez v0, :cond_6

    .line 2521936
    sget-object v0, LX/Hx3;->SUGGESTIONS:LX/Hx3;

    goto :goto_0

    .line 2521937
    :cond_6
    add-int/lit8 v0, v0, -0x1

    .line 2521938
    :cond_7
    invoke-direct {p0}, LX/Hx5;->k()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 2521939
    if-nez v0, :cond_8

    .line 2521940
    sget-object v0, LX/Hx3;->BIRTHDAYS:LX/Hx3;

    goto :goto_0

    .line 2521941
    :cond_8
    sget-object v0, LX/Hx3;->BOTTOM_PADDING:LX/Hx3;

    goto :goto_0
.end method

.method private g(II)Z
    .locals 1

    .prologue
    .line 2521942
    iget-boolean v0, p0, LX/Hx5;->C:Z

    if-nez v0, :cond_0

    iget v0, p0, LX/Hx5;->D:I

    if-ne p1, v0, :cond_0

    iget v0, p0, LX/Hx5;->E:I

    if-ne p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private h(II)Z
    .locals 1

    .prologue
    .line 2521943
    iget-boolean v0, p0, LX/Hx5;->w:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Hx5;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    iget-object v0, p0, LX/Hx5;->n:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hx2;

    iget v0, v0, LX/Hx2;->d:I

    if-ne p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private i()I
    .locals 1

    .prologue
    .line 2521944
    iget-object v0, p0, LX/Hx5;->n:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Hx5;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2521945
    :cond_0
    const/4 v0, 0x0

    .line 2521946
    :goto_0
    return v0

    .line 2521947
    :cond_1
    iget-boolean v0, p0, LX/Hx5;->C:Z

    if-nez v0, :cond_2

    iget v0, p0, LX/Hx5;->D:I

    if-gez v0, :cond_3

    .line 2521948
    :cond_2
    iget-object v0, p0, LX/Hx5;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0

    .line 2521949
    :cond_3
    iget v0, p0, LX/Hx5;->D:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static i(LX/Hx5;II)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2521950
    iget v0, p0, LX/Hx5;->y:I

    if-ne p1, v0, :cond_0

    iget v0, p0, LX/Hx5;->z:I

    if-ne p2, v0, :cond_0

    .line 2521951
    :goto_0
    return-void

    .line 2521952
    :cond_0
    iput p1, p0, LX/Hx5;->y:I

    .line 2521953
    iput p2, p0, LX/Hx5;->z:I

    .line 2521954
    invoke-virtual {p0, p1}, LX/3Tf;->d(I)[I

    move-result-object v0

    .line 2521955
    aget v0, v0, v2

    .line 2521956
    invoke-static {p0, v0}, LX/Hx5;->g(LX/Hx5;I)LX/Hx3;

    move-result-object v0

    .line 2521957
    sget-object v1, LX/Hx3;->EVENT:LX/Hx3;

    if-eq v0, v1, :cond_1

    sget-object v1, LX/Hx3;->DASHBOARD_FILTER:LX/Hx3;

    if-eq v0, v1, :cond_1

    .line 2521958
    iput-object v4, p0, LX/Hx5;->A:Ljava/lang/Object;

    .line 2521959
    :cond_1
    :goto_1
    if-lt p2, p1, :cond_4

    .line 2521960
    invoke-virtual {p0, p2}, LX/3Tf;->d(I)[I

    move-result-object v0

    .line 2521961
    aget v1, v0, v3

    if-ltz v1, :cond_3

    .line 2521962
    aget v1, v0, v2

    aget v0, v0, v3

    invoke-virtual {p0, v1, v0}, LX/Hx5;->a(II)Ljava/lang/Object;

    move-result-object v0

    .line 2521963
    instance-of v1, v0, Lcom/facebook/events/model/Event;

    if-nez v1, :cond_2

    sget-object v1, LX/Hx5;->d:Ljava/lang/Object;

    if-ne v0, v1, :cond_3

    .line 2521964
    :cond_2
    iput-object v0, p0, LX/Hx5;->A:Ljava/lang/Object;

    goto :goto_0

    .line 2521965
    :cond_3
    add-int/lit8 p2, p2, -0x1

    goto :goto_1

    .line 2521966
    :cond_4
    iput-object v4, p0, LX/Hx5;->A:Ljava/lang/Object;

    goto :goto_0
.end method

.method public static j(LX/Hx5;)V
    .locals 18
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-java.lang.System.currentTimeMillis"
        }
    .end annotation

    .prologue
    .line 2521967
    const/4 v5, -0x1

    .line 2521968
    const/4 v4, -0x1

    .line 2521969
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Hx5;->l:LX/HxK;

    invoke-virtual {v2}, LX/HxK;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2521970
    const/4 v2, 0x1

    new-array v2, v2, [LX/Hx2;

    const/4 v3, 0x0

    new-instance v4, LX/Hx2;

    sget-object v5, LX/Hx3;->EVENT:LX/Hx3;

    const-string v6, ""

    invoke-direct {v4, v5, v6}, LX/Hx2;-><init>(LX/Hx3;Ljava/lang/String;)V

    aput-object v4, v2, v3

    invoke-static {v2}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v2

    const/4 v3, -0x1

    const/4 v4, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4}, LX/Hx5;->a(Ljava/util/List;II)V

    .line 2521971
    :goto_0
    return-void

    .line 2521972
    :cond_0
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v8

    .line 2521973
    const/4 v2, 0x0

    .line 2521974
    const/4 v3, 0x0

    .line 2521975
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 2521976
    invoke-static {v10, v11}, LX/6Rc;->a(J)LX/6Rc;

    move-result-object v9

    .line 2521977
    invoke-static {v10, v11}, LX/6Rc;->b(J)LX/6Rc;

    move-result-object v12

    .line 2521978
    move-object/from16 v0, p0

    iget-object v6, v0, LX/Hx5;->l:LX/HxK;

    invoke-virtual {v6}, LX/HxK;->c()Ljava/util/Iterator;

    move-result-object v13

    move v6, v5

    move v5, v4

    move v4, v2

    .line 2521979
    :goto_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2521980
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    .line 2521981
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, v14, v15}, Ljava/util/Date;-><init>(J)V

    .line 2521982
    invoke-virtual {v9, v14, v15}, LX/6Rc;->c(J)Z

    move-result v7

    .line 2521983
    invoke-virtual {v12, v14, v15}, LX/6Rc;->c(J)Z

    move-result v16

    .line 2521984
    move-object/from16 v0, p0

    iget-object v0, v0, LX/Hx5;->H:LX/6RZ;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v14, v15, v10, v11}, LX/6RZ;->a(JJ)LX/6RY;

    move-result-object v14

    .line 2521985
    move-object/from16 v0, p0

    iget-object v15, v0, LX/Hx5;->s:LX/Hx6;

    sget-object v17, LX/Hx6;->PAST:LX/Hx6;

    move-object/from16 v0, v17

    if-ne v15, v0, :cond_6

    .line 2521986
    if-eqz v16, :cond_5

    move-object/from16 v0, p0

    iget-object v7, v0, LX/Hx5;->H:LX/6RZ;

    invoke-virtual {v7, v2}, LX/6RZ;->f(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    :goto_2
    move-object v7, v2

    .line 2521987
    :goto_3
    if-gez v6, :cond_2

    .line 2521988
    const/4 v2, 0x7

    if-lt v4, v2, :cond_2

    .line 2521989
    sget-object v2, LX/6RY;->NEXT_WEEK:LX/6RY;

    if-eq v14, v2, :cond_1

    sget-object v2, LX/6RY;->FUTURE:LX/6RY;

    if-ne v14, v2, :cond_2

    .line 2521990
    :cond_1
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v6, v2, -0x1

    .line 2521991
    iget v5, v3, LX/Hx2;->d:I

    .line 2521992
    :cond_2
    if-eqz v3, :cond_3

    iget-object v2, v3, LX/Hx2;->b:Ljava/lang/String;

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 2521993
    :cond_3
    new-instance v2, LX/Hx2;

    sget-object v3, LX/Hx3;->EVENT:LX/Hx3;

    invoke-direct {v2, v3, v7}, LX/Hx2;-><init>(LX/Hx3;Ljava/lang/String;)V

    .line 2521994
    iput v4, v2, LX/Hx2;->c:I

    .line 2521995
    invoke-interface {v8, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2521996
    :goto_4
    add-int/lit8 v3, v4, 0x1

    .line 2521997
    iget v4, v2, LX/Hx2;->d:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v2, LX/Hx2;->d:I

    .line 2521998
    move-object/from16 v0, p0

    iget-boolean v4, v0, LX/Hx5;->C:Z

    if-nez v4, :cond_4

    if-gez v6, :cond_7

    :cond_4
    move v4, v3

    move-object v3, v2

    .line 2521999
    goto :goto_1

    .line 2522000
    :cond_5
    move-object/from16 v0, p0

    iget-object v7, v0, LX/Hx5;->H:LX/6RZ;

    invoke-virtual {v7, v2}, LX/6RZ;->g(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 2522001
    :cond_6
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v14, v2, v7, v1}, LX/Hx5;->a(LX/6RY;Ljava/util/Date;ZZ)Ljava/lang/String;

    move-result-object v2

    move-object v7, v2

    goto :goto_3

    .line 2522002
    :cond_7
    move-object/from16 v0, p0

    invoke-direct {v0, v8, v6, v5}, LX/Hx5;->a(Ljava/util/List;II)V

    goto/16 :goto_0

    :cond_8
    move-object v2, v3

    goto :goto_4
.end method

.method private k()Z
    .locals 2

    .prologue
    .line 2521683
    iget-object v0, p0, LX/Hx5;->s:LX/Hx6;

    sget-object v1, LX/Hx6;->UPCOMING:LX/Hx6;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/Hx5;->m:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Hx5;->m:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/Hx5;->O:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private l()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2521565
    iget-boolean v1, p0, LX/Hx5;->P:Z

    if-eqz v1, :cond_1

    .line 2521566
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, LX/Hx5;->s:LX/Hx6;

    sget-object v2, LX/Hx6;->UPCOMING:LX/Hx6;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, LX/Hx5;->o:Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;

    if-nez v1, :cond_2

    iget-boolean v1, p0, LX/Hx5;->r:Z

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private m()Z
    .locals 2

    .prologue
    .line 2521567
    iget-object v0, p0, LX/Hx5;->s:LX/Hx6;

    sget-object v1, LX/Hx6;->UPCOMING:LX/Hx6;

    if-ne v0, v1, :cond_0

    iget v0, p0, LX/Hx5;->u:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)I
    .locals 4

    .prologue
    .line 2521568
    invoke-static {p0, p1}, LX/Hx5;->g(LX/Hx5;I)LX/Hx3;

    move-result-object v0

    .line 2521569
    sget-object v1, LX/Hx1;->a:[I

    invoke-virtual {v0}, LX/Hx3;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2521570
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No section header view type for section type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2521571
    :pswitch_0
    sget-object v0, LX/Hx4;->EMPTY_HEADER:LX/Hx4;

    invoke-virtual {v0}, LX/Hx4;->ordinal()I

    move-result v0

    .line 2521572
    :goto_0
    return v0

    .line 2521573
    :pswitch_1
    invoke-direct {p0}, LX/Hx5;->i()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2521574
    sget-object v0, LX/Hx4;->EMPTY_HEADER:LX/Hx4;

    invoke-virtual {v0}, LX/Hx4;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2521575
    :cond_0
    sget-object v0, LX/Hx4;->TEXT_HEADER:LX/Hx4;

    invoke-virtual {v0}, LX/Hx4;->ordinal()I

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2521672
    invoke-direct {p0, p1, p2, p4, p5}, LX/Hx5;->a(IILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2521576
    invoke-direct {p0, p1, p2, p3}, LX/Hx5;->b(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(II)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2521577
    invoke-static {p0, p1}, LX/Hx5;->g(LX/Hx5;I)LX/Hx3;

    move-result-object v0

    .line 2521578
    sget-object v1, LX/Hx1;->a:[I

    invoke-virtual {v0}, LX/Hx3;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2521579
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No child for section type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2521580
    :pswitch_0
    const/4 v0, 0x0

    .line 2521581
    :goto_0
    return-object v0

    .line 2521582
    :pswitch_1
    add-int/lit8 v0, p1, -0x1

    move v0, v0

    .line 2521583
    invoke-static {p0, v0, p2}, LX/Hx5;->f(LX/Hx5;II)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Landroid/database/Cursor;LX/0Rf;LX/Hx6;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "LX/0Rf",
            "<",
            "Lcom/facebook/events/model/Event;",
            ">;",
            "LX/Hx6;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 2521584
    sget-object v0, LX/Hx6;->PAST:LX/Hx6;

    if-ne p3, v0, :cond_0

    .line 2521585
    iget-object v0, p0, LX/Hx5;->l:LX/HxK;

    .line 2521586
    const/4 v1, 0x0

    invoke-static {v0, p1, p2, v1}, LX/HxK;->a(LX/HxK;Landroid/database/Cursor;LX/0Py;Z)V

    .line 2521587
    :goto_0
    iput-object p3, p0, LX/Hx5;->s:LX/Hx6;

    .line 2521588
    iput-boolean p4, p0, LX/Hx5;->x:Z

    .line 2521589
    invoke-static {p0}, LX/Hx5;->j(LX/Hx5;)V

    .line 2521590
    return-void

    .line 2521591
    :cond_0
    iget-object v0, p0, LX/Hx5;->l:LX/HxK;

    .line 2521592
    const/4 v1, 0x1

    invoke-static {v0, p1, p2, v1}, LX/HxK;->a(LX/HxK;Landroid/database/Cursor;LX/0Py;Z)V

    .line 2521593
    goto :goto_0
.end method

.method public final a(Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 2521594
    iget-boolean v1, p0, LX/Hx5;->r:Z

    .line 2521595
    iput-boolean v0, p0, LX/Hx5;->r:Z

    .line 2521596
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;->c()Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel$EventsModel;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;->c()Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel$EventsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel$EventsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 2521597
    :cond_1
    iget-object v2, p0, LX/Hx5;->o:Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;

    if-nez v2, :cond_2

    if-eqz v0, :cond_4

    .line 2521598
    :cond_2
    if-eqz v1, :cond_3

    .line 2521599
    const v0, -0x21ec41af

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2521600
    :cond_3
    :goto_0
    return-void

    .line 2521601
    :cond_4
    iput-object p1, p0, LX/Hx5;->o:Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;

    .line 2521602
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel;->c()Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel$EventsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SuggestedEventCutModel$EventsModel;->a()LX/0Px;

    move-result-object v0

    new-instance v1, LX/Hwz;

    invoke-direct {v1, p0}, LX/Hwz;-><init>(LX/Hx5;)V

    invoke-static {v0, v1}, LX/0R9;->a(Ljava/util/List;LX/0QK;)Ljava/util/List;

    move-result-object v3

    .line 2521603
    iget-object v0, p0, LX/Hx5;->I:LX/Gd0;

    iget-object v1, p0, LX/Hx5;->G:Landroid/content/Context;

    iget-object v2, p0, LX/Hx5;->F:Lcom/facebook/events/common/EventAnalyticsParams;

    sget-object v4, Lcom/facebook/events/common/ActionSource;->MOBILE_SUGGESTIONS_DASHBOARD:Lcom/facebook/events/common/ActionSource;

    sget-object v5, Lcom/facebook/events/common/ActionMechanism;->DASHBOARD_SUGGESTIONS_CARD:Lcom/facebook/events/common/ActionMechanism;

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, LX/Gd0;->a(Landroid/content/Context;Lcom/facebook/events/common/EventAnalyticsParams;Ljava/util/List;Lcom/facebook/events/common/ActionSource;Lcom/facebook/events/common/ActionMechanism;Ljava/lang/String;)LX/Gcz;

    move-result-object v0

    iput-object v0, p0, LX/Hx5;->p:LX/Gcz;

    .line 2521604
    const v0, 0x3f7a979b

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_0
.end method

.method public final a(Ljava/util/List;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2521605
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2521606
    :goto_0
    return-void

    .line 2521607
    :cond_0
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 2521608
    new-instance v1, LX/Hwy;

    invoke-direct {v1, p0, v0}, LX/Hwy;-><init>(LX/Hx5;Ljava/util/Date;)V

    .line 2521609
    iput-object p2, p0, LX/Hx5;->v:Ljava/lang/String;

    .line 2521610
    sget-object v0, LX/1zb;->a:LX/1zb;

    move-object v0, v0

    .line 2521611
    invoke-virtual {v0, v1}, LX/1sm;->a(LX/0QK;)LX/1sm;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/1sm;->d(Ljava/lang/Iterable;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/Hx5;->m:LX/0Px;

    .line 2521612
    const v0, -0x7882da52

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 2521561
    iget-boolean v0, p0, LX/Hx5;->x:Z

    if-eq v0, p1, :cond_0

    .line 2521562
    iput-boolean p1, p0, LX/Hx5;->x:Z

    .line 2521563
    invoke-static {p0}, LX/Hx5;->j(LX/Hx5;)V

    .line 2521564
    :cond_0
    return-void
.end method

.method public final b(I)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2521613
    invoke-static {p0, p1}, LX/Hx5;->g(LX/Hx5;I)LX/Hx3;

    move-result-object v0

    .line 2521614
    sget-object v1, LX/Hx1;->a:[I

    invoke-virtual {v0}, LX/Hx3;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2521615
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No section header view type for section type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2521616
    :pswitch_0
    const/4 v0, 0x0

    .line 2521617
    :goto_0
    return-object v0

    .line 2521618
    :pswitch_1
    add-int/lit8 v0, p1, -0x1

    move v0, v0

    .line 2521619
    iget-object v1, p0, LX/Hx5;->n:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 2521620
    :pswitch_2
    sget-object v0, LX/Hx5;->g:Ljava/lang/Object;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 2521621
    iget-boolean v0, p0, LX/Hx5;->w:Z

    if-eq p1, v0, :cond_0

    .line 2521622
    iput-boolean p1, p0, LX/Hx5;->w:Z

    .line 2521623
    const v0, -0x43b38082

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2521624
    :cond_0
    return-void
.end method

.method public final b(II)Z
    .locals 1

    .prologue
    .line 2521625
    const/4 v0, 0x1

    return v0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 2521626
    invoke-direct {p0}, LX/Hx5;->i()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    .line 2521627
    invoke-direct {p0}, LX/Hx5;->m()Z

    move-result v1

    .line 2521628
    if-eqz v1, :cond_0

    .line 2521629
    add-int/lit8 v0, v0, 0x1

    .line 2521630
    :cond_0
    invoke-direct {p0}, LX/Hx5;->l()Z

    move-result v1

    .line 2521631
    if-eqz v1, :cond_1

    .line 2521632
    add-int/lit8 v0, v0, 0x1

    .line 2521633
    :cond_1
    invoke-direct {p0}, LX/Hx5;->k()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2521634
    add-int/lit8 v0, v0, 0x1

    .line 2521635
    :cond_2
    return v0
.end method

.method public final c(I)I
    .locals 4

    .prologue
    .line 2521636
    invoke-static {p0, p1}, LX/Hx5;->g(LX/Hx5;I)LX/Hx3;

    move-result-object v0

    .line 2521637
    sget-object v1, LX/Hx1;->a:[I

    invoke-virtual {v0}, LX/Hx3;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2521638
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No children count for section type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2521639
    :pswitch_0
    const/4 v0, 0x1

    .line 2521640
    :cond_0
    :goto_0
    return v0

    .line 2521641
    :pswitch_1
    add-int/lit8 v0, p1, -0x1

    move v1, v0

    .line 2521642
    iget-boolean v0, p0, LX/Hx5;->C:Z

    if-nez v0, :cond_1

    iget v0, p0, LX/Hx5;->D:I

    if-ne v1, v0, :cond_1

    .line 2521643
    iget v0, p0, LX/Hx5;->E:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2521644
    :cond_1
    iget-object v0, p0, LX/Hx5;->n:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hx2;

    iget v0, v0, LX/Hx2;->d:I

    .line 2521645
    if-eqz v0, :cond_2

    iget-object v2, p0, LX/Hx5;->n:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, LX/Hx5;->w:Z

    if-eqz v1, :cond_0

    .line 2521646
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final c(II)I
    .locals 4

    .prologue
    .line 2521647
    invoke-static {p0, p1}, LX/Hx5;->g(LX/Hx5;I)LX/Hx3;

    move-result-object v0

    .line 2521648
    sget-object v1, LX/Hx1;->a:[I

    invoke-virtual {v0}, LX/Hx3;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2521649
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No child view type for section type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2521650
    :pswitch_0
    sget-object v0, LX/Hx4;->DASHBOARD_FILTER:LX/Hx4;

    invoke-virtual {v0}, LX/Hx4;->ordinal()I

    move-result v0

    .line 2521651
    :goto_0
    return v0

    .line 2521652
    :pswitch_1
    add-int/lit8 v0, p1, -0x1

    move v0, v0

    .line 2521653
    invoke-direct {p0, v0, p2}, LX/Hx5;->g(II)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2521654
    sget-object v0, LX/Hx4;->VIEW_ALL:LX/Hx4;

    invoke-virtual {v0}, LX/Hx4;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2521655
    :cond_0
    invoke-direct {p0, v0, p2}, LX/Hx5;->h(II)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2521656
    sget-object v0, LX/Hx4;->LOADING:LX/Hx4;

    invoke-virtual {v0}, LX/Hx4;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2521657
    :cond_1
    iget-object v1, p0, LX/Hx5;->n:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hx2;

    iget v0, v0, LX/Hx2;->d:I

    if-nez v0, :cond_3

    .line 2521658
    iget-boolean v0, p0, LX/Hx5;->x:Z

    if-eqz v0, :cond_2

    .line 2521659
    sget-object v0, LX/Hx4;->NO_EVENTS:LX/Hx4;

    invoke-virtual {v0}, LX/Hx4;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2521660
    :cond_2
    sget-object v0, LX/Hx4;->LOADING:LX/Hx4;

    invoke-virtual {v0}, LX/Hx4;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2521661
    :cond_3
    sget-object v0, LX/Hx4;->EVENT:LX/Hx4;

    invoke-virtual {v0}, LX/Hx4;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2521662
    :pswitch_2
    sget-object v0, LX/Hx4;->BIRTHDAYS_CARD:LX/Hx4;

    invoke-virtual {v0}, LX/Hx4;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2521663
    :pswitch_3
    sget-object v0, LX/Hx4;->SUGGESTIONS:LX/Hx4;

    invoke-virtual {v0}, LX/Hx4;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2521664
    :pswitch_4
    sget-object v0, LX/Hx4;->SUBSCRIPTIONS:LX/Hx4;

    invoke-virtual {v0}, LX/Hx4;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2521665
    :pswitch_5
    sget-object v0, LX/Hx4;->BOTTOM_PADDING:LX/Hx4;

    invoke-virtual {v0}, LX/Hx4;->ordinal()I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_5
        :pswitch_4
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2521666
    sget-object v0, LX/Hx5;->h:[LX/Hx4;

    array-length v0, v0

    return v0
.end method

.method public final notifyDataSetChanged()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2521667
    invoke-super {p0}, LX/3Tf;->notifyDataSetChanged()V

    .line 2521668
    iput v0, p0, LX/Hx5;->y:I

    .line 2521669
    iput v0, p0, LX/Hx5;->z:I

    .line 2521670
    const/4 v0, 0x0

    iput-object v0, p0, LX/Hx5;->A:Ljava/lang/Object;

    .line 2521671
    return-void
.end method
