.class public final LX/IWg;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 2584627
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_7

    .line 2584628
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2584629
    :goto_0
    return v1

    .line 2584630
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2584631
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_6

    .line 2584632
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 2584633
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2584634
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_1

    if-eqz v6, :cond_1

    .line 2584635
    const-string v7, "album_cover_photo"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 2584636
    const/4 v6, 0x0

    .line 2584637
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v7, :cond_b

    .line 2584638
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2584639
    :goto_2
    move v5, v6

    .line 2584640
    goto :goto_1

    .line 2584641
    :cond_2
    const-string v7, "id"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 2584642
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 2584643
    :cond_3
    const-string v7, "media"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 2584644
    invoke-static {p0, p1}, LX/IWf;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 2584645
    :cond_4
    const-string v7, "media_owner_object"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 2584646
    invoke-static {p0, p1}, LX/IWd;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 2584647
    :cond_5
    const-string v7, "title"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2584648
    const/4 v6, 0x0

    .line 2584649
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v7, :cond_13

    .line 2584650
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2584651
    :goto_3
    move v0, v6

    .line 2584652
    goto :goto_1

    .line 2584653
    :cond_6
    const/4 v6, 0x5

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 2584654
    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 2584655
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 2584656
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 2584657
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2584658
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2584659
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_7
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto/16 :goto_1

    .line 2584660
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2584661
    :cond_9
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_a

    .line 2584662
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 2584663
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2584664
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_9

    if-eqz v7, :cond_9

    .line 2584665
    const-string v8, "image"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 2584666
    const/4 v7, 0x0

    .line 2584667
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v8, :cond_f

    .line 2584668
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2584669
    :goto_5
    move v5, v7

    .line 2584670
    goto :goto_4

    .line 2584671
    :cond_a
    const/4 v7, 0x1

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 2584672
    invoke-virtual {p1, v6, v5}, LX/186;->b(II)V

    .line 2584673
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto/16 :goto_2

    :cond_b
    move v5, v6

    goto :goto_4

    .line 2584674
    :cond_c
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2584675
    :cond_d
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_e

    .line 2584676
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 2584677
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2584678
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_d

    if-eqz v8, :cond_d

    .line 2584679
    const-string v9, "uri"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_c

    .line 2584680
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_6

    .line 2584681
    :cond_e
    const/4 v8, 0x1

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 2584682
    invoke-virtual {p1, v7, v5}, LX/186;->b(II)V

    .line 2584683
    invoke-virtual {p1}, LX/186;->d()I

    move-result v7

    goto :goto_5

    :cond_f
    move v5, v7

    goto :goto_6

    .line 2584684
    :cond_10
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2584685
    :cond_11
    :goto_7
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_12

    .line 2584686
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 2584687
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2584688
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_11

    if-eqz v7, :cond_11

    .line 2584689
    const-string v8, "text"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_10

    .line 2584690
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_7

    .line 2584691
    :cond_12
    const/4 v7, 0x1

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 2584692
    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 2584693
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto/16 :goto_3

    :cond_13
    move v0, v6

    goto :goto_7
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2584694
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2584695
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2584696
    if-eqz v0, :cond_2

    .line 2584697
    const-string v1, "album_cover_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2584698
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2584699
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 2584700
    if-eqz v1, :cond_1

    .line 2584701
    const-string v2, "image"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2584702
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2584703
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2584704
    if-eqz v2, :cond_0

    .line 2584705
    const-string v0, "uri"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2584706
    invoke-virtual {p2, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2584707
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2584708
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2584709
    :cond_2
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2584710
    if-eqz v0, :cond_3

    .line 2584711
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2584712
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2584713
    :cond_3
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2584714
    if-eqz v0, :cond_4

    .line 2584715
    const-string v1, "media"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2584716
    invoke-static {p0, v0, p2, p3}, LX/IWf;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2584717
    :cond_4
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2584718
    if-eqz v0, :cond_5

    .line 2584719
    const-string v1, "media_owner_object"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2584720
    invoke-static {p0, v0, p2}, LX/IWd;->a(LX/15i;ILX/0nX;)V

    .line 2584721
    :cond_5
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2584722
    if-eqz v0, :cond_7

    .line 2584723
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2584724
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2584725
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2584726
    if-eqz v1, :cond_6

    .line 2584727
    const-string v2, "text"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2584728
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2584729
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2584730
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2584731
    return-void
.end method
