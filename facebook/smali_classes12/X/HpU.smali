.class public final LX/HpU;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2509497
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_8

    .line 2509498
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2509499
    :goto_0
    return v1

    .line 2509500
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_5

    .line 2509501
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 2509502
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2509503
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_0

    if-eqz v8, :cond_0

    .line 2509504
    const-string v9, "height"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 2509505
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v7, v3

    move v3, v2

    goto :goto_1

    .line 2509506
    :cond_1
    const-string v9, "name"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 2509507
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 2509508
    :cond_2
    const-string v9, "uri"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 2509509
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 2509510
    :cond_3
    const-string v9, "width"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 2509511
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v4, v0

    move v0, v2

    goto :goto_1

    .line 2509512
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2509513
    :cond_5
    const/4 v8, 0x4

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 2509514
    if-eqz v3, :cond_6

    .line 2509515
    invoke-virtual {p1, v1, v7, v1}, LX/186;->a(III)V

    .line 2509516
    :cond_6
    invoke-virtual {p1, v2, v6}, LX/186;->b(II)V

    .line 2509517
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v5}, LX/186;->b(II)V

    .line 2509518
    if-eqz v0, :cond_7

    .line 2509519
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v4, v1}, LX/186;->a(III)V

    .line 2509520
    :cond_7
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_8
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2509521
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2509522
    invoke-virtual {p0, p1, v2, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2509523
    if-eqz v0, :cond_0

    .line 2509524
    const-string v1, "height"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2509525
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2509526
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2509527
    if-eqz v0, :cond_1

    .line 2509528
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2509529
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2509530
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2509531
    if-eqz v0, :cond_2

    .line 2509532
    const-string v1, "uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2509533
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2509534
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2509535
    if-eqz v0, :cond_3

    .line 2509536
    const-string v1, "width"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2509537
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2509538
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2509539
    return-void
.end method
