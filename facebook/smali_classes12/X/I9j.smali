.class public final synthetic LX/I9j;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I

.field public static final synthetic b:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2543426
    invoke-static {}, LX/IA5;->values()[LX/IA5;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/I9j;->b:[I

    :try_start_0
    sget-object v0, LX/I9j;->b:[I

    sget-object v1, LX/IA5;->FRIENDS:LX/IA5;

    invoke-virtual {v1}, LX/IA5;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_d

    :goto_0
    :try_start_1
    sget-object v0, LX/I9j;->b:[I

    sget-object v1, LX/IA5;->NON_FRIENDS:LX/IA5;

    invoke-virtual {v1}, LX/IA5;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_c

    :goto_1
    :try_start_2
    sget-object v0, LX/I9j;->b:[I

    sget-object v1, LX/IA5;->EMAILS:LX/IA5;

    invoke-virtual {v1}, LX/IA5;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_b

    :goto_2
    :try_start_3
    sget-object v0, LX/I9j;->b:[I

    sget-object v1, LX/IA5;->SMS:LX/IA5;

    invoke-virtual {v1}, LX/IA5;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_a

    :goto_3
    :try_start_4
    sget-object v0, LX/I9j;->b:[I

    sget-object v1, LX/IA5;->TODAY:LX/IA5;

    invoke-virtual {v1}, LX/IA5;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_9

    :goto_4
    :try_start_5
    sget-object v0, LX/I9j;->b:[I

    sget-object v1, LX/IA5;->YESTERDAY:LX/IA5;

    invoke-virtual {v1}, LX/IA5;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_8

    :goto_5
    :try_start_6
    sget-object v0, LX/I9j;->b:[I

    sget-object v1, LX/IA5;->EARLIER:LX/IA5;

    invoke-virtual {v1}, LX/IA5;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_7

    .line 2543427
    :goto_6
    invoke-static {}, LX/Blc;->values()[LX/Blc;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/I9j;->a:[I

    :try_start_7
    sget-object v0, LX/I9j;->a:[I

    sget-object v1, LX/Blc;->PRIVATE_GOING:LX/Blc;

    invoke-virtual {v1}, LX/Blc;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_6

    :goto_7
    :try_start_8
    sget-object v0, LX/I9j;->a:[I

    sget-object v1, LX/Blc;->PUBLIC_GOING:LX/Blc;

    invoke-virtual {v1}, LX/Blc;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_5

    :goto_8
    :try_start_9
    sget-object v0, LX/I9j;->a:[I

    sget-object v1, LX/Blc;->PRIVATE_MAYBE:LX/Blc;

    invoke-virtual {v1}, LX/Blc;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_4

    :goto_9
    :try_start_a
    sget-object v0, LX/I9j;->a:[I

    sget-object v1, LX/Blc;->PUBLIC_WATCHED:LX/Blc;

    invoke-virtual {v1}, LX/Blc;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_3

    :goto_a
    :try_start_b
    sget-object v0, LX/I9j;->a:[I

    sget-object v1, LX/Blc;->PRIVATE_INVITED:LX/Blc;

    invoke-virtual {v1}, LX/Blc;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_2

    :goto_b
    :try_start_c
    sget-object v0, LX/I9j;->a:[I

    sget-object v1, LX/Blc;->PUBLIC_INVITED:LX/Blc;

    invoke-virtual {v1}, LX/Blc;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_1

    :goto_c
    :try_start_d
    sget-object v0, LX/I9j;->a:[I

    sget-object v1, LX/Blc;->PRIVATE_NOT_GOING:LX/Blc;

    invoke-virtual {v1}, LX/Blc;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_0

    :goto_d
    return-void

    :catch_0
    goto :goto_d

    :catch_1
    goto :goto_c

    :catch_2
    goto :goto_b

    :catch_3
    goto :goto_a

    :catch_4
    goto :goto_9

    :catch_5
    goto :goto_8

    :catch_6
    goto :goto_7

    :catch_7
    goto :goto_6

    :catch_8
    goto :goto_5

    :catch_9
    goto :goto_4

    :catch_a
    goto/16 :goto_3

    :catch_b
    goto/16 :goto_2

    :catch_c
    goto/16 :goto_1

    :catch_d
    goto/16 :goto_0
.end method
