.class public final LX/I5D;
.super LX/79T;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/common/callercontext/CallerContext;

.field public final synthetic b:LX/0Vd;

.field public final synthetic c:LX/I5E;


# direct methods
.method public constructor <init>(LX/I5E;Lcom/facebook/common/callercontext/CallerContext;LX/0Vd;)V
    .locals 0

    .prologue
    .line 2535188
    iput-object p1, p0, LX/I5D;->c:LX/I5E;

    iput-object p2, p0, LX/I5D;->a:Lcom/facebook/common/callercontext/CallerContext;

    iput-object p3, p0, LX/I5D;->b:LX/0Vd;

    invoke-direct {p0}, LX/79T;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 2535189
    iget-object v0, p0, LX/I5D;->c:LX/I5E;

    iget-object v0, v0, LX/I5E;->e:LX/1Ck;

    const-string v1, "event_discovery_get_location_task_key"

    invoke-virtual {v0, v1}, LX/1Ck;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2535190
    iget-object v0, p0, LX/I5D;->c:LX/I5E;

    iget-object v0, v0, LX/I5E;->e:LX/1Ck;

    const-string v1, "event_discovery_get_location_task_key"

    invoke-virtual {v0, v1}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2535191
    :cond_0
    iget-object v0, p0, LX/I5D;->c:LX/I5E;

    iget-object v0, v0, LX/I5E;->d:LX/1sS;

    invoke-virtual {v0}, LX/0SQ;->isDone()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2535192
    iget-object v0, p0, LX/I5D;->c:LX/I5E;

    iget-object v0, v0, LX/I5E;->d:LX/1sS;

    iget-object v1, p0, LX/I5D;->c:LX/I5E;

    iget-object v1, v1, LX/I5E;->b:Lcom/facebook/location/FbLocationOperationParams;

    iget-object v2, p0, LX/I5D;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, LX/1sS;->a(Lcom/facebook/location/FbLocationOperationParams;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2535193
    iget-object v0, p0, LX/I5D;->c:LX/I5E;

    iget-object v0, v0, LX/I5E;->e:LX/1Ck;

    const-string v1, "event_discovery_get_location_task_key"

    new-instance v2, LX/I5C;

    invoke-direct {v2, p0}, LX/I5C;-><init>(LX/I5D;)V

    iget-object v3, p0, LX/I5D;->b:LX/0Vd;

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2535194
    :goto_0
    return-void

    .line 2535195
    :cond_1
    iget-object v0, p0, LX/I5D;->b:LX/0Vd;

    iget-object v1, p0, LX/I5D;->c:LX/I5E;

    iget-object v1, v1, LX/I5E;->d:LX/1sS;

    invoke-virtual {v1}, LX/1sS;->a()Lcom/facebook/location/ImmutableLocation;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Vd;->onSuccess(Ljava/lang/Object;)V

    goto :goto_0
.end method
