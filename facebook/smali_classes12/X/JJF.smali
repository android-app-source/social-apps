.class public LX/JJF;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/2eZ;


# instance fields
.field public a:LX/23P;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Landroid/graphics/Paint;

.field public d:Landroid/text/SpannableStringBuilder;

.field public e:Landroid/text/style/ForegroundColorSpan;

.field public f:Lcom/facebook/resources/ui/FbTextView;

.field public g:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

.field public h:Lcom/facebook/resources/ui/FbTextView;

.field public i:Lcom/facebook/widget/FlowLayout;

.field public j:LX/JJE;

.field public k:Landroid/widget/ProgressBar;

.field public l:Lcom/facebook/resources/ui/FbTextView;

.field public m:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

.field public n:Ljava/lang/CharSequence;

.field public o:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2679121
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2679122
    const/16 p1, 0x8

    .line 2679123
    const-class v0, LX/JJF;

    invoke-static {v0, p0}, LX/JJF;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2679124
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/JJF;->c:Landroid/graphics/Paint;

    .line 2679125
    iget-object v0, p0, LX/JJF;->c:Landroid/graphics/Paint;

    invoke-virtual {p0}, LX/JJF;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a009e

    invoke-static {v1, v2}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2679126
    iget-object v0, p0, LX/JJF;->c:Landroid/graphics/Paint;

    invoke-virtual {p0}, LX/JJF;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0033

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2679127
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    iput-object v0, p0, LX/JJF;->d:Landroid/text/SpannableStringBuilder;

    .line 2679128
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {p0}, LX/JJF;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a00a4

    invoke-static {v1, v2}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    iput-object v0, p0, LX/JJF;->e:Landroid/text/style/ForegroundColorSpan;

    .line 2679129
    invoke-virtual {p0}, LX/JJF;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0303bf

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2679130
    const v0, 0x7f0d02c4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/JJF;->f:Lcom/facebook/resources/ui/FbTextView;

    .line 2679131
    const v0, 0x7f0d0bc7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    iput-object v0, p0, LX/JJF;->g:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    .line 2679132
    const v0, 0x7f0d02c3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/JJF;->h:Lcom/facebook/resources/ui/FbTextView;

    .line 2679133
    const v0, 0x7f0d0bc8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FlowLayout;

    iput-object v0, p0, LX/JJF;->i:Lcom/facebook/widget/FlowLayout;

    .line 2679134
    new-instance v0, LX/JJE;

    invoke-virtual {p0}, LX/JJF;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/JJE;-><init>(Landroid/content/Context;)V

    .line 2679135
    invoke-virtual {p0}, LX/JJF;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f083a6b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/JJE;->setText(Ljava/lang/CharSequence;)V

    .line 2679136
    invoke-virtual {p0}, LX/JJF;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f02036d

    invoke-static {v1, v2}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/JJE;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2679137
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/JJE;->setEnabled(Z)V

    .line 2679138
    iget-object v1, p0, LX/JJF;->i:Lcom/facebook/widget/FlowLayout;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/FlowLayout;->addView(Landroid/view/View;)V

    .line 2679139
    move-object v0, v0

    .line 2679140
    iput-object v0, p0, LX/JJF;->j:LX/JJE;

    .line 2679141
    const v0, 0x7f0d05b0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, LX/JJF;->k:Landroid/widget/ProgressBar;

    .line 2679142
    const v0, 0x7f0d0bc9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/JJF;->l:Lcom/facebook/resources/ui/FbTextView;

    .line 2679143
    const v0, 0x7f0d0bca

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    iput-object v0, p0, LX/JJF;->m:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    .line 2679144
    invoke-virtual {p0}, LX/JJF;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f083a6e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/JJF;->n:Ljava/lang/CharSequence;

    .line 2679145
    iget-object v0, p0, LX/JJF;->g:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    iget-object v1, p0, LX/JJF;->a:LX/23P;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 2679146
    iget-object v0, p0, LX/JJF;->m:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    iget-object v1, p0, LX/JJF;->a:LX/23P;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 2679147
    iget-object v0, p0, LX/JJF;->b:LX/0Uh;

    const/16 v1, 0x24e

    invoke-virtual {v0, v1}, LX/0Uh;->a(I)LX/03R;

    move-result-object v0

    sget-object v1, LX/03R;->NO:LX/03R;

    if-ne v0, v1, :cond_0

    .line 2679148
    iget-object v0, p0, LX/JJF;->g:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setVisibility(I)V

    .line 2679149
    :cond_0
    iget-object v0, p0, LX/JJF;->b:LX/0Uh;

    const/16 v1, 0x24d

    invoke-virtual {v0, v1}, LX/0Uh;->a(I)LX/03R;

    move-result-object v0

    sget-object v1, LX/03R;->NO:LX/03R;

    if-ne v0, v1, :cond_1

    .line 2679150
    iget-object v0, p0, LX/JJF;->m:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setVisibility(I)V

    .line 2679151
    :cond_1
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/JJF;

    invoke-static {p0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v1

    check-cast v1, LX/23P;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object p0

    check-cast p0, LX/0Uh;

    iput-object v1, p1, LX/JJF;->a:LX/23P;

    iput-object p0, p1, LX/JJF;->b:LX/0Uh;

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 5

    .prologue
    .line 2679114
    iget-object v0, p0, LX/JJF;->d:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->clear()V

    .line 2679115
    iget-object v0, p0, LX/JJF;->d:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->clearSpans()V

    .line 2679116
    iget-object v0, p0, LX/JJF;->d:Landroid/text/SpannableStringBuilder;

    iget-object v1, p0, LX/JJF;->n:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2679117
    iget-object v0, p0, LX/JJF;->d:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    .line 2679118
    iget-object v1, p0, LX/JJF;->d:Landroid/text/SpannableStringBuilder;

    iget-object v2, p0, LX/JJF;->e:Landroid/text/style/ForegroundColorSpan;

    iget-object v3, p0, LX/JJF;->n:Ljava/lang/CharSequence;

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    const/16 v4, 0x21

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2679119
    iget-object v0, p0, LX/JJF;->m:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    iget-object v1, p0, LX/JJF;->d:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2679120
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2679113
    iget-boolean v0, p0, LX/JJF;->o:Z

    return v0
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 2679097
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 2679098
    invoke-virtual {p0}, LX/JJF;->getHeight()I

    move-result v0

    iget-object v1, p0, LX/JJF;->m:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->getHeight()I

    move-result v1

    sub-int/2addr v0, v1

    .line 2679099
    const/4 v1, 0x0

    int-to-float v2, v0

    invoke-virtual {p0}, LX/JJF;->getWidth()I

    move-result v3

    int-to-float v3, v3

    int-to-float v4, v0

    iget-object v5, p0, LX/JJF;->c:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2679100
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 2679111
    iget-object v0, p0, LX/JJF;->k:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2679112
    return-void
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 2679109
    iget-object v0, p0, LX/JJF;->j:LX/JJE;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/JJE;->setEnabled(Z)V

    .line 2679110
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x3dff78ea

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2679105
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onAttachedToWindow()V

    .line 2679106
    const/4 v1, 0x1

    .line 2679107
    iput-boolean v1, p0, LX/JJF;->o:Z

    .line 2679108
    const/16 v1, 0x2d

    const v2, -0x2136a0e8

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x713f5ddd

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2679101
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onDetachedFromWindow()V

    .line 2679102
    const/4 v1, 0x0

    .line 2679103
    iput-boolean v1, p0, LX/JJF;->o:Z

    .line 2679104
    const/16 v1, 0x2d

    const v2, -0x68a01394

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
