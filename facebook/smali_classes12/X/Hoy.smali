.class public LX/Hoy;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private b:Ljava/util/concurrent/Executor;

.field private c:LX/46n;

.field private d:LX/03V;

.field private e:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private f:LX/0kL;

.field private g:LX/Hou;

.field private h:LX/Hos;

.field private i:I

.field private j:I

.field private k:I

.field private l:I

.field private m:LX/6IE;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2507126
    const-class v0, LX/Hoy;

    sput-object v0, LX/Hoy;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/46n;LX/03V;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0kL;LX/Hou;LX/Hos;Ljava/util/concurrent/Executor;)V
    .locals 1
    .param p7    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, -0x1

    .line 2507127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2507128
    iput v0, p0, LX/Hoy;->i:I

    .line 2507129
    iput v0, p0, LX/Hoy;->j:I

    .line 2507130
    iput v0, p0, LX/Hoy;->k:I

    .line 2507131
    iput v0, p0, LX/Hoy;->l:I

    .line 2507132
    sget-object v0, LX/6IE;->HIGH:LX/6IE;

    iput-object v0, p0, LX/Hoy;->m:LX/6IE;

    .line 2507133
    iput-object p1, p0, LX/Hoy;->c:LX/46n;

    .line 2507134
    iput-object p2, p0, LX/Hoy;->d:LX/03V;

    .line 2507135
    iput-object p3, p0, LX/Hoy;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2507136
    iput-object p4, p0, LX/Hoy;->f:LX/0kL;

    .line 2507137
    iput-object p5, p0, LX/Hoy;->g:LX/Hou;

    .line 2507138
    iput-object p6, p0, LX/Hoy;->h:LX/Hos;

    .line 2507139
    iput-object p7, p0, LX/Hoy;->b:Ljava/util/concurrent/Executor;

    .line 2507140
    return-void
.end method
