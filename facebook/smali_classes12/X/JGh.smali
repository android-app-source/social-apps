.class public final LX/JGh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5rT;


# instance fields
.field public final synthetic a:LX/JGq;

.field private final b:I

.field private final c:F

.field private final d:F

.field private final e:Lcom/facebook/react/bridge/Callback;

.field private final f:[I


# direct methods
.method private constructor <init>(LX/JGq;IFFLcom/facebook/react/bridge/Callback;)V
    .locals 1

    .prologue
    .line 2668923
    iput-object p1, p0, LX/JGh;->a:LX/JGq;

    .line 2668924
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2668925
    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, LX/JGh;->f:[I

    .line 2668926
    iput p2, p0, LX/JGh;->b:I

    .line 2668927
    iput p3, p0, LX/JGh;->c:F

    .line 2668928
    iput p4, p0, LX/JGh;->d:F

    .line 2668929
    iput-object p5, p0, LX/JGh;->e:Lcom/facebook/react/bridge/Callback;

    .line 2668930
    return-void
.end method

.method public synthetic constructor <init>(LX/JGq;IFFLcom/facebook/react/bridge/Callback;B)V
    .locals 0

    .prologue
    .line 2668931
    invoke-direct/range {p0 .. p5}, LX/JGh;-><init>(LX/JGq;IFFLcom/facebook/react/bridge/Callback;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 15

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2668932
    :try_start_0
    iget-object v0, p0, LX/JGh;->a:LX/JGq;

    iget-object v0, v0, LX/JGq;->b:LX/JGd;

    iget v1, p0, LX/JGh;->b:I

    sget-object v2, LX/JGq;->a:[I

    invoke-virtual {v0, v1, v2}, LX/5qw;->a(I[I)V
    :try_end_0
    .catch LX/5qo; {:try_start_0 .. :try_end_0} :catch_0

    .line 2668933
    sget-object v0, LX/JGq;->a:[I

    aget v0, v0, v5

    int-to-float v6, v0

    .line 2668934
    sget-object v0, LX/JGq;->a:[I

    aget v0, v0, v4

    int-to-float v7, v0

    .line 2668935
    iget-object v0, p0, LX/JGh;->a:LX/JGq;

    iget-object v0, v0, LX/JGq;->b:LX/JGd;

    iget v1, p0, LX/JGh;->b:I

    invoke-virtual {v0, v1}, LX/JGd;->d(I)Landroid/view/View;

    move-result-object v0

    .line 2668936
    iget v1, p0, LX/JGh;->c:F

    iget v2, p0, LX/JGh;->d:F

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v3, p0, LX/JGh;->f:[I

    invoke-static {v1, v2, v0, v3}, LX/5rK;->a(FFLandroid/view/ViewGroup;[I)I

    move-result v2

    .line 2668937
    :try_start_1
    iget-object v0, p0, LX/JGh;->a:LX/JGq;

    iget-object v0, v0, LX/JGq;->b:LX/JGd;

    iget-object v1, p0, LX/JGh;->f:[I

    const/4 v3, 0x0

    aget v1, v1, v3

    sget-object v3, LX/JGq;->a:[I

    invoke-virtual {v0, v1, v3}, LX/5qw;->a(I[I)V
    :try_end_1
    .catch LX/5qo; {:try_start_1 .. :try_end_1} :catch_1

    .line 2668938
    sget-object v1, LX/JGu;->b:LX/JGu;

    .line 2668939
    iget-object v0, p0, LX/JGh;->f:[I

    aget v0, v0, v5

    if-ne v0, v2, :cond_0

    move v3, v4

    .line 2668940
    :goto_0
    if-nez v3, :cond_4

    .line 2668941
    iget-object v0, p0, LX/JGh;->a:LX/JGq;

    iget-object v0, v0, LX/JGq;->b:LX/JGd;

    iget-object v8, p0, LX/JGh;->f:[I

    aget v8, v8, v5

    invoke-virtual {v0, v8}, LX/JGd;->d(I)Landroid/view/View;

    move-result-object v0

    .line 2668942
    instance-of v8, v0, LX/JGs;

    if-eqz v8, :cond_4

    .line 2668943
    check-cast v0, LX/JGs;

    iget v1, p0, LX/JGh;->b:I

    .line 2668944
    iget-object v12, v0, LX/JGs;->l:[LX/JGu;

    array-length v13, v12

    const/4 v8, 0x0

    move v11, v8

    :goto_1
    if-ge v11, v13, :cond_6

    aget-object v8, v12, v11

    .line 2668945
    invoke-virtual {v8, v1}, LX/JGu;->a(I)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 2668946
    :goto_2
    move-object v0, v8

    .line 2668947
    :goto_3
    sget-object v1, LX/JGu;->b:LX/JGu;

    if-ne v0, v1, :cond_1

    move v1, v2

    .line 2668948
    :goto_4
    iget v2, v0, LX/JGu;->e:F

    move v2, v2

    .line 2668949
    sget-object v8, LX/JGq;->a:[I

    aget v8, v8, v5

    int-to-float v8, v8

    add-float/2addr v2, v8

    sub-float/2addr v2, v6

    invoke-static {v2}, LX/5r2;->c(F)F

    move-result v6

    .line 2668950
    iget v2, v0, LX/JGu;->f:F

    move v2, v2

    .line 2668951
    sget-object v8, LX/JGq;->a:[I

    aget v8, v8, v4

    int-to-float v8, v8

    add-float/2addr v2, v8

    sub-float/2addr v2, v7

    invoke-static {v2}, LX/5r2;->c(F)F

    move-result v7

    .line 2668952
    if-eqz v3, :cond_2

    sget-object v2, LX/JGq;->a:[I

    aget v2, v2, v9

    int-to-float v2, v2

    :goto_5
    invoke-static {v2}, LX/5r2;->c(F)F

    move-result v2

    .line 2668953
    if-eqz v3, :cond_3

    sget-object v0, LX/JGq;->a:[I

    aget v0, v0, v10

    int-to-float v0, v0

    :goto_6
    invoke-static {v0}, LX/5r2;->c(F)F

    move-result v0

    .line 2668954
    iget-object v3, p0, LX/JGh;->e:Lcom/facebook/react/bridge/Callback;

    const/4 v8, 0x5

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v8, v5

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v8, v4

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v8, v9

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v8, v10

    const/4 v1, 0x4

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    aput-object v0, v8, v1

    invoke-interface {v3, v8}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    .line 2668955
    :goto_7
    return-void

    .line 2668956
    :catch_0
    iget-object v0, p0, LX/JGh;->e:Lcom/facebook/react/bridge/Callback;

    new-array v1, v5, [Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    goto :goto_7

    .line 2668957
    :catch_1
    iget-object v0, p0, LX/JGh;->e:Lcom/facebook/react/bridge/Callback;

    new-array v1, v5, [Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    goto :goto_7

    :cond_0
    move v3, v5

    .line 2668958
    goto/16 :goto_0

    .line 2668959
    :cond_1
    iget v1, v0, LX/JGu;->c:I

    goto :goto_4

    .line 2668960
    :cond_2
    iget v2, v0, LX/JGu;->g:F

    move v2, v2

    .line 2668961
    iget v8, v0, LX/JGu;->e:F

    move v8, v8

    .line 2668962
    sub-float/2addr v2, v8

    goto :goto_5

    .line 2668963
    :cond_3
    iget v3, v0, LX/JGu;->h:F

    move v3, v3

    .line 2668964
    iget v8, v0, LX/JGu;->f:F

    move v0, v8

    .line 2668965
    sub-float v0, v3, v0

    goto :goto_6

    :cond_4
    move-object v0, v1

    goto/16 :goto_3

    .line 2668966
    :cond_5
    add-int/lit8 v8, v11, 0x1

    move v11, v8

    goto/16 :goto_1

    .line 2668967
    :cond_6
    sget-object v8, LX/JGu;->b:LX/JGu;

    goto/16 :goto_2
.end method
