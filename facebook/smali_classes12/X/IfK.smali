.class public final enum LX/IfK;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IfK;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IfK;

.field public static final enum ADDING_CONTACT:LX/IfK;

.field public static final enum CONTACT_ADDED:LX/IfK;

.field public static final enum NEW_CONTACT:LX/IfK;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2599542
    new-instance v0, LX/IfK;

    const-string v1, "NEW_CONTACT"

    invoke-direct {v0, v1, v2}, LX/IfK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IfK;->NEW_CONTACT:LX/IfK;

    .line 2599543
    new-instance v0, LX/IfK;

    const-string v1, "ADDING_CONTACT"

    invoke-direct {v0, v1, v3}, LX/IfK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IfK;->ADDING_CONTACT:LX/IfK;

    .line 2599544
    new-instance v0, LX/IfK;

    const-string v1, "CONTACT_ADDED"

    invoke-direct {v0, v1, v4}, LX/IfK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IfK;->CONTACT_ADDED:LX/IfK;

    .line 2599545
    const/4 v0, 0x3

    new-array v0, v0, [LX/IfK;

    sget-object v1, LX/IfK;->NEW_CONTACT:LX/IfK;

    aput-object v1, v0, v2

    sget-object v1, LX/IfK;->ADDING_CONTACT:LX/IfK;

    aput-object v1, v0, v3

    sget-object v1, LX/IfK;->CONTACT_ADDED:LX/IfK;

    aput-object v1, v0, v4

    sput-object v0, LX/IfK;->$VALUES:[LX/IfK;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2599547
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IfK;
    .locals 1

    .prologue
    .line 2599548
    const-class v0, LX/IfK;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IfK;

    return-object v0
.end method

.method public static values()[LX/IfK;
    .locals 1

    .prologue
    .line 2599546
    sget-object v0, LX/IfK;->$VALUES:[LX/IfK;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IfK;

    return-object v0
.end method
