.class public LX/IYg;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile f:LX/IYg;


# instance fields
.field public b:LX/03V;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CIs;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1vC;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1nD;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2587995
    const-class v0, LX/IYg;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/IYg;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Ot",
            "<",
            "LX/CIs;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1vC;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1nD;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2587996
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2587997
    iput-object p1, p0, LX/IYg;->b:LX/03V;

    .line 2587998
    iput-object p2, p0, LX/IYg;->c:LX/0Ot;

    .line 2587999
    iput-object p3, p0, LX/IYg;->d:LX/0Ot;

    .line 2588000
    iput-object p4, p0, LX/IYg;->e:LX/0Ot;

    .line 2588001
    sget-object v0, LX/0ax;->fs:Ljava/lang/String;

    const-string v1, "{place_id}"

    const-string v2, "{surface}"

    const-string v3, "{place_name}"

    const-string v4, "{ranking_data}"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/IYf;

    invoke-direct {v1, p0}, LX/IYf;-><init>(LX/IYg;)V

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;LX/47M;)V

    .line 2588002
    sget-object v0, LX/0ax;->fr:Ljava/lang/String;

    const-string v1, "{place_id}"

    const-string v2, "{surface}"

    const-string v3, "{place_name}"

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/IYf;

    invoke-direct {v1, p0}, LX/IYf;-><init>(LX/IYg;)V

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;LX/47M;)V

    .line 2588003
    return-void
.end method

.method public static a(LX/0QB;)LX/IYg;
    .locals 7

    .prologue
    .line 2588004
    sget-object v0, LX/IYg;->f:LX/IYg;

    if-nez v0, :cond_1

    .line 2588005
    const-class v1, LX/IYg;

    monitor-enter v1

    .line 2588006
    :try_start_0
    sget-object v0, LX/IYg;->f:LX/IYg;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2588007
    if-eqz v2, :cond_0

    .line 2588008
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2588009
    new-instance v4, LX/IYg;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    const/16 v5, 0x25ea

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x1067

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 p0, 0x1140

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v3, v5, v6, p0}, LX/IYg;-><init>(LX/03V;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2588010
    move-object v0, v4

    .line 2588011
    sput-object v0, LX/IYg;->f:LX/IYg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2588012
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2588013
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2588014
    :cond_1
    sget-object v0, LX/IYg;->f:LX/IYg;

    return-object v0

    .line 2588015
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2588016
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/IYg;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param

    .prologue
    .line 2588017
    iget-object v0, p0, LX/IYg;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CIs;

    sget-object v1, LX/8ci;->q:LX/8ci;

    invoke-virtual {v1}, LX/8ci;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v1, p3

    move-object v2, p4

    move-object v3, p1

    move-object v4, p2

    invoke-virtual/range {v0 .. v5}, LX/CIs;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2588018
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2588019
    const/4 v0, 0x1

    return v0
.end method
