.class public final LX/IJw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:J

.field public final synthetic b:Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;J)V
    .locals 0

    .prologue
    .line 2564722
    iput-object p1, p0, LX/IJw;->b:Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;

    iput-wide p2, p0, LX/IJw;->a:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4

    .prologue
    .line 2564723
    invoke-static {}, LX/9Mb;->a()LX/9Ma;

    move-result-object v0

    .line 2564724
    const-string v1, "group_id"

    iget-wide v2, p0, LX/IJw;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2564725
    iget-object v1, p0, LX/IJw;->b:Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;

    iget-object v1, v1, Lcom/facebook/groupcommerce/deep_link/ForSalePostSellComposerActivity;->t:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2564726
    invoke-direct {p0}, LX/IJw;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
