.class public final LX/J9d;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:I

.field public final synthetic c:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;ZI)V
    .locals 0

    .prologue
    .line 2653137
    iput-object p1, p0, LX/J9d;->c:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    iput-boolean p2, p0, LX/J9d;->a:Z

    iput p3, p0, LX/J9d;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2653128
    iget-object v0, p0, LX/J9d;->c:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    iget-object v0, v0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->l:LX/J90;

    iget-boolean v1, p0, LX/J9d;->a:Z

    iget-object v2, p0, LX/J9d;->c:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    iget-object v2, v2, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->G:Ljava/lang/String;

    iget-object v3, p0, LX/J9d;->c:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    iget-object v3, v3, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->H:Ljava/lang/String;

    iget v4, p0, LX/J9d;->b:I

    .line 2653129
    const/4 v5, 0x4

    move v5, v5

    .line 2653130
    invoke-virtual {v0, v2, v3, v4, v5}, LX/J90;->a(Ljava/lang/String;Ljava/lang/String;II)LX/JBp;

    move-result-object v6

    .line 2653131
    invoke-static {v6}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v6

    const-wide/32 v8, 0x15180

    invoke-virtual {v6, v8, v9}, LX/0zO;->a(J)LX/0zO;

    move-result-object v6

    .line 2653132
    if-eqz v1, :cond_0

    sget-object v7, LX/0zS;->d:LX/0zS;

    :goto_0
    move-object v7, v7

    .line 2653133
    invoke-virtual {v6, v7}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v6

    .line 2653134
    iget-object v7, v0, LX/J90;->a:LX/0tX;

    invoke-virtual {v7, v6}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v6

    .line 2653135
    invoke-static {v6}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    move-object v0, v6

    .line 2653136
    return-object v0

    :cond_0
    sget-object v7, LX/0zS;->a:LX/0zS;

    goto :goto_0
.end method
