.class public LX/JI6;
.super LX/En6;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/En6",
        "<",
        "Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;",
        ">;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/JI7;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/JI9;",
            ">;"
        }
    .end annotation
.end field

.field public volatile c:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/JI8;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/JIf;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/JIx;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/JIS;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2677379
    invoke-direct {p0}, LX/En6;-><init>()V

    .line 2677380
    return-void
.end method

.method private a(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;LX/EnL;LX/Eny;LX/Emj;Landroid/os/Bundle;)LX/Emg;
    .locals 10

    .prologue
    .line 2677365
    sget-object v0, LX/JI5;->a:[I

    invoke-virtual {p1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->b()Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2677366
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected bucket item type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->b()Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2677367
    :pswitch_0
    iget-object v1, p0, LX/JI6;->d:LX/JIf;

    const-string v0, "discovery_curation_logging_data"

    invoke-virtual {p5, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;

    invoke-virtual {v1, p1, p3, p4, v0}, LX/JIf;->a(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;LX/Eny;LX/Emj;Lcom/facebook/profile/discovery/model/DiscoveryCurationLoggingData;)LX/JIe;

    move-result-object v0

    .line 2677368
    :goto_0
    return-object v0

    .line 2677369
    :pswitch_1
    iget-object v0, p0, LX/JI6;->e:LX/JIx;

    .line 2677370
    new-instance v1, LX/JIw;

    invoke-direct {v1, p1, p2, p4}, LX/JIw;-><init>(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;LX/EnL;LX/Emj;)V

    .line 2677371
    const/16 v2, 0x1ad9

    invoke-static {v0, v2}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    const/16 v3, 0x369e

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0x259

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x2eb

    invoke-static {v0, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x2a6d

    invoke-static {v0, v6}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0x1430

    invoke-static {v0, v7}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const/16 v8, 0x1acf

    invoke-static {v0, v8}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    const-class v9, LX/Epu;

    invoke-interface {v0, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/Epu;

    const-class p0, LX/JIs;

    invoke-interface {v0, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p0

    check-cast p0, LX/JIs;

    const-class p3, Landroid/content/Context;

    invoke-interface {v0, p3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Landroid/content/Context;

    const/16 p5, 0x259

    invoke-static {v0, p5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p5

    .line 2677372
    iput-object v2, v1, LX/JIw;->a:LX/0Or;

    iput-object v3, v1, LX/JIw;->b:LX/0Or;

    iput-object v4, v1, LX/JIw;->c:LX/0Or;

    iput-object v5, v1, LX/JIw;->d:LX/0Or;

    iput-object v6, v1, LX/JIw;->e:LX/0Or;

    iput-object v7, v1, LX/JIw;->f:LX/0Or;

    iput-object v8, v1, LX/JIw;->g:LX/0Or;

    iput-object v9, v1, LX/JIw;->i:LX/Epu;

    iput-object p0, v1, LX/JIw;->j:LX/JIs;

    iput-object p3, v1, LX/JIw;->k:Landroid/content/Context;

    iput-object p5, v1, LX/JIw;->l:LX/0Ot;

    .line 2677373
    move-object v0, v1

    .line 2677374
    goto :goto_0

    .line 2677375
    :pswitch_2
    iget-object v0, p0, LX/JI6;->f:LX/JIS;

    .line 2677376
    new-instance v1, LX/JIR;

    const/16 v2, 0x455

    invoke-static {v0, v2}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-direct {v1, v2, p4, p1}, LX/JIR;-><init>(LX/0Or;LX/Emj;Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;)V

    .line 2677377
    move-object v0, v1

    .line 2677378
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(LX/0QB;)LX/JI6;
    .locals 9

    .prologue
    .line 2677352
    const-class v1, LX/JI6;

    monitor-enter v1

    .line 2677353
    :try_start_0
    sget-object v0, LX/JI6;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2677354
    sput-object v2, LX/JI6;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2677355
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2677356
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2677357
    new-instance v3, LX/JI6;

    invoke-direct {v3}, LX/JI6;-><init>()V

    .line 2677358
    const/16 v4, 0x1ac6

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x1ac8

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x1ac7

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const-class v7, LX/JIf;

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/JIf;

    const-class v8, LX/JIx;

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/JIx;

    const-class p0, LX/JIS;

    invoke-interface {v0, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p0

    check-cast p0, LX/JIS;

    .line 2677359
    iput-object v4, v3, LX/JI6;->a:LX/0Or;

    iput-object v5, v3, LX/JI6;->b:LX/0Or;

    iput-object v6, v3, LX/JI6;->c:LX/0Or;

    iput-object v7, v3, LX/JI6;->d:LX/JIf;

    iput-object v8, v3, LX/JI6;->e:LX/JIx;

    iput-object p0, v3, LX/JI6;->f:LX/JIS;

    .line 2677360
    move-object v0, v3

    .line 2677361
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2677362
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JI6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2677363
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2677364
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/Eo3;LX/EnL;LX/Enk;LX/Eny;LX/Emj;Landroid/os/Bundle;)LX/Emg;
    .locals 6

    .prologue
    .line 2677344
    move-object v1, p1

    check-cast v1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    move-object v0, p0

    move-object v2, p3

    move-object v3, p5

    move-object v4, p6

    move-object v5, p7

    invoke-direct/range {v0 .. v5}, LX/JI6;->a(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;LX/EnL;LX/Eny;LX/Emj;Landroid/os/Bundle;)LX/Emg;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)LX/EoJ;
    .locals 3

    .prologue
    .line 2677345
    check-cast p1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;

    .line 2677346
    sget-object v0, LX/JI5;->a:[I

    invoke-virtual {p1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->b()Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2677347
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected bucket item type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->b()Lcom/facebook/graphql/enums/GraphQLProfileDiscoveryBucketItemType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2677348
    :pswitch_0
    iget-object v0, p0, LX/JI6;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JI8;

    .line 2677349
    :goto_0
    return-object v0

    .line 2677350
    :pswitch_1
    iget-object v0, p0, LX/JI6;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JI9;

    goto :goto_0

    .line 2677351
    :pswitch_2
    iget-object v0, p0, LX/JI6;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JI7;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
