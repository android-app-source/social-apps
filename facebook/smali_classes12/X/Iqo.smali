.class public LX/Iqo;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/DhY;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:LX/DhZ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:F

.field public d:F

.field public e:F

.field public f:F


# direct methods
.method private constructor <init>(FFFFLX/DhY;LX/DhZ;)V
    .locals 0

    .prologue
    .line 2617271
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2617272
    iput p1, p0, LX/Iqo;->e:F

    .line 2617273
    iput p2, p0, LX/Iqo;->f:F

    .line 2617274
    iput p3, p0, LX/Iqo;->c:F

    .line 2617275
    iput p4, p0, LX/Iqo;->d:F

    .line 2617276
    iput-object p5, p0, LX/Iqo;->a:LX/DhY;

    .line 2617277
    iput-object p6, p0, LX/Iqo;->b:LX/DhZ;

    .line 2617278
    return-void
.end method

.method public static a(Lcom/facebook/messaging/montage/model/art/ArtAsset;)LX/Iqo;
    .locals 7

    .prologue
    .line 2617279
    new-instance v0, LX/Iqo;

    iget-object v1, p0, Lcom/facebook/messaging/montage/model/art/ArtAsset;->d:Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;

    iget v1, v1, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->a:F

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/art/ArtAsset;->d:Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;

    iget v2, v2, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->b:F

    iget-object v3, p0, Lcom/facebook/messaging/montage/model/art/ArtAsset;->d:Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;

    iget v3, v3, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->c:F

    iget-object v4, p0, Lcom/facebook/messaging/montage/model/art/ArtAsset;->d:Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;

    iget v4, v4, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->d:F

    iget-object v5, p0, Lcom/facebook/messaging/montage/model/art/ArtAsset;->d:Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;

    iget-object v5, v5, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->e:LX/DhY;

    iget-object v6, p0, Lcom/facebook/messaging/montage/model/art/ArtAsset;->d:Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;

    iget-object v6, v6, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->f:LX/DhZ;

    invoke-direct/range {v0 .. v6}, LX/Iqo;-><init>(FFFFLX/DhY;LX/DhZ;)V

    return-object v0
.end method

.method public static b(Lcom/facebook/messaging/montage/model/art/ArtAsset;)LX/Iqo;
    .locals 7

    .prologue
    .line 2617280
    new-instance v0, LX/Iqo;

    iget-object v1, p0, Lcom/facebook/messaging/montage/model/art/ArtAsset;->e:Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;

    iget v1, v1, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->a:F

    iget-object v2, p0, Lcom/facebook/messaging/montage/model/art/ArtAsset;->e:Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;

    iget v2, v2, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->b:F

    iget-object v3, p0, Lcom/facebook/messaging/montage/model/art/ArtAsset;->e:Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;

    iget v3, v3, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->c:F

    iget-object v4, p0, Lcom/facebook/messaging/montage/model/art/ArtAsset;->e:Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;

    iget v4, v4, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->d:F

    iget-object v5, p0, Lcom/facebook/messaging/montage/model/art/ArtAsset;->e:Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;

    iget-object v5, v5, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->e:LX/DhY;

    iget-object v6, p0, Lcom/facebook/messaging/montage/model/art/ArtAsset;->e:Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;

    iget-object v6, v6, Lcom/facebook/messaging/montage/model/art/ArtAssetDimensions;->f:LX/DhZ;

    invoke-direct/range {v0 .. v6}, LX/Iqo;-><init>(FFFFLX/DhY;LX/DhZ;)V

    return-object v0
.end method
