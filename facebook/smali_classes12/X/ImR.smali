.class public LX/ImR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/7GW;

.field private final c:LX/7GV;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0aG;

.field private final f:LX/7G2;

.field private final g:LX/2Sz;

.field private final h:LX/IzM;

.field private final i:LX/7G3;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2609833
    const-class v0, LX/ImR;

    sput-object v0, LX/ImR;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/7GW;LX/7GV;LX/0Or;LX/0aG;LX/7G2;LX/2Sz;LX/IzM;LX/7G3;)V
    .locals 0
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/payments/p2p/config/IsP2pPaymentsSyncProtocolEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7GW;",
            "LX/7GV;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0aG;",
            "LX/7G2;",
            "LX/2Sz;",
            "LX/IzM;",
            "LX/7G3;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2609834
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2609835
    iput-object p1, p0, LX/ImR;->b:LX/7GW;

    .line 2609836
    iput-object p2, p0, LX/ImR;->c:LX/7GV;

    .line 2609837
    iput-object p3, p0, LX/ImR;->d:LX/0Or;

    .line 2609838
    iput-object p4, p0, LX/ImR;->e:LX/0aG;

    .line 2609839
    iput-object p5, p0, LX/ImR;->f:LX/7G2;

    .line 2609840
    iput-object p6, p0, LX/ImR;->g:LX/2Sz;

    .line 2609841
    iput-object p7, p0, LX/ImR;->h:LX/IzM;

    .line 2609842
    iput-object p8, p0, LX/ImR;->i:LX/7G3;

    .line 2609843
    return-void
.end method
