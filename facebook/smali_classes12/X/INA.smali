.class public final enum LX/INA;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/INA;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/INA;

.field public static final enum ALL_GROUPS:LX/INA;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2570620
    new-instance v0, LX/INA;

    const-string v1, "ALL_GROUPS"

    invoke-direct {v0, v1, v2}, LX/INA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/INA;->ALL_GROUPS:LX/INA;

    .line 2570621
    const/4 v0, 0x1

    new-array v0, v0, [LX/INA;

    sget-object v1, LX/INA;->ALL_GROUPS:LX/INA;

    aput-object v1, v0, v2

    sput-object v0, LX/INA;->$VALUES:[LX/INA;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2570622
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/INA;
    .locals 1

    .prologue
    .line 2570623
    const-class v0, LX/INA;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/INA;

    return-object v0
.end method

.method public static values()[LX/INA;
    .locals 1

    .prologue
    .line 2570624
    sget-object v0, LX/INA;->$VALUES:[LX/INA;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/INA;

    return-object v0
.end method
