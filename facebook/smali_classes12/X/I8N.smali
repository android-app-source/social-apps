.class public LX/I8N;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field public final b:I

.field public final c:I


# direct methods
.method public constructor <init>(Ljava/lang/Object;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;I)V"
        }
    .end annotation

    .prologue
    .line 2541074
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2541075
    iput-object p1, p0, LX/I8N;->a:Ljava/lang/Object;

    .line 2541076
    const/4 v0, -0x1

    iput v0, p0, LX/I8N;->b:I

    .line 2541077
    iput p2, p0, LX/I8N;->c:I

    .line 2541078
    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;II)V"
        }
    .end annotation

    .prologue
    .line 2541079
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2541080
    iput-object p1, p0, LX/I8N;->a:Ljava/lang/Object;

    .line 2541081
    iput p2, p0, LX/I8N;->b:I

    .line 2541082
    iput p3, p0, LX/I8N;->c:I

    .line 2541083
    return-void
.end method


# virtual methods
.method public final a(I)I
    .locals 2

    .prologue
    .line 2541084
    iget v0, p0, LX/I8N;->b:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2541085
    iget v0, p0, LX/I8N;->b:I

    sub-int v0, p1, v0

    return v0

    .line 2541086
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()LX/1Cw;
    .locals 1

    .prologue
    .line 2541087
    iget-object v0, p0, LX/I8N;->a:Ljava/lang/Object;

    check-cast v0, LX/1Cw;

    return-object v0
.end method

.method public final a(II)Landroid/util/Pair;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2541088
    if-ge p2, p1, :cond_0

    move-object v0, v1

    .line 2541089
    :goto_0
    return-object v0

    .line 2541090
    :cond_0
    iget v0, p0, LX/I8N;->b:I

    sub-int v0, p1, v0

    .line 2541091
    iget v2, p0, LX/I8N;->b:I

    sub-int v2, p2, v2

    .line 2541092
    if-gez v2, :cond_1

    move-object v0, v1

    .line 2541093
    goto :goto_0

    .line 2541094
    :cond_1
    new-instance v1, Landroid/util/Pair;

    if-gez v0, :cond_2

    const/4 v0, 0x0

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public final b()LX/1OM;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<VH:",
            "LX/1a1;",
            ">()",
            "LX/1OM",
            "<TVH;>;"
        }
    .end annotation

    .prologue
    .line 2541095
    iget-object v0, p0, LX/I8N;->a:Ljava/lang/Object;

    check-cast v0, LX/1OM;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2541096
    iget-object v0, p0, LX/I8N;->a:Ljava/lang/Object;

    instance-of v0, v0, LX/1OM;

    return v0
.end method
