.class public LX/Hgt;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;


# instance fields
.field public final d:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2494714
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "work_login/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 2494715
    sput-object v0, LX/Hgt;->a:LX/0Tn;

    const-string v1, "post_login_nux_seen"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Hgt;->b:LX/0Tn;

    .line 2494716
    sget-object v0, LX/Hgt;->a:LX/0Tn;

    const-string v1, "skip_post_login_nuxes"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Hgt;->c:LX/0Tn;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2494722
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2494723
    iput-object p1, p0, LX/Hgt;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2494724
    return-void
.end method

.method public static a(LX/0QB;)LX/Hgt;
    .locals 1

    .prologue
    .line 2494721
    invoke-static {p0}, LX/Hgt;->b(LX/0QB;)LX/Hgt;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/Hgt;Landroid/content/Context;LX/Hgs;)Z
    .locals 3

    .prologue
    .line 2494725
    iget-object v0, p0, LX/Hgt;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/Hgt;->c:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    move v0, v0

    .line 2494726
    if-nez v0, :cond_0

    .line 2494727
    iget-object v0, p0, LX/Hgt;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/Hgt;->b:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    move v0, v0

    .line 2494728
    if-eqz v0, :cond_1

    .line 2494729
    :cond_0
    const/4 v0, 0x0

    .line 2494730
    :goto_0
    return v0

    .line 2494731
    :cond_1
    invoke-static {p0}, LX/Hgt;->c(LX/Hgt;)V

    .line 2494732
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2494733
    iget v1, p2, LX/Hgs;->layout:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 2494734
    const v0, 0x7f0d266e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 2494735
    new-instance v2, LX/0ju;

    invoke-direct {v2, p1}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v1}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    move-result-object v1

    new-instance v2, LX/Hgq;

    invoke-direct {v2, p0}, LX/Hgq;-><init>(LX/Hgt;)V

    invoke-virtual {v1, v2}, LX/0ju;->a(Landroid/content/DialogInterface$OnDismissListener;)LX/0ju;

    move-result-object v1

    invoke-virtual {v1}, LX/0ju;->a()LX/2EJ;

    move-result-object v1

    .line 2494736
    new-instance v2, LX/Hgr;

    invoke-direct {v2, p0, v1}, LX/Hgr;-><init>(LX/Hgt;LX/2EJ;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2494737
    invoke-virtual {v1}, LX/2EJ;->show()V

    .line 2494738
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/Hgt;
    .locals 2

    .prologue
    .line 2494719
    new-instance v1, LX/Hgt;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v1, v0}, LX/Hgt;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 2494720
    return-object v1
.end method

.method public static c(LX/Hgt;)V
    .locals 3

    .prologue
    .line 2494717
    iget-object v0, p0, LX/Hgt;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/Hgt;->b:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2494718
    return-void
.end method
