.class public final LX/Hoh;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 25

    .prologue
    .line 2506680
    const/16 v21, 0x0

    .line 2506681
    const/16 v20, 0x0

    .line 2506682
    const/16 v19, 0x0

    .line 2506683
    const/16 v18, 0x0

    .line 2506684
    const/16 v17, 0x0

    .line 2506685
    const/16 v16, 0x0

    .line 2506686
    const/4 v15, 0x0

    .line 2506687
    const/4 v14, 0x0

    .line 2506688
    const/4 v13, 0x0

    .line 2506689
    const/4 v12, 0x0

    .line 2506690
    const/4 v11, 0x0

    .line 2506691
    const/4 v10, 0x0

    .line 2506692
    const/4 v9, 0x0

    .line 2506693
    const/4 v8, 0x0

    .line 2506694
    const/4 v7, 0x0

    .line 2506695
    const/4 v6, 0x0

    .line 2506696
    const/4 v5, 0x0

    .line 2506697
    const/4 v4, 0x0

    .line 2506698
    const/4 v3, 0x0

    .line 2506699
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v22

    sget-object v23, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    if-eq v0, v1, :cond_1

    .line 2506700
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2506701
    const/4 v3, 0x0

    .line 2506702
    :goto_0
    return v3

    .line 2506703
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2506704
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v22

    sget-object v23, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    if-eq v0, v1, :cond_11

    .line 2506705
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v22

    .line 2506706
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2506707
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v23

    sget-object v24, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-eq v0, v1, :cond_1

    if-eqz v22, :cond_1

    .line 2506708
    const-string v23, "currency"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_2

    .line 2506709
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v21

    goto :goto_1

    .line 2506710
    :cond_2
    const-string v23, "formatted_shipping_address"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_3

    .line 2506711
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v20

    goto :goto_1

    .line 2506712
    :cond_3
    const-string v23, "id"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_4

    .line 2506713
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v19

    goto :goto_1

    .line 2506714
    :cond_4
    const-string v23, "page"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_5

    .line 2506715
    invoke-static/range {p0 .. p1}, LX/Hod;->a(LX/15w;LX/186;)I

    move-result v18

    goto :goto_1

    .line 2506716
    :cond_5
    const-string v23, "platform_context"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_6

    .line 2506717
    invoke-static/range {p0 .. p1}, LX/Hoe;->a(LX/15w;LX/186;)I

    move-result v17

    goto :goto_1

    .line 2506718
    :cond_6
    const-string v23, "receipt_image"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_7

    .line 2506719
    invoke-static/range {p0 .. p1}, LX/Hog;->a(LX/15w;LX/186;)I

    move-result v16

    goto/16 :goto_1

    .line 2506720
    :cond_7
    const-string v23, "selected_transaction_payment_option"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_8

    .line 2506721
    invoke-static/range {p0 .. p1}, LX/Hol;->a(LX/15w;LX/186;)I

    move-result v15

    goto/16 :goto_1

    .line 2506722
    :cond_8
    const-string v23, "selected_transaction_shipping_option"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_9

    .line 2506723
    invoke-static/range {p0 .. p1}, LX/Hom;->a(LX/15w;LX/186;)I

    move-result v14

    goto/16 :goto_1

    .line 2506724
    :cond_9
    const-string v23, "transaction_discount"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_a

    .line 2506725
    const/4 v5, 0x1

    .line 2506726
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v13

    goto/16 :goto_1

    .line 2506727
    :cond_a
    const-string v23, "transaction_payment_receipt_display"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_b

    .line 2506728
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    goto/16 :goto_1

    .line 2506729
    :cond_b
    const-string v23, "transaction_products"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_c

    .line 2506730
    invoke-static/range {p0 .. p1}, LX/Hok;->a(LX/15w;LX/186;)I

    move-result v11

    goto/16 :goto_1

    .line 2506731
    :cond_c
    const-string v23, "transaction_shipment_receipt_display"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_d

    .line 2506732
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto/16 :goto_1

    .line 2506733
    :cond_d
    const-string v23, "transaction_status"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_e

    .line 2506734
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v9

    goto/16 :goto_1

    .line 2506735
    :cond_e
    const-string v23, "transaction_status_display"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_f

    .line 2506736
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto/16 :goto_1

    .line 2506737
    :cond_f
    const-string v23, "transaction_subtotal_cost"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_10

    .line 2506738
    const/4 v4, 0x1

    .line 2506739
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v7

    goto/16 :goto_1

    .line 2506740
    :cond_10
    const-string v23, "transaction_total_cost"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_0

    .line 2506741
    const/4 v3, 0x1

    .line 2506742
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    goto/16 :goto_1

    .line 2506743
    :cond_11
    const/16 v22, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 2506744
    const/16 v22, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v22

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2506745
    const/16 v21, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2506746
    const/16 v20, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2506747
    const/16 v19, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2506748
    const/16 v18, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2506749
    const/16 v17, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2506750
    const/16 v16, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1, v15}, LX/186;->b(II)V

    .line 2506751
    const/4 v15, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v14}, LX/186;->b(II)V

    .line 2506752
    if-eqz v5, :cond_12

    .line 2506753
    const/16 v5, 0x8

    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v13, v14}, LX/186;->a(III)V

    .line 2506754
    :cond_12
    const/16 v5, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v12}, LX/186;->b(II)V

    .line 2506755
    const/16 v5, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v11}, LX/186;->b(II)V

    .line 2506756
    const/16 v5, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v10}, LX/186;->b(II)V

    .line 2506757
    const/16 v5, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v9}, LX/186;->b(II)V

    .line 2506758
    const/16 v5, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v8}, LX/186;->b(II)V

    .line 2506759
    if-eqz v4, :cond_13

    .line 2506760
    const/16 v4, 0xe

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v7, v5}, LX/186;->a(III)V

    .line 2506761
    :cond_13
    if-eqz v3, :cond_14

    .line 2506762
    const/16 v3, 0xf

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6, v4}, LX/186;->a(III)V

    .line 2506763
    :cond_14
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 9

    .prologue
    const/16 v3, 0xc

    const/4 v2, 0x0

    .line 2506764
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2506765
    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2506766
    if-eqz v0, :cond_0

    .line 2506767
    const-string v1, "currency"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2506768
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2506769
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2506770
    if-eqz v0, :cond_1

    .line 2506771
    const-string v1, "formatted_shipping_address"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2506772
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2506773
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2506774
    if-eqz v0, :cond_2

    .line 2506775
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2506776
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2506777
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2506778
    if-eqz v0, :cond_3

    .line 2506779
    const-string v1, "page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2506780
    invoke-static {p0, v0, p2}, LX/Hod;->a(LX/15i;ILX/0nX;)V

    .line 2506781
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2506782
    if-eqz v0, :cond_4

    .line 2506783
    const-string v1, "platform_context"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2506784
    invoke-static {p0, v0, p2}, LX/Hoe;->a(LX/15i;ILX/0nX;)V

    .line 2506785
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2506786
    if-eqz v0, :cond_5

    .line 2506787
    const-string v1, "receipt_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2506788
    invoke-static {p0, v0, p2, p3}, LX/Hog;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2506789
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2506790
    if-eqz v0, :cond_6

    .line 2506791
    const-string v1, "selected_transaction_payment_option"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2506792
    invoke-static {p0, v0, p2}, LX/Hol;->a(LX/15i;ILX/0nX;)V

    .line 2506793
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2506794
    if-eqz v0, :cond_b

    .line 2506795
    const-string v1, "selected_transaction_shipping_option"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2506796
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2506797
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2506798
    if-eqz v1, :cond_7

    .line 2506799
    const-string v4, "name"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2506800
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2506801
    :cond_7
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 2506802
    if-eqz v1, :cond_a

    .line 2506803
    const-string v4, "shipping_price_and_currency"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2506804
    const/4 v4, 0x0

    .line 2506805
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2506806
    invoke-virtual {p0, v1, v4, v4}, LX/15i;->a(III)I

    move-result v4

    .line 2506807
    if-eqz v4, :cond_8

    .line 2506808
    const-string v0, "amount_in_hundredths"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2506809
    invoke-virtual {p2, v4}, LX/0nX;->b(I)V

    .line 2506810
    :cond_8
    const/4 v4, 0x1

    invoke-virtual {p0, v1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 2506811
    if-eqz v4, :cond_9

    .line 2506812
    const-string v0, "currency"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2506813
    invoke-virtual {p2, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2506814
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2506815
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2506816
    :cond_b
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2506817
    if-eqz v0, :cond_c

    .line 2506818
    const-string v1, "transaction_discount"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2506819
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2506820
    :cond_c
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2506821
    if-eqz v0, :cond_d

    .line 2506822
    const-string v1, "transaction_payment_receipt_display"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2506823
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2506824
    :cond_d
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2506825
    if-eqz v0, :cond_17

    .line 2506826
    const-string v1, "transaction_products"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2506827
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2506828
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 2506829
    if-eqz v1, :cond_16

    .line 2506830
    const-string v4, "edges"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2506831
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2506832
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result v5

    if-ge v4, v5, :cond_15

    .line 2506833
    invoke-virtual {p0, v1, v4}, LX/15i;->q(II)I

    move-result v5

    const/4 v8, 0x0

    .line 2506834
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2506835
    invoke-virtual {p0, v5, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v6

    .line 2506836
    if-eqz v6, :cond_e

    .line 2506837
    const-string v7, "name"

    invoke-virtual {p2, v7}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2506838
    invoke-virtual {p2, v6}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2506839
    :cond_e
    const/4 v6, 0x1

    invoke-virtual {p0, v5, v6}, LX/15i;->g(II)I

    move-result v6

    .line 2506840
    if-eqz v6, :cond_f

    .line 2506841
    const-string v7, "node"

    invoke-virtual {p2, v7}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2506842
    invoke-static {p0, v6, p2}, LX/Hoi;->a(LX/15i;ILX/0nX;)V

    .line 2506843
    :cond_f
    const/4 v6, 0x2

    invoke-virtual {p0, v5, v6, v8}, LX/15i;->a(III)I

    move-result v6

    .line 2506844
    if-eqz v6, :cond_10

    .line 2506845
    const-string v7, "per_unit_price"

    invoke-virtual {p2, v7}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2506846
    invoke-virtual {p2, v6}, LX/0nX;->b(I)V

    .line 2506847
    :cond_10
    const/4 v6, 0x3

    invoke-virtual {p0, v5, v6}, LX/15i;->g(II)I

    move-result v6

    .line 2506848
    if-eqz v6, :cond_13

    .line 2506849
    const-string v7, "product_image"

    invoke-virtual {p2, v7}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2506850
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2506851
    const/4 v7, 0x0

    invoke-virtual {p0, v6, v7}, LX/15i;->g(II)I

    move-result v7

    .line 2506852
    if-eqz v7, :cond_12

    .line 2506853
    const-string v0, "image"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2506854
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2506855
    const/4 v0, 0x0

    invoke-virtual {p0, v7, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2506856
    if-eqz v0, :cond_11

    .line 2506857
    const-string v6, "uri"

    invoke-virtual {p2, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2506858
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2506859
    :cond_11
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2506860
    :cond_12
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2506861
    :cond_13
    const/4 v6, 0x4

    invoke-virtual {p0, v5, v6, v8}, LX/15i;->a(III)I

    move-result v6

    .line 2506862
    if-eqz v6, :cond_14

    .line 2506863
    const-string v7, "quantity"

    invoke-virtual {p2, v7}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2506864
    invoke-virtual {p2, v6}, LX/0nX;->b(I)V

    .line 2506865
    :cond_14
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2506866
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    .line 2506867
    :cond_15
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2506868
    :cond_16
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2506869
    :cond_17
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2506870
    if-eqz v0, :cond_18

    .line 2506871
    const-string v1, "transaction_shipment_receipt_display"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2506872
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2506873
    :cond_18
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 2506874
    if-eqz v0, :cond_19

    .line 2506875
    const-string v0, "transaction_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2506876
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2506877
    :cond_19
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2506878
    if-eqz v0, :cond_1a

    .line 2506879
    const-string v1, "transaction_status_display"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2506880
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2506881
    :cond_1a
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2506882
    if-eqz v0, :cond_1b

    .line 2506883
    const-string v1, "transaction_subtotal_cost"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2506884
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2506885
    :cond_1b
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2506886
    if-eqz v0, :cond_1c

    .line 2506887
    const-string v1, "transaction_total_cost"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2506888
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2506889
    :cond_1c
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2506890
    return-void
.end method
