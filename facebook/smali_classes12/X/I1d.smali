.class public final LX/I1d;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/graphql/model/FeedUnit;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/I1f;


# direct methods
.method public constructor <init>(LX/I1f;)V
    .locals 0

    .prologue
    .line 2529130
    iput-object p1, p0, LX/I1d;->a:LX/I1f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2529129
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2529117
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2529118
    if-eqz p1, :cond_0

    .line 2529119
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2529120
    if-nez v0, :cond_1

    .line 2529121
    :cond_0
    :goto_0
    return-void

    .line 2529122
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2529123
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 2529124
    iget-object v1, p0, LX/I1d;->a:LX/I1f;

    iget-object v1, v1, LX/I1f;->c:LX/I1B;

    .line 2529125
    iget-object v2, v1, LX/I1B;->b:Ljava/util/HashMap;

    invoke-interface {v0}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2529126
    iget-object v2, v1, LX/I1B;->b:Ljava/util/HashMap;

    invoke-interface {v0}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 2529127
    iget-object p1, v1, LX/I1B;->a:Ljava/util/List;

    invoke-interface {p1, v2, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2529128
    :cond_2
    iget-object v0, p0, LX/I1d;->a:LX/I1f;

    invoke-static {v0}, LX/I1f;->i(LX/I1f;)V

    goto :goto_0
.end method
