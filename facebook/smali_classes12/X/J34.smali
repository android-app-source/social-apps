.class public final LX/J34;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public A:Ljava/lang/String;

.field public B:Ljava/lang/String;

.field public C:Z

.field public D:I
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation
.end field

.field public a:Z

.field public b:Z

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Z

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Z

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/String;

.field public q:Ljava/lang/String;

.field public r:Ljava/lang/String;

.field public s:Z

.field public t:Z

.field public u:Z

.field public v:Z

.field public w:Z

.field public x:Z

.field public y:Z

.field public z:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2642010
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/sample/PaymentsFlowSampleData;)LX/J34;
    .locals 1

    .prologue
    .line 2642011
    iget-boolean v0, p1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->a:Z

    .line 2642012
    iput-boolean v0, p0, LX/J34;->a:Z

    .line 2642013
    iget-boolean v0, p1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->b:Z

    .line 2642014
    iput-boolean v0, p0, LX/J34;->b:Z

    .line 2642015
    iget-object v0, p1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->c:Ljava/lang/String;

    .line 2642016
    iput-object v0, p0, LX/J34;->c:Ljava/lang/String;

    .line 2642017
    iget-object v0, p1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->d:Ljava/lang/String;

    .line 2642018
    iput-object v0, p0, LX/J34;->d:Ljava/lang/String;

    .line 2642019
    iget-object v0, p1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->e:Ljava/lang/String;

    .line 2642020
    iput-object v0, p0, LX/J34;->e:Ljava/lang/String;

    .line 2642021
    iget-object v0, p1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->f:Ljava/lang/String;

    .line 2642022
    iput-object v0, p0, LX/J34;->f:Ljava/lang/String;

    .line 2642023
    iget-boolean v0, p1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->g:Z

    .line 2642024
    iput-boolean v0, p0, LX/J34;->g:Z

    .line 2642025
    iget-object v0, p1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->h:Ljava/lang/String;

    .line 2642026
    iput-object v0, p0, LX/J34;->h:Ljava/lang/String;

    .line 2642027
    iget-object v0, p1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->i:Ljava/lang/String;

    .line 2642028
    iput-object v0, p0, LX/J34;->i:Ljava/lang/String;

    .line 2642029
    iget-object v0, p1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->j:Ljava/lang/String;

    .line 2642030
    iput-object v0, p0, LX/J34;->j:Ljava/lang/String;

    .line 2642031
    iget-object v0, p1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->k:Ljava/lang/String;

    .line 2642032
    iput-object v0, p0, LX/J34;->k:Ljava/lang/String;

    .line 2642033
    iget-object v0, p1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->l:Ljava/lang/String;

    .line 2642034
    iput-object v0, p0, LX/J34;->l:Ljava/lang/String;

    .line 2642035
    iget-boolean v0, p1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->m:Z

    .line 2642036
    iput-boolean v0, p0, LX/J34;->m:Z

    .line 2642037
    iget-object v0, p1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->n:Ljava/lang/String;

    .line 2642038
    iput-object v0, p0, LX/J34;->n:Ljava/lang/String;

    .line 2642039
    iget-object v0, p1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->o:Ljava/lang/String;

    .line 2642040
    iput-object v0, p0, LX/J34;->o:Ljava/lang/String;

    .line 2642041
    iget-object v0, p1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->p:Ljava/lang/String;

    .line 2642042
    iput-object v0, p0, LX/J34;->p:Ljava/lang/String;

    .line 2642043
    iget-object v0, p1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->q:Ljava/lang/String;

    .line 2642044
    iput-object v0, p0, LX/J34;->q:Ljava/lang/String;

    .line 2642045
    iget-object v0, p1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->r:Ljava/lang/String;

    .line 2642046
    iput-object v0, p0, LX/J34;->r:Ljava/lang/String;

    .line 2642047
    iget-boolean v0, p1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->s:Z

    .line 2642048
    iput-boolean v0, p0, LX/J34;->s:Z

    .line 2642049
    iget-boolean v0, p1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->t:Z

    .line 2642050
    iput-boolean v0, p0, LX/J34;->t:Z

    .line 2642051
    iget-boolean v0, p1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->u:Z

    .line 2642052
    iput-boolean v0, p0, LX/J34;->u:Z

    .line 2642053
    iget-boolean v0, p1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->v:Z

    .line 2642054
    iput-boolean v0, p0, LX/J34;->v:Z

    .line 2642055
    iget-boolean v0, p1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->w:Z

    .line 2642056
    iput-boolean v0, p0, LX/J34;->w:Z

    .line 2642057
    iget-boolean v0, p1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->x:Z

    .line 2642058
    iput-boolean v0, p0, LX/J34;->x:Z

    .line 2642059
    iget-boolean v0, p1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->y:Z

    .line 2642060
    iput-boolean v0, p0, LX/J34;->y:Z

    .line 2642061
    iget v0, p1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->z:I

    .line 2642062
    iput v0, p0, LX/J34;->z:I

    .line 2642063
    iget-object v0, p1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->A:Ljava/lang/String;

    .line 2642064
    iput-object v0, p0, LX/J34;->A:Ljava/lang/String;

    .line 2642065
    iget-object v0, p1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->B:Ljava/lang/String;

    .line 2642066
    iput-object v0, p0, LX/J34;->B:Ljava/lang/String;

    .line 2642067
    iget-boolean v0, p1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->C:Z

    .line 2642068
    iput-boolean v0, p0, LX/J34;->C:Z

    .line 2642069
    iget v0, p1, Lcom/facebook/payments/sample/PaymentsFlowSampleData;->D:I

    .line 2642070
    iput v0, p0, LX/J34;->D:I

    .line 2642071
    return-object p0
.end method

.method public final a()Lcom/facebook/payments/sample/PaymentsFlowSampleData;
    .locals 2

    .prologue
    .line 2642072
    new-instance v0, Lcom/facebook/payments/sample/PaymentsFlowSampleData;

    invoke-direct {v0, p0}, Lcom/facebook/payments/sample/PaymentsFlowSampleData;-><init>(LX/J34;)V

    return-object v0
.end method
