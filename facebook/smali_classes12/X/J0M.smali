.class public LX/J0M;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityParams;",
        "Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityResult;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile c:LX/J0M;


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2636875
    const-class v0, LX/J0M;

    sput-object v0, LX/J0M;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2636918
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2636919
    return-void
.end method

.method public static a(LX/0QB;)LX/J0M;
    .locals 3

    .prologue
    .line 2636906
    sget-object v0, LX/J0M;->c:LX/J0M;

    if-nez v0, :cond_1

    .line 2636907
    const-class v1, LX/J0M;

    monitor-enter v1

    .line 2636908
    :try_start_0
    sget-object v0, LX/J0M;->c:LX/J0M;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2636909
    if-eqz v2, :cond_0

    .line 2636910
    :try_start_1
    new-instance v0, LX/J0M;

    invoke-direct {v0}, LX/J0M;-><init>()V

    .line 2636911
    move-object v0, v0

    .line 2636912
    sput-object v0, LX/J0M;->c:LX/J0M;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2636913
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2636914
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2636915
    :cond_1
    sget-object v0, LX/J0M;->c:LX/J0M;

    return-object v0

    .line 2636916
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2636917
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 2636885
    check-cast p1, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityParams;

    .line 2636886
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 2636887
    const-string v1, "node(%s) { %s }"

    .line 2636888
    iget-object v2, p1, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityParams;->c:Ljava/lang/String;

    move-object v2, v2

    .line 2636889
    const-string v3, "can_viewer_send_money"

    invoke-static {v1, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/J0M;->a:Ljava/lang/String;

    .line 2636890
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "q"

    iget-object v3, p0, LX/J0M;->a:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2636891
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "fetchP2PSendEligibility"

    .line 2636892
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 2636893
    move-object v1, v1

    .line 2636894
    const-string v2, "GET"

    .line 2636895
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 2636896
    move-object v1, v1

    .line 2636897
    const-string v2, "graphql"

    .line 2636898
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 2636899
    move-object v1, v1

    .line 2636900
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 2636901
    move-object v0, v1

    .line 2636902
    sget-object v1, LX/14S;->JSONPARSER:LX/14S;

    .line 2636903
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2636904
    move-object v0, v0

    .line 2636905
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2636876
    check-cast p1, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityParams;

    .line 2636877
    invoke-virtual {p2}, LX/1pN;->e()LX/15w;

    move-result-object v0

    .line 2636878
    iget-object v1, p1, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityParams;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2636879
    invoke-virtual {v0}, LX/15w;->c()LX/15z;

    .line 2636880
    :goto_0
    invoke-virtual {v0}, LX/15w;->h()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2636881
    invoke-virtual {v0}, LX/15w;->c()LX/15z;

    goto :goto_0

    .line 2636882
    :cond_0
    invoke-virtual {v0}, LX/15w;->h()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2636883
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Response did not contain desired receiver ID"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2636884
    :cond_1
    invoke-virtual {p2}, LX/1pN;->e()LX/15w;

    move-result-object v0

    const-class v1, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityResult;

    invoke-virtual {v0, v1}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/service/model/eligibility/FetchP2PSendEligibilityResult;

    return-object v0
.end method
