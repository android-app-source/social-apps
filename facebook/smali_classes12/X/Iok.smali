.class public LX/Iok;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Ing;


# instance fields
.field private final a:LX/5fv;

.field private final b:LX/Ine;

.field private c:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

.field private d:LX/Io2;


# direct methods
.method public constructor <init>(LX/5fv;LX/Ine;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2612157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2612158
    iput-object p1, p0, LX/Iok;->a:LX/5fv;

    .line 2612159
    iput-object p2, p0, LX/Iok;->b:LX/Ine;

    .line 2612160
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 2612161
    return-void
.end method

.method public final a(LX/Io2;)V
    .locals 0

    .prologue
    .line 2612162
    iput-object p1, p0, LX/Iok;->d:LX/Io2;

    .line 2612163
    return-void
.end method

.method public final a(Landroid/os/Bundle;Lcom/facebook/messaging/payment/value/input/MessengerPayData;)V
    .locals 12

    .prologue
    const/4 v3, 0x0

    .line 2612164
    iput-object p2, p0, LX/Iok;->c:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2612165
    const-string v0, "orion_messenger_pay_params"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/payment/value/input/OrionMessengerPayParams;

    .line 2612166
    const-string v1, "payment_flow_type"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, LX/5g0;

    .line 2612167
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 2612168
    const-string v2, "nux_follow_up_action"

    .line 2612169
    iget-object v5, p2, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->w:Lcom/facebook/payments/auth/model/NuxFollowUpAction;

    move-object v5, v5

    .line 2612170
    invoke-virtual {v4, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2612171
    :try_start_0
    iget-object v2, p0, LX/Iok;->a:LX/5fv;

    iget-object v5, p0, LX/Iok;->c:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2612172
    iget-object v6, v5, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->r:Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    move-object v5, v6

    .line 2612173
    iget-object v6, v5, Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;->b:Ljava/lang/String;

    move-object v5, v6

    .line 2612174
    iget-object v6, p0, LX/Iok;->c:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2612175
    iget-object v7, v6, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->r:Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    move-object v6, v7

    .line 2612176
    iget-object v7, v6, Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;->a:Ljava/lang/String;

    move-object v6, v7

    .line 2612177
    invoke-virtual {v2, v5, v6}, LX/5fv;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/payments/currency/CurrencyAmount;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 2612178
    iget-object v2, p0, LX/Iok;->c:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2612179
    iget-object v6, v2, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->f:LX/0am;

    move-object v2, v6

    .line 2612180
    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/payments/p2p/model/PaymentCard;

    .line 2612181
    iget-wide v10, v2, Lcom/facebook/payments/p2p/model/PaymentCard;->a:J

    move-wide v6, v10

    .line 2612182
    iget-object v2, p0, LX/Iok;->c:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2612183
    iget-object v8, v2, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->p:Lcom/facebook/user/model/UserKey;

    move-object v2, v8

    .line 2612184
    invoke-virtual {v2}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v8

    .line 2612185
    iget-object v2, v0, Lcom/facebook/messaging/payment/value/input/OrionMessengerPayParams;->f:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/facebook/messaging/payment/value/input/OrionMessengerPayParams;->f:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;

    invoke-virtual {v2}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;->d()Ljava/lang/String;

    move-result-object v2

    .line 2612186
    :goto_0
    invoke-static {}, Lcom/facebook/messaging/model/payment/SentPayment;->newBuilder()LX/5e5;

    move-result-object v9

    .line 2612187
    iput-object v5, v9, LX/5e5;->a:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 2612188
    move-object v5, v9

    .line 2612189
    iput-wide v6, v5, LX/5e5;->b:J

    .line 2612190
    move-object v5, v5

    .line 2612191
    iput-object v8, v5, LX/5e5;->c:Ljava/lang/String;

    .line 2612192
    move-object v5, v5

    .line 2612193
    iget-object v6, p0, LX/Iok;->c:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2612194
    iget-object v7, v6, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->u:Ljava/lang/String;

    move-object v6, v7

    .line 2612195
    iput-object v6, v5, LX/5e5;->e:Ljava/lang/String;

    .line 2612196
    move-object v5, v5

    .line 2612197
    iget-object v6, p0, LX/Iok;->c:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2612198
    iget-object v7, v6, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->v:Ljava/lang/String;

    move-object v6, v7

    .line 2612199
    iput-object v6, v5, LX/5e5;->f:Ljava/lang/String;

    .line 2612200
    move-object v5, v5

    .line 2612201
    iget-object v6, p0, LX/Iok;->c:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2612202
    iget-object v7, v6, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->s:Ljava/lang/String;

    move-object v6, v7

    .line 2612203
    iput-object v6, v5, LX/5e5;->d:Ljava/lang/String;

    .line 2612204
    move-object v5, v5

    .line 2612205
    iget-object v6, v0, Lcom/facebook/messaging/payment/value/input/OrionMessengerPayParams;->d:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 2612206
    sget-object v7, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueActivity;->p:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v6, v7}, Lcom/facebook/payments/currency/CurrencyAmount;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    const/4 v7, 0x1

    :goto_1
    move v6, v7

    .line 2612207
    iput-boolean v6, v5, LX/5e5;->g:Z

    .line 2612208
    move-object v5, v5

    .line 2612209
    iput-object v2, v5, LX/5e5;->h:Ljava/lang/String;

    .line 2612210
    move-object v2, v5

    .line 2612211
    iget-object v5, v0, Lcom/facebook/messaging/payment/value/input/OrionMessengerPayParams;->g:Ljava/lang/String;

    .line 2612212
    iput-object v5, v2, LX/5e5;->j:Ljava/lang/String;

    .line 2612213
    move-object v2, v2

    .line 2612214
    iput-object v1, v2, LX/5e5;->i:LX/5g0;

    .line 2612215
    move-object v1, v2

    .line 2612216
    iget-object v2, p0, LX/Iok;->c:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2612217
    iget-object v5, v2, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->n:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;

    move-object v2, v5

    .line 2612218
    if-nez v2, :cond_1

    .line 2612219
    :goto_2
    iput-object v3, v1, LX/5e5;->k:Ljava/lang/String;

    .line 2612220
    move-object v1, v1

    .line 2612221
    invoke-virtual {v1}, LX/5e5;->l()Lcom/facebook/messaging/model/payment/SentPayment;

    move-result-object v1

    .line 2612222
    const-string v2, "recipient_id"

    iget-object v3, p0, LX/Iok;->c:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2612223
    iget-object v5, v3, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->p:Lcom/facebook/user/model/UserKey;

    move-object v3, v5

    .line 2612224
    invoke-virtual {v3}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2612225
    const-string v2, "sent_payment"

    invoke-virtual {v4, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2612226
    const-string v1, "thread_key"

    iget-object v0, v0, Lcom/facebook/messaging/payment/value/input/OrionMessengerPayParams;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v4, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2612227
    iget-object v0, p0, LX/Iok;->d:LX/Io2;

    invoke-virtual {v0, v4}, LX/Io2;->a(Landroid/content/Intent;)V

    .line 2612228
    return-void

    .line 2612229
    :catch_0
    move-exception v0

    .line 2612230
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_0
    move-object v2, v3

    .line 2612231
    goto :goto_0

    .line 2612232
    :cond_1
    iget-object v2, p0, LX/Iok;->c:Lcom/facebook/messaging/payment/value/input/MessengerPayData;

    .line 2612233
    iget-object v3, v2, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->n:Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;

    move-object v2, v3

    .line 2612234
    invoke-virtual {v2}, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$ThemeModel;->d()Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    :cond_2
    const/4 v7, 0x0

    goto :goto_1
.end method
