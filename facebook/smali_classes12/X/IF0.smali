.class public LX/IF0;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Zb;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/0Zb;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2553527
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2553528
    iput-object p2, p0, LX/IF0;->a:LX/0Zb;

    .line 2553529
    iput-object p1, p0, LX/IF0;->b:Ljava/lang/String;

    .line 2553530
    return-void
.end method

.method public static a(LX/IF0;LX/IEz;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 3

    .prologue
    .line 2553523
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "sticker_upsell"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/IF0;->b:Ljava/lang/String;

    .line 2553524
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 2553525
    move-object v0, v0

    .line 2553526
    const-string v1, "action"

    invoke-virtual {p1}, LX/IEz;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    return-object v0
.end method
