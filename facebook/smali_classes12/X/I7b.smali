.class public final LX/I7b;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/events/permalink/about/EventArtistRowView;


# direct methods
.method public constructor <init>(Lcom/facebook/events/permalink/about/EventArtistRowView;)V
    .locals 0

    .prologue
    .line 2539886
    iput-object p1, p0, LX/I7b;->a:Lcom/facebook/events/permalink/about/EventArtistRowView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x4a6b0141    # 3850320.2f

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2539887
    iget-object v0, p0, LX/I7b;->a:Lcom/facebook/events/permalink/about/EventArtistRowView;

    iget-object v0, v0, Lcom/facebook/events/permalink/about/EventArtistRowView;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iget-object v2, p0, LX/I7b;->a:Lcom/facebook/events/permalink/about/EventArtistRowView;

    invoke-virtual {v2}, Lcom/facebook/events/permalink/about/EventArtistRowView;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, LX/0ax;->aE:Ljava/lang/String;

    iget-object v4, p0, LX/I7b;->a:Lcom/facebook/events/permalink/about/EventArtistRowView;

    iget-object v4, v4, Lcom/facebook/events/permalink/about/EventArtistRowView;->h:Lcom/facebook/events/model/EventArtist;

    .line 2539888
    iget-object p1, v4, Lcom/facebook/events/model/EventArtist;->a:Ljava/lang/String;

    move-object v4, p1

    .line 2539889
    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2539890
    iget-object v2, p0, LX/I7b;->a:Lcom/facebook/events/permalink/about/EventArtistRowView;

    iget-object v2, v2, Lcom/facebook/events/permalink/about/EventArtistRowView;->c:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/I7b;->a:Lcom/facebook/events/permalink/about/EventArtistRowView;

    invoke-virtual {v3}, Lcom/facebook/events/permalink/about/EventArtistRowView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2539891
    const v0, 0xbeaa4fc

    invoke-static {v5, v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
