.class public LX/ImT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final l:Ljava/lang/Object;


# instance fields
.field public final b:LX/Im5;

.field public final c:LX/Im8;

.field public final d:LX/ImG;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/2Sz;

.field public final g:LX/ImS;

.field public final h:Ljava/lang/Integer;

.field public final i:LX/IzM;

.field public final j:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/2Sv;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2609876
    const-class v0, LX/ImT;

    sput-object v0, LX/ImT;->a:Ljava/lang/Class;

    .line 2609877
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/ImT;->l:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/Im5;LX/Im8;LX/ImG;LX/0Or;LX/2Sz;LX/ImS;Ljava/lang/Integer;LX/IzM;LX/0Or;LX/2Sv;)V
    .locals 0
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/payments/p2p/config/IsP2pPaymentsSyncProtocolEnabled;
        .end annotation
    .end param
    .param p7    # Ljava/lang/Integer;
        .annotation runtime Lcom/facebook/messaging/payment/sync/annotations/PaymentsSyncApiVersion;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Im5;",
            "LX/Im8;",
            "LX/ImG;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/2Sz;",
            "LX/ImS;",
            "Ljava/lang/Integer;",
            "LX/IzM;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "Lcom/facebook/sync/SyncContextChecker;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2609878
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2609879
    iput-object p1, p0, LX/ImT;->b:LX/Im5;

    .line 2609880
    iput-object p2, p0, LX/ImT;->c:LX/Im8;

    .line 2609881
    iput-object p3, p0, LX/ImT;->d:LX/ImG;

    .line 2609882
    iput-object p4, p0, LX/ImT;->e:LX/0Or;

    .line 2609883
    iput-object p5, p0, LX/ImT;->f:LX/2Sz;

    .line 2609884
    iput-object p6, p0, LX/ImT;->g:LX/ImS;

    .line 2609885
    iput-object p7, p0, LX/ImT;->h:Ljava/lang/Integer;

    .line 2609886
    iput-object p8, p0, LX/ImT;->i:LX/IzM;

    .line 2609887
    iput-object p9, p0, LX/ImT;->j:LX/0Or;

    .line 2609888
    iput-object p10, p0, LX/ImT;->k:LX/2Sv;

    .line 2609889
    return-void
.end method

.method public static a(LX/0QB;)LX/ImT;
    .locals 7

    .prologue
    .line 2609890
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2609891
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2609892
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2609893
    if-nez v1, :cond_0

    .line 2609894
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2609895
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2609896
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2609897
    sget-object v1, LX/ImT;->l:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2609898
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2609899
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2609900
    :cond_1
    if-nez v1, :cond_4

    .line 2609901
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2609902
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2609903
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/ImT;->b(LX/0QB;)LX/ImT;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 2609904
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2609905
    if-nez v1, :cond_2

    .line 2609906
    sget-object v0, LX/ImT;->l:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ImT;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2609907
    :goto_1
    if-eqz v0, :cond_3

    .line 2609908
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2609909
    :goto_3
    check-cast v0, LX/ImT;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2609910
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2609911
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2609912
    :catchall_1
    move-exception v0

    .line 2609913
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2609914
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2609915
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2609916
    :cond_2
    :try_start_8
    sget-object v0, LX/ImT;->l:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ImT;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static b(LX/0QB;)LX/ImT;
    .locals 11

    .prologue
    .line 2609917
    new-instance v0, LX/ImT;

    invoke-static {p0}, LX/Im5;->a(LX/0QB;)LX/Im5;

    move-result-object v1

    check-cast v1, LX/Im5;

    invoke-static {p0}, LX/Im8;->a(LX/0QB;)LX/Im8;

    move-result-object v2

    check-cast v2, LX/Im8;

    invoke-static {p0}, LX/ImG;->a(LX/0QB;)LX/ImG;

    move-result-object v3

    check-cast v3, LX/ImG;

    const/16 v4, 0x1547

    invoke-static {p0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {p0}, LX/2Sz;->a(LX/0QB;)LX/2Sz;

    move-result-object v5

    check-cast v5, LX/2Sz;

    invoke-static {p0}, LX/ImS;->a(LX/0QB;)LX/ImS;

    move-result-object v6

    check-cast v6, LX/ImS;

    invoke-static {p0}, LX/Im4;->b(LX/0QB;)Ljava/lang/Integer;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-static {p0}, LX/IzM;->a(LX/0QB;)LX/IzM;

    move-result-object v8

    check-cast v8, LX/IzM;

    const/16 v9, 0x19e

    invoke-static {p0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static {p0}, LX/2Sv;->b(LX/0QB;)LX/2Sv;

    move-result-object v10

    check-cast v10, LX/2Sv;

    invoke-direct/range {v0 .. v10}, LX/ImT;-><init>(LX/Im5;LX/Im8;LX/ImG;LX/0Or;LX/2Sz;LX/ImS;Ljava/lang/Integer;LX/IzM;LX/0Or;LX/2Sv;)V

    .line 2609918
    return-object v0
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 11

    .prologue
    .line 2609919
    iget-object v0, p0, LX/ImT;->k:LX/2Sv;

    invoke-virtual {v0}, LX/2Sv;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2609920
    sget-object v0, LX/1nY;->CANCELLED:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2609921
    :goto_0
    return-object v0

    .line 2609922
    :cond_0
    iget-object v0, p0, LX/ImT;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const-string v1, "Payments sync protocol disabled, but got a %s operation"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 2609923
    iget-object v4, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v4, v4

    .line 2609924
    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/0PB;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 2609925
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 2609926
    const-string v1, "ensure_payments_sync"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2609927
    invoke-static {p1}, LX/2Sz;->d(LX/1qK;)LX/7G7;

    move-result-object v0

    .line 2609928
    iget-object v1, p0, LX/ImT;->c:LX/Im8;

    iget-object v2, p0, LX/ImT;->h:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v3, p0, LX/ImT;->b:LX/Im5;

    .line 2609929
    iget-object v4, p1, LX/1qK;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    move-object v4, v4

    .line 2609930
    invoke-virtual {v1, v2, v3, v0, v4}, LX/7G8;->a(ILX/7Fx;LX/7G7;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 2609931
    :cond_1
    const-string v1, "payments_force_full_refresh"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2609932
    invoke-static {p1}, LX/2Sz;->c(LX/1qK;)Lcom/facebook/sync/service/SyncOperationParamsUtil$FullRefreshParams;

    move-result-object v0

    .line 2609933
    iget-object v1, p0, LX/ImT;->i:LX/IzM;

    sget-object v2, LX/IzL;->e:LX/IzK;

    invoke-virtual {v1, v2}, LX/2Iu;->a(LX/0To;)Ljava/lang/String;

    move-result-object v1

    .line 2609934
    iget-object v2, v0, Lcom/facebook/sync/service/SyncOperationParamsUtil$FullRefreshParams;->b:Ljava/lang/String;

    invoke-static {v2, v1}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2609935
    iget-object v1, p0, LX/ImT;->c:LX/Im8;

    iget-object v0, v0, Lcom/facebook/sync/service/SyncOperationParamsUtil$FullRefreshParams;->a:Lcom/facebook/sync/analytics/FullRefreshReason;

    .line 2609936
    iget-object v2, p1, LX/1qK;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    move-object v2, v2

    .line 2609937
    invoke-virtual {v1, v0, v2}, LX/Im8;->a(Lcom/facebook/sync/analytics/FullRefreshReason;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2609938
    :goto_1
    move-object v0, v0

    .line 2609939
    goto :goto_0

    .line 2609940
    :cond_2
    const-string v1, "payments_deltas"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2609941
    invoke-static {p1}, LX/2Sz;->a(LX/1qK;)Ljava/io/Serializable;

    move-result-object v5

    check-cast v5, LX/Iq2;

    .line 2609942
    iget-object v6, v5, LX/Iq2;->deltas:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/Ipt;

    .line 2609943
    iget v8, v6, LX/6kT;->setField_:I

    move v8, v8

    .line 2609944
    const/16 v9, 0x8

    if-ne v8, v9, :cond_3

    .line 2609945
    invoke-virtual {v6}, LX/Ipt;->f()LX/Ipn;

    move-result-object v6

    .line 2609946
    iget-object v8, v6, LX/Ipn;->fetchTransferFbId:Ljava/lang/Long;

    if-nez v8, :cond_3

    iget-object v6, v6, LX/Ipn;->fetchPaymentMethods:Ljava/lang/Boolean;

    if-nez v6, :cond_3

    .line 2609947
    iget-object v6, p0, LX/ImT;->c:LX/Im8;

    iget-object v5, v5, LX/Iq2;->firstDeltaSeqId:Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/facebook/sync/analytics/FullRefreshReason;->a(J)Lcom/facebook/sync/analytics/FullRefreshReason;

    move-result-object v5

    .line 2609948
    iget-object v7, p1, LX/1qK;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    move-object v7, v7

    .line 2609949
    invoke-virtual {v6, v5, v7}, LX/Im8;->a(Lcom/facebook/sync/analytics/FullRefreshReason;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v5

    .line 2609950
    :goto_2
    move-object v0, v5

    .line 2609951
    goto/16 :goto_0

    .line 2609952
    :cond_4
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown operation type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2609953
    :cond_5
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2609954
    goto :goto_1

    .line 2609955
    :cond_6
    :try_start_0
    iget-object v6, p0, LX/ImT;->d:LX/ImG;

    invoke-virtual {v6, v5}, LX/ImG;->a(LX/Iq2;)V

    .line 2609956
    sget-object v5, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v5, v5
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2609957
    goto :goto_2

    .line 2609958
    :catch_0
    move-exception v10

    .line 2609959
    iget-object v5, p0, LX/ImT;->g:LX/ImS;

    iget-object v6, p0, LX/ImT;->j:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2609960
    iget-object v7, v6, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v6, v7

    .line 2609961
    iget-object v7, p0, LX/ImT;->h:Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    iget-object v8, p0, LX/ImT;->b:LX/Im5;

    .line 2609962
    iget-object v9, p1, LX/1qK;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    move-object v9, v9

    .line 2609963
    invoke-virtual/range {v5 .. v10}, LX/7Gb;->a(Ljava/lang/String;ILX/7Fx;Lcom/facebook/common/callercontext/CallerContext;Ljava/lang/Exception;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v5

    goto :goto_2
.end method
