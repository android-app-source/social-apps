.class public LX/I8r;
.super LX/1Cv;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field public b:Z

.field public c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2542051
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 2542052
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/I8r;->b:Z

    .line 2542053
    iput-object p1, p0, LX/I8r;->a:Landroid/content/Context;

    .line 2542054
    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2542060
    invoke-static {}, LX/I8q;->values()[LX/I8q;

    move-result-object v0

    aget-object v0, v0, p1

    .line 2542061
    sget-object v1, LX/I8p;->a:[I

    invoke-virtual {v0}, LX/I8q;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2542062
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown viewType "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2542063
    :pswitch_0
    iget-object v0, p0, LX/I8r;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030500

    invoke-virtual {v0, v1, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2542064
    :goto_0
    return-object v0

    :pswitch_1
    iget-object v0, p0, LX/I8r;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0304e1

    invoke-virtual {v0, v1, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 2542059
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2542058
    iget-boolean v0, p0, LX/I8r;->b:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/I8r;->c:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2542065
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2542057
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2542056
    iget-boolean v0, p0, LX/I8r;->b:Z

    if-eqz v0, :cond_0

    sget-object v0, LX/I8q;->PROGRESS_BAR:LX/I8q;

    invoke-virtual {v0}, LX/I8q;->ordinal()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    sget-object v0, LX/I8q;->END_MARKER:LX/I8q;

    invoke-virtual {v0}, LX/I8q;->ordinal()I

    move-result v0

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2542055
    invoke-static {}, LX/I8q;->values()[LX/I8q;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
