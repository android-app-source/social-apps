.class public final LX/J6M;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/J64;


# instance fields
.field public final synthetic a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;)V
    .locals 0

    .prologue
    .line 2649538
    iput-object p1, p0, LX/J6M;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/1oT;)V
    .locals 9

    .prologue
    .line 2649539
    iget-object v0, p0, LX/J6M;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->b:LX/J45;

    iget-object v1, p0, LX/J6M;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;

    iget-object v1, v1, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->e:LX/J4K;

    const/4 v3, 0x0

    .line 2649540
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 2649541
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2649542
    const/4 v2, 0x0

    .line 2649543
    invoke-virtual {v0, v1}, LX/J45;->b(LX/J4K;)LX/J4L;

    move-result-object v5

    move v4, v3

    move-object v3, v2

    .line 2649544
    :goto_1
    iget-object v2, v5, LX/J4L;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v4, v2, :cond_2

    .line 2649545
    iget-object v2, v5, LX/J4L;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, LX/J4J;

    if-eqz v2, :cond_0

    .line 2649546
    iget-object v2, v5, LX/J4L;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/J4J;

    .line 2649547
    iget-object v6, v2, LX/J4J;->a:Ljava/lang/String;

    invoke-static {p1, v6}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2649548
    iget-object v3, v2, LX/J4J;->b:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    .line 2649549
    new-instance v6, LX/8SR;

    iget-object v7, v2, LX/J4J;->f:LX/8SR;

    .line 2649550
    iget-object v8, v7, LX/8SR;->a:LX/0Px;

    move-object v7, v8

    .line 2649551
    invoke-direct {v6, v7, p2}, LX/8SR;-><init>(LX/0Px;LX/1oT;)V

    .line 2649552
    iget-object v7, v5, LX/J4L;->a:Ljava/util/ArrayList;

    new-instance v8, LX/J4I;

    invoke-direct {v8, v2}, LX/J4I;-><init>(LX/J4J;)V

    .line 2649553
    iput-object v6, v8, LX/J4I;->f:LX/8SR;

    .line 2649554
    move-object v2, v8

    .line 2649555
    invoke-virtual {v6}, LX/8SR;->b()LX/1oT;

    move-result-object v1

    invoke-interface {v1}, LX/1oT;->d()Ljava/lang/String;

    move-result-object v1

    .line 2649556
    iput-object v1, v2, LX/J4I;->g:Ljava/lang/String;

    .line 2649557
    move-object v2, v2

    .line 2649558
    invoke-virtual {v6}, LX/8SR;->b()LX/1oT;

    move-result-object v1

    invoke-interface {v1}, LX/1oT;->b()LX/1Fd;

    move-result-object v1

    .line 2649559
    iput-object v1, v2, LX/J4I;->h:LX/1Fd;

    .line 2649560
    move-object v2, v2

    .line 2649561
    move-object v2, v2

    .line 2649562
    invoke-virtual {v2}, LX/J4I;->a()LX/J4J;

    move-result-object v2

    invoke-virtual {v7, v4, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_0
    move-object v2, v3

    .line 2649563
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move-object v3, v2

    goto :goto_1

    :cond_1
    move v2, v3

    .line 2649564
    goto :goto_0

    .line 2649565
    :cond_2
    if-nez v3, :cond_3

    .line 2649566
    const-string v2, "Didn\'t find a privacy type for id %s"

    invoke-static {v2, p1}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2649567
    :goto_2
    iget-object v0, p0, LX/J6M;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->h:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;

    const v1, 0x3ac18c7

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2649568
    return-void

    .line 2649569
    :cond_3
    iget-object v2, v0, LX/J45;->e:LX/J49;

    invoke-virtual {v2, p1, v3, p2}, LX/J49;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;LX/1oT;)V

    goto :goto_2
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 6

    .prologue
    .line 2649570
    iget-object v0, p0, LX/J6M;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->b:LX/J45;

    iget-object v1, p0, LX/J6M;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;

    iget-object v1, v1, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->e:LX/J4K;

    .line 2649571
    invoke-virtual {v0, v1}, LX/J45;->b(LX/J4K;)LX/J4L;

    move-result-object v4

    .line 2649572
    const/4 v2, 0x0

    move v3, v2

    :goto_0
    iget-object v2, v4, LX/J4L;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v3, v2, :cond_0

    .line 2649573
    iget-object v2, v4, LX/J4L;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, LX/J4J;

    if-eqz v2, :cond_1

    .line 2649574
    iget-object v2, v4, LX/J4L;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/J4J;

    .line 2649575
    iget-object v5, v2, LX/J4J;->a:Ljava/lang/String;

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2649576
    iget-object v4, v4, LX/J4L;->a:Ljava/util/ArrayList;

    new-instance v5, LX/J4I;

    invoke-direct {v5, v2}, LX/J4I;-><init>(LX/J4J;)V

    const/4 v2, 0x1

    .line 2649577
    iput-boolean v2, v5, LX/J4I;->i:Z

    .line 2649578
    move-object v2, v5

    .line 2649579
    invoke-virtual {v2}, LX/J4I;->a()LX/J4J;

    move-result-object v2

    invoke-virtual {v4, v3, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2649580
    :cond_0
    iget-object v2, v0, LX/J45;->e:LX/J49;

    .line 2649581
    iget-object v4, v2, LX/J49;->p:LX/J5k;

    if-eqz p2, :cond_2

    const-string v3, "delete_app_and_posts"

    :goto_1
    const/4 v5, 0x0

    invoke-virtual {v4, p1, v3, v5}, LX/J5k;->a(Ljava/lang/String;Ljava/lang/String;LX/1oU;)V

    .line 2649582
    if-eqz p2, :cond_3

    sget-object v3, LX/5ni;->DELETE_APP_AND_POSTS:LX/5ni;

    .line 2649583
    :goto_2
    new-instance v4, LX/5nl;

    invoke-direct {v4}, LX/5nl;-><init>()V

    sget-object v5, LX/5nj;->MUTATION:LX/5nj;

    .line 2649584
    iput-object v5, v4, LX/5nl;->a:LX/5nj;

    .line 2649585
    move-object v4, v4

    .line 2649586
    invoke-static {v2}, LX/J49;->c(LX/J49;)Ljava/lang/Long;

    move-result-object v5

    .line 2649587
    iput-object v5, v4, LX/5nl;->b:Ljava/lang/Long;

    .line 2649588
    move-object v4, v4

    .line 2649589
    iput-object p1, v4, LX/5nl;->c:Ljava/lang/String;

    .line 2649590
    move-object v4, v4

    .line 2649591
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;->POP_PER_APP:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    .line 2649592
    iput-object v5, v4, LX/5nl;->d:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    .line 2649593
    move-object v4, v4

    .line 2649594
    iput-object v3, v4, LX/5nl;->e:LX/5ni;

    .line 2649595
    move-object v3, v4

    .line 2649596
    invoke-virtual {v3}, LX/5nl;->a()Lcom/facebook/privacy/protocol/ReportPrivacyCheckupActionsParams$PrivacyCheckupItem;

    move-result-object v3

    .line 2649597
    invoke-static {v2, p1, v3}, LX/J49;->a(LX/J49;Ljava/lang/String;Lcom/facebook/privacy/protocol/ReportPrivacyCheckupActionsParams$PrivacyCheckupItem;)V

    .line 2649598
    iget-object v0, p0, LX/J6M;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->h:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepAdapter;

    const v1, -0x4befa6e

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2649599
    return-void

    .line 2649600
    :cond_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 2649601
    :cond_2
    const-string v3, "delete_app_only"

    goto :goto_1

    .line 2649602
    :cond_3
    sget-object v3, LX/5ni;->DELETE_APP_ONLY:LX/5ni;

    goto :goto_2
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2649603
    iget-object v0, p0, LX/J6M;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->b:LX/J45;

    .line 2649604
    iget-boolean p0, v0, LX/J45;->j:Z

    move v0, p0

    .line 2649605
    return v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2649606
    iget-object v0, p0, LX/J6M;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->b:LX/J45;

    .line 2649607
    const/4 p0, 0x1

    iput-boolean p0, v0, LX/J45;->j:Z

    .line 2649608
    return-void
.end method

.method public final c()Lcom/facebook/privacy/model/SelectablePrivacyData;
    .locals 1

    .prologue
    .line 2649609
    iget-object v0, p0, LX/J6M;->a:Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;

    iget-object v0, v0, Lcom/facebook/privacy/checkup/ui/PrivacyCheckupStepFragment;->b:LX/J45;

    .line 2649610
    iget-object p0, v0, LX/J45;->k:Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-object v0, p0

    .line 2649611
    return-object v0
.end method
