.class public LX/HLD;
.super LX/3mX;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/2km;",
        ":",
        "LX/3U8;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/2kp;",
        ">",
        "LX/3mX",
        "<",
        "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLInterfaces$HScrollPageCardFields;",
        "TE;>;"
    }
.end annotation


# instance fields
.field private final c:LX/HIQ;

.field public final d:LX/9XE;

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/961;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Lcom/facebook/reaction/common/ReactionUnitComponentNode;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Px;LX/2km;LX/25M;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/HIQ;LX/9XE;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/2km;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/25M;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/reaction/common/ReactionUnitComponentNode;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Px",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLInterfaces$HScrollPageCardFields;",
            ">;TE;",
            "LX/25M;",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "LX/HIQ;",
            "LX/9XE;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2455652
    check-cast p3, LX/1Pq;

    invoke-direct {p0, p1, p2, p3, p4}, LX/3mX;-><init>(Landroid/content/Context;LX/0Px;LX/1Pq;LX/25M;)V

    .line 2455653
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2455654
    iput-object v0, p0, LX/HLD;->e:LX/0Ot;

    .line 2455655
    iput-object p6, p0, LX/HLD;->c:LX/HIQ;

    .line 2455656
    iput-object p7, p0, LX/HLD;->d:LX/9XE;

    .line 2455657
    iput-object p5, p0, LX/HLD;->f:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2455658
    return-void
.end method

.method public static a$redex0(LX/HLD;LX/HIR;Z)V
    .locals 5

    .prologue
    .line 2455659
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2455660
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/HLD;->f:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2455661
    iget-object v3, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v3

    .line 2455662
    invoke-interface {v0}, LX/9uc;->m()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2455663
    iget-object v0, p0, LX/HLD;->f:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2455664
    iget-object v3, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v3

    .line 2455665
    invoke-interface {v0}, LX/9uc;->m()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlacePageRecommendationsSocialContextFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlacePageRecommendationsSocialContextFieldsModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p1, LX/HIR;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2455666
    iget-object v0, p0, LX/HLD;->f:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2455667
    iget-object v3, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v3

    .line 2455668
    invoke-interface {v0}, LX/9uc;->m()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlacePageRecommendationsSocialContextFieldsModel;

    .line 2455669
    new-instance v3, LX/9qd;

    invoke-direct {v3}, LX/9qd;-><init>()V

    .line 2455670
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlacePageRecommendationsSocialContextFieldsModel;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;

    move-result-object v4

    iput-object v4, v3, LX/9qd;->a:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;

    .line 2455671
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlacePageRecommendationsSocialContextFieldsModel;->k()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v4

    iput-object v4, v3, LX/9qd;->b:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 2455672
    move-object v3, v3

    .line 2455673
    iget-object v0, p0, LX/HLD;->f:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2455674
    iget-object v4, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v4

    .line 2455675
    invoke-interface {v0}, LX/9uc;->m()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlacePageRecommendationsSocialContextFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlacePageRecommendationsSocialContextFieldsModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;

    invoke-static {v0}, LX/9qZ;->a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;)LX/9qZ;

    move-result-object v0

    .line 2455676
    iput-boolean p2, v0, LX/9qZ;->c:Z

    .line 2455677
    move-object v0, v0

    .line 2455678
    invoke-virtual {v0}, LX/9qZ;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;

    move-result-object v0

    .line 2455679
    iput-object v0, v3, LX/9qd;->a:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;

    .line 2455680
    move-object v0, v3

    .line 2455681
    invoke-virtual {v0}, LX/9qd;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlacePageRecommendationsSocialContextFieldsModel;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2455682
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2455683
    :cond_0
    iget-object v0, p0, LX/HLD;->f:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2455684
    iget-object v3, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v3

    .line 2455685
    invoke-interface {v0}, LX/9uc;->m()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlacePageRecommendationsSocialContextFieldsModel;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2455686
    :cond_1
    iget-object v0, p0, LX/HLD;->f:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2455687
    iget-object v1, v0, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v1

    .line 2455688
    check-cast v0, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;

    invoke-static {v0}, Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;->a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;)Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;

    move-result-object v0

    invoke-static {v0}, LX/9vz;->a(Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;)LX/9vz;

    move-result-object v0

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    .line 2455689
    iput-object v1, v0, LX/9vz;->d:LX/0Px;

    .line 2455690
    move-object v0, v0

    .line 2455691
    invoke-virtual {v0}, LX/9vz;->a()Lcom/facebook/reaction/protocol/graphql/ReactionUnitComponentsGraphQLModels$ReactionUnitComponentModel;

    move-result-object v1

    .line 2455692
    iget-object v0, p0, LX/3mX;->b:LX/1Pq;

    check-cast v0, LX/2km;

    check-cast v0, LX/3U8;

    iget-object v2, p0, LX/HLD;->f:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-interface {v0, v2, v1}, LX/3U8;->b(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/9uc;)V

    .line 2455693
    return-void
.end method


# virtual methods
.method public final a(LX/1De;)LX/1X1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2455694
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;Ljava/lang/Object;)LX/1X1;
    .locals 2

    .prologue
    .line 2455695
    check-cast p2, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;

    .line 2455696
    iget-object v0, p0, LX/HLD;->c:LX/HIQ;

    invoke-virtual {v0, p1}, LX/HIQ;->c(LX/1De;)LX/HIO;

    move-result-object v1

    iget-object v0, p0, LX/3mX;->b:LX/1Pq;

    check-cast v0, LX/2km;

    invoke-virtual {v1, v0}, LX/HIO;->a(LX/2km;)LX/HIO;

    move-result-object v0

    .line 2455697
    new-instance v1, LX/HL9;

    invoke-direct {v1, p0}, LX/HL9;-><init>(LX/HLD;)V

    move-object v1, v1

    .line 2455698
    invoke-virtual {v0, v1}, LX/HIO;->a(LX/HIT;)LX/HIO;

    move-result-object v0

    .line 2455699
    new-instance v1, LX/HLA;

    invoke-direct {v1, p0}, LX/HLA;-><init>(LX/HLD;)V

    move-object v1, v1

    .line 2455700
    invoke-virtual {v0, v1}, LX/HIO;->a(LX/HIS;)LX/HIO;

    move-result-object v0

    .line 2455701
    new-instance v1, LX/HLC;

    invoke-direct {v1, p0}, LX/HLC;-><init>(LX/HLD;)V

    move-object v1, v1

    .line 2455702
    invoke-virtual {v0, v1}, LX/HIO;->a(LX/HIU;)LX/HIO;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/HIO;->a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$HScrollPageCardFieldsModel;)LX/HIO;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 2455703
    const/4 v0, 0x0

    return v0
.end method
