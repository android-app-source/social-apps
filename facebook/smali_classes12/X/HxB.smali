.class public final LX/HxB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/HxG;


# direct methods
.method public constructor <init>(LX/HxG;)V
    .locals 0

    .prologue
    .line 2522160
    iput-object p1, p0, LX/HxB;->a:LX/HxG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x26fdafd2

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2522161
    iget-object v1, p0, LX/HxB;->a:LX/HxG;

    iget-object v1, v1, LX/HxG;->b:LX/7v8;

    iget-object v2, p0, LX/HxB;->a:LX/HxG;

    iget-object v2, v2, LX/HxG;->r:Ljava/lang/String;

    .line 2522162
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v4, LX/7v7;->BIRTHDAYS_CLICK_PROFILE:LX/7v7;

    invoke-virtual {v4}, LX/7v7;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2522163
    invoke-static {v1, v3, v2}, LX/7v8;->a(LX/7v8;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V

    .line 2522164
    new-instance v1, LX/7vI;

    invoke-direct {v1}, LX/7vI;-><init>()V

    sget-object v2, LX/7vJ;->USER:LX/7vJ;

    .line 2522165
    iput-object v2, v1, LX/7vI;->a:LX/7vJ;

    .line 2522166
    move-object v1, v1

    .line 2522167
    iget-object v2, p0, LX/HxB;->a:LX/HxG;

    iget-object v2, v2, LX/HxG;->o:Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;

    invoke-virtual {v2}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventUserWithBirthdayFragmentModel;->m()Ljava/lang/String;

    move-result-object v2

    .line 2522168
    iput-object v2, v1, LX/7vI;->c:Ljava/lang/String;

    .line 2522169
    move-object v1, v1

    .line 2522170
    invoke-virtual {v1}, LX/7vI;->a()Lcom/facebook/events/model/EventUser;

    move-result-object v1

    .line 2522171
    iget-object v2, p0, LX/HxB;->a:LX/HxG;

    iget-object v2, v2, LX/HxG;->b:LX/7v8;

    iget-object v3, p0, LX/HxB;->a:LX/HxG;

    iget-boolean v3, v3, LX/HxG;->p:Z

    iget-object v4, p0, LX/HxB;->a:LX/HxG;

    iget-object v4, v4, LX/HxG;->r:Ljava/lang/String;

    .line 2522172
    new-instance v6, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object p1, LX/7v7;->BIRTHDAYS_CLICK_ROW:LX/7v7;

    invoke-virtual {p1}, LX/7v7;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v6, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "is_birthday_today"

    invoke-virtual {v6, p1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    .line 2522173
    invoke-static {v2, v6, v4}, LX/7v8;->a(LX/7v8;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V

    .line 2522174
    iget-object v2, p0, LX/HxB;->a:LX/HxG;

    iget-object v2, v2, LX/HxG;->d:LX/I76;

    iget-object v3, p0, LX/HxB;->a:LX/HxG;

    invoke-virtual {v3}, LX/HxG;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, LX/I76;->a(Landroid/content/Context;Lcom/facebook/events/model/EventUser;)V

    .line 2522175
    const v1, 0x13710e41

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
