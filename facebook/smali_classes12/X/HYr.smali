.class public final LX/HYr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/HYm;


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Z

.field public final synthetic c:LX/HYs;


# direct methods
.method public constructor <init>(LX/HYs;ZZ)V
    .locals 0

    .prologue
    .line 2481056
    iput-object p1, p0, LX/HYr;->c:LX/HYs;

    iput-boolean p2, p0, LX/HYr;->a:Z

    iput-boolean p3, p0, LX/HYr;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Landroid/content/Intent;
    .locals 6

    .prologue
    .line 2481057
    new-instance v0, LX/HZ1;

    const-class v1, Lcom/facebook/registration/fragment/RegistrationExistingAccountFragment;

    invoke-direct {v0, v1}, LX/HZ1;-><init>(Ljava/lang/Class;)V

    iget-boolean v1, p0, LX/HYr;->a:Z

    .line 2481058
    iput-boolean v1, v0, LX/HZ1;->b:Z

    .line 2481059
    move-object v0, v0

    .line 2481060
    iget-boolean v1, p0, LX/HYr;->b:Z

    .line 2481061
    iput-boolean v1, v0, LX/HZ1;->c:Z

    .line 2481062
    move-object v0, v0

    .line 2481063
    invoke-virtual {v0}, LX/HZ1;->a()Landroid/content/Intent;

    move-result-object v0

    .line 2481064
    const-string v1, "auto_redirect_to_ar"

    iget-object v2, p0, LX/HYr;->c:LX/HYs;

    const/4 v3, 0x0

    .line 2481065
    iget-object v4, v2, LX/HYs;->b:Lcom/facebook/registration/model/SimpleRegFormData;

    sget-object v5, LX/HYi;->EXISTING_ACCOUNT:LX/HYi;

    invoke-virtual {v4, v5}, Lcom/facebook/registration/model/SimpleRegFormData;->c(LX/HYi;)I

    move-result v4

    .line 2481066
    sparse-switch v4, :sswitch_data_0

    .line 2481067
    :goto_0
    :sswitch_0
    move v2, v3

    .line 2481068
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2481069
    const-string v1, "allow_reg_using_same_cp"

    iget-object v2, p0, LX/HYr;->c:LX/HYs;

    .line 2481070
    iget-object v3, v2, LX/HYs;->b:Lcom/facebook/registration/model/SimpleRegFormData;

    sget-object v4, LX/HYi;->EXISTING_ACCOUNT:LX/HYi;

    invoke-virtual {v3, v4}, Lcom/facebook/registration/model/SimpleRegFormData;->c(LX/HYi;)I

    move-result v3

    .line 2481071
    const/16 v4, 0xc47

    if-eq v3, v4, :cond_0

    const/16 v4, 0xc46

    if-ne v3, v4, :cond_1

    .line 2481072
    :cond_0
    const/4 v3, 0x1

    .line 2481073
    :goto_1
    move v2, v3

    .line 2481074
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2481075
    return-object v0

    .line 2481076
    :sswitch_1
    iget-object v4, v2, LX/HYs;->c:LX/0Uh;

    const/16 v5, 0x3b

    invoke-virtual {v4, v5, v3}, LX/0Uh;->a(IZ)Z

    move-result v3

    goto :goto_0

    .line 2481077
    :sswitch_2
    iget-object v4, v2, LX/HYs;->c:LX/0Uh;

    const/16 v5, 0x32

    invoke-virtual {v4, v5, v3}, LX/0Uh;->a(IZ)Z

    move-result v3

    goto :goto_0

    .line 2481078
    :sswitch_3
    const/4 v3, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0xc29 -> :sswitch_2
        0xc46 -> :sswitch_3
        0xc47 -> :sswitch_0
        0xcea -> :sswitch_1
    .end sparse-switch
.end method
