.class public LX/ISq;
.super LX/1Cv;
.source ""


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 2578427
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 2578428
    iput-object p1, p0, LX/ISq;->a:Ljava/lang/String;

    .line 2578429
    iput-object p2, p0, LX/ISq;->b:Ljava/lang/String;

    .line 2578430
    iput p3, p0, LX/ISq;->c:I

    .line 2578431
    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2578440
    new-instance v0, LX/ISr;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/ISr;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 3

    .prologue
    .line 2578435
    check-cast p3, LX/ISr;

    iget-object v0, p0, LX/ISq;->a:Ljava/lang/String;

    iget-object v1, p0, LX/ISq;->b:Ljava/lang/String;

    iget v2, p0, LX/ISq;->c:I

    .line 2578436
    iget-object p0, p3, LX/ISr;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2578437
    iget-object p0, p3, LX/ISr;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2578438
    iget-object p0, p3, LX/ISr;->c:Landroid/widget/ProgressBar;

    invoke-virtual {p0, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 2578439
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2578434
    const/4 v0, 0x1

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2578433
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2578432
    int-to-long v0, p1

    return-wide v0
.end method
