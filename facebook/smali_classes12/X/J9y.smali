.class public LX/J9y;
.super LX/0b4;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0b4",
        "<",
        "LX/J8r;",
        "LX/J9r;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/J9y;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2653648
    invoke-direct {p0}, LX/0b4;-><init>()V

    .line 2653649
    return-void
.end method

.method public static a(LX/0QB;)LX/J9y;
    .locals 3

    .prologue
    .line 2653650
    sget-object v0, LX/J9y;->a:LX/J9y;

    if-nez v0, :cond_1

    .line 2653651
    const-class v1, LX/J9y;

    monitor-enter v1

    .line 2653652
    :try_start_0
    sget-object v0, LX/J9y;->a:LX/J9y;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2653653
    if-eqz v2, :cond_0

    .line 2653654
    :try_start_1
    new-instance v0, LX/J9y;

    invoke-direct {v0}, LX/J9y;-><init>()V

    .line 2653655
    move-object v0, v0

    .line 2653656
    sput-object v0, LX/J9y;->a:LX/J9y;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2653657
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2653658
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2653659
    :cond_1
    sget-object v0, LX/J9y;->a:LX/J9y;

    return-object v0

    .line 2653660
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2653661
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
