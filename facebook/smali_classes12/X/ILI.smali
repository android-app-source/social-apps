.class public LX/ILI;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;",
            ">;"
        }
    .end annotation
.end field

.field private final b:I

.field private final c:I

.field private d:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/GroupMemberActionSourceValue;
    .end annotation
.end field

.field private e:Z

.field private f:LX/5QS;


# direct methods
.method public constructor <init>(LX/5QS;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2567782
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2567783
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, LX/ILI;->a:Ljava/util/List;

    .line 2567784
    iput v1, p0, LX/ILI;->b:I

    iput v0, p0, LX/ILI;->c:I

    .line 2567785
    iput-object p1, p0, LX/ILI;->f:LX/5QS;

    .line 2567786
    iget-object v2, p0, LX/ILI;->f:LX/5QS;

    invoke-static {v2}, LX/DKD;->a(LX/5QS;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, LX/ILI;->d:Ljava/lang/String;

    .line 2567787
    sget-object v2, LX/5QS;->VIEWER_CHILD_GROUPS:LX/5QS;

    if-ne p1, v2, :cond_0

    :goto_0
    iput-boolean v0, p0, LX/ILI;->e:Z

    .line 2567788
    return-void

    :cond_0
    move v0, v1

    .line 2567789
    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2567792
    packed-switch p2, :pswitch_data_0

    .line 2567793
    new-instance v1, LX/IO3;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, LX/IO3;-><init>(Landroid/content/Context;)V

    .line 2567794
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b00d7

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2567795
    const v2, 0x7f020c6f

    invoke-virtual {v1, v2}, LX/IO3;->setBackgroundResource(I)V

    .line 2567796
    invoke-virtual {v1, v3, v0, v3, v3}, LX/IO3;->setPadding(IIII)V

    .line 2567797
    new-instance v0, LX/ILH;

    invoke-direct {v0, v1}, LX/ILH;-><init>(LX/IO3;)V

    :goto_0
    return-object v0

    .line 2567798
    :pswitch_0
    new-instance v1, Lcom/facebook/fig/header/FigHeader;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/facebook/fig/header/FigHeader;-><init>(Landroid/content/Context;)V

    .line 2567799
    new-instance v0, LX/ILG;

    invoke-direct {v0, v1}, LX/ILG;-><init>(Lcom/facebook/fig/header/FigHeader;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(LX/1a1;I)V
    .locals 5

    .prologue
    .line 2567800
    iget v0, p1, LX/1a1;->e:I

    move v0, v0

    .line 2567801
    packed-switch v0, :pswitch_data_0

    .line 2567802
    iget-object v0, p0, LX/ILI;->a:Ljava/util/List;

    add-int/lit8 v1, p2, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;

    .line 2567803
    check-cast p1, LX/ILH;

    iget-object v1, p1, LX/ILH;->l:LX/IO3;

    iget-object v2, p0, LX/ILI;->d:Ljava/lang/String;

    iget-boolean v3, p0, LX/ILI;->e:Z

    const/4 v4, 0x0

    invoke-virtual {v1, v0, v2, v3, v4}, LX/IO3;->a(Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$CommunityGroupCondensedInfoCardFragmentModel;Ljava/lang/String;ZLX/IN0;)V

    .line 2567804
    :goto_0
    return-void

    .line 2567805
    :pswitch_0
    check-cast p1, LX/ILG;

    iget-object v0, p1, LX/ILG;->l:Lcom/facebook/fig/header/FigHeader;

    iget-object v1, p0, LX/ILI;->f:LX/5QS;

    invoke-static {v1}, LX/DKD;->b(LX/5QS;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fig/header/FigHeader;->setTitleText(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2567791
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2567790
    iget-object v0, p0, LX/ILI;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method
