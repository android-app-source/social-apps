.class public LX/HVf;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/FQY;

.field private b:LX/17Y;

.field private c:Lcom/facebook/content/SecureContextHelper;

.field private d:LX/HDT;

.field private e:LX/01T;


# direct methods
.method public constructor <init>(LX/FQY;LX/17Y;Lcom/facebook/content/SecureContextHelper;LX/HDT;LX/01T;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2475269
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2475270
    iput-object p1, p0, LX/HVf;->a:LX/FQY;

    .line 2475271
    iput-object p2, p0, LX/HVf;->b:LX/17Y;

    .line 2475272
    iput-object p3, p0, LX/HVf;->c:Lcom/facebook/content/SecureContextHelper;

    .line 2475273
    iput-object p4, p0, LX/HVf;->d:LX/HDT;

    .line 2475274
    iput-object p5, p0, LX/HVf;->e:LX/01T;

    .line 2475275
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;)V
    .locals 4

    .prologue
    .line 2475276
    iget-wide v0, p3, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->a:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2475277
    iget-object v0, p0, LX/HVf;->e:LX/01T;

    sget-object v1, LX/01T;->PAA:LX/01T;

    if-ne v0, v1, :cond_0

    .line 2475278
    iget-object v0, p0, LX/HVf;->d:LX/HDT;

    new-instance v1, LX/HDd;

    invoke-direct {v1}, LX/HDd;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2475279
    :goto_0
    return-void

    .line 2475280
    :cond_0
    iget-object v0, p0, LX/HVf;->a:LX/FQY;

    iget-wide v2, p3, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->a:J

    invoke-virtual {v0, v2, v3}, LX/FQY;->a(J)Landroid/content/Intent;

    move-result-object v0

    .line 2475281
    if-eqz v0, :cond_2

    .line 2475282
    iget-object v1, p0, LX/HVf;->a:LX/FQY;

    sget-object v2, LX/8A0;->MESSAGES:LX/8A0;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 2475283
    if-eqz v0, :cond_1

    .line 2475284
    const-string p0, "popup_state"

    invoke-virtual {v2}, LX/8A0;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2475285
    iget-object p0, v1, LX/FQY;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {p0, v0, v3}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2475286
    :cond_1
    goto :goto_0

    .line 2475287
    :cond_2
    const-string v0, "https://m.facebook.com/messages/?pageID=%s"

    iget-wide v2, p3, Lcom/facebook/pages/common/contextitems/handlingdata/PageContextItemHandlingData;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2475288
    iget-object v1, p0, LX/HVf;->b:LX/17Y;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v2, v0}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2475289
    const-string v1, "uri_unhandled_report_category_name"

    const-string v2, "PageFacewebUriNotHandled"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2475290
    iget-object v1, p0, LX/HVf;->c:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method
