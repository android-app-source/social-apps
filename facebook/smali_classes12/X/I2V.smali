.class public LX/I2V;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field public final b:LX/I2b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/I2b",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(LX/I2b;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/I2b",
            "<TT;>;TT;)V"
        }
    .end annotation

    .prologue
    .line 2530351
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2530352
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2530353
    iput-object p1, p0, LX/I2V;->b:LX/I2b;

    .line 2530354
    iput-object p2, p0, LX/I2V;->a:Ljava/lang/Object;

    .line 2530355
    return-void
.end method

.method public static a(LX/I2b;Ljava/lang/Object;)LX/I2V;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/I2b",
            "<TT;>;TT;)",
            "LX/I2V",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 2530356
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/I2V;

    invoke-direct {v0, p0, p1}, LX/I2V;-><init>(LX/I2b;Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2530357
    if-ne p0, p1, :cond_1

    .line 2530358
    :cond_0
    :goto_0
    return v0

    .line 2530359
    :cond_1
    if-eqz p1, :cond_2

    instance-of v2, p1, LX/I2V;

    if-nez v2, :cond_3

    :cond_2
    move v0, v1

    .line 2530360
    goto :goto_0

    .line 2530361
    :cond_3
    check-cast p1, LX/I2V;

    .line 2530362
    iget-object v2, p1, LX/I2V;->a:Ljava/lang/Object;

    move-object v2, v2

    .line 2530363
    iget-object v3, p0, LX/I2V;->a:Ljava/lang/Object;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2530364
    iget-object v2, p1, LX/I2V;->b:LX/I2b;

    move-object v2, v2

    .line 2530365
    iget-object v3, p0, LX/I2V;->b:LX/I2b;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2530366
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/I2V;->b:LX/I2b;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/I2V;->a:Ljava/lang/Object;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
