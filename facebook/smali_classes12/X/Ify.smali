.class public LX/Ify;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Ifx;


# instance fields
.field public a:LX/6Z6;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/03V;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private final c:Landroid/app/PendingIntent;

.field public d:LX/Ifv;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0aU;)V
    .locals 3
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2600308
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2600309
    const-string v0, "LIVE_LOCATION_UPDATE"

    invoke-virtual {p2, v0}, LX/0aU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2600310
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2600311
    invoke-static {p1, v2, v1, v2}, LX/0nt;->b(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, LX/Ify;->c:Landroid/app/PendingIntent;

    .line 2600312
    return-void
.end method

.method public static b(LX/Ify;)V
    .locals 7

    .prologue
    .line 2600298
    iget-object v0, p0, LX/Ify;->d:LX/Ifv;

    .line 2600299
    iget v1, v0, LX/Ifv;->m:I

    move v0, v1

    .line 2600300
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 2600301
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sget v1, LX/Ifv;->a:I

    if-ne v0, v1, :cond_0

    .line 2600302
    iget-object v0, p0, LX/Ify;->a:LX/6Z6;

    iget-object v1, p0, LX/Ify;->c:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, LX/6Z6;->a(Landroid/app/PendingIntent;)V

    .line 2600303
    :goto_0
    return-void

    .line 2600304
    :cond_0
    new-instance v0, LX/2Cu;

    sget-object v1, LX/2Cv;->HIGH_ACCURACY:LX/2Cv;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    int-to-long v2, v2

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-long v4, v4

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, LX/2Cu;-><init>(LX/2Cv;JJF)V

    .line 2600305
    :try_start_0
    iget-object v1, p0, LX/Ify;->a:LX/6Z6;

    iget-object v2, p0, LX/Ify;->c:Landroid/app/PendingIntent;

    invoke-virtual {v1, v2, v0}, LX/6Z6;->a(Landroid/app/PendingIntent;LX/2Cu;)V
    :try_end_0
    .catch LX/6ZF; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2600306
    :catch_0
    move-exception v0

    .line 2600307
    iget-object v1, p0, LX/Ify;->b:LX/03V;

    const-string v2, "live_location_start_sharing"

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 2600313
    invoke-static {p0}, LX/Ify;->b(LX/Ify;)V

    .line 2600314
    return-void
.end method
