.class public final LX/I6K;
.super LX/1wH;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1wH",
        "<",
        "Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic b:LX/I6L;


# direct methods
.method public constructor <init>(LX/I6L;)V
    .locals 0

    .prologue
    .line 2537478
    iput-object p1, p0, LX/I6K;->b:LX/I6L;

    invoke-direct {p0, p1}, LX/1wH;-><init>(LX/1SX;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Menu;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2537480
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2537481
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 2537482
    const v1, 0x7f081044

    invoke-interface {p1, v1}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 2537483
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    invoke-static {v2}, LX/Al4;->a(Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;)I

    move-result v2

    .line 2537484
    iget-object v3, p0, LX/I6K;->b:LX/I6L;

    .line 2537485
    invoke-virtual {v3, v1, v2, v0}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 2537486
    new-instance v2, LX/I6J;

    invoke-direct {v2, p0, v0, p3, p2}, LX/I6J;-><init>(LX/I6K;Lcom/facebook/graphql/model/FeedUnit;Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2537487
    return-void
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnit;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2537479
    const/4 v0, 0x1

    return v0
.end method
