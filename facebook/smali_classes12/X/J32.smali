.class public final enum LX/J32;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/J32;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/J32;

.field public static final enum ADD_ITEM_FORM:LX/J32;

.field public static final enum CHECKOUT_JSON_CONFIG:LX/J32;

.field public static final enum CONSENT_SCREEN:LX/J32;

.field public static final enum CREATE_INVOICE:LX/J32;

.field public static final enum CREATE_INVOICE_API:LX/J32;

.field public static final enum EDIT_CART:LX/J32;

.field public static final enum EDIT_ITEM_FORM:LX/J32;

.field public static final enum NOTE_FORM:LX/J32;

.field public static final enum PAYMENT_PROVIDERS_VIEW:LX/J32;

.field public static final enum RECEIPT_SCREEN:LX/J32;

.field public static final enum SELECTOR:LX/J32;

.field public static final enum SHIPPING_METHOD_FORM:LX/J32;


# instance fields
.field private final buttonText:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2641994
    new-instance v0, LX/J32;

    const-string v1, "CHECKOUT_JSON_CONFIG"

    const-string v2, "Checkout Json Config"

    invoke-direct {v0, v1, v4, v2}, LX/J32;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J32;->CHECKOUT_JSON_CONFIG:LX/J32;

    .line 2641995
    new-instance v0, LX/J32;

    const-string v1, "EDIT_CART"

    const-string v2, "Edit Cart"

    invoke-direct {v0, v1, v5, v2}, LX/J32;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J32;->EDIT_CART:LX/J32;

    .line 2641996
    new-instance v0, LX/J32;

    const-string v1, "NOTE_FORM"

    const-string v2, "Note Form"

    invoke-direct {v0, v1, v6, v2}, LX/J32;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J32;->NOTE_FORM:LX/J32;

    .line 2641997
    new-instance v0, LX/J32;

    const-string v1, "SHIPPING_METHOD_FORM"

    const-string v2, "Shipping Method Form"

    invoke-direct {v0, v1, v7, v2}, LX/J32;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J32;->SHIPPING_METHOD_FORM:LX/J32;

    .line 2641998
    new-instance v0, LX/J32;

    const-string v1, "ADD_ITEM_FORM"

    const-string v2, "Add Item Form"

    invoke-direct {v0, v1, v8, v2}, LX/J32;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J32;->ADD_ITEM_FORM:LX/J32;

    .line 2641999
    new-instance v0, LX/J32;

    const-string v1, "EDIT_ITEM_FORM"

    const/4 v2, 0x5

    const-string v3, "Edit Item Form"

    invoke-direct {v0, v1, v2, v3}, LX/J32;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J32;->EDIT_ITEM_FORM:LX/J32;

    .line 2642000
    new-instance v0, LX/J32;

    const-string v1, "SELECTOR"

    const/4 v2, 0x6

    const-string v3, "Selector"

    invoke-direct {v0, v1, v2, v3}, LX/J32;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J32;->SELECTOR:LX/J32;

    .line 2642001
    new-instance v0, LX/J32;

    const-string v1, "CREATE_INVOICE"

    const/4 v2, 0x7

    const-string v3, "Create Invoice"

    invoke-direct {v0, v1, v2, v3}, LX/J32;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J32;->CREATE_INVOICE:LX/J32;

    .line 2642002
    new-instance v0, LX/J32;

    const-string v1, "CREATE_INVOICE_API"

    const/16 v2, 0x8

    const-string v3, "Create Invoice API"

    invoke-direct {v0, v1, v2, v3}, LX/J32;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J32;->CREATE_INVOICE_API:LX/J32;

    .line 2642003
    new-instance v0, LX/J32;

    const-string v1, "PAYMENT_PROVIDERS_VIEW"

    const/16 v2, 0x9

    const-string v3, "Payment Providers View"

    invoke-direct {v0, v1, v2, v3}, LX/J32;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J32;->PAYMENT_PROVIDERS_VIEW:LX/J32;

    .line 2642004
    new-instance v0, LX/J32;

    const-string v1, "CONSENT_SCREEN"

    const/16 v2, 0xa

    const-string v3, "Consent Screen"

    invoke-direct {v0, v1, v2, v3}, LX/J32;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J32;->CONSENT_SCREEN:LX/J32;

    .line 2642005
    new-instance v0, LX/J32;

    const-string v1, "RECEIPT_SCREEN"

    const/16 v2, 0xb

    const-string v3, "Receipt Screen"

    invoke-direct {v0, v1, v2, v3}, LX/J32;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/J32;->RECEIPT_SCREEN:LX/J32;

    .line 2642006
    const/16 v0, 0xc

    new-array v0, v0, [LX/J32;

    sget-object v1, LX/J32;->CHECKOUT_JSON_CONFIG:LX/J32;

    aput-object v1, v0, v4

    sget-object v1, LX/J32;->EDIT_CART:LX/J32;

    aput-object v1, v0, v5

    sget-object v1, LX/J32;->NOTE_FORM:LX/J32;

    aput-object v1, v0, v6

    sget-object v1, LX/J32;->SHIPPING_METHOD_FORM:LX/J32;

    aput-object v1, v0, v7

    sget-object v1, LX/J32;->ADD_ITEM_FORM:LX/J32;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/J32;->EDIT_ITEM_FORM:LX/J32;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/J32;->SELECTOR:LX/J32;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/J32;->CREATE_INVOICE:LX/J32;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/J32;->CREATE_INVOICE_API:LX/J32;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/J32;->PAYMENT_PROVIDERS_VIEW:LX/J32;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/J32;->CONSENT_SCREEN:LX/J32;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/J32;->RECEIPT_SCREEN:LX/J32;

    aput-object v2, v0, v1

    sput-object v0, LX/J32;->$VALUES:[LX/J32;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2641991
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2641992
    iput-object p3, p0, LX/J32;->buttonText:Ljava/lang/String;

    .line 2641993
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/J32;
    .locals 1

    .prologue
    .line 2641990
    const-class v0, LX/J32;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/J32;

    return-object v0
.end method

.method public static values()[LX/J32;
    .locals 1

    .prologue
    .line 2641989
    sget-object v0, LX/J32;->$VALUES:[LX/J32;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/J32;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2641988
    iget-object v0, p0, LX/J32;->buttonText:Ljava/lang/String;

    return-object v0
.end method
