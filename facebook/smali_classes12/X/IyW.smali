.class public final LX/IyW;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/payments/invoice/protocol/PaymentInvoiceItemSearchResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/IyX;


# direct methods
.method public constructor <init>(LX/IyX;)V
    .locals 0

    .prologue
    .line 2633502
    iput-object p1, p0, LX/IyW;->a:LX/IyX;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2633503
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2633504
    check-cast p1, Lcom/facebook/payments/invoice/protocol/PaymentInvoiceItemSearchResult;

    .line 2633505
    iget-object v0, p0, LX/IyW;->a:LX/IyX;

    .line 2633506
    iget-object v1, v0, LX/IyX;->d:LX/IyH;

    iget-object v2, p1, Lcom/facebook/payments/invoice/protocol/PaymentInvoiceItemSearchResult;->a:LX/0Px;

    .line 2633507
    iget-object p0, v1, LX/IyH;->a:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result p0

    if-eqz p0, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Iy3;

    .line 2633508
    iget-object p1, p0, LX/Iy3;->a:Lcom/facebook/payments/cart/PaymentsCartFragment;

    .line 2633509
    iget-object v1, p1, Lcom/facebook/payments/cart/PaymentsCartFragment;->l:LX/Ixz;

    invoke-interface {v1}, LX/Ixz;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2633510
    iget-object v1, p1, Lcom/facebook/payments/cart/PaymentsCartFragment;->u:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 2633511
    :cond_0
    iget-object p1, p0, LX/Iy3;->a:Lcom/facebook/payments/cart/PaymentsCartFragment;

    invoke-virtual {p1, v2}, Lcom/facebook/payments/cart/PaymentsCartFragment;->a(LX/0Px;)V

    .line 2633512
    goto :goto_0

    .line 2633513
    :cond_1
    return-void
.end method
