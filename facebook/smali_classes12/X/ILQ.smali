.class public final LX/ILQ;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/community/protocol/FetchCommunityForumGroupsGraphQLModels$FetchCommunityForumGroupsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2567957
    iput-object p1, p0, LX/ILQ;->b:Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;

    iput-object p2, p0, LX/ILQ;->a:Ljava/lang/String;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    const/4 v1, -0x1

    .line 2567958
    sget v0, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->a:I

    if-gtz v0, :cond_0

    .line 2567959
    instance-of v0, p1, LX/4Ua;

    if-eqz v0, :cond_3

    move-object v0, p1

    .line 2567960
    check-cast v0, LX/4Ua;

    iget-object v0, v0, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    if-eqz v0, :cond_2

    check-cast p1, LX/4Ua;

    iget-object v0, p1, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    iget v0, v0, Lcom/facebook/graphql/error/GraphQLError;->code:I

    .line 2567961
    :goto_0
    iget-object v1, p0, LX/ILQ;->b:Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;

    iget-object v1, v1, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->g:LX/IL9;

    iget-object v2, p0, LX/ILQ;->a:Ljava/lang/String;

    const-class v3, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3}, LX/IL9;->a(Ljava/lang/String;ILjava/lang/String;)V

    .line 2567962
    :cond_0
    sget v0, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->a:I

    const/4 v1, 0x3

    if-ge v0, v1, :cond_1

    .line 2567963
    sget v0, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->a:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->a:I

    .line 2567964
    iget-object v0, p0, LX/ILQ;->b:Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;

    iget-object v1, p0, LX/ILQ;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->a$redex0(Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;Ljava/lang/String;)V

    .line 2567965
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 2567966
    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2567967
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2567968
    if-eqz p1, :cond_0

    .line 2567969
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2567970
    if-eqz v0, :cond_0

    .line 2567971
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2567972
    check-cast v0, Lcom/facebook/groups/community/protocol/FetchCommunityForumGroupsGraphQLModels$FetchCommunityForumGroupsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/community/protocol/FetchCommunityForumGroupsGraphQLModels$FetchCommunityForumGroupsModel;->a()Lcom/facebook/groups/community/protocol/FetchCommunityForumGroupsGraphQLModels$FetchCommunityForumGroupsModel$CommunityForumChildGroupsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2567973
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2567974
    check-cast v0, Lcom/facebook/groups/community/protocol/FetchCommunityForumGroupsGraphQLModels$FetchCommunityForumGroupsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/community/protocol/FetchCommunityForumGroupsGraphQLModels$FetchCommunityForumGroupsModel;->a()Lcom/facebook/groups/community/protocol/FetchCommunityForumGroupsGraphQLModels$FetchCommunityForumGroupsModel$CommunityForumChildGroupsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/community/protocol/FetchCommunityForumGroupsGraphQLModels$FetchCommunityForumGroupsModel$CommunityForumChildGroupsModel;->a()LX/0Px;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2567975
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "result from fetching community forum groups was null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2567976
    :cond_1
    iget-object v1, p0, LX/ILQ;->b:Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;

    .line 2567977
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2567978
    check-cast v0, Lcom/facebook/groups/community/protocol/FetchCommunityForumGroupsGraphQLModels$FetchCommunityForumGroupsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/community/protocol/FetchCommunityForumGroupsGraphQLModels$FetchCommunityForumGroupsModel;->a()Lcom/facebook/groups/community/protocol/FetchCommunityForumGroupsGraphQLModels$FetchCommunityForumGroupsModel$CommunityForumChildGroupsModel;

    move-result-object v0

    .line 2567979
    iput-object v0, v1, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->j:Lcom/facebook/groups/community/protocol/FetchCommunityForumGroupsGraphQLModels$FetchCommunityForumGroupsModel$CommunityForumChildGroupsModel;

    .line 2567980
    iget-object v0, p0, LX/ILQ;->b:Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;

    iget-object v1, p0, LX/ILQ;->b:Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;

    iget-object v1, v1, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->j:Lcom/facebook/groups/community/protocol/FetchCommunityForumGroupsGraphQLModels$FetchCommunityForumGroupsModel$CommunityForumChildGroupsModel;

    invoke-virtual {v1}, Lcom/facebook/groups/community/protocol/FetchCommunityForumGroupsGraphQLModels$FetchCommunityForumGroupsModel$CommunityForumChildGroupsModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->a$redex0(Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;LX/0Px;)V

    .line 2567981
    iget-object v0, p0, LX/ILQ;->b:Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;

    iget-object v1, p0, LX/ILQ;->b:Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;

    iget-object v1, v1, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->j:Lcom/facebook/groups/community/protocol/FetchCommunityForumGroupsGraphQLModels$FetchCommunityForumGroupsModel$CommunityForumChildGroupsModel;

    invoke-virtual {v1}, Lcom/facebook/groups/community/protocol/FetchCommunityForumGroupsGraphQLModels$FetchCommunityForumGroupsModel$CommunityForumChildGroupsModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;->b$redex0(Lcom/facebook/groups/community/fragments/CommunityJoinGroupsNuxFragment;LX/0Px;)V

    .line 2567982
    return-void
.end method
