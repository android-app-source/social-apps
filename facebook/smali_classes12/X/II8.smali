.class public final LX/II8;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationSettingsMutationsModels$BackgroundLocationSettingsPauseMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/support/v4/app/DialogFragment;

.field public final synthetic b:LX/IIm;

.field public final synthetic c:Lcom/facebook/widget/SwitchCompat;

.field public final synthetic d:LX/IIB;


# direct methods
.method public constructor <init>(LX/IIB;Landroid/support/v4/app/DialogFragment;LX/IIm;Lcom/facebook/widget/SwitchCompat;)V
    .locals 0

    .prologue
    .line 2561403
    iput-object p1, p0, LX/II8;->d:LX/IIB;

    iput-object p2, p0, LX/II8;->a:Landroid/support/v4/app/DialogFragment;

    iput-object p3, p0, LX/II8;->b:LX/IIm;

    iput-object p4, p0, LX/II8;->c:Lcom/facebook/widget/SwitchCompat;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2561404
    iget-object v0, p0, LX/II8;->a:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2561405
    iget-object v0, p0, LX/II8;->c:Lcom/facebook/widget/SwitchCompat;

    if-eqz v0, :cond_0

    .line 2561406
    iget-object v1, p0, LX/II8;->c:Lcom/facebook/widget/SwitchCompat;

    iget-object v0, p0, LX/II8;->c:Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {v0}, Lcom/facebook/widget/SwitchCompat;->isChecked()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/widget/SwitchCompat;->setChecked(Z)V

    .line 2561407
    :cond_0
    iget-object v0, p0, LX/II8;->d:LX/IIB;

    iget-object v0, v0, LX/IIB;->j:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f080039

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2561408
    iget-object v0, p0, LX/II8;->d:LX/IIB;

    iget-object v0, v0, LX/IIB;->c:LX/03V;

    const-string v1, "friends_nearby_pause_failed"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2561409
    return-void

    .line 2561410
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2561411
    iget-object v0, p0, LX/II8;->a:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2561412
    iget-object v0, p0, LX/II8;->b:LX/IIm;

    invoke-virtual {v0}, LX/IIm;->e()V

    .line 2561413
    return-void
.end method
