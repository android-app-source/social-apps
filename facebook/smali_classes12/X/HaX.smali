.class public final enum LX/HaX;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HaX;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/HaX;

.field public static final enum ANIMATED:LX/HaX;

.field public static final enum INITIAL:LX/HaX;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2483953
    new-instance v0, LX/HaX;

    const-string v1, "INITIAL"

    invoke-direct {v0, v1, v2}, LX/HaX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HaX;->INITIAL:LX/HaX;

    .line 2483954
    new-instance v0, LX/HaX;

    const-string v1, "ANIMATED"

    invoke-direct {v0, v1, v3}, LX/HaX;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HaX;->ANIMATED:LX/HaX;

    .line 2483955
    const/4 v0, 0x2

    new-array v0, v0, [LX/HaX;

    sget-object v1, LX/HaX;->INITIAL:LX/HaX;

    aput-object v1, v0, v2

    sget-object v1, LX/HaX;->ANIMATED:LX/HaX;

    aput-object v1, v0, v3

    sput-object v0, LX/HaX;->$VALUES:[LX/HaX;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2483950
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HaX;
    .locals 1

    .prologue
    .line 2483951
    const-class v0, LX/HaX;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HaX;

    return-object v0
.end method

.method public static values()[LX/HaX;
    .locals 1

    .prologue
    .line 2483952
    sget-object v0, LX/HaX;->$VALUES:[LX/HaX;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HaX;

    return-object v0
.end method
