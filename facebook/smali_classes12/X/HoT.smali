.class public final LX/HoT;
.super LX/0zP;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0zP",
        "<",
        "Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceMutationModels$InvoiceEditModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 12

    .prologue
    .line 2505822
    const-class v1, Lcom/facebook/commerce/invoices/graphql/TransactionInvoiceMutationModels$InvoiceEditModel;

    const v0, 0x6cbe28c8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "InvoiceEdit"

    const-string v6, "aea4a1b9c2d50d7b5f5fcd7cb8f48d88"

    const-string v7, "invoice_edit_as_seller"

    const-string v8, "0"

    const-string v9, "10155141962541729"

    const/4 v10, 0x0

    .line 2505823
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v11, v0

    .line 2505824
    move-object v0, p0

    invoke-direct/range {v0 .. v11}, LX/0zP;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2505825
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2505826
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2505827
    sparse-switch v0, :sswitch_data_0

    .line 2505828
    :goto_0
    return-object p1

    .line 2505829
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 2505830
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 2505831
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x5fb57ca -> :sswitch_0
        0x12df35c1 -> :sswitch_1
        0x6f9d60cc -> :sswitch_2
    .end sparse-switch
.end method
