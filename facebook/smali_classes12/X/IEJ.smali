.class public final LX/IEJ;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchRecentlyAddedFriendListQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 2551785
    const-class v1, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchRecentlyAddedFriendListQueryModel;

    const v0, -0x208f3c74

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "FetchRecentlyAddedFriendListQuery"

    const-string v6, "85e14de3e47dc99a1eb9664a7550dc82"

    const-string v7, "user"

    const-string v8, "10155192631696729"

    const-string v9, "10155259088181729"

    .line 2551786
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 2551787
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2551788
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2551789
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2551790
    sparse-switch v0, :sswitch_data_0

    .line 2551791
    :goto_0
    return-object p1

    .line 2551792
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 2551793
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 2551794
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 2551795
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 2551796
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x76d1794f -> :sswitch_4
        -0x41b8e48f -> :sswitch_0
        0xa174e6b -> :sswitch_2
        0x1f588673 -> :sswitch_1
        0x6c6f579a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2551797
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    packed-switch v1, :pswitch_data_1

    .line 2551798
    :goto_1
    return v0

    .line 2551799
    :pswitch_1
    const-string v2, "2"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    :pswitch_2
    const-string v2, "4"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    .line 2551800
    :pswitch_3
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 2551801
    :pswitch_4
    invoke-static {p2}, LX/0wE;->b(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x32
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
