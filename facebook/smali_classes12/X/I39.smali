.class public final LX/I39;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;",
        "Lcom/facebook/events/model/Event;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/I3A;


# direct methods
.method public constructor <init>(LX/I3A;)V
    .locals 0

    .prologue
    .line 2531072
    iput-object p1, p0, LX/I39;->a:LX/I3A;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2531073
    check-cast p1, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;

    .line 2531074
    if-nez p1, :cond_0

    .line 2531075
    const/4 v0, 0x0

    .line 2531076
    :goto_0
    return-object v0

    .line 2531077
    :cond_0
    invoke-static {p1}, LX/Bm1;->c(LX/7oa;)LX/7vC;

    move-result-object v1

    .line 2531078
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ab()Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel$EventViewerSubscribedSourceProfilesModel;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ab()Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel$EventViewerSubscribedSourceProfilesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel$EventViewerSubscribedSourceProfilesModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ab()Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel$EventViewerSubscribedSourceProfilesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel$EventViewerSubscribedSourceProfilesModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2531079
    invoke-virtual {p1}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel;->ab()Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel$EventViewerSubscribedSourceProfilesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel$EventViewerSubscribedSourceProfilesModel;->a()LX/0Px;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel$EventViewerSubscribedSourceProfilesModel$NodesModel;

    .line 2531080
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel$EventViewerSubscribedSourceProfilesModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v2

    .line 2531081
    iput-object v2, v1, LX/7vC;->al:Ljava/lang/String;

    .line 2531082
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$SubscribedProfileCalendarEventModel$EventViewerSubscribedSourceProfilesModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v0

    .line 2531083
    iput-object v0, v1, LX/7vC;->am:Ljava/lang/String;

    .line 2531084
    :cond_1
    invoke-virtual {v1}, LX/7vC;->b()Lcom/facebook/events/model/Event;

    move-result-object v0

    goto :goto_0
.end method
