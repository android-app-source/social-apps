.class public LX/JNR;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JNS;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/JNR",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/JNS;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2685894
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2685895
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/JNR;->b:LX/0Zi;

    .line 2685896
    iput-object p1, p0, LX/JNR;->a:LX/0Ot;

    .line 2685897
    return-void
.end method

.method public static a(LX/0QB;)LX/JNR;
    .locals 4

    .prologue
    .line 2685898
    const-class v1, LX/JNR;

    monitor-enter v1

    .line 2685899
    :try_start_0
    sget-object v0, LX/JNR;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2685900
    sput-object v2, LX/JNR;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2685901
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2685902
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2685903
    new-instance v3, LX/JNR;

    const/16 p0, 0x1ee9

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/JNR;-><init>(LX/0Ot;)V

    .line 2685904
    move-object v0, v3

    .line 2685905
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2685906
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/JNR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2685907
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2685908
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2685909
    invoke-static {}, LX/1dS;->b()V

    .line 2685910
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(LX/1De;IILX/1X1;)LX/1Dg;
    .locals 10

    .prologue
    .line 2685911
    check-cast p4, LX/JNQ;

    .line 2685912
    iget-object v0, p0, LX/JNR;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JNS;

    iget-object v1, p4, LX/JNQ;->a:Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;

    const/4 v3, 0x0

    const/4 p0, 0x2

    const/4 p4, 0x0

    const/4 p3, 0x1

    .line 2685913
    invoke-static {p2}, LX/1mh;->b(I)I

    move-result v2

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0b59

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    sub-int/2addr v2, v4

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0b63

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    sub-int/2addr v2, v4

    .line 2685914
    invoke-static {p1, v1}, LX/Bng;->a(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;)LX/Bnf;

    move-result-object v5

    .line 2685915
    new-instance v4, LX/1no;

    invoke-direct {v4}, LX/1no;-><init>()V

    .line 2685916
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v6

    iget-object v7, v5, LX/Bnf;->a:Ljava/lang/String;

    invoke-virtual {v6, v7}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v6

    const v7, 0x7f0a010c

    invoke-virtual {v6, v7}, LX/1ne;->n(I)LX/1ne;

    move-result-object v6

    const v7, 0x7f0b0050

    invoke-virtual {v6, v7}, LX/1ne;->q(I)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, p3}, LX/1ne;->t(I)LX/1ne;

    move-result-object v6

    sget-object v7, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v6, v7}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v6

    const v7, 0x7f0b0034

    invoke-virtual {v6, v7}, LX/1ne;->s(I)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, p0}, LX/1ne;->j(I)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, p4}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->d()LX/1X1;

    move-result-object v6

    .line 2685917
    sget v7, LX/JNS;->a:I

    sget v8, LX/JNS;->a:I

    invoke-virtual {v6, p1, v7, v8, v4}, LX/1X1;->a(LX/1De;IILX/1no;)V

    .line 2685918
    iget-object v7, v5, LX/Bnf;->c:Ljava/lang/String;

    invoke-static {v7}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    iget v4, v4, LX/1no;->a:I

    if-lt v2, v4, :cond_2

    .line 2685919
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v2

    iget-object v4, v5, LX/Bnf;->c:Ljava/lang/String;

    invoke-virtual {v2, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    const v4, 0x7f0a00a4

    invoke-virtual {v2, v4}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    const v4, 0x7f0b004e

    invoke-virtual {v2, v4}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, p4}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, p3}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v2

    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v4}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->d()LX/1X1;

    move-result-object v2

    .line 2685920
    :goto_0
    iget-object v4, v0, LX/JNS;->b:LX/6RZ;

    iget-object v7, v5, LX/Bnf;->d:Ljava/util/Date;

    invoke-virtual {v4, v7}, LX/6RZ;->c(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    .line 2685921
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v7

    const v8, 0x7f0b0b58

    invoke-interface {v7, v8}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, p3}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v7

    const/4 v8, 0x5

    const v9, 0x7f0b0b63

    invoke-interface {v7, v8, v9}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v7

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v8

    invoke-interface {v8, p4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v8

    invoke-interface {v8, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v8

    invoke-interface {v8, p3}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v8

    const v9, 0x7f0b0b59

    invoke-interface {v8, v9}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v8

    const v9, 0x7f0b0b59

    invoke-interface {v8, v9}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v8

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v9

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_1

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    :goto_1
    invoke-virtual {v9, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    const v9, 0x7f0a0094

    invoke-virtual {v4, v9}, LX/1ne;->n(I)LX/1ne;

    move-result-object v4

    const v9, 0x7f0b004e

    invoke-virtual {v4, v9}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, p3}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, p4}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v4

    invoke-interface {v8, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v8

    iget-object v9, v0, LX/JNS;->b:LX/6RZ;

    iget-object p0, v5, LX/Bnf;->d:Ljava/util/Date;

    invoke-virtual {v9, p0}, LX/6RZ;->d(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v8

    const v9, 0x7f0a00ab

    invoke-virtual {v8, v9}, LX/1ne;->n(I)LX/1ne;

    move-result-object v8

    const v9, 0x7f0b0056

    invoke-virtual {v8, v9}, LX/1ne;->q(I)LX/1ne;

    move-result-object v8

    sget-object v9, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    invoke-virtual {v8, v9}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    move-result-object v8

    invoke-virtual {v8, p3}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v8

    invoke-virtual {v8, p4}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v8

    invoke-interface {v4, v8}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    invoke-interface {v7, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, p4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v7

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-interface {v7, v8}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, p3}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, v6}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v6

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v7

    iget-object v8, v5, LX/Bnf;->b:Ljava/lang/CharSequence;

    if-eqz v8, :cond_0

    iget-object v3, v5, LX/Bnf;->b:Ljava/lang/CharSequence;

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    :cond_0
    invoke-virtual {v7, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    const v5, 0x7f0a00a4

    invoke-virtual {v3, v5}, LX/1ne;->n(I)LX/1ne;

    move-result-object v3

    const v5, 0x7f0b004e

    invoke-virtual {v3, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, p4}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, p3}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v3

    sget-object v5, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v3, v5}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v3

    const v5, 0x7f0b0b62

    invoke-virtual {v3, v5}, LX/1ne;->s(I)LX/1ne;

    move-result-object v3

    invoke-interface {v6, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v2}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v2

    invoke-interface {v4, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2685922
    return-object v0

    :cond_1
    move-object v4, v3

    goto/16 :goto_1

    :cond_2
    move-object v2, v3

    goto/16 :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2685923
    const/4 v0, 0x1

    return v0
.end method
