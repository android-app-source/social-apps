.class public LX/J3t;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field public static j:Z

.field private static final n:Ljava/lang/Object;


# instance fields
.field public a:LX/0SG;

.field public final b:LX/1Ck;

.field public final c:Ljava/util/concurrent/ExecutorService;

.field public final d:LX/J5g;

.field private final e:LX/J49;

.field public final f:LX/J4N;

.field private final g:LX/03V;

.field public h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/1oT;",
            ">;"
        }
    .end annotation
.end field

.field public i:Ljava/lang/String;

.field public k:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/1oT;",
            ">;"
        }
    .end annotation
.end field

.field public final l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/J4L;",
            ">;"
        }
    .end annotation
.end field

.field public final m:Lcom/facebook/privacy/PrivacyOperationsClient;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2643383
    const/4 v0, 0x0

    sput-boolean v0, LX/J3t;->j:Z

    .line 2643384
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/J3t;->n:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0SG;LX/1Ck;Ljava/util/concurrent/ExecutorService;LX/J5g;Lcom/facebook/privacy/PrivacyOperationsClient;LX/J49;LX/J4N;LX/03V;)V
    .locals 1
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2643369
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2643370
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/J3t;->l:Ljava/util/List;

    .line 2643371
    iput-object p2, p0, LX/J3t;->b:LX/1Ck;

    .line 2643372
    iput-object p3, p0, LX/J3t;->c:Ljava/util/concurrent/ExecutorService;

    .line 2643373
    iput-object p4, p0, LX/J3t;->d:LX/J5g;

    .line 2643374
    iput-object p5, p0, LX/J3t;->m:Lcom/facebook/privacy/PrivacyOperationsClient;

    .line 2643375
    iput-object p6, p0, LX/J3t;->e:LX/J49;

    .line 2643376
    iget-object v0, p0, LX/J3t;->e:LX/J49;

    invoke-virtual {v0}, LX/J49;->b()V

    .line 2643377
    iput-object p7, p0, LX/J3t;->f:LX/J4N;

    .line 2643378
    iput-object p8, p0, LX/J3t;->g:LX/03V;

    .line 2643379
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/J3t;->h:Ljava/util/Map;

    .line 2643380
    iput-object p1, p0, LX/J3t;->a:LX/0SG;

    .line 2643381
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/J3t;->k:Ljava/util/Map;

    .line 2643382
    return-void
.end method

.method public static a(LX/0QB;)LX/J3t;
    .locals 7

    .prologue
    .line 2643342
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2643343
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2643344
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2643345
    if-nez v1, :cond_0

    .line 2643346
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2643347
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2643348
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2643349
    sget-object v1, LX/J3t;->n:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2643350
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2643351
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2643352
    :cond_1
    if-nez v1, :cond_4

    .line 2643353
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2643354
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2643355
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/J3t;->b(LX/0QB;)LX/J3t;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 2643356
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2643357
    if-nez v1, :cond_2

    .line 2643358
    sget-object v0, LX/J3t;->n:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/J3t;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2643359
    :goto_1
    if-eqz v0, :cond_3

    .line 2643360
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2643361
    :goto_3
    check-cast v0, LX/J3t;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2643362
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2643363
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2643364
    :catchall_1
    move-exception v0

    .line 2643365
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2643366
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2643367
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2643368
    :cond_2
    :try_start_8
    sget-object v0, LX/J3t;->n:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/J3t;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static b(LX/0QB;)LX/J3t;
    .locals 9

    .prologue
    .line 2643340
    new-instance v0, LX/J3t;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v2

    check-cast v2, LX/1Ck;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/J5g;->a(LX/0QB;)LX/J5g;

    move-result-object v4

    check-cast v4, LX/J5g;

    invoke-static {p0}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(LX/0QB;)Lcom/facebook/privacy/PrivacyOperationsClient;

    move-result-object v5

    check-cast v5, Lcom/facebook/privacy/PrivacyOperationsClient;

    invoke-static {p0}, LX/J49;->a(LX/0QB;)LX/J49;

    move-result-object v6

    check-cast v6, LX/J49;

    invoke-static {p0}, LX/J4N;->a(LX/0QB;)LX/J4N;

    move-result-object v7

    check-cast v7, LX/J4N;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-direct/range {v0 .. v8}, LX/J3t;-><init>(LX/0SG;LX/1Ck;Ljava/util/concurrent/ExecutorService;LX/J5g;Lcom/facebook/privacy/PrivacyOperationsClient;LX/J49;LX/J4N;LX/03V;)V

    .line 2643341
    return-object v0
.end method


# virtual methods
.method public final a(I)LX/J4L;
    .locals 1

    .prologue
    .line 2643337
    iget-object v0, p0, LX/J3t;->l:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/J4L;

    .line 2643338
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2643339
    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 2643385
    iget-object v0, p0, LX/J3t;->k:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 2643386
    iget-object v0, p0, LX/J3t;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 2643387
    return-void
.end method

.method public final a(LX/J3r;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p3    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2643335
    iget-object v0, p0, LX/J3t;->b:LX/1Ck;

    sget-object v1, LX/J3s;->FETCH_GENERIC_PRIVACY_REVIEW_INFO:LX/J3s;

    new-instance v2, LX/J3o;

    invoke-direct {v2, p0, p3, p2}, LX/J3o;-><init>(LX/J3t;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, LX/J3q;

    invoke-direct {v3, p0, p1}, LX/J3q;-><init>(LX/J3t;LX/J3r;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2643336
    return-void
.end method

.method public final a(Ljava/lang/String;LX/0Px;LX/0Vd;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/privacy/protocol/EditObjectsPrivacyParams$ObjectPrivacyEdit;",
            ">;",
            "LX/0Vd",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2643334
    iget-object v0, p0, LX/J3t;->b:LX/1Ck;

    sget-object v1, LX/J3s;->SEND_PRIVACY_EDITS:LX/J3s;

    new-instance v2, LX/J3p;

    invoke-direct {v2, p0, p1, p2}, LX/J3p;-><init>(LX/J3t;Ljava/lang/String;LX/0Px;)V

    invoke-virtual {v0, v1, v2, p3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    move-result v0

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2643331
    iget-object v0, p0, LX/J3t;->h:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/J3t;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 2643332
    :cond_0
    const/4 v0, 0x0

    .line 2643333
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, LX/J3t;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    goto :goto_0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 2643330
    iget-object v0, p0, LX/J3t;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final c(I)LX/0Px;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/0Px",
            "<",
            "Lcom/facebook/privacy/protocol/EditObjectsPrivacyParams$ObjectPrivacyEdit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2643319
    iget-object v0, p0, LX/J3t;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2643320
    const/4 v0, 0x0

    .line 2643321
    :goto_0
    return-object v0

    .line 2643322
    :cond_0
    iget-object v0, p0, LX/J3t;->l:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, LX/J4L;

    .line 2643323
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    .line 2643324
    iget-object v0, p0, LX/J3t;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Ljava/util/Map$Entry;

    .line 2643325
    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2643326
    iget-object v1, v6, LX/J4L;->b:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, LX/J4J;

    .line 2643327
    new-instance v0, Lcom/facebook/privacy/protocol/EditObjectsPrivacyParams$ObjectPrivacyEdit;

    iget-object v1, v4, LX/J4J;->a:Ljava/lang/String;

    iget-object v2, p0, LX/J3t;->a:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    iget-object v4, v4, LX/J4J;->b:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/1oT;

    invoke-interface {v5}, LX/1oT;->c()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/facebook/privacy/protocol/EditObjectsPrivacyParams$ObjectPrivacyEdit;-><init>(Ljava/lang/String;JLcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;Ljava/lang/String;)V

    .line 2643328
    invoke-virtual {v7, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 2643329
    :cond_1
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 2643317
    iget-object v0, p0, LX/J3t;->b:LX/1Ck;

    sget-object v1, LX/J3s;->SEND_PRIVACY_EDITS:LX/J3s;

    invoke-virtual {v0, v1}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2643318
    return-void
.end method
