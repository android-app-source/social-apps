.class public final LX/HlF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/api/graphql/fetchstories/FetchInvalidStoriesGraphQlModels$FetchInvalidStoriesModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/Map;

.field public final synthetic b:LX/0oG;

.field public final synthetic c:LX/HlG;


# direct methods
.method public constructor <init>(LX/HlG;Ljava/util/Map;LX/0oG;)V
    .locals 0

    .prologue
    .line 2498242
    iput-object p1, p0, LX/HlF;->c:LX/HlG;

    iput-object p2, p0, LX/HlF;->a:Ljava/util/Map;

    iput-object p3, p0, LX/HlF;->b:LX/0oG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2498243
    iget-object v0, p0, LX/HlF;->c:LX/HlG;

    iget-object v0, v0, LX/HlG;->b:LX/HlJ;

    iget-object v1, p0, LX/HlF;->b:LX/0oG;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/HlJ;->a(LX/0oG;Ljava/lang/String;)V

    .line 2498244
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 12
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2498245
    check-cast p1, Ljava/util/List;

    const/4 v1, 0x0

    .line 2498246
    if-nez p1, :cond_0

    .line 2498247
    :goto_0
    return-void

    .line 2498248
    :cond_0
    iget-object v0, p0, LX/HlF;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v3

    .line 2498249
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/fetchstories/FetchInvalidStoriesGraphQlModels$FetchInvalidStoriesModel;

    .line 2498250
    invoke-virtual {v0}, Lcom/facebook/api/graphql/fetchstories/FetchInvalidStoriesGraphQlModels$FetchInvalidStoriesModel;->j()Ljava/lang/String;

    move-result-object v0

    .line 2498251
    if-eqz v0, :cond_1

    .line 2498252
    iget-object v4, p0, LX/HlF;->a:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 2498253
    :cond_2
    iget-object v0, p0, LX/HlF;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v4

    .line 2498254
    const-wide/16 v6, 0x0

    .line 2498255
    if-lez v4, :cond_4

    .line 2498256
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    invoke-virtual {v0, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v8

    .line 2498257
    iget-object v0, p0, LX/HlF;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move v2, v1

    move v5, v1

    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2498258
    if-ne v2, v8, :cond_3

    .line 2498259
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    invoke-interface {v1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->F_()J

    move-result-wide v6

    const-wide/32 v10, 0xea60

    div-long/2addr v6, v10

    .line 2498260
    :cond_3
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->k()Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 2498261
    add-int/lit8 v1, v5, 0x1

    .line 2498262
    :goto_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v5, v1

    .line 2498263
    goto :goto_2

    :cond_4
    move v5, v1

    .line 2498264
    :cond_5
    iget-object v0, p0, LX/HlF;->c:LX/HlG;

    iget-object v0, v0, LX/HlG;->c:LX/HlI;

    iget-object v1, p0, LX/HlF;->a:Ljava/util/Map;

    .line 2498265
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v8

    .line 2498266
    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_4
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 2498267
    invoke-static {v2}, LX/16r;->a(Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_4

    .line 2498268
    :cond_6
    iget-object v2, v0, LX/HlI;->c:LX/18A;

    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v8

    .line 2498269
    iget-object v9, v2, LX/18A;->a:Ljava/util/Set;

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/1fI;

    .line 2498270
    invoke-virtual {v9, v8}, LX/1fI;->a(LX/0Px;)V

    goto :goto_5

    .line 2498271
    :cond_7
    iget-object v0, p0, LX/HlF;->c:LX/HlG;

    iget-object v1, v0, LX/HlG;->b:LX/HlJ;

    iget-object v2, p0, LX/HlF;->b:LX/0oG;

    .line 2498272
    invoke-virtual {v2}, LX/0oG;->a()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2498273
    const-string v0, "status"

    const-string v8, "success"

    invoke-virtual {v2, v0, v8}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2498274
    const-string v0, "tried_stories_count"

    invoke-virtual {v2, v0, v3}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 2498275
    const-string v0, "deleted_stories_count"

    invoke-virtual {v2, v0, v4}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 2498276
    if-lez v4, :cond_8

    .line 2498277
    const-string v0, "marked_seen_stories_count"

    invoke-virtual {v2, v0, v5}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 2498278
    const-string v0, "random_story_fetched_time"

    invoke-virtual {v2, v0, v6, v7}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 2498279
    :cond_8
    invoke-static {v1, v2}, LX/HlJ;->a(LX/HlJ;LX/0oG;)V

    .line 2498280
    :cond_9
    goto/16 :goto_0

    :cond_a
    move v1, v5

    goto :goto_3
.end method
