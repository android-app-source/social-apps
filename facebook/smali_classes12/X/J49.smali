.class public LX/J49;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final w:Ljava/lang/Object;


# instance fields
.field public a:Z

.field public b:I

.field public c:I

.field public d:J

.field public e:I

.field public f:I

.field private g:I

.field private h:I

.field public i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/privacy/protocol/ReportPrivacyCheckupActionsParams$PrivacyCheckupItem;",
            ">;"
        }
    .end annotation
.end field

.field public j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/privacy/protocol/ReportPrivacyCheckupActionsParams$PrivacyCheckupItem;",
            ">;"
        }
    .end annotation
.end field

.field public k:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/privacy/protocol/ReportPrivacyCheckupActionsParams$PrivacyCheckupItem;",
            ">;"
        }
    .end annotation
.end field

.field private m:LX/0SG;

.field public n:LX/03V;

.field private o:LX/1Ck;

.field public p:LX/J5k;

.field private q:Lcom/facebook/privacy/PrivacyOperationsClient;

.field public r:LX/0W3;

.field public s:Ljava/lang/String;

.field public t:J

.field private final u:LX/2h0;

.field private final v:LX/2h0;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2643832
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/J49;->w:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/0SG;LX/03V;LX/1Ck;LX/J5k;Lcom/facebook/privacy/PrivacyOperationsClient;LX/0W3;)V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2643811
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2643812
    iput-boolean v1, p0, LX/J49;->a:Z

    .line 2643813
    new-instance v0, LX/J46;

    invoke-direct {v0, p0}, LX/J46;-><init>(LX/J49;)V

    iput-object v0, p0, LX/J49;->u:LX/2h0;

    .line 2643814
    new-instance v0, LX/J47;

    invoke-direct {v0, p0}, LX/J47;-><init>(LX/J49;)V

    iput-object v0, p0, LX/J49;->v:LX/2h0;

    .line 2643815
    iput-object p1, p0, LX/J49;->m:LX/0SG;

    .line 2643816
    iput-object p2, p0, LX/J49;->n:LX/03V;

    .line 2643817
    iput-object p3, p0, LX/J49;->o:LX/1Ck;

    .line 2643818
    iput-object p4, p0, LX/J49;->p:LX/J5k;

    .line 2643819
    iput-object p5, p0, LX/J49;->q:Lcom/facebook/privacy/PrivacyOperationsClient;

    .line 2643820
    iput-object p6, p0, LX/J49;->r:LX/0W3;

    .line 2643821
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/J49;->i:Ljava/util/Map;

    .line 2643822
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/J49;->j:Ljava/util/Map;

    .line 2643823
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/J49;->l:Ljava/util/Set;

    .line 2643824
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/J49;->k:Ljava/util/Map;

    .line 2643825
    iput v1, p0, LX/J49;->e:I

    .line 2643826
    iput v1, p0, LX/J49;->f:I

    .line 2643827
    iput v1, p0, LX/J49;->g:I

    .line 2643828
    iput v1, p0, LX/J49;->h:I

    .line 2643829
    iget-object v2, p0, LX/J49;->r:LX/0W3;

    sget-wide v4, LX/0X5;->gS:J

    const/4 v3, 0x3

    invoke-interface {v2, v4, v5, v3}, LX/0W4;->a(JI)I

    move-result v2

    iput v2, p0, LX/J49;->c:I

    .line 2643830
    iget-object v2, p0, LX/J49;->r:LX/0W3;

    sget-wide v4, LX/0X5;->gR:J

    const/4 v3, 0x5

    invoke-interface {v2, v4, v5, v3}, LX/0W4;->a(JI)I

    move-result v2

    iput v2, p0, LX/J49;->b:I

    .line 2643831
    return-void
.end method

.method private static a(Ljava/util/Map;Ljava/util/Set;)LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/privacy/protocol/ReportPrivacyCheckupActionsParams$PrivacyCheckupItem;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/privacy/protocol/ReportPrivacyCheckupActionsParams$PrivacyCheckupItem;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/privacy/protocol/ReportPrivacyCheckupActionsParams$PrivacyCheckupItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2643805
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 2643806
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2643807
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 2643808
    :cond_0
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/protocol/ReportPrivacyCheckupActionsParams$PrivacyCheckupItem;

    .line 2643809
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 2643810
    :cond_1
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/J49;
    .locals 14

    .prologue
    .line 2643776
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2643777
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2643778
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2643779
    if-nez v1, :cond_0

    .line 2643780
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2643781
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2643782
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2643783
    sget-object v1, LX/J49;->w:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2643784
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2643785
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2643786
    :cond_1
    if-nez v1, :cond_4

    .line 2643787
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2643788
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2643789
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2643790
    new-instance v7, LX/J49;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v8

    check-cast v8, LX/0SG;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v10

    check-cast v10, LX/1Ck;

    invoke-static {v0}, LX/J5k;->b(LX/0QB;)LX/J5k;

    move-result-object v11

    check-cast v11, LX/J5k;

    invoke-static {v0}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(LX/0QB;)Lcom/facebook/privacy/PrivacyOperationsClient;

    move-result-object v12

    check-cast v12, Lcom/facebook/privacy/PrivacyOperationsClient;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v13

    check-cast v13, LX/0W3;

    invoke-direct/range {v7 .. v13}, LX/J49;-><init>(LX/0SG;LX/03V;LX/1Ck;LX/J5k;Lcom/facebook/privacy/PrivacyOperationsClient;LX/0W3;)V

    .line 2643791
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2643792
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2643793
    if-nez v1, :cond_2

    .line 2643794
    sget-object v0, LX/J49;->w:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/J49;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2643795
    :goto_1
    if-eqz v0, :cond_3

    .line 2643796
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2643797
    :goto_3
    check-cast v0, LX/J49;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2643798
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2643799
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2643800
    :catchall_1
    move-exception v0

    .line 2643801
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2643802
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2643803
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2643804
    :cond_2
    :try_start_8
    sget-object v0, LX/J49;->w:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/J49;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public static a(LX/J49;Ljava/lang/String;Lcom/facebook/privacy/protocol/ReportPrivacyCheckupActionsParams$PrivacyCheckupItem;)V
    .locals 2

    .prologue
    .line 2643771
    iget v0, p0, LX/J49;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/J49;->f:I

    .line 2643772
    iget-object v0, p0, LX/J49;->i:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2643773
    iget-object v0, p0, LX/J49;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    iget v1, p0, LX/J49;->b:I

    if-lt v0, v1, :cond_0

    .line 2643774
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/J49;->a(Z)V

    .line 2643775
    :cond_0
    return-void
.end method

.method private b(LX/J5h;)V
    .locals 13

    .prologue
    .line 2643681
    iget-object v0, p0, LX/J49;->p:LX/J5k;

    iget-wide v2, p0, LX/J49;->t:J

    iget-object v1, p0, LX/J49;->i:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v4

    iget-object v1, p0, LX/J49;->j:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v5

    iget-object v1, p0, LX/J49;->l:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v6

    iget v7, p0, LX/J49;->e:I

    iget v8, p0, LX/J49;->f:I

    iget v9, p0, LX/J49;->g:I

    iget v10, p0, LX/J49;->h:I

    move-object v1, p1

    .line 2643682
    add-int v11, v7, v8

    .line 2643683
    iget-object v12, v1, LX/J5h;->eventName:Ljava/lang/String;

    invoke-static {v12}, LX/J5k;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v12

    .line 2643684
    const-string p0, "session_id"

    invoke-virtual {v12, p0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2643685
    const-string p0, "num_waiting_items"

    invoke-virtual {v12, p0, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2643686
    const-string p0, "num_inflight_items"

    invoke-virtual {v12, p0, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2643687
    const-string p0, "num_logging_items"

    invoke-virtual {v12, p0, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2643688
    const-string p0, "num_total_writes"

    invoke-virtual {v12, p0, v11}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2643689
    const-string v11, "num_total_logging_writes"

    invoke-virtual {v12, v11, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2643690
    const-string v11, "num_total_mutation_writes"

    invoke-virtual {v12, v11, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2643691
    const-string v11, "num_total_batch_requests_sent"

    invoke-virtual {v12, v11, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2643692
    const-string v11, "num_inflight_requests_cancelled_for_flush"

    invoke-virtual {v12, v11, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2643693
    iget-object v11, v0, LX/J5k;->a:LX/0Zb;

    invoke-interface {v11, v12}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2643694
    return-void
.end method

.method public static c(LX/J49;)Ljava/lang/Long;
    .locals 4

    .prologue
    .line 2643770
    iget-object v0, p0, LX/J49;->m:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method private d()Z
    .locals 1

    .prologue
    .line 2643769
    iget-object v0, p0, LX/J49;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/J49;->l:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    .line 2643752
    invoke-direct {p0}, LX/J49;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2643753
    sget-object v0, LX/J5h;->PRIVACY_CHECKUP_WRITE_REQUEST_ON_EXIT:LX/J5h;

    invoke-direct {p0, v0}, LX/J49;->b(LX/J5h;)V

    .line 2643754
    :goto_0
    return-void

    .line 2643755
    :cond_0
    iget-object v0, p0, LX/J49;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2643756
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/J49;->a(Z)V

    .line 2643757
    sget-object v0, LX/J5h;->PRIVACY_CHECKUP_WRITE_REQUEST_ON_EXIT:LX/J5h;

    invoke-direct {p0, v0}, LX/J49;->b(LX/J5h;)V

    goto :goto_0

    .line 2643758
    :cond_1
    sget-object v0, LX/J5h;->PRIVACY_CHECKUP_WRITE_REQUEST_FLUSH:LX/J5h;

    invoke-virtual {p0, v0}, LX/J49;->a(LX/J5h;)V

    .line 2643759
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2643760
    iget-object v1, p0, LX/J49;->j:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 2643761
    iget-object v1, p0, LX/J49;->i:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 2643762
    iget v1, p0, LX/J49;->g:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/J49;->g:I

    .line 2643763
    iget-object v1, p0, LX/J49;->j:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    iput v1, p0, LX/J49;->h:I

    .line 2643764
    sget-object v1, LX/J5h;->PRIVACY_CHECKUP_WRITE_REQUEST_ON_EXIT:LX/J5h;

    invoke-direct {p0, v1}, LX/J49;->b(LX/J5h;)V

    .line 2643765
    iget-object v1, p0, LX/J49;->o:LX/1Ck;

    sget-object v2, LX/J48;->FINAL_FLUSH_REQUESTS:LX/J48;

    iget-object v3, p0, LX/J49;->q:Lcom/facebook/privacy/PrivacyOperationsClient;

    iget-object v4, p0, LX/J49;->l:Ljava/util/Set;

    invoke-static {v0, v4}, LX/J49;->a(Ljava/util/Map;Ljava/util/Set;)LX/0Px;

    move-result-object v0

    iget-object v4, p0, LX/J49;->s:Ljava/lang/String;

    iget-wide v6, p0, LX/J49;->t:J

    invoke-virtual {v3, v0, v4, v6, v7}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(LX/0Px;Ljava/lang/String;J)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iget-object v3, p0, LX/J49;->v:LX/2h0;

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2643766
    iget-object v0, p0, LX/J49;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 2643767
    iget-object v0, p0, LX/J49;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 2643768
    iget-object v0, p0, LX/J49;->l:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    goto :goto_0
.end method

.method public final a(LX/5nj;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2643740
    iget v0, p0, LX/J49;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/J49;->e:I

    .line 2643741
    new-instance v0, LX/5nl;

    invoke-direct {v0}, LX/5nl;-><init>()V

    .line 2643742
    iput-object p1, v0, LX/5nl;->a:LX/5nj;

    .line 2643743
    move-object v0, v0

    .line 2643744
    invoke-static {p0}, LX/J49;->c(LX/J49;)Ljava/lang/Long;

    move-result-object v1

    .line 2643745
    iput-object v1, v0, LX/5nl;->b:Ljava/lang/Long;

    .line 2643746
    move-object v0, v0

    .line 2643747
    iput-object p2, v0, LX/5nl;->g:Ljava/lang/String;

    .line 2643748
    move-object v0, v0

    .line 2643749
    invoke-virtual {v0}, LX/5nl;->a()Lcom/facebook/privacy/protocol/ReportPrivacyCheckupActionsParams$PrivacyCheckupItem;

    move-result-object v0

    .line 2643750
    iget-object v1, p0, LX/J49;->l:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2643751
    return-void
.end method

.method public final a(LX/J5h;)V
    .locals 5

    .prologue
    .line 2643733
    iget-object v0, p0, LX/J49;->p:LX/J5k;

    iget-object v1, p0, LX/J49;->i:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    iget-object v2, p0, LX/J49;->j:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    iget-object v3, p0, LX/J49;->l:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    .line 2643734
    iget-object v4, p1, LX/J5h;->eventName:Ljava/lang/String;

    invoke-static {v4}, LX/J5k;->a(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    .line 2643735
    const-string p0, "num_waiting_items"

    invoke-virtual {v4, p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2643736
    const-string p0, "num_inflight_items"

    invoke-virtual {v4, p0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2643737
    const-string p0, "num_logging_items"

    invoke-virtual {v4, p0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2643738
    iget-object p0, v0, LX/J5k;->a:LX/0Zb;

    invoke-interface {p0, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2643739
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;LX/1oT;)V
    .locals 2

    .prologue
    .line 2643713
    iget-object v0, p0, LX/J49;->p:LX/J5k;

    const-string v1, "change_privacy"

    invoke-virtual {v0, p1, v1, p3}, LX/J5k;->a(Ljava/lang/String;Ljava/lang/String;LX/1oU;)V

    .line 2643714
    new-instance v0, LX/5nl;

    invoke-direct {v0}, LX/5nl;-><init>()V

    sget-object v1, LX/5nj;->MUTATION:LX/5nj;

    .line 2643715
    iput-object v1, v0, LX/5nl;->a:LX/5nj;

    .line 2643716
    move-object v0, v0

    .line 2643717
    invoke-static {p0}, LX/J49;->c(LX/J49;)Ljava/lang/Long;

    move-result-object v1

    .line 2643718
    iput-object v1, v0, LX/5nl;->b:Ljava/lang/Long;

    .line 2643719
    move-object v0, v0

    .line 2643720
    iput-object p1, v0, LX/5nl;->c:Ljava/lang/String;

    .line 2643721
    move-object v0, v0

    .line 2643722
    iput-object p2, v0, LX/5nl;->d:Lcom/facebook/graphql/enums/GraphQLEditablePrivacyScopeType;

    .line 2643723
    move-object v0, v0

    .line 2643724
    sget-object v1, LX/5ni;->CHANGE_PRIVACY:LX/5ni;

    .line 2643725
    iput-object v1, v0, LX/5nl;->e:LX/5ni;

    .line 2643726
    move-object v0, v0

    .line 2643727
    invoke-interface {p3}, LX/1oT;->c()Ljava/lang/String;

    move-result-object v1

    .line 2643728
    iput-object v1, v0, LX/5nl;->f:Ljava/lang/String;

    .line 2643729
    move-object v0, v0

    .line 2643730
    invoke-virtual {v0}, LX/5nl;->a()Lcom/facebook/privacy/protocol/ReportPrivacyCheckupActionsParams$PrivacyCheckupItem;

    move-result-object v0

    .line 2643731
    invoke-static {p0, p1, v0}, LX/J49;->a(LX/J49;Ljava/lang/String;Lcom/facebook/privacy/protocol/ReportPrivacyCheckupActionsParams$PrivacyCheckupItem;)V

    .line 2643732
    return-void
.end method

.method public final a(Z)V
    .locals 8

    .prologue
    .line 2643700
    invoke-direct {p0}, LX/J49;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2643701
    :goto_0
    return-void

    .line 2643702
    :cond_0
    iget-object v0, p0, LX/J49;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2643703
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/J49;->a:Z

    goto :goto_0

    .line 2643704
    :cond_1
    iget-object v0, p0, LX/J49;->j:Ljava/util/Map;

    iget-object v1, p0, LX/J49;->i:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 2643705
    iget-object v0, p0, LX/J49;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 2643706
    invoke-static {p0}, LX/J49;->c(LX/J49;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, LX/J49;->d:J

    .line 2643707
    iget-object v0, p0, LX/J49;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2643708
    iget-object v0, p0, LX/J49;->p:LX/J5k;

    sget-object v1, LX/J5h;->PRIVACY_CHECKUP_WRITE_REQUEST_SENT:LX/J5h;

    iget-object v2, p0, LX/J49;->j:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/J5k;->a(LX/J5h;Ljava/lang/Integer;Ljava/lang/Long;)V

    .line 2643709
    :cond_2
    if-nez p1, :cond_3

    invoke-direct {p0}, LX/J49;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2643710
    iget v0, p0, LX/J49;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/J49;->g:I

    .line 2643711
    :cond_3
    iget-object v0, p0, LX/J49;->o:LX/1Ck;

    sget-object v1, LX/J48;->SEND_REQUESTS:LX/J48;

    iget-object v2, p0, LX/J49;->q:Lcom/facebook/privacy/PrivacyOperationsClient;

    iget-object v3, p0, LX/J49;->j:Ljava/util/Map;

    iget-object v4, p0, LX/J49;->l:Ljava/util/Set;

    invoke-static {v3, v4}, LX/J49;->a(Ljava/util/Map;Ljava/util/Set;)LX/0Px;

    move-result-object v3

    iget-object v4, p0, LX/J49;->s:Ljava/lang/String;

    iget-wide v6, p0, LX/J49;->t:J

    invoke-virtual {v2, v3, v4, v6, v7}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(LX/0Px;Ljava/lang/String;J)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    iget-object v3, p0, LX/J49;->u:LX/2h0;

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2643712
    iget-object v0, p0, LX/J49;->l:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2643695
    iput v0, p0, LX/J49;->e:I

    .line 2643696
    iput v0, p0, LX/J49;->f:I

    .line 2643697
    iput v0, p0, LX/J49;->g:I

    .line 2643698
    iput v0, p0, LX/J49;->h:I

    .line 2643699
    return-void
.end method
