.class public final enum LX/ICQ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ICQ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ICQ;

.field public static final enum FINISH:LX/ICQ;

.field public static final enum HIDE:LX/ICQ;

.field public static final enum SEEN:LX/ICQ;

.field public static final enum START:LX/ICQ;

.field public static final enum TAKEN:LX/ICQ;


# instance fields
.field private final mActionType:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2549374
    new-instance v0, LX/ICQ;

    const-string v1, "SEEN"

    const-string v2, "SEEN"

    invoke-direct {v0, v1, v3, v2}, LX/ICQ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ICQ;->SEEN:LX/ICQ;

    .line 2549375
    new-instance v0, LX/ICQ;

    const-string v1, "HIDE"

    const-string v2, "HIDE"

    invoke-direct {v0, v1, v4, v2}, LX/ICQ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ICQ;->HIDE:LX/ICQ;

    .line 2549376
    new-instance v0, LX/ICQ;

    const-string v1, "TAKEN"

    const-string v2, "TAKEN"

    invoke-direct {v0, v1, v5, v2}, LX/ICQ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ICQ;->TAKEN:LX/ICQ;

    .line 2549377
    new-instance v0, LX/ICQ;

    const-string v1, "START"

    const-string v2, "START"

    invoke-direct {v0, v1, v6, v2}, LX/ICQ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ICQ;->START:LX/ICQ;

    .line 2549378
    new-instance v0, LX/ICQ;

    const-string v1, "FINISH"

    const-string v2, "FINISH"

    invoke-direct {v0, v1, v7, v2}, LX/ICQ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ICQ;->FINISH:LX/ICQ;

    .line 2549379
    const/4 v0, 0x5

    new-array v0, v0, [LX/ICQ;

    sget-object v1, LX/ICQ;->SEEN:LX/ICQ;

    aput-object v1, v0, v3

    sget-object v1, LX/ICQ;->HIDE:LX/ICQ;

    aput-object v1, v0, v4

    sget-object v1, LX/ICQ;->TAKEN:LX/ICQ;

    aput-object v1, v0, v5

    sget-object v1, LX/ICQ;->START:LX/ICQ;

    aput-object v1, v0, v6

    sget-object v1, LX/ICQ;->FINISH:LX/ICQ;

    aput-object v1, v0, v7

    sput-object v0, LX/ICQ;->$VALUES:[LX/ICQ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2549371
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2549372
    iput-object p3, p0, LX/ICQ;->mActionType:Ljava/lang/String;

    .line 2549373
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/ICQ;
    .locals 1

    .prologue
    .line 2549361
    const-class v0, LX/ICQ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ICQ;

    return-object v0
.end method

.method public static values()[LX/ICQ;
    .locals 1

    .prologue
    .line 2549370
    sget-object v0, LX/ICQ;->$VALUES:[LX/ICQ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ICQ;

    return-object v0
.end method


# virtual methods
.method public final toEventName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2549363
    sget-object v0, LX/ICP;->a:[I

    invoke-virtual {p0}, LX/ICQ;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2549364
    const-string v0, ""

    :goto_0
    return-object v0

    .line 2549365
    :pswitch_0
    const-string v0, "story_gallery_survey_feed_unit_impression"

    goto :goto_0

    .line 2549366
    :pswitch_1
    const-string v0, "story_gallery_survey_feed_unit_hide"

    goto :goto_0

    .line 2549367
    :pswitch_2
    const-string v0, "story_gallery_survey_feed_unit_taken"

    goto :goto_0

    .line 2549368
    :pswitch_3
    const-string v0, "story_gallery_survey_feed_unit_start"

    goto :goto_0

    .line 2549369
    :pswitch_4
    const-string v0, "story_gallery_survey_feed_unit_finish"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2549362
    iget-object v0, p0, LX/ICQ;->mActionType:Ljava/lang/String;

    return-object v0
.end method
