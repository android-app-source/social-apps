.class public LX/Io5;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2611516
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/5g0;Lcom/facebook/messaging/payment/value/input/OrionMessengerPayParams;)Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;
    .locals 3

    .prologue
    .line 2611517
    new-instance v0, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;

    invoke-direct {v0}, Lcom/facebook/messaging/payment/value/input/EnterPaymentValueFragment;-><init>()V

    .line 2611518
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2611519
    const-string v2, "payment_flow_type"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2611520
    const-string v2, "orion_messenger_pay_params"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2611521
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2611522
    return-object v0
.end method
