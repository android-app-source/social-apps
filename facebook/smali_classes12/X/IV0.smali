.class public final enum LX/IV0;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/IV0;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/IV0;

.field public static final enum DISCOVER_CARD_VIEW_TYPE:LX/IV0;

.field public static final enum SUGGESTION_CARD_VIEW_TYPE:LX/IV0;

.field public static final enum VISIT_COMMUNITY_CARD_VIEW_TYPE:LX/IV0;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2581213
    new-instance v0, LX/IV0;

    const-string v1, "SUGGESTION_CARD_VIEW_TYPE"

    invoke-direct {v0, v1, v2}, LX/IV0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IV0;->SUGGESTION_CARD_VIEW_TYPE:LX/IV0;

    .line 2581214
    new-instance v0, LX/IV0;

    const-string v1, "DISCOVER_CARD_VIEW_TYPE"

    invoke-direct {v0, v1, v3}, LX/IV0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IV0;->DISCOVER_CARD_VIEW_TYPE:LX/IV0;

    .line 2581215
    new-instance v0, LX/IV0;

    const-string v1, "VISIT_COMMUNITY_CARD_VIEW_TYPE"

    invoke-direct {v0, v1, v4}, LX/IV0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/IV0;->VISIT_COMMUNITY_CARD_VIEW_TYPE:LX/IV0;

    .line 2581216
    const/4 v0, 0x3

    new-array v0, v0, [LX/IV0;

    sget-object v1, LX/IV0;->SUGGESTION_CARD_VIEW_TYPE:LX/IV0;

    aput-object v1, v0, v2

    sget-object v1, LX/IV0;->DISCOVER_CARD_VIEW_TYPE:LX/IV0;

    aput-object v1, v0, v3

    sget-object v1, LX/IV0;->VISIT_COMMUNITY_CARD_VIEW_TYPE:LX/IV0;

    aput-object v1, v0, v4

    sput-object v0, LX/IV0;->$VALUES:[LX/IV0;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2581217
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/IV0;
    .locals 1

    .prologue
    .line 2581218
    const-class v0, LX/IV0;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/IV0;

    return-object v0
.end method

.method public static values()[LX/IV0;
    .locals 1

    .prologue
    .line 2581219
    sget-object v0, LX/IV0;->$VALUES:[LX/IV0;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/IV0;

    return-object v0
.end method
