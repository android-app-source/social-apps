.class public final LX/I93;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7ll;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:LX/1nQ;


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/1nQ;)V
    .locals 0

    .prologue
    .line 2542358
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2542359
    iput-object p1, p0, LX/I93;->a:Ljava/lang/String;

    .line 2542360
    iput-object p2, p0, LX/I93;->b:LX/1nQ;

    .line 2542361
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/composer/publish/common/PendingStory;)V
    .locals 0

    .prologue
    .line 2542344
    return-void
.end method

.method public final a(Lcom/facebook/composer/publish/common/PublishPostParams;)V
    .locals 0

    .prologue
    .line 2542362
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/composer/publish/common/PendingStory;)V
    .locals 4

    .prologue
    .line 2542346
    iget-object v0, p0, LX/I93;->b:LX/1nQ;

    iget-object v1, p0, LX/I93;->a:Ljava/lang/String;

    invoke-virtual {p2}, Lcom/facebook/composer/publish/common/PendingStory;->b()Lcom/facebook/composer/publish/common/PostParamsWrapper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/composer/publish/common/PostParamsWrapper;->a()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/facebook/events/common/ActionMechanism;->SAY_THANKS_COMPOSER:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v3}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2542347
    iget-object p0, v0, LX/1nQ;->i:LX/0Zb;

    const-string p1, "event_say_thanks_posted"

    const/4 p2, 0x0

    invoke-interface {p0, p1, p2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object p0

    .line 2542348
    invoke-virtual {p0}, LX/0oG;->a()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 2542349
    const-string p1, "event_permalink"

    invoke-virtual {p0, p1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2542350
    iget-object p1, v0, LX/1nQ;->j:LX/0kv;

    iget-object p2, v0, LX/1nQ;->g:Landroid/content/Context;

    invoke-virtual {p1, p2}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    .line 2542351
    const-string p1, "Event"

    invoke-virtual {p0, p1}, LX/0oG;->b(Ljava/lang/String;)LX/0oG;

    .line 2542352
    invoke-virtual {p0, v1}, LX/0oG;->c(Ljava/lang/String;)LX/0oG;

    .line 2542353
    const-string p1, "mechanism"

    invoke-virtual {p0, p1, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2542354
    const-string p1, "composer_session_id"

    invoke-virtual {p0, p1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2542355
    const-string p1, "event_id"

    invoke-virtual {p0, p1, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2542356
    invoke-virtual {p0}, LX/0oG;->d()V

    .line 2542357
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/composer/publish/common/PendingStory;Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 0

    .prologue
    .line 2542345
    return-void
.end method
