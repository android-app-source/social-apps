.class public LX/IhW;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/IhD;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:Landroid/view/LayoutInflater;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/IhE;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/Ihc;

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/IhV;

.field public g:LX/Ihj;

.field public final h:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Ihj;)V
    .locals 2
    .param p1    # LX/Ihj;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2602696
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2602697
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2602698
    iput-object v0, p0, LX/IhW;->e:LX/0Px;

    .line 2602699
    new-instance v0, LX/IhV;

    invoke-direct {v0, p0}, LX/IhV;-><init>(LX/IhW;)V

    iput-object v0, p0, LX/IhW;->f:LX/IhV;

    .line 2602700
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/IhW;->h:Ljava/util/HashSet;

    .line 2602701
    iput-object p1, p0, LX/IhW;->g:LX/Ihj;

    .line 2602702
    return-void
.end method

.method private a(LX/IhD;I)V
    .locals 1

    .prologue
    .line 2602703
    iget-object v0, p0, LX/IhW;->e:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2602704
    invoke-virtual {p1, v0}, LX/IhD;->a(Lcom/facebook/ui/media/attachments/MediaResource;)V

    .line 2602705
    iget-object p2, p0, LX/IhW;->g:LX/Ihj;

    invoke-virtual {p1, p2}, LX/IhD;->a(LX/Ihj;)V

    .line 2602706
    iget-object p2, p0, LX/IhW;->f:LX/IhV;

    .line 2602707
    iput-object p2, p1, LX/IhD;->r:LX/IhV;

    .line 2602708
    invoke-virtual {p1, v0}, LX/IhD;->a(Lcom/facebook/ui/media/attachments/MediaResource;)V

    .line 2602709
    iget-object p2, p0, LX/IhW;->h:Ljava/util/HashSet;

    invoke-virtual {p2, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result p2

    invoke-virtual {p1, p2}, LX/IhD;->b(Z)V

    .line 2602710
    iget-object p2, p0, LX/IhW;->h:Ljava/util/HashSet;

    invoke-virtual {p2}, Ljava/util/HashSet;->isEmpty()Z

    move-result p2

    if-nez p2, :cond_0

    const/4 p2, 0x1

    :goto_0
    invoke-static {p0, p1, p2}, LX/IhW;->a(LX/IhW;LX/IhD;Z)V

    .line 2602711
    return-void

    .line 2602712
    :cond_0
    const/4 p2, 0x0

    goto :goto_0
.end method

.method public static a(LX/IhW;LX/IhD;Z)V
    .locals 1

    .prologue
    .line 2602713
    if-nez p2, :cond_0

    iget-object v0, p0, LX/IhW;->g:LX/Ihj;

    iget-boolean v0, v0, LX/Ihj;->a:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 2602714
    :goto_0
    iget-object p0, p1, LX/IhD;->m:LX/Ihk;

    .line 2602715
    iget-object p1, p0, LX/Ihk;->b:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    const/4 p2, 0x0

    :goto_1
    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2602716
    return-void

    .line 2602717
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2602718
    :cond_2
    const/16 p2, 0x8

    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 9

    .prologue
    .line 2602719
    iget-object v0, p0, LX/IhW;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f030a9d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2602720
    iget-object v1, p0, LX/IhW;->c:LX/IhE;

    .line 2602721
    new-instance v3, LX/IhD;

    const-class v4, LX/Ihh;

    invoke-interface {v1, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/Ihh;

    const-class v5, LX/Ihl;

    invoke-interface {v1, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/Ihl;

    const-class v6, LX/Ihp;

    invoke-interface {v1, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/Ihp;

    const-class v7, LX/Ihr;

    invoke-interface {v1, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/Ihr;

    move-object v8, v0

    invoke-direct/range {v3 .. v8}, LX/IhD;-><init>(LX/Ihh;LX/Ihl;LX/Ihp;LX/Ihr;Landroid/view/View;)V

    .line 2602722
    move-object v0, v3

    .line 2602723
    return-object v0
.end method

.method public final bridge synthetic a(LX/1a1;I)V
    .locals 0

    .prologue
    .line 2602724
    check-cast p1, LX/IhD;

    invoke-direct {p0, p1, p2}, LX/IhW;->a(LX/IhD;I)V

    return-void
.end method

.method public final a(LX/1a1;ILjava/util/List;)V
    .locals 3

    .prologue
    .line 2602725
    check-cast p1, LX/IhD;

    .line 2602726
    if-eqz p3, :cond_0

    invoke-interface {p3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2602727
    :cond_0
    invoke-direct {p0, p1, p2}, LX/IhW;->a(LX/IhD;I)V

    .line 2602728
    :cond_1
    return-void

    .line 2602729
    :cond_2
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 2602730
    instance-of v2, v0, LX/IhB;

    if-eqz v2, :cond_3

    .line 2602731
    check-cast v0, LX/IhB;

    .line 2602732
    iget-object v2, v0, LX/IhB;->b:LX/0am;

    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v2

    move v2, v2

    .line 2602733
    if-eqz v2, :cond_4

    .line 2602734
    iget-object v2, v0, LX/IhB;->b:LX/0am;

    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v2

    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 2602735
    iget-object v2, v0, LX/IhB;->b:LX/0am;

    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    move v2, v2

    .line 2602736
    invoke-static {p0, p1, v2}, LX/IhW;->a(LX/IhW;LX/IhD;Z)V

    .line 2602737
    :cond_4
    iget-object v2, v0, LX/IhB;->c:LX/0am;

    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v2

    move v2, v2

    .line 2602738
    if-eqz v2, :cond_5

    .line 2602739
    iget-object v2, v0, LX/IhB;->c:LX/0am;

    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v2

    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 2602740
    iget-object v2, v0, LX/IhB;->c:LX/0am;

    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    move v2, v2

    .line 2602741
    invoke-virtual {p1, v2}, LX/IhD;->b(Z)V

    .line 2602742
    :cond_5
    iget-object v2, v0, LX/IhB;->a:LX/Ihj;

    move-object v2, v2

    .line 2602743
    if-eqz v2, :cond_3

    .line 2602744
    iget-object v2, v0, LX/IhB;->a:LX/Ihj;

    move-object v0, v2

    .line 2602745
    invoke-virtual {p1, v0}, LX/IhD;->a(LX/Ihj;)V

    goto :goto_0
.end method

.method public final a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2602746
    iget-object v0, p0, LX/IhW;->h:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 2602747
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2602748
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2602749
    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2602750
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 2602751
    :cond_1
    iget-object v0, p0, LX/IhW;->h:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 2602752
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 2602753
    return-void
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2602754
    iget-object v0, p0, LX/IhW;->e:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
