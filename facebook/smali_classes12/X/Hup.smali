.class public final LX/Hup;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Hur;


# direct methods
.method public constructor <init>(LX/Hur;)V
    .locals 0

    .prologue
    .line 2517923
    iput-object p1, p0, LX/Hup;->a:LX/Hur;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, -0x4ecb3997

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2517924
    iget-object v0, p0, LX/Hup;->a:LX/Hur;

    iget-object v0, v0, LX/Hur;->g:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HqQ;

    .line 2517925
    iget-object v1, p0, LX/Hup;->a:LX/Hur;

    iget-object v3, v1, LX/Hur;->h:LX/0gd;

    sget-object v4, LX/0ge;->COMPOSER_SELECTABLE_PRIVACY_PILL_CLICKED:LX/0ge;

    iget-object v1, p0, LX/Hup;->a:LX/Hur;

    iget-object v1, v1, LX/Hur;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0il;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0j0;

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v4, v1}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 2517926
    iget-object v1, v0, LX/HqQ;->a:Lcom/facebook/composer/activity/ComposerFragment;

    invoke-static {v1}, Lcom/facebook/composer/activity/ComposerFragment;->N$redex0(Lcom/facebook/composer/activity/ComposerFragment;)V

    .line 2517927
    const v0, 0x6a67a54a

    invoke-static {v5, v5, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
