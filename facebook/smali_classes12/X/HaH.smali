.class public final enum LX/HaH;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HaH;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/HaH;

.field public static final enum BROWSER:LX/HaH;

.field public static final enum DATA_USAGE:LX/HaH;

.field public static final enum FRIEND_FINDER_LEARN_MORE:LX/HaH;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2483691
    new-instance v0, LX/HaH;

    const-string v1, "BROWSER"

    invoke-direct {v0, v1, v2}, LX/HaH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HaH;->BROWSER:LX/HaH;

    .line 2483692
    new-instance v0, LX/HaH;

    const-string v1, "FRIEND_FINDER_LEARN_MORE"

    invoke-direct {v0, v1, v3}, LX/HaH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HaH;->FRIEND_FINDER_LEARN_MORE:LX/HaH;

    .line 2483693
    new-instance v0, LX/HaH;

    const-string v1, "DATA_USAGE"

    invoke-direct {v0, v1, v4}, LX/HaH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HaH;->DATA_USAGE:LX/HaH;

    .line 2483694
    const/4 v0, 0x3

    new-array v0, v0, [LX/HaH;

    sget-object v1, LX/HaH;->BROWSER:LX/HaH;

    aput-object v1, v0, v2

    sget-object v1, LX/HaH;->FRIEND_FINDER_LEARN_MORE:LX/HaH;

    aput-object v1, v0, v3

    sget-object v1, LX/HaH;->DATA_USAGE:LX/HaH;

    aput-object v1, v0, v4

    sput-object v0, LX/HaH;->$VALUES:[LX/HaH;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2483695
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HaH;
    .locals 1

    .prologue
    .line 2483696
    const-class v0, LX/HaH;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HaH;

    return-object v0
.end method

.method public static values()[LX/HaH;
    .locals 1

    .prologue
    .line 2483697
    sget-object v0, LX/HaH;->$VALUES:[LX/HaH;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HaH;

    return-object v0
.end method
