.class public LX/IBt;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/IBt;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2547839
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2547840
    sget-object v0, LX/0ax;->cp:Ljava/lang/String;

    const-string v1, "com.facebook.katana.profile.id"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/IBs;

    invoke-direct {v1}, LX/IBs;-><init>()V

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;LX/47M;)V

    .line 2547841
    return-void
.end method

.method public static a(LX/0QB;)LX/IBt;
    .locals 3

    .prologue
    .line 2547825
    sget-object v0, LX/IBt;->a:LX/IBt;

    if-nez v0, :cond_1

    .line 2547826
    const-class v1, LX/IBt;

    monitor-enter v1

    .line 2547827
    :try_start_0
    sget-object v0, LX/IBt;->a:LX/IBt;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2547828
    if-eqz v2, :cond_0

    .line 2547829
    :try_start_1
    new-instance v0, LX/IBt;

    invoke-direct {v0}, LX/IBt;-><init>()V

    .line 2547830
    move-object v0, v0

    .line 2547831
    sput-object v0, LX/IBt;->a:LX/IBt;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2547832
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2547833
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2547834
    :cond_1
    sget-object v0, LX/IBt;->a:LX/IBt;

    return-object v0

    .line 2547835
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2547836
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2547837
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 2547838
    return v0
.end method
