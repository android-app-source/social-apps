.class public LX/IK6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1PH;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static p:LX/0Xm;


# instance fields
.field public final a:LX/IK9;

.field private final b:LX/IKm;

.field private final c:LX/IKl;

.field private final d:LX/1rq;

.field private final e:Lcom/facebook/content/SecureContextHelper;

.field private final f:LX/6eF;

.field private final g:LX/0Uh;

.field private final h:LX/14x;

.field private final i:LX/0kL;

.field public final j:LX/0iA;

.field public k:LX/2kW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/controller/connectioncontroller/common/ConnectionController",
            "<",
            "Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/2kW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/controller/connectioncontroller/common/ConnectionController",
            "<",
            "Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field public m:Z

.field private n:LX/1vq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1vq",
            "<",
            "Lcom/facebook/groups/channels/protocol/FetchGroupParticipatingChannelsQueryModels$GroupParticipatingChannelsQueryModel$GroupParticipatingChannelsModel$EdgesModel$NodeModel;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final o:LX/1vq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1vq",
            "<",
            "Lcom/facebook/groups/channels/protocol/FetchAllGroupChannelsQueryModels$AllGroupChannelsQueryModel$GroupChannelsModel$EdgesModel$NodeModel;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/IK9;LX/14x;Lcom/facebook/content/SecureContextHelper;LX/IKm;LX/IKl;LX/1rq;LX/6eF;LX/0Uh;LX/0kL;LX/0iA;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2564957
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2564958
    new-instance v0, LX/IK4;

    invoke-direct {v0, p0}, LX/IK4;-><init>(LX/IK6;)V

    iput-object v0, p0, LX/IK6;->n:LX/1vq;

    .line 2564959
    new-instance v0, LX/IK5;

    invoke-direct {v0, p0}, LX/IK5;-><init>(LX/IK6;)V

    iput-object v0, p0, LX/IK6;->o:LX/1vq;

    .line 2564960
    iput-object p1, p0, LX/IK6;->a:LX/IK9;

    .line 2564961
    iput-object p2, p0, LX/IK6;->h:LX/14x;

    .line 2564962
    iput-object p3, p0, LX/IK6;->e:Lcom/facebook/content/SecureContextHelper;

    .line 2564963
    iput-object p4, p0, LX/IK6;->b:LX/IKm;

    .line 2564964
    iput-object p5, p0, LX/IK6;->c:LX/IKl;

    .line 2564965
    iput-object p6, p0, LX/IK6;->d:LX/1rq;

    .line 2564966
    iput-object p7, p0, LX/IK6;->f:LX/6eF;

    .line 2564967
    iput-object p8, p0, LX/IK6;->g:LX/0Uh;

    .line 2564968
    iput-object p9, p0, LX/IK6;->i:LX/0kL;

    .line 2564969
    iput-object p10, p0, LX/IK6;->j:LX/0iA;

    .line 2564970
    return-void
.end method

.method public static a(LX/0QB;)LX/IK6;
    .locals 14

    .prologue
    .line 2564971
    const-class v1, LX/IK6;

    monitor-enter v1

    .line 2564972
    :try_start_0
    sget-object v0, LX/IK6;->p:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2564973
    sput-object v2, LX/IK6;->p:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2564974
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2564975
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2564976
    new-instance v3, LX/IK6;

    invoke-static {v0}, LX/IK9;->a(LX/0QB;)LX/IK9;

    move-result-object v4

    check-cast v4, LX/IK9;

    invoke-static {v0}, LX/14x;->a(LX/0QB;)LX/14x;

    move-result-object v5

    check-cast v5, LX/14x;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v6

    check-cast v6, Lcom/facebook/content/SecureContextHelper;

    .line 2564977
    new-instance v8, LX/IKm;

    invoke-static {v0}, LX/IK9;->a(LX/0QB;)LX/IK9;

    move-result-object v7

    check-cast v7, LX/IK9;

    invoke-direct {v8, v7}, LX/IKm;-><init>(LX/IK9;)V

    .line 2564978
    move-object v7, v8

    .line 2564979
    check-cast v7, LX/IKm;

    .line 2564980
    new-instance v9, LX/IKl;

    invoke-static {v0}, LX/IK9;->a(LX/0QB;)LX/IK9;

    move-result-object v8

    check-cast v8, LX/IK9;

    invoke-direct {v9, v8}, LX/IKl;-><init>(LX/IK9;)V

    .line 2564981
    move-object v8, v9

    .line 2564982
    check-cast v8, LX/IKl;

    const-class v9, LX/1rq;

    invoke-interface {v0, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/1rq;

    invoke-static {v0}, LX/6eF;->a(LX/0QB;)LX/6eF;

    move-result-object v10

    check-cast v10, LX/6eF;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v11

    check-cast v11, LX/0Uh;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v12

    check-cast v12, LX/0kL;

    invoke-static {v0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v13

    check-cast v13, LX/0iA;

    invoke-direct/range {v3 .. v13}, LX/IK6;-><init>(LX/IK9;LX/14x;Lcom/facebook/content/SecureContextHelper;LX/IKm;LX/IKl;LX/1rq;LX/6eF;LX/0Uh;LX/0kL;LX/0iA;)V

    .line 2564983
    move-object v0, v3

    .line 2564984
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2564985
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/IK6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2564986
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2564987
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2564988
    iget-object v0, p0, LX/IK6;->a:LX/IK9;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/IK9;->a(Z)V

    .line 2564989
    iget-object v0, p0, LX/IK6;->a:LX/IK9;

    invoke-virtual {v0, v2}, LX/IK9;->a(LX/2kM;)V

    .line 2564990
    iget-object v0, p0, LX/IK6;->a:LX/IK9;

    invoke-virtual {v0, v2}, LX/IK9;->b(LX/2kM;)V

    .line 2564991
    iget-object v0, p0, LX/IK6;->k:LX/2kW;

    const/16 v1, 0xa

    invoke-virtual {v0, v1, v2}, LX/2kW;->c(ILjava/lang/Object;)V

    .line 2564992
    return-void
.end method

.method public final a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2564993
    iget-object v0, p0, LX/IK6;->h:LX/14x;

    const-string v1, "98.0"

    invoke-virtual {v0, v1}, LX/14x;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/IK6;->g:LX/0Uh;

    const/16 v1, 0x3cb

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2564994
    :cond_0
    iget-object v0, p0, LX/IK6;->i:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f08389f    # 1.81069E38f

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2564995
    :goto_0
    return-void

    .line 2564996
    :cond_1
    iget-object v0, p0, LX/IK6;->a:LX/IK9;

    .line 2564997
    iget-object v1, v0, LX/IK9;->b:Ljava/lang/String;

    move-object v0, v1

    .line 2564998
    invoke-static {v0}, LX/6eF;->b(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2564999
    iget-object v1, p0, LX/IK6;->e:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2565000
    iget-object v0, p0, LX/IK6;->a:LX/IK9;

    .line 2565001
    iget-object v1, v0, LX/IK9;->f:LX/1lD;

    move-object v0, v1

    .line 2565002
    sget-object v1, LX/1lD;->LOADING:LX/1lD;

    if-ne v0, v1, :cond_0

    .line 2565003
    :goto_0
    return-void

    .line 2565004
    :cond_0
    iget-object v0, p0, LX/IK6;->a:LX/IK9;

    .line 2565005
    iput-object p1, v0, LX/IK9;->b:Ljava/lang/String;

    .line 2565006
    invoke-virtual {p0}, LX/IK6;->b()V

    goto :goto_0
.end method

.method public final b()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0xa

    .line 2565007
    iget-object v0, p0, LX/IK6;->a:LX/IK9;

    .line 2565008
    iget-object v1, v0, LX/IK9;->b:Ljava/lang/String;

    move-object v0, v1

    .line 2565009
    iget-object v1, p0, LX/IK6;->a:LX/IK9;

    .line 2565010
    iget-object v2, v1, LX/IK9;->c:LX/2kM;

    if-eqz v2, :cond_0

    iget-object v2, v1, LX/IK9;->c:LX/2kM;

    invoke-interface {v2}, LX/2kM;->b()LX/2nj;

    move-result-object v2

    .line 2565011
    iget-boolean v3, v2, LX/2nj;->d:Z

    move v2, v3

    .line 2565012
    if-eqz v2, :cond_4

    invoke-virtual {v1}, LX/IK9;->d()I

    move-result v2

    if-lez v2, :cond_4

    :cond_0
    const/4 v2, 0x1

    :goto_0
    move v1, v2

    .line 2565013
    if-eqz v1, :cond_2

    .line 2565014
    iget-object v1, p0, LX/IK6;->a:LX/IK9;

    sget-object v2, LX/1lD;->LOADING:LX/1lD;

    invoke-virtual {v1, v2}, LX/IK9;->a(LX/1lD;)V

    .line 2565015
    iget-object v1, p0, LX/IK6;->d:LX/1rq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "group_participating_channels_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, LX/IK6;->b:LX/IKm;

    invoke-virtual {v1, v0, v2}, LX/1rq;->a(Ljava/lang/String;LX/1rs;)LX/2jj;

    move-result-object v0

    invoke-virtual {v0}, LX/2jj;->a()LX/2kW;

    move-result-object v0

    iput-object v0, p0, LX/IK6;->k:LX/2kW;

    .line 2565016
    iget-object v0, p0, LX/IK6;->k:LX/2kW;

    iget-object v1, p0, LX/IK6;->n:LX/1vq;

    invoke-virtual {v0, v1}, LX/2kW;->a(LX/1vq;)V

    .line 2565017
    iget-object v0, p0, LX/IK6;->k:LX/2kW;

    invoke-virtual {v0, v4, v5}, LX/2kW;->b(ILjava/lang/Object;)V

    .line 2565018
    :cond_1
    :goto_1
    return-void

    .line 2565019
    :cond_2
    iget-object v1, p0, LX/IK6;->a:LX/IK9;

    .line 2565020
    iget-object v2, v1, LX/IK9;->d:LX/2kM;

    if-eqz v2, :cond_3

    iget-object v2, v1, LX/IK9;->d:LX/2kM;

    invoke-interface {v2}, LX/2kM;->b()LX/2nj;

    move-result-object v2

    .line 2565021
    iget-boolean v1, v2, LX/2nj;->d:Z

    move v2, v1

    .line 2565022
    if-eqz v2, :cond_5

    :cond_3
    const/4 v2, 0x1

    :goto_2
    move v1, v2

    .line 2565023
    if-eqz v1, :cond_1

    .line 2565024
    iget-object v1, p0, LX/IK6;->a:LX/IK9;

    sget-object v2, LX/1lD;->LOADING:LX/1lD;

    invoke-virtual {v1, v2}, LX/IK9;->a(LX/1lD;)V

    .line 2565025
    iget-object v1, p0, LX/IK6;->d:LX/1rq;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "group_other_channels_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, LX/IK6;->c:LX/IKl;

    invoke-virtual {v1, v0, v2}, LX/1rq;->a(Ljava/lang/String;LX/1rs;)LX/2jj;

    move-result-object v0

    invoke-virtual {v0}, LX/2jj;->a()LX/2kW;

    move-result-object v0

    iput-object v0, p0, LX/IK6;->l:LX/2kW;

    .line 2565026
    iget-object v0, p0, LX/IK6;->l:LX/2kW;

    iget-object v1, p0, LX/IK6;->o:LX/1vq;

    invoke-virtual {v0, v1}, LX/2kW;->a(LX/1vq;)V

    .line 2565027
    iget-object v0, p0, LX/IK6;->l:LX/2kW;

    invoke-virtual {v0, v4, v5}, LX/2kW;->b(ILjava/lang/Object;)V

    goto :goto_1

    :cond_4
    const/4 v2, 0x0

    goto :goto_0

    :cond_5
    const/4 v2, 0x0

    goto :goto_2
.end method
