.class public final LX/IuK;
.super LX/1Mt;
.source ""


# instance fields
.field public final synthetic a:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic b:Lcom/facebook/messaging/tincan/messenger/TincanSendMessageManager;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/tincan/messenger/TincanSendMessageManager;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 0

    .prologue
    .line 2626001
    iput-object p1, p0, LX/IuK;->b:Lcom/facebook/messaging/tincan/messenger/TincanSendMessageManager;

    iput-object p2, p0, LX/IuK;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-direct {p0}, LX/1Mt;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2625999
    iget-object v0, p0, LX/IuK;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, p1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 2626000
    return-void
.end method

.method public final onSuccessfulResult(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 5

    .prologue
    .line 2626002
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 2626003
    iget-object v1, p0, LX/IuK;->a:Lcom/google/common/util/concurrent/SettableFuture;

    new-instance v2, LX/FKH;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-direct {v2, v0, v3, v4}, LX/FKH;-><init>(Lcom/facebook/messaging/model/messages/Message;ZZ)V

    const v0, -0x28cffe3b

    invoke-static {v1, v2, v0}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 2626004
    return-void
.end method

.method public final bridge synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2625998
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    invoke-virtual {p0, p1}, LX/IuK;->onSuccessfulResult(Lcom/facebook/fbservice/service/OperationResult;)V

    return-void
.end method
