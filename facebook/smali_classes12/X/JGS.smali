.class public abstract LX/JGS;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2667202
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 2667198
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 2667199
    if-eqz v0, :cond_0

    .line 2667200
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cannot add view "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to DrawCommandManager while it has a parent "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2667201
    :cond_0
    return-void
.end method


# virtual methods
.method public abstract a(LX/JGd;[I[I)V
.end method

.method public abstract a(Landroid/graphics/Canvas;)V
.end method

.method public abstract a(Landroid/graphics/Rect;)V
.end method

.method public abstract a(Landroid/view/View;)V
.end method

.method public abstract a([LX/JGM;Landroid/util/SparseIntArray;[F[FZ)V
.end method

.method public abstract a([LX/JGu;[F[F)V
.end method

.method public abstract b(FF)LX/JGu;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract b(Landroid/graphics/Canvas;)V
.end method

.method public abstract b()Z
.end method

.method public abstract c(FF)LX/JGu;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract c()Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end method
