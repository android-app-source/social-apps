.class public LX/IXE;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/IXE;


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/webkit/WebView;",
            ">;"
        }
    .end annotation
.end field

.field public b:Landroid/content/Context;

.field public c:I


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2585806
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2585807
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/IXE;->a:Ljava/util/List;

    .line 2585808
    return-void
.end method

.method public static a(LX/0QB;)LX/IXE;
    .locals 3

    .prologue
    .line 2585788
    sget-object v0, LX/IXE;->d:LX/IXE;

    if-nez v0, :cond_1

    .line 2585789
    const-class v1, LX/IXE;

    monitor-enter v1

    .line 2585790
    :try_start_0
    sget-object v0, LX/IXE;->d:LX/IXE;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2585791
    if-eqz v2, :cond_0

    .line 2585792
    :try_start_1
    new-instance v0, LX/IXE;

    invoke-direct {v0}, LX/IXE;-><init>()V

    .line 2585793
    move-object v0, v0

    .line 2585794
    sput-object v0, LX/IXE;->d:LX/IXE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2585795
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2585796
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2585797
    :cond_1
    sget-object v0, LX/IXE;->d:LX/IXE;

    return-object v0

    .line 2585798
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2585799
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2585800
    iget-object v0, p0, LX/IXE;->b:Landroid/content/Context;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 2585801
    iget-object v0, p0, LX/IXE;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    .line 2585802
    invoke-static {v0}, LX/Cs3;->b(Landroid/webkit/WebView;)V

    goto :goto_0

    .line 2585803
    :cond_0
    iget-object v0, p0, LX/IXE;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2585804
    const/4 v0, 0x0

    iput-object v0, p0, LX/IXE;->b:Landroid/content/Context;

    .line 2585805
    return-void
.end method
