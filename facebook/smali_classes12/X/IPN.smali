.class public LX/IPN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8Vc;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Landroid/net/Uri;

.field private final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/net/Uri;LX/0Px;LX/0Px;)V
    .locals 0
    .param p2    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;",
            "LX/0Px",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2574089
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2574090
    iput-object p1, p0, LX/IPN;->a:Ljava/lang/String;

    .line 2574091
    iput-object p2, p0, LX/IPN;->b:Landroid/net/Uri;

    .line 2574092
    iput-object p3, p0, LX/IPN;->c:LX/0Px;

    .line 2574093
    iput-object p4, p0, LX/IPN;->d:LX/0Px;

    .line 2574094
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2574095
    iget-object v0, p0, LX/IPN;->b:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 2574096
    const/4 v0, 0x1

    .line 2574097
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, LX/IPN;->d()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    goto :goto_0
.end method

.method public final a(III)LX/1bf;
    .locals 1

    .prologue
    .line 2574098
    iget-object v0, p0, LX/IPN;->b:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 2574099
    iget-object v0, p0, LX/IPN;->b:Landroid/net/Uri;

    invoke-static {v0}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v0

    .line 2574100
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/IPN;->d:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-static {v0}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(III)LX/1bf;
    .locals 1

    .prologue
    .line 2574101
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b()LX/8ue;
    .locals 1

    .prologue
    .line 2574102
    sget-object v0, LX/8ue;->NONE:LX/8ue;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2574103
    iget-object v0, p0, LX/IPN;->b:Landroid/net/Uri;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2574104
    invoke-virtual {p0}, LX/IPN;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2574105
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2574106
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/IPN;->c:LX/0Px;

    goto :goto_0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2574107
    const/4 v0, 0x0

    return-object v0
.end method

.method public final f()I
    .locals 1
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation

    .prologue
    .line 2574108
    const/4 v0, 0x0

    return v0
.end method
