.class public final LX/Hoj;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2506911
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_9

    .line 2506912
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2506913
    :goto_0
    return v1

    .line 2506914
    :cond_0
    const-string v10, "per_unit_price"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 2506915
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v6, v3

    move v3, v2

    .line 2506916
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_6

    .line 2506917
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 2506918
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2506919
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_1

    if-eqz v9, :cond_1

    .line 2506920
    const-string v10, "name"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 2506921
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 2506922
    :cond_2
    const-string v10, "node"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 2506923
    invoke-static {p0, p1}, LX/Hoi;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 2506924
    :cond_3
    const-string v10, "product_image"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 2506925
    const/4 v9, 0x0

    .line 2506926
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v10, :cond_d

    .line 2506927
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2506928
    :goto_2
    move v5, v9

    .line 2506929
    goto :goto_1

    .line 2506930
    :cond_4
    const-string v10, "quantity"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 2506931
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v4, v0

    move v0, v2

    goto :goto_1

    .line 2506932
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2506933
    :cond_6
    const/4 v9, 0x5

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 2506934
    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 2506935
    invoke-virtual {p1, v2, v7}, LX/186;->b(II)V

    .line 2506936
    if-eqz v3, :cond_7

    .line 2506937
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v6, v1}, LX/186;->a(III)V

    .line 2506938
    :cond_7
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v5}, LX/186;->b(II)V

    .line 2506939
    if-eqz v0, :cond_8

    .line 2506940
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4, v1}, LX/186;->a(III)V

    .line 2506941
    :cond_8
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_9
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    goto/16 :goto_1

    .line 2506942
    :cond_a
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2506943
    :cond_b
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_c

    .line 2506944
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 2506945
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2506946
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_b

    if-eqz v10, :cond_b

    .line 2506947
    const-string v11, "image"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_a

    .line 2506948
    const/4 v10, 0x0

    .line 2506949
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v11, :cond_11

    .line 2506950
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2506951
    :goto_4
    move v5, v10

    .line 2506952
    goto :goto_3

    .line 2506953
    :cond_c
    const/4 v10, 0x1

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 2506954
    invoke-virtual {p1, v9, v5}, LX/186;->b(II)V

    .line 2506955
    invoke-virtual {p1}, LX/186;->d()I

    move-result v9

    goto :goto_2

    :cond_d
    move v5, v9

    goto :goto_3

    .line 2506956
    :cond_e
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2506957
    :cond_f
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_10

    .line 2506958
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 2506959
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2506960
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_f

    if-eqz v11, :cond_f

    .line 2506961
    const-string v12, "uri"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_e

    .line 2506962
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_5

    .line 2506963
    :cond_10
    const/4 v11, 0x1

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 2506964
    invoke-virtual {p1, v10, v5}, LX/186;->b(II)V

    .line 2506965
    invoke-virtual {p1}, LX/186;->d()I

    move-result v10

    goto :goto_4

    :cond_11
    move v5, v10

    goto :goto_5
.end method
