.class public LX/HMG;
.super LX/3x6;
.source ""


# instance fields
.field private final a:I

.field private final b:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(ILandroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p2    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2456952
    invoke-direct {p0}, LX/3x6;-><init>()V

    .line 2456953
    iput p1, p0, LX/HMG;->a:I

    .line 2456954
    iput-object p2, p0, LX/HMG;->b:Landroid/graphics/drawable/Drawable;

    .line 2456955
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;LX/1Ok;)V
    .locals 7

    .prologue
    .line 2456956
    iget-object v0, p0, LX/HMG;->b:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_1

    .line 2456957
    :cond_0
    return-void

    .line 2456958
    :cond_1
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v2

    .line 2456959
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v0

    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v1

    sub-int v3, v0, v1

    .line 2456960
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getChildCount()I

    move-result v4

    .line 2456961
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    .line 2456962
    invoke-virtual {p2, v1}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 2456963
    invoke-virtual {p0, v5, p2}, LX/HMG;->a(Landroid/view/View;Landroid/support/v7/widget/RecyclerView;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2456964
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/1a3;

    .line 2456965
    invoke-virtual {v5}, Landroid/view/View;->getBottom()I

    move-result v5

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v0, v5

    .line 2456966
    iget v5, p0, LX/HMG;->a:I

    add-int/2addr v5, v0

    .line 2456967
    iget-object v6, p0, LX/HMG;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6, v2, v0, v3, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2456968
    iget-object v0, p0, LX/HMG;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 2456969
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final a(Landroid/graphics/Rect;Landroid/view/View;Landroid/support/v7/widget/RecyclerView;LX/1Ok;)V
    .locals 1

    .prologue
    .line 2456970
    invoke-super {p0, p1, p2, p3, p4}, LX/3x6;->a(Landroid/graphics/Rect;Landroid/view/View;Landroid/support/v7/widget/RecyclerView;LX/1Ok;)V

    .line 2456971
    invoke-virtual {p0, p2, p3}, LX/HMG;->a(Landroid/view/View;Landroid/support/v7/widget/RecyclerView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2456972
    iget v0, p0, LX/HMG;->a:I

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    .line 2456973
    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;Landroid/support/v7/widget/RecyclerView;)Z
    .locals 1

    .prologue
    .line 2456974
    const/4 v0, 0x1

    return v0
.end method
