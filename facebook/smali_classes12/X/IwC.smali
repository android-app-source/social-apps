.class public LX/IwC;
.super LX/3Tf;
.source ""


# static fields
.field private static final c:Ljava/lang/Object;

.field private static final d:Ljava/lang/Object;


# instance fields
.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/IwA;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Landroid/content/Context;

.field public g:LX/154;

.field private h:LX/0wM;

.field public i:LX/17W;

.field public j:LX/Iw6;

.field public k:LX/Iwb;

.field public l:LX/Iwr;

.field private m:Z

.field private n:Landroid/graphics/drawable/Drawable;

.field private o:Landroid/graphics/drawable/Drawable;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2629562
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/IwC;->c:Ljava/lang/Object;

    .line 2629563
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/IwC;->d:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/154;LX/0wM;LX/Iw6;LX/17W;LX/Iwb;LX/Iwr;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2629458
    invoke-direct {p0}, LX/3Tf;-><init>()V

    .line 2629459
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/IwC;->m:Z

    .line 2629460
    iput-object p1, p0, LX/IwC;->f:Landroid/content/Context;

    .line 2629461
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/IwC;->e:Ljava/util/List;

    .line 2629462
    iput-object p2, p0, LX/IwC;->g:LX/154;

    .line 2629463
    iput-object p3, p0, LX/IwC;->h:LX/0wM;

    .line 2629464
    iput-object p4, p0, LX/IwC;->j:LX/Iw6;

    .line 2629465
    iput-object p5, p0, LX/IwC;->i:LX/17W;

    .line 2629466
    iput-object p6, p0, LX/IwC;->k:LX/Iwb;

    .line 2629467
    iput-object p7, p0, LX/IwC;->l:LX/Iwr;

    .line 2629468
    iget-object v0, p0, LX/IwC;->h:LX/0wM;

    const v1, 0x7f0208fa

    const v2, -0xa76f01

    invoke-virtual {v0, v1, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/IwC;->o:Landroid/graphics/drawable/Drawable;

    .line 2629469
    iget-object v0, p0, LX/IwC;->h:LX/0wM;

    const v1, 0x7f0208fa

    const v2, -0x808081

    invoke-virtual {v0, v1, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/IwC;->n:Landroid/graphics/drawable/Drawable;

    .line 2629470
    return-void
.end method

.method public static a(LX/IwC;Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;Z)V
    .locals 1

    .prologue
    .line 2629558
    if-eqz p2, :cond_0

    .line 2629559
    iget-object v0, p0, LX/IwC;->o:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2629560
    :goto_0
    return-void

    .line 2629561
    :cond_0
    iget-object v0, p0, LX/IwC;->n:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/IwC;
    .locals 8

    .prologue
    .line 2629556
    new-instance v0, LX/IwC;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/154;->a(LX/0QB;)LX/154;

    move-result-object v2

    check-cast v2, LX/154;

    invoke-static {p0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v3

    check-cast v3, LX/0wM;

    invoke-static {p0}, LX/Iw6;->a(LX/0QB;)LX/Iw6;

    move-result-object v4

    check-cast v4, LX/Iw6;

    invoke-static {p0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v5

    check-cast v5, LX/17W;

    invoke-static {p0}, LX/Iwb;->a(LX/0QB;)LX/Iwb;

    move-result-object v6

    check-cast v6, LX/Iwb;

    invoke-static {p0}, LX/Iwr;->a(LX/0QB;)LX/Iwr;

    move-result-object v7

    check-cast v7, LX/Iwr;

    invoke-direct/range {v0 .. v7}, LX/IwC;-><init>(Landroid/content/Context;LX/154;LX/0wM;LX/Iw6;LX/17W;LX/Iwb;LX/Iwr;)V

    .line 2629557
    return-object v0
.end method

.method public static c(LX/IwC;Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 2629547
    invoke-virtual {p1}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->m()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v6}, LX/15i;->j(II)I

    move-result v0

    if-eqz v0, :cond_0

    .line 2629548
    invoke-virtual {p1}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->m()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2629549
    invoke-virtual {p1}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->m()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2629550
    iget-object v4, p0, LX/IwC;->f:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f017e

    invoke-virtual {v1, v0, v6}, LX/15i;->j(II)I

    move-result v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v3, v2, v6}, LX/15i;->j(II)I

    move-result v2

    .line 2629551
    const/16 v3, 0x3e8

    if-ge v2, v3, :cond_1

    .line 2629552
    const-string v3, "%,d"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v7, v8

    invoke-static {v3, v7}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2629553
    :goto_0
    move-object v2, v3

    .line 2629554
    aput-object v2, v1, v6

    invoke-virtual {v4, v5, v0, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2629555
    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    iget-object v3, p0, LX/IwC;->g:LX/154;

    invoke-virtual {v3, v2}, LX/154;->a(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method private f(I)LX/IwA;
    .locals 1

    .prologue
    .line 2629546
    iget-object v0, p0, LX/IwC;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IwA;

    return-object v0
.end method


# virtual methods
.method public final a(I)I
    .locals 1

    .prologue
    .line 2629545
    sget-object v0, LX/IwB;->HEADER:LX/IwB;

    invoke-virtual {v0}, LX/IwB;->ordinal()I

    move-result v0

    return v0
.end method

.method public final a(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2629498
    invoke-virtual {p0, p1, p2}, LX/IwC;->a(II)Ljava/lang/Object;

    move-result-object v0

    .line 2629499
    sget-object v1, LX/IwC;->c:Ljava/lang/Object;

    if-ne v0, v1, :cond_0

    .line 2629500
    if-eqz p4, :cond_6

    instance-of v0, p4, LX/Iwp;

    if-eqz v0, :cond_6

    .line 2629501
    check-cast p4, LX/Iwp;

    .line 2629502
    :goto_0
    move-object v0, p4

    .line 2629503
    :goto_1
    return-object v0

    .line 2629504
    :cond_0
    sget-object v1, LX/IwC;->d:Ljava/lang/Object;

    if-ne v0, v1, :cond_1

    .line 2629505
    invoke-direct {p0, p1}, LX/IwC;->f(I)LX/IwA;

    move-result-object v0

    iget-object v0, v0, LX/IwA;->b:Ljava/lang/String;

    .line 2629506
    if-eqz p4, :cond_7

    instance-of v1, p4, LX/Iwq;

    if-eqz v1, :cond_7

    .line 2629507
    check-cast p4, LX/Iwq;

    .line 2629508
    :goto_2
    move-object v0, p4

    .line 2629509
    goto :goto_1

    .line 2629510
    :cond_1
    invoke-virtual {p0, p1, p2}, LX/IwC;->a(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;

    .line 2629511
    if-eqz p4, :cond_8

    instance-of v1, p4, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    if-eqz v1, :cond_8

    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    .line 2629512
    :goto_3
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setMaxLinesFromThumbnailSize(Z)V

    .line 2629513
    sget-object v1, LX/6VF;->LARGE:LX/6VF;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setThumbnailSize(LX/6VF;)V

    .line 2629514
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2629515
    iget-object v1, p0, LX/IwC;->l:LX/Iwr;

    invoke-virtual {v0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->c()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, LX/Iwr;->c(Ljava/lang/String;)Z

    move-result v1

    invoke-static {p0, p4, v1}, LX/IwC;->a(LX/IwC;Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;Z)V

    .line 2629516
    new-instance v1, LX/Iw7;

    invoke-direct {v1, p0, v0}, LX/Iw7;-><init>(LX/IwC;Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;)V

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2629517
    new-instance v1, LX/Iw8;

    invoke-direct {v1, p0, p4, v0}, LX/Iw8;-><init>(LX/IwC;Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;)V

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2629518
    invoke-virtual {v0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->l()Ljava/lang/String;

    move-result-object v1

    .line 2629519
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_2

    .line 2629520
    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2629521
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->j()LX/0Px;

    move-result-object v1

    const/4 p1, 0x0

    invoke-virtual {v1, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v1, v1

    .line 2629522
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_3

    .line 2629523
    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2629524
    :cond_3
    const/4 p1, 0x0

    .line 2629525
    invoke-virtual {v0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->k()Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel$InvitersForViewerToLikeModel;

    move-result-object v1

    if-eqz v1, :cond_a

    invoke-virtual {v0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->k()Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel$InvitersForViewerToLikeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel$InvitersForViewerToLikeModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_a

    invoke-virtual {v0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->k()Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel$InvitersForViewerToLikeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel$InvitersForViewerToLikeModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel$InvitersForViewerToLikeModel$NodesModel;

    invoke-virtual {v1}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel$InvitersForViewerToLikeModel$NodesModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    const/4 v1, 0x1

    :goto_4
    move v1, v1

    .line 2629526
    if-eqz v1, :cond_9

    .line 2629527
    const/4 p5, 0x0

    .line 2629528
    iget-object v1, p0, LX/IwC;->f:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f0838cb

    const/4 v1, 0x1

    new-array p3, v1, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->k()Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel$InvitersForViewerToLikeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel$InvitersForViewerToLikeModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, p5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel$InvitersForViewerToLikeModel$NodesModel;

    invoke-virtual {v1}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel$InvitersForViewerToLikeModel$NodesModel;->a()Ljava/lang/String;

    move-result-object v1

    aput-object v1, p3, p5

    invoke-virtual {p1, p2, p3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 2629529
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_4

    .line 2629530
    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setMetaText(Ljava/lang/CharSequence;)V

    .line 2629531
    :cond_4
    :goto_5
    invoke-virtual {v0}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    .line 2629532
    if-eqz v0, :cond_5

    .line 2629533
    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 2629534
    :cond_5
    move-object v0, p4

    .line 2629535
    goto/16 :goto_1

    .line 2629536
    :cond_6
    new-instance p4, LX/Iwp;

    iget-object v0, p0, LX/IwC;->f:Landroid/content/Context;

    invoke-direct {p4, v0}, LX/Iwp;-><init>(Landroid/content/Context;)V

    .line 2629537
    const v0, 0x7f0a00d5

    invoke-virtual {p4, v0}, LX/Iwp;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 2629538
    :cond_7
    new-instance p4, LX/Iwq;

    iget-object v1, p0, LX/IwC;->f:Landroid/content/Context;

    invoke-direct {p4, v1}, LX/Iwq;-><init>(Landroid/content/Context;)V

    .line 2629539
    const v1, 0x7f0a00d5

    invoke-virtual {p4, v1}, LX/Iwq;->setBackgroundResource(I)V

    .line 2629540
    new-instance v1, LX/Iw9;

    invoke-direct {v1, p0, v0}, LX/Iw9;-><init>(LX/IwC;Ljava/lang/String;)V

    invoke-virtual {p4, v1}, LX/Iwq;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_2

    .line 2629541
    :cond_8
    new-instance p4, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    iget-object v1, p0, LX/IwC;->f:Landroid/content/Context;

    invoke-direct {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;-><init>(Landroid/content/Context;)V

    goto/16 :goto_3

    .line 2629542
    :cond_9
    invoke-static {p0, v0}, LX/IwC;->c(LX/IwC;Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$RecommendedPageFieldsModel;)Ljava/lang/String;

    move-result-object v1

    .line 2629543
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_4

    .line 2629544
    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setMetaText(Ljava/lang/CharSequence;)V

    goto :goto_5

    :cond_a
    move v1, p1

    goto/16 :goto_4
.end method

.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2629493
    instance-of v0, p2, Lcom/facebook/fbui/widget/header/SectionHeaderView;

    if-eqz v0, :cond_0

    if-nez p2, :cond_1

    .line 2629494
    :cond_0
    new-instance p2, Lcom/facebook/fbui/widget/header/SectionHeaderView;

    iget-object v0, p0, LX/IwC;->f:Landroid/content/Context;

    invoke-direct {p2, v0}, Lcom/facebook/fbui/widget/header/SectionHeaderView;-><init>(Landroid/content/Context;)V

    .line 2629495
    :goto_0
    invoke-direct {p0, p1}, LX/IwC;->f(I)LX/IwA;

    move-result-object v0

    iget-object v0, v0, LX/IwA;->a:Ljava/lang/String;

    invoke-virtual {p2, v0}, Lcom/facebook/fbui/widget/header/SectionHeaderView;->setTitleText(Ljava/lang/String;)V

    .line 2629496
    return-object p2

    .line 2629497
    :cond_1
    check-cast p2, Lcom/facebook/fbui/widget/header/SectionHeaderView;

    goto :goto_0
.end method

.method public final a(II)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2629490
    invoke-direct {p0, p1}, LX/IwC;->f(I)LX/IwA;

    move-result-object v0

    iget-object v0, v0, LX/IwA;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eq p2, v0, :cond_0

    invoke-direct {p0, p1}, LX/IwC;->f(I)LX/IwA;

    move-result-object v0

    iget-boolean v0, v0, LX/IwA;->d:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    if-ne p2, v0, :cond_1

    .line 2629491
    :cond_0
    sget-object v0, LX/IwC;->d:Ljava/lang/Object;

    .line 2629492
    :goto_0
    return-object v0

    :cond_1
    invoke-direct {p0, p1}, LX/IwC;->f(I)LX/IwA;

    move-result-object v0

    iget-object v0, v0, LX/IwA;->c:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;ZZ)V
    .locals 3

    .prologue
    .line 2629484
    new-instance v0, LX/IwA;

    invoke-virtual {p1}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/IwA;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2629485
    invoke-virtual {p1}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel;->k()Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel$SuggestedPagesModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/pages/browser/data/graphql/RecommendedPagesModels$AllSectionsRecommendedPagesModel$PageBrowserCategoriesModel$NodesModel$SuggestedPagesModel;->a()LX/0Px;

    move-result-object v1

    iput-object v1, v0, LX/IwA;->c:Ljava/util/List;

    .line 2629486
    iput-boolean p2, v0, LX/IwA;->d:Z

    .line 2629487
    iput-boolean p3, v0, LX/IwA;->e:Z

    .line 2629488
    iget-object v1, p0, LX/IwC;->e:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2629489
    return-void
.end method

.method public final synthetic b(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2629483
    invoke-direct {p0, p1}, LX/IwC;->f(I)LX/IwA;

    move-result-object v0

    return-object v0
.end method

.method public final b(II)Z
    .locals 1

    .prologue
    .line 2629482
    const/4 v0, 0x1

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 2629481
    iget-object v0, p0, LX/IwC;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final c(I)I
    .locals 1

    .prologue
    .line 2629475
    invoke-direct {p0, p1}, LX/IwC;->f(I)LX/IwA;

    move-result-object v0

    iget-boolean v0, v0, LX/IwA;->d:Z

    if-eqz v0, :cond_0

    .line 2629476
    const/4 v0, 0x3

    .line 2629477
    :goto_0
    return v0

    .line 2629478
    :cond_0
    invoke-direct {p0, p1}, LX/IwC;->f(I)LX/IwA;

    move-result-object v0

    iget-boolean v0, v0, LX/IwA;->e:Z

    if-eqz v0, :cond_1

    .line 2629479
    invoke-direct {p0, p1}, LX/IwC;->f(I)LX/IwA;

    move-result-object v0

    iget-object v0, v0, LX/IwA;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2629480
    :cond_1
    invoke-direct {p0, p1}, LX/IwC;->f(I)LX/IwA;

    move-result-object v0

    iget-object v0, v0, LX/IwA;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public final c(II)I
    .locals 2

    .prologue
    .line 2629471
    invoke-virtual {p0, p1, p2}, LX/IwC;->a(II)Ljava/lang/Object;

    move-result-object v0

    .line 2629472
    sget-object v1, LX/IwC;->c:Ljava/lang/Object;

    if-ne v0, v1, :cond_0

    .line 2629473
    sget-object v0, LX/IwB;->LOADING:LX/IwB;

    invoke-virtual {v0}, LX/IwB;->ordinal()I

    move-result v0

    .line 2629474
    :goto_0
    return v0

    :cond_0
    sget-object v0, LX/IwB;->PAGE:LX/IwB;

    invoke-virtual {v0}, LX/IwB;->ordinal()I

    move-result v0

    goto :goto_0
.end method
