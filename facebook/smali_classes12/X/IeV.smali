.class public LX/IeV;
.super LX/3Ml;
.source ""


# instance fields
.field private final c:LX/2UR;

.field private final d:LX/1Ml;


# direct methods
.method public constructor <init>(LX/0Zr;LX/2UR;LX/1Ml;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2598665
    invoke-direct {p0, p1}, LX/3Ml;-><init>(LX/0Zr;)V

    .line 2598666
    iput-object p2, p0, LX/IeV;->c:LX/2UR;

    .line 2598667
    iput-object p3, p0, LX/IeV;->d:LX/1Ml;

    .line 2598668
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2598669
    iget-object v0, p0, LX/IeV;->c:LX/2UR;

    const/16 v1, 0x1e

    invoke-virtual {v0, p1, v1}, LX/2UR;->a(Ljava/lang/String;I)LX/6N9;

    move-result-object v1

    .line 2598670
    :cond_0
    :goto_0
    :try_start_0
    invoke-virtual {v1}, LX/0Rr;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2598671
    invoke-virtual {v1}, LX/0Rr;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2598672
    invoke-static {v0}, LX/FFa;->a(Lcom/facebook/user/model/User;)Lcom/facebook/user/model/UserPhoneNumber;

    move-result-object v2

    .line 2598673
    if-eqz v2, :cond_0

    .line 2598674
    iget-object v2, v0, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v2, v2

    .line 2598675
    invoke-interface {p2, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2598676
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/6N9;->c()V

    throw v0

    :cond_1
    invoke-virtual {v1}, LX/6N9;->c()V

    .line 2598677
    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/CharSequence;)LX/39y;
    .locals 6

    .prologue
    .line 2598678
    new-instance v1, LX/39y;

    invoke-direct {v1}, LX/39y;-><init>()V

    .line 2598679
    if-eqz p1, :cond_2

    :try_start_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 2598680
    :goto_0
    iget-object v2, p0, LX/IeV;->d:LX/1Ml;

    const-string v3, "android.permission.READ_CONTACTS"

    invoke-virtual {v2, v3}, LX/1Ml;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 2598681
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v2

    .line 2598682
    invoke-direct {p0, v0, v2}, LX/IeV;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 2598683
    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2598684
    new-instance v2, LX/IeU;

    invoke-direct {v2, p0}, LX/IeU;-><init>(LX/IeV;)V

    invoke-static {v0, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2598685
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2598686
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/user/model/User;

    .line 2598687
    iget-object v5, p0, LX/3Ml;->b:LX/3Md;

    invoke-interface {v5, v3}, LX/3Md;->a(Ljava/lang/Object;)LX/3OQ;

    move-result-object v3

    .line 2598688
    if-eqz v3, :cond_0

    .line 2598689
    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 2598690
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2598691
    invoke-static {p1, v0}, LX/3Og;->a(Ljava/lang/CharSequence;LX/0Px;)LX/3Og;

    move-result-object v0

    .line 2598692
    iput-object v0, v1, LX/39y;->a:Ljava/lang/Object;

    .line 2598693
    iget v2, v0, LX/3Og;->d:I

    move v0, v2

    .line 2598694
    iput v0, v1, LX/39y;->b:I

    .line 2598695
    :goto_2
    return-object v1

    .line 2598696
    :cond_2
    const-string v0, ""

    goto :goto_0

    .line 2598697
    :cond_3
    invoke-static {p1}, LX/3Og;->a(Ljava/lang/CharSequence;)LX/3Og;

    move-result-object v0

    iput-object v0, v1, LX/39y;->a:Ljava/lang/Object;

    .line 2598698
    const/4 v0, -0x1

    iput v0, v1, LX/39y;->b:I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 2598699
    :catch_0
    move-exception v0

    .line 2598700
    const-string v1, "orca:ContactPickerPhoneContactsFilter"

    const-string v2, "exception while filtering"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2598701
    throw v0
.end method
