.class public final LX/J9e;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;)V
    .locals 0

    .prologue
    .line 2653151
    iput-object p1, p0, LX/J9e;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2653138
    iget-object v0, p0, LX/J9e;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    invoke-virtual {v0, p1}, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->a(Ljava/lang/Throwable;)V

    .line 2653139
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2653140
    check-cast p1, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel;

    .line 2653141
    iget-object v0, p0, LX/J9e;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    iget-object v0, v0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0xbf0001

    iget-object v2, p0, LX/J9e;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    iget v2, v2, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->J:I

    const/4 v3, 0x2

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 2653142
    iget-object v0, p0, LX/J9e;->a:Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;

    .line 2653143
    invoke-static {v0}, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->n$redex0(Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 2653144
    iget-object v1, v0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->t:LX/J9W;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->c:LX/J8y;

    if-nez v1, :cond_2

    .line 2653145
    :cond_0
    :goto_1
    return-void

    .line 2653146
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 2653147
    :cond_2
    iget-object v1, v0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->t:LX/J9W;

    invoke-virtual {v1}, LX/J9W;->a()V

    .line 2653148
    iget-object v1, v0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->t:LX/J9W;

    invoke-virtual {v1, p1}, LX/J9W;->a(Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel;)V

    .line 2653149
    iget-object v1, v0, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->c:LX/J8y;

    sget-object v2, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->D:LX/J8x;

    invoke-virtual {v1, v2}, LX/J8y;->c(LX/J8x;)V

    .line 2653150
    invoke-static {v0}, Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;->k(Lcom/facebook/timeline/aboutpage/collection/CollectionsCollectionFragment;)V

    goto :goto_1
.end method
