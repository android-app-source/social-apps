.class public LX/IX0;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/EtN;

.field public b:LX/IT4;

.field private c:Lcom/facebook/fig/actionbar/FigActionBar;


# direct methods
.method public constructor <init>(Lcom/facebook/fig/actionbar/FigActionBar;)V
    .locals 3

    .prologue
    .line 2585396
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2585397
    new-instance v0, LX/IWz;

    invoke-direct {v0, p0}, LX/IWz;-><init>(LX/IX0;)V

    iput-object v0, p0, LX/IX0;->a:LX/EtN;

    .line 2585398
    iput-object p1, p0, LX/IX0;->c:Lcom/facebook/fig/actionbar/FigActionBar;

    .line 2585399
    iget-object v0, p0, LX/IX0;->c:Lcom/facebook/fig/actionbar/FigActionBar;

    iget-object v1, p0, LX/IX0;->a:LX/EtN;

    .line 2585400
    iput-object v1, v0, LX/EtL;->b:LX/EtN;

    .line 2585401
    iget-object v0, p0, LX/IX0;->c:Lcom/facebook/fig/actionbar/FigActionBar;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/EtL;->a(ZZ)V

    .line 2585402
    return-void
.end method


# virtual methods
.method public final a(LX/0Px;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/IX1;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2585403
    iget-object v0, p0, LX/IX0;->c:Lcom/facebook/fig/actionbar/FigActionBar;

    invoke-virtual {v0}, LX/EtL;->a()V

    .line 2585404
    iget-object v0, p0, LX/IX0;->c:Lcom/facebook/fig/actionbar/FigActionBar;

    invoke-virtual {v0}, LX/EtL;->clear()V

    .line 2585405
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IX1;

    .line 2585406
    iget-object v4, p0, LX/IX0;->c:Lcom/facebook/fig/actionbar/FigActionBar;

    invoke-virtual {v0}, LX/IX1;->ordinal()I

    move-result v5

    iget v6, v0, LX/IX1;->labelResId:I

    invoke-virtual {v4, v2, v5, v2, v6}, LX/EtL;->a(IIII)LX/3qv;

    move-result-object v4

    iget v5, v0, LX/IX1;->showAsAction:I

    invoke-interface {v4, v5}, LX/3qv;->setShowAsActionFlags(I)Landroid/view/MenuItem;

    move-result-object v4

    iget v5, v0, LX/IX1;->iconMediumResId:I

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v4

    iget-boolean v5, v0, LX/IX1;->enabled:Z

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    move-result-object v4

    iget-boolean v5, v0, LX/IX1;->checkable:Z

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setCheckable(Z)Landroid/view/MenuItem;

    move-result-object v4

    iget-boolean v0, v0, LX/IX1;->checked:Z

    invoke-interface {v4, v0}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    .line 2585407
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2585408
    :cond_0
    iget-object v0, p0, LX/IX0;->c:Lcom/facebook/fig/actionbar/FigActionBar;

    invoke-virtual {v0}, LX/EtL;->b()V

    .line 2585409
    return-void
.end method
