.class public final LX/If0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Iez;


# instance fields
.field public final synthetic a:LX/If1;


# direct methods
.method public constructor <init>(LX/If1;)V
    .locals 0

    .prologue
    .line 2599267
    iput-object p1, p0, LX/If0;->a:LX/If1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)Z
    .locals 2

    .prologue
    .line 2599242
    iget-object v0, p0, LX/If0;->a:LX/If1;

    .line 2599243
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;

    .line 2599244
    if-eqz v1, :cond_0

    iget-object p0, v0, LX/If1;->c:LX/IfB;

    if-eqz p0, :cond_0

    .line 2599245
    iget-object p0, v0, LX/If1;->c:LX/IfB;

    invoke-interface {p0, v1}, LX/IfB;->d(Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)Z

    move-result v1

    .line 2599246
    :goto_0
    move v0, v1

    .line 2599247
    return v0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2599258
    iget-object v0, p0, LX/If0;->a:LX/If1;

    .line 2599259
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;

    .line 2599260
    if-eqz v1, :cond_0

    .line 2599261
    iget-object v2, v0, LX/If1;->d:Ljava/util/Set;

    iget-object p0, v1, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;->a:Lcom/facebook/user/model/User;

    .line 2599262
    iget-object p1, p0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object p0, p1

    .line 2599263
    invoke-interface {v2, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2599264
    :cond_0
    iget-object v2, v0, LX/If1;->c:LX/IfB;

    if-eqz v2, :cond_1

    .line 2599265
    iget-object v2, v0, LX/If1;->c:LX/IfB;

    invoke-interface {v2, v1}, LX/IfB;->a(Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)V

    .line 2599266
    :cond_1
    return-void
.end method

.method public final c(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2599253
    iget-object v0, p0, LX/If0;->a:LX/If1;

    .line 2599254
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;

    .line 2599255
    if-eqz v1, :cond_0

    iget-object p0, v0, LX/If1;->c:LX/IfB;

    if-eqz p0, :cond_0

    .line 2599256
    iget-object p0, v0, LX/If1;->c:LX/IfB;

    invoke-interface {p0, v1}, LX/IfB;->b(Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)V

    .line 2599257
    :cond_0
    return-void
.end method

.method public final d(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2599248
    iget-object v0, p0, LX/If0;->a:LX/If1;

    .line 2599249
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;

    .line 2599250
    if-eqz v1, :cond_0

    iget-object p0, v0, LX/If1;->c:LX/IfB;

    if-eqz p0, :cond_0

    .line 2599251
    iget-object p0, v0, LX/If1;->c:LX/IfB;

    invoke-interface {p0, v1}, LX/IfB;->c(Lcom/facebook/messaging/contactsyoumayknow/ContactSuggestion;)V

    .line 2599252
    :cond_0
    return-void
.end method
