.class public LX/J0a;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Ljava/lang/String;",
        "Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLInterfaces$PaymentPlatformContext;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/J0a;


# direct methods
.method public constructor <init>(LX/0sO;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2637354
    invoke-direct {p0, p1}, LX/0ro;-><init>(LX/0sO;)V

    .line 2637355
    return-void
.end method

.method public static a(LX/0QB;)LX/J0a;
    .locals 4

    .prologue
    .line 2637360
    sget-object v0, LX/J0a;->b:LX/J0a;

    if-nez v0, :cond_1

    .line 2637361
    const-class v1, LX/J0a;

    monitor-enter v1

    .line 2637362
    :try_start_0
    sget-object v0, LX/J0a;->b:LX/J0a;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2637363
    if-eqz v2, :cond_0

    .line 2637364
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2637365
    new-instance p0, LX/J0a;

    invoke-static {v0}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v3

    check-cast v3, LX/0sO;

    invoke-direct {p0, v3}, LX/J0a;-><init>(LX/0sO;)V

    .line 2637366
    move-object v0, p0

    .line 2637367
    sput-object v0, LX/J0a;->b:LX/J0a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2637368
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2637369
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2637370
    :cond_1
    sget-object v0, LX/J0a;->b:LX/J0a;

    return-object v0

    .line 2637371
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2637372
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2637373
    const-class v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;

    invoke-virtual {p3, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/graphql/PaymentGraphQLModels$PaymentPlatformContextModel;

    .line 2637374
    return-object v0
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 2637359
    const/4 v0, 0x1

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 2

    .prologue
    .line 2637356
    check-cast p1, Ljava/lang/String;

    .line 2637357
    new-instance v0, LX/Dtd;

    invoke-direct {v0}, LX/Dtd;-><init>()V

    move-object v0, v0

    .line 2637358
    const-string v1, "platform_context_id"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    return-object v0
.end method
