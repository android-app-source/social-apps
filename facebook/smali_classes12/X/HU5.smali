.class public LX/HU5;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/HU4;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel$OpinionsEdgesModel$OpinionsEdgesNodeModel;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/view/LayoutInflater;

.field public d:LX/0Uh;

.field public e:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;


# direct methods
.method public constructor <init>(Ljava/util/List;Landroid/content/Context;Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;LX/0Uh;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel$OpinionsEdgesModel$OpinionsEdgesNodeModel;",
            ">;",
            "Landroid/content/Context;",
            "Lcom/facebook/pages/fb4a/politics/PagePoliticalIssuesConfig;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2471577
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2471578
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2471579
    iput-object p2, p0, LX/HU5;->a:Landroid/content/Context;

    .line 2471580
    iput-object p1, p0, LX/HU5;->b:Ljava/util/List;

    .line 2471581
    iget-object v0, p0, LX/HU5;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, LX/HU5;->c:Landroid/view/LayoutInflater;

    .line 2471582
    iput-object p3, p0, LX/HU5;->e:Lcom/facebook/pages/fb4a/politics/PageSurfacePoliticalIssuesListFragment;

    .line 2471583
    iput-object p4, p0, LX/HU5;->d:LX/0Uh;

    .line 2471584
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2471585
    iget-object v0, p0, LX/HU5;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f0309a0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2471586
    new-instance v1, LX/HU4;

    invoke-direct {v1, p0, v0}, LX/HU4;-><init>(LX/HU5;Landroid/view/View;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 1

    .prologue
    .line 2471587
    check-cast p1, LX/HU4;

    .line 2471588
    iget-object v0, p0, LX/HU5;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel$OpinionsEdgesModel$OpinionsEdgesNodeModel;

    .line 2471589
    invoke-virtual {p1, v0}, LX/HU4;->a(Lcom/facebook/pages/fb4a/politics/PoliticalIssuesGraphQLModels$PoliticalIssuesQueryModel$PoliticianIssueViewsCollectionModel$EdgesModel$NodeModel$OpinionsModel$OpinionsEdgesModel$OpinionsEdgesNodeModel;)V

    .line 2471590
    return-void
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2471591
    iget-object v0, p0, LX/HU5;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
