.class public final LX/JEm;
.super Landroid/widget/ArrayAdapter;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "LX/JEn;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "LX/JEn;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2666399
    iput-object p1, p0, LX/JEm;->a:Lcom/facebook/zero/settings/FreeFacebookSettingsActivity;

    .line 2666400
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 2666401
    return-void
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 2666402
    if-nez p2, :cond_0

    .line 2666403
    invoke-virtual {p0}, LX/JEm;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0306cc

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 2666404
    :cond_0
    invoke-virtual {p0, p1}, LX/JEm;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/JEn;

    .line 2666405
    const v1, 0x7f0d11bb

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    .line 2666406
    iget-boolean v2, v0, LX/JEn;->f:Z

    if-eqz v2, :cond_3

    iget-object v2, v0, LX/JEn;->a:Ljava/lang/String;

    :goto_0
    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2666407
    const v1, 0x7f0d125a

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CompoundButton;

    .line 2666408
    iget-boolean v2, v0, LX/JEn;->f:Z

    invoke-virtual {v1, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 2666409
    iget-object v2, v0, LX/JEn;->j:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2666410
    const v1, 0x7f0d125b

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    .line 2666411
    const v2, 0x7f0d125c

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/resources/ui/FbTextView;

    .line 2666412
    iget-boolean v3, v0, LX/JEn;->f:Z

    if-eqz v3, :cond_4

    iget-object v3, v0, LX/JEn;->c:Ljava/lang/CharSequence;

    if-eqz v3, :cond_4

    .line 2666413
    iget-object v3, v0, LX/JEn;->c:Ljava/lang/CharSequence;

    invoke-virtual {v1, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2666414
    invoke-virtual {v1, v4}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2666415
    invoke-virtual {v2, v5}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2666416
    :cond_1
    :goto_1
    iget-boolean v1, v0, LX/JEn;->g:Z

    if-eqz v1, :cond_2

    .line 2666417
    const v1, 0x7f0d125d

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 2666418
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2666419
    const v1, 0x7f0d125e

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    .line 2666420
    const v2, 0x7f0d125f

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    .line 2666421
    iget-object v3, v0, LX/JEn;->h:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2666422
    iget-boolean v0, v0, LX/JEn;->i:Z

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 2666423
    :cond_2
    return-object p2

    .line 2666424
    :cond_3
    iget-object v2, v0, LX/JEn;->b:Ljava/lang/String;

    goto :goto_0

    .line 2666425
    :cond_4
    iget-boolean v3, v0, LX/JEn;->f:Z

    if-nez v3, :cond_1

    iget-object v3, v0, LX/JEn;->d:Ljava/lang/CharSequence;

    if-eqz v3, :cond_1

    .line 2666426
    iget-object v3, v0, LX/JEn;->d:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2666427
    invoke-virtual {v2, v4}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2666428
    invoke-virtual {v1, v5}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2666429
    new-instance v1, LX/JEl;

    invoke-direct {v1, p0}, LX/JEl;-><init>(LX/JEm;)V

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method
