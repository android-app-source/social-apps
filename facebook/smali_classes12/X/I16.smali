.class public final LX/I16;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;


# direct methods
.method public constructor <init>(Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;)V
    .locals 0

    .prologue
    .line 2528509
    iput-object p1, p0, LX/I16;->a:Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/graphql/EventDashboardGraphQLModels$FetchEventDashboardTimeFiltersQueryModel$EventDiscoverTimeFiltersModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2528510
    iget-object v0, p0, LX/I16;->a:Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;

    iget-object v0, v0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->k:LX/I1f;

    .line 2528511
    iget-object v1, v0, LX/I1f;->h:LX/I1Y;

    .line 2528512
    iget-object v2, v1, LX/I1Y;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 2528513
    iget-object v2, v1, LX/I1Y;->e:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2528514
    invoke-virtual {v1}, LX/1OM;->notifyDataSetChanged()V

    .line 2528515
    invoke-static {v0}, LX/I1f;->i(LX/I1f;)V

    .line 2528516
    iget-object v0, p0, LX/I16;->a:Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;

    iget-object v0, v0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->p:LX/HzH;

    sget-object v1, LX/Hx7;->DISCOVER:LX/Hx7;

    invoke-virtual {v0, v1}, LX/HzH;->a(LX/Hx7;)V

    .line 2528517
    iget-object v0, p0, LX/I16;->a:Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;

    iget-object v0, v0, Lcom/facebook/events/dashboard/home/EventsHomeDashboardController;->k:LX/I1f;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/I1f;->b(Z)V

    .line 2528518
    return-void
.end method
