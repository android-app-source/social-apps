.class public LX/J8y;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/J8y;


# instance fields
.field private final a:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field private final b:LX/J8z;


# direct methods
.method public constructor <init>(Lcom/facebook/quicklog/QuickPerformanceLogger;LX/J8z;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2652343
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2652344
    iput-object p1, p0, LX/J8y;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 2652345
    iput-object p2, p0, LX/J8y;->b:LX/J8z;

    .line 2652346
    return-void
.end method

.method public static a(LX/0QB;)LX/J8y;
    .locals 5

    .prologue
    .line 2652330
    sget-object v0, LX/J8y;->c:LX/J8y;

    if-nez v0, :cond_1

    .line 2652331
    const-class v1, LX/J8y;

    monitor-enter v1

    .line 2652332
    :try_start_0
    sget-object v0, LX/J8y;->c:LX/J8y;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2652333
    if-eqz v2, :cond_0

    .line 2652334
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2652335
    new-instance p0, LX/J8y;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v3

    check-cast v3, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {v0}, LX/J8z;->b(LX/0QB;)LX/J8z;

    move-result-object v4

    check-cast v4, LX/J8z;

    invoke-direct {p0, v3, v4}, LX/J8y;-><init>(Lcom/facebook/quicklog/QuickPerformanceLogger;LX/J8z;)V

    .line 2652336
    move-object v0, p0

    .line 2652337
    sput-object v0, LX/J8y;->c:LX/J8y;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2652338
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2652339
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2652340
    :cond_1
    sget-object v0, LX/J8y;->c:LX/J8y;

    return-object v0

    .line 2652341
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2652342
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static d(LX/J8x;)I
    .locals 2

    .prologue
    .line 2652325
    sget-object v0, LX/J8w;->a:[I

    invoke-virtual {p0}, LX/J8x;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2652326
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 2652327
    :pswitch_0
    const v0, 0x1a002b

    goto :goto_0

    .line 2652328
    :pswitch_1
    const v0, 0x1a002c

    goto :goto_0

    .line 2652329
    :pswitch_2
    const v0, 0x1a002d

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static e(LX/J8x;)I
    .locals 2

    .prologue
    .line 2652320
    sget-object v0, LX/J8w;->a:[I

    invoke-virtual {p0}, LX/J8x;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2652321
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 2652322
    :pswitch_0
    const v0, 0x1a002e

    goto :goto_0

    .line 2652323
    :pswitch_1
    const v0, 0x1a002f

    goto :goto_0

    .line 2652324
    :pswitch_2
    const v0, 0x1a0030

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static f(LX/J8x;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/J8x;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2652270
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Integer;

    const/4 v2, 0x0

    invoke-static {p0}, LX/J8y;->d(LX/J8x;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p0}, LX/J8y;->e(LX/J8x;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2652271
    sget-object v1, LX/J8x;->COLLECTION:LX/J8x;

    invoke-virtual {p0, v1}, LX/J8x;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2652272
    const v1, 0x1a0022

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2652273
    const v1, 0x1a0023

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2652274
    :cond_0
    sget-object v1, LX/J8x;->SUMMARY:LX/J8x;

    invoke-virtual {p0, v1}, LX/J8x;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2652275
    const v1, 0x1a001e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2652276
    :cond_1
    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2652347
    iget-object v0, p0, LX/J8y;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x1a0023

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 2652348
    iget-object v0, p0, LX/J8y;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x1a0022

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2652349
    return-void
.end method

.method public final a(LX/J8x;)V
    .locals 6

    .prologue
    .line 2652298
    const/4 v0, 0x1

    .line 2652299
    sget-object v1, LX/J8w;->a:[I

    invoke-virtual {p1}, LX/J8x;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    move v1, v0

    .line 2652300
    :goto_0
    invoke-static {p1}, LX/J8y;->f(LX/J8x;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 2652301
    iget-object v2, p0, LX/J8y;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v2, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 2652302
    iget-object v4, p0, LX/J8y;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-eqz v1, :cond_2

    const-string v2, "cursor"

    :goto_2
    invoke-interface {v4, v5, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 2652303
    if-eqz v1, :cond_0

    .line 2652304
    sget-object v2, LX/J8x;->SUMMARY:LX/J8x;

    if-ne p1, v2, :cond_1

    .line 2652305
    iget-object v4, p0, LX/J8y;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iget-object v2, p0, LX/J8y;->b:LX/J8z;

    invoke-virtual {v2}, LX/J8z;->b()Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "batch_rerun"

    :goto_3
    invoke-interface {v4, v5, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 2652306
    iget-object v4, p0, LX/J8y;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 2652307
    const/4 v2, 0x1

    move v2, v2

    .line 2652308
    if-eqz v2, :cond_4

    const-string v2, "model_cache"

    :goto_4
    invoke-interface {v4, v5, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 2652309
    :cond_1
    iget-object v4, p0, LX/J8y;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 2652310
    const/4 v2, 0x1

    move v2, v2

    .line 2652311
    if-eqz v2, :cond_5

    const-string v2, "memory_fast_path"

    :goto_5
    invoke-interface {v4, v5, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 2652312
    iget-object v2, p0, LX/J8y;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget-object v0, p0, LX/J8y;->b:LX/J8z;

    invoke-virtual {v0}, LX/J8z;->e()Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "stale_cache"

    :goto_6
    invoke-interface {v2, v4, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    goto :goto_1

    .line 2652313
    :pswitch_0
    iget-object v0, p0, LX/J8y;->b:LX/J8z;

    invoke-virtual {v0}, LX/J8z;->a()Z

    move-result v0

    move v1, v0

    goto :goto_0

    .line 2652314
    :cond_2
    const-string v2, "not_cursor"

    goto :goto_2

    .line 2652315
    :cond_3
    const-string v2, "not_batch_rerun"

    goto :goto_3

    .line 2652316
    :cond_4
    const-string v2, "no_model_cache"

    goto :goto_4

    .line 2652317
    :cond_5
    const-string v2, "not_memory_fast_path"

    goto :goto_5

    .line 2652318
    :cond_6
    const-string v0, "not_stale_cache"

    goto :goto_6

    .line 2652319
    :cond_7
    return-void

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method public final a(LX/J8x;Z)V
    .locals 3

    .prologue
    .line 2652286
    iget-object v0, p0, LX/J8y;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 2652287
    sget-object v1, LX/J8w;->a:[I

    invoke-virtual {p1}, LX/J8x;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2652288
    const/4 v1, 0x0

    :goto_0
    move v1, v1

    .line 2652289
    const/16 v2, 0x18

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IS)V

    .line 2652290
    invoke-static {p1}, LX/J8y;->d(LX/J8x;)I

    move-result v0

    .line 2652291
    if-eqz p2, :cond_0

    .line 2652292
    iget-object v1, p0, LX/J8y;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/16 v2, 0x1f

    invoke-interface {v1, v0, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2652293
    :goto_1
    return-void

    .line 2652294
    :cond_0
    iget-object v1, p0, LX/J8y;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/16 v2, 0x21

    invoke-interface {v1, v0, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    goto :goto_1

    .line 2652295
    :pswitch_0
    if-eqz p2, :cond_1

    const v1, 0x1a0031

    goto :goto_0

    :cond_1
    const v1, 0x1a0032

    goto :goto_0

    .line 2652296
    :pswitch_1
    if-eqz p2, :cond_2

    const v1, 0x1a0033

    goto :goto_0

    :cond_2
    const v1, 0x1a0034

    goto :goto_0

    .line 2652297
    :pswitch_2
    if-eqz p2, :cond_3

    const v1, 0x1a0035

    goto :goto_0

    :cond_3
    const v1, 0x1a0036

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 2652284
    iget-object v0, p0, LX/J8y;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x1a001e

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2652285
    return-void
.end method

.method public final b(LX/J8x;)V
    .locals 4

    .prologue
    .line 2652281
    invoke-static {p1}, LX/J8y;->f(LX/J8x;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 2652282
    iget-object v2, p0, LX/J8y;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v3, 0x4

    invoke-interface {v2, v0, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    goto :goto_0

    .line 2652283
    :cond_0
    return-void
.end method

.method public final c(LX/J8x;)V
    .locals 3

    .prologue
    .line 2652277
    invoke-static {p1}, LX/J8y;->e(LX/J8x;)I

    move-result v0

    .line 2652278
    iget-object v1, p0, LX/J8y;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v2, 0x2

    invoke-interface {v1, v0, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2652279
    invoke-virtual {p0}, LX/J8y;->b()V

    .line 2652280
    return-void
.end method
