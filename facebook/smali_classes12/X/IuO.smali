.class public abstract LX/IuO;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final m:Ljava/lang/String;


# instance fields
.field public final a:LX/3Ec;

.field public final b:LX/2Ox;

.field public final c:LX/2Oi;

.field public final d:LX/2P4;

.field public final e:LX/IuT;

.field public final f:LX/7V0;

.field public final g:LX/IuN;

.field public final h:LX/2NB;

.field public final i:Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;

.field public final j:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/messaging/tincan/outbound/Sender;",
            ">;"
        }
    .end annotation
.end field

.field public final k:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final l:LX/2PJ;

.field public final n:LX/FDp;

.field private final o:LX/2Ow;

.field public final p:LX/DoY;

.field public final q:LX/2P0;

.field public final r:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2626716
    const-class v0, LX/IuO;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/IuO;->m:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/3Ec;LX/2Ox;LX/2Oi;LX/2P4;LX/FDp;LX/IuT;LX/7V0;LX/2Ow;LX/IuN;LX/2NB;LX/DoY;LX/2P0;Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/2PJ;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threadkey/ThreadKeyFactory;",
            "LX/2Ox;",
            "LX/2Oi;",
            "LX/2P4;",
            "LX/FDp;",
            "LX/IuT;",
            "LX/7V0;",
            "LX/2Ow;",
            "LX/IuN;",
            "LX/2NB;",
            "LX/DoY;",
            "LX/2P0;",
            "Lcom/facebook/messaging/tincan/messenger/interfaces/PreKeyManager;",
            "LX/0Or",
            "<",
            "Lcom/facebook/messaging/tincan/outbound/Sender;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/2PJ;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2626678
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2626679
    iput-object p1, p0, LX/IuO;->a:LX/3Ec;

    .line 2626680
    iput-object p2, p0, LX/IuO;->b:LX/2Ox;

    .line 2626681
    iput-object p3, p0, LX/IuO;->c:LX/2Oi;

    .line 2626682
    iput-object p4, p0, LX/IuO;->d:LX/2P4;

    .line 2626683
    iput-object p5, p0, LX/IuO;->n:LX/FDp;

    .line 2626684
    iput-object p6, p0, LX/IuO;->e:LX/IuT;

    .line 2626685
    iput-object p7, p0, LX/IuO;->f:LX/7V0;

    .line 2626686
    iput-object p8, p0, LX/IuO;->o:LX/2Ow;

    .line 2626687
    iput-object p9, p0, LX/IuO;->g:LX/IuN;

    .line 2626688
    iput-object p10, p0, LX/IuO;->h:LX/2NB;

    .line 2626689
    iput-object p11, p0, LX/IuO;->p:LX/DoY;

    .line 2626690
    iput-object p12, p0, LX/IuO;->q:LX/2P0;

    .line 2626691
    iput-object p13, p0, LX/IuO;->i:Lcom/facebook/messaging/tincan/messenger/TincanPreKeyManager;

    .line 2626692
    iput-object p14, p0, LX/IuO;->j:LX/0Or;

    .line 2626693
    move-object/from16 v0, p15

    iput-object v0, p0, LX/IuO;->r:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2626694
    move-object/from16 v0, p16

    iput-object v0, p0, LX/IuO;->k:LX/0Or;

    .line 2626695
    move-object/from16 v0, p17

    iput-object v0, p0, LX/IuO;->l:LX/2PJ;

    .line 2626696
    return-void
.end method

.method public static d(LX/IuO;Lcom/facebook/messaging/model/messages/Message;)LX/IuS;
    .locals 4

    .prologue
    .line 2626697
    invoke-static {p1}, LX/IuO;->e(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2626698
    iget-object v0, p0, LX/IuO;->e:LX/IuT;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->k:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->J:Ljava/lang/Integer;

    .line 2626699
    new-instance v3, LX/Dpg;

    invoke-direct {v3, v1}, LX/Dpg;-><init>(Ljava/lang/Long;)V

    .line 2626700
    new-instance p0, LX/DpZ;

    invoke-direct {p0}, LX/DpZ;-><init>()V

    .line 2626701
    invoke-static {p0, v3}, LX/DpZ;->b(LX/DpZ;LX/Dpg;)V

    .line 2626702
    move-object v3, p0

    .line 2626703
    const/4 p0, 0x6

    invoke-static {v0, p0, v3, v2}, LX/IuT;->a(LX/IuT;ILX/DpZ;Ljava/lang/Integer;)LX/IuS;

    move-result-object v3

    move-object v0, v3

    .line 2626704
    :goto_0
    return-object v0

    .line 2626705
    :cond_0
    invoke-static {p1}, LX/IuO;->f(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2626706
    iget-object v0, p0, LX/IuO;->e:LX/IuT;

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->f:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->J:Ljava/lang/Integer;

    .line 2626707
    new-instance v3, LX/DpZ;

    invoke-direct {v3}, LX/DpZ;-><init>()V

    .line 2626708
    invoke-static {v3, v1}, LX/DpZ;->c(LX/DpZ;Ljava/lang/String;)V

    .line 2626709
    move-object v3, v3

    .line 2626710
    const/4 p0, 0x3

    invoke-static {v0, p0, v3, v2}, LX/IuT;->a(LX/IuT;ILX/DpZ;Ljava/lang/Integer;)LX/IuS;

    move-result-object v3

    move-object v0, v3

    .line 2626711
    goto :goto_0

    .line 2626712
    :cond_1
    invoke-static {p1}, LX/IuO;->i(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2626713
    iget-object v0, p0, LX/IuO;->e:LX/IuT;

    invoke-virtual {v0, p1}, LX/IuT;->a(Lcom/facebook/messaging/model/messages/Message;)LX/IuS;

    move-result-object v0

    goto :goto_0

    .line 2626714
    :cond_2
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Tried to send an unsupported message."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static e(Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 1

    .prologue
    .line 2626715
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->k:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static f(Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 1

    .prologue
    .line 2626721
    invoke-static {p0}, LX/IuO;->i(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static h(Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 2

    .prologue
    .line 2626717
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2626718
    invoke-virtual {v0}, Lcom/facebook/ui/media/attachments/MediaResource;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2626719
    const/4 v0, 0x1

    .line 2626720
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static i(Lcom/facebook/messaging/model/messages/Message;)Z
    .locals 1

    .prologue
    .line 2626677
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->i:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public abstract a(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/messages/Message;
.end method

.method public abstract a(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;
.end method

.method public a(JLcom/facebook/user/model/User;Ljava/lang/Integer;)Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .locals 7
    .param p4    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2626661
    const-string v4, "Unknown"

    .line 2626662
    const-string v5, "Unknown"

    .line 2626663
    const-string v6, "Unknown"

    .line 2626664
    if-eqz p3, :cond_0

    .line 2626665
    invoke-virtual {p3}, Lcom/facebook/user/model/User;->g()Ljava/lang/String;

    move-result-object v4

    .line 2626666
    invoke-virtual {p3}, Lcom/facebook/user/model/User;->h()Ljava/lang/String;

    move-result-object v5

    .line 2626667
    iget-object v0, p3, Lcom/facebook/user/model/User;->e:Lcom/facebook/user/model/Name;

    move-object v0, v0

    .line 2626668
    invoke-virtual {v0}, Lcom/facebook/user/model/Name;->f()Ljava/lang/String;

    move-result-object v6

    .line 2626669
    :cond_0
    iget-object v1, p0, LX/IuO;->b:LX/2Ox;

    move-wide v2, p1

    invoke-virtual/range {v1 .. v6}, LX/2Ox;->a(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2626670
    invoke-virtual {p0, p1, p2}, LX/IuO;->a(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 2626671
    iget-object v1, p0, LX/IuO;->b:LX/2Ox;

    invoke-virtual {v1, v0}, LX/2Ox;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2626672
    iget-object v1, p0, LX/IuO;->b:LX/2Ox;

    .line 2626673
    sget-object v2, LX/Doq;->a:LX/Doq;

    move-object v2, v2

    .line 2626674
    invoke-virtual {v2}, LX/Doq;->a()J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, LX/2Ox;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;J)V

    .line 2626675
    iget-object v1, p0, LX/IuO;->b:LX/2Ox;

    invoke-virtual {v1, v0, p4}, LX/2Ox;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/Integer;)V

    .line 2626676
    return-object v0
.end method

.method public abstract a(Lcom/facebook/messaging/model/messages/Message;LX/IuS;)V
.end method

.method public final a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2626660
    iget-object v0, p0, LX/IuO;->p:LX/DoY;

    invoke-virtual {v0, p1, p2}, LX/DoY;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 7

    .prologue
    .line 2626623
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6f7;

    move-result-object v0

    .line 2626624
    sget-object v1, LX/Doq;->a:LX/Doq;

    move-object v1, v1

    .line 2626625
    invoke-virtual {v1}, LX/Doq;->a()J

    move-result-wide v2

    .line 2626626
    iput-wide v2, v0, LX/6f7;->c:J

    .line 2626627
    move-object v1, v0

    .line 2626628
    iput-wide v2, v1, LX/6f7;->d:J

    .line 2626629
    move-object v1, v1

    .line 2626630
    invoke-virtual {v1}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    .line 2626631
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v2, v1, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->J:Ljava/lang/Integer;

    .line 2626632
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v5

    .line 2626633
    iget-object v4, p0, LX/IuO;->c:LX/2Oi;

    invoke-virtual {v4, v5}, LX/2Oi;->a(Lcom/facebook/user/model/UserKey;)Lcom/facebook/user/model/User;

    move-result-object v4

    .line 2626634
    if-nez v4, :cond_0

    .line 2626635
    iget-object v4, p0, LX/IuO;->n:LX/FDp;

    invoke-virtual {v4, v5}, LX/FDp;->a(Lcom/facebook/user/model/UserKey;)Lcom/facebook/user/model/User;

    move-result-object v4

    .line 2626636
    :cond_0
    iget-object v5, p0, LX/IuO;->d:LX/2P4;

    invoke-virtual {v5, v2, v3}, LX/2P4;->a(J)LX/0Px;

    move-result-object v5

    .line 2626637
    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_3

    .line 2626638
    const/4 v4, 0x0

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2626639
    :goto_0
    move-object v1, v4

    .line 2626640
    iput-object v1, v0, LX/6f7;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2626641
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->J:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->J:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-lez v1, :cond_1

    .line 2626642
    sget-object v1, LX/0SF;->a:LX/0SF;

    move-object v1, v1

    .line 2626643
    invoke-virtual {v1}, LX/0SF;->a()J

    move-result-wide v2

    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->J:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-long v4, v1

    add-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 2626644
    iput-object v1, v0, LX/6f7;->K:Ljava/lang/Long;

    .line 2626645
    move-object v1, v0

    .line 2626646
    invoke-virtual {v1}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    .line 2626647
    :cond_1
    invoke-virtual {v0}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 2626648
    invoke-static {p0, v0}, LX/IuO;->d(LX/IuO;Lcom/facebook/messaging/model/messages/Message;)LX/IuS;

    move-result-object v1

    .line 2626649
    iget-object v2, p0, LX/IuO;->h:LX/2NB;

    iget-object v3, v0, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    invoke-virtual {v2, v3}, LX/2NB;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    .line 2626650
    invoke-virtual {p0, v0, v1}, LX/IuO;->b(Lcom/facebook/messaging/model/messages/Message;LX/IuS;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 2626651
    iget-object v3, p0, LX/IuO;->g:LX/IuN;

    iget-object v1, v1, LX/IuS;->a:[B

    invoke-virtual {v3, v0, v1, v2}, LX/IuN;->a(Lcom/facebook/messaging/model/messages/Message;[BLjava/lang/String;)V

    .line 2626652
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v2, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v1, v2}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2626653
    iget-object v1, p0, LX/IuO;->o:LX/2Ow;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v3, v0, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2626654
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 2626655
    sget-object v5, LX/0aY;->j:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2626656
    const-string v5, "outdated_thread_key"

    invoke-virtual {v4, v5, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2626657
    const-string v5, "updated_thread_key"

    invoke-virtual {v4, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2626658
    invoke-static {v1, v4}, LX/2Ow;->a(LX/2Ow;Landroid/content/Intent;)V

    .line 2626659
    :cond_2
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0

    :cond_3
    invoke-virtual {p0, v2, v3, v4, v1}, LX/IuO;->a(JLcom/facebook/user/model/User;Ljava/lang/Integer;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v4

    goto :goto_0
.end method

.method public b(Lcom/facebook/messaging/model/messages/Message;LX/IuS;)Lcom/facebook/messaging/model/messages/Message;
    .locals 1

    .prologue
    .line 2626618
    invoke-static {p1}, LX/IuO;->e(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, LX/IuO;->f(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2626619
    :cond_0
    invoke-virtual {p0, p1, p2}, LX/IuO;->c(Lcom/facebook/messaging/model/messages/Message;LX/IuS;)V

    .line 2626620
    :goto_0
    return-object p1

    .line 2626621
    :cond_1
    invoke-static {p1}, LX/IuO;->i(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p1}, LX/IuO;->h(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 2626622
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final c(Lcom/facebook/messaging/model/messages/Message;LX/IuS;)V
    .locals 4

    .prologue
    .line 2626605
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->i(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2626606
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->J:Ljava/lang/Integer;

    const/4 v3, 0x0

    .line 2626607
    iget-object v1, p0, LX/IuO;->r:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/Dp9;->f:LX/0Tn;

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2626608
    iget-object v1, p0, LX/IuO;->e:LX/IuT;

    .line 2626609
    const/4 v2, 0x3

    const/4 v3, 0x0

    invoke-static {v1, v2, v3, v0}, LX/IuT;->a(LX/IuT;ILX/DpZ;Ljava/lang/Integer;)LX/IuS;

    move-result-object v2

    move-object p2, v2

    .line 2626610
    :cond_0
    :goto_0
    move-object v0, p2

    .line 2626611
    invoke-virtual {p0, p1, v0}, LX/IuO;->a(Lcom/facebook/messaging/model/messages/Message;LX/IuS;)V

    .line 2626612
    return-void

    .line 2626613
    :cond_1
    iget-object v1, p0, LX/IuO;->r:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/Dp9;->g:LX/0Tn;

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2626614
    iget-object v1, p0, LX/IuO;->e:LX/IuT;

    .line 2626615
    const-string v2, "BAD"

    invoke-static {v2}, LX/DpZ;->b(Ljava/lang/String;)LX/DpZ;

    move-result-object v2

    .line 2626616
    const/4 v3, 0x3

    invoke-static {v1, v3, v2, v0}, LX/IuT;->a(LX/IuT;ILX/DpZ;Ljava/lang/Integer;)LX/IuS;

    move-result-object v2

    move-object p2, v2

    .line 2626617
    goto :goto_0
.end method
