.class public LX/HOq;
.super LX/1CV;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9XE;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/967;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/967;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/9XE;",
            ">;",
            "LX/967;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2460994
    invoke-direct {p0}, LX/1CV;-><init>()V

    .line 2460995
    iput-object p1, p0, LX/HOq;->a:LX/0Ot;

    .line 2460996
    iput-object p2, p0, LX/HOq;->b:LX/0Ot;

    .line 2460997
    iput-object p3, p0, LX/HOq;->c:LX/967;

    .line 2460998
    return-void
.end method

.method public static a(LX/0QB;)LX/HOq;
    .locals 4

    .prologue
    .line 2460999
    new-instance v1, LX/HOq;

    const/16 v0, 0x259

    invoke-static {p0, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v0, 0x2b68

    invoke-static {p0, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {p0}, LX/967;->b(LX/0QB;)LX/967;

    move-result-object v0

    check-cast v0, LX/967;

    invoke-direct {v1, v2, v3, v0}, LX/HOq;-><init>(LX/0Ot;LX/0Ot;LX/967;)V

    .line 2461000
    move-object v0, v1

    .line 2461001
    return-object v0
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 7

    .prologue
    .line 2461002
    check-cast p1, LX/1Nc;

    const/4 v3, 0x0

    .line 2461003
    iget-object v0, p0, LX/HOq;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9XE;

    .line 2461004
    iget-object v1, p1, LX/1Nc;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2461005
    iget-boolean v2, p1, LX/1Nc;->h:Z

    move v2, v2

    .line 2461006
    const-string v4, "timeline_story_notify_me"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v5

    const-string v6, "native_timeline"

    invoke-static {v4, v1, v5, v6}, LX/17V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    .line 2461007
    iget-object v5, v0, LX/9XE;->a:LX/0Zb;

    invoke-interface {v5, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2461008
    iget-object v0, p0, LX/HOq;->c:LX/967;

    .line 2461009
    iget-object v1, p1, LX/1Nc;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v1, v1

    .line 2461010
    iget-boolean v2, p1, LX/1Nc;->h:Z

    move v2, v2

    .line 2461011
    new-instance v5, LX/HOp;

    invoke-direct {v5, p0, p1}, LX/HOp;-><init>(LX/HOq;LX/1Nc;)V

    move-object v4, v3

    invoke-virtual/range {v0 .. v5}, LX/967;->a(Lcom/facebook/feed/rows/core/props/FeedProps;ZLjava/lang/String;Ljava/lang/String;LX/1L9;)V

    .line 2461012
    return-void
.end method
