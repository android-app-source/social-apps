.class public LX/Hmz;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0TD;

.field public final b:LX/Hm4;


# direct methods
.method public constructor <init>(LX/Hm4;Ljava/util/concurrent/ExecutorService;)V
    .locals 1
    .param p1    # LX/Hm4;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/SingleThreadedExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2501200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2501201
    iput-object p1, p0, LX/Hmz;->b:LX/Hm4;

    .line 2501202
    invoke-static {p2}, LX/0TA;->a(Ljava/util/concurrent/ExecutorService;)LX/0TD;

    move-result-object v0

    iput-object v0, p0, LX/Hmz;->a:LX/0TD;

    .line 2501203
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;LX/Hm3;)V
    .locals 2
    .param p2    # LX/Hm3;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/Hm3;",
            ">;",
            "LX/Hm3;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2501204
    sget-object v0, LX/Hm3;->ERROR:LX/Hm3;

    if-ne p2, v0, :cond_0

    .line 2501205
    new-instance v0, LX/Hmx;

    iget-object v1, p0, LX/Hmz;->b:LX/Hm4;

    invoke-virtual {v1}, LX/Hm4;->f()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Hmx;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2501206
    :cond_0
    if-eqz p2, :cond_1

    invoke-interface {p1, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2501207
    :cond_1
    new-instance v0, LX/Hmy;

    invoke-direct {v0}, LX/Hmy;-><init>()V

    throw v0

    .line 2501208
    :cond_2
    return-void
.end method
