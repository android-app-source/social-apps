.class public final LX/I6I;
.super LX/1wH;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1wH",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic b:LX/I6L;


# direct methods
.method public constructor <init>(LX/I6L;)V
    .locals 0

    .prologue
    .line 2537423
    iput-object p1, p0, LX/I6I;->b:LX/I6L;

    invoke-direct {p0, p1}, LX/1wH;-><init>(LX/1SX;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Menu;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2537424
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2537425
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2537426
    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 2537427
    invoke-virtual {p0, v0}, LX/1wH;->b(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2537428
    sget-object v1, LX/04D;->EVENT_CHEVRON:LX/04D;

    invoke-virtual {v1}, LX/04D;->asString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1, p2, p3, v1}, LX/1wH;->a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;Ljava/lang/String;)V

    .line 2537429
    :cond_0
    invoke-virtual {p0, v0}, LX/1wH;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2537430
    invoke-virtual {p0, p1, p2, p3}, LX/1wH;->b(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V

    .line 2537431
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    if-eqz v1, :cond_9

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    if-eqz v1, :cond_9

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v3, 0x403827a

    if-ne v1, v3, :cond_9

    iget-object v1, p0, LX/I6I;->b:LX/I6L;

    iget-object v1, v1, LX/I6L;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Bkv;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/Bkv;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 2537432
    if-eqz v1, :cond_2

    .line 2537433
    iget-object v1, p0, LX/I6I;->b:LX/I6L;

    iget-object v1, v1, LX/I6L;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Bkv;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, LX/Bkv;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    .line 2537434
    if-eqz v3, :cond_8

    const v1, 0x7f0837ff

    :goto_1
    invoke-interface {p1, v1}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 2537435
    new-instance v4, LX/I6E;

    invoke-direct {v4, p0, v3, v0, v2}, LX/I6E;-><init>(LX/I6I;ZLcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V

    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2537436
    iget-object v3, p0, LX/I6I;->b:LX/I6L;

    .line 2537437
    const v4, 0x7f020781

    move v4, v4

    .line 2537438
    invoke-virtual {v3, v1, v4, v0}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 2537439
    :cond_2
    iget-object v1, p0, LX/I6I;->b:LX/I6L;

    invoke-virtual {v1, v0}, LX/1SX;->g(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2537440
    const v1, 0x7f08106b

    invoke-interface {p1, v1}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 2537441
    new-instance v3, LX/I6F;

    invoke-direct {v3, p0, v0, v2}, LX/I6F;-><init>(LX/I6I;Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2537442
    iget-object v3, p0, LX/I6I;->b:LX/I6L;

    .line 2537443
    const v4, 0x7f020952

    move v4, v4

    .line 2537444
    invoke-virtual {v3, v1, v4, v0}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 2537445
    :cond_3
    invoke-static {v0}, LX/1Sn;->d(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v3

    move v1, v3

    .line 2537446
    if-eqz v1, :cond_4

    .line 2537447
    const v1, 0x7f08106d

    invoke-interface {p1, v1}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 2537448
    iget-object v3, p0, LX/I6I;->b:LX/I6L;

    .line 2537449
    const v4, 0x7f0207fd

    move v4, v4

    .line 2537450
    invoke-virtual {v3, v1, v4, v0}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 2537451
    new-instance v3, LX/I6G;

    invoke-direct {v3, p0, v0, v2}, LX/I6G;-><init>(LX/I6I;Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2537452
    :cond_4
    invoke-virtual {p0, p2}, LX/1wH;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2537453
    iget-object v1, p0, LX/I6I;->b:LX/I6L;

    .line 2537454
    invoke-virtual {v1, p1, p2, p3}, LX/1SX;->a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V

    .line 2537455
    :cond_5
    iget-object v1, p0, LX/I6I;->b:LX/I6L;

    invoke-virtual {v1, v0}, LX/1SX;->i(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2537456
    const v1, 0x7f08104c

    invoke-interface {p1, v1}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 2537457
    new-instance v3, LX/I6H;

    invoke-direct {v3, p0, p2, v2}, LX/I6H;-><init>(LX/I6I;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;)V

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2537458
    iget-object v3, p0, LX/I6I;->b:LX/I6L;

    .line 2537459
    const v4, 0x7f020a0b

    move v4, v4

    .line 2537460
    invoke-virtual {v3, v1, v4, v0}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 2537461
    :cond_6
    invoke-virtual {p0, p1, p2}, LX/1wH;->a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2537462
    invoke-virtual {p0, p2}, LX/1wH;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2537463
    invoke-virtual {p0, p1, p2, v2}, LX/1wH;->a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;)V

    .line 2537464
    :cond_7
    return-void

    .line 2537465
    :cond_8
    const v1, 0x7f0837fe

    goto/16 :goto_1

    :cond_9
    const/4 v1, 0x0

    goto/16 :goto_0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2537466
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2537467
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2537468
    invoke-virtual {p0, p1}, LX/1wH;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/I6I;->b:LX/I6L;

    invoke-virtual {v1, v0}, LX/1SX;->i(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/I6I;->b:LX/I6L;

    invoke-virtual {v1, v0}, LX/1SX;->g(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2537469
    invoke-static {v0}, LX/1Sn;->d(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result p1

    move v1, p1

    .line 2537470
    if-nez v1, :cond_0

    invoke-virtual {p0, v0}, LX/1wH;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v0}, LX/1wH;->d(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
