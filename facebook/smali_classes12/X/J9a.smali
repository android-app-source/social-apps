.class public LX/J9a;
.super LX/3tK;
.source ""


# instance fields
.field public final j:LX/J94;

.field public final k:LX/JDL;

.field public final l:LX/JCx;

.field public final m:LX/9lP;

.field public final n:LX/JDA;

.field private final o:LX/J93;

.field public final p:Landroid/view/LayoutInflater;

.field private final q:LX/J8p;

.field private r:Ljava/lang/String;

.field private s:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

.field public t:LX/J9l;

.field public u:Z


# direct methods
.method public constructor <init>(LX/9lP;Landroid/content/Context;Landroid/view/LayoutInflater;LX/J8p;LX/J94;LX/JDL;LX/JDA;LX/JCx;LX/J93;)V
    .locals 2
    .param p1    # LX/9lP;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Landroid/view/LayoutInflater;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/J8p;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2652980
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p2, v0, v1}, LX/3tK;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    .line 2652981
    iput-object p7, p0, LX/J9a;->n:LX/JDA;

    .line 2652982
    iput-object p5, p0, LX/J9a;->j:LX/J94;

    .line 2652983
    iput-object p6, p0, LX/J9a;->k:LX/JDL;

    .line 2652984
    iput-object p1, p0, LX/J9a;->m:LX/9lP;

    .line 2652985
    iput-object p8, p0, LX/J9a;->l:LX/JCx;

    .line 2652986
    iput-object p9, p0, LX/J9a;->o:LX/J93;

    .line 2652987
    iput-object p3, p0, LX/J9a;->p:Landroid/view/LayoutInflater;

    .line 2652988
    iput-object p4, p0, LX/J9a;->q:LX/J8p;

    .line 2652989
    return-void
.end method

.method private a(I)LX/J9V;
    .locals 2

    .prologue
    .line 2653078
    invoke-virtual {p0}, LX/3tK;->a()Landroid/database/Cursor;

    move-result-object v0

    check-cast v0, LX/9J9;

    .line 2653079
    invoke-virtual {v0}, LX/9J9;->getCount()I

    move-result v1

    if-lt p1, v1, :cond_0

    .line 2653080
    sget-object v0, LX/J9V;->LOADING_INDICATOR:LX/J9V;

    .line 2653081
    :goto_0
    return-object v0

    .line 2653082
    :cond_0
    if-nez p1, :cond_1

    .line 2653083
    sget-object v0, LX/J9V;->SECTION_HEADER:LX/J9V;

    goto :goto_0

    .line 2653084
    :cond_1
    const/4 v1, 0x1

    if-ne p1, v1, :cond_2

    .line 2653085
    sget-object v0, LX/J9V;->COLLECTION_HEADER:LX/J9V;

    goto :goto_0

    .line 2653086
    :cond_2
    invoke-virtual {v0}, LX/9J9;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_3

    .line 2653087
    sget-object v0, LX/J9V;->SUB_ADAPTER_ITEM_BOTTOM:LX/J9V;

    goto :goto_0

    .line 2653088
    :cond_3
    sget-object v0, LX/J9V;->SUB_ADAPTER_ITEM_MIDDLE:LX/J9V;

    goto :goto_0
.end method

.method private a(Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;)LX/JD9;
    .locals 18

    .prologue
    .line 2653071
    invoke-static/range {p1 .. p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2653072
    const/4 v14, 0x0

    .line 2653073
    if-eqz p1, :cond_0

    .line 2653074
    move-object/from16 v0, p0

    iget-object v2, v0, LX/J9a;->s:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/J9a;->t:LX/J9l;

    invoke-interface {v3}, LX/J9k;->e()Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v2, v3}, LX/J93;->a(LX/JBL;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;)Ljava/lang/String;

    move-result-object v14

    .line 2653075
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->l()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;->a()I

    move-result v1

    if-nez v1, :cond_1

    const/4 v5, 0x0

    .line 2653076
    :goto_0
    new-instance v1, LX/JD9;

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->mW_()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->p()Ljava/lang/String;

    move-result-object v7

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v15, v0, LX/J9a;->s:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    move-object/from16 v0, p0

    iget-object v0, v0, LX/J9a;->m:LX/9lP;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    move-object/from16 v8, p1

    invoke-direct/range {v1 .. v17}, LX/JD9;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/1Fb;Ljava/lang/String;LX/JBL;Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionMediasetModel;Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineAppSectionsGraphQLModels$CollectionsAppSectionRequestableFieldsModel;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;LX/9lP;Z)V

    return-object v1

    .line 2653077
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->l()Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$CollectionItemConnectionWithFieldsModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    .line 2653061
    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    invoke-direct {p0, v0}, LX/J9a;->a(I)LX/J9V;

    move-result-object v0

    .line 2653062
    sget-object v1, LX/J9Z;->a:[I

    invoke-virtual {v0}, LX/J9V;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2653063
    iget-object v1, p0, LX/J9a;->t:LX/J9l;

    iget-object v2, p0, LX/3tK;->d:Landroid/content/Context;

    invoke-interface {v1, v2, v0, p3}, LX/J9k;->a(Landroid/content/Context;LX/J9V;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    .line 2653064
    :pswitch_0
    new-instance v0, Lcom/facebook/timeline/aboutpage/views/header/CollectionSectionHeader;

    iget-object v1, p0, LX/3tK;->d:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/facebook/timeline/aboutpage/views/header/CollectionSectionHeader;-><init>(Landroid/content/Context;)V

    .line 2653065
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 2653066
    const v2, 0x7f0b249c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    const v3, 0x7f0b249a

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 2653067
    const v3, 0x7f0b248d

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    const v4, 0x7f0b249c

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v1, v4, v2}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_0

    .line 2653068
    :pswitch_1
    new-instance v0, LX/JDe;

    iget-object v1, p0, LX/3tK;->d:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/JDe;-><init>(Landroid/content/Context;)V

    .line 2653069
    iget-object v2, p0, LX/J9a;->p:Landroid/view/LayoutInflater;

    invoke-static {v0, v2}, LX/J94;->b(Landroid/view/View;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 2653070
    :pswitch_2
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Landroid/view/View;Landroid/database/Cursor;)V
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2653025
    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    .line 2653026
    invoke-direct {p0, v1}, LX/J9a;->a(I)LX/J9V;

    move-result-object v2

    move-object v1, p2

    .line 2653027
    check-cast v1, LX/9J9;

    invoke-virtual {v1}, LX/9J9;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    .line 2653028
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2653029
    :try_start_0
    sget-object v3, LX/J9Z;->a:[I

    invoke-virtual {v2}, LX/J9V;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    .line 2653030
    iget-object v2, p0, LX/J9a;->t:LX/J9l;

    instance-of v2, v2, LX/J9o;

    if-eqz v2, :cond_2

    .line 2653031
    check-cast v1, Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;

    iget-object v2, p0, LX/J9a;->s:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    invoke-static {v1, v2}, LX/JDK;->a(Lcom/facebook/timeline/aboutpage/protocol/CollectionsHelperGraphQLModels$AppCollectionItemModel;Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;)LX/JDK;

    move-result-object v1

    .line 2653032
    :goto_0
    iget-object v2, p0, LX/J9a;->t:LX/J9l;

    iget-object v3, p0, LX/J9a;->m:LX/9lP;

    invoke-interface {v2, v1, p1, v3}, LX/J9k;->a(Ljava/lang/Object;Landroid/view/View;LX/9lP;)V

    .line 2653033
    :cond_0
    :goto_1
    return-void

    .line 2653034
    :pswitch_0
    check-cast v1, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel;

    .line 2653035
    check-cast p1, Lcom/facebook/timeline/aboutpage/views/header/CollectionSectionHeader;

    invoke-virtual {p1, v1}, Lcom/facebook/timeline/aboutpage/views/header/CollectionSectionHeader;->a(LX/JAc;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 2653036
    :catch_0
    move-exception v1

    .line 2653037
    const-class v2, LX/J9a;

    const-string v3, "Error binding view at position %d"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v1, v3, v4}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 2653038
    :pswitch_1
    :try_start_1
    check-cast v1, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;

    .line 2653039
    invoke-direct {p0, v1}, LX/J9a;->a(Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;)LX/JD9;

    move-result-object v6

    .line 2653040
    invoke-static {p1}, LX/J94;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v2

    check-cast v2, LX/JDe;

    .line 2653041
    const/4 v7, 0x0

    iget-object v3, v6, LX/JD9;->m:Ljava/lang/String;

    if-eqz v3, :cond_1

    move v3, v4

    :goto_2
    const/4 v8, 0x0

    invoke-virtual {v2, v6, v7, v3, v8}, LX/JDe;->a(LX/JD9;ZZZ)V

    .line 2653042
    iget-object v2, p0, LX/J9a;->r:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 2653043
    iget-object v2, p0, LX/J9a;->q:LX/J8p;

    iget-object v3, p0, LX/J9a;->m:LX/9lP;

    .line 2653044
    iget-object v0, v3, LX/9lP;->a:Ljava/lang/String;

    move-object v3, v0

    .line 2653045
    iget-object v6, p0, LX/J9a;->m:LX/9lP;

    invoke-static {v6}, LX/J8p;->a(LX/9lP;)LX/9lQ;

    move-result-object v6

    iget-object v7, p0, LX/J9a;->r:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v6, v7, v1}, LX/J8p;->a(Ljava/lang/String;LX/9lQ;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    move v3, v5

    .line 2653046
    goto :goto_2

    .line 2653047
    :pswitch_2
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1

    .line 2653048
    :cond_2
    move-object v0, p2

    check-cast v0, LX/9J9;

    move-object v1, v0

    .line 2653049
    iget v0, v1, LX/9J9;->b:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_3

    .line 2653050
    invoke-virtual {v1}, LX/9J9;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    .line 2653051
    :goto_3
    move-object v1, v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 2653052
    goto :goto_0

    .line 2653053
    :cond_3
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2653054
    iget v0, v1, LX/9J9;->b:I

    .line 2653055
    iget-object v3, v1, LX/9J9;->c:LX/2nf;

    invoke-static {v1}, LX/9J9;->f(LX/9J9;)I

    move-result v6

    invoke-interface {v3, v6}, LX/2nf;->moveToPosition(I)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 2653056
    :cond_4
    iget-object v3, v1, LX/9J9;->c:LX/2nf;

    invoke-interface {v3}, LX/2nf;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v3

    .line 2653057
    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2653058
    add-int/lit8 v0, v0, -0x1

    .line 2653059
    if-lez v0, :cond_5

    iget-object v3, v1, LX/9J9;->c:LX/2nf;

    invoke-interface {v3}, LX/2nf;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_4

    .line 2653060
    :cond_5
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 10

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x1

    .line 2653006
    if-nez p1, :cond_0

    .line 2653007
    const/4 v0, 0x0

    invoke-super {p0, v0}, LX/3tK;->b(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    .line 2653008
    :goto_0
    return-object v0

    .line 2653009
    :cond_0
    iget-object v0, p0, LX/J9a;->t:LX/J9l;

    if-nez v0, :cond_1

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lt v0, v3, :cond_1

    .line 2653010
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    move-object v0, p1

    .line 2653011
    check-cast v0, LX/2nf;

    invoke-interface {v0}, LX/2nf;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel;

    .line 2653012
    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel;->mV_()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, LX/J9a;->r:Ljava/lang/String;

    .line 2653013
    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionsGraphQLModels$FetchTimelineSingleCollectionViewQueryModel;->e()Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    move-result-object v0

    iput-object v0, p0, LX/J9a;->s:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    .line 2653014
    invoke-interface {p1, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    move-object v0, p1

    .line 2653015
    check-cast v0, LX/2nf;

    invoke-interface {v0}, LX/2nf;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;

    .line 2653016
    invoke-virtual {v0}, Lcom/facebook/timeline/aboutpage/protocol/FetchTimelineCollectionItemsGraphQLModels$CollectionWithItemsAndSuggestionsModel;->d()LX/0Px;

    move-result-object v5

    invoke-static {v5}, LX/JCx;->a(Ljava/lang/Iterable;)Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    move-result-object v9

    .line 2653017
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->LIST:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    invoke-virtual {v9, v4}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2653018
    new-instance v4, LX/J9o;

    iget-object v5, p0, LX/J9a;->k:LX/JDL;

    iget-object v6, p0, LX/J9a;->j:LX/J94;

    iget-object v7, p0, LX/J9a;->m:LX/9lP;

    iget-object v8, p0, LX/J9a;->p:Landroid/view/LayoutInflater;

    invoke-direct/range {v4 .. v9}, LX/J9o;-><init>(LX/JDL;LX/J94;LX/9lP;Landroid/view/LayoutInflater;Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;)V

    iput-object v4, p0, LX/J9a;->t:LX/J9l;

    .line 2653019
    :cond_1
    :goto_1
    iget-object v0, p0, LX/J9a;->t:LX/J9l;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/J9a;->t:LX/J9l;

    invoke-interface {v0}, LX/J9k;->b()I

    move-result v0

    .line 2653020
    :goto_2
    new-instance v1, LX/9J9;

    check-cast p1, LX/2nf;

    invoke-direct {v1, p1, v3, v0}, LX/9J9;-><init>(LX/2nf;II)V

    invoke-super {p0, v1}, LX/3tK;->b(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    :cond_2
    move v0, v1

    .line 2653021
    goto :goto_2

    .line 2653022
    :cond_3
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->GRID:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    invoke-virtual {v9, v4}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;

    invoke-virtual {v9, v4}, Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2653023
    :cond_4
    new-instance v4, LX/J9q;

    iget-object v5, p0, LX/J9a;->n:LX/JDA;

    iget-object v6, p0, LX/J9a;->j:LX/J94;

    iget-object v7, p0, LX/J9a;->p:Landroid/view/LayoutInflater;

    invoke-direct {v4, v5, v6, v9, v7}, LX/J9q;-><init>(LX/JDA;LX/J94;Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;Landroid/view/LayoutInflater;)V

    iput-object v4, p0, LX/J9a;->t:LX/J9l;

    goto :goto_1

    .line 2653024
    :cond_5
    new-instance v4, LX/J9m;

    iget-object v5, p0, LX/J9a;->n:LX/JDA;

    iget-object v6, p0, LX/J9a;->j:LX/J94;

    iget-object v7, p0, LX/J9a;->p:Landroid/view/LayoutInflater;

    invoke-direct {v4, v5, v6, v9, v7}, LX/J9m;-><init>(LX/JDA;LX/J94;Lcom/facebook/graphql/enums/GraphQLTimelineAppCollectionStyle;Landroid/view/LayoutInflater;)V

    iput-object v4, p0, LX/J9a;->t:LX/J9l;

    goto :goto_1
.end method

.method public final getCount()I
    .locals 2

    .prologue
    .line 2653000
    iget-object v0, p0, LX/J9a;->t:LX/J9l;

    if-nez v0, :cond_1

    .line 2653001
    const/4 v0, 0x0

    .line 2653002
    :cond_0
    :goto_0
    return v0

    .line 2653003
    :cond_1
    invoke-super {p0}, LX/3tK;->getCount()I

    move-result v0

    .line 2653004
    iget-boolean v1, p0, LX/J9a;->u:Z

    if-eqz v1, :cond_0

    .line 2653005
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2652999
    invoke-direct {p0, p1}, LX/J9a;->a(I)LX/J9V;

    move-result-object v0

    invoke-virtual {v0}, LX/J9V;->ordinal()I

    move-result v0

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2652991
    invoke-virtual {p0}, LX/3tK;->a()Landroid/database/Cursor;

    move-result-object v0

    check-cast v0, LX/2nf;

    .line 2652992
    invoke-interface {v0}, LX/2nf;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_2

    .line 2652993
    invoke-direct {p0, p1}, LX/J9a;->a(I)LX/J9V;

    move-result-object v0

    sget-object v2, LX/J9V;->LOADING_INDICATOR:LX/J9V;

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2652994
    if-nez p2, :cond_0

    .line 2652995
    iget-object v0, p0, LX/J9a;->p:Landroid/view/LayoutInflater;

    const v2, 0x7f0314f0

    invoke-virtual {v0, v2, p3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 2652996
    :cond_0
    :goto_1
    return-object p2

    :cond_1
    move v0, v1

    .line 2652997
    goto :goto_0

    .line 2652998
    :cond_2
    invoke-super {p0, p1, p2, p3}, LX/3tK;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto :goto_1
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2652990
    sget v0, LX/J9V;->NUM_VIEW_TYPES:I

    return v0
.end method
