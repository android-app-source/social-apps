.class public LX/Hur;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0iK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0io;",
        ":",
        "LX/0j0;",
        ":",
        "LX/0j2;",
        ":",
        "LX/0iq;",
        ":",
        "LX/0jD;",
        "DerivedData::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicDataProviders$ProvidesIsTagExpansionPillSupported;",
        "Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;>",
        "Ljava/lang/Object;",
        "LX/0iK",
        "<TModelData;TDerivedData;>;"
    }
.end annotation


# instance fields
.field public final a:Landroid/content/res/Resources;

.field public final b:LX/0wM;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7l0;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/resources/ui/FbTextView;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/user/model/User;

.field public final g:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/composer/privacy/controller/SelectablePrivacyPillViewController$PillClickedListener;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0gd;

.field public final i:LX/8Sa;

.field private final j:Landroid/view/View$OnClickListener;

.field public k:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

.field private l:Z


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/0wM;LX/0Ot;LX/0gd;Lcom/facebook/user/model/User;LX/0il;LX/0zw;LX/HqQ;LX/8Sa;)V
    .locals 1
    .param p5    # Lcom/facebook/user/model/User;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .param p6    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/0zw;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # LX/HqQ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "LX/0wM;",
            "LX/0Ot",
            "<",
            "LX/7l0;",
            ">;",
            "LX/0gd;",
            "Lcom/facebook/user/model/User;",
            "TServices;",
            "LX/0zw",
            "<",
            "Lcom/facebook/resources/ui/FbTextView;",
            ">;",
            "Lcom/facebook/composer/privacy/controller/SelectablePrivacyPillViewController$PillClickedListener;",
            "LX/8Sa;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2517929
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2517930
    new-instance v0, LX/Hup;

    invoke-direct {v0, p0}, LX/Hup;-><init>(LX/Hur;)V

    iput-object v0, p0, LX/Hur;->j:Landroid/view/View$OnClickListener;

    .line 2517931
    iput-object p1, p0, LX/Hur;->a:Landroid/content/res/Resources;

    .line 2517932
    iput-object p2, p0, LX/Hur;->b:LX/0wM;

    .line 2517933
    iput-object p3, p0, LX/Hur;->c:LX/0Ot;

    .line 2517934
    iput-object p4, p0, LX/Hur;->h:LX/0gd;

    .line 2517935
    iput-object p5, p0, LX/Hur;->f:Lcom/facebook/user/model/User;

    .line 2517936
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p6}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/Hur;->e:Ljava/lang/ref/WeakReference;

    .line 2517937
    iput-object p7, p0, LX/Hur;->d:LX/0zw;

    .line 2517938
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p8}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/Hur;->g:Ljava/lang/ref/WeakReference;

    .line 2517939
    iput-object p9, p0, LX/Hur;->i:LX/8Sa;

    .line 2517940
    invoke-direct {p0}, LX/Hur;->b()V

    .line 2517941
    return-void
.end method

.method private b()V
    .locals 13

    .prologue
    const/4 v7, 0x0

    .line 2517942
    iget-object v0, p0, LX/Hur;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, LX/0il;

    .line 2517943
    invoke-interface {v6}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0iq;

    invoke-interface {v0}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v8

    .line 2517944
    iget-boolean v0, v8, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->d:Z

    if-nez v0, :cond_0

    iget-object v0, v8, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-eqz v0, :cond_0

    iget-object v0, v8, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 2517945
    iget-object v1, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v1

    .line 2517946
    if-nez v0, :cond_2

    .line 2517947
    :cond_0
    iget-object v0, p0, LX/Hur;->d:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->c()V

    .line 2517948
    const/4 v0, 0x0

    iput-object v0, p0, LX/Hur;->k:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    .line 2517949
    iput-boolean v7, p0, LX/Hur;->l:Z

    .line 2517950
    :cond_1
    :goto_0
    return-void

    .line 2517951
    :cond_2
    iget-object v0, p0, LX/Hur;->f:Lcom/facebook/user/model/User;

    .line 2517952
    iget-object v1, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v1

    .line 2517953
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iget-object v2, p0, LX/Hur;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/7l0;

    invoke-interface {v6}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0io;

    check-cast v3, LX/0jD;

    invoke-interface {v3}, LX/0jD;->getTaggedUsers()LX/0Px;

    move-result-object v3

    invoke-interface {v6}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0io;

    check-cast v4, LX/0j2;

    invoke-interface {v4}, LX/0j2;->getTextWithEntities()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-interface {v6}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0io;

    invoke-interface {v5}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v5

    invoke-static/range {v0 .. v5}, LX/7ky;->a(JLX/7l0;LX/0Px;Lcom/facebook/graphql/model/GraphQLTextWithEntities;LX/0Px;)LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    move v1, v0

    .line 2517954
    :goto_1
    iget-boolean v0, p0, LX/Hur;->l:Z

    if-ne v1, v0, :cond_3

    iget-object v2, p0, LX/Hur;->k:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    invoke-interface {v6}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0iq;

    invoke-interface {v0}, LX/0iq;->q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    if-eq v2, v0, :cond_1

    .line 2517955
    :cond_3
    iput-boolean v1, p0, LX/Hur;->l:Z

    .line 2517956
    iput-object v8, p0, LX/Hur;->k:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    .line 2517957
    iget-object v0, p0, LX/Hur;->d:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/Hur;->j:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2517958
    const v8, -0x6e685d

    const/4 v3, 0x0

    .line 2517959
    invoke-static {p0}, LX/Hur;->d(LX/Hur;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2517960
    iget-object v0, p0, LX/Hur;->d:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2517961
    iget-object v0, p0, LX/Hur;->k:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iget-object v0, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 2517962
    iget-object v1, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v1

    .line 2517963
    invoke-static {v0}, LX/2cA;->c(LX/1oS;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2517964
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->FRIENDS_EXCEPT_ACQUAINTANCES:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    sget-object v2, LX/8SZ;->PILL:LX/8SZ;

    invoke-static {v1, v2}, LX/8Sa;->a(Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;LX/8SZ;)I

    move-result v0

    .line 2517965
    :goto_2
    move v0, v0

    .line 2517966
    if-eqz v0, :cond_6

    .line 2517967
    iget-object v1, p0, LX/Hur;->b:LX/0wM;

    iget-object v2, p0, LX/Hur;->a:Landroid/content/res/Resources;

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0, v8}, LX/0wM;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    move-object v2, v0

    .line 2517968
    :goto_3
    iget-object v0, p0, LX/Hur;->b:LX/0wM;

    iget-object v1, p0, LX/Hur;->a:Landroid/content/res/Resources;

    const v4, 0x7f020319

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1, v8}, LX/0wM;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 2517969
    iget-object v0, p0, LX/Hur;->d:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2517970
    iget-object v1, p0, LX/Hur;->k:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iget-object v1, v1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 2517971
    iget-object v4, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v1, v4

    .line 2517972
    invoke-static {v1}, LX/2cA;->c(LX/1oS;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 2517973
    invoke-interface {v1}, LX/1oS;->x_()LX/0Px;

    move-result-object v1

    .line 2517974
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_b

    .line 2517975
    iget-object v4, p0, LX/Hur;->a:Landroid/content/res/Resources;

    const v6, 0x7f0812e4

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2517976
    :goto_4
    move-object v1, v4

    .line 2517977
    :goto_5
    move-object v4, v1

    .line 2517978
    invoke-static {p0}, LX/Hur;->g(LX/Hur;)I

    move-result v6

    .line 2517979
    if-eqz v6, :cond_5

    iget-object v1, p0, LX/Hur;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0il;

    check-cast v1, LX/0ik;

    invoke-interface {v1}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2zG;

    invoke-virtual {v1}, LX/2zG;->B()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_5

    .line 2517980
    iget-object v1, p0, LX/Hur;->b:LX/0wM;

    iget-object v7, p0, LX/Hur;->a:Landroid/content/res/Resources;

    invoke-virtual {v7, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v1, v6, v8}, LX/0wM;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2517981
    new-instance v6, LX/ASa;

    invoke-static {v1, v5}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    iget-object v5, p0, LX/Hur;->a:Landroid/content/res/Resources;

    const v7, 0x7f0b0ca5

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-direct {v6, v1, v5}, LX/ASa;-><init>(LX/0Px;I)V

    .line 2517982
    invoke-static {v0, v2, v3, v6, v3}, LX/4lM;->a(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2517983
    const-string v1, ","

    invoke-virtual {v4, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2517984
    :goto_6
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2517985
    goto/16 :goto_0

    :cond_4
    move v1, v7

    .line 2517986
    goto/16 :goto_1

    .line 2517987
    :cond_5
    invoke-static {v0, v2, v3, v5, v3}, LX/4lM;->a(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    move-object v1, v4

    goto :goto_6

    :cond_6
    move-object v2, v3

    goto/16 :goto_3

    .line 2517988
    :cond_7
    invoke-static {v0}, LX/2cA;->e(LX/1oS;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 2517989
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->CUSTOM:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    sget-object v2, LX/8SZ;->PILL:LX/8SZ;

    invoke-static {v1, v2}, LX/8Sa;->a(Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;LX/8SZ;)I

    move-result v0

    goto/16 :goto_2

    .line 2517990
    :cond_8
    invoke-static {v0}, LX/2cA;->a(LX/1oV;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v0

    sget-object v2, LX/8SZ;->PILL:LX/8SZ;

    invoke-static {v0, v2}, LX/8Sa;->a(Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;LX/8SZ;)I

    move-result v0

    goto/16 :goto_2

    .line 2517991
    :cond_9
    invoke-static {v1}, LX/2cA;->e(LX/1oS;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 2517992
    invoke-interface {v1}, LX/1oS;->j()LX/0Px;

    move-result-object v1

    .line 2517993
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_e

    .line 2517994
    iget-object v4, p0, LX/Hur;->a:Landroid/content/res/Resources;

    const v6, 0x7f0812ef

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2517995
    :goto_7
    move-object v1, v4

    .line 2517996
    goto/16 :goto_5

    .line 2517997
    :cond_a
    invoke-interface {v1}, LX/1oS;->d()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_5

    :cond_b
    const/4 v9, 0x3

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 2517998
    if-eqz v1, :cond_c

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_d

    .line 2517999
    :cond_c
    iget-object v4, p0, LX/Hur;->a:Landroid/content/res/Resources;

    const v6, 0x7f0812d5

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2518000
    :goto_8
    move-object v4, v4

    .line 2518001
    goto/16 :goto_4

    .line 2518002
    :cond_d
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 2518003
    iget-object v6, p0, LX/Hur;->a:Landroid/content/res/Resources;

    const v7, 0x7f0812ea

    new-array v9, v9, [Ljava/lang/Object;

    invoke-interface {v1, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2cp;

    invoke-interface {v4}, LX/2cp;->d()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v9, v10

    invoke-interface {v1, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2cp;

    invoke-interface {v4}, LX/2cp;->d()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v9, v11

    invoke-interface {v1, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2cp;

    invoke-interface {v4}, LX/2cp;->d()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v9, v12

    invoke-virtual {v6, v7, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_8

    .line 2518004
    :pswitch_0
    iget-object v6, p0, LX/Hur;->a:Landroid/content/res/Resources;

    const v7, 0x7f0812e7

    new-array v9, v11, [Ljava/lang/Object;

    invoke-interface {v1, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2cp;

    invoke-interface {v4}, LX/2cp;->d()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v9, v10

    invoke-virtual {v6, v7, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_8

    .line 2518005
    :pswitch_1
    iget-object v6, p0, LX/Hur;->a:Landroid/content/res/Resources;

    const v7, 0x7f0812e8

    new-array v9, v12, [Ljava/lang/Object;

    invoke-interface {v1, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2cp;

    invoke-interface {v4}, LX/2cp;->d()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v9, v10

    invoke-interface {v1, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2cp;

    invoke-interface {v4}, LX/2cp;->d()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v9, v11

    invoke-virtual {v6, v7, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_8

    .line 2518006
    :pswitch_2
    iget-object v6, p0, LX/Hur;->a:Landroid/content/res/Resources;

    const v7, 0x7f0812e9

    new-array v9, v9, [Ljava/lang/Object;

    invoke-interface {v1, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2cp;

    invoke-interface {v4}, LX/2cp;->d()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v9, v10

    invoke-interface {v1, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2cp;

    invoke-interface {v4}, LX/2cp;->d()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v9, v11

    invoke-interface {v1, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2cp;

    invoke-interface {v4}, LX/2cp;->d()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v9, v12

    invoke-virtual {v6, v7, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_8

    :cond_e
    const/4 v9, 0x3

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 2518007
    if-eqz v1, :cond_f

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_10

    .line 2518008
    :cond_f
    iget-object v4, p0, LX/Hur;->a:Landroid/content/res/Resources;

    const v6, 0x7f0812d5

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2518009
    :goto_9
    move-object v4, v4

    .line 2518010
    goto/16 :goto_7

    .line 2518011
    :cond_10
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    .line 2518012
    iget-object v6, p0, LX/Hur;->a:Landroid/content/res/Resources;

    const v7, 0x7f0812f7

    new-array v9, v9, [Ljava/lang/Object;

    invoke-interface {v1, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2cp;

    invoke-interface {v4}, LX/2cp;->d()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v9, v10

    invoke-interface {v1, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2cp;

    invoke-interface {v4}, LX/2cp;->d()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v9, v11

    invoke-interface {v1, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2cp;

    invoke-interface {v4}, LX/2cp;->d()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v9, v12

    invoke-virtual {v6, v7, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_9

    .line 2518013
    :pswitch_3
    iget-object v6, p0, LX/Hur;->a:Landroid/content/res/Resources;

    const v7, 0x7f0812f4

    new-array v9, v11, [Ljava/lang/Object;

    invoke-interface {v1, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2cp;

    invoke-interface {v4}, LX/2cp;->d()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v9, v10

    invoke-virtual {v6, v7, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_9

    .line 2518014
    :pswitch_4
    iget-object v6, p0, LX/Hur;->a:Landroid/content/res/Resources;

    const v7, 0x7f0812f5

    new-array v9, v12, [Ljava/lang/Object;

    invoke-interface {v1, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2cp;

    invoke-interface {v4}, LX/2cp;->d()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v9, v10

    invoke-interface {v1, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2cp;

    invoke-interface {v4}, LX/2cp;->d()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v9, v11

    invoke-virtual {v6, v7, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_9

    .line 2518015
    :pswitch_5
    iget-object v6, p0, LX/Hur;->a:Landroid/content/res/Resources;

    const v7, 0x7f0812f6

    new-array v9, v9, [Ljava/lang/Object;

    invoke-interface {v1, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2cp;

    invoke-interface {v4}, LX/2cp;->d()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v9, v10

    invoke-interface {v1, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2cp;

    invoke-interface {v4}, LX/2cp;->d()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v9, v11

    invoke-interface {v1, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2cp;

    invoke-interface {v4}, LX/2cp;->d()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v9, v12

    invoke-virtual {v6, v7, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_9

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static d(LX/Hur;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2518016
    iget-object v1, p0, LX/Hur;->k:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iget-object v1, v1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 2518017
    if-nez v1, :cond_1

    .line 2518018
    :cond_0
    :goto_0
    return v0

    .line 2518019
    :cond_1
    iget-object p0, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v1, p0

    .line 2518020
    if-eqz v1, :cond_0

    .line 2518021
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static g(LX/Hur;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2518022
    iget-object v1, p0, LX/Hur;->k:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iget-object v1, v1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 2518023
    invoke-virtual {v1}, Lcom/facebook/privacy/model/SelectablePrivacyData;->f()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2518024
    iget-boolean v2, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->b:Z

    move v2, v2

    .line 2518025
    if-nez v2, :cond_0

    iget-boolean v2, p0, LX/Hur;->l:Z

    if-nez v2, :cond_1

    .line 2518026
    :cond_0
    :goto_0
    return v0

    .line 2518027
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/privacy/model/SelectablePrivacyData;->g()Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    move-result-object v1

    .line 2518028
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    if-eq v1, v2, :cond_0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;->NONE:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    if-eq v1, v2, :cond_0

    .line 2518029
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;->FRIENDS_OF_TAGGEES:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    if-ne v1, v0, :cond_2

    .line 2518030
    const v0, 0x7f0200cb

    goto :goto_0

    .line 2518031
    :cond_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;->TAGGEES:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    if-ne v1, v0, :cond_3

    .line 2518032
    const v0, 0x7f0200cf

    goto :goto_0

    .line 2518033
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "GraphQLPrivacyOptionTagExpansionType cannot be "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2518034
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public final a(LX/5L2;)V
    .locals 2

    .prologue
    .line 2518035
    sget-object v0, LX/Huq;->a:[I

    invoke-virtual {p1}, LX/5L2;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2518036
    :goto_0
    return-void

    .line 2518037
    :pswitch_0
    invoke-direct {p0}, LX/Hur;->b()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2518038
    invoke-direct {p0}, LX/Hur;->b()V

    .line 2518039
    return-void
.end method
