.class public final LX/HH6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field public final synthetic a:Lcom/facebook/widget/listview/BetterListView;

.field public final synthetic b:Lcom/facebook/pages/common/photos/PagesPhotosFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/photos/PagesPhotosFragment;Lcom/facebook/widget/listview/BetterListView;)V
    .locals 0

    .prologue
    .line 2447098
    iput-object p1, p0, LX/HH6;->b:Lcom/facebook/pages/common/photos/PagesPhotosFragment;

    iput-object p2, p0, LX/HH6;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2447100
    iget-object v0, p0, LX/HH6;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/HH6;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    move v1, v0

    .line 2447101
    :goto_0
    iget-object v0, p0, LX/HH6;->b:Lcom/facebook/pages/common/photos/PagesPhotosFragment;

    iget-boolean v0, v0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->m:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HH6;->b:Lcom/facebook/pages/common/photos/PagesPhotosFragment;

    iget v0, v0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->n:I

    if-eq v0, v1, :cond_1

    .line 2447102
    :cond_0
    iget-object v0, p0, LX/HH6;->b:Lcom/facebook/pages/common/photos/PagesPhotosFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->f:Landroid/view/View;

    iget-object v3, p0, LX/HH6;->a:Lcom/facebook/widget/listview/BetterListView;

    sget-object v4, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->l:[I

    iget-object v5, p0, LX/HH6;->b:Lcom/facebook/pages/common/photos/PagesPhotosFragment;

    iget-object v5, v5, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->a:LX/0hB;

    invoke-virtual {v5}, LX/0hB;->d()I

    move-result v5

    invoke-static {v0, v3, p2, v4, v5}, LX/8FX;->a(Landroid/view/View;Landroid/view/ViewGroup;I[II)LX/3rL;

    move-result-object v3

    .line 2447103
    iget-object v4, p0, LX/HH6;->b:Lcom/facebook/pages/common/photos/PagesPhotosFragment;

    iget-object v0, v3, LX/3rL;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 2447104
    iput-boolean v0, v4, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->m:Z

    .line 2447105
    iget-object v0, p0, LX/HH6;->b:Lcom/facebook/pages/common/photos/PagesPhotosFragment;

    iget-boolean v0, v0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->m:Z

    if-eqz v0, :cond_1

    .line 2447106
    iget-object v4, p0, LX/HH6;->b:Lcom/facebook/pages/common/photos/PagesPhotosFragment;

    iget-object v0, v3, LX/3rL;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v4, v0}, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->E_(I)V

    .line 2447107
    iget-object v0, p0, LX/HH6;->b:Lcom/facebook/pages/common/photos/PagesPhotosFragment;

    .line 2447108
    iput v1, v0, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->n:I

    .line 2447109
    :cond_1
    iget-object v0, p0, LX/HH6;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/BetterListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/HH6;->b:Lcom/facebook/pages/common/photos/PagesPhotosFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2447110
    iget-object v0, p0, LX/HH6;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/BetterListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    .line 2447111
    iget-object v1, p0, LX/HH6;->b:Lcom/facebook/pages/common/photos/PagesPhotosFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->g:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/HH6;->b:Lcom/facebook/pages/common/photos/PagesPhotosFragment;

    iget v1, v1, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->i:I

    if-eq v1, v0, :cond_2

    .line 2447112
    iget-object v1, p0, LX/HH6;->b:Lcom/facebook/pages/common/photos/PagesPhotosFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->g:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v2, p0, LX/HH6;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v1, v2, p2}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->a(Landroid/view/ViewGroup;I)V

    .line 2447113
    iget-object v1, p0, LX/HH6;->b:Lcom/facebook/pages/common/photos/PagesPhotosFragment;

    .line 2447114
    iput v0, v1, Lcom/facebook/pages/common/photos/PagesPhotosFragment;->i:I

    .line 2447115
    :cond_2
    return-void

    :cond_3
    move v1, v2

    .line 2447116
    goto :goto_0
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 2447099
    return-void
.end method
