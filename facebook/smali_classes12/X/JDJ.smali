.class public final enum LX/JDJ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/JDJ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/JDJ;

.field public static final enum REQUEST_FAILED:LX/JDJ;

.field public static final enum REQUEST_NONE:LX/JDJ;

.field public static final enum REQUEST_PENDING:LX/JDJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2663664
    new-instance v0, LX/JDJ;

    const-string v1, "REQUEST_NONE"

    invoke-direct {v0, v1, v2}, LX/JDJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JDJ;->REQUEST_NONE:LX/JDJ;

    .line 2663665
    new-instance v0, LX/JDJ;

    const-string v1, "REQUEST_PENDING"

    invoke-direct {v0, v1, v3}, LX/JDJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JDJ;->REQUEST_PENDING:LX/JDJ;

    .line 2663666
    new-instance v0, LX/JDJ;

    const-string v1, "REQUEST_FAILED"

    invoke-direct {v0, v1, v4}, LX/JDJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/JDJ;->REQUEST_FAILED:LX/JDJ;

    .line 2663667
    const/4 v0, 0x3

    new-array v0, v0, [LX/JDJ;

    sget-object v1, LX/JDJ;->REQUEST_NONE:LX/JDJ;

    aput-object v1, v0, v2

    sget-object v1, LX/JDJ;->REQUEST_PENDING:LX/JDJ;

    aput-object v1, v0, v3

    sget-object v1, LX/JDJ;->REQUEST_FAILED:LX/JDJ;

    aput-object v1, v0, v4

    sput-object v0, LX/JDJ;->$VALUES:[LX/JDJ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2663668
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/JDJ;
    .locals 1

    .prologue
    .line 2663663
    const-class v0, LX/JDJ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/JDJ;

    return-object v0
.end method

.method public static values()[LX/JDJ;
    .locals 1

    .prologue
    .line 2663662
    sget-object v0, LX/JDJ;->$VALUES:[LX/JDJ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/JDJ;

    return-object v0
.end method
