.class public LX/InN;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private final a:Landroid/widget/LinearLayout;

.field private final b:Lcom/facebook/widget/text/BetterTextView;

.field private final c:Lcom/facebook/widget/text/BetterTextView;

.field private final d:Lcom/facebook/widget/text/BetterTextView;

.field private final e:Lcom/facebook/widget/text/BetterTextView;

.field private final f:Landroid/widget/ProgressBar;

.field private final g:Landroid/widget/LinearLayout;

.field private final h:Landroid/widget/LinearLayout;

.field private i:LX/InQ;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2610628
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/InN;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2610629
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2610630
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/InN;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2610631
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2610632
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2610633
    const v0, 0x7f030f8f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2610634
    const v0, 0x7f0d258e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/InN;->a:Landroid/widget/LinearLayout;

    .line 2610635
    const v0, 0x7f0d2590

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/InN;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 2610636
    const v0, 0x7f0d2593

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/InN;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 2610637
    const v0, 0x7f0d2596

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/InN;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 2610638
    const v0, 0x7f0d2598

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/InN;->e:Lcom/facebook/widget/text/BetterTextView;

    .line 2610639
    const v0, 0x7f0d2599

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, LX/InN;->f:Landroid/widget/ProgressBar;

    .line 2610640
    const v0, 0x7f0d2591

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/InN;->g:Landroid/widget/LinearLayout;

    .line 2610641
    const v0, 0x7f0d2594

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/InN;->h:Landroid/widget/LinearLayout;

    .line 2610642
    return-void
.end method

.method private a(Landroid/widget/LinearLayout;Lcom/facebook/widget/text/BetterTextView;Ljava/lang/String;I)V
    .locals 2
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 2610643
    if-nez p3, :cond_0

    .line 2610644
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2610645
    :goto_0
    return-void

    .line 2610646
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2610647
    invoke-static {p3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2610648
    invoke-virtual {p2, p4}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 2610649
    invoke-virtual {p0}, LX/InN;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0673

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    goto :goto_0

    .line 2610650
    :cond_1
    invoke-virtual {p2, p3}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2610651
    invoke-virtual {p0}, LX/InN;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0038

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    goto :goto_0
.end method


# virtual methods
.method public setViewParams(LX/InQ;)V
    .locals 4

    .prologue
    .line 2610652
    iput-object p1, p0, LX/InN;->i:LX/InQ;

    .line 2610653
    iget-object v0, p0, LX/InN;->b:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, LX/InN;->i:LX/InQ;

    iget-object v1, v1, LX/InQ;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2610654
    iget-object v0, p0, LX/InN;->g:Landroid/widget/LinearLayout;

    iget-object v1, p0, LX/InN;->c:Lcom/facebook/widget/text/BetterTextView;

    iget-object v2, p0, LX/InN;->i:LX/InQ;

    iget-object v2, v2, LX/InQ;->b:Ljava/lang/String;

    const v3, 0x7f082d1b

    invoke-direct {p0, v0, v1, v2, v3}, LX/InN;->a(Landroid/widget/LinearLayout;Lcom/facebook/widget/text/BetterTextView;Ljava/lang/String;I)V

    .line 2610655
    iget-object v0, p0, LX/InN;->h:Landroid/widget/LinearLayout;

    iget-object v1, p0, LX/InN;->d:Lcom/facebook/widget/text/BetterTextView;

    iget-object v2, p0, LX/InN;->i:LX/InQ;

    iget-object v2, v2, LX/InQ;->c:Ljava/lang/String;

    const v3, 0x7f082d1c

    invoke-direct {p0, v0, v1, v2, v3}, LX/InN;->a(Landroid/widget/LinearLayout;Lcom/facebook/widget/text/BetterTextView;Ljava/lang/String;I)V

    .line 2610656
    iget-object v0, p0, LX/InN;->e:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, LX/InN;->i:LX/InQ;

    iget-object v1, v1, LX/InQ;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2610657
    return-void
.end method
