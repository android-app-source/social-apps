.class public final LX/IcA;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/IcC;


# direct methods
.method public constructor <init>(LX/IcC;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2595248
    iput-object p1, p0, LX/IcA;->c:LX/IcC;

    iput-object p2, p0, LX/IcA;->a:Ljava/lang/String;

    iput-object p3, p0, LX/IcA;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2595257
    iget-object v0, p0, LX/IcA;->c:LX/IcC;

    iget-object v0, v0, LX/IcC;->e:LX/IZK;

    invoke-virtual {v0}, LX/IZK;->a()V

    .line 2595258
    return-void
.end method

.method public final a(LX/15i;I)V
    .locals 12
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "onSuccess"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2595249
    iget-object v0, p0, LX/IcA;->c:LX/IcC;

    iget-object v1, p0, LX/IcA;->a:Ljava/lang/String;

    iget-object v2, p0, LX/IcA;->b:Ljava/lang/String;

    const/4 v9, 0x1

    const/4 v3, 0x0

    .line 2595250
    invoke-virtual {p1, p2, v9}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {p1, p2, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2595251
    :cond_0
    :goto_0
    move v0, v3

    .line 2595252
    if-eqz v0, :cond_1

    .line 2595253
    :goto_1
    return-void

    .line 2595254
    :cond_1
    iget-object v0, p0, LX/IcA;->c:LX/IcC;

    iget-object v1, p0, LX/IcA;->a:Ljava/lang/String;

    iget-object v2, p0, LX/IcA;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2, p1, p2}, LX/IcC;->a(LX/IcC;Ljava/lang/String;Ljava/lang/String;LX/15i;I)V

    goto :goto_1

    .line 2595255
    :cond_2
    new-instance v4, LX/31Y;

    iget-object v5, v0, LX/IcC;->a:Landroid/content/Context;

    invoke-direct {v4, v5}, LX/31Y;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1, p2, v9}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v4

    invoke-virtual {p1, p2, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v10

    const v11, 0x7f08002a

    new-instance v3, LX/IcB;

    move-object v4, v0

    move-object v5, v1

    move-object v6, v2

    move-object v7, p1

    move v8, p2

    invoke-direct/range {v3 .. v8}, LX/IcB;-><init>(LX/IcC;Ljava/lang/String;Ljava/lang/String;LX/15i;I)V

    invoke-virtual {v10, v11, v3}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v3

    invoke-virtual {v3}, LX/0ju;->b()LX/2EJ;

    move v3, v9

    .line 2595256
    goto :goto_0
.end method
