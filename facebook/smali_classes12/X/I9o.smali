.class public LX/I9o;
.super LX/2s5;
.source ""


# static fields
.field public static final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/Blc;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Landroid/content/Context;

.field public final d:LX/154;

.field private final e:LX/BiT;

.field public f:Z

.field public g:[Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;

.field public h:LX/I9V;

.field public i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/Blc;",
            ">;"
        }
    .end annotation
.end field

.field public j:[I

.field private k:Landroid/support/v4/app/Fragment;

.field public l:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/model/EventUser;",
            ">;>;"
        }
    .end annotation
.end field

.field public m:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/model/EventUser;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2544217
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    const/4 v1, 0x3

    new-array v1, v1, [LX/Blc;

    const/4 v2, 0x0

    sget-object v3, LX/Blc;->PRIVATE_GOING:LX/Blc;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, LX/Blc;->PRIVATE_MAYBE:LX/Blc;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    sget-object v3, LX/Blc;->PRIVATE_INVITED:LX/Blc;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, LX/0Pz;->b([Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    sput-object v0, LX/I9o;->b:LX/0Px;

    return-void
.end method

.method public constructor <init>(LX/BiT;LX/154;LX/0gc;Landroid/content/Context;Z)V
    .locals 0
    .param p3    # LX/0gc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2544218
    invoke-direct {p0, p3}, LX/2s5;-><init>(LX/0gc;)V

    .line 2544219
    iput-object p1, p0, LX/I9o;->e:LX/BiT;

    .line 2544220
    iput-object p2, p0, LX/I9o;->d:LX/154;

    .line 2544221
    iput-object p4, p0, LX/I9o;->c:Landroid/content/Context;

    .line 2544222
    iput-boolean p5, p0, LX/I9o;->f:Z

    .line 2544223
    return-void
.end method

.method public static b(LX/I9o;LX/Blc;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 2544224
    iget-object v0, p0, LX/I9o;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2544225
    sget-object v1, LX/I9n;->a:[I

    invoke-virtual {p1}, LX/Blc;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2544226
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "GuestListType has no resource assigned to it"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2544227
    :pswitch_0
    const v1, 0x7f081f1c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2544228
    :goto_0
    return-object v0

    .line 2544229
    :pswitch_1
    const v1, 0x7f081f1d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2544230
    :pswitch_2
    const v1, 0x7f081f80

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2544231
    :pswitch_3
    const v1, 0x7f081f1e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2544232
    :pswitch_4
    const v1, 0x7f081f1f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static c(LX/Blc;)I
    .locals 2

    .prologue
    .line 2544233
    sget-object v0, LX/I9n;->a:[I

    invoke-virtual {p0}, LX/Blc;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2544234
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "GuestListType has no resource assigned to it"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2544235
    :pswitch_1
    const v0, 0x7f0f00dd

    .line 2544236
    :goto_0
    return v0

    .line 2544237
    :pswitch_2
    const v0, 0x7f0f00de

    goto :goto_0

    .line 2544238
    :pswitch_3
    const v0, 0x7f0f00df

    goto :goto_0

    .line 2544239
    :pswitch_4
    const v0, 0x7f0f00e0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public final G_(I)Ljava/lang/CharSequence;
    .locals 8

    .prologue
    .line 2544240
    iget-object v0, p0, LX/I9o;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge p1, v0, :cond_1

    .line 2544241
    iget-object v0, p0, LX/I9o;->a:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;

    .line 2544242
    iget-object v1, v0, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;->a:LX/Blc;

    move-object v1, v1

    .line 2544243
    iget-object v2, v0, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;->b:Ljava/lang/Integer;

    if-nez v2, :cond_2

    const/4 v2, 0x0

    :goto_0
    move-object v0, v2

    .line 2544244
    iget-boolean v2, p0, LX/I9o;->f:Z

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-gez v2, :cond_3

    .line 2544245
    :cond_0
    invoke-static {p0, v1}, LX/I9o;->b(LX/I9o;LX/Blc;)Ljava/lang/String;

    move-result-object v2

    .line 2544246
    :goto_1
    move-object v0, v2

    .line 2544247
    :goto_2
    return-object v0

    :cond_1
    const-string v0, ""

    goto :goto_2

    :cond_2
    iget-object v2, v0, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget p1, v0, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;->c:I

    add-int/2addr v2, p1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto :goto_0

    :cond_3
    iget-object v2, p0, LX/I9o;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v1}, LX/I9o;->c(LX/Blc;)I

    move-result v3

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, LX/I9o;->d:LX/154;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {v7, p1}, LX/154;->a(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 2544248
    iget-object v0, p0, LX/I9o;->g:[Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2544249
    invoke-super {p0, p1, p2}, LX/2s5;->a(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v1

    .line 2544250
    iget-object v2, p0, LX/I9o;->g:[Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;

    move-object v0, v1

    check-cast v0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;

    aput-object v0, v2, p2

    .line 2544251
    return-object v1
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2544252
    iget-object v0, p0, LX/I9o;->g:[Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;

    array-length v0, v0

    return v0
.end method

.method public final b(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 7

    .prologue
    .line 2544253
    move-object v0, p3

    check-cast v0, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;

    .line 2544254
    iget-object v1, p0, LX/I9o;->k:Landroid/support/v4/app/Fragment;

    if-eq v0, v1, :cond_7

    .line 2544255
    invoke-super {p0, p1, p2, p3}, LX/2s5;->b(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 2544256
    iget-object v1, p0, LX/I9o;->j:[I

    aget v2, v1, p2

    add-int/lit8 v2, v2, 0x1

    aput v2, v1, p2

    .line 2544257
    iget-object v1, p0, LX/I9o;->h:LX/I9V;

    if-eqz v1, :cond_6

    .line 2544258
    iget-object v2, p0, LX/I9o;->h:LX/I9V;

    iget-object v1, p0, LX/I9o;->i:LX/0Px;

    invoke-virtual {v1, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Blc;

    .line 2544259
    iget-object v3, v2, LX/I9V;->a:Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;

    iget-object v3, v3, Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;->c:LX/IAH;

    invoke-virtual {v3, v1}, LX/IAH;->a(LX/Blc;)V

    .line 2544260
    iget-object v3, v2, LX/I9V;->a:Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;

    iget-object v3, v3, Lcom/facebook/events/permalink/guestlist/EventGuestListFrameFragment;->h:LX/I9o;

    .line 2544261
    const/4 v4, 0x0

    move v5, v4

    :goto_0
    iget-object v4, v3, LX/I9o;->i:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    if-ge v5, v4, :cond_6

    .line 2544262
    iget-object v4, v3, LX/I9o;->m:LX/0Px;

    invoke-virtual {v4, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    .line 2544263
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_2

    .line 2544264
    iget-object v6, v3, LX/I9o;->g:[Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;

    aget-object v6, v6, v5

    .line 2544265
    iget-object p1, v6, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->d:LX/I9l;

    if-eqz p1, :cond_0

    .line 2544266
    iget-object p1, v6, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->d:LX/I9l;

    .line 2544267
    iget-object p2, p1, LX/I9l;->d:LX/0Px;

    invoke-static {v4, p2}, LX/IAE;->a(Ljava/util/List;Ljava/util/List;)V

    .line 2544268
    invoke-static {p1}, LX/I9l;->l(LX/I9l;)V

    .line 2544269
    :cond_0
    iget-object p1, v6, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->g:LX/I9L;

    if-eqz p1, :cond_1

    .line 2544270
    iget-object p1, v6, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->g:LX/I9L;

    .line 2544271
    iget-object p2, p1, LX/I9L;->j:Ljava/util/Map;

    invoke-interface {p2}, Ljava/util/Map;->clear()V

    .line 2544272
    iget-object p1, v6, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->g:LX/I9L;

    const p2, 0x21917d29

    invoke-static {p1, p2}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2544273
    :cond_1
    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 2544274
    :cond_2
    iget-object v4, v3, LX/I9o;->l:LX/0Px;

    invoke-virtual {v4, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    .line 2544275
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_5

    .line 2544276
    iget-object v6, v3, LX/I9o;->g:[Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;

    aget-object v6, v6, v5

    .line 2544277
    iget-object p1, v6, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->d:LX/I9l;

    if-eqz p1, :cond_3

    .line 2544278
    iget-object p1, v6, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->d:LX/I9l;

    .line 2544279
    iget-object p2, p1, LX/I9l;->e:LX/I9k;

    move-object p2, p2

    .line 2544280
    sget-object p3, LX/I9k;->INITIAL:LX/I9k;

    if-ne p2, p3, :cond_8

    .line 2544281
    :cond_3
    :goto_1
    iget-object p1, v6, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->g:LX/I9L;

    if-eqz p1, :cond_4

    .line 2544282
    iget-object p1, v6, Lcom/facebook/events/permalink/guestlist/EventGuestListFragment;->g:LX/I9L;

    const p2, 0x64928e77

    invoke-static {p1, p2}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2544283
    :cond_4
    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 2544284
    :cond_5
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_0

    .line 2544285
    :cond_6
    iput-object v0, p0, LX/I9o;->k:Landroid/support/v4/app/Fragment;

    .line 2544286
    :cond_7
    return-void

    .line 2544287
    :cond_8
    iget-object p2, p1, LX/I9l;->h:LX/IA6;

    if-eqz p2, :cond_3

    iget-object p2, p1, LX/I9l;->h:LX/IA6;

    instance-of p2, p2, LX/IAE;

    if-eqz p2, :cond_3

    .line 2544288
    iget-object p2, p1, LX/I9l;->d:LX/0Px;

    invoke-static {v4, p2}, LX/IAE;->a(Ljava/util/List;Ljava/util/List;)V

    .line 2544289
    iget-object p2, p1, LX/I9l;->h:LX/IA6;

    check-cast p2, LX/IAE;

    .line 2544290
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/facebook/events/model/EventUser;

    .line 2544291
    iget-object v2, p2, LX/IA6;->a:Ljava/util/List;

    check-cast v2, Ljava/util/LinkedList;

    invoke-virtual {v2, p3}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    goto :goto_2

    .line 2544292
    :cond_9
    invoke-static {p1}, LX/I9l;->l(LX/I9l;)V

    goto :goto_1
.end method
