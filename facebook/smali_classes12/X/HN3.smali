.class public final enum LX/HN3;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HN3;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/HN3;

.field public static final enum ADD_SERVICE_BUTTON:LX/HN3;

.field public static final enum ADMIN_NO_SERVICES_TEXT:LX/HN3;

.field public static final enum ADMIN_PUBLISH_SERVICES_TEXT:LX/HN3;

.field public static final enum EMPTY_LIST_TEXT:LX/HN3;

.field public static final enum INTRO_MESSAGE:LX/HN3;

.field public static final enum SERVICE_ITEM:LX/HN3;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2458022
    new-instance v0, LX/HN3;

    const-string v1, "ADMIN_PUBLISH_SERVICES_TEXT"

    invoke-direct {v0, v1, v3}, LX/HN3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HN3;->ADMIN_PUBLISH_SERVICES_TEXT:LX/HN3;

    .line 2458023
    new-instance v0, LX/HN3;

    const-string v1, "ADD_SERVICE_BUTTON"

    invoke-direct {v0, v1, v4}, LX/HN3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HN3;->ADD_SERVICE_BUTTON:LX/HN3;

    .line 2458024
    new-instance v0, LX/HN3;

    const-string v1, "INTRO_MESSAGE"

    invoke-direct {v0, v1, v5}, LX/HN3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HN3;->INTRO_MESSAGE:LX/HN3;

    .line 2458025
    new-instance v0, LX/HN3;

    const-string v1, "EMPTY_LIST_TEXT"

    invoke-direct {v0, v1, v6}, LX/HN3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HN3;->EMPTY_LIST_TEXT:LX/HN3;

    .line 2458026
    new-instance v0, LX/HN3;

    const-string v1, "SERVICE_ITEM"

    invoke-direct {v0, v1, v7}, LX/HN3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HN3;->SERVICE_ITEM:LX/HN3;

    .line 2458027
    new-instance v0, LX/HN3;

    const-string v1, "ADMIN_NO_SERVICES_TEXT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/HN3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HN3;->ADMIN_NO_SERVICES_TEXT:LX/HN3;

    .line 2458028
    const/4 v0, 0x6

    new-array v0, v0, [LX/HN3;

    sget-object v1, LX/HN3;->ADMIN_PUBLISH_SERVICES_TEXT:LX/HN3;

    aput-object v1, v0, v3

    sget-object v1, LX/HN3;->ADD_SERVICE_BUTTON:LX/HN3;

    aput-object v1, v0, v4

    sget-object v1, LX/HN3;->INTRO_MESSAGE:LX/HN3;

    aput-object v1, v0, v5

    sget-object v1, LX/HN3;->EMPTY_LIST_TEXT:LX/HN3;

    aput-object v1, v0, v6

    sget-object v1, LX/HN3;->SERVICE_ITEM:LX/HN3;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/HN3;->ADMIN_NO_SERVICES_TEXT:LX/HN3;

    aput-object v2, v0, v1

    sput-object v0, LX/HN3;->$VALUES:[LX/HN3;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2458029
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HN3;
    .locals 1

    .prologue
    .line 2458030
    const-class v0, LX/HN3;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HN3;

    return-object v0
.end method

.method public static values()[LX/HN3;
    .locals 1

    .prologue
    .line 2458031
    sget-object v0, LX/HN3;->$VALUES:[LX/HN3;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HN3;

    return-object v0
.end method
