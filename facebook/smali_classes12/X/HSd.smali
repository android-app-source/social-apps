.class public final LX/HSd;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$PageUpcomingEventsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/HSQ;

.field public final synthetic b:I

.field public final synthetic c:LX/HSk;


# direct methods
.method public constructor <init>(LX/HSk;LX/HSQ;I)V
    .locals 0

    .prologue
    .line 2467382
    iput-object p1, p0, LX/HSd;->c:LX/HSk;

    iput-object p2, p0, LX/HSd;->a:LX/HSQ;

    iput p3, p0, LX/HSd;->b:I

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 2467383
    iget-object v0, p0, LX/HSd;->a:LX/HSQ;

    sget-object v5, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    move v2, v1

    move v4, v1

    move-object v6, v3

    move v7, v1

    invoke-virtual/range {v0 .. v7}, LX/HSQ;->a(ZZLcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;ILjava/util/List;Ljava/lang/String;Z)V

    .line 2467384
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 10

    .prologue
    .line 2467385
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 2467386
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2467387
    move-object v2, v0

    check-cast v2, Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$PageUpcomingEventsQueryModel;

    .line 2467388
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$PageUpcomingEventsQueryModel;->l()Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$PageUpcomingEventsQueryModel$OwnedEventsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2467389
    invoke-virtual {v2}, Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$PageUpcomingEventsQueryModel;->m()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2467390
    new-instance v0, LX/8A4;

    invoke-virtual {v2}, Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$PageUpcomingEventsQueryModel;->m()LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, LX/8A4;-><init>(Ljava/util/List;)V

    sget-object v1, LX/8A3;->CREATE_CONTENT:LX/8A3;

    invoke-virtual {v0, v1}, LX/8A4;->a(LX/8A3;)Z

    move-result v1

    .line 2467391
    :goto_0
    invoke-virtual {v2}, Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$PageUpcomingEventsQueryModel;->l()Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$PageUpcomingEventsQueryModel$OwnedEventsModel;

    move-result-object v0

    .line 2467392
    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$PageUpcomingEventsQueryModel$OwnedEventsModel;->a()LX/0Px;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$PageUpcomingEventsQueryModel$OwnedEventsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$PageUpcomingEventsQueryModel$OwnedEventsModel;->a()LX/0Px;

    move-result-object v5

    .line 2467393
    :goto_1
    invoke-virtual {v2}, Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$PageUpcomingEventsQueryModel;->k()Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    move-result-object v3

    .line 2467394
    invoke-virtual {v2}, Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$PageUpcomingEventsQueryModel;->j()I

    move-result v4

    .line 2467395
    invoke-virtual {v0}, Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$PageUpcomingEventsQueryModel$OwnedEventsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v9, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2467396
    sget-object v6, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v6

    :try_start_0
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2467397
    if-nez v0, :cond_3

    const/4 v6, 0x0

    .line 2467398
    :goto_2
    if-eqz v0, :cond_0

    invoke-virtual {v9, v0, v8}, LX/15i;->h(II)Z

    move-result v0

    if-eqz v0, :cond_0

    move v7, v8

    .line 2467399
    :cond_0
    iget-object v0, p0, LX/HSd;->a:LX/HSQ;

    invoke-virtual {v2}, Lcom/facebook/pages/fb4a/events/eventslist/protocol/PageEventsGraphQLModels$PageUpcomingEventsQueryModel;->a()Z

    move-result v2

    invoke-virtual/range {v0 .. v7}, LX/HSQ;->a(ZZLcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;ILjava/util/List;Ljava/lang/String;Z)V

    .line 2467400
    iget-object v0, p0, LX/HSd;->c:LX/HSk;

    invoke-static {v0, v5, p1}, LX/HSk;->a$redex0(LX/HSk;Ljava/util/List;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 2467401
    :cond_1
    return-void

    .line 2467402
    :cond_2
    sget-object v5, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    goto :goto_1

    .line 2467403
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2467404
    :cond_3
    invoke-virtual {v9, v0, v7}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v6

    goto :goto_2

    :cond_4
    move v1, v7

    goto :goto_0
.end method
