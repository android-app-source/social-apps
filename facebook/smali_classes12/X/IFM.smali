.class public LX/IFM;
.super LX/Ep8;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/IFM;


# instance fields
.field private final d:LX/EoY;


# direct methods
.method public constructor <init>(LX/EoB;LX/EnR;LX/EoY;LX/Eod;LX/Enj;LX/EpA;)V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2554035
    move-object v0, p0

    move-object v1, p1

    move-object v2, p6

    move-object v3, p2

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, LX/Ep8;-><init>(LX/EoB;LX/EpA;LX/EnR;LX/Eod;LX/Enj;)V

    .line 2554036
    iput-object p3, p0, LX/IFM;->d:LX/EoY;

    .line 2554037
    return-void
.end method

.method public static a(LX/0QB;)LX/IFM;
    .locals 10

    .prologue
    .line 2554022
    sget-object v0, LX/IFM;->e:LX/IFM;

    if-nez v0, :cond_1

    .line 2554023
    const-class v1, LX/IFM;

    monitor-enter v1

    .line 2554024
    :try_start_0
    sget-object v0, LX/IFM;->e:LX/IFM;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2554025
    if-eqz v2, :cond_0

    .line 2554026
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2554027
    new-instance v3, LX/IFM;

    invoke-static {v0}, LX/EoB;->b(LX/0QB;)LX/EoB;

    move-result-object v4

    check-cast v4, LX/EoB;

    invoke-static {v0}, LX/EnR;->a(LX/0QB;)LX/EnR;

    move-result-object v5

    check-cast v5, LX/EnR;

    invoke-static {v0}, LX/EoY;->a(LX/0QB;)LX/EoY;

    move-result-object v6

    check-cast v6, LX/EoY;

    const-class v7, LX/Eod;

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/Eod;

    const-class v8, LX/Enj;

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/Enj;

    invoke-static {v0}, LX/EpA;->a(LX/0QB;)LX/EpA;

    move-result-object v9

    check-cast v9, LX/EpA;

    invoke-direct/range {v3 .. v9}, LX/IFM;-><init>(LX/EoB;LX/EnR;LX/EoY;LX/Eod;LX/Enj;LX/EpA;)V

    .line 2554028
    move-object v0, v3

    .line 2554029
    sput-object v0, LX/IFM;->e:LX/IFM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2554030
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2554031
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2554032
    :cond_1
    sget-object v0, LX/IFM;->e:LX/IFM;

    return-object v0

    .line 2554033
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2554034
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)LX/Emj;
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2554021
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2554038
    const-string v0, "friends_nearby"

    return-object v0
.end method

.method public final b()LX/Enm;
    .locals 1

    .prologue
    .line 2554020
    iget-object v0, p0, LX/IFM;->d:LX/EoY;

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)LX/Jxq;
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2554019
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 2554018
    const/4 v0, 0x0

    return v0
.end method
