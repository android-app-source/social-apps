.class public final LX/Hwn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/accounts/Account;

.field public final synthetic b:LX/4gy;

.field public final synthetic c:LX/2NM;


# direct methods
.method public constructor <init>(LX/2NM;Landroid/accounts/Account;LX/4gy;)V
    .locals 0

    .prologue
    .line 2521415
    iput-object p1, p0, LX/Hwn;->c:LX/2NM;

    iput-object p2, p0, LX/Hwn;->a:Landroid/accounts/Account;

    iput-object p3, p0, LX/Hwn;->b:LX/4gy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2521416
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2521417
    check-cast p1, Ljava/lang/String;

    .line 2521418
    if-nez p1, :cond_0

    .line 2521419
    :goto_0
    return-void

    .line 2521420
    :cond_0
    iget-object v0, p0, LX/Hwn;->c:LX/2NM;

    iget-object v0, v0, LX/2NM;->h:Lcom/facebook/account/recovery/common/model/AccountRecoveryData;

    new-instance v1, Lcom/facebook/openidconnect/model/OpenIDCredential;

    iget-object v2, p0, LX/Hwn;->a:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v3, p0, LX/Hwn;->b:LX/4gy;

    invoke-direct {v1, v2, v3, p1}, Lcom/facebook/openidconnect/model/OpenIDCredential;-><init>(Ljava/lang/String;LX/4gy;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/facebook/account/recovery/common/model/AccountRecoveryData;->a(Lcom/facebook/openidconnect/model/OpenIDCredential;)V

    goto :goto_0
.end method
