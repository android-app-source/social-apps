.class public LX/JLl;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2681498
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/103;)Lcom/facebook/search/api/GraphSearchQuery;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 2681499
    sget-object v0, LX/7BH;->LIGHT:LX/7BH;

    const-string v2, ""

    const-string v3, ""

    sget-object v4, LX/7B5;->SCOPED_ONLY:LX/7B5;

    move-object v1, p0

    invoke-static/range {v0 .. v5}, Lcom/facebook/search/api/GraphSearchQuery;->a(LX/7BH;LX/103;Ljava/lang/String;Ljava/lang/String;LX/7B5;Z)Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v0

    .line 2681500
    sget-object v1, LX/7B4;->SCOPED_TAB:LX/7B4;

    new-instance v2, LX/7BC;

    invoke-direct {v2}, LX/7BC;-><init>()V

    .line 2681501
    iput-boolean v5, v2, LX/7BC;->b:Z

    .line 2681502
    move-object v2, v2

    .line 2681503
    invoke-virtual {v2}, LX/7BC;->a()Lcom/facebook/search/api/GraphSearchQueryTabModifier;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/search/api/GraphSearchQuery;->a(LX/7B4;Landroid/os/Parcelable;)V

    .line 2681504
    return-object v0
.end method
