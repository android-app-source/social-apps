.class public LX/IpQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/IoC;


# instance fields
.field private final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2612920
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2612921
    iput-object p1, p0, LX/IpQ;->a:LX/0Zb;

    .line 2612922
    return-void
.end method

.method public static b(LX/0QB;)LX/IpQ;
    .locals 2

    .prologue
    .line 2612891
    new-instance v1, LX/IpQ;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-direct {v1, v0}, LX/IpQ;-><init>(LX/0Zb;)V

    .line 2612892
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/facebook/messaging/payment/value/input/MessengerPayData;)V
    .locals 1

    .prologue
    .line 2612893
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/IpQ;->a(Ljava/lang/String;Lcom/facebook/messaging/payment/value/input/MessengerPayData;Ljava/lang/String;)V

    .line 2612894
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/messaging/payment/value/input/MessengerPayData;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 2612895
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/messaging/payment/value/input/MessengerPayData;Ljava/lang/String;)V
    .locals 3
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2612896
    const-string v0, "p2p_request"

    invoke-static {p1, v0}, Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;->d(Ljava/lang/String;Ljava/lang/String;)LX/5fz;

    move-result-object v1

    .line 2612897
    const/4 v0, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 2612898
    :goto_1
    return-void

    .line 2612899
    :sswitch_0
    const-string v2, "p2p_confirm_request"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v2, "p2p_request_success"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v2, "p2p_request_fail"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    .line 2612900
    :pswitch_0
    iget-object v0, p0, LX/IpQ;->a:LX/0Zb;

    .line 2612901
    iget-object v2, p2, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->r:Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    move-object v2, v2

    .line 2612902
    invoke-virtual {v2}, Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;->c()Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/5fz;->a(Lcom/facebook/payments/currency/CurrencyAmount;)LX/5fz;

    move-result-object v1

    .line 2612903
    iget-object v2, v1, LX/5fz;->a:Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;

    move-object v1, v2

    .line 2612904
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_1

    .line 2612905
    :pswitch_1
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2612906
    iget-object v0, p0, LX/IpQ;->a:LX/0Zb;

    invoke-virtual {v1, p3}, LX/5fz;->b(Ljava/lang/String;)LX/5fz;

    move-result-object v1

    .line 2612907
    iget-object v2, p2, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->p:Lcom/facebook/user/model/UserKey;

    move-object v2, v2

    .line 2612908
    invoke-virtual {v2}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/5fz;->i(Ljava/lang/String;)LX/5fz;

    move-result-object v1

    .line 2612909
    iget-object v2, p2, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->r:Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    move-object v2, v2

    .line 2612910
    invoke-virtual {v2}, Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;->c()Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/5fz;->a(Lcom/facebook/payments/currency/CurrencyAmount;)LX/5fz;

    move-result-object v1

    .line 2612911
    iget-object v2, v1, LX/5fz;->a:Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;

    move-object v1, v2

    .line 2612912
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_1

    .line 2612913
    :pswitch_2
    iget-object v0, p0, LX/IpQ;->a:LX/0Zb;

    .line 2612914
    iget-object v2, p2, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->p:Lcom/facebook/user/model/UserKey;

    move-object v2, v2

    .line 2612915
    invoke-virtual {v2}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/5fz;->i(Ljava/lang/String;)LX/5fz;

    move-result-object v1

    .line 2612916
    iget-object v2, p2, Lcom/facebook/messaging/payment/value/input/MessengerPayData;->r:Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;

    move-object v2, v2

    .line 2612917
    invoke-virtual {v2}, Lcom/facebook/payments/p2p/value/input/P2pPaymentAmount;->c()Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/5fz;->a(Lcom/facebook/payments/currency/CurrencyAmount;)LX/5fz;

    move-result-object v1

    .line 2612918
    iget-object v2, v1, LX/5fz;->a:Lcom/facebook/payments/p2p/analytics/P2pPaymentsLogEvent;

    move-object v1, v2

    .line 2612919
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x2204af81 -> :sswitch_2
        0x12373f02 -> :sswitch_1
        0x641824bf -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
