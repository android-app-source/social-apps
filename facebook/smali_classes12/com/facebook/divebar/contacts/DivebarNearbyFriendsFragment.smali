.class public Lcom/facebook/divebar/contacts/DivebarNearbyFriendsFragment;
.super Lcom/facebook/ui/drawers/DrawerContentFragment;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public b:LX/3Kv;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/0aG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private e:LX/3Ne;

.field private f:Landroid/view/View;

.field private g:LX/JHs;

.field public h:LX/3LU;

.field public i:LX/1Mv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Mv",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2677144
    const-class v0, Lcom/facebook/divebar/contacts/DivebarNearbyFriendsFragment;

    sput-object v0, Lcom/facebook/divebar/contacts/DivebarNearbyFriendsFragment;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2677145
    invoke-direct {p0}, Lcom/facebook/ui/drawers/DrawerContentFragment;-><init>()V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/divebar/contacts/DivebarNearbyFriendsFragment;LX/0Px;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2677146
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2677147
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2677148
    iget-object v4, p0, Lcom/facebook/divebar/contacts/DivebarNearbyFriendsFragment;->b:LX/3Kv;

    sget-object v5, LX/3OH;->NEARBY:LX/3OH;

    .line 2677149
    const/4 v6, 0x1

    invoke-static {v4, v0, v6, v5}, LX/3Kv;->a(LX/3Kv;Lcom/facebook/user/model/User;ZLX/3OH;)LX/3OJ;

    move-result-object v6

    invoke-virtual {v6}, LX/3OJ;->a()LX/3OO;

    move-result-object v6

    move-object v0, v6

    .line 2677150
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2677151
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2677152
    :cond_0
    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarNearbyFriendsFragment;->e:LX/3Ne;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3Ne;->a(LX/0Px;)V

    .line 2677153
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2677154
    invoke-super {p0, p1}, Lcom/facebook/ui/drawers/DrawerContentFragment;->a(Landroid/os/Bundle;)V

    .line 2677155
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/divebar/contacts/DivebarNearbyFriendsFragment;

    invoke-static {v0}, LX/3Kv;->b(LX/0QB;)LX/3Kv;

    move-result-object v2

    check-cast v2, LX/3Kv;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object p1

    check-cast p1, LX/0aG;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    iput-object v2, p0, Lcom/facebook/divebar/contacts/DivebarNearbyFriendsFragment;->b:LX/3Kv;

    iput-object p1, p0, Lcom/facebook/divebar/contacts/DivebarNearbyFriendsFragment;->c:LX/0aG;

    iput-object v0, p0, Lcom/facebook/divebar/contacts/DivebarNearbyFriendsFragment;->d:Ljava/util/concurrent/ExecutorService;

    .line 2677156
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x2c475a6e

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2677157
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0104b1

    const v3, 0x7f0e0552

    invoke-static {v1, v2, v3}, LX/0WH;->a(Landroid/content/Context;II)Landroid/content/Context;

    move-result-object v1

    .line 2677158
    new-instance v2, LX/JHs;

    invoke-direct {v2, v1}, LX/JHs;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/facebook/divebar/contacts/DivebarNearbyFriendsFragment;->g:LX/JHs;

    .line 2677159
    new-instance v2, LX/3Ne;

    const v3, 0x7f030cf5

    invoke-direct {v2, v1, v3}, LX/3Ne;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, Lcom/facebook/divebar/contacts/DivebarNearbyFriendsFragment;->e:LX/3Ne;

    .line 2677160
    iget-object v1, p0, Lcom/facebook/divebar/contacts/DivebarNearbyFriendsFragment;->e:LX/3Ne;

    iget-object v2, p0, Lcom/facebook/divebar/contacts/DivebarNearbyFriendsFragment;->g:LX/JHs;

    invoke-virtual {v1, v2}, LX/3Ne;->setAdapter(LX/3LH;)V

    .line 2677161
    iget-object v1, p0, Lcom/facebook/divebar/contacts/DivebarNearbyFriendsFragment;->e:LX/3Ne;

    new-instance v2, LX/JHp;

    invoke-direct {v2, p0}, LX/JHp;-><init>(Lcom/facebook/divebar/contacts/DivebarNearbyFriendsFragment;)V

    .line 2677162
    iput-object v2, v1, LX/3Ne;->c:LX/3O4;

    .line 2677163
    iget-object v1, p0, Lcom/facebook/divebar/contacts/DivebarNearbyFriendsFragment;->e:LX/3Ne;

    const v2, 0x7f0d205a

    invoke-virtual {v1, v2}, LX/3Ne;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/divebar/contacts/DivebarNearbyFriendsFragment;->f:Landroid/view/View;

    .line 2677164
    iget-object v1, p0, Lcom/facebook/divebar/contacts/DivebarNearbyFriendsFragment;->f:Landroid/view/View;

    new-instance v2, LX/JHq;

    invoke-direct {v2, p0}, LX/JHq;-><init>(Lcom/facebook/divebar/contacts/DivebarNearbyFriendsFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2677165
    iget-object v1, p0, Lcom/facebook/divebar/contacts/DivebarNearbyFriendsFragment;->e:LX/3Ne;

    sget-object v2, LX/3Nx;->LOADING:LX/3Nx;

    invoke-virtual {v1, v2}, LX/3Ne;->a(LX/3Nx;)V

    .line 2677166
    iget-object v1, p0, Lcom/facebook/divebar/contacts/DivebarNearbyFriendsFragment;->e:LX/3Ne;

    const/16 v2, 0x2b

    const v3, 0x7a37e552

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x4e7d43e7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2677167
    invoke-super {p0}, Lcom/facebook/ui/drawers/DrawerContentFragment;->onDestroyView()V

    .line 2677168
    iget-object v1, p0, Lcom/facebook/divebar/contacts/DivebarNearbyFriendsFragment;->i:LX/1Mv;

    if-eqz v1, :cond_0

    .line 2677169
    iget-object v1, p0, Lcom/facebook/divebar/contacts/DivebarNearbyFriendsFragment;->i:LX/1Mv;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/1Mv;->a(Z)V

    .line 2677170
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/divebar/contacts/DivebarNearbyFriendsFragment;->i:LX/1Mv;

    .line 2677171
    :cond_0
    const/16 v1, 0x2b

    const v2, 0xe63b1ed

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x2458db5f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2677172
    invoke-super {p0}, Lcom/facebook/ui/drawers/DrawerContentFragment;->onResume()V

    .line 2677173
    iget-object v1, p0, Lcom/facebook/divebar/contacts/DivebarNearbyFriendsFragment;->i:LX/1Mv;

    if-eqz v1, :cond_0

    .line 2677174
    :goto_0
    const/16 v1, 0x2b

    const v2, -0x5bad4fc6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2677175
    :cond_0
    iget-object v1, p0, Lcom/facebook/divebar/contacts/DivebarNearbyFriendsFragment;->c:LX/0aG;

    const-string v2, "fetch_nearby_suggestions"

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const v5, 0x71f78c67

    invoke-static {v1, v2, v4, v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v1

    invoke-interface {v1}, LX/1MF;->start()LX/1ML;

    move-result-object v1

    .line 2677176
    new-instance v2, LX/JHr;

    invoke-direct {v2, p0}, LX/JHr;-><init>(Lcom/facebook/divebar/contacts/DivebarNearbyFriendsFragment;)V

    .line 2677177
    invoke-static {v1, v2}, LX/1Mv;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)LX/1Mv;

    move-result-object v4

    iput-object v4, p0, Lcom/facebook/divebar/contacts/DivebarNearbyFriendsFragment;->i:LX/1Mv;

    .line 2677178
    iget-object v4, p0, Lcom/facebook/divebar/contacts/DivebarNearbyFriendsFragment;->d:Ljava/util/concurrent/ExecutorService;

    invoke-static {v1, v2, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
