.class public Lcom/facebook/divebar/contacts/DivebarAvailabilityDialogFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field public volatile m:LX/0Or;
    .annotation runtime Lcom/facebook/push/prefs/IsMobileOnlineAvailabilityEnabled;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public n:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public o:LX/0Zb;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2677090
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2677091
    iget-object v0, p0, Lcom/facebook/divebar/contacts/DivebarAvailabilityDialogFragment;->m:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 2677092
    iget-object v2, p0, Lcom/facebook/divebar/contacts/DivebarAvailabilityDialogFragment;->n:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/1qz;->a:LX/0Tn;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-interface {v2, v3, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v2

    .line 2677093
    new-instance v0, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v3, 0x7f08040a

    invoke-virtual {v0, v3}, LX/0ju;->a(I)LX/0ju;

    move-result-object v3

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/CharSequence;

    const v0, 0x7f08040b

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    if-eqz v2, :cond_0

    move v0, v1

    :goto_0
    new-instance v1, LX/JHk;

    invoke-direct {v1, p0, v2}, LX/JHk;-><init>(Lcom/facebook/divebar/contacts/DivebarAvailabilityDialogFragment;Z)V

    invoke-virtual {v3, v4, v0, v1}, LX/0ju;->a([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x7e2c00b6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2677094
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2677095
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, Lcom/facebook/divebar/contacts/DivebarAvailabilityDialogFragment;

    const/16 v4, 0x155d

    invoke-static {p1, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {p1}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p1}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object p1

    check-cast p1, LX/0Zb;

    iput-object v1, p0, Lcom/facebook/divebar/contacts/DivebarAvailabilityDialogFragment;->m:LX/0Or;

    iput-object v4, p0, Lcom/facebook/divebar/contacts/DivebarAvailabilityDialogFragment;->n:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p1, p0, Lcom/facebook/divebar/contacts/DivebarAvailabilityDialogFragment;->o:LX/0Zb;

    .line 2677096
    const/16 v1, 0x2b

    const v2, -0x1f77944a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
