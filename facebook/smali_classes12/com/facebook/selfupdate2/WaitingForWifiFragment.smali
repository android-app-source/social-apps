.class public Lcom/facebook/selfupdate2/WaitingForWifiFragment;
.super Lcom/facebook/selfupdate2/SelfUpdateFragment;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public a:Ljava/lang/String;
    .annotation runtime Lcom/facebook/selfupdate2/AppNameForSelfUpdate;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Landroid/view/View;

.field private d:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2486670
    invoke-direct {p0}, Lcom/facebook/selfupdate2/SelfUpdateFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2486671
    invoke-super {p0, p1}, Lcom/facebook/selfupdate2/SelfUpdateFragment;->a(Landroid/os/Bundle;)V

    .line 2486672
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/selfupdate2/WaitingForWifiFragment;

    invoke-static {v0}, LX/1wj;->b(LX/0QB;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    iput-object p1, p0, Lcom/facebook/selfupdate2/WaitingForWifiFragment;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/selfupdate2/WaitingForWifiFragment;->b:Landroid/content/res/Resources;

    .line 2486673
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x525fd672

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2486674
    iget-object v1, p0, Lcom/facebook/selfupdate2/WaitingForWifiFragment;->c:Landroid/view/View;

    if-ne p1, v1, :cond_0

    .line 2486675
    const-string v1, "selfupdate2_back_to_facebook_waiting_for_wifi_click"

    invoke-virtual {p0, v1}, Lcom/facebook/selfupdate2/SelfUpdateFragment;->a(Ljava/lang/String;)V

    .line 2486676
    iget-object v1, p0, Lcom/facebook/selfupdate2/SelfUpdateFragment;->f:Lcom/facebook/selfupdate2/SelfUpdateActivity;

    invoke-virtual {v1}, Lcom/facebook/selfupdate2/SelfUpdateActivity;->finish()V

    .line 2486677
    :goto_0
    const v1, 0x37ab41cb

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2486678
    :cond_0
    iget-object v1, p0, Lcom/facebook/selfupdate2/WaitingForWifiFragment;->d:Landroid/widget/Button;

    if-ne p1, v1, :cond_1

    .line 2486679
    const-string v1, "selfupdate2_download_using_mobile_data_click"

    invoke-virtual {p0, v1}, Lcom/facebook/selfupdate2/SelfUpdateFragment;->a(Ljava/lang/String;)V

    .line 2486680
    iget-object v1, p0, Lcom/facebook/selfupdate2/SelfUpdateFragment;->f:Lcom/facebook/selfupdate2/SelfUpdateActivity;

    .line 2486681
    iget-object v2, v1, Lcom/facebook/selfupdate2/SelfUpdateActivity;->B:LX/EeS;

    move-object v1, v2

    .line 2486682
    invoke-virtual {v1}, LX/EeS;->d()Z

    goto :goto_0

    .line 2486683
    :cond_1
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected click event on element: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    const v2, -0x9c20786

    invoke-static {v2, v0}, LX/02F;->a(II)V

    throw v1
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x0

    const/16 v0, 0x2a

    const v1, -0x603099d9

    invoke-static {v8, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2486684
    const v0, 0x7f030bf9

    invoke-virtual {p1, v0, p2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2486685
    const v0, 0x7f0d1daf

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/facebook/selfupdate2/WaitingForWifiFragment;->b:Landroid/content/res/Resources;

    const v4, 0x7f083658

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/facebook/selfupdate2/WaitingForWifiFragment;->a:Ljava/lang/String;

    aput-object v6, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2486686
    const v0, 0x7f0d0508

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/selfupdate2/WaitingForWifiFragment;->c:Landroid/view/View;

    .line 2486687
    const v0, 0x7f0d1db6

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/selfupdate2/WaitingForWifiFragment;->d:Landroid/widget/Button;

    .line 2486688
    iget-object v0, p0, Lcom/facebook/selfupdate2/WaitingForWifiFragment;->c:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2486689
    iget-object v0, p0, Lcom/facebook/selfupdate2/WaitingForWifiFragment;->d:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2486690
    const/16 v0, 0x2b

    const v3, -0x7392411f

    invoke-static {v8, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method
