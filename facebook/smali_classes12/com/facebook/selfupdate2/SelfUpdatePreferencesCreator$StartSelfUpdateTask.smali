.class public final Lcom/facebook/selfupdate2/SelfUpdatePreferencesCreator$StartSelfUpdateTask;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/31K;

.field public final b:Landroid/app/Activity;

.field public final c:Landroid/preference/Preference;


# direct methods
.method public constructor <init>(LX/31K;Landroid/app/Activity;Landroid/preference/Preference;)V
    .locals 0

    .prologue
    .line 2486650
    iput-object p1, p0, Lcom/facebook/selfupdate2/SelfUpdatePreferencesCreator$StartSelfUpdateTask;->a:LX/31K;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2486651
    iput-object p2, p0, Lcom/facebook/selfupdate2/SelfUpdatePreferencesCreator$StartSelfUpdateTask;->b:Landroid/app/Activity;

    .line 2486652
    iput-object p3, p0, Lcom/facebook/selfupdate2/SelfUpdatePreferencesCreator$StartSelfUpdateTask;->c:Landroid/preference/Preference;

    .line 2486653
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2486654
    iget-object v0, p0, Lcom/facebook/selfupdate2/SelfUpdatePreferencesCreator$StartSelfUpdateTask;->a:LX/31K;

    iget-object v0, v0, LX/31K;->a:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/selfupdate2/SelfUpdatePreferencesCreator$StartSelfUpdateTask$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/selfupdate2/SelfUpdatePreferencesCreator$StartSelfUpdateTask$1;-><init>(Lcom/facebook/selfupdate2/SelfUpdatePreferencesCreator$StartSelfUpdateTask;Ljava/lang/String;)V

    const v2, 0x1539fca2

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 2486655
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 2486656
    iget-object v0, p0, Lcom/facebook/selfupdate2/SelfUpdatePreferencesCreator$StartSelfUpdateTask;->a:LX/31K;

    iget-object v0, v0, LX/31K;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1wo;

    .line 2486657
    invoke-virtual {v0}, LX/1wo;->d()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EeS;

    .line 2486658
    invoke-virtual {v0, v1}, LX/1wo;->a(LX/EeS;)V

    goto :goto_0

    .line 2486659
    :cond_0
    iget-object v0, p0, Lcom/facebook/selfupdate2/SelfUpdatePreferencesCreator$StartSelfUpdateTask;->a:LX/31K;

    iget-object v0, v0, LX/31K;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1wt;

    invoke-virtual {v0}, LX/1wt;->a()V

    .line 2486660
    iget-object v0, p0, Lcom/facebook/selfupdate2/SelfUpdatePreferencesCreator$StartSelfUpdateTask;->a:LX/31K;

    iget-object v0, v0, LX/31K;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1wr;

    invoke-virtual {v0}, LX/1wr;->b()V

    .line 2486661
    iget-object v0, p0, Lcom/facebook/selfupdate2/SelfUpdatePreferencesCreator$StartSelfUpdateTask;->a:LX/31K;

    iget-object v0, v0, LX/31K;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1wi;

    invoke-virtual {v0}, LX/1wi;->b()V

    .line 2486662
    :try_start_0
    const-string v0, "Fetching fresh release..."

    invoke-direct {p0, v0}, Lcom/facebook/selfupdate2/SelfUpdatePreferencesCreator$StartSelfUpdateTask;->a(Ljava/lang/String;)V

    .line 2486663
    iget-object v0, p0, Lcom/facebook/selfupdate2/SelfUpdatePreferencesCreator$StartSelfUpdateTask;->a:LX/31K;

    iget-object v0, v0, LX/31K;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2V0;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/2V0;->a(Z)V

    .line 2486664
    const-string v0, "Launching activity..."

    invoke-direct {p0, v0}, Lcom/facebook/selfupdate2/SelfUpdatePreferencesCreator$StartSelfUpdateTask;->a(Ljava/lang/String;)V

    .line 2486665
    iget-object v0, p0, Lcom/facebook/selfupdate2/SelfUpdatePreferencesCreator$StartSelfUpdateTask;->a:LX/31K;

    iget-object v0, v0, LX/31K;->a:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/selfupdate2/SelfUpdatePreferencesCreator$StartSelfUpdateTask$2;

    invoke-direct {v1, p0}, Lcom/facebook/selfupdate2/SelfUpdatePreferencesCreator$StartSelfUpdateTask$2;-><init>(Lcom/facebook/selfupdate2/SelfUpdatePreferencesCreator$StartSelfUpdateTask;)V

    const v2, -0x424a6a5d

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 2486666
    const-string v0, "Resets any timeouts and starts a selfupdate immediately."

    invoke-direct {p0, v0}, Lcom/facebook/selfupdate2/SelfUpdatePreferencesCreator$StartSelfUpdateTask;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 2486667
    :goto_1
    return-void

    .line 2486668
    :catch_0
    move-exception v0

    .line 2486669
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error! "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/selfupdate2/SelfUpdatePreferencesCreator$StartSelfUpdateTask;->a(Ljava/lang/String;)V

    goto :goto_1
.end method
