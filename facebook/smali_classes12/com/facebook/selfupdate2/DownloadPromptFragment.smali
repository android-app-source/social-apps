.class public Lcom/facebook/selfupdate2/DownloadPromptFragment;
.super Lcom/facebook/selfupdate2/SelfUpdateFragment;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public a:Ljava/lang/String;
    .annotation runtime Lcom/facebook/selfupdate2/AppNameForSelfUpdate;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/Hbs;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:Landroid/widget/Button;

.field private g:Landroid/widget/Button;

.field private h:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2486548
    invoke-direct {p0}, Lcom/facebook/selfupdate2/SelfUpdateFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2486513
    invoke-super {p0, p1}, Lcom/facebook/selfupdate2/SelfUpdateFragment;->a(Landroid/os/Bundle;)V

    .line 2486514
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/selfupdate2/DownloadPromptFragment;

    invoke-static {v0}, LX/1wj;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object p1

    check-cast p1, Landroid/content/res/Resources;

    invoke-static {v0}, LX/Hbs;->b(LX/0QB;)LX/Hbs;

    move-result-object v0

    check-cast v0, LX/Hbs;

    iput-object v2, p0, Lcom/facebook/selfupdate2/DownloadPromptFragment;->a:Ljava/lang/String;

    iput-object p1, p0, Lcom/facebook/selfupdate2/DownloadPromptFragment;->b:Landroid/content/res/Resources;

    iput-object v0, p0, Lcom/facebook/selfupdate2/DownloadPromptFragment;->c:LX/Hbs;

    .line 2486515
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 13
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/16 v0, 0x2a

    const v1, -0x38667211

    invoke-static {v10, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2486540
    invoke-super {p0, p1}, Lcom/facebook/selfupdate2/SelfUpdateFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2486541
    invoke-static {p0}, Lcom/facebook/selfupdate2/SelfUpdateFragment;->c(Lcom/facebook/selfupdate2/SelfUpdateFragment;)Lcom/facebook/appupdate/ReleaseInfo;

    move-result-object v11

    .line 2486542
    invoke-static {p0}, Lcom/facebook/selfupdate2/SelfUpdateFragment;->d(Lcom/facebook/selfupdate2/SelfUpdateFragment;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 2486543
    iget-wide v11, v11, Lcom/facebook/appupdate/ReleaseInfo;->bsDiffDownloadSize:J

    .line 2486544
    :goto_0
    move-wide v2, v11

    .line 2486545
    iget-object v1, p0, Lcom/facebook/selfupdate2/DownloadPromptFragment;->h:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/facebook/selfupdate2/DownloadPromptFragment;->b:Landroid/content/res/Resources;

    const v5, 0x7f083655

    new-array v6, v9, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/facebook/selfupdate2/DownloadPromptFragment;->a:Ljava/lang/String;

    aput-object v7, v6, v8

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2486546
    iget-object v1, p0, Lcom/facebook/selfupdate2/DownloadPromptFragment;->d:Landroid/widget/Button;

    iget-object v4, p0, Lcom/facebook/selfupdate2/DownloadPromptFragment;->b:Landroid/content/res/Resources;

    const v5, 0x7f083656

    new-array v6, v9, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/facebook/selfupdate2/SelfUpdateFragment;->a(J)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v6, v8

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 2486547
    const/16 v1, 0x2b

    const v2, 0x49541c31

    invoke-static {v10, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_0
    iget-wide v11, v11, Lcom/facebook/appupdate/ReleaseInfo;->downloadSize:J

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x106795b3

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2486524
    iget-object v1, p0, Lcom/facebook/selfupdate2/SelfUpdateFragment;->e:LX/1wh;

    invoke-virtual {v1}, LX/1wh;->a()V

    .line 2486525
    iget-object v1, p0, Lcom/facebook/selfupdate2/DownloadPromptFragment;->d:Landroid/widget/Button;

    if-ne p1, v1, :cond_1

    .line 2486526
    const-string v1, "selfupdate2_download_click"

    invoke-virtual {p0, v1}, Lcom/facebook/selfupdate2/SelfUpdateFragment;->a(Ljava/lang/String;)V

    .line 2486527
    iget-object v1, p0, Lcom/facebook/selfupdate2/SelfUpdateFragment;->f:Lcom/facebook/selfupdate2/SelfUpdateActivity;

    .line 2486528
    iget-boolean v2, v1, Lcom/facebook/selfupdate2/SelfUpdateActivity;->z:Z

    move v1, v2

    .line 2486529
    if-eqz v1, :cond_0

    .line 2486530
    iget-object v1, p0, Lcom/facebook/selfupdate2/SelfUpdateFragment;->f:Lcom/facebook/selfupdate2/SelfUpdateActivity;

    invoke-virtual {v1}, Lcom/facebook/selfupdate2/SelfUpdateActivity;->l()LX/EeS;

    move-result-object v1

    .line 2486531
    invoke-virtual {v1}, LX/EeS;->b()Z

    .line 2486532
    :goto_0
    const v1, 0x3937e4b

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2486533
    :cond_0
    iget-object v1, p0, Lcom/facebook/selfupdate2/SelfUpdateFragment;->f:Lcom/facebook/selfupdate2/SelfUpdateActivity;

    .line 2486534
    iget-object v2, v1, Lcom/facebook/selfupdate2/SelfUpdateActivity;->B:LX/EeS;

    move-object v1, v2

    .line 2486535
    invoke-virtual {v1}, LX/EeS;->b()Z

    goto :goto_0

    .line 2486536
    :cond_1
    iget-object v1, p0, Lcom/facebook/selfupdate2/DownloadPromptFragment;->g:Landroid/widget/Button;

    if-ne p1, v1, :cond_2

    .line 2486537
    const-string v1, "selfupdate2_remind_me_later_click"

    invoke-virtual {p0, v1}, Lcom/facebook/selfupdate2/SelfUpdateFragment;->a(Ljava/lang/String;)V

    .line 2486538
    iget-object v1, p0, Lcom/facebook/selfupdate2/SelfUpdateFragment;->f:Lcom/facebook/selfupdate2/SelfUpdateActivity;

    invoke-virtual {v1}, Lcom/facebook/selfupdate2/SelfUpdateActivity;->finish()V

    goto :goto_0

    .line 2486539
    :cond_2
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected click event on element: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    const v2, 0x18901fed

    invoke-static {v2, v0}, LX/02F;->a(II)V

    throw v1
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x75b2b4e1

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2486516
    const v0, 0x7f030bf6

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2486517
    const v0, 0x7f0d0eb6

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/facebook/selfupdate2/DownloadPromptFragment;->c:LX/Hbs;

    invoke-virtual {v3}, LX/Hbs;->a()I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2486518
    const v0, 0x7f0d0550

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/selfupdate2/DownloadPromptFragment;->h:Landroid/widget/TextView;

    .line 2486519
    const v0, 0x7f0d1db2

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/selfupdate2/DownloadPromptFragment;->d:Landroid/widget/Button;

    .line 2486520
    const v0, 0x7f0d1db3

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/selfupdate2/DownloadPromptFragment;->g:Landroid/widget/Button;

    .line 2486521
    iget-object v0, p0, Lcom/facebook/selfupdate2/DownloadPromptFragment;->d:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2486522
    iget-object v0, p0, Lcom/facebook/selfupdate2/DownloadPromptFragment;->g:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2486523
    const/16 v0, 0x2b

    const v3, 0x50bcd93c

    invoke-static {v4, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method
