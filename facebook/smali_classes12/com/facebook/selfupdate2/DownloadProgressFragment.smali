.class public Lcom/facebook/selfupdate2/DownloadProgressFragment;
.super Lcom/facebook/selfupdate2/SelfUpdateFragment;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public a:Ljava/lang/String;
    .annotation runtime Lcom/facebook/selfupdate2/AppNameForSelfUpdate;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1wr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private g:Landroid/view/View;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/ProgressBar;

.field private j:Landroid/widget/TextView;

.field private k:Landroid/widget/Button;

.field private final l:LX/1sX;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2486446
    invoke-direct {p0}, Lcom/facebook/selfupdate2/SelfUpdateFragment;-><init>()V

    .line 2486447
    new-instance v0, LX/Hbt;

    invoke-direct {v0, p0}, LX/Hbt;-><init>(Lcom/facebook/selfupdate2/DownloadProgressFragment;)V

    iput-object v0, p0, Lcom/facebook/selfupdate2/DownloadProgressFragment;->l:LX/1sX;

    return-void
.end method

.method private a(FIILjava/lang/Integer;)Ljava/lang/String;
    .locals 5
    .param p4    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/high16 v1, 0x41f00000    # 30.0f

    .line 2486505
    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    cmpg-float v0, p1, v1

    if-gtz v0, :cond_0

    .line 2486506
    iget-object v0, p0, Lcom/facebook/selfupdate2/DownloadProgressFragment;->d:Landroid/content/res/Resources;

    float-to-int v1, p1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    float-to-int v4, p1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, p2, v1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2486507
    :goto_0
    return-object v0

    .line 2486508
    :cond_0
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_1

    cmpg-float v0, p1, v1

    if-gtz v0, :cond_1

    .line 2486509
    iget-object v0, p0, Lcom/facebook/selfupdate2/DownloadProgressFragment;->d:Landroid/content/res/Resources;

    invoke-virtual {v0, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2486510
    :cond_1
    if-eqz p4, :cond_2

    .line 2486511
    iget-object v0, p0, Lcom/facebook/selfupdate2/DownloadProgressFragment;->d:Landroid/content/res/Resources;

    invoke-virtual {p4}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2486512
    :cond_2
    const-string v0, ""

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/selfupdate2/DownloadProgressFragment;LX/EeX;)V
    .locals 12

    .prologue
    .line 2486493
    iget-wide v0, p1, LX/EeX;->downloadProgress:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 2486494
    iget-object v0, p0, Lcom/facebook/selfupdate2/DownloadProgressFragment;->h:Landroid/widget/TextView;

    const v1, 0x7f08365c

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2486495
    iget-object v0, p0, Lcom/facebook/selfupdate2/DownloadProgressFragment;->i:Landroid/widget/ProgressBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 2486496
    :goto_0
    return-void

    .line 2486497
    :cond_0
    iget-wide v8, p1, LX/EeX;->downloadSize:J

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-gtz v8, :cond_1

    .line 2486498
    const/4 v8, 0x0

    .line 2486499
    :goto_1
    move v0, v8

    .line 2486500
    iget-object v1, p0, Lcom/facebook/selfupdate2/DownloadProgressFragment;->h:Landroid/widget/TextView;

    invoke-static {}, Ljava/text/NumberFormat;->getPercentInstance()Ljava/text/NumberFormat;

    move-result-object v2

    int-to-double v4, v0

    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    div-double/2addr v4, v6

    invoke-virtual {v2, v4, v5}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2486501
    iget-object v1, p0, Lcom/facebook/selfupdate2/DownloadProgressFragment;->i:Landroid/widget/ProgressBar;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 2486502
    iget-object v1, p0, Lcom/facebook/selfupdate2/DownloadProgressFragment;->i:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 2486503
    invoke-virtual {p1}, LX/EeX;->e()F

    move-result v0

    const v1, 0x7f0f016d

    const v2, 0x7f08365d

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/facebook/selfupdate2/DownloadProgressFragment;->a(FIILjava/lang/Integer;)Ljava/lang/String;

    move-result-object v0

    .line 2486504
    iget-object v1, p0, Lcom/facebook/selfupdate2/DownloadProgressFragment;->j:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget-wide v8, p1, LX/EeX;->downloadProgress:J

    const-wide/16 v10, 0x64

    mul-long/2addr v8, v10

    iget-wide v10, p1, LX/EeX;->downloadSize:J

    div-long/2addr v8, v10

    long-to-int v8, v8

    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2486490
    invoke-super {p0, p1}, Lcom/facebook/selfupdate2/SelfUpdateFragment;->a(Landroid/os/Bundle;)V

    .line 2486491
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/selfupdate2/DownloadProgressFragment;

    invoke-static {v0}, LX/1wj;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v3

    check-cast v3, Landroid/os/Handler;

    invoke-static {v0}, LX/1wr;->b(LX/0QB;)LX/1wr;

    move-result-object p1

    check-cast p1, LX/1wr;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    iput-object v2, p0, Lcom/facebook/selfupdate2/DownloadProgressFragment;->a:Ljava/lang/String;

    iput-object v3, p0, Lcom/facebook/selfupdate2/DownloadProgressFragment;->b:Landroid/os/Handler;

    iput-object p1, p0, Lcom/facebook/selfupdate2/DownloadProgressFragment;->c:LX/1wr;

    iput-object v0, p0, Lcom/facebook/selfupdate2/DownloadProgressFragment;->d:Landroid/content/res/Resources;

    .line 2486492
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x2a8fc65d

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2486471
    iget-object v0, p0, Lcom/facebook/selfupdate2/DownloadProgressFragment;->g:Landroid/view/View;

    if-ne p1, v0, :cond_0

    .line 2486472
    const-string v0, "selfupdate2_back_to_facebook_download_progress_click"

    invoke-virtual {p0, v0}, Lcom/facebook/selfupdate2/SelfUpdateFragment;->a(Ljava/lang/String;)V

    .line 2486473
    iget-object v0, p0, Lcom/facebook/selfupdate2/SelfUpdateFragment;->f:Lcom/facebook/selfupdate2/SelfUpdateActivity;

    invoke-virtual {v0}, Lcom/facebook/selfupdate2/SelfUpdateActivity;->finish()V

    .line 2486474
    :goto_0
    const v0, -0x56fc0f9c

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 2486475
    :cond_0
    iget-object v0, p0, Lcom/facebook/selfupdate2/DownloadProgressFragment;->k:Landroid/widget/Button;

    if-ne p1, v0, :cond_1

    .line 2486476
    new-instance v0, LX/0ju;

    iget-object v2, p0, Lcom/facebook/selfupdate2/SelfUpdateFragment;->f:Lcom/facebook/selfupdate2/SelfUpdateActivity;

    invoke-direct {v0, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 2486477
    invoke-virtual {p0, v4}, Landroid/support/v4/app/Fragment;->getLayoutInflater(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 2486478
    const v3, 0x7f030bf4

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 2486479
    invoke-virtual {v0, v2}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    .line 2486480
    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v3

    .line 2486481
    iget-object v0, p0, Lcom/facebook/selfupdate2/SelfUpdateFragment;->f:Lcom/facebook/selfupdate2/SelfUpdateActivity;

    .line 2486482
    iget-object v4, v0, Lcom/facebook/selfupdate2/SelfUpdateActivity;->B:LX/EeS;

    move-object v0, v4

    .line 2486483
    invoke-virtual {v0}, LX/EeS;->e()LX/EeX;

    move-result-object v0

    invoke-virtual {v0}, LX/EeX;->e()F

    move-result v0

    const v4, 0x7f0f016e

    const v5, 0x7f083661

    const v6, 0x7f083660

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-direct {p0, v0, v4, v5, v6}, Lcom/facebook/selfupdate2/DownloadProgressFragment;->a(FIILjava/lang/Integer;)Ljava/lang/String;

    move-result-object v4

    .line 2486484
    const-string v0, "selfupdate2_cancel_click"

    invoke-virtual {p0, v0}, Lcom/facebook/selfupdate2/SelfUpdateFragment;->a(Ljava/lang/String;)V

    .line 2486485
    const v0, 0x7f0d0578

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2486486
    const v0, 0x7f0d0c17

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v4, LX/Hbu;

    invoke-direct {v4, p0, v3}, LX/Hbu;-><init>(Lcom/facebook/selfupdate2/DownloadProgressFragment;LX/2EJ;)V

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2486487
    const v0, 0x7f0d1dae

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v2, LX/Hbv;

    invoke-direct {v2, p0, v3}, LX/Hbv;-><init>(Lcom/facebook/selfupdate2/DownloadProgressFragment;LX/2EJ;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2486488
    invoke-virtual {v3}, LX/2EJ;->show()V

    goto :goto_0

    .line 2486489
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected click event on element: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    const v2, 0x321eb2be

    invoke-static {v2, v1}, LX/02F;->a(II)V

    throw v0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x0

    const/16 v0, 0x2a

    const v1, 0x34462d63

    invoke-static {v8, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2486461
    const v0, 0x7f030bf7

    invoke-virtual {p1, v0, p2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2486462
    const v0, 0x7f0d1daf

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/facebook/selfupdate2/DownloadProgressFragment;->d:Landroid/content/res/Resources;

    const v4, 0x7f083658

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/facebook/selfupdate2/DownloadProgressFragment;->a:Ljava/lang/String;

    aput-object v6, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2486463
    const v0, 0x7f0d0508

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/selfupdate2/DownloadProgressFragment;->g:Landroid/view/View;

    .line 2486464
    const v0, 0x7f0d1db4

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/selfupdate2/DownloadProgressFragment;->h:Landroid/widget/TextView;

    .line 2486465
    const v0, 0x7f0d0e9d

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/selfupdate2/DownloadProgressFragment;->i:Landroid/widget/ProgressBar;

    .line 2486466
    const v0, 0x7f0d1db5

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/selfupdate2/DownloadProgressFragment;->j:Landroid/widget/TextView;

    .line 2486467
    const v0, 0x7f0d0c17

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/selfupdate2/DownloadProgressFragment;->k:Landroid/widget/Button;

    .line 2486468
    iget-object v0, p0, Lcom/facebook/selfupdate2/DownloadProgressFragment;->g:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2486469
    iget-object v0, p0, Lcom/facebook/selfupdate2/DownloadProgressFragment;->k:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2486470
    const/16 v0, 0x2b

    const v3, -0x5a76bd87

    invoke-static {v8, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x6dd5be9c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2486456
    invoke-super {p0}, Lcom/facebook/selfupdate2/SelfUpdateFragment;->onPause()V

    .line 2486457
    iget-object v1, p0, Lcom/facebook/selfupdate2/SelfUpdateFragment;->f:Lcom/facebook/selfupdate2/SelfUpdateActivity;

    .line 2486458
    iget-object v2, v1, Lcom/facebook/selfupdate2/SelfUpdateActivity;->B:LX/EeS;

    move-object v1, v2

    .line 2486459
    iget-object v2, p0, Lcom/facebook/selfupdate2/DownloadProgressFragment;->l:LX/1sX;

    invoke-virtual {v1, v2}, LX/EeS;->b(LX/1sX;)Z

    .line 2486460
    const/16 v1, 0x2b

    const v2, -0x35c4b903

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x4b9fc37

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2486448
    invoke-super {p0}, Lcom/facebook/selfupdate2/SelfUpdateFragment;->onResume()V

    .line 2486449
    iget-object v1, p0, Lcom/facebook/selfupdate2/SelfUpdateFragment;->f:Lcom/facebook/selfupdate2/SelfUpdateActivity;

    .line 2486450
    iget-object v2, v1, Lcom/facebook/selfupdate2/SelfUpdateActivity;->B:LX/EeS;

    move-object v1, v2

    .line 2486451
    invoke-virtual {v1}, LX/EeS;->e()LX/EeX;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/selfupdate2/DownloadProgressFragment;->a$redex0(Lcom/facebook/selfupdate2/DownloadProgressFragment;LX/EeX;)V

    .line 2486452
    iget-object v1, p0, Lcom/facebook/selfupdate2/SelfUpdateFragment;->f:Lcom/facebook/selfupdate2/SelfUpdateActivity;

    .line 2486453
    iget-object v2, v1, Lcom/facebook/selfupdate2/SelfUpdateActivity;->B:LX/EeS;

    move-object v1, v2

    .line 2486454
    iget-object v2, p0, Lcom/facebook/selfupdate2/DownloadProgressFragment;->l:LX/1sX;

    invoke-virtual {v1, v2}, LX/EeS;->a(LX/1sX;)Z

    .line 2486455
    const/16 v1, 0x2b

    const v2, -0x63fce056

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
