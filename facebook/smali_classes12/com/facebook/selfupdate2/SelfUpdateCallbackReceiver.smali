.class public Lcom/facebook/selfupdate2/SelfUpdateCallbackReceiver;
.super Landroid/content/BroadcastReceiver;
.source ""

# interfaces
.implements LX/0Ya;


# static fields
.field private static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1wi;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2486595
    const-class v0, Lcom/facebook/selfupdate2/SelfUpdateCallbackReceiver;

    sput-object v0, Lcom/facebook/selfupdate2/SelfUpdateCallbackReceiver;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2486594
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/selfupdate2/SelfUpdateCallbackReceiver;

    const/16 v1, 0x11df

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/selfupdate2/SelfUpdateCallbackReceiver;->a:LX/0Ot;

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x26

    const v2, 0x309e848

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2486578
    invoke-static {p0, p1}, Lcom/facebook/selfupdate2/SelfUpdateCallbackReceiver;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2486579
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    .line 2486580
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const/4 v0, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_1

    .line 2486581
    :goto_1
    const v0, -0x1ad90ca7

    invoke-static {p2, v0, v1}, LX/02F;->a(Landroid/content/Intent;II)V

    return-void

    .line 2486582
    :pswitch_0
    const-string v3, "action_show_download_reminder"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 2486583
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/selfupdate2/SelfUpdateCallbackReceiver;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1wi;

    const/4 v6, 0x1

    const/4 p1, 0x0

    .line 2486584
    new-instance v2, Landroid/content/Intent;

    iget-object v3, v0, LX/1wi;->a:Landroid/content/Context;

    const-class v4, Lcom/facebook/selfupdate2/SelfUpdateActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2486585
    const-string v3, "use_release_info"

    invoke-virtual {v2, v3, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2486586
    iget-object v3, v0, LX/1wi;->a:Landroid/content/Context;

    const/high16 v4, 0x10000000

    invoke-static {v3, p1, v2, v4}, LX/0nt;->a(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 2486587
    new-instance v3, LX/2HB;

    iget-object v4, v0, LX/1wi;->a:Landroid/content/Context;

    invoke-direct {v3, v4}, LX/2HB;-><init>(Landroid/content/Context;)V

    .line 2486588
    const v4, 0x7f0217fe

    invoke-virtual {v3, v4}, LX/2HB;->a(I)LX/2HB;

    .line 2486589
    iget-object v4, v0, LX/1wi;->c:Landroid/content/res/Resources;

    const v5, 0x7f08366b

    new-array v6, v6, [Ljava/lang/Object;

    iget-object p0, v0, LX/1wi;->d:Ljava/lang/String;

    aput-object p0, v6, p1

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    .line 2486590
    iget-object v4, v0, LX/1wi;->c:Landroid/content/res/Resources;

    const v5, 0x7f08366e

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    .line 2486591
    iput-object v2, v3, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 2486592
    iget-object v2, v0, LX/1wi;->b:Landroid/app/NotificationManager;

    const-string v4, "selfupdate_notification"

    invoke-virtual {v3}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v3

    invoke-virtual {v2, v4, p1, v3}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 2486593
    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x2bde5b0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
