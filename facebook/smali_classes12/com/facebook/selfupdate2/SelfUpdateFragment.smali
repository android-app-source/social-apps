.class public abstract Lcom/facebook/selfupdate2/SelfUpdateFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public e:LX/1wh;

.field public f:Lcom/facebook/selfupdate2/SelfUpdateActivity;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2486366
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method public static a(J)I
    .locals 4

    .prologue
    const-wide/16 v2, 0x400

    .line 2486367
    div-long v0, p0, v2

    div-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public static c(Lcom/facebook/selfupdate2/SelfUpdateFragment;)Lcom/facebook/appupdate/ReleaseInfo;
    .locals 2

    .prologue
    .line 2486368
    iget-object v0, p0, Lcom/facebook/selfupdate2/SelfUpdateFragment;->f:Lcom/facebook/selfupdate2/SelfUpdateActivity;

    .line 2486369
    iget-boolean v1, v0, Lcom/facebook/selfupdate2/SelfUpdateActivity;->z:Z

    move v0, v1

    .line 2486370
    if-eqz v0, :cond_0

    .line 2486371
    iget-object v0, p0, Lcom/facebook/selfupdate2/SelfUpdateFragment;->f:Lcom/facebook/selfupdate2/SelfUpdateActivity;

    .line 2486372
    iget-object v1, v0, Lcom/facebook/selfupdate2/SelfUpdateActivity;->A:Lcom/facebook/appupdate/ReleaseInfo;

    move-object v0, v1

    .line 2486373
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/selfupdate2/SelfUpdateFragment;->f:Lcom/facebook/selfupdate2/SelfUpdateActivity;

    .line 2486374
    iget-object v1, v0, Lcom/facebook/selfupdate2/SelfUpdateActivity;->B:LX/EeS;

    move-object v0, v1

    .line 2486375
    invoke-virtual {v0}, LX/EeS;->e()LX/EeX;

    move-result-object v0

    iget-object v0, v0, LX/EeX;->releaseInfo:Lcom/facebook/appupdate/ReleaseInfo;

    goto :goto_0
.end method

.method public static d(Lcom/facebook/selfupdate2/SelfUpdateFragment;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2486376
    invoke-static {p0}, Lcom/facebook/selfupdate2/SelfUpdateFragment;->c(Lcom/facebook/selfupdate2/SelfUpdateFragment;)Lcom/facebook/appupdate/ReleaseInfo;

    move-result-object v1

    .line 2486377
    iget-object v2, p0, Lcom/facebook/selfupdate2/SelfUpdateFragment;->f:Lcom/facebook/selfupdate2/SelfUpdateActivity;

    iget-object v2, v2, Lcom/facebook/selfupdate2/SelfUpdateActivity;->v:LX/0Uh;

    const/16 v3, 0x491

    invoke-virtual {v2, v3, v0}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2486378
    invoke-static {}, LX/1wl;->a()LX/1wl;

    move-result-object v2

    invoke-virtual {v2}, LX/1wl;->l()Landroid/content/SharedPreferences;

    move-result-object v2

    .line 2486379
    invoke-static {v2, v1}, LX/Eem;->a(Landroid/content/SharedPreferences;Lcom/facebook/appupdate/ReleaseInfo;)Z

    move-result v2

    move v2, v2

    .line 2486380
    if-nez v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/appupdate/ReleaseInfo;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2486381
    iget-object v0, p0, Lcom/facebook/selfupdate2/SelfUpdateFragment;->e:LX/1wh;

    .line 2486382
    invoke-static {p0}, Lcom/facebook/selfupdate2/SelfUpdateFragment;->c(Lcom/facebook/selfupdate2/SelfUpdateFragment;)Lcom/facebook/appupdate/ReleaseInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/appupdate/ReleaseInfo;->a()Lorg/json/JSONObject;

    move-result-object v1

    .line 2486383
    invoke-static {p0}, Lcom/facebook/selfupdate2/SelfUpdateFragment;->d(Lcom/facebook/selfupdate2/SelfUpdateFragment;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2486384
    const-string v2, "diff_algorithm"

    sget-object v3, LX/Eeh;->BSDIFF:LX/Eeh;

    invoke-virtual {v3}, LX/Eeh;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, LX/Ef2;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)V

    .line 2486385
    :cond_0
    move-object v1, v1

    .line 2486386
    invoke-virtual {v0, p1, v1}, LX/1wh;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 2486387
    iget-object v0, p0, Lcom/facebook/selfupdate2/SelfUpdateFragment;->e:LX/1wh;

    invoke-static {p0}, Lcom/facebook/selfupdate2/SelfUpdateFragment;->c(Lcom/facebook/selfupdate2/SelfUpdateFragment;)Lcom/facebook/appupdate/ReleaseInfo;

    move-result-object v1

    .line 2486388
    invoke-static {p0}, Lcom/facebook/selfupdate2/SelfUpdateFragment;->d(Lcom/facebook/selfupdate2/SelfUpdateFragment;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2486389
    sget-object v2, LX/Eeh;->BSDIFF:LX/Eeh;

    .line 2486390
    :goto_0
    move-object v2, v2

    .line 2486391
    const-string v3, "click"

    invoke-virtual {v0, p1, v1, v2, v3}, LX/1wh;->a(Ljava/lang/String;Lcom/facebook/appupdate/ReleaseInfo;LX/Eeh;Ljava/lang/String;)V

    .line 2486392
    return-void

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x57e307d4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2486393
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2486394
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/facebook/selfupdate2/SelfUpdateActivity;

    iput-object v0, p0, Lcom/facebook/selfupdate2/SelfUpdateFragment;->f:Lcom/facebook/selfupdate2/SelfUpdateActivity;

    .line 2486395
    iget-object v0, p0, Lcom/facebook/selfupdate2/SelfUpdateFragment;->f:Lcom/facebook/selfupdate2/SelfUpdateActivity;

    .line 2486396
    iget-object v2, v0, Lcom/facebook/selfupdate2/SelfUpdateActivity;->q:LX/1wh;

    move-object v0, v2

    .line 2486397
    iput-object v0, p0, Lcom/facebook/selfupdate2/SelfUpdateFragment;->e:LX/1wh;

    .line 2486398
    const/16 v0, 0x2b

    const v2, 0x462ad65d

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
