.class public Lcom/facebook/selfupdate2/DownloadFailedFragment;
.super Lcom/facebook/selfupdate2/SelfUpdateFragment;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public a:Ljava/lang/String;
    .annotation runtime Lcom/facebook/selfupdate2/AppNameForSelfUpdate;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1wr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:Landroid/view/View;

.field private g:Landroid/widget/Button;

.field private h:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2486399
    invoke-direct {p0}, Lcom/facebook/selfupdate2/SelfUpdateFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2486410
    invoke-super {p0, p1}, Lcom/facebook/selfupdate2/SelfUpdateFragment;->a(Landroid/os/Bundle;)V

    .line 2486411
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/selfupdate2/DownloadFailedFragment;

    invoke-static {v0}, LX/1wj;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object p1

    check-cast p1, Landroid/content/res/Resources;

    invoke-static {v0}, LX/1wr;->b(LX/0QB;)LX/1wr;

    move-result-object v0

    check-cast v0, LX/1wr;

    iput-object v2, p0, Lcom/facebook/selfupdate2/DownloadFailedFragment;->a:Ljava/lang/String;

    iput-object p1, p0, Lcom/facebook/selfupdate2/DownloadFailedFragment;->b:Landroid/content/res/Resources;

    iput-object v0, p0, Lcom/facebook/selfupdate2/DownloadFailedFragment;->c:LX/1wr;

    .line 2486412
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x2c98e852

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2486413
    iget-object v1, p0, Lcom/facebook/selfupdate2/SelfUpdateFragment;->f:Lcom/facebook/selfupdate2/SelfUpdateActivity;

    .line 2486414
    iget-object v2, v1, Lcom/facebook/selfupdate2/SelfUpdateActivity;->B:LX/EeS;

    move-object v1, v2

    .line 2486415
    iget-object v2, p0, Lcom/facebook/selfupdate2/DownloadFailedFragment;->d:Landroid/view/View;

    if-ne p1, v2, :cond_0

    .line 2486416
    const-string v2, "selfupdate2_back_to_facebook_download_failed_click"

    invoke-virtual {p0, v2}, Lcom/facebook/selfupdate2/SelfUpdateFragment;->a(Ljava/lang/String;)V

    .line 2486417
    invoke-virtual {v1}, LX/EeS;->g()Z

    .line 2486418
    iget-object v1, p0, Lcom/facebook/selfupdate2/DownloadFailedFragment;->c:LX/1wr;

    invoke-virtual {v1}, LX/1wr;->a()V

    .line 2486419
    :goto_0
    const v1, -0x46c8064f

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2486420
    :cond_0
    iget-object v2, p0, Lcom/facebook/selfupdate2/DownloadFailedFragment;->g:Landroid/widget/Button;

    if-ne p1, v2, :cond_1

    .line 2486421
    const-string v2, "selfupdate2_retry_download_click"

    invoke-virtual {p0, v2}, Lcom/facebook/selfupdate2/SelfUpdateFragment;->a(Ljava/lang/String;)V

    .line 2486422
    invoke-virtual {v1}, LX/EeS;->c()Z

    goto :goto_0

    .line 2486423
    :cond_1
    iget-object v2, p0, Lcom/facebook/selfupdate2/DownloadFailedFragment;->h:Landroid/widget/Button;

    if-ne p1, v2, :cond_2

    .line 2486424
    const-string v2, "selfupdate2_download_later_click"

    invoke-virtual {p0, v2}, Lcom/facebook/selfupdate2/SelfUpdateFragment;->a(Ljava/lang/String;)V

    .line 2486425
    invoke-virtual {v1}, LX/EeS;->g()Z

    goto :goto_0

    .line 2486426
    :cond_2
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Unexpected click event on element"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    const v2, 0x61e8f200

    invoke-static {v2, v0}, LX/02F;->a(II)V

    throw v1
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/16 v0, 0x2a

    const v1, 0x3fc04dfd

    invoke-static {v9, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2486400
    const v0, 0x7f030bf5

    invoke-virtual {p1, v0, p2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2486401
    const v0, 0x7f0d1daf

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/facebook/selfupdate2/DownloadFailedFragment;->b:Landroid/content/res/Resources;

    const v4, 0x7f083658

    new-array v5, v8, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/facebook/selfupdate2/DownloadFailedFragment;->a:Ljava/lang/String;

    aput-object v6, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2486402
    const v0, 0x7f0d1db0

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/facebook/selfupdate2/DownloadFailedFragment;->b:Landroid/content/res/Resources;

    const v4, 0x7f083665

    new-array v5, v8, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/facebook/selfupdate2/DownloadFailedFragment;->a:Ljava/lang/String;

    aput-object v6, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2486403
    const v0, 0x7f0d0508

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/selfupdate2/DownloadFailedFragment;->d:Landroid/view/View;

    .line 2486404
    const v0, 0x7f0d0556

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/selfupdate2/DownloadFailedFragment;->g:Landroid/widget/Button;

    .line 2486405
    const v0, 0x7f0d1db1

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/selfupdate2/DownloadFailedFragment;->h:Landroid/widget/Button;

    .line 2486406
    iget-object v0, p0, Lcom/facebook/selfupdate2/DownloadFailedFragment;->d:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2486407
    iget-object v0, p0, Lcom/facebook/selfupdate2/DownloadFailedFragment;->g:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2486408
    iget-object v0, p0, Lcom/facebook/selfupdate2/DownloadFailedFragment;->h:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2486409
    const/16 v0, 0x2b

    const v3, -0x18f38bd0

    invoke-static {v9, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method
