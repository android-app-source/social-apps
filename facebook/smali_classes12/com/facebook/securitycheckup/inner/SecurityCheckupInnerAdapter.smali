.class public Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/HbH;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/HbG;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/HbD;


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/HbG;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2485037
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2485038
    iput-object p1, p0, Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;->a:Ljava/util/List;

    .line 2485039
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2485040
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0312ce

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2485041
    new-instance v1, LX/HbH;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/HbH;-><init>(Landroid/view/View;Landroid/content/Context;)V

    .line 2485042
    iget-object v0, p0, Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;->b:LX/HbD;

    if-eqz v0, :cond_0

    .line 2485043
    iget-object v0, p0, Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;->b:LX/HbD;

    .line 2485044
    iput-object v0, v1, LX/HbH;->q:LX/HbD;

    .line 2485045
    :cond_0
    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 4

    .prologue
    .line 2485046
    check-cast p1, LX/HbH;

    .line 2485047
    iget-object v0, p0, Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HbG;

    .line 2485048
    const/16 p0, 0x8

    const/4 v3, 0x0

    .line 2485049
    iput-object v0, p1, LX/HbH;->r:LX/HbG;

    .line 2485050
    iput p2, p1, LX/HbH;->t:I

    .line 2485051
    iget-object v1, p1, LX/HbH;->l:Lcom/facebook/resources/ui/FbTextView;

    .line 2485052
    iget-object v2, v0, LX/HbG;->c:Ljava/lang/String;

    move-object v2, v2

    .line 2485053
    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2485054
    iget-object v1, p1, LX/HbH;->r:LX/HbG;

    .line 2485055
    iget-object v2, v1, LX/HbG;->d:Ljava/lang/String;

    move-object v1, v2

    .line 2485056
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2485057
    iget-object v1, p1, LX/HbH;->m:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, p0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2485058
    :goto_0
    iget-object v1, p1, LX/HbH;->r:LX/HbG;

    .line 2485059
    iget-object v2, v1, LX/HbG;->b:LX/0am;

    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v2

    move v1, v2

    .line 2485060
    if-eqz v1, :cond_2

    .line 2485061
    iget-object v1, p1, LX/HbH;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2485062
    iget-object v1, p1, LX/HbH;->n:Landroid/widget/ImageView;

    invoke-virtual {v1, p0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2485063
    iget-object v1, p1, LX/HbH;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2485064
    iget-object v2, v0, LX/HbG;->b:LX/0am;

    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    move-object v2, v2

    .line 2485065
    const-class v3, LX/HbH;

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2485066
    :cond_0
    :goto_1
    iget-object v1, p1, LX/HbH;->p:Lcom/facebook/securitycheckup/inner/InertCheckBox;

    .line 2485067
    iget-boolean v2, v0, LX/HbG;->e:Z

    move v2, v2

    .line 2485068
    invoke-virtual {v1, v2}, Lcom/facebook/securitycheckup/inner/InertCheckBox;->setChecked(Z)V

    .line 2485069
    return-void

    .line 2485070
    :cond_1
    iget-object v1, p1, LX/HbH;->m:Lcom/facebook/resources/ui/FbTextView;

    .line 2485071
    iget-object v2, v0, LX/HbG;->d:Ljava/lang/String;

    move-object v2, v2

    .line 2485072
    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2485073
    iget-object v1, p1, LX/HbH;->m:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0

    .line 2485074
    :cond_2
    iget-object v1, p1, LX/HbH;->r:LX/HbG;

    .line 2485075
    iget-object v2, v1, LX/HbG;->a:LX/0am;

    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v2

    move v1, v2

    .line 2485076
    if-eqz v1, :cond_0

    .line 2485077
    iget-object v1, p1, LX/HbH;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, p0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2485078
    iget-object v1, p1, LX/HbH;->n:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2485079
    iget-object v1, p1, LX/HbH;->n:Landroid/widget/ImageView;

    iget-object v2, p1, LX/HbH;->s:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 2485080
    iget-object v3, v0, LX/HbG;->a:LX/0am;

    invoke-virtual {v3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    move v3, v3

    .line 2485081
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method

.method public final d()I
    .locals 3

    .prologue
    .line 2485082
    const/4 v0, 0x0

    .line 2485083
    iget-object v1, p0, Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HbG;

    .line 2485084
    iget-boolean p0, v0, LX/HbG;->e:Z

    move v0, p0

    .line 2485085
    if-eqz v0, :cond_1

    .line 2485086
    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    .line 2485087
    goto :goto_0

    .line 2485088
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final e(I)LX/HbG;
    .locals 1

    .prologue
    .line 2485089
    iget-object v0, p0, Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2485090
    iget-object v0, p0, Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HbG;

    return-object v0

    .line 2485091
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2485092
    iget-object v0, p0, Lcom/facebook/securitycheckup/inner/SecurityCheckupInnerAdapter;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
