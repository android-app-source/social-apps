.class public final Lcom/facebook/securitycheckup/items/SecurityCheckupConclusionViewHolder$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/HbO;


# direct methods
.method public constructor <init>(LX/HbO;)V
    .locals 0

    .prologue
    .line 2485412
    iput-object p1, p0, Lcom/facebook/securitycheckup/items/SecurityCheckupConclusionViewHolder$3;->a:LX/HbO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 2485413
    iget-object v0, p0, Lcom/facebook/securitycheckup/items/SecurityCheckupConclusionViewHolder$3;->a:LX/HbO;

    .line 2485414
    iget-object v1, v0, LX/HbN;->m:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 2485415
    iget-object v2, v0, LX/HbO;->u:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v2

    iget-object p0, v0, LX/HbO;->p:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/resources/ui/FbTextView;->getHeight()I

    move-result p0

    add-int/2addr v2, p0

    const/16 p0, 0x8

    invoke-static {v1, p0}, LX/Hbr;->a(Landroid/util/DisplayMetrics;I)I

    move-result p0

    add-int/2addr v2, p0

    iget-object p0, v0, LX/HbO;->q:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/resources/ui/FbTextView;->getHeight()I

    move-result p0

    add-int/2addr v2, p0

    iget-object p0, v0, LX/HbO;->r:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {p0}, Lcom/facebook/resources/ui/FbButton;->getHeight()I

    move-result p0

    add-int/2addr v2, p0

    iget-object p0, v0, LX/HbO;->s:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {p0}, Lcom/facebook/resources/ui/FbButton;->getHeight()I

    move-result p0

    add-int/2addr v2, p0

    .line 2485416
    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    sub-int/2addr v1, v2

    .line 2485417
    int-to-float v1, v1

    const/high16 v2, 0x40e00000    # 7.0f

    div-float/2addr v1, v2

    float-to-int v1, v1

    move v2, v1

    .line 2485418
    iget-object v1, v0, LX/HbO;->u:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 2485419
    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 2485420
    mul-int/lit8 p0, v2, 0x2

    iput p0, v1, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 2485421
    iget-object p0, v0, LX/HbO;->u:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2485422
    iget-object v1, v0, LX/HbO;->r:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 2485423
    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 2485424
    iget-object p0, v0, LX/HbO;->r:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {p0, v1}, Lcom/facebook/resources/ui/FbButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2485425
    iget-object v1, v0, LX/HbO;->s:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 2485426
    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 2485427
    iget-object v2, v0, LX/HbO;->s:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2485428
    return-void
.end method
