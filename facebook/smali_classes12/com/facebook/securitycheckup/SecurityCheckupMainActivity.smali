.class public Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# static fields
.field public static final p:Ljava/lang/String;


# instance fields
.field private q:Ljava/util/concurrent/ExecutorService;

.field private r:LX/0tX;

.field private s:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field private t:Lcom/facebook/fbui/glyph/GlyphButton;

.field private u:Landroid/widget/ProgressBar;

.field public v:LX/Hag;

.field private w:LX/Hah;

.field private x:LX/HaZ;

.field private y:Z

.field private z:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2484157
    const-class v0, Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;->p:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2484156
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 6

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;

    invoke-static {v5}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ExecutorService;

    invoke-static {v5}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    const-class v3, LX/Hah;

    invoke-interface {v5, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/Hah;

    invoke-static {v5}, LX/HaZ;->a(LX/0QB;)LX/HaZ;

    move-result-object v4

    check-cast v4, LX/HaZ;

    invoke-static {v5}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-direct/range {v0 .. v5}, Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;->a(Ljava/util/concurrent/ExecutorService;LX/0tX;LX/Hah;LX/HaZ;Lcom/facebook/content/SecureContextHelper;)V

    return-void
.end method

.method private a(Ljava/util/concurrent/ExecutorService;LX/0tX;LX/Hah;LX/HaZ;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2484143
    iput-object p1, p0, Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;->q:Ljava/util/concurrent/ExecutorService;

    .line 2484144
    iput-object p2, p0, Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;->r:LX/0tX;

    .line 2484145
    iput-object p3, p0, Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;->w:LX/Hah;

    .line 2484146
    iput-object p4, p0, Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;->x:LX/HaZ;

    .line 2484147
    iput-object p5, p0, Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;->z:Lcom/facebook/content/SecureContextHelper;

    .line 2484148
    return-void
.end method

.method public static a$redex0(Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;)V
    .locals 10

    .prologue
    .line 2484149
    iget-object v0, p0, Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;->x:LX/HaZ;

    const-string v1, "CHECKUP_LOAD_COMPLETE"

    invoke-virtual {v0, v1}, LX/HaZ;->a(Ljava/lang/String;)V

    .line 2484150
    iget-object v0, p0, Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;->w:LX/Hah;

    iget-object v1, p0, Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;->s:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v2, p0, Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;->t:Lcom/facebook/fbui/glyph/GlyphButton;

    .line 2484151
    new-instance v3, LX/Hag;

    const-class v4, LX/HbK;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/HbK;

    invoke-static {v0}, LX/HaZ;->a(LX/0QB;)LX/HaZ;

    move-result-object v9

    check-cast v9, LX/HaZ;

    move-object v4, p0

    move-object v5, p1

    move-object v6, v1

    move-object v7, v2

    invoke-direct/range {v3 .. v9}, LX/Hag;-><init>(Landroid/content/Context;Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;Lcom/facebook/widget/recyclerview/BetterRecyclerView;Lcom/facebook/fbui/glyph/GlyphButton;LX/HbK;LX/HaZ;)V

    .line 2484152
    move-object v0, v3

    .line 2484153
    iput-object v0, p0, Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;->v:LX/Hag;

    .line 2484154
    new-instance v0, Lcom/facebook/securitycheckup/SecurityCheckupMainActivity$2;

    invoke-direct {v0, p0}, Lcom/facebook/securitycheckup/SecurityCheckupMainActivity$2;-><init>(Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;)V

    invoke-virtual {p0, v0}, Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2484155
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 2484137
    iget-object v0, p0, Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;->x:LX/HaZ;

    const-string v1, "CHECKUP_LOAD_START"

    invoke-virtual {v0, v1}, LX/HaZ;->a(Ljava/lang/String;)V

    .line 2484138
    iget-object v0, p0, Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;->r:LX/0tX;

    .line 2484139
    new-instance v1, LX/Haz;

    invoke-direct {v1}, LX/Haz;-><init>()V

    move-object v1, v1

    .line 2484140
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    sget-object v2, LX/0zS;->c:LX/0zS;

    invoke-virtual {v1, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2484141
    new-instance v1, LX/Haa;

    invoke-direct {v1, p0}, LX/Haa;-><init>(Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;)V

    iget-object v2, p0, Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;->q:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2484142
    return-void
.end method

.method public static l(Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2484133
    iget-object v0, p0, Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;->s:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 2484134
    iget-object v0, p0, Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;->t:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    .line 2484135
    iget-object v0, p0, Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;->u:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2484136
    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2484103
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2484104
    invoke-static {p0, p0}, Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2484105
    invoke-virtual {p0}, Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x2000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 2484106
    const v0, 0x7f0312d6

    invoke-virtual {p0, v0}, Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;->setContentView(I)V

    .line 2484107
    const v0, 0x7f0d2be4

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;->s:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2484108
    const v0, 0x7f0d2be5

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphButton;

    iput-object v0, p0, Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;->t:Lcom/facebook/fbui/glyph/GlyphButton;

    .line 2484109
    const v0, 0x7f0d2be6

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;->u:Landroid/widget/ProgressBar;

    .line 2484110
    const-string v1, "QP"

    .line 2484111
    invoke-virtual {p0}, Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2484112
    if-eqz v0, :cond_0

    .line 2484113
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    .line 2484114
    if-eqz v2, :cond_0

    .line 2484115
    const-string v0, "source"

    invoke-virtual {v2, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2484116
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2484117
    :goto_0
    const-string v1, "redirect_to_feed"

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/net/Uri;->getBooleanQueryParameter(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;->y:Z

    move-object v1, v0

    .line 2484118
    :cond_0
    iget-object v0, p0, Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;->x:LX/HaZ;

    .line 2484119
    iput-object v1, v0, LX/HaZ;->b:Ljava/lang/String;

    .line 2484120
    invoke-direct {p0}, Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;->b()V

    .line 2484121
    return-void

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final onBackPressed()V
    .locals 3

    .prologue
    .line 2484122
    iget-object v0, p0, Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;->v:LX/Hag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;->v:LX/Hag;

    .line 2484123
    iget-object v1, v0, LX/Hag;->e:LX/Hac;

    move-object v0, v1

    .line 2484124
    sget-object v1, LX/Hac;->EXPAND_COMPLETED:LX/Hac;

    if-ne v0, v1, :cond_0

    .line 2484125
    iget-object v0, p0, Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;->v:LX/Hag;

    invoke-virtual {v0}, LX/Hag;->c()V

    .line 2484126
    :goto_0
    return-void

    .line 2484127
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;->y:Z

    if-eqz v0, :cond_1

    .line 2484128
    sget-object v0, LX/0ax;->cL:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2484129
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 2484130
    iget-object v1, p0, Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;->z:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2484131
    invoke-virtual {p0}, Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;->finish()V

    goto :goto_0

    .line 2484132
    :cond_1
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    goto :goto_0
.end method
