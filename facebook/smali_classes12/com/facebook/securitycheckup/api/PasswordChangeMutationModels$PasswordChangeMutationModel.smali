.class public final Lcom/facebook/securitycheckup/api/PasswordChangeMutationModels$PasswordChangeMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x28535e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/securitycheckup/api/PasswordChangeMutationModels$PasswordChangeMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/securitycheckup/api/PasswordChangeMutationModels$PasswordChangeMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2484493
    const-class v0, Lcom/facebook/securitycheckup/api/PasswordChangeMutationModels$PasswordChangeMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2484492
    const-class v0, Lcom/facebook/securitycheckup/api/PasswordChangeMutationModels$PasswordChangeMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2484490
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2484491
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2484488
    iget-object v0, p0, Lcom/facebook/securitycheckup/api/PasswordChangeMutationModels$PasswordChangeMutationModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/securitycheckup/api/PasswordChangeMutationModels$PasswordChangeMutationModel;->e:Ljava/lang/String;

    .line 2484489
    iget-object v0, p0, Lcom/facebook/securitycheckup/api/PasswordChangeMutationModels$PasswordChangeMutationModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2484482
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2484483
    invoke-direct {p0}, Lcom/facebook/securitycheckup/api/PasswordChangeMutationModels$PasswordChangeMutationModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2484484
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2484485
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2484486
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2484487
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2484474
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2484475
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2484476
    return-object p0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2484479
    new-instance v0, Lcom/facebook/securitycheckup/api/PasswordChangeMutationModels$PasswordChangeMutationModel;

    invoke-direct {v0}, Lcom/facebook/securitycheckup/api/PasswordChangeMutationModels$PasswordChangeMutationModel;-><init>()V

    .line 2484480
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2484481
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2484478
    const v0, -0x1bb6737c

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2484477
    const v0, -0x6da3be

    return v0
.end method
