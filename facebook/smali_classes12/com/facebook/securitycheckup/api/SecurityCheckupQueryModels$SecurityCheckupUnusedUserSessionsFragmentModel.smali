.class public final Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupUnusedUserSessionsFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x55c1b3d4
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupUnusedUserSessionsFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupUnusedUserSessionsFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I

.field private g:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2484891
    const-class v0, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupUnusedUserSessionsFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2484890
    const-class v0, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupUnusedUserSessionsFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2484888
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2484889
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2484877
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2484878
    invoke-virtual {p0}, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupUnusedUserSessionsFragmentModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2484879
    invoke-virtual {p0}, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupUnusedUserSessionsFragmentModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2484880
    invoke-virtual {p0}, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupUnusedUserSessionsFragmentModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2484881
    const/4 v3, 0x4

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2484882
    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 2484883
    const/4 v0, 0x1

    iget v3, p0, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupUnusedUserSessionsFragmentModel;->f:I

    invoke-virtual {p1, v0, v3, v4}, LX/186;->a(III)V

    .line 2484884
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2484885
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2484886
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2484887
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2484869
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2484870
    invoke-virtual {p0}, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupUnusedUserSessionsFragmentModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2484871
    invoke-virtual {p0}, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupUnusedUserSessionsFragmentModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2484872
    invoke-virtual {p0}, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupUnusedUserSessionsFragmentModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2484873
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupUnusedUserSessionsFragmentModel;

    .line 2484874
    iput-object v0, v1, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupUnusedUserSessionsFragmentModel;->g:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2484875
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2484876
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2484853
    iget-object v0, p0, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupUnusedUserSessionsFragmentModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupUnusedUserSessionsFragmentModel;->e:Ljava/lang/String;

    .line 2484854
    iget-object v0, p0, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupUnusedUserSessionsFragmentModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2484866
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2484867
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupUnusedUserSessionsFragmentModel;->f:I

    .line 2484868
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2484863
    new-instance v0, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupUnusedUserSessionsFragmentModel;

    invoke-direct {v0}, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupUnusedUserSessionsFragmentModel;-><init>()V

    .line 2484864
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2484865
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2484862
    const v0, -0x27ed9e78

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2484861
    const v0, -0x206e99d8

    return v0
.end method

.method public final j()I
    .locals 2

    .prologue
    .line 2484859
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2484860
    iget v0, p0, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupUnusedUserSessionsFragmentModel;->f:I

    return v0
.end method

.method public final k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2484857
    iget-object v0, p0, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupUnusedUserSessionsFragmentModel;->g:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    iput-object v0, p0, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupUnusedUserSessionsFragmentModel;->g:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 2484858
    iget-object v0, p0, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupUnusedUserSessionsFragmentModel;->g:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2484855
    iget-object v0, p0, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupUnusedUserSessionsFragmentModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupUnusedUserSessionsFragmentModel;->h:Ljava/lang/String;

    .line 2484856
    iget-object v0, p0, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupUnusedUserSessionsFragmentModel;->h:Ljava/lang/String;

    return-object v0
.end method
