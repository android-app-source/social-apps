.class public final Lcom/facebook/securitycheckup/SecurityCheckupMainActivity$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;)V
    .locals 0

    .prologue
    .line 2484044
    iput-object p1, p0, Lcom/facebook/securitycheckup/SecurityCheckupMainActivity$2;->a:Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 15

    .prologue
    .line 2484045
    iget-object v0, p0, Lcom/facebook/securitycheckup/SecurityCheckupMainActivity$2;->a:Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;

    iget-object v0, v0, Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;->v:LX/Hag;

    const/4 v4, 0x0

    .line 2484046
    iget-object v1, v0, LX/Hag;->j:LX/HbK;

    .line 2484047
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2484048
    new-instance v7, LX/Hbb;

    iget-object v8, v0, LX/Hag;->b:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f083623

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    iget-object v9, v0, LX/Hag;->b:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f083624

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    const-string v10, ""

    sget-object v11, LX/Hba;->INTRO:LX/Hba;

    iget-object v12, v0, LX/Hag;->g:Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;

    invoke-direct/range {v7 .. v12}, LX/Hbb;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Hba;Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;)V

    .line 2484049
    const/4 v8, 0x0

    .line 2484050
    iput-boolean v8, v7, LX/Hbb;->e:Z

    .line 2484051
    move-object v6, v7

    .line 2484052
    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2484053
    const/4 v8, 0x0

    const/4 v14, 0x1

    .line 2484054
    iget-object v7, v0, LX/Hag;->g:Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;

    if-eqz v7, :cond_3

    .line 2484055
    iget-object v7, v0, LX/Hag;->g:Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;

    invoke-virtual {v7}, Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;->j()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v7

    move v13, v7

    .line 2484056
    :goto_0
    iget-object v7, v0, LX/Hag;->b:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    if-nez v13, :cond_1

    const v7, 0x7f083629

    :goto_1
    invoke-virtual {v9, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 2484057
    if-nez v13, :cond_2

    iget-object v7, v0, LX/Hag;->b:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f083650

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 2484058
    :goto_2
    new-instance v7, LX/Hbb;

    iget-object v8, v0, LX/Hag;->b:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v11, 0x7f083625

    invoke-virtual {v8, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    sget-object v11, LX/Hba;->UNUSED_SESSIONS:LX/Hba;

    iget-object v12, v0, LX/Hag;->g:Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;

    invoke-direct/range {v7 .. v12}, LX/Hbb;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Hba;Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;)V

    .line 2484059
    iput-boolean v14, v7, LX/Hbb;->e:Z

    .line 2484060
    if-nez v13, :cond_0

    .line 2484061
    iput-boolean v14, v7, LX/Hbb;->f:Z

    .line 2484062
    :cond_0
    move-object v6, v7

    .line 2484063
    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2484064
    invoke-static {v0}, LX/Hag;->h(LX/Hag;)Z

    move-result v7

    if-eqz v7, :cond_4

    const v7, 0x7f08362e

    move v10, v7

    .line 2484065
    :goto_3
    invoke-static {v0}, LX/Hag;->h(LX/Hag;)Z

    move-result v7

    if-eqz v7, :cond_5

    const v7, 0x7f08362c

    move v9, v7

    .line 2484066
    :goto_4
    new-instance v7, LX/Hbb;

    iget-object v8, v0, LX/Hag;->b:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v11, 0x7f08362a

    invoke-virtual {v8, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    iget-object v11, v0, LX/Hag;->b:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    invoke-virtual {v11, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    iget-object v11, v0, LX/Hag;->b:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    invoke-virtual {v11, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    sget-object v11, LX/Hba;->LOGIN_ALERTS:LX/Hba;

    iget-object v12, v0, LX/Hag;->g:Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;

    invoke-direct/range {v7 .. v12}, LX/Hbb;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Hba;Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;)V

    .line 2484067
    const/4 v8, 0x1

    .line 2484068
    iput-boolean v8, v7, LX/Hbb;->e:Z

    .line 2484069
    move-object v6, v7

    .line 2484070
    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2484071
    new-instance v7, LX/Hbb;

    iget-object v8, v0, LX/Hag;->b:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f08362f

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    iget-object v9, v0, LX/Hag;->b:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f083630

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    iget-object v10, v0, LX/Hag;->b:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f083631

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    sget-object v11, LX/Hba;->PASSWORD:LX/Hba;

    iget-object v12, v0, LX/Hag;->g:Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;

    invoke-direct/range {v7 .. v12}, LX/Hbb;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Hba;Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;)V

    .line 2484072
    const/4 v8, 0x1

    .line 2484073
    iput-boolean v8, v7, LX/Hbb;->e:Z

    .line 2484074
    move-object v6, v7

    .line 2484075
    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2484076
    new-instance v7, LX/Hbb;

    iget-object v8, v0, LX/Hag;->b:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f083632

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    iget-object v9, v0, LX/Hag;->b:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f083633

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    iget-object v10, v0, LX/Hag;->b:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f083634

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    sget-object v11, LX/Hba;->CONCLUSION:LX/Hba;

    iget-object v12, v0, LX/Hag;->g:Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;

    invoke-direct/range {v7 .. v12}, LX/Hbb;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Hba;Lcom/facebook/securitycheckup/api/SecurityCheckupQueryModels$SecurityCheckupQueryModel$SecurityCheckupModel;)V

    .line 2484077
    const/4 v8, 0x1

    .line 2484078
    iput-boolean v8, v7, LX/Hbb;->e:Z

    .line 2484079
    move-object v6, v7

    .line 2484080
    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2484081
    move-object v2, v5

    .line 2484082
    new-instance v6, LX/HbJ;

    const-class v3, LX/HbZ;

    invoke-interface {v1, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/HbZ;

    const-class v5, LX/HbP;

    invoke-interface {v1, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/HbP;

    invoke-direct {v6, v2, v3, v5}, LX/HbJ;-><init>(Ljava/util/List;LX/HbZ;LX/HbP;)V

    .line 2484083
    move-object v1, v6

    .line 2484084
    iput-object v1, v0, LX/Hag;->d:LX/HbJ;

    .line 2484085
    iget-object v1, v0, LX/Hag;->d:LX/HbJ;

    new-instance v2, LX/Had;

    invoke-direct {v2, v0}, LX/Had;-><init>(LX/Hag;)V

    .line 2484086
    iput-object v2, v1, LX/HbJ;->b:LX/Had;

    .line 2484087
    sget-object v1, LX/Hac;->SCROLLING:LX/Hac;

    iput-object v1, v0, LX/Hag;->e:LX/Hac;

    .line 2484088
    new-instance v1, LX/Hbd;

    iget-object v2, v0, LX/Hag;->b:Landroid/content/Context;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3, v4}, LX/Hbd;-><init>(Landroid/content/Context;IZ)V

    iput-object v1, v0, LX/Hag;->c:LX/Hbd;

    .line 2484089
    iget-object v1, v0, LX/Hag;->c:LX/Hbd;

    new-instance v2, LX/Haf;

    invoke-direct {v2, v0}, LX/Haf;-><init>(LX/Hag;)V

    .line 2484090
    iput-object v2, v1, LX/Hbd;->e:LX/Haf;

    .line 2484091
    iget-object v1, v0, LX/Hag;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v2, v0, LX/Hag;->c:LX/Hbd;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2484092
    iget-object v1, v0, LX/Hag;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v2, v0, LX/Hag;->d:LX/HbJ;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2484093
    iget-object v1, v0, LX/Hag;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v2, LX/HbI;

    iget-object v3, v0, LX/Hag;->b:Landroid/content/Context;

    invoke-direct {v2, v3}, LX/HbI;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 2484094
    iget-object v1, v0, LX/Hag;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v2, LX/1Oe;

    invoke-direct {v2}, LX/1Oe;-><init>()V

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setItemAnimator(LX/1Of;)V

    .line 2484095
    iget-object v1, v0, LX/Hag;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v2, LX/Hae;

    invoke-direct {v2, v0}, LX/Hae;-><init>(LX/Hag;)V

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setOnScrollListener(LX/1OX;)V

    .line 2484096
    iget-object v1, v0, LX/Hag;->f:Lcom/facebook/fbui/glyph/GlyphButton;

    new-instance v2, LX/Hab;

    invoke-direct {v2, v0}, LX/Hab;-><init>(LX/Hag;)V

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2484097
    iget-object v0, p0, Lcom/facebook/securitycheckup/SecurityCheckupMainActivity$2;->a:Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;

    invoke-static {v0}, Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;->l(Lcom/facebook/securitycheckup/SecurityCheckupMainActivity;)V

    .line 2484098
    return-void

    .line 2484099
    :cond_1
    const v7, 0x7f083628

    goto/16 :goto_1

    .line 2484100
    :cond_2
    iget-object v7, v0, LX/Hag;->b:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v9, 0x7f083626

    new-array v11, v14, [Ljava/lang/Object;

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v11, v8

    invoke-virtual {v7, v9, v11}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_2

    :cond_3
    move v13, v8

    goto/16 :goto_0

    .line 2484101
    :cond_4
    const v7, 0x7f08362d

    move v10, v7

    goto/16 :goto_3

    .line 2484102
    :cond_5
    const v7, 0x7f08362b

    move v9, v7

    goto/16 :goto_4
.end method
