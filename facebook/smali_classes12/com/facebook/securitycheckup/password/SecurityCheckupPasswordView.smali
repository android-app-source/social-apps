.class public Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# static fields
.field private static final g:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "LX/HaS;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final h:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "LX/HaS;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final i:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "LX/HaS;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final j:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "LX/HaS;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Lcom/facebook/resources/ui/FbTextView;

.field public b:Lcom/facebook/resources/ui/FbEditText;

.field private c:Lcom/facebook/resources/ui/FbTextView;

.field private d:Landroid/widget/ImageView;

.field public e:Landroid/widget/ImageView;

.field public f:LX/HaS;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2486334
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    sget-object v1, LX/HaS;->WEAK:LX/HaS;

    const v2, 0x7f0a08d9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/HaS;->OK:LX/HaS;

    const v2, 0x7f0a08da

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/HaS;->STRONG:LX/HaS;

    const v2, 0x7f0a08db

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->g:LX/0P1;

    .line 2486335
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    sget-object v1, LX/HaS;->WEAK:LX/HaS;

    const v2, 0x7f021829

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/HaS;->OK:LX/HaS;

    const v2, 0x7f021825

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/HaS;->STRONG:LX/HaS;

    const v2, 0x7f021827

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->h:LX/0P1;

    .line 2486336
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    sget-object v1, LX/HaS;->WEAK:LX/HaS;

    const v2, 0x7f02182a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/HaS;->OK:LX/HaS;

    const v2, 0x7f021826

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/HaS;->STRONG:LX/HaS;

    const v2, 0x7f021828

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->i:LX/0P1;

    .line 2486337
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    sget-object v1, LX/HaS;->WEAK:LX/HaS;

    const v2, 0x7f083649

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/HaS;->OK:LX/HaS;

    const v2, 0x7f08364a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/HaS;->STRONG:LX/HaS;

    const v2, 0x7f08364b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->j:LX/0P1;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2486341
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2486342
    invoke-direct {p0}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->f()V

    .line 2486343
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2486338
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2486339
    invoke-direct {p0}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->f()V

    .line 2486340
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2486331
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2486332
    invoke-direct {p0}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->f()V

    .line 2486333
    return-void
.end method

.method private f()V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .prologue
    .line 2486320
    sget-object v0, LX/HaS;->NULL:LX/HaS;

    iput-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->f:LX/HaS;

    .line 2486321
    const v0, 0x7f0312da

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2486322
    const v0, 0x7f0d2bf1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->a:Lcom/facebook/resources/ui/FbTextView;

    .line 2486323
    const v0, 0x7f0d2bf2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbEditText;

    iput-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->b:Lcom/facebook/resources/ui/FbEditText;

    .line 2486324
    const v0, 0x7f0d2bf4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 2486325
    const v0, 0x7f0d2bf5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->d:Landroid/widget/ImageView;

    .line 2486326
    const v0, 0x7f0d2bf6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->e:Landroid/widget/ImageView;

    .line 2486327
    iget-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->b:Lcom/facebook/resources/ui/FbEditText;

    const/4 v1, 0x0

    invoke-static {v1}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setTypeface(Landroid/graphics/Typeface;)V

    .line 2486328
    iget-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->b:Lcom/facebook/resources/ui/FbEditText;

    new-instance v1, LX/Hbo;

    invoke-direct {v1, p0}, LX/Hbo;-><init>(Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2486329
    new-instance v0, LX/Hbp;

    invoke-direct {v0, p0}, LX/Hbp;-><init>(Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;)V

    invoke-virtual {p0, v0}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2486330
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2486317
    iget-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->e:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0217ab

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2486318
    iget-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->b:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {p0}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setTextColor(I)V

    .line 2486319
    return-void
.end method

.method public final a(Landroid/text/TextWatcher;)V
    .locals 1

    .prologue
    .line 2486315
    iget-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->b:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2486316
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 2486312
    invoke-virtual {p0}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->c()V

    .line 2486313
    iget-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->d:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f021824

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2486314
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 2486310
    iget-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->c:Lcom/facebook/resources/ui/FbTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2486311
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2486344
    iget-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->b:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setEnabled(Z)V

    .line 2486345
    iget-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->b:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setFocusableInTouchMode(Z)V

    .line 2486346
    iget-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->b:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setCursorVisible(Z)V

    .line 2486347
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2486306
    iget-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->b:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setEnabled(Z)V

    .line 2486307
    iget-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->b:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setFocusable(Z)V

    .line 2486308
    iget-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->b:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setCursorVisible(Z)V

    .line 2486309
    return-void
.end method

.method public getCurrentDisplayedStrengthType()LX/HaS;
    .locals 1

    .prologue
    .line 2486305
    iget-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->f:LX/HaS;

    return-object v0
.end method

.method public getNameWidth()I
    .locals 1

    .prologue
    .line 2486304
    iget-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getWidth()I

    move-result v0

    return v0
.end method

.method public getPasswordEditText()Lcom/facebook/resources/ui/FbEditText;
    .locals 1

    .prologue
    .line 2486303
    iget-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->b:Lcom/facebook/resources/ui/FbEditText;

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2486302
    iget-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->b:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setName(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2486300
    iget-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2486301
    return-void
.end method

.method public setNameWidth(I)V
    .locals 1

    .prologue
    .line 2486298
    iget-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setWidth(I)V

    .line 2486299
    return-void
.end method

.method public setPasswordStrength(LX/HaS;)V
    .locals 3

    .prologue
    .line 2486284
    iput-object p1, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->f:LX/HaS;

    .line 2486285
    sget-object v0, LX/HaS;->NULL:LX/HaS;

    if-ne p1, v0, :cond_0

    .line 2486286
    iget-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->c:Lcom/facebook/resources/ui/FbTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2486287
    iget-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->d:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2486288
    :goto_0
    return-void

    .line 2486289
    :cond_0
    iget-object v1, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->c:Lcom/facebook/resources/ui/FbTextView;

    sget-object v0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->j:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2486290
    iget-object v1, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget-object v0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->g:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    goto :goto_0
.end method

.method public setPasswordsMatch(Z)V
    .locals 3

    .prologue
    .line 2486293
    iget-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->f:LX/HaS;

    sget-object v1, LX/HaS;->NULL:LX/HaS;

    if-ne v0, v1, :cond_0

    .line 2486294
    :goto_0
    return-void

    .line 2486295
    :cond_0
    if-eqz p1, :cond_1

    sget-object v0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->i:LX/0P1;

    iget-object v1, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->f:LX/HaS;

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    :goto_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2486296
    iget-object v1, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->d:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 2486297
    :cond_1
    sget-object v0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->h:LX/0P1;

    iget-object v1, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->f:LX/HaS;

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    goto :goto_1
.end method

.method public setStrengthString(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2486291
    iget-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2486292
    return-void
.end method
