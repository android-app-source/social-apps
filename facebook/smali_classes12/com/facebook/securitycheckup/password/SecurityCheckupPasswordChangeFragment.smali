.class public Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field private a:Lcom/facebook/resources/ui/FbButton;

.field public b:Lcom/facebook/resources/ui/FbTextView;

.field private c:Landroid/widget/ProgressBar;

.field public d:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;

.field public e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/util/concurrent/ExecutorService;

.field public g:LX/HaZ;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2486146
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method public static d(Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;)V
    .locals 3

    .prologue
    .line 2486144
    iget-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08364e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2486145
    return-void
.end method

.method public static e(Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2486138
    iget-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;->a:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setEnabled(Z)V

    .line 2486139
    iget-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;->c:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2486140
    iget-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;->d:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;

    .line 2486141
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->c:Z

    .line 2486142
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    new-instance p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment$3;

    invoke-direct {p0, v0}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment$3;-><init>(Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;)V

    invoke-virtual {v1, p0}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2486143
    return-void
.end method

.method public static k(Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;)V
    .locals 2

    .prologue
    .line 2486132
    iget-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;->a:Lcom/facebook/resources/ui/FbButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setEnabled(Z)V

    .line 2486133
    iget-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;->c:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2486134
    iget-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;->d:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;

    .line 2486135
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->c:Z

    .line 2486136
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    new-instance p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment$4;

    invoke-direct {p0, v0}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment$4;-><init>(Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;)V

    invoke-virtual {v1, p0}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2486137
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2486129
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2486130
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;

    invoke-static {p1}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ExecutorService;

    const/16 v0, 0xafd

    invoke-static {p1, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v0

    invoke-static {p1}, LX/HaZ;->a(LX/0QB;)LX/HaZ;

    move-result-object p1

    check-cast p1, LX/HaZ;

    iput-object v2, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;->f:Ljava/util/concurrent/ExecutorService;

    iput-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;->e:LX/0Ot;

    iput-object p1, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;->g:LX/HaZ;

    .line 2486131
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2486147
    iget-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;->a:Lcom/facebook/resources/ui/FbButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setEnabled(Z)V

    .line 2486148
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 2486127
    iget-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;->a:Lcom/facebook/resources/ui/FbButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setEnabled(Z)V

    .line 2486128
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x2a

    const v1, 0x27a971ba

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2486107
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2486108
    const v0, 0x7f0d2be9

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;->a:Lcom/facebook/resources/ui/FbButton;

    .line 2486109
    const v0, 0x7f0d2be7

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 2486110
    const v0, 0x7f0d2bea

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;->c:Landroid/widget/ProgressBar;

    .line 2486111
    new-instance v0, LX/Hbl;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2, p0}, LX/Hbl;-><init>(Landroid/content/Context;Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;)V

    .line 2486112
    new-instance v2, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;

    invoke-direct {v2}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;-><init>()V

    .line 2486113
    iget-object v3, v0, LX/Hbl;->b:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;

    .line 2486114
    iput-object v3, v2, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->j:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;

    .line 2486115
    iget-object v3, v0, LX/Hbl;->c:Ljava/lang/String;

    .line 2486116
    iput-object v3, v2, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->g:Ljava/lang/String;

    .line 2486117
    iget-object v3, v0, LX/Hbl;->d:Ljava/lang/String;

    .line 2486118
    iput-object v3, v2, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->h:Ljava/lang/String;

    .line 2486119
    iget-object v3, v0, LX/Hbl;->e:Ljava/lang/String;

    .line 2486120
    iput-object v3, v2, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->i:Ljava/lang/String;

    .line 2486121
    move-object v0, v2

    .line 2486122
    iput-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;->d:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;

    .line 2486123
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v2, 0x7f0d2be8

    iget-object v3, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;->d:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;

    sget-object v4, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->a:Ljava/lang/String;

    invoke-virtual {v0, v2, v3, v4}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2486124
    iget-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;->a:Lcom/facebook/resources/ui/FbButton;

    new-instance v2, LX/Hbg;

    invoke-direct {v2, p0}, LX/Hbg;-><init>(Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;)V

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2486125
    iget-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;->a:Lcom/facebook/resources/ui/FbButton;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbButton;->setEnabled(Z)V

    .line 2486126
    const/16 v0, 0x2b

    const v2, -0x6d131dd8

    invoke-static {v5, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onAttach(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2486104
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onAttach(Landroid/content/Context;)V

    .line 2486105
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x2000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 2486106
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x693547f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2486102
    const v1, 0x7f0312d7

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2486103
    const/16 v2, 0x2b

    const v3, -0x726361b7

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onStart()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0xc02286f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2486096
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2486097
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2486098
    if-eqz v0, :cond_0

    .line 2486099
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f083645

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2486100
    const/4 v2, 0x1

    invoke-interface {v0, v2}, LX/1ZF;->k_(Z)V

    .line 2486101
    :cond_0
    const/16 v0, 0x2b

    const v2, 0x21db3018

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
