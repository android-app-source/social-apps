.class public final Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment$2$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/Throwable;

.field public final synthetic b:LX/Hbh;


# direct methods
.method public constructor <init>(LX/Hbh;Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2486069
    iput-object p1, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment$2$2;->b:LX/Hbh;

    iput-object p2, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment$2$2;->a:Ljava/lang/Throwable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 2486070
    iget-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment$2$2;->b:LX/Hbh;

    iget-object v0, v0, LX/Hbh;->a:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;

    iget-object v1, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment$2$2;->a:Ljava/lang/Throwable;

    .line 2486071
    iget-object v2, v0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;->g:LX/HaZ;

    const-string v3, "PASSWORD_CHANGE_ERROR"

    invoke-virtual {v2, v3}, LX/HaZ;->a(Ljava/lang/String;)V

    .line 2486072
    iget-object v2, v0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;->b:Lcom/facebook/resources/ui/FbTextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2486073
    instance-of v2, v1, LX/4Ua;

    if-eqz v2, :cond_2

    .line 2486074
    check-cast v1, LX/4Ua;

    .line 2486075
    iget-object v2, v1, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    move-object v2, v2

    .line 2486076
    if-nez v2, :cond_0

    .line 2486077
    invoke-static {v0}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;->d(Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;)V

    .line 2486078
    :cond_0
    iget-object v2, v1, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    move-object v2, v2

    .line 2486079
    invoke-virtual {v2}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v2

    .line 2486080
    const v3, 0x1879a2

    if-ne v2, v3, :cond_1

    .line 2486081
    iget-object v2, v0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08364d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2486082
    iget-object v2, v0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;->d:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;

    .line 2486083
    iget-object v3, v2, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->d:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    invoke-virtual {v3}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->a()V

    .line 2486084
    :goto_0
    iget-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment$2$2;->b:LX/Hbh;

    iget-object v0, v0, LX/Hbh;->a:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;

    invoke-static {v0}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;->k(Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;)V

    .line 2486085
    return-void

    .line 2486086
    :cond_1
    iget-object v2, v0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 2486087
    iget-object v3, v1, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    move-object v3, v3

    .line 2486088
    invoke-virtual {v3}, Lcom/facebook/http/protocol/ApiErrorResult;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2486089
    :cond_2
    invoke-static {v0}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;->d(Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;)V

    goto :goto_0
.end method
