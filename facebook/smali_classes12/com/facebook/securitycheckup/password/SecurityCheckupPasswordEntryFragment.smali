.class public Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public b:Z

.field public c:Z

.field public d:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

.field public e:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

.field public f:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordChangeFragment;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2486251
    const-class v0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2486252
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2486253
    iput-boolean v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->b:Z

    .line 2486254
    iput-boolean v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->c:Z

    .line 2486255
    return-void
.end method

.method public static n(Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;)Z
    .locals 2

    .prologue
    .line 2486256
    iget-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->e:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    invoke-virtual {v0}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->d:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    invoke-virtual {v0}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->f:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    invoke-virtual {v0}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->e:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    invoke-virtual {v0}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->getText()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->f:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    invoke-virtual {v1}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x2a

    const v1, 0x302cdd8d

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2486257
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2486258
    const v0, 0x7f0d2beb

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    iput-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->d:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    .line 2486259
    const v0, 0x7f0d2bec

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    iput-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->e:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    .line 2486260
    const v0, 0x7f0d2bed

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    iput-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->f:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    .line 2486261
    iget-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->d:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    iget-object v2, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->g:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->setName(Ljava/lang/String;)V

    .line 2486262
    iget-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->e:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    iget-object v2, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->h:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->setName(Ljava/lang/String;)V

    .line 2486263
    iget-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->f:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    iget-object v2, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->i:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->setName(Ljava/lang/String;)V

    .line 2486264
    iget-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->f:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    new-instance v2, LX/Hbj;

    invoke-direct {v2, p0}, LX/Hbj;-><init>(Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;)V

    invoke-virtual {v0, v2}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 2486265
    iget-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->e:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    new-instance v2, LX/Hbm;

    iget-object v3, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->e:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    iget-object v4, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->f:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    invoke-direct {v2, p0, v3, v4}, LX/Hbm;-><init>(Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;)V

    invoke-virtual {v0, v2}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->a(Landroid/text/TextWatcher;)V

    .line 2486266
    iget-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->f:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    new-instance v2, LX/Hbn;

    iget-object v3, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->f:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    iget-object v4, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->e:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    invoke-direct {v2, p0, v3, v4}, LX/Hbn;-><init>(Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;)V

    invoke-virtual {v0, v2}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->a(Landroid/text/TextWatcher;)V

    .line 2486267
    iget-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->d:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    new-instance v2, LX/Hbk;

    invoke-direct {v2, p0}, LX/Hbk;-><init>(Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;)V

    invoke-virtual {v0, v2}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->a(Landroid/text/TextWatcher;)V

    .line 2486268
    iget-object v0, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->d:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    invoke-virtual {v0}, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->callOnClick()Z

    .line 2486269
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    const-string v2, "input_method"

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 2486270
    iget-object v2, p0, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordEntryFragment;->d:Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;

    .line 2486271
    iget-object v3, v2, Lcom/facebook/securitycheckup/password/SecurityCheckupPasswordView;->b:Lcom/facebook/resources/ui/FbEditText;

    move-object v2, v3

    .line 2486272
    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 2486273
    const/16 v0, 0x2b

    const v2, -0x1c9d0bed

    invoke-static {v5, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0xe88d05d

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2486274
    const v1, 0x7f0312d8

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2486275
    const/16 v2, 0x2b

    const v3, -0x397dec21

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method
