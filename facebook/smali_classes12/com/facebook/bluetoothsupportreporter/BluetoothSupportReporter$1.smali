.class public final Lcom/facebook/bluetoothsupportreporter/BluetoothSupportReporter$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/HnO;


# direct methods
.method public constructor <init>(LX/HnO;)V
    .locals 0

    .prologue
    .line 2501780
    iput-object p1, p0, Lcom/facebook/bluetoothsupportreporter/BluetoothSupportReporter$1;->a:LX/HnO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 2501781
    iget-object v0, p0, Lcom/facebook/bluetoothsupportreporter/BluetoothSupportReporter$1;->a:LX/HnO;

    .line 2501782
    iget-object v1, v0, LX/HnO;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 2501783
    const-string v2, "android.hardware.bluetooth"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v2

    .line 2501784
    const-string v3, "android.hardware.bluetooth_le"

    invoke-virtual {v1, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    .line 2501785
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p0, "android_bluetooth_support_detection"

    invoke-direct {v3, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p0, "bluetooth_supported"

    invoke-virtual {v3, p0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "ble_supported"

    invoke-virtual {v2, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 2501786
    iget-object v1, v0, LX/HnO;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Zb;

    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2501787
    return-void
.end method
