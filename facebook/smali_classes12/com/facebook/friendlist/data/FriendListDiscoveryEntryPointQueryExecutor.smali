.class public Lcom/facebook/friendlist/data/FriendListDiscoveryEntryPointQueryExecutor;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:LX/0tX;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2550345
    const-class v0, Lcom/facebook/friendlist/data/FriendListDiscoveryEntryPointQueryExecutor;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/friendlist/data/FriendListDiscoveryEntryPointQueryExecutor;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0tX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2550346
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2550347
    iput-object p1, p0, Lcom/facebook/friendlist/data/FriendListDiscoveryEntryPointQueryExecutor;->b:LX/0tX;

    .line 2550348
    return-void
.end method

.method public static a(LX/0zO;)LX/0zO;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0zO",
            "<TT;>;)",
            "LX/0zO",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 2550349
    sget-object v0, Lcom/facebook/friendlist/data/FriendListDiscoveryEntryPointQueryExecutor;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 2550350
    iput-object v0, p0, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2550351
    move-object v0, p0

    .line 2550352
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->a:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    const-wide/32 v2, 0x15180

    invoke-virtual {v0, v2, v3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v0

    return-object v0
.end method
