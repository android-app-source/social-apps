.class public Lcom/facebook/friendlist/fragment/FriendPageFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0hF;


# instance fields
.field public a:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/IEC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/17W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Landroid/view/View;

.field public f:Ljava/lang/String;

.field private g:Z

.field public h:LX/DHs;

.field private i:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

.field private j:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

.field public k:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/DHs;",
            ">;"
        }
    .end annotation
.end field

.field public l:Z

.field public m:Z

.field public n:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2551065
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2551066
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/friendlist/fragment/FriendPageFragment;->l:Z

    .line 2551067
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/friendlist/fragment/FriendPageFragment;

    invoke-static {p0}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {p0}, LX/IEC;->a(LX/0QB;)LX/IEC;

    move-result-object v2

    check-cast v2, LX/IEC;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {p0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object p0

    check-cast p0, LX/17W;

    iput-object v1, p1, Lcom/facebook/friendlist/fragment/FriendPageFragment;->a:Ljava/lang/String;

    iput-object v2, p1, Lcom/facebook/friendlist/fragment/FriendPageFragment;->b:LX/IEC;

    iput-object v3, p1, Lcom/facebook/friendlist/fragment/FriendPageFragment;->c:LX/0ad;

    iput-object p0, p1, Lcom/facebook/friendlist/fragment/FriendPageFragment;->d:LX/17W;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/friendlist/fragment/FriendPageFragment;Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2551051
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/friendlist/fragment/FriendPageFragment;->l:Z

    .line 2551052
    const v0, 0x7f0d12c8

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendlist/fragment/FriendPageFragment;->e:Landroid/view/View;

    .line 2551053
    if-eqz p1, :cond_0

    .line 2551054
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f04004e

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 2551055
    iget-object v1, p0, Lcom/facebook/friendlist/fragment/FriendPageFragment;->e:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2551056
    :cond_0
    iget-object v0, p0, Lcom/facebook/friendlist/fragment/FriendPageFragment;->e:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2551057
    const v0, 0x7f0d12c4

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2551058
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v1, p0, Lcom/facebook/friendlist/fragment/FriendPageFragment;->c:LX/0ad;

    sget-short v3, LX/ID2;->a:S

    invoke-interface {v1, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_1

    const v1, 0x7f08383b

    :goto_0
    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2551059
    new-instance v1, LX/IDc;

    invoke-direct {v1, p0}, LX/IDc;-><init>(Lcom/facebook/friendlist/fragment/FriendPageFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2551060
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f04004f

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 2551061
    new-instance v1, LX/IDd;

    invoke-direct {v1, p0}, LX/IDd;-><init>(Lcom/facebook/friendlist/fragment/FriendPageFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2551062
    const v1, 0x7f0d0632

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, LX/IDe;

    invoke-direct {v2, p0, v0}, LX/IDe;-><init>(Lcom/facebook/friendlist/fragment/FriendPageFragment;Landroid/view/animation/Animation;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2551063
    return-void

    .line 2551064
    :cond_1
    const v1, 0x7f08383a

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2551050
    const-string v0, "profile_friends_page"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2550989
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2550990
    const-class v0, Lcom/facebook/friendlist/fragment/FriendPageFragment;

    invoke-static {v0, p0}, Lcom/facebook/friendlist/fragment/FriendPageFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2550991
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v0

    .line 2550992
    iget-object v0, p0, Lcom/facebook/friendlist/fragment/FriendPageFragment;->c:LX/0ad;

    sget-short v2, LX/ID2;->d:S

    invoke-interface {v0, v2, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 2550993
    iget-object v2, p0, Lcom/facebook/friendlist/fragment/FriendPageFragment;->c:LX/0ad;

    sget-short v3, LX/ID2;->b:S

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    iput-boolean v2, p0, Lcom/facebook/friendlist/fragment/FriendPageFragment;->m:Z

    .line 2550994
    iget-object v2, p0, Lcom/facebook/friendlist/fragment/FriendPageFragment;->c:LX/0ad;

    sget-short v3, LX/ID2;->c:S

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    iput-boolean v2, p0, Lcom/facebook/friendlist/fragment/FriendPageFragment;->n:Z

    .line 2550995
    const-string v2, "com.facebook.katana.profile.id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/friendlist/fragment/FriendPageFragment;->f:Ljava/lang/String;

    .line 2550996
    iget-object v2, p0, Lcom/facebook/friendlist/fragment/FriendPageFragment;->f:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/friendlist/fragment/FriendPageFragment;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/facebook/friendlist/fragment/FriendPageFragment;->g:Z

    .line 2550997
    iget-boolean v2, p0, Lcom/facebook/friendlist/fragment/FriendPageFragment;->g:Z

    .line 2550998
    if-eqz v2, :cond_4

    .line 2550999
    if-eqz v0, :cond_3

    sget-object v3, LX/IEF;->b:LX/0Px;

    .line 2551000
    :goto_0
    move-object v0, v3

    .line 2551001
    iput-object v0, p0, Lcom/facebook/friendlist/fragment/FriendPageFragment;->k:LX/0Px;

    .line 2551002
    const-string v0, "target_tab_name"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/DHs;->fromString(Ljava/lang/String;)LX/DHs;

    move-result-object v0

    .line 2551003
    iget-object v2, p0, Lcom/facebook/friendlist/fragment/FriendPageFragment;->k:LX/0Px;

    invoke-virtual {v2, v0}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :goto_1
    iput-object v0, p0, Lcom/facebook/friendlist/fragment/FriendPageFragment;->h:LX/DHs;

    .line 2551004
    const-string v0, "friendship_status"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2551005
    const-string v0, "friendship_status"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendlist/fragment/FriendPageFragment;->i:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2551006
    :cond_0
    const-string v0, "subscribe_status"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2551007
    const-string v0, "subscribe_status"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendlist/fragment/FriendPageFragment;->j:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 2551008
    :cond_1
    iget-object v0, p0, Lcom/facebook/friendlist/fragment/FriendPageFragment;->b:LX/IEC;

    iget-object v2, p0, Lcom/facebook/friendlist/fragment/FriendPageFragment;->h:LX/DHs;

    invoke-virtual {v2}, LX/DHs;->name()Ljava/lang/String;

    move-result-object v2

    const-string v3, "source_ref"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/facebook/friendlist/fragment/FriendPageFragment;->f:Ljava/lang/String;

    .line 2551009
    iget-object v4, v0, LX/IEC;->a:LX/0Zb;

    sget-object p0, LX/IEB;->FRIEND_LIST_OPENED:LX/IEB;

    invoke-static {v0, p0, v3}, LX/IEC;->a(LX/IEC;LX/IEB;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "initial_tab"

    invoke-virtual {p0, p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "source_ref"

    invoke-virtual {p0, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-interface {v4, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2551010
    return-void

    .line 2551011
    :cond_2
    iget-object v0, p0, Lcom/facebook/friendlist/fragment/FriendPageFragment;->k:LX/0Px;

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DHs;

    goto :goto_1

    .line 2551012
    :cond_3
    sget-object v3, LX/IEF;->a:LX/0Px;

    goto :goto_0

    .line 2551013
    :cond_4
    sget-object v3, LX/IEF;->c:LX/0Px;

    goto :goto_0
.end method

.method public final b()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2551045
    iget-boolean v0, p0, Lcom/facebook/friendlist/fragment/FriendPageFragment;->g:Z

    iget-object v1, p0, Lcom/facebook/friendlist/fragment/FriendPageFragment;->i:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iget-object v2, p0, Lcom/facebook/friendlist/fragment/FriendPageFragment;->j:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-static {v0, v1, v2}, LX/9lQ;->getRelationshipType(ZLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)LX/9lQ;

    move-result-object v0

    invoke-virtual {v0}, LX/9lQ;->getValue()I

    move-result v0

    .line 2551046
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v1

    .line 2551047
    const-string v2, "relationship_type"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2551048
    const-string v0, "profile_id"

    iget-object v2, p0, Lcom/facebook/friendlist/fragment/FriendPageFragment;->f:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2551049
    return-object v1
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x42d23326

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2551044
    const v1, 0x7f030705

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x1106c1

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x6f4de11b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2551041
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/friendlist/fragment/FriendPageFragment;->e:Landroid/view/View;

    .line 2551042
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2551043
    const/16 v1, 0x2b

    const v2, -0x40961e05

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x66bb53f2

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2551034
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2551035
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2551036
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 2551037
    const-string v3, "profile_name"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2551038
    if-eqz v0, :cond_0

    .line 2551039
    invoke-interface {v0, v2}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2551040
    :cond_0
    const/16 v0, 0x2b

    const v2, -0x5ff18cdc

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 2551014
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2551015
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2551016
    const-string v1, "profile_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2551017
    const-string v1, "first_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2551018
    new-instance v0, LX/IEE;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/friendlist/fragment/FriendPageFragment;->f:Ljava/lang/String;

    iget-object v6, p0, Lcom/facebook/friendlist/fragment/FriendPageFragment;->k:LX/0Px;

    new-instance v7, LX/IDa;

    invoke-direct {v7, p0}, LX/IDa;-><init>(Lcom/facebook/friendlist/fragment/FriendPageFragment;)V

    invoke-direct/range {v0 .. v7}, LX/IEE;-><init>(LX/0gc;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2551019
    const v1, 0x7f0d12c7

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/ViewPager;

    .line 2551020
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 2551021
    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2551022
    iget-object v0, p0, Lcom/facebook/friendlist/fragment/FriendPageFragment;->k:LX/0Px;

    iget-object v2, p0, Lcom/facebook/friendlist/fragment/FriendPageFragment;->h:LX/DHs;

    invoke-virtual {v0, v2}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 2551023
    const v0, 0x7f0d12c6

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    .line 2551024
    invoke-virtual {v0, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 2551025
    iget-object v2, p0, Lcom/facebook/friendlist/fragment/FriendPageFragment;->k:LX/0Px;

    iget-object v3, p0, Lcom/facebook/friendlist/fragment/FriendPageFragment;->h:LX/DHs;

    invoke-virtual {v2, v3}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->B_(I)V

    .line 2551026
    invoke-virtual {v0, v8}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setVisibility(I)V

    .line 2551027
    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v1

    invoke-virtual {v1}, LX/0gG;->b()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 2551028
    invoke-virtual {v0, v8}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setUnderlineHeight(I)V

    .line 2551029
    :cond_0
    new-instance v1, LX/IDb;

    invoke-direct {v1, p0}, LX/IDb;-><init>(Lcom/facebook/friendlist/fragment/FriendPageFragment;)V

    .line 2551030
    iput-object v1, v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->l:LX/0hc;

    .line 2551031
    iget-boolean v0, p0, Lcom/facebook/friendlist/fragment/FriendPageFragment;->m:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/facebook/friendlist/fragment/FriendPageFragment;->n:Z

    if-nez v0, :cond_1

    .line 2551032
    invoke-static {p0, v8}, Lcom/facebook/friendlist/fragment/FriendPageFragment;->a$redex0(Lcom/facebook/friendlist/fragment/FriendPageFragment;Z)V

    .line 2551033
    :cond_1
    return-void
.end method
