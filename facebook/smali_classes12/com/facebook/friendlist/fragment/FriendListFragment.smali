.class public abstract Lcom/facebook/friendlist/fragment/FriendListFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field private A:Ljava/lang/String;

.field public B:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field private C:Z

.field public D:Z

.field public E:Z

.field public F:Z

.field public G:Z

.field public H:I

.field private I:Z

.field public J:Landroid/widget/Filter$FilterListener;

.field private K:Landroid/text/TextWatcher;

.field private L:LX/IDV;

.field private M:LX/IDX;

.field private N:LX/IDZ;

.field private O:LX/2hO;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private P:LX/BPt;

.field public Q:Landroid/widget/AbsListView$OnScrollListener;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public a:Lcom/facebook/widget/listview/BetterListView;

.field public b:Landroid/view/View;

.field public c:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/2do;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/IEC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/IDu;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/IDz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/IDF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/IE2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/ID3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/1mR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:Landroid/view/inputmethod/InputMethodManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/17W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/BPs;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0W3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:Landroid/view/View;

.field public s:Landroid/view/View;

.field public t:Landroid/view/View;

.field public u:Landroid/widget/EditText;

.field public v:Landroid/view/View;

.field private w:LX/IDE;

.field public x:LX/DSo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/DSo",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public y:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

.field public z:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2550571
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2550572
    iput-boolean v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->C:Z

    .line 2550573
    iput-boolean v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->D:Z

    .line 2550574
    iput-boolean v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->E:Z

    .line 2550575
    iput-boolean v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->F:Z

    .line 2550576
    iput-boolean v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->G:Z

    .line 2550577
    iput v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->H:I

    .line 2550578
    iput-boolean v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->I:Z

    .line 2550579
    return-void
.end method

.method private static a(Lcom/facebook/friendlist/fragment/FriendListFragment;Ljava/lang/String;LX/2do;LX/IEC;LX/IDu;LX/IDz;LX/IDF;LX/IE2;LX/ID3;LX/1mR;Landroid/view/inputmethod/InputMethodManager;LX/17W;LX/0ad;LX/1Ck;LX/BPs;LX/0W3;)V
    .locals 0

    .prologue
    .line 2550743
    iput-object p1, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->c:Ljava/lang/String;

    iput-object p2, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->d:LX/2do;

    iput-object p3, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->e:LX/IEC;

    iput-object p4, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->f:LX/IDu;

    iput-object p5, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->g:LX/IDz;

    iput-object p6, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->h:LX/IDF;

    iput-object p7, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->i:LX/IE2;

    iput-object p8, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->j:LX/ID3;

    iput-object p9, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->k:LX/1mR;

    iput-object p10, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->l:Landroid/view/inputmethod/InputMethodManager;

    iput-object p11, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->m:LX/17W;

    iput-object p12, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->n:LX/0ad;

    iput-object p13, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->o:LX/1Ck;

    iput-object p14, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->p:LX/BPs;

    iput-object p15, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->q:LX/0W3;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 16

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v15

    move-object/from16 v0, p0

    check-cast v0, Lcom/facebook/friendlist/fragment/FriendListFragment;

    invoke-static {v15}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v15}, LX/2do;->a(LX/0QB;)LX/2do;

    move-result-object v2

    check-cast v2, LX/2do;

    invoke-static {v15}, LX/IEC;->a(LX/0QB;)LX/IEC;

    move-result-object v3

    check-cast v3, LX/IEC;

    invoke-static {v15}, LX/IDu;->b(LX/0QB;)LX/IDu;

    move-result-object v4

    check-cast v4, LX/IDu;

    const-class v5, LX/IDz;

    invoke-interface {v15, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/IDz;

    const-class v6, LX/IDF;

    invoke-interface {v15, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/IDF;

    invoke-static {v15}, LX/IE2;->b(LX/0QB;)LX/IE2;

    move-result-object v7

    check-cast v7, LX/IE2;

    invoke-static {v15}, LX/ID3;->b(LX/0QB;)LX/ID3;

    move-result-object v8

    check-cast v8, LX/ID3;

    invoke-static {v15}, LX/1mR;->a(LX/0QB;)LX/1mR;

    move-result-object v9

    check-cast v9, LX/1mR;

    invoke-static {v15}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v10

    check-cast v10, Landroid/view/inputmethod/InputMethodManager;

    invoke-static {v15}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v11

    check-cast v11, LX/17W;

    invoke-static {v15}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v12

    check-cast v12, LX/0ad;

    invoke-static {v15}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v13

    check-cast v13, LX/1Ck;

    invoke-static {v15}, LX/BPs;->a(LX/0QB;)LX/BPs;

    move-result-object v14

    check-cast v14, LX/BPs;

    invoke-static {v15}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v15

    check-cast v15, LX/0W3;

    invoke-static/range {v0 .. v15}, Lcom/facebook/friendlist/fragment/FriendListFragment;->a(Lcom/facebook/friendlist/fragment/FriendListFragment;Ljava/lang/String;LX/2do;LX/IEC;LX/IDu;LX/IDz;LX/IDF;LX/IE2;LX/ID3;LX/1mR;Landroid/view/inputmethod/InputMethodManager;LX/17W;LX/0ad;LX/1Ck;LX/BPs;LX/0W3;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/friendlist/fragment/FriendListFragment;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2550736
    iget-boolean v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->I:Z

    if-eqz v0, :cond_0

    .line 2550737
    :goto_0
    return-void

    .line 2550738
    :cond_0
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 2550739
    iget-object v1, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 2550740
    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 2550741
    iget-object v1, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->z:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 2550742
    iget-object v1, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->k:LX/1mR;

    invoke-virtual {v1, v0}, LX/1mR;->a(Ljava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 2550731
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2550732
    const-string v1, "com.facebook.katana.profile.id"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2550733
    const-string v1, "profile_name"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2550734
    const-string v1, "first_name"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2550735
    return-object v0
.end method

.method public static m(Lcom/facebook/friendlist/fragment/FriendListFragment;)V
    .locals 5

    .prologue
    .line 2550722
    iget-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->y:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->o:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2550723
    iget-boolean v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->I:Z

    if-eqz v0, :cond_3

    .line 2550724
    invoke-virtual {p0}, Lcom/facebook/friendlist/fragment/FriendListFragment;->b()LX/DHs;

    move-result-object v0

    .line 2550725
    sget-object v1, LX/DHs;->PYMK:LX/DHs;

    if-eq v0, v1, :cond_0

    sget-object v1, LX/DHs;->SUGGESTIONS:LX/DHs;

    if-ne v0, v1, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 2550726
    :goto_0
    iget-object v1, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->w:LX/IDE;

    iget-object v2, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->y:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    invoke-virtual {v2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->a()Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/IDU;

    invoke-direct {v3, p0, v0}, LX/IDU;-><init>(Lcom/facebook/friendlist/fragment/FriendListFragment;Z)V

    invoke-virtual {v1, v2, v3}, LX/IDE;->a(Ljava/lang/String;LX/0TF;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2550727
    :goto_1
    iget-object v1, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->o:LX/1Ck;

    const-string v2, "load_friend_list"

    new-instance v3, LX/IDJ;

    invoke-direct {v3, p0, v0}, LX/IDJ;-><init>(Lcom/facebook/friendlist/fragment/FriendListFragment;Lcom/google/common/util/concurrent/ListenableFuture;)V

    new-instance v4, LX/IDK;

    invoke-direct {v4, p0}, LX/IDK;-><init>(Lcom/facebook/friendlist/fragment/FriendListFragment;)V

    invoke-virtual {v1, v2, v3, v4}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2550728
    :cond_1
    return-void

    .line 2550729
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 2550730
    :cond_3
    iget-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->w:LX/IDE;

    iget-object v1, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->y:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/IDE;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_1
.end method

.method public static p$redex0(Lcom/facebook/friendlist/fragment/FriendListFragment;)V
    .locals 2

    .prologue
    .line 2550719
    iget-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->a:Lcom/facebook/widget/listview/BetterListView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->b:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2550720
    iget-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->a:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->removeFooterView(Landroid/view/View;)Z

    .line 2550721
    :cond_0
    return-void
.end method

.method public static r(Lcom/facebook/friendlist/fragment/FriendListFragment;)V
    .locals 3

    .prologue
    .line 2550716
    iget-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->l:Landroid/view/inputmethod/InputMethodManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->u:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 2550717
    iget-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->l:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->u:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 2550718
    :cond_0
    return-void
.end method


# virtual methods
.method public abstract a(Z)LX/0Rl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "LX/0Rl",
            "<",
            "LX/IDH;",
            ">;"
        }
    .end annotation
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2550707
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2550708
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->E:Z

    .line 2550709
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->kX_()LX/0QA;

    move-result-object v0

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicklog/QuickPerformanceLogger;

    iput-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->B:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 2550710
    iget-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->B:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-virtual {p0}, Lcom/facebook/friendlist/fragment/FriendListFragment;->e()I

    move-result v1

    const-string v2, "profile_friends_page"

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/util/Collection;)V

    .line 2550711
    iget-boolean v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->D:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2550712
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->D:Z

    .line 2550713
    iget-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->B:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-virtual {p0}, Lcom/facebook/friendlist/fragment/FriendListFragment;->e()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 2550714
    :cond_0
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/friendlist/fragment/FriendListFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2550715
    return-void
.end method

.method public abstract b()LX/DHs;
.end method

.method public abstract c()LX/DHr;
.end method

.method public abstract d()I
.end method

.method public abstract e()I
.end method

.method public abstract k()I
.end method

.method public abstract l()Z
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2a

    const v1, -0x5610ed57

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2550696
    new-instance v1, LX/4a7;

    invoke-direct {v1}, LX/4a7;-><init>()V

    .line 2550697
    iput-boolean v3, v1, LX/4a7;->c:Z

    .line 2550698
    move-object v1, v1

    .line 2550699
    const/4 v2, 0x1

    .line 2550700
    iput-boolean v2, v1, LX/4a7;->b:Z

    .line 2550701
    move-object v1, v1

    .line 2550702
    invoke-virtual {v1}, LX/4a7;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->y:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 2550703
    const v1, 0x7f030703

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->b:Landroid/view/View;

    .line 2550704
    const v1, 0x7f030702

    invoke-virtual {p1, v1, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->r:Landroid/view/View;

    .line 2550705
    iget-object v1, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->r:Landroid/view/View;

    invoke-virtual {p0}, Lcom/facebook/friendlist/fragment/FriendListFragment;->d()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setId(I)V

    .line 2550706
    iget-object v1, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->r:Landroid/view/View;

    const/16 v2, 0x2b

    const v3, -0x100f2561

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x596e1f92

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2550686
    iget-object v1, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->d:LX/2do;

    iget-object v2, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->L:LX/IDV;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2550687
    iget-object v1, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->d:LX/2do;

    iget-object v2, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->M:LX/IDX;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2550688
    iget-object v1, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->d:LX/2do;

    iget-object v2, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->N:LX/IDZ;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2550689
    iget-object v1, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->d:LX/2do;

    iget-object v2, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->O:LX/2hO;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2550690
    iget-object v1, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->p:LX/BPs;

    iget-object v2, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->P:LX/BPt;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2550691
    iget-boolean v1, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->I:Z

    if-eqz v1, :cond_0

    .line 2550692
    iget-object v1, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->w:LX/IDE;

    .line 2550693
    iget-object v2, v1, LX/IDE;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1My;

    invoke-virtual {v2}, LX/1My;->a()V

    .line 2550694
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2550695
    const/16 v1, 0x2b

    const v2, 0x386824bb

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 10

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    const/16 v0, 0x2a

    const v1, -0x43e22bb4

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2550667
    iget-object v1, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->o:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2550668
    iget-boolean v1, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->G:Z

    if-eqz v1, :cond_0

    .line 2550669
    iget-object v1, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->e:LX/IEC;

    invoke-virtual {p0}, Lcom/facebook/friendlist/fragment/FriendListFragment;->b()LX/DHs;

    move-result-object v2

    iget v3, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->H:I

    iget-object v4, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->z:Ljava/lang/String;

    .line 2550670
    iget-object v7, v1, LX/IEC;->a:LX/0Zb;

    invoke-static {v2}, LX/IEC;->a(LX/DHs;)LX/IEB;

    move-result-object v8

    invoke-static {v1, v8, v4}, LX/IEC;->a(LX/IEC;LX/IEB;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    const-string v9, "number_of_friends_seen"

    invoke-virtual {v8, v9, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    invoke-interface {v7, v8}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2550671
    :cond_0
    iget-object v1, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v1, v5}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2550672
    iget-object v1, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v1}, Lcom/facebook/widget/listview/BetterListView;->c()V

    .line 2550673
    iget-object v1, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->u:Landroid/widget/EditText;

    invoke-virtual {v1, v5}, Landroid/widget/EditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2550674
    iget-object v1, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->u:Landroid/widget/EditText;

    invoke-virtual {v1, v5}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2550675
    iget-object v1, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->u:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->K:Landroid/text/TextWatcher;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2550676
    iput-object v5, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->Q:Landroid/widget/AbsListView$OnScrollListener;

    .line 2550677
    iput-object v5, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->r:Landroid/view/View;

    .line 2550678
    iput-object v5, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->a:Lcom/facebook/widget/listview/BetterListView;

    .line 2550679
    iput-object v5, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->b:Landroid/view/View;

    .line 2550680
    iput-object v5, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->s:Landroid/view/View;

    .line 2550681
    iput-object v5, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->t:Landroid/view/View;

    .line 2550682
    iput-object v5, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->u:Landroid/widget/EditText;

    .line 2550683
    iput-object v5, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->v:Landroid/view/View;

    .line 2550684
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2550685
    const/16 v1, 0x2b

    const v2, 0x28b44eb7

    invoke-static {v6, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 13

    .prologue
    const/4 v11, 0x0

    .line 2550594
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2550595
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2550596
    const-string v1, "com.facebook.katana.profile.id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->z:Ljava/lang/String;

    .line 2550597
    const-string v1, "first_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->A:Ljava/lang/String;

    .line 2550598
    iget-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->z:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2550599
    invoke-virtual {p0}, Lcom/facebook/friendlist/fragment/FriendListFragment;->b()LX/DHs;

    move-result-object v4

    .line 2550600
    iget-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->h:LX/IDF;

    iget-object v1, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->z:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, LX/IDF;->a(Ljava/lang/String;LX/DHs;)LX/IDE;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->w:LX/IDE;

    .line 2550601
    iget-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->n:LX/0ad;

    sget-short v1, LX/2ez;->o:S

    invoke-interface {v0, v1, v11}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->I:Z

    .line 2550602
    sget-object v0, LX/DHs;->ALL_FRIENDS:LX/DHs;

    invoke-virtual {v0, v4}, LX/DHs;->equals(Ljava/lang/Object;)Z

    move-result v12

    .line 2550603
    iget-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->q:LX/0W3;

    sget-wide v2, LX/0X5;->gN:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    .line 2550604
    iget-object v1, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->i:LX/IE2;

    .line 2550605
    iput-boolean v12, v1, LX/IE2;->c:Z

    .line 2550606
    invoke-static {v1}, LX/IE2;->e(LX/IE2;)V

    .line 2550607
    iget-object v1, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->i:LX/IE2;

    if-eqz v0, :cond_5

    if-eqz v12, :cond_5

    const/4 v0, 0x1

    .line 2550608
    :goto_0
    iput-boolean v0, v1, LX/IE2;->d:Z

    .line 2550609
    iget-boolean v2, v1, LX/IE2;->d:Z

    if-eqz v2, :cond_0

    .line 2550610
    invoke-static {v1}, LX/IE2;->g(LX/IE2;)V

    .line 2550611
    :cond_0
    iget-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->g:LX/IDz;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->z:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->A:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/facebook/friendlist/fragment/FriendListFragment;->c()LX/DHr;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/IDz;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;LX/DHs;LX/DHr;)LX/IDy;

    move-result-object v9

    .line 2550612
    new-instance v5, LX/DSo;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v6

    iget-object v7, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->i:LX/IE2;

    iget-object v8, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->i:LX/IE2;

    iget-object v10, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->f:LX/IDu;

    invoke-direct/range {v5 .. v10}, LX/DSo;-><init>(Landroid/content/Context;LX/BWf;LX/DSX;LX/DS5;LX/BWd;)V

    iput-object v5, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->x:LX/DSo;

    .line 2550613
    const v0, 0x7f0d12c2

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->a:Lcom/facebook/widget/listview/BetterListView;

    .line 2550614
    iget-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->a:Lcom/facebook/widget/listview/BetterListView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->b:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 2550615
    iget-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->a:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->b:Landroid/view/View;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/widget/listview/BetterListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 2550616
    :cond_1
    iget-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->a:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->x:LX/DSo;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2550617
    iget-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, v12}, Lcom/facebook/widget/listview/BetterListView;->setStickyHeaderEnabled(Z)V

    .line 2550618
    const v0, 0x7f0d2bb3

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->t:Landroid/view/View;

    .line 2550619
    const v0, 0x7f0d0a6b

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->u:Landroid/widget/EditText;

    .line 2550620
    iget-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->u:Landroid/widget/EditText;

    const v1, 0x7f083826

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    .line 2550621
    iget-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->u:Landroid/widget/EditText;

    new-instance v1, LX/IDM;

    invoke-direct {v1, p0}, LX/IDM;-><init>(Lcom/facebook/friendlist/fragment/FriendListFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2550622
    iget-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->u:Landroid/widget/EditText;

    new-instance v1, LX/IDN;

    invoke-direct {v1, p0}, LX/IDN;-><init>(Lcom/facebook/friendlist/fragment/FriendListFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2550623
    iget-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->c:Ljava/lang/String;

    iget-object v1, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->z:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2550624
    iget-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->u:Landroid/widget/EditText;

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2550625
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2550626
    const-string v2, "first_name"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2550627
    const-string v3, "profile_name"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2550628
    if-eqz v2, :cond_8

    .line 2550629
    const v1, 0x7f083827

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v2, v3, v5

    invoke-virtual {p0, v1, v3}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2550630
    :goto_1
    move-object v1, v1

    .line 2550631
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 2550632
    :cond_2
    const v0, 0x7f0d094d

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->v:Landroid/view/View;

    .line 2550633
    iget-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->v:Landroid/view/View;

    new-instance v1, LX/IDO;

    invoke-direct {v1, p0}, LX/IDO;-><init>(Lcom/facebook/friendlist/fragment/FriendListFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2550634
    const v0, 0x7f0d12be

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2550635
    invoke-virtual {p0}, Lcom/facebook/friendlist/fragment/FriendListFragment;->k()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2550636
    const v0, 0x7f0d12bf

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 2550637
    iget-object v1, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->j:LX/ID3;

    .line 2550638
    iget-object v2, v1, LX/ID3;->a:LX/0Uh;

    const/16 v3, 0x449

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v5}, LX/0Uh;->a(IZ)Z

    move-result v2

    move v1, v2

    .line 2550639
    if-eqz v1, :cond_7

    .line 2550640
    iget-object v1, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->z:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    sget-object v1, LX/5P0;->CONTACTS:LX/5P0;

    .line 2550641
    :goto_2
    new-instance v2, LX/IDP;

    invoke-direct {v2, p0, v1}, LX/IDP;-><init>(Lcom/facebook/friendlist/fragment/FriendListFragment;LX/5P0;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2550642
    :goto_3
    new-instance v0, LX/IDQ;

    invoke-direct {v0, p0}, LX/IDQ;-><init>(Lcom/facebook/friendlist/fragment/FriendListFragment;)V

    iput-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->J:Landroid/widget/Filter$FilterListener;

    .line 2550643
    iget-boolean v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->I:Z

    if-nez v0, :cond_4

    .line 2550644
    new-instance v0, LX/IDV;

    invoke-direct {v0, p0}, LX/IDV;-><init>(Lcom/facebook/friendlist/fragment/FriendListFragment;)V

    iput-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->L:LX/IDV;

    .line 2550645
    new-instance v0, LX/IDX;

    invoke-direct {v0, p0}, LX/IDX;-><init>(Lcom/facebook/friendlist/fragment/FriendListFragment;)V

    iput-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->M:LX/IDX;

    .line 2550646
    iget-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->d:LX/2do;

    iget-object v1, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->L:LX/IDV;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2550647
    iget-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->d:LX/2do;

    iget-object v1, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->M:LX/IDX;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2550648
    sget-object v0, LX/DHs;->PYMK:LX/DHs;

    if-eq v4, v0, :cond_3

    sget-object v0, LX/DHs;->SUGGESTIONS:LX/DHs;

    if-ne v4, v0, :cond_4

    .line 2550649
    :cond_3
    new-instance v0, LX/IDW;

    invoke-direct {v0, p0}, LX/IDW;-><init>(Lcom/facebook/friendlist/fragment/FriendListFragment;)V

    iput-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->O:LX/2hO;

    .line 2550650
    iget-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->d:LX/2do;

    iget-object v1, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->O:LX/2hO;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2550651
    :cond_4
    new-instance v0, LX/IDZ;

    invoke-direct {v0, p0}, LX/IDZ;-><init>(Lcom/facebook/friendlist/fragment/FriendListFragment;)V

    iput-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->N:LX/IDZ;

    .line 2550652
    iget-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->d:LX/2do;

    iget-object v1, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->N:LX/IDZ;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2550653
    new-instance v0, LX/IDY;

    invoke-direct {v0, p0}, LX/IDY;-><init>(Lcom/facebook/friendlist/fragment/FriendListFragment;)V

    iput-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->P:LX/BPt;

    .line 2550654
    iget-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->p:LX/BPs;

    iget-object v1, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->P:LX/BPt;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2550655
    iget-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->a:Lcom/facebook/widget/listview/BetterListView;

    new-instance v1, LX/IDR;

    invoke-direct {v1, p0}, LX/IDR;-><init>(Lcom/facebook/friendlist/fragment/FriendListFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2550656
    new-instance v0, LX/IDS;

    invoke-direct {v0, p0}, LX/IDS;-><init>(Lcom/facebook/friendlist/fragment/FriendListFragment;)V

    iput-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->K:Landroid/text/TextWatcher;

    .line 2550657
    iget-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->u:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->K:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2550658
    iget-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->a:Lcom/facebook/widget/listview/BetterListView;

    new-instance v1, LX/IDT;

    invoke-direct {v1, p0}, LX/IDT;-><init>(Lcom/facebook/friendlist/fragment/FriendListFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setOnDrawListenerTo(LX/0fu;)V

    .line 2550659
    invoke-static {p0}, Lcom/facebook/friendlist/fragment/FriendListFragment;->m(Lcom/facebook/friendlist/fragment/FriendListFragment;)V

    .line 2550660
    return-void

    :cond_5
    move v0, v11

    .line 2550661
    goto/16 :goto_0

    .line 2550662
    :cond_6
    sget-object v1, LX/5P0;->SUGGESTIONS:LX/5P0;

    goto/16 :goto_2

    .line 2550663
    :cond_7
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_3

    .line 2550664
    :cond_8
    if-eqz v1, :cond_9

    .line 2550665
    const v2, 0x7f083827

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v1, v3, v5

    invoke-virtual {p0, v2, v3}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    .line 2550666
    :cond_9
    const v1, 0x7f083828

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1
.end method

.method public final setUserVisibleHint(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2550580
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->setUserVisibleHint(Z)V

    .line 2550581
    if-nez p1, :cond_2

    .line 2550582
    iget-boolean v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->C:Z

    if-nez v0, :cond_1

    .line 2550583
    iput-boolean v3, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->C:Z

    .line 2550584
    :cond_0
    :goto_0
    return-void

    .line 2550585
    :cond_1
    iget-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->B:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-virtual {p0}, Lcom/facebook/friendlist/fragment/FriendListFragment;->e()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    goto :goto_0

    .line 2550586
    :cond_2
    iget-boolean v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->D:Z

    if-nez v0, :cond_3

    .line 2550587
    iput-boolean v3, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->D:Z

    .line 2550588
    iget-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->B:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-virtual {p0}, Lcom/facebook/friendlist/fragment/FriendListFragment;->e()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 2550589
    :cond_3
    iget-boolean v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->E:Z

    if-eqz v0, :cond_4

    .line 2550590
    iget-object v1, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->B:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-virtual {p0}, Lcom/facebook/friendlist/fragment/FriendListFragment;->e()I

    move-result v2

    iget-boolean v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->F:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x2

    :goto_1
    invoke-interface {v1, v2, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2550591
    :cond_4
    iget-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->i:LX/IE2;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->i:LX/IE2;

    invoke-virtual {v0}, LX/IE2;->a()I

    move-result v0

    if-lez v0, :cond_0

    .line 2550592
    iput-boolean v3, p0, Lcom/facebook/friendlist/fragment/FriendListFragment;->G:Z

    goto :goto_0

    .line 2550593
    :cond_5
    const/4 v0, 0x3

    goto :goto_1
.end method
