.class public final Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchRecentlyAddedFriendListQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x5156c394
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchRecentlyAddedFriendListQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchRecentlyAddedFriendListQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchRecentlyAddedFriendListQueryModel$FriendsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2552379
    const-class v0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchRecentlyAddedFriendListQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2552378
    const-class v0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchRecentlyAddedFriendListQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2552376
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2552377
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2552370
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2552371
    invoke-virtual {p0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchRecentlyAddedFriendListQueryModel;->a()Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchRecentlyAddedFriendListQueryModel$FriendsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2552372
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2552373
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2552374
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2552375
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2552362
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2552363
    invoke-virtual {p0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchRecentlyAddedFriendListQueryModel;->a()Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchRecentlyAddedFriendListQueryModel$FriendsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2552364
    invoke-virtual {p0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchRecentlyAddedFriendListQueryModel;->a()Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchRecentlyAddedFriendListQueryModel$FriendsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchRecentlyAddedFriendListQueryModel$FriendsModel;

    .line 2552365
    invoke-virtual {p0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchRecentlyAddedFriendListQueryModel;->a()Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchRecentlyAddedFriendListQueryModel$FriendsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2552366
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchRecentlyAddedFriendListQueryModel;

    .line 2552367
    iput-object v0, v1, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchRecentlyAddedFriendListQueryModel;->e:Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchRecentlyAddedFriendListQueryModel$FriendsModel;

    .line 2552368
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2552369
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2552361
    new-instance v0, LX/IEP;

    invoke-direct {v0, p1}, LX/IEP;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchRecentlyAddedFriendListQueryModel$FriendsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getFriends"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2552380
    iget-object v0, p0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchRecentlyAddedFriendListQueryModel;->e:Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchRecentlyAddedFriendListQueryModel$FriendsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchRecentlyAddedFriendListQueryModel$FriendsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchRecentlyAddedFriendListQueryModel$FriendsModel;

    iput-object v0, p0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchRecentlyAddedFriendListQueryModel;->e:Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchRecentlyAddedFriendListQueryModel$FriendsModel;

    .line 2552381
    iget-object v0, p0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchRecentlyAddedFriendListQueryModel;->e:Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchRecentlyAddedFriendListQueryModel$FriendsModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2552359
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2552360
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2552358
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2552355
    new-instance v0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchRecentlyAddedFriendListQueryModel;

    invoke-direct {v0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchRecentlyAddedFriendListQueryModel;-><init>()V

    .line 2552356
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2552357
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2552354
    const v0, -0x208f3c74

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2552353
    const v0, 0x285feb

    return v0
.end method
