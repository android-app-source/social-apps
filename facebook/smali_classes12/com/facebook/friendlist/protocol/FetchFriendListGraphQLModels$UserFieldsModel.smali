.class public final Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0xb56867d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z

.field private h:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel$StructuredNameModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2552746
    const-class v0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2552747
    const-class v0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2552748
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2552749
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 4

    .prologue
    .line 2552750
    iput-object p1, p0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2552751
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2552752
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2552753
    if-eqz v0, :cond_0

    .line 2552754
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 2552755
    :cond_0
    return-void

    .line 2552756
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V
    .locals 4

    .prologue
    .line 2552757
    iput-object p1, p0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->l:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 2552758
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2552759
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2552760
    if-eqz v0, :cond_0

    .line 2552761
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x7

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 2552762
    :cond_0
    return-void

    .line 2552763
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 2552784
    iput-boolean p1, p0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->g:Z

    .line 2552785
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2552786
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2552787
    if-eqz v0, :cond_0

    .line 2552788
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 2552789
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 2552764
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2552765
    invoke-virtual {p0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->j()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 2552766
    invoke-virtual {p0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2552767
    invoke-virtual {p0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->m()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v4, 0x5757c31

    invoke-static {v3, v2, v4}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$DraculaImplementation;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2552768
    invoke-virtual {p0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->n()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2552769
    invoke-virtual {p0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2552770
    invoke-virtual {p0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->p()Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel$StructuredNameModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 2552771
    invoke-virtual {p0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->q()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    .line 2552772
    const/16 v7, 0x9

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 2552773
    invoke-virtual {p1, v8, v0}, LX/186;->b(II)V

    .line 2552774
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2552775
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->g:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2552776
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2552777
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2552778
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2552779
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2552780
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2552781
    const/16 v0, 0x8

    iget v1, p0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->m:I

    invoke-virtual {p1, v0, v1, v8}, LX/186;->a(III)V

    .line 2552782
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2552783
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2552796
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2552797
    invoke-virtual {p0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2552798
    invoke-virtual {p0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->m()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x5757c31

    invoke-static {v2, v0, v3}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2552799
    invoke-virtual {p0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->m()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2552800
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;

    .line 2552801
    iput v3, v0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->h:I

    move-object v1, v0

    .line 2552802
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2552803
    invoke-virtual {p0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    .line 2552804
    invoke-virtual {p0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2552805
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;

    .line 2552806
    iput-object v0, v1, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    .line 2552807
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->p()Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel$StructuredNameModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2552808
    invoke-virtual {p0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->p()Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel$StructuredNameModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel$StructuredNameModel;

    .line 2552809
    invoke-virtual {p0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->p()Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel$StructuredNameModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2552810
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;

    .line 2552811
    iput-object v0, v1, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->k:Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel$StructuredNameModel;

    .line 2552812
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2552813
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    .line 2552814
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_3
    move-object p0, v1

    .line 2552815
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2552708
    new-instance v0, LX/IES;

    invoke-direct {v0, p1}, LX/IES;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2552795
    invoke-virtual {p0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2552790
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2552791
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->g:Z

    .line 2552792
    const/4 v0, 0x3

    const v1, 0x5757c31

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->h:I

    .line 2552793
    const/16 v0, 0x8

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->m:I

    .line 2552794
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 2552725
    const-string v0, "friendship_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2552726
    invoke-virtual {p0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->j()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2552727
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2552728
    const/4 v0, 0x0

    iput v0, p2, LX/18L;->c:I

    .line 2552729
    :goto_0
    return-void

    .line 2552730
    :cond_0
    const-string v0, "local_is_pymk_blacklisted"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2552731
    invoke-virtual {p0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->l()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2552732
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2552733
    const/4 v0, 0x2

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 2552734
    :cond_1
    const-string v0, "subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2552735
    invoke-virtual {p0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->q()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2552736
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2552737
    const/4 v0, 0x7

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 2552738
    :cond_2
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 2552739
    const-string v0, "friendship_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2552740
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-direct {p0, p2}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 2552741
    :cond_0
    :goto_0
    return-void

    .line 2552742
    :cond_1
    const-string v0, "local_is_pymk_blacklisted"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2552743
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->a(Z)V

    goto :goto_0

    .line 2552744
    :cond_2
    const-string v0, "subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2552745
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-direct {p0, p2}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->a(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2552701
    new-instance v0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;

    invoke-direct {v0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;-><init>()V

    .line 2552702
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2552703
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2552704
    const v0, 0x5388fd7f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2552705
    const v0, 0x285feb

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2552706
    iget-object v0, p0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2552707
    iget-object v0, p0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2552709
    iget-object v0, p0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->f:Ljava/lang/String;

    .line 2552710
    iget-object v0, p0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Z
    .locals 2

    .prologue
    .line 2552711
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2552712
    iget-boolean v0, p0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->g:Z

    return v0
.end method

.method public final m()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getMutualFriends"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2552713
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2552714
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->h:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2552715
    iget-object v0, p0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->i:Ljava/lang/String;

    .line 2552716
    iget-object v0, p0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2552717
    iget-object v0, p0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    iput-object v0, p0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    .line 2552718
    iget-object v0, p0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    return-object v0
.end method

.method public final p()Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel$StructuredNameModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2552719
    iget-object v0, p0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->k:Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel$StructuredNameModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel$StructuredNameModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel$StructuredNameModel;

    iput-object v0, p0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->k:Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel$StructuredNameModel;

    .line 2552720
    iget-object v0, p0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->k:Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel$StructuredNameModel;

    return-object v0
.end method

.method public final q()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2552721
    iget-object v0, p0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->l:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iput-object v0, p0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->l:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 2552722
    iget-object v0, p0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->l:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    return-object v0
.end method

.method public final r()I
    .locals 2

    .prologue
    .line 2552723
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2552724
    iget v0, p0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$UserFieldsModel;->m:I

    return v0
.end method
