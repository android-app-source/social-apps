.class public final Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchFriendListQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x30829c7c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchFriendListQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchFriendListQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchFriendListQueryModel$FriendsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2552015
    const-class v0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchFriendListQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2552014
    const-class v0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchFriendListQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2552012
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2552013
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2552006
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2552007
    invoke-virtual {p0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchFriendListQueryModel;->a()Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchFriendListQueryModel$FriendsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2552008
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2552009
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2552010
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2552011
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2551987
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2551988
    invoke-virtual {p0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchFriendListQueryModel;->a()Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchFriendListQueryModel$FriendsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2551989
    invoke-virtual {p0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchFriendListQueryModel;->a()Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchFriendListQueryModel$FriendsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchFriendListQueryModel$FriendsModel;

    .line 2551990
    invoke-virtual {p0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchFriendListQueryModel;->a()Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchFriendListQueryModel$FriendsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2551991
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchFriendListQueryModel;

    .line 2551992
    iput-object v0, v1, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchFriendListQueryModel;->e:Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchFriendListQueryModel$FriendsModel;

    .line 2551993
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2551994
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2552005
    new-instance v0, LX/IEN;

    invoke-direct {v0, p1}, LX/IEN;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchFriendListQueryModel$FriendsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getFriends"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2552003
    iget-object v0, p0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchFriendListQueryModel;->e:Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchFriendListQueryModel$FriendsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchFriendListQueryModel$FriendsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchFriendListQueryModel$FriendsModel;

    iput-object v0, p0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchFriendListQueryModel;->e:Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchFriendListQueryModel$FriendsModel;

    .line 2552004
    iget-object v0, p0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchFriendListQueryModel;->e:Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchFriendListQueryModel$FriendsModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2552001
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2552002
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2552000
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2551997
    new-instance v0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchFriendListQueryModel;

    invoke-direct {v0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchFriendListQueryModel;-><init>()V

    .line 2551998
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2551999
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2551996
    const v0, 0x1f24e72f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2551995
    const v0, 0x285feb

    return v0
.end method
