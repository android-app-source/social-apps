.class public final Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchMutualFriendListQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x7195295a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchMutualFriendListQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchMutualFriendListQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchMutualFriendListQueryModel$MutualFriendsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2552136
    const-class v0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchMutualFriendListQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2552137
    const-class v0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchMutualFriendListQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2552111
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2552112
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2552130
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2552131
    invoke-virtual {p0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchMutualFriendListQueryModel;->a()Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchMutualFriendListQueryModel$MutualFriendsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2552132
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2552133
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2552134
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2552135
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2552122
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2552123
    invoke-virtual {p0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchMutualFriendListQueryModel;->a()Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchMutualFriendListQueryModel$MutualFriendsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2552124
    invoke-virtual {p0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchMutualFriendListQueryModel;->a()Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchMutualFriendListQueryModel$MutualFriendsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchMutualFriendListQueryModel$MutualFriendsModel;

    .line 2552125
    invoke-virtual {p0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchMutualFriendListQueryModel;->a()Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchMutualFriendListQueryModel$MutualFriendsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2552126
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchMutualFriendListQueryModel;

    .line 2552127
    iput-object v0, v1, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchMutualFriendListQueryModel;->e:Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchMutualFriendListQueryModel$MutualFriendsModel;

    .line 2552128
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2552129
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2552121
    new-instance v0, LX/IEO;

    invoke-direct {v0, p1}, LX/IEO;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchMutualFriendListQueryModel$MutualFriendsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getMutualFriends"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2552138
    iget-object v0, p0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchMutualFriendListQueryModel;->e:Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchMutualFriendListQueryModel$MutualFriendsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchMutualFriendListQueryModel$MutualFriendsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchMutualFriendListQueryModel$MutualFriendsModel;

    iput-object v0, p0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchMutualFriendListQueryModel;->e:Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchMutualFriendListQueryModel$MutualFriendsModel;

    .line 2552139
    iget-object v0, p0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchMutualFriendListQueryModel;->e:Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchMutualFriendListQueryModel$MutualFriendsModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2552119
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2552120
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2552118
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2552115
    new-instance v0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchMutualFriendListQueryModel;

    invoke-direct {v0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchMutualFriendListQueryModel;-><init>()V

    .line 2552116
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2552117
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2552114
    const v0, 0x7d24eccd

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2552113
    const v0, 0x285feb

    return v0
.end method
