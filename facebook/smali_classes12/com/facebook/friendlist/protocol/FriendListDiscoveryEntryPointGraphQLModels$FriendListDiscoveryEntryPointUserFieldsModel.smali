.class public final Lcom/facebook/friendlist/protocol/FriendListDiscoveryEntryPointGraphQLModels$FriendListDiscoveryEntryPointUserFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x12f5cf3d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendlist/protocol/FriendListDiscoveryEntryPointGraphQLModels$FriendListDiscoveryEntryPointUserFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friendlist/protocol/FriendListDiscoveryEntryPointGraphQLModels$FriendListDiscoveryEntryPointUserFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/friendlist/protocol/FriendListDiscoveryEntryPointGraphQLModels$FriendListDiscoveryEntryPointUserFieldsModel$FriendsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2553295
    const-class v0, Lcom/facebook/friendlist/protocol/FriendListDiscoveryEntryPointGraphQLModels$FriendListDiscoveryEntryPointUserFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2553323
    const-class v0, Lcom/facebook/friendlist/protocol/FriendListDiscoveryEntryPointGraphQLModels$FriendListDiscoveryEntryPointUserFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2553321
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2553322
    return-void
.end method

.method private j()Lcom/facebook/friendlist/protocol/FriendListDiscoveryEntryPointGraphQLModels$FriendListDiscoveryEntryPointUserFieldsModel$FriendsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2553319
    iget-object v0, p0, Lcom/facebook/friendlist/protocol/FriendListDiscoveryEntryPointGraphQLModels$FriendListDiscoveryEntryPointUserFieldsModel;->e:Lcom/facebook/friendlist/protocol/FriendListDiscoveryEntryPointGraphQLModels$FriendListDiscoveryEntryPointUserFieldsModel$FriendsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/friendlist/protocol/FriendListDiscoveryEntryPointGraphQLModels$FriendListDiscoveryEntryPointUserFieldsModel$FriendsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendlist/protocol/FriendListDiscoveryEntryPointGraphQLModels$FriendListDiscoveryEntryPointUserFieldsModel$FriendsModel;

    iput-object v0, p0, Lcom/facebook/friendlist/protocol/FriendListDiscoveryEntryPointGraphQLModels$FriendListDiscoveryEntryPointUserFieldsModel;->e:Lcom/facebook/friendlist/protocol/FriendListDiscoveryEntryPointGraphQLModels$FriendListDiscoveryEntryPointUserFieldsModel$FriendsModel;

    .line 2553320
    iget-object v0, p0, Lcom/facebook/friendlist/protocol/FriendListDiscoveryEntryPointGraphQLModels$FriendListDiscoveryEntryPointUserFieldsModel;->e:Lcom/facebook/friendlist/protocol/FriendListDiscoveryEntryPointGraphQLModels$FriendListDiscoveryEntryPointUserFieldsModel$FriendsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2553313
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2553314
    invoke-direct {p0}, Lcom/facebook/friendlist/protocol/FriendListDiscoveryEntryPointGraphQLModels$FriendListDiscoveryEntryPointUserFieldsModel;->j()Lcom/facebook/friendlist/protocol/FriendListDiscoveryEntryPointGraphQLModels$FriendListDiscoveryEntryPointUserFieldsModel$FriendsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2553315
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2553316
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2553317
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2553318
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2553305
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2553306
    invoke-direct {p0}, Lcom/facebook/friendlist/protocol/FriendListDiscoveryEntryPointGraphQLModels$FriendListDiscoveryEntryPointUserFieldsModel;->j()Lcom/facebook/friendlist/protocol/FriendListDiscoveryEntryPointGraphQLModels$FriendListDiscoveryEntryPointUserFieldsModel$FriendsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2553307
    invoke-direct {p0}, Lcom/facebook/friendlist/protocol/FriendListDiscoveryEntryPointGraphQLModels$FriendListDiscoveryEntryPointUserFieldsModel;->j()Lcom/facebook/friendlist/protocol/FriendListDiscoveryEntryPointGraphQLModels$FriendListDiscoveryEntryPointUserFieldsModel$FriendsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendlist/protocol/FriendListDiscoveryEntryPointGraphQLModels$FriendListDiscoveryEntryPointUserFieldsModel$FriendsModel;

    .line 2553308
    invoke-direct {p0}, Lcom/facebook/friendlist/protocol/FriendListDiscoveryEntryPointGraphQLModels$FriendListDiscoveryEntryPointUserFieldsModel;->j()Lcom/facebook/friendlist/protocol/FriendListDiscoveryEntryPointGraphQLModels$FriendListDiscoveryEntryPointUserFieldsModel$FriendsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2553309
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendlist/protocol/FriendListDiscoveryEntryPointGraphQLModels$FriendListDiscoveryEntryPointUserFieldsModel;

    .line 2553310
    iput-object v0, v1, Lcom/facebook/friendlist/protocol/FriendListDiscoveryEntryPointGraphQLModels$FriendListDiscoveryEntryPointUserFieldsModel;->e:Lcom/facebook/friendlist/protocol/FriendListDiscoveryEntryPointGraphQLModels$FriendListDiscoveryEntryPointUserFieldsModel$FriendsModel;

    .line 2553311
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2553312
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2553324
    new-instance v0, LX/IEi;

    invoke-direct {v0, p1}, LX/IEi;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final synthetic a()Lcom/facebook/friendlist/protocol/FriendListDiscoveryEntryPointGraphQLModels$FriendListDiscoveryEntryPointUserFieldsModel$FriendsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2553304
    invoke-direct {p0}, Lcom/facebook/friendlist/protocol/FriendListDiscoveryEntryPointGraphQLModels$FriendListDiscoveryEntryPointUserFieldsModel;->j()Lcom/facebook/friendlist/protocol/FriendListDiscoveryEntryPointGraphQLModels$FriendListDiscoveryEntryPointUserFieldsModel$FriendsModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2553302
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2553303
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2553301
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2553298
    new-instance v0, Lcom/facebook/friendlist/protocol/FriendListDiscoveryEntryPointGraphQLModels$FriendListDiscoveryEntryPointUserFieldsModel;

    invoke-direct {v0}, Lcom/facebook/friendlist/protocol/FriendListDiscoveryEntryPointGraphQLModels$FriendListDiscoveryEntryPointUserFieldsModel;-><init>()V

    .line 2553299
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2553300
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2553297
    const v0, 0x69169694

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2553296
    const v0, 0x285feb

    return v0
.end method
