.class public final Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchWithNewPostsFriendListQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x273117cb
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchWithNewPostsFriendListQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchWithNewPostsFriendListQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchWithNewPostsFriendListQueryModel$FriendsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2552627
    const-class v0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchWithNewPostsFriendListQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2552601
    const-class v0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchWithNewPostsFriendListQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2552625
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2552626
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2552619
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2552620
    invoke-virtual {p0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchWithNewPostsFriendListQueryModel;->a()Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchWithNewPostsFriendListQueryModel$FriendsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2552621
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2552622
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2552623
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2552624
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2552611
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2552612
    invoke-virtual {p0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchWithNewPostsFriendListQueryModel;->a()Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchWithNewPostsFriendListQueryModel$FriendsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2552613
    invoke-virtual {p0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchWithNewPostsFriendListQueryModel;->a()Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchWithNewPostsFriendListQueryModel$FriendsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchWithNewPostsFriendListQueryModel$FriendsModel;

    .line 2552614
    invoke-virtual {p0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchWithNewPostsFriendListQueryModel;->a()Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchWithNewPostsFriendListQueryModel$FriendsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2552615
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchWithNewPostsFriendListQueryModel;

    .line 2552616
    iput-object v0, v1, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchWithNewPostsFriendListQueryModel;->e:Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchWithNewPostsFriendListQueryModel$FriendsModel;

    .line 2552617
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2552618
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2552610
    new-instance v0, LX/IER;

    invoke-direct {v0, p1}, LX/IER;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchWithNewPostsFriendListQueryModel$FriendsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getFriends"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2552628
    iget-object v0, p0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchWithNewPostsFriendListQueryModel;->e:Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchWithNewPostsFriendListQueryModel$FriendsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchWithNewPostsFriendListQueryModel$FriendsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchWithNewPostsFriendListQueryModel$FriendsModel;

    iput-object v0, p0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchWithNewPostsFriendListQueryModel;->e:Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchWithNewPostsFriendListQueryModel$FriendsModel;

    .line 2552629
    iget-object v0, p0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchWithNewPostsFriendListQueryModel;->e:Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchWithNewPostsFriendListQueryModel$FriendsModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2552608
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2552609
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2552607
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2552604
    new-instance v0, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchWithNewPostsFriendListQueryModel;

    invoke-direct {v0}, Lcom/facebook/friendlist/protocol/FetchFriendListGraphQLModels$FetchWithNewPostsFriendListQueryModel;-><init>()V

    .line 2552605
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2552606
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2552603
    const v0, 0xdd8ca4d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2552602
    const v0, 0x285feb

    return v0
.end method
