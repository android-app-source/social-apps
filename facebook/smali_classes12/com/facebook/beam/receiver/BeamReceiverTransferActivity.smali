.class public Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# static fields
.field public static final u:Ljava/lang/Long;


# instance fields
.field public p:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/SingleThreadedExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public q:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public r:LX/HmV;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public s:LX/Hmf;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public t:LX/0W3;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public v:LX/HmM;

.field private w:LX/0TD;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2500603
    const-wide/32 v0, 0xf4240

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->u:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2500601
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2500602
    return-void
.end method

.method private static a(Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;LX/HmV;LX/Hmf;LX/0W3;)V
    .locals 0

    .prologue
    .line 2500600
    iput-object p1, p0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->p:Ljava/util/concurrent/ExecutorService;

    iput-object p2, p0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->q:Ljava/util/concurrent/ExecutorService;

    iput-object p3, p0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->r:LX/HmV;

    iput-object p4, p0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->s:LX/Hmf;

    iput-object p5, p0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->t:LX/0W3;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 6

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    invoke-static {v5}, LX/0UA;->b(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ExecutorService;

    invoke-static {v5}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ExecutorService;

    invoke-static {v5}, LX/HmV;->b(LX/0QB;)LX/HmV;

    move-result-object v3

    check-cast v3, LX/HmV;

    invoke-static {v5}, LX/Hmf;->a(LX/0QB;)LX/Hmf;

    move-result-object v4

    check-cast v4, LX/Hmf;

    invoke-static {v5}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v5

    check-cast v5, LX/0W3;

    invoke-static/range {v0 .. v5}, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->a(Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;LX/HmV;LX/Hmf;LX/0W3;)V

    return-void
.end method

.method public static b(Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;)V
    .locals 3

    .prologue
    .line 2500597
    iget-object v0, p0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->w:LX/0TD;

    new-instance v1, LX/HmD;

    invoke-direct {v1, p0}, LX/HmD;-><init>(Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2500598
    new-instance v1, LX/HmE;

    invoke-direct {v1, p0}, LX/HmE;-><init>(Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;)V

    iget-object v2, p0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->q:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2500599
    return-void
.end method

.method public static l(Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;)V
    .locals 3

    .prologue
    .line 2500586
    iget-object v0, p0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->s:LX/Hmf;

    .line 2500587
    sget-object v1, LX/Hmd;->INITIALIZING:LX/Hmd;

    invoke-static {v0, v1}, LX/Hmf;->a(LX/Hmf;LX/Hmd;)V

    .line 2500588
    iget-object v0, p0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->r:LX/HmV;

    new-instance v1, LX/HmG;

    invoke-direct {v1, p0}, LX/HmG;-><init>(Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;)V

    .line 2500589
    iget-object v2, v0, LX/HmV;->f:LX/Hmc;

    .line 2500590
    sget-object p0, LX/Hmb;->INITIALIZE:LX/Hmb;

    invoke-static {v2, p0}, LX/Hmc;->a(LX/Hmc;LX/Hmb;)V

    .line 2500591
    iget-object v2, v0, LX/HmV;->k:LX/HmU;

    sget-object p0, LX/HmU;->WAIT_INITIALIZE:LX/HmU;

    if-ne v2, p0, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, LX/0Tp;->a(Z)V

    .line 2500592
    sget-object v2, LX/HmU;->INITIALIZE:LX/HmU;

    iput-object v2, v0, LX/HmV;->k:LX/HmU;

    .line 2500593
    iget-object v2, v0, LX/HmV;->b:LX/0TD;

    new-instance p0, LX/HmO;

    invoke-direct {p0, v0}, LX/HmO;-><init>(LX/HmV;)V

    invoke-interface {v2, p0}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 2500594
    invoke-static {v0, v2, v1}, LX/HmV;->a(LX/HmV;Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2500595
    return-void

    .line 2500596
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static m(Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;)V
    .locals 3

    .prologue
    .line 2500573
    iget-object v0, p0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->s:LX/Hmf;

    .line 2500574
    sget-object v1, LX/Hmd;->PREFLIGHT_INFO_SENDING:LX/Hmd;

    invoke-static {v0, v1}, LX/Hmf;->a(LX/Hmf;LX/Hmd;)V

    .line 2500575
    iget-object v0, p0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->r:LX/HmV;

    new-instance v1, LX/HmH;

    invoke-direct {v1, p0}, LX/HmH;-><init>(Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;)V

    .line 2500576
    iget-object v2, v0, LX/HmV;->f:LX/Hmc;

    .line 2500577
    sget-object p0, LX/Hmb;->SEND_RECEIVE_PREFLIGHT:LX/Hmb;

    invoke-static {v2, p0}, LX/Hmc;->a(LX/Hmc;LX/Hmb;)V

    .line 2500578
    iget-object v2, v0, LX/HmV;->k:LX/HmU;

    sget-object p0, LX/HmU;->WAIT_SEND_RECEIVE_PREFLIGHT:LX/HmU;

    if-ne v2, p0, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, LX/0Tp;->a(Z)V

    .line 2500579
    sget-object v2, LX/HmU;->SEND_RECEIVE_PREFLIGHT:LX/HmU;

    iput-object v2, v0, LX/HmV;->k:LX/HmU;

    .line 2500580
    iget-object v2, v0, LX/HmV;->i:LX/Hm2;

    invoke-virtual {v2}, LX/Hm2;->a()Lcom/facebook/beam/protocol/BeamPreflightInfo;

    move-result-object v2

    iput-object v2, v0, LX/HmV;->q:Lcom/facebook/beam/protocol/BeamPreflightInfo;

    .line 2500581
    iget-object v2, v0, LX/HmV;->q:Lcom/facebook/beam/protocol/BeamPreflightInfo;

    invoke-virtual {v2}, LX/Hm1;->a()Ljava/lang/String;

    .line 2500582
    iget-object v2, v0, LX/HmV;->b:LX/0TD;

    new-instance p0, LX/HmP;

    invoke-direct {p0, v0}, LX/HmP;-><init>(LX/HmV;)V

    invoke-interface {v2, p0}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 2500583
    invoke-static {v0, v2, v1}, LX/HmV;->a(LX/HmV;Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2500584
    return-void

    .line 2500585
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static n(Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;)V
    .locals 3

    .prologue
    .line 2500604
    iget-object v0, p0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->r:LX/HmV;

    new-instance v1, LX/HmI;

    invoke-direct {v1, p0}, LX/HmI;-><init>(Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;)V

    .line 2500605
    iget-object v2, v0, LX/HmV;->f:LX/Hmc;

    .line 2500606
    sget-object p0, LX/Hmb;->RECEIVE_INTEND_TO_SEND:LX/Hmb;

    invoke-static {v2, p0}, LX/Hmc;->a(LX/Hmc;LX/Hmb;)V

    .line 2500607
    iget-object v2, v0, LX/HmV;->k:LX/HmU;

    sget-object p0, LX/HmU;->WAIT_RECEIVE_INTEND_TO_SEND:LX/HmU;

    if-ne v2, p0, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, LX/0Tp;->a(Z)V

    .line 2500608
    sget-object v2, LX/HmU;->RECEIVE_INTEND_TO_SEND:LX/HmU;

    iput-object v2, v0, LX/HmV;->k:LX/HmU;

    .line 2500609
    iget-object v2, v0, LX/HmV;->b:LX/0TD;

    new-instance p0, LX/HmQ;

    invoke-direct {p0, v0}, LX/HmQ;-><init>(LX/HmV;)V

    invoke-interface {v2, p0}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 2500610
    invoke-static {v0, v2, v1}, LX/HmV;->a(LX/HmV;Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2500611
    return-void

    .line 2500612
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static o(Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;)V
    .locals 8

    .prologue
    .line 2500545
    iget-object v0, p0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->s:LX/Hmf;

    iget-object v1, p0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->r:LX/HmV;

    .line 2500546
    iget-object v2, v1, LX/HmV;->o:Lcom/facebook/beam/protocol/BeamPreflightInfo;

    move-object v1, v2

    .line 2500547
    iget-object v2, p0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->r:LX/HmV;

    .line 2500548
    iget-object v3, v2, LX/HmV;->p:Ljava/lang/String;

    move-object v2, v3

    .line 2500549
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v3

    const-string v4, "beamTransactionID"

    invoke-virtual {v3, v4, v2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v3

    const-string v4, "senderUserId"

    .line 2500550
    iget-object v5, v1, Lcom/facebook/beam/protocol/BeamPreflightInfo;->mUserInfo:Lcom/facebook/beam/protocol/BeamUserInfo;

    move-object v5, v5

    .line 2500551
    iget-object v5, v5, Lcom/facebook/beam/protocol/BeamUserInfo;->mUserId:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v3

    const-string v4, "senderDeviceBrand"

    .line 2500552
    iget-object v5, v1, Lcom/facebook/beam/protocol/BeamPreflightInfo;->mDeviceInfo:Lcom/facebook/beam/protocol/BeamDeviceInfo;

    move-object v5, v5

    .line 2500553
    iget-object v5, v5, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mDeviceBrand:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v3

    const-string v4, "senderDeviceModel"

    .line 2500554
    iget-object v5, v1, Lcom/facebook/beam/protocol/BeamPreflightInfo;->mDeviceInfo:Lcom/facebook/beam/protocol/BeamDeviceInfo;

    move-object v5, v5

    .line 2500555
    iget-object v5, v5, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mDeviceModel:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v3

    const-string v4, "senderApkVersion"

    .line 2500556
    iget-object v5, v1, Lcom/facebook/beam/protocol/BeamPreflightInfo;->mPackageInfo:Lcom/facebook/beam/protocol/BeamPackageInfo;

    move-object v5, v5

    .line 2500557
    iget v5, v5, Lcom/facebook/beam/protocol/BeamPackageInfo;->mVersionCode:I

    invoke-virtual {v3, v4, v5}, LX/1rQ;->a(Ljava/lang/String;I)LX/1rQ;

    move-result-object v3

    .line 2500558
    iget-object v4, v0, LX/Hmf;->a:LX/0if;

    sget-object v5, LX/0ig;->aX:LX/0ih;

    sget-object v6, LX/Hmd;->APK_RECEIVING:LX/Hmd;

    iget-object v6, v6, LX/Hmd;->actionName:Ljava/lang/String;

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v6, v7, v3}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2500559
    iget-object v0, p0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->r:LX/HmV;

    .line 2500560
    iget-object v1, v0, LX/HmV;->o:Lcom/facebook/beam/protocol/BeamPreflightInfo;

    move-object v0, v1

    .line 2500561
    iget-object v1, p0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->v:LX/HmM;

    const v2, 0x7f08394a

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 2500562
    iget-object v5, v0, Lcom/facebook/beam/protocol/BeamPreflightInfo;->mUserInfo:Lcom/facebook/beam/protocol/BeamUserInfo;

    move-object v0, v5

    .line 2500563
    iget-object v0, v0, Lcom/facebook/beam/protocol/BeamUserInfo;->mDisplayName:Ljava/lang/String;

    aput-object v0, v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/HmM;->a(Ljava/lang/String;)V

    .line 2500564
    iget-object v0, p0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->r:LX/HmV;

    new-instance v1, LX/HmJ;

    invoke-direct {v1, p0}, LX/HmJ;-><init>(Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;)V

    .line 2500565
    iget-object v2, v0, LX/HmV;->f:LX/Hmc;

    .line 2500566
    sget-object v3, LX/Hmb;->RECEIVE_APK:LX/Hmb;

    invoke-static {v2, v3}, LX/Hmc;->a(LX/Hmc;LX/Hmb;)V

    .line 2500567
    iget-object v2, v0, LX/HmV;->k:LX/HmU;

    sget-object v3, LX/HmU;->WAIT_RECEIVE_APK:LX/HmU;

    if-ne v2, v3, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, LX/0Tp;->a(Z)V

    .line 2500568
    sget-object v2, LX/HmU;->RECEIVE_APK:LX/HmU;

    iput-object v2, v0, LX/HmV;->k:LX/HmU;

    .line 2500569
    iget-object v2, v0, LX/HmV;->b:LX/0TD;

    new-instance v3, LX/HmR;

    invoke-direct {v3, v0}, LX/HmR;-><init>(LX/HmV;)V

    invoke-interface {v2, v3}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 2500570
    invoke-static {v0, v2, v1}, LX/HmV;->a(LX/HmV;Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2500571
    return-void

    .line 2500572
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static p(Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;)V
    .locals 3

    .prologue
    .line 2500533
    iget-object v0, p0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->s:LX/Hmf;

    .line 2500534
    sget-object v1, LX/Hmd;->APK_VERIFYING:LX/Hmd;

    invoke-static {v0, v1}, LX/Hmf;->a(LX/Hmf;LX/Hmd;)V

    .line 2500535
    iget-object v0, p0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->v:LX/HmM;

    const v1, 0x7f08394b

    invoke-virtual {p0, v1}, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/HmM;->a(Ljava/lang/String;)V

    .line 2500536
    iget-object v0, p0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->r:LX/HmV;

    new-instance v1, LX/HmK;

    invoke-direct {v1, p0}, LX/HmK;-><init>(Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;)V

    .line 2500537
    iget-object v2, v0, LX/HmV;->f:LX/Hmc;

    .line 2500538
    sget-object p0, LX/Hmb;->VERIFY_APK:LX/Hmb;

    invoke-static {v2, p0}, LX/Hmc;->a(LX/Hmc;LX/Hmb;)V

    .line 2500539
    iget-object v2, v0, LX/HmV;->k:LX/HmU;

    sget-object p0, LX/HmU;->WAIT_VERIFY_APK:LX/HmU;

    if-ne v2, p0, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, LX/0Tp;->a(Z)V

    .line 2500540
    sget-object v2, LX/HmU;->VERIFY_APK:LX/HmU;

    iput-object v2, v0, LX/HmV;->k:LX/HmU;

    .line 2500541
    iget-object v2, v0, LX/HmV;->b:LX/0TD;

    new-instance p0, LX/HmS;

    invoke-direct {p0, v0}, LX/HmS;-><init>(LX/HmV;)V

    invoke-interface {v2, p0}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 2500542
    invoke-static {v0, v2, v1}, LX/HmV;->a(LX/HmV;Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2500543
    return-void

    .line 2500544
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static q(Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;)V
    .locals 8

    .prologue
    .line 2500518
    iget-object v0, p0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->s:LX/Hmf;

    .line 2500519
    sget-object v1, LX/Hmd;->INSTALL_SCREEN_OPENED:LX/Hmd;

    invoke-static {v0, v1}, LX/Hmf;->a(LX/Hmf;LX/Hmd;)V

    .line 2500520
    invoke-virtual {p0}, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "ssid"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, LX/HnN;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2500521
    iget-object v0, p0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->r:LX/HmV;

    .line 2500522
    iget-object v1, v0, LX/HmV;->o:Lcom/facebook/beam/protocol/BeamPreflightInfo;

    move-object v1, v1

    .line 2500523
    iget-object v0, p0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->r:LX/HmV;

    .line 2500524
    iget-object v2, v0, LX/HmV;->r:Ljava/io/File;

    move-object v0, v2

    .line 2500525
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 2500526
    iget-object v2, p0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->v:LX/HmM;

    .line 2500527
    iget-object v3, v1, Lcom/facebook/beam/protocol/BeamPreflightInfo;->mUserInfo:Lcom/facebook/beam/protocol/BeamUserInfo;

    move-object v1, v3

    .line 2500528
    iget-object v1, v1, Lcom/facebook/beam/protocol/BeamUserInfo;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-nez v3, :cond_0

    const/4 v0, 0x0

    :cond_0
    invoke-virtual {v2, v1, v0}, LX/HmM;->a(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2500529
    iget-object v0, p0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->v:LX/HmM;

    iget-object v0, v0, LX/HmM;->c:Landroid/widget/LinearLayout;

    const v1, 0x7f0d0573

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    .line 2500530
    new-instance v1, LX/Hm9;

    invoke-direct {v1, p0}, LX/Hm9;-><init>(Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2500531
    iget-object v0, p0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->r:LX/HmV;

    invoke-virtual {v0}, LX/HmV;->a()V

    .line 2500532
    return-void
.end method

.method public static r(Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;)V
    .locals 4

    .prologue
    .line 2500515
    iget-object v0, p0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->s:LX/Hmf;

    const v1, 0x7f083945

    invoke-virtual {p0, v1}, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Hmf;->e(Ljava/lang/String;)V

    .line 2500516
    iget-object v0, p0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->v:LX/HmM;

    const v1, 0x7f083951

    invoke-virtual {p0, v1}, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f083945

    invoke-virtual {p0, v2}, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, LX/HmM;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2500517
    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 2500502
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2500503
    invoke-static {p0, p0}, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2500504
    const v0, 0x7f030192

    invoke-virtual {p0, v0}, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->setContentView(I)V

    .line 2500505
    invoke-static {p0}, LX/HmZ;->a(Landroid/app/Activity;)LX/0h5;

    .line 2500506
    new-instance v0, LX/HmM;

    invoke-direct {v0, p0}, LX/HmM;-><init>(Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;)V

    iput-object v0, p0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->v:LX/HmM;

    .line 2500507
    iget-object v0, p0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->p:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0TA;->a(Ljava/util/concurrent/ExecutorService;)LX/0TD;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->w:LX/0TD;

    .line 2500508
    invoke-virtual {p0}, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2500509
    const-string v1, "ssid"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2500510
    const-string v2, "code"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2500511
    iget-object v2, p0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->v:LX/HmM;

    const v3, 0x7f083949

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-virtual {p0, v3, v4}, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/HmM;->a(Ljava/lang/String;)V

    .line 2500512
    iget-object v2, p0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->w:LX/0TD;

    new-instance v3, LX/HmB;

    invoke-direct {v3, p0, v1, v0}, LX/HmB;-><init>(Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v3}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2500513
    new-instance v2, LX/HmC;

    invoke-direct {v2, p0, v1}, LX/HmC;-><init>(Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->q:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v2, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2500514
    return-void
.end method

.method public final onBackPressed()V
    .locals 2

    .prologue
    .line 2500494
    iget-object v0, p0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->s:LX/Hmf;

    .line 2500495
    sget-object v1, LX/Hmd;->RECEIVER_FLOW_BACK_PRESSED:LX/Hmd;

    invoke-static {v0, v1}, LX/Hmf;->a(LX/Hmf;LX/Hmd;)V

    .line 2500496
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 2500497
    return-void
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x1b8cffbf

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2500498
    iget-object v1, p0, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->r:LX/HmV;

    invoke-virtual {v1}, LX/HmV;->h()V

    .line 2500499
    invoke-virtual {p0}, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "ssid"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, LX/HnN;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2500500
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 2500501
    const/16 v1, 0x23

    const v2, 0x24794d6c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
