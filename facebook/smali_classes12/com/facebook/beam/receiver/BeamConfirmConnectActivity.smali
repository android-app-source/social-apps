.class public Lcom/facebook/beam/receiver/BeamConfirmConnectActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:LX/Hmf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2500139
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/beam/receiver/BeamConfirmConnectActivity;

    invoke-static {v0}, LX/Hmf;->a(LX/0QB;)LX/Hmf;

    move-result-object v0

    check-cast v0, LX/Hmf;

    iput-object v0, p0, Lcom/facebook/beam/receiver/BeamConfirmConnectActivity;->p:LX/Hmf;

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2500140
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2500141
    invoke-static {p0, p0}, Lcom/facebook/beam/receiver/BeamConfirmConnectActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2500142
    const v0, 0x7f030190

    invoke-virtual {p0, v0}, Lcom/facebook/beam/receiver/BeamConfirmConnectActivity;->setContentView(I)V

    .line 2500143
    invoke-virtual {p0}, Lcom/facebook/beam/receiver/BeamConfirmConnectActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2500144
    const-string v1, "ssid"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2500145
    const-string v2, "passkey"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2500146
    invoke-static {p0}, LX/HmZ;->a(Landroid/app/Activity;)LX/0h5;

    .line 2500147
    const v0, 0x7f0d06c8

    invoke-virtual {p0, v0}, Lcom/facebook/beam/receiver/BeamConfirmConnectActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2500148
    const v3, 0x7f083940

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-virtual {p0, v3, v4}, Lcom/facebook/beam/receiver/BeamConfirmConnectActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2500149
    iget-object v0, p0, Lcom/facebook/beam/receiver/BeamConfirmConnectActivity;->p:LX/Hmf;

    .line 2500150
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v3

    const-string v4, "ssid"

    invoke-virtual {v3, v4, v1}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v3

    const-string v4, "passkey"

    invoke-virtual {v3, v4, v2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v3

    .line 2500151
    iget-object v4, v0, LX/Hmf;->a:LX/0if;

    sget-object v5, LX/0ig;->aX:LX/0ih;

    sget-object v6, LX/Hmd;->CONFIRM_SCREEN_OPENED:LX/Hmd;

    iget-object v6, v6, LX/Hmd;->actionName:Ljava/lang/String;

    const/4 p1, 0x0

    invoke-virtual {v4, v5, v6, p1, v3}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2500152
    const v0, 0x7f0d06c9

    invoke-virtual {p0, v0}, Lcom/facebook/beam/receiver/BeamConfirmConnectActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    .line 2500153
    new-instance v1, LX/Hm5;

    invoke-direct {v1, p0}, LX/Hm5;-><init>(Lcom/facebook/beam/receiver/BeamConfirmConnectActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2500154
    return-void
.end method
