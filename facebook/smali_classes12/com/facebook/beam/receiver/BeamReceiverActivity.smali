.class public Lcom/facebook/beam/receiver/BeamReceiverActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# static fields
.field public static final p:Ljava/lang/Long;


# instance fields
.field public q:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0W3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/Hmf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private u:LX/Hm0;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private v:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2500197
    const-wide/16 v0, 0xa

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/facebook/beam/receiver/BeamReceiverActivity;->p:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2500321
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2500322
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/beam/receiver/BeamReceiverActivity;->v:Z

    return-void
.end method

.method private static a(Ljava/lang/Integer;)Ljava/lang/Long;
    .locals 2

    .prologue
    .line 2500320
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/high16 v1, 0x100000

    mul-int/2addr v0, v1

    int-to-long v0, v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/Hm0;)V
    .locals 3

    .prologue
    .line 2500311
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/beam/receiver/BeamConfirmConnectActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2500312
    const-string v1, "ssid"

    .line 2500313
    iget-object v2, p1, LX/Hm0;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2500314
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2500315
    const-string v1, "passkey"

    .line 2500316
    iget-object v2, p1, LX/Hm0;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2500317
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2500318
    iget-object v1, p0, Lcom/facebook/beam/receiver/BeamReceiverActivity;->q:Lcom/facebook/content/SecureContextHelper;

    const/4 v2, 0x1

    invoke-interface {v1, v0, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2500319
    return-void
.end method

.method private static a(Lcom/facebook/beam/receiver/BeamReceiverActivity;Lcom/facebook/content/SecureContextHelper;LX/0SG;LX/0W3;LX/Hmf;)V
    .locals 0

    .prologue
    .line 2500310
    iput-object p1, p0, Lcom/facebook/beam/receiver/BeamReceiverActivity;->q:Lcom/facebook/content/SecureContextHelper;

    iput-object p2, p0, Lcom/facebook/beam/receiver/BeamReceiverActivity;->r:LX/0SG;

    iput-object p3, p0, Lcom/facebook/beam/receiver/BeamReceiverActivity;->s:LX/0W3;

    iput-object p4, p0, Lcom/facebook/beam/receiver/BeamReceiverActivity;->t:LX/Hmf;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/beam/receiver/BeamReceiverActivity;

    invoke-static {v3}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v3}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-static {v3}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v2

    check-cast v2, LX/0W3;

    invoke-static {v3}, LX/Hmf;->a(LX/0QB;)LX/Hmf;

    move-result-object v3

    check-cast v3, LX/Hmf;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/beam/receiver/BeamReceiverActivity;->a(Lcom/facebook/beam/receiver/BeamReceiverActivity;Lcom/facebook/content/SecureContextHelper;LX/0SG;LX/0W3;LX/Hmf;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/beam/receiver/BeamReceiverActivity;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2500286
    const v0, 0x7f0d06ce

    invoke-virtual {p0, v0}, Lcom/facebook/beam/receiver/BeamReceiverActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbEditText;

    .line 2500287
    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2500288
    iget-object v2, p0, Lcom/facebook/beam/receiver/BeamReceiverActivity;->t:LX/Hmf;

    .line 2500289
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v3

    const-string v4, "connectionCode"

    invoke-virtual {v3, v4, v0}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v3

    .line 2500290
    iget-object v4, v2, LX/Hmf;->a:LX/0if;

    sget-object v5, LX/0ig;->aX:LX/0ih;

    sget-object v6, LX/Hmd;->CONNECT_INPUT_SCREEN_CLICKED:LX/Hmd;

    iget-object v6, v6, LX/Hmd;->actionName:Ljava/lang/String;

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v6, v7, v3}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2500291
    const/4 v2, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2500292
    const-string v3, "-"

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 2500293
    const-string v4, "-"

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2500294
    array-length v4, v3

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    aget-object v4, v3, v6

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    aget-object v4, v3, v7

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2500295
    :cond_0
    :goto_0
    move-object v0, v2

    .line 2500296
    iput-object v0, p0, Lcom/facebook/beam/receiver/BeamReceiverActivity;->u:LX/Hm0;

    .line 2500297
    iget-object v2, p0, Lcom/facebook/beam/receiver/BeamReceiverActivity;->t:LX/Hmf;

    iget-object v0, p0, Lcom/facebook/beam/receiver/BeamReceiverActivity;->u:LX/Hm0;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 2500298
    :goto_1
    if-eqz v0, :cond_5

    sget-object v3, LX/Hmd;->CONNECT_INPUT_VALID:LX/Hmd;

    :goto_2
    invoke-static {v2, v3}, LX/Hmf;->a(LX/Hmf;LX/Hmd;)V

    .line 2500299
    const v0, 0x7f0d06cf

    invoke-virtual {p0, v0}, Lcom/facebook/beam/receiver/BeamReceiverActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2500300
    iget-object v2, p0, Lcom/facebook/beam/receiver/BeamReceiverActivity;->u:LX/Hm0;

    if-nez v2, :cond_2

    .line 2500301
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2500302
    :goto_3
    return-void

    :cond_1
    move v0, v1

    .line 2500303
    goto :goto_1

    .line 2500304
    :cond_2
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2500305
    iget-object v0, p0, Lcom/facebook/beam/receiver/BeamReceiverActivity;->u:LX/Hm0;

    invoke-direct {p0, v0}, Lcom/facebook/beam/receiver/BeamReceiverActivity;->b(LX/Hm0;)V

    goto :goto_3

    .line 2500306
    :cond_3
    new-instance v2, LX/Hm0;

    aget-object v4, v3, v6

    aget-object v3, v3, v7

    invoke-direct {v2, v4, v3}, LX/Hm0;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2500307
    :cond_4
    array-length v4, v3

    if-ne v4, v7, :cond_0

    aget-object v4, v3, v6

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 2500308
    new-instance v2, LX/Hm0;

    const-string v4, "fbap"

    aget-object v3, v3, v6

    invoke-direct {v2, v4, v3}, LX/Hm0;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2500309
    :cond_5
    sget-object v3, LX/Hmd;->CONNECT_INPUT_INVALID:LX/Hmd;

    goto :goto_2
.end method

.method private b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2500285
    invoke-virtual {p0}, Lcom/facebook/beam/receiver/BeamReceiverActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "ref"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b(LX/Hm0;)V
    .locals 3

    .prologue
    .line 2500276
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/beam/receiver/BeamReceiverTransferActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2500277
    const-string v1, "ssid"

    .line 2500278
    iget-object v2, p1, LX/Hm0;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2500279
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2500280
    const-string v1, "code"

    .line 2500281
    iget-object v2, p1, LX/Hm0;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2500282
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2500283
    iget-object v1, p0, Lcom/facebook/beam/receiver/BeamReceiverActivity;->q:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2500284
    return-void
.end method

.method private c(Landroid/content/Intent;)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2500260
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    if-nez v2, :cond_1

    .line 2500261
    :cond_0
    :goto_0
    return v0

    .line 2500262
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "ssid"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2500263
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "code"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2500264
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "timestamp"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 2500265
    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v6, 0x0

    cmp-long v2, v2, v6

    if-eqz v2, :cond_0

    .line 2500266
    iget-object v2, p0, Lcom/facebook/beam/receiver/BeamReceiverActivity;->s:LX/0W3;

    sget-wide v6, LX/0X5;->in:J

    invoke-interface {v2, v6, v7, v0}, LX/0W4;->a(JZ)Z

    move-result v2

    .line 2500267
    if-eqz v2, :cond_2

    move v0, v1

    .line 2500268
    goto :goto_0

    .line 2500269
    :cond_2
    iget-object v2, p0, Lcom/facebook/beam/receiver/BeamReceiverActivity;->r:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, LX/1lQ;->d(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 2500270
    iget-object v3, p0, Lcom/facebook/beam/receiver/BeamReceiverActivity;->s:LX/0W3;

    sget-wide v4, LX/0X5;->io:J

    invoke-interface {v3, v4, v5}, LX/0W4;->c(J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 2500271
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    neg-long v4, v4

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-gez v4, :cond_3

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v2, v4, v2

    if-gez v2, :cond_3

    move v0, v1

    .line 2500272
    :cond_3
    if-nez v0, :cond_0

    .line 2500273
    iget-object v1, p0, Lcom/facebook/beam/receiver/BeamReceiverActivity;->t:LX/Hmf;

    .line 2500274
    sget-object v2, LX/Hmd;->TIMESTAMP_CHECK_FAILED:LX/Hmd;

    invoke-static {v1, v2}, LX/Hmf;->a(LX/Hmf;LX/Hmd;)V

    .line 2500275
    goto :goto_0
.end method

.method private l()Z
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 2500257
    iget-object v1, p0, Lcom/facebook/beam/receiver/BeamReceiverActivity;->s:LX/0W3;

    sget-wide v2, LX/0X5;->ir:J

    invoke-interface {v1, v2, v3, v0}, LX/0W4;->a(JI)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 2500258
    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v8

    invoke-virtual {v8}, Ljava/io/File;->getFreeSpace()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    move-object v2, v8

    .line 2500259
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1}, Lcom/facebook/beam/receiver/BeamReceiverActivity;->a(Ljava/lang/Integer;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-gtz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private m()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2500247
    iget-object v1, p0, Lcom/facebook/beam/receiver/BeamReceiverActivity;->s:LX/0W3;

    sget-wide v2, LX/0X5;->im:J

    invoke-interface {v1, v2, v3, v0}, LX/0W4;->a(JZ)Z

    move-result v1

    .line 2500248
    if-eqz v1, :cond_0

    .line 2500249
    :goto_0
    return v0

    .line 2500250
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 2500251
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 2500252
    invoke-virtual {v1, v0}, Landroid/content/pm/PackageManager;->getInstallerPackageName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2500253
    if-nez v0, :cond_1

    .line 2500254
    const/4 v0, 0x0

    .line 2500255
    :goto_1
    move v0, v0

    .line 2500256
    goto :goto_0

    :cond_1
    sget-object v1, LX/HmZ;->a:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2500213
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2500214
    invoke-static {p0, p0}, Lcom/facebook/beam/receiver/BeamReceiverActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2500215
    iget-object v0, p0, Lcom/facebook/beam/receiver/BeamReceiverActivity;->s:LX/0W3;

    sget-wide v2, LX/0X5;->iv:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2500216
    invoke-virtual {p0}, Lcom/facebook/beam/receiver/BeamReceiverActivity;->finish()V

    .line 2500217
    :goto_0
    return-void

    .line 2500218
    :cond_0
    const v0, 0x7f030191

    invoke-virtual {p0, v0}, Lcom/facebook/beam/receiver/BeamReceiverActivity;->setContentView(I)V

    .line 2500219
    invoke-static {p0}, LX/HmZ;->a(Landroid/app/Activity;)LX/0h5;

    .line 2500220
    iget-object v0, p0, Lcom/facebook/beam/receiver/BeamReceiverActivity;->t:LX/Hmf;

    .line 2500221
    iget-object v2, v0, LX/Hmf;->a:LX/0if;

    sget-object v3, LX/0ig;->aX:LX/0ih;

    invoke-virtual {v2, v3}, LX/0if;->a(LX/0ih;)V

    .line 2500222
    invoke-direct {p0}, Lcom/facebook/beam/receiver/BeamReceiverActivity;->b()Ljava/lang/String;

    move-result-object v0

    .line 2500223
    if-eqz v0, :cond_1

    .line 2500224
    iget-object v0, p0, Lcom/facebook/beam/receiver/BeamReceiverActivity;->t:LX/Hmf;

    .line 2500225
    iget-object v2, v0, LX/Hmf;->a:LX/0if;

    sget-object v3, LX/0ig;->aX:LX/0ih;

    sget-object p1, LX/Hme;->REF_BOOKMARK:LX/Hme;

    iget-object p1, p1, LX/Hme;->tag:Ljava/lang/String;

    invoke-virtual {v2, v3, p1}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 2500226
    :cond_1
    invoke-direct {p0}, Lcom/facebook/beam/receiver/BeamReceiverActivity;->m()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2500227
    iget-object v0, p0, Lcom/facebook/beam/receiver/BeamReceiverActivity;->t:LX/Hmf;

    .line 2500228
    sget-object v2, LX/Hmd;->GOOGLE_PLAY_ERROR_OPENED:LX/Hmd;

    invoke-static {v0, v2}, LX/Hmf;->a(LX/Hmf;LX/Hmd;)V

    .line 2500229
    const v0, 0x7f0d06cb

    invoke-virtual {p0, v0}, Lcom/facebook/beam/receiver/BeamReceiverActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 2500230
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 2500231
    :cond_2
    invoke-direct {p0}, Lcom/facebook/beam/receiver/BeamReceiverActivity;->l()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2500232
    iget-object v0, p0, Lcom/facebook/beam/receiver/BeamReceiverActivity;->t:LX/Hmf;

    .line 2500233
    sget-object v2, LX/Hmd;->INSUFFICIENT_STORAGE_OPENED:LX/Hmd;

    invoke-static {v0, v2}, LX/Hmf;->a(LX/Hmf;LX/Hmd;)V

    .line 2500234
    const v0, 0x7f0d06cc

    invoke-virtual {p0, v0}, Lcom/facebook/beam/receiver/BeamReceiverActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 2500235
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 2500236
    :cond_3
    const v0, 0x7f0d06cd

    invoke-virtual {p0, v0}, Lcom/facebook/beam/receiver/BeamReceiverActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 2500237
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2500238
    const v0, 0x7f0d06d0

    invoke-virtual {p0, v0}, Lcom/facebook/beam/receiver/BeamReceiverActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    .line 2500239
    new-instance v1, LX/Hm8;

    invoke-direct {v1, p0}, LX/Hm8;-><init>(Lcom/facebook/beam/receiver/BeamReceiverActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2500240
    invoke-virtual {p0}, Lcom/facebook/beam/receiver/BeamReceiverActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2500241
    invoke-direct {p0, v0}, Lcom/facebook/beam/receiver/BeamReceiverActivity;->c(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2500242
    new-instance v1, LX/Hm0;

    const-string v2, "ssid"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "code"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, LX/Hm0;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/facebook/beam/receiver/BeamReceiverActivity;->u:LX/Hm0;

    .line 2500243
    const v0, 0x7f0d06ce

    invoke-virtual {p0, v0}, Lcom/facebook/beam/receiver/BeamReceiverActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbEditText;

    .line 2500244
    iget-object v1, p0, Lcom/facebook/beam/receiver/BeamReceiverActivity;->u:LX/Hm0;

    invoke-virtual {v1}, LX/Hm0;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2500245
    iget-object v0, p0, Lcom/facebook/beam/receiver/BeamReceiverActivity;->u:LX/Hm0;

    invoke-direct {p0, v0}, Lcom/facebook/beam/receiver/BeamReceiverActivity;->a(LX/Hm0;)V

    goto/16 :goto_0

    .line 2500246
    :cond_4
    iget-object v0, p0, Lcom/facebook/beam/receiver/BeamReceiverActivity;->t:LX/Hmf;

    const-string v1, ""

    invoke-virtual {v0, v1}, LX/Hmf;->a(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2500208
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/activity/FbFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 2500209
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 2500210
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 2500211
    iget-object v0, p0, Lcom/facebook/beam/receiver/BeamReceiverActivity;->u:LX/Hm0;

    invoke-direct {p0, v0}, Lcom/facebook/beam/receiver/BeamReceiverActivity;->b(LX/Hm0;)V

    .line 2500212
    :cond_0
    return-void
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x1e19c611

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2500205
    iget-object v1, p0, Lcom/facebook/beam/receiver/BeamReceiverActivity;->t:LX/Hmf;

    invoke-virtual {v1}, LX/Hmf;->b()V

    .line 2500206
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 2500207
    const/16 v1, 0x23

    const v2, -0x7da14d0c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x22

    const v2, -0x607fb719

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2500198
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 2500199
    iget-boolean v0, p0, Lcom/facebook/beam/receiver/BeamReceiverActivity;->v:Z

    if-nez v0, :cond_0

    .line 2500200
    const v0, 0x7f0d06ce

    invoke-virtual {p0, v0}, Lcom/facebook/beam/receiver/BeamReceiverActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbEditText;

    .line 2500201
    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2500202
    iget-object v2, p0, Lcom/facebook/beam/receiver/BeamReceiverActivity;->t:LX/Hmf;

    invoke-virtual {v2, v0}, LX/Hmf;->a(Ljava/lang/String;)V

    .line 2500203
    :goto_0
    const v0, -0x4b11fa17

    invoke-static {v0, v1}, LX/02F;->c(II)V

    return-void

    .line 2500204
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/beam/receiver/BeamReceiverActivity;->v:Z

    goto :goto_0
.end method
