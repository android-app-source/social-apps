.class public Lcom/facebook/beam/protocol/BeamPreflightInfoSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/beam/protocol/BeamPreflightInfo;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2499999
    const-class v0, Lcom/facebook/beam/protocol/BeamPreflightInfo;

    new-instance v1, Lcom/facebook/beam/protocol/BeamPreflightInfoSerializer;

    invoke-direct {v1}, Lcom/facebook/beam/protocol/BeamPreflightInfoSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2500000
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2500001
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/beam/protocol/BeamPreflightInfo;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2500002
    if-nez p0, :cond_0

    .line 2500003
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 2500004
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2500005
    invoke-static {p0, p1, p2}, Lcom/facebook/beam/protocol/BeamPreflightInfoSerializer;->b(Lcom/facebook/beam/protocol/BeamPreflightInfo;LX/0nX;LX/0my;)V

    .line 2500006
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2500007
    return-void
.end method

.method private static b(Lcom/facebook/beam/protocol/BeamPreflightInfo;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2500008
    const-string v0, "packageInfo"

    iget-object v1, p0, Lcom/facebook/beam/protocol/BeamPreflightInfo;->mPackageInfo:Lcom/facebook/beam/protocol/BeamPackageInfo;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2500009
    const-string v0, "userInfo"

    iget-object v1, p0, Lcom/facebook/beam/protocol/BeamPreflightInfo;->mUserInfo:Lcom/facebook/beam/protocol/BeamUserInfo;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2500010
    const-string v0, "deviceInfo"

    iget-object v1, p0, Lcom/facebook/beam/protocol/BeamPreflightInfo;->mDeviceInfo:Lcom/facebook/beam/protocol/BeamDeviceInfo;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2500011
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2500012
    check-cast p1, Lcom/facebook/beam/protocol/BeamPreflightInfo;

    invoke-static {p1, p2, p3}, Lcom/facebook/beam/protocol/BeamPreflightInfoSerializer;->a(Lcom/facebook/beam/protocol/BeamPreflightInfo;LX/0nX;LX/0my;)V

    return-void
.end method
