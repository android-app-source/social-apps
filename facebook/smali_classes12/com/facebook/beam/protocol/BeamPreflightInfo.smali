.class public Lcom/facebook/beam/protocol/BeamPreflightInfo;
.super LX/Hm1;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation


# instance fields
.field public final mDeviceInfo:Lcom/facebook/beam/protocol/BeamDeviceInfo;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "deviceInfo"
    .end annotation
.end field

.field public final mPackageInfo:Lcom/facebook/beam/protocol/BeamPackageInfo;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "packageInfo"
    .end annotation
.end field

.field public final mUserInfo:Lcom/facebook/beam/protocol/BeamUserInfo;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "userInfo"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2499964
    const-class v0, Lcom/facebook/beam/protocol/BeamPreflightInfoDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2499965
    const-class v0, Lcom/facebook/beam/protocol/BeamPreflightInfoSerializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2499966
    invoke-direct {p0}, LX/Hm1;-><init>()V

    .line 2499967
    iput-object v0, p0, Lcom/facebook/beam/protocol/BeamPreflightInfo;->mPackageInfo:Lcom/facebook/beam/protocol/BeamPackageInfo;

    .line 2499968
    iput-object v0, p0, Lcom/facebook/beam/protocol/BeamPreflightInfo;->mUserInfo:Lcom/facebook/beam/protocol/BeamUserInfo;

    .line 2499969
    iput-object v0, p0, Lcom/facebook/beam/protocol/BeamPreflightInfo;->mDeviceInfo:Lcom/facebook/beam/protocol/BeamDeviceInfo;

    .line 2499970
    return-void
.end method

.method public constructor <init>(Lcom/facebook/beam/protocol/BeamPackageInfo;Lcom/facebook/beam/protocol/BeamUserInfo;Lcom/facebook/beam/protocol/BeamDeviceInfo;)V
    .locals 0

    .prologue
    .line 2499971
    invoke-direct {p0}, LX/Hm1;-><init>()V

    .line 2499972
    iput-object p1, p0, Lcom/facebook/beam/protocol/BeamPreflightInfo;->mPackageInfo:Lcom/facebook/beam/protocol/BeamPackageInfo;

    .line 2499973
    iput-object p2, p0, Lcom/facebook/beam/protocol/BeamPreflightInfo;->mUserInfo:Lcom/facebook/beam/protocol/BeamUserInfo;

    .line 2499974
    iput-object p3, p0, Lcom/facebook/beam/protocol/BeamPreflightInfo;->mDeviceInfo:Lcom/facebook/beam/protocol/BeamDeviceInfo;

    .line 2499975
    return-void
.end method
