.class public Lcom/facebook/beam/protocol/BeamPackageInfoSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/beam/protocol/BeamPackageInfo;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2499962
    const-class v0, Lcom/facebook/beam/protocol/BeamPackageInfo;

    new-instance v1, Lcom/facebook/beam/protocol/BeamPackageInfoSerializer;

    invoke-direct {v1}, Lcom/facebook/beam/protocol/BeamPackageInfoSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2499963
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2499944
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/beam/protocol/BeamPackageInfo;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2499956
    if-nez p0, :cond_0

    .line 2499957
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 2499958
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2499959
    invoke-static {p0, p1, p2}, Lcom/facebook/beam/protocol/BeamPackageInfoSerializer;->b(Lcom/facebook/beam/protocol/BeamPackageInfo;LX/0nX;LX/0my;)V

    .line 2499960
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2499961
    return-void
.end method

.method private static b(Lcom/facebook/beam/protocol/BeamPackageInfo;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2499946
    const-string v0, "packageName"

    iget-object v1, p0, Lcom/facebook/beam/protocol/BeamPackageInfo;->mPackageName:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2499947
    const-string v0, "versionName"

    iget-object v1, p0, Lcom/facebook/beam/protocol/BeamPackageInfo;->mVersionName:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2499948
    const-string v0, "versionCode"

    iget v1, p0, Lcom/facebook/beam/protocol/BeamPackageInfo;->mVersionCode:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2499949
    const-string v0, "installerPackageName"

    iget-object v1, p0, Lcom/facebook/beam/protocol/BeamPackageInfo;->mInstallerPackageName:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2499950
    const-string v0, "apkSize"

    iget-object v1, p0, Lcom/facebook/beam/protocol/BeamPackageInfo;->mApkSize:Ljava/lang/Long;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 2499951
    const-string v0, "targetSdkVersion"

    iget-object v1, p0, Lcom/facebook/beam/protocol/BeamPackageInfo;->mTargetSdkVersion:Ljava/lang/Integer;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2499952
    const-string v0, "compatibleWidthLimitDp"

    iget-object v1, p0, Lcom/facebook/beam/protocol/BeamPackageInfo;->mCompatibleWidthLimitDp:Ljava/lang/Integer;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2499953
    const-string v0, "requiresSmallestWidthDp"

    iget-object v1, p0, Lcom/facebook/beam/protocol/BeamPackageInfo;->mRequiresSmallestWidthDp:Ljava/lang/Integer;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2499954
    const-string v0, "largestWidthLimitDp"

    iget-object v1, p0, Lcom/facebook/beam/protocol/BeamPackageInfo;->mLargestWidthLimitDp:Ljava/lang/Integer;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2499955
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2499945
    check-cast p1, Lcom/facebook/beam/protocol/BeamPackageInfo;

    invoke-static {p1, p2, p3}, Lcom/facebook/beam/protocol/BeamPackageInfoSerializer;->a(Lcom/facebook/beam/protocol/BeamPackageInfo;LX/0nX;LX/0my;)V

    return-void
.end method
