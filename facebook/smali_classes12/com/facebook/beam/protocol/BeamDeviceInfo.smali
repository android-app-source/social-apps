.class public Lcom/facebook/beam/protocol/BeamDeviceInfo;
.super LX/Hm1;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation


# instance fields
.field public final mAvailableSpace:Ljava/lang/Long;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "availableSpace"
    .end annotation
.end field

.field public final mCPUAbis:Ljava/util/List;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "cpuAbis"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final mDensity:Ljava/lang/Float;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "density"
    .end annotation
.end field

.field public final mDeviceBrand:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "deviceBrand"
    .end annotation
.end field

.field public final mDeviceModel:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "deviceModel"
    .end annotation
.end field

.field public final mMaxCPUFreqKHz:Ljava/lang/Integer;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "maxCPUFreqKHz"
    .end annotation
.end field

.field public final mNumCPUCores:Ljava/lang/Integer;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "numCPUCores"
    .end annotation
.end field

.field public final mSDKVersion:Ljava/lang/Integer;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "sdkVersion"
    .end annotation
.end field

.field public final mTotalMemory:Ljava/lang/Long;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "totalMemory"
    .end annotation
.end field

.field public final mUnknownSourcesChecked:Ljava/lang/Boolean;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "unknownSourcesChecked"
    .end annotation
.end field

.field public final mYearClass:Ljava/lang/Integer;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "yearClass"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2499809
    const-class v0, Lcom/facebook/beam/protocol/BeamDeviceInfoDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2499808
    const-class v0, Lcom/facebook/beam/protocol/BeamDeviceInfoSerializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2499782
    invoke-direct {p0}, LX/Hm1;-><init>()V

    .line 2499783
    iput-object v0, p0, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mDeviceBrand:Ljava/lang/String;

    .line 2499784
    iput-object v0, p0, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mDeviceModel:Ljava/lang/String;

    .line 2499785
    iput-object v0, p0, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mYearClass:Ljava/lang/Integer;

    .line 2499786
    iput-object v0, p0, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mTotalMemory:Ljava/lang/Long;

    .line 2499787
    iput-object v0, p0, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mAvailableSpace:Ljava/lang/Long;

    .line 2499788
    iput-object v0, p0, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mNumCPUCores:Ljava/lang/Integer;

    .line 2499789
    iput-object v0, p0, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mMaxCPUFreqKHz:Ljava/lang/Integer;

    .line 2499790
    iput-object v0, p0, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mCPUAbis:Ljava/util/List;

    .line 2499791
    iput-object v0, p0, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mSDKVersion:Ljava/lang/Integer;

    .line 2499792
    iput-object v0, p0, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mDensity:Ljava/lang/Float;

    .line 2499793
    iput-object v0, p0, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mUnknownSourcesChecked:Ljava/lang/Boolean;

    .line 2499794
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/Float;Ljava/lang/Boolean;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Float;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2499795
    invoke-direct {p0}, LX/Hm1;-><init>()V

    .line 2499796
    iput-object p1, p0, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mDeviceBrand:Ljava/lang/String;

    .line 2499797
    iput-object p2, p0, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mDeviceModel:Ljava/lang/String;

    .line 2499798
    iput-object p3, p0, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mYearClass:Ljava/lang/Integer;

    .line 2499799
    iput-object p4, p0, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mTotalMemory:Ljava/lang/Long;

    .line 2499800
    iput-object p5, p0, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mAvailableSpace:Ljava/lang/Long;

    .line 2499801
    iput-object p6, p0, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mNumCPUCores:Ljava/lang/Integer;

    .line 2499802
    iput-object p7, p0, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mMaxCPUFreqKHz:Ljava/lang/Integer;

    .line 2499803
    iput-object p8, p0, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mCPUAbis:Ljava/util/List;

    .line 2499804
    iput-object p9, p0, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mSDKVersion:Ljava/lang/Integer;

    .line 2499805
    iput-object p10, p0, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mDensity:Ljava/lang/Float;

    .line 2499806
    iput-object p11, p0, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mUnknownSourcesChecked:Ljava/lang/Boolean;

    .line 2499807
    return-void
.end method
