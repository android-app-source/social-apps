.class public Lcom/facebook/beam/protocol/BeamPackageInfo;
.super LX/Hm1;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation


# instance fields
.field public final mApkSize:Ljava/lang/Long;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "apkSize"
    .end annotation
.end field

.field public final mCompatibleWidthLimitDp:Ljava/lang/Integer;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "compatibleWidthLimitDp"
    .end annotation
.end field

.field public final mInstallerPackageName:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "installerPackageName"
    .end annotation
.end field

.field public final mLargestWidthLimitDp:Ljava/lang/Integer;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "largestWidthLimitDp"
    .end annotation
.end field

.field public final mPackageName:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "packageName"
    .end annotation
.end field

.field public final mRequiresSmallestWidthDp:Ljava/lang/Integer;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "requiresSmallestWidthDp"
    .end annotation
.end field

.field public final mTargetSdkVersion:Ljava/lang/Integer;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "targetSdkVersion"
    .end annotation
.end field

.field public final mVersionCode:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "versionCode"
    .end annotation
.end field

.field public final mVersionName:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "versionName"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2499891
    const-class v0, Lcom/facebook/beam/protocol/BeamPackageInfoDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2499892
    const-class v0, Lcom/facebook/beam/protocol/BeamPackageInfoSerializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2499893
    invoke-direct {p0}, LX/Hm1;-><init>()V

    .line 2499894
    iput-object v1, p0, Lcom/facebook/beam/protocol/BeamPackageInfo;->mPackageName:Ljava/lang/String;

    .line 2499895
    iput-object v1, p0, Lcom/facebook/beam/protocol/BeamPackageInfo;->mVersionName:Ljava/lang/String;

    .line 2499896
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/beam/protocol/BeamPackageInfo;->mVersionCode:I

    .line 2499897
    iput-object v1, p0, Lcom/facebook/beam/protocol/BeamPackageInfo;->mInstallerPackageName:Ljava/lang/String;

    .line 2499898
    iput-object v1, p0, Lcom/facebook/beam/protocol/BeamPackageInfo;->mApkSize:Ljava/lang/Long;

    .line 2499899
    iput-object v1, p0, Lcom/facebook/beam/protocol/BeamPackageInfo;->mTargetSdkVersion:Ljava/lang/Integer;

    .line 2499900
    iput-object v1, p0, Lcom/facebook/beam/protocol/BeamPackageInfo;->mCompatibleWidthLimitDp:Ljava/lang/Integer;

    .line 2499901
    iput-object v1, p0, Lcom/facebook/beam/protocol/BeamPackageInfo;->mRequiresSmallestWidthDp:Ljava/lang/Integer;

    .line 2499902
    iput-object v1, p0, Lcom/facebook/beam/protocol/BeamPackageInfo;->mLargestWidthLimitDp:Ljava/lang/Integer;

    .line 2499903
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/Long;IIII)V
    .locals 1

    .prologue
    .line 2499904
    invoke-direct {p0}, LX/Hm1;-><init>()V

    .line 2499905
    iput-object p1, p0, Lcom/facebook/beam/protocol/BeamPackageInfo;->mPackageName:Ljava/lang/String;

    .line 2499906
    iput-object p2, p0, Lcom/facebook/beam/protocol/BeamPackageInfo;->mVersionName:Ljava/lang/String;

    .line 2499907
    iput p3, p0, Lcom/facebook/beam/protocol/BeamPackageInfo;->mVersionCode:I

    .line 2499908
    iput-object p4, p0, Lcom/facebook/beam/protocol/BeamPackageInfo;->mInstallerPackageName:Ljava/lang/String;

    .line 2499909
    iput-object p5, p0, Lcom/facebook/beam/protocol/BeamPackageInfo;->mApkSize:Ljava/lang/Long;

    .line 2499910
    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/beam/protocol/BeamPackageInfo;->mTargetSdkVersion:Ljava/lang/Integer;

    .line 2499911
    invoke-static {p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/beam/protocol/BeamPackageInfo;->mCompatibleWidthLimitDp:Ljava/lang/Integer;

    .line 2499912
    invoke-static {p8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/beam/protocol/BeamPackageInfo;->mRequiresSmallestWidthDp:Ljava/lang/Integer;

    .line 2499913
    invoke-static {p9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/beam/protocol/BeamPackageInfo;->mLargestWidthLimitDp:Ljava/lang/Integer;

    .line 2499914
    return-void
.end method
