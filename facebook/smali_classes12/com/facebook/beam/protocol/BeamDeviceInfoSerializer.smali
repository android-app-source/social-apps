.class public Lcom/facebook/beam/protocol/BeamDeviceInfoSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/beam/protocol/BeamDeviceInfo;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2499841
    const-class v0, Lcom/facebook/beam/protocol/BeamDeviceInfo;

    new-instance v1, Lcom/facebook/beam/protocol/BeamDeviceInfoSerializer;

    invoke-direct {v1}, Lcom/facebook/beam/protocol/BeamDeviceInfoSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2499842
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2499843
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/beam/protocol/BeamDeviceInfo;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2499844
    if-nez p0, :cond_0

    .line 2499845
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 2499846
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2499847
    invoke-static {p0, p1, p2}, Lcom/facebook/beam/protocol/BeamDeviceInfoSerializer;->b(Lcom/facebook/beam/protocol/BeamDeviceInfo;LX/0nX;LX/0my;)V

    .line 2499848
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2499849
    return-void
.end method

.method private static b(Lcom/facebook/beam/protocol/BeamDeviceInfo;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2499850
    const-string v0, "deviceBrand"

    iget-object v1, p0, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mDeviceBrand:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2499851
    const-string v0, "deviceModel"

    iget-object v1, p0, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mDeviceModel:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2499852
    const-string v0, "yearClass"

    iget-object v1, p0, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mYearClass:Ljava/lang/Integer;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2499853
    const-string v0, "totalMemory"

    iget-object v1, p0, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mTotalMemory:Ljava/lang/Long;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 2499854
    const-string v0, "availableSpace"

    iget-object v1, p0, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mAvailableSpace:Ljava/lang/Long;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 2499855
    const-string v0, "numCPUCores"

    iget-object v1, p0, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mNumCPUCores:Ljava/lang/Integer;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2499856
    const-string v0, "maxCPUFreqKHz"

    iget-object v1, p0, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mMaxCPUFreqKHz:Ljava/lang/Integer;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2499857
    const-string v0, "cpuAbis"

    iget-object v1, p0, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mCPUAbis:Ljava/util/List;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 2499858
    const-string v0, "sdkVersion"

    iget-object v1, p0, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mSDKVersion:Ljava/lang/Integer;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2499859
    const-string v0, "density"

    iget-object v1, p0, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mDensity:Ljava/lang/Float;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Float;)V

    .line 2499860
    const-string v0, "unknownSourcesChecked"

    iget-object v1, p0, Lcom/facebook/beam/protocol/BeamDeviceInfo;->mUnknownSourcesChecked:Ljava/lang/Boolean;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2499861
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2499862
    check-cast p1, Lcom/facebook/beam/protocol/BeamDeviceInfo;

    invoke-static {p1, p2, p3}, Lcom/facebook/beam/protocol/BeamDeviceInfoSerializer;->a(Lcom/facebook/beam/protocol/BeamDeviceInfo;LX/0nX;LX/0my;)V

    return-void
.end method
