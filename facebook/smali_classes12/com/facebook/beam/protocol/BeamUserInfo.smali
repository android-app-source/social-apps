.class public Lcom/facebook/beam/protocol/BeamUserInfo;
.super LX/Hm1;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation


# instance fields
.field public final mDisplayName:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "displayName"
    .end annotation
.end field

.field public final mUserId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "userid"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2500088
    const-class v0, Lcom/facebook/beam/protocol/BeamUserInfoDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2500089
    const-class v0, Lcom/facebook/beam/protocol/BeamUserInfoSerializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2500090
    invoke-direct {p0}, LX/Hm1;-><init>()V

    .line 2500091
    iput-object v0, p0, Lcom/facebook/beam/protocol/BeamUserInfo;->mUserId:Ljava/lang/String;

    .line 2500092
    iput-object v0, p0, Lcom/facebook/beam/protocol/BeamUserInfo;->mDisplayName:Ljava/lang/String;

    .line 2500093
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2500094
    invoke-direct {p0}, LX/Hm1;-><init>()V

    .line 2500095
    iput-object p1, p0, Lcom/facebook/beam/protocol/BeamUserInfo;->mUserId:Ljava/lang/String;

    .line 2500096
    iput-object p2, p0, Lcom/facebook/beam/protocol/BeamUserInfo;->mDisplayName:Ljava/lang/String;

    .line 2500097
    return-void
.end method
