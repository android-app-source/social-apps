.class public Lcom/facebook/beam/protocol/BeamUserInfoSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/beam/protocol/BeamUserInfo;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2500120
    const-class v0, Lcom/facebook/beam/protocol/BeamUserInfo;

    new-instance v1, Lcom/facebook/beam/protocol/BeamUserInfoSerializer;

    invoke-direct {v1}, Lcom/facebook/beam/protocol/BeamUserInfoSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2500121
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2500132
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/beam/protocol/BeamUserInfo;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2500126
    if-nez p0, :cond_0

    .line 2500127
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 2500128
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2500129
    invoke-static {p0, p1, p2}, Lcom/facebook/beam/protocol/BeamUserInfoSerializer;->b(Lcom/facebook/beam/protocol/BeamUserInfo;LX/0nX;LX/0my;)V

    .line 2500130
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2500131
    return-void
.end method

.method private static b(Lcom/facebook/beam/protocol/BeamUserInfo;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2500123
    const-string v0, "userid"

    iget-object v1, p0, Lcom/facebook/beam/protocol/BeamUserInfo;->mUserId:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2500124
    const-string v0, "displayName"

    iget-object v1, p0, Lcom/facebook/beam/protocol/BeamUserInfo;->mDisplayName:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2500125
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2500122
    check-cast p1, Lcom/facebook/beam/protocol/BeamUserInfo;

    invoke-static {p1, p2, p3}, Lcom/facebook/beam/protocol/BeamUserInfoSerializer;->a(Lcom/facebook/beam/protocol/BeamUserInfo;LX/0nX;LX/0my;)V

    return-void
.end method
