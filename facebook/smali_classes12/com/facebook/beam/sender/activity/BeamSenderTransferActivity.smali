.class public Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# static fields
.field public static final w:Ljava/lang/Long;


# instance fields
.field public A:LX/Hmp;

.field public B:LX/HnH;

.field public C:Lcom/facebook/beam/protocol/BeamPreflightInfo;

.field public D:Lcom/facebook/beam/protocol/BeamPreflightInfo;

.field private final E:LX/44p;

.field private final F:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final G:LX/0Vj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Vj",
            "<",
            "Ljava/lang/Boolean;",
            "Ljava/net/Socket;",
            ">;"
        }
    .end annotation
.end field

.field private final H:LX/0Vj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Vj",
            "<",
            "Ljava/net/Socket;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final I:LX/0Vj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Vj",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/facebook/beam/protocol/BeamPreflightInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final J:LX/0Vj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Vj",
            "<",
            "Lcom/facebook/beam/protocol/BeamPreflightInfo;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final K:LX/0Vj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Vj",
            "<",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final L:LX/0Vj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Vj",
            "<",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public p:LX/Hn0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/Hmo;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/Hmi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/HnK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/Hm2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/Hmj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/Hm4;

.field public y:LX/Hmz;

.field public z:LX/Hm0;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2501651
    const-wide/32 v0, 0x100000

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->w:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2501641
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2501642
    invoke-static {}, LX/44p;->b()LX/44p;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->E:LX/44p;

    .line 2501643
    new-instance v0, LX/Hn8;

    invoke-direct {v0, p0}, LX/Hn8;-><init>(Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;)V

    iput-object v0, p0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->F:LX/0TF;

    .line 2501644
    new-instance v0, LX/Hn9;

    invoke-direct {v0, p0}, LX/Hn9;-><init>(Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;)V

    iput-object v0, p0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->G:LX/0Vj;

    .line 2501645
    new-instance v0, LX/HnA;

    invoke-direct {v0, p0}, LX/HnA;-><init>(Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;)V

    iput-object v0, p0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->H:LX/0Vj;

    .line 2501646
    new-instance v0, LX/HnB;

    invoke-direct {v0, p0}, LX/HnB;-><init>(Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;)V

    iput-object v0, p0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->I:LX/0Vj;

    .line 2501647
    new-instance v0, LX/HnC;

    invoke-direct {v0, p0}, LX/HnC;-><init>(Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;)V

    iput-object v0, p0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->J:LX/0Vj;

    .line 2501648
    new-instance v0, LX/HnD;

    invoke-direct {v0, p0}, LX/HnD;-><init>(Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;)V

    iput-object v0, p0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->K:LX/0Vj;

    .line 2501649
    new-instance v0, LX/HnE;

    invoke-direct {v0, p0}, LX/HnE;-><init>(Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;)V

    iput-object v0, p0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->L:LX/0Vj;

    .line 2501650
    return-void
.end method

.method private a(LX/0Px;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/Hmh;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 2501626
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 2501627
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Hmh;

    .line 2501628
    const-string v1, "- "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2501629
    const/4 v1, 0x0

    .line 2501630
    sget-object v5, LX/HnF;->a:[I

    invoke-virtual {v0}, LX/Hmh;->ordinal()I

    move-result v0

    aget v0, v5, v0

    packed-switch v0, :pswitch_data_0

    move-object v0, v1

    .line 2501631
    :goto_1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2501632
    const-string v0, "\n"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2501633
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2501634
    :pswitch_0
    const v0, 0x7f08396a

    invoke-virtual {p0, v0}, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2501635
    :pswitch_1
    const v0, 0x7f08396b

    invoke-virtual {p0, v0}, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2501636
    :pswitch_2
    const v0, 0x7f08396c

    invoke-virtual {p0, v0}, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2501637
    :pswitch_3
    const v0, 0x7f08396d

    invoke-virtual {p0, v0}, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2501638
    :pswitch_4
    const v0, 0x7f08396e

    invoke-virtual {p0, v0}, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2501639
    :pswitch_5
    const v0, 0x7f08396f

    invoke-virtual {p0, v0}, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2501640
    :cond_0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static synthetic a(Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;LX/0Px;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2501625
    invoke-direct {p0, p1}, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->a(LX/0Px;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;LX/Hn0;LX/Hmo;LX/Hmi;LX/HnK;LX/Hm2;LX/03V;LX/Hmj;)V
    .locals 0

    .prologue
    .line 2501624
    iput-object p1, p0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->p:LX/Hn0;

    iput-object p2, p0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->q:LX/Hmo;

    iput-object p3, p0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->r:LX/Hmi;

    iput-object p4, p0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->s:LX/HnK;

    iput-object p5, p0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->t:LX/Hm2;

    iput-object p6, p0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->u:LX/03V;

    iput-object p7, p0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->v:LX/Hmj;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 8

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v7

    move-object v0, p0

    check-cast v0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    const-class v1, LX/Hn0;

    invoke-interface {v7, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/Hn0;

    new-instance v5, LX/Hmo;

    const-class v2, Landroid/content/Context;

    invoke-interface {v7, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {v7}, LX/1Ml;->b(LX/0QB;)LX/1Ml;

    move-result-object v3

    check-cast v3, LX/1Ml;

    invoke-static {v7}, LX/0UA;->b(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-direct {v5, v2, v3, v4}, LX/Hmo;-><init>(Landroid/content/Context;LX/1Ml;Ljava/util/concurrent/ExecutorService;)V

    move-object v2, v5

    check-cast v2, LX/Hmo;

    new-instance v5, LX/Hmi;

    invoke-static {v7}, LX/0WF;->a(LX/0QB;)Landroid/content/pm/PackageManager;

    move-result-object v3

    check-cast v3, Landroid/content/pm/PackageManager;

    invoke-static {v7}, LX/Hmj;->b(LX/0QB;)LX/Hmj;

    move-result-object v4

    check-cast v4, LX/Hmj;

    invoke-direct {v5, v3, v4}, LX/Hmi;-><init>(Landroid/content/pm/PackageManager;LX/Hmj;)V

    move-object v3, v5

    check-cast v3, LX/Hmi;

    invoke-static {v7}, LX/HnK;->a(LX/0QB;)LX/HnK;

    move-result-object v4

    check-cast v4, LX/HnK;

    invoke-static {v7}, LX/Hm2;->b(LX/0QB;)LX/Hm2;

    move-result-object v5

    check-cast v5, LX/Hm2;

    invoke-static {v7}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static {v7}, LX/Hmj;->b(LX/0QB;)LX/Hmj;

    move-result-object v7

    check-cast v7, LX/Hmj;

    invoke-static/range {v0 .. v7}, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->a(Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;LX/Hn0;LX/Hmo;LX/Hmi;LX/HnK;LX/Hm2;LX/03V;LX/Hmj;)V

    return-void
.end method

.method private b()V
    .locals 4

    .prologue
    .line 2501584
    iget-object v0, p0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->B:LX/HnH;

    invoke-virtual {v0}, LX/HnH;->a()V

    .line 2501585
    iget-object v0, p0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->z:LX/Hm0;

    invoke-virtual {v0}, LX/Hm0;->c()Ljava/lang/String;

    .line 2501586
    iget-object v0, p0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->q:LX/Hmo;

    iget-object v1, p0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->z:LX/Hm0;

    .line 2501587
    iget-object v2, v0, LX/Hmo;->a:LX/0TD;

    new-instance v3, LX/Hmk;

    invoke-direct {v3, v0, v1}, LX/Hmk;-><init>(LX/Hmo;LX/Hm0;)V

    invoke-interface {v2, v3}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v0, v2

    .line 2501588
    iget-object v1, p0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->G:LX/0Vj;

    iget-object v2, p0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->E:LX/44p;

    invoke-static {v0, v1, v2}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2501589
    iget-object v1, p0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->H:LX/0Vj;

    iget-object v2, p0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->E:LX/44p;

    invoke-static {v0, v1, v2}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2501590
    iget-object v1, p0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->I:LX/0Vj;

    iget-object v2, p0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->E:LX/44p;

    invoke-static {v0, v1, v2}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2501591
    iget-object v1, p0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->J:LX/0Vj;

    iget-object v2, p0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->E:LX/44p;

    invoke-static {v0, v1, v2}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2501592
    iget-object v1, p0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->K:LX/0Vj;

    iget-object v2, p0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->E:LX/44p;

    invoke-static {v0, v1, v2}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2501593
    iget-object v1, p0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->L:LX/0Vj;

    iget-object v2, p0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->E:LX/44p;

    invoke-static {v0, v1, v2}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2501594
    iget-object v1, p0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->F:LX/0TF;

    iget-object v2, p0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->E:LX/44p;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2501595
    return-void
.end method

.method private static c(Landroid/content/Intent;)LX/Hm0;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2501619
    const-string v0, "ssid"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2501620
    const-string v0, "passkey"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2501621
    if-eqz v1, :cond_0

    if-nez v2, :cond_1

    .line 2501622
    :cond_0
    const/4 v0, 0x0

    .line 2501623
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, LX/Hm0;

    invoke-direct {v0, v1, v2}, LX/Hm0;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2501608
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2501609
    invoke-static {p0, p0}, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2501610
    const v0, 0x7f030195

    invoke-virtual {p0, v0}, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->setContentView(I)V

    .line 2501611
    invoke-static {p0}, LX/Hn4;->a(Landroid/app/Activity;)LX/0h5;

    .line 2501612
    new-instance v0, LX/HnH;

    invoke-direct {v0, p0}, LX/HnH;-><init>(Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;)V

    iput-object v0, p0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->B:LX/HnH;

    .line 2501613
    invoke-virtual {p0}, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2501614
    invoke-static {v0}, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->c(Landroid/content/Intent;)LX/Hm0;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->z:LX/Hm0;

    .line 2501615
    iget-object v0, p0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->z:LX/Hm0;

    if-nez v0, :cond_0

    .line 2501616
    invoke-virtual {p0}, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->finish()V

    .line 2501617
    :goto_0
    return-void

    .line 2501618
    :cond_0
    invoke-direct {p0}, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->b()V

    goto :goto_0
.end method

.method public final onBackPressed()V
    .locals 2

    .prologue
    .line 2501604
    iget-object v0, p0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->s:LX/HnK;

    .line 2501605
    sget-object v1, LX/HnJ;->TRANSFER_ACTIVITY_BACK_PRESSED:LX/HnJ;

    invoke-static {v0, v1}, LX/HnK;->a(LX/HnK;LX/HnJ;)V

    .line 2501606
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 2501607
    return-void
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x22

    const v2, 0x7c04045f

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2501596
    iget-object v1, p0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->s:LX/HnK;

    .line 2501597
    sget-object v2, LX/HnJ;->TRANSFER_ACTIVITY_ON_DESTROY:LX/HnJ;

    invoke-static {v1, v2}, LX/HnK;->a(LX/HnK;LX/HnJ;)V

    .line 2501598
    :try_start_0
    iget-object v1, p0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->x:LX/Hm4;

    invoke-virtual {v1}, LX/Hm4;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 2501599
    :goto_0
    :try_start_1
    iget-object v1, p0, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;->q:LX/Hmo;

    .line 2501600
    invoke-static {v1}, LX/Hmo;->b(LX/Hmo;)V

    .line 2501601
    iget-object v2, v1, LX/Hmo;->a:LX/0TD;

    new-instance v3, LX/Hmn;

    invoke-direct {v3, v1}, LX/Hmn;-><init>(LX/Hmo;)V

    invoke-interface {v2, v3}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 2501602
    :goto_1
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 2501603
    const v1, 0x5278ed0c

    invoke-static {v1, v0}, LX/02F;->c(II)V

    return-void

    :catch_0
    goto :goto_1

    :catch_1
    goto :goto_0
.end method
