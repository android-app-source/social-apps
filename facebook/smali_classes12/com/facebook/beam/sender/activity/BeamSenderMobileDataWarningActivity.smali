.class public Lcom/facebook/beam/sender/activity/BeamSenderMobileDataWarningActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:LX/HnK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/14G;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/Hmj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2501355
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/beam/sender/activity/BeamSenderMobileDataWarningActivity;LX/HnK;LX/14G;LX/Hmj;)V
    .locals 0

    .prologue
    .line 2501356
    iput-object p1, p0, Lcom/facebook/beam/sender/activity/BeamSenderMobileDataWarningActivity;->p:LX/HnK;

    iput-object p2, p0, Lcom/facebook/beam/sender/activity/BeamSenderMobileDataWarningActivity;->q:LX/14G;

    iput-object p3, p0, Lcom/facebook/beam/sender/activity/BeamSenderMobileDataWarningActivity;->r:LX/Hmj;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/beam/sender/activity/BeamSenderMobileDataWarningActivity;

    invoke-static {v2}, LX/HnK;->a(LX/0QB;)LX/HnK;

    move-result-object v0

    check-cast v0, LX/HnK;

    invoke-static {v2}, LX/14G;->a(LX/0QB;)LX/14G;

    move-result-object v1

    check-cast v1, LX/14G;

    invoke-static {v2}, LX/Hmj;->b(LX/0QB;)LX/Hmj;

    move-result-object v2

    check-cast v2, LX/Hmj;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/beam/sender/activity/BeamSenderMobileDataWarningActivity;->a(Lcom/facebook/beam/sender/activity/BeamSenderMobileDataWarningActivity;LX/HnK;LX/14G;LX/Hmj;)V

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 2501357
    iget-object v0, p0, Lcom/facebook/beam/sender/activity/BeamSenderMobileDataWarningActivity;->q:LX/14G;

    invoke-virtual {v0}, LX/14G;->b()LX/0am;

    move-result-object v0

    .line 2501358
    iget-object v1, p0, Lcom/facebook/beam/sender/activity/BeamSenderMobileDataWarningActivity;->r:LX/Hmj;

    invoke-virtual {v1}, LX/Hmj;->f()Z

    move-result v1

    .line 2501359
    if-eqz v1, :cond_0

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2501360
    const v0, 0x7f083976

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2501361
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2501362
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2501363
    invoke-static {p0, p0}, Lcom/facebook/beam/sender/activity/BeamSenderMobileDataWarningActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2501364
    const v0, 0x7f030194

    invoke-virtual {p0, v0}, Lcom/facebook/beam/sender/activity/BeamSenderMobileDataWarningActivity;->setContentView(I)V

    .line 2501365
    invoke-static {p0}, LX/Hn4;->a(Landroid/app/Activity;)LX/0h5;

    .line 2501366
    iget-object v0, p0, Lcom/facebook/beam/sender/activity/BeamSenderMobileDataWarningActivity;->p:LX/HnK;

    .line 2501367
    sget-object v1, LX/HnJ;->MOBILE_DATA_WARNING_SCREEN_OPENED:LX/HnJ;

    invoke-static {v0, v1}, LX/HnK;->a(LX/HnK;LX/HnJ;)V

    .line 2501368
    const v0, 0x7f0d06dd

    invoke-virtual {p0, v0}, Lcom/facebook/beam/sender/activity/BeamSenderMobileDataWarningActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    .line 2501369
    new-instance v1, LX/Hn7;

    invoke-direct {v1, p0}, LX/Hn7;-><init>(Lcom/facebook/beam/sender/activity/BeamSenderMobileDataWarningActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2501370
    return-void
.end method
