.class public Lcom/facebook/beam/sender/activity/BeamSenderActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# instance fields
.field public p:LX/Hn6;

.field public q:LX/Hm0;

.field public r:LX/14G;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/HnK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/0kb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/17Y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/Hmj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private x:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2501339
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2501340
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/beam/sender/activity/BeamSenderActivity;->x:Z

    .line 2501341
    return-void
.end method

.method private static a(Lcom/facebook/beam/sender/activity/BeamSenderActivity;LX/14G;Lcom/facebook/content/SecureContextHelper;LX/HnK;LX/0kb;LX/17Y;LX/Hmj;)V
    .locals 0

    .prologue
    .line 2501338
    iput-object p1, p0, Lcom/facebook/beam/sender/activity/BeamSenderActivity;->r:LX/14G;

    iput-object p2, p0, Lcom/facebook/beam/sender/activity/BeamSenderActivity;->s:Lcom/facebook/content/SecureContextHelper;

    iput-object p3, p0, Lcom/facebook/beam/sender/activity/BeamSenderActivity;->t:LX/HnK;

    iput-object p4, p0, Lcom/facebook/beam/sender/activity/BeamSenderActivity;->u:LX/0kb;

    iput-object p5, p0, Lcom/facebook/beam/sender/activity/BeamSenderActivity;->v:LX/17Y;

    iput-object p6, p0, Lcom/facebook/beam/sender/activity/BeamSenderActivity;->w:LX/Hmj;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 7

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v6

    move-object v0, p0

    check-cast v0, Lcom/facebook/beam/sender/activity/BeamSenderActivity;

    invoke-static {v6}, LX/14G;->a(LX/0QB;)LX/14G;

    move-result-object v1

    check-cast v1, LX/14G;

    invoke-static {v6}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v6}, LX/HnK;->a(LX/0QB;)LX/HnK;

    move-result-object v3

    check-cast v3, LX/HnK;

    invoke-static {v6}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v4

    check-cast v4, LX/0kb;

    invoke-static {v6}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v5

    check-cast v5, LX/17Y;

    invoke-static {v6}, LX/Hmj;->b(LX/0QB;)LX/Hmj;

    move-result-object v6

    check-cast v6, LX/Hmj;

    invoke-static/range {v0 .. v6}, Lcom/facebook/beam/sender/activity/BeamSenderActivity;->a(Lcom/facebook/beam/sender/activity/BeamSenderActivity;LX/14G;Lcom/facebook/content/SecureContextHelper;LX/HnK;LX/0kb;LX/17Y;LX/Hmj;)V

    return-void
.end method

.method private static c(Landroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 2501337
    const-string v0, "ssid"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "code"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(Landroid/content/Intent;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2501329
    const-string v1, "redirect_uri"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2501330
    if-nez v1, :cond_1

    .line 2501331
    :cond_0
    :goto_0
    return v0

    .line 2501332
    :cond_1
    invoke-direct {p0}, Lcom/facebook/beam/sender/activity/BeamSenderActivity;->n()Z

    move-result v2

    .line 2501333
    iget-object v3, p0, Lcom/facebook/beam/sender/activity/BeamSenderActivity;->t:LX/HnK;

    .line 2501334
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v4

    const-string p1, "redirectUri"

    invoke-virtual {v4, p1, v1}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v4

    const-string p1, "hasInternetConnection"

    invoke-virtual {v4, p1, v2}, LX/1rQ;->a(Ljava/lang/String;Z)LX/1rQ;

    move-result-object v4

    .line 2501335
    sget-object p1, LX/HnJ;->WEBVIEW_CHECKED:LX/HnJ;

    invoke-static {v3, p1, v4}, LX/HnK;->a(LX/HnK;LX/HnJ;LX/1rQ;)V

    .line 2501336
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, LX/2yp;->d(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/facebook/beam/sender/activity/BeamSenderActivity;->n()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private l()V
    .locals 6

    .prologue
    .line 2501308
    invoke-direct {p0}, Lcom/facebook/beam/sender/activity/BeamSenderActivity;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2501309
    iget-object v0, p0, Lcom/facebook/beam/sender/activity/BeamSenderActivity;->t:LX/HnK;

    .line 2501310
    sget-object v1, LX/HnJ;->GOOGLE_PLAY_ERROR_SCREEN_OPENED:LX/HnJ;

    invoke-static {v0, v1}, LX/HnK;->a(LX/HnK;LX/HnJ;)V

    .line 2501311
    iget-object v0, p0, Lcom/facebook/beam/sender/activity/BeamSenderActivity;->p:LX/Hn6;

    invoke-virtual {v0}, LX/Hn6;->a()V

    .line 2501312
    :goto_0
    return-void

    .line 2501313
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/beam/sender/activity/BeamSenderActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2501314
    invoke-direct {p0, v0}, Lcom/facebook/beam/sender/activity/BeamSenderActivity;->d(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2501315
    iget-object v1, p0, Lcom/facebook/beam/sender/activity/BeamSenderActivity;->v:LX/17Y;

    sget-object v2, LX/0ax;->do:Ljava/lang/String;

    const-string v3, "redirect_uri"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, p0, v2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2501316
    iget-object v2, p0, Lcom/facebook/beam/sender/activity/BeamSenderActivity;->t:LX/HnK;

    const-string v3, "redirect_uri"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2501317
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v3

    const-string v4, "redirectUri"

    invoke-virtual {v3, v4, v0}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v3

    .line 2501318
    sget-object v4, LX/HnJ;->WEBVIEW_PICKER_OPENED:LX/HnJ;

    invoke-static {v2, v4, v3}, LX/HnK;->a(LX/HnK;LX/HnJ;LX/1rQ;)V

    .line 2501319
    iget-object v0, p0, Lcom/facebook/beam/sender/activity/BeamSenderActivity;->s:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, v1, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0

    .line 2501320
    :cond_1
    invoke-static {v0}, Lcom/facebook/beam/sender/activity/BeamSenderActivity;->c(Landroid/content/Intent;)Z

    move-result v1

    .line 2501321
    if-eqz v1, :cond_2

    .line 2501322
    new-instance v2, LX/Hm0;

    const-string v3, "ssid"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "code"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v0}, LX/Hm0;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/facebook/beam/sender/activity/BeamSenderActivity;->q:LX/Hm0;

    .line 2501323
    :goto_1
    iget-object v0, p0, Lcom/facebook/beam/sender/activity/BeamSenderActivity;->t:LX/HnK;

    iget-object v2, p0, Lcom/facebook/beam/sender/activity/BeamSenderActivity;->q:LX/Hm0;

    .line 2501324
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v3

    const-string v4, "hasPrefilledHotspotParams"

    invoke-virtual {v3, v4, v1}, LX/1rQ;->a(Ljava/lang/String;Z)LX/1rQ;

    move-result-object v3

    const-string v4, "connectionCode"

    invoke-virtual {v2}, LX/Hm0;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v3

    .line 2501325
    sget-object v4, LX/HnJ;->START_TRANSFER_SCREEN_OPENED:LX/HnJ;

    invoke-static {v0, v4, v3}, LX/HnK;->a(LX/HnK;LX/HnJ;LX/1rQ;)V

    .line 2501326
    iget-object v0, p0, Lcom/facebook/beam/sender/activity/BeamSenderActivity;->p:LX/Hn6;

    invoke-virtual {v0}, LX/Hn6;->b()V

    .line 2501327
    iget-object v0, p0, Lcom/facebook/beam/sender/activity/BeamSenderActivity;->p:LX/Hn6;

    new-instance v1, LX/Hn5;

    invoke-direct {v1, p0}, LX/Hn5;-><init>(Lcom/facebook/beam/sender/activity/BeamSenderActivity;)V

    invoke-virtual {v0, v1}, LX/Hn6;->a(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 2501328
    :cond_2
    invoke-static {}, LX/Hm0;->d()LX/Hm0;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/beam/sender/activity/BeamSenderActivity;->q:LX/Hm0;

    goto :goto_1
.end method

.method private m()Z
    .locals 5

    .prologue
    .line 2501299
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 2501300
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 2501301
    invoke-virtual {v1, v0}, Landroid/content/pm/PackageManager;->getInstallerPackageName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2501302
    if-nez v0, :cond_1

    .line 2501303
    const/4 v0, 0x0

    .line 2501304
    :goto_0
    move v0, v0

    .line 2501305
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/beam/sender/activity/BeamSenderActivity;->w:LX/Hmj;

    .line 2501306
    iget-object v1, v0, LX/Hmj;->a:LX/0W3;

    sget-wide v3, LX/0X5;->iq:J

    invoke-interface {v1, v3, v4}, LX/0W4;->a(J)Z

    move-result v1

    move v0, v1

    .line 2501307
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    sget-object v1, LX/Hn4;->a:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method private n()Z
    .locals 1

    .prologue
    .line 2501257
    iget-object v0, p0, Lcom/facebook/beam/sender/activity/BeamSenderActivity;->u:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(LX/Hm0;)V
    .locals 3

    .prologue
    .line 2501289
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/beam/sender/activity/BeamSenderActivity;->x:Z

    .line 2501290
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/beam/sender/activity/BeamSenderTransferActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2501291
    const-string v1, "ssid"

    .line 2501292
    iget-object v2, p1, LX/Hm0;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2501293
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2501294
    const-string v1, "passkey"

    .line 2501295
    iget-object v2, p1, LX/Hm0;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2501296
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2501297
    iget-object v1, p0, Lcom/facebook/beam/sender/activity/BeamSenderActivity;->s:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2501298
    return-void
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 2501287
    iget-object v0, p0, Lcom/facebook/beam/sender/activity/BeamSenderActivity;->r:LX/14G;

    invoke-virtual {v0}, LX/14G;->b()LX/0am;

    move-result-object v0

    .line 2501288
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 2501284
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/beam/sender/activity/BeamSenderMobileDataWarningActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2501285
    iget-object v1, p0, Lcom/facebook/beam/sender/activity/BeamSenderActivity;->s:Lcom/facebook/content/SecureContextHelper;

    const/4 v2, 0x1

    invoke-interface {v1, v0, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2501286
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2501273
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2501274
    invoke-static {p0, p0}, Lcom/facebook/beam/sender/activity/BeamSenderActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2501275
    const v0, 0x7f030193

    invoke-virtual {p0, v0}, Lcom/facebook/beam/sender/activity/BeamSenderActivity;->setContentView(I)V

    .line 2501276
    iget-object v0, p0, Lcom/facebook/beam/sender/activity/BeamSenderActivity;->w:LX/Hmj;

    invoke-virtual {v0}, LX/Hmj;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2501277
    invoke-virtual {p0}, Lcom/facebook/beam/sender/activity/BeamSenderActivity;->finish()V

    .line 2501278
    :goto_0
    return-void

    .line 2501279
    :cond_0
    invoke-static {p0}, LX/Hn4;->a(Landroid/app/Activity;)LX/0h5;

    .line 2501280
    new-instance v0, LX/Hn6;

    invoke-direct {v0, p0}, LX/Hn6;-><init>(Lcom/facebook/beam/sender/activity/BeamSenderActivity;)V

    iput-object v0, p0, Lcom/facebook/beam/sender/activity/BeamSenderActivity;->p:LX/Hn6;

    .line 2501281
    iget-object v0, p0, Lcom/facebook/beam/sender/activity/BeamSenderActivity;->t:LX/HnK;

    .line 2501282
    iget-object v1, v0, LX/HnK;->a:LX/0if;

    sget-object p1, LX/0ig;->aY:LX/0ih;

    invoke-virtual {v1, p1}, LX/0if;->a(LX/0ih;)V

    .line 2501283
    invoke-direct {p0}, Lcom/facebook/beam/sender/activity/BeamSenderActivity;->l()V

    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2501268
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/activity/FbFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 2501269
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 2501270
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 2501271
    iget-object v0, p0, Lcom/facebook/beam/sender/activity/BeamSenderActivity;->q:LX/Hm0;

    invoke-virtual {p0, v0}, Lcom/facebook/beam/sender/activity/BeamSenderActivity;->a(LX/Hm0;)V

    .line 2501272
    :cond_0
    return-void
.end method

.method public final onDestroy()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x70296cde

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2501264
    iget-object v1, p0, Lcom/facebook/beam/sender/activity/BeamSenderActivity;->t:LX/HnK;

    .line 2501265
    iget-object v2, v1, LX/HnK;->a:LX/0if;

    sget-object v4, LX/0ig;->aY:LX/0ih;

    invoke-virtual {v2, v4}, LX/0if;->c(LX/0ih;)V

    .line 2501266
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 2501267
    const/16 v1, 0x23

    const v2, 0x64985703

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x248d1e4e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2501258
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 2501259
    iget-boolean v1, p0, Lcom/facebook/beam/sender/activity/BeamSenderActivity;->x:Z

    if-eqz v1, :cond_0

    .line 2501260
    invoke-static {}, LX/Hm0;->d()LX/Hm0;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/beam/sender/activity/BeamSenderActivity;->q:LX/Hm0;

    .line 2501261
    iget-object v1, p0, Lcom/facebook/beam/sender/activity/BeamSenderActivity;->q:LX/Hm0;

    invoke-virtual {v1}, LX/Hm0;->c()Ljava/lang/String;

    .line 2501262
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/beam/sender/activity/BeamSenderActivity;->x:Z

    .line 2501263
    :cond_0
    const/16 v1, 0x23

    const v2, -0x70f9fe97

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
