.class public Lcom/facebook/looknow/LookNowPermalinkFragment;
.super Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;
.source ""


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
    fragment = .enum LX/0cQ;->LOOK_NOW_FRAGMENT:LX/0cQ;
.end annotation


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/customizedstory/CustomizedStoryRootPartDefinition;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1DS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/IYk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/1Db;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/IYq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2iz;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/IYi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private i:LX/0g8;

.field public j:LX/1Qq;

.field public k:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2588142
    invoke-direct {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;-><init>()V

    return-void
.end method

.method public static b(Lcom/facebook/looknow/LookNowPermalinkFragment;)LX/1Qq;
    .locals 5

    .prologue
    .line 2588131
    iget-object v0, p0, Lcom/facebook/looknow/LookNowPermalinkFragment;->j:LX/1Qq;

    if-nez v0, :cond_0

    .line 2588132
    iget-object v0, p0, Lcom/facebook/looknow/LookNowPermalinkFragment;->c:LX/IYk;

    .line 2588133
    sget-object v1, LX/IYl;->a:LX/IYl;

    move-object v1, v1

    .line 2588134
    new-instance v2, Lcom/facebook/looknow/LookNowPermalinkFragment$2;

    invoke-direct {v2, p0}, Lcom/facebook/looknow/LookNowPermalinkFragment$2;-><init>(Lcom/facebook/looknow/LookNowPermalinkFragment;)V

    .line 2588135
    new-instance v4, LX/IYj;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {v4, v3, v1, v2}, LX/IYj;-><init>(Landroid/content/Context;LX/1PT;Ljava/lang/Runnable;)V

    .line 2588136
    move-object v0, v4

    .line 2588137
    iget-object v1, p0, Lcom/facebook/looknow/LookNowPermalinkFragment;->b:LX/1DS;

    iget-object v2, p0, Lcom/facebook/looknow/LookNowPermalinkFragment;->a:LX/0Ot;

    invoke-static {p0}, Lcom/facebook/looknow/LookNowPermalinkFragment;->d(Lcom/facebook/looknow/LookNowPermalinkFragment;)LX/0fz;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v1

    .line 2588138
    iput-object v0, v1, LX/1Ql;->f:LX/1PW;

    .line 2588139
    move-object v0, v1

    .line 2588140
    invoke-virtual {v0}, LX/1Ql;->e()LX/1Qq;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/looknow/LookNowPermalinkFragment;->j:LX/1Qq;

    .line 2588141
    :cond_0
    iget-object v0, p0, Lcom/facebook/looknow/LookNowPermalinkFragment;->j:LX/1Qq;

    return-object v0
.end method

.method public static d(Lcom/facebook/looknow/LookNowPermalinkFragment;)LX/0fz;
    .locals 1

    .prologue
    .line 2588128
    iget-object v0, p0, Lcom/facebook/looknow/LookNowPermalinkFragment;->e:LX/IYq;

    .line 2588129
    iget-object p0, v0, LX/IYq;->d:LX/0fz;

    move-object v0, p0

    .line 2588130
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    .line 2588125
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/looknow/LookNowPermalinkFragment;

    const/16 v3, 0x897

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {v0}, LX/1DS;->b(LX/0QB;)LX/1DS;

    move-result-object v4

    check-cast v4, LX/1DS;

    const-class v5, LX/IYk;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/IYk;

    invoke-static {v0}, LX/1Db;->a(LX/0QB;)LX/1Db;

    move-result-object v6

    check-cast v6, LX/1Db;

    invoke-static {v0}, LX/IYq;->b(LX/0QB;)LX/IYq;

    move-result-object v7

    check-cast v7, LX/IYq;

    const/16 v8, 0x1061

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x19c6

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {v0}, LX/IYi;->a(LX/0QB;)LX/IYi;

    move-result-object v0

    check-cast v0, LX/IYi;

    iput-object v3, v2, Lcom/facebook/looknow/LookNowPermalinkFragment;->a:LX/0Ot;

    iput-object v4, v2, Lcom/facebook/looknow/LookNowPermalinkFragment;->b:LX/1DS;

    iput-object v5, v2, Lcom/facebook/looknow/LookNowPermalinkFragment;->c:LX/IYk;

    iput-object v6, v2, Lcom/facebook/looknow/LookNowPermalinkFragment;->d:LX/1Db;

    iput-object v7, v2, Lcom/facebook/looknow/LookNowPermalinkFragment;->e:LX/IYq;

    iput-object v8, v2, Lcom/facebook/looknow/LookNowPermalinkFragment;->f:LX/0Ot;

    iput-object v9, v2, Lcom/facebook/looknow/LookNowPermalinkFragment;->g:LX/0Ot;

    iput-object v0, v2, Lcom/facebook/looknow/LookNowPermalinkFragment;->h:LX/IYi;

    .line 2588126
    invoke-super {p0, p1}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->a(Landroid/os/Bundle;)V

    .line 2588127
    return-void
.end method

.method public final mJ_()V
    .locals 0

    .prologue
    .line 2588124
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 2588078
    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    .line 2588079
    :cond_0
    :goto_0
    return-void

    .line 2588080
    :cond_1
    const/16 v0, 0x6dc

    if-ne p1, v0, :cond_0

    .line 2588081
    iget-object v0, p0, Lcom/facebook/looknow/LookNowPermalinkFragment;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    .line 2588082
    invoke-virtual {v0, p3}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2588083
    const-string v0, "publishPostParams"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/PublishPostParams;

    .line 2588084
    iget-object v1, p0, Lcom/facebook/looknow/LookNowPermalinkFragment;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2iz;

    iget-object v0, v0, Lcom/facebook/composer/publish/common/PublishPostParams;->composerSessionId:Ljava/lang/String;

    invoke-virtual {v1, v0, p0}, LX/2iz;->a(Ljava/lang/String;Lcom/facebook/base/fragment/FbFragment;)Z

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x199649e1

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2588099
    const v0, 0x7f030a5e

    .line 2588100
    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2588101
    const v0, 0x7f0d1a5d

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2588102
    new-instance v3, LX/1Oz;

    invoke-direct {v3, v0}, LX/1Oz;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2588103
    new-instance v3, LX/0g7;

    invoke-direct {v3, v0}, LX/0g7;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    iput-object v3, p0, Lcom/facebook/looknow/LookNowPermalinkFragment;->i:LX/0g8;

    .line 2588104
    iget-object v0, p0, Lcom/facebook/looknow/LookNowPermalinkFragment;->i:LX/0g8;

    invoke-static {p0}, Lcom/facebook/looknow/LookNowPermalinkFragment;->b(Lcom/facebook/looknow/LookNowPermalinkFragment;)LX/1Qq;

    move-result-object v3

    invoke-interface {v0, v3}, LX/0g8;->a(Landroid/widget/ListAdapter;)V

    .line 2588105
    iget-object v0, p0, Lcom/facebook/looknow/LookNowPermalinkFragment;->i:LX/0g8;

    iget-object v3, p0, Lcom/facebook/looknow/LookNowPermalinkFragment;->d:LX/1Db;

    invoke-virtual {v3}, LX/1Db;->a()LX/1St;

    move-result-object v3

    invoke-interface {v0, v3}, LX/0g8;->a(LX/1St;)V

    .line 2588106
    const v0, 0x7f0d0a95

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/looknow/LookNowPermalinkFragment;->k:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2588107
    iget-object v0, p0, Lcom/facebook/looknow/LookNowPermalinkFragment;->e:LX/IYq;

    new-instance v3, LX/IYm;

    invoke-direct {v3, p0}, LX/IYm;-><init>(Lcom/facebook/looknow/LookNowPermalinkFragment;)V

    .line 2588108
    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/IYm;

    iput-object p1, v0, LX/IYq;->g:LX/IYm;

    .line 2588109
    iget-object v0, p0, Lcom/facebook/looknow/LookNowPermalinkFragment;->h:LX/IYi;

    sget-object v3, LX/IYh;->LOOK_NOW_PERMALINK_OPENED:LX/IYh;

    invoke-virtual {v0, v3}, LX/IYi;->a(LX/IYh;)V

    .line 2588110
    iget-object v0, p0, Lcom/facebook/looknow/LookNowPermalinkFragment;->e:LX/IYq;

    const/4 p3, 0x0

    .line 2588111
    iget-object v3, v0, LX/IYq;->e:LX/1Ck;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    .line 2588112
    iget-object p1, v0, LX/IYq;->g:LX/IYm;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, LX/IYm;->a(Z)V

    .line 2588113
    new-instance p1, LX/IYr;

    invoke-direct {p1}, LX/IYr;-><init>()V

    move-object p1, p1

    .line 2588114
    invoke-static {p1}, LX/0w9;->a(LX/0gW;)LX/0gW;

    .line 2588115
    iget-object p2, v0, LX/IYq;->f:LX/0w9;

    invoke-virtual {p2, p1}, LX/0w9;->b(LX/0gW;)LX/0gW;

    .line 2588116
    iget-object p2, v0, LX/IYq;->f:LX/0w9;

    invoke-virtual {p2, p1}, LX/0w9;->c(LX/0gW;)LX/0gW;

    .line 2588117
    invoke-static {p1}, LX/0w9;->d(LX/0gW;)LX/0gW;

    .line 2588118
    invoke-static {p1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object p1

    sget-object p2, LX/0zS;->c:LX/0zS;

    invoke-virtual {p1, p2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object p1

    move-object p1, p1

    .line 2588119
    iget-object p2, v0, LX/IYq;->c:LX/0tX;

    invoke-virtual {p2, p1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object p1

    .line 2588120
    iget-object p2, v0, LX/IYq;->a:LX/0QK;

    iget-object p3, v0, LX/IYq;->b:Ljava/util/concurrent/Executor;

    invoke-static {p1, p2, p3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object p1

    move-object p1, p1

    .line 2588121
    move-object p1, p1

    .line 2588122
    new-instance p2, LX/IYp;

    invoke-direct {p2, v0}, LX/IYp;-><init>(LX/IYq;)V

    invoke-virtual {v3, p0, p1, p2}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2588123
    const/16 v0, 0x2b

    const v3, -0x5fc9f2e5

    invoke-static {v4, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x492e479e    # 713849.9f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2588095
    invoke-super {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onDestroy()V

    .line 2588096
    iget-object v1, p0, Lcom/facebook/looknow/LookNowPermalinkFragment;->j:LX/1Qq;

    if-eqz v1, :cond_0

    .line 2588097
    iget-object v1, p0, Lcom/facebook/looknow/LookNowPermalinkFragment;->j:LX/1Qq;

    invoke-interface {v1}, LX/0Vf;->dispose()V

    .line 2588098
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x1029f6c0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x14b79dba

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2588091
    invoke-super {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onDestroyView()V

    .line 2588092
    iget-object v1, p0, Lcom/facebook/looknow/LookNowPermalinkFragment;->j:LX/1Qq;

    if-eqz v1, :cond_0

    .line 2588093
    iget-object v1, p0, Lcom/facebook/looknow/LookNowPermalinkFragment;->j:LX/1Qq;

    invoke-interface {v1}, LX/0Vf;->dispose()V

    .line 2588094
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x267c99f2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x43019f92

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2588085
    invoke-super {p0}, Lcom/facebook/feed/ui/basefeedfragment/BaseFeedFragment;->onStart()V

    .line 2588086
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2588087
    if-eqz v0, :cond_0

    .line 2588088
    const v2, 0x7f0838a9

    invoke-interface {v0, v2}, LX/1ZF;->x_(I)V

    .line 2588089
    const/4 v2, 0x1

    invoke-interface {v0, v2}, LX/1ZF;->k_(Z)V

    .line 2588090
    :cond_0
    const/16 v0, 0x2b

    const v2, -0x606e8b88

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
