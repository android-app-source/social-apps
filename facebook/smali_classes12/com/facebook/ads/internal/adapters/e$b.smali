.class public final Lcom/facebook/ads/internal/adapters/e$b;
.super Lcom/facebook/ads/internal/util/t;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/ads/internal/util/t",
        "<",
        "LX/HjZ;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/HjZ;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/facebook/ads/internal/util/t;-><init>(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    invoke-virtual {p0}, Lcom/facebook/ads/internal/util/t;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HjZ;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v1, v0, LX/HjZ;->i:Z

    if-nez v1, :cond_2

    iget-boolean v1, v0, LX/HjZ;->j:Z

    if-nez v1, :cond_0

    :cond_2
    iget-object v1, v0, LX/HjZ;->d:Landroid/view/View;

    iget-object v2, v0, LX/HjZ;->f:LX/Hj7;

    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    iget-object v3, v0, LX/HjZ;->c:Landroid/content/Context;

    iget v4, v0, LX/HjZ;->e:I

    invoke-static {v3, v1, v4}, LX/Hko;->a(Landroid/content/Context;Landroid/view/View;I)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v2}, LX/Hj7;->a()V

    const/4 v1, 0x1

    iput-boolean v1, v0, LX/HjZ;->j:Z

    goto :goto_0

    :cond_3
    iget-object v1, v0, LX/HjZ;->g:Landroid/os/Handler;

    iget-object v2, v0, LX/HjZ;->h:Ljava/lang/Runnable;

    iget v0, v0, LX/HjZ;->b:I

    int-to-long v4, v0

    const v0, 0x27c7197b

    invoke-static {v1, v2, v4, v5, v0}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_0
.end method
