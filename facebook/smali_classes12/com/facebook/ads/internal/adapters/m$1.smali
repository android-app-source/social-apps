.class public final Lcom/facebook/ads/internal/adapters/m$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/Hjm;


# direct methods
.method public constructor <init>(LX/Hjm;)V
    .locals 0

    iput-object p1, p0, Lcom/facebook/ads/internal/adapters/m$1;->a:LX/Hjm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    iget-object v0, p0, Lcom/facebook/ads/internal/adapters/m$1;->a:LX/Hjm;

    iget-object v0, v0, LX/Hjm;->c:LX/Hl2;

    iget-boolean v1, v0, LX/Hl2;->a:Z

    move v0, v1

    if-eqz v0, :cond_0

    sget-object v0, LX/Hjm;->b:Ljava/lang/String;

    const-string v1, "Webview already destroyed, cannot activate"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/facebook/ads/internal/adapters/m$1;->a:LX/Hjm;

    iget-object v0, v0, LX/Hjm;->c:LX/Hl2;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "javascript:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/ads/internal/adapters/m$1;->a:LX/Hjm;

    iget-object v2, v2, LX/Hjm;->d:LX/Hjl;

    iget-object p0, v2, LX/Hjl;->b:Ljava/lang/String;

    move-object v2, p0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Hl2;->loadUrl(Ljava/lang/String;)V

    goto :goto_0
.end method
