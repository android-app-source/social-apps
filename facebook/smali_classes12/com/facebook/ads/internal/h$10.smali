.class public final Lcom/facebook/ads/internal/h$10;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/Hjr;

.field public final synthetic b:LX/HkC;


# direct methods
.method public constructor <init>(LX/HkC;LX/Hjr;)V
    .locals 0

    iput-object p1, p0, Lcom/facebook/ads/internal/h$10;->b:LX/HkC;

    iput-object p2, p0, Lcom/facebook/ads/internal/h$10;->a:LX/Hjr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    iget-object v0, p0, Lcom/facebook/ads/internal/h$10;->b:LX/HkC;

    iget-object v0, v0, LX/HkC;->a:LX/HjO;

    iget-object v1, p0, Lcom/facebook/ads/internal/h$10;->a:LX/Hjr;

    invoke-virtual {v0, v1}, LX/HjO;->a(LX/Hjr;)V

    iget-object v0, p0, Lcom/facebook/ads/internal/h$10;->b:LX/HkC;

    iget-boolean v0, v0, LX/HkC;->h:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/ads/internal/h$10;->b:LX/HkC;

    iget-boolean v0, v0, LX/HkC;->g:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/facebook/ads/internal/h$10;->a:LX/Hjr;

    iget-object v1, v0, LX/Hjr;->a:LX/HjN;

    move-object v0, v1

    invoke-virtual {v0}, LX/HjN;->getErrorCode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    sget-object v0, LX/Hk7;->a:[I

    iget-object v1, p0, Lcom/facebook/ads/internal/h$10;->b:LX/HkC;

    invoke-static {v1}, LX/HkC;->c(LX/HkC;)LX/HkF;

    move-result-object v1

    invoke-virtual {v1}, LX/HkF;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/facebook/ads/internal/h$10;->b:LX/HkC;

    iget-object v0, v0, LX/HkC;->d:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/ads/internal/h$10;->b:LX/HkC;

    iget-object v1, v1, LX/HkC;->e:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7530

    const v4, 0x8b14fa8

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    iget-object v0, p0, Lcom/facebook/ads/internal/h$10;->b:LX/HkC;

    const/4 v1, 0x1

    iput-boolean v1, v0, LX/HkC;->g:Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_2
    .end packed-switch
.end method
