.class public final Lcom/facebook/ads/internal/server/a$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:LX/Hk0;

.field public final synthetic c:LX/HkJ;


# direct methods
.method public constructor <init>(LX/HkJ;Landroid/content/Context;LX/Hk0;)V
    .locals 0

    iput-object p1, p0, Lcom/facebook/ads/internal/server/a$1;->c:LX/HkJ;

    iput-object p2, p0, Lcom/facebook/ads/internal/server/a$1;->a:Landroid/content/Context;

    iput-object p3, p0, Lcom/facebook/ads/internal/server/a$1;->b:LX/Hk0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    iget-object v0, p0, Lcom/facebook/ads/internal/server/a$1;->a:Landroid/content/Context;

    const/4 v2, 0x0

    sget-boolean v1, LX/Hk1;->p:Z

    if-nez v1, :cond_7

    :goto_0
    iget-object v0, p0, Lcom/facebook/ads/internal/server/a$1;->c:LX/HkJ;

    iget-object v1, p0, Lcom/facebook/ads/internal/server/a$1;->b:LX/Hk0;

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    const-string v2, "PLACEMENT_ID"

    iget-object v3, v1, LX/Hk0;->a:Ljava/lang/String;

    invoke-static {v4, v2, v3}, LX/Hk0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, v1, LX/Hk0;->b:LX/HkF;

    sget-object v3, LX/HkF;->UNKNOWN:LX/HkF;

    if-eq v2, v3, :cond_0

    const-string v2, "PLACEMENT_TYPE"

    iget-object v3, v1, LX/Hk0;->b:LX/HkF;

    invoke-virtual {v3}, LX/HkF;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v2, v3}, LX/Hk0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v2, v1, LX/Hk0;->d:Landroid/content/Context;

    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    const-string v3, "VIEWABLE"

    const-string v6, "1"

    invoke-interface {v5, v3, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "SCHEMA"

    const-string v6, "json"

    invoke-interface {v5, v3, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "SDK"

    const-string v6, "android"

    invoke-interface {v5, v3, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "SDK_VERSION"

    const-string v6, "4.7.0"

    invoke-interface {v5, v3, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "LOCALE"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v3, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    iget v6, v6, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v7

    iget v7, v7, Landroid/util/DisplayMetrics;->heightPixels:I

    const-string v8, "DENSITY"

    invoke-static {v3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v5, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v8, "SCREEN_WIDTH"

    int-to-float v6, v6

    div-float/2addr v6, v3

    float-to-int v6, v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v8, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v6, "SCREEN_HEIGHT"

    int-to-float v7, v7

    div-float v3, v7, v3

    float-to-int v3, v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v5, v6, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "IDFA"

    sget-object v6, LX/Hk1;->n:Ljava/lang/String;

    invoke-interface {v5, v3, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v6, "IDFA_FLAG"

    sget-boolean v3, LX/Hk1;->o:Z

    if-eqz v3, :cond_c

    const-string v3, "0"

    :goto_1
    invoke-interface {v5, v6, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "ATTRIBUTION_ID"

    sget-object v6, LX/Hk1;->m:Ljava/lang/String;

    invoke-interface {v5, v3, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "OS"

    const-string v6, "Android"

    invoke-interface {v5, v3, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "OSVERS"

    sget-object v6, LX/Hk1;->a:Ljava/lang/String;

    invoke-interface {v5, v3, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "BUNDLE"

    sget-object v6, LX/Hk1;->d:Ljava/lang/String;

    invoke-interface {v5, v3, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "APPNAME"

    sget-object v6, LX/Hk1;->e:Ljava/lang/String;

    invoke-interface {v5, v3, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "APPVERS"

    sget-object v6, LX/Hk1;->f:Ljava/lang/String;

    invoke-interface {v5, v3, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "APPBUILD"

    sget v6, LX/Hk1;->g:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v3, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "CARRIER"

    sget-object v6, LX/Hk1;->h:Ljava/lang/String;

    invoke-interface {v5, v3, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "MAKE"

    sget-object v6, LX/Hk1;->b:Ljava/lang/String;

    invoke-interface {v5, v3, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "MODEL"

    sget-object v6, LX/Hk1;->c:Ljava/lang/String;

    invoke-interface {v5, v3, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "COPPA"

    sget-boolean v6, LX/Hj2;->f:Z

    move v6, v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v3, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "SDK_CAPABILITY"

    invoke-static {}, LX/Hjt;->b()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v3, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v2, v5

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v4, v3, v2}, LX/Hk0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_1
    iget-object v2, v1, LX/Hk0;->i:LX/Hj3;

    if-eqz v2, :cond_2

    const-string v2, "WIDTH"

    iget-object v3, v1, LX/Hk0;->i:LX/Hj3;

    invoke-virtual {v3}, LX/Hj3;->getWidth()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v2, v3}, LX/Hk0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "HEIGHT"

    iget-object v3, v1, LX/Hk0;->i:LX/Hj3;

    invoke-virtual {v3}, LX/Hj3;->getHeight()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v2, v3}, LX/Hk0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    const-string v2, "ADAPTERS"

    iget-object v3, v1, LX/Hk0;->b:LX/HkF;

    sget-object v5, LX/Hjb;->b:Ljava/util/Map;

    invoke-interface {v5, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_d

    sget-object v5, LX/Hjb;->b:Ljava/util/Map;

    invoke-interface {v5, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    :goto_3
    move-object v3, v5

    invoke-static {v4, v2, v3}, LX/Hk0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, v1, LX/Hk0;->e:LX/Hk2;

    if-eqz v2, :cond_3

    const-string v2, "TEMPLATE_ID"

    iget-object v3, v1, LX/Hk0;->e:LX/Hk2;

    invoke-virtual {v3}, LX/Hk2;->a()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v2, v3}, LX/Hk0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget-object v2, v1, LX/Hk0;->g:LX/Hjs;

    if-eqz v2, :cond_4

    const-string v2, "REQUEST_TYPE"

    iget-object v3, v1, LX/Hk0;->g:LX/Hjs;

    invoke-virtual {v3}, LX/Hjs;->a()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v2, v3}, LX/Hk0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    iget-boolean v2, v1, LX/Hk0;->f:Z

    if-eqz v2, :cond_5

    const-string v2, "TEST_MODE"

    const-string v3, "1"

    invoke-static {v4, v2, v3}, LX/Hk0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    iget v2, v1, LX/Hk0;->h:I

    if-eqz v2, :cond_6

    const-string v2, "NUM_ADS_REQUESTED"

    iget v3, v1, LX/Hk0;->h:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v2, v3}, LX/Hk0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    const-string v2, "CLIENT_EVENTS"

    invoke-static {}, LX/Hkh;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v2, v3}, LX/Hk0;->a(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v4

    iput-object v1, v0, LX/HkJ;->a:Ljava/util/Map;

    :try_start_0
    iget-object v0, p0, Lcom/facebook/ads/internal/server/a$1;->c:LX/HkJ;

    new-instance v1, LX/HkR;

    iget-object v2, p0, Lcom/facebook/ads/internal/server/a$1;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/facebook/ads/internal/server/a$1;->b:LX/Hk0;

    iget-object v3, v3, LX/Hk0;->e:LX/Hk2;

    invoke-direct {v1, v2, v3}, LX/HkR;-><init>(Landroid/content/Context;LX/Hk2;)V

    iput-object v1, v0, LX/HkJ;->e:LX/HkR;

    iget-object v0, p0, Lcom/facebook/ads/internal/server/a$1;->c:LX/HkJ;

    iget-object v0, v0, LX/HkJ;->e:LX/HkR;

    iget-object v1, p0, Lcom/facebook/ads/internal/server/a$1;->c:LX/HkJ;

    iget-object v1, v1, LX/HkJ;->f:Ljava/lang/String;

    new-instance v2, LX/Hkc;

    invoke-direct {v2}, LX/Hkc;-><init>()V

    move-object v2, v2

    iget-object v3, p0, Lcom/facebook/ads/internal/server/a$1;->c:LX/HkJ;

    iget-object v3, v3, LX/HkJ;->a:Ljava/util/Map;

    invoke-virtual {v2, v3}, LX/Hkc;->putAll(Ljava/util/Map;)V

    move-object v2, v2

    iget-object v3, p0, Lcom/facebook/ads/internal/server/a$1;->c:LX/HkJ;

    new-instance v4, LX/HkH;

    invoke-direct {v4, v3}, LX/HkH;-><init>(LX/HkJ;)V

    move-object v3, v4

    new-instance v4, LX/HkZ;

    invoke-direct {v4, v1, v2}, LX/HkZ;-><init>(Ljava/lang/String;LX/Hkc;)V

    new-instance v2, LX/HkV;

    invoke-direct {v2, v0, v3}, LX/HkV;-><init>(LX/HkR;LX/HkG;)V

    move-object v1, v2

    invoke-interface {v1, v4}, LX/HkS;->a(LX/HkW;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_4
    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/facebook/ads/internal/server/a$1;->c:LX/HkJ;

    sget-object v2, LX/HjN;->AD_REQUEST_FAILED:LX/HjN;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/HjN;->getAdErrorWrapper(Ljava/lang/String;)LX/Hjr;

    move-result-object v0

    invoke-static {v1, v0}, LX/HkJ;->a$redex0(LX/HkJ;LX/Hjr;)V

    goto :goto_4

    :cond_7
    :try_start_1
    const-string v1, "SDKIDFA"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v1, "attributionId"

    invoke-interface {v3, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    const-string v1, "attributionId"

    const-string v4, ""

    invoke-interface {v3, v1, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, LX/Hk1;->m:Ljava/lang/String;

    :cond_8
    const-string v1, "advertisingId"

    invoke-interface {v3, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    const-string v1, "advertisingId"

    const-string v4, ""

    invoke-interface {v3, v1, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, LX/Hk1;->n:Ljava/lang/String;

    const-string v1, "limitAdTracking"

    sget-boolean v4, LX/Hk1;->o:Z

    invoke-interface {v3, v1, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, LX/Hk1;->o:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_9
    :try_start_2
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1}, LX/Hko;->a(Landroid/content/ContentResolver;)LX/Hkn;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v1

    :goto_5
    if-eqz v1, :cond_a

    :try_start_3
    iget-object v4, v1, LX/Hkn;->a:Ljava/lang/String;

    if-eqz v4, :cond_a

    sput-object v4, LX/Hk1;->m:Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    :cond_a
    :try_start_4
    invoke-static {v0, v1}, LX/Hk6;->a(Landroid/content/Context;LX/Hkn;)LX/Hk6;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    move-result-object v1

    :goto_6
    if-eqz v1, :cond_b

    :try_start_5
    iget-object v2, v1, LX/Hk6;->b:Ljava/lang/String;

    move-object v2, v2

    iget-boolean v4, v1, LX/Hk6;->c:Z

    move v1, v4

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    if-eqz v2, :cond_b

    sput-object v2, LX/Hk1;->n:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    sput-boolean v1, LX/Hk1;->o:Z

    :cond_b
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "attributionId"

    sget-object v3, LX/Hk1;->m:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v2, "advertisingId"

    sget-object v3, LX/Hk1;->n:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v2, "limitAdTracking"

    sget-boolean v3, LX/Hk1;->o:Z

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    goto/16 :goto_0

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    :catch_2
    move-exception v1

    :try_start_6
    const-string v4, "Error retrieving attribution id from fb4a"

    invoke-static {v1, v4}, LX/Hkg;->a(Ljava/lang/Throwable;Ljava/lang/String;)LX/Hkg;

    move-result-object v1

    invoke-static {v1}, LX/Hkh;->a(LX/Hkg;)V

    move-object v1, v2

    goto :goto_5

    :catch_3
    move-exception v1

    const-string v4, "Error retrieving advertising id from Google Play Services"

    invoke-static {v1, v4}, LX/Hkg;->a(Ljava/lang/Throwable;Ljava/lang/String;)LX/Hkg;

    move-result-object v1

    invoke-static {v1}, LX/Hkh;->a(LX/Hkg;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    move-object v1, v2

    goto :goto_6

    :cond_c
    const-string v3, "1"

    goto/16 :goto_1

    :cond_d
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    sget-object v5, LX/Hjb;->a:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_e
    :goto_7
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_f

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/Hjd;

    iget-object v8, v5, LX/Hjd;->g:LX/HkF;

    if-ne v8, v3, :cond_e

    iget-object v5, v5, LX/Hjd;->f:LX/Hjc;

    invoke-virtual {v5}, LX/Hjc;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v6, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_7

    :cond_f
    const-string v5, ","

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_8
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_10

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_8

    :cond_10
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    if-lez v7, :cond_11

    const/4 v7, 0x0

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->length()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {v8, v7, v9}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v7

    :goto_9
    move-object v5, v7

    sget-object v6, LX/Hjb;->b:Ljava/util/Map;

    invoke-interface {v6, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_3

    :cond_11
    const-string v7, ""

    goto :goto_9
.end method
