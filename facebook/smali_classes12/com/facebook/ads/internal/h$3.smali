.class public final Lcom/facebook/ads/internal/h$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/HkC;


# direct methods
.method public constructor <init>(LX/HkC;)V
    .locals 0

    iput-object p1, p0, Lcom/facebook/ads/internal/h$3;->a:LX/HkC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    :try_start_0
    iget-object v0, p0, Lcom/facebook/ads/internal/h$3;->a:LX/HkC;

    const-wide/16 v7, 0x2710

    iget-object v2, v0, LX/HkC;->l:LX/Hjx;

    invoke-virtual {v2}, LX/Hjx;->c()LX/Hju;

    move-result-object v3

    if-nez v3, :cond_0

    iget-object v1, v0, LX/HkC;->a:LX/HjO;

    sget-object v2, LX/HjN;->NO_FILL:LX/HjN;

    const-string v3, ""

    invoke-virtual {v2, v3}, LX/HjN;->getAdErrorWrapper(Ljava/lang/String;)LX/Hjr;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/HjO;->a(LX/Hjr;)V

    invoke-static {v0}, LX/HkC;->f(LX/HkC;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :cond_0
    iget-object v4, v3, LX/Hju;->b:Ljava/lang/String;

    iget-object v1, v2, LX/Hjx;->c:LX/Hjy;

    move-object v1, v1

    iget-object v5, v1, LX/Hjy;->a:LX/HkF;

    move-object v1, v5

    invoke-static {v4, v1}, LX/Hjb;->a(Ljava/lang/String;LX/HkF;)LX/HjT;

    move-result-object v1

    if-nez v1, :cond_1

    sget-object v1, LX/HkC;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Adapter does not exist: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v0}, LX/HkC;->d$redex0(LX/HkC;)V

    goto :goto_0

    :cond_1
    invoke-static {v0}, LX/HkC;->c(LX/HkC;)LX/HkF;

    move-result-object v4

    invoke-interface {v1}, LX/HjT;->a()LX/HkF;

    move-result-object v5

    if-eq v4, v5, :cond_2

    iget-object v1, v0, LX/HkC;->a:LX/HjO;

    sget-object v2, LX/HjN;->INTERNAL_ERROR:LX/HjN;

    const-string v3, ""

    invoke-virtual {v2, v3}, LX/HjN;->getAdErrorWrapper(Ljava/lang/String;)LX/Hjr;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/HjO;->a(LX/Hjr;)V

    goto :goto_0

    :cond_2
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iget-object v5, v2, LX/Hjx;->c:LX/Hjy;

    move-object v2, v5

    const-string v5, "data"

    iget-object v3, v3, LX/Hju;->c:Lorg/json/JSONObject;

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "definition"

    invoke-interface {v4, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, v0, LX/HkC;->m:LX/Hk0;

    if-nez v2, :cond_3

    sget-object v2, LX/HjN;->UNKNOWN_ERROR:LX/HjN;

    const-string v3, "environment is empty"

    invoke-virtual {v2, v3}, LX/HjN;->getAdErrorWrapper(Ljava/lang/String;)LX/Hjr;

    move-result-object v2

    iget-object v3, v0, LX/HkC;->a:LX/HjO;

    invoke-virtual {v3, v2}, LX/HjO;->a(LX/Hjr;)V

    :cond_3
    sget-object v2, LX/Hk7;->a:[I

    invoke-interface {v1}, LX/HjT;->a()LX/HkF;

    move-result-object v3

    invoke-virtual {v3}, LX/HkF;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    sget-object v1, LX/HkC;->b:Ljava/lang/String;

    const-string v2, "attempt unexpected adapter type"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_0
    check-cast v1, LX/HjU;

    new-instance v2, Lcom/facebook/ads/internal/h$4;

    invoke-direct {v2, v0, v1}, Lcom/facebook/ads/internal/h$4;-><init>(LX/HkC;LX/HjU;)V

    iget-object v3, v0, LX/HkC;->d:Landroid/os/Handler;

    const v5, -0x752a0f2c

    invoke-static {v3, v2, v7, v8, v5}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    iget-object v3, v0, LX/HkC;->c:Landroid/content/Context;

    new-instance v5, LX/Hk8;

    invoke-direct {v5, v0, v2}, LX/Hk8;-><init>(LX/HkC;Ljava/lang/Runnable;)V

    invoke-virtual {v1, v3, v5, v4}, LX/HjU;->a(Landroid/content/Context;LX/Hk8;Ljava/util/Map;)V

    goto/16 :goto_0

    :pswitch_1
    check-cast v1, LX/HjV;

    new-instance v2, Lcom/facebook/ads/internal/h$6;

    invoke-direct {v2, v0, v1}, Lcom/facebook/ads/internal/h$6;-><init>(LX/HkC;LX/HjV;)V

    iget-object v3, v0, LX/HkC;->d:Landroid/os/Handler;

    const v5, 0x1e3987af

    invoke-static {v3, v2, v7, v8, v5}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    iget-object v3, v0, LX/HkC;->c:Landroid/content/Context;

    new-instance v5, LX/Hk9;

    invoke-direct {v5, v0, v2}, LX/Hk9;-><init>(LX/HkC;Ljava/lang/Runnable;)V

    invoke-virtual {v1, v3, v5, v4}, LX/HjV;->a(Landroid/content/Context;LX/Hk9;Ljava/util/Map;)V

    goto/16 :goto_0

    :pswitch_2
    check-cast v1, LX/Hjj;

    new-instance v2, Lcom/facebook/ads/internal/h$8;

    invoke-direct {v2, v0, v1}, Lcom/facebook/ads/internal/h$8;-><init>(LX/HkC;LX/Hjj;)V

    iget-object v3, v0, LX/HkC;->d:Landroid/os/Handler;

    const v5, -0x6da2aba5

    invoke-static {v3, v2, v7, v8, v5}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    iget-object v3, v0, LX/HkC;->c:Landroid/content/Context;

    new-instance v5, LX/HkA;

    invoke-direct {v5, v0, v2}, LX/HkA;-><init>(LX/HkC;Ljava/lang/Runnable;)V

    invoke-virtual {v1, v3, v5, v4}, LX/Hjj;->a(Landroid/content/Context;LX/Hjq;Ljava/util/Map;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
