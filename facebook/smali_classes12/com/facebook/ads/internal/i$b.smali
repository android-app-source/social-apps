.class public final Lcom/facebook/ads/internal/i$b;
.super Lcom/facebook/ads/internal/util/t;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/ads/internal/util/t",
        "<",
        "LX/HkE;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/HkE;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/facebook/ads/internal/util/t;-><init>(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    invoke-virtual {p0}, Lcom/facebook/ads/internal/util/t;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HkE;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, v0, LX/HkE;->a:Landroid/content/Context;

    invoke-static {v1}, LX/Hko;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, LX/HkE;->a()V

    goto :goto_0

    :cond_1
    iget-object v1, v0, LX/HkE;->i:Landroid/os/Handler;

    iget-object v0, v0, LX/HkE;->j:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1388

    const v4, 0x346f0086    # 2.22588E-7f

    invoke-static {v1, v0, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_0
.end method
