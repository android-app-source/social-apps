.class public Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/DiscoveryCardPhotoProtileFetcher;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final d:Lcom/facebook/common/callercontext/CallerContext;

.field private static volatile e:Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/DiscoveryCardPhotoProtileFetcher;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0rq;",
            ">;"
        }
    .end annotation
.end field

.field public volatile c:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0se;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2677890
    const-class v0, Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/DiscoveryCardPhotoProtileFetcher;

    const-string v1, "people_discovery"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/DiscoveryCardPhotoProtileFetcher;->d:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2677891
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2677892
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/DiscoveryCardPhotoProtileFetcher;
    .locals 6

    .prologue
    .line 2677893
    sget-object v0, Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/DiscoveryCardPhotoProtileFetcher;->e:Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/DiscoveryCardPhotoProtileFetcher;

    if-nez v0, :cond_1

    .line 2677894
    const-class v1, Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/DiscoveryCardPhotoProtileFetcher;

    monitor-enter v1

    .line 2677895
    :try_start_0
    sget-object v0, Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/DiscoveryCardPhotoProtileFetcher;->e:Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/DiscoveryCardPhotoProtileFetcher;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2677896
    if-eqz v2, :cond_0

    .line 2677897
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2677898
    new-instance v3, Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/DiscoveryCardPhotoProtileFetcher;

    invoke-direct {v3}, Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/DiscoveryCardPhotoProtileFetcher;-><init>()V

    .line 2677899
    const/16 v4, 0xafd

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0xb24

    invoke-static {v0, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 p0, 0xf27

    invoke-static {v0, p0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    .line 2677900
    iput-object v4, v3, Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/DiscoveryCardPhotoProtileFetcher;->a:LX/0Or;

    iput-object v5, v3, Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/DiscoveryCardPhotoProtileFetcher;->b:LX/0Or;

    iput-object p0, v3, Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/DiscoveryCardPhotoProtileFetcher;->c:LX/0Or;

    .line 2677901
    move-object v0, v3

    .line 2677902
    sput-object v0, Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/DiscoveryCardPhotoProtileFetcher;->e:Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/DiscoveryCardPhotoProtileFetcher;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2677903
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2677904
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2677905
    :cond_1
    sget-object v0, Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/DiscoveryCardPhotoProtileFetcher;->e:Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/DiscoveryCardPhotoProtileFetcher;

    return-object v0

    .line 2677906
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2677907
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
