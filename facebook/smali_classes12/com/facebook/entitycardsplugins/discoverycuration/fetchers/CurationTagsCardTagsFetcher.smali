.class public Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/CurationTagsCardTagsFetcher;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:LX/0tX;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2677836
    const-class v0, Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/CurationTagsCardTagsFetcher;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/CurationTagsCardTagsFetcher;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0tX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2677837
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2677838
    iput-object p1, p0, Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/CurationTagsCardTagsFetcher;->b:LX/0tX;

    .line 2677839
    return-void
.end method

.method public static b(Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;)Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;
    .locals 2

    .prologue
    .line 2677840
    new-instance v0, LX/FTB;

    invoke-direct {v0}, LX/FTB;-><init>()V

    .line 2677841
    iput-object p0, v0, LX/FTB;->b:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    .line 2677842
    move-object v0, v0

    .line 2677843
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2677844
    iput-object v1, v0, LX/FTB;->a:LX/0Px;

    .line 2677845
    move-object v0, v0

    .line 2677846
    invoke-virtual {v0}, LX/FTB;->a()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$TagsFragmentModel$SelectedAndSuggestedTagsModel;

    move-result-object v0

    return-object v0
.end method
