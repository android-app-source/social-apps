.class public Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/CurationTagsCardFetcher;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static volatile c:Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/CurationTagsCardFetcher;


# instance fields
.field private final b:LX/0tX;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2677826
    const-class v0, Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/CurationTagsCardFetcher;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/CurationTagsCardFetcher;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0tX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2677823
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2677824
    iput-object p1, p0, Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/CurationTagsCardFetcher;->b:LX/0tX;

    .line 2677825
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/CurationTagsCardFetcher;
    .locals 4

    .prologue
    .line 2677801
    sget-object v0, Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/CurationTagsCardFetcher;->c:Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/CurationTagsCardFetcher;

    if-nez v0, :cond_1

    .line 2677802
    const-class v1, Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/CurationTagsCardFetcher;

    monitor-enter v1

    .line 2677803
    :try_start_0
    sget-object v0, Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/CurationTagsCardFetcher;->c:Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/CurationTagsCardFetcher;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2677804
    if-eqz v2, :cond_0

    .line 2677805
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2677806
    new-instance p0, Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/CurationTagsCardFetcher;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-direct {p0, v3}, Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/CurationTagsCardFetcher;-><init>(LX/0tX;)V

    .line 2677807
    move-object v0, p0

    .line 2677808
    sput-object v0, Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/CurationTagsCardFetcher;->c:Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/CurationTagsCardFetcher;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2677809
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2677810
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2677811
    :cond_1
    sget-object v0, Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/CurationTagsCardFetcher;->c:Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/CurationTagsCardFetcher;

    return-object v0

    .line 2677812
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2677813
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;I)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;",
            "I)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2677814
    invoke-static {}, LX/FSs;->b()LX/FSp;

    move-result-object v0

    const-string v1, "bucket_item_id"

    invoke-virtual {p1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "tags_page_size"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/FSp;

    .line 2677815
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    sget-object v1, Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/CurationTagsCardFetcher;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 2677816
    iput-object v1, v0, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2677817
    move-object v0, v0

    .line 2677818
    sget-object v1, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    .line 2677819
    iget-object v1, p0, Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/CurationTagsCardFetcher;->b:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2677820
    new-instance v1, LX/JIL;

    invoke-direct {v1, p0, p1}, LX/JIL;-><init>(Lcom/facebook/entitycardsplugins/discoverycuration/fetchers/CurationTagsCardFetcher;Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel;)V

    .line 2677821
    sget-object v2, LX/131;->INSTANCE:LX/131;

    move-object v2, v2

    .line 2677822
    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
