.class public Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryContextRowComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static h:LX/0Xm;


# instance fields
.field public final b:Landroid/content/res/Resources;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Epm;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/1vg;

.field public f:Landroid/graphics/PorterDuffColorFilter;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Landroid/graphics/PorterDuffColorFilter;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2677541
    const-class v0, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryContextRowComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryContextRowComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;LX/0Or;LX/0Or;LX/1vg;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;",
            "LX/0Or",
            "<",
            "LX/Epm;",
            ">;",
            "LX/1vg;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2677542
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2677543
    iput-object p1, p0, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryContextRowComponentSpec;->b:Landroid/content/res/Resources;

    .line 2677544
    iput-object p2, p0, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryContextRowComponentSpec;->c:LX/0Or;

    .line 2677545
    iput-object p3, p0, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryContextRowComponentSpec;->d:LX/0Or;

    .line 2677546
    iput-object p4, p0, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryContextRowComponentSpec;->e:LX/1vg;

    .line 2677547
    return-void
.end method

.method public static a(Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryContextRowComponentSpec;I)Landroid/graphics/PorterDuffColorFilter;
    .locals 3
    .param p0    # Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryContextRowComponentSpec;
        .annotation build Landroid/support/annotation/ColorRes;
        .end annotation
    .end param

    .prologue
    .line 2677548
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryContextRowComponentSpec;->b:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 2677549
    new-instance v1, Landroid/graphics/PorterDuffColorFilter;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v0, v2}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    return-object v1
.end method

.method public static a(LX/0QB;)Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryContextRowComponentSpec;
    .locals 7

    .prologue
    .line 2677550
    const-class v1, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryContextRowComponentSpec;

    monitor-enter v1

    .line 2677551
    :try_start_0
    sget-object v0, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryContextRowComponentSpec;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2677552
    sput-object v2, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryContextRowComponentSpec;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2677553
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2677554
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2677555
    new-instance v5, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryContextRowComponentSpec;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    const/16 v4, 0x509

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v4, 0x1ae0

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v4

    check-cast v4, LX/1vg;

    invoke-direct {v5, v3, v6, p0, v4}, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryContextRowComponentSpec;-><init>(Landroid/content/res/Resources;LX/0Or;LX/0Or;LX/1vg;)V

    .line 2677556
    move-object v0, v5

    .line 2677557
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2677558
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryContextRowComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2677559
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2677560
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
