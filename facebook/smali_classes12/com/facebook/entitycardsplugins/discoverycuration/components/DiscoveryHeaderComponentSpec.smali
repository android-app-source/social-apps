.class public Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryHeaderComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static final b:Landroid/graphics/PointF;

.field private static e:LX/0Xm;


# instance fields
.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/JIy;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/high16 v2, 0x3f000000    # 0.5f

    .line 2677728
    const-class v0, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryHeaderComponentSpec;

    const-string v1, "people_discovery"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryHeaderComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 2677729
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v2, v2}, Landroid/graphics/PointF;-><init>(FF)V

    sput-object v0, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryHeaderComponentSpec;->b:Landroid/graphics/PointF;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0Or;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;",
            "LX/0Or",
            "<",
            "LX/JIy;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2677672
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2677673
    iput-object p1, p0, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryHeaderComponentSpec;->c:LX/0Or;

    .line 2677674
    iput-object p2, p0, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryHeaderComponentSpec;->d:LX/0Or;

    .line 2677675
    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;LX/Emj;)LX/1Ai;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/Emj;",
            ")",
            "LX/1Ai",
            "<",
            "Lcom/facebook/imagepipeline/image/ImageInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2677727
    new-instance v0, LX/JIG;

    invoke-direct {v0, p2, p0, p1}, LX/JIG;-><init>(LX/Emj;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(LX/1De;LX/FSx;LX/1Ad;LX/Emj;)LX/1Di;
    .locals 3

    .prologue
    .line 2677709
    invoke-interface {p1}, LX/FSx;->C()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, LX/FSx;->C()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, LX/FSx;->C()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2677710
    :goto_0
    invoke-virtual {p2}, LX/1Ad;->o()LX/1Ad;

    move-result-object v1

    sget-object v2, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryHeaderComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    const-string v1, "ec_config_profile_picture"

    invoke-interface {p1}, LX/FSx;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, p3}, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryHeaderComponentSpec;->a(Ljava/lang/String;Ljava/lang/String;LX/Emj;)LX/1Ai;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 2677711
    const/4 v1, 0x0

    .line 2677712
    new-instance v2, LX/JII;

    invoke-direct {v2}, LX/JII;-><init>()V

    .line 2677713
    sget-object p1, LX/JIJ;->b:LX/0Zi;

    invoke-virtual {p1}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/JIH;

    .line 2677714
    if-nez p1, :cond_0

    .line 2677715
    new-instance p1, LX/JIH;

    invoke-direct {p1}, LX/JIH;-><init>()V

    .line 2677716
    :cond_0
    invoke-static {p1, p0, v1, v1, v2}, LX/JIH;->a$redex0(LX/JIH;LX/1De;IILX/JII;)V

    .line 2677717
    move-object v2, p1

    .line 2677718
    move-object v1, v2

    .line 2677719
    move-object v1, v1

    .line 2677720
    iget-object v2, v1, LX/JIH;->a:LX/JII;

    iput-object v0, v2, LX/JII;->a:LX/1aZ;

    .line 2677721
    iget-object v2, v1, LX/JIH;->d:Ljava/util/BitSet;

    const/4 p1, 0x0

    invoke-virtual {v2, p1}, Ljava/util/BitSet;->set(I)V

    .line 2677722
    move-object v0, v1

    .line 2677723
    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    .line 2677724
    const v1, -0x10053195

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v1

    move-object v1, v1

    .line 2677725
    invoke-interface {v0, v1}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v0, v1}, LX/1Di;->b(I)LX/1Di;

    move-result-object v0

    const/4 v1, 0x1

    const/16 v2, -0x46

    invoke-interface {v0, v1, v2}, LX/1Di;->d(II)LX/1Di;

    move-result-object v0

    const v1, 0x7f0b253a

    invoke-interface {v0, v1}, LX/1Di;->i(I)LX/1Di;

    move-result-object v0

    const v1, 0x7f0b253a

    invoke-interface {v0, v1}, LX/1Di;->q(I)LX/1Di;

    move-result-object v0

    return-object v0

    .line 2677726
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/1De;LX/FSx;Landroid/graphics/drawable/Drawable;LX/1Ad;LX/Emj;)LX/1Di;
    .locals 8
    .param p2    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x0

    .line 2677687
    invoke-interface {p1}, LX/FSx;->t()LX/FSv;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, LX/FSx;->t()LX/FSv;

    move-result-object v0

    invoke-interface {v0}, LX/FSv;->c()LX/FSu;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, LX/FSx;->t()LX/FSv;

    move-result-object v0

    invoke-interface {v0}, LX/FSv;->c()LX/FSu;

    move-result-object v0

    invoke-interface {v0}, LX/FSu;->b()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2677688
    invoke-interface {p1}, LX/FSx;->t()LX/FSv;

    move-result-object v0

    invoke-interface {v0}, LX/FSv;->c()LX/FSu;

    move-result-object v0

    invoke-interface {v0}, LX/FSu;->b()LX/1Fb;

    move-result-object v0

    .line 2677689
    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v1

    .line 2677690
    invoke-interface {p1}, LX/FSx;->t()LX/FSv;

    move-result-object v0

    invoke-interface {v0}, LX/FSv;->a()LX/1f8;

    move-result-object v2

    .line 2677691
    if-eqz v2, :cond_0

    new-instance v0, Landroid/graphics/PointF;

    invoke-interface {v2}, LX/1f8;->a()D

    move-result-wide v4

    double-to-float v3, v4

    invoke-interface {v2}, LX/1f8;->b()D

    move-result-wide v4

    double-to-float v2, v4

    invoke-direct {v0, v3, v2}, Landroid/graphics/PointF;-><init>(FF)V

    :goto_0
    move-object v7, v0

    move-object v0, v1

    move-object v1, v7

    .line 2677692
    :goto_1
    invoke-virtual {p3}, LX/1Ad;->o()LX/1Ad;

    move-result-object v2

    sget-object v3, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryHeaderComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v0

    const-string v2, "ec_config_cover_photo"

    invoke-interface {p1}, LX/FSx;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, p4}, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryHeaderComponentSpec;->a(Ljava/lang/String;Ljava/lang/String;LX/Emj;)LX/1Ai;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 2677693
    invoke-virtual {p0}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b253f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2677694
    new-instance v3, LX/4Ab;

    invoke-direct {v3}, LX/4Ab;-><init>()V

    sget-object v4, LX/4Aa;->OVERLAY_COLOR:LX/4Aa;

    .line 2677695
    iput-object v4, v3, LX/4Ab;->a:LX/4Aa;

    .line 2677696
    move-object v3, v3

    .line 2677697
    const v4, 0x7f0a00a3

    invoke-static {p0, v4}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v4

    invoke-virtual {v3, v4}, LX/4Ab;->a(I)LX/4Ab;

    move-result-object v3

    int-to-float v4, v2

    int-to-float v2, v2

    invoke-virtual {v3, v4, v2, v6, v6}, LX/4Ab;->a(FFFF)LX/4Ab;

    move-result-object v2

    .line 2677698
    invoke-static {p0}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v3

    invoke-virtual {v3, v0}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/1up;->b(Landroid/graphics/PointF;)LX/1up;

    move-result-object v0

    sget-object v3, LX/1Up;->h:LX/1Up;

    invoke-virtual {v0, v3}, LX/1up;->b(LX/1Up;)LX/1up;

    move-result-object v0

    sget-object v3, LX/1Up;->h:LX/1Up;

    invoke-virtual {v0, v3}, LX/1up;->a(LX/1Up;)LX/1up;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/1up;->a(LX/4Ab;)LX/1up;

    move-result-object v0

    .line 2677699
    if-eqz p2, :cond_2

    .line 2677700
    invoke-static {}, LX/1n3;->b()LX/1n5;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/1n5;->a(Landroid/graphics/drawable/Drawable;)LX/1n5;

    move-result-object v2

    invoke-virtual {v2}, LX/1n6;->b()LX/1dc;

    move-result-object v2

    .line 2677701
    invoke-virtual {v0, v2}, LX/1up;->a(LX/1dc;)LX/1up;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/1up;->a(Landroid/graphics/PointF;)LX/1up;

    .line 2677702
    :goto_2
    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    .line 2677703
    const v1, -0x10055456

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v1

    move-object v1, v1

    .line 2677704
    invoke-interface {v0, v1}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v0

    const/16 v1, 0x8c

    invoke-interface {v0, v1}, LX/1Di;->r(I)LX/1Di;

    move-result-object v0

    return-object v0

    .line 2677705
    :cond_0
    sget-object v0, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryHeaderComponentSpec;->b:Landroid/graphics/PointF;

    goto/16 :goto_0

    .line 2677706
    :cond_1
    const/4 v1, 0x0

    .line 2677707
    sget-object v0, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryHeaderComponentSpec;->b:Landroid/graphics/PointF;

    move-object v7, v0

    move-object v0, v1

    move-object v1, v7

    goto/16 :goto_1

    .line 2677708
    :cond_2
    const v1, 0x7f021888

    invoke-virtual {v0, v1}, LX/1up;->h(I)LX/1up;

    move-result-object v1

    sget-object v2, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryHeaderComponentSpec;->b:Landroid/graphics/PointF;

    invoke-virtual {v1, v2}, LX/1up;->a(Landroid/graphics/PointF;)LX/1up;

    goto :goto_2
.end method

.method public static a(LX/0QB;)Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryHeaderComponentSpec;
    .locals 5

    .prologue
    .line 2677676
    const-class v1, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryHeaderComponentSpec;

    monitor-enter v1

    .line 2677677
    :try_start_0
    sget-object v0, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryHeaderComponentSpec;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2677678
    sput-object v2, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryHeaderComponentSpec;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2677679
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2677680
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2677681
    new-instance v3, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryHeaderComponentSpec;

    const/16 v4, 0x509

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 p0, 0x1ad2

    invoke-static {v0, p0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, v4, p0}, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryHeaderComponentSpec;-><init>(LX/0Or;LX/0Or;)V

    .line 2677682
    move-object v0, v3

    .line 2677683
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2677684
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/entitycardsplugins/discoverycuration/components/DiscoveryHeaderComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2677685
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2677686
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
