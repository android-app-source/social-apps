.class public Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2678868
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2678869
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 2678870
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 2678871
    new-instance v1, LX/JJ2;

    invoke-direct {v1, p0}, LX/JJ2;-><init>(Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 2678872
    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2678873
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2678874
    const v0, 0x7f0303c0

    invoke-virtual {p0, v0}, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadActivity;->setContentView(I)V

    .line 2678875
    invoke-direct {p0}, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadActivity;->a()V

    .line 2678876
    if-nez p1, :cond_0

    .line 2678877
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d0bcb

    new-instance v2, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;

    invoke-direct {v2}, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;-><init>()V

    invoke-virtual {v0, v1, v2}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2678878
    :cond_0
    return-void
.end method
