.class public final Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFetcher$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/0Vd;

.field public final synthetic c:LX/JJ5;


# direct methods
.method public constructor <init>(LX/JJ5;Ljava/lang/String;LX/0Vd;)V
    .locals 0

    .prologue
    .line 2678905
    iput-object p1, p0, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFetcher$1;->c:LX/JJ5;

    iput-object p2, p0, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFetcher$1;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFetcher$1;->b:LX/0Vd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 2678906
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFetcher$1;->c:LX/JJ5;

    iget-object v1, p0, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFetcher$1;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFetcher$1;->b:LX/0Vd;

    .line 2678907
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2678908
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/0Vd;->onSuccess(Ljava/lang/Object;)V

    .line 2678909
    :goto_0
    return-void

    .line 2678910
    :cond_0
    new-instance v3, LX/FSq;

    invoke-direct {v3}, LX/FSq;-><init>()V

    move-object v3, v3

    .line 2678911
    const-string v4, "query"

    invoke-virtual {v3, v4, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2678912
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    sget-object v4, LX/0zS;->c:LX/0zS;

    invoke-virtual {v3, v4}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v3

    .line 2678913
    iget-object v4, v0, LX/JJ5;->b:LX/1Ck;

    const-string v5, "fetch_suggestions_task_key"

    iget-object p0, v0, LX/JJ5;->a:LX/0tX;

    invoke-virtual {p0, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    invoke-virtual {v4, v5, v3, v2}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0
.end method
