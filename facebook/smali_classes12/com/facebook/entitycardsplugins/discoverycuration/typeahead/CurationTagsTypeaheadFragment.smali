.class public Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:Landroid/view/inputmethod/InputMethodManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/JJ5;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/JIO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/JJ4;

.field public g:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

.field public h:Landroid/widget/ProgressBar;

.field public i:LX/0Vd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Vd",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$CurationTagsTypeaheadQueryModel;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2678999
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    move-object v1, p1

    check-cast v1, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;

    invoke-static {p0}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v2

    check-cast v2, Landroid/view/inputmethod/InputMethodManager;

    new-instance p1, LX/JJ5;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    invoke-static {p0}, LX/43r;->a(LX/0QB;)Landroid/os/Handler;

    move-result-object v5

    check-cast v5, Landroid/os/Handler;

    invoke-direct {p1, v3, v4, v5}, LX/JJ5;-><init>(LX/0tX;LX/1Ck;Landroid/os/Handler;)V

    move-object v3, p1

    check-cast v3, LX/JJ5;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/Executor;

    const-class v5, LX/JIO;

    invoke-interface {p0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/JIO;

    invoke-static {p0}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    iput-object v2, v1, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;->a:Landroid/view/inputmethod/InputMethodManager;

    iput-object v3, v1, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;->b:LX/JJ5;

    iput-object v4, v1, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;->c:Ljava/util/concurrent/Executor;

    iput-object v5, v1, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;->d:LX/JIO;

    iput-object p0, v1, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;->e:Ljava/lang/String;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;LX/JJD;)Z
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 2679000
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;->g:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getPickedTokens()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8QK;

    .line 2679001
    check-cast v0, LX/JJD;

    .line 2679002
    iget-wide v10, v0, LX/JJD;->f:J

    move-wide v6, v10

    .line 2679003
    iget-wide v10, p1, LX/JJD;->f:J

    move-wide v8, v10

    .line 2679004
    cmp-long v0, v6, v8

    if-nez v0, :cond_0

    .line 2679005
    const/4 v0, 0x1

    .line 2679006
    :goto_1
    return v0

    .line 2679007
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 2679008
    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2679009
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2679010
    const-class v0, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;

    invoke-static {v0, p0}, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2679011
    new-instance v0, LX/JJ4;

    .line 2679012
    new-instance v1, LX/JJ6;

    invoke-direct {v1, p0}, LX/JJ6;-><init>(Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;)V

    move-object v1, v1

    .line 2679013
    invoke-direct {v0, v1}, LX/JJ4;-><init>(Landroid/view/View$OnClickListener;)V

    iput-object v0, p0, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;->f:LX/JJ4;

    .line 2679014
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x58518d8c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2679015
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2679016
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0d00bc

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, LX/0h5;

    .line 2679017
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v2

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const p1, 0x7f083a72

    invoke-virtual {v4, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2679018
    iput-object v4, v2, LX/108;->g:Ljava/lang/String;

    .line 2679019
    move-object v2, v2

    .line 2679020
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const p1, 0x7f083a72

    invoke-virtual {v4, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2679021
    iput-object v4, v2, LX/108;->j:Ljava/lang/String;

    .line 2679022
    move-object v2, v2

    .line 2679023
    invoke-virtual {v2}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v2

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 2679024
    invoke-interface {v1, v2}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2679025
    new-instance v2, LX/JJ7;

    invoke-direct {v2, p0}, LX/JJ7;-><init>(Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;)V

    invoke-interface {v1, v2}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 2679026
    const/16 v1, 0x2b

    const v2, -0x7f126a5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x490d9263

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2679027
    const v1, 0x7f0303c1

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2679028
    const v2, 0x7f0d0bcc

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    iput-object v2, p0, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;->g:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    .line 2679029
    iget-object v2, p0, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;->g:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setEnabled(Z)V

    .line 2679030
    iget-object v2, p0, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;->g:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    new-instance v3, LX/JJ8;

    invoke-direct {v3, p0}, LX/JJ8;-><init>(Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;)V

    invoke-virtual {v2, v3}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2679031
    iget-object v2, p0, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;->g:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    new-instance v3, LX/JJ9;

    invoke-direct {v3, p0}, LX/JJ9;-><init>(Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;)V

    invoke-virtual {v2, v3}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2679032
    iget-object v2, p0, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;->g:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    new-instance v3, LX/JJA;

    invoke-direct {v3, p0}, LX/JJA;-><init>(Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;)V

    .line 2679033
    iput-object v3, v2, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->M:LX/8uw;

    .line 2679034
    const v2, 0x7f0d0bcd

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2679035
    new-instance v3, LX/1P0;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {v3, p1}, LX/1P0;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2679036
    iget-object v3, p0, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;->f:LX/JJ4;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2679037
    const v2, 0x7f0d05b0

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    iput-object v2, p0, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;->h:Landroid/widget/ProgressBar;

    .line 2679038
    const/16 v2, 0x2b

    const v3, 0x4c0c845b    # 3.6835692E7f

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x4cc3e210

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2679039
    iget-object v1, p0, Lcom/facebook/entitycardsplugins/discoverycuration/typeahead/CurationTagsTypeaheadFragment;->i:LX/0Vd;

    invoke-virtual {v1}, LX/0Vd;->dispose()V

    .line 2679040
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2679041
    const/16 v1, 0x2b

    const v2, -0x43d3861b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
