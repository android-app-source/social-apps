.class public Lcom/facebook/entitycardsplugins/discoverycuration/views/BioCurationCardView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/2eZ;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:Landroid/graphics/Paint;

.field public c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public d:Lcom/facebook/resources/ui/FbTextView;

.field public e:Lcom/facebook/resources/ui/FbTextView;

.field public f:Lcom/facebook/resources/ui/FbTextView;

.field public g:Lcom/facebook/widget/CustomFrameLayout;

.field public h:Lcom/facebook/fig/button/FigButton;

.field public i:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2679078
    const-class v0, Lcom/facebook/entitycardsplugins/discoverycuration/views/BioCurationCardView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/entitycardsplugins/discoverycuration/views/BioCurationCardView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2679066
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2679067
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/entitycardsplugins/discoverycuration/views/BioCurationCardView;->b:Landroid/graphics/Paint;

    .line 2679068
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/discoverycuration/views/BioCurationCardView;->b:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/discoverycuration/views/BioCurationCardView;->getContext()Landroid/content/Context;

    move-result-object v1

    const p1, 0x7f0a009e

    invoke-static {v1, p1}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2679069
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/discoverycuration/views/BioCurationCardView;->b:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/discoverycuration/views/BioCurationCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const p1, 0x7f0b0033

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2679070
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/discoverycuration/views/BioCurationCardView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030198

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2679071
    const v0, 0x7f0d0503

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/entitycardsplugins/discoverycuration/views/BioCurationCardView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2679072
    const v0, 0x7f0d02c4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/entitycardsplugins/discoverycuration/views/BioCurationCardView;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 2679073
    const v0, 0x7f0d02c3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/entitycardsplugins/discoverycuration/views/BioCurationCardView;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 2679074
    const v0, 0x7f0d06f1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/entitycardsplugins/discoverycuration/views/BioCurationCardView;->f:Lcom/facebook/resources/ui/FbTextView;

    .line 2679075
    const v0, 0x7f0d06f2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomFrameLayout;

    iput-object v0, p0, Lcom/facebook/entitycardsplugins/discoverycuration/views/BioCurationCardView;->g:Lcom/facebook/widget/CustomFrameLayout;

    .line 2679076
    const v0, 0x7f0d06f3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    iput-object v0, p0, Lcom/facebook/entitycardsplugins/discoverycuration/views/BioCurationCardView;->h:Lcom/facebook/fig/button/FigButton;

    .line 2679077
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2679079
    iget-boolean v0, p0, Lcom/facebook/entitycardsplugins/discoverycuration/views/BioCurationCardView;->i:Z

    return v0
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 2679062
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 2679063
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/discoverycuration/views/BioCurationCardView;->getHeight()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/entitycardsplugins/discoverycuration/views/BioCurationCardView;->g:Lcom/facebook/widget/CustomFrameLayout;

    invoke-virtual {v1}, Lcom/facebook/widget/CustomFrameLayout;->getHeight()I

    move-result v1

    sub-int/2addr v0, v1

    .line 2679064
    const/4 v1, 0x0

    int-to-float v2, v0

    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/discoverycuration/views/BioCurationCardView;->getWidth()I

    move-result v3

    int-to-float v3, v3

    int-to-float v4, v0

    iget-object v5, p0, Lcom/facebook/entitycardsplugins/discoverycuration/views/BioCurationCardView;->b:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2679065
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x7d38eec0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2679058
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onAttachedToWindow()V

    .line 2679059
    const/4 v1, 0x1

    .line 2679060
    iput-boolean v1, p0, Lcom/facebook/entitycardsplugins/discoverycuration/views/BioCurationCardView;->i:Z

    .line 2679061
    const/16 v1, 0x2d

    const v2, -0x713de2f9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x5d5dde58

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2679054
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onDetachedFromWindow()V

    .line 2679055
    const/4 v1, 0x0

    .line 2679056
    iput-boolean v1, p0, Lcom/facebook/entitycardsplugins/discoverycuration/views/BioCurationCardView;->i:Z

    .line 2679057
    const/16 v1, 0x2d

    const v2, -0x6394bbd4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
