.class public Lcom/facebook/entitycardsplugins/discoverycuration/views/DiscoveryPhotoProtileView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2679152
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2679153
    invoke-direct {p0}, Lcom/facebook/entitycardsplugins/discoverycuration/views/DiscoveryPhotoProtileView;->a()V

    .line 2679154
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2679155
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2679156
    invoke-direct {p0}, Lcom/facebook/entitycardsplugins/discoverycuration/views/DiscoveryPhotoProtileView;->a()V

    .line 2679157
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2679158
    const v0, 0x7f030437

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2679159
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/entitycardsplugins/discoverycuration/views/DiscoveryPhotoProtileView;->a:Landroid/graphics/Paint;

    .line 2679160
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/discoverycuration/views/DiscoveryPhotoProtileView;->a:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/discoverycuration/views/DiscoveryPhotoProtileView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00e9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2679161
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/discoverycuration/views/DiscoveryPhotoProtileView;->a:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/discoverycuration/views/DiscoveryPhotoProtileView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0034

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2679162
    return-void
.end method


# virtual methods
.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2679163
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 2679164
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/discoverycuration/views/DiscoveryPhotoProtileView;->getRight()I

    move-result v0

    int-to-float v3, v0

    iget-object v5, p0, Lcom/facebook/entitycardsplugins/discoverycuration/views/DiscoveryPhotoProtileView;->a:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    move v4, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2679165
    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/discoverycuration/views/DiscoveryPhotoProtileView;->getBottom()I

    move-result v0

    int-to-float v2, v0

    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/discoverycuration/views/DiscoveryPhotoProtileView;->getRight()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/facebook/entitycardsplugins/discoverycuration/views/DiscoveryPhotoProtileView;->getBottom()I

    move-result v0

    int-to-float v4, v0

    iget-object v5, p0, Lcom/facebook/entitycardsplugins/discoverycuration/views/DiscoveryPhotoProtileView;->a:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2679166
    return-void
.end method
