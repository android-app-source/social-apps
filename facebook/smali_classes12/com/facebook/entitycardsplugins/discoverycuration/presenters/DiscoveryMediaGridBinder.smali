.class public Lcom/facebook/entitycardsplugins/discoverycuration/presenters/DiscoveryMediaGridBinder;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static volatile c:Lcom/facebook/entitycardsplugins/discoverycuration/presenters/DiscoveryMediaGridBinder;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/G0r;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2678743
    const-class v0, Lcom/facebook/entitycardsplugins/discoverycuration/presenters/DiscoveryMediaGridBinder;

    const-string v1, "people_discovery"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/entitycardsplugins/discoverycuration/presenters/DiscoveryMediaGridBinder;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2678744
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2678745
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/entitycardsplugins/discoverycuration/presenters/DiscoveryMediaGridBinder;
    .locals 4

    .prologue
    .line 2678746
    sget-object v0, Lcom/facebook/entitycardsplugins/discoverycuration/presenters/DiscoveryMediaGridBinder;->c:Lcom/facebook/entitycardsplugins/discoverycuration/presenters/DiscoveryMediaGridBinder;

    if-nez v0, :cond_1

    .line 2678747
    const-class v1, Lcom/facebook/entitycardsplugins/discoverycuration/presenters/DiscoveryMediaGridBinder;

    monitor-enter v1

    .line 2678748
    :try_start_0
    sget-object v0, Lcom/facebook/entitycardsplugins/discoverycuration/presenters/DiscoveryMediaGridBinder;->c:Lcom/facebook/entitycardsplugins/discoverycuration/presenters/DiscoveryMediaGridBinder;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2678749
    if-eqz v2, :cond_0

    .line 2678750
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2678751
    new-instance v3, Lcom/facebook/entitycardsplugins/discoverycuration/presenters/DiscoveryMediaGridBinder;

    invoke-direct {v3}, Lcom/facebook/entitycardsplugins/discoverycuration/presenters/DiscoveryMediaGridBinder;-><init>()V

    .line 2678752
    const/16 p0, 0x369f

    invoke-static {v0, p0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    .line 2678753
    iput-object p0, v3, Lcom/facebook/entitycardsplugins/discoverycuration/presenters/DiscoveryMediaGridBinder;->a:LX/0Or;

    .line 2678754
    move-object v0, v3

    .line 2678755
    sput-object v0, Lcom/facebook/entitycardsplugins/discoverycuration/presenters/DiscoveryMediaGridBinder;->c:Lcom/facebook/entitycardsplugins/discoverycuration/presenters/DiscoveryMediaGridBinder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2678756
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2678757
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2678758
    :cond_1
    sget-object v0, Lcom/facebook/entitycardsplugins/discoverycuration/presenters/DiscoveryMediaGridBinder;->c:Lcom/facebook/entitycardsplugins/discoverycuration/presenters/DiscoveryMediaGridBinder;

    return-object v0

    .line 2678759
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2678760
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0Px;LX/Fx7;LX/JIl;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0Px",
            "<+TT;>;",
            "LX/Fx7",
            "<TT;>;",
            "LX/JIl;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2678761
    invoke-static {}, LX/6WP;->newBuilder()LX/6WO;

    move-result-object v0

    const/4 v1, 0x6

    .line 2678762
    iput v1, v0, LX/6WO;->c:I

    .line 2678763
    move-object v2, v0

    .line 2678764
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 2678765
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2678766
    invoke-interface {p2, p1, v1}, LX/Fx7;->b(LX/0Px;I)LX/5vn;

    move-result-object v4

    .line 2678767
    iget-object v0, p0, Lcom/facebook/entitycardsplugins/discoverycuration/presenters/DiscoveryMediaGridBinder;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G0r;

    .line 2678768
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v5

    invoke-virtual {v0, v5, v4, v2}, LX/G0r;->a(ILX/5vn;LX/6WO;)V

    .line 2678769
    invoke-interface {p2, p1, v1}, LX/Fx7;->a(LX/0Px;I)LX/5vo;

    move-result-object v5

    invoke-virtual {v0, v5, v4}, LX/G0r;->a(LX/1U8;LX/5vn;)LX/1bf;

    move-result-object v4

    .line 2678770
    sget-object v5, Lcom/facebook/entitycardsplugins/discoverycuration/presenters/DiscoveryMediaGridBinder;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v5, v4}, LX/G0r;->a(Lcom/facebook/common/callercontext/CallerContext;LX/1bf;)LX/1aZ;

    move-result-object v0

    .line 2678771
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2678772
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2678773
    :cond_0
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v2}, LX/6WO;->a()LX/6WP;

    move-result-object v1

    invoke-interface {p3, v0, v1}, LX/JIl;->a(LX/0Px;LX/6WP;)V

    .line 2678774
    return-void
.end method
