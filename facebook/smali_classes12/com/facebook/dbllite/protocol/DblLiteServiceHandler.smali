.class public Lcom/facebook/dbllite/protocol/DblLiteServiceHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/1qM;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field public final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final b:LX/2Vu;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/11H;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JHg;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/JHi;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2Vu;LX/0Or;LX/11H;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/2Vu;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            "LX/0Ot",
            "<",
            "LX/JHg;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/JHi;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2677020
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2677021
    iput-object p1, p0, Lcom/facebook/dbllite/protocol/DblLiteServiceHandler;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2677022
    iput-object p2, p0, Lcom/facebook/dbllite/protocol/DblLiteServiceHandler;->b:LX/2Vu;

    .line 2677023
    iput-object p3, p0, Lcom/facebook/dbllite/protocol/DblLiteServiceHandler;->c:LX/0Or;

    .line 2677024
    iput-object p4, p0, Lcom/facebook/dbllite/protocol/DblLiteServiceHandler;->d:LX/11H;

    .line 2677025
    iput-object p5, p0, Lcom/facebook/dbllite/protocol/DblLiteServiceHandler;->e:LX/0Ot;

    .line 2677026
    iput-object p6, p0, Lcom/facebook/dbllite/protocol/DblLiteServiceHandler;->f:LX/0Ot;

    .line 2677027
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/dbllite/protocol/DblLiteServiceHandler;
    .locals 10

    .prologue
    .line 2676985
    const-class v1, Lcom/facebook/dbllite/protocol/DblLiteServiceHandler;

    monitor-enter v1

    .line 2676986
    :try_start_0
    sget-object v0, Lcom/facebook/dbllite/protocol/DblLiteServiceHandler;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2676987
    sput-object v2, Lcom/facebook/dbllite/protocol/DblLiteServiceHandler;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2676988
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2676989
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2676990
    new-instance v3, Lcom/facebook/dbllite/protocol/DblLiteServiceHandler;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/2Vu;->a(LX/0QB;)LX/2Vu;

    move-result-object v5

    check-cast v5, LX/2Vu;

    const/16 v6, 0x19e

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {v0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v7

    check-cast v7, LX/11H;

    const/16 v8, 0x1a71

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x1a72

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-direct/range {v3 .. v9}, Lcom/facebook/dbllite/protocol/DblLiteServiceHandler;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2Vu;LX/0Or;LX/11H;LX/0Ot;LX/0Ot;)V

    .line 2676991
    move-object v0, v3

    .line 2676992
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2676993
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/dbllite/protocol/DblLiteServiceHandler;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2676994
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2676995
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 6

    .prologue
    .line 2676996
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 2676997
    const-string v1, "get_dbl_nonce"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2676998
    iget-object v0, p0, Lcom/facebook/dbllite/protocol/DblLiteServiceHandler;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/26p;->f:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2676999
    iget-object v2, p0, Lcom/facebook/dbllite/protocol/DblLiteServiceHandler;->d:LX/11H;

    iget-object v0, p0, Lcom/facebook/dbllite/protocol/DblLiteServiceHandler;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0e6;

    new-instance v3, LX/JHf;

    invoke-direct {v3, v1}, LX/JHf;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string p1, "DblLiteServiceHandler"

    invoke-static {v1, p1}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    invoke-virtual {v2, v0, v3, v1}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2677000
    iget-object v2, p0, Lcom/facebook/dbllite/protocol/DblLiteServiceHandler;->b:LX/2Vu;

    new-instance v3, Lcom/facebook/dbllite/data/DblLiteCredentials;

    iget-object v1, p0, Lcom/facebook/dbllite/protocol/DblLiteServiceHandler;->c:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2677001
    iget-object p1, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v1, p1

    .line 2677002
    const/4 p1, 0x0

    invoke-direct {v3, v1, v0, p1}, Lcom/facebook/dbllite/data/DblLiteCredentials;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v2, v3}, LX/2Vu;->a(Lcom/facebook/dbllite/data/DblLiteCredentials;)V

    .line 2677003
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2677004
    move-object v0, v0

    .line 2677005
    :goto_0
    return-object v0

    .line 2677006
    :cond_0
    const-string v1, "expire_dbl_nonce"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2677007
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2677008
    const-string v1, "account_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2677009
    iget-object v0, p0, Lcom/facebook/dbllite/protocol/DblLiteServiceHandler;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/26p;->f:LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2677010
    iget-object v0, p0, Lcom/facebook/dbllite/protocol/DblLiteServiceHandler;->b:LX/2Vu;

    invoke-virtual {v0, v1}, LX/2Vu;->a(Ljava/lang/String;)Lcom/facebook/dbllite/data/DblLiteCredentials;

    move-result-object v3

    .line 2677011
    if-nez v3, :cond_2

    .line 2677012
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2677013
    :goto_1
    move-object v0, v0

    .line 2677014
    goto :goto_0

    .line 2677015
    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unhandled operation type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2677016
    :cond_2
    iget-object v4, p0, Lcom/facebook/dbllite/protocol/DblLiteServiceHandler;->d:LX/11H;

    iget-object v0, p0, Lcom/facebook/dbllite/protocol/DblLiteServiceHandler;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0e6;

    new-instance v5, LX/JHh;

    iget-object v3, v3, Lcom/facebook/dbllite/data/DblLiteCredentials;->nonce:Ljava/lang/String;

    invoke-direct {v5, v2, v1, v3}, LX/JHh;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "DblLiteServiceHandler"

    invoke-static {v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    invoke-virtual {v4, v0, v5, v2}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    .line 2677017
    iget-object v0, p0, Lcom/facebook/dbllite/protocol/DblLiteServiceHandler;->b:LX/2Vu;

    invoke-virtual {v0, v1}, LX/2Vu;->b(Ljava/lang/String;)V

    .line 2677018
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2677019
    goto :goto_1
.end method
