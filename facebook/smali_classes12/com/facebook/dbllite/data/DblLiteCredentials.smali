.class public Lcom/facebook/dbllite/data/DblLiteCredentials;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/dbllite/data/DblLiteCredentialsDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/dbllite/data/DblLiteCredentials;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final isPinSet:Ljava/lang/Boolean;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_pin_set"
    .end annotation
.end field

.field public final nonce:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "nonce"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final userId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "uid"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2676936
    const-class v0, Lcom/facebook/dbllite/data/DblLiteCredentialsDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2676937
    new-instance v0, LX/JHd;

    invoke-direct {v0}, LX/JHd;-><init>()V

    sput-object v0, Lcom/facebook/dbllite/data/DblLiteCredentials;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2676938
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2676939
    iput-object v0, p0, Lcom/facebook/dbllite/data/DblLiteCredentials;->userId:Ljava/lang/String;

    .line 2676940
    iput-object v0, p0, Lcom/facebook/dbllite/data/DblLiteCredentials;->nonce:Ljava/lang/String;

    .line 2676941
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/dbllite/data/DblLiteCredentials;->isPinSet:Ljava/lang/Boolean;

    .line 2676942
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2676943
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2676944
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/dbllite/data/DblLiteCredentials;->userId:Ljava/lang/String;

    .line 2676945
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/dbllite/data/DblLiteCredentials;->nonce:Ljava/lang/String;

    .line 2676946
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/dbllite/data/DblLiteCredentials;->isPinSet:Ljava/lang/Boolean;

    .line 2676947
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 2676948
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2676949
    iput-object p1, p0, Lcom/facebook/dbllite/data/DblLiteCredentials;->userId:Ljava/lang/String;

    .line 2676950
    iput-object p2, p0, Lcom/facebook/dbllite/data/DblLiteCredentials;->nonce:Ljava/lang/String;

    .line 2676951
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/dbllite/data/DblLiteCredentials;->isPinSet:Ljava/lang/Boolean;

    .line 2676952
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2676953
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2676954
    iget-object v0, p0, Lcom/facebook/dbllite/data/DblLiteCredentials;->userId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2676955
    iget-object v0, p0, Lcom/facebook/dbllite/data/DblLiteCredentials;->nonce:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2676956
    iget-object v0, p0, Lcom/facebook/dbllite/data/DblLiteCredentials;->isPinSet:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2676957
    return-void
.end method
