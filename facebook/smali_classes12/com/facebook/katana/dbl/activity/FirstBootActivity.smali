.class public Lcom/facebook/katana/dbl/activity/FirstBootActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0l6;


# instance fields
.field public A:Landroid/widget/Button;

.field public B:Landroid/widget/RadioButton;

.field public C:Landroid/widget/RadioButton;

.field private D:I

.field public p:LX/GvB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/2NO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private u:Landroid/view/ViewGroup;

.field private v:Landroid/view/ViewGroup;

.field private w:Landroid/view/ViewGroup;

.field private x:Landroid/view/ViewGroup;

.field private y:Landroid/widget/Button;

.field private z:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2587953
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2587954
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FirstBootActivity;->v:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2587955
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FirstBootActivity;->y:Landroid/widget/Button;

    new-instance v1, LX/IYZ;

    invoke-direct {v1, p0}, LX/IYZ;-><init>(Lcom/facebook/katana/dbl/activity/FirstBootActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2587956
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FirstBootActivity;->z:Landroid/widget/Button;

    new-instance v1, LX/IYa;

    invoke-direct {v1, p0}, LX/IYa;-><init>(Lcom/facebook/katana/dbl/activity/FirstBootActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2587957
    return-void
.end method

.method private static a(Lcom/facebook/katana/dbl/activity/FirstBootActivity;LX/GvB;Lcom/facebook/content/SecureContextHelper;LX/2NO;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Zb;)V
    .locals 0

    .prologue
    .line 2587958
    iput-object p1, p0, Lcom/facebook/katana/dbl/activity/FirstBootActivity;->p:LX/GvB;

    iput-object p2, p0, Lcom/facebook/katana/dbl/activity/FirstBootActivity;->q:Lcom/facebook/content/SecureContextHelper;

    iput-object p3, p0, Lcom/facebook/katana/dbl/activity/FirstBootActivity;->r:LX/2NO;

    iput-object p4, p0, Lcom/facebook/katana/dbl/activity/FirstBootActivity;->s:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p5, p0, Lcom/facebook/katana/dbl/activity/FirstBootActivity;->t:LX/0Zb;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 6

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/katana/dbl/activity/FirstBootActivity;

    invoke-static {v5}, LX/GvB;->b(LX/0QB;)LX/GvB;

    move-result-object v1

    check-cast v1, LX/GvB;

    invoke-static {v5}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v5}, LX/2NO;->b(LX/0QB;)LX/2NO;

    move-result-object v3

    check-cast v3, LX/2NO;

    invoke-static {v5}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v5}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v5

    check-cast v5, LX/0Zb;

    invoke-static/range {v0 .. v5}, Lcom/facebook/katana/dbl/activity/FirstBootActivity;->a(Lcom/facebook/katana/dbl/activity/FirstBootActivity;LX/GvB;Lcom/facebook/content/SecureContextHelper;LX/2NO;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Zb;)V

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 2587928
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FirstBootActivity;->u:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2587929
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FirstBootActivity;->w:Landroid/view/ViewGroup;

    new-instance v1, LX/IYb;

    invoke-direct {v1, p0}, LX/IYb;-><init>(Lcom/facebook/katana/dbl/activity/FirstBootActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2587930
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FirstBootActivity;->x:Landroid/view/ViewGroup;

    new-instance v1, LX/IYc;

    invoke-direct {v1, p0}, LX/IYc;-><init>(Lcom/facebook/katana/dbl/activity/FirstBootActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2587931
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FirstBootActivity;->A:Landroid/widget/Button;

    new-instance v1, LX/IYd;

    invoke-direct {v1, p0}, LX/IYd;-><init>(Lcom/facebook/katana/dbl/activity/FirstBootActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2587932
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FirstBootActivity;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2587933
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FirstBootActivity;->w:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->performClick()Z

    .line 2587934
    :cond_0
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2587943
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "first_boot_activity_waterfall"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "login"

    .line 2587944
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2587945
    move-object v0, v0

    .line 2587946
    const-string v1, "state"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "exp_group"

    iget v2, p0, Lcom/facebook/katana/dbl/activity/FirstBootActivity;->D:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2587947
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FirstBootActivity;->t:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2587948
    return-void
.end method

.method public static l(Lcom/facebook/katana/dbl/activity/FirstBootActivity;)V
    .locals 1

    .prologue
    .line 2587949
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FirstBootActivity;->n()V

    .line 2587950
    const-string v0, "OPEN_LOGIN"

    invoke-direct {p0, v0}, Lcom/facebook/katana/dbl/activity/FirstBootActivity;->b(Ljava/lang/String;)V

    .line 2587951
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FirstBootActivity;->p:LX/GvB;

    invoke-virtual {v0, p0}, LX/GvB;->a(Landroid/app/Activity;)V

    .line 2587952
    return-void
.end method

.method public static m(Lcom/facebook/katana/dbl/activity/FirstBootActivity;)V
    .locals 2

    .prologue
    .line 2587937
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FirstBootActivity;->n()V

    .line 2587938
    const-string v0, "OPEN_REG"

    invoke-direct {p0, v0}, Lcom/facebook/katana/dbl/activity/FirstBootActivity;->b(Ljava/lang/String;)V

    .line 2587939
    const-string v0, "FIRST_BOOT_ACTIVITY"

    invoke-static {p0, v0}, Lcom/facebook/registration/activity/AccountRegistrationActivity;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2587940
    iget-object v1, p0, Lcom/facebook/katana/dbl/activity/FirstBootActivity;->q:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2587941
    invoke-virtual {p0}, Lcom/facebook/katana/dbl/activity/FirstBootActivity;->finish()V

    .line 2587942
    return-void
.end method

.method private n()V
    .locals 3

    .prologue
    .line 2587935
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FirstBootActivity;->s:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/1CA;->A:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2587936
    return-void
.end method

.method private o()Z
    .locals 1

    .prologue
    .line 2587927
    iget v0, p0, Lcom/facebook/katana/dbl/activity/FirstBootActivity;->D:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private p()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2587926
    iget v1, p0, Lcom/facebook/katana/dbl/activity/FirstBootActivity;->D:I

    if-eq v1, v0, :cond_0

    iget v1, p0, Lcom/facebook/katana/dbl/activity/FirstBootActivity;->D:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private q()Z
    .locals 2

    .prologue
    .line 2587925
    iget v0, p0, Lcom/facebook/katana/dbl/activity/FirstBootActivity;->D:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2587904
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2587905
    invoke-static {p0, p0}, Lcom/facebook/katana/dbl/activity/FirstBootActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2587906
    const v0, 0x7f03066d

    invoke-virtual {p0, v0}, Lcom/facebook/katana/dbl/activity/FirstBootActivity;->setContentView(I)V

    .line 2587907
    const v0, 0x7f0d11a8

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FirstBootActivity;->u:Landroid/view/ViewGroup;

    .line 2587908
    const v0, 0x7f0d11ab

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FirstBootActivity;->v:Landroid/view/ViewGroup;

    .line 2587909
    const v0, 0x7f0d11ac

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FirstBootActivity;->w:Landroid/view/ViewGroup;

    .line 2587910
    const v0, 0x7f0d11ae

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FirstBootActivity;->x:Landroid/view/ViewGroup;

    .line 2587911
    const v0, 0x7f0d11a9

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FirstBootActivity;->y:Landroid/widget/Button;

    .line 2587912
    const v0, 0x7f0d11aa

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FirstBootActivity;->z:Landroid/widget/Button;

    .line 2587913
    const v0, 0x7f0d06a5

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FirstBootActivity;->A:Landroid/widget/Button;

    .line 2587914
    const v0, 0x7f0d11ad

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FirstBootActivity;->B:Landroid/widget/RadioButton;

    .line 2587915
    const v0, 0x7f0d11af

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/facebook/katana/dbl/activity/FirstBootActivity;->C:Landroid/widget/RadioButton;

    .line 2587916
    iget-object v0, p0, Lcom/facebook/katana/dbl/activity/FirstBootActivity;->r:LX/2NO;

    sget-object v1, LX/27f;->FB4A_LOGIN_FIRST_BOOT_PAGE:LX/27f;

    invoke-virtual {v0, v1}, LX/2NO;->a(LX/27f;)I

    move-result v0

    iput v0, p0, Lcom/facebook/katana/dbl/activity/FirstBootActivity;->D:I

    .line 2587917
    const-string v0, "ACTIVITY_CREATED"

    invoke-direct {p0, v0}, Lcom/facebook/katana/dbl/activity/FirstBootActivity;->b(Ljava/lang/String;)V

    .line 2587918
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FirstBootActivity;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2587919
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FirstBootActivity;->a()V

    .line 2587920
    :goto_0
    return-void

    .line 2587921
    :cond_0
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FirstBootActivity;->p()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2587922
    invoke-direct {p0}, Lcom/facebook/katana/dbl/activity/FirstBootActivity;->b()V

    goto :goto_0

    .line 2587923
    :cond_1
    const-string v0, "NOT_PART_OF_EXPERIMENT"

    invoke-direct {p0, v0}, Lcom/facebook/katana/dbl/activity/FirstBootActivity;->b(Ljava/lang/String;)V

    .line 2587924
    invoke-static {p0}, Lcom/facebook/katana/dbl/activity/FirstBootActivity;->l(Lcom/facebook/katana/dbl/activity/FirstBootActivity;)V

    goto :goto_0
.end method
