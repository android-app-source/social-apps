.class public final Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2549826
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2549827
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2549878
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2549879
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 2549865
    if-nez p1, :cond_0

    .line 2549866
    :goto_0
    return v0

    .line 2549867
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2549868
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2549869
    :sswitch_0
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 2549870
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 2549871
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 2549872
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2549873
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2549874
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2549875
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 2549876
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2549877
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x71534e05 -> :sswitch_1
        -0x11c6519c -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2549864
    new-instance v0, Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 2549861
    sparse-switch p0, :sswitch_data_0

    .line 2549862
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2549863
    :sswitch_0
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x71534e05 -> :sswitch_0
        -0x11c6519c -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2549860
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 2549858
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$DraculaImplementation;->b(I)V

    .line 2549859
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2549853
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2549854
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2549855
    :cond_0
    iput-object p1, p0, Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 2549856
    iput p2, p0, Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$DraculaImplementation;->b:I

    .line 2549857
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2549880
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2549852
    new-instance v0, Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2549849
    iget v0, p0, LX/1vt;->c:I

    .line 2549850
    move v0, v0

    .line 2549851
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2549846
    iget v0, p0, LX/1vt;->c:I

    .line 2549847
    move v0, v0

    .line 2549848
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2549843
    iget v0, p0, LX/1vt;->b:I

    .line 2549844
    move v0, v0

    .line 2549845
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2549840
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2549841
    move-object v0, v0

    .line 2549842
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2549831
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2549832
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2549833
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2549834
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2549835
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2549836
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2549837
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2549838
    invoke-static {v3, v9, v2}, Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2549839
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2549828
    iget v0, p0, LX/1vt;->c:I

    .line 2549829
    move v0, v0

    .line 2549830
    return v0
.end method
