.class public final Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FetchFriendingSuggestionQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x25c90798
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FetchFriendingSuggestionQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FetchFriendingSuggestionQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FetchFriendingSuggestionQueryModel$FriendSuggestionsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2549995
    const-class v0, Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FetchFriendingSuggestionQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2549996
    const-class v0, Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FetchFriendingSuggestionQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2550003
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2550004
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2549997
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2549998
    invoke-virtual {p0}, Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FetchFriendingSuggestionQueryModel;->a()Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FetchFriendingSuggestionQueryModel$FriendSuggestionsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2549999
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2550000
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2550001
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2550002
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2549986
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2549987
    invoke-virtual {p0}, Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FetchFriendingSuggestionQueryModel;->a()Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FetchFriendingSuggestionQueryModel$FriendSuggestionsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2549988
    invoke-virtual {p0}, Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FetchFriendingSuggestionQueryModel;->a()Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FetchFriendingSuggestionQueryModel$FriendSuggestionsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FetchFriendingSuggestionQueryModel$FriendSuggestionsModel;

    .line 2549989
    invoke-virtual {p0}, Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FetchFriendingSuggestionQueryModel;->a()Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FetchFriendingSuggestionQueryModel$FriendSuggestionsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2549990
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FetchFriendingSuggestionQueryModel;

    .line 2549991
    iput-object v0, v1, Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FetchFriendingSuggestionQueryModel;->e:Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FetchFriendingSuggestionQueryModel$FriendSuggestionsModel;

    .line 2549992
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2549993
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2549994
    new-instance v0, LX/ICq;

    invoke-direct {v0, p1}, LX/ICq;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FetchFriendingSuggestionQueryModel$FriendSuggestionsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getFriendSuggestions"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2549984
    iget-object v0, p0, Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FetchFriendingSuggestionQueryModel;->e:Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FetchFriendingSuggestionQueryModel$FriendSuggestionsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FetchFriendingSuggestionQueryModel$FriendSuggestionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FetchFriendingSuggestionQueryModel$FriendSuggestionsModel;

    iput-object v0, p0, Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FetchFriendingSuggestionQueryModel;->e:Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FetchFriendingSuggestionQueryModel$FriendSuggestionsModel;

    .line 2549985
    iget-object v0, p0, Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FetchFriendingSuggestionQueryModel;->e:Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FetchFriendingSuggestionQueryModel$FriendSuggestionsModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2549982
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2549983
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2549981
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2549978
    new-instance v0, Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FetchFriendingSuggestionQueryModel;

    invoke-direct {v0}, Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FetchFriendingSuggestionQueryModel;-><init>()V

    .line 2549979
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2549980
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2549977
    const v0, -0x70fbc188

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2549976
    const v0, 0x285feb

    return v0
.end method
