.class public Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/17W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/2dj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/2do;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/ICx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Lcom/facebook/friending/suggestion/model/FriendingSuggestionListLoader;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

.field public l:I

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Lcom/facebook/widget/listview/BetterListView;

.field private q:Lcom/facebook/resources/ui/FbTextView;

.field public r:LX/ICw;

.field public s:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public t:Z

.field private u:LX/ICi;

.field private final v:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2549720
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2549721
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->t:Z

    .line 2549722
    new-instance v0, LX/ICb;

    invoke-direct {v0, p0}, LX/ICb;-><init>(Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;)V

    iput-object v0, p0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->v:Landroid/view/View$OnClickListener;

    .line 2549723
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    move-object v1, p1

    check-cast v1, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v2

    check-cast v2, LX/0SG;

    invoke-static {p0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v3

    check-cast v3, LX/17W;

    invoke-static {p0}, LX/2dj;->b(LX/0QB;)LX/2dj;

    move-result-object v4

    check-cast v4, LX/2dj;

    invoke-static {p0}, LX/2do;->a(LX/0QB;)LX/2do;

    move-result-object v5

    check-cast v5, LX/2do;

    const-class v6, LX/ICx;

    invoke-interface {p0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/ICx;

    new-instance v9, Lcom/facebook/friending/suggestion/model/FriendingSuggestionListLoader;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/ExecutorService;

    const/16 v8, 0xafd

    invoke-static {p0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v8, 0xafc

    invoke-static {p0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-direct {v9, v7, v10, p1, v8}, Lcom/facebook/friending/suggestion/model/FriendingSuggestionListLoader;-><init>(Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/0Ot;LX/0ad;)V

    move-object v7, v9

    check-cast v7, Lcom/facebook/friending/suggestion/model/FriendingSuggestionListLoader;

    invoke-static {p0}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v10

    check-cast v10, LX/1Ck;

    invoke-static {p0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object p0

    check-cast p0, LX/0kL;

    iput-object v2, v1, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->a:LX/0SG;

    iput-object v3, v1, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->b:LX/17W;

    iput-object v4, v1, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->c:LX/2dj;

    iput-object v5, v1, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->d:LX/2do;

    iput-object v6, v1, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->e:LX/ICx;

    iput-object v7, v1, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->f:Lcom/facebook/friending/suggestion/model/FriendingSuggestionListLoader;

    iput-object v8, v1, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->g:Ljava/lang/String;

    iput-object v9, v1, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->h:LX/0ad;

    iput-object v10, v1, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->i:LX/1Ck;

    iput-object p0, v1, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->j:LX/0kL;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;I)V
    .locals 10

    .prologue
    const/4 v4, 0x0

    .line 2549714
    iget-object v0, p0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->r:LX/ICw;

    invoke-virtual {v0, p1}, LX/ICw;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, LX/ICk;

    .line 2549715
    sget-object v0, LX/0ax;->bE:Ljava/lang/String;

    invoke-virtual {v3}, LX/ICk;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 2549716
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2549717
    invoke-virtual {v3}, LX/ICk;->a()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3}, LX/ICk;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3}, LX/ICk;->b()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-static/range {v0 .. v5}, LX/5ve;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/1f8;)V

    .line 2549718
    iget-object v1, p0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->b:LX/17W;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v6, v0}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 2549719
    return-void
.end method

.method public static b(Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;)V
    .locals 4

    .prologue
    .line 2549711
    iget-object v0, p0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2549712
    :goto_0
    return-void

    .line 2549713
    :cond_0
    iget-object v0, p0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->i:LX/1Ck;

    sget-object v1, LX/ICj;->FETCH_FRIENDING_SUGGESTIONS:LX/ICj;

    new-instance v2, LX/ICf;

    invoke-direct {v2, p0}, LX/ICf;-><init>(Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;)V

    new-instance v3, LX/ICh;

    invoke-direct {v3, p0}, LX/ICh;-><init>(Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 2549724
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2549725
    const-class v0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;

    invoke-static {v0, p0}, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2549726
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2549727
    const-string v1, "com.facebook.katana.profile.id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->n:Ljava/lang/String;

    .line 2549728
    const-string v1, "profile_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->o:Ljava/lang/String;

    .line 2549729
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "A"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->n:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->a:LX/0SG;

    invoke-interface {v7}, LX/0SG;->a()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/03l;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, v5

    .line 2549730
    iput-object v0, p0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->m:Ljava/lang/String;

    .line 2549731
    iget-object v0, p0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->h:LX/0ad;

    sget-short v1, LX/2ez;->n:S

    invoke-interface {v0, v1, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->t:Z

    .line 2549732
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 2549733
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x7f010205

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 2549734
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->l:I

    .line 2549735
    iget-boolean v0, p0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->t:Z

    if-nez v0, :cond_0

    .line 2549736
    new-instance v0, LX/ICi;

    invoke-direct {v0, p0}, LX/ICi;-><init>(Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;)V

    iput-object v0, p0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->u:LX/ICi;

    .line 2549737
    iget-object v0, p0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->d:LX/2do;

    iget-object v1, p0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->u:LX/ICi;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2549738
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x45763202

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2549710
    const v1, 0x7f0306ff

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x6825923a

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x48258aab

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2549703
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2549704
    iget-object v1, p0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->i:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2549705
    iget-boolean v1, p0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->t:Z

    if-eqz v1, :cond_0

    .line 2549706
    iget-object v1, p0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->f:Lcom/facebook/friending/suggestion/model/FriendingSuggestionListLoader;

    .line 2549707
    iget-object v2, v1, Lcom/facebook/friending/suggestion/model/FriendingSuggestionListLoader;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1My;

    invoke-virtual {v2}, LX/1My;->a()V

    .line 2549708
    :goto_0
    const v1, -0x2a60a296

    invoke-static {v1, v0}, LX/02F;->f(II)V

    return-void

    .line 2549709
    :cond_0
    iget-object v1, p0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->d:LX/2do;

    iget-object v2, p0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->u:LX/ICi;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    goto :goto_0
.end method

.method public final onResume()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v3, 0x1

    const/16 v0, 0x2a

    const v1, 0x628f609f

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2549696
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2549697
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2549698
    if-eqz v0, :cond_0

    .line 2549699
    invoke-interface {v0, v3}, LX/1ZF;->k_(Z)V

    .line 2549700
    const v2, 0x7f083815

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->o:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v2, v3}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2549701
    invoke-interface {v0, v2}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2549702
    :cond_0
    const/16 v0, 0x2b

    const v2, 0xd9de193

    invoke-static {v6, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2549670
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2549671
    const v0, 0x7f0d12bb

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->q:Lcom/facebook/resources/ui/FbTextView;

    .line 2549672
    iget-object v0, p0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->q:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f083816

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->o:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v1, v2}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2549673
    new-instance v1, LX/0zw;

    const v0, 0x7f0d12b9

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->s:LX/0zw;

    .line 2549674
    const v0, 0x7f0d12bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->p:Lcom/facebook/widget/listview/BetterListView;

    .line 2549675
    iget-object v0, p0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->p:Lcom/facebook/widget/listview/BetterListView;

    .line 2549676
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 2549677
    const v2, 0x1020004

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setEmptyView(Landroid/view/View;)V

    .line 2549678
    iget-object v0, p0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->e:LX/ICx;

    iget-object v1, p0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->v:Landroid/view/View$OnClickListener;

    .line 2549679
    new-instance v3, LX/ICw;

    .line 2549680
    new-instance p1, LX/ICy;

    const-class v2, Landroid/content/Context;

    invoke-interface {v0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-direct {p1, v2}, LX/ICy;-><init>(Landroid/content/Context;)V

    .line 2549681
    move-object v2, p1

    .line 2549682
    check-cast v2, LX/ICy;

    invoke-direct {v3, v2, v1}, LX/ICw;-><init>(LX/ICy;Landroid/view/View$OnClickListener;)V

    .line 2549683
    move-object v0, v3

    .line 2549684
    iput-object v0, p0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->r:LX/ICw;

    .line 2549685
    iget-object v0, p0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->p:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->r:LX/ICw;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2549686
    iget-object v0, p0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->p:Lcom/facebook/widget/listview/BetterListView;

    new-instance v1, LX/ICc;

    invoke-direct {v1, p0}, LX/ICc;-><init>(Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->a(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2549687
    iget-object v0, p0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->p:Lcom/facebook/widget/listview/BetterListView;

    new-instance v1, LX/ICd;

    invoke-direct {v1, p0}, LX/ICd;-><init>(Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2549688
    new-instance v0, LX/4a7;

    invoke-direct {v0}, LX/4a7;-><init>()V

    .line 2549689
    iput-boolean v4, v0, LX/4a7;->c:Z

    .line 2549690
    move-object v0, v0

    .line 2549691
    iput-boolean v5, v0, LX/4a7;->b:Z

    .line 2549692
    move-object v0, v0

    .line 2549693
    invoke-virtual {v0}, LX/4a7;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 2549694
    invoke-static {p0}, Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;->b(Lcom/facebook/friending/suggestion/fragment/FriendingSuggestionFragment;)V

    .line 2549695
    return-void
.end method
