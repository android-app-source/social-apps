.class public Lcom/facebook/friending/suggestion/model/FriendingSuggestionListLoader;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public final a:Ljava/util/concurrent/ExecutorService;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1My;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/lang/String;

.field public e:I


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/0Ot;LX/0ad;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1My;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2549779
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2549780
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/friending/suggestion/model/FriendingSuggestionListLoader;->e:I

    .line 2549781
    iput-object p1, p0, Lcom/facebook/friending/suggestion/model/FriendingSuggestionListLoader;->a:Ljava/util/concurrent/ExecutorService;

    .line 2549782
    iput-object p2, p0, Lcom/facebook/friending/suggestion/model/FriendingSuggestionListLoader;->b:LX/0Ot;

    .line 2549783
    iput-object p3, p0, Lcom/facebook/friending/suggestion/model/FriendingSuggestionListLoader;->c:LX/0Ot;

    .line 2549784
    const/4 p1, 0x0

    .line 2549785
    sget-short v0, LX/AqT;->c:S

    invoke-interface {p4, v0, p1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_0

    sget-short v0, LX/AqT;->d:S

    invoke-interface {p4, v0, p1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2549786
    :cond_0
    const-string v0, "friend_suggestion_pymk"

    .line 2549787
    :goto_0
    move-object v0, v0

    .line 2549788
    iput-object v0, p0, Lcom/facebook/friending/suggestion/model/FriendingSuggestionListLoader;->d:Ljava/lang/String;

    .line 2549789
    return-void

    :cond_1
    const-string v0, "friend_suggestion_connectivity"

    goto :goto_0
.end method

.method public static b(Lcom/facebook/friending/suggestion/model/FriendingSuggestionListLoader;Ljava/lang/String;ILjava/lang/String;)LX/0zO;
    .locals 7
    .param p2    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            ")",
            "LX/0zO",
            "<",
            "Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FetchFriendingSuggestionQueryModel;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 2549797
    new-instance v0, LX/ICo;

    invoke-direct {v0}, LX/ICo;-><init>()V

    move-object v0, v0

    .line 2549798
    const-string v1, "order_param"

    iget-object v2, p0, Lcom/facebook/friending/suggestion/model/FriendingSuggestionListLoader;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "first_param"

    const-wide/16 v4, 0x14

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "after_param"

    invoke-virtual {v1, v2, p3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "profile_id"

    invoke-virtual {v1, v2, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "size"

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    .line 2549799
    iput-boolean v6, v1, LX/0gW;->l:Z

    .line 2549800
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2549801
    sget-object v1, LX/0zS;->a:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    .line 2549802
    iput-object v2, v1, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2549803
    move-object v1, v1

    .line 2549804
    iput-boolean v6, v1, LX/0zO;->p:Z

    .line 2549805
    move-object v1, v1

    .line 2549806
    const-wide/16 v2, 0xe10

    invoke-virtual {v1, v2, v3}, LX/0zO;->a(J)LX/0zO;

    .line 2549807
    return-object v0
.end method

.method public static c(Lcom/facebook/friending/suggestion/model/FriendingSuggestionListLoader;)LX/0QK;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/friending/suggestion/protocol/FetchFriendingSuggestionGraphQLModels$FetchFriendingSuggestionQueryModel;",
            ">;",
            "LX/ICn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2549796
    new-instance v0, LX/ICm;

    invoke-direct {v0, p0}, LX/ICm;-><init>(Lcom/facebook/friending/suggestion/model/FriendingSuggestionListLoader;)V

    return-object v0
.end method

.method public static d()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2549790
    new-instance v0, LX/4a7;

    invoke-direct {v0}, LX/4a7;-><init>()V

    .line 2549791
    iput-boolean v1, v0, LX/4a7;->c:Z

    .line 2549792
    move-object v0, v0

    .line 2549793
    iput-boolean v1, v0, LX/4a7;->b:Z

    .line 2549794
    move-object v0, v0

    .line 2549795
    invoke-virtual {v0}, LX/4a7;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    return-object v0
.end method
