.class public Lcom/facebook/fbreact/views/fbscroll/FbReactScrollViewManager;
.super Lcom/facebook/react/uimanager/ViewGroupManager;
.source ""

# interfaces
.implements LX/FUp;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/react/uimanager/ViewGroupManager",
        "<",
        "LX/JLw;",
        ">;",
        "LX/FUp",
        "<",
        "LX/JLw;",
        ">;"
    }
.end annotation


# instance fields
.field private b:LX/F6L;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2681754
    invoke-direct {p0}, Lcom/facebook/react/uimanager/ViewGroupManager;-><init>()V

    .line 2681755
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/fbreact/views/fbscroll/FbReactScrollViewManager;->b:LX/F6L;

    .line 2681756
    return-void
.end method

.method private static a(LX/JLw;LX/FUr;)V
    .locals 2

    .prologue
    .line 2681750
    iget-boolean v0, p1, LX/FUr;->c:Z

    if-eqz v0, :cond_0

    .line 2681751
    iget v0, p1, LX/FUr;->a:I

    iget v1, p1, LX/FUr;->b:I

    invoke-virtual {p0, v0, v1}, LX/JLv;->a(II)V

    .line 2681752
    :goto_0
    return-void

    .line 2681753
    :cond_0
    iget v0, p1, LX/FUr;->a:I

    iget v1, p1, LX/FUr;->b:I

    invoke-virtual {p0, v0, v1}, LX/JLw;->scrollTo(II)V

    goto :goto_0
.end method


# virtual methods
.method public synthetic a(LX/5rJ;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2681749
    invoke-virtual {p0, p1}, Lcom/facebook/fbreact/views/fbscroll/FbReactScrollViewManager;->b(LX/5rJ;)LX/JLw;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Landroid/view/View;ILX/5pC;)V
    .locals 0
    .param p3    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2681748
    check-cast p1, LX/JLw;

    invoke-static {p0, p1, p2, p3}, LX/FUs;->a(LX/FUp;Ljava/lang/Object;ILX/5pC;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;LX/FUr;)V
    .locals 0

    .prologue
    .line 2681730
    check-cast p1, LX/JLw;

    invoke-static {p1, p2}, Lcom/facebook/fbreact/views/fbscroll/FbReactScrollViewManager;->a(LX/JLw;LX/FUr;)V

    return-void
.end method

.method public b(LX/5rJ;)LX/JLw;
    .locals 2

    .prologue
    .line 2681747
    new-instance v0, LX/JLw;

    iget-object v1, p0, Lcom/facebook/fbreact/views/fbscroll/FbReactScrollViewManager;->b:LX/F6L;

    invoke-direct {v0, p1, v1}, LX/JLw;-><init>(LX/5pX;LX/F6L;)V

    return-object v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2681746
    const-string v0, "RCTScrollView"

    return-object v0
.end method

.method public setBottomFillColor(LX/JLw;I)V
    .locals 1
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        c = 0x0
        customType = "Color"
        name = "endFillColor"
    .end annotation

    .prologue
    .line 2681742
    iget v0, p1, LX/JLw;->l:I

    if-eq p2, v0, :cond_0

    .line 2681743
    iput p2, p1, LX/JLw;->l:I

    .line 2681744
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    iget p0, p1, LX/JLw;->l:I

    invoke-direct {v0, p0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v0, p1, LX/JLw;->k:Landroid/graphics/drawable/Drawable;

    .line 2681745
    :cond_0
    return-void
.end method

.method public setContentInset(LX/JLw;LX/5pG;)V
    .locals 3
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "contentInset"
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2681757
    if-nez p2, :cond_1

    .line 2681758
    invoke-virtual {p1, v2}, LX/JLv;->setMinScrollDeltaY(I)V

    .line 2681759
    invoke-virtual {p1, v2}, LX/JLv;->setMaxScrollDeltaY(I)V

    .line 2681760
    :cond_0
    :goto_0
    return-void

    .line 2681761
    :cond_1
    const-string v0, "top"

    invoke-interface {p2, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2681762
    const-string v0, "top"

    invoke-interface {p2, v0}, LX/5pG;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2681763
    invoke-virtual {p1, v2}, LX/JLv;->setMinScrollDeltaY(I)V

    .line 2681764
    :cond_2
    :goto_1
    const-string v0, "bottom"

    invoke-interface {p2, v0}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2681765
    const-string v0, "bottom"

    invoke-interface {p2, v0}, LX/5pG;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2681766
    invoke-virtual {p1, v2}, LX/JLv;->setMaxScrollDeltaY(I)V

    goto :goto_0

    .line 2681767
    :cond_3
    const-string v0, "top"

    invoke-interface {p2, v0}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    invoke-static {v0, v1}, LX/5r2;->a(D)F

    move-result v0

    float-to-int v0, v0

    neg-int v0, v0

    invoke-virtual {p1, v0}, LX/JLv;->setMinScrollDeltaY(I)V

    goto :goto_1

    .line 2681768
    :cond_4
    const-string v0, "bottom"

    invoke-interface {p2, v0}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    invoke-static {v0, v1}, LX/5r2;->a(D)F

    move-result v0

    float-to-int v0, v0

    neg-int v0, v0

    invoke-virtual {p1, v0}, LX/JLv;->setMaxScrollDeltaY(I)V

    goto :goto_0
.end method

.method public setRemoveClippedSubviews(LX/JLw;Z)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "removeClippedSubviews"
    .end annotation

    .prologue
    .line 2681737
    if-eqz p2, :cond_0

    iget-object p0, p1, LX/JLw;->c:Landroid/graphics/Rect;

    if-nez p0, :cond_0

    .line 2681738
    new-instance p0, Landroid/graphics/Rect;

    invoke-direct {p0}, Landroid/graphics/Rect;-><init>()V

    iput-object p0, p1, LX/JLw;->c:Landroid/graphics/Rect;

    .line 2681739
    :cond_0
    iput-boolean p2, p1, LX/JLw;->b:Z

    .line 2681740
    invoke-virtual {p1}, LX/JLw;->a()V

    .line 2681741
    return-void
.end method

.method public setScrollEnabled(LX/JLw;Z)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        d = true
        name = "scrollEnabled"
    .end annotation

    .prologue
    .line 2681735
    iput-boolean p2, p1, LX/JLw;->h:Z

    .line 2681736
    return-void
.end method

.method public setScrollPerfTag(LX/JLw;Ljava/lang/String;)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "scrollPerfTag"
    .end annotation

    .prologue
    .line 2681733
    iput-object p2, p1, LX/JLw;->j:Ljava/lang/String;

    .line 2681734
    return-void
.end method

.method public setSendMomentumEvents(LX/JLw;Z)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "sendMomentumEvents"
    .end annotation

    .prologue
    .line 2681731
    iput-boolean p2, p1, LX/JLw;->d:Z

    .line 2681732
    return-void
.end method

.method public setShowsVerticalScrollIndicator(LX/JLw;Z)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "showsVerticalScrollIndicator"
    .end annotation

    .prologue
    .line 2681728
    invoke-virtual {p1, p2}, LX/JLw;->setVerticalScrollBarEnabled(Z)V

    .line 2681729
    return-void
.end method

.method public final u()Z
    .locals 1

    .prologue
    .line 2681727
    const/4 v0, 0x1

    return v0
.end method

.method public final v()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2681726
    invoke-static {}, LX/FUs;->a()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final x()Ljava/util/Map;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2681725
    invoke-static {}, Lcom/facebook/react/views/scroll/ReactScrollViewManager;->A()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method
