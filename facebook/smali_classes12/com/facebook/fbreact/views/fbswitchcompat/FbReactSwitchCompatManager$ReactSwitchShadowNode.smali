.class public final Lcom/facebook/fbreact/views/fbswitchcompat/FbReactSwitchCompatManager$ReactSwitchShadowNode;
.super Lcom/facebook/react/uimanager/LayoutShadowNode;
.source ""

# interfaces
.implements Lcom/facebook/csslayout/YogaMeasureFunction;


# instance fields
.field private a:I

.field private b:I

.field private c:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2683317
    invoke-direct {p0}, Lcom/facebook/react/uimanager/LayoutShadowNode;-><init>()V

    .line 2683318
    invoke-virtual {p0, p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->a(Lcom/facebook/csslayout/YogaMeasureFunction;)V

    .line 2683319
    return-void
.end method

.method public synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 2683308
    invoke-direct {p0}, Lcom/facebook/fbreact/views/fbswitchcompat/FbReactSwitchCompatManager$ReactSwitchShadowNode;-><init>()V

    return-void
.end method


# virtual methods
.method public final measure(LX/1mn;FLcom/facebook/csslayout/YogaMeasureMode;FLcom/facebook/csslayout/YogaMeasureMode;)J
    .locals 3

    .prologue
    .line 2683309
    iget-boolean v0, p0, Lcom/facebook/fbreact/views/fbswitchcompat/FbReactSwitchCompatManager$ReactSwitchShadowNode;->c:Z

    if-nez v0, :cond_0

    .line 2683310
    new-instance v0, LX/JMD;

    invoke-virtual {p0}, Lcom/facebook/react/uimanager/ReactShadowNode;->o()LX/5rJ;

    move-result-object v1

    invoke-direct {v0, v1}, LX/JMD;-><init>(Landroid/content/Context;)V

    .line 2683311
    const/4 v1, -0x2

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 2683312
    invoke-virtual {v0, v1, v1}, LX/JMD;->measure(II)V

    .line 2683313
    invoke-virtual {v0}, LX/JMD;->getMeasuredWidth()I

    move-result v1

    iput v1, p0, Lcom/facebook/fbreact/views/fbswitchcompat/FbReactSwitchCompatManager$ReactSwitchShadowNode;->a:I

    .line 2683314
    invoke-virtual {v0}, LX/JMD;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Lcom/facebook/fbreact/views/fbswitchcompat/FbReactSwitchCompatManager$ReactSwitchShadowNode;->b:I

    .line 2683315
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/fbreact/views/fbswitchcompat/FbReactSwitchCompatManager$ReactSwitchShadowNode;->c:Z

    .line 2683316
    :cond_0
    iget v0, p0, Lcom/facebook/fbreact/views/fbswitchcompat/FbReactSwitchCompatManager$ReactSwitchShadowNode;->a:I

    iget v1, p0, Lcom/facebook/fbreact/views/fbswitchcompat/FbReactSwitchCompatManager$ReactSwitchShadowNode;->b:I

    invoke-static {v0, v1}, LX/1Dr;->a(II)J

    move-result-wide v0

    return-wide v0
.end method
