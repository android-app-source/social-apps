.class public Lcom/facebook/fbreact/views/fbswitchcompat/FbReactSwitchCompatManager;
.super Lcom/facebook/react/uimanager/SimpleViewManager;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "AndroidSwitch"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/react/uimanager/SimpleViewManager",
        "<",
        "LX/JMD;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Landroid/widget/CompoundButton$OnCheckedChangeListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2683339
    new-instance v0, LX/JMF;

    invoke-direct {v0}, LX/JMF;-><init>()V

    sput-object v0, Lcom/facebook/fbreact/views/fbswitchcompat/FbReactSwitchCompatManager;->a:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2683337
    invoke-direct {p0}, Lcom/facebook/react/uimanager/SimpleViewManager;-><init>()V

    .line 2683338
    return-void
.end method

.method private static a(LX/JMD;)V
    .locals 1

    .prologue
    .line 2683335
    sget-object v0, Lcom/facebook/fbreact/views/fbswitchcompat/FbReactSwitchCompatManager;->a:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {p0, v0}, LX/JMD;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2683336
    return-void
.end method

.method private static b(LX/5rJ;)LX/JMD;
    .locals 2

    .prologue
    .line 2683332
    new-instance v0, LX/JMD;

    invoke-direct {v0, p0}, LX/JMD;-><init>(Landroid/content/Context;)V

    .line 2683333
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/SwitchCompat;->setShowText(Z)V

    .line 2683334
    return-object v0
.end method


# virtual methods
.method public final synthetic a(LX/5rJ;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2683331
    invoke-static {p1}, Lcom/facebook/fbreact/views/fbswitchcompat/FbReactSwitchCompatManager;->b(LX/5rJ;)LX/JMD;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/5rJ;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2683330
    check-cast p2, LX/JMD;

    invoke-static {p2}, Lcom/facebook/fbreact/views/fbswitchcompat/FbReactSwitchCompatManager;->a(LX/JMD;)V

    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2683329
    const-string v0, "AndroidSwitch"

    return-object v0
.end method

.method public final h()Lcom/facebook/react/uimanager/LayoutShadowNode;
    .locals 2

    .prologue
    .line 2683320
    new-instance v0, Lcom/facebook/fbreact/views/fbswitchcompat/FbReactSwitchCompatManager$ReactSwitchShadowNode;

    invoke-direct {v0}, Lcom/facebook/fbreact/views/fbswitchcompat/FbReactSwitchCompatManager$ReactSwitchShadowNode;-><init>()V

    return-object v0
.end method

.method public final i()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 2683328
    const-class v0, Lcom/facebook/fbreact/views/fbswitchcompat/FbReactSwitchCompatManager$ReactSwitchShadowNode;

    return-object v0
.end method

.method public final synthetic s()Lcom/facebook/react/uimanager/ReactShadowNode;
    .locals 1

    .prologue
    .line 2683327
    invoke-virtual {p0}, Lcom/facebook/react/uimanager/SimpleViewManager;->h()Lcom/facebook/react/uimanager/LayoutShadowNode;

    move-result-object v0

    return-object v0
.end method

.method public setEnabled(LX/JMD;Z)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        d = true
        name = "enabled"
    .end annotation

    .prologue
    .line 2683325
    invoke-virtual {p1, p2}, LX/JMD;->setEnabled(Z)V

    .line 2683326
    return-void
.end method

.method public setOn(LX/JMD;Z)V
    .locals 1
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "on"
    .end annotation

    .prologue
    .line 2683321
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, LX/JMD;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2683322
    invoke-virtual {p1, p2}, LX/JMD;->a(Z)V

    .line 2683323
    sget-object v0, Lcom/facebook/fbreact/views/fbswitchcompat/FbReactSwitchCompatManager;->a:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {p1, v0}, LX/JMD;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2683324
    return-void
.end method
