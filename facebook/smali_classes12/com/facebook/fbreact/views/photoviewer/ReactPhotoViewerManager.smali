.class public Lcom/facebook/fbreact/views/photoviewer/ReactPhotoViewerManager;
.super Lcom/facebook/react/uimanager/SimpleViewManager;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "RCTPhotoViewer"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/react/uimanager/SimpleViewManager",
        "<",
        "LX/JMK;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/1Ae;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2683645
    invoke-direct {p0}, Lcom/facebook/react/uimanager/SimpleViewManager;-><init>()V

    .line 2683646
    const/4 v0, 0x1

    iput v0, p0, Lcom/facebook/fbreact/views/photoviewer/ReactPhotoViewerManager;->b:I

    .line 2683647
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/fbreact/views/photoviewer/ReactPhotoViewerManager;->a:LX/1Ae;

    return-void
.end method

.method private a(LX/JMK;)V
    .locals 0

    .prologue
    .line 2683642
    invoke-super {p0, p1}, Lcom/facebook/react/uimanager/SimpleViewManager;->b(Landroid/view/View;)V

    .line 2683643
    invoke-virtual {p1}, LX/JMK;->c()V

    .line 2683644
    return-void
.end method

.method private a(LX/JMK;ILX/5pC;)V
    .locals 9
    .param p3    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2683632
    packed-switch p2, :pswitch_data_0

    .line 2683633
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/react/uimanager/SimpleViewManager;->a(Landroid/view/View;ILX/5pC;)V

    .line 2683634
    :goto_0
    return-void

    .line 2683635
    :pswitch_0
    if-eqz p3, :cond_0

    invoke-interface {p3}, LX/5pC;->size()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    .line 2683636
    :cond_0
    new-instance v0, LX/5pA;

    const-string v1, "zoomToPoint called with incorrect args"

    invoke-direct {v0, v1}, LX/5pA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2683637
    :cond_1
    const/4 v0, 0x0

    invoke-interface {p3, v0}, LX/5pC;->getDouble(I)D

    move-result-wide v0

    double-to-float v2, v0

    .line 2683638
    new-instance v4, Landroid/graphics/PointF;

    const/4 v0, 0x1

    invoke-interface {p3, v0}, LX/5pC;->getDouble(I)D

    move-result-wide v0

    invoke-static {v0, v1}, LX/5r2;->a(D)F

    move-result v0

    const/4 v1, 0x2

    invoke-interface {p3, v1}, LX/5pC;->getDouble(I)D

    move-result-wide v6

    invoke-static {v6, v7}, LX/5r2;->a(D)F

    move-result v1

    invoke-direct {v4, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2683639
    const/4 v0, 0x3

    invoke-interface {p3, v0}, LX/5pC;->getInt(I)I

    move-result v0

    int-to-long v6, v0

    .line 2683640
    invoke-virtual {p1}, LX/5ui;->getZoomableController()LX/5ua;

    move-result-object v1

    check-cast v1, LX/5ue;

    .line 2683641
    invoke-virtual {v1, v4}, LX/5ua;->a(Landroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v3

    const/4 v5, 0x7

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, LX/5ub;->a(FLandroid/graphics/PointF;Landroid/graphics/PointF;IJLjava/lang/Runnable;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private b(LX/5rJ;)LX/JMK;
    .locals 2

    .prologue
    .line 2683631
    new-instance v0, LX/JMK;

    invoke-direct {p0}, Lcom/facebook/fbreact/views/photoviewer/ReactPhotoViewerManager;->t()LX/1Ae;

    move-result-object v1

    invoke-direct {v0, p1, v1}, LX/JMK;-><init>(Landroid/content/Context;LX/1Ae;)V

    return-object v0
.end method

.method private t()LX/1Ae;
    .locals 1

    .prologue
    .line 2683628
    iget-object v0, p0, Lcom/facebook/fbreact/views/photoviewer/ReactPhotoViewerManager;->a:LX/1Ae;

    if-nez v0, :cond_0

    .line 2683629
    invoke-static {}, LX/4AN;->a()LX/1bk;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fbreact/views/photoviewer/ReactPhotoViewerManager;->a:LX/1Ae;

    .line 2683630
    :cond_0
    iget-object v0, p0, Lcom/facebook/fbreact/views/photoviewer/ReactPhotoViewerManager;->a:LX/1Ae;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(LX/5rJ;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2683602
    invoke-direct {p0, p1}, Lcom/facebook/fbreact/views/photoviewer/ReactPhotoViewerManager;->b(LX/5rJ;)LX/JMK;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Landroid/view/View;ILX/5pC;)V
    .locals 0
    .param p3    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2683627
    check-cast p1, LX/JMK;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbreact/views/photoviewer/ReactPhotoViewerManager;->a(LX/JMK;ILX/5pC;)V

    return-void
.end method

.method public final synthetic b(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2683626
    check-cast p1, LX/JMK;

    invoke-direct {p0, p1}, Lcom/facebook/fbreact/views/photoviewer/ReactPhotoViewerManager;->a(LX/JMK;)V

    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2683625
    const-string v0, "RCTPhotoViewer"

    return-object v0
.end method

.method public setMaxScaleFactor(LX/JMK;F)V
    .locals 1
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "maxScaleFactor"
    .end annotation

    .prologue
    .line 2683622
    invoke-virtual {p1}, LX/5ui;->getZoomableController()LX/5ua;

    move-result-object v0

    check-cast v0, LX/5ua;

    .line 2683623
    iput p2, v0, LX/5ua;->j:F

    .line 2683624
    return-void
.end method

.method public setMinScaleFactor(LX/JMK;F)V
    .locals 1
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "minScaleFactor"
    .end annotation

    .prologue
    .line 2683619
    invoke-virtual {p1}, LX/5ui;->getZoomableController()LX/5ua;

    move-result-object v0

    check-cast v0, LX/5ua;

    .line 2683620
    iput p2, v0, LX/5ua;->i:F

    .line 2683621
    return-void
.end method

.method public setSource(LX/JMK;LX/5pC;)V
    .locals 11
    .param p2    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "src"
    .end annotation

    .prologue
    .line 2683605
    const-wide/16 v8, 0x0

    .line 2683606
    iget-object v0, p1, LX/JMK;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2683607
    if-eqz p2, :cond_2

    .line 2683608
    const/4 v0, 0x0

    :goto_0
    invoke-interface {p2}, LX/5pC;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 2683609
    invoke-interface {p2, v0}, LX/5pC;->a(I)LX/5pG;

    move-result-object v1

    .line 2683610
    const-string v2, "uri"

    invoke-interface {v1, v2}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2683611
    const-string v2, "width"

    invoke-interface {v1, v2}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "width"

    invoke-interface {v1, v2}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    .line 2683612
    :goto_1
    const-string v2, "height"

    invoke-interface {v1, v2}, LX/5pG;->hasKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "height"

    invoke-interface {v1, v2}, LX/5pG;->getDouble(Ljava/lang/String;)D

    move-result-wide v6

    .line 2683613
    :goto_2
    iget-object v10, p1, LX/JMK;->b:Ljava/util/List;

    new-instance v1, LX/9nR;

    invoke-virtual {p1}, LX/JMK;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct/range {v1 .. v7}, LX/9nR;-><init>(Landroid/content/Context;Ljava/lang/String;DD)V

    invoke-interface {v10, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2683614
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move-wide v4, v8

    .line 2683615
    goto :goto_1

    :cond_1
    move-wide v6, v8

    .line 2683616
    goto :goto_2

    .line 2683617
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p1, LX/JMK;->c:Z

    .line 2683618
    return-void
.end method

.method public final v()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2683604
    const-string v0, "zoomToPoint"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final x()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2683603
    invoke-static {}, LX/5ps;->b()LX/5pr;

    move-result-object v0

    const-string v1, "topZoom"

    const-string v2, "registrationName"

    const-string v3, "onZoom"

    invoke-static {v2, v3}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/5pr;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/5pr;

    move-result-object v0

    invoke-virtual {v0}, LX/5pr;->a()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method
