.class public Lcom/facebook/fbreact/views/shimmer/FbReactShimmerFrameLayoutManager;
.super Lcom/facebook/react/uimanager/ViewGroupManager;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "RKShimmeringView"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/react/uimanager/ViewGroupManager",
        "<",
        "Lcom/facebook/widget/ShimmerFrameLayout;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2683741
    invoke-direct {p0}, Lcom/facebook/react/uimanager/ViewGroupManager;-><init>()V

    return-void
.end method

.method private static b(LX/5rJ;)Lcom/facebook/widget/ShimmerFrameLayout;
    .locals 2

    .prologue
    .line 2683738
    new-instance v0, Lcom/facebook/widget/ShimmerFrameLayout;

    invoke-direct {v0, p0}, Lcom/facebook/widget/ShimmerFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2683739
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/ShimmerFrameLayout;->setAutoStart(Z)V

    .line 2683740
    return-object v0
.end method


# virtual methods
.method public final synthetic a(LX/5rJ;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2683730
    invoke-static {p1}, Lcom/facebook/fbreact/views/shimmer/FbReactShimmerFrameLayoutManager;->b(LX/5rJ;)Lcom/facebook/widget/ShimmerFrameLayout;

    move-result-object v0

    return-object v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2683737
    const-string v0, "RKShimmeringView"

    return-object v0
.end method

.method public setDisabled(Lcom/facebook/widget/ShimmerFrameLayout;Z)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "enabled"
    .end annotation

    .prologue
    .line 2683733
    if-eqz p2, :cond_0

    .line 2683734
    invoke-virtual {p1}, Lcom/facebook/widget/ShimmerFrameLayout;->b()V

    .line 2683735
    :goto_0
    return-void

    .line 2683736
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/widget/ShimmerFrameLayout;->c()V

    goto :goto_0
.end method

.method public setSpeed(Lcom/facebook/widget/ShimmerFrameLayout;I)V
    .locals 0
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "speed"
    .end annotation

    .prologue
    .line 2683731
    invoke-virtual {p1, p2}, Lcom/facebook/widget/ShimmerFrameLayout;->setDuration(I)V

    .line 2683732
    return-void
.end method
