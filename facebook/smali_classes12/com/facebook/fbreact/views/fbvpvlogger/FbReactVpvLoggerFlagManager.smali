.class public Lcom/facebook/fbreact/views/fbvpvlogger/FbReactVpvLoggerFlagManager;
.super Lcom/facebook/react/uimanager/SimpleViewManager;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "FbReactVpvLoggerFlag"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/react/uimanager/SimpleViewManager",
        "<",
        "LX/JMG;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2683416
    invoke-direct {p0}, Lcom/facebook/react/uimanager/SimpleViewManager;-><init>()V

    return-void
.end method

.method private static b(LX/5rJ;)LX/JMG;
    .locals 1

    .prologue
    .line 2683417
    new-instance v0, LX/JMG;

    invoke-direct {v0, p0}, LX/JMG;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public final synthetic a(LX/5rJ;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2683418
    invoke-static {p1}, Lcom/facebook/fbreact/views/fbvpvlogger/FbReactVpvLoggerFlagManager;->b(LX/5rJ;)LX/JMG;

    move-result-object v0

    return-object v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2683419
    const-string v0, "FbReactVpvLoggerFlag"

    return-object v0
.end method

.method public setFeedUnitId(LX/JMG;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "feedUnitId"
    .end annotation

    .prologue
    .line 2683420
    iput-object p2, p1, LX/JMG;->a:Ljava/lang/String;

    .line 2683421
    return-void
.end method

.method public setTop(LX/JMG;Z)V
    .locals 0
    .param p2    # Z
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "isTop"
    .end annotation

    .prologue
    .line 2683422
    iput-boolean p2, p1, LX/JMG;->b:Z

    .line 2683423
    return-void
.end method

.method public final x()Ljava/util/Map;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2683424
    const-string v0, "reactVpvEvent"

    const-string v1, "registrationName"

    const-string v2, "onAttachmentEvent"

    invoke-static {v1, v2}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v1

    invoke-static {v0, v1}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method
