.class public Lcom/facebook/fbreact/views/fbperflogger/FbReactPerfLoggerFlagManager;
.super Lcom/facebook/react/uimanager/SimpleViewManager;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "FBReactPerfLoggerFlag"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/react/uimanager/SimpleViewManager",
        "<",
        "LX/JMA;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/JKh;


# direct methods
.method public constructor <init>(LX/JKh;)V
    .locals 0

    .prologue
    .line 2682972
    invoke-direct {p0}, Lcom/facebook/react/uimanager/SimpleViewManager;-><init>()V

    .line 2682973
    iput-object p1, p0, Lcom/facebook/fbreact/views/fbperflogger/FbReactPerfLoggerFlagManager;->a:LX/JKh;

    .line 2682974
    return-void
.end method

.method private b(LX/5rJ;)LX/JMA;
    .locals 2

    .prologue
    .line 2682965
    new-instance v0, LX/JMA;

    iget-object v1, p0, Lcom/facebook/fbreact/views/fbperflogger/FbReactPerfLoggerFlagManager;->a:LX/JKh;

    invoke-direct {v0, p1, v1}, LX/JMA;-><init>(Landroid/content/Context;LX/JKh;)V

    return-object v0
.end method


# virtual methods
.method public final synthetic a(LX/5rJ;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2682966
    invoke-direct {p0, p1}, Lcom/facebook/fbreact/views/fbperflogger/FbReactPerfLoggerFlagManager;->b(LX/5rJ;)LX/JMA;

    move-result-object v0

    return-object v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2682967
    const-string v0, "FBReactPerfLoggerFlag"

    return-object v0
.end method

.method public setEventName(LX/JMA;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "eventName"
    .end annotation

    .prologue
    .line 2682968
    iput-object p2, p1, LX/JMA;->b:Ljava/lang/String;

    .line 2682969
    return-void
.end method

.method public setExtraData(LX/JMA;LX/5pG;)V
    .locals 0
    .param p2    # LX/5pG;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "extraData"
    .end annotation

    .prologue
    .line 2682970
    iput-object p2, p1, LX/JMA;->c:LX/5pG;

    .line 2682971
    return-void
.end method
