.class public Lcom/facebook/fbreact/goodwill/GoodwillVideoState;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/fbreact/goodwill/GoodwillVideoState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/fbreact/goodwill/GoodwillVideoState$PhotoData;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2681010
    new-instance v0, LX/JLM;

    invoke-direct {v0}, LX/JLM;-><init>()V

    sput-object v0, Lcom/facebook/fbreact/goodwill/GoodwillVideoState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2681011
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2681012
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fbreact/goodwill/GoodwillVideoState;->a:Ljava/lang/String;

    .line 2681013
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fbreact/goodwill/GoodwillVideoState;->b:Ljava/lang/String;

    .line 2681014
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fbreact/goodwill/GoodwillVideoState;->c:Ljava/lang/String;

    .line 2681015
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fbreact/goodwill/GoodwillVideoState;->d:Ljava/lang/String;

    .line 2681016
    const-class v0, Lcom/facebook/fbreact/goodwill/GoodwillVideoState$PhotoData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2681017
    if-nez v0, :cond_0

    .line 2681018
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2681019
    :goto_0
    iput-object v0, p0, Lcom/facebook/fbreact/goodwill/GoodwillVideoState;->e:LX/0Px;

    .line 2681020
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fbreact/goodwill/GoodwillVideoState;->f:Ljava/lang/String;

    .line 2681021
    return-void

    .line 2681022
    :cond_0
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/fbreact/goodwill/GoodwillVideoState$PhotoData;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2681023
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2681024
    iput-object p1, p0, Lcom/facebook/fbreact/goodwill/GoodwillVideoState;->a:Ljava/lang/String;

    .line 2681025
    iput-object p2, p0, Lcom/facebook/fbreact/goodwill/GoodwillVideoState;->b:Ljava/lang/String;

    .line 2681026
    iput-object p3, p0, Lcom/facebook/fbreact/goodwill/GoodwillVideoState;->c:Ljava/lang/String;

    .line 2681027
    iput-object p4, p0, Lcom/facebook/fbreact/goodwill/GoodwillVideoState;->d:Ljava/lang/String;

    .line 2681028
    iput-object p5, p0, Lcom/facebook/fbreact/goodwill/GoodwillVideoState;->e:LX/0Px;

    .line 2681029
    iput-object p6, p0, Lcom/facebook/fbreact/goodwill/GoodwillVideoState;->f:Ljava/lang/String;

    .line 2681030
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2681031
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2681032
    iget-object v0, p0, Lcom/facebook/fbreact/goodwill/GoodwillVideoState;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2681033
    iget-object v0, p0, Lcom/facebook/fbreact/goodwill/GoodwillVideoState;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2681034
    iget-object v0, p0, Lcom/facebook/fbreact/goodwill/GoodwillVideoState;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2681035
    iget-object v0, p0, Lcom/facebook/fbreact/goodwill/GoodwillVideoState;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2681036
    iget-object v0, p0, Lcom/facebook/fbreact/goodwill/GoodwillVideoState;->e:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2681037
    iget-object v0, p0, Lcom/facebook/fbreact/goodwill/GoodwillVideoState;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2681038
    return-void
.end method
