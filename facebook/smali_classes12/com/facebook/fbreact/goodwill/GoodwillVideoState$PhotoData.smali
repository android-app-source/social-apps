.class public final Lcom/facebook/fbreact/goodwill/GoodwillVideoState$PhotoData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/fbreact/goodwill/GoodwillVideoState$PhotoData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2681009
    new-instance v0, LX/JLN;

    invoke-direct {v0}, LX/JLN;-><init>()V

    sput-object v0, Lcom/facebook/fbreact/goodwill/GoodwillVideoState$PhotoData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2681005
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2681006
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fbreact/goodwill/GoodwillVideoState$PhotoData;->a:Ljava/lang/String;

    .line 2681007
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fbreact/goodwill/GoodwillVideoState$PhotoData;->b:Ljava/lang/String;

    .line 2681008
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2681001
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2681002
    iput-object p1, p0, Lcom/facebook/fbreact/goodwill/GoodwillVideoState$PhotoData;->a:Ljava/lang/String;

    .line 2681003
    iput-object p2, p0, Lcom/facebook/fbreact/goodwill/GoodwillVideoState$PhotoData;->b:Ljava/lang/String;

    .line 2681004
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2680997
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2680998
    iget-object v0, p0, Lcom/facebook/fbreact/goodwill/GoodwillVideoState$PhotoData;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2680999
    iget-object v0, p0, Lcom/facebook/fbreact/goodwill/GoodwillVideoState$PhotoData;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2681000
    return-void
.end method
