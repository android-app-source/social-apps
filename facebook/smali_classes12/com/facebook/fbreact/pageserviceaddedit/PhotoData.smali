.class public Lcom/facebook/fbreact/pageserviceaddedit/PhotoData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/fbreact/pageserviceaddedit/PhotoData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:I

.field public final d:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2681619
    new-instance v0, LX/JLq;

    invoke-direct {v0}, LX/JLq;-><init>()V

    sput-object v0, Lcom/facebook/fbreact/pageserviceaddedit/PhotoData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2681620
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2681621
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fbreact/pageserviceaddedit/PhotoData;->a:Ljava/lang/String;

    .line 2681622
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fbreact/pageserviceaddedit/PhotoData;->b:Ljava/lang/String;

    .line 2681623
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/fbreact/pageserviceaddedit/PhotoData;->c:I

    .line 2681624
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/fbreact/pageserviceaddedit/PhotoData;->d:I

    .line 2681625
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 2681626
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2681627
    iput-object p1, p0, Lcom/facebook/fbreact/pageserviceaddedit/PhotoData;->a:Ljava/lang/String;

    .line 2681628
    iput-object p2, p0, Lcom/facebook/fbreact/pageserviceaddedit/PhotoData;->b:Ljava/lang/String;

    .line 2681629
    iput p3, p0, Lcom/facebook/fbreact/pageserviceaddedit/PhotoData;->c:I

    .line 2681630
    iput p4, p0, Lcom/facebook/fbreact/pageserviceaddedit/PhotoData;->d:I

    .line 2681631
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2681632
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2681633
    iget-object v0, p0, Lcom/facebook/fbreact/pageserviceaddedit/PhotoData;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2681634
    iget-object v0, p0, Lcom/facebook/fbreact/pageserviceaddedit/PhotoData;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2681635
    iget v0, p0, Lcom/facebook/fbreact/pageserviceaddedit/PhotoData;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2681636
    iget v0, p0, Lcom/facebook/fbreact/pageserviceaddedit/PhotoData;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2681637
    return-void
.end method
