.class public Lcom/facebook/fbreact/marketplace/ReactFragmentWithMarketplaceSearch;
.super Lcom/facebook/fbreact/fragment/FbReactFragment;
.source ""

# interfaces
.implements LX/0o2;


# instance fields
.field public f:LX/FiU;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/FiT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/FgR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private i:Landroid/view/View$OnTouchListener;

.field private j:LX/103;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2681551
    invoke-direct {p0}, Lcom/facebook/fbreact/fragment/FbReactFragment;-><init>()V

    .line 2681552
    sget-object v0, LX/103;->MARKETPLACE:LX/103;

    iput-object v0, p0, Lcom/facebook/fbreact/marketplace/ReactFragmentWithMarketplaceSearch;->j:LX/103;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/fbreact/marketplace/ReactFragmentWithMarketplaceSearch;

    invoke-static {p0}, LX/FiU;->a(LX/0QB;)LX/FiU;

    move-result-object v1

    check-cast v1, LX/FiU;

    invoke-static {p0}, LX/FiT;->a(LX/0QB;)LX/FiT;

    move-result-object v2

    check-cast v2, LX/FiT;

    invoke-static {p0}, LX/FgR;->a(LX/0QB;)LX/FgR;

    move-result-object p0

    check-cast p0, LX/FgR;

    iput-object v1, p1, Lcom/facebook/fbreact/marketplace/ReactFragmentWithMarketplaceSearch;->f:LX/FiU;

    iput-object v2, p1, Lcom/facebook/fbreact/marketplace/ReactFragmentWithMarketplaceSearch;->g:LX/FiT;

    iput-object p0, p1, Lcom/facebook/fbreact/marketplace/ReactFragmentWithMarketplaceSearch;->h:LX/FgR;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2681509
    const-class v0, Lcom/facebook/fbreact/marketplace/ReactFragmentWithMarketplaceSearch;

    invoke-static {v0, p0}, Lcom/facebook/fbreact/marketplace/ReactFragmentWithMarketplaceSearch;->a(Ljava/lang/Class;LX/02k;)V

    .line 2681510
    invoke-super {p0, p1}, Lcom/facebook/fbreact/fragment/FbReactFragment;->a(Landroid/os/Bundle;)V

    .line 2681511
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2681512
    const-string v1, "react_search_module"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2681513
    const-string v1, "MarketplaceSearch"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2681514
    sget-object v0, LX/103;->MARKETPLACE:LX/103;

    iput-object v0, p0, Lcom/facebook/fbreact/marketplace/ReactFragmentWithMarketplaceSearch;->j:LX/103;

    .line 2681515
    :cond_0
    :goto_0
    return-void

    .line 2681516
    :cond_1
    const-string v1, "B2CSearch"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2681517
    sget-object v0, LX/103;->COMMERCE:LX/103;

    iput-object v0, p0, Lcom/facebook/fbreact/marketplace/ReactFragmentWithMarketplaceSearch;->j:LX/103;

    goto :goto_0
.end method

.method public final j()Lcom/facebook/search/api/GraphSearchQuery;
    .locals 1

    .prologue
    .line 2681550
    iget-object v0, p0, Lcom/facebook/fbreact/marketplace/ReactFragmentWithMarketplaceSearch;->j:LX/103;

    invoke-static {v0}, LX/JLl;->a(LX/103;)Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v0

    return-object v0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x2a

    const v1, -0x533678d2

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2681545
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/fbreact/fragment/FbReactFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 2681546
    new-instance v2, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;-><init>(Landroid/content/Context;)V

    .line 2681547
    iget-object v3, p0, Lcom/facebook/fbreact/marketplace/ReactFragmentWithMarketplaceSearch;->f:LX/FiU;

    const-string v4, ""

    invoke-virtual {v3, v2, v4}, LX/FiU;->a(Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;Ljava/lang/String;)V

    .line 2681548
    iget-object v3, p0, Lcom/facebook/fbreact/marketplace/ReactFragmentWithMarketplaceSearch;->j:LX/103;

    invoke-virtual {v2, v3}, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->a(LX/103;)V

    .line 2681549
    const/16 v2, 0x2b

    const v3, 0x45c40c48

    invoke-static {v5, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x2d45a34b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2681538
    invoke-super {p0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->onDestroyView()V

    .line 2681539
    iget-object v1, p0, Lcom/facebook/fbreact/marketplace/ReactFragmentWithMarketplaceSearch;->h:LX/FgR;

    .line 2681540
    iget-object v2, v1, LX/FgR;->a:Lcom/facebook/ui/search/SearchEditText;

    move-object v1, v2

    .line 2681541
    if-eqz v1, :cond_0

    .line 2681542
    iget-object v2, p0, Lcom/facebook/fbreact/marketplace/ReactFragmentWithMarketplaceSearch;->i:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Lcom/facebook/ui/search/SearchEditText;->b(Landroid/view/View$OnTouchListener;)V

    .line 2681543
    :cond_0
    iget-object v1, p0, Lcom/facebook/fbreact/marketplace/ReactFragmentWithMarketplaceSearch;->f:LX/FiU;

    invoke-virtual {v1}, LX/FiU;->b()V

    .line 2681544
    const/16 v1, 0x2b

    const v2, -0x2a482a93

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x32a5d7ca

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2681535
    invoke-super {p0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->onPause()V

    .line 2681536
    iget-object v1, p0, Lcom/facebook/fbreact/marketplace/ReactFragmentWithMarketplaceSearch;->f:LX/FiU;

    invoke-virtual {v1}, LX/FiU;->a()V

    .line 2681537
    const/16 v1, 0x2b

    const v2, -0x305bf0f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/16 v0, 0x2a

    const v1, 0x4d3dcf56    # 1.99030112E8f

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2681525
    invoke-super {p0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->onStart()V

    .line 2681526
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2681527
    if-eqz v0, :cond_0

    .line 2681528
    iget-object v2, p0, Lcom/facebook/fbreact/marketplace/ReactFragmentWithMarketplaceSearch;->f:LX/FiU;

    const-string v3, ""

    invoke-virtual {v2, v4, v0, v3}, LX/FiU;->a(ZLX/1ZF;Ljava/lang/String;)V

    .line 2681529
    :cond_0
    iget-object v0, p0, Lcom/facebook/fbreact/marketplace/ReactFragmentWithMarketplaceSearch;->h:LX/FgR;

    .line 2681530
    iget-object v2, v0, LX/FgR;->a:Lcom/facebook/ui/search/SearchEditText;

    move-object v0, v2

    .line 2681531
    if-eqz v0, :cond_1

    .line 2681532
    invoke-virtual {v0}, Lcom/facebook/ui/search/SearchEditText;->clearFocus()V

    .line 2681533
    invoke-virtual {v0, v4}, Lcom/facebook/ui/search/SearchEditText;->setFocusable(Z)V

    .line 2681534
    :cond_1
    const/16 v0, 0x2b

    const v2, 0x74626195

    invoke-static {v5, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2681518
    invoke-super {p0, p1, p2}, Lcom/facebook/fbreact/fragment/FbReactFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2681519
    iget-object v0, p0, Lcom/facebook/fbreact/marketplace/ReactFragmentWithMarketplaceSearch;->h:LX/FgR;

    .line 2681520
    iget-object v1, v0, LX/FgR;->a:Lcom/facebook/ui/search/SearchEditText;

    move-object v0, v1

    .line 2681521
    if-eqz v0, :cond_0

    .line 2681522
    new-instance v1, LX/JLm;

    invoke-direct {v1, p0, p0}, LX/JLm;-><init>(Lcom/facebook/fbreact/marketplace/ReactFragmentWithMarketplaceSearch;Landroid/support/v4/app/Fragment;)V

    iput-object v1, p0, Lcom/facebook/fbreact/marketplace/ReactFragmentWithMarketplaceSearch;->i:Landroid/view/View$OnTouchListener;

    .line 2681523
    iget-object v1, p0, Lcom/facebook/fbreact/marketplace/ReactFragmentWithMarketplaceSearch;->i:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Lcom/facebook/ui/search/SearchEditText;->a(Landroid/view/View$OnTouchListener;)V

    .line 2681524
    :cond_0
    return-void
.end method
