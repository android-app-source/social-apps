.class public Lcom/facebook/fbreact/events/EventsDashboardReactNativeFragment;
.super Lcom/facebook/fbreact/fragment/FbReactFragment;
.source ""


# static fields
.field private static final k:Ljava/lang/String;


# instance fields
.field public f:LX/JKg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/JK6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:Lcom/facebook/fbreact/persistedqueries/Fb4aPersistedQueryRequestSender;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0Sg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private l:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2679971
    const-class v0, Lcom/facebook/fbreact/events/EventsDashboardReactNativeFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/fbreact/events/EventsDashboardReactNativeFragment;->k:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2679970
    invoke-direct {p0}, Lcom/facebook/fbreact/fragment/FbReactFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v6

    move-object v1, p1

    check-cast v1, Lcom/facebook/fbreact/events/EventsDashboardReactNativeFragment;

    invoke-static {v6}, LX/JKg;->a(LX/0QB;)LX/JKg;

    move-result-object v2

    check-cast v2, LX/JKg;

    invoke-static {v6}, LX/JK6;->a(LX/0QB;)LX/JK6;

    move-result-object v3

    check-cast v3, LX/JK6;

    invoke-static {v6}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    new-instance v0, Lcom/facebook/fbreact/persistedqueries/Fb4aPersistedQueryRequestSender;

    new-instance v5, LX/990;

    invoke-direct {v5}, LX/990;-><init>()V

    move-object v5, v5

    move-object v5, v5

    check-cast v5, LX/990;

    invoke-static {v6}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object p0

    check-cast p0, LX/11H;

    invoke-static {v6}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object p1

    check-cast p1, LX/0TD;

    invoke-direct {v0, v5, p0, p1}, Lcom/facebook/fbreact/persistedqueries/Fb4aPersistedQueryRequestSender;-><init>(LX/990;LX/11H;LX/0TD;)V

    move-object v5, v0

    check-cast v5, Lcom/facebook/fbreact/persistedqueries/Fb4aPersistedQueryRequestSender;

    invoke-static {v6}, LX/0Sg;->a(LX/0QB;)LX/0Sg;

    move-result-object v6

    check-cast v6, LX/0Sg;

    iput-object v2, v1, Lcom/facebook/fbreact/events/EventsDashboardReactNativeFragment;->f:LX/JKg;

    iput-object v3, v1, Lcom/facebook/fbreact/events/EventsDashboardReactNativeFragment;->g:LX/JK6;

    iput-object v4, v1, Lcom/facebook/fbreact/events/EventsDashboardReactNativeFragment;->h:LX/0ad;

    iput-object v5, v1, Lcom/facebook/fbreact/events/EventsDashboardReactNativeFragment;->i:Lcom/facebook/fbreact/persistedqueries/Fb4aPersistedQueryRequestSender;

    iput-object v6, v1, Lcom/facebook/fbreact/events/EventsDashboardReactNativeFragment;->j:LX/0Sg;

    return-void
.end method

.method public static m(Lcom/facebook/fbreact/events/EventsDashboardReactNativeFragment;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2679967
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2679968
    const-string v1, "extra_ref_module"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2679969
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "unknown"

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 2679930
    const-class v0, Lcom/facebook/fbreact/events/EventsDashboardReactNativeFragment;

    invoke-static {v0, p0}, Lcom/facebook/fbreact/events/EventsDashboardReactNativeFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2679931
    invoke-super {p0, p1}, Lcom/facebook/fbreact/fragment/FbReactFragment;->a(Landroid/os/Bundle;)V

    .line 2679932
    iget-object v0, p0, Lcom/facebook/fbreact/events/EventsDashboardReactNativeFragment;->f:LX/JKg;

    iget-object v1, p0, Lcom/facebook/fbreact/events/EventsDashboardReactNativeFragment;->g:LX/JK6;

    .line 2679933
    iget-object v2, v0, LX/JKg;->a:Ljava/util/Collection;

    invoke-interface {v2, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 2679934
    if-eqz p1, :cond_0

    .line 2679935
    const-string v0, "forceRefresh"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/fbreact/events/EventsDashboardReactNativeFragment;->l:Z

    .line 2679936
    :cond_0
    iget-object v0, p0, Lcom/facebook/fbreact/events/EventsDashboardReactNativeFragment;->h:LX/0ad;

    sget-object v1, LX/0c0;->Live:LX/0c0;

    sget-short v2, LX/347;->H:S

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2679937
    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/facebook/fbreact/events/EventsDashboardReactNativeFragment;->l:Z

    if-eqz v0, :cond_2

    .line 2679938
    :cond_1
    iget-object v0, p0, Lcom/facebook/fbreact/events/EventsDashboardReactNativeFragment;->j:LX/0Sg;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/facebook/fbreact/events/EventsDashboardReactNativeFragment;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - Prefetch"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/facebook/fbreact/events/EventsDashboardReactNativeFragment$1;

    invoke-direct {v2, p0}, Lcom/facebook/fbreact/events/EventsDashboardReactNativeFragment$1;-><init>(Lcom/facebook/fbreact/events/EventsDashboardReactNativeFragment;)V

    sget-object v3, LX/0VZ;->APPLICATION_LOADED_HIGH_PRIORITY:LX/0VZ;

    sget-object v4, LX/0Vm;->BACKGROUND:LX/0Vm;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0Sg;->a(Ljava/lang/String;Ljava/lang/Runnable;LX/0VZ;LX/0Vm;)LX/0Va;

    .line 2679939
    :cond_2
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 2679964
    iget-object v0, p0, Lcom/facebook/fbreact/events/EventsDashboardReactNativeFragment;->g:LX/JK6;

    .line 2679965
    iget-object v1, v0, LX/JK6;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const p0, 0x60018

    invoke-interface {v1, p0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 2679966
    return-void
.end method

.method public final d()V
    .locals 8

    .prologue
    .line 2679956
    iget-object v0, p0, Lcom/facebook/fbreact/events/EventsDashboardReactNativeFragment;->g:LX/JK6;

    .line 2679957
    iget-object v2, v0, LX/JK6;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x60019

    const/4 v4, 0x0

    iget-object v5, v0, LX/JK6;->a:LX/0So;

    invoke-interface {v5}, LX/0So;->now()J

    move-result-wide v6

    invoke-interface {v2, v3, v4, v6, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->markerStart(IIJ)V

    .line 2679958
    invoke-virtual {p0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->l()LX/33y;

    move-result-object v0

    invoke-virtual {v0}, LX/33y;->k()LX/5pX;

    move-result-object v0

    .line 2679959
    if-eqz v0, :cond_0

    .line 2679960
    const-class v1, LX/JK3;

    invoke-virtual {v0, v1}, LX/5pX;->b(Ljava/lang/Class;)LX/5p4;

    move-result-object v0

    check-cast v0, LX/JK3;

    invoke-static {p0}, Lcom/facebook/fbreact/events/EventsDashboardReactNativeFragment;->m(Lcom/facebook/fbreact/events/EventsDashboardReactNativeFragment;)Ljava/lang/String;

    move-result-object v1

    .line 2679961
    iput-object v1, v0, LX/JK3;->p:Ljava/lang/String;

    .line 2679962
    :goto_0
    return-void

    .line 2679963
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->l()LX/33y;

    move-result-object v0

    new-instance v1, LX/JJw;

    invoke-direct {v1, p0}, LX/JJw;-><init>(Lcom/facebook/fbreact/events/EventsDashboardReactNativeFragment;)V

    invoke-virtual {v0, v1}, LX/33y;->a(LX/9mm;)V

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x2e5d2bba

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2679952
    iget-object v1, p0, Lcom/facebook/fbreact/events/EventsDashboardReactNativeFragment;->f:LX/JKg;

    iget-object v2, p0, Lcom/facebook/fbreact/events/EventsDashboardReactNativeFragment;->g:LX/JK6;

    .line 2679953
    iget-object v4, v1, LX/JKg;->a:Ljava/util/Collection;

    invoke-interface {v4, v2}, Ljava/util/Collection;->remove(Ljava/lang/Object;)Z

    .line 2679954
    invoke-super {p0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->onDestroy()V

    .line 2679955
    const/16 v1, 0x2b

    const v2, 0x140a2190

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x52c337d7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2679949
    invoke-super {p0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->onPause()V

    .line 2679950
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/fbreact/events/EventsDashboardReactNativeFragment;->l:Z

    .line 2679951
    const/16 v1, 0x2b

    const v2, -0x574ce8b1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x2e68dd7f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2679943
    invoke-super {p0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->onResume()V

    .line 2679944
    iget-boolean v0, p0, Lcom/facebook/fbreact/events/EventsDashboardReactNativeFragment;->l:Z

    if-eqz v0, :cond_0

    .line 2679945
    invoke-virtual {p0}, Lcom/facebook/fbreact/fragment/FbReactFragment;->l()LX/33y;

    move-result-object v0

    invoke-virtual {v0}, LX/33y;->k()LX/5pX;

    move-result-object v0

    .line 2679946
    if-eqz v0, :cond_0

    .line 2679947
    const-class v2, Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;

    invoke-virtual {v0, v2}, LX/5pX;->a(Ljava/lang/Class;)Lcom/facebook/react/bridge/JavaScriptModule;

    move-result-object v0

    check-cast v0, Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;

    const-string v2, "EventsDashboardRefresh"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Lcom/facebook/react/modules/core/RCTNativeAppEventEmitter;->emit(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2679948
    :cond_0
    const/16 v0, 0x2b

    const v2, -0x383f609b

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2679940
    invoke-super {p0, p1}, Lcom/facebook/fbreact/fragment/FbReactFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2679941
    const-string v0, "forceRefresh"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2679942
    return-void
.end method
