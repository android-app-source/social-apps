.class public Lcom/facebook/fbreact/perf/FrameLoggingFbReactScrollViewManager;
.super Lcom/facebook/fbreact/views/fbscroll/FbReactScrollViewManager;
.source ""


# instance fields
.field public b:Lcom/facebook/common/perftest/DrawFrameLogger;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2681780
    invoke-direct {p0}, Lcom/facebook/fbreact/views/fbscroll/FbReactScrollViewManager;-><init>()V

    .line 2681781
    return-void
.end method

.method public constructor <init>(Lcom/facebook/common/perftest/DrawFrameLogger;)V
    .locals 0

    .prologue
    .line 2681777
    invoke-direct {p0}, Lcom/facebook/fbreact/views/fbscroll/FbReactScrollViewManager;-><init>()V

    .line 2681778
    iput-object p1, p0, Lcom/facebook/fbreact/perf/FrameLoggingFbReactScrollViewManager;->b:Lcom/facebook/common/perftest/DrawFrameLogger;

    .line 2681779
    return-void
.end method

.method private c(LX/5rJ;)LX/JLx;
    .locals 2

    .prologue
    .line 2681771
    iget-object v0, p0, Lcom/facebook/fbreact/perf/FrameLoggingFbReactScrollViewManager;->b:Lcom/facebook/common/perftest/DrawFrameLogger;

    if-nez v0, :cond_0

    .line 2681772
    invoke-static {}, LX/JLz;->a()Lcom/facebook/common/perftest/DrawFrameLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fbreact/perf/FrameLoggingFbReactScrollViewManager;->b:Lcom/facebook/common/perftest/DrawFrameLogger;

    .line 2681773
    :cond_0
    new-instance v0, LX/JLx;

    invoke-direct {v0, p1}, LX/JLx;-><init>(LX/5pX;)V

    .line 2681774
    new-instance v1, LX/JLs;

    invoke-direct {v1, p0}, LX/JLs;-><init>(Lcom/facebook/fbreact/perf/FrameLoggingFbReactScrollViewManager;)V

    .line 2681775
    iput-object v1, v0, LX/JLx;->a:LX/JLs;

    .line 2681776
    return-object v0
.end method


# virtual methods
.method public final synthetic a(LX/5rJ;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2681770
    invoke-direct {p0, p1}, Lcom/facebook/fbreact/perf/FrameLoggingFbReactScrollViewManager;->c(LX/5rJ;)LX/JLx;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(LX/5rJ;)LX/JLw;
    .locals 1

    .prologue
    .line 2681769
    invoke-direct {p0, p1}, Lcom/facebook/fbreact/perf/FrameLoggingFbReactScrollViewManager;->c(LX/5rJ;)LX/JLx;

    move-result-object v0

    return-object v0
.end method
