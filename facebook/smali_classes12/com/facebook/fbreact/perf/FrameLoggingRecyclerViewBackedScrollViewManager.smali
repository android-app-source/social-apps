.class public Lcom/facebook/fbreact/perf/FrameLoggingRecyclerViewBackedScrollViewManager;
.super Lcom/facebook/react/views/recyclerview/RecyclerViewBackedScrollViewManager;
.source ""


# instance fields
.field public b:Lcom/facebook/common/perftest/DrawFrameLogger;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2681953
    invoke-direct {p0}, Lcom/facebook/react/views/recyclerview/RecyclerViewBackedScrollViewManager;-><init>()V

    .line 2681954
    return-void
.end method

.method public constructor <init>(Lcom/facebook/common/perftest/DrawFrameLogger;)V
    .locals 0

    .prologue
    .line 2681960
    invoke-direct {p0}, Lcom/facebook/react/views/recyclerview/RecyclerViewBackedScrollViewManager;-><init>()V

    .line 2681961
    iput-object p1, p0, Lcom/facebook/fbreact/perf/FrameLoggingRecyclerViewBackedScrollViewManager;->b:Lcom/facebook/common/perftest/DrawFrameLogger;

    .line 2681962
    return-void
.end method


# virtual methods
.method public final synthetic a(LX/5rJ;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2681963
    invoke-virtual {p0, p1}, Lcom/facebook/fbreact/perf/FrameLoggingRecyclerViewBackedScrollViewManager;->b(LX/5rJ;)LX/K0o;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/5rJ;)LX/K0o;
    .locals 2

    .prologue
    .line 2681955
    iget-object v0, p0, Lcom/facebook/fbreact/perf/FrameLoggingRecyclerViewBackedScrollViewManager;->b:Lcom/facebook/common/perftest/DrawFrameLogger;

    if-nez v0, :cond_0

    .line 2681956
    invoke-static {}, LX/JLz;->a()Lcom/facebook/common/perftest/DrawFrameLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fbreact/perf/FrameLoggingRecyclerViewBackedScrollViewManager;->b:Lcom/facebook/common/perftest/DrawFrameLogger;

    .line 2681957
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/react/views/recyclerview/RecyclerViewBackedScrollViewManager;->b(LX/5rJ;)LX/K0o;

    move-result-object v0

    .line 2681958
    new-instance v1, LX/JLu;

    invoke-direct {v1, p0}, LX/JLu;-><init>(Lcom/facebook/fbreact/perf/FrameLoggingRecyclerViewBackedScrollViewManager;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setOnScrollListener(LX/1OX;)V

    .line 2681959
    return-object v0
.end method
