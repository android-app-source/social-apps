.class public Lcom/facebook/fbreact/perf/FrameLoggingReactScrollViewManager;
.super Lcom/facebook/react/views/scroll/ReactScrollViewManager;
.source ""


# instance fields
.field public b:Lcom/facebook/common/perftest/DrawFrameLogger;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2681859
    invoke-direct {p0}, Lcom/facebook/react/views/scroll/ReactScrollViewManager;-><init>()V

    .line 2681860
    return-void
.end method

.method public constructor <init>(Lcom/facebook/common/perftest/DrawFrameLogger;)V
    .locals 0

    .prologue
    .line 2681861
    invoke-direct {p0}, Lcom/facebook/react/views/scroll/ReactScrollViewManager;-><init>()V

    .line 2681862
    iput-object p1, p0, Lcom/facebook/fbreact/perf/FrameLoggingReactScrollViewManager;->b:Lcom/facebook/common/perftest/DrawFrameLogger;

    .line 2681863
    return-void
.end method


# virtual methods
.method public final synthetic a(LX/5rJ;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2681864
    invoke-virtual {p0, p1}, Lcom/facebook/react/views/scroll/ReactScrollViewManager;->b(LX/5rJ;)LX/FUq;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/5rJ;)LX/FUq;
    .locals 2

    .prologue
    .line 2681865
    iget-object v0, p0, Lcom/facebook/fbreact/perf/FrameLoggingReactScrollViewManager;->b:Lcom/facebook/common/perftest/DrawFrameLogger;

    if-nez v0, :cond_0

    .line 2681866
    invoke-static {}, LX/JLz;->a()Lcom/facebook/common/perftest/DrawFrameLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fbreact/perf/FrameLoggingReactScrollViewManager;->b:Lcom/facebook/common/perftest/DrawFrameLogger;

    .line 2681867
    :cond_0
    new-instance v0, LX/JLy;

    invoke-direct {v0, p1}, LX/JLy;-><init>(LX/5pX;)V

    .line 2681868
    new-instance v1, LX/JLt;

    invoke-direct {v1, p0}, LX/JLt;-><init>(Lcom/facebook/fbreact/perf/FrameLoggingReactScrollViewManager;)V

    .line 2681869
    iput-object v1, v0, LX/JLy;->a:LX/JLt;

    .line 2681870
    return-object v0
.end method
