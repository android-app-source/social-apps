.class public Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;
.super LX/9mi;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:LX/1Ad;

.field public final c:Lcom/facebook/http/common/FbHttpRequestProcessor;

.field public final d:LX/IC0;

.field public final e:LX/JMY;

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/JMU;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0s2;

.field public final h:LX/JJe;

.field private final i:LX/JKh;

.field private final j:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/98j;",
            ">;"
        }
    .end annotation
.end field

.field public final k:LX/98x;

.field private final l:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/JMV;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/JLR;

.field private final n:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/catalyst/modules/mobileconfig/MobileConfigModule;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2680350
    const-class v0, Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;

    const-string v1, "ReactNative"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1Ad;Lcom/facebook/http/common/FbHttpRequestProcessor;LX/IC0;LX/JMY;LX/0Or;LX/0s2;LX/JJe;LX/JKh;LX/0Or;LX/98x;LX/0Or;LX/JLR;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Ad;",
            "Lcom/facebook/http/common/FbHttpRequestProcessor;",
            "LX/IC0;",
            "LX/JMY;",
            "LX/0Or",
            "<",
            "LX/JMU;",
            ">;",
            "LX/0s2;",
            "LX/JJe;",
            "LX/JKh;",
            "LX/0Or",
            "<",
            "LX/98j;",
            ">;",
            "LX/98x;",
            "LX/0Or",
            "<",
            "LX/JMV;",
            ">;",
            "LX/JLR;",
            "LX/0Or",
            "<",
            "Lcom/facebook/catalyst/modules/mobileconfig/MobileConfigModule;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2680351
    invoke-direct {p0}, LX/9mi;-><init>()V

    .line 2680352
    iput-object p1, p0, Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;->b:LX/1Ad;

    .line 2680353
    iput-object p2, p0, Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;->c:Lcom/facebook/http/common/FbHttpRequestProcessor;

    .line 2680354
    iput-object p3, p0, Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;->d:LX/IC0;

    .line 2680355
    iput-object p4, p0, Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;->e:LX/JMY;

    .line 2680356
    iput-object p5, p0, Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;->f:LX/0Or;

    .line 2680357
    iput-object p6, p0, Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;->g:LX/0s2;

    .line 2680358
    iput-object p7, p0, Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;->h:LX/JJe;

    .line 2680359
    iput-object p8, p0, Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;->i:LX/JKh;

    .line 2680360
    iput-object p9, p0, Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;->j:LX/0Or;

    .line 2680361
    iput-object p10, p0, Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;->k:LX/98x;

    .line 2680362
    iput-object p11, p0, Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;->l:LX/0Or;

    .line 2680363
    iput-object p12, p0, Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;->m:LX/JLR;

    .line 2680364
    iput-object p13, p0, Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;->n:LX/0Or;

    .line 2680365
    return-void
.end method

.method public static b(LX/0QB;)Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;
    .locals 14

    .prologue
    .line 2680366
    new-instance v0, Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;

    invoke-static {p0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v1

    check-cast v1, LX/1Ad;

    invoke-static {p0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/0QB;)Lcom/facebook/http/common/FbHttpRequestProcessor;

    move-result-object v2

    check-cast v2, Lcom/facebook/http/common/FbHttpRequestProcessor;

    const-class v3, LX/IC0;

    invoke-interface {p0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/IC0;

    const-class v4, LX/JMY;

    invoke-interface {p0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/JMY;

    const/16 v5, 0x1c53

    invoke-static {p0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {p0}, LX/0s2;->b(LX/0QB;)LX/0s2;

    move-result-object v6

    check-cast v6, LX/0s2;

    const-class v7, LX/JJe;

    invoke-interface {p0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/JJe;

    .line 2680367
    new-instance v10, LX/JKh;

    invoke-static {p0}, LX/JK6;->a(LX/0QB;)LX/JK6;

    move-result-object v8

    check-cast v8, LX/JK6;

    invoke-static {p0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v9

    check-cast v9, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-direct {v10, v8, v9}, LX/JKh;-><init>(LX/JK6;Lcom/facebook/quicklog/QuickPerformanceLogger;)V

    .line 2680368
    move-object v8, v10

    .line 2680369
    check-cast v8, LX/JKh;

    const/16 v9, 0x1c3e

    invoke-static {p0, v9}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    const-class v10, LX/98x;

    invoke-interface {p0, v10}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/98x;

    const/16 v11, 0x1c54

    invoke-static {p0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    const-class v12, LX/JLR;

    invoke-interface {p0, v12}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/JLR;

    const/16 v13, 0x188e

    invoke-static {p0, v13}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    invoke-direct/range {v0 .. v13}, Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;-><init>(LX/1Ad;Lcom/facebook/http/common/FbHttpRequestProcessor;LX/IC0;LX/JMY;LX/0Or;LX/0s2;LX/JJe;LX/JKh;LX/0Or;LX/98x;LX/0Or;LX/JLR;LX/0Or;)V

    .line 2680370
    return-object v0
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/react/bridge/JavaScriptModule;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2680371
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/fbreact/interfaces/RCTViewEventEmitter;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/5pY;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5pY;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/5pS;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2680372
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2680373
    new-instance v1, LX/5pS;

    const-class v2, LX/JG6;

    new-instance v3, LX/JKJ;

    invoke-direct {v3, p0, p1}, LX/JKJ;-><init>(Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;LX/5pY;)V

    invoke-direct {v1, v2, v3}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2680374
    new-instance v1, LX/5pS;

    const-class v2, LX/Gd3;

    new-instance v3, LX/JKU;

    invoke-direct {v3, p0, p1}, LX/JKU;-><init>(Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;LX/5pY;)V

    invoke-direct {v1, v2, v3}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2680375
    new-instance v1, LX/5pS;

    const-class v2, LX/9nO;

    new-instance v3, LX/JKW;

    invoke-direct {v3, p0, p1}, LX/JKW;-><init>(Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;LX/5pY;)V

    invoke-direct {v1, v2, v3}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2680376
    new-instance v1, LX/5pS;

    const-class v2, LX/GVm;

    new-instance v3, LX/JKX;

    invoke-direct {v3, p0, p1}, LX/JKX;-><init>(Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;LX/5pY;)V

    invoke-direct {v1, v2, v3}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2680377
    new-instance v1, LX/5pS;

    const-class v2, LX/Jzf;

    new-instance v3, LX/JKY;

    invoke-direct {v3, p0, p1}, LX/JKY;-><init>(Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;LX/5pY;)V

    invoke-direct {v1, v2, v3}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2680378
    new-instance v1, LX/5pS;

    const-class v2, LX/Jzg;

    new-instance v3, LX/JKZ;

    invoke-direct {v3, p0, p1}, LX/JKZ;-><init>(Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;LX/5pY;)V

    invoke-direct {v1, v2, v3}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2680379
    new-instance v1, LX/5pS;

    const-class v2, LX/8xQ;

    new-instance v3, LX/JKa;

    invoke-direct {v3, p0, p1}, LX/JKa;-><init>(Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;LX/5pY;)V

    invoke-direct {v1, v2, v3}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2680380
    new-instance v1, LX/5pS;

    const-class v2, LX/Jzk;

    new-instance v3, LX/JKb;

    invoke-direct {v3, p0, p1}, LX/JKb;-><init>(Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;LX/5pY;)V

    invoke-direct {v1, v2, v3}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2680381
    new-instance v1, LX/5pS;

    const-class v2, LX/Jzq;

    new-instance v3, LX/JKc;

    invoke-direct {v3, p0, p1}, LX/JKc;-><init>(Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;LX/5pY;)V

    invoke-direct {v1, v2, v3}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2680382
    new-instance v1, LX/5pS;

    const-class v2, LX/JJd;

    new-instance v3, LX/JK9;

    invoke-direct {v3, p0, p1}, LX/JK9;-><init>(Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;LX/5pY;)V

    invoke-direct {v1, v2, v3}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2680383
    new-instance v1, LX/5pS;

    const-class v2, LX/JMT;

    new-instance v3, LX/JKA;

    invoke-direct {v3, p0, p1}, LX/JKA;-><init>(Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;LX/5pY;)V

    invoke-direct {v1, v2, v3}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2680384
    new-instance v1, LX/5pS;

    const-class v2, LX/98w;

    new-instance v3, LX/JKB;

    invoke-direct {v3, p0, p1}, LX/JKB;-><init>(Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;LX/5pY;)V

    invoke-direct {v1, v2, v3}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2680385
    new-instance v1, LX/5pS;

    const-class v2, LX/JMV;

    iget-object v3, p0, Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;->l:LX/0Or;

    invoke-direct {v1, v2, v3}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2680386
    new-instance v1, LX/5pS;

    const-class v2, LX/98j;

    iget-object v3, p0, Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;->j:LX/0Or;

    invoke-direct {v1, v2, v3}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2680387
    new-instance v1, LX/5pS;

    const-class v2, LX/GdJ;

    new-instance v3, LX/JKC;

    invoke-direct {v3, p0, p1}, LX/JKC;-><init>(Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;LX/5pY;)V

    invoke-direct {v1, v2, v3}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2680388
    new-instance v1, LX/5pS;

    const-class v2, LX/IBz;

    new-instance v3, LX/JKD;

    invoke-direct {v3, p0, p1}, LX/JKD;-><init>(Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;LX/5pY;)V

    invoke-direct {v1, v2, v3}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2680389
    new-instance v1, LX/5pS;

    const-class v2, LX/JMU;

    iget-object v3, p0, Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;->f:LX/0Or;

    invoke-direct {v1, v2, v3}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2680390
    new-instance v1, LX/5pS;

    const-class v2, LX/JMX;

    new-instance v3, LX/JKE;

    invoke-direct {v3, p0, p1}, LX/JKE;-><init>(Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;LX/5pY;)V

    invoke-direct {v1, v2, v3}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2680391
    new-instance v1, LX/5pS;

    const-class v2, LX/GVo;

    new-instance v3, LX/JKF;

    invoke-direct {v3, p0, p1}, LX/JKF;-><init>(Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;LX/5pY;)V

    invoke-direct {v1, v2, v3}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2680392
    new-instance v1, LX/5pS;

    const-class v2, LX/Jzs;

    new-instance v3, LX/JKG;

    invoke-direct {v3, p0, p1}, LX/JKG;-><init>(Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;LX/5pY;)V

    invoke-direct {v1, v2, v3}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2680393
    new-instance v1, LX/5pS;

    const-class v2, LX/JG7;

    new-instance v3, LX/JKH;

    invoke-direct {v3, p0, p1}, LX/JKH;-><init>(Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;LX/5pY;)V

    invoke-direct {v1, v2, v3}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2680394
    new-instance v1, LX/5pS;

    const-class v2, LX/5qZ;

    new-instance v3, LX/JKI;

    invoke-direct {v3, p0, p1}, LX/JKI;-><init>(Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;LX/5pY;)V

    invoke-direct {v1, v2, v3}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2680395
    new-instance v1, LX/5pS;

    const-class v2, LX/Jzu;

    new-instance v3, LX/JKK;

    invoke-direct {v3, p0, p1}, LX/JKK;-><init>(Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;LX/5pY;)V

    invoke-direct {v1, v2, v3}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2680396
    new-instance v1, LX/5pS;

    const-class v2, LX/JLQ;

    new-instance v3, LX/JKL;

    invoke-direct {v3, p0, p1}, LX/JKL;-><init>(Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;LX/5pY;)V

    invoke-direct {v1, v2, v3}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2680397
    new-instance v1, LX/5pS;

    const-class v2, LX/Jzz;

    new-instance v3, LX/JKM;

    invoke-direct {v3, p0, p1}, LX/JKM;-><init>(Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;LX/5pY;)V

    invoke-direct {v1, v2, v3}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2680398
    new-instance v1, LX/5pS;

    const-class v2, Lcom/facebook/catalyst/modules/mobileconfig/MobileConfigModule;

    iget-object v3, p0, Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;->n:LX/0Or;

    invoke-direct {v1, v2, v3}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2680399
    new-instance v1, LX/5pS;

    const-class v2, LX/JzR;

    new-instance v3, LX/JKN;

    invoke-direct {v3, p0, p1}, LX/JKN;-><init>(Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;LX/5pY;)V

    invoke-direct {v1, v2, v3}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2680400
    new-instance v1, LX/5pS;

    const-class v2, LX/K02;

    new-instance v3, LX/JKO;

    invoke-direct {v3, p0, p1}, LX/JKO;-><init>(Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;LX/5pY;)V

    invoke-direct {v1, v2, v3}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2680401
    new-instance v1, LX/5pS;

    const-class v2, LX/K05;

    new-instance v3, LX/JKP;

    invoke-direct {v3, p0, p1}, LX/JKP;-><init>(Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;LX/5pY;)V

    invoke-direct {v1, v2, v3}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2680402
    new-instance v1, LX/5pS;

    const-class v2, LX/8xe;

    new-instance v3, LX/JKQ;

    invoke-direct {v3, p0, p1}, LX/JKQ;-><init>(Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;LX/5pY;)V

    invoke-direct {v1, v2, v3}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2680403
    new-instance v1, LX/5pS;

    const-class v2, LX/JGL;

    new-instance v3, LX/JKR;

    invoke-direct {v3, p0, p1}, LX/JKR;-><init>(Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;LX/5pY;)V

    invoke-direct {v1, v2, v3}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2680404
    new-instance v1, LX/5pS;

    const-class v2, LX/K08;

    new-instance v3, LX/JKS;

    invoke-direct {v3, p0, p1}, LX/JKS;-><init>(Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;LX/5pY;)V

    invoke-direct {v1, v2, v3}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2680405
    new-instance v1, LX/5pS;

    const-class v2, LX/9nQ;

    new-instance v3, LX/JKT;

    invoke-direct {v3, p0, p1}, LX/JKT;-><init>(Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;LX/5pY;)V

    invoke-direct {v1, v2, v3}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2680406
    sget-boolean v1, LX/0AN;->a:Z

    if-nez v1, :cond_0

    sget-boolean v1, LX/0AN;->b:Z

    if-eqz v1, :cond_1

    .line 2680407
    :cond_0
    new-instance v1, LX/5pS;

    const-class v2, LX/K0A;

    new-instance v3, LX/JKV;

    invoke-direct {v3, p0, p1}, LX/JKV;-><init>(Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;LX/5pY;)V

    invoke-direct {v1, v2, v3}, LX/5pS;-><init>(Ljava/lang/Class;LX/0Or;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2680408
    :cond_1
    return-object v0
.end method

.method public final b()LX/5qR;
    .locals 1

    .prologue
    .line 2680409
    new-instance v0, LX/JK8;

    invoke-direct {v0}, LX/JK8;-><init>()V

    return-object v0
.end method

.method public final c(LX/5pY;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5pY;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/react/uimanager/ViewManager;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2680410
    const/16 v0, 0x21

    new-array v0, v0, [Lcom/facebook/react/uimanager/ViewManager;

    const/4 v1, 0x0

    invoke-static {}, Lcom/facebook/react/views/art/ARTRenderableViewManager;->h()Lcom/facebook/react/views/art/ARTRenderableViewManager;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-static {}, Lcom/facebook/react/views/art/ARTRenderableViewManager;->t()Lcom/facebook/react/views/art/ARTRenderableViewManager;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-static {}, Lcom/facebook/react/views/art/ARTRenderableViewManager;->u()Lcom/facebook/react/views/art/ARTRenderableViewManager;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, Lcom/facebook/react/views/art/ARTSurfaceViewManager;

    invoke-direct {v2}, Lcom/facebook/react/views/art/ARTSurfaceViewManager;-><init>()V

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-instance v2, Lcom/facebook/fbreact/views/fbperflogger/FbReactPerfLoggerFlagManager;

    iget-object v3, p0, Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;->i:LX/JKh;

    invoke-direct {v2, v3}, Lcom/facebook/fbreact/views/fbperflogger/FbReactPerfLoggerFlagManager;-><init>(LX/JKh;)V

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-instance v2, Lcom/facebook/fbreact/views/shimmer/FbReactShimmerFrameLayoutManager;

    invoke-direct {v2}, Lcom/facebook/fbreact/views/shimmer/FbReactShimmerFrameLayoutManager;-><init>()V

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-instance v2, Lcom/facebook/fbreact/views/fbswitchcompat/FbReactSwitchCompatManager;

    invoke-direct {v2}, Lcom/facebook/fbreact/views/fbswitchcompat/FbReactSwitchCompatManager;-><init>()V

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-instance v2, Lcom/facebook/fbreact/views/fbvpvlogger/FbReactVpvLoggerFlagManager;

    invoke-direct {v2}, Lcom/facebook/fbreact/views/fbvpvlogger/FbReactVpvLoggerFlagManager;-><init>()V

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-instance v2, Lcom/facebook/catalyst/views/gradient/ReactAxialGradientManager;

    invoke-direct {v2}, Lcom/facebook/catalyst/views/gradient/ReactAxialGradientManager;-><init>()V

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-instance v2, Lcom/facebook/react/views/picker/ReactDialogPickerManager;

    invoke-direct {v2}, Lcom/facebook/react/views/picker/ReactDialogPickerManager;-><init>()V

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-instance v2, Lcom/facebook/react/views/drawer/ReactDrawerLayoutManager;

    invoke-direct {v2}, Lcom/facebook/react/views/drawer/ReactDrawerLayoutManager;-><init>()V

    aput-object v2, v0, v1

    const/16 v1, 0xb

    new-instance v2, Lcom/facebook/react/views/picker/ReactDropdownPickerManager;

    invoke-direct {v2}, Lcom/facebook/react/views/picker/ReactDropdownPickerManager;-><init>()V

    aput-object v2, v0, v1

    const/16 v1, 0xc

    new-instance v2, Lcom/facebook/react/views/scroll/ReactHorizontalScrollViewManager;

    invoke-direct {v2}, Lcom/facebook/react/views/scroll/ReactHorizontalScrollViewManager;-><init>()V

    aput-object v2, v0, v1

    const/16 v1, 0xd

    new-instance v2, Lcom/facebook/react/views/text/frescosupport/FrescoBasedReactTextInlineImageViewManager;

    iget-object v3, p0, Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;->b:LX/1Ad;

    sget-object v4, Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v2, v3, v4}, Lcom/facebook/react/views/text/frescosupport/FrescoBasedReactTextInlineImageViewManager;-><init>(LX/1Ae;Ljava/lang/Object;)V

    aput-object v2, v0, v1

    const/16 v1, 0xe

    new-instance v2, Lcom/facebook/react/views/image/ReactImageManager;

    iget-object v3, p0, Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;->b:LX/1Ad;

    sget-object v4, Lcom/facebook/fbreact/fb4a/Fb4aReactInfraPackage;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v2, v3, v4}, Lcom/facebook/react/views/image/ReactImageManager;-><init>(LX/1Ae;Ljava/lang/Object;)V

    aput-object v2, v0, v1

    const/16 v1, 0xf

    new-instance v2, Lcom/facebook/catalyst/views/maps/ReactFbMapViewManager;

    invoke-direct {v2}, Lcom/facebook/catalyst/views/maps/ReactFbMapViewManager;-><init>()V

    aput-object v2, v0, v1

    const/16 v1, 0x10

    new-instance v2, Lcom/facebook/catalyst/views/maps/ReactMapViewManager;

    invoke-direct {v2, p1}, Lcom/facebook/catalyst/views/maps/ReactMapViewManager;-><init>(LX/5pX;)V

    aput-object v2, v0, v1

    const/16 v1, 0x11

    new-instance v2, Lcom/facebook/react/views/modal/ReactModalHostManager;

    invoke-direct {v2}, Lcom/facebook/react/views/modal/ReactModalHostManager;-><init>()V

    aput-object v2, v0, v1

    const/16 v1, 0x12

    new-instance v2, Lcom/facebook/fbreact/views/photoviewer/ReactPhotoViewerManager;

    invoke-direct {v2}, Lcom/facebook/fbreact/views/photoviewer/ReactPhotoViewerManager;-><init>()V

    aput-object v2, v0, v1

    const/16 v1, 0x13

    new-instance v2, Lcom/facebook/react/views/progressbar/ReactProgressBarViewManager;

    invoke-direct {v2}, Lcom/facebook/react/views/progressbar/ReactProgressBarViewManager;-><init>()V

    aput-object v2, v0, v1

    const/16 v1, 0x14

    new-instance v2, Lcom/facebook/react/views/text/ReactRawTextManager;

    invoke-direct {v2}, Lcom/facebook/react/views/text/ReactRawTextManager;-><init>()V

    aput-object v2, v0, v1

    const/16 v1, 0x15

    new-instance v2, Lcom/facebook/react/views/scroll/ReactScrollViewManager;

    invoke-direct {v2}, Lcom/facebook/react/views/scroll/ReactScrollViewManager;-><init>()V

    aput-object v2, v0, v1

    const/16 v1, 0x16

    new-instance v2, Lcom/facebook/react/views/textinput/ReactTextInputManager;

    invoke-direct {v2}, Lcom/facebook/react/views/textinput/ReactTextInputManager;-><init>()V

    aput-object v2, v0, v1

    const/16 v1, 0x17

    new-instance v2, Lcom/facebook/react/views/text/ReactTextViewManager;

    invoke-direct {v2}, Lcom/facebook/react/views/text/ReactTextViewManager;-><init>()V

    aput-object v2, v0, v1

    const/16 v1, 0x18

    new-instance v2, Lcom/facebook/catalyst/views/video/ReactVideoManager;

    invoke-direct {v2}, Lcom/facebook/catalyst/views/video/ReactVideoManager;-><init>()V

    aput-object v2, v0, v1

    const/16 v1, 0x19

    new-instance v2, Lcom/facebook/react/views/view/ReactViewManager;

    invoke-direct {v2}, Lcom/facebook/react/views/view/ReactViewManager;-><init>()V

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    new-instance v2, Lcom/facebook/react/views/viewpager/ReactViewPagerManager;

    invoke-direct {v2}, Lcom/facebook/react/views/viewpager/ReactViewPagerManager;-><init>()V

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    new-instance v2, Lcom/facebook/react/views/text/ReactVirtualTextViewManager;

    invoke-direct {v2}, Lcom/facebook/react/views/text/ReactVirtualTextViewManager;-><init>()V

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    new-instance v2, Lcom/facebook/react/views/webview/ReactWebViewManager;

    invoke-direct {v2}, Lcom/facebook/react/views/webview/ReactWebViewManager;-><init>()V

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    new-instance v2, Lcom/facebook/fbreactcomponents/stickers/StickerViewInputManager;

    invoke-direct {v2}, Lcom/facebook/fbreactcomponents/stickers/StickerViewInputManager;-><init>()V

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    new-instance v2, Lcom/facebook/fbreactcomponents/stickers/StickerViewManager;

    invoke-direct {v2}, Lcom/facebook/fbreactcomponents/stickers/StickerViewManager;-><init>()V

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    new-instance v2, Lcom/facebook/react/views/swiperefresh/SwipeRefreshLayoutManager;

    invoke-direct {v2}, Lcom/facebook/react/views/swiperefresh/SwipeRefreshLayoutManager;-><init>()V

    aput-object v2, v0, v1

    const/16 v1, 0x20

    new-instance v2, Lcom/facebook/groups/fb4a/react/ReactGYSCViewManager;

    invoke-direct {v2}, Lcom/facebook/groups/fb4a/react/ReactGYSCViewManager;-><init>()V

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
