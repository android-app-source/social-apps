.class public final Lcom/facebook/fbreact/devicerequests/DeviceRequestsNativeModule$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Landroid/content/Intent;

.field public final synthetic b:I

.field public final synthetic c:LX/JJn;


# direct methods
.method public constructor <init>(LX/JJn;Landroid/content/Intent;I)V
    .locals 0

    .prologue
    .line 2679793
    iput-object p1, p0, Lcom/facebook/fbreact/devicerequests/DeviceRequestsNativeModule$2;->c:LX/JJn;

    iput-object p2, p0, Lcom/facebook/fbreact/devicerequests/DeviceRequestsNativeModule$2;->a:Landroid/content/Intent;

    iput p3, p0, Lcom/facebook/fbreact/devicerequests/DeviceRequestsNativeModule$2;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2679794
    iget-object v0, p0, Lcom/facebook/fbreact/devicerequests/DeviceRequestsNativeModule$2;->a:Landroid/content/Intent;

    if-nez v0, :cond_1

    move-object v0, v1

    .line 2679795
    :goto_0
    iget-object v2, p0, Lcom/facebook/fbreact/devicerequests/DeviceRequestsNativeModule$2;->a:Landroid/content/Intent;

    if-eqz v2, :cond_3

    iget v2, p0, Lcom/facebook/fbreact/devicerequests/DeviceRequestsNativeModule$2;->b:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_3

    .line 2679796
    iget-object v2, p0, Lcom/facebook/fbreact/devicerequests/DeviceRequestsNativeModule$2;->a:Landroid/content/Intent;

    const-string v3, "error_message"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2679797
    iget-object v3, p0, Lcom/facebook/fbreact/devicerequests/DeviceRequestsNativeModule$2;->a:Landroid/content/Intent;

    const-string v4, "access_token"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2679798
    if-nez v0, :cond_0

    if-eqz v2, :cond_2

    .line 2679799
    :cond_0
    iget-object v3, p0, Lcom/facebook/fbreact/devicerequests/DeviceRequestsNativeModule$2;->c:LX/JJn;

    iget-object v3, v3, LX/JJn;->c:LX/5pW;

    invoke-interface {v3, v0, v2}, LX/5pW;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2679800
    :goto_1
    iget-object v0, p0, Lcom/facebook/fbreact/devicerequests/DeviceRequestsNativeModule$2;->c:LX/JJn;

    .line 2679801
    iput-object v1, v0, LX/JJn;->c:LX/5pW;

    .line 2679802
    return-void

    .line 2679803
    :cond_1
    iget-object v0, p0, Lcom/facebook/fbreact/devicerequests/DeviceRequestsNativeModule$2;->a:Landroid/content/Intent;

    const-string v2, "error_code"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2679804
    :cond_2
    iget-object v0, p0, Lcom/facebook/fbreact/devicerequests/DeviceRequestsNativeModule$2;->c:LX/JJn;

    iget-object v0, v0, LX/JJn;->c:LX/5pW;

    invoke-interface {v0, v3}, LX/5pW;->a(Ljava/lang/Object;)V

    goto :goto_1

    .line 2679805
    :cond_3
    iget-object v2, p0, Lcom/facebook/fbreact/devicerequests/DeviceRequestsNativeModule$2;->c:LX/JJn;

    iget-object v2, v2, LX/JJn;->c:LX/5pW;

    if-nez v0, :cond_4

    const-string v0, "-1"

    :cond_4
    const-string v3, "User Cancelled"

    invoke-interface {v2, v0, v3}, LX/5pW;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
