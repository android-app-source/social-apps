.class public Lcom/facebook/installnotifier/InstallNotifierService;
.super LX/1ZN;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final h:Ljava/lang/String;

.field private static i:LX/1ql;


# instance fields
.field public a:Landroid/content/Context;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/IX4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/2Af;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0hw;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/11H;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2585533
    const-class v0, Lcom/facebook/installnotifier/InstallNotifierService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/installnotifier/InstallNotifierService;->h:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2585534
    const-string v0, "InstallNotifierService"

    invoke-direct {p0, v0}, LX/1ZN;-><init>(Ljava/lang/String;)V

    .line 2585535
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;LX/1r1;)V
    .locals 2

    .prologue
    .line 2585523
    sget-object v0, Lcom/facebook/installnotifier/InstallNotifierService;->i:LX/1ql;

    if-nez v0, :cond_0

    if-eqz p2, :cond_0

    .line 2585524
    const/4 v0, 0x1

    sget-object v1, Lcom/facebook/installnotifier/InstallNotifierService;->h:Ljava/lang/String;

    invoke-virtual {p2, v0, v1}, LX/1r1;->a(ILjava/lang/String;)LX/1ql;

    move-result-object v0

    sput-object v0, Lcom/facebook/installnotifier/InstallNotifierService;->i:LX/1ql;

    .line 2585525
    :cond_0
    sget-object v0, Lcom/facebook/installnotifier/InstallNotifierService;->i:LX/1ql;

    invoke-virtual {v0}, LX/1ql;->c()V

    .line 2585526
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2585527
    invoke-virtual {v0, p1}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    .line 2585528
    const-class v1, Lcom/facebook/installnotifier/InstallNotifierService;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 2585529
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 2585530
    :goto_0
    return-void

    .line 2585531
    :catch_0
    sget-object v0, Lcom/facebook/installnotifier/InstallNotifierService;->i:LX/1ql;

    invoke-virtual {v0}, LX/1ql;->d()V

    goto :goto_0
.end method

.method private static a(Lcom/facebook/installnotifier/InstallNotifierService;Landroid/content/Context;LX/03V;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/IX4;LX/2Af;LX/0hw;LX/11H;)V
    .locals 0

    .prologue
    .line 2585532
    iput-object p1, p0, Lcom/facebook/installnotifier/InstallNotifierService;->a:Landroid/content/Context;

    iput-object p2, p0, Lcom/facebook/installnotifier/InstallNotifierService;->b:LX/03V;

    iput-object p3, p0, Lcom/facebook/installnotifier/InstallNotifierService;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p4, p0, Lcom/facebook/installnotifier/InstallNotifierService;->d:LX/IX4;

    iput-object p5, p0, Lcom/facebook/installnotifier/InstallNotifierService;->e:LX/2Af;

    iput-object p6, p0, Lcom/facebook/installnotifier/InstallNotifierService;->f:LX/0hw;

    iput-object p7, p0, Lcom/facebook/installnotifier/InstallNotifierService;->g:LX/11H;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 8

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v7

    move-object v0, p0

    check-cast v0, Lcom/facebook/installnotifier/InstallNotifierService;

    const-class v1, Landroid/content/Context;

    invoke-interface {v7, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {v7}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-static {v7}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    new-instance v4, LX/IX4;

    invoke-direct {v4}, LX/IX4;-><init>()V

    move-object v4, v4

    move-object v4, v4

    check-cast v4, LX/IX4;

    invoke-static {v7}, LX/2Af;->b(LX/0QB;)LX/2Af;

    move-result-object v5

    check-cast v5, LX/2Af;

    invoke-static {v7}, LX/0hw;->b(LX/0QB;)LX/0hw;

    move-result-object v6

    check-cast v6, LX/0hw;

    invoke-static {v7}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v7

    check-cast v7, LX/11H;

    invoke-static/range {v0 .. v7}, Lcom/facebook/installnotifier/InstallNotifierService;->a(Lcom/facebook/installnotifier/InstallNotifierService;Landroid/content/Context;LX/03V;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/IX4;LX/2Af;LX/0hw;LX/11H;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 13

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x24

    const v2, 0x5357b663

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2585490
    :try_start_0
    iget-object v0, p0, Lcom/facebook/installnotifier/InstallNotifierService;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/2Af;->a:LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2585491
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 2585492
    iget-object v2, p0, Lcom/facebook/installnotifier/InstallNotifierService;->f:LX/0hw;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, LX/0hw;->a(Z)Ljava/lang/String;

    move-result-object v2

    .line 2585493
    const-string v3, "notifCnt"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 2585494
    const-string v4, "interval"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 2585495
    iget-object v4, p0, Lcom/facebook/installnotifier/InstallNotifierService;->g:LX/11H;

    iget-object v5, p0, Lcom/facebook/installnotifier/InstallNotifierService;->d:LX/IX4;

    new-instance v6, LX/IX2;

    invoke-direct {v6, v2, v3, v0}, LX/IX2;-><init>(Ljava/lang/String;II)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    invoke-virtual {v4, v5, v6, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/IX3;

    .line 2585496
    iget-boolean v2, v0, LX/IX3;->a:Z

    if-eqz v2, :cond_0

    .line 2585497
    iget-object v2, p0, Lcom/facebook/installnotifier/InstallNotifierService;->a:Landroid/content/Context;

    iget-object v4, v0, LX/IX3;->d:Ljava/lang/String;

    iget-object v5, v0, LX/IX3;->e:Ljava/lang/String;

    .line 2585498
    new-instance v7, Landroid/content/Intent;

    const-class v8, Lcom/facebook/katana/activity/FbMainTabActivity;

    invoke-direct {v7, v2, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2585499
    const/4 v8, 0x0

    const/high16 v9, 0x8000000

    invoke-static {v2, v8, v7, v9}, LX/0nt;->a(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v7

    .line 2585500
    sget-object v8, LX/0SF;->a:LX/0SF;

    move-object v8, v8

    .line 2585501
    invoke-virtual {v8}, LX/0SF;->a()J

    move-result-wide v9

    const-wide/16 v11, 0x3e8

    div-long/2addr v9, v11

    long-to-int v8, v9

    .line 2585502
    new-instance v9, LX/2HB;

    invoke-direct {v9, v2}, LX/2HB;-><init>(Landroid/content/Context;)V

    invoke-virtual {v9, v4}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v9

    invoke-virtual {v9, v5}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v9

    .line 2585503
    iput-object v7, v9, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 2585504
    move-object v7, v9

    .line 2585505
    const v9, 0x7f0218e4

    invoke-virtual {v7, v9}, LX/2HB;->a(I)LX/2HB;

    move-result-object v7

    const/4 v9, -0x1

    .line 2585506
    iput v9, v7, LX/2HB;->j:I

    .line 2585507
    move-object v7, v7

    .line 2585508
    const/4 v9, 0x1

    invoke-virtual {v7, v9}, LX/2HB;->c(Z)LX/2HB;

    move-result-object v7

    invoke-virtual {v7, v5}, LX/2HB;->e(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v9

    .line 2585509
    const-string v7, "notification"

    invoke-virtual {v2, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/app/NotificationManager;

    .line 2585510
    invoke-virtual {v9}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 2585511
    :cond_0
    iget-boolean v2, v0, LX/IX3;->b:Z

    if-eqz v2, :cond_1

    .line 2585512
    iget-object v2, p0, Lcom/facebook/installnotifier/InstallNotifierService;->e:LX/2Af;

    iget-object v4, p0, Lcom/facebook/installnotifier/InstallNotifierService;->a:Landroid/content/Context;

    iget v0, v0, LX/IX3;->c:I

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v4, v0, v3}, LX/2Af;->a(Landroid/content/Context;II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2585513
    :cond_1
    sget-object v0, Lcom/facebook/installnotifier/InstallNotifierService;->i:LX/1ql;

    if-eqz v0, :cond_2

    .line 2585514
    sget-object v0, Lcom/facebook/installnotifier/InstallNotifierService;->i:LX/1ql;

    invoke-virtual {v0}, LX/1ql;->d()V

    .line 2585515
    :goto_0
    const v0, 0x70972d44

    invoke-static {v0, v1}, LX/02F;->d(II)V

    return-void

    .line 2585516
    :cond_2
    iget-object v0, p0, Lcom/facebook/installnotifier/InstallNotifierService;->b:LX/03V;

    sget-object v2, Lcom/facebook/installnotifier/InstallNotifierService;->h:Ljava/lang/String;

    const-string v3, "wakelock is null and cannot be released"

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2585517
    :catch_0
    sget-object v0, Lcom/facebook/installnotifier/InstallNotifierService;->i:LX/1ql;

    if-eqz v0, :cond_3

    .line 2585518
    sget-object v0, Lcom/facebook/installnotifier/InstallNotifierService;->i:LX/1ql;

    invoke-virtual {v0}, LX/1ql;->d()V

    goto :goto_0

    .line 2585519
    :cond_3
    iget-object v0, p0, Lcom/facebook/installnotifier/InstallNotifierService;->b:LX/03V;

    sget-object v2, Lcom/facebook/installnotifier/InstallNotifierService;->h:Ljava/lang/String;

    const-string v3, "wakelock is null and cannot be released"

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2585520
    :catchall_0
    move-exception v0

    sget-object v2, Lcom/facebook/installnotifier/InstallNotifierService;->i:LX/1ql;

    if-eqz v2, :cond_4

    .line 2585521
    sget-object v2, Lcom/facebook/installnotifier/InstallNotifierService;->i:LX/1ql;

    invoke-virtual {v2}, LX/1ql;->d()V

    .line 2585522
    :goto_1
    const v2, 0x2ce824d3

    invoke-static {v2, v1}, LX/02F;->d(II)V

    throw v0

    :cond_4
    iget-object v2, p0, Lcom/facebook/installnotifier/InstallNotifierService;->b:LX/03V;

    sget-object v3, Lcom/facebook/installnotifier/InstallNotifierService;->h:Ljava/lang/String;

    const-string v4, "wakelock is null and cannot be released"

    invoke-virtual {v2, v3, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x591f2f8c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2585487
    invoke-super {p0}, LX/1ZN;->onCreate()V

    .line 2585488
    invoke-static {p0, p0}, Lcom/facebook/installnotifier/InstallNotifierService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2585489
    const/16 v1, 0x25

    const v2, -0x6b1240d6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
