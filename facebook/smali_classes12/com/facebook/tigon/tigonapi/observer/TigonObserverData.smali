.class public Lcom/facebook/tigon/tigonapi/observer/TigonObserverData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/tigon/tigonapi/observer/TigonRequestErrored;


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field private a:J

.field private b:Lcom/facebook/tigon/iface/TigonRequest;

.field private c:I

.field private d:Lcom/facebook/tigon/iface/TigonRequest;

.field private e:LX/1pB;

.field private f:LX/1pK;

.field private g:Lcom/facebook/tigon/tigonapi/TigonError;


# direct methods
.method private constructor <init>(JLcom/facebook/tigon/iface/TigonRequest;Ljava/nio/ByteBuffer;)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2487128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2487129
    iput-wide p1, p0, Lcom/facebook/tigon/tigonapi/observer/TigonObserverData;->a:J

    .line 2487130
    iput-object p3, p0, Lcom/facebook/tigon/tigonapi/observer/TigonObserverData;->b:Lcom/facebook/tigon/iface/TigonRequest;

    .line 2487131
    iget-object v0, p0, Lcom/facebook/tigon/tigonapi/observer/TigonObserverData;->b:Lcom/facebook/tigon/iface/TigonRequest;

    if-nez v0, :cond_0

    .line 2487132
    invoke-static {p4}, LX/1j4;->a(Ljava/nio/ByteBuffer;)Lcom/facebook/tigon/iface/TigonRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tigon/tigonapi/observer/TigonObserverData;->b:Lcom/facebook/tigon/iface/TigonRequest;

    .line 2487133
    :cond_0
    return-void
.end method

.method private onFinished(Ljava/nio/ByteBuffer;)V
    .locals 2
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2487124
    invoke-static {p1}, LX/1j4;->c(Ljava/nio/ByteBuffer;)Landroid/util/Pair;

    move-result-object v1

    .line 2487125
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/tigon/tigonapi/TigonError;

    iput-object v0, p0, Lcom/facebook/tigon/tigonapi/observer/TigonObserverData;->g:Lcom/facebook/tigon/tigonapi/TigonError;

    .line 2487126
    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, LX/1pK;

    iput-object v0, p0, Lcom/facebook/tigon/tigonapi/observer/TigonObserverData;->f:LX/1pK;

    .line 2487127
    return-void
.end method

.method private onResponse(Ljava/nio/ByteBuffer;)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2487122
    invoke-static {p1}, LX/1j4;->b(Ljava/nio/ByteBuffer;)LX/1pB;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tigon/tigonapi/observer/TigonObserverData;->e:LX/1pB;

    .line 2487123
    return-void
.end method

.method private onStarted(ILjava/nio/ByteBuffer;)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2487119
    iput p1, p0, Lcom/facebook/tigon/tigonapi/observer/TigonObserverData;->c:I

    .line 2487120
    invoke-static {p2}, LX/1j4;->a(Ljava/nio/ByteBuffer;)Lcom/facebook/tigon/iface/TigonRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/tigon/tigonapi/observer/TigonObserverData;->d:Lcom/facebook/tigon/iface/TigonRequest;

    .line 2487121
    return-void
.end method


# virtual methods
.method public final attempts()I
    .locals 1

    .prologue
    .line 2487134
    iget v0, p0, Lcom/facebook/tigon/tigonapi/observer/TigonObserverData;->c:I

    return v0
.end method

.method public final error()Lcom/facebook/tigon/tigonapi/TigonError;
    .locals 1

    .prologue
    .line 2487118
    iget-object v0, p0, Lcom/facebook/tigon/tigonapi/observer/TigonObserverData;->g:Lcom/facebook/tigon/tigonapi/TigonError;

    return-object v0
.end method

.method public final requestId()J
    .locals 2

    .prologue
    .line 2487117
    iget-wide v0, p0, Lcom/facebook/tigon/tigonapi/observer/TigonObserverData;->a:J

    return-wide v0
.end method

.method public final response()LX/1pB;
    .locals 1

    .prologue
    .line 2487116
    iget-object v0, p0, Lcom/facebook/tigon/tigonapi/observer/TigonObserverData;->e:LX/1pB;

    return-object v0
.end method

.method public final submittedRequest()Lcom/facebook/tigon/iface/TigonRequest;
    .locals 1

    .prologue
    .line 2487115
    iget-object v0, p0, Lcom/facebook/tigon/tigonapi/observer/TigonObserverData;->b:Lcom/facebook/tigon/iface/TigonRequest;

    return-object v0
.end method

.method public final summary()LX/1pK;
    .locals 1

    .prologue
    .line 2487114
    iget-object v0, p0, Lcom/facebook/tigon/tigonapi/observer/TigonObserverData;->f:LX/1pK;

    return-object v0
.end method
