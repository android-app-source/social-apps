.class public final Lcom/facebook/tigon/httpclientadapter/TigonHttpEntityBodyProvider$EntityReader;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/HcQ;

.field private final b:Lcom/facebook/tigon/tigonapi/TigonBodyStream;


# direct methods
.method public constructor <init>(LX/HcQ;Lcom/facebook/tigon/tigonapi/TigonBodyStream;)V
    .locals 0

    .prologue
    .line 2487064
    iput-object p1, p0, Lcom/facebook/tigon/httpclientadapter/TigonHttpEntityBodyProvider$EntityReader;->a:LX/HcQ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2487065
    iput-object p2, p0, Lcom/facebook/tigon/httpclientadapter/TigonHttpEntityBodyProvider$EntityReader;->b:Lcom/facebook/tigon/tigonapi/TigonBodyStream;

    .line 2487066
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 2487067
    :try_start_0
    new-instance v0, LX/HcP;

    iget-object v1, p0, Lcom/facebook/tigon/httpclientadapter/TigonHttpEntityBodyProvider$EntityReader;->a:LX/HcQ;

    iget-object v2, p0, Lcom/facebook/tigon/httpclientadapter/TigonHttpEntityBodyProvider$EntityReader;->b:Lcom/facebook/tigon/tigonapi/TigonBodyStream;

    invoke-direct {v0, v1, v2}, LX/HcP;-><init>(LX/HcQ;Lcom/facebook/tigon/tigonapi/TigonBodyStream;)V

    .line 2487068
    iget-object v1, p0, Lcom/facebook/tigon/httpclientadapter/TigonHttpEntityBodyProvider$EntityReader;->a:LX/HcQ;

    iget-object v1, v1, LX/HcQ;->a:Lorg/apache/http/HttpEntity;

    invoke-interface {v1, v0}, Lorg/apache/http/HttpEntity;->writeTo(Ljava/io/OutputStream;)V

    .line 2487069
    invoke-virtual {v0}, LX/HcP;->a()V

    .line 2487070
    iget-boolean v1, v0, LX/HcP;->d:Z

    move v0, v1

    .line 2487071
    if-nez v0, :cond_0

    .line 2487072
    iget-object v0, p0, Lcom/facebook/tigon/httpclientadapter/TigonHttpEntityBodyProvider$EntityReader;->b:Lcom/facebook/tigon/tigonapi/TigonBodyStream;

    invoke-interface {v0}, Lcom/facebook/tigon/tigonapi/TigonBodyStream;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2487073
    :cond_0
    :goto_0
    return-void

    .line 2487074
    :catch_0
    move-exception v0

    .line 2487075
    iget-object v1, p0, Lcom/facebook/tigon/httpclientadapter/TigonHttpEntityBodyProvider$EntityReader;->b:Lcom/facebook/tigon/tigonapi/TigonBodyStream;

    new-instance v2, Lcom/facebook/tigon/tigonapi/TigonError;

    const/4 v3, 0x3

    const-string v4, "TigonHttpEntityBodyProviderDomain"

    const/4 v5, 0x0

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v4, v5, v0}, Lcom/facebook/tigon/tigonapi/TigonError;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    invoke-interface {v1, v2}, Lcom/facebook/tigon/tigonapi/TigonBodyStream;->a(Lcom/facebook/tigon/tigonapi/TigonError;)V

    goto :goto_0
.end method
