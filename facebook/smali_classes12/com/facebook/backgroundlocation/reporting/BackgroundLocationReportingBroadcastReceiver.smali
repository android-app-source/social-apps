.class public Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingBroadcastReceiver;
.super LX/25h;
.source ""


# static fields
.field private static final h:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Uo;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/2Cx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/2Cw;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/2Ct;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/2Hc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/2Hb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2498451
    const-class v0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingBroadcastReceiver;

    sput-object v0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingBroadcastReceiver;->h:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 2498449
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "BACKGROUND_LOCATION_REPORTING_SETTINGS_REQUEST_REFRESH_ACTION"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "BACKGROUND_LOCATION_REPORTING_ACTION_FETCH_IS_ENABLED_FINISHED"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "BACKGROUND_LOCATION_REPORTING_SETTINGS_CHANGED_ACTION"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "BACKGROUND_LOCATION_REPORTING_ACTION_LOCATION_UPDATE"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "BACKGROUND_LOCATION_REPORTING_ACTION_WRITE_FINISHED"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "BACKGROUND_LOCATION_REPORTING_ACTION_OBTAIN_SINGLE_LOCATION_FINISHED"

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, LX/25h;-><init>([Ljava/lang/String;)V

    .line 2498450
    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 2498435
    const-string v0, "expected_location_history_setting"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2498436
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "request refresh action did not have parameter"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2498437
    :cond_0
    const-string v0, "expected_location_history_setting"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 2498438
    iget-object v1, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingBroadcastReceiver;->d:LX/2Cw;

    .line 2498439
    invoke-static {v1}, LX/2Cw;->d(LX/2Cw;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 2498440
    :cond_1
    :goto_0
    return-void

    .line 2498441
    :cond_2
    invoke-static {v1}, LX/2Cw;->b(LX/2Cw;)LX/03R;

    move-result-object p0

    .line 2498442
    const/4 v2, 0x0

    .line 2498443
    sget-object p1, LX/03R;->UNSET:LX/03R;

    if-ne p0, p1, :cond_4

    .line 2498444
    sget-object v2, LX/HlN;->FIRST:LX/HlN;

    .line 2498445
    :cond_3
    :goto_1
    if-eqz v2, :cond_1

    .line 2498446
    invoke-static {v1, v2}, LX/2Cw;->a(LX/2Cw;LX/HlN;)V

    goto :goto_0

    .line 2498447
    :cond_4
    invoke-virtual {p0}, LX/03R;->asBoolean()Z

    move-result p0

    if-eq p0, v0, :cond_3

    .line 2498448
    sget-object v2, LX/HlN;->HINT:LX/HlN;

    goto :goto_1
.end method

.method private static a(Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingBroadcastReceiver;Ljava/util/concurrent/ExecutorService;LX/0Uo;LX/2Cx;LX/2Cw;LX/2Ct;LX/2Hc;LX/2Hb;)V
    .locals 0

    .prologue
    .line 2498434
    iput-object p1, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingBroadcastReceiver;->a:Ljava/util/concurrent/ExecutorService;

    iput-object p2, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingBroadcastReceiver;->b:LX/0Uo;

    iput-object p3, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingBroadcastReceiver;->c:LX/2Cx;

    iput-object p4, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingBroadcastReceiver;->d:LX/2Cw;

    iput-object p5, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingBroadcastReceiver;->e:LX/2Ct;

    iput-object p6, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingBroadcastReceiver;->f:LX/2Hc;

    iput-object p7, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingBroadcastReceiver;->g:LX/2Hb;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 8

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v7

    move-object v0, p0

    check-cast v0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingBroadcastReceiver;

    invoke-static {v7}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ExecutorService;

    invoke-static {v7}, LX/0Uo;->a(LX/0QB;)LX/0Uo;

    move-result-object v2

    check-cast v2, LX/0Uo;

    invoke-static {v7}, LX/2Cx;->a(LX/0QB;)LX/2Cx;

    move-result-object v3

    check-cast v3, LX/2Cx;

    invoke-static {v7}, LX/2Cw;->a(LX/0QB;)LX/2Cw;

    move-result-object v4

    check-cast v4, LX/2Cw;

    invoke-static {v7}, LX/2Ct;->a(LX/0QB;)LX/2Ct;

    move-result-object v5

    check-cast v5, LX/2Ct;

    invoke-static {v7}, LX/2Hc;->a(LX/0QB;)LX/2Hc;

    move-result-object v6

    check-cast v6, LX/2Hc;

    invoke-static {v7}, LX/2Hb;->b(LX/0QB;)LX/2Hb;

    move-result-object v7

    check-cast v7, LX/2Hb;

    invoke-static/range {v0 .. v7}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingBroadcastReceiver;->a(Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingBroadcastReceiver;Ljava/util/concurrent/ExecutorService;LX/0Uo;LX/2Cx;LX/2Cw;LX/2Ct;LX/2Hc;LX/2Hb;)V

    return-void
.end method

.method private b(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 2498452
    invoke-static {p1}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingNewImplService;->b(Landroid/content/Intent;)LX/0am;

    move-result-object v0

    .line 2498453
    iget-object v1, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingBroadcastReceiver;->d:LX/2Cw;

    invoke-virtual {v1, v0}, LX/2Cw;->a(LX/0am;)V

    .line 2498454
    return-void
.end method

.method private c(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 2498424
    const-string v0, "is_location_history_enabled"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2498425
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "settings changed broadcast did not have parameter"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2498426
    :cond_0
    const-string v0, "is_location_history_enabled"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 2498427
    iget-object v1, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingBroadcastReceiver;->e:LX/2Ct;

    .line 2498428
    if-eqz v0, :cond_1

    .line 2498429
    invoke-static {v1}, LX/2Ct;->e(LX/2Ct;)LX/2Cu;

    move-result-object p0

    invoke-static {v1, p0}, LX/2Ct;->a(LX/2Ct;LX/2Cu;)V

    .line 2498430
    iget-object p0, v1, LX/2Ct;->p:LX/2D6;

    invoke-virtual {p0}, LX/2D6;->a()V

    .line 2498431
    :goto_0
    return-void

    .line 2498432
    :cond_1
    iget-object p0, v1, LX/2Ct;->p:LX/2D6;

    invoke-virtual {p0}, LX/2D6;->c()V

    .line 2498433
    invoke-static {v1}, LX/2Ct;->c(LX/2Ct;)V

    goto :goto_0
.end method

.method private d(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 2498422
    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingBroadcastReceiver;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingBroadcastReceiver$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingBroadcastReceiver$1;-><init>(Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingBroadcastReceiver;Landroid/content/Intent;)V

    const v2, 0x47ee82d7

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2498423
    return-void
.end method

.method private e(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 2498418
    invoke-static {p1}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingNewImplService;->d(Landroid/content/Intent;)Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateResult;

    move-result-object v0

    .line 2498419
    invoke-static {p1}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingNewImplService;->c(Landroid/content/Intent;)Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateParams;

    move-result-object v1

    .line 2498420
    iget-object v2, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingBroadcastReceiver;->f:LX/2Hc;

    invoke-virtual {v2, v0, v1}, LX/2Hc;->a(Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateResult;Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateParams;)V

    .line 2498421
    return-void
.end method

.method private f(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 2498407
    invoke-static {p1}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingNewImplService;->f(Landroid/content/Intent;)Lcom/facebook/location/ImmutableLocation;

    move-result-object v0

    .line 2498408
    new-instance v1, LX/2TT;

    invoke-direct {v1}, LX/2TT;-><init>()V

    .line 2498409
    iput-object v0, v1, LX/2TT;->a:Lcom/facebook/location/ImmutableLocation;

    .line 2498410
    move-object v0, v1

    .line 2498411
    iget-object v1, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingBroadcastReceiver;->b:LX/0Uo;

    invoke-virtual {v1}, LX/0Uo;->l()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 2498412
    iput-object v1, v0, LX/2TT;->b:Ljava/lang/Boolean;

    .line 2498413
    move-object v0, v0

    .line 2498414
    invoke-virtual {v0}, LX/2TT;->a()Lcom/facebook/location/LocationSignalDataPackage;

    move-result-object v0

    .line 2498415
    invoke-static {p1}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingNewImplService;->e(Landroid/content/Intent;)Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingLocationRequestParams;

    move-result-object v1

    .line 2498416
    iget-object v2, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingBroadcastReceiver;->f:LX/2Hc;

    invoke-virtual {v2, v0, v1}, LX/2Hc;->a(Lcom/facebook/location/LocationSignalDataPackage;Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingLocationRequestParams;)V

    .line 2498417
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2498392
    invoke-static {p0, p1}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingBroadcastReceiver;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2498393
    const-string v0, "BACKGROUND_LOCATION_REPORTING_SETTINGS_REQUEST_REFRESH_ACTION"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2498394
    invoke-direct {p0, p2}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingBroadcastReceiver;->a(Landroid/content/Intent;)V

    .line 2498395
    :goto_0
    return-void

    .line 2498396
    :cond_0
    const-string v0, "BACKGROUND_LOCATION_REPORTING_ACTION_FETCH_IS_ENABLED_FINISHED"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2498397
    invoke-direct {p0, p2}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingBroadcastReceiver;->b(Landroid/content/Intent;)V

    goto :goto_0

    .line 2498398
    :cond_1
    const-string v0, "BACKGROUND_LOCATION_REPORTING_SETTINGS_CHANGED_ACTION"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2498399
    invoke-direct {p0, p2}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingBroadcastReceiver;->c(Landroid/content/Intent;)V

    goto :goto_0

    .line 2498400
    :cond_2
    const-string v0, "BACKGROUND_LOCATION_REPORTING_ACTION_LOCATION_UPDATE"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2498401
    invoke-direct {p0, p2}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingBroadcastReceiver;->d(Landroid/content/Intent;)V

    goto :goto_0

    .line 2498402
    :cond_3
    const-string v0, "BACKGROUND_LOCATION_REPORTING_ACTION_WRITE_FINISHED"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2498403
    invoke-direct {p0, p2}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingBroadcastReceiver;->e(Landroid/content/Intent;)V

    goto :goto_0

    .line 2498404
    :cond_4
    const-string v0, "BACKGROUND_LOCATION_REPORTING_ACTION_OBTAIN_SINGLE_LOCATION_FINISHED"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2498405
    invoke-direct {p0, p2}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingBroadcastReceiver;->f(Landroid/content/Intent;)V

    goto :goto_0

    .line 2498406
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown action: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
