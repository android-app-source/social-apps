.class public final Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;
.super LX/0te;
.source ""

# interfaces
.implements Landroid/hardware/SensorEventListener;


# static fields
.field private static final h:Ljava/lang/String;


# instance fields
.field public a:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/location/threading/ForLocationNonUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1r1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0W3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/3C2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private i:LX/1ql;

.field private j:Landroid/hardware/SensorManager;

.field private k:Landroid/os/Handler;

.field private l:I

.field private m:I

.field private n:J

.field private final o:[D

.field private final p:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/json/JSONArray;",
            ">;"
        }
    .end annotation
.end field

.field private q:Z

.field private r:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2499268
    const-class v0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->h:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2499265
    invoke-direct {p0}, LX/0te;-><init>()V

    .line 2499266
    const/4 v0, 0x3

    new-array v0, v0, [D

    iput-object v0, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->o:[D

    .line 2499267
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->p:Ljava/util/ArrayList;

    return-void
.end method

.method private static a(Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;LX/0Zb;Landroid/os/Handler;LX/1r1;LX/03V;LX/0W3;LX/3C2;LX/0SG;)V
    .locals 0

    .prologue
    .line 2499164
    iput-object p1, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->a:LX/0Zb;

    iput-object p2, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->b:Landroid/os/Handler;

    iput-object p3, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->c:LX/1r1;

    iput-object p4, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->d:LX/03V;

    iput-object p5, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->e:LX/0W3;

    iput-object p6, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->f:LX/3C2;

    iput-object p7, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->g:LX/0SG;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 8

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v7

    move-object v0, p0

    check-cast v0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;

    invoke-static {v7}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v1

    check-cast v1, LX/0Zb;

    invoke-static {v7}, LX/1rX;->a(LX/0QB;)Landroid/os/Handler;

    move-result-object v2

    check-cast v2, Landroid/os/Handler;

    invoke-static {v7}, LX/1r1;->a(LX/0QB;)LX/1r1;

    move-result-object v3

    check-cast v3, LX/1r1;

    invoke-static {v7}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v7}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v5

    check-cast v5, LX/0W3;

    invoke-static {v7}, LX/3C2;->a(LX/0QB;)LX/3C2;

    move-result-object v6

    check-cast v6, LX/3C2;

    invoke-static {v7}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v7

    check-cast v7, LX/0SG;

    invoke-static/range {v0 .. v7}, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->a(Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;LX/0Zb;Landroid/os/Handler;LX/1r1;LX/03V;LX/0W3;LX/3C2;LX/0SG;)V

    return-void
.end method

.method private a(Landroid/hardware/SensorManager;Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;II)Z
    .locals 6

    .prologue
    .line 2499262
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 2499263
    iget-object v5, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->b:Landroid/os/Handler;

    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move v3, p4

    move v4, p5

    invoke-static/range {v0 .. v5}, LX/Hll;->a(Landroid/hardware/SensorManager;Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;IILandroid/os/Handler;)Z

    move-result v0

    .line 2499264
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1, p2, p3, p4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v0

    goto :goto_0
.end method

.method private a(Lorg/json/JSONArray;[D)Z
    .locals 18

    .prologue
    .line 2499235
    invoke-virtual/range {p1 .. p1}, Lorg/json/JSONArray;->length()I

    move-result v3

    .line 2499236
    if-nez v3, :cond_0

    .line 2499237
    const/4 v2, 0x0

    .line 2499238
    :goto_0
    return v2

    .line 2499239
    :cond_0
    const-wide/16 v10, 0x0

    .line 2499240
    const-wide/16 v8, 0x0

    .line 2499241
    const-wide/16 v6, 0x1

    .line 2499242
    const-wide v4, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 2499243
    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_1

    .line 2499244
    :try_start_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lorg/json/JSONArray;->getJSONArray(I)Lorg/json/JSONArray;

    move-result-object v12

    .line 2499245
    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Lorg/json/JSONArray;->getDouble(I)D

    move-result-wide v14

    .line 2499246
    const/4 v13, 0x1

    invoke-virtual {v12, v13}, Lorg/json/JSONArray;->getDouble(I)D

    move-result-wide v16

    .line 2499247
    const/4 v13, 0x2

    invoke-virtual {v12, v13}, Lorg/json/JSONArray;->getDouble(I)D

    move-result-wide v12

    .line 2499248
    mul-double/2addr v14, v14

    mul-double v16, v16, v16

    add-double v14, v14, v16

    mul-double/2addr v12, v12

    add-double/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v12

    .line 2499249
    mul-double v14, v12, v12

    add-double/2addr v10, v14

    .line 2499250
    add-double/2addr v8, v12

    .line 2499251
    invoke-static {v6, v7, v12, v13}, Ljava/lang/Math;->max(DD)D

    move-result-wide v6

    .line 2499252
    invoke-static {v4, v5, v12, v13}, Ljava/lang/Math;->min(DD)D

    move-result-wide v4

    .line 2499253
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2499254
    :cond_1
    int-to-double v12, v3

    div-double/2addr v8, v12

    .line 2499255
    const/4 v2, 0x0

    int-to-double v12, v3

    div-double/2addr v10, v12

    mul-double/2addr v8, v8

    sub-double v8, v10, v8

    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    aput-wide v8, p2, v2

    .line 2499256
    const/4 v2, 0x1

    aput-wide v4, p2, v2

    .line 2499257
    const/4 v2, 0x2

    aput-wide v6, p2, v2
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2499258
    const/4 v2, 0x1

    goto :goto_0

    .line 2499259
    :catch_0
    move-exception v2

    .line 2499260
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->d:LX/03V;

    sget-object v4, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->h:Ljava/lang/String;

    invoke-virtual {v3, v4, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2499261
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;)V
    .locals 20

    .prologue
    .line 2499200
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->j:Landroid/hardware/SensorManager;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 2499201
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->a:LX/0Zb;

    const-string v3, "android_background_location_sensor_data"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v3

    .line 2499202
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x13

    if-lt v2, v4, :cond_0

    .line 2499203
    new-instance v2, Lorg/json/JSONArray;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->p:Ljava/util/ArrayList;

    invoke-direct {v2, v4}, Lorg/json/JSONArray;-><init>(Ljava/util/Collection;)V

    .line 2499204
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->p:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 2499205
    :goto_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->o:[D

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v4}, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->a(Lorg/json/JSONArray;[D)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v4

    if-nez v4, :cond_1

    .line 2499206
    move-object/from16 v0, p0

    iget v2, v0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->r:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->stopSelf(I)V

    .line 2499207
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->q:Z

    .line 2499208
    :goto_1
    return-void

    .line 2499209
    :cond_0
    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->p:Ljava/util/ArrayList;

    monitor-enter v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2499210
    :try_start_2
    new-instance v2, Lorg/json/JSONArray;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->p:Ljava/util/ArrayList;

    invoke-direct {v2, v5}, Lorg/json/JSONArray;-><init>(Ljava/util/Collection;)V

    .line 2499211
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->p:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 2499212
    monitor-exit v4

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2499213
    :catchall_1
    move-exception v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->r:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->stopSelf(I)V

    .line 2499214
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->q:Z

    throw v2

    .line 2499215
    :cond_1
    :try_start_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->o:[D

    const/4 v5, 0x0

    aget-wide v4, v4, v5

    .line 2499216
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->o:[D

    const/4 v7, 0x1

    aget-wide v6, v6, v7

    .line 2499217
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->o:[D

    const/4 v9, 0x2

    aget-wide v8, v8, v9

    .line 2499218
    invoke-virtual {v2}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v10

    .line 2499219
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->g:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v12

    .line 2499220
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 2499221
    sget-object v11, LX/3C5;->b:LX/0U1;

    invoke-virtual {v11}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v2, v11, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2499222
    sget-object v11, LX/3C5;->c:LX/0U1;

    invoke-virtual {v11}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v11

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v14

    invoke-virtual {v2, v11, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2499223
    sget-object v11, LX/3C5;->d:LX/0U1;

    invoke-virtual {v11}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v11

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v14

    invoke-virtual {v2, v11, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2499224
    sget-object v11, LX/3C5;->e:LX/0U1;

    invoke-virtual {v11}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v11

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v14

    invoke-virtual {v2, v11, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2499225
    sget-object v11, LX/3C5;->f:LX/0U1;

    invoke-virtual {v11}, LX/0U1;->a()Ljava/lang/String;

    move-result-object v11

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-virtual {v2, v11, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2499226
    :try_start_5
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->f:LX/3C2;

    invoke-virtual {v11}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v11

    .line 2499227
    sget-object v14, LX/3C5;->a:Ljava/lang/String;

    const/4 v15, 0x0

    const v16, -0x1005a056

    invoke-static/range {v16 .. v16}, LX/03h;->a(I)V

    invoke-virtual {v11, v14, v15, v2}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v2, -0x53639947

    invoke-static {v2}, LX/03h;->a(I)V

    .line 2499228
    sget-object v2, LX/3C5;->a:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v15, LX/3C5;->f:LX/0U1;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " < ?"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/String;

    const/16 v16, 0x0

    const-wide/32 v18, 0x36ee80

    sub-long v12, v12, v18

    invoke-static {v12, v13}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v15, v16

    invoke-virtual {v11, v2, v14, v15}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_5
    .catch Landroid/database/SQLException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 2499229
    :goto_2
    :try_start_6
    invoke-virtual {v3}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2499230
    const-string v2, "accelerometer_data"

    invoke-virtual {v3, v2, v10}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v2

    const-string v3, "accelerometer_sampling_duration_us"

    move-object/from16 v0, p0

    iget v10, v0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->l:I

    invoke-virtual {v2, v3, v10}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    move-result-object v2

    const-string v3, "standard_deviation"

    invoke-virtual {v2, v3, v4, v5}, LX/0oG;->a(Ljava/lang/String;D)LX/0oG;

    move-result-object v2

    const-string v3, "max"

    invoke-virtual {v2, v3, v8, v9}, LX/0oG;->a(Ljava/lang/String;D)LX/0oG;

    move-result-object v2

    const-string v3, "min"

    invoke-virtual {v2, v3, v6, v7}, LX/0oG;->a(Ljava/lang/String;D)LX/0oG;

    move-result-object v2

    invoke-virtual {v2}, LX/0oG;->d()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2499231
    :cond_2
    move-object/from16 v0, p0

    iget v2, v0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->r:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->stopSelf(I)V

    .line 2499232
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->q:Z

    goto/16 :goto_1

    .line 2499233
    :catch_0
    move-exception v2

    .line 2499234
    :try_start_7
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->d:LX/03V;

    sget-object v12, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->h:Ljava/lang/String;

    invoke-virtual {v11, v12, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_2
.end method

.method public static a$redex0(Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;Landroid/content/Intent;I)V
    .locals 8

    .prologue
    const-wide/32 v6, 0xea60

    .line 2499269
    iput p2, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->r:I

    .line 2499270
    :try_start_0
    iget-boolean v0, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->q:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_2

    .line 2499271
    if-nez p1, :cond_1

    move-wide v0, v6

    :goto_0
    invoke-static {p0, v0, v1}, LX/Hln;->c(Landroid/content/Context;J)V

    .line 2499272
    iget-boolean v0, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->q:Z

    if-nez v0, :cond_0

    .line 2499273
    iget v0, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->r:I

    invoke-virtual {p0, v0}, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->stopSelf(I)V

    .line 2499274
    :cond_0
    :goto_1
    return-void

    .line 2499275
    :cond_1
    const-string v0, "extra_trigger_period_ms"

    invoke-virtual {p1, v0, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    goto :goto_0

    .line 2499276
    :cond_2
    if-nez p1, :cond_4

    .line 2499277
    :try_start_1
    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->d:LX/03V;

    sget-object v1, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->h:Ljava/lang/String;

    const-string v2, "intent is null"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2499278
    if-nez p1, :cond_3

    :goto_2
    invoke-static {p0, v6, v7}, LX/Hln;->c(Landroid/content/Context;J)V

    .line 2499279
    iget-boolean v0, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->q:Z

    if-nez v0, :cond_0

    .line 2499280
    iget v0, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->r:I

    invoke-virtual {p0, v0}, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->stopSelf(I)V

    goto :goto_1

    .line 2499281
    :cond_3
    const-string v0, "extra_trigger_period_ms"

    invoke-virtual {p1, v0, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    goto :goto_2

    .line 2499282
    :cond_4
    :try_start_2
    const-string v0, "action_collect_sensor_data"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 2499283
    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->d:LX/03V;

    sget-object v1, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->h:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "action is not expected: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2499284
    if-nez p1, :cond_5

    :goto_3
    invoke-static {p0, v6, v7}, LX/Hln;->c(Landroid/content/Context;J)V

    .line 2499285
    iget-boolean v0, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->q:Z

    if-nez v0, :cond_0

    .line 2499286
    iget v0, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->r:I

    invoke-virtual {p0, v0}, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->stopSelf(I)V

    goto :goto_1

    .line 2499287
    :cond_5
    const-string v0, "extra_trigger_period_ms"

    invoke-virtual {p1, v0, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    goto :goto_3

    .line 2499288
    :cond_6
    :try_start_3
    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->j:Landroid/hardware/SensorManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v3

    .line 2499289
    if-nez v3, :cond_8

    .line 2499290
    if-nez p1, :cond_7

    :goto_4
    invoke-static {p0, v6, v7}, LX/Hln;->c(Landroid/content/Context;J)V

    .line 2499291
    iget-boolean v0, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->q:Z

    if-nez v0, :cond_0

    .line 2499292
    iget v0, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->r:I

    invoke-virtual {p0, v0}, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->stopSelf(I)V

    goto :goto_1

    .line 2499293
    :cond_7
    const-string v0, "extra_trigger_period_ms"

    invoke-virtual {p1, v0, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    goto :goto_4

    .line 2499294
    :cond_8
    :try_start_4
    iget-object v1, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->j:Landroid/hardware/SensorManager;

    iget v4, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->m:I

    iget v5, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->l:I

    move-object v0, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->a(Landroid/hardware/SensorManager;Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->q:Z

    .line 2499295
    iget-boolean v0, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->q:Z

    if-nez v0, :cond_a

    .line 2499296
    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->d:LX/03V;

    sget-object v1, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->h:Ljava/lang/String;

    const-string v2, "Fail to register listener to sensors"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2499297
    if-nez p1, :cond_9

    :goto_5
    invoke-static {p0, v6, v7}, LX/Hln;->c(Landroid/content/Context;J)V

    .line 2499298
    iget-boolean v0, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->q:Z

    if-nez v0, :cond_0

    .line 2499299
    iget v0, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->r:I

    invoke-virtual {p0, v0}, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->stopSelf(I)V

    goto/16 :goto_1

    .line 2499300
    :cond_9
    const-string v0, "extra_trigger_period_ms"

    invoke-virtual {p1, v0, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    goto :goto_5

    .line 2499301
    :cond_a
    :try_start_5
    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->k:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->k:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    iget-wide v2, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->n:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 2499302
    if-nez p1, :cond_b

    :goto_6
    invoke-static {p0, v6, v7}, LX/Hln;->c(Landroid/content/Context;J)V

    .line 2499303
    iget-boolean v0, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->q:Z

    if-nez v0, :cond_0

    .line 2499304
    iget v0, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->r:I

    invoke-virtual {p0, v0}, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->stopSelf(I)V

    goto/16 :goto_1

    .line 2499305
    :cond_b
    const-string v0, "extra_trigger_period_ms"

    invoke-virtual {p1, v0, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    goto :goto_6

    :catchall_0
    move-exception v0

    if-nez p1, :cond_d

    :goto_7
    invoke-static {p0, v6, v7}, LX/Hln;->c(Landroid/content/Context;J)V

    .line 2499306
    iget-boolean v1, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->q:Z

    if-nez v1, :cond_c

    .line 2499307
    iget v1, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->r:I

    invoke-virtual {p0, v1}, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->stopSelf(I)V

    :cond_c
    throw v0

    .line 2499308
    :cond_d
    const-string v1, "extra_trigger_period_ms"

    invoke-virtual {p1, v1, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    goto :goto_7
.end method


# virtual methods
.method public final onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0

    .prologue
    .line 2499199
    return-void
.end method

.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 2499198
    const/4 v0, 0x0

    return-object v0
.end method

.method public final onFbCreate()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/16 v0, 0x24

    const v1, -0x30c8f229

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2499187
    invoke-super {p0}, LX/0te;->onFbCreate()V

    .line 2499188
    invoke-static {p0, p0}, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2499189
    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->c:LX/1r1;

    const/4 v2, 0x1

    sget-object v3, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->h:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, LX/1r1;->a(ILjava/lang/String;)LX/1ql;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->i:LX/1ql;

    .line 2499190
    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->i:LX/1ql;

    invoke-virtual {v0}, LX/1ql;->c()V

    .line 2499191
    const-string v0, "sensor"

    invoke-virtual {p0, v0}, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->j:Landroid/hardware/SensorManager;

    .line 2499192
    new-instance v0, LX/Hlm;

    iget-object v2, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->b:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v0, p0, v2}, LX/Hlm;-><init>(Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->k:Landroid/os/Handler;

    .line 2499193
    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->e:LX/0W3;

    sget-wide v2, LX/0X5;->af:J

    const v4, 0x2dc6c0

    invoke-interface {v0, v2, v3, v4}, LX/0W4;->a(JI)I

    move-result v0

    iput v0, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->l:I

    .line 2499194
    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->e:LX/0W3;

    sget-wide v2, LX/0X5;->ag:J

    const/16 v4, 0x14

    invoke-interface {v0, v2, v3, v4}, LX/0W4;->a(JI)I

    move-result v0

    int-to-long v2, v0

    .line 2499195
    const-wide/32 v4, 0xf4240

    div-long v2, v4, v2

    const-wide/16 v4, 0x1388

    sub-long/2addr v2, v4

    long-to-int v0, v2

    iput v0, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->m:I

    .line 2499196
    iget v0, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->l:I

    iget v2, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->m:I

    mul-int/lit8 v2, v2, 0xa

    add-int/2addr v0, v2

    int-to-long v2, v0

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    iput-wide v2, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->n:J

    .line 2499197
    const/16 v0, 0x25

    const v2, 0x740405a5

    invoke-static {v6, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFbDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0xa0729b8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2499182
    invoke-super {p0}, LX/0te;->onFbDestroy()V

    .line 2499183
    iget-object v1, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->i:LX/1ql;

    invoke-virtual {v1}, LX/1ql;->d()V

    .line 2499184
    iget-object v1, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->c:LX/1r1;

    iget-object v2, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->i:LX/1ql;

    invoke-virtual {v1, v2}, LX/1r1;->a(LX/1ql;)V

    .line 2499185
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->i:LX/1ql;

    .line 2499186
    const/16 v1, 0x25

    const v2, -0x45a33f73

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFbStartCommand(Landroid/content/Intent;II)I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x26125718

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2499177
    iget-object v1, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->k:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 2499178
    iput-object p1, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 2499179
    iput p3, v1, Landroid/os/Message;->arg1:I

    .line 2499180
    iget-object v2, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->k:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 2499181
    const/16 v1, 0x25

    const v2, 0x84ed988

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v3
.end method

.method public final onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 6

    .prologue
    .line 2499165
    :try_start_0
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 2499166
    const/4 v0, 0x0

    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    array-length v2, v2

    :goto_0
    if-ge v0, v2, :cond_0

    .line 2499167
    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v3, v3, v0

    float-to-double v4, v3

    invoke-virtual {v1, v4, v5}, Lorg/json/JSONArray;->put(D)Lorg/json/JSONArray;

    .line 2499168
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2499169
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v0, v2, :cond_1

    .line 2499170
    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2499171
    :goto_1
    return-void

    .line 2499172
    :cond_1
    iget-object v2, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->p:Ljava/util/ArrayList;

    monitor-enter v2
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2499173
    :try_start_1
    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2499174
    monitor-exit v2

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    .line 2499175
    :catch_0
    move-exception v0

    .line 2499176
    iget-object v1, p0, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->d:LX/03V;

    sget-object v2, Lcom/facebook/backgroundlocation/reporting/UserActivityDetector$UserActivitySamplingService;->h:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method
