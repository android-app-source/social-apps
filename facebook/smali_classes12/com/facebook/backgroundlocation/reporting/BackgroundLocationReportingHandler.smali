.class public Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/1qM;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/11H;

.field public final c:LX/3C6;

.field public final d:LX/0aU;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/11H;LX/3C6;LX/0aU;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2498455
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2498456
    iput-object p1, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingHandler;->a:Landroid/content/Context;

    .line 2498457
    iput-object p2, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingHandler;->b:LX/11H;

    .line 2498458
    iput-object p3, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingHandler;->c:LX/3C6;

    .line 2498459
    iput-object p4, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingHandler;->d:LX/0aU;

    .line 2498460
    return-void
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 2498461
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 2498462
    const-string v1, "background_location_update"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2498463
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2498464
    const-string v1, "BackgroundLocationReportingUpdateParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateParams;

    .line 2498465
    iget-object v1, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingHandler;->b:LX/11H;

    iget-object v2, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingHandler;->c:LX/3C6;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateResult;

    .line 2498466
    iget-boolean v1, v0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateResult;->b:Z

    if-nez v1, :cond_0

    .line 2498467
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingHandler;->d:LX/0aU;

    const-string v3, "BACKGROUND_LOCATION_REPORTING_SETTINGS_REQUEST_REFRESH_ACTION"

    invoke-virtual {v2, v3}, LX/0aU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2498468
    const-string v2, "expected_location_history_setting"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2498469
    iget-object v2, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingHandler;->a:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 2498470
    :cond_0
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 2498471
    return-object v0

    .line 2498472
    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown operation type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
