.class public final Lcom/facebook/backgroundlocation/reporting/graphql/BackgroundLocationReportingGraphQLModels$FetchBackgroundLocationReportingSettingsModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2499374
    const-class v0, Lcom/facebook/backgroundlocation/reporting/graphql/BackgroundLocationReportingGraphQLModels$FetchBackgroundLocationReportingSettingsModel;

    new-instance v1, Lcom/facebook/backgroundlocation/reporting/graphql/BackgroundLocationReportingGraphQLModels$FetchBackgroundLocationReportingSettingsModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/backgroundlocation/reporting/graphql/BackgroundLocationReportingGraphQLModels$FetchBackgroundLocationReportingSettingsModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2499375
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2499376
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2499377
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2499378
    const/4 v2, 0x0

    .line 2499379
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 2499380
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2499381
    :goto_0
    move v1, v2

    .line 2499382
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2499383
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2499384
    new-instance v1, Lcom/facebook/backgroundlocation/reporting/graphql/BackgroundLocationReportingGraphQLModels$FetchBackgroundLocationReportingSettingsModel;

    invoke-direct {v1}, Lcom/facebook/backgroundlocation/reporting/graphql/BackgroundLocationReportingGraphQLModels$FetchBackgroundLocationReportingSettingsModel;-><init>()V

    .line 2499385
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2499386
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2499387
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2499388
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2499389
    :cond_0
    return-object v1

    .line 2499390
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2499391
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 2499392
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2499393
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2499394
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 2499395
    const-string v4, "location_sharing"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2499396
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2499397
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v5, :cond_9

    .line 2499398
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2499399
    :goto_2
    move v1, v3

    .line 2499400
    goto :goto_1

    .line 2499401
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 2499402
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2499403
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 2499404
    :cond_5
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, p0, :cond_7

    .line 2499405
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 2499406
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2499407
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_5

    if-eqz v6, :cond_5

    .line 2499408
    const-string p0, "is_tracking_enabled"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 2499409
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v1

    move v5, v1

    move v1, v4

    goto :goto_3

    .line 2499410
    :cond_6
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 2499411
    :cond_7
    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 2499412
    if-eqz v1, :cond_8

    .line 2499413
    invoke-virtual {v0, v3, v5}, LX/186;->a(IZ)V

    .line 2499414
    :cond_8
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_9
    move v1, v3

    move v5, v3

    goto :goto_3
.end method
