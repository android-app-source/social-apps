.class public Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService;
.super LX/0te;
.source ""


# static fields
.field public static final g:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/location/threading/ForLocationNonUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1r1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/2Fu;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0W3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Landroid/hardware/SensorManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private h:LX/1ql;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2499578
    const-class v0, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService;

    sput-object v0, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService;->g:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2499576
    invoke-direct {p0}, LX/0te;-><init>()V

    .line 2499577
    return-void
.end method

.method private static a(Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService;Landroid/os/Handler;Landroid/os/Handler;LX/1r1;LX/2Fu;LX/0W3;Landroid/hardware/SensorManager;)V
    .locals 0

    .prologue
    .line 2499575
    iput-object p1, p0, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService;->a:Landroid/os/Handler;

    iput-object p2, p0, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService;->b:Landroid/os/Handler;

    iput-object p3, p0, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService;->c:LX/1r1;

    iput-object p4, p0, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService;->d:LX/2Fu;

    iput-object p5, p0, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService;->e:LX/0W3;

    iput-object p6, p0, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService;->f:Landroid/hardware/SensorManager;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 7

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v6

    move-object v0, p0

    check-cast v0, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService;

    invoke-static {v6}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v1

    check-cast v1, Landroid/os/Handler;

    invoke-static {v6}, LX/1rX;->a(LX/0QB;)Landroid/os/Handler;

    move-result-object v2

    check-cast v2, Landroid/os/Handler;

    invoke-static {v6}, LX/1r1;->a(LX/0QB;)LX/1r1;

    move-result-object v3

    check-cast v3, LX/1r1;

    invoke-static {v6}, LX/2Fu;->b(LX/0QB;)LX/2Fu;

    move-result-object v4

    check-cast v4, LX/2Fu;

    invoke-static {v6}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v5

    check-cast v5, LX/0W3;

    invoke-static {v6}, LX/2Fq;->b(LX/0QB;)Landroid/hardware/SensorManager;

    move-result-object v6

    check-cast v6, Landroid/hardware/SensorManager;

    invoke-static/range {v0 .. v6}, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService;->a(Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService;Landroid/os/Handler;Landroid/os/Handler;LX/1r1;LX/2Fu;LX/0W3;Landroid/hardware/SensorManager;)V

    return-void
.end method


# virtual methods
.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 2499574
    const/4 v0, 0x0

    return-object v0
.end method

.method public final onFbCreate()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x24

    const v1, 0x255b9eb7

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2499555
    invoke-super {p0}, LX/0te;->onFbCreate()V

    .line 2499556
    invoke-static {p0, p0}, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2499557
    iget-object v1, p0, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService;->c:LX/1r1;

    const/4 v2, 0x1

    sget-object v3, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService;->g:Ljava/lang/Class;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/1r1;->a(ILjava/lang/String;)LX/1ql;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService;->h:LX/1ql;

    .line 2499558
    iget-object v1, p0, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService;->h:LX/1ql;

    invoke-virtual {v1}, LX/1ql;->c()V

    .line 2499559
    const/16 v1, 0x25

    const v2, -0x769de902

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFbDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x189b5cff

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2499570
    invoke-super {p0}, LX/0te;->onFbDestroy()V

    .line 2499571
    iget-object v1, p0, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService;->h:LX/1ql;

    invoke-virtual {v1}, LX/1ql;->d()V

    .line 2499572
    iget-object v1, p0, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService;->c:LX/1r1;

    iget-object v2, p0, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService;->h:LX/1ql;

    invoke-virtual {v1, v2}, LX/1r1;->a(LX/1ql;)V

    .line 2499573
    const/16 v1, 0x25

    const v2, -0x249868e5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFbStartCommand(Landroid/content/Intent;II)I
    .locals 10

    .prologue
    const/4 v7, 0x2

    const/16 v0, 0x24

    const v1, -0x316a910f

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2499560
    iget-object v1, p0, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService;->f:Landroid/hardware/SensorManager;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v1

    .line 2499561
    iget-object v2, p0, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService;->e:LX/0W3;

    sget-wide v4, LX/0X5;->z:J

    invoke-interface {v2, v4, v5}, LX/0W4;->c(J)J

    move-result-wide v2

    .line 2499562
    const-wide/32 v4, 0xf4240

    div-long v2, v4, v2

    long-to-int v2, v2

    .line 2499563
    new-instance v3, LX/Hlu;

    invoke-direct {v3}, LX/Hlu;-><init>()V

    .line 2499564
    iget-object v4, p0, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService;->f:Landroid/hardware/SensorManager;

    iget-object v5, p0, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService;->b:Landroid/os/Handler;

    invoke-virtual {v4, v3, v1, v2, v5}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;ILandroid/os/Handler;)Z

    .line 2499565
    iget-object v1, p0, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService;->e:LX/0W3;

    sget-wide v4, LX/0X5;->x:J

    invoke-interface {v1, v4, v5}, LX/0W4;->c(J)J

    move-result-wide v8

    .line 2499566
    iget-object v1, p0, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService;->e:LX/0W3;

    sget-wide v4, LX/0X5;->A:J

    invoke-interface {v1, v4, v5}, LX/0W4;->g(J)D

    move-result-wide v4

    .line 2499567
    new-instance v1, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService$1;

    move-object v2, p0

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService$1;-><init>(Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService;LX/Hlu;DI)V

    .line 2499568
    iget-object v2, p0, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService;->a:Landroid/os/Handler;

    const v3, 0x71295429

    invoke-static {v2, v1, v8, v9, v3}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2499569
    const/16 v1, 0x25

    const v2, -0x5179da05

    invoke-static {v7, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v7
.end method
