.class public Lcom/facebook/backgroundlocation/reporting/monitors/SpeedChangeMonitorReceiver;
.super Landroid/content/BroadcastReceiver;
.source ""

# interfaces
.implements LX/0Ya;


# instance fields
.field public a:LX/2Hg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2499612
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/backgroundlocation/reporting/monitors/SpeedChangeMonitorReceiver;

    invoke-static {v0}, LX/2Hg;->b(LX/0QB;)LX/2Hg;

    move-result-object v0

    check-cast v0, LX/2Hg;

    iput-object v0, p0, Lcom/facebook/backgroundlocation/reporting/monitors/SpeedChangeMonitorReceiver;->a:LX/2Hg;

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x26

    const v1, 0x905d5a8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2499613
    invoke-static {p0, p1}, Lcom/facebook/backgroundlocation/reporting/monitors/SpeedChangeMonitorReceiver;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2499614
    iget-object v1, p0, Lcom/facebook/backgroundlocation/reporting/monitors/SpeedChangeMonitorReceiver;->a:LX/2Hg;

    .line 2499615
    iget-object v2, v1, LX/2Hg;->g:Ljava/util/concurrent/Executor;

    new-instance p0, Lcom/facebook/backgroundlocation/reporting/monitors/SpeedChangeMonitor$1;

    invoke-direct {p0, v1, p2}, Lcom/facebook/backgroundlocation/reporting/monitors/SpeedChangeMonitor$1;-><init>(LX/2Hg;Landroid/content/Intent;)V

    const p1, 0x77b477ab

    invoke-static {v2, p0, p1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2499616
    const/16 v1, 0x27

    const v2, 0x19a0593d

    invoke-static {p2, v3, v1, v2, v0}, LX/02F;->a(Landroid/content/Intent;IIII)V

    return-void
.end method
