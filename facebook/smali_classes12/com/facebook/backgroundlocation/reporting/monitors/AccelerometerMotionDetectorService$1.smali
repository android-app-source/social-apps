.class public final Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/Hlu;

.field public final synthetic b:D

.field public final synthetic c:I

.field public final synthetic d:Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService;


# direct methods
.method public constructor <init>(Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService;LX/Hlu;DI)V
    .locals 1

    .prologue
    .line 2499537
    iput-object p1, p0, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService$1;->d:Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService;

    iput-object p2, p0, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService$1;->a:LX/Hlu;

    iput-wide p3, p0, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService$1;->b:D

    iput p5, p0, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService$1;->c:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 2499530
    :try_start_0
    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService$1;->d:Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService;

    iget-object v0, v0, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService;->f:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService$1;->a:LX/Hlu;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 2499531
    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService$1;->a:LX/Hlu;

    invoke-virtual {v0}, LX/Hlu;->a()F

    move-result v0

    .line 2499532
    float-to-double v0, v0

    iget-wide v2, p0, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService$1;->b:D

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    .line 2499533
    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService$1;->d:Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService;

    iget-object v0, v0, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService;->d:LX/2Fu;

    sget-object v1, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService;->g:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2Fu;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2499534
    :cond_0
    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService$1;->d:Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService;

    iget v1, p0, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService$1;->c:I

    invoke-virtual {v0, v1}, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService;->stopSelf(I)V

    .line 2499535
    return-void

    .line 2499536
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService$1;->d:Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService;

    iget v2, p0, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService$1;->c:I

    invoke-virtual {v1, v2}, Lcom/facebook/backgroundlocation/reporting/monitors/AccelerometerMotionDetectorService;->stopSelf(I)V

    throw v0
.end method
