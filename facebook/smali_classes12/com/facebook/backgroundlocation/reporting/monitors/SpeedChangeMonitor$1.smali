.class public final Lcom/facebook/backgroundlocation/reporting/monitors/SpeedChangeMonitor$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Landroid/content/Intent;

.field public final synthetic b:LX/2Hg;


# direct methods
.method public constructor <init>(LX/2Hg;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 2499584
    iput-object p1, p0, Lcom/facebook/backgroundlocation/reporting/monitors/SpeedChangeMonitor$1;->b:LX/2Hg;

    iput-object p2, p0, Lcom/facebook/backgroundlocation/reporting/monitors/SpeedChangeMonitor$1;->a:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 2499585
    :try_start_0
    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/monitors/SpeedChangeMonitor$1;->a:Landroid/content/Intent;

    invoke-static {v0}, Lcom/google/android/gms/location/LocationResult;->a(Landroid/content/Intent;)Lcom/google/android/gms/location/LocationResult;

    move-result-object v0

    .line 2499586
    if-nez v0, :cond_0

    .line 2499587
    :goto_0
    return-void

    .line 2499588
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/location/LocationResult;->a()Landroid/location/Location;

    move-result-object v0

    .line 2499589
    iget-object v1, p0, Lcom/facebook/backgroundlocation/reporting/monitors/SpeedChangeMonitor$1;->b:LX/2Hg;

    invoke-virtual {v1, v0}, LX/2Hg;->a(Landroid/location/Location;)V

    .line 2499590
    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/monitors/SpeedChangeMonitor$1;->b:LX/2Hg;

    iget-object v0, v0, LX/2Hg;->k:LX/2Hb;

    iget-object v1, p0, Lcom/facebook/backgroundlocation/reporting/monitors/SpeedChangeMonitor$1;->a:Landroid/content/Intent;

    invoke-virtual {v0, v1}, LX/2Hb;->a(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2499591
    :catch_0
    move-exception v0

    .line 2499592
    iget-object v1, p0, Lcom/facebook/backgroundlocation/reporting/monitors/SpeedChangeMonitor$1;->b:LX/2Hg;

    iget-object v1, v1, LX/2Hg;->i:LX/03V;

    sget-object v2, LX/2Hg;->o:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Error processing location update"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
