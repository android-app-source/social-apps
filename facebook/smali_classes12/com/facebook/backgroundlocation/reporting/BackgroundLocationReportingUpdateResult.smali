.class public Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Z

.field public final b:Z

.field public final c:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2498648
    new-instance v0, LX/HlS;

    invoke-direct {v0}, LX/HlS;-><init>()V

    sput-object v0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2498638
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2498639
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateResult;->a:Z

    .line 2498640
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateResult;->b:Z

    .line 2498641
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateResult;->c:Z

    .line 2498642
    return-void
.end method

.method public constructor <init>(ZZZ)V
    .locals 0

    .prologue
    .line 2498643
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2498644
    iput-boolean p1, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateResult;->a:Z

    .line 2498645
    iput-boolean p2, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateResult;->b:Z

    .line 2498646
    iput-boolean p3, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateResult;->c:Z

    .line 2498647
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2498632
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2498637
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "didUpdateSucceed"

    iget-boolean v2, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateResult;->a:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "isLocationHistoryEnabled"

    iget-boolean v2, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateResult;->b:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "isBestDevice"

    iget-boolean v2, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateResult;->c:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2498633
    iget-boolean v0, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateResult;->a:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2498634
    iget-boolean v0, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateResult;->b:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2498635
    iget-boolean v0, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateResult;->c:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2498636
    return-void
.end method
