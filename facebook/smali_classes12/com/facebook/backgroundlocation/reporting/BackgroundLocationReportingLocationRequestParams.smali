.class public Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingLocationRequestParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingLocationRequestParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:F

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Lcom/facebook/location/FbLocationOperationParams;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2498491
    new-instance v0, LX/HlM;

    invoke-direct {v0}, LX/HlM;-><init>()V

    sput-object v0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingLocationRequestParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(FLjava/lang/String;Lcom/facebook/location/FbLocationOperationParams;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2498481
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2498482
    iput p1, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingLocationRequestParams;->a:F

    .line 2498483
    iput-object p2, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingLocationRequestParams;->b:Ljava/lang/String;

    .line 2498484
    iput-object p3, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingLocationRequestParams;->c:Lcom/facebook/location/FbLocationOperationParams;

    .line 2498485
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2498486
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2498487
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingLocationRequestParams;->a:F

    .line 2498488
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingLocationRequestParams;->b:Ljava/lang/String;

    .line 2498489
    const-class v0, Lcom/facebook/location/FbLocationOperationParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/location/FbLocationOperationParams;

    iput-object v0, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingLocationRequestParams;->c:Lcom/facebook/location/FbLocationOperationParams;

    .line 2498490
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2498480
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2498476
    iget v0, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingLocationRequestParams;->a:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 2498477
    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingLocationRequestParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2498478
    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingLocationRequestParams;->c:Lcom/facebook/location/FbLocationOperationParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2498479
    return-void
.end method
