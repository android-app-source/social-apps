.class public final Lcom/facebook/backgroundlocation/reporting/wifi/WifiCollectorJobLogic$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/45o;

.field public final synthetic b:LX/Hly;


# direct methods
.method public constructor <init>(LX/Hly;LX/45o;)V
    .locals 0

    .prologue
    .line 2499624
    iput-object p1, p0, Lcom/facebook/backgroundlocation/reporting/wifi/WifiCollectorJobLogic$1;->b:LX/Hly;

    iput-object p2, p0, Lcom/facebook/backgroundlocation/reporting/wifi/WifiCollectorJobLogic$1;->a:LX/45o;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 2499625
    :try_start_0
    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/wifi/WifiCollectorJobLogic$1;->b:LX/Hly;

    iget-object v0, v0, LX/Hly;->d:LX/2GC;

    invoke-virtual {v0}, LX/2GC;->b()Ljava/util/List;

    move-result-object v1

    .line 2499626
    if-eqz v1, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object v4, v0

    .line 2499627
    :goto_0
    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/wifi/WifiCollectorJobLogic$1;->b:LX/Hly;

    iget-object v0, v0, LX/Hly;->d:LX/2GC;

    invoke-virtual {v0}, LX/2GC;->a()Lcom/facebook/wifiscan/WifiScanResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 2499628
    if-nez v4, :cond_1

    if-nez v5, :cond_1

    .line 2499629
    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/wifi/WifiCollectorJobLogic$1;->a:LX/45o;

    invoke-interface {v0, v2}, LX/45o;->a(Z)V

    .line 2499630
    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/wifi/WifiCollectorJobLogic$1;->b:LX/Hly;

    iget-object v0, v0, LX/Hly;->g:LX/2D6;

    invoke-virtual {v0}, LX/2D6;->b()V

    .line 2499631
    :goto_1
    return-void

    .line 2499632
    :cond_0
    const/4 v0, 0x0

    move-object v4, v0

    goto :goto_0

    .line 2499633
    :cond_1
    if-eqz v4, :cond_5

    :try_start_1
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    move v3, v0

    .line 2499634
    :goto_2
    if-eqz v4, :cond_2

    .line 2499635
    new-instance v0, LX/Hlx;

    invoke-direct {v0, p0}, LX/Hlx;-><init>(Lcom/facebook/backgroundlocation/reporting/wifi/WifiCollectorJobLogic$1;)V

    invoke-static {v4, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2499636
    :cond_2
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 2499637
    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/wifi/WifiCollectorJobLogic$1;->b:LX/Hly;

    iget-object v0, v0, LX/Hly;->h:LX/0W3;

    sget-wide v8, LX/0X5;->Z:J

    const/16 v1, -0xc8

    invoke-interface {v0, v8, v9, v1}, LX/0W4;->a(JI)I

    move-result v7

    .line 2499638
    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/wifi/WifiCollectorJobLogic$1;->b:LX/Hly;

    iget-object v0, v0, LX/Hly;->h:LX/0W3;

    sget-wide v8, LX/0X5;->ab:J

    const/4 v1, 0x0

    invoke-interface {v0, v8, v9, v1}, LX/0W4;->a(JI)I

    move-result v8

    .line 2499639
    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/wifi/WifiCollectorJobLogic$1;->b:LX/Hly;

    iget-object v0, v0, LX/Hly;->h:LX/0W3;

    sget-wide v10, LX/0X5;->ac:J

    const/16 v1, 0x14

    invoke-interface {v0, v10, v11, v1}, LX/0W4;->a(JI)I

    move-result v9

    move v1, v2

    .line 2499640
    :goto_3
    if-ge v1, v3, :cond_6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    if-ge v0, v9, :cond_6

    .line 2499641
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/wifiscan/WifiScanResult;

    .line 2499642
    iget v10, v0, Lcom/facebook/wifiscan/WifiScanResult;->c:I

    if-ge v10, v7, :cond_3

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v10

    if-ge v10, v8, :cond_4

    .line 2499643
    :cond_3
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2499644
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_5
    move v3, v2

    .line 2499645
    goto :goto_2

    .line 2499646
    :cond_6
    invoke-interface {v6}, Ljava/util/List;->size()I

    .line 2499647
    const/4 v0, 0x3

    .line 2499648
    invoke-static {v0}, LX/01m;->b(I)Z

    move-result v1

    if-nez v1, :cond_9

    .line 2499649
    :cond_7
    if-nez v5, :cond_8

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-eqz v0, :cond_8

    .line 2499650
    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/wifi/WifiCollectorJobLogic$1;->a:LX/45o;

    invoke-interface {v0, v2}, LX/45o;->a(Z)V

    .line 2499651
    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/wifi/WifiCollectorJobLogic$1;->b:LX/Hly;

    iget-object v0, v0, LX/Hly;->g:LX/2D6;

    invoke-virtual {v0}, LX/2D6;->b()V

    goto :goto_1

    .line 2499652
    :cond_8
    :try_start_2
    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/wifi/WifiCollectorJobLogic$1;->b:LX/Hly;

    iget-object v0, v0, LX/Hly;->f:LX/Hli;

    new-instance v1, LX/2TT;

    invoke-direct {v1}, LX/2TT;-><init>()V

    iget-object v3, p0, Lcom/facebook/backgroundlocation/reporting/wifi/WifiCollectorJobLogic$1;->b:LX/Hly;

    iget-object v3, v3, LX/Hly;->e:LX/0Uo;

    invoke-virtual {v3}, LX/0Uo;->l()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 2499653
    iput-object v3, v1, LX/2TT;->b:Ljava/lang/Boolean;

    .line 2499654
    move-object v1, v1

    .line 2499655
    iput-object v5, v1, LX/2TT;->c:Lcom/facebook/wifiscan/WifiScanResult;

    .line 2499656
    move-object v1, v1

    .line 2499657
    iput-object v6, v1, LX/2TT;->d:Ljava/util/List;

    .line 2499658
    move-object v1, v1

    .line 2499659
    invoke-virtual {v1}, LX/2TT;->a()Lcom/facebook/location/LocationSignalDataPackage;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v3}, LX/Hli;->a(Lcom/facebook/location/LocationSignalDataPackage;Z)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2499660
    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/wifi/WifiCollectorJobLogic$1;->a:LX/45o;

    invoke-interface {v0, v2}, LX/45o;->a(Z)V

    .line 2499661
    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/wifi/WifiCollectorJobLogic$1;->b:LX/Hly;

    iget-object v0, v0, LX/Hly;->g:LX/2D6;

    invoke-virtual {v0}, LX/2D6;->b()V

    goto/16 :goto_1

    .line 2499662
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2499663
    :try_start_3
    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/wifi/WifiCollectorJobLogic$1;->b:LX/Hly;

    iget-object v0, v0, LX/Hly;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v3, "Periodic ambient wifi collection failed"

    invoke-virtual {v0, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2499664
    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/wifi/WifiCollectorJobLogic$1;->a:LX/45o;

    invoke-interface {v0, v2}, LX/45o;->a(Z)V

    .line 2499665
    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/wifi/WifiCollectorJobLogic$1;->b:LX/Hly;

    iget-object v0, v0, LX/Hly;->g:LX/2D6;

    invoke-virtual {v0}, LX/2D6;->b()V

    goto/16 :goto_1

    .line 2499666
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/facebook/backgroundlocation/reporting/wifi/WifiCollectorJobLogic$1;->a:LX/45o;

    invoke-interface {v1, v2}, LX/45o;->a(Z)V

    .line 2499667
    iget-object v1, p0, Lcom/facebook/backgroundlocation/reporting/wifi/WifiCollectorJobLogic$1;->b:LX/Hly;

    iget-object v1, v1, LX/Hly;->g:LX/2D6;

    invoke-virtual {v1}, LX/2D6;->b()V

    throw v0

    .line 2499668
    :cond_9
    if-eqz v4, :cond_7

    if-eqz v6, :cond_7

    .line 2499669
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/wifiscan/WifiScanResult;

    .line 2499670
    const/4 v7, 0x4

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, v1, Lcom/facebook/wifiscan/WifiScanResult;->d:Ljava/lang/String;

    aput-object v9, v7, v8

    const/4 v8, 0x1

    iget-object v9, v1, Lcom/facebook/wifiscan/WifiScanResult;->b:Ljava/lang/String;

    aput-object v9, v7, v8

    const/4 v8, 0x2

    iget v9, v1, Lcom/facebook/wifiscan/WifiScanResult;->c:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    if-eqz v5, :cond_a

    iget-object v1, v1, Lcom/facebook/wifiscan/WifiScanResult;->b:Ljava/lang/String;

    iget-object v8, v5, Lcom/facebook/wifiscan/WifiScanResult;->b:Ljava/lang/String;

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    const-string v1, "(connected)"

    :goto_5
    aput-object v1, v7, v0

    goto :goto_4

    :cond_a
    invoke-interface {v6, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    const-string v1, "(included)"

    goto :goto_5

    :cond_b
    const-string v1, "(filtered)"

    goto :goto_5
.end method
