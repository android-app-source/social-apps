.class public final Lcom/facebook/backgroundlocation/reporting/GeofenceLocationTracker$GeofenceLocationMonitorService;
.super LX/1ZN;
.source ""


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field public a:LX/Hlk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Hlj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2498892
    const-class v0, Lcom/facebook/backgroundlocation/reporting/GeofenceLocationTracker$GeofenceLocationMonitorService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/backgroundlocation/reporting/GeofenceLocationTracker$GeofenceLocationMonitorService;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2498893
    sget-object v0, Lcom/facebook/backgroundlocation/reporting/GeofenceLocationTracker$GeofenceLocationMonitorService;->c:Ljava/lang/String;

    invoke-direct {p0, v0}, LX/1ZN;-><init>(Ljava/lang/String;)V

    .line 2498894
    return-void
.end method

.method private static a(Lcom/facebook/backgroundlocation/reporting/GeofenceLocationTracker$GeofenceLocationMonitorService;LX/Hlk;LX/Hlj;)V
    .locals 0

    .prologue
    .line 2498895
    iput-object p1, p0, Lcom/facebook/backgroundlocation/reporting/GeofenceLocationTracker$GeofenceLocationMonitorService;->a:LX/Hlk;

    iput-object p2, p0, Lcom/facebook/backgroundlocation/reporting/GeofenceLocationTracker$GeofenceLocationMonitorService;->b:LX/Hlj;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/backgroundlocation/reporting/GeofenceLocationTracker$GeofenceLocationMonitorService;

    invoke-static {v1}, LX/Hlk;->a(LX/0QB;)LX/Hlk;

    move-result-object v0

    check-cast v0, LX/Hlk;

    invoke-static {v1}, LX/Hlj;->a(LX/0QB;)LX/Hlj;

    move-result-object v1

    check-cast v1, LX/Hlj;

    invoke-static {p0, v0, v1}, Lcom/facebook/backgroundlocation/reporting/GeofenceLocationTracker$GeofenceLocationMonitorService;->a(Lcom/facebook/backgroundlocation/reporting/GeofenceLocationTracker$GeofenceLocationMonitorService;LX/Hlk;LX/Hlj;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x61a7b1d0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2498896
    if-nez p1, :cond_4

    const/4 v1, 0x0

    :goto_0
    move-object v1, v1

    .line 2498897
    if-eqz v1, :cond_0

    iget v2, v1, LX/7Zy;->a:I

    const/4 v4, -0x1

    if-eq v2, v4, :cond_8

    const/4 v2, 0x1

    :goto_1
    move v2, v2

    .line 2498898
    if-eqz v2, :cond_2

    .line 2498899
    :cond_0
    iget-object v2, p0, Lcom/facebook/backgroundlocation/reporting/GeofenceLocationTracker$GeofenceLocationMonitorService;->b:LX/Hlj;

    .line 2498900
    invoke-static {v2}, LX/Hlj;->c(LX/Hlj;)LX/0oG;

    move-result-object v4

    .line 2498901
    invoke-virtual {v4}, LX/0oG;->a()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2498902
    const-string v5, "action"

    const-string v6, "geofence_error_event_received"

    invoke-virtual {v4, v5, v6}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v5

    const-string v6, "api_error_code"

    if-nez v1, :cond_9

    const/4 v4, 0x0

    :goto_2
    invoke-virtual {v5, v6, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    move-result-object v4

    invoke-virtual {v4}, LX/0oG;->d()V

    .line 2498903
    :cond_1
    iget-object v1, p0, Lcom/facebook/backgroundlocation/reporting/GeofenceLocationTracker$GeofenceLocationMonitorService;->a:LX/Hlk;

    invoke-virtual {v1}, LX/Hlk;->a()V

    .line 2498904
    const/16 v1, 0x25

    const v2, -0x7441f450

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2498905
    :goto_3
    return-void

    .line 2498906
    :cond_2
    iget v2, v1, LX/7Zy;->b:I

    move v2, v2

    .line 2498907
    if-ne v2, v3, :cond_3

    .line 2498908
    iget-object v2, p0, Lcom/facebook/backgroundlocation/reporting/GeofenceLocationTracker$GeofenceLocationMonitorService;->a:LX/Hlk;

    iget-object v3, v1, LX/7Zy;->d:Landroid/location/Location;

    move-object v1, v3

    .line 2498909
    invoke-static {v1}, Lcom/facebook/location/ImmutableLocation;->a(Landroid/location/Location;)Lcom/facebook/location/ImmutableLocation;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/Hlk;->b(Lcom/facebook/location/ImmutableLocation;)V

    .line 2498910
    :cond_3
    const v1, 0x2a60e625

    invoke-static {v1, v0}, LX/02F;->d(II)V

    goto :goto_3

    :cond_4
    const-string v1, "gms_error_code"

    const/4 v2, -0x1

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    const/4 v1, -0x1

    const-string v2, "com.google.android.location.intent.extra.transition"

    invoke-virtual {p1, v2, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v1, :cond_6

    :cond_5
    :goto_4
    move v5, v1

    invoke-static {p1}, LX/7Zy;->c(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v6

    const-string v1, "com.google.android.location.intent.extra.triggering_location"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/location/Location;

    new-instance v2, LX/7Zy;

    invoke-direct {v2, v4, v5, v6, v1}, LX/7Zy;-><init>(IILjava/util/List;Landroid/location/Location;)V

    move-object v1, v2

    goto :goto_0

    :cond_6
    const/4 v5, 0x1

    if-eq v2, v5, :cond_7

    const/4 v5, 0x2

    if-eq v2, v5, :cond_7

    const/4 v5, 0x4

    if-ne v2, v5, :cond_5

    :cond_7
    move v1, v2

    goto :goto_4

    :cond_8
    const/4 v2, 0x0

    goto :goto_1

    .line 2498911
    :cond_9
    iget v4, v1, LX/7Zy;->a:I

    move v4, v4

    .line 2498912
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto :goto_2
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x65391c05

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2498913
    invoke-super {p0}, LX/1ZN;->onCreate()V

    .line 2498914
    invoke-static {p0, p0}, Lcom/facebook/backgroundlocation/reporting/GeofenceLocationTracker$GeofenceLocationMonitorService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2498915
    const/16 v1, 0x25

    const v2, 0x1866a294

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
