.class public Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingNewImplService;
.super LX/1ZN;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final e:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final f:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/HlQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0aG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1sS;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2498600
    const-class v0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingNewImplService;

    sput-object v0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingNewImplService;->e:Ljava/lang/Class;

    .line 2498601
    const-class v0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingNewImplService;

    const-string v1, "background_location"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingNewImplService;->f:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2498598
    const-class v0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingNewImplService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1ZN;-><init>(Ljava/lang/String;)V

    .line 2498599
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/app/PendingIntent;)V
    .locals 2

    .prologue
    .line 2498592
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2498593
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 2498594
    const-string v1, "BackgroundLocationReportingNewImplService.FETCH_IS_ENABLED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2498595
    const-string v1, "BackgroundLocationReportingNewImplService.FETCH_IS_ENABLED.callback"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2498596
    invoke-static {p0, v0}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingNewImplService;->a(Landroid/content/Context;Landroid/content/Intent;)V

    .line 2498597
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 2498570
    invoke-static {p0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    .line 2498571
    invoke-static {v1}, LX/HlQ;->a(LX/0QB;)LX/HlQ;

    move-result-object v0

    check-cast v0, LX/HlQ;

    .line 2498572
    iget-object v2, v0, LX/1qk;->a:LX/1ql;

    invoke-virtual {v2}, LX/1ql;->c()V

    .line 2498573
    :try_start_0
    const-class v2, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingNewImplService;

    invoke-virtual {p1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 2498574
    invoke-virtual {p0, p1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v2

    .line 2498575
    if-nez v2, :cond_0

    .line 2498576
    invoke-static {v1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    sget-object v2, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingNewImplService;->e:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "could not start service"

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2498577
    iget-object v0, v0, LX/1qk;->a:LX/1ql;

    invoke-virtual {v0}, LX/1ql;->d()V

    :cond_0
    return-void

    .line 2498578
    :catchall_0
    move-exception v1

    .line 2498579
    iget-object v0, v0, LX/1qk;->a:LX/1ql;

    invoke-virtual {v0}, LX/1ql;->d()V

    throw v1
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingLocationRequestParams;Landroid/app/PendingIntent;)V
    .locals 2

    .prologue
    .line 2498585
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2498586
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 2498587
    const-string v1, "BackgroundLocationReportingNewImplService.OBTAIN_SINGLE_LOCATION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2498588
    const-string v1, "BackgroundLocationReportingNewImplService.OBTAIN_SINGLE_LOCATION.params"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2498589
    const-string v1, "BackgroundLocationReportingNewImplService.OBTAIN_SINGLE_LOCATION.callback"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2498590
    invoke-static {p0, v0}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingNewImplService;->a(Landroid/content/Context;Landroid/content/Intent;)V

    .line 2498591
    return-void
.end method

.method private static a(Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingNewImplService;LX/HlQ;LX/0tX;LX/0aG;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingNewImplService;",
            "LX/HlQ;",
            "LX/0tX;",
            "LX/0aG;",
            "LX/0Or",
            "<",
            "LX/1sS;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2498584
    iput-object p1, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingNewImplService;->a:LX/HlQ;

    iput-object p2, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingNewImplService;->b:LX/0tX;

    iput-object p3, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingNewImplService;->c:LX/0aG;

    iput-object p4, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingNewImplService;->d:LX/0Or;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 5

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingNewImplService;

    invoke-static {v3}, LX/HlQ;->a(LX/0QB;)LX/HlQ;

    move-result-object v0

    check-cast v0, LX/HlQ;

    invoke-static {v3}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-static {v3}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v2

    check-cast v2, LX/0aG;

    const/16 v4, 0xc81

    invoke-static {v3, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingNewImplService;->a(Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingNewImplService;LX/HlQ;LX/0tX;LX/0aG;LX/0Or;)V

    return-void
.end method

.method public static b(Landroid/content/Intent;)LX/0am;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "LX/0am",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2498581
    const-string v0, "BackgroundLocationReportingNewImplService.FETCH_IS_ENABLED.result"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2498582
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    .line 2498583
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "BackgroundLocationReportingNewImplService.FETCH_IS_ENABLED.result"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    goto :goto_0
.end method

.method public static c(Landroid/content/Intent;)Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateParams;
    .locals 1

    .prologue
    .line 2498580
    const-string v0, "BackgroundLocationReportingNewImplService.WRITE_TO_SERVER.params"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateParams;

    return-object v0
.end method

.method public static d(Landroid/content/Intent;)Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateResult;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2498602
    const-string v0, "BackgroundLocationReportingNewImplService.WRITE_TO_SERVER.result"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateResult;

    return-object v0
.end method

.method public static e(Landroid/content/Intent;)Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingLocationRequestParams;
    .locals 1

    .prologue
    .line 2498506
    const-string v0, "BackgroundLocationReportingNewImplService.OBTAIN_SINGLE_LOCATION.params"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingLocationRequestParams;

    return-object v0
.end method

.method public static f(Landroid/content/Intent;)Lcom/facebook/location/ImmutableLocation;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2498507
    const-string v0, "BackgroundLocationReportingNewImplService.OBTAIN_SINGLE_LOCATION.result"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/location/ImmutableLocation;

    return-object v0
.end method

.method private g(Landroid/content/Intent;)Z
    .locals 4

    .prologue
    .line 2498508
    if-nez p1, :cond_0

    .line 2498509
    const/4 v0, 0x0

    .line 2498510
    :goto_0
    return v0

    .line 2498511
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 2498512
    const-string v1, "BackgroundLocationReportingNewImplService.FETCH_IS_ENABLED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2498513
    invoke-direct {p0, p1}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingNewImplService;->h(Landroid/content/Intent;)Z

    move-result v0

    goto :goto_0

    .line 2498514
    :cond_1
    const-string v1, "BackgroundLocationReportingNewImplService.WRITE_TO_SERVER"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2498515
    invoke-direct {p0, p1}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingNewImplService;->i(Landroid/content/Intent;)Z

    move-result v0

    goto :goto_0

    .line 2498516
    :cond_2
    const-string v1, "BackgroundLocationReportingNewImplService.OBTAIN_SINGLE_LOCATION"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2498517
    invoke-direct {p0, p1}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingNewImplService;->j(Landroid/content/Intent;)Z

    move-result v0

    goto :goto_0

    .line 2498518
    :cond_3
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown action: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private h(Landroid/content/Intent;)Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2498519
    new-instance v0, LX/Hlo;

    invoke-direct {v0}, LX/Hlo;-><init>()V

    move-object v0, v0

    .line 2498520
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2498521
    iget-object v1, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingNewImplService;->b:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2498522
    const/4 v1, 0x0

    .line 2498523
    :try_start_0
    invoke-static {v0}, LX/1v3;->b(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    move-object v1, v0

    .line 2498524
    :goto_0
    const-string v0, "BackgroundLocationReportingNewImplService.FETCH_IS_ENABLED.callback"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    .line 2498525
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 2498526
    if-eqz v1, :cond_0

    .line 2498527
    iget-object v4, v1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v4

    .line 2498528
    check-cast v1, Lcom/facebook/backgroundlocation/reporting/graphql/BackgroundLocationReportingGraphQLModels$FetchBackgroundLocationReportingSettingsModel;

    invoke-virtual {v1}, Lcom/facebook/backgroundlocation/reporting/graphql/BackgroundLocationReportingGraphQLModels$FetchBackgroundLocationReportingSettingsModel;->a()LX/1vs;

    move-result-object v1

    iget-object v4, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-virtual {v4, v1, v2}, LX/15i;->h(II)Z

    move-result v1

    .line 2498529
    const-string v4, "BackgroundLocationReportingNewImplService.FETCH_IS_ENABLED.result"

    invoke-virtual {v3, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2498530
    :cond_0
    const/4 v1, 0x0

    :try_start_1
    invoke-virtual {v0, p0, v1, v3}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V
    :try_end_1
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_1 .. :try_end_1} :catch_0

    .line 2498531
    const/4 v0, 0x1

    .line 2498532
    :goto_1
    return v0

    :catch_0
    move v0, v2

    goto :goto_1

    :catch_1
    goto :goto_0
.end method

.method private i(Landroid/content/Intent;)Z
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 2498533
    const-string v0, "BackgroundLocationReportingNewImplService.WRITE_TO_SERVER.params"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateParams;

    .line 2498534
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2498535
    const-string v0, "BackgroundLocationReportingUpdateParams"

    invoke-virtual {v2, v0, v6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2498536
    iget-object v0, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingNewImplService;->c:LX/0aG;

    const-string v1, "background_location_update"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    sget-object v4, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingNewImplService;->f:Lcom/facebook/common/callercontext/CallerContext;

    const v5, -0x3993b518

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->startOnMainThread()LX/1ML;

    move-result-object v0

    .line 2498537
    const/4 v1, 0x0

    .line 2498538
    :try_start_0
    invoke-static {v0}, LX/1v3;->b(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbservice/service/OperationResult;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    move-object v1, v0

    .line 2498539
    :goto_0
    const-string v0, "BackgroundLocationReportingNewImplService.WRITE_TO_SERVER.callback"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    .line 2498540
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 2498541
    const-string v3, "BackgroundLocationReportingNewImplService.WRITE_TO_SERVER.params"

    invoke-virtual {v2, v3, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2498542
    if-eqz v1, :cond_0

    .line 2498543
    invoke-virtual {v1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingUpdateResult;

    .line 2498544
    const-string v3, "BackgroundLocationReportingNewImplService.WRITE_TO_SERVER.result"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2498545
    :cond_0
    const/4 v1, 0x0

    :try_start_1
    invoke-virtual {v0, p0, v1, v2}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V
    :try_end_1
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_1 .. :try_end_1} :catch_0

    .line 2498546
    const/4 v0, 0x1

    .line 2498547
    :goto_1
    return v0

    :catch_0
    move v0, v7

    goto :goto_1

    :catch_1
    goto :goto_0
.end method

.method private j(Landroid/content/Intent;)Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 2498548
    const-string v0, "BackgroundLocationReportingNewImplService.OBTAIN_SINGLE_LOCATION.params"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingLocationRequestParams;

    .line 2498549
    iget-object v1, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingNewImplService;->d:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1sS;

    .line 2498550
    iget-object v2, v0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingLocationRequestParams;->c:Lcom/facebook/location/FbLocationOperationParams;

    sget-object v4, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingNewImplService;->f:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2, v4}, LX/1sS;->a(Lcom/facebook/location/FbLocationOperationParams;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2498551
    const/4 v2, 0x0

    .line 2498552
    :try_start_0
    invoke-static {v1}, LX/1v3;->b(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/location/ImmutableLocation;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    move-object v2, v1

    .line 2498553
    :goto_0
    const-string v1, "BackgroundLocationReportingNewImplService.OBTAIN_SINGLE_LOCATION.callback"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/app/PendingIntent;

    .line 2498554
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 2498555
    const-string v5, "BackgroundLocationReportingNewImplService.OBTAIN_SINGLE_LOCATION.params"

    invoke-virtual {v4, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2498556
    if-eqz v2, :cond_0

    .line 2498557
    const-string v0, "BackgroundLocationReportingNewImplService.OBTAIN_SINGLE_LOCATION.result"

    invoke-virtual {v4, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2498558
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {v1, p0, v0, v4}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V
    :try_end_1
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_1 .. :try_end_1} :catch_0

    .line 2498559
    const/4 v0, 0x1

    .line 2498560
    :goto_1
    return v0

    :catch_0
    move v0, v3

    goto :goto_1

    :catch_1
    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x24

    const v2, 0x69102cc7

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2498561
    :try_start_0
    invoke-direct {p0, p1}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingNewImplService;->g(Landroid/content/Intent;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 2498562
    iget-object v2, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingNewImplService;->a:LX/HlQ;

    iget-object v2, v2, LX/1qk;->a:LX/1ql;

    invoke-virtual {v2}, LX/1ql;->d()V

    .line 2498563
    if-eqz v1, :cond_0

    .line 2498564
    iget-object v1, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingNewImplService;->a:LX/HlQ;

    iget-object v1, v1, LX/1qk;->a:LX/1ql;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v2, v3}, LX/1ql;->a(J)V

    .line 2498565
    :cond_0
    const v1, 0x3806b172

    invoke-static {v1, v0}, LX/02F;->d(II)V

    return-void

    .line 2498566
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingNewImplService;->a:LX/HlQ;

    iget-object v2, v2, LX/1qk;->a:LX/1ql;

    invoke-virtual {v2}, LX/1ql;->d()V

    const v2, 0x7de2f863

    invoke-static {v2, v0}, LX/02F;->d(II)V

    throw v1
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x74bd59f9

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2498567
    invoke-super {p0}, LX/1ZN;->onCreate()V

    .line 2498568
    invoke-static {p0, p0}, Lcom/facebook/backgroundlocation/reporting/BackgroundLocationReportingNewImplService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2498569
    const/16 v1, 0x25

    const v2, -0x85e65d8

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
