.class public Lcom/facebook/fbreactcomponents/stickers/StickerViewManager;
.super Lcom/facebook/react/uimanager/SimpleViewManager;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "RCTStickerView"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/react/uimanager/SimpleViewManager",
        "<",
        "Lcom/facebook/attachments/ui/AttachmentViewSticker;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2683903
    invoke-direct {p0}, Lcom/facebook/react/uimanager/SimpleViewManager;-><init>()V

    return-void
.end method

.method private static b(LX/5rJ;)Lcom/facebook/attachments/ui/AttachmentViewSticker;
    .locals 1

    .prologue
    .line 2683901
    new-instance v0, Lcom/facebook/attachments/ui/AttachmentViewSticker;

    invoke-direct {v0, p0}, Lcom/facebook/attachments/ui/AttachmentViewSticker;-><init>(Landroid/content/Context;)V

    .line 2683902
    return-object v0
.end method


# virtual methods
.method public final synthetic a(LX/5rJ;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2683904
    invoke-static {p1}, Lcom/facebook/fbreactcomponents/stickers/StickerViewManager;->b(LX/5rJ;)Lcom/facebook/attachments/ui/AttachmentViewSticker;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2683898
    check-cast p1, Lcom/facebook/attachments/ui/AttachmentViewSticker;

    .line 2683899
    invoke-virtual {p1}, Lcom/facebook/attachments/ui/AttachmentViewSticker;->a()V

    .line 2683900
    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2683895
    const-string v0, "RCTStickerView"

    return-object v0
.end method

.method public setStickerFbid(Lcom/facebook/attachments/ui/AttachmentViewSticker;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/react/uimanager/annotations/ReactProp;
        name = "stickerFBID"
    .end annotation

    .prologue
    .line 2683896
    iput-object p2, p1, Lcom/facebook/attachments/ui/AttachmentViewSticker;->l:Ljava/lang/String;

    .line 2683897
    return-void
.end method
