.class public Lcom/facebook/fbreactcomponents/stickers/StickerViewInputManager;
.super Lcom/facebook/react/uimanager/SimpleViewManager;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "RCTStickerInputView"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/react/uimanager/SimpleViewManager",
        "<",
        "LX/JMM;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2683838
    invoke-direct {p0}, Lcom/facebook/react/uimanager/SimpleViewManager;-><init>()V

    return-void
.end method

.method private a(LX/5rJ;LX/JMM;)V
    .locals 2

    .prologue
    .line 2683832
    const-class v0, LX/5rQ;

    invoke-virtual {p1, v0}, LX/5pX;->b(Ljava/lang/Class;)LX/5p4;

    move-result-object v0

    check-cast v0, LX/5rQ;

    .line 2683833
    iget-object v1, v0, LX/5rQ;->a:LX/5s9;

    move-object v0, v1

    .line 2683834
    new-instance v1, LX/JMO;

    invoke-direct {v1, p0, v0, p2}, LX/JMO;-><init>(Lcom/facebook/fbreactcomponents/stickers/StickerViewInputManager;LX/5s9;LX/JMM;)V

    .line 2683835
    iget-object v0, p2, LX/JMM;->a:Lcom/facebook/stickers/keyboard/StickerKeyboardView;

    .line 2683836
    iput-object v1, v0, Lcom/facebook/stickers/keyboard/StickerKeyboardView;->f:LX/8ky;

    .line 2683837
    return-void
.end method

.method private static b(LX/5rJ;)LX/JMM;
    .locals 1

    .prologue
    .line 2683827
    new-instance v0, LX/JMM;

    invoke-direct {v0, p0}, LX/JMM;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public final synthetic a(LX/5rJ;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2683831
    invoke-static {p1}, Lcom/facebook/fbreactcomponents/stickers/StickerViewInputManager;->b(LX/5rJ;)LX/JMM;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/5rJ;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2683830
    check-cast p2, LX/JMM;

    invoke-direct {p0, p1, p2}, Lcom/facebook/fbreactcomponents/stickers/StickerViewInputManager;->a(LX/5rJ;LX/JMM;)V

    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2683829
    const-string v0, "RCTStickerInputView"

    return-object v0
.end method

.method public final x()Ljava/util/Map;
    .locals 3

    .prologue
    .line 2683828
    const-string v0, "topStickerSelect"

    const-string v1, "registrationName"

    const-string v2, "onStickerSelect"

    invoke-static {v1, v2}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v1

    invoke-static {v0, v1}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method
