.class public Lcom/facebook/uberbar/ui/UberbarResultsFragment;
.super Lcom/facebook/base/fragment/FbListFragment;
.source ""


# static fields
.field private static final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/api/SearchTypeaheadResult;",
            ">;"
        }
    .end annotation
.end field

.field public static final j:Ljava/lang/String;


# instance fields
.field private final k:LX/HeF;

.field public l:LX/He8;

.field public m:Landroid/view/inputmethod/InputMethodManager;

.field private n:LX/G5X;

.field private o:LX/17W;

.field public p:LX/03V;

.field public q:LX/G5R;

.field private r:Lcom/facebook/content/SecureContextHelper;

.field public s:LX/2hX;

.field public t:LX/961;

.field public u:Ljava/lang/String;

.field private v:LX/G5W;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2490169
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->i:Ljava/util/List;

    .line 2490170
    const-class v0, Lcom/facebook/uberbar/ui/UberbarResultsFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->j:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2490166
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbListFragment;-><init>()V

    .line 2490167
    new-instance v0, LX/HeF;

    invoke-direct {v0, p0}, LX/HeF;-><init>(Lcom/facebook/uberbar/ui/UberbarResultsFragment;)V

    iput-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->k:LX/HeF;

    .line 2490168
    return-void
.end method

.method public static a$redex0(Lcom/facebook/uberbar/ui/UberbarResultsFragment;Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2490160
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->o:LX/17W;

    invoke-virtual {v0, p1, p2, p3}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2490161
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 2490162
    :try_start_0
    iget-object v1, p0, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->r:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p1}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2490163
    :cond_0
    :goto_0
    return-void

    .line 2490164
    :catch_0
    move-exception v0

    .line 2490165
    iget-object v1, p0, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->p:LX/03V;

    sget-object v2, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->j:Ljava/lang/String;

    const-string v3, "Failed to launch activity."

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 2490121
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2490122
    iput-object p1, p0, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->u:Ljava/lang/String;

    .line 2490123
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2490124
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->l:LX/He8;

    sget-object v1, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->i:Ljava/util/List;

    sget-object v2, LX/He6;->NO_QUERY:LX/He6;

    invoke-virtual {v0, v1, v2}, LX/He8;->a(Ljava/util/List;LX/He6;)V

    .line 2490125
    :cond_0
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->v:LX/G5W;

    if-eqz v0, :cond_1

    .line 2490126
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->v:LX/G5W;

    invoke-virtual {v0}, LX/G5W;->b()V

    .line 2490127
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->v:LX/G5W;

    const/4 v1, 0x0

    .line 2490128
    iput-object v1, v0, LX/G5W;->o:LX/HeF;

    .line 2490129
    :cond_1
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->n:LX/G5X;

    const/16 v1, 0x1f4

    invoke-static {p1}, Lcom/facebook/search/api/GraphSearchQuery;->a(Ljava/lang/String;)Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->q:LX/G5R;

    .line 2490130
    iget-object p1, v3, LX/G5R;->a:Ljava/lang/String;

    move-object v3, p1

    .line 2490131
    invoke-virtual {v0, v1, v2, v3}, LX/G5X;->a(ILcom/facebook/search/api/GraphSearchQuery;Ljava/lang/String;)LX/G5W;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->v:LX/G5W;

    .line 2490132
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->v:LX/G5W;

    iget-object v1, p0, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->k:LX/HeF;

    .line 2490133
    iput-object v1, v0, LX/G5W;->o:LX/HeF;

    .line 2490134
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->v:LX/G5W;

    const/4 v4, 0x0

    .line 2490135
    iput-boolean v4, v0, LX/G5W;->m:Z

    .line 2490136
    iput-boolean v4, v0, LX/G5W;->n:Z

    .line 2490137
    iget-object v4, v0, LX/G5W;->t:LX/G5P;

    .line 2490138
    sget-object v5, LX/G5O;->a:LX/G5N;

    invoke-static {v4, v5}, LX/G5P;->a(LX/G5P;LX/0Pq;)V

    .line 2490139
    iget-object v4, v0, LX/G5W;->t:LX/G5P;

    .line 2490140
    sget-object v5, LX/G5O;->b:LX/G5M;

    invoke-static {v4, v5}, LX/G5P;->a(LX/G5P;LX/0Pq;)V

    .line 2490141
    iget-object v4, v0, LX/G5W;->t:LX/G5P;

    .line 2490142
    sget-object v5, LX/G5O;->a:LX/G5N;

    const-string v6, "request_time_to_request_begin_time"

    invoke-static {v4, v5, v6}, LX/G5P;->a(LX/G5P;LX/0Pq;Ljava/lang/String;)V

    .line 2490143
    iget-boolean v4, v0, LX/G5W;->l:Z

    if-eqz v4, :cond_3

    .line 2490144
    iget-object v4, v0, LX/G5W;->v:LX/03V;

    sget-object v5, LX/G5W;->a:Ljava/lang/String;

    const-string v6, "Fetch called when in aborted state"

    invoke-virtual {v4, v5, v6}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2490145
    :cond_2
    :goto_0
    return-void

    .line 2490146
    :cond_3
    iget-object v4, v0, LX/G5W;->i:Lcom/facebook/search/api/GraphSearchQuery;

    .line 2490147
    iget-object v5, v4, LX/7B6;->b:Ljava/lang/String;

    move-object v4, v5

    .line 2490148
    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 2490149
    iget-boolean v4, v0, LX/G5W;->s:Z

    if-nez v4, :cond_4

    .line 2490150
    iget-object v4, v0, LX/G5W;->t:LX/G5P;

    .line 2490151
    sget-object v5, LX/G5O;->b:LX/G5M;

    const-string v6, "fetch_users_time"

    invoke-static {v4, v5, v6}, LX/G5P;->a(LX/G5P;LX/0Pq;Ljava/lang/String;)V

    .line 2490152
    iget-object v4, v0, LX/G5W;->e:LX/G5b;

    iget-object v5, v0, LX/G5W;->i:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-virtual {v4, v5}, LX/G5b;->a(Lcom/facebook/search/api/GraphSearchQuery;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 2490153
    sget-object v5, LX/7BK;->USER:LX/7BK;

    invoke-static {v0, v5}, LX/G5W;->a(LX/G5W;LX/7BK;)LX/0TF;

    move-result-object v5

    invoke-static {v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2490154
    :goto_1
    iget-object v4, v0, LX/G5W;->t:LX/G5P;

    .line 2490155
    sget-object v5, LX/G5O;->b:LX/G5M;

    const-string v6, "fetch_pages_time"

    invoke-static {v4, v5, v6}, LX/G5P;->a(LX/G5P;LX/0Pq;Ljava/lang/String;)V

    .line 2490156
    iget-object v4, v0, LX/G5W;->f:LX/G5b;

    iget-object v5, v0, LX/G5W;->i:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-virtual {v4, v5}, LX/G5b;->a(Lcom/facebook/search/api/GraphSearchQuery;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 2490157
    sget-object v5, LX/7BK;->PAGE:LX/7BK;

    invoke-static {v0, v5}, LX/G5W;->a(LX/G5W;LX/7BK;)LX/0TF;

    move-result-object v5

    invoke-static {v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2490158
    iget-object v4, v0, LX/G5W;->g:LX/0Sh;

    new-instance v5, Lcom/facebook/uberbar/core/UberbarResultFetcher$1;

    invoke-direct {v5, v0}, Lcom/facebook/uberbar/core/UberbarResultFetcher$1;-><init>(LX/G5W;)V

    iget-wide v6, v0, LX/G5W;->k:J

    invoke-virtual {v4, v5, v6, v7}, LX/0Sh;->b(Ljava/lang/Runnable;J)V

    goto :goto_0

    .line 2490159
    :cond_4
    const/4 v4, 0x1

    iput-boolean v4, v0, LX/G5W;->m:Z

    goto :goto_1
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x19090a6c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2490171
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2490172
    invoke-virtual {p0}, Landroid/support/v4/app/ListFragment;->ld_()Landroid/widget/ListView;

    move-result-object v1

    .line 2490173
    iget-object v2, p0, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->l:LX/He8;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2490174
    new-instance v2, LX/HeG;

    invoke-direct {v2, p0}, LX/HeG;-><init>(Lcom/facebook/uberbar/ui/UberbarResultsFragment;)V

    move-object v2, v2

    .line 2490175
    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2490176
    new-instance v2, LX/HeH;

    invoke-direct {v2, p0}, LX/HeH;-><init>(Lcom/facebook/uberbar/ui/UberbarResultsFragment;)V

    move-object v2, v2

    .line 2490177
    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2490178
    const/16 v1, 0x2b

    const v2, 0x500dc96a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 14

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x13cf6290

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2490091
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2490092
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    .line 2490093
    invoke-static {v2}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v0

    check-cast v0, LX/17W;

    iput-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->o:LX/17W;

    .line 2490094
    new-instance p1, LX/He8;

    invoke-static {v2}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    invoke-static {v2}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v3

    check-cast v3, LX/0kb;

    invoke-direct {p1, v0, v3}, LX/He8;-><init>(Landroid/view/LayoutInflater;LX/0kb;)V

    .line 2490095
    move-object v0, p1

    .line 2490096
    check-cast v0, LX/He8;

    iput-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->l:LX/He8;

    .line 2490097
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->l:LX/He8;

    .line 2490098
    new-instance v3, LX/HeI;

    invoke-direct {v3, p0}, LX/HeI;-><init>(Lcom/facebook/uberbar/ui/UberbarResultsFragment;)V

    move-object v3, v3

    .line 2490099
    iput-object v3, v0, LX/He8;->g:LX/HeI;

    .line 2490100
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->l:LX/He8;

    .line 2490101
    new-instance v3, LX/HeJ;

    invoke-direct {v3, p0}, LX/HeJ;-><init>(Lcom/facebook/uberbar/ui/UberbarResultsFragment;)V

    move-object v3, v3

    .line 2490102
    iput-object v3, v0, LX/He8;->f:LX/HeJ;

    .line 2490103
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->l:LX/He8;

    .line 2490104
    new-instance v3, LX/HeL;

    invoke-direct {v3, p0}, LX/HeL;-><init>(Lcom/facebook/uberbar/ui/UberbarResultsFragment;)V

    move-object v3, v3

    .line 2490105
    iput-object v3, v0, LX/He8;->h:LX/HeL;

    .line 2490106
    new-instance v5, LX/G5X;

    invoke-static {v2}, LX/G5c;->a(LX/0QB;)LX/G5c;

    move-result-object v6

    check-cast v6, LX/G5b;

    invoke-static {v2}, LX/G5f;->a(LX/0QB;)LX/G5f;

    move-result-object v7

    check-cast v7, LX/G5b;

    invoke-static {v2}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v8

    check-cast v8, LX/0Sh;

    invoke-static {v2}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v9

    check-cast v9, LX/0aG;

    .line 2490107
    invoke-static {}, LX/G5Z;->a()Ljava/lang/Boolean;

    move-result-object v10

    move-object v10, v10

    .line 2490108
    check-cast v10, Ljava/lang/Boolean;

    .line 2490109
    new-instance v13, LX/G5P;

    invoke-static {v2}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v11

    check-cast v11, LX/0So;

    invoke-static {v2}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v12

    check-cast v12, LX/11i;

    invoke-direct {v13, v11, v12}, LX/G5P;-><init>(LX/0So;LX/11i;)V

    .line 2490110
    move-object v11, v13

    .line 2490111
    check-cast v11, LX/G5P;

    invoke-static {v2}, LX/7BO;->a(LX/0QB;)LX/7BO;

    move-result-object v12

    check-cast v12, LX/7BO;

    invoke-static {v2}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v13

    check-cast v13, LX/03V;

    invoke-direct/range {v5 .. v13}, LX/G5X;-><init>(LX/G5b;LX/G5b;LX/0Sh;LX/0aG;Ljava/lang/Boolean;LX/G5P;LX/7BO;LX/03V;)V

    .line 2490112
    move-object v0, v5

    .line 2490113
    check-cast v0, LX/G5X;

    iput-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->n:LX/G5X;

    .line 2490114
    invoke-static {v2}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->m:Landroid/view/inputmethod/InputMethodManager;

    .line 2490115
    invoke-static {v2}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    iput-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->p:LX/03V;

    .line 2490116
    invoke-static {v2}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iput-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->r:Lcom/facebook/content/SecureContextHelper;

    .line 2490117
    invoke-static {v2}, LX/2hX;->b(LX/0QB;)LX/2hX;

    move-result-object v0

    check-cast v0, LX/2hX;

    iput-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->s:LX/2hX;

    .line 2490118
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->v:LX/G5W;

    .line 2490119
    invoke-static {v2}, LX/961;->b(LX/0QB;)LX/961;

    move-result-object v0

    check-cast v0, LX/961;

    iput-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->t:LX/961;

    .line 2490120
    const/16 v0, 0x2b

    const v2, 0x5a828bee

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x28129acc

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2490090
    const v1, 0x7f031536    # 1.74239E38f

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0xb9f2ebb

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v1, -0x755fc7a3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2490074
    invoke-super {p0}, Lcom/facebook/base/fragment/FbListFragment;->onDestroy()V

    .line 2490075
    iget-object v1, p0, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->v:LX/G5W;

    if-eqz v1, :cond_0

    .line 2490076
    iget-object v1, p0, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->v:LX/G5W;

    invoke-virtual {v1}, LX/G5W;->b()V

    .line 2490077
    iget-object v1, p0, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->v:LX/G5W;

    .line 2490078
    iput-object v2, v1, LX/G5W;->o:LX/HeF;

    .line 2490079
    iput-object v2, p0, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->v:LX/G5W;

    .line 2490080
    :cond_0
    iget-object v1, p0, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->l:LX/He8;

    .line 2490081
    iput-object v2, v1, LX/He8;->f:LX/HeJ;

    .line 2490082
    iget-object v1, p0, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->l:LX/He8;

    .line 2490083
    iput-object v2, v1, LX/He8;->g:LX/HeI;

    .line 2490084
    iget-object v1, p0, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->l:LX/He8;

    .line 2490085
    iput-object v2, v1, LX/He8;->h:LX/HeL;

    .line 2490086
    iput-object v2, p0, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->l:LX/He8;

    .line 2490087
    iget-object v1, p0, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->s:LX/2hX;

    const/4 v2, 0x1

    .line 2490088
    iput-boolean v2, v1, LX/2hY;->d:Z

    .line 2490089
    const/16 v1, 0x2b

    const v2, -0x1f5bead7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
