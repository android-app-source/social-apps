.class public Lcom/facebook/uberbar/ui/UberbarFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fj;


# instance fields
.field public a:LX/He3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/view/inputmethod/InputMethodManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Landroid/widget/EditText;

.field private d:Landroid/widget/ImageButton;

.field private e:Landroid/view/ViewGroup;

.field private f:Landroid/view/View;

.field public g:Lcom/facebook/uberbar/ui/UberbarResultsFragment;

.field public h:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2489722
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2489723
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/uberbar/ui/UberbarFragment;->h:Z

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p1, Lcom/facebook/uberbar/ui/UberbarFragment;

    new-instance v0, LX/He3;

    invoke-static {v2}, LX/0hx;->a(LX/0QB;)LX/0hx;

    move-result-object v1

    check-cast v1, LX/0hx;

    invoke-static {v2}, LX/1No;->b(LX/0QB;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    check-cast v3, Landroid/support/v4/app/FragmentActivity;

    invoke-static {v2}, LX/G5R;->b(LX/0QB;)LX/G5R;

    move-result-object p0

    check-cast p0, LX/G5R;

    invoke-direct {v0, v1, v3, p0}, LX/He3;-><init>(LX/0hx;Landroid/support/v4/app/FragmentActivity;LX/G5R;)V

    move-object v1, v0

    check-cast v1, LX/He3;

    invoke-static {v2}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v2

    check-cast v2, Landroid/view/inputmethod/InputMethodManager;

    iput-object v1, p1, Lcom/facebook/uberbar/ui/UberbarFragment;->a:LX/He3;

    iput-object v2, p1, Lcom/facebook/uberbar/ui/UberbarFragment;->b:Landroid/view/inputmethod/InputMethodManager;

    return-void
.end method


# virtual methods
.method public final S_()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2489724
    iget-boolean v0, p0, Lcom/facebook/uberbar/ui/UberbarFragment;->h:Z

    if-eqz v0, :cond_0

    .line 2489725
    iput-boolean v1, p0, Lcom/facebook/uberbar/ui/UberbarFragment;->h:Z

    .line 2489726
    :goto_0
    return v1

    .line 2489727
    :cond_0
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarFragment;->a:LX/He3;

    .line 2489728
    iget-object p0, v0, LX/He2;->h:LX/G5R;

    .line 2489729
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/G5R;->f:Z

    .line 2489730
    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2489731
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2489732
    const-class v0, Lcom/facebook/uberbar/ui/UberbarFragment;

    invoke-static {v0, p0}, Lcom/facebook/uberbar/ui/UberbarFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2489733
    new-instance v0, LX/4nE;

    invoke-direct {v0}, LX/4nE;-><init>()V

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(LX/1Lb;)V

    .line 2489734
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x6cfa8457

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2489735
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2489736
    const v0, 0x7f0d1901

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/uberbar/ui/UberbarFragment;->c:Landroid/widget/EditText;

    .line 2489737
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarFragment;->c:Landroid/widget/EditText;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 2489738
    new-instance v2, LX/He9;

    invoke-direct {v2, p0}, LX/He9;-><init>(Lcom/facebook/uberbar/ui/UberbarFragment;)V

    .line 2489739
    const v0, 0x7f0d2fc8

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/facebook/uberbar/ui/UberbarFragment;->d:Landroid/widget/ImageButton;

    .line 2489740
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarFragment;->d:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2489741
    const v0, 0x7f0d2fc6

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/uberbar/ui/UberbarFragment;->f:Landroid/view/View;

    .line 2489742
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarFragment;->f:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2489743
    const v0, 0x7f0d2fc9

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/uberbar/ui/UberbarFragment;->e:Landroid/view/ViewGroup;

    .line 2489744
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarFragment;->e:Landroid/view/ViewGroup;

    .line 2489745
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v4

    .line 2489746
    const-string v2, "results_fragment"

    invoke-virtual {v4, v2}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/facebook/uberbar/ui/UberbarResultsFragment;

    iput-object v2, p0, Lcom/facebook/uberbar/ui/UberbarFragment;->g:Lcom/facebook/uberbar/ui/UberbarResultsFragment;

    .line 2489747
    iget-object v2, p0, Lcom/facebook/uberbar/ui/UberbarFragment;->g:Lcom/facebook/uberbar/ui/UberbarResultsFragment;

    if-nez v2, :cond_0

    .line 2489748
    new-instance v2, Lcom/facebook/uberbar/ui/UberbarResultsFragment;

    invoke-direct {v2}, Lcom/facebook/uberbar/ui/UberbarResultsFragment;-><init>()V

    iput-object v2, p0, Lcom/facebook/uberbar/ui/UberbarFragment;->g:Lcom/facebook/uberbar/ui/UberbarResultsFragment;

    .line 2489749
    invoke-virtual {v4}, LX/0gc;->a()LX/0hH;

    move-result-object v2

    .line 2489750
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getId()I

    move-result v4

    iget-object v5, p0, Lcom/facebook/uberbar/ui/UberbarFragment;->g:Lcom/facebook/uberbar/ui/UberbarResultsFragment;

    const-string p1, "results_fragment"

    invoke-virtual {v2, v4, v5, p1}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    .line 2489751
    invoke-virtual {v2}, LX/0hH;->b()I

    .line 2489752
    :cond_0
    iget-object v2, p0, Lcom/facebook/uberbar/ui/UberbarFragment;->a:LX/He3;

    iget-object v4, p0, Lcom/facebook/uberbar/ui/UberbarFragment;->g:Lcom/facebook/uberbar/ui/UberbarResultsFragment;

    .line 2489753
    iput-object v4, v2, LX/He3;->g:Lcom/facebook/uberbar/ui/UberbarResultsFragment;

    .line 2489754
    iget-object v5, v2, LX/He2;->g:Lcom/facebook/uberbar/ui/UberbarResultsFragment;

    iget-object p1, v2, LX/He2;->h:LX/G5R;

    .line 2489755
    iput-object p1, v5, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->q:LX/G5R;

    .line 2489756
    const/16 v0, 0x2b

    const v2, -0x5f40640c

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x57577b9d

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2489757
    const v1, 0x7f031535

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2489758
    const/16 v2, 0x2b

    const v3, 0x2bca24cb

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v1, 0x12bb9e54

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2489759
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2489760
    iget-object v1, p0, Lcom/facebook/uberbar/ui/UberbarFragment;->f:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2489761
    iget-object v1, p0, Lcom/facebook/uberbar/ui/UberbarFragment;->d:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2489762
    iput-object v2, p0, Lcom/facebook/uberbar/ui/UberbarFragment;->d:Landroid/widget/ImageButton;

    .line 2489763
    iput-object v2, p0, Lcom/facebook/uberbar/ui/UberbarFragment;->c:Landroid/widget/EditText;

    .line 2489764
    iput-object v2, p0, Lcom/facebook/uberbar/ui/UberbarFragment;->e:Landroid/view/ViewGroup;

    .line 2489765
    const/16 v1, 0x2b

    const v2, 0x626403c7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 8

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x6a7e0d96

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2489766
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2489767
    iget-object v1, p0, Lcom/facebook/uberbar/ui/UberbarFragment;->a:LX/He3;

    .line 2489768
    iget-boolean v2, v1, LX/He1;->c:Z

    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 2489769
    iget-object v2, v1, LX/He2;->h:LX/G5R;

    iget-object v4, v1, LX/He1;->d:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2489770
    iget-object v5, v2, LX/G5R;->a:Ljava/lang/String;

    invoke-static {v5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2489771
    iget-object v5, v2, LX/G5R;->g:LX/0Zb;

    .line 2489772
    sget-object v7, LX/G5Q;->APP_BACKGROUNDED:LX/G5Q;

    .line 2489773
    iget-object v6, v2, LX/G5R;->b:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_2

    .line 2489774
    iget-object v6, v2, LX/G5R;->b:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/G5L;

    .line 2489775
    iget-object v1, v6, LX/G5L;->a:Ljava/lang/String;

    move-object v6, v1

    .line 2489776
    const-string v1, "click"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2489777
    sget-object v6, LX/G5Q;->USER_CLICKED_RESULT:LX/G5Q;

    .line 2489778
    :goto_0
    move-object v6, v6

    .line 2489779
    new-instance v7, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p0, "full_session"

    invoke-direct {v7, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p0, "search_typeahead"

    .line 2489780
    iput-object p0, v7, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2489781
    move-object v7, v7

    .line 2489782
    const-string p0, "query"

    invoke-virtual {v7, p0, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p0, "uuid"

    iget-object v1, v2, LX/G5R;->a:Ljava/lang/String;

    invoke-virtual {v7, p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p0, "context"

    const-string v1, "mobile_search_android"

    invoke-virtual {v7, p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p0, "keys_pressed"

    iget v1, v2, LX/G5R;->c:I

    invoke-virtual {v7, p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p0, "session_end_reason"

    invoke-virtual {v7, p0, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const-string v7, "events"

    invoke-static {v2}, LX/G5R;->f(LX/G5R;)Lorg/json/JSONArray;

    move-result-object p0

    invoke-virtual {p0}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v6, v7, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    move-object v6, v6

    .line 2489783
    invoke-interface {v5, v6}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2489784
    const/4 v5, 0x0

    iput-object v5, v2, LX/G5R;->a:Ljava/lang/String;

    .line 2489785
    const/16 v1, 0x2b

    const v2, -0x2ece4b2f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2489786
    :cond_1
    const-string v1, "call_quick_action"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2489787
    sget-object v6, LX/G5Q;->USER_CLICKED_INLINE_CALL:LX/G5Q;

    goto :goto_0

    .line 2489788
    :cond_2
    iget-boolean v6, v2, LX/G5R;->f:Z

    if-eqz v6, :cond_3

    .line 2489789
    sget-object v6, LX/G5Q;->BACK_PRESSED:LX/G5Q;

    goto :goto_0

    .line 2489790
    :cond_3
    iget-boolean v6, v2, LX/G5R;->e:Z

    if-eqz v6, :cond_4

    .line 2489791
    sget-object v6, LX/G5Q;->SEARCH_EXIT_PRESSED:LX/G5Q;

    goto :goto_0

    :cond_4
    move-object v6, v7

    goto :goto_0
.end method

.method public final onResume()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x2a

    const v1, 0x2cec56d8

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2489792
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2489793
    const v1, 0x7f0d1446

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    .line 2489794
    new-instance v1, LX/He4;

    iget-object v2, p0, Lcom/facebook/uberbar/ui/UberbarFragment;->a:LX/He3;

    iget-object v3, p0, Lcom/facebook/uberbar/ui/UberbarFragment;->d:Landroid/widget/ImageButton;

    invoke-direct {v1, v2, v3}, LX/He4;-><init>(LX/He2;Landroid/widget/ImageButton;)V

    .line 2489795
    iget-object v2, p0, Lcom/facebook/uberbar/ui/UberbarFragment;->a:LX/He3;

    iget-object v3, p0, Lcom/facebook/uberbar/ui/UberbarFragment;->c:Landroid/widget/EditText;

    iget-object v4, p0, Lcom/facebook/uberbar/ui/UberbarFragment;->e:Landroid/view/ViewGroup;

    invoke-virtual {v2, v3, v4, v1}, LX/He2;->a(Landroid/widget/EditText;Landroid/view/ViewGroup;Landroid/text/TextWatcher;)V

    .line 2489796
    iget-object v1, p0, Lcom/facebook/uberbar/ui/UberbarFragment;->c:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2489797
    iget-object v1, p0, Lcom/facebook/uberbar/ui/UberbarFragment;->c:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    .line 2489798
    iget-object v1, p0, Lcom/facebook/uberbar/ui/UberbarFragment;->b:Landroid/view/inputmethod/InputMethodManager;

    iget-object v2, p0, Lcom/facebook/uberbar/ui/UberbarFragment;->c:Landroid/widget/EditText;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 2489799
    :cond_0
    iget-object v1, p0, Lcom/facebook/uberbar/ui/UberbarFragment;->a:LX/He3;

    .line 2489800
    iget-boolean v2, v1, LX/He1;->c:Z

    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 2489801
    iget-object v2, v1, LX/He2;->h:LX/G5R;

    const/4 v1, 0x0

    .line 2489802
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, LX/G5R;->a:Ljava/lang/String;

    .line 2489803
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    iput-object v3, v2, LX/G5R;->b:Ljava/util/List;

    .line 2489804
    iput-boolean v1, v2, LX/G5R;->e:Z

    .line 2489805
    iput-boolean v1, v2, LX/G5R;->f:Z

    .line 2489806
    const/16 v1, 0x2b

    const v2, 0x7692a5ad

    invoke-static {v5, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
