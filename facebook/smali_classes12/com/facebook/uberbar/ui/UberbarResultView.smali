.class public Lcom/facebook/uberbar/ui/UberbarResultView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:Landroid/widget/TextView;

.field public c:Landroid/widget/TextView;

.field public d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public e:Landroid/widget/ImageView;

.field public f:Landroid/content/res/Resources;

.field public g:Landroid/view/View$OnClickListener;

.field public h:Landroid/view/View$OnClickListener;

.field public i:Landroid/view/View$OnClickListener;

.field public j:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/2do;

.field public l:Lcom/facebook/search/api/SearchTypeaheadResult;

.field public m:I

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/String;

.field public q:Ljava/lang/String;

.field public r:Ljava/lang/String;

.field public s:LX/HeJ;

.field public t:LX/HeI;

.field public u:LX/HeL;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2489992
    const-class v0, Lcom/facebook/uberbar/ui/UberbarResultView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/uberbar/ui/UberbarResultView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2489974
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2489975
    const-class v0, Lcom/facebook/uberbar/ui/UberbarResultView;

    invoke-static {v0, p0}, Lcom/facebook/uberbar/ui/UberbarResultView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2489976
    const v0, 0x7f031539

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2489977
    const v0, 0x7f0d2fcd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->b:Landroid/widget/TextView;

    .line 2489978
    const v0, 0x7f0d2fce

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->c:Landroid/widget/TextView;

    .line 2489979
    const v0, 0x7f0d2fcb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2489980
    const v0, 0x7f0d2fcc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->e:Landroid/widget/ImageView;

    .line 2489981
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->f:Landroid/content/res/Resources;

    .line 2489982
    const v0, 0x7f082a7f

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->n:Ljava/lang/String;

    .line 2489983
    const v0, 0x7f082a80

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->o:Ljava/lang/String;

    .line 2489984
    const v0, 0x7f080f7b

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->p:Ljava/lang/String;

    .line 2489985
    const v0, 0x7f082a81

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->q:Ljava/lang/String;

    .line 2489986
    const v0, 0x7f082a82

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->r:Ljava/lang/String;

    .line 2489987
    new-instance v0, LX/HeA;

    invoke-direct {v0, p0}, LX/HeA;-><init>(Lcom/facebook/uberbar/ui/UberbarResultView;)V

    iput-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->g:Landroid/view/View$OnClickListener;

    .line 2489988
    new-instance v0, LX/HeB;

    invoke-direct {v0, p0}, LX/HeB;-><init>(Lcom/facebook/uberbar/ui/UberbarResultView;)V

    iput-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->h:Landroid/view/View$OnClickListener;

    .line 2489989
    new-instance v0, LX/HeC;

    invoke-direct {v0, p0}, LX/HeC;-><init>(Lcom/facebook/uberbar/ui/UberbarResultView;)V

    iput-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->i:Landroid/view/View$OnClickListener;

    .line 2489990
    new-instance v0, LX/HeE;

    invoke-direct {v0, p0}, LX/HeE;-><init>(Lcom/facebook/uberbar/ui/UberbarResultView;)V

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(LX/0b2;)Z

    .line 2489991
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, Lcom/facebook/uberbar/ui/UberbarResultView;

    const/16 p0, 0x158b

    invoke-static {v1, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v1}, LX/2do;->a(LX/0QB;)LX/2do;

    move-result-object v1

    check-cast v1, LX/2do;

    iput-object p0, p1, Lcom/facebook/uberbar/ui/UberbarResultView;->j:LX/0Or;

    iput-object v1, p1, Lcom/facebook/uberbar/ui/UberbarResultView;->k:LX/2do;

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/search/api/SearchTypeaheadResult;)Lcom/facebook/uberbar/ui/UberbarResultView;
    .locals 7

    .prologue
    const/16 v4, 0x8

    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 2489913
    iput-object p1, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->l:Lcom/facebook/search/api/SearchTypeaheadResult;

    .line 2489914
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->b:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/facebook/search/api/SearchTypeaheadResult;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2489915
    const/4 v6, 0x0

    .line 2489916
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->f:Landroid/content/res/Resources;

    const v1, 0x7f082a7b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PO;->on(Ljava/lang/String;)LX/0PO;

    move-result-object v0

    invoke-virtual {v0}, LX/0PO;->skipNulls()LX/0PO;

    move-result-object v1

    .line 2489917
    sget-object v0, LX/HeD;->b:[I

    iget-object v2, p1, Lcom/facebook/search/api/SearchTypeaheadResult;->m:LX/7BK;

    invoke-virtual {v2}, LX/7BK;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 2489918
    const-string v0, ""

    :goto_0
    move-object v0, v0

    .line 2489919
    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2489920
    iget-object v1, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2489921
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2489922
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2489923
    :goto_1
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p1, Lcom/facebook/search/api/SearchTypeaheadResult;->f:Landroid/net/Uri;

    sget-object v2, Lcom/facebook/uberbar/ui/UberbarResultView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2489924
    sget-object v0, LX/HeD;->b:[I

    iget-object v1, p1, Lcom/facebook/search/api/SearchTypeaheadResult;->m:LX/7BK;

    invoke-virtual {v1}, LX/7BK;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 2489925
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2489926
    :goto_2
    iget-boolean v0, p1, Lcom/facebook/search/api/SearchTypeaheadResult;->o:Z

    if-eqz v0, :cond_4

    .line 2489927
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->f:Landroid/content/res/Resources;

    const v1, 0x7f0219bd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2489928
    iget-object v1, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v3, v3, v0, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2489929
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->b:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/uberbar/ui/UberbarResultView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082a83

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p1, Lcom/facebook/search/api/SearchTypeaheadResult;->l:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2489930
    :goto_3
    return-object p0

    .line 2489931
    :cond_0
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 2489932
    :pswitch_0
    sget-object v0, LX/HeD;->a:[I

    iget-object v1, p1, Lcom/facebook/search/api/SearchTypeaheadResult;->c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    .line 2489933
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    .line 2489934
    :pswitch_1
    invoke-virtual {p1}, Lcom/facebook/search/api/SearchTypeaheadResult;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2489935
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->e:Landroid/widget/ImageView;

    const v1, 0x7f0219b5

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2489936
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->e:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2489937
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->e:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->h:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2489938
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    .line 2489939
    :cond_1
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    .line 2489940
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->e:Landroid/widget/ImageView;

    const v1, 0x7f0219b1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2489941
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->e:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->p:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2489942
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2489943
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->e:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->g:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_2

    .line 2489944
    :pswitch_3
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->e:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->g:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2489945
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->e:Landroid/widget/ImageView;

    const v1, 0x7f0219b4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2489946
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->e:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->q:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2489947
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_2

    .line 2489948
    :pswitch_4
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->j:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2489949
    iget-boolean v0, p1, Lcom/facebook/search/api/SearchTypeaheadResult;->C:Z

    move v0, v0

    .line 2489950
    if-eqz v0, :cond_2

    .line 2489951
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->e:Landroid/widget/ImageView;

    const v1, 0x7f0219bb

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2489952
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->e:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->o:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2489953
    :goto_4
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->e:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->i:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2489954
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_2

    .line 2489955
    :cond_2
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->e:Landroid/widget/ImageView;

    const v1, 0x7f0219ba

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2489956
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->e:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_4

    .line 2489957
    :cond_3
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_2

    .line 2489958
    :cond_4
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v3, v3, v3, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_3

    .line 2489959
    :pswitch_5
    iget-object v0, p1, Lcom/facebook/search/api/SearchTypeaheadResult;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->emptyToNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p1, Lcom/facebook/search/api/SearchTypeaheadResult;->g:Ljava/lang/String;

    invoke-static {v2}, LX/0XM;->emptyToNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v6}, LX/0PO;->join(Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 2489960
    :pswitch_6
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->f:Landroid/content/res/Resources;

    const v2, 0x7f082a7e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->emptyToNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p1, Lcom/facebook/search/api/SearchTypeaheadResult;->g:Ljava/lang/String;

    invoke-static {v2}, LX/0XM;->emptyToNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v6}, LX/0PO;->join(Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 2489961
    :pswitch_7
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->f:Landroid/content/res/Resources;

    const v2, 0x7f082a7d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->emptyToNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p1, Lcom/facebook/search/api/SearchTypeaheadResult;->g:Ljava/lang/String;

    invoke-static {v2}, LX/0XM;->emptyToNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v6}, LX/0PO;->join(Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 2489962
    :pswitch_8
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->f:Landroid/content/res/Resources;

    const v2, 0x7f082a7c

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->emptyToNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p1, Lcom/facebook/search/api/SearchTypeaheadResult;->g:Ljava/lang/String;

    invoke-static {v2}, LX/0XM;->emptyToNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v6}, LX/0PO;->join(Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 2489963
    :pswitch_9
    sget-object v0, LX/HeD;->a:[I

    iget-object v2, p1, Lcom/facebook/search/api/SearchTypeaheadResult;->c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_3

    .line 2489964
    iget-object v0, p1, Lcom/facebook/search/api/SearchTypeaheadResult;->g:Ljava/lang/String;

    .line 2489965
    :goto_5
    invoke-static {v0}, LX/0XM;->emptyToNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p1, Lcom/facebook/search/api/SearchTypeaheadResult;->a:Ljava/lang/String;

    invoke-static {v2}, LX/0XM;->emptyToNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v6}, LX/0PO;->join(Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 2489966
    :pswitch_a
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->f:Landroid/content/res/Resources;

    const v2, 0x7f080f85

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9
        :pswitch_6
        :pswitch_5
        :pswitch_7
        :pswitch_8
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_0
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x4
        :pswitch_a
    .end packed-switch
.end method

.method public bridge synthetic getEventBus()LX/0b4;
    .locals 1

    .prologue
    .line 2489967
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->k:LX/2do;

    move-object v0, v0

    .line 2489968
    return-object v0
.end method

.method public final onLayout(ZIIII)V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DrawAllocation"
        }
    .end annotation

    .prologue
    .line 2489969
    invoke-super/range {p0 .. p5}, Lcom/facebook/widget/CustomRelativeLayout;->onLayout(ZIIII)V

    .line 2489970
    if-eqz p1, :cond_0

    .line 2489971
    iget-object v0, p0, Lcom/facebook/uberbar/ui/UberbarResultView;->e:Landroid/widget/ImageView;

    const/16 v1, 0xc

    invoke-static {v0, v1}, LX/190;->a(Landroid/view/View;I)Landroid/view/TouchDelegate;

    move-result-object v0

    .line 2489972
    invoke-virtual {p0, v0}, Lcom/facebook/uberbar/ui/UberbarResultView;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    .line 2489973
    :cond_0
    return-void
.end method
