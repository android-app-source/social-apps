.class public Lcom/facebook/pages/identity/fragments/photo/PageIdentityPhotosFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2U1;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/auth/viewercontext/ViewerContext;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0SI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2476558
    const-class v0, Lcom/facebook/pages/identity/fragments/photo/PageIdentityPhotosFragment;

    const-string v1, "pages_public_view"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/identity/fragments/photo/PageIdentityPhotosFragment;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2476559
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2476560
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2476561
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, Lcom/facebook/pages/identity/fragments/photo/PageIdentityPhotosFragment;

    const/16 v2, 0xec6

    invoke-static {p1, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v0

    invoke-static {p1}, LX/0eQ;->b(LX/0QB;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v2

    check-cast v2, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-static {p1}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v3

    check-cast v3, LX/0SI;

    invoke-static {p1}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object p1

    check-cast p1, LX/0ad;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/photo/PageIdentityPhotosFragment;->b:LX/0Ot;

    iput-object v2, p0, Lcom/facebook/pages/identity/fragments/photo/PageIdentityPhotosFragment;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    iput-object v3, p0, Lcom/facebook/pages/identity/fragments/photo/PageIdentityPhotosFragment;->d:LX/0SI;

    iput-object p1, p0, Lcom/facebook/pages/identity/fragments/photo/PageIdentityPhotosFragment;->e:LX/0ad;

    .line 2476562
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x5965259e

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2476563
    const v0, 0x7f030e6d

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v7

    .line 2476564
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2476565
    const-string v1, "com.facebook.katana.profile.id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 2476566
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    const-string v2, "page_albums_fragment_tag"

    invoke-virtual {v1, v2}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    if-nez v1, :cond_2

    if-eqz v8, :cond_2

    .line 2476567
    const-string v1, "extra_pages_admin_permissions"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 2476568
    const-string v1, "page_profile_pic_url_extra"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2476569
    const-string v1, "profile_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2476570
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/photo/PageIdentityPhotosFragment;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2U1;

    invoke-virtual {v0, v8}, LX/2U2;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Dk;

    .line 2476571
    if-eqz v0, :cond_9

    .line 2476572
    iget-object v1, v0, LX/8Dk;->b:LX/0am;

    move-object v1, v1

    .line 2476573
    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 2476574
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/photo/PageIdentityPhotosFragment;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2476575
    iget-boolean v5, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v1, v5

    .line 2476576
    if-nez v1, :cond_0

    .line 2476577
    invoke-static {}, Lcom/facebook/auth/viewercontext/ViewerContext;->newBuilder()LX/0SK;

    move-result-object v1

    const/4 v5, 0x1

    .line 2476578
    iput-boolean v5, v1, LX/0SK;->d:Z

    .line 2476579
    move-object v1, v1

    .line 2476580
    iget-object v5, p0, Lcom/facebook/pages/identity/fragments/photo/PageIdentityPhotosFragment;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2476581
    iget-object v9, v5, Lcom/facebook/auth/viewercontext/ViewerContext;->c:Ljava/lang/String;

    move-object v5, v9

    .line 2476582
    iput-object v5, v1, LX/0SK;->c:Ljava/lang/String;

    .line 2476583
    move-object v1, v1

    .line 2476584
    iget-object v5, p0, Lcom/facebook/pages/identity/fragments/photo/PageIdentityPhotosFragment;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2476585
    iget-object v9, v5, Lcom/facebook/auth/viewercontext/ViewerContext;->f:Ljava/lang/String;

    move-object v5, v9

    .line 2476586
    iput-object v5, v1, LX/0SK;->f:Ljava/lang/String;

    .line 2476587
    move-object v1, v1

    .line 2476588
    iget-object v5, p0, Lcom/facebook/pages/identity/fragments/photo/PageIdentityPhotosFragment;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2476589
    iget-object v9, v5, Lcom/facebook/auth/viewercontext/ViewerContext;->e:Ljava/lang/String;

    move-object v5, v9

    .line 2476590
    iput-object v5, v1, LX/0SK;->e:Ljava/lang/String;

    .line 2476591
    move-object v1, v1

    .line 2476592
    iput-object v8, v1, LX/0SK;->a:Ljava/lang/String;

    .line 2476593
    move-object v5, v1

    .line 2476594
    iget-object v1, v0, LX/8Dk;->b:LX/0am;

    move-object v1, v1

    .line 2476595
    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2476596
    iput-object v1, v5, LX/0SK;->b:Ljava/lang/String;

    .line 2476597
    move-object v1, v5

    .line 2476598
    iget-object v5, v0, LX/8Dk;->a:Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;

    move-object v5, v5

    .line 2476599
    invoke-virtual {v5}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->e()Ljava/lang/String;

    move-result-object v5

    .line 2476600
    iput-object v5, v1, LX/0SK;->g:Ljava/lang/String;

    .line 2476601
    move-object v1, v1

    .line 2476602
    iget-object v5, p0, Lcom/facebook/pages/identity/fragments/photo/PageIdentityPhotosFragment;->d:LX/0SI;

    invoke-virtual {v1}, LX/0SK;->h()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    invoke-interface {v5, v1}, LX/0SI;->a(Lcom/facebook/auth/viewercontext/ViewerContext;)V

    .line 2476603
    :cond_0
    if-nez v2, :cond_8

    .line 2476604
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2476605
    iget-object v1, v0, LX/8Dk;->a:Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;

    move-object v1, v1

    .line 2476606
    invoke-virtual {v1}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->eD_()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    .line 2476607
    const/4 v1, 0x0

    move v5, v1

    :goto_0
    if-ge v5, v10, :cond_1

    invoke-virtual {v9, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2476608
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2476609
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_0

    :cond_1
    move-object v1, v2

    .line 2476610
    :goto_1
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2476611
    iget-object v2, v0, LX/8Dk;->a:Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;

    move-object v2, v2

    .line 2476612
    invoke-virtual {v2}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->c()LX/1vs;

    move-result-object v2

    iget-object v5, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2476613
    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :goto_2
    if-eqz v2, :cond_7

    .line 2476614
    iget-object v2, v0, LX/8Dk;->a:Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;

    move-object v2, v2

    .line 2476615
    invoke-virtual {v2}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->c()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const/4 v5, 0x0

    invoke-virtual {v3, v2, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    .line 2476616
    :goto_3
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 2476617
    iget-object v3, v0, LX/8Dk;->a:Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;

    move-object v3, v3

    .line 2476618
    invoke-virtual {v3}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->e()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_6

    .line 2476619
    iget-object v3, v0, LX/8Dk;->a:Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;

    move-object v0, v3

    .line 2476620
    invoke-virtual {v0}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->e()Ljava/lang/String;

    move-result-object v0

    .line 2476621
    :goto_4
    new-instance v3, LX/89I;

    invoke-static {v8}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    sget-object v9, LX/2rw;->PAGE:LX/2rw;

    invoke-direct {v3, v4, v5, v9}, LX/89I;-><init>(JLX/2rw;)V

    .line 2476622
    iput-object v0, v3, LX/89I;->c:Ljava/lang/String;

    .line 2476623
    move-object v0, v3

    .line 2476624
    iput-object v2, v0, LX/89I;->d:Ljava/lang/String;

    .line 2476625
    move-object v0, v0

    .line 2476626
    invoke-virtual {v0}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    .line 2476627
    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/photo/PageIdentityPhotosFragment;->e:LX/0ad;

    sget-object v3, LX/0c0;->Live:LX/0c0;

    sget-short v4, LX/17g;->d:S

    const/4 v5, 0x0

    invoke-interface {v2, v3, v4, v5}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v2

    if-nez v2, :cond_5

    .line 2476628
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {v0, v8, v1}, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->a(Landroid/os/Bundle;Ljava/lang/String;Z)Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    move-result-object v0

    .line 2476629
    :goto_5
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d2351

    const-string v3, "page_albums_fragment_tag"

    invoke-virtual {v1, v2, v0, v3}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2476630
    :cond_2
    const v0, 0x5ba9627c

    invoke-static {v0, v6}, LX/02F;->f(II)V

    return-object v7

    .line 2476631
    :cond_3
    const/4 v2, 0x0

    goto :goto_2

    :cond_4
    const/4 v2, 0x0

    goto :goto_2

    .line 2476632
    :cond_5
    sget-object v2, Lcom/facebook/pages/identity/fragments/photo/PageIdentityPhotosFragment;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-static {v8, v2, v1, v0}, Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;Ljava/util/ArrayList;Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/pages/common/photos/PagesAlbumsListFragment;

    move-result-object v0

    goto :goto_5

    :cond_6
    move-object v0, v4

    goto :goto_4

    :cond_7
    move-object v2, v3

    goto :goto_3

    :cond_8
    move-object v1, v2

    goto/16 :goto_1

    :cond_9
    move-object v0, v4

    move-object v1, v2

    move-object v2, v3

    goto :goto_4
.end method

.method public final onStart()V
    .locals 6

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x1f7933b

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2476633
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2476634
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2476635
    if-eqz v0, :cond_0

    .line 2476636
    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/photo/PageIdentityPhotosFragment;->e:LX/0ad;

    sget-object v3, LX/0c0;->Live:LX/0c0;

    sget-short v4, LX/17g;->d:S

    const/4 v5, 0x0

    invoke-interface {v2, v3, v4, v5}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2476637
    const v2, 0x7f0817db

    invoke-interface {v0, v2}, LX/1ZF;->x_(I)V

    .line 2476638
    invoke-interface {v0}, LX/1ZF;->lH_()V

    .line 2476639
    :cond_0
    :goto_0
    const v0, -0x68b78130

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void

    .line 2476640
    :cond_1
    const v2, 0x7f081281

    invoke-interface {v0, v2}, LX/1ZF;->x_(I)V

    .line 2476641
    const/4 v2, 0x1

    invoke-interface {v0, v2}, LX/1ZF;->k_(Z)V

    goto :goto_0
.end method
