.class public Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;
.super LX/E8t;
.source ""


# instance fields
.field public b:LX/HEW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/HEk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/CXj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/H8N;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/HNF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/HN8;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/HNE;

.field private j:LX/HN7;

.field private k:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/pages/common/megaphone/ui/PageAdminMegaphoneStoryView;",
            ">;"
        }
    .end annotation
.end field

.field private l:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;",
            ">;"
        }
    .end annotation
.end field

.field private m:LX/H8M;

.field public n:Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;

.field private o:Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;

.field private p:Landroid/view/View;

.field private q:Landroid/view/View;

.field public r:Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;

.field private s:LX/CXx;

.field private t:Landroid/os/ParcelUuid;

.field private u:J

.field private v:Z

.field private w:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2476772
    invoke-direct {p0, p1}, LX/E8t;-><init>(Landroid/content/Context;)V

    .line 2476773
    iput-boolean v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->v:Z

    .line 2476774
    iput-boolean v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->w:Z

    .line 2476775
    invoke-direct {p0}, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->f()V

    .line 2476776
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2476777
    invoke-direct {p0, p1, p2}, LX/E8t;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2476778
    iput-boolean v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->v:Z

    .line 2476779
    iput-boolean v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->w:Z

    .line 2476780
    invoke-direct {p0}, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->f()V

    .line 2476781
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLPageActionType;Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;LX/0ta;)LX/HN7;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2476763
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_SERVICES:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-ne p1, v0, :cond_0

    invoke-virtual {p2}, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;->c()Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel$ServicesCardModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;->c()Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel$ServicesCardModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel$ServicesCardModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/HN7;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2476764
    :cond_0
    const/4 v0, 0x0

    .line 2476765
    :goto_0
    return-object v0

    .line 2476766
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->j:LX/HN7;

    if-eqz v0, :cond_2

    sget-object v0, LX/0ta;->FROM_SERVER:LX/0ta;

    if-ne p3, v0, :cond_3

    .line 2476767
    :cond_2
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->h:LX/HN8;

    invoke-virtual {p2}, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;->c()Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel$ServicesCardModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel$ServicesCardModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/HN8;->a(Ljava/lang/String;)LX/HN7;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->j:LX/HN7;

    .line 2476768
    :cond_3
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->j:LX/HN7;

    goto :goto_0
.end method

.method public static synthetic a(Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;Lcom/facebook/graphql/enums/GraphQLPageActionType;Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;LX/0ta;)LX/HN7;
    .locals 1

    .prologue
    .line 2476782
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->a(Lcom/facebook/graphql/enums/GraphQLPageActionType;Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;LX/0ta;)LX/HN7;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;LX/HEW;LX/HEk;LX/CXj;LX/H8N;LX/0ad;LX/HNF;LX/HN8;)V
    .locals 0

    .prologue
    .line 2476783
    iput-object p1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->b:LX/HEW;

    iput-object p2, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->c:LX/HEk;

    iput-object p3, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->d:LX/CXj;

    iput-object p4, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->e:LX/H8N;

    iput-object p5, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->f:LX/0ad;

    iput-object p6, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->g:LX/HNF;

    iput-object p7, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->h:LX/HN8;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 9

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v7

    move-object v0, p0

    check-cast v0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;

    invoke-static {v7}, LX/HEW;->a(LX/0QB;)LX/HEW;

    move-result-object v1

    check-cast v1, LX/HEW;

    invoke-static {v7}, LX/HEk;->a(LX/0QB;)LX/HEk;

    move-result-object v2

    check-cast v2, LX/HEk;

    invoke-static {v7}, LX/CXj;->a(LX/0QB;)LX/CXj;

    move-result-object v3

    check-cast v3, LX/CXj;

    const-class v4, LX/H8N;

    invoke-interface {v7, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/H8N;

    invoke-static {v7}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    const-class v6, LX/HNF;

    invoke-interface {v7, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/HNF;

    const-class v8, LX/HN8;

    invoke-interface {v7, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/HN8;

    invoke-static/range {v0 .. v7}, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->a(Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;LX/HEW;LX/HEk;LX/CXj;LX/H8N;LX/0ad;LX/HNF;LX/HN8;)V

    return-void
.end method

.method private b(LX/CZd;)V
    .locals 2

    .prologue
    .line 2476784
    invoke-direct {p0, p1}, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->c(LX/CZd;)V

    .line 2476785
    const v0, 0x7f0d242f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->p:Landroid/view/View;

    .line 2476786
    iget-boolean v0, p1, LX/CZd;->l:Z

    move v0, v0

    .line 2476787
    if-nez v0, :cond_0

    .line 2476788
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->p:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2476789
    :cond_0
    invoke-direct {p0, p1}, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->setupMetabox(LX/CZd;)V

    .line 2476790
    return-void
.end method

.method private c(LX/CZd;)V
    .locals 7

    .prologue
    .line 2476791
    iget-object v0, p1, LX/CZd;->g:LX/0Px;

    move-object v0, v0

    .line 2476792
    if-eqz v0, :cond_0

    .line 2476793
    iget-object v0, p1, LX/CZd;->g:LX/0Px;

    move-object v0, v0

    .line 2476794
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2476795
    :cond_0
    invoke-direct {p0}, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->g()V

    .line 2476796
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->l:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2476797
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->l:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->c()V

    .line 2476798
    :cond_1
    :goto_0
    return-void

    .line 2476799
    :cond_2
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->l:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;

    .line 2476800
    invoke-virtual {v1}, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->removeAllViews()V

    .line 2476801
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->t:Landroid/os/ParcelUuid;

    invoke-virtual {v1, v0}, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->setLoggingUuid(Landroid/os/ParcelUuid;)V

    .line 2476802
    iget-wide v2, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->u:J

    .line 2476803
    iget-object v0, p1, LX/CZd;->g:LX/0Px;

    move-object v4, v0

    .line 2476804
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->MOBILE_PAGE_PRESENCE_CALL_TO_ACTION:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    iget-boolean v6, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->w:Z

    invoke-virtual/range {v1 .. v6}, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->a(JLX/0Px;Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;Z)V

    .line 2476805
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->setVisibility(I)V

    goto :goto_0
.end method

.method private f()V
    .locals 13

    .prologue
    .line 2476806
    const-class v0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;

    invoke-static {v0, p0}, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2476807
    const v0, 0x7f030ed1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2476808
    const v0, 0x7f0d242a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->n:Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;

    .line 2476809
    new-instance v1, LX/0zw;

    const v0, 0x7f0d2428

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->k:LX/0zw;

    .line 2476810
    const v0, 0x7f0d2429

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->q:Landroid/view/View;

    .line 2476811
    new-instance v1, LX/0zw;

    const v0, 0x7f0d242b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->l:LX/0zw;

    .line 2476812
    const v0, 0x7f0d242c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomFrameLayout;

    .line 2476813
    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->e:LX/H8N;

    const v1, 0x7f0d242d

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fig/actionbar/FigActionBar;

    .line 2476814
    new-instance v3, LX/H8M;

    const-class v4, LX/H9E;

    invoke-interface {v2, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/H9E;

    invoke-static {v2}, LX/HDT;->a(LX/0QB;)LX/HDT;

    move-result-object v5

    check-cast v5, LX/HDT;

    invoke-static {v2}, LX/HAF;->a(LX/0QB;)LX/HAF;

    move-result-object v6

    check-cast v6, LX/HAF;

    invoke-static {v2}, LX/CXj;->a(LX/0QB;)LX/CXj;

    move-result-object v7

    check-cast v7, LX/CXj;

    invoke-static {v2}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v8

    check-cast v8, LX/0kb;

    invoke-static {v2}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-static {v2}, LX/8Do;->a(LX/0QB;)LX/8Do;

    move-result-object v10

    check-cast v10, LX/8Do;

    .line 2476815
    new-instance v12, LX/H8P;

    invoke-static {v2}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v11

    check-cast v11, LX/0iA;

    invoke-direct {v12, v11}, LX/H8P;-><init>(LX/0iA;)V

    .line 2476816
    move-object v11, v12

    .line 2476817
    check-cast v11, LX/H8P;

    move-object v12, v1

    invoke-direct/range {v3 .. v12}, LX/H8M;-><init>(LX/H9E;LX/HDT;LX/HAF;LX/CXj;LX/0kb;LX/03V;LX/8Do;LX/H8P;Lcom/facebook/fig/actionbar/FigActionBar;)V

    .line 2476818
    move-object v1, v3

    .line 2476819
    iput-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->m:LX/H8M;

    .line 2476820
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->m:LX/H8M;

    .line 2476821
    new-instance v2, LX/H8J;

    invoke-direct {v2, v1}, LX/H8J;-><init>(LX/H8M;)V

    move-object v1, v2

    .line 2476822
    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomFrameLayout;->a(LX/10U;)V

    .line 2476823
    const v0, 0x7f0d242e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->o:Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;

    .line 2476824
    return-void
.end method

.method private g()V
    .locals 5

    .prologue
    .line 2476825
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->d:LX/CXj;

    new-instance v1, LX/CXq;

    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->t:Landroid/os/ParcelUuid;

    sget-object v3, LX/CXp;->CALL_TO_ACTION:LX/CXp;

    sget-object v4, LX/0ta;->NO_DATA:LX/0ta;

    invoke-static {v4}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, LX/CXq;-><init>(Landroid/os/ParcelUuid;LX/CXp;LX/0am;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2476826
    return-void
.end method

.method private h()V
    .locals 6

    .prologue
    .line 2476769
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->t:Landroid/os/ParcelUuid;

    if-eqz v0, :cond_0

    .line 2476770
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->d:LX/CXj;

    new-instance v1, LX/CXq;

    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->t:Landroid/os/ParcelUuid;

    sget-object v3, LX/CXp;->METABOX:LX/CXp;

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v4

    const-string v5, "NoMetabox"

    invoke-static {v5}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, LX/CXq;-><init>(Landroid/os/ParcelUuid;LX/CXp;LX/0am;LX/0Px;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2476771
    :cond_0
    return-void
.end method

.method private i()V
    .locals 2

    .prologue
    .line 2476759
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->s:LX/CXx;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->v:Z

    if-nez v0, :cond_1

    .line 2476760
    :cond_0
    :goto_0
    return-void

    .line 2476761
    :cond_1
    new-instance v0, LX/HWZ;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->t:Landroid/os/ParcelUuid;

    invoke-direct {v0, p0, v1}, LX/HWZ;-><init>(Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;Landroid/os/ParcelUuid;)V

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->s:LX/CXx;

    .line 2476762
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->d:LX/CXj;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->s:LX/CXx;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    goto :goto_0
.end method

.method public static j(Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;)V
    .locals 1

    .prologue
    .line 2476755
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->r:Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->v:Z

    if-nez v0, :cond_1

    .line 2476756
    :cond_0
    :goto_0
    return-void

    .line 2476757
    :cond_1
    const v0, 0x7f0d2430

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2476758
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->r:Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;

    goto :goto_0
.end method

.method public static k(Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;)V
    .locals 2

    .prologue
    .line 2476752
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->i:LX/HNE;

    if-nez v0, :cond_0

    .line 2476753
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->g:LX/HNF;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->t:Landroid/os/ParcelUuid;

    invoke-virtual {v0, v1}, LX/HNF;->a(Landroid/os/ParcelUuid;)LX/HNE;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->i:LX/HNE;

    .line 2476754
    :cond_0
    return-void
.end method

.method private setupMetabox(LX/CZd;)V
    .locals 2

    .prologue
    .line 2476736
    const/4 v1, 0x0

    .line 2476737
    if-eqz p1, :cond_1

    .line 2476738
    iget-object v0, p1, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v0

    .line 2476739
    if-eqz v0, :cond_1

    .line 2476740
    iget-object v0, p1, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v0

    .line 2476741
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->p()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2476742
    iget-object v0, p1, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v0

    .line 2476743
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->p()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2476744
    iget-object v0, p1, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v0

    .line 2476745
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->p()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2476746
    if-nez v0, :cond_0

    .line 2476747
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->o:Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->setVisibility(I)V

    .line 2476748
    invoke-direct {p0}, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->h()V

    .line 2476749
    :goto_1
    return-void

    .line 2476750
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->o:Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;

    invoke-virtual {v0, p1}, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->a(LX/CZd;)V

    .line 2476751
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->o:Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->setVisibility(I)V

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/CZd;)V
    .locals 4

    .prologue
    .line 2476725
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->n:Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;

    invoke-virtual {v0, p1}, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->setPageHeaderData(LX/CZd;)V

    .line 2476726
    iget-wide v2, p1, LX/CZd;->a:J

    move-wide v0, v2

    .line 2476727
    iput-wide v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->u:J

    .line 2476728
    sget-object v0, LX/8A3;->BASIC_ADMIN:LX/8A3;

    invoke-virtual {p1, v0}, LX/CZd;->a(LX/8A3;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->v:Z

    .line 2476729
    sget-object v0, LX/8A3;->EDIT_PROFILE:LX/8A3;

    invoke-virtual {p1, v0}, LX/CZd;->a(LX/8A3;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->w:Z

    .line 2476730
    invoke-direct {p0}, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->i()V

    .line 2476731
    invoke-virtual {p1}, LX/CZd;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, LX/CZd;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->v:Z

    if-eqz v0, :cond_1

    .line 2476732
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->m:LX/H8M;

    invoke-virtual {v0, p1}, LX/H8M;->a(LX/CZd;)V

    .line 2476733
    :cond_1
    invoke-virtual {p1}, LX/CZd;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2476734
    invoke-direct {p0, p1}, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->b(LX/CZd;)V

    .line 2476735
    :cond_2
    return-void
.end method

.method public final a(LX/HEh;)V
    .locals 5

    .prologue
    .line 2476689
    iget-object v0, p1, LX/HEh;->c:Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;

    move-object v0, v0

    .line 2476690
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->b:LX/HEW;

    .line 2476691
    iget-object v1, v0, LX/HEW;->a:LX/0ad;

    sget-short v2, LX/HEV;->a:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    move v0, v1

    .line 2476692
    if-eqz v0, :cond_0

    .line 2476693
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->c:LX/HEk;

    .line 2476694
    iput-object p1, v0, LX/HEk;->a:LX/HEh;

    .line 2476695
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->c:LX/HEk;

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->k:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/megaphone/ui/PageAdminMegaphoneStoryView;

    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->q:Landroid/view/View;

    .line 2476696
    iput-object v0, v1, LX/HEk;->b:Lcom/facebook/pages/common/megaphone/ui/PageAdminMegaphoneStoryView;

    .line 2476697
    iput-object v2, v1, LX/HEk;->c:Landroid/view/View;

    .line 2476698
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 2476699
    iget-object v3, v1, LX/HEk;->a:LX/HEh;

    .line 2476700
    iget-object v4, v3, LX/HEh;->c:Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;

    move-object v3, v4

    .line 2476701
    iget-object v4, v1, LX/HEk;->b:Lcom/facebook/pages/common/megaphone/ui/PageAdminMegaphoneStoryView;

    invoke-virtual {v4, v0}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setShowPrimaryButton(Z)V

    .line 2476702
    iget-object v4, v1, LX/HEk;->b:Lcom/facebook/pages/common/megaphone/ui/PageAdminMegaphoneStoryView;

    invoke-virtual {v3}, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;->j()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4, p1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    .line 2476703
    iget-object v4, v1, LX/HEk;->b:Lcom/facebook/pages/common/megaphone/ui/PageAdminMegaphoneStoryView;

    invoke-virtual {v4, v2}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setShowSecondaryButton(Z)V

    .line 2476704
    iget-object v4, v1, LX/HEk;->b:Lcom/facebook/pages/common/megaphone/ui/PageAdminMegaphoneStoryView;

    invoke-virtual {v4, v0}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setShowCloseButton(Z)V

    .line 2476705
    iget-object v4, v1, LX/HEk;->b:Lcom/facebook/pages/common/megaphone/ui/PageAdminMegaphoneStoryView;

    invoke-virtual {v3}, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;->o()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4, p1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setTitle(Ljava/lang/CharSequence;)V

    .line 2476706
    iget-object v4, v1, LX/HEk;->b:Lcom/facebook/pages/common/megaphone/ui/PageAdminMegaphoneStoryView;

    invoke-virtual {v3}, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;->l()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4, p1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 2476707
    invoke-virtual {v3}, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;->n()LX/1vs;

    move-result-object v4

    iget-object p1, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    iget-object v0, v1, LX/HEk;->b:Lcom/facebook/pages/common/megaphone/ui/PageAdminMegaphoneStoryView;

    invoke-virtual {p1, v4, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setImageUri(Landroid/net/Uri;)V

    .line 2476708
    iget-object v4, v1, LX/HEk;->b:Lcom/facebook/pages/common/megaphone/ui/PageAdminMegaphoneStoryView;

    invoke-virtual {v3}, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;->k()Ljava/lang/String;

    move-result-object v3

    .line 2476709
    new-instance p1, LX/HEi;

    invoke-direct {p1, v1, v3}, LX/HEi;-><init>(LX/HEk;Ljava/lang/String;)V

    move-object v3, p1

    .line 2476710
    invoke-virtual {v4, v3}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setOnPrimaryButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 2476711
    iget-object v3, v1, LX/HEk;->b:Lcom/facebook/pages/common/megaphone/ui/PageAdminMegaphoneStoryView;

    .line 2476712
    new-instance v4, LX/HEj;

    invoke-direct {v4, v1}, LX/HEj;-><init>(LX/HEk;)V

    move-object v4, v4

    .line 2476713
    iput-object v4, v3, Lcom/facebook/fbui/widget/megaphone/Megaphone;->m:LX/AhV;

    .line 2476714
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->c:LX/HEk;

    const/4 v2, 0x0

    .line 2476715
    iget-object v1, v0, LX/HEk;->b:Lcom/facebook/pages/common/megaphone/ui/PageAdminMegaphoneStoryView;

    invoke-virtual {v1, v2}, Lcom/facebook/pages/common/megaphone/ui/PageAdminMegaphoneStoryView;->setVisibility(I)V

    .line 2476716
    iget-object v1, v0, LX/HEk;->c:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2476717
    iget-object v1, v0, LX/HEk;->d:LX/HEU;

    iget-object v2, v0, LX/HEk;->a:LX/HEh;

    .line 2476718
    iget-object v3, v2, LX/HEh;->c:Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;

    move-object v2, v3

    .line 2476719
    invoke-virtual {v2}, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;->m()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, LX/HEk;->a:LX/HEh;

    .line 2476720
    iget-object v0, v3, LX/HEh;->b:Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel;

    move-object v3, v0

    .line 2476721
    invoke-virtual {v3}, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel;->j()Ljava/lang/String;

    move-result-object v3

    .line 2476722
    const-string v0, "IMPRESSION"

    invoke-static {v1, v2, v3, v0}, LX/HEU;->a(LX/HEU;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2476723
    :goto_0
    return-void

    .line 2476724
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->c:LX/HEk;

    invoke-virtual {v0}, LX/HEk;->b()V

    goto :goto_0
.end method

.method public final b()V
    .locals 6

    .prologue
    .line 2476672
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->s:LX/CXx;

    if-eqz v0, :cond_0

    .line 2476673
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->d:LX/CXj;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->s:LX/CXx;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 2476674
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->m:LX/H8M;

    const/4 v5, 0x0

    .line 2476675
    iget-object v1, v0, LX/H8M;->n:LX/H8F;

    if-eqz v1, :cond_1

    .line 2476676
    iget-object v1, v0, LX/H8M;->a:LX/HDT;

    iget-object v2, v0, LX/H8M;->n:LX/H8F;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2476677
    iput-object v5, v0, LX/H8M;->n:LX/H8F;

    .line 2476678
    :cond_1
    iget-object v1, v0, LX/H8M;->o:LX/H8H;

    if-eqz v1, :cond_2

    .line 2476679
    iget-object v1, v0, LX/H8M;->a:LX/HDT;

    iget-object v2, v0, LX/H8M;->o:LX/H8H;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2476680
    iput-object v5, v0, LX/H8M;->o:LX/H8H;

    .line 2476681
    :cond_2
    iget-object v1, v0, LX/H8M;->m:LX/0Px;

    if-eqz v1, :cond_4

    .line 2476682
    iget-object v1, v0, LX/H8M;->m:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v3

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_3

    iget-object v1, v0, LX/H8M;->m:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/H8E;

    .line 2476683
    iget-object v4, v0, LX/H8M;->a:LX/HDT;

    invoke-virtual {v4, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 2476684
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2476685
    :cond_3
    iput-object v5, v0, LX/H8M;->m:LX/0Px;

    .line 2476686
    :cond_4
    invoke-static {v0}, LX/H8M;->f(LX/H8M;)V

    .line 2476687
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->l:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;

    invoke-virtual {v0}, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->b()V

    .line 2476688
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 2476669
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->r:Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;

    if-eqz v0, :cond_0

    .line 2476670
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->r:Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;->a()V

    .line 2476671
    :cond_0
    return-void
.end method

.method public getActionBar()Landroid/view/View;
    .locals 1

    .prologue
    .line 2476666
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->m:LX/H8M;

    .line 2476667
    iget-object p0, v0, LX/H8M;->k:Lcom/facebook/fig/actionbar/FigActionBar;

    move-object v0, p0

    .line 2476668
    return-object v0
.end method

.method public getAdminBarMeasuredHeight()I
    .locals 1

    .prologue
    .line 2476657
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->r:Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->r:Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->r:Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;->getMeasuredHeight()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getHeaderView()Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;
    .locals 1

    .prologue
    .line 2476665
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->n:Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;

    return-object v0
.end method

.method public setLoggingUuid(Landroid/os/ParcelUuid;)V
    .locals 1

    .prologue
    .line 2476658
    iput-object p1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->t:Landroid/os/ParcelUuid;

    .line 2476659
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->m:LX/H8M;

    .line 2476660
    iput-object p1, v0, LX/H8M;->p:Landroid/os/ParcelUuid;

    .line 2476661
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->n:Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;

    invoke-virtual {v0, p1}, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->setFragmentUuidForLogging(Landroid/os/ParcelUuid;)V

    .line 2476662
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->o:Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;

    .line 2476663
    iput-object p1, v0, Lcom/facebook/pages/common/surface/ui/metabox/PagesMetaboxView;->n:Landroid/os/ParcelUuid;

    .line 2476664
    return-void
.end method
