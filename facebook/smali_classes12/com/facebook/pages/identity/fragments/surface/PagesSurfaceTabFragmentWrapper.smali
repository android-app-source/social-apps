.class public Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements LX/0fj;
.implements LX/8EK;
.implements LX/HPA;
.implements LX/HPB;
.implements LX/GZf;


# instance fields
.field public a:Z

.field private b:Z

.field public c:LX/GZf;

.field private d:Z

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2478498
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2478499
    iput-boolean v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->a:Z

    .line 2478500
    iput-boolean v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->b:Z

    .line 2478501
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->d:Z

    .line 2478502
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2478503
    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->e:LX/0Ot;

    .line 2478504
    return-void
.end method


# virtual methods
.method public final E_(I)V
    .locals 1

    .prologue
    .line 2478505
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->c:LX/GZf;

    invoke-interface {v0, p1}, LX/GZf;->E_(I)V

    .line 2478506
    return-void
.end method

.method public final S_()Z
    .locals 1

    .prologue
    .line 2478462
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->c:LX/GZf;

    instance-of v0, v0, LX/0fj;

    if-eqz v0, :cond_0

    .line 2478463
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->c:LX/GZf;

    check-cast v0, LX/0fj;

    invoke-interface {v0}, LX/0fj;->S_()Z

    move-result v0

    .line 2478464
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2478507
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->c:LX/GZf;

    if-nez v0, :cond_0

    move-object v0, v1

    .line 2478508
    :goto_0
    return-object v0

    .line 2478509
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->c:LX/GZf;

    instance-of v0, v0, LX/0fh;

    if-nez v0, :cond_1

    .line 2478510
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v2, "page_fragment_analytics_name_return_null"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Page Fragment "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->c:LX/GZf;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "is not an instance of AnalyticsFragment"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 2478511
    goto :goto_0

    .line 2478512
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->c:LX/GZf;

    check-cast v0, LX/0fh;

    invoke-interface {v0}, LX/0f2;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 2478513
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->c:LX/GZf;

    invoke-interface {v0, p1}, LX/GZf;->a(I)V

    .line 2478514
    return-void
.end method

.method public final a(LX/8EI;)V
    .locals 1

    .prologue
    .line 2478515
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->c:LX/GZf;

    instance-of v0, v0, LX/8EK;

    if-eqz v0, :cond_0

    .line 2478516
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->c:LX/GZf;

    check-cast v0, LX/8EK;

    invoke-interface {v0, p1}, LX/8EK;->a(LX/8EI;)V

    .line 2478517
    :cond_0
    return-void
.end method

.method public final a(LX/CZd;)V
    .locals 1

    .prologue
    .line 2478518
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->c:LX/GZf;

    instance-of v0, v0, LX/HPA;

    if-eqz v0, :cond_0

    .line 2478519
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->c:LX/GZf;

    check-cast v0, LX/HPA;

    invoke-interface {v0, p1}, LX/HPA;->a(LX/CZd;)V

    .line 2478520
    :cond_0
    return-void
.end method

.method public final a(LX/E8t;)V
    .locals 1

    .prologue
    .line 2478521
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->c:LX/GZf;

    invoke-interface {v0, p1}, LX/GZf;->a(LX/E8t;)V

    .line 2478522
    return-void
.end method

.method public final a(LX/GZf;)V
    .locals 2

    .prologue
    .line 2478523
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2478524
    iput-object p1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->c:LX/GZf;

    .line 2478525
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->c:LX/GZf;

    check-cast v0, Landroid/support/v4/app/Fragment;

    iget-boolean v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->d:Z

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setUserVisibleHint(Z)V

    .line 2478526
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->a:Z

    .line 2478527
    return-void
.end method

.method public final a(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V
    .locals 1

    .prologue
    .line 2478528
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->c:LX/GZf;

    invoke-interface {v0, p1}, LX/GZf;->a(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V

    .line 2478529
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 2478495
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->c:LX/GZf;

    instance-of v0, v0, LX/8EK;

    if-eqz v0, :cond_0

    .line 2478496
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->c:LX/GZf;

    check-cast v0, LX/8EK;

    invoke-interface {v0, p1}, LX/8EK;->a(Z)V

    .line 2478497
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2478530
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->c:LX/GZf;

    instance-of v0, v0, LX/HPB;

    if-eqz v0, :cond_0

    .line 2478531
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->c:LX/GZf;

    check-cast v0, LX/HPB;

    invoke-interface {v0}, LX/HPB;->b()V

    .line 2478532
    :cond_0
    return-void
.end method

.method public final b(Z)V
    .locals 3

    .prologue
    .line 2478454
    iget-boolean v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->b:Z

    if-nez v0, :cond_1

    .line 2478455
    iget-boolean v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->a:Z

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2478456
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d23e2

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->c:LX/GZf;

    check-cast v0, Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {v1, v2, v0}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->c()I

    .line 2478457
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->b()Z

    .line 2478458
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->c:LX/GZf;

    instance-of v0, v0, LX/HPB;

    if-eqz v0, :cond_0

    .line 2478459
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->c:LX/GZf;

    check-cast v0, LX/HPB;

    invoke-interface {v0}, LX/HPB;->b()V

    .line 2478460
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->b:Z

    .line 2478461
    :cond_1
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 2478465
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->c:LX/GZf;

    instance-of v0, v0, LX/HPB;

    if-eqz v0, :cond_0

    .line 2478466
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->c:LX/GZf;

    check-cast v0, LX/HPB;

    invoke-interface {v0}, LX/HPB;->f()V

    .line 2478467
    :cond_0
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 2478468
    iget-boolean v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->b:Z

    if-eqz v0, :cond_0

    .line 2478469
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->c:LX/GZf;

    invoke-interface {v0}, LX/GZf;->g()V

    .line 2478470
    :cond_0
    return-void
.end method

.method public final getUserVisibleHint()Z
    .locals 1

    .prologue
    .line 2478471
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->c:LX/GZf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->c:LX/GZf;

    check-cast v0, Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2478472
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->d:Z

    .line 2478473
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->c:LX/GZf;

    check-cast v0, Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2478474
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2478475
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->c:LX/GZf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->c:LX/GZf;

    instance-of v0, v0, Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_0

    .line 2478476
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->c:LX/GZf;

    check-cast v0, Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2478477
    :cond_0
    return-void
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 2478478
    iget-boolean v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->b:Z

    if-eqz v0, :cond_0

    .line 2478479
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->c:LX/GZf;

    check-cast v0, Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/app/Fragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 2478480
    :goto_0
    return-void

    .line 2478481
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x523f7af9

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2478482
    const v1, 0x7f030eac

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x66dd7f77

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final setHasOptionsMenu(Z)V
    .locals 1

    .prologue
    .line 2478483
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->setHasOptionsMenu(Z)V

    .line 2478484
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->c:LX/GZf;

    if-eqz v0, :cond_0

    .line 2478485
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->c:LX/GZf;

    check-cast v0, Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/Fragment;->setHasOptionsMenu(Z)V

    .line 2478486
    :cond_0
    return-void
.end method

.method public final setMenuVisibility(Z)V
    .locals 1

    .prologue
    .line 2478487
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->setMenuVisibility(Z)V

    .line 2478488
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->c:LX/GZf;

    if-eqz v0, :cond_0

    .line 2478489
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->c:LX/GZf;

    check-cast v0, Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/Fragment;->setMenuVisibility(Z)V

    .line 2478490
    :cond_0
    return-void
.end method

.method public final setUserVisibleHint(Z)V
    .locals 1

    .prologue
    .line 2478491
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->c:LX/GZf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->c:LX/GZf;

    check-cast v0, Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2478492
    :cond_0
    iput-boolean p1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->d:Z

    .line 2478493
    :goto_0
    return-void

    .line 2478494
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;->c:LX/GZf;

    check-cast v0, Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/Fragment;->setUserVisibleHint(Z)V

    goto :goto_0
.end method
