.class public Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fj;


# instance fields
.field public A:Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;

.field public B:Lcom/facebook/pages/common/deeplink/simplifiedheader/view/PagesSimplifiedHeader;

.field public C:Lcom/facebook/pages/common/surface/adminbar/ui/PagesAdminTabBarView;

.field public D:LX/CXx;

.field public E:LX/CXx;

.field public F:LX/CXt;

.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/HBf;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/HPs;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/HNF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/HN8;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/HBi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/CXj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/HNE;

.field public m:LX/HBh;

.field public n:LX/HN7;

.field public o:Landroid/os/ParcelUuid;

.field public p:Lcom/facebook/base/fragment/FbFragment;

.field public q:J

.field public r:Ljava/lang/String;

.field public s:Ljava/lang/String;

.field public t:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public u:Z

.field public v:Z

.field public w:Ljava/lang/String;

.field public x:Lcom/facebook/widget/FbSwipeRefreshLayout;

.field private y:Lcom/facebook/resources/ui/FbFrameLayout;

.field public z:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Landroid/view/ViewGroup;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2477000
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method public static a(JLjava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLPageActionType;ZZLjava/lang/String;)Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;
    .locals 4
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2476988
    new-instance v0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    invoke-direct {v0}, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;-><init>()V

    .line 2476989
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2476990
    const-string v2, "com.facebook.katana.profile.id"

    invoke-virtual {v1, v2, p0, p1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2476991
    const-string v2, "profile_name"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2476992
    invoke-static {p3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2476993
    const-string v2, "page_profile_pic_url_extra"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2476994
    :cond_0
    const-string v2, "extra_page_presence_tab_type"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2476995
    const-string v2, "extra_launched_from_deeplink"

    invoke-virtual {v1, v2, p5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2476996
    const-string v2, "extra_is_admin"

    invoke-virtual {v1, v2, p6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2476997
    const-string v2, "extra_page_tab_entry_point"

    invoke-virtual {v1, v2, p7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2476998
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2476999
    return-object v0
.end method

.method public static a(Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;JLcom/facebook/graphql/enums/GraphQLPageActionType;Z)V
    .locals 9

    .prologue
    .line 2476986
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "tab_data_fetch_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HPs;

    iget-boolean v6, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->u:Z

    move-wide v2, p1

    move-object v4, p3

    move v5, p4

    invoke-virtual/range {v1 .. v6}, LX/HPs;->a(JLcom/facebook/graphql/enums/GraphQLPageActionType;ZZ)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, LX/HWb;

    invoke-direct {v2, p0, p3}, LX/HWb;-><init>(Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;Lcom/facebook/graphql/enums/GraphQLPageActionType;)V

    invoke-virtual {v0, v7, v1, v2}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2476987
    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    .line 2476909
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d2459

    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->p:Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {v0, v1, v2}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->c()I

    .line 2476910
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->b()Z

    .line 2476911
    return-void
.end method


# virtual methods
.method public final S_()Z
    .locals 6

    .prologue
    .line 2476978
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HBf;

    invoke-virtual {v0}, LX/HBf;->d()V

    .line 2476979
    iget-boolean v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->u:Z

    if-eqz v0, :cond_0

    .line 2476980
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, LX/0ax;->aE:Ljava/lang/String;

    iget-wide v4, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->q:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2476981
    const-string v0, "page_view_referrer"

    sget-object v2, LX/89z;->DEEPLINK:LX/89z;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2476982
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2476983
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2476984
    const/4 v0, 0x1

    .line 2476985
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 13

    .prologue
    const/4 v4, 0x0

    .line 2476964
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2476965
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;

    const/16 v3, 0x259

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v5, 0xac0

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x2b53

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x2c03

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x455

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x12b1

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0xc49

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const-class v11, LX/HNF;

    invoke-interface {v0, v11}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/HNF;

    const-class v12, LX/HN8;

    invoke-interface {v0, v12}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/HN8;

    const-class p1, LX/HBi;

    invoke-interface {v0, p1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/HBi;

    invoke-static {v0}, LX/CXj;->a(LX/0QB;)LX/CXj;

    move-result-object v0

    check-cast v0, LX/CXj;

    iput-object v3, v2, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->a:LX/0Ot;

    iput-object v5, v2, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->b:LX/0Ot;

    iput-object v6, v2, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->c:LX/0Ot;

    iput-object v7, v2, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->d:LX/0Ot;

    iput-object v8, v2, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->e:LX/0Ot;

    iput-object v9, v2, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->f:LX/0Ot;

    iput-object v10, v2, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->g:LX/0Ot;

    iput-object v11, v2, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->h:LX/HNF;

    iput-object v12, v2, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->i:LX/HN8;

    iput-object p1, v2, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->j:LX/HBi;

    iput-object v0, v2, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->k:LX/CXj;

    .line 2476966
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v0

    .line 2476967
    const-string v0, "com.facebook.katana.profile.id"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->q:J

    .line 2476968
    const-string v0, "profile_name"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->r:Ljava/lang/String;

    .line 2476969
    const-string v0, "page_profile_pic_url_extra"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->s:Ljava/lang/String;

    .line 2476970
    const-string v0, "extra_page_presence_tab_type"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->t:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 2476971
    const-string v0, "extra_launched_from_deeplink"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->u:Z

    .line 2476972
    const-string v0, "extra_is_admin"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->v:Z

    .line 2476973
    const-string v0, "extra_page_tab_entry_point"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->w:Ljava/lang/String;

    .line 2476974
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HBf;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->p:Lcom/facebook/base/fragment/FbFragment;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->p:Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    .line 2476975
    :goto_0
    iget-object v2, v0, LX/HBf;->a:LX/0if;

    sget-object v3, LX/0ig;->ap:LX/0ih;

    const-string v4, "target_fragment_create"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "target_fragment:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 2476976
    return-void

    .line 2476977
    :cond_0
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->t:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->name()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public final a(Lcom/facebook/base/fragment/FbFragment;)V
    .locals 1

    .prologue
    .line 2476959
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2476960
    iput-object p1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->p:Lcom/facebook/base/fragment/FbFragment;

    .line 2476961
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->y:Lcom/facebook/resources/ui/FbFrameLayout;

    if-eqz v0, :cond_0

    .line 2476962
    invoke-direct {p0}, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->c()V

    .line 2476963
    :cond_0
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2476955
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2476956
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->p:Lcom/facebook/base/fragment/FbFragment;

    if-eqz v0, :cond_0

    .line 2476957
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->p:Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2476958
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x570de1dd

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2476954
    const v1, 0x7f030eea

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x36ad0d5b

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x10c2846c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2476945
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2476946
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->k:LX/CXj;

    if-eqz v1, :cond_2

    .line 2476947
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->D:LX/CXx;

    if-eqz v1, :cond_0

    .line 2476948
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->k:LX/CXj;

    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->D:LX/CXx;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2476949
    :cond_0
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->E:LX/CXx;

    if-eqz v1, :cond_1

    .line 2476950
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->k:LX/CXj;

    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->E:LX/CXx;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2476951
    :cond_1
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->F:LX/CXt;

    if-eqz v1, :cond_2

    .line 2476952
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->k:LX/CXj;

    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->F:LX/CXt;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2476953
    :cond_2
    const/16 v1, 0x2b

    const v2, -0x7337c0e5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2476912
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2476913
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->o:Landroid/os/ParcelUuid;

    if-nez v0, :cond_0

    .line 2476914
    new-instance v0, Landroid/os/ParcelUuid;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->o:Landroid/os/ParcelUuid;

    .line 2476915
    :cond_0
    const v0, 0x7f0d2459

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbFrameLayout;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->y:Lcom/facebook/resources/ui/FbFrameLayout;

    .line 2476916
    const v0, 0x7f0d0595

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbSwipeRefreshLayout;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->x:Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 2476917
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->x:Lcom/facebook/widget/FbSwipeRefreshLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FbSwipeRefreshLayout;->setEnabled(Z)V

    .line 2476918
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    sget v1, LX/8Dm;->h:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2476919
    new-instance v1, LX/0zw;

    const v0, 0x7f0d23e0

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->z:LX/0zw;

    .line 2476920
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->z:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const v1, 0x7f0d2e6b

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->A:Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;

    .line 2476921
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->A:Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->o:Landroid/os/ParcelUuid;

    invoke-virtual {v0, v1}, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->a(Landroid/os/ParcelUuid;)V

    .line 2476922
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->D:LX/CXx;

    if-nez v0, :cond_1

    .line 2476923
    new-instance v0, LX/HWc;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->o:Landroid/os/ParcelUuid;

    invoke-direct {v0, p0, v1}, LX/HWc;-><init>(Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;Landroid/os/ParcelUuid;)V

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->D:LX/CXx;

    .line 2476924
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->D:LX/CXx;

    move-object v0, v0

    .line 2476925
    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->D:LX/CXx;

    .line 2476926
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->k:LX/CXj;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->D:LX/CXx;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2476927
    :cond_2
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->t:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 2476928
    iget-boolean v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->u:Z

    if-eqz v1, :cond_6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_HOME:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-eq v0, v1, :cond_6

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->r:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 2476929
    if-eqz v0, :cond_3

    .line 2476930
    const v0, 0x7f0d2457

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2476931
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/deeplink/simplifiedheader/view/PagesSimplifiedHeader;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->B:Lcom/facebook/pages/common/deeplink/simplifiedheader/view/PagesSimplifiedHeader;

    .line 2476932
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->B:Lcom/facebook/pages/common/deeplink/simplifiedheader/view/PagesSimplifiedHeader;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->s:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->r:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/pages/common/deeplink/simplifiedheader/view/PagesSimplifiedHeader;->b(Landroid/net/Uri;Ljava/lang/String;)V

    .line 2476933
    :cond_3
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->E:LX/CXx;

    if-nez v0, :cond_4

    .line 2476934
    new-instance v0, LX/HWd;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->o:Landroid/os/ParcelUuid;

    invoke-direct {v0, p0, v1}, LX/HWd;-><init>(Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;Landroid/os/ParcelUuid;)V

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->E:LX/CXx;

    .line 2476935
    :cond_4
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->E:LX/CXx;

    move-object v0, v0

    .line 2476936
    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->E:LX/CXx;

    .line 2476937
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->k:LX/CXj;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->E:LX/CXx;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2476938
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->p:Lcom/facebook/base/fragment/FbFragment;

    if-eqz v0, :cond_5

    .line 2476939
    invoke-direct {p0}, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->c()V

    .line 2476940
    :cond_5
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HBf;

    .line 2476941
    iget-object v1, v0, LX/HBf;->a:LX/0if;

    sget-object v2, LX/0ig;->ap:LX/0ih;

    const-string v3, "target_fragment_view_created"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2476942
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->x:Lcom/facebook/widget/FbSwipeRefreshLayout;

    new-instance v1, LX/HWa;

    invoke-direct {v1, p0}, LX/HWa;-><init>(Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 2476943
    iget-wide v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->q:J

    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->t:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    const/4 v3, 0x1

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;->a(Lcom/facebook/pages/identity/fragments/surface/PagesStandaloneTabFragmentWrapper;JLcom/facebook/graphql/enums/GraphQLPageActionType;Z)V

    .line 2476944
    return-void

    :cond_6
    const/4 v1, 0x0

    goto :goto_0
.end method
