.class public Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0hF;
.implements LX/0fj;
.implements LX/HOo;
.implements LX/145;
.implements LX/HQv;
.implements LX/HQw;
.implements LX/HQx;
.implements LX/HQy;
.implements LX/0gr;
.implements LX/0o2;
.implements LX/0g9;
.implements LX/63S;


# static fields
.field private static Q:Z

.field private static final R:Lcom/facebook/interstitial/manager/InterstitialTrigger;


# instance fields
.field public A:LX/16I;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public B:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public D:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/HMH;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public E:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public F:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public G:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/HQg;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public H:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0iA;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public I:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2hf;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public J:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public K:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/HQ9;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public L:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/HPs;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public M:LX/0if;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public N:LX/HMa;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public O:Z

.field public P:Z

.field public S:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private T:Landroid/view/View;

.field private U:Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;

.field private V:Landroid/view/LayoutInflater;

.field private W:Lcom/facebook/widget/CustomFrameLayout;

.field private X:Landroid/support/v4/widget/SwipeRefreshLayout;

.field public Y:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

.field public Z:Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;

.field public a:LX/HX7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public aA:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

.field public aB:I

.field private aC:Z

.field public aD:Z

.field private aE:LX/0fx;

.field private aF:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field private aG:Z

.field public final aH:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/HDS;",
            ">;"
        }
    .end annotation
.end field

.field public aI:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aJ:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public aK:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public aL:LX/4At;

.field public aM:LX/4At;

.field public aN:Z

.field public final aO:LX/HWx;

.field public aa:LX/63T;

.field public ab:LX/HQB;

.field private ac:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;",
            ">;"
        }
    .end annotation
.end field

.field public ad:Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;

.field public final ae:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLPageActionType;",
            "Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;",
            ">;"
        }
    .end annotation
.end field

.field public af:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Landroid/view/ViewGroup;",
            ">;"
        }
    .end annotation
.end field

.field public ag:Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;

.field public ah:LX/CXx;

.field private ai:LX/CXt;

.field private aj:LX/HX5;

.field public ak:LX/HPn;

.field private al:LX/63Q;

.field public am:LX/1B1;

.field public an:LX/1B1;

.field private ao:LX/HBQ;

.field private ap:LX/8FZ;

.field public aq:LX/8EI;

.field private final ar:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field public as:LX/HX9;

.field private at:I

.field public au:LX/CZd;

.field private av:Z

.field private aw:Z

.field public ax:I

.field private ay:I

.field public az:I

.field public b:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/HPo;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0zG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/BQb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/8Do;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/HEW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/HEQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1B1;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/HDT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/CXj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/7jB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/01T;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/2U1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/8EJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/9XF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/8Fe;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/8E2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/9XE;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/8FU;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/CSN;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/HBf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/HXC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/HA5;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/HES;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2478230
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->PAGE_ADMIN_TIMELINE_VIEW:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->R:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 2477880
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2477881
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ae:Ljava/util/Map;

    .line 2477882
    new-instance v0, LX/HWp;

    invoke-direct {v0, p0}, LX/HWp;-><init>(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ar:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 2477883
    iput-boolean v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->av:Z

    .line 2477884
    iput-boolean v3, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aw:Z

    .line 2477885
    iput v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ax:I

    .line 2477886
    iput v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ay:I

    .line 2477887
    iput v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->az:I

    .line 2477888
    iput-boolean v3, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aC:Z

    .line 2477889
    iput-boolean v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aG:Z

    .line 2477890
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aH:Ljava/util/List;

    .line 2477891
    iput-object v2, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aK:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 2477892
    iput-object v2, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aL:LX/4At;

    .line 2477893
    iput-object v2, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aM:LX/4At;

    .line 2477894
    new-instance v0, LX/HWx;

    invoke-direct {v0, p0}, LX/HWx;-><init>(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aO:LX/HWx;

    .line 2477895
    new-instance v0, LX/HQB;

    invoke-direct {v0}, LX/HQB;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ab:LX/HQB;

    .line 2477896
    return-void
.end method

.method private C()Lcom/facebook/graphql/enums/GraphQLPageActionType;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2478227
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ab:LX/HQB;

    .line 2478228
    iget-object v1, v0, LX/HQB;->b:LX/0Px;

    move-object v0, v1

    .line 2478229
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ab:LX/HQB;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->Y:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    invoke-virtual {v0, v1}, LX/HQB;->a(I)LX/9Y7;

    move-result-object v0

    invoke-interface {v0}, LX/9Y7;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ab:LX/HQB;

    invoke-virtual {v0}, LX/HQB;->c()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v0

    goto :goto_0
.end method

.method private D()V
    .locals 3

    .prologue
    .line 2478222
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2478223
    iget-object v1, v0, LX/HX9;->b:LX/03R;

    move-object v0, v1

    .line 2478224
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2478225
    :goto_0
    return-void

    .line 2478226
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->t:LX/8FU;

    sget-object v1, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->R:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/8FU;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Landroid/content/Context;)Z

    goto :goto_0
.end method

.method public static F(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V
    .locals 2

    .prologue
    .line 2478217
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->af:LX/0zw;

    if-eqz v0, :cond_0

    .line 2478218
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->af:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2478219
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ag:Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;

    if-eqz v0, :cond_1

    .line 2478220
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ag:Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;

    invoke-virtual {v0}, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->b()V

    .line 2478221
    :cond_1
    return-void
.end method

.method public static a(LX/CZd;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2478211
    iget-object v0, p0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v0

    .line 2478212
    if-eqz v0, :cond_0

    .line 2478213
    iget-object v0, p0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v0

    .line 2478214
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->L()Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2478215
    iget-object v0, p0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v0

    .line 2478216
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->L()Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "unknown"

    goto :goto_0
.end method

.method private a(I)V
    .locals 5

    .prologue
    .line 2478192
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->Z:Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->setTranslationY(F)V

    .line 2478193
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->G:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HQg;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->Z:Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;

    .line 2478194
    iget-object v2, v1, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->n:Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;

    move-object v1, v2

    .line 2478195
    invoke-direct {p0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->x()I

    move-result v2

    .line 2478196
    neg-int v3, p1

    if-le v2, v3, :cond_3

    sget-object v3, LX/03R;->YES:LX/03R;

    .line 2478197
    :goto_0
    iget-object v4, v0, LX/HQg;->b:LX/03R;

    sget-object p0, LX/03R;->UNSET:LX/03R;

    if-eq v4, p0, :cond_0

    iget-object v4, v0, LX/HQg;->b:LX/03R;

    if-eq v4, v3, :cond_2

    .line 2478198
    :cond_0
    sget-object v4, LX/03R;->YES:LX/03R;

    if-ne v3, v4, :cond_4

    .line 2478199
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->b()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2478200
    invoke-virtual {v1}, LX/Ban;->getProfileVideoView()Lcom/facebook/caspian/ui/standardheader/ProfileVideoView;

    move-result-object v4

    .line 2478201
    if-eqz v4, :cond_1

    .line 2478202
    sget-object p0, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {v4, p0}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    .line 2478203
    :cond_1
    :goto_1
    iput-object v3, v0, LX/HQg;->b:LX/03R;

    .line 2478204
    :cond_2
    return-void

    .line 2478205
    :cond_3
    sget-object v3, LX/03R;->NO:LX/03R;

    goto :goto_0

    .line 2478206
    :cond_4
    if-eqz v1, :cond_5

    .line 2478207
    invoke-virtual {v1}, LX/Ban;->getProfileVideoView()Lcom/facebook/caspian/ui/standardheader/ProfileVideoView;

    move-result-object v4

    .line 2478208
    if-eqz v4, :cond_5

    .line 2478209
    sget-object p0, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {v4, p0}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 2478210
    :cond_5
    goto :goto_1
.end method

.method private a(IZ)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2478179
    iput p1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->az:I

    .line 2478180
    iget v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->az:I

    invoke-static {p0, v0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->e$redex0(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;I)V

    .line 2478181
    iget v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->az:I

    invoke-static {p0, v0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->h(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;I)V

    .line 2478182
    iget v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->az:I

    invoke-direct {p0, v0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->a(I)V

    .line 2478183
    iget v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->az:I

    invoke-direct {p0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->x()I

    move-result v3

    neg-int v3, v3

    if-le v0, v3, :cond_2

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aw:Z

    .line 2478184
    iget v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->az:I

    invoke-direct {p0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->x()I

    move-result v3

    neg-int v3, v3

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ay:I

    .line 2478185
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->X:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-eqz v0, :cond_0

    .line 2478186
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->X:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setEnabled(Z)V

    .line 2478187
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aE:LX/0fx;

    if-eqz v0, :cond_1

    if-eqz p2, :cond_1

    .line 2478188
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aE:LX/0fx;

    const/4 v3, 0x0

    invoke-interface {v0, v3, v2, v2, v2}, LX/0fx;->a(LX/0g8;III)V

    .line 2478189
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aj:LX/HX5;

    invoke-virtual {v0, v1}, LX/HX5;->a(Z)V

    .line 2478190
    return-void

    :cond_2
    move v0, v2

    .line 2478191
    goto :goto_0
.end method

.method private a(LX/HMI;Landroid/content/Intent;I)V
    .locals 10

    .prologue
    .line 2478083
    invoke-interface {p1}, LX/HMI;->a()LX/4At;

    move-result-object v0

    .line 2478084
    if-eqz v0, :cond_0

    .line 2478085
    invoke-virtual {v0}, LX/4At;->beginShowingProgress()V

    .line 2478086
    :cond_0
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2478087
    iget-wide v8, v1, LX/HX9;->a:J

    move-wide v2, v8

    .line 2478088
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    .line 2478089
    iget-object v4, v1, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v1, v4

    .line 2478090
    if-eqz v1, :cond_1

    new-instance v4, LX/8A4;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    .line 2478091
    iget-object v5, v1, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v1, v5

    .line 2478092
    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->P()LX/0Px;

    move-result-object v1

    invoke-direct {v4, v1}, LX/8A4;-><init>(Ljava/util/List;)V

    :goto_0
    move-object v1, p1

    move-object v5, p0

    move-object v6, p2

    move v7, p3

    invoke-interface/range {v1 .. v7}, LX/HMI;->a(JLX/8A4;Lcom/facebook/base/fragment/FbFragment;Landroid/content/Intent;I)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2478093
    if-nez v1, :cond_2

    .line 2478094
    :goto_1
    return-void

    .line 2478095
    :cond_1
    const/4 v4, 0x0

    goto :goto_0

    .line 2478096
    :cond_2
    new-instance v2, LX/HWr;

    invoke-direct {v2, p0, v0, p1}, LX/HWr;-><init>(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;LX/4At;LX/HMI;)V

    .line 2478097
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->C:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    const-string v3, "pages_activity_result_handler"

    new-instance v4, LX/HWs;

    invoke-direct {v4, p0, v1}, LX/HWs;-><init>(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;Lcom/google/common/util/concurrent/ListenableFuture;)V

    invoke-virtual {v0, v3, v4, v2}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_1
.end method

.method private a(Landroid/os/ParcelUuid;)V
    .locals 1

    .prologue
    .line 2478170
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2478171
    invoke-static {v0}, LX/HX8;->a(LX/HX9;)LX/HX8;

    move-result-object p0

    .line 2478172
    iput-object p1, p0, LX/HX8;->c:Landroid/os/ParcelUuid;

    .line 2478173
    invoke-virtual {p0}, LX/HX8;->a()LX/HX9;

    .line 2478174
    return-void
.end method

.method private a(Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;)V
    .locals 5

    .prologue
    .line 2478154
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2478155
    iget-wide v2, v0, LX/HX9;->a:J

    move-wide v0, v2

    .line 2478156
    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->u()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->P()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/8A4;->c(LX/0Px;)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;->NOT_VERIFIED:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->O()Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2478157
    :cond_0
    const/4 v2, 0x0

    .line 2478158
    :goto_0
    move v0, v2

    .line 2478159
    if-eqz v0, :cond_2

    .line 2478160
    :cond_1
    :goto_1
    return-void

    .line 2478161
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->P()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/8A4;->a(LX/0Px;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2478162
    const/4 v0, 0x0

    .line 2478163
    :goto_2
    move v0, v0

    .line 2478164
    if-eqz v0, :cond_1

    goto :goto_1

    .line 2478165
    :cond_3
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 2478166
    const-string v3, "current_page_id"

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2478167
    sget-object v3, LX/77s;->g:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-static {p0, v3, v2}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->a(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/util/Map;)Z

    move-result v2

    goto :goto_0

    .line 2478168
    :cond_4
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2478169
    sget-object v1, LX/77s;->f:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-static {p0, v1, v0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->a(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/util/Map;)Z

    move-result v0

    goto :goto_2
.end method

.method private static a(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;LX/HX7;LX/0hB;LX/HPo;LX/0zG;LX/BQb;LX/8Do;LX/HEW;LX/HEQ;LX/0Or;LX/HDT;LX/CXj;LX/7jB;LX/01T;LX/2U1;LX/8EJ;LX/9XF;LX/8Fe;LX/8E2;LX/9XE;LX/8FU;LX/CSN;LX/HBf;LX/HXC;LX/HA5;LX/HES;LX/0ad;LX/16I;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0if;LX/HMa;LX/0Or;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;",
            "LX/HX7;",
            "LX/0hB;",
            "LX/HPo;",
            "LX/0zG;",
            "LX/BQb;",
            "LX/8Do;",
            "LX/HEW;",
            "LX/HEQ;",
            "LX/0Or",
            "<",
            "LX/1B1;",
            ">;",
            "LX/HDT;",
            "LX/CXj;",
            "LX/7jB;",
            "LX/01T;",
            "LX/2U1;",
            "LX/8EJ;",
            "LX/9XF;",
            "LX/8Fe;",
            "LX/8E2;",
            "LX/9XE;",
            "LX/8FU;",
            "LX/CSN;",
            "LX/HBf;",
            "LX/HXC;",
            "LX/HA5;",
            "LX/HES;",
            "LX/0ad;",
            "LX/16I;",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/HMH;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/HQg;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0iA;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2hf;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/HQ9;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/HPs;",
            ">;",
            "Lcom/facebook/funnellogger/FunnelLogger;",
            "LX/HMa;",
            "LX/0Or",
            "<",
            "Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2478153
    iput-object p1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->a:LX/HX7;

    iput-object p2, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->b:LX/0hB;

    iput-object p3, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->c:LX/HPo;

    iput-object p4, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->d:LX/0zG;

    iput-object p5, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->e:LX/BQb;

    iput-object p6, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->f:LX/8Do;

    iput-object p7, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->g:LX/HEW;

    iput-object p8, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->h:LX/HEQ;

    iput-object p9, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->i:LX/0Or;

    iput-object p10, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->j:LX/HDT;

    iput-object p11, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->k:LX/CXj;

    iput-object p12, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->l:LX/7jB;

    iput-object p13, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->m:LX/01T;

    iput-object p14, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->n:LX/2U1;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->o:LX/8EJ;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->p:LX/9XF;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->q:LX/8Fe;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->r:LX/8E2;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->s:LX/9XE;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->t:LX/8FU;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->u:LX/CSN;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->v:LX/HBf;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->w:LX/HXC;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->x:LX/HA5;

    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->y:LX/HES;

    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->z:LX/0ad;

    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->A:LX/16I;

    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->B:LX/0Ot;

    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->C:LX/0Ot;

    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->D:LX/0Ot;

    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->E:LX/0Ot;

    move-object/from16 v0, p32

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->F:LX/0Ot;

    move-object/from16 v0, p33

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->G:LX/0Ot;

    move-object/from16 v0, p34

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->H:LX/0Ot;

    move-object/from16 v0, p35

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->I:LX/0Ot;

    move-object/from16 v0, p36

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->J:LX/0Ot;

    move-object/from16 v0, p37

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->K:LX/0Ot;

    move-object/from16 v0, p38

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->L:LX/0Ot;

    move-object/from16 v0, p39

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->M:LX/0if;

    move-object/from16 v0, p40

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->N:LX/HMa;

    move-object/from16 v0, p41

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->S:LX/0Or;

    return-void
.end method

.method private static a(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2478149
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2478150
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->h:LX/HEQ;

    .line 2478151
    iget-object v1, v0, LX/HEQ;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x130080

    iget-object p0, v0, LX/HEQ;->j:Ljava/lang/String;

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result p0

    const-string p1, "ProfilePicFromLowRes"

    invoke-interface {v1, v2, p0, p1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 2478152
    :cond_0
    return-void
.end method

.method public static a(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;Z)V
    .locals 5

    .prologue
    .line 2478135
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->C:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 2478136
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->am:LX/1B1;

    if-eqz v0, :cond_0

    .line 2478137
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->am:LX/1B1;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->j:LX/HDT;

    invoke-virtual {v0, v1}, LX/1B1;->b(LX/0b4;)V

    .line 2478138
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->h:LX/HEQ;

    const v4, 0x130080

    .line 2478139
    if-eqz p1, :cond_2

    .line 2478140
    iget-object v1, v0, LX/HEQ;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v2, v0, LX/HEQ;->j:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    const/4 v3, 0x3

    invoke-interface {v1, v4, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 2478141
    :goto_0
    iget-object v1, v0, LX/HEQ;->d:LX/8E2;

    iget-object v2, v0, LX/HEQ;->h:Ljava/lang/String;

    sget-object v3, LX/8E7;->PAGES_HEADER_PERF_LOGGING_STOPPED:LX/8E7;

    invoke-static {v3}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/8E2;->a(Ljava/lang/String;LX/0Rf;)V

    .line 2478142
    if-eqz p1, :cond_1

    .line 2478143
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aq:LX/8EI;

    invoke-virtual {v0}, LX/8EI;->f()V

    .line 2478144
    :goto_1
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->G:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HQg;

    .line 2478145
    sget-object v1, LX/03R;->UNSET:LX/03R;

    iput-object v1, v0, LX/HQg;->b:LX/03R;

    .line 2478146
    return-void

    .line 2478147
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aq:LX/8EI;

    invoke-virtual {v0}, LX/8EI;->e()V

    goto :goto_1

    .line 2478148
    :cond_2
    iget-object v1, v0, LX/HEQ;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v2, v0, LX/HEQ;->j:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-interface {v1, v4, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->markerCancel(II)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 45

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v43

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    invoke-static/range {v43 .. v43}, LX/HX7;->a(LX/0QB;)LX/HX7;

    move-result-object v3

    check-cast v3, LX/HX7;

    invoke-static/range {v43 .. v43}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v4

    check-cast v4, LX/0hB;

    const-class v5, LX/HPo;

    move-object/from16 v0, v43

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/HPo;

    invoke-static/range {v43 .. v43}, LX/0zF;->a(LX/0QB;)LX/0zF;

    move-result-object v6

    check-cast v6, LX/0zG;

    invoke-static/range {v43 .. v43}, LX/BQb;->a(LX/0QB;)LX/BQb;

    move-result-object v7

    check-cast v7, LX/BQb;

    invoke-static/range {v43 .. v43}, LX/8Do;->a(LX/0QB;)LX/8Do;

    move-result-object v8

    check-cast v8, LX/8Do;

    invoke-static/range {v43 .. v43}, LX/HEW;->a(LX/0QB;)LX/HEW;

    move-result-object v9

    check-cast v9, LX/HEW;

    invoke-static/range {v43 .. v43}, LX/HEQ;->a(LX/0QB;)LX/HEQ;

    move-result-object v10

    check-cast v10, LX/HEQ;

    const/16 v11, 0x45a

    move-object/from16 v0, v43

    invoke-static {v0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    invoke-static/range {v43 .. v43}, LX/HDT;->a(LX/0QB;)LX/HDT;

    move-result-object v12

    check-cast v12, LX/HDT;

    invoke-static/range {v43 .. v43}, LX/CXj;->a(LX/0QB;)LX/CXj;

    move-result-object v13

    check-cast v13, LX/CXj;

    invoke-static/range {v43 .. v43}, LX/7jB;->a(LX/0QB;)LX/7jB;

    move-result-object v14

    check-cast v14, LX/7jB;

    invoke-static/range {v43 .. v43}, LX/15N;->a(LX/0QB;)LX/01T;

    move-result-object v15

    check-cast v15, LX/01T;

    invoke-static/range {v43 .. v43}, LX/2U1;->a(LX/0QB;)LX/2U1;

    move-result-object v16

    check-cast v16, LX/2U1;

    const-class v17, LX/8EJ;

    move-object/from16 v0, v43

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v17

    check-cast v17, LX/8EJ;

    invoke-static/range {v43 .. v43}, LX/9XF;->a(LX/0QB;)LX/9XF;

    move-result-object v18

    check-cast v18, LX/9XF;

    invoke-static/range {v43 .. v43}, LX/8Fe;->a(LX/0QB;)LX/8Fe;

    move-result-object v19

    check-cast v19, LX/8Fe;

    invoke-static/range {v43 .. v43}, LX/8E2;->a(LX/0QB;)LX/8E2;

    move-result-object v20

    check-cast v20, LX/8E2;

    invoke-static/range {v43 .. v43}, LX/9XE;->a(LX/0QB;)LX/9XE;

    move-result-object v21

    check-cast v21, LX/9XE;

    invoke-static/range {v43 .. v43}, LX/8FU;->a(LX/0QB;)LX/8FU;

    move-result-object v22

    check-cast v22, LX/8FU;

    invoke-static/range {v43 .. v43}, LX/CSN;->a(LX/0QB;)LX/CSN;

    move-result-object v23

    check-cast v23, LX/CSN;

    invoke-static/range {v43 .. v43}, LX/HBf;->a(LX/0QB;)LX/HBf;

    move-result-object v24

    check-cast v24, LX/HBf;

    invoke-static/range {v43 .. v43}, LX/HXC;->a(LX/0QB;)LX/HXC;

    move-result-object v25

    check-cast v25, LX/HXC;

    const-class v26, LX/HA5;

    move-object/from16 v0, v43

    move-object/from16 v1, v26

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v26

    check-cast v26, LX/HA5;

    invoke-static/range {v43 .. v43}, LX/HES;->a(LX/0QB;)LX/HES;

    move-result-object v27

    check-cast v27, LX/HES;

    invoke-static/range {v43 .. v43}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v28

    check-cast v28, LX/0ad;

    invoke-static/range {v43 .. v43}, LX/16I;->a(LX/0QB;)LX/16I;

    move-result-object v29

    check-cast v29, LX/16I;

    const/16 v30, 0x12c4

    move-object/from16 v0, v43

    move/from16 v1, v30

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v30

    const/16 v31, 0x12b1

    move-object/from16 v0, v43

    move/from16 v1, v31

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v31

    const/16 v32, 0x2be4

    move-object/from16 v0, v43

    move/from16 v1, v32

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v32

    const/16 v33, 0x259

    move-object/from16 v0, v43

    move/from16 v1, v33

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v33

    const/16 v34, 0xac0

    move-object/from16 v0, v43

    move/from16 v1, v34

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v34

    const/16 v35, 0x2c07

    move-object/from16 v0, v43

    move/from16 v1, v35

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v35

    const/16 v36, 0xbd2

    move-object/from16 v0, v43

    move/from16 v1, v36

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v36

    const/16 v37, 0x1052

    move-object/from16 v0, v43

    move/from16 v1, v37

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v37

    const/16 v38, 0x2eb

    move-object/from16 v0, v43

    move/from16 v1, v38

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v38

    const/16 v39, 0x2c04

    move-object/from16 v0, v43

    move/from16 v1, v39

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v39

    const/16 v40, 0x2c03

    move-object/from16 v0, v43

    move/from16 v1, v40

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v40

    invoke-static/range {v43 .. v43}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v41

    check-cast v41, LX/0if;

    invoke-static/range {v43 .. v43}, LX/HMa;->a(LX/0QB;)LX/HMa;

    move-result-object v42

    check-cast v42, LX/HMa;

    const/16 v44, 0x2c61

    invoke-static/range {v43 .. v44}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v43

    invoke-static/range {v2 .. v43}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->a(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;LX/HX7;LX/0hB;LX/HPo;LX/0zG;LX/BQb;LX/8Do;LX/HEW;LX/HEQ;LX/0Or;LX/HDT;LX/CXj;LX/7jB;LX/01T;LX/2U1;LX/8EJ;LX/9XF;LX/8Fe;LX/8E2;LX/9XE;LX/8FU;LX/CSN;LX/HBf;LX/HXC;LX/HA5;LX/HES;LX/0ad;LX/16I;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0if;LX/HMa;LX/0Or;)V

    return-void
.end method

.method private static a(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;Lcom/facebook/graphql/executor/GraphQLResult;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2478125
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2478126
    if-nez v0, :cond_0

    move v0, v1

    .line 2478127
    :goto_0
    return v0

    .line 2478128
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2478129
    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->J()LX/0Px;

    move-result-object v2

    .line 2478130
    if-eqz v2, :cond_1

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel$RedirectionInfoModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel$RedirectionInfoModel;->a()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel$RedirectionInfoModel$NodeModel;

    move-result-object v0

    if-nez v0, :cond_2

    :cond_1
    move v0, v1

    .line 2478131
    goto :goto_0

    .line 2478132
    :cond_2
    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel$RedirectionInfoModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel$RedirectionInfoModel;->a()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel$RedirectionInfoModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel$RedirectionInfoModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2478133
    iget-wide v6, v0, LX/HX9;->a:J

    move-wide v4, v6

    .line 2478134
    cmp-long v0, v2, v4

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public static a(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/util/Map;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2478098
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTriggerContext;

    invoke-direct {v0, p2}, Lcom/facebook/interstitial/manager/InterstitialTriggerContext;-><init>(Ljava/util/Map;)V

    .line 2478099
    new-instance v2, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-direct {v2, p1, v0}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger;Lcom/facebook/interstitial/manager/InterstitialTriggerContext;)V

    .line 2478100
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->H:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0iA;

    const-class v3, LX/77s;

    invoke-virtual {v0, v2, v3}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/13D;

    .line 2478101
    if-nez v0, :cond_0

    move v0, v1

    .line 2478102
    :goto_0
    return v0

    .line 2478103
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/13D;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    .line 2478104
    if-nez v2, :cond_1

    move v0, v1

    .line 2478105
    goto :goto_0

    .line 2478106
    :cond_1
    iget-object v3, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->T:Landroid/view/View;

    if-eqz v3, :cond_2

    const-string v3, "1820"

    invoke-virtual {v0}, LX/13D;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    move v0, v1

    .line 2478107
    goto :goto_0

    .line 2478108
    :cond_3
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->U:Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->U:Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;

    invoke-virtual {v0, v2}, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->a(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2478109
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->U:Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2478110
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->U:Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;

    invoke-virtual {v0}, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;->r()V

    :cond_4
    move v0, v1

    .line 2478111
    goto :goto_0

    .line 2478112
    :cond_5
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->I:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2hf;

    invoke-virtual {v0, v2}, LX/2hf;->a(Landroid/content/Intent;)Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;

    move-result-object v0

    .line 2478113
    instance-of v2, v0, Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;

    if-eqz v2, :cond_7

    .line 2478114
    check-cast v0, Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->U:Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;

    .line 2478115
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->U:Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;

    .line 2478116
    iget-object v2, v0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 2478117
    const-string v3, "ACTION_BUTTON_THEME_ARG"

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->f:LX/8Do;

    .line 2478118
    iget-object v4, v0, LX/8Do;->a:LX/0ad;

    sget-object v5, LX/0c0;->Live:LX/0c0;

    sget-short p1, LX/8Dn;->d:S

    const/4 p2, 0x0

    invoke-interface {v4, v5, p1, p2}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v4

    move v0, v4

    .line 2478119
    if-eqz v0, :cond_6

    sget-object v0, LX/77w;->PROMO:LX/77w;

    :goto_1
    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2478120
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v2, 0x7f0d23e1

    iget-object v3, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->U:Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;

    invoke-virtual {v0, v2, v3}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->c()I

    .line 2478121
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->T:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2478122
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 2478123
    :cond_6
    sget-object v0, LX/77w;->SPECIAL:LX/77w;

    goto :goto_1

    :cond_7
    move v0, v1

    .line 2478124
    goto/16 :goto_0
.end method

.method public static a$redex0(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;LX/HDS;)V
    .locals 2

    .prologue
    .line 2478283
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2478284
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment$26;

    invoke-direct {v1, p0, p1}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment$26;-><init>(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;LX/HDS;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2478285
    :cond_0
    return-void
.end method

.method public static a$redex0(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;LX/HX6;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2478414
    sget-object v0, LX/HWw;->a:[I

    invoke-virtual {p1}, LX/HX6;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2478415
    :goto_0
    return-void

    .line 2478416
    :pswitch_0
    const/4 v0, 0x0

    .line 2478417
    iget v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ax:I

    invoke-static {p0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->y(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)I

    move-result v2

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2478418
    iget-object p1, v2, LX/HX9;->b:LX/03R;

    move-object v2, p1

    .line 2478419
    invoke-virtual {v2, v0}, LX/03R;->asBoolean(Z)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_1
    sub-int v0, v1, v0

    mul-int/lit8 v0, v0, -0x1

    move v0, v0

    .line 2478420
    invoke-direct {p0, v0, v3}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->a(IZ)V

    goto :goto_0

    .line 2478421
    :pswitch_1
    invoke-direct {p0, v2, v3}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->a(IZ)V

    goto :goto_0

    .line 2478422
    :pswitch_2
    iget v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->az:I

    invoke-direct {p0, v0, v2}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->a(IZ)V

    goto :goto_0

    :cond_0
    iget v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aB:I

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a$redex0(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;Lcom/facebook/graphql/enums/GraphQLPageActionType;Z)V
    .locals 12

    .prologue
    .line 2478403
    if-nez p1, :cond_0

    .line 2478404
    invoke-static {p0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->F(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V

    .line 2478405
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->Z:Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->e()V

    .line 2478406
    :goto_0
    return-void

    .line 2478407
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_HOME:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-ne p1, v0, :cond_1

    .line 2478408
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->Z:Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->e()V

    .line 2478409
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    .line 2478410
    iget-wide v5, v0, LX/CZd;->a:J

    move-wide v2, v5

    .line 2478411
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->C:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "tab_data_fetch_"

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "_"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->L:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HPs;

    .line 2478412
    const/4 v11, 0x0

    move-object v6, v1

    move-wide v7, v2

    move-object v9, p1

    move v10, p2

    invoke-virtual/range {v6 .. v11}, LX/HPs;->a(JLcom/facebook/graphql/enums/GraphQLPageActionType;ZZ)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    move-object v1, v5

    .line 2478413
    new-instance v2, LX/HWn;

    invoke-direct {v2, p0, p1}, LX/HWn;-><init>(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;Lcom/facebook/graphql/enums/GraphQLPageActionType;)V

    invoke-virtual {v0, v4, v1, v2}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0
.end method

.method private b(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 2478373
    invoke-static {p0, p1}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->a(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;Lcom/facebook/graphql/executor/GraphQLResult;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2478374
    if-eqz p1, :cond_0

    .line 2478375
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2478376
    if-eqz v0, :cond_0

    .line 2478377
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2478378
    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->J()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2478379
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2478380
    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->J()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2478381
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2478382
    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->J()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel$RedirectionInfoModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel$RedirectionInfoModel;->a()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel$RedirectionInfoModel$NodeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2478383
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2478384
    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->J()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel$RedirectionInfoModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel$RedirectionInfoModel;->a()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel$RedirectionInfoModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel$RedirectionInfoModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2478385
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->E:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "No global redirection_info"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2478386
    :goto_0
    return-void

    .line 2478387
    :cond_1
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->s:LX/9XE;

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2478388
    iget-wide v7, v0, LX/HX9;->a:J

    move-wide v2, v7

    .line 2478389
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2478390
    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->J()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel$RedirectionInfoModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel$RedirectionInfoModel;->a()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel$RedirectionInfoModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel$RedirectionInfoModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 2478391
    iget-object v0, v1, LX/9XE;->a:LX/0Zb;

    sget-object v7, LX/9XG;->EVENT_GLOBAL_PAGE_REDIRECTION:LX/9XG;

    invoke-static {v7, v4, v5}, LX/9XE;->c(LX/9X2;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v8, "original_page_id"

    invoke-virtual {v7, v8, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    invoke-interface {v0, v7}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2478392
    new-instance v1, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    invoke-direct {v1}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;-><init>()V

    .line 2478393
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v0

    .line 2478394
    const-string v0, "page_fragment_uuid"

    new-instance v3, Landroid/os/ParcelUuid;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2478395
    const-string v3, "com.facebook.katana.profile.id"

    .line 2478396
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2478397
    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->J()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel$RedirectionInfoModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel$RedirectionInfoModel;->a()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel$RedirectionInfoModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageGeneralDataModel$RedirectionInfoModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2478398
    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2478399
    invoke-static {p0, v6}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->a(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;Z)V

    .line 2478400
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v2, 0x7f0d002f

    invoke-virtual {v0, v2, v1}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->c()I

    .line 2478401
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->M:LX/0if;

    sget-object v1, LX/0ig;->an:LX/0ih;

    const-string v2, "global_redirect"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2478402
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aN:Z

    goto/16 :goto_0
.end method

.method private c(Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;)V
    .locals 5

    .prologue
    .line 2478366
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->u:LX/CSN;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "is_admin:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2478367
    iget-object p0, v4, LX/HX9;->b:LX/03R;

    move-object v4, p0

    .line 2478368
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "is_fan:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->r()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "category:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->L()Lcom/facebook/graphql/enums/GraphQLPageSuperCategoryType;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 2478369
    array-length v3, v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, v1, v2

    .line 2478370
    invoke-static {v0, v4}, LX/CSN;->d(LX/CSN;Ljava/lang/String;)V

    .line 2478371
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2478372
    :cond_0
    return-void
.end method

.method public static d$redex0(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;I)V
    .locals 1

    .prologue
    .line 2478362
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aj:LX/HX5;

    invoke-virtual {v0, p1}, LX/HX5;->e(I)Lcom/facebook/base/fragment/FbFragment;

    move-result-object v0

    check-cast v0, LX/GZf;

    .line 2478363
    if-eqz v0, :cond_0

    .line 2478364
    invoke-interface {v0}, LX/GZf;->g()V

    .line 2478365
    :cond_0
    return-void
.end method

.method public static e$redex0(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;I)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2478352
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    .line 2478353
    iget-boolean v2, v1, LX/CZd;->l:Z

    move v1, v2

    .line 2478354
    if-eqz v1, :cond_0

    .line 2478355
    :goto_0
    return-void

    .line 2478356
    :cond_0
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2478357
    iget-object v2, v1, LX/HX9;->b:LX/03R;

    move-object v1, v2

    .line 2478358
    invoke-virtual {v1, v0}, LX/03R;->asBoolean(Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2478359
    :goto_1
    iget v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ax:I

    invoke-static {p0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->y(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)I

    move-result v2

    sub-int/2addr v1, v2

    add-int/2addr v1, p1

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 2478360
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aA:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setTranslationY(F)V

    goto :goto_0

    .line 2478361
    :cond_1
    iget v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aB:I

    goto :goto_1
.end method

.method public static e$redex0(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;Lcom/facebook/graphql/enums/GraphQLPageActionType;)V
    .locals 1

    .prologue
    .line 2478349
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ab:LX/HQB;

    invoke-virtual {v0, p1}, LX/HQB;->a(Lcom/facebook/graphql/enums/GraphQLPageActionType;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2478350
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ab:LX/HQB;

    invoke-virtual {v0, p1}, LX/HQB;->b(Lcom/facebook/graphql/enums/GraphQLPageActionType;)I

    move-result v0

    invoke-static {p0, v0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->d$redex0(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;I)V

    .line 2478351
    :cond_0
    return-void
.end method

.method public static f(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;I)V
    .locals 3

    .prologue
    .line 2478347
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->W:Lcom/facebook/widget/CustomFrameLayout;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, LX/3oW;->a(Landroid/view/View;Ljava/lang/CharSequence;I)LX/3oW;

    move-result-object v0

    invoke-virtual {v0}, LX/3oW;->b()V

    .line 2478348
    return-void
.end method

.method public static f(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;Lcom/facebook/graphql/enums/GraphQLPageActionType;)Z
    .locals 3

    .prologue
    .line 2478274
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ab:LX/HQB;

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    .line 2478275
    iget-object p0, v0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, p0

    .line 2478276
    :goto_0
    if-eqz v1, :cond_1

    invoke-virtual {v1, p1}, LX/HQB;->a(Lcom/facebook/graphql/enums/GraphQLPageActionType;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    .line 2478277
    if-nez v0, :cond_2

    .line 2478278
    :goto_1
    move v2, v2

    .line 2478279
    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :goto_2
    move v0, v2

    .line 2478280
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_2

    .line 2478281
    :cond_2
    sget-object p0, LX/HQ8;->a:[I

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->ordinal()I

    move-result v1

    aget p0, p0, v1

    packed-switch p0, :pswitch_data_0

    goto :goto_1

    .line 2478282
    :pswitch_0
    const/4 v2, 0x1

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public static g(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;I)Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2478346
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ae:Ljava/util/Map;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ab:LX/HQB;

    invoke-virtual {v1, p1}, LX/HQB;->a(I)LX/9Y7;

    move-result-object v1

    invoke-interface {v1}, LX/9Y7;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceTabFragmentWrapper;

    return-object v0
.end method

.method public static h(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;I)V
    .locals 3

    .prologue
    .line 2478337
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    .line 2478338
    iget-boolean v1, v0, LX/CZd;->l:Z

    move v0, v1

    .line 2478339
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->X:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-nez v0, :cond_1

    .line 2478340
    :cond_0
    :goto_0
    return-void

    .line 2478341
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ag:Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->X:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->af:LX/0zw;

    if-eqz v0, :cond_0

    .line 2478342
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->F:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    sget v1, LX/8Dm;->h:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2478343
    iget v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ax:I

    add-int/2addr v0, p1

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ag:Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;

    invoke-virtual {v1}, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->getMeasuredHeight()I

    move-result v1

    if-le v0, v1, :cond_2

    .line 2478344
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->af:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->X:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->getMeasuredHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setTranslationY(F)V

    goto :goto_0

    .line 2478345
    :cond_2
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->af:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->X:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->getMeasuredHeight()I

    move-result v2

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->af:LX/0zw;

    invoke-virtual {v1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v1

    sub-int v1, v2, v1

    iget v2, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ax:I

    add-int/2addr v2, p1

    add-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setTranslationY(F)V

    goto :goto_0
.end method

.method public static h(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;Lcom/facebook/graphql/enums/GraphQLPageActionType;)V
    .locals 1

    .prologue
    .line 2478331
    if-eqz p1, :cond_0

    invoke-virtual {p0, p1}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->a(Lcom/facebook/graphql/enums/GraphQLPageActionType;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2478332
    invoke-virtual {p0, p1}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->b(Lcom/facebook/graphql/enums/GraphQLPageActionType;)V

    .line 2478333
    :goto_0
    return-void

    .line 2478334
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0, p1}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->c(Lcom/facebook/graphql/enums/GraphQLPageActionType;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2478335
    invoke-virtual {p0, p1}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->d(Lcom/facebook/graphql/enums/GraphQLPageActionType;)V

    goto :goto_0

    .line 2478336
    :cond_1
    const v0, 0x7f0817cc

    invoke-static {p0, v0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->f(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;I)V

    goto :goto_0
.end method

.method private k(Lcom/facebook/graphql/enums/GraphQLPageActionType;)V
    .locals 8

    .prologue
    .line 2478316
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    .line 2478317
    iget-wide v6, v0, LX/CZd;->a:J

    move-wide v2, v6

    .line 2478318
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    .line 2478319
    iget-object v1, v0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v1

    .line 2478320
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    .line 2478321
    iget-object v1, v0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v1

    .line 2478322
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->x()Ljava/lang/String;

    move-result-object v0

    .line 2478323
    :goto_0
    sget-object v1, LX/HWw;->b:[I

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->ordinal()I

    move-result v4

    aget v1, v1, v4

    packed-switch v1, :pswitch_data_0

    .line 2478324
    :goto_1
    return-void

    .line 2478325
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2478326
    :pswitch_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2478327
    const-string v4, "com.facebook.katana.profile.id"

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2478328
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 2478329
    const-string v4, "profile_name"

    invoke-virtual {v1, v4, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2478330
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->J:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17W;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    sget-object v5, LX/0ax;->aF:Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v5, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v4, v2, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private o()V
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 2478310
    const-class v0, LX/63U;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/63U;

    .line 2478311
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->al:LX/63Q;

    if-nez v0, :cond_0

    if-eqz v4, :cond_0

    .line 2478312
    new-instance v0, LX/63Q;

    invoke-direct {v0}, LX/63Q;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->al:LX/63Q;

    .line 2478313
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->al:LX/63Q;

    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aa:LX/63T;

    iget-boolean v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->P:Z

    if-nez v1, :cond_1

    move v6, v5

    :goto_0
    move-object v1, p0

    move-object v3, p0

    invoke-virtual/range {v0 .. v6}, LX/63Q;->a(LX/63S;LX/63T;LX/0g9;LX/63U;ZZ)V

    .line 2478314
    :cond_0
    return-void

    .line 2478315
    :cond_1
    const/4 v6, 0x0

    goto :goto_0
.end method

.method private q()V
    .locals 6

    .prologue
    .line 2478298
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->g:LX/HEW;

    invoke-virtual {v0}, LX/HEW;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2478299
    :goto_0
    return-void

    .line 2478300
    :cond_0
    new-instance v0, LX/8E3;

    invoke-direct {v0}, LX/8E3;-><init>()V

    new-instance v1, LX/HWo;

    invoke-direct {v1, p0}, LX/HWo;-><init>(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/8E3;->a(Ljava/util/concurrent/Callable;Z)LX/8E3;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "pma_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    .line 2478301
    iget-wide v4, v2, LX/CZd;->a:J

    move-wide v2, v4

    .line 2478302
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2478303
    iput-object v1, v0, LX/8E3;->b:Ljava/lang/String;

    .line 2478304
    move-object v0, v0

    .line 2478305
    sget-object v1, LX/8E5;->INTERACTIVE:LX/8E5;

    .line 2478306
    iput-object v1, v0, LX/8E3;->a:LX/8E5;

    .line 2478307
    move-object v0, v0

    .line 2478308
    sget-object v1, LX/8E7;->PAGE_ADMIN_MEGAHPHONE_DATA_FETCHED:LX/8E7;

    invoke-static {v1}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8E3;->a(LX/0Rf;)LX/8E3;

    move-result-object v0

    invoke-virtual {v0}, LX/8E3;->a()LX/8E6;

    move-result-object v0

    .line 2478309
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->r:LX/8E2;

    invoke-virtual {v1, v0}, LX/8E2;->a(LX/8E6;)V

    goto :goto_0
.end method

.method private r()V
    .locals 6

    .prologue
    .line 2478286
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->g:LX/HEW;

    invoke-virtual {v0}, LX/HEW;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2478287
    :goto_0
    return-void

    .line 2478288
    :cond_0
    new-instance v0, LX/8E3;

    invoke-direct {v0}, LX/8E3;-><init>()V

    new-instance v1, LX/HWq;

    invoke-direct {v1, p0}, LX/HWq;-><init>(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/8E3;->a(Ljava/util/concurrent/Callable;Z)LX/8E3;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "pma_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    .line 2478289
    iget-wide v4, v2, LX/CZd;->a:J

    move-wide v2, v4

    .line 2478290
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2478291
    iput-object v1, v0, LX/8E3;->b:Ljava/lang/String;

    .line 2478292
    move-object v0, v0

    .line 2478293
    sget-object v1, LX/8E5;->INTERACTIVE:LX/8E5;

    .line 2478294
    iput-object v1, v0, LX/8E3;->a:LX/8E5;

    .line 2478295
    move-object v0, v0

    .line 2478296
    sget-object v1, LX/8E7;->PAGE_HEADER_DATA_FETCHED:LX/8E7;

    invoke-static {v1}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8E3;->a(LX/0Rf;)LX/8E3;

    move-result-object v0

    invoke-virtual {v0}, LX/8E3;->a()LX/8E6;

    move-result-object v0

    .line 2478297
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->r:LX/8E2;

    invoke-virtual {v1, v0}, LX/8E2;->a(LX/8E6;)V

    goto :goto_0
.end method

.method private s()V
    .locals 6

    .prologue
    .line 2478175
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->r:LX/8E2;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "pma_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2478176
    iget-wide v4, v2, LX/HX9;->a:J

    move-wide v2, v4

    .line 2478177
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/8E2;->b(Ljava/lang/String;LX/0Rf;)V

    .line 2478178
    return-void
.end method

.method private t()V
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2478231
    new-instance v4, LX/9Yx;

    invoke-direct {v4}, LX/9Yx;-><init>()V

    .line 2478232
    const/4 v1, 0x0

    .line 2478233
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v5, v0

    .line 2478234
    const-string v0, "model_bundle_page_id"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2478235
    const-string v0, "model_bundle_page_name"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2478236
    const-string v0, "model_bundle_page_profile_pic_uri"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2478237
    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_7

    iget-object v8, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2478238
    iget-wide v10, v8, LX/HX9;->a:J

    move-wide v8, v10

    .line 2478239
    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 2478240
    invoke-static {v7}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 2478241
    iput-object v7, v4, LX/9Yx;->y:Ljava/lang/String;

    .line 2478242
    :cond_0
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_7

    .line 2478243
    :goto_0
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->n:LX/2U1;

    iget-object v6, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2478244
    iget-wide v10, v6, LX/HX9;->a:J

    move-wide v6, v10

    .line 2478245
    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, LX/2U1;->c(Ljava/lang/String;)LX/8Dk;

    move-result-object v6

    .line 2478246
    if-eqz v6, :cond_2

    .line 2478247
    iget-object v1, v6, LX/8Dk;->a:Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;

    move-object v7, v1

    .line 2478248
    if-eqz v7, :cond_2

    .line 2478249
    invoke-virtual {v7}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->e()Ljava/lang/String;

    move-result-object v1

    .line 2478250
    iput-object v1, v4, LX/9Yx;->y:Ljava/lang/String;

    .line 2478251
    if-nez v0, :cond_5

    invoke-virtual {v7}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->c()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_4

    move v1, v2

    :goto_1
    if-eqz v1, :cond_1

    .line 2478252
    iget-object v0, v6, LX/8Dk;->a:Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;

    move-object v0, v0

    .line 2478253
    invoke-virtual {v0}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->c()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 2478254
    :cond_1
    invoke-virtual {v7}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->eD_()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 2478255
    invoke-virtual {v7}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->eD_()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    .line 2478256
    iput-object v1, v4, LX/9Yx;->Y:LX/0Px;

    .line 2478257
    :goto_2
    invoke-virtual {v7}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->b()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_2

    .line 2478258
    invoke-virtual {v7}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->b()LX/1vs;

    move-result-object v1

    iget-object v6, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2478259
    invoke-virtual {v6, v1, v3}, LX/15i;->h(II)Z

    move-result v1

    new-instance v6, LX/186;

    const/16 v7, 0x400

    invoke-direct {v6, v7}, LX/186;-><init>(I)V

    const/4 v7, 0x3

    invoke-virtual {v6, v7}, LX/186;->c(I)V

    invoke-virtual {v6, v3, v1}, LX/186;->a(IZ)V

    invoke-virtual {v6, v2, v3}, LX/186;->a(IZ)V

    const/4 v1, 0x2

    invoke-virtual {v6, v1, v3}, LX/186;->a(IZ)V

    const v1, 0x8c3f43

    invoke-static {v6, v1}, LX/1vs;->a(LX/186;I)LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2478260
    invoke-virtual {v4, v2, v1}, LX/9Yx;->a(LX/15i;I)LX/9Yx;

    .line 2478261
    :cond_2
    invoke-static {p0, v0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->a(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;Ljava/lang/String;)V

    .line 2478262
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    invoke-virtual {v4}, LX/9Yx;->a()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-result-object v2

    .line 2478263
    iget-wide v10, v1, LX/CZd;->a:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-static {v10}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2478264
    invoke-static {v1, v2}, LX/CZd;->a(LX/CZd;Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;)V

    .line 2478265
    iput-object v0, v1, LX/CZd;->k:Ljava/lang/String;

    .line 2478266
    sget-object v10, LX/CZc;->PRELIMINARY_DATA:LX/CZc;

    iput-object v10, v1, LX/CZd;->d:LX/CZc;

    .line 2478267
    const-string v0, "extra_deeplink_to_tab_type"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2478268
    const-string v0, "extra_deeplink_to_tab_type"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aK:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 2478269
    :cond_3
    return-void

    :cond_4
    move v1, v3

    .line 2478270
    goto/16 :goto_1

    :cond_5
    move v1, v3

    goto/16 :goto_1

    .line 2478271
    :cond_6
    sget-object v1, LX/8A3;->BASIC_ADMIN:LX/8A3;

    invoke-virtual {v1}, LX/8A3;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 2478272
    iput-object v1, v4, LX/9Yx;->Y:LX/0Px;

    .line 2478273
    goto :goto_2

    :cond_7
    move-object v0, v1

    goto/16 :goto_0
.end method

.method public static v(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V
    .locals 2

    .prologue
    .line 2477796
    invoke-direct {p0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->s()V

    .line 2477797
    invoke-direct {p0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->r()V

    .line 2477798
    invoke-direct {p0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->q()V

    .line 2477799
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ak:LX/HPn;

    sget-object v1, LX/HPm;->FORCED_BY_USER:LX/HPm;

    invoke-virtual {v0, v1}, LX/HPn;->a(LX/HPm;)V

    .line 2477800
    return-void
.end method

.method private x()I
    .locals 2

    .prologue
    .line 2477795
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->b:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->c()I

    move-result v0

    iget v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->at:I

    invoke-static {v0, v1}, LX/Baq;->b(II)I

    move-result v0

    return v0
.end method

.method public static y(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)I
    .locals 2

    .prologue
    .line 2477789
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    .line 2477790
    iget-boolean v1, v0, LX/CZd;->l:Z

    move v0, v1

    .line 2477791
    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 2477792
    :goto_0
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->Z:Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;

    invoke-virtual {v1}, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->getAdminBarMeasuredHeight()I

    move-result v1

    .line 2477793
    add-int/2addr v0, v1

    return v0

    .line 2477794
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aA:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    invoke-virtual {v0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->getMeasuredHeight()I

    move-result v0

    goto :goto_0
.end method

.method private z()V
    .locals 12

    .prologue
    .line 2477745
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aa:LX/63T;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->al:LX/63Q;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->Z:Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;

    .line 2477746
    iget-object v1, v0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->n:Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;

    move-object v0, v1

    .line 2477747
    invoke-virtual {v0}, LX/Ban;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2477748
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->al:LX/63Q;

    .line 2477749
    iget-object v1, v0, LX/63Q;->a:LX/63S;

    invoke-interface {v1}, LX/63S;->mK_()LX/63R;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 2477750
    iget-object v1, v0, LX/63Q;->a:LX/63S;

    invoke-interface {v1}, LX/63S;->mK_()LX/63R;

    move-result-object v1

    invoke-interface {v1}, LX/63R;->a()Z

    move-result v1

    iput-boolean v1, v0, LX/63Q;->d:Z

    .line 2477751
    :goto_0
    iget-object v1, v0, LX/63Q;->c:LX/63U;

    iget-boolean v2, v0, LX/63Q;->d:Z

    invoke-interface {v1, v2}, LX/63U;->j_(Z)V

    .line 2477752
    iget-object v1, v0, LX/63Q;->c:LX/63U;

    iget-boolean v2, v0, LX/63Q;->e:Z

    invoke-interface {v1, v2}, LX/63U;->b(Z)V

    .line 2477753
    iget-object v1, v0, LX/63Q;->b:LX/63T;

    iget-boolean v2, v0, LX/63Q;->d:Z

    invoke-interface {v1, v2}, LX/63T;->setFadingModeEnabled(Z)V

    .line 2477754
    iget-object v2, v0, LX/63Q;->b:LX/63T;

    iget-boolean v1, v0, LX/63Q;->d:Z

    if-eqz v1, :cond_8

    const/4 v1, 0x0

    :goto_1
    invoke-interface {v2, v1}, LX/63T;->a(F)V

    .line 2477755
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    .line 2477756
    iget-object v1, v0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v1

    .line 2477757
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    .line 2477758
    iget-object v1, v0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v1

    .line 2477759
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->x()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->P:Z

    if-eqz v0, :cond_2

    .line 2477760
    :cond_1
    :goto_2
    return-void

    .line 2477761
    :cond_2
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2477762
    if-eqz v0, :cond_1

    .line 2477763
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    .line 2477764
    iget-object v2, v1, LX/CZd;->h:LX/0Px;

    move-object v1, v2

    .line 2477765
    if-eqz v1, :cond_3

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2477766
    :cond_3
    invoke-interface {v0}, LX/1ZF;->lH_()V

    .line 2477767
    :goto_3
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2477768
    iget-boolean v2, v1, LX/HX9;->e:Z

    move v1, v2

    .line 2477769
    if-nez v1, :cond_1

    .line 2477770
    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/1ZF;->setCustomTitle(Landroid/view/View;)V

    .line 2477771
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    .line 2477772
    iget-object v2, v1, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v1, v2

    .line 2477773
    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->x()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1ZF;->a_(Ljava/lang/String;)V

    goto :goto_2

    .line 2477774
    :cond_4
    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->x:LX/HA5;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->W:Lcom/facebook/widget/CustomFrameLayout;

    .line 2477775
    new-instance v5, LX/HA4;

    invoke-static {v2}, LX/HDT;->a(LX/0QB;)LX/HDT;

    move-result-object v6

    check-cast v6, LX/HDT;

    invoke-static {v2}, LX/HAF;->a(LX/0QB;)LX/HAF;

    move-result-object v7

    check-cast v7, LX/HAF;

    const-class v8, LX/H9E;

    invoke-interface {v2, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/H9E;

    move-object v9, v0

    move-object v10, v3

    move-object v11, v4

    invoke-direct/range {v5 .. v11}, LX/HA4;-><init>(LX/HDT;LX/HAF;LX/H9E;LX/1ZF;Landroid/content/Context;Landroid/view/View;)V

    .line 2477776
    move-object v2, v5

    .line 2477777
    iget-object v3, v2, LX/HA4;->f:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/H8Z;

    .line 2477778
    iget-object v5, v2, LX/HA4;->b:LX/HAF;

    invoke-virtual {v5, v3}, LX/HAF;->b(LX/H8Y;)V

    goto :goto_4

    .line 2477779
    :cond_5
    iget-object v3, v2, LX/HA4;->f:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->clear()V

    .line 2477780
    const/4 v3, 0x0

    move v4, v3

    :goto_5
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v3

    if-ge v4, v3, :cond_6

    .line 2477781
    iget-object v5, v2, LX/HA4;->c:LX/H9D;

    invoke-virtual {v1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    invoke-virtual {v5, v3}, LX/H9D;->a(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;)LX/H8Y;

    move-result-object v3

    check-cast v3, LX/H8Z;

    .line 2477782
    iget-object v5, v2, LX/HA4;->f:Ljava/util/LinkedHashMap;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2477783
    iget-object v5, v2, LX/HA4;->b:LX/HAF;

    invoke-virtual {v5, v3}, LX/HAF;->a(LX/H8Y;)V

    .line 2477784
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_5

    .line 2477785
    :cond_6
    invoke-virtual {v2}, LX/HA4;->a()V

    .line 2477786
    goto/16 :goto_3

    .line 2477787
    :cond_7
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/63Q;->d:Z

    goto/16 :goto_0

    .line 2477788
    :cond_8
    const/high16 v1, 0x3f800000    # 1.0f

    goto/16 :goto_1
.end method


# virtual methods
.method public final S_()Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2477729
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aj:LX/HX5;

    iget-object v3, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->Y:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v3

    invoke-virtual {v0, v3}, LX/HX5;->e(I)Lcom/facebook/base/fragment/FbFragment;

    move-result-object v0

    .line 2477730
    instance-of v3, v0, LX/0fj;

    if-eqz v3, :cond_0

    check-cast v0, LX/0fj;

    invoke-interface {v0}, LX/0fj;->S_()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 2477731
    :goto_0
    return v0

    .line 2477732
    :cond_0
    invoke-direct {p0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->C()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v0

    iget-object v3, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ab:LX/HQB;

    invoke-virtual {v3}, LX/HQB;->c()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v3

    if-eq v0, v3, :cond_2

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->F:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    sget v3, LX/8Dm;->e:I

    invoke-virtual {v0, v3, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2477733
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ab:LX/HQB;

    invoke-virtual {v0}, LX/HQB;->c()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2477734
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->u:LX/CSN;

    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ab:LX/HQB;

    invoke-virtual {v2}, LX/HQB;->c()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->name()Ljava/lang/String;

    move-result-object v2

    .line 2477735
    sget-object v3, LX/CSM;->BACK:LX/CSM;

    iput-object v3, v0, LX/CSN;->a:LX/CSM;

    .line 2477736
    iget-object v3, v0, LX/CSN;->b:LX/0if;

    sget-object v4, LX/0ig;->ag:LX/0ih;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "backTo_"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2477737
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ab:LX/HQB;

    invoke-virtual {v0}, LX/HQB;->c()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->d(Lcom/facebook/graphql/enums/GraphQLPageActionType;)V

    move v0, v1

    .line 2477738
    goto :goto_0

    .line 2477739
    :cond_2
    invoke-direct {p0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->C()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2477740
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->u:LX/CSN;

    invoke-direct {p0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->C()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->name()Ljava/lang/String;

    move-result-object v1

    .line 2477741
    sget-object v3, LX/CSM;->BACK:LX/CSM;

    iput-object v3, v0, LX/CSN;->a:LX/CSM;

    .line 2477742
    iget-object v3, v0, LX/CSN;->b:LX/0if;

    sget-object v4, LX/0ig;->ag:LX/0ih;

    const-string v5, "back"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string p0, "last_tab:"

    invoke-direct {v6, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 2477743
    :cond_3
    move v0, v2

    .line 2477744
    goto/16 :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2477720
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aj:LX/HX5;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->Y:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v2

    .line 2477721
    :goto_0
    return-object v0

    .line 2477722
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aj:LX/HX5;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->Y:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    invoke-virtual {v0, v1}, LX/HX5;->e(I)Lcom/facebook/base/fragment/FbFragment;

    move-result-object v1

    .line 2477723
    if-nez v1, :cond_2

    move-object v0, v2

    .line 2477724
    goto :goto_0

    .line 2477725
    :cond_2
    instance-of v0, v1, LX/0fh;

    if-nez v0, :cond_3

    .line 2477726
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->E:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v3, "page_fragment_analytics_name_return_null"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Page Fragment "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "is not an instance of AnalyticsFragment"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v2

    .line 2477727
    goto :goto_0

    :cond_3
    move-object v0, v1

    .line 2477728
    check-cast v0, LX/0fh;

    invoke-interface {v0}, LX/0f2;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 14

    .prologue
    .line 2477604
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2477605
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2477606
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2477607
    new-instance v6, LX/HX8;

    invoke-direct {v6}, LX/HX8;-><init>()V

    .line 2477608
    const-string v5, "com.facebook.katana.profile.id"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v7

    .line 2477609
    iput-wide v7, v6, LX/HX8;->a:J

    .line 2477610
    if-eqz p1, :cond_5

    const-string v5, "extra_is_admin"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 2477611
    const-string v5, "extra_is_admin"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, LX/03R;->fromDbValue(I)LX/03R;

    move-result-object v5

    .line 2477612
    iput-object v5, v6, LX/HX8;->b:LX/03R;

    .line 2477613
    :goto_0
    const-string v5, "page_fragment_uuid"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Landroid/os/ParcelUuid;

    .line 2477614
    iput-object v5, v6, LX/HX8;->c:Landroid/os/ParcelUuid;

    .line 2477615
    const-string v5, "extra_user_location"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Landroid/location/Location;

    .line 2477616
    iput-object v5, v6, LX/HX8;->d:Landroid/location/Location;

    .line 2477617
    const-string v5, "extra_in_admin_container_frag"

    const/4 v7, 0x0

    invoke-virtual {v0, v5, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    .line 2477618
    iput-boolean v5, v6, LX/HX8;->e:Z

    .line 2477619
    const-string v5, "page_view_referrer"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 2477620
    const-string v5, "page_view_referrer"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v5

    check-cast v5, LX/89z;

    .line 2477621
    iput-object v5, v6, LX/HX8;->f:LX/89z;

    .line 2477622
    :goto_1
    invoke-virtual {v6}, LX/HX8;->a()LX/HX9;

    move-result-object v5

    move-object v0, v5

    .line 2477623
    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2477624
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->o:LX/8EJ;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2477625
    iget-wide v5, v1, LX/HX9;->a:J

    move-wide v2, v5

    .line 2477626
    new-instance v6, LX/8EI;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v7

    check-cast v7, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {v0}, LX/0id;->a(LX/0QB;)LX/0id;

    move-result-object v8

    check-cast v8, LX/0id;

    invoke-static {v0}, LX/8E2;->a(LX/0QB;)LX/8E2;

    move-result-object v9

    check-cast v9, LX/8E2;

    invoke-static {v0}, LX/8Do;->a(LX/0QB;)LX/8Do;

    move-result-object v10

    check-cast v10, LX/8Do;

    move-wide v11, v2

    invoke-direct/range {v6 .. v12}, LX/8EI;-><init>(Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0id;LX/8E2;LX/8Do;J)V

    .line 2477627
    move-object v0, v6

    .line 2477628
    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aq:LX/8EI;

    .line 2477629
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->s:LX/9XE;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2477630
    iget-wide v5, v1, LX/HX9;->a:J

    move-wide v2, v5

    .line 2477631
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2477632
    iget-object v4, v1, LX/HX9;->f:LX/89z;

    move-object v1, v4

    .line 2477633
    sget-object v4, LX/9XB;->EVENT_PAGE_VISIT:LX/9XB;

    invoke-static {v4, v2, v3}, LX/9XE;->c(LX/9X2;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    .line 2477634
    if-eqz v1, :cond_7

    .line 2477635
    const-string v5, "ref"

    iget-object v6, v1, LX/89z;->loggingName:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2477636
    :goto_2
    iget-object v5, v0, LX/9XE;->a:LX/0Zb;

    invoke-interface {v5, v4}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2477637
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->u:LX/CSN;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2477638
    iget-wide v5, v1, LX/HX9;->a:J

    move-wide v2, v5

    .line 2477639
    iget-object v1, v0, LX/CSN;->b:LX/0if;

    sget-object v4, LX/0ig;->ag:LX/0ih;

    invoke-virtual {v1, v4}, LX/0if;->a(LX/0ih;)V

    .line 2477640
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "page_id:"

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/CSN;->d(LX/CSN;Ljava/lang/String;)V

    .line 2477641
    new-instance v0, LX/HEP;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->f:LX/8Do;

    .line 2477642
    invoke-virtual {v1}, LX/8Do;->a()Z

    move-result v2

    move v1, v2

    .line 2477643
    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->f:LX/8Do;

    invoke-virtual {v2}, LX/8Do;->a()Z

    move-result v2

    iget-object v3, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->f:LX/8Do;

    invoke-virtual {v3}, LX/8Do;->a()Z

    move-result v3

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, LX/HEP;-><init>(ZZZZ)V

    .line 2477644
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->h:LX/HEQ;

    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2477645
    iget-wide v5, v2, LX/HX9;->a:J

    move-wide v2, v5

    .line 2477646
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2477647
    iget-object v4, v3, LX/HX9;->c:Landroid/os/ParcelUuid;

    move-object v3, v4

    .line 2477648
    const/4 v6, 0x1

    .line 2477649
    iput-object v2, v1, LX/HEQ;->h:Ljava/lang/String;

    .line 2477650
    if-eqz v3, :cond_0

    .line 2477651
    invoke-virtual {v3}, Landroid/os/ParcelUuid;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, LX/HEQ;->j:Ljava/lang/String;

    .line 2477652
    :cond_0
    sget-object v4, LX/CXp;->HEADER_DISPATCH_DRAW_HAS_DATA:LX/CXp;

    invoke-virtual {v4}, LX/CXp;->ordinal()I

    move-result v4

    shl-int v4, v6, v4

    sget-object v5, LX/CXp;->COVER_PHOTO_COMPLETE:LX/CXp;

    invoke-virtual {v5}, LX/CXp;->ordinal()I

    move-result v5

    shl-int v5, v6, v5

    or-int/2addr v4, v5

    sget-object v5, LX/CXp;->PROFILE_PHOTO_COMPLETE:LX/CXp;

    invoke-virtual {v5}, LX/CXp;->ordinal()I

    move-result v5

    shl-int v5, v6, v5

    or-int/2addr v4, v5

    sget-object v5, LX/CXp;->ACTION_BAR_DISPATCH_DRAW_WITH_DATA:LX/CXp;

    invoke-virtual {v5}, LX/CXp;->ordinal()I

    move-result v5

    shl-int v5, v6, v5

    or-int/2addr v4, v5

    sget-object v5, LX/CXp;->CALL_TO_ACTION:LX/CXp;

    invoke-virtual {v5}, LX/CXp;->ordinal()I

    move-result v5

    shl-int v5, v6, v5

    or-int/2addr v4, v5

    sget-object v5, LX/CXp;->METABOX:LX/CXp;

    invoke-virtual {v5}, LX/CXp;->ordinal()I

    move-result v5

    shl-int v5, v6, v5

    or-int/2addr v4, v5

    iput v4, v1, LX/HEQ;->e:I

    .line 2477653
    new-instance v4, LX/HEN;

    invoke-direct {v4, v1, v3}, LX/HEN;-><init>(LX/HEQ;Landroid/os/ParcelUuid;)V

    iput-object v4, v1, LX/HEQ;->g:LX/CXr;

    .line 2477654
    iget-object v4, v1, LX/HEQ;->a:LX/CXj;

    iget-object v5, v1, LX/HEQ;->g:LX/CXr;

    invoke-virtual {v4, v5}, LX/0b4;->a(LX/0b2;)Z

    .line 2477655
    iget-object v4, v1, LX/HEQ;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v5, 0x130080

    iget-object v6, v1, LX/HEQ;->j:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->hashCode()I

    move-result v6

    invoke-interface {v4, v5, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 2477656
    iget-boolean v4, v0, LX/HEP;->a:Z

    if-eqz v4, :cond_2

    .line 2477657
    iget-boolean v4, v0, LX/HEP;->b:Z

    if-eqz v4, :cond_1

    .line 2477658
    const-string v4, "SplittingHeader"

    invoke-virtual {v1, v4}, LX/HEQ;->a(Ljava/lang/String;)V

    .line 2477659
    :cond_1
    iget-boolean v4, v0, LX/HEP;->c:Z

    if-eqz v4, :cond_2

    .line 2477660
    const-string v4, "NarrowCta"

    invoke-virtual {v1, v4}, LX/HEQ;->a(Ljava/lang/String;)V

    .line 2477661
    :cond_2
    new-instance v0, LX/CZd;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2477662
    iget-wide v5, v1, LX/HX9;->a:J

    move-wide v2, v5

    .line 2477663
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2477664
    iget-object v4, v1, LX/HX9;->d:Landroid/location/Location;

    move-object v1, v4

    .line 2477665
    invoke-direct {v0, v2, v3, v1}, LX/CZd;-><init>(JLandroid/location/Location;)V

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    .line 2477666
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->c:LX/HPo;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2477667
    iget-wide v5, v1, LX/HX9;->a:J

    move-wide v2, v5

    .line 2477668
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2477669
    iget-object v3, v2, LX/HX9;->f:LX/89z;

    move-object v2, v3

    .line 2477670
    iget-object v3, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2477671
    iget-object v4, v3, LX/HX9;->c:Landroid/os/ParcelUuid;

    move-object v3, v4

    .line 2477672
    invoke-virtual {v3}, Landroid/os/ParcelUuid;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, p0, v1, v2, v3}, LX/HPo;->a(LX/145;Ljava/lang/Long;LX/89z;Ljava/lang/String;)LX/HPn;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ak:LX/HPn;

    .line 2477673
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ak:LX/HPn;

    sget-object v1, LX/HPm;->DEFAULT:LX/HPm;

    invoke-virtual {v0, v1}, LX/HPn;->a(LX/HPm;)V

    .line 2477674
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ak:LX/HPn;

    .line 2477675
    iget-object v1, v0, LX/HPn;->r:Ljava/lang/String;

    move-object v0, v1

    .line 2477676
    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aI:Ljava/lang/String;

    .line 2477677
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ak:LX/HPn;

    .line 2477678
    iget-object v1, v0, LX/HPn;->q:Ljava/lang/String;

    move-object v0, v1

    .line 2477679
    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aJ:Ljava/lang/String;

    .line 2477680
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->e:LX/BQb;

    new-instance v1, LX/HWy;

    invoke-direct {v1, p0}, LX/HWy;-><init>(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V

    .line 2477681
    iput-object v1, v0, LX/BQb;->d:LX/HWy;

    .line 2477682
    iget-object v2, v0, LX/BQb;->a:LX/0b3;

    iget-object v3, v0, LX/BQb;->b:LX/BQa;

    invoke-virtual {v2, v3}, LX/0b4;->a(LX/0b2;)Z

    .line 2477683
    iget-object v2, v0, LX/BQb;->a:LX/0b3;

    iget-object v3, v0, LX/BQb;->c:LX/BQZ;

    invoke-virtual {v2, v3}, LX/0b4;->a(LX/0b2;)Z

    .line 2477684
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->i:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1B1;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->am:LX/1B1;

    .line 2477685
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->i:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1B1;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->an:LX/1B1;

    .line 2477686
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->am:LX/1B1;

    new-instance v1, LX/HX0;

    invoke-direct {v1, p0}, LX/HX0;-><init>(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2477687
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->am:LX/1B1;

    new-instance v1, LX/HX1;

    invoke-direct {v1, p0}, LX/HX1;-><init>(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2477688
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->am:LX/1B1;

    new-instance v1, LX/HX2;

    invoke-direct {v1, p0}, LX/HX2;-><init>(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2477689
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->am:LX/1B1;

    new-instance v1, LX/HX3;

    invoke-direct {v1, p0}, LX/HX3;-><init>(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2477690
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->am:LX/1B1;

    new-instance v1, LX/HX4;

    invoke-direct {v1, p0}, LX/HX4;-><init>(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2477691
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->an:LX/1B1;

    new-instance v1, LX/HWf;

    invoke-direct {v1, p0}, LX/HWf;-><init>(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2477692
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->an:LX/1B1;

    new-instance v1, LX/HWg;

    invoke-direct {v1, p0}, LX/HWg;-><init>(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2477693
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->an:LX/1B1;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->l:LX/7jB;

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b4;)V

    .line 2477694
    const/4 v0, 0x0

    .line 2477695
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    .line 2477696
    if-eqz v1, :cond_8

    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    move-result v2

    if-nez v2, :cond_8

    .line 2477697
    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 2477698
    :goto_3
    move-object v1, v1

    .line 2477699
    if-eqz v1, :cond_3

    const-string v2, "parent_control_title_bar"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    :cond_3
    move v0, v0

    .line 2477700
    iput-boolean v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->P:Z

    .line 2477701
    new-instance v0, LX/HWz;

    invoke-direct {v0, p0}, LX/HWz;-><init>(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ao:LX/HBQ;

    .line 2477702
    invoke-direct {p0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->t()V

    .line 2477703
    sget-boolean v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->Q:Z

    .line 2477704
    if-eqz v0, :cond_4

    .line 2477705
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->h:LX/HEQ;

    const-string v2, "FirstVisit"

    invoke-virtual {v1, v2}, LX/HEQ;->a(Ljava/lang/String;)V

    .line 2477706
    :cond_4
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aq:LX/8EI;

    const-string v2, "FirstVisit"

    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/8EI;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2477707
    const/4 v0, 0x1

    sput-boolean v0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->Q:Z

    .line 2477708
    new-instance v0, LX/8FZ;

    invoke-direct {v0}, LX/8FZ;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ap:LX/8FZ;

    .line 2477709
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ap:LX/8FZ;

    .line 2477710
    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(LX/1Lb;)V

    .line 2477711
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->q:LX/8Fe;

    invoke-virtual {v0}, LX/8Fe;->a()V

    .line 2477712
    return-void

    .line 2477713
    :cond_5
    sget-object v5, LX/03R;->UNSET:LX/03R;

    .line 2477714
    iput-object v5, v6, LX/HX8;->b:LX/03R;

    .line 2477715
    goto/16 :goto_0

    .line 2477716
    :cond_6
    sget-object v5, LX/89z;->UNKNOWN:LX/89z;

    .line 2477717
    iput-object v5, v6, LX/HX8;->f:LX/89z;

    .line 2477718
    goto/16 :goto_1

    .line 2477719
    :cond_7
    const-string v5, "ref"

    sget-object v6, LX/89z;->UNKNOWN:LX/89z;

    iget-object v6, v6, LX/89z;->loggingName:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto/16 :goto_2

    :cond_8
    const/4 v1, 0x0

    goto :goto_3
.end method

.method public final a(Landroid/view/ViewGroup;I)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2477357
    if-eqz p2, :cond_0

    .line 2477358
    iget v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ax:I

    neg-int v0, v0

    invoke-direct {p0, v0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->a(I)V

    .line 2477359
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->X:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, v3}, Landroid/support/v4/widget/SwipeRefreshLayout;->setEnabled(Z)V

    .line 2477360
    iput-boolean v3, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aw:Z

    .line 2477361
    iget v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ax:I

    neg-int v0, v0

    iput v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ay:I

    .line 2477362
    iget v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ay:I

    invoke-static {p0, v0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->e$redex0(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;I)V

    .line 2477363
    iget v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ay:I

    invoke-static {p0, v0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->h(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;I)V

    .line 2477364
    :goto_0
    return-void

    .line 2477365
    :cond_0
    invoke-virtual {p1, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    .line 2477366
    instance-of v1, p1, Landroid/widget/ScrollView;

    if-eqz v1, :cond_1

    .line 2477367
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getScrollY()I

    move-result v0

    mul-int/lit8 v0, v0, -0x1

    .line 2477368
    :cond_1
    invoke-static {p0, v0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->e$redex0(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;I)V

    .line 2477369
    invoke-static {p0, v0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->h(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;I)V

    .line 2477370
    iget v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ax:I

    neg-int v1, v1

    .line 2477371
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 2477372
    invoke-direct {p0, v4}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->a(I)V

    .line 2477373
    invoke-direct {p0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->x()I

    move-result v1

    neg-int v1, v1

    if-le v4, v1, :cond_3

    move v1, v2

    :goto_1
    iput-boolean v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aw:Z

    .line 2477374
    invoke-direct {p0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->x()I

    move-result v1

    neg-int v1, v1

    invoke-static {v4, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ay:I

    .line 2477375
    if-nez v0, :cond_4

    .line 2477376
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->X:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setEnabled(Z)V

    .line 2477377
    :goto_2
    iget v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ax:I

    if-nez v0, :cond_5

    .line 2477378
    iput v4, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->az:I

    .line 2477379
    :goto_3
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aE:LX/0fx;

    if-eqz v0, :cond_2

    .line 2477380
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aE:LX/0fx;

    const/4 v1, 0x0

    invoke-interface {v0, v1, v3, v3, v3}, LX/0fx;->a(LX/0g8;III)V

    .line 2477381
    :cond_2
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aj:LX/HX5;

    invoke-virtual {v0, v3}, LX/HX5;->a(Z)V

    goto :goto_0

    :cond_3
    move v1, v3

    .line 2477382
    goto :goto_1

    .line 2477383
    :cond_4
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->X:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, v3}, Landroid/support/v4/widget/SwipeRefreshLayout;->setEnabled(Z)V

    goto :goto_2

    .line 2477384
    :cond_5
    iget v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ax:I

    invoke-static {p0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->y(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)I

    move-result v1

    sub-int v1, v0, v1

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2477385
    iget-object v2, v0, LX/HX9;->b:LX/03R;

    move-object v0, v2

    .line 2477386
    invoke-virtual {v0, v3}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v3

    :goto_4
    sub-int v0, v1, v0

    mul-int/lit8 v0, v0, -0x1

    invoke-static {v4, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->az:I

    goto :goto_3

    :cond_6
    iget v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aB:I

    goto :goto_4
.end method

.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;LX/HPm;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;",
            ">;",
            "LX/HPm;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 2477436
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2477437
    move-object v7, v0

    check-cast v7, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    .line 2477438
    iget-object v0, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v8, v0

    .line 2477439
    if-nez v7, :cond_2

    .line 2477440
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->E:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v2, "page_identity_data_graphql_returned_null"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v1, "Page header model is null: is from network == "

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2477441
    iget-object v1, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v1, v1

    .line 2477442
    sget-object v4, LX/0ta;->FROM_SERVER:LX/0ta;

    invoke-virtual {v1, v4}, LX/0ta;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "true"

    :goto_0
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2477443
    invoke-static {p0, v11}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->a(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;Z)V

    .line 2477444
    :cond_0
    :goto_1
    return-void

    .line 2477445
    :cond_1
    const-string v1, "false"

    goto :goto_0

    .line 2477446
    :cond_2
    invoke-static {p0, p1}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->a(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;Lcom/facebook/graphql/executor/GraphQLResult;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2477447
    invoke-direct {p0, p1}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->b(Lcom/facebook/graphql/executor/GraphQLResult;)V

    goto :goto_1

    .line 2477448
    :cond_3
    invoke-virtual {v7}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->G()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1a

    invoke-virtual {v7}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->G()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1a

    .line 2477449
    invoke-virtual {v7}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->G()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    move-object v6, v0

    .line 2477450
    :goto_2
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2477451
    invoke-static {v0}, LX/HX8;->a(LX/HX9;)LX/HX8;

    move-result-object v1

    .line 2477452
    new-instance v2, LX/8A4;

    invoke-virtual {v7}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->P()LX/0Px;

    move-result-object v3

    invoke-direct {v2, v3}, LX/8A4;-><init>(Ljava/util/List;)V

    sget-object v3, LX/8A3;->BASIC_ADMIN:LX/8A3;

    invoke-virtual {v2, v3}, LX/8A4;->a(LX/8A3;)Z

    move-result v2

    if-eqz v2, :cond_1b

    .line 2477453
    sget-object v2, LX/03R;->YES:LX/03R;

    .line 2477454
    iput-object v2, v1, LX/HX8;->b:LX/03R;

    .line 2477455
    :goto_3
    invoke-virtual {v1}, LX/HX8;->a()LX/HX9;

    move-result-object v1

    move-object v0, v1

    .line 2477456
    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2477457
    invoke-static {}, LX/009;->getInstance()LX/009;

    move-result-object v0

    const-string v1, "Is Page admin"

    new-instance v2, LX/HAX;

    iget-object v3, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2477458
    iget-object v5, v3, LX/HX9;->b:LX/03R;

    move-object v3, v5

    .line 2477459
    invoke-direct {v2, v3}, LX/HAX;-><init>(LX/03R;)V

    invoke-virtual {v0, v1, v2}, LX/009;->putLazyCustomData(Ljava/lang/String;LX/00T;)V

    .line 2477460
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->r:LX/8E2;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "pma_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    invoke-virtual {v2}, LX/CZd;->e()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/8E7;->PAGE_HEADER_DATA_FETCHED:LX/8E7;

    invoke-static {v2}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/8E2;->a(Ljava/lang/String;LX/0Rf;)V

    .line 2477461
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2477462
    iget-object v1, v0, LX/HX9;->b:LX/03R;

    move-object v0, v1

    .line 2477463
    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_4

    invoke-virtual {v7}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->P()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/8A4;->c(LX/0Px;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2477464
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->q:LX/8Fe;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    invoke-virtual {v1}, LX/HX9;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8Fe;->a(Ljava/lang/String;)V

    .line 2477465
    :cond_4
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2477466
    iget-object v1, v0, LX/HX9;->b:LX/03R;

    move-object v0, v1

    .line 2477467
    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_5

    .line 2477468
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->M:LX/0if;

    sget-object v1, LX/0ig;->an:LX/0ih;

    const-string v2, "is_admin:true"

    invoke-virtual {v0, v1, v2}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 2477469
    :cond_5
    sget-object v0, LX/0ta;->FROM_SERVER:LX/0ta;

    if-ne v8, v0, :cond_6

    .line 2477470
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2477471
    iget-object v1, v0, LX/HX9;->b:LX/03R;

    move-object v0, v1

    .line 2477472
    sget-object v1, LX/03R;->NO:LX/03R;

    if-ne v0, v1, :cond_7

    .line 2477473
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->n:LX/2U1;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    invoke-virtual {v1}, LX/HX9;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2U1;->b(Ljava/lang/String;)V

    .line 2477474
    :cond_6
    :goto_4
    const/4 v1, 0x0

    .line 2477475
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->m:LX/01T;

    sget-object v2, LX/01T;->PAA:LX/01T;

    invoke-virtual {v0, v2}, LX/01T;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    move v0, v1

    .line 2477476
    :goto_5
    move v0, v0

    .line 2477477
    if-eqz v0, :cond_a

    .line 2477478
    const/4 v5, 0x1

    .line 2477479
    new-instance v0, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;

    invoke-direct {v0}, Lcom/facebook/pages/identity/fragments/identity/PageIdentityFragment;-><init>()V

    .line 2477480
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2477481
    const-string v2, "extra_has_been_redirected"

    invoke-virtual {v1, v2, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2477482
    const-string v2, "page_fragment_uuid"

    new-instance v3, Landroid/os/ParcelUuid;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2477483
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2477484
    const/4 v1, 0x0

    invoke-static {p0, v1}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->a(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;Z)V

    .line 2477485
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d002f

    invoke-virtual {v1, v2, v0}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->c()I

    .line 2477486
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->M:LX/0if;

    sget-object v1, LX/0ig;->an:LX/0ih;

    const-string v2, "admin_redirect"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2477487
    iput-boolean v5, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aN:Z

    .line 2477488
    goto/16 :goto_1

    .line 2477489
    :cond_7
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2477490
    iget-object v1, v0, LX/HX9;->b:LX/03R;

    move-object v0, v1

    .line 2477491
    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_6

    .line 2477492
    invoke-virtual {v7}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->n()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_8

    invoke-virtual {v7}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->n()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v10}, LX/15i;->h(II)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    .line 2477493
    :goto_6
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->n:LX/2U1;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    invoke-virtual {v1}, LX/HX9;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->x()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->P()LX/0Px;

    move-result-object v3

    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_9

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v6

    :goto_7
    invoke-virtual/range {v0 .. v6}, LX/2U1;->b(Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/String;Ljava/lang/Boolean;LX/0am;)V

    goto/16 :goto_4

    :cond_8
    move-object v5, v4

    .line 2477494
    goto :goto_6

    .line 2477495
    :cond_9
    invoke-static {v6}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v6

    goto :goto_7

    .line 2477496
    :cond_a
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    invoke-virtual {v0, v7, v8}, LX/CZd;->a(Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;LX/0ta;)V

    .line 2477497
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    .line 2477498
    iget-object v1, v0, LX/CZd;->j:LX/0Px;

    move-object v0, v1

    .line 2477499
    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    .line 2477500
    iget-object v1, v0, LX/CZd;->j:LX/0Px;

    move-object v0, v1

    .line 2477501
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_e

    .line 2477502
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ab:LX/HQB;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    .line 2477503
    iget-object v2, v1, LX/CZd;->j:LX/0Px;

    move-object v1, v2

    .line 2477504
    const/4 v4, 0x0

    .line 2477505
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    move v3, v4

    .line 2477506
    :goto_8
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v2

    if-ge v3, v2, :cond_c

    .line 2477507
    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/9Y7;

    .line 2477508
    if-eqz v2, :cond_b

    invoke-static {v2}, LX/HQA;->a(LX/9Y7;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 2477509
    invoke-interface {v2}, LX/9Y7;->k()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NameUppercaseModel;

    move-result-object v6

    if-eqz v6, :cond_22

    invoke-interface {v2}, LX/9Y7;->k()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NameUppercaseModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NameUppercaseModel;->a()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_22

    invoke-interface {v2}, LX/9Y7;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v6

    if-eqz v6, :cond_22

    invoke-interface {v2}, LX/9Y7;->e()Lcom/facebook/graphql/enums/GraphQLPagePresenceTabContentType;

    move-result-object v6

    if-eqz v6, :cond_22

    const/4 v6, 0x1

    :goto_9
    move v6, v6

    .line 2477510
    if-eqz v6, :cond_b

    .line 2477511
    invoke-virtual {v5, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2477512
    :cond_b
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_8

    .line 2477513
    :cond_c
    iget-object v2, v0, LX/HQB;->b:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v3

    if-eq v2, v3, :cond_d

    const/4 v4, 0x1

    .line 2477514
    :cond_d
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    iput-object v2, v0, LX/HQB;->b:LX/0Px;

    .line 2477515
    move v0, v4

    .line 2477516
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    .line 2477517
    iget-boolean v2, v1, LX/CZd;->l:Z

    move v1, v2

    .line 2477518
    if-nez v1, :cond_e

    .line 2477519
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aj:LX/HX5;

    .line 2477520
    iput-boolean v0, v1, LX/HX5;->e:Z

    .line 2477521
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aj:LX/HX5;

    invoke-virtual {v0}, LX/0gG;->kV_()V

    .line 2477522
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aA:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    invoke-virtual {v0, v10}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setVisibility(I)V

    .line 2477523
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aA:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    invoke-virtual {v0}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->b()V

    .line 2477524
    sget-object v0, LX/HPm;->DEFAULT:LX/HPm;

    if-ne p2, v0, :cond_19

    .line 2477525
    iget-boolean v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aG:Z

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ab:LX/HQB;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aF:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-virtual {v0, v1}, LX/HQB;->a(Lcom/facebook/graphql/enums/GraphQLPageActionType;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 2477526
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->Y:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ab:LX/HQB;

    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aF:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-virtual {v1, v2}, LX/HQB;->b(Lcom/facebook/graphql/enums/GraphQLPageActionType;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 2477527
    iput-boolean v10, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aG:Z

    .line 2477528
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aF:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-static {p0, v0, v11}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->a$redex0(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;Lcom/facebook/graphql/enums/GraphQLPageActionType;Z)V

    .line 2477529
    :cond_e
    :goto_a
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2477530
    iget-object v1, v0, LX/HX9;->b:LX/03R;

    move-object v0, v1

    .line 2477531
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2477532
    iget-object v3, v2, LX/HX9;->f:LX/89z;

    move-object v2, v3

    .line 2477533
    iget-object v3, v1, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v3, v3

    .line 2477534
    if-eqz v3, :cond_23

    .line 2477535
    iget-object v3, v1, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v3, v3

    .line 2477536
    invoke-virtual {v3}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->v()Z

    move-result v3

    if-eqz v3, :cond_23

    const/4 v3, 0x1

    .line 2477537
    :goto_b
    iget-object v4, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->h:LX/HEQ;

    .line 2477538
    iput-object v0, v4, LX/HEQ;->i:LX/03R;

    .line 2477539
    if-eqz v3, :cond_f

    .line 2477540
    iget-object v3, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->h:LX/HEQ;

    const-string v4, "Owned"

    invoke-virtual {v3, v4}, LX/HEQ;->a(Ljava/lang/String;)V

    .line 2477541
    :cond_f
    iget-object v3, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->h:LX/HEQ;

    const-string v4, "SuperCategoryType"

    iget-object v5, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    invoke-static {v5}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->a(LX/CZd;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/HEQ;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2477542
    iget-object v3, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->h:LX/HEQ;

    const-string v4, "Referrer"

    iget-object v5, v2, LX/89z;->loggingName:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, LX/HEQ;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2477543
    iget-object v3, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aI:Ljava/lang/String;

    if-eqz v3, :cond_10

    .line 2477544
    iget-object v3, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->h:LX/HEQ;

    const-string v4, "SurfaceFirstCardFromEarlyFetcher"

    invoke-virtual {v3, v4}, LX/HEQ;->a(Ljava/lang/String;)V

    .line 2477545
    :cond_10
    iget-object v3, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aJ:Ljava/lang/String;

    if-eqz v3, :cond_11

    .line 2477546
    iget-object v3, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->h:LX/HEQ;

    const-string v4, "SurfaceFirstCardCachedWithEarlyFetcher"

    invoke-virtual {v3, v4}, LX/HEQ;->a(Ljava/lang/String;)V

    .line 2477547
    :cond_11
    iget-object v3, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->h:LX/HEQ;

    const-string v4, "FirstCardInitFetchNum"

    iget-object v5, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->f:LX/8Do;

    invoke-virtual {v5}, LX/8Do;->j()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/HEQ;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2477548
    iget-object v3, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->h:LX/HEQ;

    const-string v4, "WrapperOnHomeTab"

    iget-object v5, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->f:LX/8Do;

    invoke-virtual {v5}, LX/8Do;->k()Z

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/HEQ;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2477549
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->Z:Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    invoke-virtual {v0, v1}, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->a(LX/CZd;)V

    .line 2477550
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aj:LX/HX5;

    .line 2477551
    const/4 v1, 0x0

    move v2, v1

    :goto_c
    iget-object v1, v0, LX/HX5;->b:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v2, v1, :cond_13

    .line 2477552
    iget-object v1, v0, LX/HX5;->b:Landroid/util/SparseArray;

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v1

    .line 2477553
    iget-object v3, v0, LX/HX5;->b:Landroid/util/SparseArray;

    invoke-virtual {v3, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    .line 2477554
    if-eqz v1, :cond_12

    .line 2477555
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/base/fragment/FbFragment;

    .line 2477556
    instance-of v3, v1, LX/HPA;

    if-eqz v3, :cond_12

    .line 2477557
    check-cast v1, LX/HPA;

    iget-object v3, v0, LX/HX5;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v3, v3, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    invoke-interface {v1, v3}, LX/HPA;->a(LX/CZd;)V

    .line 2477558
    :cond_12
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_c

    .line 2477559
    :cond_13
    iput-boolean v11, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->O:Z

    .line 2477560
    invoke-direct {p0, v7}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->c(Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;)V

    .line 2477561
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aj:LX/HX5;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ab:LX/HQB;

    invoke-virtual {v1}, LX/HQB;->d()I

    move-result v1

    invoke-virtual {v0, v1}, LX/HX5;->e(I)Lcom/facebook/base/fragment/FbFragment;

    move-result-object v0

    .line 2477562
    instance-of v1, v0, LX/HPB;

    if-eqz v1, :cond_14

    .line 2477563
    check-cast v0, LX/HPB;

    invoke-interface {v0}, LX/HPB;->b()V

    .line 2477564
    :cond_14
    invoke-direct {p0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->z()V

    .line 2477565
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mParentFragment:Landroid/support/v4/app/Fragment;

    move-object v0, v0

    .line 2477566
    if-eqz v0, :cond_15

    instance-of v1, v0, LX/145;

    if-eqz v1, :cond_15

    .line 2477567
    check-cast v0, LX/145;

    invoke-interface {v0, p1, p2}, LX/145;->a(Lcom/facebook/graphql/executor/GraphQLResult;LX/HPm;)V

    .line 2477568
    :cond_15
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->X:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-eqz v0, :cond_16

    .line 2477569
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->X:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, v10}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2477570
    :cond_16
    invoke-direct {p0, v7}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->a(Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;)V

    .line 2477571
    invoke-direct {p0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->D()V

    .line 2477572
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aK:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-eqz v0, :cond_17

    .line 2477573
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment$23;

    invoke-direct {v1, p0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment$23;-><init>(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V

    const v2, -0x466d27e

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 2477574
    :cond_17
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aM:LX/4At;

    if-eqz v0, :cond_0

    .line 2477575
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aM:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    goto/16 :goto_1

    .line 2477576
    :cond_18
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ab:LX/HQB;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ab:LX/HQB;

    invoke-virtual {v1}, LX/HQB;->c()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/HQB;->a(Lcom/facebook/graphql/enums/GraphQLPageActionType;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 2477577
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->Y:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ab:LX/HQB;

    invoke-virtual {v1}, LX/HQB;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 2477578
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ab:LX/HQB;

    invoke-virtual {v0}, LX/HQB;->c()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v0

    invoke-static {p0, v0, v11}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->a$redex0(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;Lcom/facebook/graphql/enums/GraphQLPageActionType;Z)V

    goto/16 :goto_a

    .line 2477579
    :cond_19
    sget-object v0, LX/HPm;->FORCED_BY_USER:LX/HPm;

    if-ne p2, v0, :cond_e

    .line 2477580
    invoke-direct {p0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->C()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v0

    invoke-static {p0, v0, v10}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->a$redex0(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;Lcom/facebook/graphql/enums/GraphQLPageActionType;Z)V

    goto/16 :goto_a

    :cond_1a
    move-object v6, v4

    goto/16 :goto_2

    .line 2477581
    :cond_1b
    sget-object v2, LX/03R;->NO:LX/03R;

    .line 2477582
    iput-object v2, v1, LX/HX8;->b:LX/03R;

    .line 2477583
    goto/16 :goto_3

    .line 2477584
    :cond_1c
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2477585
    iget-object v2, v0, LX/HX9;->b:LX/03R;

    move-object v0, v2

    .line 2477586
    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-nez v0, :cond_1e

    .line 2477587
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2477588
    iget-boolean v2, v0, LX/HX9;->e:Z

    move v0, v2

    .line 2477589
    if-eqz v0, :cond_1d

    .line 2477590
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->E:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Non-admin ends up in admin container fragment"

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1d
    move v0, v1

    .line 2477591
    goto/16 :goto_5

    .line 2477592
    :cond_1e
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2477593
    iget-boolean v2, v0, LX/HX9;->e:Z

    move v0, v2

    .line 2477594
    if-nez v0, :cond_21

    .line 2477595
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->E:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "admin redirection"

    invoke-static {v2, v3}, LX/0VG;->b(Ljava/lang/String;Ljava/lang/String;)LX/0VG;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/03V;->a(LX/0VG;)V

    .line 2477596
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 2477597
    if-eqz v0, :cond_1f

    .line 2477598
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 2477599
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_1f

    iget-boolean v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aD:Z

    if-eqz v0, :cond_20

    :cond_1f
    move v0, v1

    .line 2477600
    goto/16 :goto_5

    .line 2477601
    :cond_20
    const/4 v0, 0x1

    goto/16 :goto_5

    :cond_21
    move v0, v1

    .line 2477602
    goto/16 :goto_5

    :cond_22
    const/4 v6, 0x0

    goto/16 :goto_9

    .line 2477603
    :cond_23
    const/4 v3, 0x0

    goto/16 :goto_b
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2477424
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mParentFragment:Landroid/support/v4/app/Fragment;

    move-object v0, v0

    .line 2477425
    if-eqz v0, :cond_0

    instance-of v1, v0, LX/145;

    if-eqz v1, :cond_0

    .line 2477426
    check-cast v0, LX/145;

    invoke-interface {v0, p1}, LX/145;->a(Ljava/lang/Throwable;)V

    .line 2477427
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->A:LX/16I;

    invoke-virtual {v0}, LX/16I;->a()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->z:LX/0ad;

    sget-short v1, LX/8Dn;->j:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2477428
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->B:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0817c4

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2477429
    :cond_2
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->X:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-eqz v0, :cond_3

    .line 2477430
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->X:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, v3}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2477431
    :cond_3
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->a(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;Z)V

    .line 2477432
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aK:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 2477433
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aL:LX/4At;

    if-eqz v0, :cond_4

    .line 2477434
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aL:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 2477435
    :cond_4
    return-void
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLPageActionType;)Z
    .locals 6

    .prologue
    .line 2477411
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ab:LX/HQB;

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    .line 2477412
    iget-object p0, v0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, p0

    .line 2477413
    :goto_0
    if-eqz v1, :cond_2

    invoke-virtual {v1, p1}, LX/HQB;->a(Lcom/facebook/graphql/enums/GraphQLPageActionType;)Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v3, 0x0

    .line 2477414
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->k()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$AddableTabsChannelModel;

    move-result-object v2

    if-nez v2, :cond_3

    :cond_0
    move v2, v3

    .line 2477415
    :goto_1
    move v2, v2

    .line 2477416
    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :goto_2
    move v0, v2

    .line 2477417
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_2

    .line 2477418
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->k()Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$AddableTabsChannelModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageActionChannelDataModel$AddableTabsChannelModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result p0

    move v4, v3

    .line 2477419
    :goto_3
    if-ge v4, p0, :cond_5

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;

    .line 2477420
    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v2

    if-ne v2, p1, :cond_4

    .line 2477421
    const/4 v2, 0x1

    goto :goto_1

    .line 2477422
    :cond_4
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_3

    :cond_5
    move v2, v3

    .line 2477423
    goto :goto_1
.end method

.method public final b()Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2477406
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2477407
    const-string v1, "profile_id"

    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2477408
    iget-wide v4, v2, LX/HX9;->a:J

    move-wide v2, v4

    .line 2477409
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2477410
    return-object v0
.end method

.method public final b(LX/0fx;)V
    .locals 0

    .prologue
    .line 2477404
    iput-object p1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aE:LX/0fx;

    .line 2477405
    return-void
.end method

.method public final b(Lcom/facebook/graphql/enums/GraphQLPageActionType;)V
    .locals 9

    .prologue
    .line 2477397
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aL:LX/4At;

    if-nez v0, :cond_0

    .line 2477398
    new-instance v0, LX/4At;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0817ce

    invoke-direct {v0, v1, v2}, LX/4At;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aL:LX/4At;

    .line 2477399
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aL:LX/4At;

    invoke-virtual {v0}, LX/4At;->beginShowingProgress()V

    .line 2477400
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->K:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HQ9;

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    .line 2477401
    iget-wide v7, v0, LX/CZd;->a:J

    move-wide v2, v7

    .line 2477402
    const/4 v5, 0x0

    new-instance v6, LX/HWt;

    invoke-direct {v6, p0}, LX/HWt;-><init>(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V

    move-object v4, p1

    invoke-virtual/range {v1 .. v6}, LX/HQ9;->a(JLcom/facebook/graphql/enums/GraphQLPageActionType;Ljava/lang/String;LX/HBy;)V

    .line 2477403
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 2477394
    invoke-static {p0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->v(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V

    .line 2477395
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->Y:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    invoke-static {p0, v0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->d$redex0(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;I)V

    .line 2477396
    return-void
.end method

.method public final c(Z)V
    .locals 2

    .prologue
    .line 2477388
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->U:Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;

    if-eqz v0, :cond_0

    .line 2477389
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->U:Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;

    invoke-virtual {v0, v1}, LX/0hH;->a(Landroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->c()I

    .line 2477390
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->U:Lcom/facebook/quickpromotion/ui/QuickPromotionFragment;

    .line 2477391
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->T:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2477392
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->T:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2477393
    :cond_0
    return-void
.end method

.method public final c(Lcom/facebook/graphql/enums/GraphQLPageActionType;)Z
    .locals 1

    .prologue
    .line 2477387
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ab:LX/HQB;

    invoke-virtual {v0, p1}, LX/HQB;->a(Lcom/facebook/graphql/enums/GraphQLPageActionType;)Z

    move-result v0

    return v0
.end method

.method public final d(Lcom/facebook/graphql/enums/GraphQLPageActionType;)V
    .locals 9

    .prologue
    .line 2477992
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ab:LX/HQB;

    invoke-virtual {v0, p1}, LX/HQB;->a(Lcom/facebook/graphql/enums/GraphQLPageActionType;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2477993
    :goto_0
    return-void

    .line 2477994
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ab:LX/HQB;

    invoke-virtual {v0, p1}, LX/HQB;->b(Lcom/facebook/graphql/enums/GraphQLPageActionType;)I

    move-result v5

    .line 2477995
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    .line 2477996
    iget-boolean v1, v0, LX/CZd;->l:Z

    move v0, v1

    .line 2477997
    if-eqz v0, :cond_1

    .line 2477998
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->N:LX/HMa;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    .line 2477999
    iget-wide v7, v2, LX/CZd;->a:J

    move-wide v2, v7

    .line 2478000
    iget-object v4, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    .line 2478001
    iget-object v6, v4, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v4, v6

    .line 2478002
    invoke-virtual {v4}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->x()Ljava/lang/String;

    move-result-object v4

    iget-object v6, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ab:LX/HQB;

    invoke-virtual {v6, v5}, LX/HQB;->a(I)LX/9Y7;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/HMa;->a(Landroid/app/Activity;JLjava/lang/String;LX/9Y7;)V

    goto :goto_0

    .line 2478003
    :cond_1
    sget-object v0, LX/HX6;->SCROLL_TO_TAB:LX/HX6;

    invoke-static {p0, v0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->a$redex0(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;LX/HX6;)V

    .line 2478004
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->Y:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    invoke-virtual {v0, v5}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 2478082
    iget-boolean v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aw:Z

    return v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 2478081
    iget v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ay:I

    return v0
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 2478079
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->av:Z

    .line 2478080
    return-void
.end method

.method public final j()Lcom/facebook/search/api/GraphSearchQuery;
    .locals 13

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v10, 0x0

    .line 2478053
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    .line 2478054
    iget-object v1, v0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v0, v1

    .line 2478055
    if-nez v0, :cond_2

    .line 2478056
    :cond_0
    sget-object v1, Lcom/facebook/search/api/GraphSearchQuery;->e:Lcom/facebook/search/api/GraphSearchQuery;

    .line 2478057
    :cond_1
    :goto_0
    return-object v1

    .line 2478058
    :cond_2
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    .line 2478059
    iget-object v1, v0, LX/CZd;->e:Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;

    move-object v7, v1

    .line 2478060
    invoke-virtual {v7}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->x()Ljava/lang/String;

    move-result-object v3

    .line 2478061
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->F:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    sget v1, LX/2SU;->u:I

    invoke-virtual {v0, v1, v10}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, LX/7BH;->LIGHT:LX/7BH;

    sget-object v1, LX/103;->PAGE:LX/103;

    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    .line 2478062
    iget-wide v11, v2, LX/CZd;->a:J

    move-wide v8, v11

    .line 2478063
    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    sget-object v4, LX/7B5;->TAB:LX/7B5;

    invoke-static/range {v0 .. v5}, Lcom/facebook/search/api/GraphSearchQuery;->a(LX/7BH;LX/103;Ljava/lang/String;Ljava/lang/String;LX/7B5;Z)Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v0

    move-object v1, v0

    .line 2478064
    :goto_1
    invoke-virtual {v7}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->B()Lcom/facebook/graphql/enums/GraphQLPlaceType;

    move-result-object v0

    .line 2478065
    if-eqz v0, :cond_1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPlaceType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    if-eq v0, v2, :cond_1

    .line 2478066
    invoke-virtual {v7}, Lcom/facebook/pages/data/graphql/pageheader/FetchPageHeaderGraphQLModels$PageAllHeaderDataModel;->p()LX/0Px;

    move-result-object v2

    .line 2478067
    sget-object v3, LX/7B4;->PLACE:LX/7B4;

    new-instance v4, LX/7BA;

    invoke-direct {v4}, LX/7BA;-><init>()V

    .line 2478068
    iput-object v0, v4, LX/7BA;->a:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    .line 2478069
    move-object v4, v4

    .line 2478070
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    move-object v0, v6

    .line 2478071
    :goto_2
    iput-object v0, v4, LX/7BA;->b:Ljava/lang/String;

    .line 2478072
    move-object v0, v4

    .line 2478073
    new-instance v2, Lcom/facebook/search/api/GraphSearchQueryPlaceModifier;

    invoke-direct {v2, v0}, Lcom/facebook/search/api/GraphSearchQueryPlaceModifier;-><init>(LX/7BA;)V

    move-object v0, v2

    .line 2478074
    invoke-virtual {v1, v3, v0}, Lcom/facebook/search/api/GraphSearchQuery;->a(LX/7B4;Landroid/os/Parcelable;)V

    goto :goto_0

    .line 2478075
    :cond_3
    sget-object v0, LX/7BH;->LIGHT:LX/7BH;

    sget-object v1, LX/103;->PAGE:LX/103;

    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    .line 2478076
    iget-wide v11, v2, LX/CZd;->a:J

    move-wide v8, v11

    .line 2478077
    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    move-object v4, v6

    invoke-static/range {v0 .. v5}, Lcom/facebook/search/api/GraphSearchQuery;->a(LX/7BH;LX/103;Ljava/lang/String;Ljava/lang/String;LX/7B5;Z)Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    .line 2478078
    :cond_4
    invoke-virtual {v2, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_2
.end method

.method public final k()Landroid/os/ParcelUuid;
    .locals 2

    .prologue
    .line 2478043
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2478044
    iget-object v1, v0, LX/HX9;->c:Landroid/os/ParcelUuid;

    move-object v0, v1

    .line 2478045
    if-nez v0, :cond_0

    .line 2478046
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2478047
    if-eqz v0, :cond_0

    .line 2478048
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2478049
    const-string v1, "page_fragment_uuid"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/ParcelUuid;

    invoke-direct {p0, v0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->a(Landroid/os/ParcelUuid;)V

    .line 2478050
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2478051
    iget-object v1, v0, LX/HX9;->c:Landroid/os/ParcelUuid;

    move-object v0, v1

    .line 2478052
    return-object v0
.end method

.method public final mK_()LX/63R;
    .locals 1

    .prologue
    .line 2478040
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->Z:Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;

    .line 2478041
    iget-object p0, v0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->n:Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;

    move-object v0, p0

    .line 2478042
    return-object v0
.end method

.method public final mm_()LX/HBQ;
    .locals 1

    .prologue
    .line 2478039
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ao:LX/HBQ;

    return-object v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    .line 2478005
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2478006
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 2478007
    const/16 v0, 0x2782

    if-ne p1, v0, :cond_5

    if-eqz p3, :cond_5

    .line 2478008
    const-string v0, "extra_add_tab_type"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2478009
    const-string v0, "extra_add_tab_type"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 2478010
    invoke-static {p0, v0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->h(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;Lcom/facebook/graphql/enums/GraphQLPageActionType;)V

    .line 2478011
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->D:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HMH;

    invoke-virtual {v0, p1}, LX/HMH;->a(I)LX/HMI;

    move-result-object v0

    .line 2478012
    if-eqz v0, :cond_1

    .line 2478013
    invoke-direct {p0, v0, p3, p1}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->a(LX/HMI;Landroid/content/Intent;I)V

    .line 2478014
    :cond_1
    return-void

    .line 2478015
    :cond_2
    const-string v0, "extra_go_to_tab_type"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2478016
    const-string v0, "extra_go_to_tab_type"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 2478017
    if-eqz v0, :cond_0

    .line 2478018
    invoke-virtual {p0, v0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->c(Lcom/facebook/graphql/enums/GraphQLPageActionType;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2478019
    invoke-virtual {p0, v0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->d(Lcom/facebook/graphql/enums/GraphQLPageActionType;)V

    goto :goto_0

    .line 2478020
    :cond_3
    invoke-direct {p0, v0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->k(Lcom/facebook/graphql/enums/GraphQLPageActionType;)V

    goto :goto_0

    .line 2478021
    :cond_4
    const-string v0, "extra_deleted_tab_type"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2478022
    invoke-static {p0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->v(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V

    .line 2478023
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_HOME:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-static {p0, v0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->e$redex0(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;Lcom/facebook/graphql/enums/GraphQLPageActionType;)V

    .line 2478024
    const-string v0, "extra_deleted_tab_type"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 2478025
    invoke-direct {p0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->C()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v1

    if-ne v1, v0, :cond_0

    .line 2478026
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ab:LX/HQB;

    invoke-virtual {v0}, LX/HQB;->c()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->d(Lcom/facebook/graphql/enums/GraphQLPageActionType;)V

    goto :goto_0

    .line 2478027
    :cond_5
    const/16 v0, 0x2781

    if-ne p1, v0, :cond_7

    .line 2478028
    const/4 v1, 0x0

    .line 2478029
    const-string v0, "extra_update_services_tab"

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2478030
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_SERVICES:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-virtual {p0, v0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->c(Lcom/facebook/graphql/enums/GraphQLPageActionType;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2478031
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_SERVICES:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-static {p0, v0, v1}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->a$redex0(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;Lcom/facebook/graphql/enums/GraphQLPageActionType;Z)V

    .line 2478032
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_SERVICES:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-static {p0, v0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->e$redex0(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;Lcom/facebook/graphql/enums/GraphQLPageActionType;)V

    .line 2478033
    :cond_6
    goto :goto_0

    .line 2478034
    :cond_7
    const/16 v0, 0x278b

    if-ne p1, v0, :cond_0

    .line 2478035
    const-string v0, "subscribe_status"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 2478036
    const-string v1, "secondary_subscribe_status"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 2478037
    const-string v2, "notification_status"

    const/4 v3, 0x0

    invoke-virtual {p3, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 2478038
    iget-object v3, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->j:LX/HDT;

    new-instance v4, LX/HDW;

    invoke-direct {v4, v0, v1, v2}, LX/HDW;-><init>(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;Z)V

    invoke-virtual {v3, v4}, LX/0b4;->a(LX/0b7;)V

    goto/16 :goto_0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 2477801
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2477802
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->at:I

    .line 2477803
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x7cccbc10

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2477985
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->f:LX/8Do;

    invoke-virtual {v0}, LX/8Do;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2477986
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0e067c

    invoke-direct {v0, v2, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p1, v0}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->V:Landroid/view/LayoutInflater;

    .line 2477987
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->V:Landroid/view/LayoutInflater;

    const v2, 0x7f030eab

    invoke-virtual {v0, v2, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomFrameLayout;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->W:Lcom/facebook/widget/CustomFrameLayout;

    .line 2477988
    :goto_0
    invoke-direct {p0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->r()V

    .line 2477989
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->W:Lcom/facebook/widget/CustomFrameLayout;

    const v2, -0x1c04ff57

    invoke-static {v2, v1}, LX/02F;->f(II)V

    return-object v0

    .line 2477990
    :cond_0
    iput-object p1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->V:Landroid/view/LayoutInflater;

    .line 2477991
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->V:Landroid/view/LayoutInflater;

    const v2, 0x7f030eab

    invoke-virtual {v0, v2, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomFrameLayout;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->W:Lcom/facebook/widget/CustomFrameLayout;

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 8

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/16 v0, 0x2a

    const v1, -0x5de56b53

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2477952
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2477953
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->u:LX/CSN;

    .line 2477954
    iget-object v2, v1, LX/CSN;->b:LX/0if;

    sget-object v3, LX/0ig;->ag:LX/0ih;

    invoke-virtual {v2, v3}, LX/0if;->c(LX/0ih;)V

    .line 2477955
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->v:LX/HBf;

    invoke-virtual {v1}, LX/HBf;->d()V

    .line 2477956
    iget-boolean v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aN:Z

    if-nez v1, :cond_0

    .line 2477957
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->M:LX/0if;

    sget-object v2, LX/0ig;->an:LX/0ih;

    invoke-virtual {v1, v2}, LX/0if;->c(LX/0ih;)V

    .line 2477958
    :cond_0
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ak:LX/HPn;

    invoke-virtual {v1}, LX/HPn;->a()V

    .line 2477959
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->X:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-eqz v1, :cond_1

    .line 2477960
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->X:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v1, v4}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 2477961
    iput-object v4, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->X:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 2477962
    :cond_1
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->an:LX/1B1;

    if-eqz v1, :cond_2

    .line 2477963
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->an:LX/1B1;

    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->l:LX/7jB;

    invoke-virtual {v1, v2}, LX/1B1;->b(LX/0b4;)V

    .line 2477964
    :cond_2
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->h:LX/HEQ;

    .line 2477965
    iget-object v2, v1, LX/HEQ;->a:LX/CXj;

    iget-object v3, v1, LX/HEQ;->g:LX/CXr;

    invoke-virtual {v2, v3}, LX/0b4;->b(LX/0b2;)Z

    .line 2477966
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->e:LX/BQb;

    .line 2477967
    const/4 v2, 0x0

    iput-object v2, v1, LX/BQb;->d:LX/HWy;

    .line 2477968
    iget-object v2, v1, LX/BQb;->a:LX/0b3;

    iget-object v3, v1, LX/BQb;->b:LX/BQa;

    invoke-virtual {v2, v3}, LX/0b4;->b(LX/0b2;)Z

    .line 2477969
    iget-object v2, v1, LX/BQb;->a:LX/0b3;

    iget-object v3, v1, LX/BQb;->c:LX/BQZ;

    invoke-virtual {v2, v3}, LX/0b4;->b(LX/0b2;)Z

    .line 2477970
    invoke-direct {p0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->s()V

    .line 2477971
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->r:LX/8E2;

    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2477972
    iget-wide v6, v2, LX/HX9;->a:J

    move-wide v2, v6

    .line 2477973
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v4}, LX/8E2;->b(Ljava/lang/String;LX/0Rf;)V

    .line 2477974
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->Z:Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;

    if-eqz v1, :cond_3

    .line 2477975
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->Z:Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;

    invoke-virtual {v1}, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->b()V

    .line 2477976
    :cond_3
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->k:LX/CXj;

    if-eqz v1, :cond_5

    .line 2477977
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ai:LX/CXt;

    if-eqz v1, :cond_4

    .line 2477978
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->k:LX/CXj;

    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ai:LX/CXt;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2477979
    :cond_4
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ah:LX/CXx;

    if-eqz v1, :cond_5

    .line 2477980
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->k:LX/CXj;

    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ah:LX/CXx;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2477981
    :cond_5
    invoke-static {p0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->F(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V

    .line 2477982
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->q:LX/8Fe;

    invoke-virtual {v1}, LX/8Fe;->b()V

    .line 2477983
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aD:Z

    .line 2477984
    const/16 v1, 0x2b

    const v2, -0x285bf3ad

    invoke-static {v5, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x434ecf50

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2477945
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2477946
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->O:Z

    .line 2477947
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aj:LX/HX5;

    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ab:LX/HQB;

    invoke-virtual {v2}, LX/HQB;->d()I

    move-result v2

    invoke-virtual {v0, v2}, LX/HX5;->e(I)Lcom/facebook/base/fragment/FbFragment;

    move-result-object v0

    .line 2477948
    instance-of v2, v0, LX/HPB;

    if-eqz v2, :cond_0

    .line 2477949
    check-cast v0, LX/HPB;

    invoke-interface {v0}, LX/HPB;->f()V

    .line 2477950
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->Z:Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;

    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ar:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-static {v0, v2}, LX/1r0;->a(Landroid/view/View;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 2477951
    const/16 v0, 0x2b

    const v2, -0x7e141258

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x4dfb6d07

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2477942
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2477943
    const/4 v1, 0x0

    invoke-static {p0, v1}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->a(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;Z)V

    .line 2477944
    const/16 v1, 0x2b

    const v2, 0x656d84c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 12

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x7a314296

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2477903
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2477904
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ak:LX/HPn;

    invoke-virtual {v0}, LX/HPn;->b()V

    .line 2477905
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->am:LX/1B1;

    if-eqz v0, :cond_0

    .line 2477906
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->am:LX/1B1;

    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->j:LX/HDT;

    invoke-virtual {v0, v2}, LX/1B1;->a(LX/0b4;)V

    .line 2477907
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aH:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HDS;

    .line 2477908
    iget-object v3, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->j:LX/HDT;

    invoke-virtual {v3, v0}, LX/0b4;->a(LX/0b7;)V

    goto :goto_0

    .line 2477909
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aH:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2477910
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->p:LX/9XF;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2477911
    iget-wide v6, v3, LX/HX9;->a:J

    move-wide v4, v6

    .line 2477912
    invoke-direct {p0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->C()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v3

    .line 2477913
    iget-boolean v6, v0, LX/9XF;->b:Z

    if-eqz v6, :cond_4

    .line 2477914
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 2477915
    const-string v6, "keyguard"

    invoke-virtual {v2, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/KeyguardManager;

    .line 2477916
    invoke-virtual {v6}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v6

    if-eqz v6, :cond_5

    move v6, v7

    .line 2477917
    :goto_1
    move v6, v6

    .line 2477918
    if-eqz v6, :cond_2

    .line 2477919
    iget-object v6, v0, LX/9XF;->a:LX/9XE;

    .line 2477920
    invoke-static {v6, v4, v5, v3}, LX/9XE;->c(LX/9XE;JLcom/facebook/graphql/enums/GraphQLPageActionType;)V

    .line 2477921
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->h:LX/HEQ;

    .line 2477922
    iget-object v2, v0, LX/HEQ;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x130080

    iget-object v4, v0, LX/HEQ;->j:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v4

    const/16 v5, 0x2a

    invoke-interface {v2, v3, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 2477923
    iget-boolean v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aC:Z

    if-eqz v0, :cond_3

    .line 2477924
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aC:Z

    .line 2477925
    :goto_3
    invoke-static {}, LX/009;->getInstance()LX/009;

    move-result-object v0

    const-string v2, "Last Opened Page Id"

    new-instance v3, LX/HAW;

    iget-object v4, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2477926
    iget-wide v6, v4, LX/HX9;->a:J

    move-wide v4, v6

    .line 2477927
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, LX/HAW;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2, v3}, LX/009;->putLazyCustomData(Ljava/lang/String;LX/00T;)V

    .line 2477928
    const v0, 0x62c29cb6

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void

    .line 2477929
    :cond_3
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->u:LX/CSN;

    .line 2477930
    iget-object v2, v0, LX/CSN;->b:LX/0if;

    sget-object v3, LX/0ig;->ag:LX/0ih;

    const-string v4, "fragment_resumed"

    invoke-virtual {v2, v3, v4}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2477931
    goto :goto_3

    .line 2477932
    :cond_4
    const/4 v6, 0x1

    iput-boolean v6, v0, LX/9XF;->b:Z

    goto :goto_2

    .line 2477933
    :cond_5
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v9, 0x14

    if-lt v6, v9, :cond_8

    .line 2477934
    const-string v6, "display"

    invoke-virtual {v2, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/hardware/display/DisplayManager;

    .line 2477935
    invoke-virtual {v6}, Landroid/hardware/display/DisplayManager;->getDisplays()[Landroid/view/Display;

    move-result-object v9

    array-length v10, v9

    move v6, v7

    :goto_4
    if-ge v6, v10, :cond_7

    aget-object v11, v9, v6

    .line 2477936
    invoke-virtual {v11}, Landroid/view/Display;->getState()I

    move-result v11

    if-eq v11, v8, :cond_6

    move v6, v8

    .line 2477937
    goto :goto_1

    .line 2477938
    :cond_6
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    :cond_7
    move v6, v7

    .line 2477939
    goto :goto_1

    .line 2477940
    :cond_8
    const-string v6, "power"

    invoke-virtual {v2, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/os/PowerManager;

    .line 2477941
    invoke-virtual {v6}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v6

    goto :goto_1
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2477897
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2477898
    const-string v0, "extra_is_admin"

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2477899
    iget-object v2, v1, LX/HX9;->b:LX/03R;

    move-object v1, v2

    .line 2477900
    invoke-virtual {v1}, LX/03R;->getDbValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2477901
    const-string v0, "extra_starting_tab"

    invoke-direct {p0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->C()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2477902
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 13
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2477804
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2477805
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->at:I

    .line 2477806
    const v0, 0x7f0d0595

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbSwipeRefreshLayout;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->X:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 2477807
    const v0, 0x7f0d23e1

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->T:Landroid/view/View;

    .line 2477808
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->F:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    sget v1, LX/8Dm;->h:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2477809
    new-instance v1, LX/0zw;

    const v0, 0x7f0d23e0

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->af:LX/0zw;

    .line 2477810
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->af:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const v1, 0x7f0d2e6b

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ag:Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;

    .line 2477811
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ag:Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2477812
    iget-object v2, v1, LX/HX9;->c:Landroid/os/ParcelUuid;

    move-object v1, v2

    .line 2477813
    invoke-virtual {v0, v1}, Lcom/facebook/pages/common/actionchannel/tabcalltoaction/PagesTabCallToActionButton;->a(Landroid/os/ParcelUuid;)V

    .line 2477814
    new-instance v0, LX/HWv;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2477815
    iget-object v2, v1, LX/HX9;->c:Landroid/os/ParcelUuid;

    move-object v1, v2

    .line 2477816
    invoke-direct {v0, p0, v1}, LX/HWv;-><init>(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;Landroid/os/ParcelUuid;)V

    move-object v0, v0

    .line 2477817
    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ah:LX/CXx;

    .line 2477818
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->k:LX/CXj;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ah:LX/CXx;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2477819
    :cond_0
    const v0, 0x7f0d23de

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->Z:Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;

    .line 2477820
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->Z:Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;

    invoke-virtual {p0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->k()Landroid/os/ParcelUuid;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->setLoggingUuid(Landroid/os/ParcelUuid;)V

    .line 2477821
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->Z:Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;

    .line 2477822
    iget-object v1, v0, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->n:Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;

    move-object v0, v1

    .line 2477823
    iget-object v1, v0, Lcom/facebook/pages/common/surface/ui/header/CaspianPagesHeaderView;->v:LX/HQL;

    move-object v0, v1

    .line 2477824
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    invoke-interface {v0, v1}, LX/HQL;->a(LX/0gc;)V

    .line 2477825
    new-instance v1, LX/0zw;

    const v0, 0x7f0d2427

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    new-instance v2, LX/HWh;

    invoke-direct {v2, p0}, LX/HWh;-><init>(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V

    invoke-direct {v1, v0, v2}, LX/0zw;-><init>(Landroid/view/ViewStub;LX/0zy;)V

    iput-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ac:LX/0zw;

    .line 2477826
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ad:Lcom/facebook/pages/fb4a/admintabs/PageIdentityAdminTabsView;

    if-eqz v0, :cond_1

    .line 2477827
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ac:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    .line 2477828
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->Z:Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 2477829
    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2477830
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ar:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 2477831
    :cond_2
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->X:Landroid/support/v4/widget/SwipeRefreshLayout;

    new-instance v1, LX/HWi;

    invoke-direct {v1, p0}, LX/HWi;-><init>(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 2477832
    const v0, 0x7f0d23df

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->Y:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    .line 2477833
    new-instance v1, LX/HX5;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v3

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2477834
    iget-wide v7, v0, LX/HX9;->a:J

    move-wide v4, v7

    .line 2477835
    move-object v2, p0

    move-object v6, p0

    invoke-direct/range {v1 .. v6}, LX/HX5;-><init>(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;LX/0gc;JLcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V

    iput-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aj:LX/HX5;

    .line 2477836
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->Y:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aj:LX/HX5;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2477837
    if-eqz p2, :cond_3

    .line 2477838
    const-string v0, "extra_starting_tab"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 2477839
    if-eqz v0, :cond_3

    .line 2477840
    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aF:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 2477841
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aG:Z

    .line 2477842
    :cond_3
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->d:LX/0zG;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->d:LX/0zG;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, LX/63T;

    if-eqz v0, :cond_4

    .line 2477843
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->d:LX/0zG;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/63T;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aa:LX/63T;

    .line 2477844
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aa:LX/63T;

    invoke-interface {v0}, LX/63T;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aB:I

    .line 2477845
    :cond_4
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aa:LX/63T;

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->av:Z

    if-nez v0, :cond_5

    .line 2477846
    invoke-direct {p0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->o()V

    .line 2477847
    :cond_5
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    .line 2477848
    iget-boolean v1, v0, LX/CZd;->l:Z

    move v0, v1

    .line 2477849
    if-nez v0, :cond_6

    .line 2477850
    const v7, 0x7f0d1570

    invoke-virtual {p0, v7}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iput-object v7, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aA:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    .line 2477851
    iget-object v7, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aA:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iget-object v8, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->Y:Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;

    invoke-virtual {v7, v8}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 2477852
    new-instance v7, LX/HWk;

    invoke-direct {v7, p0}, LX/HWk;-><init>(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V

    .line 2477853
    iget-object v8, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aA:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    .line 2477854
    iput-object v7, v8, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->m:LX/6Uh;

    .line 2477855
    iget-object v7, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->aA:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    new-instance v8, LX/HWl;

    invoke-direct {v8, p0}, LX/HWl;-><init>(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V

    .line 2477856
    iput-object v8, v7, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->l:LX/0hc;

    .line 2477857
    iget-object v7, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->f:LX/8Do;

    invoke-virtual {v7}, LX/8Do;->k()Z

    move-result v7

    if-eqz v7, :cond_6

    .line 2477858
    new-instance v7, LX/8E3;

    invoke-direct {v7}, LX/8E3;-><init>()V

    new-instance v8, LX/HWm;

    invoke-direct {v8, p0}, LX/HWm;-><init>(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V

    const/4 v9, 0x1

    invoke-virtual {v7, v8, v9}, LX/8E3;->a(Ljava/util/concurrent/Callable;Z)LX/8E3;

    move-result-object v7

    iget-object v8, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2477859
    iget-wide v11, v8, LX/HX9;->a:J

    move-wide v9, v11

    .line 2477860
    invoke-static {v9, v10}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    .line 2477861
    iput-object v8, v7, LX/8E3;->b:Ljava/lang/String;

    .line 2477862
    move-object v7, v7

    .line 2477863
    sget-object v8, LX/8E5;->INTERACTIVE:LX/8E5;

    .line 2477864
    iput-object v8, v7, LX/8E3;->a:LX/8E5;

    .line 2477865
    move-object v7, v7

    .line 2477866
    sget-object v8, LX/8E7;->PAGES_HEADER_PERF_LOGGING_STOPPED:LX/8E7;

    invoke-static {v8}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/8E3;->a(LX/0Rf;)LX/8E3;

    move-result-object v7

    sget-object v8, LX/8E7;->PAGES_FIRST_CARD_LOADING_TIMER:LX/8E7;

    invoke-static {v8}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/8E3;->a(LX/0Rf;)LX/8E3;

    move-result-object v7

    sget-object v8, LX/8E7;->PAGES_DEFAULT_TAB_FRAGMENT_LOADED:LX/8E7;

    invoke-static {v8}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/8E3;->b(LX/0Rf;)LX/8E3;

    move-result-object v7

    invoke-virtual {v7}, LX/8E3;->a()LX/8E6;

    move-result-object v7

    .line 2477867
    iget-object v8, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->r:LX/8E2;

    invoke-virtual {v8, v7}, LX/8E2;->a(LX/8E6;)V

    .line 2477868
    :cond_6
    invoke-direct {p0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->q()V

    .line 2477869
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    invoke-virtual {v0}, LX/CZd;->d()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2477870
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->Z:Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    invoke-virtual {v0, v1}, Lcom/facebook/pages/identity/fragments/surface/PagesHeaderContainer;->a(LX/CZd;)V

    .line 2477871
    :cond_7
    :goto_0
    new-instance v0, LX/HWj;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->as:LX/HX9;

    .line 2477872
    iget-object v2, v1, LX/HX9;->c:Landroid/os/ParcelUuid;

    move-object v1, v2

    .line 2477873
    invoke-direct {v0, p0, v1}, LX/HWj;-><init>(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;Landroid/os/ParcelUuid;)V

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ai:LX/CXt;

    .line 2477874
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->k:LX/CXj;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ai:LX/CXt;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2477875
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->h:LX/HEQ;

    .line 2477876
    iget-object v1, v0, LX/HEQ;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x130080

    iget-object v3, v0, LX/HEQ;->j:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    const/16 v4, 0x65

    invoke-interface {v1, v2, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 2477877
    return-void

    .line 2477878
    :cond_8
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->au:LX/CZd;

    invoke-virtual {v0}, LX/CZd;->c()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2477879
    invoke-direct {p0}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->D()V

    goto :goto_0
.end method
