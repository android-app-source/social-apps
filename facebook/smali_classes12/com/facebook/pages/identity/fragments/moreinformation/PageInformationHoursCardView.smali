.class public Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationHoursCardView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:Landroid/widget/LinearLayout;

.field private final c:LX/Dt4;

.field private final d:Landroid/view/LayoutInflater;

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2476432
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationHoursCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2476433
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2476434
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationHoursCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2476435
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2476436
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2476437
    const v0, 0x7f030e85

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2476438
    const v0, 0x7f0d2392

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationHoursCardView;->b:Landroid/widget/LinearLayout;

    .line 2476439
    new-instance v0, LX/Dt4;

    invoke-direct {v0}, LX/Dt4;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationHoursCardView;->c:LX/Dt4;

    .line 2476440
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationHoursCardView;->d:Landroid/view/LayoutInflater;

    .line 2476441
    const-class v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationHoursCardView;

    invoke-static {v0, p0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationHoursCardView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2476442
    return-void
.end method

.method private static a(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2476443
    invoke-virtual {p0, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2476444
    invoke-virtual {p0, p4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2476445
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2476446
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2476447
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2476448
    invoke-virtual {p1, p2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 2476449
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 2476450
    invoke-virtual {v0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v2

    .line 2476451
    invoke-virtual {p1, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v3

    .line 2476452
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v1, v4

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v1

    .line 2476453
    cmpl-float v2, v3, v1

    if-lez v2, :cond_1

    .line 2476454
    sub-float v1, v3, v1

    float-to-int v1, v1

    invoke-virtual {v0, v5, v5, v1, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 2476455
    :cond_0
    :goto_0
    return-void

    .line 2476456
    :cond_1
    cmpl-float v2, v1, v3

    if-lez v2, :cond_0

    .line 2476457
    sub-float/2addr v1, v3

    float-to-int v1, v1

    invoke-virtual {v0, v1, v5, v5, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationHoursCardView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationHoursCardView;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SG;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationHoursCardView;->a:LX/0SG;

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel$LocationModel;)V
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultTimeRangeFieldsModel;",
            ">;",
            "Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel$LocationModel;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2476458
    const/4 v1, 0x0

    .line 2476459
    if-eqz p2, :cond_0

    .line 2476460
    invoke-static/range {p2 .. p2}, LX/Dt4;->a(Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel$LocationModel;)Ljava/util/TimeZone;

    move-result-object v1

    .line 2476461
    :cond_0
    if-nez v1, :cond_9

    .line 2476462
    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationHoursCardView;->e:Z

    .line 2476463
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    move-object v4, v1

    .line 2476464
    :goto_0
    new-instance v1, LX/Dt3;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationHoursCardView;->a:LX/0SG;

    move-object/from16 v0, p1

    invoke-direct {v1, v0, v4, v2}, LX/Dt3;-><init>(Ljava/util/List;Ljava/util/TimeZone;LX/0SG;)V

    .line 2476465
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationHoursCardView;->c:LX/Dt4;

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationHoursCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, LX/Dt4;->a(LX/Dt3;Landroid/content/res/Resources;)Ljava/util/ArrayList;

    move-result-object v12

    .line 2476466
    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationHoursCardView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_1

    .line 2476467
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationHoursCardView;->b:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    goto :goto_1

    .line 2476468
    :cond_1
    const/4 v2, 0x0

    .line 2476469
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v13

    .line 2476470
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationHoursCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0830ee

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 2476471
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationHoursCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0a0579

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v15

    .line 2476472
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v16

    const/4 v1, 0x0

    move v10, v1

    move v11, v2

    :goto_2
    move/from16 v0, v16

    if-ge v10, v0, :cond_7

    invoke-virtual {v12, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Dt0;

    .line 2476473
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationHoursCardView;->d:Landroid/view/LayoutInflater;

    const v3, 0x7f030e87

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v17

    .line 2476474
    const v2, 0x7f0d2398

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 2476475
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 2476476
    invoke-virtual {v1}, LX/Dt0;->a()LX/Dsz;

    move-result-object v18

    .line 2476477
    invoke-virtual/range {v18 .. v18}, LX/Dsz;->a()Ljava/util/List;

    move-result-object v19

    .line 2476478
    invoke-virtual {v1}, LX/Dt0;->c()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2476479
    const v8, 0x7f0d239a

    .line 2476480
    const v7, 0x7f0d2399

    .line 2476481
    const v6, 0x7f0d2395

    .line 2476482
    const v5, 0x7f0d2394

    .line 2476483
    const v3, 0x7f0d2397

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 2476484
    const v9, 0x7f0d2396

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    const/16 v20, 0x8

    move/from16 v0, v20

    invoke-virtual {v9, v0}, Landroid/view/View;->setVisibility(I)V

    move v9, v8

    move v8, v7

    move v7, v6

    move v6, v5

    move-object v5, v3

    .line 2476485
    :goto_3
    invoke-interface/range {v19 .. v19}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    const/4 v3, 0x0

    move-object/from16 v0, v19

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2476486
    :cond_2
    invoke-virtual {v2, v9}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v3, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2476487
    invoke-virtual {v2, v9}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v3, v15}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2476488
    invoke-virtual {v2, v8}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/16 v8, 0x8

    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    .line 2476489
    :goto_4
    const/4 v3, 0x1

    move v8, v3

    :goto_5
    invoke-interface/range {v19 .. v19}, Ljava/util/List;->size()I

    move-result v3

    if-ge v8, v3, :cond_5

    .line 2476490
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationHoursCardView;->d:Landroid/view/LayoutInflater;

    const v9, 0x7f030e86

    const/16 v20, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v3, v9, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    .line 2476491
    move-object/from16 v0, v19

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object/from16 v0, v18

    iget-object v0, v0, LX/Dsz;->a:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-static {v9, v3, v0, v7, v6}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationHoursCardView;->a(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;II)V

    .line 2476492
    invoke-virtual {v2, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2476493
    add-int/lit8 v3, v8, 0x1

    move v8, v3

    goto :goto_5

    .line 2476494
    :cond_3
    const v8, 0x7f0d2399

    .line 2476495
    const v7, 0x7f0d239a

    .line 2476496
    const v6, 0x7f0d2394

    .line 2476497
    const v5, 0x7f0d2395

    .line 2476498
    const v3, 0x7f0d2396

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 2476499
    const v9, 0x7f0d2397

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    const/16 v20, 0x8

    move/from16 v0, v20

    invoke-virtual {v9, v0}, Landroid/view/View;->setVisibility(I)V

    move v9, v8

    move v8, v7

    move v7, v6

    move v6, v5

    move-object v5, v3

    goto/16 :goto_3

    .line 2476500
    :cond_4
    const/4 v3, 0x0

    move-object/from16 v0, v19

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object/from16 v0, v18

    iget-object v0, v0, LX/Dsz;->a:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-static {v2, v3, v0, v9, v8}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationHoursCardView;->a(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;II)V

    goto :goto_4

    .line 2476501
    :cond_5
    const/4 v2, 0x0

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2476502
    invoke-virtual {v1}, LX/Dt0;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2476503
    add-int/lit8 v2, v11, 0x1

    .line 2476504
    if-ne v2, v13, :cond_6

    .line 2476505
    const v1, 0x7f0d239b

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2476506
    :cond_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationHoursCardView;->b:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationHoursCardView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    move-object/from16 v0, v17

    invoke-virtual {v1, v0, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 2476507
    add-int/lit8 v1, v10, 0x1

    move v10, v1

    move v11, v2

    goto/16 :goto_2

    .line 2476508
    :cond_7
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationHoursCardView;->e:Z

    if-nez v1, :cond_8

    .line 2476509
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationHoursCardView;->b:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationHoursCardView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 2476510
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v2

    .line 2476511
    invoke-virtual {v4, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 2476512
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4, v2}, Ljava/util/TimeZone;->inDaylightTime(Ljava/util/Date;)Z

    move-result v2

    const/4 v3, 0x1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v4, v2, v3, v5}, Ljava/util/TimeZone;->getDisplayName(ZILjava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    .line 2476513
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationHoursCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0817b4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2476514
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2476515
    :cond_8
    return-void

    :cond_9
    move-object v4, v1

    goto/16 :goto_0
.end method
