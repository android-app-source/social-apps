.class public Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationReportProblemCard;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public final a:Landroid/widget/TextView;

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6G2;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/9XE;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2476520
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationReportProblemCard;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2476521
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2476522
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationReportProblemCard;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2476523
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2476524
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2476525
    const v0, 0x7f030e8c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2476526
    const v0, 0x7f0d23a4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationReportProblemCard;->a:Landroid/widget/TextView;

    .line 2476527
    const-class v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationReportProblemCard;

    invoke-static {v0, p0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationReportProblemCard;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2476528
    return-void
.end method

.method private static a(Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationReportProblemCard;LX/0Ot;LX/9XE;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationReportProblemCard;",
            "LX/0Ot",
            "<",
            "LX/6G2;",
            ">;",
            "LX/9XE;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2476529
    iput-object p1, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationReportProblemCard;->b:LX/0Ot;

    iput-object p2, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationReportProblemCard;->c:LX/9XE;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationReportProblemCard;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationReportProblemCard;

    const/16 v1, 0x186a

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-static {v0}, LX/9XE;->a(LX/0QB;)LX/9XE;

    move-result-object v0

    check-cast v0, LX/9XE;

    invoke-static {p0, v1, v0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationReportProblemCard;->a(Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationReportProblemCard;LX/0Ot;LX/9XE;)V

    return-void
.end method


# virtual methods
.method public setupOnClickReportBug(J)V
    .locals 3

    .prologue
    .line 2476530
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationReportProblemCard;->a:Landroid/widget/TextView;

    new-instance v1, LX/HWX;

    invoke-direct {v1, p0, p1, p2}, LX/HWX;-><init>(Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationReportProblemCard;J)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2476531
    return-void
.end method
