.class public Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private final a:Lcom/facebook/pages/identity/ui/text/TwoStringTextView;

.field private final b:Landroid/widget/LinearLayout;

.field private final c:Landroid/widget/LinearLayout;

.field private final d:Landroid/widget/TextView;

.field private final e:Landroid/widget/ImageView;

.field private final f:Landroid/widget/ImageView;

.field private final g:Landroid/widget/ImageView;

.field private final h:Landroid/widget/ImageView;

.field private final i:Landroid/view/View;

.field private final j:Landroid/view/View;

.field private final k:Landroid/content/res/ColorStateList;

.field private final l:Landroid/view/LayoutInflater;

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:LX/15i;

.field private q:I

.field private r:LX/15i;

.field private s:I

.field private t:LX/15i;

.field private u:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2475979
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2475980
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2475977
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2475978
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 2475962
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2475963
    const v0, 0x7f030e82

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2475964
    const v0, 0x7f0d237f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->a:Lcom/facebook/pages/identity/ui/text/TwoStringTextView;

    .line 2475965
    const v0, 0x7f0d2381

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->b:Landroid/widget/LinearLayout;

    .line 2475966
    const v0, 0x7f0d2383

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->c:Landroid/widget/LinearLayout;

    .line 2475967
    const v0, 0x7f0d2384

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->d:Landroid/widget/TextView;

    .line 2475968
    const v0, 0x7f0d2385

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->g:Landroid/widget/ImageView;

    .line 2475969
    const v0, 0x7f0d2388

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->h:Landroid/widget/ImageView;

    .line 2475970
    const v0, 0x7f0d2387

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->f:Landroid/widget/ImageView;

    .line 2475971
    const v0, 0x7f0d2386

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->e:Landroid/widget/ImageView;

    .line 2475972
    const v0, 0x7f0d2380

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->i:Landroid/view/View;

    .line 2475973
    const v0, 0x7f0d2382

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->j:Landroid/view/View;

    .line 2475974
    invoke-virtual {p0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a010c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-static {v0}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->k:Landroid/content/res/ColorStateList;

    .line 2475975
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->l:Landroid/view/LayoutInflater;

    .line 2475976
    return-void
.end method

.method private a(Ljava/lang/String;LX/3Sb;)Landroid/text/SpannableString;
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 2475953
    new-instance v6, Landroid/text/SpannableStringBuilder;

    invoke-direct {v6, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2475954
    invoke-interface {p2}, LX/3Sb;->b()LX/2sN;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v8, v0, LX/1vs;->a:LX/15i;

    iget v9, v0, LX/1vs;->b:I

    .line 2475955
    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    const/4 v3, -0x1

    iget-object v4, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->k:Landroid/content/res/ColorStateList;

    move-object v5, v1

    invoke-direct/range {v0 .. v5}, Landroid/text/style/TextAppearanceSpan;-><init>(Ljava/lang/String;IILandroid/content/res/ColorStateList;Landroid/content/res/ColorStateList;)V

    .line 2475956
    :try_start_0
    new-instance v3, LX/1yL;

    const/4 v4, 0x1

    invoke-virtual {v8, v9, v4}, LX/15i;->j(II)I

    move-result v4

    const/4 v5, 0x0

    invoke-virtual {v8, v9, v5}, LX/15i;->j(II)I

    move-result v5

    invoke-direct {v3, v4, v5}, LX/1yL;-><init>(II)V

    invoke-static {p1, v3}, LX/1yM;->a(Ljava/lang/String;LX/1yL;)LX/1yN;

    move-result-object v3

    .line 2475957
    iget v4, v3, LX/1yN;->a:I

    move v4, v4

    .line 2475958
    invoke-virtual {v3}, LX/1yN;->c()I

    move-result v3

    const/16 v5, 0x12

    invoke-virtual {v6, v0, v4, v3, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V
    :try_end_0
    .catch LX/47A; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2475959
    :catch_0
    move-exception v0

    .line 2475960
    const-string v3, "PageInformationBusinessInfoView"

    invoke-virtual {v0}, LX/47A;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 2475961
    :cond_0
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, v6}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2475946
    iget-boolean v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->m:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->n:Z

    if-eqz v0, :cond_0

    .line 2475947
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->i:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2475948
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->n:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->o:Z

    if-eqz v0, :cond_1

    .line 2475949
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->j:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2475950
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->m:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->n:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->o:Z

    if-eqz v0, :cond_2

    .line 2475951
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->i:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2475952
    :cond_2
    return-void
.end method

.method private static a(LX/15i;I)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2475943
    if-eqz p1, :cond_1

    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    .line 2475944
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 2475945
    invoke-virtual {p0, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    :goto_1
    return v1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_1
.end method

.method private b()V
    .locals 10

    .prologue
    const/4 v1, 0x0

    const v9, -0x7f0a2784

    const/4 v8, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2475851
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->p:LX/15i;

    iget v5, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->q:I

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v0, v5}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->a(LX/15i;I)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v3

    :goto_0
    if-eqz v0, :cond_1

    .line 2475852
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->a:Lcom/facebook/pages/identity/ui/text/TwoStringTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;->setVisibility(I)V

    .line 2475853
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_1
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->p:LX/15i;

    iget v5, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->q:I

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    invoke-static {v0, v5}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->a(LX/15i;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2475854
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_2
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->p:LX/15i;

    iget v5, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->q:I

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    invoke-virtual {v0, v5, v8}, LX/15i;->g(II)I

    move-result v2

    .line 2475855
    sget-object v5, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v5

    :try_start_3
    iget-object v6, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->p:LX/15i;

    iget v7, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->q:I

    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    invoke-virtual {v6, v7, v8}, LX/15i;->g(II)I

    move-result v5

    invoke-static {v6, v5, v4, v9}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v5

    .line 2475856
    invoke-virtual {v0, v2, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    if-eqz v5, :cond_3

    invoke-static {v5}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_1
    invoke-direct {p0, v2, v0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->a(Ljava/lang/String;LX/3Sb;)Landroid/text/SpannableString;

    move-result-object v0

    .line 2475857
    :goto_2
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_4
    iget-object v5, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->r:LX/15i;

    iget v6, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->s:I

    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_5

    invoke-static {v5, v6}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->a(LX/15i;I)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2475858
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_5
    iget-object v5, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->r:LX/15i;

    iget v6, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->s:I

    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_6

    .line 2475859
    invoke-virtual {v5, v6, v8}, LX/15i;->g(II)I

    move-result v2

    if-eqz v2, :cond_5

    move v2, v3

    :goto_3
    if-eqz v2, :cond_0

    .line 2475860
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_6
    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->r:LX/15i;

    iget v5, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->s:I

    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_7

    invoke-virtual {v2, v5, v8}, LX/15i;->g(II)I

    move-result v1

    .line 2475861
    sget-object v5, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v5

    :try_start_7
    iget-object v6, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->r:LX/15i;

    iget v7, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->s:I

    monitor-exit v5
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_8

    invoke-virtual {v6, v7, v8}, LX/15i;->g(II)I

    move-result v5

    invoke-static {v6, v5, v4, v9}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v4

    .line 2475862
    invoke-virtual {v2, v1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    if-eqz v4, :cond_7

    invoke-static {v4}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v1

    :goto_4
    invoke-direct {p0, v2, v1}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->a(Ljava/lang/String;LX/3Sb;)Landroid/text/SpannableString;

    move-result-object v1

    .line 2475863
    :cond_0
    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->a:Lcom/facebook/pages/identity/ui/text/TwoStringTextView;

    const-string v4, " \u00b7 "

    invoke-virtual {v2, v0, v1, v4}, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 2475864
    iput-boolean v3, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->m:Z

    .line 2475865
    :cond_1
    return-void

    .line 2475866
    :catchall_0
    move-exception v0

    :try_start_8
    monitor-exit v2
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    throw v0

    :cond_2
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_9
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->r:LX/15i;

    iget v5, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->s:I

    monitor-exit v2
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    invoke-static {v0, v5}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->a(LX/15i;I)Z

    move-result v0

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    :try_start_a
    monitor-exit v2
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    throw v0

    .line 2475867
    :catchall_2
    move-exception v0

    :try_start_b
    monitor-exit v2
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    throw v0

    .line 2475868
    :catchall_3
    move-exception v0

    :try_start_c
    monitor-exit v2
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    throw v0

    .line 2475869
    :catchall_4
    move-exception v0

    :try_start_d
    monitor-exit v5
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_4

    throw v0

    .line 2475870
    :cond_3
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_1

    :cond_4
    move-object v0, v1

    .line 2475871
    goto :goto_2

    .line 2475872
    :catchall_5
    move-exception v0

    :try_start_e
    monitor-exit v2
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_5

    throw v0

    .line 2475873
    :catchall_6
    move-exception v0

    :try_start_f
    monitor-exit v2
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_6

    throw v0

    :cond_5
    move v2, v4

    .line 2475874
    goto :goto_3

    :cond_6
    move v2, v4

    goto :goto_3

    .line 2475875
    :catchall_7
    move-exception v0

    :try_start_10
    monitor-exit v1
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_7

    throw v0

    .line 2475876
    :catchall_8
    move-exception v0

    :try_start_11
    monitor-exit v5
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_8

    throw v0

    .line 2475877
    :cond_7
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v1

    goto :goto_4
.end method

.method private e()V
    .locals 8

    .prologue
    const/4 v5, 0x2

    const/4 v7, -0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2475914
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 2475915
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->t:LX/15i;

    iget v4, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->u:I

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v0, v4}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->a(LX/15i;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2475916
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_1
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->t:LX/15i;

    iget v4, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->u:I

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2475917
    invoke-virtual {v0, v4, v5}, LX/15i;->g(II)I

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_0
    if-eqz v0, :cond_6

    .line 2475918
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_2
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->t:LX/15i;

    iget v4, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->u:I

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    invoke-virtual {v0, v4, v5}, LX/15i;->g(II)I

    move-result v3

    invoke-virtual {v0, v3, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 2475919
    array-length v0, v3

    if-lez v0, :cond_0

    .line 2475920
    iput-boolean v1, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->n:Z

    .line 2475921
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2475922
    :cond_0
    :goto_1
    array-length v0, v3

    add-int/lit8 v0, v0, 0x1

    div-int/lit8 v0, v0, 0x2

    if-ge v2, v0, :cond_6

    .line 2475923
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->l:Landroid/view/LayoutInflater;

    const v4, 0x7f030e8e

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 2475924
    array-length v0, v3

    if-ne v0, v1, :cond_1

    .line 2475925
    const v0, 0x7f0d23a6

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 2475926
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v5, v7, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2475927
    const/16 v6, 0xd

    invoke-virtual {v5, v6, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 2475928
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2475929
    :cond_1
    mul-int/lit8 v0, v2, 0x2

    array-length v5, v3

    if-ge v0, v5, :cond_2

    .line 2475930
    const v0, 0x7f0d23a8

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 2475931
    const v5, 0x7f0d23a7

    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    mul-int/lit8 v5, v2, 0x2

    aget-object v5, v3, v5

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2475932
    :cond_2
    mul-int/lit8 v0, v2, 0x2

    add-int/lit8 v0, v0, 0x1

    array-length v5, v3

    if-ge v0, v5, :cond_5

    .line 2475933
    const v0, 0x7f0d23a9

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 2475934
    const v5, 0x7f0d23a7

    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    mul-int/lit8 v5, v2, 0x2

    add-int/lit8 v5, v5, 0x1

    aget-object v5, v3, v5

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2475935
    :goto_2
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2475936
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2475937
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 2475938
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    :cond_3
    move v0, v2

    .line 2475939
    goto/16 :goto_0

    :cond_4
    move v0, v2

    goto/16 :goto_0

    .line 2475940
    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0

    .line 2475941
    :cond_5
    const v0, 0x7f0d23a9

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v5, 0x8

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 2475942
    :cond_6
    return-void
.end method

.method private setPaymentOptions(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2475893
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

    .line 2475894
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;->MASTERCARD:Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

    invoke-virtual {v0, v2}, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2475895
    iput-boolean v5, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->o:Z

    .line 2475896
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 2475897
    :cond_1
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;->VISA:Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

    invoke-virtual {v0, v2}, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2475898
    iput-boolean v5, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->o:Z

    .line 2475899
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 2475900
    :cond_2
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;->AMEX:Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

    invoke-virtual {v0, v2}, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2475901
    iput-boolean v5, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->o:Z

    .line 2475902
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->g:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 2475903
    :cond_3
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;->DISCOVER:Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

    invoke-virtual {v0, v2}, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2475904
    iput-boolean v5, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->o:Z

    .line 2475905
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 2475906
    :cond_4
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;->CASH_ONLY:Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;

    invoke-virtual {v0, v2}, Lcom/facebook/graphql/enums/GraphQLPagePaymentOption;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2475907
    iput-boolean v5, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->o:Z

    .line 2475908
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2475909
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v5, :cond_0

    .line 2475910
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->d:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0817da

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2475911
    :cond_5
    iget-boolean v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->o:Z

    if-eqz v0, :cond_6

    .line 2475912
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2475913
    :cond_6
    return-void
.end method


# virtual methods
.method public setData(Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 2475878
    invoke-virtual {p1}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;->k()LX/2uF;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;->k()LX/2uF;

    move-result-object v0

    invoke-virtual {v0}, LX/39O;->a()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2475879
    invoke-virtual {p1}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;->k()LX/2uF;

    move-result-object v0

    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, LX/2sN;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, LX/2sN;->b()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2475880
    const-class v3, Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;

    invoke-virtual {v2, v1, v5, v3, v4}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;->SPECIALTY:Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;

    if-ne v3, v4, :cond_1

    .line 2475881
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iput-object v2, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->p:LX/15i;

    iput v1, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->q:I

    monitor-exit v3

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 2475882
    :cond_1
    const-class v3, Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;

    invoke-virtual {v2, v1, v5, v3, v4}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;->PARKING:Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;

    if-ne v3, v4, :cond_2

    .line 2475883
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_1
    iput-object v2, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->r:LX/15i;

    iput v1, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->s:I

    monitor-exit v3

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0

    .line 2475884
    :cond_2
    const-class v3, Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;

    invoke-virtual {v2, v1, v5, v3, v4}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;->SERVICES:Lcom/facebook/graphql/enums/GraphQLBusinessInfoType;

    if-ne v3, v4, :cond_0

    .line 2475885
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_2
    iput-object v2, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->t:LX/15i;

    iput v1, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->u:I

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 2475886
    invoke-direct {p0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->e()V

    goto :goto_0

    .line 2475887
    :catchall_2
    move-exception v0

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    throw v0

    .line 2475888
    :cond_3
    invoke-direct {p0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->b()V

    .line 2475889
    invoke-virtual {p1}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;->u()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2475890
    invoke-virtual {p1}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;->u()LX/0Px;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->setPaymentOptions(Ljava/util/List;)V

    .line 2475891
    :cond_4
    invoke-direct {p0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->a()V

    .line 2475892
    return-void
.end method
