.class public Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements LX/GZf;


# static fields
.field public static final N:[I


# instance fields
.field public A:J

.field public B:Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;

.field public C:LX/14l;

.field public D:LX/HWU;

.field public E:Landroid/view/LayoutInflater;

.field public F:LX/E8s;

.field private G:LX/E8t;

.field public H:Landroid/view/View;

.field public I:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

.field public J:Z

.field public K:I

.field private L:I

.field private M:I

.field public O:Z

.field public P:Z

.field public Q:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;",
            ">;>;"
        }
    .end annotation
.end field

.field public a:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0hx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0So;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0zG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0kb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9XE;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0rq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private l:Landroid/view/View;

.field private m:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationActionSheet;

.field private n:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationHoursCardView;

.field private o:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;

.field private p:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationSuggestEditCard;

.field private q:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationReportProblemCard;

.field private r:Lcom/facebook/pages/fb4a/vertex_attribution/PagesVertexAttributionView;

.field public s:Landroid/widget/LinearLayout;

.field public t:Lcom/facebook/widget/ScrollingAwareScrollView;

.field private u:Landroid/widget/LinearLayout;

.field private v:Landroid/widget/ProgressBar;

.field private w:Landroid/view/View;

.field private x:Landroid/view/View;

.field private y:Landroid/widget/LinearLayout;

.field private z:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2476340
    const/4 v0, 0x2

    new-array v0, v0, [I

    sput-object v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->N:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2476288
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2476289
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->z:J

    .line 2476290
    iput-boolean v2, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->J:Z

    .line 2476291
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->M:I

    .line 2476292
    iput-boolean v2, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->O:Z

    .line 2476293
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->P:Z

    .line 2476294
    return-void
.end method

.method public static a(JZ)Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;
    .locals 4

    .prologue
    .line 2476295
    new-instance v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;

    invoke-direct {v0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;-><init>()V

    .line 2476296
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2476297
    const-string v2, "com.facebook.katana.profile.id"

    invoke-virtual {v1, v2, p0, p1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2476298
    const-string v2, "extra_is_inside_page_surface_tab"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2476299
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2476300
    return-object v0
.end method

.method public static a$redex0(Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;LX/HWU;)V
    .locals 10

    .prologue
    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    const/16 v6, 0x8

    .line 2476301
    iput-object p1, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->D:LX/HWU;

    .line 2476302
    sget-object v0, LX/HWS;->a:[I

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->D:LX/HWU;

    invoke-virtual {v1}, LX/HWU;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2476303
    iget-wide v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->z:J

    cmp-long v0, v0, v8

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->b:LX/0hx;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->c:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->z:J

    sub-long/2addr v2, v4

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->v:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2, v3, v1}, LX/0hx;->a(JLandroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2476304
    iput-wide v8, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->z:J

    .line 2476305
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->v:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v6}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2476306
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->w:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 2476307
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->x:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 2476308
    :goto_0
    return-void

    .line 2476309
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->v:Landroid/widget/ProgressBar;

    invoke-static {v0}, LX/0hx;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2476310
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->z:J

    .line 2476311
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->b:LX/0hx;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0hx;->a(Z)V

    .line 2476312
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->v:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v7}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2476313
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->w:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 2476314
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->x:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 2476315
    :pswitch_1
    iget-wide v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->z:J

    cmp-long v0, v0, v8

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->b:LX/0hx;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->c:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->z:J

    sub-long/2addr v2, v4

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->v:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2, v3, v1}, LX/0hx;->a(JLandroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2476316
    iput-wide v8, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->z:J

    .line 2476317
    :cond_2
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->v:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v6}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2476318
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->w:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 2476319
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->x:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private b(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2476320
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2476321
    const-string v0, "com.facebook.katana.profile.id"

    const-wide/16 v2, -0x1

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->A:J

    .line 2476322
    iget-wide v2, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->A:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid page id: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->A:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2476323
    const-string v0, "extra_page_data"

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->B:Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;

    .line 2476324
    const-string v0, "extra_is_inside_page_surface_tab"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->J:Z

    .line 2476325
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 2476326
    new-instance v1, LX/HWP;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, p0, v2, v0}, LX/HWP;-><init>(Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;Landroid/content/Context;Landroid/content/IntentFilter;)V

    iput-object v1, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->C:LX/14l;

    .line 2476327
    return-void

    :cond_0
    move v0, v1

    .line 2476328
    goto :goto_0
.end method

.method private k()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2476329
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->G:LX/E8t;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->u:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    .line 2476330
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->F:LX/E8s;

    if-eqz v0, :cond_0

    .line 2476331
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->u:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->F:LX/E8s;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 2476332
    :cond_0
    new-instance v0, LX/E8s;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->G:LX/E8t;

    invoke-direct {v0, v1}, LX/E8s;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->F:LX/E8s;

    .line 2476333
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->F:LX/E8s;

    const/4 v1, 0x1

    .line 2476334
    iput-boolean v1, v0, LX/E8s;->c:Z

    .line 2476335
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->u:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->F:LX/E8s;

    invoke-virtual {v0, v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 2476336
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->G:LX/E8t;

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->F:LX/E8s;

    invoke-virtual {v0, v1}, LX/E8t;->a(LX/E8s;)V

    .line 2476337
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->t:Lcom/facebook/widget/ScrollingAwareScrollView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/ScrollingAwareScrollView;->setBackgroundResource(I)V

    .line 2476338
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->u:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 2476339
    :cond_1
    return-void
.end method

.method public static l(Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;)V
    .locals 5

    .prologue
    .line 2476341
    sget-object v0, LX/HWU;->LOADING:LX/HWU;

    invoke-static {p0, v0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->a$redex0(Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;LX/HWU;)V

    .line 2476342
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->d:LX/1Ck;

    sget-object v1, LX/8Dp;->FETCH_PAGE_INFORMATION_DATA:LX/8Dp;

    new-instance v2, LX/HWR;

    invoke-direct {v2, p0}, LX/HWR;-><init>(Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;)V

    new-instance v3, LX/HWT;

    invoke-direct {v3, p0}, LX/HWT;-><init>(Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2476343
    return-void
.end method


# virtual methods
.method public final E_(I)V
    .locals 4

    .prologue
    .line 2476345
    iget v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->M:I

    if-ltz v0, :cond_1

    iget v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->M:I

    if-le p1, v0, :cond_1

    .line 2476346
    :cond_0
    :goto_0
    return-void

    .line 2476347
    :cond_1
    iput p1, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->M:I

    .line 2476348
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->H:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2476349
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->H:Landroid/view/View;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    iget v3, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->M:I

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public final F_(I)V
    .locals 2

    .prologue
    .line 2476350
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->t:Lcom/facebook/widget/ScrollingAwareScrollView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Lcom/facebook/widget/ScrollingAwareScrollView;->scrollBy(II)V

    .line 2476351
    return-void
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2476344
    const-string v0, "page_information"

    return-object v0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 2476282
    iput p1, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->L:I

    .line 2476283
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->t:Lcom/facebook/widget/ScrollingAwareScrollView;

    iget v1, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->L:I

    invoke-static {v0, v1}, LX/8FX;->a(Landroid/view/ViewGroup;I)V

    .line 2476284
    return-void
.end method

.method public final a(LX/E8t;)V
    .locals 0

    .prologue
    .line 2476285
    iput-object p1, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->G:LX/E8t;

    .line 2476286
    invoke-direct {p0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->k()V

    .line 2476287
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 13

    .prologue
    .line 2476120
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v0}, LX/0hx;->a(LX/0QB;)LX/0hx;

    move-result-object v4

    check-cast v4, LX/0hx;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v5

    check-cast v5, LX/0So;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v6

    check-cast v6, LX/1Ck;

    invoke-static {v0}, LX/0zF;->a(LX/0QB;)LX/0zF;

    move-result-object v7

    check-cast v7, LX/0zG;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v8

    check-cast v8, LX/0kb;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v9

    check-cast v9, LX/0hB;

    const/16 v10, 0x259

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x2b68

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x12c4

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-static {v0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v0

    check-cast v0, LX/0rq;

    iput-object v3, v2, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->a:LX/0tX;

    iput-object v4, v2, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->b:LX/0hx;

    iput-object v5, v2, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->c:LX/0So;

    iput-object v6, v2, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->d:LX/1Ck;

    iput-object v7, v2, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->e:LX/0zG;

    iput-object v8, v2, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->f:LX/0kb;

    iput-object v9, v2, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->g:LX/0hB;

    iput-object v10, v2, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->h:LX/0Ot;

    iput-object v11, v2, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->i:LX/0Ot;

    iput-object v12, v2, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->j:LX/0Ot;

    iput-object v0, v2, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->k:LX/0rq;

    .line 2476121
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2476122
    if-eqz p1, :cond_0

    const-string v0, "com.facebook.katana.profile.id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2476123
    invoke-direct {p0, p1}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->b(Landroid/os/Bundle;)V

    .line 2476124
    :goto_0
    return-void

    .line 2476125
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2476126
    invoke-direct {p0, v0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->b(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;)V
    .locals 0

    .prologue
    .line 2476127
    iput-object p1, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->I:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    .line 2476128
    return-void
.end method

.method public final b()V
    .locals 14

    .prologue
    const/16 v4, 0x8

    const/4 v1, 0x0

    .line 2476129
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->B:Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;

    if-nez v0, :cond_0

    .line 2476130
    :goto_0
    return-void

    .line 2476131
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->B:Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;->r()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2476132
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->B:Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;->r()Ljava/lang/String;

    move-result-object v0

    .line 2476133
    iget-boolean v2, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->J:Z

    if-eqz v2, :cond_14

    .line 2476134
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->B:Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;->l()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->B:Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;->l()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->B:Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;->y()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->B:Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;->y()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2476135
    :cond_3
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->m:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationActionSheet;

    invoke-virtual {v0, v1}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationActionSheet;->setVisibility(I)V

    .line 2476136
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->m:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationActionSheet;

    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->B:Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;

    invoke-virtual {v0, v2}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationActionSheet;->setPageData(Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;)V

    .line 2476137
    :goto_2
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->B:Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;->n()LX/0Px;

    move-result-object v0

    .line 2476138
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;->NOT_PERMANENTLY_CLOSED:Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;

    iget-object v3, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->B:Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;

    invoke-virtual {v3}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;->v()Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/enums/GraphQLPermanentlyClosedStatus;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    if-eqz v0, :cond_5

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    .line 2476139
    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->n:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationHoursCardView;

    invoke-virtual {v2, v1}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationHoursCardView;->setVisibility(I)V

    .line 2476140
    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->n:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationHoursCardView;

    iget-object v3, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->B:Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;

    invoke-virtual {v3}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;->q()Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel$LocationModel;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationHoursCardView;->a(Ljava/util/List;Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel$LocationModel;)V

    .line 2476141
    :goto_3
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->B:Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;->k()LX/2uF;

    move-result-object v0

    .line 2476142
    if-eqz v0, :cond_6

    invoke-virtual {v0}, LX/39O;->a()Z

    move-result v0

    if-nez v0, :cond_6

    .line 2476143
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->o:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;

    invoke-virtual {v0, v1}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->setVisibility(I)V

    .line 2476144
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->o:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;

    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->B:Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;

    invoke-virtual {v0, v2}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->setData(Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;)V

    .line 2476145
    :goto_4
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->B:Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;->w()Lcom/facebook/graphql/enums/GraphQLPlaceType;

    move-result-object v2

    .line 2476146
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->B:Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;->x()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_13

    .line 2476147
    new-instance v0, LX/8A4;

    iget-object v3, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->B:Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;

    invoke-virtual {v3}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;->x()LX/0Px;

    move-result-object v3

    invoke-direct {v0, v3}, LX/8A4;-><init>(Ljava/util/List;)V

    sget-object v3, LX/8A3;->BASIC_ADMIN:LX/8A3;

    invoke-virtual {v0, v3}, LX/8A4;->a(LX/8A3;)Z

    move-result v0

    .line 2476148
    :goto_5
    iget-object v3, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->B:Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;

    invoke-virtual {v3}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;->m()Z

    move-result v3

    if-eqz v3, :cond_7

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPlaceType;->RESIDENCE:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    if-eq v2, v3, :cond_7

    if-nez v0, :cond_7

    .line 2476149
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->p:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationSuggestEditCard;

    invoke-virtual {v0, v1}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationSuggestEditCard;->setVisibility(I)V

    .line 2476150
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->p:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationSuggestEditCard;

    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->B:Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;

    invoke-virtual {v2}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;->o()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->B:Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;

    invoke-virtual {v3}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;->r()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationSuggestEditCard;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2476151
    :goto_6
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->q:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationReportProblemCard;

    iget-wide v2, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->A:J

    invoke-virtual {v0, v2, v3}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationReportProblemCard;->setupOnClickReportBug(J)V

    .line 2476152
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->s:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 2476153
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->B:Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;->t()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_7
    if-ge v2, v4, :cond_8

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel$PageInfoSectionsModel;

    .line 2476154
    iget-object v5, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->E:Landroid/view/LayoutInflater;

    const v6, 0x7f030e84

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationGenericAboutCardView;

    .line 2476155
    invoke-virtual {v5, v0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationGenericAboutCardView;->setSectionData(Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel$PageInfoSectionsModel;)V

    .line 2476156
    iget-object v6, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->s:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2476157
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_7

    .line 2476158
    :cond_4
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->m:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationActionSheet;

    invoke-virtual {v0, v4}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationActionSheet;->setVisibility(I)V

    goto/16 :goto_2

    .line 2476159
    :cond_5
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->n:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationHoursCardView;

    invoke-virtual {v0, v4}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationHoursCardView;->setVisibility(I)V

    goto/16 :goto_3

    .line 2476160
    :cond_6
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->o:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;

    invoke-virtual {v0, v4}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;->setVisibility(I)V

    goto/16 :goto_4

    .line 2476161
    :cond_7
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->p:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationSuggestEditCard;

    invoke-virtual {v0, v4}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationSuggestEditCard;->setVisibility(I)V

    goto :goto_6

    .line 2476162
    :cond_8
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->B:Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;->p()Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->B:Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;->j()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->B:Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 2476163
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->r:Lcom/facebook/pages/fb4a/vertex_attribution/PagesVertexAttributionView;

    invoke-virtual {v0, v1}, Lcom/facebook/pages/fb4a/vertex_attribution/PagesVertexAttributionView;->setVisibility(I)V

    .line 2476164
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->r:Lcom/facebook/pages/fb4a/vertex_attribution/PagesVertexAttributionView;

    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->B:Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;

    invoke-virtual {v2}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;->j()LX/0Px;

    move-result-object v2

    new-instance v3, LX/HWQ;

    invoke-direct {v3, p0}, LX/HWQ;-><init>(Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;)V

    invoke-static {v2, v3}, LX/0R9;->a(Ljava/util/List;LX/0QK;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/pages/fb4a/vertex_attribution/PagesVertexAttributionView;->setAttribtutions(LX/0Px;)V

    .line 2476165
    :cond_9
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->B:Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;->s()Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel$PageFeaturedAdminInfoModel;

    move-result-object v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->B:Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;->s()Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel$PageFeaturedAdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel$PageFeaturedAdminInfoModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->B:Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;->s()Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel$PageFeaturedAdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel$PageFeaturedAdminInfoModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_12

    move v2, v1

    .line 2476166
    :goto_8
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->B:Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;->s()Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel$PageFeaturedAdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel$PageFeaturedAdminInfoModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_11

    .line 2476167
    new-instance v3, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;-><init>(Landroid/content/Context;)V

    .line 2476168
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->B:Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;->s()Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel$PageFeaturedAdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel$PageFeaturedAdminInfoModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel$PageFeaturedAdminInfoModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel$PageFeaturedAdminInfoModel$EdgesModel;->a()Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;

    move-result-object v0

    .line 2476169
    if-eqz v0, :cond_10

    .line 2476170
    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->o()Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel$UserModel;

    move-result-object v5

    if-nez v5, :cond_16

    .line 2476171
    const-string v5, "FBFeaturedAdminInfoFragmentModel.getUser() shouldn\'t be null"

    invoke-static {v3, v5}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;->b(Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;Ljava/lang/String;)V

    .line 2476172
    :cond_a
    :goto_9
    iget-object v5, v3, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->l()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2476173
    const/4 v6, 0x0

    .line 2476174
    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->o()Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel$UserModel;

    move-result-object v5

    if-nez v5, :cond_17

    .line 2476175
    const-string v5, "FBFeaturedAdminInfoFragmentModel.getUser() shouldn\'t be null"

    invoke-static {v3, v5}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;->b(Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;Ljava/lang/String;)V

    .line 2476176
    :cond_b
    :goto_a
    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->k()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_c

    .line 2476177
    iget-object v5, v3, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->k()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2476178
    iget-object v5, v3, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;->h:Lcom/facebook/resources/ui/FbTextView;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2476179
    :cond_c
    const/4 v13, 0x0

    .line 2476180
    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->j()J

    move-result-wide v7

    const-wide/16 v9, 0x0

    cmp-long v7, v7, v9

    if-lez v7, :cond_e

    .line 2476181
    invoke-virtual {v3}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f08183c

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 2476182
    iget-object v8, v3, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;->c:LX/11T;

    .line 2476183
    iget-object v9, v8, LX/11T;->k:Ljava/text/SimpleDateFormat;

    if-nez v9, :cond_d

    .line 2476184
    invoke-virtual {v8}, LX/11T;->b()Ljava/text/DateFormat;

    move-result-object v9

    invoke-virtual {v9}, Ljava/text/DateFormat;->clone()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/text/SimpleDateFormat;

    .line 2476185
    const-string v10, "MMMM yyyy"

    iget-object v11, v8, LX/11T;->a:Ljava/util/Locale;

    invoke-static {v9, v10, v11}, LX/11T;->a(Ljava/text/SimpleDateFormat;Ljava/lang/String;Ljava/util/Locale;)V

    .line 2476186
    iput-object v9, v8, LX/11T;->k:Ljava/text/SimpleDateFormat;

    .line 2476187
    :cond_d
    iget-object v9, v8, LX/11T;->k:Ljava/text/SimpleDateFormat;

    move-object v8, v9

    .line 2476188
    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->j()J

    move-result-wide v9

    const-wide/16 v11, 0x3e8

    mul-long/2addr v9, v11

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 2476189
    iget-object v9, v3, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;->i:Lcom/facebook/resources/ui/FbTextView;

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    aput-object v8, v10, v13

    invoke-static {v7, v10}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v9, v7}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2476190
    iget-object v7, v3, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;->i:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v7, v13}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2476191
    :cond_e
    const/4 v10, 0x0

    .line 2476192
    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->m()I

    move-result v5

    if-lez v5, :cond_f

    .line 2476193
    invoke-virtual {v3}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0f00b6

    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->m()I

    move-result v7

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->m()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 2476194
    iget-object v6, v3, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;->j:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v6, v5}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2476195
    iget-object v5, v3, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;->j:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v5, v10}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2476196
    :cond_f
    const/4 v10, 0x0

    .line 2476197
    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->n()I

    move-result v5

    if-lez v5, :cond_10

    .line 2476198
    invoke-virtual {v3}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0f00b7

    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->n()I

    move-result v7

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->n()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 2476199
    iget-object v6, v3, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;->k:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v6, v5}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2476200
    iget-object v5, v3, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;->k:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v5, v10}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2476201
    :cond_10
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->y:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2476202
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_8

    .line 2476203
    :cond_11
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->y:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2476204
    :cond_12
    sget-object v0, LX/HWU;->LOADED:LX/HWU;

    invoke-static {p0, v0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->a$redex0(Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;LX/HWU;)V

    .line 2476205
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->l:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    goto/16 :goto_0

    :cond_13
    move v0, v1

    goto/16 :goto_5

    .line 2476206
    :cond_14
    const-class v2, LX/1ZF;

    invoke-virtual {p0, v2}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1ZF;

    .line 2476207
    iget-object v3, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->e:LX/0zG;

    invoke-interface {v3}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0h5;

    .line 2476208
    if-eqz v2, :cond_15

    .line 2476209
    invoke-interface {v2, v0}, LX/1ZF;->a_(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2476210
    :cond_15
    if-eqz v3, :cond_1

    .line 2476211
    invoke-interface {v3, v0}, LX/0h5;->setTitle(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2476212
    :cond_16
    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->o()Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel$UserModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel$UserModel;->l()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_a

    .line 2476213
    iget-object v5, v3, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->o()Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel$UserModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel$UserModel;->l()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2476214
    iget-object v5, v3, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->o()Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel$UserModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel$UserModel;->k()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;->a(Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_9

    .line 2476215
    :cond_17
    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->o()Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel$UserModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel$UserModel;->j()LX/1vs;

    move-result-object v5

    iget v5, v5, LX/1vs;->b:I

    if-eqz v5, :cond_19

    .line 2476216
    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->o()Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel$UserModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel$UserModel;->j()LX/1vs;

    move-result-object v5

    iget-object v7, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    .line 2476217
    invoke-virtual {v7, v5, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_18

    const/4 v5, 0x1

    :goto_b
    if-eqz v5, :cond_b

    .line 2476218
    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->o()Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel$UserModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel$UserModel;->j()LX/1vs;

    move-result-object v5

    iget-object v7, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    invoke-virtual {v7, v5, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 2476219
    iget-object v6, v3, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;->g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v7, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;->m:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v6, v5, v7}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2476220
    iget-object v5, v3, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;->g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel;->o()Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel$UserModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$FeaturedAdminInfoFragmentModel$UserModel;->k()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;->a(Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_a

    :cond_18
    move v5, v6

    .line 2476221
    goto :goto_b

    :cond_19
    move v5, v6

    goto :goto_b
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 2476222
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->Q:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 2476223
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->Q:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2476224
    :cond_0
    invoke-static {p0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->l(Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;)V

    .line 2476225
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 2476277
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 2476278
    packed-switch p1, :pswitch_data_0

    .line 2476279
    :cond_0
    :goto_0
    return-void

    .line 2476280
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0817d1

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2476281
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9XE;

    sget-object v1, LX/9XB;->EVENT_SUGGEST_EDIT_SUCCESS:LX/9XB;

    iget-object v2, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->B:Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;

    invoke-virtual {v2}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, LX/9XE;->a(LX/9X2;J)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2776
        :pswitch_0
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x115de45f

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2476226
    iput-object p1, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->E:Landroid/view/LayoutInflater;

    .line 2476227
    const v0, 0x7f030039

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->l:Landroid/view/View;

    .line 2476228
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->l:Landroid/view/View;

    const v2, 0x7f0d03b9

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationActionSheet;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->m:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationActionSheet;

    .line 2476229
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->l:Landroid/view/View;

    const v2, 0x7f0d03b7

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationHoursCardView;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->n:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationHoursCardView;

    .line 2476230
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->l:Landroid/view/View;

    const v2, 0x7f0d03b8

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->o:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationBusinessInfoView;

    .line 2476231
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->l:Landroid/view/View;

    const v2, 0x7f0d03be

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationSuggestEditCard;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->p:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationSuggestEditCard;

    .line 2476232
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->l:Landroid/view/View;

    const v2, 0x7f0d03c0

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationReportProblemCard;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->q:Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationReportProblemCard;

    .line 2476233
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->l:Landroid/view/View;

    const v2, 0x7f0d03b5

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/ScrollingAwareScrollView;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->t:Lcom/facebook/widget/ScrollingAwareScrollView;

    .line 2476234
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->l:Landroid/view/View;

    const v2, 0x7f0d03b6

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->u:Landroid/widget/LinearLayout;

    .line 2476235
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->l:Landroid/view/View;

    const v2, 0x7f0d03ba

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->s:Landroid/widget/LinearLayout;

    .line 2476236
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->l:Landroid/view/View;

    const v2, 0x7f0d03c1

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->v:Landroid/widget/ProgressBar;

    .line 2476237
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->l:Landroid/view/View;

    const v2, 0x7f0d03c2

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->w:Landroid/view/View;

    .line 2476238
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->l:Landroid/view/View;

    const v2, 0x7f0d03c3

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->x:Landroid/view/View;

    .line 2476239
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->l:Landroid/view/View;

    const v2, 0x7f0d03bf

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/fb4a/vertex_attribution/PagesVertexAttributionView;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->r:Lcom/facebook/pages/fb4a/vertex_attribution/PagesVertexAttributionView;

    .line 2476240
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->l:Landroid/view/View;

    const v2, 0x7f0d03c4

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->H:Landroid/view/View;

    .line 2476241
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->l:Landroid/view/View;

    const v2, 0x7f0d03bb

    invoke-static {v0, v2}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->y:Landroid/widget/LinearLayout;

    .line 2476242
    iget v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->M:I

    invoke-virtual {p0, v0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->E_(I)V

    .line 2476243
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->B:Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->Q:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->Q:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2476244
    :cond_0
    invoke-static {p0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->l(Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;)V

    .line 2476245
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->l:Landroid/view/View;

    const v2, 0x4b065758    # 8804184.0f

    invoke-static {v2, v1}, LX/02F;->f(II)V

    return-object v0

    .line 2476246
    :cond_2
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->Q:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->Q:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2476247
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->b()V

    goto :goto_0
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x7f75a365

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2476248
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2476249
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->C:LX/14l;

    .line 2476250
    const/16 v1, 0x2b

    const v2, -0x22f7c9f6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x12276618

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2476251
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2476252
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->d:LX/1Ck;

    if-eqz v1, :cond_0

    .line 2476253
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->d:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2476254
    :cond_0
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->C:LX/14l;

    if-eqz v1, :cond_1

    .line 2476255
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->C:LX/14l;

    invoke-virtual {v1}, LX/14l;->b()V

    .line 2476256
    :cond_1
    const/16 v1, 0x2b

    const v2, 0x45494f1e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x1d5a0378

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2476257
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2476258
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->C:LX/14l;

    if-eqz v1, :cond_0

    .line 2476259
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->C:LX/14l;

    invoke-virtual {v1}, LX/14l;->a()V

    .line 2476260
    :cond_0
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->Q:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->Q:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v1}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2476261
    invoke-static {p0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->l(Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;)V

    .line 2476262
    :cond_1
    const/16 v1, 0x2b

    const v2, 0x560d84a4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2476263
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2476264
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->B:Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;

    if-eqz v0, :cond_0

    .line 2476265
    const-string v0, "com.facebook.katana.profile.id"

    iget-wide v2, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->A:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2476266
    const-string v0, "extra_page_data"

    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->B:Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;

    invoke-static {p1, v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2476267
    :cond_0
    const-string v0, "extra_is_inside_page_surface_tab"

    iget-boolean v1, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->J:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2476268
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2476269
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2476270
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->t:Lcom/facebook/widget/ScrollingAwareScrollView;

    new-instance v1, LX/HWO;

    invoke-direct {v1, p0}, LX/HWO;-><init>(Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/ScrollingAwareScrollView;->a(LX/4oV;)V

    .line 2476271
    iget-boolean v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->J:Z

    if-eqz v0, :cond_0

    .line 2476272
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->t:Lcom/facebook/widget/ScrollingAwareScrollView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/ScrollingAwareScrollView;->setVerticalScrollBarEnabled(Z)V

    .line 2476273
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->u:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 2476274
    :cond_0
    invoke-direct {p0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->k()V

    .line 2476275
    iget v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->L:I

    invoke-virtual {p0, v0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFragment;->a(I)V

    .line 2476276
    return-void
.end method
