.class public Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationGenericAboutCardView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/1Uf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:Landroid/view/LayoutInflater;

.field private final c:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2476397
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationGenericAboutCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2476398
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2476352
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationGenericAboutCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2476353
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2476354
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2476355
    const v0, 0x7f030e8b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2476356
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationGenericAboutCardView;->b:Landroid/view/LayoutInflater;

    .line 2476357
    const v0, 0x7f0d23a3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationGenericAboutCardView;->c:Landroid/widget/LinearLayout;

    .line 2476358
    const-class v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationGenericAboutCardView;

    invoke-static {v0, p0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationGenericAboutCardView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2476359
    return-void
.end method

.method private a()Landroid/view/View;
    .locals 3

    .prologue
    .line 2476360
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationGenericAboutCardView;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f030e88

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 2476361
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2476362
    return-object v0
.end method

.method private a(Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInfoSectionFieldsModel;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2476363
    invoke-virtual {p1}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInfoSectionFieldsModel;->j()LX/0Px;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;->PARAGRAPH:Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2476364
    invoke-direct {p0, p1}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationGenericAboutCardView;->b(Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInfoSectionFieldsModel;)Landroid/view/View;

    move-result-object v0

    .line 2476365
    :goto_0
    return-object v0

    .line 2476366
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInfoSectionFieldsModel;->j()LX/0Px;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;->LINE:Lcom/facebook/graphql/enums/GraphQLPageInfoFieldStyle;

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2476367
    invoke-direct {p0, p1}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationGenericAboutCardView;->c(Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInfoSectionFieldsModel;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 2476368
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationGenericAboutCardView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationGenericAboutCardView;

    invoke-static {v0}, LX/1Uf;->a(LX/0QB;)LX/1Uf;

    move-result-object v0

    check-cast v0, LX/1Uf;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationGenericAboutCardView;->a:LX/1Uf;

    return-void
.end method

.method private b(Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInfoSectionFieldsModel;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2476369
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationGenericAboutCardView;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f030e8a

    invoke-virtual {v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 2476370
    const v0, 0x7f0d23a1

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2476371
    const v1, 0x7f0d23a2

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;

    .line 2476372
    invoke-virtual {p1}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInfoSectionFieldsModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2476373
    invoke-virtual {v1}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->a()V

    .line 2476374
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationGenericAboutCardView;->a:LX/1Uf;

    invoke-virtual {p1}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInfoSectionFieldsModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v3

    invoke-static {v3}, LX/9JZ;->a(LX/175;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-static {v3}, LX/1eD;->c(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/1eE;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v0, v3, v4, v5}, LX/1Uf;->a(LX/1eE;ZLX/0lF;)Landroid/text/Spannable;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/pages/common/ui/widgets/ExpandableTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2476375
    return-object v2
.end method

.method private c(Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInfoSectionFieldsModel;)Landroid/view/View;
    .locals 6

    .prologue
    .line 2476376
    new-instance v0, LX/HWW;

    invoke-virtual {p0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationGenericAboutCardView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/HWW;-><init>(Landroid/content/Context;)V

    .line 2476377
    invoke-virtual {p1}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInfoSectionFieldsModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInfoSectionFieldsModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v2

    invoke-static {v2}, LX/9JZ;->a(LX/175;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    .line 2476378
    iget-object v3, v0, LX/HWW;->i:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->clear()V

    .line 2476379
    iput-object v1, v0, LX/HWW;->f:Ljava/lang/String;

    .line 2476380
    iput-object v2, v0, LX/HWW;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2476381
    iget-object v3, v0, LX/HWW;->b:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2476382
    iget-object v3, v0, LX/HWW;->d:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setVisibility(I)V

    .line 2476383
    iget-object v3, v0, LX/HWW;->c:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setVisibility(I)V

    .line 2476384
    iget-object v3, v0, LX/HWW;->c:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    iget-object v4, v0, LX/HWW;->a:LX/1Uf;

    iget-object v5, v0, LX/HWW;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-static {v5}, LX/1eD;->c(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/1eE;

    move-result-object v5

    const/4 p0, 0x1

    const/4 p1, 0x0

    invoke-virtual {v4, v5, p0, p1}, LX/1Uf;->a(LX/1eE;ZLX/0lF;)Landroid/text/Spannable;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setText(Ljava/lang/CharSequence;)V

    .line 2476385
    iget-object v3, v0, LX/HWW;->c:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    iput-object v3, v0, LX/HWW;->e:Landroid/widget/TextView;

    .line 2476386
    iget-object v3, v0, LX/HWW;->h:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v3

    new-instance v4, LX/HWV;

    invoke-direct {v4, v0}, LX/HWV;-><init>(LX/HWW;)V

    invoke-virtual {v3, v4}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 2476387
    return-object v0
.end method


# virtual methods
.method public setSectionData(Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel$PageInfoSectionsModel;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 2476388
    invoke-virtual {p1}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel$PageInfoSectionsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v0

    move v1, v0

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInfoSectionFieldsModel;

    .line 2476389
    if-lez v1, :cond_0

    .line 2476390
    iget-object v5, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationGenericAboutCardView;->c:Landroid/widget/LinearLayout;

    invoke-direct {p0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationGenericAboutCardView;->a()Landroid/view/View;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2476391
    :cond_0
    invoke-direct {p0, v0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationGenericAboutCardView;->a(Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInfoSectionFieldsModel;)Landroid/view/View;

    move-result-object v0

    .line 2476392
    if-eqz v0, :cond_2

    .line 2476393
    iget-object v5, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationGenericAboutCardView;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2476394
    add-int/lit8 v0, v1, 0x1

    .line 2476395
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 2476396
    :cond_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_1
.end method
