.class public Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationSuggestEditCard;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:Landroid/widget/Button;

.field public b:LX/Bgf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/9XE;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/31f;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2476555
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2476556
    invoke-direct {p0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationSuggestEditCard;->a()V

    .line 2476557
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2476552
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2476553
    invoke-direct {p0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationSuggestEditCard;->a()V

    .line 2476554
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2476541
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2476542
    invoke-direct {p0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationSuggestEditCard;->a()V

    .line 2476543
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2476548
    const-class v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationSuggestEditCard;

    invoke-static {v0, p0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationSuggestEditCard;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2476549
    const v0, 0x7f030e8f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2476550
    const v0, 0x7f0d23aa

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationSuggestEditCard;->a:Landroid/widget/Button;

    .line 2476551
    return-void
.end method

.method private static a(Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationSuggestEditCard;LX/Bgf;LX/03V;Lcom/facebook/content/SecureContextHelper;LX/9XE;LX/31f;)V
    .locals 0

    .prologue
    .line 2476547
    iput-object p1, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationSuggestEditCard;->b:LX/Bgf;

    iput-object p2, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationSuggestEditCard;->c:LX/03V;

    iput-object p3, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationSuggestEditCard;->d:Lcom/facebook/content/SecureContextHelper;

    iput-object p4, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationSuggestEditCard;->e:LX/9XE;

    iput-object p5, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationSuggestEditCard;->f:LX/31f;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationSuggestEditCard;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 6

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationSuggestEditCard;

    invoke-static {v5}, LX/Bgf;->a(LX/0QB;)LX/Bgf;

    move-result-object v1

    check-cast v1, LX/Bgf;

    invoke-static {v5}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-static {v5}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v5}, LX/9XE;->a(LX/0QB;)LX/9XE;

    move-result-object v4

    check-cast v4, LX/9XE;

    invoke-static {v5}, LX/31f;->a(LX/0QB;)LX/31f;

    move-result-object v5

    check-cast v5, LX/31f;

    invoke-static/range {v0 .. v5}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationSuggestEditCard;->a(Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationSuggestEditCard;LX/Bgf;LX/03V;Lcom/facebook/content/SecureContextHelper;LX/9XE;LX/31f;)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2476544
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationSuggestEditCard;->f:LX/31f;

    const-string v1, "android_page_more_information_suggest_edits"

    invoke-static {p1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/31f;->a(Ljava/lang/String;LX/0am;)V

    .line 2476545
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationSuggestEditCard;->a:Landroid/widget/Button;

    new-instance v1, LX/HWY;

    invoke-direct {v1, p0, p1, p2}, LX/HWY;-><init>(Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationSuggestEditCard;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2476546
    return-void
.end method
