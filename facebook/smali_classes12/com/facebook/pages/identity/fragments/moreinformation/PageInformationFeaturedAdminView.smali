.class public Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final m:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/17Y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/11T;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final e:Lcom/facebook/resources/ui/FbTextView;

.field public final f:Lcom/facebook/resources/ui/FbTextView;

.field public final g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final h:Lcom/facebook/resources/ui/FbTextView;

.field public final i:Lcom/facebook/resources/ui/FbTextView;

.field public final j:Lcom/facebook/resources/ui/FbTextView;

.field public final k:Lcom/facebook/resources/ui/FbTextView;

.field private l:Landroid/view/View$OnClickListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2475995
    const-class v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;->m:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2475993
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2475994
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2475991
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2475992
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2475996
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2475997
    const-class v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;

    invoke-static {v0, p0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2475998
    const v0, 0x7f030e83

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2475999
    const v0, 0x7f0d238b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 2476000
    const v0, 0x7f0d238c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;->f:Lcom/facebook/resources/ui/FbTextView;

    .line 2476001
    const v0, 0x7f0d238a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;->g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2476002
    const v0, 0x7f0d238d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;->h:Lcom/facebook/resources/ui/FbTextView;

    .line 2476003
    const v0, 0x7f0d238e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;->i:Lcom/facebook/resources/ui/FbTextView;

    .line 2476004
    const v0, 0x7f0d238f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;->j:Lcom/facebook/resources/ui/FbTextView;

    .line 2476005
    const v0, 0x7f0d2390

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;->k:Lcom/facebook/resources/ui/FbTextView;

    .line 2476006
    return-void
.end method

.method public static a(Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;Ljava/lang/String;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2475988
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;->l:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_0

    .line 2475989
    new-instance v0, LX/HWN;

    invoke-direct {v0, p0, p1}, LX/HWN;-><init>(Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;->l:Landroid/view/View$OnClickListener;

    .line 2475990
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;->l:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v4

    check-cast p1, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;

    invoke-static {v4}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v1

    check-cast v1, LX/17Y;

    invoke-static {v4}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v4}, LX/11T;->a(LX/0QB;)LX/11T;

    move-result-object v3

    check-cast v3, LX/11T;

    const/16 p0, 0x259

    invoke-static {v4, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    iput-object v1, p1, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;->a:LX/17Y;

    iput-object v2, p1, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;->b:Lcom/facebook/content/SecureContextHelper;

    iput-object v3, p1, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;->c:LX/11T;

    iput-object v4, p1, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;->d:LX/0Ot;

    return-void
.end method

.method public static b(Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2475986
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationFeaturedAdminView;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2475987
    return-void
.end method
