.class public Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationActionSheet;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public final a:Landroid/view/View;

.field public final b:Landroid/view/View;

.field public c:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2475843
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationActionSheet;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2475844
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2475841
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationActionSheet;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2475842
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2475845
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2475846
    const v0, 0x7f030e81

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2475847
    const v0, 0x7f0d237d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationActionSheet;->a:Landroid/view/View;

    .line 2475848
    const v0, 0x7f0d237e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationActionSheet;->b:Landroid/view/View;

    .line 2475849
    const-class v0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationActionSheet;

    invoke-static {v0, p0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationActionSheet;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2475850
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationActionSheet;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationActionSheet;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iput-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationActionSheet;->c:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method


# virtual methods
.method public setPageData(Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 2475830
    invoke-virtual {p1}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;->l()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 2475831
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationActionSheet;->a:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2475832
    new-instance v0, LX/HWL;

    invoke-direct {v0, p0, p1}, LX/HWL;-><init>(Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationActionSheet;Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;)V

    .line 2475833
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationActionSheet;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2475834
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;->y()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 2475835
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationActionSheet;->b:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2475836
    new-instance v0, LX/HWM;

    invoke-direct {v0, p0, p1}, LX/HWM;-><init>(Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationActionSheet;Lcom/facebook/pages/identity/protocol/graphql/PageInformationDataGraphQLModels$PageInformationDataModel;)V

    .line 2475837
    iget-object v1, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationActionSheet;->b:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2475838
    :goto_1
    return-void

    .line 2475839
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationActionSheet;->a:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 2475840
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/identity/fragments/moreinformation/PageInformationActionSheet;->b:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method
