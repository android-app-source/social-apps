.class public Lcom/facebook/pages/identity/timeline/PageIdentityPinnedPostGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;

.field private final b:Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

.field private final c:Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;

.field private final d:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

.field private final e:Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/feedplugins/graphqlstory/translation/TranslationOrContentSelectorPartDefinition;

.field private final g:Lcom/facebook/feed/rows/sections/header/components/PinnedPostHeaderComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/header/components/PinnedPostHeaderComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

.field private final i:Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Lcom/facebook/feedplugins/profile/calltoaction/CreateProfileVideoCallToActionComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/profile/calltoaction/CreateProfileVideoCallToActionComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final l:Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;Lcom/facebook/feed/rows/sections/header/components/PinnedPostHeaderComponentPartDefinition;Lcom/facebook/feedplugins/graphqlstory/translation/TranslationOrContentSelectorPartDefinition;Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition;Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;Lcom/facebook/feedplugins/profile/calltoaction/CreateProfileVideoCallToActionComponentPartDefinition;Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2478611
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2478612
    iput-object p8, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPinnedPostGroupPartDefinition;->a:Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;

    .line 2478613
    iput-object p7, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPinnedPostGroupPartDefinition;->b:Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    .line 2478614
    iput-object p6, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPinnedPostGroupPartDefinition;->c:Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;

    .line 2478615
    iput-object p5, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPinnedPostGroupPartDefinition;->d:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    .line 2478616
    iput-object p4, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPinnedPostGroupPartDefinition;->e:Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;

    .line 2478617
    iput-object p3, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPinnedPostGroupPartDefinition;->f:Lcom/facebook/feedplugins/graphqlstory/translation/TranslationOrContentSelectorPartDefinition;

    .line 2478618
    iput-object p2, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPinnedPostGroupPartDefinition;->g:Lcom/facebook/feed/rows/sections/header/components/PinnedPostHeaderComponentPartDefinition;

    .line 2478619
    iput-object p1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPinnedPostGroupPartDefinition;->h:Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

    .line 2478620
    iput-object p9, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPinnedPostGroupPartDefinition;->i:Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition;

    .line 2478621
    iput-object p10, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPinnedPostGroupPartDefinition;->j:Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;

    .line 2478622
    iput-object p11, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPinnedPostGroupPartDefinition;->k:Lcom/facebook/feedplugins/profile/calltoaction/CreateProfileVideoCallToActionComponentPartDefinition;

    .line 2478623
    iput-object p12, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPinnedPostGroupPartDefinition;->l:Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition;

    .line 2478624
    return-void
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2478625
    check-cast p2, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2478626
    invoke-static {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 2478627
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPinnedPostGroupPartDefinition;->h:Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2478628
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPinnedPostGroupPartDefinition;->g:Lcom/facebook/feed/rows/sections/header/components/PinnedPostHeaderComponentPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2478629
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPinnedPostGroupPartDefinition;->f:Lcom/facebook/feedplugins/graphqlstory/translation/TranslationOrContentSelectorPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2478630
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPinnedPostGroupPartDefinition;->e:Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2478631
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPinnedPostGroupPartDefinition;->d:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2478632
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPinnedPostGroupPartDefinition;->i:Lcom/facebook/feedplugins/profile/calltoaction/ProfilePictureOverlayCallToActionComponentPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2478633
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPinnedPostGroupPartDefinition;->j:Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2478634
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPinnedPostGroupPartDefinition;->k:Lcom/facebook/feedplugins/profile/calltoaction/CreateProfileVideoCallToActionComponentPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2478635
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPinnedPostGroupPartDefinition;->l:Lcom/facebook/feedplugins/profile/calltoaction/ProfileGenericCallToActionPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2478636
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPinnedPostGroupPartDefinition;->b:Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2478637
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPinnedPostGroupPartDefinition;->a:Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterPartSelector;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2478638
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPinnedPostGroupPartDefinition;->c:Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2478639
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2478640
    const/4 v0, 0x1

    return v0
.end method
