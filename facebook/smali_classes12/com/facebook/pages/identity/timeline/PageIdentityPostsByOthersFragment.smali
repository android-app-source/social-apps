.class public Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;
.super Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;
.source ""

# interfaces
.implements LX/0fh;


# instance fields
.field private A:LX/1Qq;

.field private B:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

.field public C:LX/0kb;

.field private D:LX/1PF;

.field public E:Landroid/view/View;

.field public F:Lcom/facebook/feed/banner/GenericNotificationBanner;

.field public G:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

.field public H:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/2U1;

.field public h:LX/01T;

.field public i:J

.field public j:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Z

.field public n:Z

.field public o:Z

.field public p:Z

.field public q:LX/0tX;

.field public r:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "LX/8Dp;",
            ">;"
        }
    .end annotation
.end field

.field public s:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/pages/data/graphql/fragments/PostsByOthersFragmentGraphQLModels$PostsByOthersFragmentQueryModel;",
            ">;>;"
        }
    .end annotation
.end field

.field public t:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SI;",
            ">;"
        }
    .end annotation
.end field

.field public u:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3iH;",
            ">;"
        }
    .end annotation
.end field

.field public v:Ljava/util/concurrent/ExecutorService;

.field public w:LX/8EB;

.field private x:LX/0g8;

.field public y:LX/62C;

.field public z:LX/HXN;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 2478971
    invoke-direct {p0}, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;-><init>()V

    .line 2478972
    iput-boolean v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->o:Z

    .line 2478973
    iput-boolean v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->p:Z

    return-void
.end method

.method private I()V
    .locals 4

    .prologue
    .line 2478974
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->r:LX/1Ck;

    sget-object v1, LX/8Dp;->FETCH_PAGE_POSTS_BY_OTHERS_FRAGMENT_DATA:LX/8Dp;

    new-instance v2, LX/HXJ;

    invoke-direct {v2, p0}, LX/HXJ;-><init>(Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;)V

    new-instance v3, LX/HXK;

    invoke-direct {v3, p0}, LX/HXK;-><init>(Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2478975
    return-void
.end method

.method public static J(Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;)V
    .locals 4

    .prologue
    .line 2478976
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->j:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->j:LX/0Px;

    invoke-static {v0}, LX/8A4;->c(LX/0Px;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2478977
    :cond_0
    :goto_0
    return-void

    .line 2478978
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3iH;

    iget-wide v2, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->i:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/HXL;

    invoke-direct {v2, p0}, LX/HXL;-><init>(Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;)V

    iget-object v3, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->v:Ljava/util/concurrent/ExecutorService;

    invoke-virtual {v0, v1, v2, v3}, LX/3iH;->a(Ljava/lang/String;LX/8E8;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    move-object v1, p1

    check-cast v1, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;

    invoke-static {p0}, LX/2U1;->a(LX/0QB;)LX/2U1;

    move-result-object v2

    check-cast v2, LX/2U1;

    invoke-static {p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v3

    check-cast v3, LX/0kb;

    invoke-static {p0}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(LX/0QB;)Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    move-result-object v4

    check-cast v4, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {p0}, LX/8EB;->a(LX/0QB;)LX/8EB;

    move-result-object v6

    check-cast v6, LX/8EB;

    invoke-static {p0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v7

    check-cast v7, LX/01T;

    const/16 v8, 0x259

    invoke-static {p0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v9

    check-cast v9, LX/1Ck;

    const/16 v10, 0x1a1

    invoke-static {p0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0xf07

    invoke-static {p0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object p0

    check-cast p0, Ljava/util/concurrent/ExecutorService;

    iput-object v2, v1, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->g:LX/2U1;

    iput-object v3, v1, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->C:LX/0kb;

    iput-object v4, v1, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->G:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    iput-object v5, v1, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->q:LX/0tX;

    iput-object v6, v1, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->w:LX/8EB;

    iput-object v7, v1, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->h:LX/01T;

    iput-object v8, v1, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->H:LX/0Ot;

    iput-object v9, v1, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->r:LX/1Ck;

    iput-object v10, v1, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->t:LX/0Ot;

    iput-object v11, v1, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->u:LX/0Ot;

    iput-object p0, v1, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->v:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;Lcom/facebook/auth/viewercontext/ViewerContext;)V
    .locals 3
    .param p0    # Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2478979
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->t:LX/0Ot;

    if-eqz v0, :cond_0

    .line 2478980
    if-eqz p1, :cond_1

    .line 2478981
    iget-boolean v0, p1, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v0, v0

    .line 2478982
    if-nez v0, :cond_1

    .line 2478983
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->H:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "PageViewerContext"

    const-string v2, "ViewerContextUtil fetched non-page ViewerContext"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2478984
    :cond_0
    :goto_0
    return-void

    .line 2478985
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->t:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SI;

    invoke-interface {v0, p1}, LX/0SI;->a(Lcom/facebook/auth/viewercontext/ViewerContext;)V

    goto :goto_0
.end method


# virtual methods
.method public final G()V
    .locals 2

    .prologue
    .line 2478986
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->F:Lcom/facebook/feed/banner/GenericNotificationBanner;

    if-eqz v0, :cond_0

    .line 2478987
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->C:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2478988
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->F:Lcom/facebook/feed/banner/GenericNotificationBanner;

    sget-object v1, LX/DBa;->NO_CONNECTION:LX/DBa;

    invoke-virtual {v0, v1}, Lcom/facebook/feed/banner/GenericNotificationBanner;->a(LX/DBa;)V

    .line 2478989
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->lo_()V

    .line 2478990
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->B:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    if-eqz v0, :cond_1

    .line 2478991
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->B:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    invoke-virtual {v0}, LX/62l;->f()V

    .line 2478992
    :cond_1
    return-void

    .line 2478993
    :cond_2
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->F:Lcom/facebook/feed/banner/GenericNotificationBanner;

    invoke-virtual {v0}, LX/4nk;->a()V

    goto :goto_0
.end method

.method public final K()V
    .locals 0

    .prologue
    .line 2478995
    return-void
.end method

.method public final L()V
    .locals 0

    .prologue
    .line 2478994
    return-void
.end method

.method public final M()V
    .locals 0

    .prologue
    .line 2478998
    return-void
.end method

.method public final a(I)I
    .locals 1

    .prologue
    .line 2478999
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->A:LX/1Qq;

    invoke-interface {v0, p1}, LX/1Qr;->h_(I)I

    move-result v0

    return v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2478997
    const-string v0, "pages_posts_by_others_module_name"

    return-object v0
.end method

.method public final a(LX/0ta;Lcom/facebook/timeline/protocol/ResultSource;J)V
    .locals 0

    .prologue
    .line 2478996
    return-void
.end method

.method public final a(LX/Fso;)V
    .locals 4

    .prologue
    .line 2478946
    iget-boolean v0, p1, LX/Fso;->f:Z

    if-eqz v0, :cond_0

    .line 2478947
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->w:LX/8EB;

    .line 2478948
    iget-object v1, v0, LX/8EB;->c:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v3

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    iget-object v1, v0, LX/8EB;->c:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Pq;

    .line 2478949
    iget-object p0, v0, LX/8EB;->a:LX/11i;

    const/4 p1, 0x0

    invoke-interface {p0, v1, p1}, LX/11i;->a(LX/0Pq;LX/0P1;)LX/11o;

    .line 2478950
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2478951
    :cond_0
    return-void
.end method

.method public final a(LX/Fso;LX/0ta;J)V
    .locals 3

    .prologue
    .line 2478952
    invoke-virtual {p0}, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->G()V

    .line 2478953
    iget-boolean v0, p1, LX/Fso;->g:Z

    if-eqz v0, :cond_3

    .line 2478954
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    .line 2478955
    const-string v1, "data_freshness"

    invoke-virtual {p2}, LX/0ta;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2478956
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->w:LX/8EB;

    iget-object v2, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->j:LX/0Px;

    invoke-static {v2}, LX/8A4;->a(LX/0Px;)Z

    move-result v2

    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    .line 2478957
    iget-object p0, v1, LX/8EB;->b:LX/01T;

    sget-object p1, LX/01T;->PAA:LX/01T;

    if-eq p0, p1, :cond_0

    .line 2478958
    if-eqz v2, :cond_2

    .line 2478959
    iget-object p0, v1, LX/8EB;->a:LX/11i;

    sget-object p1, LX/8Dx;->e:LX/8Dt;

    invoke-interface {p0, p1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object p0

    .line 2478960
    if-eqz p0, :cond_0

    .line 2478961
    iget-object p0, v1, LX/8EB;->a:LX/11i;

    sget-object p1, LX/8Dx;->e:LX/8Dt;

    invoke-interface {p0, p1}, LX/11i;->d(LX/0Pq;)V

    .line 2478962
    :cond_0
    :goto_0
    iget-object p0, v1, LX/8EB;->c:LX/0Px;

    invoke-virtual {p0}, LX/0Px;->size()I

    move-result p2

    const/4 p0, 0x0

    move p1, p0

    :goto_1
    if-ge p1, p2, :cond_3

    iget-object p0, v1, LX/8EB;->c:LX/0Px;

    invoke-virtual {p0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/0Pq;

    .line 2478963
    iget-object p3, v1, LX/8EB;->a:LX/11i;

    invoke-interface {p3, p0}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object p3

    .line 2478964
    if-eqz p3, :cond_1

    .line 2478965
    iget-object p3, v1, LX/8EB;->a:LX/11i;

    invoke-interface {p3, p0, v0}, LX/11i;->b(LX/0Pq;LX/0P1;)V

    .line 2478966
    :cond_1
    add-int/lit8 p0, p1, 0x1

    move p1, p0

    goto :goto_1

    .line 2478967
    :cond_2
    iget-object p0, v1, LX/8EB;->a:LX/11i;

    sget-object p1, LX/8Dx;->f:LX/2rx;

    invoke-interface {p0, p1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object p0

    .line 2478968
    if-eqz p0, :cond_0

    .line 2478969
    iget-object p0, v1, LX/8EB;->a:LX/11i;

    sget-object p1, LX/8Dx;->f:LX/2rx;

    invoke-interface {p0, p1}, LX/11i;->d(LX/0Pq;)V

    goto :goto_0

    .line 2478970
    :cond_3
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2478843
    invoke-super {p0, p1}, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->a(Landroid/os/Bundle;)V

    .line 2478844
    const-class v0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;

    invoke-static {v0, p0}, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2478845
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2478846
    const-string v3, "com.facebook.katana.profile.id"

    const-wide/16 v4, -0x1

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->i:J

    .line 2478847
    const-string v3, "extra_page_name"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->k:Ljava/lang/String;

    .line 2478848
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2478849
    iput-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->j:LX/0Px;

    .line 2478850
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->g:LX/2U1;

    iget-wide v4, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->i:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/2U2;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Dk;

    .line 2478851
    if-eqz v0, :cond_1

    .line 2478852
    iget-object v3, v0, LX/8Dk;->a:Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;

    move-object v3, v3

    .line 2478853
    invoke-virtual {v3}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->eD_()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2478854
    invoke-virtual {v3}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->eD_()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->j:LX/0Px;

    .line 2478855
    iput-boolean v2, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->o:Z

    .line 2478856
    invoke-static {p0}, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->J(Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;)V

    .line 2478857
    :cond_0
    invoke-virtual {v3}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->c()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_3

    .line 2478858
    invoke-virtual {v3}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->c()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2478859
    invoke-virtual {v4, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    .line 2478860
    invoke-virtual {v3}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->c()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v3, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->l:Ljava/lang/String;

    .line 2478861
    iput-boolean v2, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->p:Z

    .line 2478862
    :cond_1
    iget-wide v4, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->i:J

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-lez v0, :cond_4

    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Invalid page id: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->i:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2478863
    if-eqz p1, :cond_5

    .line 2478864
    const-string v0, "fragment_uuid"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/ParcelUuid;

    move-object v4, v0

    .line 2478865
    :goto_2
    iget-wide v2, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->i:J

    const-string v5, "others"

    new-instance v6, LX/HXZ;

    new-instance v0, LX/8A4;

    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->j:LX/0Px;

    invoke-direct {v0, v1}, LX/8A4;-><init>(Ljava/util/List;)V

    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->H:LX/0Ot;

    invoke-direct {v6, v0, v1}, LX/HXZ;-><init>(LX/8A4;LX/0Ot;)V

    move-object v1, p0

    invoke-virtual/range {v1 .. v6}, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->a(JLandroid/os/ParcelUuid;Ljava/lang/String;LX/HBQ;)V

    .line 2478866
    return-void

    :cond_2
    move v0, v2

    .line 2478867
    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    move v1, v2

    .line 2478868
    goto :goto_1

    .line 2478869
    :cond_5
    new-instance v4, Landroid/os/ParcelUuid;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-direct {v4, v0}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    goto :goto_2
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2478870
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->E:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2478871
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->y:LX/62C;

    invoke-virtual {v0}, LX/62C;->getCount()I

    move-result v0

    if-nez v0, :cond_3

    .line 2478872
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->E:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2478873
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->E:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->E:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    .line 2478874
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->F:Lcom/facebook/feed/banner/GenericNotificationBanner;

    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->C:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, LX/DBa;->FETCH_PAGE_FAILED:LX/DBa;

    :goto_1
    invoke-virtual {v1, v0}, Lcom/facebook/feed/banner/GenericNotificationBanner;->a(LX/DBa;)V

    .line 2478875
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->B:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    if-eqz v0, :cond_2

    .line 2478876
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->B:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    invoke-virtual {v0}, LX/62l;->f()V

    .line 2478877
    :cond_2
    return-void

    .line 2478878
    :cond_3
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->E:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 2478879
    :cond_4
    sget-object v0, LX/DBa;->NO_CONNECTION:LX/DBa;

    goto :goto_1
.end method

.method public final b(LX/Fso;)V
    .locals 1

    .prologue
    .line 2478880
    iget-boolean v0, p1, LX/Fso;->f:Z

    if-eqz v0, :cond_0

    .line 2478881
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->w:LX/8EB;

    invoke-virtual {v0}, LX/8EB;->b()LX/8EB;

    .line 2478882
    :cond_0
    return-void
.end method

.method public final d()Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 2478883
    new-instance v0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment$4;

    invoke-direct {v0, p0}, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment$4;-><init>(Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;)V

    return-object v0
.end method

.method public final k()LX/0g8;
    .locals 1

    .prologue
    .line 2478884
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->x:LX/0g8;

    return-object v0
.end method

.method public final mJ_()V
    .locals 2

    .prologue
    .line 2478885
    invoke-virtual {p0}, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->t()LX/Frd;

    move-result-object v0

    .line 2478886
    if-eqz v0, :cond_0

    .line 2478887
    invoke-virtual {v0}, LX/Frd;->b()V

    .line 2478888
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/Frd;->a(Z)LX/FsJ;

    move-result-object v1

    .line 2478889
    invoke-virtual {v0, v1}, LX/Frd;->b(LX/FsJ;)V

    .line 2478890
    :cond_0
    return-void
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 2478891
    invoke-super {p0, p1}, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2478892
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->A:LX/1Qq;

    invoke-interface {v0}, LX/1Cw;->notifyDataSetChanged()V

    .line 2478893
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 16

    .prologue
    const/4 v2, 0x2

    const/16 v3, 0x2a

    const v4, -0x4498a3e0

    invoke-static {v2, v3, v4}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v14

    .line 2478894
    const v2, 0x7f0306ac

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v15

    .line 2478895
    const-string v3, "page_timeline"

    move-object/from16 v0, p0

    invoke-static {v15, v3, v0}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(Landroid/view/View;Ljava/lang/String;Landroid/support/v4/app/Fragment;)V

    .line 2478896
    new-instance v3, LX/2iI;

    const v2, 0x7f0d123a

    invoke-virtual {v15, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/widget/listview/BetterListView;

    invoke-direct {v3, v2}, LX/2iI;-><init>(Lcom/facebook/widget/listview/BetterListView;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->x:LX/0g8;

    .line 2478897
    const v2, 0x7f0d08b9

    invoke-virtual {v15, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->E:Landroid/view/View;

    .line 2478898
    const v2, 0x7f0d08bd

    invoke-virtual {v15, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/feed/banner/GenericNotificationBanner;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->F:Lcom/facebook/feed/banner/GenericNotificationBanner;

    .line 2478899
    const v2, 0x7f0d1239

    invoke-virtual {v15, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->B:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    .line 2478900
    new-instance v3, LX/HXN;

    const-string v5, "pages_posts_by_others_module_name"

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->i:J

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->k:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->h:LX/01T;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->l:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->m:Z

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->n:Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->j:LX/0Px;

    invoke-static {v2}, LX/8A4;->c(LX/0Px;)Z

    move-result v13

    move-object/from16 v4, p1

    invoke-direct/range {v3 .. v13}, LX/HXN;-><init>(Landroid/view/LayoutInflater;Ljava/lang/String;JLjava/lang/String;LX/01T;Ljava/lang/String;ZZZ)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->z:LX/HXN;

    .line 2478901
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->l()LX/1Qq;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->A:LX/1Qq;

    .line 2478902
    const/4 v2, 0x2

    new-array v2, v2, [LX/1Cw;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->z:LX/HXN;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->A:LX/1Qq;

    aput-object v4, v2, v3

    invoke-static {v2}, LX/62C;->a([LX/1Cw;)LX/62C;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->y:LX/62C;

    .line 2478903
    invoke-direct/range {p0 .. p0}, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->I()V

    .line 2478904
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->E:Landroid/view/View;

    const v3, 0x7f0d1106

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 2478905
    new-instance v3, LX/HXG;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, LX/HXG;-><init>(Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2478906
    new-instance v2, LX/HXH;

    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, LX/HXH;-><init>(Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->D:LX/1PF;

    .line 2478907
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->B:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    new-instance v3, LX/HXI;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, LX/HXI;-><init>(Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;)V

    invoke-virtual {v2, v3}, LX/62l;->setOnRefreshListener(LX/62n;)V

    .line 2478908
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->m()V

    .line 2478909
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->n()V

    .line 2478910
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/timeline/BaseTimelineFragment;->lp_()V

    .line 2478911
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/timeline/BaseTimelineFragment;->lq_()V

    .line 2478912
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->mJ_()V

    .line 2478913
    const/4 v2, 0x2

    const/16 v3, 0x2b

    const v4, -0x25be7bad

    invoke-static {v2, v3, v4, v14}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v15
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x53b21ec0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2478914
    invoke-super {p0}, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->onDestroy()V

    .line 2478915
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->A:LX/1Qq;

    if-eqz v1, :cond_0

    .line 2478916
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->A:LX/1Qq;

    invoke-interface {v1}, LX/0Vf;->dispose()V

    .line 2478917
    :cond_0
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->B:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    if-eqz v1, :cond_1

    .line 2478918
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->B:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    invoke-virtual {v1}, Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;->destroyDrawingCache()V

    .line 2478919
    :cond_1
    const/16 v1, 0x2b

    const v2, 0x3fbf30c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x38526b6c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2478920
    invoke-super {p0}, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->onPause()V

    .line 2478921
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->r:LX/1Ck;

    if-eqz v1, :cond_0

    .line 2478922
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->r:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2478923
    :cond_0
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->w:LX/8EB;

    invoke-virtual {v1}, LX/8EB;->b()LX/8EB;

    .line 2478924
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->D:LX/1PF;

    if-eqz v1, :cond_1

    .line 2478925
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->D:LX/1PF;

    invoke-virtual {v1}, LX/14l;->b()V

    .line 2478926
    :cond_1
    const/16 v1, 0x2b

    const v2, 0x43d6895b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x6b57811a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2478927
    invoke-super {p0}, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->onResume()V

    .line 2478928
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->D:LX/1PF;

    if-eqz v1, :cond_0

    .line 2478929
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->D:LX/1PF;

    invoke-virtual {v1}, LX/14l;->a()V

    .line 2478930
    :cond_0
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->s:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->s:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v1}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2478931
    invoke-direct {p0}, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->I()V

    .line 2478932
    :cond_1
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->k:Ljava/lang/String;

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2478933
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2478934
    if-eqz v1, :cond_2

    .line 2478935
    const/4 v2, 0x0

    invoke-interface {v1, v2}, LX/1ZF;->setCustomTitle(Landroid/view/View;)V

    .line 2478936
    invoke-interface {v1}, LX/1ZF;->lH_()V

    .line 2478937
    iget-object v2, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->k:Ljava/lang/String;

    invoke-interface {v1, v2}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2478938
    :cond_2
    const/16 v1, 0x2b

    const v2, 0xc0c3c9a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2478939
    invoke-super {p0, p1}, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2478940
    const-string v0, "fragment_uuid"

    invoke-virtual {p0}, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->y()LX/5SB;

    move-result-object v1

    .line 2478941
    iget-object p0, v1, LX/5SB;->d:Landroid/os/ParcelUuid;

    move-object v1, p0

    .line 2478942
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2478943
    return-void
.end method

.method public final v()LX/1OP;
    .locals 1

    .prologue
    .line 2478944
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityPostsByOthersFragment;->y:LX/62C;

    return-object v0
.end method

.method public final z()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2478945
    const-string v0, "pages_identity_ufi"

    return-object v0
.end method
