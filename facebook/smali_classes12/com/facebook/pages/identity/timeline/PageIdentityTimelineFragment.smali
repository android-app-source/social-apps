.class public abstract Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;
.super Lcom/facebook/timeline/BaseTimelineFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements LX/FqO;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/timeline/BaseTimelineFragment",
        "<",
        "LX/62C;",
        ">;",
        "LX/0fh;",
        "LX/FqO;"
    }
.end annotation


# instance fields
.field private A:LX/HBS;

.field private B:LX/HXP;

.field private C:J

.field public D:LX/4BY;

.field private E:LX/0ad;

.field private F:LX/1Kt;

.field private G:LX/1LV;

.field private H:LX/1B2;

.field private I:LX/FuR;

.field private J:LX/0Xl;

.field private K:LX/0Yb;

.field private final L:LX/0YZ;

.field public d:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

.field public e:LX/0Sh;

.field public f:LX/9XE;

.field private g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Db;",
            ">;"
        }
    .end annotation
.end field

.field private h:LX/4mt;

.field private i:LX/03V;

.field public j:LX/0hx;

.field public k:LX/0So;

.field private l:LX/0bH;

.field private m:LX/1AM;

.field public n:LX/0kL;

.field private o:LX/Fre;

.field private p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2dj;",
            ">;"
        }
    .end annotation
.end field

.field private q:Ljava/lang/String;

.field public r:LX/G4x;

.field private s:LX/1DS;

.field private t:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/timeline/feed/parts/TimelineFeedUnitRootPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private u:LX/HXQ;

.field private v:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation
.end field

.field public w:J

.field private x:LX/1B1;

.field private y:LX/1B1;

.field private z:LX/Frd;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2478779
    invoke-direct {p0}, Lcom/facebook/timeline/BaseTimelineFragment;-><init>()V

    .line 2478780
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->w:J

    .line 2478781
    new-instance v0, LX/HXR;

    invoke-direct {v0, p0}, LX/HXR;-><init>(Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;)V

    iput-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->L:LX/0YZ;

    return-void
.end method

.method private static a(Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;LX/4mt;LX/03V;LX/0hx;LX/0So;Lcom/facebook/composer/publish/ComposerPublishServiceHelper;LX/0Sh;LX/0bH;LX/1AM;LX/9XE;LX/0kL;LX/Fre;LX/0Ot;LX/1Kt;LX/1LV;LX/1B2;Ljava/lang/String;LX/G4x;LX/0Or;LX/0Or;LX/1DS;LX/0Ot;LX/HXQ;LX/0Xl;LX/0ad;)V
    .locals 1
    .param p15    # LX/1B2;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p22    # LX/HXQ;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4mt;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0hx;",
            "LX/0So;",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0bH;",
            "LX/1AM;",
            "LX/9XE;",
            "LX/0kL;",
            "LX/Fre;",
            "LX/0Ot",
            "<",
            "LX/2dj;",
            ">;",
            "Lcom/facebook/common/viewport/ViewportMonitor;",
            "LX/1LV;",
            "LX/1B2;",
            "Ljava/lang/String;",
            "LX/G4x;",
            "LX/0Or",
            "<",
            "LX/1Db;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;",
            "LX/1DS;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/timeline/feed/parts/TimelineFeedUnitRootPartDefinition;",
            ">;",
            "LX/HXQ;",
            "LX/0Xl;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2478782
    iput-object p1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->h:LX/4mt;

    .line 2478783
    iput-object p2, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->i:LX/03V;

    .line 2478784
    iput-object p3, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->j:LX/0hx;

    .line 2478785
    iput-object p4, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->k:LX/0So;

    .line 2478786
    iput-object p5, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->d:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    .line 2478787
    iput-object p6, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->e:LX/0Sh;

    .line 2478788
    iput-object p7, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->l:LX/0bH;

    .line 2478789
    iput-object p8, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->m:LX/1AM;

    .line 2478790
    iput-object p9, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->f:LX/9XE;

    .line 2478791
    iput-object p10, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->n:LX/0kL;

    .line 2478792
    iput-object p11, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->o:LX/Fre;

    .line 2478793
    iput-object p12, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->p:LX/0Ot;

    .line 2478794
    iput-object p13, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->F:LX/1Kt;

    .line 2478795
    iput-object p14, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->G:LX/1LV;

    .line 2478796
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->q:Ljava/lang/String;

    .line 2478797
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->r:LX/G4x;

    .line 2478798
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->v:LX/0Or;

    .line 2478799
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->s:LX/1DS;

    .line 2478800
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->t:LX/0Ot;

    .line 2478801
    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->u:LX/HXQ;

    .line 2478802
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->g:LX/0Or;

    .line 2478803
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->H:LX/1B2;

    .line 2478804
    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->J:LX/0Xl;

    .line 2478805
    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->E:LX/0ad;

    .line 2478806
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 27

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v26

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;

    invoke-static/range {v26 .. v26}, LX/4mt;->a(LX/0QB;)LX/4mt;

    move-result-object v3

    check-cast v3, LX/4mt;

    invoke-static/range {v26 .. v26}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static/range {v26 .. v26}, LX/0hx;->a(LX/0QB;)LX/0hx;

    move-result-object v5

    check-cast v5, LX/0hx;

    invoke-static/range {v26 .. v26}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v6

    check-cast v6, LX/0So;

    invoke-static/range {v26 .. v26}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(LX/0QB;)Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    move-result-object v7

    check-cast v7, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-static/range {v26 .. v26}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v8

    check-cast v8, LX/0Sh;

    invoke-static/range {v26 .. v26}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v9

    check-cast v9, LX/0bH;

    invoke-static/range {v26 .. v26}, LX/1AM;->a(LX/0QB;)LX/1AM;

    move-result-object v10

    check-cast v10, LX/1AM;

    invoke-static/range {v26 .. v26}, LX/9XE;->a(LX/0QB;)LX/9XE;

    move-result-object v11

    check-cast v11, LX/9XE;

    invoke-static/range {v26 .. v26}, LX/0kL;->a(LX/0QB;)LX/0kL;

    move-result-object v12

    check-cast v12, LX/0kL;

    const-class v13, LX/Fre;

    move-object/from16 v0, v26

    invoke-interface {v0, v13}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v13

    check-cast v13, LX/Fre;

    const/16 v14, 0xa71

    move-object/from16 v0, v26

    invoke-static {v0, v14}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v14

    invoke-static/range {v26 .. v26}, LX/23N;->a(LX/0QB;)LX/23N;

    move-result-object v15

    check-cast v15, LX/1Kt;

    invoke-static/range {v26 .. v26}, LX/1LV;->a(LX/0QB;)LX/1LV;

    move-result-object v16

    check-cast v16, LX/1LV;

    invoke-static/range {v26 .. v26}, LX/1B2;->a(LX/0QB;)LX/1B2;

    move-result-object v17

    check-cast v17, LX/1B2;

    invoke-static/range {v26 .. v26}, LX/0dG;->a(LX/0QB;)Ljava/lang/String;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    invoke-static/range {v26 .. v26}, LX/G4x;->a(LX/0QB;)LX/G4x;

    move-result-object v19

    check-cast v19, LX/G4x;

    const/16 v20, 0x6c9

    move-object/from16 v0, v26

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v20

    const/16 v21, 0xac0

    move-object/from16 v0, v26

    move/from16 v1, v21

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v21

    invoke-static/range {v26 .. v26}, LX/1DS;->a(LX/0QB;)LX/1DS;

    move-result-object v22

    check-cast v22, LX/1DS;

    const/16 v23, 0x3654

    move-object/from16 v0, v26

    move/from16 v1, v23

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v23

    const-class v24, LX/HXQ;

    move-object/from16 v0, v26

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v24

    check-cast v24, LX/HXQ;

    invoke-static/range {v26 .. v26}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v25

    check-cast v25, LX/0Xl;

    invoke-static/range {v26 .. v26}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v26

    check-cast v26, LX/0ad;

    invoke-static/range {v2 .. v26}, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->a(Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;LX/4mt;LX/03V;LX/0hx;LX/0So;Lcom/facebook/composer/publish/ComposerPublishServiceHelper;LX/0Sh;LX/0bH;LX/1AM;LX/9XE;LX/0kL;LX/Fre;LX/0Ot;LX/1Kt;LX/1LV;LX/1B2;Ljava/lang/String;LX/G4x;LX/0Or;LX/0Or;LX/1DS;LX/0Ot;LX/HXQ;LX/0Xl;LX/0ad;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;LX/9X2;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 2478807
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->f:LX/9XE;

    invoke-virtual {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->a()Ljava/lang/String;

    move-result-object v2

    iget-wide v4, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->C:J

    move-object v3, p1

    move-object v6, p2

    invoke-virtual/range {v1 .. v6}, LX/9XE;->a(Ljava/lang/String;LX/9X2;JLjava/lang/String;)V

    .line 2478808
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2478809
    const-string v0, "pages_native_timeline"

    return-object v0
.end method

.method public final a(JLandroid/os/ParcelUuid;Ljava/lang/String;LX/HBQ;)V
    .locals 9

    .prologue
    .line 2478810
    iput-wide p1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->C:J

    .line 2478811
    :try_start_0
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->q:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 2478812
    :goto_0
    iget-wide v2, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->C:J

    const/4 v4, 0x0

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    invoke-static/range {v0 .. v7}, LX/HBS;->a(JJLjava/lang/String;Landroid/os/ParcelUuid;Ljava/lang/String;LX/HBQ;)LX/HBS;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->A:LX/HBS;

    .line 2478813
    new-instance v0, LX/FuR;

    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->A:LX/HBS;

    invoke-direct {v0, v1}, LX/FuR;-><init>(LX/5SB;)V

    iput-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->I:LX/FuR;

    .line 2478814
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->u:LX/HXQ;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->I:LX/FuR;

    iget-object v3, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->A:LX/HBS;

    invoke-virtual {p0}, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->d()Ljava/lang/Runnable;

    move-result-object v4

    .line 2478815
    new-instance v5, LX/HXT;

    invoke-direct {v5, p0}, LX/HXT;-><init>(Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;)V

    move-object v5, v5

    .line 2478816
    invoke-virtual/range {v0 .. v5}, LX/HXQ;->a(Landroid/content/Context;LX/1PT;LX/5SB;Ljava/lang/Runnable;LX/1PY;)LX/HXP;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->B:LX/HXP;

    .line 2478817
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 2478818
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->o:LX/Fre;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_1
    iget-object v3, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->A:LX/HBS;

    sget-object v4, LX/G11;->PAGE:LX/G11;

    iget-object v5, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->r:LX/G4x;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->a()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v8

    move-object v2, p0

    invoke-virtual/range {v0 .. v8}, LX/Fre;->a(Landroid/content/Context;LX/FqO;LX/5SB;LX/G11;LX/G4x;LX/G4m;LX/G1N;Lcom/facebook/common/callercontext/CallerContext;)LX/Frd;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->z:LX/Frd;

    .line 2478819
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/BaseTimelineFragment;->a(LX/G4e;)V

    .line 2478820
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->J:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.STREAM_PUBLISH_COMPLETE"

    iget-object v2, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->L:LX/0YZ;

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->K:LX/0Yb;

    .line 2478821
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->K:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 2478822
    return-void

    .line 2478823
    :catch_0
    const-wide/16 v0, -0x1

    .line 2478824
    iget-object v2, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->i:LX/03V;

    const-string v3, "page_timeline_invalid_meuser"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "logged in user: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->q:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2478825
    :cond_0
    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    goto :goto_1
.end method

.method public final a(LX/0g8;I)V
    .locals 1

    .prologue
    .line 2478826
    invoke-super {p0, p1, p2}, Lcom/facebook/timeline/BaseTimelineFragment;->a(LX/0g8;I)V

    .line 2478827
    packed-switch p2, :pswitch_data_0

    .line 2478828
    :goto_0
    return-void

    .line 2478829
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->F:LX/1Kt;

    invoke-virtual {v0, p1}, LX/1Kt;->b(LX/0g8;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(LX/0g8;III)V
    .locals 1

    .prologue
    .line 2478830
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/timeline/BaseTimelineFragment;->a(LX/0g8;III)V

    .line 2478831
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->F:LX/1Kt;

    invoke-interface {v0, p1, p2, p3, p4}, LX/0fx;->a(LX/0g8;III)V

    .line 2478832
    return-void
.end method

.method public a(LX/Fso;)V
    .locals 0

    .prologue
    .line 2478833
    return-void
.end method

.method public a(LX/Fso;LX/0ta;J)V
    .locals 6

    .prologue
    .line 2478834
    invoke-virtual {p0}, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->G()V

    .line 2478835
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->f:LX/9XE;

    invoke-virtual {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->a()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/9XB;->EVENT_SECTION_LOADED:LX/9XB;

    iget-wide v4, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->C:J

    invoke-virtual {v0, v1, v2, v4, v5}, LX/9XE;->a(Ljava/lang/String;LX/9X2;J)V

    .line 2478836
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2478837
    invoke-super {p0, p1}, Lcom/facebook/timeline/BaseTimelineFragment;->a(Landroid/os/Bundle;)V

    .line 2478838
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2478839
    new-instance v0, LX/1B1;

    invoke-direct {v0}, LX/1B1;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->x:LX/1B1;

    .line 2478840
    new-instance v0, LX/1B1;

    invoke-direct {v0}, LX/1B1;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->y:LX/1B1;

    .line 2478841
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->F:LX/1Kt;

    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->G:LX/1LV;

    invoke-virtual {v0, v1}, LX/1Kt;->a(LX/1Ce;)V

    .line 2478842
    return-void
.end method

.method public abstract b()V
.end method

.method public b(LX/Fso;)V
    .locals 6

    .prologue
    .line 2478776
    invoke-virtual {p0}, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->b()V

    .line 2478777
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->f:LX/9XE;

    invoke-virtual {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->a()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/9XA;->EVENT_SECTION_LOAD_ERROR:LX/9XA;

    iget-wide v4, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->C:J

    invoke-virtual {v0, v1, v2, v4, v5}, LX/9XE;->a(Ljava/lang/String;LX/9X2;J)V

    .line 2478778
    return-void
.end method

.method public abstract d()Ljava/lang/Runnable;
.end method

.method public final l()LX/1Qq;
    .locals 3

    .prologue
    .line 2478703
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->s:LX/1DS;

    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->t:LX/0Ot;

    iget-object v2, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->r:LX/G4x;

    invoke-virtual {v0, v1, v2}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->B:LX/HXP;

    .line 2478704
    iput-object v1, v0, LX/1Ql;->f:LX/1PW;

    .line 2478705
    move-object v0, v0

    .line 2478706
    invoke-virtual {v0}, LX/1Ql;->e()LX/1Qq;

    move-result-object v0

    return-object v0
.end method

.method public final m()V
    .locals 2

    .prologue
    .line 2478707
    invoke-virtual {p0}, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->k()LX/0g8;

    move-result-object v1

    .line 2478708
    invoke-virtual {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->v()LX/1OP;

    move-result-object v0

    check-cast v0, Landroid/widget/ListAdapter;

    invoke-interface {v1, v0}, LX/0g8;->a(Landroid/widget/ListAdapter;)V

    .line 2478709
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Db;

    invoke-virtual {v0}, LX/1Db;->a()LX/1St;

    move-result-object v0

    invoke-interface {v1, v0}, LX/0g8;->a(LX/1St;)V

    .line 2478710
    invoke-interface {v1, p0}, LX/0g8;->b(LX/0fx;)V

    .line 2478711
    invoke-virtual {p0}, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->n()V

    .line 2478712
    invoke-virtual {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->lp_()V

    .line 2478713
    invoke-virtual {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->lq_()V

    .line 2478714
    return-void
.end method

.method public final n()V
    .locals 2

    .prologue
    .line 2478715
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->x:LX/1B1;

    new-instance v1, LX/HXU;

    invoke-direct {v1, p0}, LX/HXU;-><init>(Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2478716
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->x:LX/1B1;

    new-instance v1, LX/HXV;

    invoke-direct {v1, p0}, LX/HXV;-><init>(Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2478717
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->x:LX/1B1;

    new-instance v1, LX/HXW;

    invoke-direct {v1, p0}, LX/HXW;-><init>(Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2478718
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->y:LX/1B1;

    new-instance v1, LX/HXX;

    invoke-direct {v1, p0}, LX/HXX;-><init>(Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2478719
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->y:LX/1B1;

    new-instance v1, LX/HXY;

    invoke-direct {v1, p0}, LX/HXY;-><init>(Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2478720
    return-void
.end method

.method public final o()LX/G4x;
    .locals 1

    .prologue
    .line 2478721
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->r:LX/G4x;

    return-object v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    .line 2478722
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/timeline/BaseTimelineFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2478723
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 2478724
    :goto_0
    return-void

    .line 2478725
    :cond_0
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 2478726
    :pswitch_1
    const/4 v4, 0x1

    .line 2478727
    const-string v1, "is_uploading_media"

    const/4 v2, 0x0

    invoke-virtual {p3, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2478728
    :goto_1
    goto :goto_0

    .line 2478729
    :cond_1
    const/16 v1, 0x6de

    if-ne p1, v1, :cond_2

    const v1, 0x7f081413

    .line 2478730
    :goto_2
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0817c1

    invoke-virtual {p0, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v3, v1, v4}, LX/4BY;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)LX/4BY;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->D:LX/4BY;

    .line 2478731
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->k:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->w:J

    .line 2478732
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->j:LX/0hx;

    invoke-virtual {v1, v4}, LX/0hx;->a(Z)V

    .line 2478733
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->d:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-virtual {v1, p3}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_1

    .line 2478734
    :cond_2
    const v1, 0x7f081412

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x6dc
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2a

    const v1, 0x3dfa2c5

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2478735
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->z:LX/Frd;

    if-eqz v1, :cond_0

    .line 2478736
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->z:LX/Frd;

    invoke-virtual {v1}, LX/Frd;->b()V

    .line 2478737
    :cond_0
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->h:LX/4mt;

    if-eqz v1, :cond_1

    .line 2478738
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->h:LX/4mt;

    invoke-virtual {v1}, LX/4mt;->a()V

    .line 2478739
    :cond_1
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->y:LX/1B1;

    if-eqz v1, :cond_2

    .line 2478740
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->y:LX/1B1;

    iget-object v2, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->m:LX/1AM;

    invoke-virtual {v1, v2}, LX/1B1;->b(LX/0b4;)V

    .line 2478741
    :cond_2
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->x:LX/1B1;

    if-eqz v1, :cond_3

    .line 2478742
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->x:LX/1B1;

    iget-object v2, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->l:LX/0bH;

    invoke-virtual {v1, v2}, LX/1B1;->b(LX/0b4;)V

    .line 2478743
    :cond_3
    iput-object v3, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->z:LX/Frd;

    .line 2478744
    iput-object v3, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->i:LX/03V;

    .line 2478745
    iput-object v3, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->h:LX/4mt;

    .line 2478746
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->K:LX/0Yb;

    if-eqz v1, :cond_4

    .line 2478747
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->K:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 2478748
    :cond_4
    invoke-super {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->onDestroy()V

    .line 2478749
    const/16 v1, 0x2b

    const v2, -0x73b093f0

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x290824ca

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2478750
    invoke-super {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->onPause()V

    .line 2478751
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->h:LX/4mt;

    if-eqz v1, :cond_0

    .line 2478752
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->h:LX/4mt;

    invoke-virtual {v1}, LX/4mt;->a()V

    .line 2478753
    :cond_0
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->y:LX/1B1;

    if-eqz v1, :cond_1

    .line 2478754
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->y:LX/1B1;

    iget-object v2, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->m:LX/1AM;

    invoke-virtual {v1, v2}, LX/1B1;->b(LX/0b4;)V

    .line 2478755
    :cond_1
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->x:LX/1B1;

    if-eqz v1, :cond_2

    .line 2478756
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->x:LX/1B1;

    iget-object v2, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->l:LX/0bH;

    invoke-virtual {v1, v2}, LX/1B1;->b(LX/0b4;)V

    .line 2478757
    :cond_2
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->F:LX/1Kt;

    invoke-virtual {p0}, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->k()LX/0g8;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1Kt;->c(LX/0g8;)V

    .line 2478758
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->G:LX/1LV;

    const-string v2, "unknown"

    invoke-virtual {v1, v2}, LX/1LV;->a(Ljava/lang/String;)V

    .line 2478759
    const/16 v1, 0x2b

    const v2, 0x309ea214

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x1b5db90d

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2478760
    invoke-super {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->onResume()V

    .line 2478761
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->y:LX/1B1;

    if-eqz v0, :cond_0

    .line 2478762
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->y:LX/1B1;

    iget-object v2, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->m:LX/1AM;

    invoke-virtual {v0, v2}, LX/1B1;->a(LX/0b4;)V

    .line 2478763
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->x:LX/1B1;

    if-eqz v0, :cond_1

    .line 2478764
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->x:LX/1B1;

    iget-object v2, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->l:LX/0bH;

    invoke-virtual {v0, v2}, LX/1B1;->a(LX/0b4;)V

    .line 2478765
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->v:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    sget v2, LX/FQe;->d:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v0

    .line 2478766
    iget-object v2, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->F:LX/1Kt;

    invoke-virtual {p0}, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->k()LX/0g8;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, LX/1Kt;->a(ZLX/0g8;)V

    .line 2478767
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->G:LX/1LV;

    const-string v2, "pages_native_timeline"

    invoke-virtual {v0, v2}, LX/1LV;->a(Ljava/lang/String;)V

    .line 2478768
    const/16 v0, 0x2b

    const v2, -0x4654c758

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x195f3310

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2478769
    invoke-super {p0}, Lcom/facebook/timeline/BaseTimelineFragment;->onStop()V

    .line 2478770
    iget-object v1, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->z:LX/Frd;

    invoke-virtual {v1}, LX/Frd;->c()V

    .line 2478771
    const/16 v1, 0x2b

    const v2, 0x3ede5858

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final t()LX/Frd;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2478772
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->z:LX/Frd;

    return-object v0
.end method

.method public final w()LX/1Kt;
    .locals 1

    .prologue
    .line 2478773
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->F:LX/1Kt;

    return-object v0
.end method

.method public final x()LX/2dj;
    .locals 1

    .prologue
    .line 2478774
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2dj;

    return-object v0
.end method

.method public final y()LX/5SB;
    .locals 1

    .prologue
    .line 2478775
    iget-object v0, p0, Lcom/facebook/pages/identity/timeline/PageIdentityTimelineFragment;->A:LX/HBS;

    return-object v0
.end method
