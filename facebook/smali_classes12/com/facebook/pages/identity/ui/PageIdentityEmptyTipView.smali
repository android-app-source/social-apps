.class public Lcom/facebook/pages/identity/ui/PageIdentityEmptyTipView;
.super Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/intent/feed/IFeedIntentBuilder;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2479457
    invoke-direct {p0, p1}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2479458
    invoke-direct {p0}, Lcom/facebook/pages/identity/ui/PageIdentityEmptyTipView;->a()V

    .line 2479459
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2479454
    invoke-direct {p0, p1, p2}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2479455
    invoke-direct {p0}, Lcom/facebook/pages/identity/ui/PageIdentityEmptyTipView;->a()V

    .line 2479456
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2479451
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2479452
    invoke-direct {p0}, Lcom/facebook/pages/identity/ui/PageIdentityEmptyTipView;->a()V

    .line 2479453
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2479449
    const-class v0, Lcom/facebook/pages/identity/ui/PageIdentityEmptyTipView;

    invoke-static {v0, p0}, Lcom/facebook/pages/identity/ui/PageIdentityEmptyTipView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2479450
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/identity/ui/PageIdentityEmptyTipView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/pages/identity/ui/PageIdentityEmptyTipView;

    invoke-static {v0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v0

    check-cast v0, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iput-object v0, p0, Lcom/facebook/pages/identity/ui/PageIdentityEmptyTipView;->a:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    return-void
.end method


# virtual methods
.method public final onFinishInflate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x21f606b6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2479446
    const v0, 0x7f0d2335

    invoke-virtual {p0, v0}, Lcom/facebook/pages/identity/ui/PageIdentityEmptyTipView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2479447
    new-instance v2, LX/HXr;

    invoke-direct {v2, p0}, LX/HXr;-><init>(Lcom/facebook/pages/identity/ui/PageIdentityEmptyTipView;)V

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2479448
    const/16 v0, 0x2d

    const v2, 0x47d8b1eb

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
