.class public Lcom/facebook/pages/identity/ui/PageIdentityPublisher;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/1ay;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1Kf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/CSL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/9XE;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:J

.field public f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2479488
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2479489
    invoke-direct {p0}, Lcom/facebook/pages/identity/ui/PageIdentityPublisher;->a()V

    .line 2479490
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2479480
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2479481
    invoke-direct {p0}, Lcom/facebook/pages/identity/ui/PageIdentityPublisher;->a()V

    .line 2479482
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2479491
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2479492
    invoke-direct {p0}, Lcom/facebook/pages/identity/ui/PageIdentityPublisher;->a()V

    .line 2479493
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2479483
    const-class v0, Lcom/facebook/pages/identity/ui/PageIdentityPublisher;

    invoke-static {v0, p0}, Lcom/facebook/pages/identity/ui/PageIdentityPublisher;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2479484
    const v0, 0x7f030e72

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2479485
    return-void
.end method

.method private a(Landroid/view/View;Ljava/lang/Runnable;LX/9X2;)V
    .locals 1

    .prologue
    .line 2479486
    new-instance v0, LX/HXs;

    invoke-direct {v0, p0, p3, p2}, LX/HXs;-><init>(Lcom/facebook/pages/identity/ui/PageIdentityPublisher;LX/9X2;Ljava/lang/Runnable;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2479487
    return-void
.end method

.method private static a(Lcom/facebook/pages/identity/ui/PageIdentityPublisher;LX/1ay;LX/1Kf;LX/CSL;LX/9XE;)V
    .locals 0

    .prologue
    .line 2479472
    iput-object p1, p0, Lcom/facebook/pages/identity/ui/PageIdentityPublisher;->a:LX/1ay;

    iput-object p2, p0, Lcom/facebook/pages/identity/ui/PageIdentityPublisher;->b:LX/1Kf;

    iput-object p3, p0, Lcom/facebook/pages/identity/ui/PageIdentityPublisher;->c:LX/CSL;

    iput-object p4, p0, Lcom/facebook/pages/identity/ui/PageIdentityPublisher;->d:LX/9XE;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/identity/ui/PageIdentityPublisher;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/pages/identity/ui/PageIdentityPublisher;

    invoke-static {v3}, LX/1ay;->b(LX/0QB;)LX/1ay;

    move-result-object v0

    check-cast v0, LX/1ay;

    invoke-static {v3}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v1

    check-cast v1, LX/1Kf;

    invoke-static {v3}, LX/HSx;->b(LX/0QB;)LX/HSx;

    move-result-object v2

    check-cast v2, LX/CSL;

    invoke-static {v3}, LX/9XE;->a(LX/0QB;)LX/9XE;

    move-result-object v3

    check-cast v3, LX/9XE;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/pages/identity/ui/PageIdentityPublisher;->a(Lcom/facebook/pages/identity/ui/PageIdentityPublisher;LX/1ay;LX/1Kf;LX/CSL;LX/9XE;)V

    return-void
.end method


# virtual methods
.method public final a(JLjava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 9

    .prologue
    .line 2479473
    iput-wide p1, p0, Lcom/facebook/pages/identity/ui/PageIdentityPublisher;->e:J

    .line 2479474
    iput-object p6, p0, Lcom/facebook/pages/identity/ui/PageIdentityPublisher;->f:Ljava/lang/String;

    .line 2479475
    if-eqz p5, :cond_0

    .line 2479476
    const v0, 0x7f0d235a

    invoke-virtual {p0, v0}, Lcom/facebook/pages/identity/ui/PageIdentityPublisher;->findViewById(I)Landroid/view/View;

    move-result-object v6

    new-instance v0, Lcom/facebook/pages/identity/ui/PageIdentityPublisher$1;

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/facebook/pages/identity/ui/PageIdentityPublisher$1;-><init>(Lcom/facebook/pages/identity/ui/PageIdentityPublisher;JLjava/lang/String;Ljava/lang/String;)V

    sget-object v1, LX/9X3;->EVENT_NON_ADMIN_SHARE_PHOTO:LX/9X3;

    invoke-direct {p0, v6, v0, v1}, Lcom/facebook/pages/identity/ui/PageIdentityPublisher;->a(Landroid/view/View;Ljava/lang/Runnable;LX/9X2;)V

    .line 2479477
    :goto_0
    const v0, 0x7f0d2359

    invoke-virtual {p0, v0}, Lcom/facebook/pages/identity/ui/PageIdentityPublisher;->findViewById(I)Landroid/view/View;

    move-result-object v7

    new-instance v0, Lcom/facebook/pages/identity/ui/PageIdentityPublisher$2;

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/facebook/pages/identity/ui/PageIdentityPublisher$2;-><init>(Lcom/facebook/pages/identity/ui/PageIdentityPublisher;JLjava/lang/String;Ljava/lang/String;Z)V

    sget-object v1, LX/9X3;->EVENT_NON_ADMIN_WRITE_POST:LX/9X3;

    invoke-direct {p0, v7, v0, v1}, Lcom/facebook/pages/identity/ui/PageIdentityPublisher;->a(Landroid/view/View;Ljava/lang/Runnable;LX/9X2;)V

    .line 2479478
    return-void

    .line 2479479
    :cond_0
    const v0, 0x7f0d235a

    invoke-virtual {p0, v0}, Lcom/facebook/pages/identity/ui/PageIdentityPublisher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
