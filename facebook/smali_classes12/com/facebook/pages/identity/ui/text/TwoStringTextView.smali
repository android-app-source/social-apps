.class public Lcom/facebook/pages/identity/ui/text/TwoStringTextView;
.super Lcom/facebook/resources/ui/FbTextView;
.source ""


# instance fields
.field private a:Ljava/lang/CharSequence;

.field private b:Ljava/lang/CharSequence;

.field private c:Ljava/lang/CharSequence;

.field private d:Ljava/lang/CharSequence;

.field private e:Z

.field private f:F

.field private g:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2479494
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2479495
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2479541
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2479542
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2479536
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2479537
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;->e:Z

    .line 2479538
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;->f:F

    .line 2479539
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;->g:F

    .line 2479540
    return-void
.end method

.method private a(Ljava/lang/CharSequence;Landroid/text/TextPaint;IF)I
    .locals 8

    .prologue
    .line 2479533
    invoke-virtual {p2, p4}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 2479534
    new-instance v0, Landroid/text/StaticLayout;

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    iget v5, p0, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;->f:F

    iget v6, p0, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;->g:F

    const/4 v7, 0x1

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 2479535
    invoke-virtual {v0}, Landroid/text/StaticLayout;->getLineCount()I

    move-result v0

    return v0
.end method

.method private a(II)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    .line 2479522
    iget-object v0, p0, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;->a:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;->b:Ljava/lang/CharSequence;

    if-nez v0, :cond_1

    .line 2479523
    :cond_0
    :goto_0
    return-void

    .line 2479524
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 2479525
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-eqz v1, :cond_0

    if-lez p2, :cond_0

    if-lez p1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;->getTextSize()F

    move-result v1

    float-to-double v2, v1

    const-wide v4, 0x3ee4f8b588e368f1L    # 1.0E-5

    cmpg-double v1, v2, v4

    if-ltz v1, :cond_0

    .line 2479526
    invoke-virtual {p0}, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    .line 2479527
    invoke-virtual {p0}, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;->getTextSize()F

    move-result v2

    invoke-direct {p0, v0, v1, p1, v2}, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;IF)I

    move-result v0

    .line 2479528
    const/4 v2, 0x1

    if-le v0, v2, :cond_3

    .line 2479529
    iget-object v2, p0, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;->a:Ljava/lang/CharSequence;

    invoke-virtual {p0}, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;->getTextSize()F

    move-result v3

    invoke-direct {p0, v2, v1, p1, v3}, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;IF)I

    move-result v1

    .line 2479530
    if-ne v1, v6, :cond_2

    if-eq v0, v6, :cond_3

    .line 2479531
    :cond_2
    iget-object v0, p0, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;->d:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2479532
    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;->e:Z

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2479509
    iput-object p1, p0, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;->a:Ljava/lang/CharSequence;

    .line 2479510
    iput-object p2, p0, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;->b:Ljava/lang/CharSequence;

    .line 2479511
    iget-object v0, p0, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;->a:Ljava/lang/CharSequence;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2479512
    iget-object v0, p0, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;->b:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2479513
    iput-boolean v2, p0, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;->e:Z

    .line 2479514
    :goto_0
    return-void

    .line 2479515
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;->b:Ljava/lang/CharSequence;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2479516
    iget-object v0, p0, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;->a:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2479517
    iput-boolean v2, p0, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;->e:Z

    goto :goto_0

    .line 2479518
    :cond_1
    iput-boolean v3, p0, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;->e:Z

    .line 2479519
    new-array v0, v5, [Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;->a:Ljava/lang/CharSequence;

    aput-object v1, v0, v2

    aput-object p3, v0, v3

    iget-object v1, p0, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;->b:Ljava/lang/CharSequence;

    aput-object v1, v0, v4

    invoke-static {v0}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;->c:Ljava/lang/CharSequence;

    .line 2479520
    new-array v0, v5, [Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;->a:Ljava/lang/CharSequence;

    aput-object v1, v0, v2

    const-string v1, "\n"

    aput-object v1, v0, v3

    iget-object v1, p0, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;->b:Ljava/lang/CharSequence;

    aput-object v1, v0, v4

    invoke-static {v0}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;->d:Ljava/lang/CharSequence;

    .line 2479521
    iget-object v0, p0, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;->c:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final onLayout(ZIIII)V
    .locals 3

    .prologue
    .line 2479503
    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;->e:Z

    if-eqz v0, :cond_1

    .line 2479504
    :cond_0
    sub-int v0, p4, p2

    invoke-virtual {p0}, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;->getCompoundPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;->getCompoundPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    .line 2479505
    sub-int v1, p5, p3

    invoke-virtual {p0}, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;->getCompoundPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;->getCompoundPaddingTop()I

    move-result v2

    sub-int/2addr v1, v2

    .line 2479506
    invoke-direct {p0, v0, v1}, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;->a(II)V

    .line 2479507
    :cond_1
    invoke-super/range {p0 .. p5}, Lcom/facebook/resources/ui/FbTextView;->onLayout(ZIIII)V

    .line 2479508
    return-void
.end method

.method public final onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x699af397

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2479500
    if-ne p1, p3, :cond_0

    if-eq p2, p4, :cond_1

    .line 2479501
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;->e:Z

    .line 2479502
    :cond_1
    const/16 v1, 0x2d

    const v2, -0x5cff456

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final setLineSpacing(FF)V
    .locals 0

    .prologue
    .line 2479496
    invoke-super {p0, p1, p2}, Lcom/facebook/resources/ui/FbTextView;->setLineSpacing(FF)V

    .line 2479497
    iput p2, p0, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;->f:F

    .line 2479498
    iput p1, p0, Lcom/facebook/pages/identity/ui/text/TwoStringTextView;->g:F

    .line 2479499
    return-void
.end method
