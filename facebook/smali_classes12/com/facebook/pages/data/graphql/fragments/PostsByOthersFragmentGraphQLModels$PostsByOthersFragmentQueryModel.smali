.class public final Lcom/facebook/pages/data/graphql/fragments/PostsByOthersFragmentGraphQLModels$PostsByOthersFragmentQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6dd2d289
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/data/graphql/fragments/PostsByOthersFragmentGraphQLModels$PostsByOthersFragmentQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/data/graphql/fragments/PostsByOthersFragmentGraphQLModels$PostsByOthersFragmentQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:Z

.field private g:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2464578
    const-class v0, Lcom/facebook/pages/data/graphql/fragments/PostsByOthersFragmentGraphQLModels$PostsByOthersFragmentQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2464577
    const-class v0, Lcom/facebook/pages/data/graphql/fragments/PostsByOthersFragmentGraphQLModels$PostsByOthersFragmentQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2464575
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2464576
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2464565
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2464566
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/fragments/PostsByOthersFragmentGraphQLModels$PostsByOthersFragmentQueryModel;->k()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, -0x590beed7

    invoke-static {v1, v0, v2}, Lcom/facebook/pages/data/graphql/fragments/PostsByOthersFragmentGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/fragments/PostsByOthersFragmentGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2464567
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/fragments/PostsByOthersFragmentGraphQLModels$PostsByOthersFragmentQueryModel;->l()LX/0Px;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v1

    .line 2464568
    const/4 v2, 0x4

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2464569
    const/4 v2, 0x0

    iget-boolean v3, p0, Lcom/facebook/pages/data/graphql/fragments/PostsByOthersFragmentGraphQLModels$PostsByOthersFragmentQueryModel;->e:Z

    invoke-virtual {p1, v2, v3}, LX/186;->a(IZ)V

    .line 2464570
    const/4 v2, 0x1

    iget-boolean v3, p0, Lcom/facebook/pages/data/graphql/fragments/PostsByOthersFragmentGraphQLModels$PostsByOthersFragmentQueryModel;->f:Z

    invoke-virtual {p1, v2, v3}, LX/186;->a(IZ)V

    .line 2464571
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2464572
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2464573
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2464574
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2464555
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2464556
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/fragments/PostsByOthersFragmentGraphQLModels$PostsByOthersFragmentQueryModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2464557
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/fragments/PostsByOthersFragmentGraphQLModels$PostsByOthersFragmentQueryModel;->k()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x590beed7

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/data/graphql/fragments/PostsByOthersFragmentGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/fragments/PostsByOthersFragmentGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2464558
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/fragments/PostsByOthersFragmentGraphQLModels$PostsByOthersFragmentQueryModel;->k()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2464559
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/fragments/PostsByOthersFragmentGraphQLModels$PostsByOthersFragmentQueryModel;

    .line 2464560
    iput v3, v0, Lcom/facebook/pages/data/graphql/fragments/PostsByOthersFragmentGraphQLModels$PostsByOthersFragmentQueryModel;->g:I

    .line 2464561
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2464562
    if-nez v0, :cond_0

    :goto_1
    return-object p0

    .line 2464563
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move-object p0, v0

    .line 2464564
    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2464554
    new-instance v0, LX/HRA;

    invoke-direct {v0, p1}, LX/HRA;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2464549
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2464550
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/data/graphql/fragments/PostsByOthersFragmentGraphQLModels$PostsByOthersFragmentQueryModel;->e:Z

    .line 2464551
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/data/graphql/fragments/PostsByOthersFragmentGraphQLModels$PostsByOthersFragmentQueryModel;->f:Z

    .line 2464552
    const/4 v0, 0x2

    const v1, -0x590beed7

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/data/graphql/fragments/PostsByOthersFragmentGraphQLModels$PostsByOthersFragmentQueryModel;->g:I

    .line 2464553
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2464579
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2464580
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2464548
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2464546
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2464547
    iget-boolean v0, p0, Lcom/facebook/pages/data/graphql/fragments/PostsByOthersFragmentGraphQLModels$PostsByOthersFragmentQueryModel;->e:Z

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2464543
    new-instance v0, Lcom/facebook/pages/data/graphql/fragments/PostsByOthersFragmentGraphQLModels$PostsByOthersFragmentQueryModel;

    invoke-direct {v0}, Lcom/facebook/pages/data/graphql/fragments/PostsByOthersFragmentGraphQLModels$PostsByOthersFragmentQueryModel;-><init>()V

    .line 2464544
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2464545
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2464542
    const v0, -0x3523be6d    # -7217353.5f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2464541
    const v0, 0x25d6af

    return v0
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 2464539
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2464540
    iget-boolean v0, p0, Lcom/facebook/pages/data/graphql/fragments/PostsByOthersFragmentGraphQLModels$PostsByOthersFragmentQueryModel;->f:Z

    return v0
.end method

.method public final k()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProfilePicture"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2464537
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2464538
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/data/graphql/fragments/PostsByOthersFragmentGraphQLModels$PostsByOthersFragmentQueryModel;->g:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final l()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2464535
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/fragments/PostsByOthersFragmentGraphQLModels$PostsByOthersFragmentQueryModel;->h:Ljava/util/List;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/fragments/PostsByOthersFragmentGraphQLModels$PostsByOthersFragmentQueryModel;->h:Ljava/util/List;

    .line 2464536
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/fragments/PostsByOthersFragmentGraphQLModels$PostsByOthersFragmentQueryModel;->h:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method
