.class public final Lcom/facebook/pages/data/graphql/fragments/PostsByOthersFragmentGraphQLModels$PostsByOthersFragmentQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2464452
    const-class v0, Lcom/facebook/pages/data/graphql/fragments/PostsByOthersFragmentGraphQLModels$PostsByOthersFragmentQueryModel;

    new-instance v1, Lcom/facebook/pages/data/graphql/fragments/PostsByOthersFragmentGraphQLModels$PostsByOthersFragmentQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/pages/data/graphql/fragments/PostsByOthersFragmentGraphQLModels$PostsByOthersFragmentQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2464453
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2464454
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 2464455
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2464456
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2464457
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_9

    .line 2464458
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2464459
    :goto_0
    move v1, v2

    .line 2464460
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2464461
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2464462
    new-instance v1, Lcom/facebook/pages/data/graphql/fragments/PostsByOthersFragmentGraphQLModels$PostsByOthersFragmentQueryModel;

    invoke-direct {v1}, Lcom/facebook/pages/data/graphql/fragments/PostsByOthersFragmentGraphQLModels$PostsByOthersFragmentQueryModel;-><init>()V

    .line 2464463
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2464464
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2464465
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2464466
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2464467
    :cond_0
    return-object v1

    .line 2464468
    :cond_1
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_6

    .line 2464469
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 2464470
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2464471
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object p0, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, p0, :cond_1

    if-eqz v9, :cond_1

    .line 2464472
    const-string v10, "can_viewer_post"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 2464473
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v4

    move v8, v4

    move v4, v3

    goto :goto_1

    .line 2464474
    :cond_2
    const-string v10, "can_viewer_post_photo_to_timeline"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 2464475
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v1

    move v7, v1

    move v1, v3

    goto :goto_1

    .line 2464476
    :cond_3
    const-string v10, "profile_picture"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 2464477
    const/4 v9, 0x0

    .line 2464478
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v6, v10, :cond_d

    .line 2464479
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2464480
    :goto_2
    move v6, v9

    .line 2464481
    goto :goto_1

    .line 2464482
    :cond_4
    const-string v10, "viewer_profile_permissions"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 2464483
    invoke-static {p1, v0}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 2464484
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2464485
    :cond_6
    const/4 v9, 0x4

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 2464486
    if-eqz v4, :cond_7

    .line 2464487
    invoke-virtual {v0, v2, v8}, LX/186;->a(IZ)V

    .line 2464488
    :cond_7
    if-eqz v1, :cond_8

    .line 2464489
    invoke-virtual {v0, v3, v7}, LX/186;->a(IZ)V

    .line 2464490
    :cond_8
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 2464491
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 2464492
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_9
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    goto :goto_1

    .line 2464493
    :cond_a
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2464494
    :cond_b
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, p0, :cond_c

    .line 2464495
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 2464496
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2464497
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_b

    if-eqz v10, :cond_b

    .line 2464498
    const-string p0, "uri"

    invoke-virtual {v10, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_a

    .line 2464499
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_3

    .line 2464500
    :cond_c
    const/4 v10, 0x1

    invoke-virtual {v0, v10}, LX/186;->c(I)V

    .line 2464501
    invoke-virtual {v0, v9, v6}, LX/186;->b(II)V

    .line 2464502
    invoke-virtual {v0}, LX/186;->d()I

    move-result v9

    goto :goto_2

    :cond_d
    move v6, v9

    goto :goto_3
.end method
