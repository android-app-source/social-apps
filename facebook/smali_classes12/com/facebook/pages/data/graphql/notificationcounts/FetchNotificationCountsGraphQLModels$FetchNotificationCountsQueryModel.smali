.class public final Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$FetchNotificationCountsQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2e5edfc1
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$FetchNotificationCountsQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$FetchNotificationCountsQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$FetchNotificationCountsQueryModel$AdminedPagesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2464770
    const-class v0, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$FetchNotificationCountsQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2464771
    const-class v0, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$FetchNotificationCountsQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2464772
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2464773
    return-void
.end method

.method private a()Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$FetchNotificationCountsQueryModel$AdminedPagesModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAdminedPages"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2464774
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$FetchNotificationCountsQueryModel;->e:Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$FetchNotificationCountsQueryModel$AdminedPagesModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$FetchNotificationCountsQueryModel$AdminedPagesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$FetchNotificationCountsQueryModel$AdminedPagesModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$FetchNotificationCountsQueryModel;->e:Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$FetchNotificationCountsQueryModel$AdminedPagesModel;

    .line 2464775
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$FetchNotificationCountsQueryModel;->e:Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$FetchNotificationCountsQueryModel$AdminedPagesModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2464776
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2464777
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$FetchNotificationCountsQueryModel;->a()Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$FetchNotificationCountsQueryModel$AdminedPagesModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2464778
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2464779
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2464780
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2464781
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2464782
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2464783
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$FetchNotificationCountsQueryModel;->a()Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$FetchNotificationCountsQueryModel$AdminedPagesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2464784
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$FetchNotificationCountsQueryModel;->a()Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$FetchNotificationCountsQueryModel$AdminedPagesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$FetchNotificationCountsQueryModel$AdminedPagesModel;

    .line 2464785
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$FetchNotificationCountsQueryModel;->a()Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$FetchNotificationCountsQueryModel$AdminedPagesModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2464786
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$FetchNotificationCountsQueryModel;

    .line 2464787
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$FetchNotificationCountsQueryModel;->e:Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$FetchNotificationCountsQueryModel$AdminedPagesModel;

    .line 2464788
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2464789
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2464790
    new-instance v0, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$FetchNotificationCountsQueryModel;

    invoke-direct {v0}, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$FetchNotificationCountsQueryModel;-><init>()V

    .line 2464791
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2464792
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2464793
    const v0, 0x4cbfcbb1    # 1.00556168E8f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2464794
    const v0, -0x6747e1ce

    return v0
.end method
