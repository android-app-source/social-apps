.class public final Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6159764
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel$Serializer;
.end annotation


# instance fields
.field private e:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I

.field private i:Z

.field private j:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2464860
    const-class v0, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2464861
    const-class v0, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2464862
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2464863
    return-void
.end method

.method private l()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCommsHubConfig"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2464882
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2464883
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2464864
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel;->g:Ljava/lang/String;

    .line 2464865
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method private n()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPageLikers"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2464866
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2464867
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel;->j:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2464868
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2464869
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, -0x13b41b9

    invoke-static {v1, v0, v2}, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2464870
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel;->l()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, 0x706c071e

    invoke-static {v2, v1, v3}, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2464871
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2464872
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel;->n()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, -0x11f52a1d

    invoke-static {v4, v3, v5}, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2464873
    const/4 v4, 0x6

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2464874
    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 2464875
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2464876
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2464877
    const/4 v0, 0x3

    iget v1, p0, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel;->h:I

    invoke-virtual {p1, v0, v1, v6}, LX/186;->a(III)V

    .line 2464878
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel;->i:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2464879
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2464880
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2464881
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2464837
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2464838
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2464839
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x13b41b9

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2464840
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2464841
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel;

    .line 2464842
    iput v3, v0, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel;->e:I

    move-object v1, v0

    .line 2464843
    :cond_0
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2464844
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel;->l()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x706c071e

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2464845
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel;->l()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2464846
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel;

    .line 2464847
    iput v3, v0, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel;->f:I

    move-object v1, v0

    .line 2464848
    :cond_1
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel;->n()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    .line 2464849
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel;->n()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x11f52a1d

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 2464850
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel;->n()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2464851
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel;

    .line 2464852
    iput v3, v0, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel;->j:I

    move-object v1, v0

    .line 2464853
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2464854
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    .line 2464855
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 2464856
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 2464857
    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0

    :cond_3
    move-object p0, v1

    .line 2464858
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2464859
    new-instance v0, LX/HRF;

    invoke-direct {v0, p1}, LX/HRF;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2464836
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2464829
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2464830
    const v0, -0x13b41b9

    invoke-static {p1, p2, v2, v0}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel;->e:I

    .line 2464831
    const/4 v0, 0x1

    const v1, 0x706c071e

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel;->f:I

    .line 2464832
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0, v2}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel;->h:I

    .line 2464833
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel;->i:Z

    .line 2464834
    const/4 v0, 0x5

    const v1, -0x11f52a1d

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel;->j:I

    .line 2464835
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2464827
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2464828
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2464826
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2464823
    new-instance v0, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel;

    invoke-direct {v0}, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel;-><init>()V

    .line 2464824
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2464825
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2464822
    const v0, 0x6c24f46e

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2464821
    const v0, 0x25d6af

    return v0
.end method

.method public final j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAdminInfo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2464819
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2464820
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel;->e:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final k()I
    .locals 2

    .prologue
    .line 2464817
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2464818
    iget v0, p0, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$PageNotificationCountsModel;->h:I

    return v0
.end method
