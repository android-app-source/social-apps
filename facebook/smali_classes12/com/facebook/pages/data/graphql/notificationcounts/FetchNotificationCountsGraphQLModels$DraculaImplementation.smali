.class public final Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2464679
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2464680
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2464677
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2464678
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 2464640
    if-nez p1, :cond_0

    .line 2464641
    :goto_0
    return v0

    .line 2464642
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2464643
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2464644
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 2464645
    const v2, -0x7bd9a3e6

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 2464646
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2464647
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2464648
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2464649
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 2464650
    const v2, -0x186119af

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 2464651
    invoke-virtual {p0, p1, v4}, LX/15i;->p(II)I

    move-result v2

    .line 2464652
    const v3, 0x78b72edf

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 2464653
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2464654
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2464655
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 2464656
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2464657
    :sswitch_2
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 2464658
    invoke-virtual {p0, p1, v4, v0}, LX/15i;->a(III)I

    move-result v2

    .line 2464659
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2464660
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 2464661
    invoke-virtual {p3, v4, v2, v0}, LX/186;->a(III)V

    .line 2464662
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2464663
    :sswitch_3
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 2464664
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2464665
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 2464666
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2464667
    :sswitch_4
    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v1

    .line 2464668
    invoke-virtual {p0, p1, v4, v0}, LX/15i;->a(III)I

    move-result v2

    .line 2464669
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2464670
    invoke-virtual {p3, v0, v1}, LX/186;->a(IZ)V

    .line 2464671
    invoke-virtual {p3, v4, v2, v0}, LX/186;->a(III)V

    .line 2464672
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2464673
    :sswitch_5
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 2464674
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2464675
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 2464676
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x7bd9a3e6 -> :sswitch_1
        -0x186119af -> :sswitch_2
        -0x11f52a1d -> :sswitch_5
        -0x13b41b9 -> :sswitch_0
        0x706c071e -> :sswitch_4
        0x78b72edf -> :sswitch_3
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2464639
    new-instance v0, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2464630
    sparse-switch p2, :sswitch_data_0

    .line 2464631
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2464632
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2464633
    const v1, -0x7bd9a3e6

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 2464634
    :goto_0
    :sswitch_1
    return-void

    .line 2464635
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2464636
    const v1, -0x186119af

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 2464637
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2464638
    const v1, 0x78b72edf

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7bd9a3e6 -> :sswitch_2
        -0x186119af -> :sswitch_1
        -0x11f52a1d -> :sswitch_1
        -0x13b41b9 -> :sswitch_0
        0x706c071e -> :sswitch_1
        0x78b72edf -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2464624
    if-eqz p1, :cond_0

    .line 2464625
    invoke-static {p0, p1, p2}, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 2464626
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$DraculaImplementation;

    .line 2464627
    if-eq v0, v1, :cond_0

    .line 2464628
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2464629
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2464623
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2464621
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2464622
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2464681
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2464682
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2464683
    :cond_0
    iput-object p1, p0, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 2464684
    iput p2, p0, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$DraculaImplementation;->b:I

    .line 2464685
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2464620
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2464619
    new-instance v0, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2464616
    iget v0, p0, LX/1vt;->c:I

    .line 2464617
    move v0, v0

    .line 2464618
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2464613
    iget v0, p0, LX/1vt;->c:I

    .line 2464614
    move v0, v0

    .line 2464615
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2464610
    iget v0, p0, LX/1vt;->b:I

    .line 2464611
    move v0, v0

    .line 2464612
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2464607
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2464608
    move-object v0, v0

    .line 2464609
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2464598
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2464599
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2464600
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2464601
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2464602
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2464603
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2464604
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2464605
    invoke-static {v3, v9, v2}, Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/notificationcounts/FetchNotificationCountsGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2464606
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2464595
    iget v0, p0, LX/1vt;->c:I

    .line 2464596
    move v0, v0

    .line 2464597
    return v0
.end method
