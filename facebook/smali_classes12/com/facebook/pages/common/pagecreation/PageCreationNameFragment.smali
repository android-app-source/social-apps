.class public Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fj;


# static fields
.field public static final f:Ljava/lang/String;


# instance fields
.field public a:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/HFc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/HF3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/HFf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private g:Ljava/lang/String;

.field public h:LX/HFJ;

.field public i:Lcom/facebook/widget/text/BetterEditTextView;

.field public j:Lcom/facebook/fig/button/FigButton;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2444166
    const-class v0, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->f:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2444165
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method public static l(Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;)V
    .locals 5

    .prologue
    .line 2444154
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v0, v0

    .line 2444155
    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0400d6

    const v2, 0x7f0400e1

    const v3, 0x7f0400d5

    const v4, 0x7f0400e2

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0hH;->a(IIII)LX/0hH;

    move-result-object v0

    .line 2444156
    iget v1, p0, Landroid/support/v4/app/Fragment;->mFragmentId:I

    move v1, v1

    .line 2444157
    iget-object v2, p0, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->g:Ljava/lang/String;

    .line 2444158
    new-instance v3, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    invoke-direct {v3}, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;-><init>()V

    .line 2444159
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2444160
    const-string p0, "page_creation_fragment_uuid"

    invoke-virtual {v4, p0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2444161
    invoke-virtual {v3, v4}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2444162
    move-object v2, v3

    .line 2444163
    invoke-virtual {v0, v1, v2}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2444164
    return-void
.end method


# virtual methods
.method public final S_()Z
    .locals 3

    .prologue
    .line 2444148
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->c:LX/HF3;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2444149
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->c:LX/HF3;

    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->g:Ljava/lang/String;

    .line 2444150
    iget-object v2, v0, LX/HF3;->a:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2444151
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->e:LX/HFf;

    sget-object v1, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/HFf;->c(Ljava/lang/String;)V

    .line 2444152
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->e:LX/HFf;

    invoke-virtual {v0}, LX/HFf;->b()V

    .line 2444153
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 2444132
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2444133
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {v0}, LX/HFc;->b(LX/0QB;)LX/HFc;

    move-result-object v4

    check-cast v4, LX/HFc;

    invoke-static {v0}, LX/HF3;->a(LX/0QB;)LX/HF3;

    move-result-object v5

    check-cast v5, LX/HF3;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v6

    check-cast v6, LX/1Ck;

    invoke-static {v0}, LX/HFf;->a(LX/0QB;)LX/HFf;

    move-result-object v0

    check-cast v0, LX/HFf;

    iput-object v3, v2, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->a:LX/03V;

    iput-object v4, v2, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->b:LX/HFc;

    iput-object v5, v2, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->c:LX/HF3;

    iput-object v6, v2, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->d:LX/1Ck;

    iput-object v0, v2, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->e:LX/HFf;

    .line 2444134
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->e:LX/HFf;

    .line 2444135
    iget-object v1, v0, LX/HFf;->a:LX/0if;

    sget-object v2, LX/0ig;->ao:LX/0ih;

    invoke-virtual {v1, v2}, LX/0if;->a(LX/0ih;)V

    .line 2444136
    if-eqz p1, :cond_1

    .line 2444137
    const-string v0, "page_creation_fragment_uuid"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2444138
    if-eqz v0, :cond_0

    .line 2444139
    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->g:Ljava/lang/String;

    .line 2444140
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->c:LX/HF3;

    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/HF3;->a(Ljava/lang/String;)LX/HFJ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->h:LX/HFJ;

    .line 2444141
    :cond_0
    :goto_0
    return-void

    .line 2444142
    :cond_1
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->g:Ljava/lang/String;

    .line 2444143
    new-instance v0, LX/HFJ;

    invoke-direct {v0}, LX/HFJ;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->h:LX/HFJ;

    .line 2444144
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->c:LX/HF3;

    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->h:LX/HFJ;

    .line 2444145
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2444146
    iget-object v3, v0, LX/HF3;->a:Ljava/util/Map;

    invoke-interface {v3, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2444147
    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x6e1b08f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2444108
    const v1, 0x7f030e4a

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x332c319c

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0xc845181

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2444129
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2444130
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->d:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2444131
    const/16 v1, 0x2b

    const v2, 0x467a9432

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2444127
    const-string v0, "page_creation_fragment_uuid"

    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2444128
    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x4be39bae

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2444121
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2444122
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2444123
    if-eqz v1, :cond_0

    .line 2444124
    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1ZF;->k_(Z)V

    .line 2444125
    const v2, 0x7f0821fb

    invoke-interface {v1, v2}, LX/1ZF;->x_(I)V

    .line 2444126
    :cond_0
    const/16 v1, 0x2b

    const v2, 0xdc9697e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2444109
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2444110
    const v0, 0x7f0d22de

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2444111
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f0213d2

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    .line 2444112
    invoke-virtual {v0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2444113
    const v0, 0x7f0d22d4

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterEditTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->i:Lcom/facebook/widget/text/BetterEditTextView;

    .line 2444114
    const v0, 0x7f0d22db

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2444115
    const/16 p1, 0x8

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2444116
    const v0, 0x7f0d22dc

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->j:Lcom/facebook/fig/button/FigButton;

    .line 2444117
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->j:Lcom/facebook/fig/button/FigButton;

    const/4 p1, 0x0

    invoke-virtual {v0, p1}, Lcom/facebook/fig/button/FigButton;->setEnabled(Z)V

    .line 2444118
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->j:Lcom/facebook/fig/button/FigButton;

    new-instance p1, LX/HFg;

    invoke-direct {p1, p0}, LX/HFg;-><init>(Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;)V

    invoke-virtual {v0, p1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2444119
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;->i:Lcom/facebook/widget/text/BetterEditTextView;

    new-instance p1, LX/HFh;

    invoke-direct {p1, p0}, LX/HFh;-><init>(Lcom/facebook/pages/common/pagecreation/PageCreationNameFragment;)V

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2444120
    return-void
.end method
