.class public Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fj;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public b:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/HFc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/HF3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/HFf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:Landroid/view/View;

.field public i:Ljava/lang/String;

.field public j:Lcom/facebook/resources/ui/FbEditText;

.field private k:Ljava/lang/String;

.field public l:LX/HFJ;

.field public m:Lcom/facebook/resources/ui/FbEditText;

.field public n:LX/HEu;

.field public o:LX/1ZF;

.field public p:Lcom/facebook/fig/button/FigButton;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2443175
    const-class v0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2443272
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2443273
    return-void
.end method


# virtual methods
.method public final S_()Z
    .locals 2

    .prologue
    .line 2443274
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->g:LX/HFf;

    sget-object v1, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/HFf;->a(Ljava/lang/String;)V

    .line 2443275
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 2443276
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2443277
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-static {v0}, LX/HFc;->b(LX/0QB;)LX/HFc;

    move-result-object v5

    check-cast v5, LX/HFc;

    invoke-static {v0}, LX/HF3;->a(LX/0QB;)LX/HF3;

    move-result-object v6

    check-cast v6, LX/HF3;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v7

    check-cast v7, LX/1Ck;

    invoke-static {v0}, LX/HFf;->a(LX/0QB;)LX/HFf;

    move-result-object v0

    check-cast v0, LX/HFf;

    iput-object v3, v2, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->b:LX/03V;

    iput-object v4, v2, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->c:LX/0Uh;

    iput-object v5, v2, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->d:LX/HFc;

    iput-object v6, v2, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->e:LX/HF3;

    iput-object v7, v2, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->f:LX/1Ck;

    iput-object v0, v2, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->g:LX/HFf;

    .line 2443278
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2443279
    const-string v1, "page_creation_fragment_uuid"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->k:Ljava/lang/String;

    .line 2443280
    if-eqz p1, :cond_0

    .line 2443281
    const-string v0, "super_cat_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->i:Ljava/lang/String;

    .line 2443282
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2443254
    const v0, 0x7f0d22d2

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2443255
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2443256
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->i:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 2443257
    const v1, 0x7f082209

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2443258
    :goto_0
    return-void

    .line 2443259
    :cond_0
    const v1, 0x7f08220a

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    goto :goto_0
.end method

.method public final c()V
    .locals 5

    .prologue
    .line 2443260
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v0, v0

    .line 2443261
    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0400d6

    const v2, 0x7f0400e1

    const v3, 0x7f0400d5

    const v4, 0x7f0400e2

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0hH;->a(IIII)LX/0hH;

    move-result-object v0

    .line 2443262
    iget v1, p0, Landroid/support/v4/app/Fragment;->mFragmentId:I

    move v1, v1

    .line 2443263
    iget-object v2, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->k:Ljava/lang/String;

    .line 2443264
    new-instance v3, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    invoke-direct {v3}, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;-><init>()V

    .line 2443265
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2443266
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2443267
    const-string p0, "page_creation_fragment_uuid"

    invoke-virtual {v4, p0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2443268
    invoke-virtual {v3, v4}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2443269
    move-object v2, v3

    .line 2443270
    invoke-virtual {v0, v1, v2}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2443271
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x310164c6

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2443252
    const v1, 0x7f030e39

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->h:Landroid/view/View;

    .line 2443253
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->h:Landroid/view/View;

    const/16 v2, 0x2b

    const v3, 0x589ff73a

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x319778d1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2443249
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2443250
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->o:LX/1ZF;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2443251
    const/16 v1, 0x2b

    const v2, -0x1545fc1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2443240
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2443241
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->l:LX/HFJ;

    .line 2443242
    iget-object v1, v0, LX/HFJ;->c:LX/HEu;

    move-object v0, v1

    .line 2443243
    if-eqz v0, :cond_0

    .line 2443244
    const-string v0, "super_cat_id"

    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->l:LX/HFJ;

    .line 2443245
    iget-object p0, v1, LX/HFJ;->c:LX/HEu;

    move-object v1, p0

    .line 2443246
    iget-object p0, v1, LX/HEu;->b:Ljava/lang/String;

    move-object v1, p0

    .line 2443247
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2443248
    :cond_0
    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x66685bf6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2443234
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2443235
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    iput-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->o:LX/1ZF;

    .line 2443236
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->o:LX/1ZF;

    if-eqz v1, :cond_0

    .line 2443237
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->o:LX/1ZF;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1ZF;->k_(Z)V

    .line 2443238
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->o:LX/1ZF;

    const v2, 0x7f0821fb

    invoke-interface {v1, v2}, LX/1ZF;->x_(I)V

    .line 2443239
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x775e8e15

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 2443176
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2443177
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->e:LX/HF3;

    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/HF3;->a(Ljava/lang/String;)LX/HFJ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->l:LX/HFJ;

    .line 2443178
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 2443179
    const v0, 0x7f0d22a8

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbEditText;

    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->j:Lcom/facebook/resources/ui/FbEditText;

    .line 2443180
    const v0, 0x7f0d22a9

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbEditText;

    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->m:Lcom/facebook/resources/ui/FbEditText;

    .line 2443181
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->j:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 2443182
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->m:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 2443183
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->l:LX/HFJ;

    .line 2443184
    iget-object v1, v0, LX/HFJ;->d:LX/HEu;

    move-object v0, v1

    .line 2443185
    if-nez v0, :cond_1

    .line 2443186
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->j:Lcom/facebook/resources/ui/FbEditText;

    const v1, 0x7f082206

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setText(I)V

    .line 2443187
    :goto_0
    const v0, 0x7f0d22d0

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2443188
    const v1, 0x7f082205

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2443189
    const v0, 0x7f0d22d1

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2443190
    const v1, 0x7f082208

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2443191
    const v0, 0x7f0d22db

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2443192
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2443193
    const v0, 0x7f0d22de

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2443194
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0213d3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2443195
    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2443196
    const v0, 0x7f0d22a8

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbEditText;

    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->j:Lcom/facebook/resources/ui/FbEditText;

    .line 2443197
    new-instance v0, LX/HFB;

    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->j:Lcom/facebook/resources/ui/FbEditText;

    invoke-direct {v0, p0, v1}, LX/HFB;-><init>(Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;Lcom/facebook/resources/ui/FbEditText;)V

    .line 2443198
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->j:Lcom/facebook/resources/ui/FbEditText;

    new-instance v2, LX/HF4;

    invoke-direct {v2, p0, v0}, LX/HF4;-><init>(Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;LX/HEy;)V

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbEditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2443199
    new-instance v0, LX/HFC;

    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->m:Lcom/facebook/resources/ui/FbEditText;

    invoke-direct {v0, p0, v1}, LX/HFC;-><init>(Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;Lcom/facebook/resources/ui/FbEditText;)V

    .line 2443200
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->m:Lcom/facebook/resources/ui/FbEditText;

    new-instance v2, LX/HF5;

    invoke-direct {v2, p0, v0}, LX/HF5;-><init>(Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;LX/HEy;)V

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbEditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2443201
    const v0, 0x7f0d22dc

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->p:Lcom/facebook/fig/button/FigButton;

    .line 2443202
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->p:Lcom/facebook/fig/button/FigButton;

    new-instance v1, LX/HF6;

    invoke-direct {v1, p0}, LX/HF6;-><init>(Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2443203
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->l:LX/HFJ;

    .line 2443204
    iget-object v1, v0, LX/HFJ;->a:Ljava/lang/String;

    move-object v0, v1

    .line 2443205
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->c:LX/0Uh;

    const/16 v1, 0x5b5

    invoke-virtual {v0, v1, v3}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2443206
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->l:LX/HFJ;

    .line 2443207
    iget-object v1, v0, LX/HFJ;->b:Ljava/lang/String;

    move-object v0, v1

    .line 2443208
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->f:LX/1Ck;

    const-string v2, "get_cat_gql_task_key"

    iget-object v3, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->d:LX/HFc;

    const/4 v4, 0x4

    .line 2443209
    new-instance v5, LX/4I1;

    invoke-direct {v5}, LX/4I1;-><init>()V

    .line 2443210
    const-string p1, "page_name"

    invoke-virtual {v5, p1, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2443211
    move-object p1, v5

    .line 2443212
    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p2

    .line 2443213
    const-string v0, "num_result"

    invoke-virtual {p1, v0, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2443214
    new-instance p1, LX/HG0;

    invoke-direct {p1}, LX/HG0;-><init>()V

    move-object p1, p1

    .line 2443215
    const-string p2, "input"

    invoke-virtual {p1, p2, v5}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object p1

    check-cast p1, LX/HG0;

    .line 2443216
    iget-object p2, v3, LX/HFc;->a:LX/0tX;

    invoke-static {p1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object p1

    invoke-virtual {p2, p1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object p1

    new-instance p2, LX/HFb;

    invoke-direct {p2, v3}, LX/HFb;-><init>(LX/HFc;)V

    .line 2443217
    sget-object v0, LX/131;->INSTANCE:LX/131;

    move-object v0, v0

    .line 2443218
    invoke-static {p1, p2, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object p1

    move-object v5, p1

    .line 2443219
    move-object v3, v5

    .line 2443220
    new-instance v4, LX/HF9;

    invoke-direct {v4, p0}, LX/HF9;-><init>(Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;)V

    invoke-virtual {v1, v2, v3, v4}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2443221
    :cond_0
    return-void

    .line 2443222
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->j:Lcom/facebook/resources/ui/FbEditText;

    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->l:LX/HFJ;

    .line 2443223
    iget-object v2, v1, LX/HFJ;->c:LX/HEu;

    move-object v1, v2

    .line 2443224
    iget-object v2, v1, LX/HEu;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2443225
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2443226
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->m:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbEditText;->setVisibility(I)V

    .line 2443227
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->m:Lcom/facebook/resources/ui/FbEditText;

    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->l:LX/HFJ;

    .line 2443228
    iget-object v2, v1, LX/HFJ;->d:LX/HEu;

    move-object v1, v2

    .line 2443229
    iget-object v2, v1, LX/HEu;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2443230
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2443231
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->l:LX/HFJ;

    .line 2443232
    iget-object v1, v0, LX/HFJ;->d:LX/HEu;

    move-object v0, v1

    .line 2443233
    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCategoryFragment;->n:LX/HEu;

    goto/16 :goto_0
.end method
