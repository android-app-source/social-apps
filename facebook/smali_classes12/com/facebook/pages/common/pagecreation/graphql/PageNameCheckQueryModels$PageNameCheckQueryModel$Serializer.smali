.class public final Lcom/facebook/pages/common/pagecreation/graphql/PageNameCheckQueryModels$PageNameCheckQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/common/pagecreation/graphql/PageNameCheckQueryModels$PageNameCheckQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2446282
    const-class v0, Lcom/facebook/pages/common/pagecreation/graphql/PageNameCheckQueryModels$PageNameCheckQueryModel;

    new-instance v1, Lcom/facebook/pages/common/pagecreation/graphql/PageNameCheckQueryModels$PageNameCheckQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/common/pagecreation/graphql/PageNameCheckQueryModels$PageNameCheckQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2446283
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2446284
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/common/pagecreation/graphql/PageNameCheckQueryModels$PageNameCheckQueryModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2446285
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2446286
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2446287
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2446288
    const/4 p0, 0x0

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 2446289
    if-eqz p0, :cond_0

    .line 2446290
    const-string p2, "error"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2446291
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2446292
    :cond_0
    const/4 p0, 0x1

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 2446293
    if-eqz p0, :cond_1

    .line 2446294
    const-string p2, "input_name"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2446295
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2446296
    :cond_1
    const/4 p0, 0x2

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 2446297
    if-eqz p0, :cond_2

    .line 2446298
    const-string p2, "suggested_name"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2446299
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2446300
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2446301
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2446302
    check-cast p1, Lcom/facebook/pages/common/pagecreation/graphql/PageNameCheckQueryModels$PageNameCheckQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/common/pagecreation/graphql/PageNameCheckQueryModels$PageNameCheckQueryModel$Serializer;->a(Lcom/facebook/pages/common/pagecreation/graphql/PageNameCheckQueryModels$PageNameCheckQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
