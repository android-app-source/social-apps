.class public final Lcom/facebook/pages/common/pagecreation/graphql/PageFetchCategoryQueryModels$PageFetchCategoryQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2446136
    const-class v0, Lcom/facebook/pages/common/pagecreation/graphql/PageFetchCategoryQueryModels$PageFetchCategoryQueryModel;

    new-instance v1, Lcom/facebook/pages/common/pagecreation/graphql/PageFetchCategoryQueryModels$PageFetchCategoryQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/pages/common/pagecreation/graphql/PageFetchCategoryQueryModels$PageFetchCategoryQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2446137
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2446138
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2446139
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2446140
    const/4 v2, 0x0

    .line 2446141
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_5

    .line 2446142
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2446143
    :goto_0
    move v1, v2

    .line 2446144
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2446145
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2446146
    new-instance v1, Lcom/facebook/pages/common/pagecreation/graphql/PageFetchCategoryQueryModels$PageFetchCategoryQueryModel;

    invoke-direct {v1}, Lcom/facebook/pages/common/pagecreation/graphql/PageFetchCategoryQueryModels$PageFetchCategoryQueryModel;-><init>()V

    .line 2446147
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2446148
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2446149
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2446150
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2446151
    :cond_0
    return-object v1

    .line 2446152
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2446153
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_4

    .line 2446154
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2446155
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2446156
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 2446157
    const-string v4, "categories"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2446158
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2446159
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_ARRAY:LX/15z;

    if-ne v3, v4, :cond_3

    .line 2446160
    :goto_2
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_ARRAY:LX/15z;

    if-eq v3, v4, :cond_3

    .line 2446161
    const/4 v4, 0x0

    .line 2446162
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v5, :cond_a

    .line 2446163
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2446164
    :goto_3
    move v3, v4

    .line 2446165
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2446166
    :cond_3
    invoke-static {v1, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 2446167
    goto :goto_1

    .line 2446168
    :cond_4
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 2446169
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2446170
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_5
    move v1, v2

    goto :goto_1

    .line 2446171
    :cond_6
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2446172
    :cond_7
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, p0, :cond_9

    .line 2446173
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 2446174
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2446175
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_7

    if-eqz v6, :cond_7

    .line 2446176
    const-string p0, "category_id"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 2446177
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_4

    .line 2446178
    :cond_8
    const-string p0, "category_name"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 2446179
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_4

    .line 2446180
    :cond_9
    const/4 v6, 0x2

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 2446181
    invoke-virtual {v0, v4, v5}, LX/186;->b(II)V

    .line 2446182
    const/4 v4, 0x1

    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 2446183
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    goto :goto_3

    :cond_a
    move v3, v4

    move v5, v4

    goto :goto_4
.end method
