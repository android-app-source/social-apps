.class public final Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x704247d5
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel$PageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2445611
    const-class v0, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2445583
    const-class v0, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2445609
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2445610
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2445601
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2445602
    invoke-virtual {p0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2445603
    invoke-virtual {p0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel;->j()Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel$PageModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2445604
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2445605
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2445606
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2445607
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2445608
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2445593
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2445594
    invoke-virtual {p0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel;->j()Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel$PageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2445595
    invoke-virtual {p0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel;->j()Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel$PageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel$PageModel;

    .line 2445596
    invoke-virtual {p0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel;->j()Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel$PageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2445597
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel;

    .line 2445598
    iput-object v0, v1, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel;->f:Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel$PageModel;

    .line 2445599
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2445600
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2445591
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel;->e:Ljava/lang/String;

    .line 2445592
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2445588
    new-instance v0, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel;-><init>()V

    .line 2445589
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2445590
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2445587
    const v0, 0x7d3f7e1d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2445586
    const v0, 0xe829602

    return v0
.end method

.method public final j()Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel$PageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2445584
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel;->f:Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel$PageModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel$PageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel$PageModel;

    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel;->f:Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel$PageModel;

    .line 2445585
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel;->f:Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel$PageModel;

    return-object v0
.end method
