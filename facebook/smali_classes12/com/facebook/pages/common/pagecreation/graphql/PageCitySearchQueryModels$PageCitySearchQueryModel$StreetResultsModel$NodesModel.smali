.class public final Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x7882b4e8
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel$NodesModel$PageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2445079
    const-class v0, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2445078
    const-class v0, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2445051
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2445052
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2445070
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2445071
    invoke-virtual {p0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel$NodesModel;->a()Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel$NodesModel$PageModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2445072
    invoke-virtual {p0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2445073
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2445074
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2445075
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2445076
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2445077
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2445062
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2445063
    invoke-virtual {p0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel$NodesModel;->a()Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel$NodesModel$PageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2445064
    invoke-virtual {p0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel$NodesModel;->a()Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel$NodesModel$PageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel$NodesModel$PageModel;

    .line 2445065
    invoke-virtual {p0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel$NodesModel;->a()Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel$NodesModel$PageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2445066
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel$NodesModel;

    .line 2445067
    iput-object v0, v1, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel$NodesModel;->e:Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel$NodesModel$PageModel;

    .line 2445068
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2445069
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel$NodesModel$PageModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2445060
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel$NodesModel;->e:Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel$NodesModel$PageModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel$NodesModel$PageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel$NodesModel$PageModel;

    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel$NodesModel;->e:Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel$NodesModel$PageModel;

    .line 2445061
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel$NodesModel;->e:Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel$NodesModel$PageModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2445057
    new-instance v0, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel$NodesModel;-><init>()V

    .line 2445058
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2445059
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2445056
    const v0, -0x2e50a570

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2445055
    const v0, 0x665eb56e

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2445053
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel$NodesModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel$NodesModel;->f:Ljava/lang/String;

    .line 2445054
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel$NodesModel;->f:Ljava/lang/String;

    return-object v0
.end method
