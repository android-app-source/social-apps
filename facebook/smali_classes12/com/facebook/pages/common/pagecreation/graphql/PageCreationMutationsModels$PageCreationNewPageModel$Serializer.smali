.class public final Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2445566
    const-class v0, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel;

    new-instance v1, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2445567
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2445568
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2445569
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2445570
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2445571
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2445572
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2445573
    if-eqz v2, :cond_0

    .line 2445574
    const-string p0, "error_message"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2445575
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2445576
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2445577
    if-eqz v2, :cond_1

    .line 2445578
    const-string p0, "page"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2445579
    invoke-static {v1, v2, p1}, LX/HGP;->a(LX/15i;ILX/0nX;)V

    .line 2445580
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2445581
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2445582
    check-cast p1, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel$Serializer;->a(Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageCreationNewPageModel;LX/0nX;LX/0my;)V

    return-void
.end method
