.class public final Lcom/facebook/pages/common/pagecreation/graphql/PageCategorySuggestionQueryModels$PageCategorySuggestionQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/common/pagecreation/graphql/PageCategorySuggestionQueryModels$PageCategorySuggestionQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2444760
    const-class v0, Lcom/facebook/pages/common/pagecreation/graphql/PageCategorySuggestionQueryModels$PageCategorySuggestionQueryModel;

    new-instance v1, Lcom/facebook/pages/common/pagecreation/graphql/PageCategorySuggestionQueryModels$PageCategorySuggestionQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/common/pagecreation/graphql/PageCategorySuggestionQueryModels$PageCategorySuggestionQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2444761
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2444762
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/common/pagecreation/graphql/PageCategorySuggestionQueryModels$PageCategorySuggestionQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2444763
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2444764
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2444765
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2444766
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2444767
    if-eqz v2, :cond_1

    .line 2444768
    const-string p0, "categories"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2444769
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2444770
    const/4 p0, 0x0

    :goto_0
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result v0

    if-ge p0, v0, :cond_0

    .line 2444771
    invoke-virtual {v1, v2, p0}, LX/15i;->q(II)I

    move-result v0

    invoke-static {v1, v0, p1}, LX/HG3;->a(LX/15i;ILX/0nX;)V

    .line 2444772
    add-int/lit8 p0, p0, 0x1

    goto :goto_0

    .line 2444773
    :cond_0
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2444774
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2444775
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2444776
    check-cast p1, Lcom/facebook/pages/common/pagecreation/graphql/PageCategorySuggestionQueryModels$PageCategorySuggestionQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/common/pagecreation/graphql/PageCategorySuggestionQueryModels$PageCategorySuggestionQueryModel$Serializer;->a(Lcom/facebook/pages/common/pagecreation/graphql/PageCategorySuggestionQueryModels$PageCategorySuggestionQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
