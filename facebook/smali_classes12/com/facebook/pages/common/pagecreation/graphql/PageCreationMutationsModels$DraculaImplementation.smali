.class public final Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2445323
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2445324
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2445345
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2445346
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 2445325
    if-nez p1, :cond_0

    .line 2445326
    :goto_0
    return v0

    .line 2445327
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2445328
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2445329
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2445330
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2445331
    invoke-virtual {p0, p1, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2445332
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2445333
    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 2445334
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2445335
    const/4 v4, 0x3

    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2445336
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2445337
    invoke-virtual {p3, v5, v2}, LX/186;->b(II)V

    .line 2445338
    invoke-virtual {p3, v6, v3}, LX/186;->b(II)V

    .line 2445339
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2445340
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2445341
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2445342
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2445343
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2445344
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x78f89a2a -> :sswitch_1
        0x70a98ff -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2445286
    new-instance v0, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 2445320
    sparse-switch p0, :sswitch_data_0

    .line 2445321
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2445322
    :sswitch_0
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x78f89a2a -> :sswitch_0
        0x70a98ff -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2445319
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 2445317
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$DraculaImplementation;->b(I)V

    .line 2445318
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2445312
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2445313
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2445314
    :cond_0
    iput-object p1, p0, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$DraculaImplementation;->a:LX/15i;

    .line 2445315
    iput p2, p0, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$DraculaImplementation;->b:I

    .line 2445316
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2445347
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2445311
    new-instance v0, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2445308
    iget v0, p0, LX/1vt;->c:I

    .line 2445309
    move v0, v0

    .line 2445310
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2445305
    iget v0, p0, LX/1vt;->c:I

    .line 2445306
    move v0, v0

    .line 2445307
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2445302
    iget v0, p0, LX/1vt;->b:I

    .line 2445303
    move v0, v0

    .line 2445304
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2445299
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2445300
    move-object v0, v0

    .line 2445301
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2445290
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2445291
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2445292
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2445293
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2445294
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2445295
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2445296
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2445297
    invoke-static {v3, v9, v2}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2445298
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2445287
    iget v0, p0, LX/1vt;->c:I

    .line 2445288
    move v0, v0

    .line 2445289
    return v0
.end method
