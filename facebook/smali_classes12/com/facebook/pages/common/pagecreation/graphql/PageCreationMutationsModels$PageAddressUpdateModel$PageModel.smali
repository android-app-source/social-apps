.class public final Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageAddressUpdateModel$PageModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x16dc9c10
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageAddressUpdateModel$PageModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageAddressUpdateModel$PageModel$Serializer;
.end annotation


# instance fields
.field private e:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2445443
    const-class v0, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageAddressUpdateModel$PageModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2445442
    const-class v0, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageAddressUpdateModel$PageModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2445399
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2445400
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2445434
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2445435
    invoke-virtual {p0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageAddressUpdateModel$PageModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, 0x70a98ff

    invoke-static {v1, v0, v2}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2445436
    invoke-virtual {p0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageAddressUpdateModel$PageModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, -0x78f89a2a

    invoke-static {v2, v1, v3}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2445437
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2445438
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2445439
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2445440
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2445441
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2445418
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2445419
    invoke-virtual {p0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageAddressUpdateModel$PageModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2445420
    invoke-virtual {p0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageAddressUpdateModel$PageModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x70a98ff

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2445421
    invoke-virtual {p0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageAddressUpdateModel$PageModel;->a()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2445422
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageAddressUpdateModel$PageModel;

    .line 2445423
    iput v3, v0, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageAddressUpdateModel$PageModel;->e:I

    move-object v1, v0

    .line 2445424
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageAddressUpdateModel$PageModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2445425
    invoke-virtual {p0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageAddressUpdateModel$PageModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x78f89a2a

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2445426
    invoke-virtual {p0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageAddressUpdateModel$PageModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2445427
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageAddressUpdateModel$PageModel;

    .line 2445428
    iput v3, v0, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageAddressUpdateModel$PageModel;->f:I

    move-object v1, v0

    .line 2445429
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2445430
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    .line 2445431
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 2445432
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_2
    move-object p0, v1

    .line 2445433
    goto :goto_0
.end method

.method public final a()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAddress"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2445416
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2445417
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageAddressUpdateModel$PageModel;->e:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2445415
    new-instance v0, LX/HGJ;

    invoke-direct {v0, p1}, LX/HGJ;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2445411
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2445412
    const/4 v0, 0x0

    const v1, 0x70a98ff

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageAddressUpdateModel$PageModel;->e:I

    .line 2445413
    const/4 v0, 0x1

    const v1, -0x78f89a2a

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageAddressUpdateModel$PageModel;->f:I

    .line 2445414
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2445409
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2445410
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2445408
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2445405
    new-instance v0, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageAddressUpdateModel$PageModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageAddressUpdateModel$PageModel;-><init>()V

    .line 2445406
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2445407
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2445404
    const v0, 0x4af5f4c6    # 8059491.0f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2445403
    const v0, 0x25d6af

    return v0
.end method

.method public final j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPhoneNumber"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2445401
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2445402
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageAddressUpdateModel$PageModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
