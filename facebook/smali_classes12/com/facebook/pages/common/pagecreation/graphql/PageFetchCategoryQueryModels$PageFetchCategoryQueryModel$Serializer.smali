.class public final Lcom/facebook/pages/common/pagecreation/graphql/PageFetchCategoryQueryModels$PageFetchCategoryQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/common/pagecreation/graphql/PageFetchCategoryQueryModels$PageFetchCategoryQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2446209
    const-class v0, Lcom/facebook/pages/common/pagecreation/graphql/PageFetchCategoryQueryModels$PageFetchCategoryQueryModel;

    new-instance v1, Lcom/facebook/pages/common/pagecreation/graphql/PageFetchCategoryQueryModels$PageFetchCategoryQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/common/pagecreation/graphql/PageFetchCategoryQueryModels$PageFetchCategoryQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2446210
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2446208
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/common/pagecreation/graphql/PageFetchCategoryQueryModels$PageFetchCategoryQueryModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 2446184
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2446185
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2446186
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2446187
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2446188
    if-eqz v2, :cond_3

    .line 2446189
    const-string v3, "categories"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2446190
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2446191
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result p0

    if-ge v3, p0, :cond_2

    .line 2446192
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result p0

    .line 2446193
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2446194
    const/4 v0, 0x0

    invoke-virtual {v1, p0, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2446195
    if-eqz v0, :cond_0

    .line 2446196
    const-string p2, "category_id"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2446197
    invoke-virtual {p1, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2446198
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {v1, p0, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2446199
    if-eqz v0, :cond_1

    .line 2446200
    const-string p2, "category_name"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2446201
    invoke-virtual {p1, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2446202
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2446203
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 2446204
    :cond_2
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2446205
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2446206
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2446207
    check-cast p1, Lcom/facebook/pages/common/pagecreation/graphql/PageFetchCategoryQueryModels$PageFetchCategoryQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/common/pagecreation/graphql/PageFetchCategoryQueryModels$PageFetchCategoryQueryModel$Serializer;->a(Lcom/facebook/pages/common/pagecreation/graphql/PageFetchCategoryQueryModels$PageFetchCategoryQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
