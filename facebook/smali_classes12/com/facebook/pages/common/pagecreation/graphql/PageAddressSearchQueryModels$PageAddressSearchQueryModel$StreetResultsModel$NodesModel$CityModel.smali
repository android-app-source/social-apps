.class public final Lcom/facebook/pages/common/pagecreation/graphql/PageAddressSearchQueryModels$PageAddressSearchQueryModel$StreetResultsModel$NodesModel$CityModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x46ac8a11
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/pagecreation/graphql/PageAddressSearchQueryModels$PageAddressSearchQueryModel$StreetResultsModel$NodesModel$CityModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/pagecreation/graphql/PageAddressSearchQueryModels$PageAddressSearchQueryModel$StreetResultsModel$NodesModel$CityModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2444455
    const-class v0, Lcom/facebook/pages/common/pagecreation/graphql/PageAddressSearchQueryModels$PageAddressSearchQueryModel$StreetResultsModel$NodesModel$CityModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2444454
    const-class v0, Lcom/facebook/pages/common/pagecreation/graphql/PageAddressSearchQueryModels$PageAddressSearchQueryModel$StreetResultsModel$NodesModel$CityModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2444452
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2444453
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2444444
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2444445
    invoke-virtual {p0}, Lcom/facebook/pages/common/pagecreation/graphql/PageAddressSearchQueryModels$PageAddressSearchQueryModel$StreetResultsModel$NodesModel$CityModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2444446
    invoke-virtual {p0}, Lcom/facebook/pages/common/pagecreation/graphql/PageAddressSearchQueryModels$PageAddressSearchQueryModel$StreetResultsModel$NodesModel$CityModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2444447
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2444448
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2444449
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2444450
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2444451
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2444441
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2444442
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2444443
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2444440
    new-instance v0, LX/HFu;

    invoke-direct {v0, p1}, LX/HFu;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2444456
    invoke-virtual {p0}, Lcom/facebook/pages/common/pagecreation/graphql/PageAddressSearchQueryModels$PageAddressSearchQueryModel$StreetResultsModel$NodesModel$CityModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2444438
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2444439
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2444437
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2444434
    new-instance v0, Lcom/facebook/pages/common/pagecreation/graphql/PageAddressSearchQueryModels$PageAddressSearchQueryModel$StreetResultsModel$NodesModel$CityModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/pagecreation/graphql/PageAddressSearchQueryModels$PageAddressSearchQueryModel$StreetResultsModel$NodesModel$CityModel;-><init>()V

    .line 2444435
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2444436
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2444433
    const v0, 0xa2e8dec

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2444428
    const v0, 0x25d6af

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2444431
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/graphql/PageAddressSearchQueryModels$PageAddressSearchQueryModel$StreetResultsModel$NodesModel$CityModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/graphql/PageAddressSearchQueryModels$PageAddressSearchQueryModel$StreetResultsModel$NodesModel$CityModel;->e:Ljava/lang/String;

    .line 2444432
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/graphql/PageAddressSearchQueryModels$PageAddressSearchQueryModel$StreetResultsModel$NodesModel$CityModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2444429
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/graphql/PageAddressSearchQueryModels$PageAddressSearchQueryModel$StreetResultsModel$NodesModel$CityModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/graphql/PageAddressSearchQueryModels$PageAddressSearchQueryModel$StreetResultsModel$NodesModel$CityModel;->f:Ljava/lang/String;

    .line 2444430
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/graphql/PageAddressSearchQueryModels$PageAddressSearchQueryModel$StreetResultsModel$NodesModel$CityModel;->f:Ljava/lang/String;

    return-object v0
.end method
