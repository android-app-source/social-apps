.class public final Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x67d6ce9a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2445136
    const-class v0, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2445135
    const-class v0, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2445112
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2445113
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2445129
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2445130
    invoke-virtual {p0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel;->a()Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2445131
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2445132
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2445133
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2445134
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2445121
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2445122
    invoke-virtual {p0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel;->a()Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2445123
    invoke-virtual {p0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel;->a()Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel;

    .line 2445124
    invoke-virtual {p0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel;->a()Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2445125
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel;

    .line 2445126
    iput-object v0, v1, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel;->e:Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel;

    .line 2445127
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2445128
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getStreetResults"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2445119
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel;->e:Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel;

    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel;->e:Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel;

    .line 2445120
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel;->e:Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel$StreetResultsModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2445116
    new-instance v0, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCitySearchQueryModels$PageCitySearchQueryModel;-><init>()V

    .line 2445117
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2445118
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2445115
    const v0, 0x25102a51

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2445114
    const v0, -0x425c124e

    return v0
.end method
