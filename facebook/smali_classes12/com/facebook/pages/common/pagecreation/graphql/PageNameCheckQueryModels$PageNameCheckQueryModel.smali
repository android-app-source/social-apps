.class public final Lcom/facebook/pages/common/pagecreation/graphql/PageNameCheckQueryModels$PageNameCheckQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x4793d757
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/pagecreation/graphql/PageNameCheckQueryModels$PageNameCheckQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/pagecreation/graphql/PageNameCheckQueryModels$PageNameCheckQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2446330
    const-class v0, Lcom/facebook/pages/common/pagecreation/graphql/PageNameCheckQueryModels$PageNameCheckQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2446329
    const-class v0, Lcom/facebook/pages/common/pagecreation/graphql/PageNameCheckQueryModels$PageNameCheckQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2446327
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2446328
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2446303
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/graphql/PageNameCheckQueryModels$PageNameCheckQueryModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/graphql/PageNameCheckQueryModels$PageNameCheckQueryModel;->g:Ljava/lang/String;

    .line 2446304
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/graphql/PageNameCheckQueryModels$PageNameCheckQueryModel;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2446317
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2446318
    invoke-virtual {p0}, Lcom/facebook/pages/common/pagecreation/graphql/PageNameCheckQueryModels$PageNameCheckQueryModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2446319
    invoke-virtual {p0}, Lcom/facebook/pages/common/pagecreation/graphql/PageNameCheckQueryModels$PageNameCheckQueryModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2446320
    invoke-direct {p0}, Lcom/facebook/pages/common/pagecreation/graphql/PageNameCheckQueryModels$PageNameCheckQueryModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2446321
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2446322
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2446323
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2446324
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2446325
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2446326
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2446314
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2446315
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2446316
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2446312
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/graphql/PageNameCheckQueryModels$PageNameCheckQueryModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/graphql/PageNameCheckQueryModels$PageNameCheckQueryModel;->e:Ljava/lang/String;

    .line 2446313
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/graphql/PageNameCheckQueryModels$PageNameCheckQueryModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2446309
    new-instance v0, Lcom/facebook/pages/common/pagecreation/graphql/PageNameCheckQueryModels$PageNameCheckQueryModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/pagecreation/graphql/PageNameCheckQueryModels$PageNameCheckQueryModel;-><init>()V

    .line 2446310
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2446311
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2446308
    const v0, 0x11d1abf7

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2446307
    const v0, 0x4ee0390b

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2446305
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/graphql/PageNameCheckQueryModels$PageNameCheckQueryModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/graphql/PageNameCheckQueryModels$PageNameCheckQueryModel;->f:Ljava/lang/String;

    .line 2446306
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/graphql/PageNameCheckQueryModels$PageNameCheckQueryModel;->f:Ljava/lang/String;

    return-object v0
.end method
