.class public final Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel$PageModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x3f3cec3c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel$PageModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel$PageModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2445863
    const-class v0, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel$PageModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2445862
    const-class v0, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel$PageModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2445860
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2445861
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2445854
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2445855
    invoke-virtual {p0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel$PageModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/util/List;)I

    move-result v0

    .line 2445856
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2445857
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2445858
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2445859
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2445852
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel$PageModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel$PageModel;->e:Ljava/util/List;

    .line 2445853
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel$PageModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2445864
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2445865
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2445866
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2445851
    new-instance v0, LX/HGM;

    invoke-direct {v0, p1}, LX/HGM;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2445849
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2445850
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2445848
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2445845
    new-instance v0, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel$PageModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel$PageModel;-><init>()V

    .line 2445846
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2445847
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2445844
    const v0, 0x5a75d0a4

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2445843
    const v0, 0x25d6af

    return v0
.end method
