.class public final Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x63caa367
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel$PageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2445912
    const-class v0, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2445911
    const-class v0, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2445884
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2445885
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2445903
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2445904
    invoke-virtual {p0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2445905
    invoke-virtual {p0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel;->j()Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel$PageModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2445906
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2445907
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2445908
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2445909
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2445910
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2445895
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2445896
    invoke-virtual {p0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel;->j()Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel$PageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2445897
    invoke-virtual {p0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel;->j()Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel$PageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel$PageModel;

    .line 2445898
    invoke-virtual {p0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel;->j()Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel$PageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2445899
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel;

    .line 2445900
    iput-object v0, v1, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel;->f:Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel$PageModel;

    .line 2445901
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2445902
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2445893
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel;->e:Ljava/lang/String;

    .line 2445894
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2445890
    new-instance v0, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel;-><init>()V

    .line 2445891
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2445892
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2445889
    const v0, 0x9b3b54d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2445888
    const v0, 0x41fe8a78

    return v0
.end method

.method public final j()Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel$PageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2445886
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel;->f:Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel$PageModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel$PageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel$PageModel;

    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel;->f:Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel$PageModel;

    .line 2445887
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel;->f:Lcom/facebook/pages/common/pagecreation/graphql/PageCreationMutationsModels$PageUpdateWebsiteModel$PageModel;

    return-object v0
.end method
