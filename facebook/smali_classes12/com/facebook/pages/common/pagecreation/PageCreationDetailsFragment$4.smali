.class public final Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment$4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;)V
    .locals 0

    .prologue
    .line 2443576
    iput-object p1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment$4;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 3

    .prologue
    .line 2443560
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2443561
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment$4;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->q:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/text/BetterEditTextView;->hasFocus()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment$4;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-boolean v1, v1, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->B:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment$4;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-boolean v1, v1, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->A:Z

    if-eqz v1, :cond_1

    .line 2443562
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment$4;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    const-string v2, "address_typeahead"

    .line 2443563
    iput-object v2, v1, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->w:Ljava/lang/String;

    .line 2443564
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment$4;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    .line 2443565
    iget-object v2, v1, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->z:Landroid/location/Location;

    if-nez v2, :cond_2

    .line 2443566
    new-instance v2, LX/HFX;

    invoke-direct {v2, v1, v0}, LX/HFX;-><init>(Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;Ljava/lang/String;)V

    .line 2443567
    iget-object p0, v1, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->d:LX/1Ck;

    const-string p1, "get_location_task_key"

    invoke-virtual {p0, p1}, LX/1Ck;->a(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 2443568
    iget-object p0, v1, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->d:LX/1Ck;

    const-string p1, "get_location_task_key"

    invoke-virtual {p0, p1}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2443569
    :cond_0
    iget-object p0, v1, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->i:LX/1sS;

    invoke-virtual {p0}, LX/0SQ;->isDone()Z

    move-result p0

    if-nez p0, :cond_3

    .line 2443570
    iget-object p0, v1, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->i:LX/1sS;

    iget-object p1, v1, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->m:Lcom/facebook/location/FbLocationOperationParams;

    const-class v0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, LX/1sS;->a(Lcom/facebook/location/FbLocationOperationParams;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2443571
    iget-object p0, v1, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->d:LX/1Ck;

    const-string p1, "get_location_task_key"

    new-instance v0, LX/HFL;

    invoke-direct {v0, v1}, LX/HFL;-><init>(Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;)V

    invoke-virtual {p0, p1, v0, v2}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2443572
    :goto_0
    return-void

    .line 2443573
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment$4;->a:Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->t:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    goto :goto_0

    .line 2443574
    :cond_2
    invoke-static {v1, v0}, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->e(Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;Ljava/lang/String;)V

    goto :goto_0

    .line 2443575
    :cond_3
    iget-object p0, v1, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->i:LX/1sS;

    invoke-virtual {p0}, LX/1sS;->a()Lcom/facebook/location/ImmutableLocation;

    move-result-object p0

    invoke-virtual {v2, p0}, LX/0Vd;->onSuccess(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2443559
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2443558
    return-void
.end method
