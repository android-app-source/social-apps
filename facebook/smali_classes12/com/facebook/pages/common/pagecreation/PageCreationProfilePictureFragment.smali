.class public Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fj;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final j:Ljava/lang/String;


# instance fields
.field public a:LX/HF3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/3iH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BQc;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BQX;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/HFf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0Yb;

.field public l:Landroid/view/View;

.field private m:Ljava/lang/String;

.field public n:LX/HFJ;

.field public o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public p:Lcom/facebook/fig/button/FigButton;

.field public q:Lcom/facebook/fig/button/FigButton;

.field public r:Landroid/net/Uri;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2444321
    const-class v0, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->j:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2444322
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 2444212
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const-class v1, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;

    invoke-static {v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2444213
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->q:Lcom/facebook/fig/button/FigButton;

    const v1, 0x7f082219

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setText(I)V

    .line 2444214
    return-void
.end method

.method private b(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    .line 2444309
    const-string v0, "extra_profile_pic_expiration"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 2444310
    const-string v0, "extra_profile_pic_caption"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2444311
    const-string v0, "profile_photo_method_extra"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2444312
    const-string v0, "extra_app_attribution"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v8

    check-cast v8, Lcom/facebook/share/model/ComposerAppAttribution;

    .line 2444313
    const-string v0, "extra_msqrd_mask_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 2444314
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->c:LX/3iH;

    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->n:LX/HFJ;

    .line 2444315
    iget-object v2, v1, LX/HFJ;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2444316
    invoke-virtual {v0, v1}, LX/3iH;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2444317
    iget-object v10, p0, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->d:LX/1Ck;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "profile_pic_fetch_vc"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->n:LX/HFJ;

    .line 2444318
    iget-object v3, v2, LX/HFJ;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2444319
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    new-instance v1, LX/HFp;

    move-object v2, p0

    move-object v3, p1

    invoke-direct/range {v1 .. v9}, LX/HFp;-><init>(Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;Landroid/os/Bundle;JLjava/lang/String;Ljava/lang/String;Lcom/facebook/share/model/ComposerAppAttribution;Ljava/lang/String;)V

    invoke-virtual {v10, v11, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2444320
    return-void
.end method

.method public static m(Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;)V
    .locals 5

    .prologue
    .line 2444302
    sget-object v0, LX/8AB;->PAGE_PROFILE_PIC:LX/8AB;

    .line 2444303
    sget-object v1, LX/8A9;->LAUNCH_PROFILE_PIC_CROPPER:LX/8A9;

    .line 2444304
    new-instance v2, LX/8AA;

    invoke-direct {v2, v0}, LX/8AA;-><init>(LX/8AB;)V

    invoke-virtual {v2}, LX/8AA;->i()LX/8AA;

    move-result-object v0

    invoke-virtual {v0}, LX/8AA;->j()LX/8AA;

    move-result-object v0

    invoke-virtual {v0}, LX/8AA;->t()LX/8AA;

    move-result-object v0

    invoke-virtual {v0}, LX/8AA;->l()LX/8AA;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/8AA;->a(LX/8A9;)LX/8AA;

    move-result-object v0

    .line 2444305
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2444306
    const-string v2, "extra_simple_picker_launcher_settings"

    invoke-virtual {v0}, LX/8AA;->w()Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2444307
    iget-object v2, p0, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->b:Lcom/facebook/content/SecureContextHelper;

    const/16 v3, 0xc35

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v4, Landroid/app/Activity;

    invoke-static {v0, v4}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-interface {v2, v1, v3, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2444308
    return-void
.end method

.method public static o(Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;)V
    .locals 5

    .prologue
    .line 2444291
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v0, v0

    .line 2444292
    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0400d6

    const v2, 0x7f0400e1

    const v3, 0x7f0400d5

    const v4, 0x7f0400e2

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0hH;->a(IIII)LX/0hH;

    move-result-object v0

    .line 2444293
    iget v1, p0, Landroid/support/v4/app/Fragment;->mFragmentId:I

    move v1, v1

    .line 2444294
    iget-object v2, p0, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->m:Ljava/lang/String;

    .line 2444295
    new-instance v3, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;

    invoke-direct {v3}, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;-><init>()V

    .line 2444296
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2444297
    const-string p0, "page_creation_fragment_uuid"

    invoke-virtual {v4, p0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2444298
    invoke-virtual {v3, v4}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2444299
    move-object v2, v3

    .line 2444300
    invoke-virtual {v0, v1, v2}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2444301
    return-void
.end method


# virtual methods
.method public final S_()Z
    .locals 2

    .prologue
    .line 2444323
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->i:LX/HFf;

    sget-object v1, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/HFf;->a(Ljava/lang/String;)V

    .line 2444324
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    .line 2444286
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2444287
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;

    invoke-static {v0}, LX/HF3;->a(LX/0QB;)LX/HF3;

    move-result-object v3

    check-cast v3, LX/HF3;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/3iH;->b(LX/0QB;)LX/3iH;

    move-result-object v5

    check-cast v5, LX/3iH;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v6

    check-cast v6, LX/1Ck;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    const/16 v8, 0x36a9

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x36a7

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object p1

    check-cast p1, LX/0Xl;

    invoke-static {v0}, LX/HFf;->a(LX/0QB;)LX/HFf;

    move-result-object v0

    check-cast v0, LX/HFf;

    iput-object v3, v2, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->a:LX/HF3;

    iput-object v4, v2, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->b:Lcom/facebook/content/SecureContextHelper;

    iput-object v5, v2, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->c:LX/3iH;

    iput-object v6, v2, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->d:LX/1Ck;

    iput-object v7, v2, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->e:LX/03V;

    iput-object v8, v2, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->f:LX/0Ot;

    iput-object v9, v2, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->g:LX/0Ot;

    iput-object p1, v2, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->h:LX/0Xl;

    iput-object v0, v2, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->i:LX/HFf;

    .line 2444288
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2444289
    const-string v1, "page_creation_fragment_uuid"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->m:Ljava/lang/String;

    .line 2444290
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2444272
    const/16 v0, 0xc35

    if-ne p1, v0, :cond_0

    .line 2444273
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 2444274
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 2444275
    sget-object v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

    .line 2444276
    iget-object v2, v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->b:Landroid/net/Uri;

    move-object v2, v2

    .line 2444277
    invoke-static {p0, v2}, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->a(Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;Landroid/net/Uri;)V

    .line 2444278
    iget-object v2, p0, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->q:Lcom/facebook/fig/button/FigButton;

    const v3, 0x7f082220

    invoke-virtual {v2, v3}, Lcom/facebook/fig/button/FigButton;->setText(I)V

    .line 2444279
    iget-object v2, v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->b:Landroid/net/Uri;

    move-object v0, v2

    .line 2444280
    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->r:Landroid/net/Uri;

    .line 2444281
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->q:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0, v4}, Lcom/facebook/fig/button/FigButton;->setEnabled(Z)V

    .line 2444282
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->p:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0, v4}, Lcom/facebook/fig/button/FigButton;->setEnabled(Z)V

    .line 2444283
    invoke-direct {p0, v1}, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->b(Landroid/os/Bundle;)V

    .line 2444284
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->l:Landroid/view/View;

    new-instance v1, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment$6;

    invoke-direct {v1, p0}, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment$6;-><init>(Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 2444285
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x4020f6e2

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2444271
    const v1, 0x7f030e4d

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x4c646250    # 5.9869504E7f

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x45c044d4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2444266
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2444267
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->k:LX/0Yb;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->k:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2444268
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->k:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 2444269
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->k:LX/0Yb;

    .line 2444270
    const/16 v1, 0x2b

    const v2, -0x2f0b7260

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x2e132842

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2444259
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2444260
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2444261
    if-eqz v1, :cond_0

    .line 2444262
    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1ZF;->k_(Z)V

    .line 2444263
    const v2, 0x7f0821fb

    invoke-interface {v1, v2}, LX/1ZF;->x_(I)V

    .line 2444264
    new-instance v2, LX/HFk;

    invoke-direct {v2, p0}, LX/HFk;-><init>(Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;)V

    invoke-interface {v1, v2}, LX/1ZF;->a(LX/63W;)V

    .line 2444265
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x270aef60

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2444215
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2444216
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->a:LX/HF3;

    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/HF3;->a(Ljava/lang/String;)LX/HFJ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->n:LX/HFJ;

    .line 2444217
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->h:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.intent.action.PROFILE_PIC_UPDATED"

    new-instance p1, LX/HFl;

    invoke-direct {p1, p0}, LX/HFl;-><init>(Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;)V

    invoke-interface {v0, v1, p1}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->k:LX/0Yb;

    .line 2444218
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->k:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 2444219
    const v0, 0x7f0d22dc

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->p:Lcom/facebook/fig/button/FigButton;

    .line 2444220
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->p:Lcom/facebook/fig/button/FigButton;

    new-instance v1, LX/HFm;

    invoke-direct {v1, p0}, LX/HFm;-><init>(Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2444221
    const v0, 0x7f0d22e3

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    .line 2444222
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2444223
    const v0, 0x7f0d22e1

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    .line 2444224
    new-instance v1, LX/HFn;

    invoke-direct {v1, p0}, LX/HFn;-><init>(Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2444225
    const v0, 0x7f0d22e4

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->q:Lcom/facebook/fig/button/FigButton;

    .line 2444226
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->q:Lcom/facebook/fig/button/FigButton;

    const v1, 0x7f08221a

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setText(I)V

    .line 2444227
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->q:Lcom/facebook/fig/button/FigButton;

    new-instance v1, LX/HFo;

    invoke-direct {v1, p0}, LX/HFo;-><init>(Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2444228
    const v0, 0x7f0d22df

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->l:Landroid/view/View;

    .line 2444229
    const v0, 0x7f0d22d0

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2444230
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2444231
    const v0, 0x7f0d22d1

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2444232
    const v1, 0x7f08221b

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2444233
    const v0, 0x7f0d22db

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2444234
    const v1, 0x7f08221d

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2444235
    const v0, 0x7f0d22de

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2444236
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->n:LX/HFJ;

    .line 2444237
    iget-object p1, v1, LX/HFJ;->l:Landroid/net/Uri;

    move-object v1, p1

    .line 2444238
    if-eqz v1, :cond_0

    .line 2444239
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->n:LX/HFJ;

    .line 2444240
    iget-object p1, v1, LX/HFJ;->l:Landroid/net/Uri;

    move-object v1, p1

    .line 2444241
    const-class p1, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;

    invoke-static {p1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2444242
    :goto_0
    const v0, 0x7f0d22e2

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2444243
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->n:LX/HFJ;

    .line 2444244
    iget-object v1, v0, LX/HFJ;->k:Landroid/net/Uri;

    move-object v0, v1

    .line 2444245
    if-eqz v0, :cond_1

    .line 2444246
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->n:LX/HFJ;

    .line 2444247
    iget-object v1, v0, LX/HFJ;->k:Landroid/net/Uri;

    move-object v0, v1

    .line 2444248
    invoke-static {p0, v0}, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->a(Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;Landroid/net/Uri;)V

    .line 2444249
    :goto_1
    const v0, 0x7f0d04e8

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2444250
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->n:LX/HFJ;

    .line 2444251
    iget-object p1, v1, LX/HFJ;->b:Ljava/lang/String;

    move-object v1, p1

    .line 2444252
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2444253
    return-void

    .line 2444254
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const p1, 0x7f0213d7

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2444255
    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 2444256
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0213d1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2444257
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2444258
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const v1, 0x7f021389

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setBackgroundResource(I)V

    goto :goto_1
.end method
