.class public Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fj;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final k:[Ljava/lang/String;

.field public static final l:Ljava/lang/String;


# instance fields
.field public A:Z

.field public B:Z

.field public final C:Landroid/view/View$OnFocusChangeListener;

.field public final D:Landroid/view/View$OnClickListener;

.field public final E:Landroid/view/View$OnClickListener;

.field public a:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/HFc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/HF3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/HEr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/HF2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/HFf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0i4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/1sS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/HFr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final m:Lcom/facebook/location/FbLocationOperationParams;

.field private n:Ljava/lang/String;

.field public o:LX/HFJ;

.field public p:Lcom/facebook/widget/text/BetterEditTextView;

.field public q:Lcom/facebook/widget/text/BetterEditTextView;

.field public r:Lcom/facebook/widget/text/BetterEditTextView;

.field public s:Lcom/facebook/widget/text/BetterEditTextView;

.field public t:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public u:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public v:Ljava/lang/String;

.field public w:Ljava/lang/String;

.field public x:Landroid/widget/ScrollView;

.field public y:Lcom/facebook/fig/button/FigButton;

.field public z:Landroid/location/Location;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2443860
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.ACCESS_FINE_LOCATION"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "android.permission.ACCESS_COARSE_LOCATION"

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->k:[Ljava/lang/String;

    .line 2443861
    const-class v0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->l:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    .line 2443850
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2443851
    sget-object v0, LX/0yF;->HIGH_ACCURACY:LX/0yF;

    invoke-static {v0}, Lcom/facebook/location/FbLocationOperationParams;->a(LX/0yF;)LX/1S7;

    move-result-object v0

    const-wide/32 v2, 0xea60

    invoke-virtual {v0, v2, v3}, LX/1S7;->c(J)LX/1S7;

    move-result-object v0

    const-wide/16 v2, 0x4e20

    .line 2443852
    iput-wide v2, v0, LX/1S7;->b:J

    .line 2443853
    move-object v0, v0

    .line 2443854
    invoke-virtual {v0}, LX/1S7;->a()Lcom/facebook/location/FbLocationOperationParams;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->m:Lcom/facebook/location/FbLocationOperationParams;

    .line 2443855
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->A:Z

    .line 2443856
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->B:Z

    .line 2443857
    new-instance v0, LX/HFQ;

    invoke-direct {v0, p0}, LX/HFQ;-><init>(Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;)V

    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->C:Landroid/view/View$OnFocusChangeListener;

    .line 2443858
    new-instance v0, LX/HFM;

    invoke-direct {v0, p0}, LX/HFM;-><init>(Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;)V

    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->D:Landroid/view/View$OnClickListener;

    .line 2443859
    new-instance v0, LX/HFN;

    invoke-direct {v0, p0}, LX/HFN;-><init>(Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;)V

    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->E:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public static e(Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 2443838
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->d:LX/1Ck;

    const-string v1, "address_search_gql_task_key"

    iget-object v2, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->b:LX/HFc;

    iget-object v3, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->z:Landroid/location/Location;

    const/4 v4, 0x5

    .line 2443839
    new-instance v5, LX/4DK;

    invoke-direct {v5}, LX/4DK;-><init>()V

    .line 2443840
    new-instance v6, LX/3Aj;

    invoke-direct {v6}, LX/3Aj;-><init>()V

    invoke-virtual {v3}, Landroid/location/Location;->getLatitude()D

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/3Aj;->a(Ljava/lang/Double;)LX/3Aj;

    move-result-object v6

    invoke-virtual {v3}, Landroid/location/Location;->getLongitude()D

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/3Aj;->b(Ljava/lang/Double;)LX/3Aj;

    move-result-object v6

    .line 2443841
    invoke-virtual {v5, p1}, LX/4DK;->a(Ljava/lang/String;)LX/4DK;

    move-result-object v7

    invoke-virtual {v7, v6}, LX/4DK;->a(LX/3Aj;)LX/4DK;

    move-result-object v6

    const-string v7, "ANDROID_PAGES"

    invoke-virtual {v6, v7}, LX/4DK;->f(Ljava/lang/String;)LX/4DK;

    move-result-object v6

    const-string v7, "HERE_THRIFT"

    invoke-virtual {v6, v7}, LX/4DK;->b(Ljava/lang/String;)LX/4DK;

    move-result-object v6

    const-string v7, "STREET_TYPEAHEAD"

    invoke-virtual {v6, v7}, LX/4DK;->c(Ljava/lang/String;)LX/4DK;

    .line 2443842
    new-instance v6, LX/HFs;

    invoke-direct {v6}, LX/HFs;-><init>()V

    move-object v6, v6

    .line 2443843
    const-string v7, "query_params"

    invoke-virtual {v6, v7, v5}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v6

    const-string v7, "num_results"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v6

    check-cast v6, LX/HFs;

    .line 2443844
    iget-object v7, v2, LX/HFc;->a:LX/0tX;

    invoke-static {v6}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v6

    invoke-virtual {v7, v6}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v6

    new-instance v7, LX/HFa;

    invoke-direct {v7, v2}, LX/HFa;-><init>(LX/HFc;)V

    .line 2443845
    sget-object v8, LX/131;->INSTANCE:LX/131;

    move-object v8, v8

    .line 2443846
    invoke-static {v6, v7, v8}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    move-object v5, v6

    .line 2443847
    move-object v2, v5

    .line 2443848
    new-instance v3, LX/HFK;

    invoke-direct {v3, p0}, LX/HFK;-><init>(Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2443849
    return-void
.end method


# virtual methods
.method public final S_()Z
    .locals 6

    .prologue
    .line 2443835
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->g:LX/HFf;

    sget-object v1, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->l:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->w:Ljava/lang/String;

    .line 2443836
    iget-object v3, v0, LX/HFf;->a:LX/0if;

    sget-object v4, LX/0ig;->ao:LX/0ih;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string p0, "backFrom_"

    invoke-direct {v5, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5, v2}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 2443837
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    .line 2443862
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2443863
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {v0}, LX/HFc;->b(LX/0QB;)LX/HFc;

    move-result-object v4

    check-cast v4, LX/HFc;

    invoke-static {v0}, LX/HF3;->a(LX/0QB;)LX/HF3;

    move-result-object v5

    check-cast v5, LX/HF3;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v6

    check-cast v6, LX/1Ck;

    new-instance v7, LX/HEr;

    invoke-direct {v7}, LX/HEr;-><init>()V

    move-object v7, v7

    move-object v7, v7

    check-cast v7, LX/HEr;

    new-instance v8, LX/HF2;

    invoke-direct {v8}, LX/HF2;-><init>()V

    move-object v8, v8

    move-object v8, v8

    check-cast v8, LX/HF2;

    invoke-static {v0}, LX/HFf;->a(LX/0QB;)LX/HFf;

    move-result-object v9

    check-cast v9, LX/HFf;

    const-class v10, LX/0i4;

    invoke-interface {v0, v10}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/0i4;

    invoke-static {v0}, LX/1sS;->b(LX/0QB;)LX/1sS;

    move-result-object p1

    check-cast p1, LX/1sS;

    invoke-static {v0}, LX/HFr;->a(LX/0QB;)LX/HFr;

    move-result-object v0

    check-cast v0, LX/HFr;

    iput-object v3, v2, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->a:LX/03V;

    iput-object v4, v2, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->b:LX/HFc;

    iput-object v5, v2, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->c:LX/HF3;

    iput-object v6, v2, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->d:LX/1Ck;

    iput-object v7, v2, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->e:LX/HEr;

    iput-object v8, v2, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->f:LX/HF2;

    iput-object v9, v2, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->g:LX/HFf;

    iput-object v10, v2, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->h:LX/0i4;

    iput-object p1, v2, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->i:LX/1sS;

    iput-object v0, v2, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->j:LX/HFr;

    .line 2443864
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2443865
    const-string v1, "page_creation_fragment_uuid"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->n:Ljava/lang/String;

    .line 2443866
    return-void
.end method

.method public final b()V
    .locals 5

    .prologue
    .line 2443707
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v0, v0

    .line 2443708
    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0400d6

    const v2, 0x7f0400e1

    const v3, 0x7f0400d5

    const v4, 0x7f0400e2

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0hH;->a(IIII)LX/0hH;

    move-result-object v0

    .line 2443709
    iget v1, p0, Landroid/support/v4/app/Fragment;->mFragmentId:I

    move v1, v1

    .line 2443710
    iget-object v2, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->n:Ljava/lang/String;

    .line 2443711
    new-instance v3, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;

    invoke-direct {v3}, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;-><init>()V

    .line 2443712
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2443713
    const-string p0, "page_creation_fragment_uuid"

    invoke-virtual {v4, p0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2443714
    invoke-virtual {v3, v4}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2443715
    move-object v2, v3

    .line 2443716
    invoke-virtual {v0, v1, v2}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2443717
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x687b7585

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2443718
    const v1, 0x7f030e49

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x3e758206

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onStart()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x1372f14a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2443719
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2443720
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2443721
    if-eqz v1, :cond_0

    .line 2443722
    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1ZF;->k_(Z)V

    .line 2443723
    const v2, 0x7f0821fb

    invoke-interface {v1, v2}, LX/1ZF;->x_(I)V

    .line 2443724
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v2

    const v4, 0x7f080030

    invoke-virtual {p0, v4}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2443725
    iput-object v4, v2, LX/108;->g:Ljava/lang/String;

    .line 2443726
    move-object v2, v2

    .line 2443727
    invoke-virtual {v2}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2443728
    new-instance v2, LX/HFR;

    invoke-direct {v2, p0}, LX/HFR;-><init>(Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;)V

    invoke-interface {v1, v2}, LX/1ZF;->a(LX/63W;)V

    .line 2443729
    :cond_0
    const/16 v1, 0x2b

    const v2, 0xb5a30f9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2443730
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2443731
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->n:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2443732
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->c:LX/HF3;

    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/HF3;->a(Ljava/lang/String;)LX/HFJ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->o:LX/HFJ;

    .line 2443733
    const v0, 0x7f0d22d6

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->t:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2443734
    const v0, 0x7f0d22d8

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->u:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2443735
    const/4 p2, 0x1

    .line 2443736
    new-instance v0, LX/1P1;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1P1;-><init>(Landroid/content/Context;)V

    .line 2443737
    invoke-virtual {v0, p2}, LX/1P1;->b(I)V

    .line 2443738
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->f:LX/HF2;

    iget-object p1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->D:Landroid/view/View$OnClickListener;

    .line 2443739
    iput-object p1, v1, LX/HF2;->b:Landroid/view/View$OnClickListener;

    .line 2443740
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->u:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object p1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->f:LX/HF2;

    invoke-virtual {v1, p1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2443741
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->u:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2443742
    new-instance v0, LX/1P1;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1P1;-><init>(Landroid/content/Context;)V

    .line 2443743
    invoke-virtual {v0, p2}, LX/1P1;->b(I)V

    .line 2443744
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->e:LX/HEr;

    iget-object p1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->E:Landroid/view/View$OnClickListener;

    .line 2443745
    iput-object p1, v1, LX/HEr;->b:Landroid/view/View$OnClickListener;

    .line 2443746
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->e:LX/HEr;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    .line 2443747
    iput-object p1, v1, LX/HEr;->c:Landroid/content/Context;

    .line 2443748
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->t:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object p1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->e:LX/HEr;

    invoke-virtual {v1, p1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2443749
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->t:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2443750
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->o:LX/HFJ;

    .line 2443751
    iget-object v1, v0, LX/HFJ;->c:LX/HEu;

    move-object v0, v1

    .line 2443752
    iget-object v1, v0, LX/HEu;->b:Ljava/lang/String;

    move-object v0, v1

    .line 2443753
    const/4 v1, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 2443754
    const-string v1, "website"

    iput-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->w:Ljava/lang/String;

    .line 2443755
    const v1, 0x7f0d22de

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2443756
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v0, 0x7f0213d6

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 2443757
    invoke-virtual {v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2443758
    const v1, 0x7f0d22d0

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    .line 2443759
    const v2, 0x7f08220c

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2443760
    const v1, 0x7f0d22d1

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    .line 2443761
    const v2, 0x7f08220d

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2443762
    const v1, 0x7f0d22d4

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/text/BetterEditTextView;

    .line 2443763
    iget-object v2, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->o:LX/HFJ;

    .line 2443764
    iget-object v0, v2, LX/HFJ;->e:Ljava/lang/String;

    move-object v2, v0

    .line 2443765
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 2443766
    iget-object v2, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->o:LX/HFJ;

    .line 2443767
    iget-object v0, v2, LX/HFJ;->e:Ljava/lang/String;

    move-object v2, v0

    .line 2443768
    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterEditTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2443769
    :goto_1
    const v1, 0x7f0d22db

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    .line 2443770
    const v2, 0x7f082217

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2443771
    const v1, 0x7f0d22d4

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2443772
    const v1, 0x7f0d22dc

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fig/button/FigButton;

    iput-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->y:Lcom/facebook/fig/button/FigButton;

    .line 2443773
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->y:Lcom/facebook/fig/button/FigButton;

    new-instance v2, LX/HFV;

    invoke-direct {v2, p0}, LX/HFV;-><init>(Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;)V

    invoke-virtual {v1, v2}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2443774
    :goto_2
    return-void

    .line 2443775
    :pswitch_0
    const-string v2, "1006"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x0

    goto/16 :goto_0

    .line 2443776
    :pswitch_1
    const-string v1, "address"

    iput-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->w:Ljava/lang/String;

    .line 2443777
    const/4 p2, 0x0

    .line 2443778
    const v1, 0x7f0d22d3

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ScrollView;

    iput-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->x:Landroid/widget/ScrollView;

    .line 2443779
    const v1, 0x7f0d22de

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2443780
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p1, 0x7f0213d5

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 2443781
    invoke-virtual {v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2443782
    const v1, 0x7f0d22d5

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/text/BetterEditTextView;

    iput-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->q:Lcom/facebook/widget/text/BetterEditTextView;

    .line 2443783
    const v1, 0x7f0d22d7

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/text/BetterEditTextView;

    iput-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->p:Lcom/facebook/widget/text/BetterEditTextView;

    .line 2443784
    const v1, 0x7f0d22d0

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    .line 2443785
    const v2, 0x7f08220e

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2443786
    const v1, 0x7f0d22d1

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    .line 2443787
    const v2, 0x7f08220f

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2443788
    const v1, 0x7f0d22db

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    .line 2443789
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2443790
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->o:LX/HFJ;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2443791
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->q:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v1, p2}, Lcom/facebook/widget/text/BetterEditTextView;->setVisibility(I)V

    .line 2443792
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->o:LX/HFJ;

    .line 2443793
    iget-object v2, v1, LX/HFJ;->f:Ljava/lang/String;

    move-object v1, v2

    .line 2443794
    if-eqz v1, :cond_1

    .line 2443795
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->q:Lcom/facebook/widget/text/BetterEditTextView;

    iget-object v2, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->o:LX/HFJ;

    .line 2443796
    iget-object p1, v2, LX/HFJ;->f:Ljava/lang/String;

    move-object v2, p1

    .line 2443797
    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterEditTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2443798
    :cond_1
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->p:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v1, p2}, Lcom/facebook/widget/text/BetterEditTextView;->setVisibility(I)V

    .line 2443799
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->o:LX/HFJ;

    .line 2443800
    iget-object v2, v1, LX/HFJ;->h:Ljava/lang/String;

    move-object v1, v2

    .line 2443801
    if-eqz v1, :cond_2

    .line 2443802
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->p:Lcom/facebook/widget/text/BetterEditTextView;

    iget-object v2, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->o:LX/HFJ;

    .line 2443803
    iget-object p1, v2, LX/HFJ;->h:Ljava/lang/String;

    move-object v2, p1

    .line 2443804
    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterEditTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2443805
    :cond_2
    const v1, 0x7f0d22d9

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/text/BetterEditTextView;

    iput-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->r:Lcom/facebook/widget/text/BetterEditTextView;

    .line 2443806
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->r:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v1, p2}, Lcom/facebook/widget/text/BetterEditTextView;->setVisibility(I)V

    .line 2443807
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->o:LX/HFJ;

    .line 2443808
    iget-object v2, v1, LX/HFJ;->i:Ljava/lang/String;

    move-object v1, v2

    .line 2443809
    if-eqz v1, :cond_3

    .line 2443810
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->r:Lcom/facebook/widget/text/BetterEditTextView;

    iget-object v2, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->o:LX/HFJ;

    .line 2443811
    iget-object p1, v2, LX/HFJ;->i:Ljava/lang/String;

    move-object v2, p1

    .line 2443812
    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterEditTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2443813
    :cond_3
    const v1, 0x7f0d22da

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/text/BetterEditTextView;

    iput-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->s:Lcom/facebook/widget/text/BetterEditTextView;

    .line 2443814
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->s:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v1, p2}, Lcom/facebook/widget/text/BetterEditTextView;->setVisibility(I)V

    .line 2443815
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->o:LX/HFJ;

    .line 2443816
    iget-object v2, v1, LX/HFJ;->j:Ljava/lang/String;

    move-object v1, v2

    .line 2443817
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 2443818
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->s:Lcom/facebook/widget/text/BetterEditTextView;

    iget-object v2, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->o:LX/HFJ;

    .line 2443819
    iget-object p1, v2, LX/HFJ;->j:Ljava/lang/String;

    move-object v2, p1

    .line 2443820
    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterEditTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2443821
    :cond_4
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->j:LX/HFr;

    .line 2443822
    iget-object v2, v1, LX/HFr;->a:LX/0ad;

    sget-object p1, LX/0c0;->Live:LX/0c0;

    sget-short p2, LX/HFq;->a:S

    const/4 v0, 0x0

    invoke-interface {v2, p1, p2, v0}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v2

    move v1, v2

    .line 2443823
    if-eqz v1, :cond_5

    .line 2443824
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->h:LX/0i4;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v1

    sget-object v2, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->k:[Ljava/lang/String;

    new-instance p1, LX/HFS;

    invoke-direct {p1, p0}, LX/HFS;-><init>(Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;)V

    invoke-virtual {v1, v2, p1}, LX/0i5;->a([Ljava/lang/String;LX/6Zx;)V

    .line 2443825
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->q:Lcom/facebook/widget/text/BetterEditTextView;

    new-instance v2, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment$4;

    invoke-direct {v2, p0}, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment$4;-><init>(Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;)V

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterEditTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2443826
    :cond_5
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->p:Lcom/facebook/widget/text/BetterEditTextView;

    new-instance v2, LX/HFT;

    invoke-direct {v2, p0}, LX/HFT;-><init>(Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;)V

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterEditTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2443827
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->q:Lcom/facebook/widget/text/BetterEditTextView;

    iget-object v2, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->C:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterEditTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2443828
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->p:Lcom/facebook/widget/text/BetterEditTextView;

    iget-object v2, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->C:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterEditTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2443829
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->r:Lcom/facebook/widget/text/BetterEditTextView;

    iget-object v2, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->C:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterEditTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2443830
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->s:Lcom/facebook/widget/text/BetterEditTextView;

    iget-object v2, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->C:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterEditTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2443831
    const v1, 0x7f0d22dc

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fig/button/FigButton;

    iput-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->y:Lcom/facebook/fig/button/FigButton;

    .line 2443832
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;->y:Lcom/facebook/fig/button/FigButton;

    new-instance v2, LX/HFU;

    invoke-direct {v2, p0}, LX/HFU;-><init>(Lcom/facebook/pages/common/pagecreation/PageCreationDetailsFragment;)V

    invoke-virtual {v1, v2}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2443833
    goto/16 :goto_2

    .line 2443834
    :cond_6
    const v2, 0x7f082216

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterEditTextView;->setHint(I)V

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x170065
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
