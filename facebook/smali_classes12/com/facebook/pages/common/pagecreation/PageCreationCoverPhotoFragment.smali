.class public Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fj;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final k:Ljava/lang/String;


# instance fields
.field public a:LX/HF3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/3iH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/HFf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BQX;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/0Yb;

.field public m:Landroid/view/View;

.field private n:Ljava/lang/String;

.field public o:LX/HFJ;

.field public p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public q:Lcom/facebook/fig/button/FigButton;

.field public r:Lcom/facebook/fig/button/FigButton;

.field public s:Landroid/net/Uri;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2443442
    const-class v0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->k:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2443441
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 2443438
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const-class v1, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;

    invoke-static {v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2443439
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->r:Lcom/facebook/fig/button/FigButton;

    const v1, 0x7f082219

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setText(I)V

    .line 2443440
    return-void
.end method

.method public static m(Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;)V
    .locals 5

    .prologue
    .line 2443431
    sget-object v0, LX/8A9;->LAUNCH_COVER_PIC_CROPPER:LX/8A9;

    .line 2443432
    sget-object v1, LX/8AB;->PAGE_COVER_PHOTO:LX/8AB;

    .line 2443433
    new-instance v2, LX/8AA;

    invoke-direct {v2, v1}, LX/8AA;-><init>(LX/8AB;)V

    invoke-virtual {v2}, LX/8AA;->i()LX/8AA;

    move-result-object v1

    invoke-virtual {v1}, LX/8AA;->j()LX/8AA;

    move-result-object v1

    invoke-virtual {v1}, LX/8AA;->t()LX/8AA;

    move-result-object v1

    invoke-virtual {v1}, LX/8AA;->l()LX/8AA;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/8AA;->a(LX/8A9;)LX/8AA;

    move-result-object v0

    .line 2443434
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2443435
    const-string v2, "extra_simple_picker_launcher_settings"

    invoke-virtual {v0}, LX/8AA;->w()Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2443436
    iget-object v2, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->b:Lcom/facebook/content/SecureContextHelper;

    const/16 v3, 0xc34

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v4, Landroid/app/Activity;

    invoke-static {v0, v4}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-interface {v2, v1, v3, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2443437
    return-void
.end method

.method public static o(Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;)V
    .locals 5

    .prologue
    .line 2443423
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->f:LX/HFf;

    sget-object v1, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/HFf;->c(Ljava/lang/String;)V

    .line 2443424
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->f:LX/HFf;

    invoke-virtual {v0}, LX/HFf;->b()V

    .line 2443425
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, LX/0ax;->aE:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->o:LX/HFJ;

    .line 2443426
    iget-object v4, v3, LX/HFJ;->a:Ljava/lang/String;

    move-object v3, v4

    .line 2443427
    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2443428
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2443429
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2443430
    return-void
.end method


# virtual methods
.method public final S_()Z
    .locals 2

    .prologue
    .line 2443421
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->f:LX/HFf;

    sget-object v1, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/HFf;->a(Ljava/lang/String;)V

    .line 2443422
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    .line 2443416
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2443417
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;

    invoke-static {v0}, LX/HF3;->a(LX/0QB;)LX/HF3;

    move-result-object v3

    check-cast v3, LX/HF3;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    const/16 v5, 0xbc6

    invoke-static {v0, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v0}, LX/3iH;->b(LX/0QB;)LX/3iH;

    move-result-object v6

    check-cast v6, LX/3iH;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v7

    check-cast v7, LX/1Ck;

    invoke-static {v0}, LX/HFf;->a(LX/0QB;)LX/HFf;

    move-result-object v8

    check-cast v8, LX/HFf;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    const/16 v10, 0x36aa

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 p1, 0x36a7

    invoke-static {v0, p1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v0

    check-cast v0, LX/0Xl;

    iput-object v3, v2, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->a:LX/HF3;

    iput-object v4, v2, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->b:Lcom/facebook/content/SecureContextHelper;

    iput-object v5, v2, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->c:LX/0Or;

    iput-object v6, v2, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->d:LX/3iH;

    iput-object v7, v2, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->e:LX/1Ck;

    iput-object v8, v2, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->f:LX/HFf;

    iput-object v9, v2, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->g:LX/03V;

    iput-object v10, v2, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->h:LX/0Ot;

    iput-object p1, v2, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->i:LX/0Ot;

    iput-object v0, v2, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->j:LX/0Xl;

    .line 2443418
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2443419
    const-string v1, "page_creation_fragment_uuid"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->n:Ljava/lang/String;

    .line 2443420
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 12

    .prologue
    const/4 v3, 0x0

    .line 2443389
    const/16 v0, 0xc34

    if-ne p1, v0, :cond_0

    .line 2443390
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 2443391
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 2443392
    sget-object v1, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

    .line 2443393
    iget-object v1, v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->b:Landroid/net/Uri;

    move-object v1, v1

    .line 2443394
    iput-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->s:Landroid/net/Uri;

    .line 2443395
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->s:Landroid/net/Uri;

    invoke-static {p0, v1}, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->a(Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;Landroid/net/Uri;)V

    .line 2443396
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->r:Lcom/facebook/fig/button/FigButton;

    const v2, 0x7f082220

    invoke-virtual {v1, v2}, Lcom/facebook/fig/button/FigButton;->setText(I)V

    .line 2443397
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->r:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v1, v3}, Lcom/facebook/fig/button/FigButton;->setEnabled(Z)V

    .line 2443398
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->q:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v1, v3}, Lcom/facebook/fig/button/FigButton;->setEnabled(Z)V

    .line 2443399
    iget-object v4, v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->b:Landroid/net/Uri;

    move-object v4, v4

    .line 2443400
    invoke-virtual {v4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/2Qx;->a(Ljava/lang/String;)LX/434;

    move-result-object v9

    .line 2443401
    new-instance v4, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;

    iget-object v5, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->o:LX/HFJ;

    .line 2443402
    iget-object v6, v5, LX/HFJ;->a:Ljava/lang/String;

    move-object v5, v6

    .line 2443403
    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    .line 2443404
    iget-object v7, v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->b:Landroid/net/Uri;

    move-object v7, v7

    .line 2443405
    invoke-virtual {v7}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v7

    .line 2443406
    iget-object v8, v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->g:Landroid/graphics/RectF;

    move-object v8, v8

    .line 2443407
    const-wide/16 v10, 0x0

    invoke-direct/range {v4 .. v11}, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;-><init>(JLjava/lang/String;Landroid/graphics/RectF;LX/434;J)V

    .line 2443408
    iget-object v5, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->d:LX/3iH;

    iget-object v6, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->o:LX/HFJ;

    .line 2443409
    iget-object v7, v6, LX/HFJ;->a:Ljava/lang/String;

    move-object v6, v7

    .line 2443410
    invoke-virtual {v5, v6}, LX/3iH;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    .line 2443411
    iget-object v6, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->e:LX/1Ck;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "cover_photo_fetch_vc"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->o:LX/HFJ;

    .line 2443412
    iget-object v9, v8, LX/HFJ;->a:Ljava/lang/String;

    move-object v8, v9

    .line 2443413
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-instance v8, LX/HFI;

    invoke-direct {v8, p0, v0, v4}, LX/HFI;-><init>(Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;)V

    invoke-virtual {v6, v7, v5, v8}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2443414
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->m:Landroid/view/View;

    new-instance v1, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment$6;

    invoke-direct {v1, p0}, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment$6;-><init>(Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 2443415
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x2f94065f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2443328
    const v1, 0x7f030e4d

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x3b043d69

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x2c7f4ca5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2443384
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2443385
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->l:LX/0Yb;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->l:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2443386
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->l:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 2443387
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->l:LX/0Yb;

    .line 2443388
    const/16 v1, 0x2b

    const v2, 0x7fff5194

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0xd558def

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2443377
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2443378
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 2443379
    if-eqz v1, :cond_0

    .line 2443380
    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1ZF;->k_(Z)V

    .line 2443381
    const v2, 0x7f0821fb

    invoke-interface {v1, v2}, LX/1ZF;->x_(I)V

    .line 2443382
    new-instance v2, LX/HFD;

    invoke-direct {v2, p0}, LX/HFD;-><init>(Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;)V

    invoke-interface {v1, v2}, LX/1ZF;->a(LX/63W;)V

    .line 2443383
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x3f598653

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2443329
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2443330
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->a:LX/HF3;

    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/HF3;->a(Ljava/lang/String;)LX/HFJ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->o:LX/HFJ;

    .line 2443331
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->j:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.intent.action.COVER_PHOTO_UPDATED"

    new-instance p1, LX/HFE;

    invoke-direct {p1, p0}, LX/HFE;-><init>(Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;)V

    invoke-interface {v0, v1, p1}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->l:LX/0Yb;

    .line 2443332
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->l:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 2443333
    const v0, 0x7f0d22dc

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->q:Lcom/facebook/fig/button/FigButton;

    .line 2443334
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->q:Lcom/facebook/fig/button/FigButton;

    const v1, 0x7f082221

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setText(I)V

    .line 2443335
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->q:Lcom/facebook/fig/button/FigButton;

    new-instance v1, LX/HFF;

    invoke-direct {v1, p0}, LX/HFF;-><init>(Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2443336
    const v0, 0x7f0d0de8

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    .line 2443337
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2443338
    const v0, 0x7f0d22dd

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    .line 2443339
    new-instance v1, LX/HFG;

    invoke-direct {v1, p0}, LX/HFG;-><init>(Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2443340
    const v0, 0x7f0d22e4

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->r:Lcom/facebook/fig/button/FigButton;

    .line 2443341
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->r:Lcom/facebook/fig/button/FigButton;

    const v1, 0x7f08221e

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setText(I)V

    .line 2443342
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->r:Lcom/facebook/fig/button/FigButton;

    new-instance v1, LX/HFH;

    invoke-direct {v1, p0}, LX/HFH;-><init>(Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2443343
    const v0, 0x7f0d22df

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->m:Landroid/view/View;

    .line 2443344
    const v0, 0x7f0d22d0

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2443345
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2443346
    const v0, 0x7f0d22d1

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2443347
    const v1, 0x7f08221c

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2443348
    const v0, 0x7f0d22db

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2443349
    const v1, 0x7f08221f

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2443350
    const v0, 0x7f0d22de

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2443351
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->o:LX/HFJ;

    .line 2443352
    iget-object v1, v0, LX/HFJ;->l:Landroid/net/Uri;

    move-object v0, v1

    .line 2443353
    if-eqz v0, :cond_0

    .line 2443354
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->o:LX/HFJ;

    .line 2443355
    iget-object v1, v0, LX/HFJ;->l:Landroid/net/Uri;

    move-object v0, v1

    .line 2443356
    invoke-static {p0, v0}, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->a(Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;Landroid/net/Uri;)V

    .line 2443357
    :goto_0
    const v0, 0x7f0d22e2

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2443358
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->o:LX/HFJ;

    .line 2443359
    iget-object p1, v1, LX/HFJ;->k:Landroid/net/Uri;

    move-object v1, p1

    .line 2443360
    if-eqz v1, :cond_1

    .line 2443361
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->o:LX/HFJ;

    .line 2443362
    iget-object p1, v1, LX/HFJ;->k:Landroid/net/Uri;

    move-object v1, p1

    .line 2443363
    const-class p1, Lcom/facebook/pages/common/pagecreation/PageCreationProfilePictureFragment;

    invoke-static {p1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2443364
    :goto_1
    const v0, 0x7f0d04e8

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2443365
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->o:LX/HFJ;

    .line 2443366
    iget-object p1, v1, LX/HFJ;->b:Ljava/lang/String;

    move-object v1, p1

    .line 2443367
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2443368
    return-void

    .line 2443369
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0213d7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2443370
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2443371
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 2443372
    const/high16 v1, 0x40a00000    # 5.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 2443373
    iget-object v1, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0, v0, v0, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setPadding(IIII)V

    .line 2443374
    iget-object v0, p0, Lcom/facebook/pages/common/pagecreation/PageCreationCoverPhotoFragment;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const v1, 0x7f021389

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setBackgroundResource(I)V

    goto :goto_0

    .line 2443375
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const p1, 0x7f0213d1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2443376
    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method
