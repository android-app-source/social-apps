.class public Lcom/facebook/pages/common/brandedcontent/BrandedContentSelectionAdapter;
.super LX/1OM;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/Iwu;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/Ix1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2631095
    const-class v0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSelectionAdapter;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSelectionAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2631091
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2631092
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2631093
    iput-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSelectionAdapter;->b:LX/0Px;

    .line 2631094
    return-void
.end method


# virtual methods
.method public final C_(I)J
    .locals 2

    .prologue
    .line 2631090
    int-to-long v0, p1

    return-wide v0
.end method

.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2631087
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2631088
    const v1, 0x7f0301d7

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2631089
    new-instance v1, LX/Iwu;

    invoke-direct {v1, v0}, LX/Iwu;-><init>(Landroid/view/View;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 4

    .prologue
    .line 2631073
    check-cast p1, LX/Iwu;

    .line 2631074
    iget-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSelectionAdapter;->b:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;

    .line 2631075
    iget-object v1, p1, LX/Iwu;->l:Lcom/facebook/resources/ui/FbTextView;

    .line 2631076
    iget-object v2, v0, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2631077
    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2631078
    iget-object v1, v0, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;->d:Landroid/net/Uri;

    move-object v1, v1

    .line 2631079
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2631080
    iget-object v2, p1, LX/Iwu;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v3, Lcom/facebook/pages/common/brandedcontent/BrandedContentSelectionAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v1, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2631081
    iget-object v2, p1, LX/Iwu;->n:Landroid/widget/ImageView;

    .line 2631082
    iget-boolean v1, v0, Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;->g:Z

    move v1, v1

    .line 2631083
    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2631084
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    new-instance v1, LX/Iwt;

    invoke-direct {v1, p0, p2}, LX/Iwt;-><init>(Lcom/facebook/pages/common/brandedcontent/BrandedContentSelectionAdapter;I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2631085
    return-void

    .line 2631086
    :cond_0
    const/16 v1, 0x8

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2631072
    iget-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSelectionAdapter;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
