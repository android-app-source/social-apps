.class public Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# static fields
.field public static final p:Ljava/lang/String;


# instance fields
.field private q:Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2631096
    const-class v0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionActivity;->p:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2631097
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private d(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2631098
    if-nez p1, :cond_0

    .line 2631099
    new-instance v0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;

    invoke-direct {v0}, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionActivity;->q:Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;

    .line 2631100
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2631101
    const-string v1, "composer_target_data"

    invoke-virtual {p0}, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "composer_target_data"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2631102
    iget-object v1, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionActivity;->q:Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;

    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2631103
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d002f

    iget-object v2, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionActivity;->q:Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;

    invoke-virtual {v0, v1, v2}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2631104
    :goto_0
    return-void

    .line 2631105
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d002f

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;

    iput-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionActivity;->q:Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;

    goto :goto_0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2631106
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2631107
    const v0, 0x7f0301d9

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionActivity;->setContentView(I)V

    .line 2631108
    invoke-direct {p0, p1}, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionActivity;->d(Landroid/os/Bundle;)V

    .line 2631109
    return-void
.end method
