.class public Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public A:Landroid/widget/LinearLayout;

.field private final B:Landroid/text/TextWatcher;

.field public a:LX/9Wz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/9Wi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/Iws;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/pages/common/brandedcontent/protocol/PageUnit;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/lang/String;

.field public h:Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;

.field public i:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

.field public j:Landroid/support/v7/widget/RecyclerView;

.field public k:Lcom/facebook/pages/common/brandedcontent/BrandedContentSelectionAdapter;

.field public l:Landroid/widget/TextView;

.field public m:Lcom/facebook/resources/ui/FbTextView;

.field public n:Landroid/widget/LinearLayout;

.field public o:Lcom/facebook/resources/ui/FbTextView;

.field public p:Landroid/widget/LinearLayout;

.field public q:Landroid/widget/LinearLayout;

.field public r:Landroid/widget/LinearLayout;

.field public s:Lcom/facebook/widget/text/BetterTextView;

.field public t:Lcom/facebook/widget/text/BetterTextView;

.field public u:Lcom/facebook/widget/text/BetterTextView;

.field public v:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

.field public w:Lcom/facebook/resources/ui/FbButton;

.field public x:Lcom/facebook/resources/ui/FbButton;

.field public y:Landroid/widget/ImageView;

.field public z:Lcom/facebook/fbui/glyph/GlyphView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2631280
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2631281
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2631282
    iput-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->f:LX/0Px;

    .line 2631283
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->g:Ljava/lang/String;

    .line 2631284
    new-instance v0, LX/Iwy;

    invoke-direct {v0, p0}, LX/Iwy;-><init>(Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;)V

    iput-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->B:Landroid/text/TextWatcher;

    return-void
.end method

.method public static c(Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;)V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 2631207
    iget-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2631208
    iget-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2631209
    iget-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->p:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2631210
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 2631274
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2631275
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;

    new-instance p1, LX/9Wz;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    invoke-static {v0}, LX/2Sc;->a(LX/0QB;)LX/2Sc;

    move-result-object v5

    check-cast v5, LX/2Sc;

    invoke-direct {p1, v3, v4, v5}, LX/9Wz;-><init>(LX/0tX;LX/1Ck;LX/2Sc;)V

    move-object v3, p1

    check-cast v3, LX/9Wz;

    new-instance v5, LX/9Wi;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-direct {v5, v4}, LX/9Wi;-><init>(LX/0tX;)V

    move-object v4, v5

    check-cast v4, LX/9Wi;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/Executor;

    invoke-static {v0}, LX/Iws;->a(LX/0QB;)LX/Iws;

    move-result-object p1

    check-cast p1, LX/Iws;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    iput-object v3, v2, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->a:LX/9Wz;

    iput-object v4, v2, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->b:LX/9Wi;

    iput-object v5, v2, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->c:Ljava/util/concurrent/Executor;

    iput-object p1, v2, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->d:LX/Iws;

    iput-object v0, v2, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->e:LX/0Uh;

    .line 2631276
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2631277
    const-string v1, "composer_target_data"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iput-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->v:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 2631278
    new-instance v0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSelectionAdapter;

    invoke-direct {v0}, Lcom/facebook/pages/common/brandedcontent/BrandedContentSelectionAdapter;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->k:Lcom/facebook/pages/common/brandedcontent/BrandedContentSelectionAdapter;

    .line 2631279
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x203fddbb

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2631285
    const v1, 0x7f0301d4

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x3c3d529c

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x6ccdde24

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2631271
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2631272
    iget-object v1, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->h:Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;

    iget-object v2, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->B:Landroid/text/TextWatcher;

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2631273
    const/16 v1, 0x2b

    const v2, 0x12f34c43

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 2631211
    const/4 p2, 0x0

    .line 2631212
    const v0, 0x7f0d0761

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->A:Landroid/widget/LinearLayout;

    .line 2631213
    iget-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->e:LX/0Uh;

    const/16 v1, 0x379

    invoke-virtual {v0, v1, p2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2631214
    iget-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->A:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2631215
    const v0, 0x7f0d0763

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->z:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2631216
    iget-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->z:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v1, LX/Ix0;

    invoke-direct {v1, p0}, LX/Ix0;-><init>(Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2631217
    :cond_0
    const v0, 0x7f0d075f

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    iput-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->i:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 2631218
    const v0, 0x7f0d0768

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->o:Lcom/facebook/resources/ui/FbTextView;

    .line 2631219
    const v0, 0x7f0d0765

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->n:Landroid/widget/LinearLayout;

    .line 2631220
    const v0, 0x7f0d0766

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->l:Landroid/widget/TextView;

    .line 2631221
    const v0, 0x7f08398e

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    .line 2631222
    iget-object v1, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->l:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2631223
    const v0, 0x7f0d0767

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->m:Lcom/facebook/resources/ui/FbTextView;

    .line 2631224
    const v0, 0x7f0d0760

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;

    iput-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->h:Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;

    .line 2631225
    const v0, 0x7f0d0769

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->j:Landroid/support/v7/widget/RecyclerView;

    .line 2631226
    const v0, 0x7f0d077f

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->p:Landroid/widget/LinearLayout;

    .line 2631227
    const v0, 0x7f0d0781

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->s:Lcom/facebook/widget/text/BetterTextView;

    .line 2631228
    const v0, 0x7f0d0782

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->t:Lcom/facebook/widget/text/BetterTextView;

    .line 2631229
    const v0, 0x7f0d0776

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->q:Landroid/widget/LinearLayout;

    .line 2631230
    const v0, 0x7f0d077a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->w:Lcom/facebook/resources/ui/FbButton;

    .line 2631231
    const v0, 0x7f0d076a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->r:Landroid/widget/LinearLayout;

    .line 2631232
    const v0, 0x7f0d0775

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->x:Lcom/facebook/resources/ui/FbButton;

    .line 2631233
    const v0, 0x7f0d075e

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->y:Landroid/widget/ImageView;

    .line 2631234
    const v0, 0x7f0d0780

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->u:Lcom/facebook/widget/text/BetterTextView;

    .line 2631235
    iget-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->u:Lcom/facebook/widget/text/BetterTextView;

    const v1, 0x7f0839a2

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2631236
    iget-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->k:Lcom/facebook/pages/common/brandedcontent/BrandedContentSelectionAdapter;

    new-instance v1, LX/Ix1;

    invoke-direct {v1, p0}, LX/Ix1;-><init>(Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;)V

    .line 2631237
    iput-object v1, v0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSelectionAdapter;->c:LX/Ix1;

    .line 2631238
    iget-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->w:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/Ix2;

    invoke-direct {v1, p0}, LX/Ix2;-><init>(Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2631239
    iget-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->x:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/Ix3;

    invoke-direct {v1, p0}, LX/Ix3;-><init>(Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2631240
    iget-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->y:Landroid/widget/ImageView;

    new-instance v1, LX/Ix4;

    invoke-direct {v1, p0}, LX/Ix4;-><init>(Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2631241
    iget-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->d:LX/Iws;

    .line 2631242
    iget-object v1, v0, LX/Iws;->a:LX/0if;

    sget-object v2, LX/0ig;->S:LX/0ih;

    invoke-virtual {v1, v2}, LX/0if;->a(LX/0ih;)V

    .line 2631243
    iget-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->v:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->v:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-object v0, v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object v1, LX/2rw;->PAGE:LX/2rw;

    if-ne v0, v1, :cond_3

    .line 2631244
    iget-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->d:LX/Iws;

    iget-object v1, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->v:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-wide v2, v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    .line 2631245
    iput-wide v2, v0, LX/Iws;->b:J

    .line 2631246
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->v:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->v:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-object v0, v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object v1, LX/2rw;->PAGE:LX/2rw;

    if-eq v0, v1, :cond_2

    .line 2631247
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 2631248
    iget-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->b:LX/9Wi;

    .line 2631249
    new-instance v1, LX/9Wo;

    invoke-direct {v1}, LX/9Wo;-><init>()V

    move-object v1, v1

    .line 2631250
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    sget-object v2, LX/0zS;->c:LX/0zS;

    invoke-virtual {v1, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    const/4 v2, 0x1

    .line 2631251
    iput-boolean v2, v1, LX/0zO;->p:Z

    .line 2631252
    move-object v1, v1

    .line 2631253
    iget-object v2, v0, LX/9Wi;->a:LX/0tX;

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    move-object v0, v1

    .line 2631254
    new-instance v1, LX/Ix7;

    invoke-direct {v1, p0}, LX/Ix7;-><init>(Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;)V

    iget-object v2, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->c:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2631255
    :cond_2
    iget-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->l:Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0420

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLinkTextColor(I)V

    .line 2631256
    iget-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->l:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2631257
    iget-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->u:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0420

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setLinkTextColor(I)V

    .line 2631258
    iget-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->u:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2631259
    iget-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->m:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f083999

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2631260
    iget-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->i:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    const v1, 0x7f083987

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(I)V

    .line 2631261
    iget-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->i:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    new-instance v1, LX/Iwx;

    invoke-direct {v1, p0}, LX/Iwx;-><init>(Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a(Landroid/view/View$OnClickListener;)V

    .line 2631262
    iget-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->h:Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f083988

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 2631263
    new-instance v0, LX/1P1;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1P1;-><init>(Landroid/content/Context;)V

    .line 2631264
    iget-object v1, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->j:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2631265
    iget-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->j:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->k:Lcom/facebook/pages/common/brandedcontent/BrandedContentSelectionAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2631266
    return-void

    .line 2631267
    :cond_3
    iget-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->v:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->v:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-object v0, v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object v1, LX/2rw;->UNDIRECTED:LX/2rw;

    if-ne v0, v1, :cond_1

    .line 2631268
    iget-object v0, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->d:LX/Iws;

    iget-object v1, p0, Lcom/facebook/pages/common/brandedcontent/BrandedContentSuggestionAndSelectionFragment;->v:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-wide v2, v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    .line 2631269
    iput-wide v2, v0, LX/Iws;->c:J

    .line 2631270
    goto/16 :goto_0
.end method
