.class public final Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment$DeleteStoryEventSubscriber$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements Ljava/util/concurrent/Callable;


# instance fields
.field public final synthetic a:LX/BPT;

.field public final synthetic b:LX/HP9;


# direct methods
.method public constructor <init>(LX/HP9;LX/BPT;)V
    .locals 0

    .prologue
    .line 2461310
    iput-object p1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment$DeleteStoryEventSubscriber$1;->b:LX/HP9;

    iput-object p2, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment$DeleteStoryEventSubscriber$1;->a:LX/BPT;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2461311
    new-instance v0, Lcom/facebook/api/feed/DeleteStoryMethod$Params;

    iget-object v1, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment$DeleteStoryEventSubscriber$1;->a:LX/BPT;

    iget-object v1, v1, LX/BPT;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment$DeleteStoryEventSubscriber$1;->a:LX/BPT;

    iget-object v2, v2, LX/BPT;->b:Ljava/lang/String;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment$DeleteStoryEventSubscriber$1;->a:LX/BPT;

    iget-object v3, v3, LX/BPT;->c:Ljava/lang/String;

    sget-object v4, LX/55G;->LOCAL_AND_SERVER:LX/55G;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/facebook/api/feed/DeleteStoryMethod$Params;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;LX/55G;)V

    .line 2461312
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2461313
    const-string v1, "operationParams"

    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2461314
    iget-object v0, p0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment$DeleteStoryEventSubscriber$1;->b:LX/HP9;

    iget-object v0, v0, LX/HP9;->a:Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/surface/fragments/reaction/PagesGenericReactionSurfaceTabFragment;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0aG;

    const-string v1, "timeline_delete_story"

    sget-object v3, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, -0x6d421151

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    return-object v0
.end method
